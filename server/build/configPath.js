'use strict';

var path = require('path');

var base = path.normalize(__dirname + '/');
var common_paths = {
	user_pic_absolute: '/upload/user_pic/',
	user_pic_relative: path.resolve(base + '../../client/build/upload/user_pic/'),
	postattachment_absolute: '/upload/postattachments/',
	postattachment_relative: path.resolve(base + '../../client/build/upload/postattachments/'),
	slide_relative: path.resolve(base + '../../client/build/upload/'),
	slide_absolute: '/upload/',
	grp_pic_relative: path.resolve(base + '../../client/build/upload/'),
	grp_pic_absolute: '/upload/',
	usr_material_file_relative: path.resolve(base + '../../client/resources/user-material/'),
	usr_material_file_absolute: '/resources/user-material/'
};

var api = {
	auth: {
		authMongo: base + 'api/auth/auth.mongo.api.js',
		authCass: base + 'api/auth/auth.cass.api.js'
	},
	chat: {
		friends: base + 'api/chat/friends.api.js',
		msgs: base + 'api/chat/msgs.api.js',
		grp: base + 'api/chat/grp.api.js',
		chatRedis: base + 'api/chat/chat_redis.api.js'
	},
	dashboard: {
		comment: base + 'api/dashboard/comment.api.js',
		post: base + 'api/dashboard/post.api.js',
		profile: base + 'api/dashboard/profile.api.js',
		stats: base + 'api/dashboard/stats.api.js'
	},
	kafka: {
		userstats: base + 'api/kafka/userstats.api.js'
	},

	mail: base + 'api/mail/mail.api.js',
	emailTemplateAPI: {
		registerTemplate: base + 'api/mail/registerTemplate.js',
		forgotpasswordTemplate: base + 'api/mail/forgotpasswordTemplate.js',
		resendotpTemplate: base + 'api/mail/resendotpTemplate.js',
		contactusTemplate: base + 'api/mail/contactusTemplate.js'
	},
	subscribe: {
		subscribe: base + 'api/mail/subscribe.api.js'
	},
	qa: {
		highlight: base + 'api/qa/highlight.api.js',
		practice: base + 'api/qa/practice.api.js',
		practiceCass: base + 'api/qa/practice.cass.api.js',
		createtest: base + 'api/qa/createtest.api.js'
	},
	usercourses: base + 'api/usercourses/usercourses.api.js',
	video: {
		videonote: base + 'api/video/videonote.api.js',
		videoentity: base + 'api/video/videoentity.api.js',
		videoviews: base + 'api/video/videoviews.api.js',
		videorating: base + 'api/video/videorating.api.js',
		videovotes: base + 'api/video/videovotes.api.js'
	},
	admin: {
		course: base + 'api/admin/course.api.js',
		courseModule: base + 'api/admin/courseModule.api.js',
		courseMaster: base + 'api/admin/courseMaster.api.js',
		courseVideos: base + 'api/admin/courseVideos.api.js',
		videoSlide: base + 'api/admin/videoSlide.api.js',
		author: base + 'api/admin/author.api.js',
		publication: base + 'api/admin/publication.api.js',
		book: base + 'api/admin/book.api.js',
		subject: base + 'api/admin/subject.api.js',
		exam: base + 'api/admin/exam.api.js',
		topic: base + 'api/admin/topic.api.js',
		subtopic: base + 'api/admin/subtopic.api.js',
		tag: base + 'api/admin/tag.api.js',
		moduleItems: base + 'api/admin/moduleItems.api.js',
		courseBundle: base + 'api/admin/courseBundle.api.js',
		subgroup: base + 'api/admin/subgroup.api.js',
		sideLinks: base + 'api/admin/sideLinks.api.js',
		allLinksCategory: base + 'api/admin/allLinksCategory.api.js',
		allLinks: base + 'api/admin/allLinks.api.js',
		emailTemplates: base + 'api/admin/emailTemplates.api.js',
		userEmailGroup: base + 'api/admin/userEmailGroup.api.js',
		sendEmailTemplates: base + 'api/admin/sendEmailTemplates.api.js',
		sendNotification: base + 'api/admin/sendNotification.api.js',
		roles: base + 'api/admin/roles.api.js',
		forms: base + 'api/admin/forms.api.js',
		userRoles: base + 'api/admin/userRoles.api.js',
		helpForm: base + 'api/admin/helpForm.api.js',
		userBatchEnroll: base + 'api/admin/userBatchEnroll.api.js',
		booktopic: base + 'api/admin/booktopic.api.js',
		jobOpenings: base + 'api/admin/jobOpenings.api.js',
		topicGroup: base + 'api/admin/topicGroup.api.js',
		boardCompetitive: base + 'api/admin/boardCompetitive.api.js',
		previousPapers: base + 'api/admin/previousPapers.api.js',
		general: base + 'api/admin/general.api.js'
	},

	forums: {
		forums: base + 'api/forums/forums.api.js',
		category: base + 'api/forums/category.api.js',
		follow: base + 'api/forums/follow.api.js',
		ans: base + 'api/forums/ans.api.js'
	},
	payment: {
		paypal: base + 'api/payment/paypal.js',
		payment: base + 'api/payment/payment.api.js',
		ccavenue: base + 'api/payment/ccavenue.api.js'
	},
	employee: {
		empTypes: base + 'api/employee/empTypes.api.js',
		empDetails: base + 'api/employee/empDetails.api.js',
		empSkills: base + 'api/employee/empSkills.api.js'
	},
	batchCourse: {
		batchCourse: base + 'api/batchCourse/batchCourse.api.js',
		batchTiming: base + 'api/batchCourse/batchTiming.api.js',
		batchTimingDashboard: base + 'api/batchCourse/batchTimingDashboard.api.js',
		currency: base + 'api/batchCourse/currency.api.js',
		gettingStarted: base + 'api/batchCourse/gettingStarted.api.js',
		sessions: base + 'api/batchCourse/sessions.api.js',
		sessionItems: base + 'api/batchCourse/sessionItems.api.js'
	},
	pricelist: {
		pricelist: base + 'api/pricelist/pricelist.api.js'
	},
	bundle: {
		bundle: base + 'api/bundle/bundle.api.js'
	},
	itemComments: {
		itemComments: base + 'api/itemComments/itemComments.api.js',
		itemQueries: base + 'api/itemComments/itemQueries.api.js'
	},
	aws: {
		s3: base + 'api/aws/s3.api.js'
	},
	job: {
		job: base + 'api/job/job.api.js'
	},
	sms: {
		sms: base + 'api/sms/sms.api.js'
	},
	book: {
		highlights: base + 'api/book/highlights.api.js'
	},
	notes: {
		notes: base + 'api/notes/notes.api.js'
	},
	authorizeitems: base + 'api/authorizeitems.api.js',
	bookmark: base + 'api/bookmark.api.js',
	globalFunctions: base + 'api/globalFunctions.js',
	videoUrl: base + 'api/videoUrl.js',
	websocket: base + 'api/websocket.api.js',
	videostats: base + 'api/videostats.api.js',
	contact: base + 'api/contact.api.js'
};

var lib = {
	utility: base + 'lib/utility.js',
	ccavutil: base + 'lib/ccavutil.js',
	log_to_file: base + 'lib/log_to_file.js',
	session: base + 'lib/session.js',
	passport: base + 'lib/passport.js',
	singleLogin: base + 'lib/single-login.js'
};

var dbconn = {
	cassconn: base + 'dbconn/cassconn.js',
	mongoconn: base + 'dbconn/mongoconn.js',
	oracledb: base + 'dbconn/oracledb.js',
	redisconn: base + 'dbconn/redisconn.js'
};

var config = {
	cloudFrontConfig: base + 'config/cloudfrontConfig.js',
	globalConfig: base + 'config/globalConfig.js',
	userErrors: base + 'config/userErrors.config.js',
	videoSourceConfig: base + 'config/videoSourceConfig.js',
	awsConfig: base + 'config/awsConfig.js',
	baseUrlConfig: base + 'config/baseUrlConfig.js',
	ccavConfig: base + 'config/ccavConfig.js',
	textLocalConfig: base + 'config/textLocalConfig.js'
};

var routes = {
	auth: base + 'routes/auth/auth.route.js',
	dashboard: {
		comment: base + 'routes/dashboard/comment.route.js',
		post: base + 'routes/dashboard/post.route.js',
		profile: base + 'routes/dashboard/profile.route.js',
		stats: base + 'routes/dashboard/stats.route.js'
	},
	chat: {
		friends: base + 'routes/chat/friends.route.js',
		msgs: base + 'routes/chat/msgs.route.js',
		grp: base + 'routes/chat/grp.route.js'
	},
	kafka: {
		userstats: base + 'routes/kafka/userstats.route.js'
	},
	qa_dir: {
		highlight: base + 'routes/qa/highlight.route.js',
		practice: base + 'routes/qa/practice.route.js',
		createtest: base + 'routes/qa/createtest.route.js'
	},
	usercourses: base + 'routes/usercourses/usercourses.route.js',
	video: {
		videonote: base + 'routes/video/videonote.route.js',
		videoentity: base + 'routes/video/videoentity.route.js',
		videoviews: base + 'routes/video/videoviews.route.js',
		videorating: base + 'routes/video/videorating.route.js',
		index: base + 'routes/video/index.js'
	},
	admin: base + 'routes/admin.js',
	admin_dir: {
		course: base + 'routes/admin/course.route.js',
		courseModule: base + 'routes/admin/courseModule.route.js',
		courseMaster: base + 'routes/admin/courseMaster.route.js',
		courseVideos: base + 'routes/admin/courseVideos.route.js',
		videoSlide: base + 'routes/admin/videoSlide.route.js',
		author: base + 'routes/admin/author.route.js',
		publication: base + 'routes/admin/publication.route.js',
		book: base + 'routes/admin/book.route.js',
		subject: base + 'routes/admin/subject.route.js',
		exam: base + 'routes/admin/exam.route.js',
		topic: base + 'routes/admin/topic.route.js',
		tag: base + 'routes/admin/tag.route.js',
		subtopic: base + 'routes/admin/subtopic.route.js',
		moduleItems: base + 'routes/admin/moduleItems.route.js',
		courseBundle: base + 'routes/admin/courseBundle.route.js',
		subgroup: base + 'routes/admin/subgroup.route.js',
		sideLinks: base + 'routes/admin/sideLinks.route.js',
		allLinksCategory: base + 'routes/admin/allLinksCategory.route.js',
		allLinks: base + 'routes/admin/allLinks.route.js',
		emailTemplates: base + 'routes/admin/emailTemplates.route.js',
		userEmailGroup: base + 'routes/admin/userEmailGroup.route.js',
		sendEmailTemplates: base + 'routes/admin/sendEmailTemplates.route.js',
		sendNotification: base + 'routes/admin/sendNotification.route.js',
		roles: base + 'routes/admin/roles.route.js',
		forms: base + 'routes/admin/forms.route.js',
		userRoles: base + 'routes/admin/userRoles.route.js',
		helpForm: base + 'routes/admin/helpForm.route.js',
		userBatchEnroll: base + 'routes/admin/userBatchEnroll.route.js',
		booktopic: base + 'routes/admin/booktopic.route.js',
		jobOpenings: base + 'routes/admin/jobOpenings.route.js',
		topicGroup: base + 'routes/admin/topicGroup.route.js',
		boardCompetitive: base + 'routes/admin/boardCompetitive.route.js',
		previousPapers: base + 'routes/admin/previousPapers.route.js',
		general: base + 'routes/admin/general.route.js'
	},
	forums: {
		forums: base + 'routes/forums/forums.route.js',
		category: base + 'routes/forums/category.route.js',
		follow: base + 'routes/forums/follow.route.js',
		ans: base + 'routes/forums/ans.route.js'
	},
	payment: {
		paypal: base + 'routes/payment/paypal.js',
		index: base + 'routes/payment/index.js',
		payment: base + 'routes/payment/payment.route.js'
	},
	employee: {
		empTypes: base + 'routes/employee/empTypes.routes.js',
		empDetails: base + 'routes/employee/empDetails.route.js',
		empSkills: base + 'routes/employee/empSkills.route.js',
		index: base + 'routes/employee/index.js'
	},
	batchCourse: {
		index: base + 'routes/batchCourse/index.js',
		batchCourse: base + 'routes/batchCourse/batchCourse.route.js',
		batchTiming: base + 'routes/batchCourse/batchTiming.route.js',
		batchTimingDashboard: base + 'routes/batchCourse/batchTimingDashboard.route.js',
		currency: base + 'routes/batchCourse/currency.route.js',
		gettingStarted: base + 'routes/batchCourse/gettingStarted.route.js',
		sessions: base + 'routes/batchCourse/sessions.route.js',
		sessionItems: base + 'routes/batchCourse/sessionItems.route.js'
	},
	pricelist: {
		index: base + 'routes/pricelist/index.js',
		pricelist: base + 'routes/pricelist/pricelist.route.js'
	},
	bundle: {
		index: base + 'routes/bundle/index.js',
		bundle: base + 'routes/bundle/bundle.route.js'
	},
	itemComments: {
		index: base + 'routes/itemComments/index.js',
		itemComments: base + 'routes/itemComments/itemComments.route.js',
		itemQueries: base + 'routes/itemComments/itemQueries.route.js'
	},
	aws: {
		s3: base + 'routes/aws/s3.route.js',
		index: base + 'routes/aws/index.js'
	},
	job: {
		job: base + 'routes/job/job.route.js',
		index: base + 'routes/job/index.js'
	},
	mail: {
		mail: base + 'routes/mail/mail.js'
	},
	sms: {
		sms: base + 'routes/sms/sms.route.js',
		index: base + 'routes/sms/index.js'
	},
	book: {
		highlights: base + 'routes/book/highlights.route.js',
		index: base + 'routes/book/index.js'
	},
	notes: {
		notes: base + 'routes/notes/notes.route.js',
		index: base + 'routes/notes/index.js'
	},
	authorize: base + 'routes/authorize.js',
	authorizeitems: base + 'routes/authorizeitems.route.js',
	bookmark: base + 'routes/bookmark.route.js',
	ejsRoutes: base + 'routes/ejsRoutes.js',
	mongoapi: base + 'routes/mongoapi.js',
	mongoconnect: base + 'routes/mongoconnect.js',
	qa: base + 'routes/qa.js',
	test: base + 'routes/test.js',
	users: base + 'routes/users.js',
	videoUrl: base + 'routes/videoUrl.js',
	videostats: base + 'routes/videostats.route.js',
	ckeditor: base + 'routes/ckeditor.js',
	contact: base + 'routes/contact.route.js'
};

var privateRoutes = ["/chat/friends/find"];

module.exports = {
	api: api,
	lib: lib,
	dbconn: dbconn,
	config: config,
	routes: routes,
	common_paths: common_paths,
	privateRoutes: privateRoutes
};