'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var shortid = require('shortid');
var Promise = require("bluebird");
var debug = require('debug')("app:api:aws:s3.api.js");
var fs = require('fs');
var path = require('path');

var awsConfig = require(configPath.config.awsConfig);
var AWS = require('aws-sdk');
AWS.config.region = 'ap-south-1';
var awsS3 = new AWS.S3();
awsS3 = Promise.promisifyAll(awsS3);

function S3() {}

S3.prototype.createBucket = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var bucketName, params, res;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						bucketName = awsConfig["bucket"][data.type];
						params = {
							Bucket: bucketName,
							CreateBucketConfiguration: {
								LocationConstraint: 'ap-south-1'
							}
						};
						_context.next = 5;
						return awsS3.createBucketAsync(params);

					case 5:
						res = _context.sent;

						debug(res);
						return _context.abrupt('return', res);

					case 10:
						_context.prev = 10;
						_context.t0 = _context['catch'](0);

						debug(_context.t0);
						throw _context.t0;

					case 14:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 10]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

S3.prototype.getBuckets = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var res;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						_context2.next = 3;
						return awsS3.listBucketsAsync();

					case 3:
						res = _context2.sent;

						debug(res);
						return _context2.abrupt('return', res);

					case 8:
						_context2.prev = 8;
						_context2.t0 = _context2['catch'](0);

						debug(_context2.t0);
						throw _context2.t0;

					case 12:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 8]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

S3.prototype.uploadFile = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(type, fileName, filePath, folderPath) {
		var bucketName, uploadParams, file, fileStream, res;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						bucketName = awsConfig["bucket"][type];
						uploadParams = {
							Bucket: bucketName,
							Key: '',
							Body: '',
							ACL: awsConfig["ACL"][type]
						};
						file = (folderPath || '') + filePath;
						fileStream = fs.createReadStream(file);


						debug(bucketName, type, fileName, filePath);

						fileStream.on('error', function (err) {
							console.log('File Error', err);
						});

						uploadParams.Body = fileStream;
						uploadParams.Key = fileName;

						_context3.next = 11;
						return awsS3.uploadAsync(uploadParams);

					case 11:
						res = _context3.sent;
						return _context3.abrupt('return', res);

					case 15:
						_context3.prev = 15;
						_context3.t0 = _context3['catch'](0);

						debug(_context3.t0);
						throw _context3.t0;

					case 19:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 15]]);
	}));

	return function (_x3, _x4, _x5, _x6) {
		return _ref3.apply(this, arguments);
	};
}();

S3.prototype.getSignedUrl = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(type, key) {
		var bucketName, expiryTime, params, url;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						bucketName = awsConfig["bucket"][type];
						expiryTime = awsConfig.expiryTime[type] || 3600;
						params = { Bucket: bucketName, Key: key, Expires: expiryTime };
						_context4.next = 6;
						return awsS3.getSignedUrlAsync('getObject', params);

					case 6:
						url = _context4.sent;
						return _context4.abrupt('return', url);

					case 10:
						_context4.prev = 10;
						_context4.t0 = _context4['catch'](0);

						debug(_context4.t0);
						throw _context4.t0;

					case 14:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 10]]);
	}));

	return function (_x7, _x8) {
		return _ref4.apply(this, arguments);
	};
}();

module.exports = new S3();