'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WebSocketServer = require('socket.io');
var shortid = require('shortid');
var debug = require('debug')('app:websocket');

var Q = require('q');

var configPath = require('../configPath.js');
var globalFunctions = require(configPath.api.globalFunctions);
var MsgsApi = require(configPath.api.chat.msgs);
var GrpApi = require(configPath.api.chat.grp);
var FrndApi = require(configPath.api.chat.friends);
var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);
var chatRedis = require(configPath.api.chat.chatRedis);
var sendStatsApi = require(configPath.api.kafka.userstats);

// var examwarrior_session = require(configPath.lib.session);

function WebSocket() {
    // logger.debug("WebSocket Started............");
    this.clickStatsRoomPrefix = "/clickStats/";
}

WebSocket.prototype.create = function (server) {
    this.wsServer = new WebSocketServer(server);
    // this.wsServer.use(function(socket, next) {
    //     examwarrior_session(socket.handshake, {}, next);
    // })
    this.clients = {};
    this.userInfo = {};
    this.rooms = {};
    this.clickStatsRoomPrefix = "/clickStats/";
    this.allRedisChannel = {};
    this.redisPublisher = getRedisClient("publisher");
    this.allWhiteBoardSockets = {};
    this.whiteboardRoomData = {};
    this.listen();
};

WebSocket.prototype.listen = function () {
    var self = this;

    self.wsServer.on('connection', function (socket) {
        var _this = this;

        // if (socket.handshake && socket.handshake.session && socket.handshake.session.save) {
        //     if(socket.handshake.session && socket.handshake.session.loginData){
        //         socket.handshake.session.socketId = socket.handshake.session.socketId || [];
        //         socket.handshake.session.socketId = socket.handshake.session.socketId.concat(socket.id)
        //         socket.handshake.session.save();    
        //     }
        // }


        logger.debug(socket.id + " " + new Date() + ' Connection accepted.');
        self.clients[socket.id] = socket;

        socket.on('userInfo', function (data) {
            //create room with userid 
            //and add socket to that room
            self.joinRoom(data.user_id, data.socketId);
            socket.usr_id = data.user_id;

            connectUserInfo();

            function connectUserInfo() {
                return chatRedis.setChatUserInfo(data).then(findUserFriend).then(findUserSocketsInfo).catch(globalFunctions.err);

                function findUserFriend(raw) {
                    var defer = Q.defer();
                    FrndApi.findFriends({
                        user_id: data.user_id
                    }).then(function (doc) {
                        var friendInfo = doc;
                        var frndLen = friendInfo.length;
                        var usr_Ids = [];

                        var usr_Info = [{
                            user_id: data.user_id,
                            display_name: data.display_name
                        }];

                        var frnd_usr_Ids = [];
                        //send data to friends
                        for (var i = 0; i < frndLen; i++) {
                            if (friendInfo[i].typ == "usr") {
                                frnd_usr_Ids.push(friendInfo[i].user_id);
                                self.sendMsgToRoom(friendInfo[i].user_id, "connectedSockets", {
                                    clients: usr_Info
                                });
                            }
                        }

                        var resolveData = {
                            frnd_usr_Ids: frnd_usr_Ids,
                            friendInfo: friendInfo
                        };

                        defer.resolve(resolveData);
                    });
                    return defer.promise;
                }

                function findUserSocketsInfo(frndData) {
                    var defer = Q.defer();

                    var frnd_usr_Ids = frndData.frnd_usr_Ids;
                    var friendInfo = frndData.friendInfo;

                    //find own online friends info
                    chatRedis.getUserSocketIds(frnd_usr_Ids).then(function (onlineFriendData) {

                        var frnd_Info = [];
                        var onLen = onlineFriendData.length;
                        for (var i = 0; i < onLen; i++) {
                            if (onlineFriendData[i].user_id == friendInfo[i].user_id && onlineFriendData[i].socketIds.length > 0) {
                                var o = {
                                    user_id: friendInfo[i].user_id,
                                    display_name: friendInfo[i].display_name
                                };
                                frnd_Info.push(o);
                            }
                        }

                        //send online friends data to own
                        self.sendMsgToRoom(data.user_id, "connectedSockets", {
                            clients: frnd_Info
                        });
                        defer.resolve([]);
                    });
                    return defer.promise;
                }
            }
        });
        socket.on('sendMsg', function (data) {
            MsgsApi.addMsg(data);
            //just send message to room and it will get sent to all socket into that room
            self.sendMsgToRoom(data.fr_id, 'newMsg', data);

            //send msg to self room also in case user has open multiple tabs
            var sender = data.user_id;
            self.sendMsgToRoom(sender, 'selfNewMsg', data);
        });
        socket.on('editMsg', function (data) {
            MsgsApi.editMsgs(data);
            //just send message to room and it will get sent to all socket into that room
            self.sendMsgToRoom(data.fr_id, 'newEditMsg', data);

            //send msg to self room also in case user has open multiple tabs
            var sender = data.user_id;
            self.sendMsgToRoom(sender, 'selfEditMsg', data);
        });
        socket.on('deleteMsg', function (data) {
            MsgsApi.deleteMsgs(data);
            //just send message to room and it will get sent to all socket into that room
            self.sendMsgToRoom(data.fr_id, 'newDeleteMsg', data);

            //send msg to self room also in case user has open multiple tabs
            var sender = data.user_id;
            self.sendMsgToRoom(sender, 'selfDeleteMsg', data);
        });
        socket.on('sendTypingMsg', function (data) {
            //just send message to room and it will get sent to all socket into that room
            self.sendMsgToRoom(data.fr_id, 'newTypingMsg', data);
        });

        socket.on('setUserChatActivityStatus', function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return chatRedis.setUserChatActivity(data);

                            case 2:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, _this);
            }));

            return function (_x) {
                return _ref.apply(this, arguments);
            };
        }());

        socket.on('setUserManualChatStatus', function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.next = 2;
                                return chatRedis.setUserManualStatus(data);

                            case 2:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, _this);
            }));

            return function (_x2) {
                return _ref2.apply(this, arguments);
            };
        }());

        socket.on('getUserChatStatus', function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var chatStatus;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.next = 2;
                                return chatRedis.getUserChatStatusToShow(data);

                            case 2:
                                chatStatus = _context3.sent;

                                self.sendMsgUsingSocketId(socket.id, "userChatStatus", chatStatus);

                            case 4:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, _this);
            }));

            return function (_x3) {
                return _ref3.apply(this, arguments);
            };
        }());

        socket.on('getFriendsStatus', function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var usersStatus;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.next = 2;
                                return chatRedis.getMultipleUsersChatStatusToShow(data);

                            case 2:
                                usersStatus = _context4.sent;

                                self.sendMsgUsingSocketId(socket.id, "friendStatus", usersStatus);

                            case 4:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, _this);
            }));

            return function (_x4) {
                return _ref4.apply(this, arguments);
            };
        }());

        socket.on('createGroup', function (data) {
            var roomId = data.room_id;
            GrpApi.findMembers({
                grp_id: data.room_id
            }).then(function (d) {
                var members = d;
                var user_Ids = members.map(function (v, i) {
                    return v["user_id"];
                });
                // find socket id of all group members
                chatRedis.getUserSocketIds(user_Ids).then(function (onlineFriendData) {
                    var onLen = onlineFriendData.length;
                    for (var i = 0; i < onLen; i++) {
                        if (onlineFriendData[i].socketIds.length > 0) {
                            var user_socketIds = onlineFriendData[i].socketIds;
                            user_socketIds.forEach(function (id) {
                                self.joinRoom(roomId, id);
                            });
                        }
                    }
                });
            }).catch(function (err) {
                logger.debug(err);
            });
        });
        socket.on('addToGroup', function (data) {
            var obj = self.rooms[data.groupId];
            self.joinRoom(data.groupId, data.socketId);
            self.sendMsgToRoom(data.groupId, 'newMemberAdded', obj);
        });
        socket.on('sendMsgToGroup', function (data) {
            logger.debug("Data msg : sendMsgToGroup", data);
            MsgsApi.addMsg(data);
            //fr_id means grp_id
            self.sendMsgToRoom(data.fr_id, 'newMsgInGroup', data);
        });

        socket.on('sendTypingMsgToGroup', function (data) {
            logger.debug("Data msg : sendTypingMsgToGroup", data);
            //fr_id means grp_id
            self.sendMsgToRoom(data.fr_id, 'newTypingMsgInGroup', data);
        });

        // socket.on('startAudioCall', function(req) {
        //  socket.broadcast.to('signal_room').emit('signaling_message', {
        //         type: req.type,
        //      message: req.message
        //     });
        // });

        socket.on('startAudioCallRequest', function (req) {
            var to_usr = req.to;
            var from_usr = req.from;
            logger.debug(req);

            // find socket id of form user
            chatRedis.getUserSocketIds([to_usr]).then(function (onlineFriendData) {
                var onLen = onlineFriendData.length;
                for (var i = 0; i < onLen; i++) {
                    if (onlineFriendData[i].socketIds.length > 0) {
                        var to_usr_info_socketIds = onlineFriendData[i].socketIds;
                        to_usr_info_socketIds.forEach(function (id) {
                            self.send(id, 'startAudioCallRequest', req);
                        });
                    }
                }
            });
        });

        socket.on('acceptedAudioCall', function (req) {
            var to_usr = req.to;
            var from_usr = req.from;

            logger.debug(req);
            var room_id = to_usr + "_" + from_usr;

            // find socket id of form and to usr
            chatRedis.getUserSocketIds([to_usr, from_usr]).then(function (onlineFriendData) {
                var onLen = onlineFriendData.length;
                for (var i = 0; i < onLen; i++) {
                    if (onlineFriendData[i].socketIds.length > 0 && to_usr == onlineFriendData[i].user_id) {
                        var to_usr_info_socketIds = onlineFriendData[i].socketIds;
                        to_usr_info_socketIds.forEach(function (id) {
                            self.joinRoom(room_id, id);
                        });
                    } else if (onlineFriendData[i].socketIds.length > 0 && to_usr == onlineFriendData[i].user_id) {
                        var from_usr_info = onlineFriendData[i].socketIds;
                        from_usr_info.forEach(function (id) {
                            self.joinRoom(room_id, id);
                            self.send(id, 'acceptedAudioCall', req);
                        });
                    }
                }
            });
        });

        socket.on('signal', function (req) {
            logger.debug("signal......", req);
            socket.broadcast.to(req.room).emit("signaling_message", {
                type: req.type,
                message: req.message
            });
        });

        socket.on('startVideoCallRequest', function (req) {
            var to_usr = req.to;
            var from_usr = req.from;
            logger.debug(req);

            // find socket id of form user
            chatRedis.getUserSocketIds([to_usr]).then(function (onlineFriendData) {
                var onLen = onlineFriendData.length;
                for (var i = 0; i < onLen; i++) {
                    if (onlineFriendData[i].socketIds.length > 0) {
                        var to_usr_info_socketIds = onlineFriendData[i].socketIds;
                        to_usr_info_socketIds.forEach(function (id) {
                            self.send(id, 'startVideoCallRequest', req);
                        });
                    }
                }
            });
        });

        socket.on('acceptedVideoCall', function (req) {
            var to_usr = req.to;
            var from_usr = req.from;
            logger.debug(req);
            var room_id = to_usr + "_" + from_usr;
            logger.debug("room_id.......", room_id);

            // find socket id of form and to usr
            chatRedis.getUserSocketIds([to_usr, from_usr]).then(function (onlineFriendData) {
                var onLen = onlineFriendData.length;
                for (var i = 0; i < onLen; i++) {
                    if (onlineFriendData[i].socketIds.length > 0 && to_usr == onlineFriendData[i].user_id) {
                        var to_usr_info_socketIds = onlineFriendData[i].socketIds;
                        to_usr_info_socketIds.forEach(function (id) {
                            self.joinRoom(room_id, id);
                        });
                    } else if (onlineFriendData[i].socketIds.length > 0 && to_usr == onlineFriendData[i].user_id) {
                        var from_usr_info = onlineFriendData[i].socketIds;
                        from_usr_info.forEach(function (id) {
                            self.joinRoom(room_id, id);
                            self.send(id, 'acceptedVideoCall', req);
                        });
                    }
                }
            });
        });

        socket.on("endCall", function (data) {
            socket.broadcast.to(data.room_id).emit("endCall");
        });

        socket.on("joinDrawRoom", function (data) {
            data.room_id = "whiteboard_" + data.room_id;
            self.rooms[data.room_id] = self.rooms[data.room_id] || {};
            self.rooms[data.room_id].members = self.rooms[data.room_id].members || [];
            self.sendMsgToRoom(data.room_id, "joining", {
                usr_id: data.usr_id,
                total: self.rooms[data.room_id].members.length + 1,
                timestamp: new Date()
            });
            self.allWhiteBoardSockets[socket.id] = {
                socket: socket,
                conenctedRoom: data.room_id,
                usr_id: data.usr_id
            };
            self.joinRoom(data.room_id, socket.id);
            if (self.whiteboardRoomData[data.room_id]) {
                for (var i = 0; i < self.whiteboardRoomData[data.room_id].length; i++) {
                    var d = self.whiteboardRoomData[data.room_id][i];
                    self.send(socket.id, "moving", d);
                }
            }
        });

        socket.on("mousemove", function (data) {
            data.room_id = "whiteboard_" + data.room_id;
            if (!self.whiteboardRoomData[data.room_id]) {
                self.whiteboardRoomData[data.room_id] = [];
                self.whiteboardRoomData[data.room_id].push(data);
            } else {
                self.whiteboardRoomData[data.room_id].push(data);
            }
            self.sendMsgToRoom(data.room_id, "moving", data);
        });

        socket.on('joinRoomForClickStats', function (data) {
            self.joinRoomForClickStats(data.usr_id, socket.id);
        });

        socket.on('leaveRoomForClickStats', function (data) {
            self.leaveRoomForClickStats(data.usr_id, socket.id);
        });

        socket.on('test_broadcast', function () {
            self.broadcast("test_broadcast", "from_server");
        });

        socket.on('test-redis-subscribe', function (channel_name) {
            logger.debug("Subscribed to redis");
            channel_name = "__key*__:*";

            if (self.allRedisChannel.hasOwnProperty(channel_name)) {
                //If channel is already present, make this socket connection one of its listeners
                self.allRedisChannel[channel_name].listeners[socket.id] = socket;
            } else {
                //Else, initialize new Redis Client as a channel and make it subscribe to channel_name
                self.allRedisChannel[channel_name].redis_client = getRedisClient("subscriber");
                self.allRedisChannel[channel_name].redis_client.psubscribe(channel_name);
                self.allRedisChannel[channel_name].listeners = {};
                //Add this connection to the listeners
                self.allRedisChannel[channel_name].listeners[socket.id] = socket;
                //Tell this new Redis client to send published messages to all of its listeners
                self.allRedisChannel[channel_name].redis_client.on('pmessage', function (channel, message) {
                    logger.debug("__key*__:* message", channel, message);
                    Object.keys(self.allRedisChannel[channel].listeners).forEach(function (key) {
                        self.allRedisChannel[channel].listeners[key].emit("redis-channel-msg", message);
                    });
                });
            }
        });

        socket.on('subscribe-practice-stats-channel', function (_ref5) {
            var channel_name = _ref5.channel_name,
                usr_id = _ref5.usr_id;

            var stats_data = {
                typ: "channel",
                channel_name: channel_name,
                action: "create",
                usr_id: usr_id
            };
            logger.debug(stats_data);
            sendStatsApi.sendStats({
                topic: "questionstats",
                stats: JSON.stringify(stats_data)
            });
            _this.subscribeRedisChannel(channel_name, socket);
        });

        socket.on('unsubscribe-practice-stats-channel', function (_ref6) {
            var channel_name = _ref6.channel_name,
                usr_id = _ref6.usr_id;

            _this.unsubscribeRedisChannel(channel_name, socket);
            if (_this.redisChannelNoOfListeners(channel_name) <= 0) {
                sendStatsApi.sendStats({
                    topic: "questionstats",
                    stats: JSON.stringify({
                        typ: "channel",
                        channel_name: channel_name,
                        action: "closed",
                        usr_id: usr_id
                    })
                });
            }
        });

        socket.on('subscribe-redis-channel', function (channel_name) {
            logger.debug("Subscribed to redis-channel..", channel_name);

            if (channel_name.indexOf("cnl_") === 0) {
                var stats_data = {
                    typ: "channel",
                    channel_name: channel_name,
                    action: "create",
                    usr_id: channel_name.substring(channel_name.indexOf("_") + 1)
                };
                logger.debug(stats_data);
                sendStatsApi.sendStats({
                    topic: "questionstats",
                    stats: JSON.stringify(stats_data)
                });
            }

            if (self.allRedisChannel.hasOwnProperty(channel_name)) {
                //If channel is already present, make this socket connection one of its listeners
                self.allRedisChannel[channel_name].listeners[socket.id] = socket;
            } else {
                //Else, initialize new Redis Client as a channel and make it subscribe to channel_name
                self.allRedisChannel[channel_name] = {};
                self.allRedisChannel[channel_name].redis_client = getRedisClient("subscriber-" + channel_name);
                self.allRedisChannel[channel_name].redis_client.subscribe(channel_name);
                self.allRedisChannel[channel_name].listeners = {};
                //Add this connection to the listeners
                self.allRedisChannel[channel_name].listeners[socket.id] = socket;
                // self.allRedisChannel[channel_name].redis_client.on('subscribe', function t(channel, count){
                //     logger.debug("channel sub:"+channel," count: " + count)
                // })
                // self.allRedisChannel[channel_name].redis_client.on('unsubscribe', function t(channel, count){
                //     logger.debug("channel unsub:"+channel," count: " + count)
                // })
                //Tell this new Redis client to send published messages to all of its listeners
                self.allRedisChannel[channel_name].redis_client.on('message', function t(channel, message) {
                    // logger.debug("channel:"+channel," message: " + message)
                    Object.keys(self.allRedisChannel[channel].listeners).forEach(function (key) {
                        self.allRedisChannel[channel].listeners[key].emit("redis-channel-msg", {
                            socketId: self.allRedisChannel[channel].listeners[key].id,
                            channel: channel,
                            message: message
                        });
                    });
                });
            }
        });

        socket.on('unsubscribe-redis-channel', function (channel_name) {
            logger.debug("Unsubscribed to redis-channel..", channel_name);
            if (self.allRedisChannel.hasOwnProperty(channel_name)) {
                delete self.allRedisChannel[channel_name].listeners[socket.id];
                var no_of_listeners = Object.keys(self.allRedisChannel[channel_name].listeners);
                if (no_of_listeners <= 0) {
                    self.allRedisChannel[channel_name].redis_client.unsubscribe(channel_name);
                    self.allRedisChannel[channel_name].redis_client.quit();
                    if (channel_name.indexOf("cnl_") === 0) {
                        sendStatsApi.sendStats({
                            topic: "questionstats",
                            stats: JSON.stringify({
                                typ: "channel",
                                channel_name: channel_name,
                                action: "closed",
                                usr_id: channel_name.substring(channel_name.indexOf("_") + 1)
                            })
                        });
                    }
                    delete self.allRedisChannel[channel_name];
                }
            }
        });

        socket.on('test-redis-get-clicks', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5() {
            var redisClient, data;
            return _regenerator2.default.wrap(function _callee5$(_context5) {
                while (1) {
                    switch (_context5.prev = _context5.next) {
                        case 0:
                            redisClient = getRedisClient("publisher");
                            _context5.next = 3;
                            return redisClient.getAsync("noOfClicks");

                        case 3:
                            data = _context5.sent;

                            socket.emit("test-redis-clicks", data);

                        case 5:
                        case 'end':
                            return _context5.stop();
                    }
                }
            }, _callee5, _this);
        })));

        socket.on('start-test-performance', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6() {
            var pub;
            return _regenerator2.default.wrap(function _callee6$(_context6) {
                while (1) {
                    switch (_context6.prev = _context6.next) {
                        case 0:
                            logger.debug("start-test-performance");
                            pub = getRedisClient("publisher");

                            pub.publish("start-test-performance", "");

                            // setInterval(async ()=>{
                            //     try{
                            //         var clicks = await pub.getAsync("total_count");
                            //         socket.emit("count-msg",parseInt(clicks));
                            //     }catch(err){
                            //         logger.debug("Redis start-test-performance error ...",err)
                            //     }
                            // },1000)

                        case 3:
                        case 'end':
                            return _context6.stop();
                    }
                }
            }, _callee6, _this);
        })));

        socket.on('disconnect', function () {
            logger.debug("Socket Disconnected...", socket.id);

            if (self.allWhiteBoardSockets[socket.id]) {
                var d = self.allWhiteBoardSockets[socket.id];
                //in case someone left from dashboard page
                self.sendMsgToRoom(d.conenctedRoom, "leftRoom", {
                    usr_id: d.usr_id,
                    total: self.rooms[d.conenctedRoom].members.length - 1,
                    timestamp: new Date()
                });
                self.leaveRoom(d.conenctedRoom, socket.id);
                if (self.rooms[d.conenctedRoom].members.length <= 0) {
                    //delete room data
                    self.whiteboardRoomData[d.conenctedRoom] = [];
                }
                delete self.allWhiteBoardSockets[socket.id];
            }

            delete self.clients[socket.id];

            removeChatUserInfo();

            function removeChatUserInfo() {
                return chatRedis.removeChatUserInfo({
                    user_id: socket.usr_id,
                    socketId: socket.id
                }).then(findUserSocketsInfo).then(findUserFriend).catch(globalFunctions.err);

                function findUserSocketsInfo(data) {
                    var defer = Q.defer();
                    var usr_Ids = [socket.usr_id];

                    chatRedis.getUserSocketIds(usr_Ids).then(function (onlineFriendData) {
                        defer.resolve(onlineFriendData[0].socketIds);
                    });

                    return defer.promise;
                }

                function findUserFriend(socketIds) {
                    var defer = Q.defer();

                    if (socketIds.length == 0) {
                        // find friend and send disconnect event
                        FrndApi.findFriends({
                            user_id: socket.usr_id
                        }).then(function (doc) {
                            var friendInfo = doc;
                            var frndLen = friendInfo.length;
                            var usr_Ids = [];

                            var usr_Info = {
                                user_id: socket.usr_id,
                                display_name: socket.display_name
                            };

                            //send data to friends
                            for (var i = 0; i < frndLen; i++) {
                                if (friendInfo[i].typ == "usr") {
                                    self.sendMsgToRoom(friendInfo[i].user_id, "disconnectedSockets", {
                                        client: usr_Info
                                    });
                                }
                            }
                        });
                    }

                    defer.resolve([]);

                    return defer.promise;
                }
            }
        });
    });
};

WebSocket.prototype.logoutMsgToOtherBrowser = function (usr_id, session_socket_ids) {
    var _this2 = this;

    var self = this;
    try {
        var userInfo = Object.assign({}, self.userInfo);
        if (userInfo) {
            var user = userInfo[usr_id];
            if (user.socketIds) {
                session_socket_ids = session_socket_ids || [];
                user.socketIds.map(function (v, i) {
                    if (session_socket_ids.indexOf(v) <= -1) {
                        _this2.send(v, "logoutMsg", {
                            msg: "Your session is expired."
                        });
                    }
                });
            }
        }
    } catch (err) {
        logger.debug("logoutMsgToOtherBrowser Err..", err);
    }
};

WebSocket.prototype.send = function (id, msg, data) {
    var self = this;
    if (self.clients[id]) {
        logger.debug("data.........", data);
        self.clients[id].emit(msg, data);
    }
};
WebSocket.prototype.broadcast = function (msg, data) {
    logger.debug("Broadcast........", msg);
    var self = this;
    self.wsServer.emit(msg, data);
};

WebSocket.prototype.subscribeRedisChannel = function (channel_name, socket) {
    if (self.allRedisChannel.hasOwnProperty(channel_name)) {
        //If channel is already present, make this socket connection one of its listeners
        self.allRedisChannel[channel_name].listeners[socket.id] = socket;
    } else {
        //Else, initialize new Redis Client as a channel and make it subscribe to channel_name
        self.allRedisChannel[channel_name] = {};
        self.allRedisChannel[channel_name].redis_client = getRedisClient("subscriber");
        self.allRedisChannel[channel_name].redis_client.subscribe(channel_name);
        self.allRedisChannel[channel_name].listeners = {};
        //Add this connection to the listeners
        self.allRedisChannel[channel_name].listeners[socket.id] = socket;
        //Tell this new Redis client to send published messages to all of its listeners
        self.allRedisChannel[channel_name].redis_client.on('message', function (channel, message) {
            Object.keys(self.allRedisChannel[channel].listeners).forEach(function (key) {
                self.allRedisChannel[channel].listeners[key].emit("redis-channel-msg", {
                    socketId: self.allRedisChannel[channel].listeners[key].id,
                    channel: channel,
                    message: message
                });
            });
        });
    }
};

WebSocket.prototype.unsubscribeRedisChannel = function (channel_name, socket) {
    if (self.allRedisChannel.hasOwnProperty(channel_name)) {
        delete self.allRedisChannel[channel_name].listeners[socket.id];
    }
};

WebSocket.prototype.redisChannelNoOfListeners = function (channel_name) {
    if (self.allRedisChannel.hasOwnProperty(channel_name)) {
        var no_of_listeners = Object.keys(self.allRedisChannel[channel_name].listeners);
        return no_of_listeners;
    }
    return 0;
};

WebSocket.prototype.joinRoom = function (roomId, socketId) {
    try {
        var self = this;
        // logger.debug("16.In join room func : " , roomId , "........Socket Id: ", socketId , " total sockets....",Object.keys(self.clients));
        //Join socket to room
        if (self.clients[socketId]) {
            logger.debug("join room: ", roomId, "........Socket Id: ", socketId);
            self.clients[socketId].join(roomId);
            //Add Socket to room array
            self.rooms[roomId] = self.rooms[roomId] || {};
            self.rooms[roomId].members = self.rooms[roomId].members || [];
            if (self.rooms[roomId].members.indexOf(socketId) <= -1) {
                logger.debug("adding scoketid : ", socketId, "........to room members: ", roomId);
                self.rooms[roomId].members.push(socketId);
            }
        }
    } catch (err) {
        logger.debug("err: ", err);
    }
};
WebSocket.prototype.leaveRoom = function (roomId, socketId) {
    var self = this;
    if (self.clients[socketId]) {
        logger.debug("leave room: ", roomId, "........Socket Id: ", socketId);
        //leave room
        self.clients[socketId].leave(roomId);
        //find position of socket id in room members
        var pos = self.rooms[roomId].members.indexOf(socketId);
        //remove socket id from room sockets array
        self.rooms[roomId].members.splice(pos, 1);
    }
};

WebSocket.prototype.sendMsgToRoom = function (roomId, msg, data) {
    var self = this;
    try {
        // logger.debug("sendMsgToRoom : ", roomId, " , msg: ", msg, " ,data: ", data);
    } catch (err) {
        logger.debug(err);
    }

    self.wsServer.to(roomId).emit(msg, data);
};

WebSocket.prototype.sendMsgUsingSocketId = function (socketId, msg, data) {
    var self = this;
    self.wsServer.to(socketId).emit(msg, data);
};

WebSocket.prototype.joinRoomForClickStats = function (userIdToTrack, trackerSocketId) {
    var self = this;
    self.joinRoom(this.clickStatsRoomPrefix + userIdToTrack, trackerSocketId);
};

WebSocket.prototype.leaveRoomForClickStats = function (userIdToTrack, trackerSocketId) {
    var self = this;
    self.leaveRoom(this.clickStatsRoomPrefix + userIdToTrack, trackerSocketId);
};

WebSocket.prototype.sendClickStatsOfUser = function (usr_id, data) {
    var self = this;
    logger.debug("sendClickStatsOfUser : ", this.clickStatsRoomPrefix + usr_id, " , msg:clickstats ", " ,data: ", data);
    self.sendMsgToRoom(this.clickStatsRoomPrefix + usr_id, "clickstats", data);
};

WebSocket.prototype.sendMsgFromRethinkDB = function (msg, data) {
    var self = this;
    logger.debug("sendMsgFromRethinkDB : msg: ", msg, " ,data: ", data);
    self.wsServer.emit(msg, data);
};

WebSocket.prototype.sendMsgToRedisChannel = function (msg, data) {
    var self = this;
    this.redisPublisher.publish("redis-channel", data);
    logger.debug("sendMsgToRedisChannel : msg: ", msg, " ,data: ", data);
};

var singleton = new WebSocket();

module.exports = singleton;