'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var uuid = require('uuid');
var debug = require('debug')("app:api:bundle.api.js");

var Q = require('q');

function Bundle() {}

Bundle.prototype.getAll = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
    var connection, collection, find_res, find;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    find = function find(col) {
                        return col.find({}, { _id: 0 }).toArray();
                    };

                    _context.prev = 1;
                    _context.next = 4;
                    return getConnection();

                case 4:
                    connection = _context.sent;
                    _context.next = 7;
                    return dbCollection(connection);

                case 7:
                    collection = _context.sent;
                    _context.next = 10;
                    return find(collection);

                case 10:
                    find_res = _context.sent;
                    return _context.abrupt('return', find_res);

                case 14:
                    _context.prev = 14;
                    _context.t0 = _context['catch'](1);

                    globalFunctions.err(_context.t0);
                    throw _context.t0;

                case 18:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this, [[1, 14]]);
}));

Bundle.prototype.add = function (data) {
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        //insert/update in batchcourse
        var bndl_id = data.bndl_id || uuid.v4();
        data.bndl_id = bndl_id;

        return col.update({ bndl_id: bndl_id }, data, { upsert: true, w: 1 });
    }
};
Bundle.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ bndl_id: data.bndl_id });
    }
};

Bundle.prototype.getFullBundleDetailById = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var defer, bndl_id, connection, collection, res, crs_ids, res_crs, find;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        find = function find(col) {
                            return col.findOne({ bndl_id: bndl_id }, { _id: 0 });
                        };

                        defer = Q.defer();
                        bndl_id = data.bndl_id;
                        _context2.prev = 3;
                        _context2.next = 6;
                        return getConnection();

                    case 6:
                        connection = _context2.sent;
                        _context2.next = 9;
                        return dbCollection(connection);

                    case 9:
                        collection = _context2.sent;
                        _context2.next = 12;
                        return find(collection);

                    case 12:
                        res = _context2.sent;
                        crs_ids = res.includedCourses;
                        _context2.next = 16;
                        return courseListAPI.getCoursesById({ ids: crs_ids });

                    case 16:
                        res_crs = _context2.sent;

                        res["courses"] = res_crs;
                        defer.resolve(res);
                        _context2.next = 24;
                        break;

                    case 21:
                        _context2.prev = 21;
                        _context2.t0 = _context2['catch'](3);

                        defer.reject(_context2.t0);

                    case 24:
                        return _context2.abrupt('return', defer.promise);

                    case 25:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[3, 21]]);
    }));

    return function (_x) {
        return _ref2.apply(this, arguments);
    };
}();

//Here you can pass multiple bundles
Bundle.prototype.getFullBundlesDetail = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var defer, bndl_ids, res, i, bndl_dtl, find;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        find = function find(col) {
                            return col.findOne({ bndl_id: bndl_id }, { _id: 0 });
                        };

                        defer = Q.defer();
                        bndl_ids = data.bndl_ids;

                        console.log("getFullBundlesDetail..." + bndl_ids);
                        _context3.prev = 4;
                        res = [];
                        i = bndl_ids.length - 1;

                    case 7:
                        if (!(i >= 0)) {
                            _context3.next = 15;
                            break;
                        }

                        _context3.next = 10;
                        return this.getFullBundleDetailById({ bndl_id: bndl_ids[i] });

                    case 10:
                        bndl_dtl = _context3.sent;

                        res.push(bndl_dtl);

                    case 12:
                        i--;
                        _context3.next = 7;
                        break;

                    case 15:
                        defer.resolve(res);
                        _context3.next = 21;
                        break;

                    case 18:
                        _context3.prev = 18;
                        _context3.t0 = _context3['catch'](4);

                        defer.reject(_context3.t0);

                    case 21:
                        return _context3.abrupt('return', defer.promise);

                    case 22:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[4, 18]]);
    }));

    return function (_x2) {
        return _ref3.apply(this, arguments);
    };
}();

Bundle.prototype.getCoursesInBundle = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var bndl_id, conn, col, res;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        bndl_id = data.bndl_id;
                        _context4.prev = 1;
                        _context4.next = 4;
                        return getConnection();

                    case 4:
                        conn = _context4.sent;
                        _context4.next = 7;
                        return dbCollection(conn);

                    case 7:
                        col = _context4.sent;
                        _context4.next = 10;
                        return col.findOne({ bndl_id: bndl_id }, { _id: 0 });

                    case 10:
                        res = _context4.sent;

                        res = res || {};
                        res.includedCourses = res.includedCourses || [];
                        return _context4.abrupt('return', res.includedCourses);

                    case 16:
                        _context4.prev = 16;
                        _context4.t0 = _context4['catch'](1);

                        debug(_context4.t0);
                        throw _context4.t0;

                    case 21:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[1, 16]]);
    }));

    return function (_x3) {
        return _ref4.apply(this, arguments);
    };
}();

Bundle.prototype.getBundlesByCourseId = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
        var crs_id, conn, col, res;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        crs_id = data.crs_id;
                        _context5.prev = 1;
                        _context5.next = 4;
                        return getConnection();

                    case 4:
                        conn = _context5.sent;
                        _context5.next = 7;
                        return dbCollection(conn);

                    case 7:
                        col = _context5.sent;
                        _context5.next = 10;
                        return col.find({ includedCourses: crs_id }, { _id: 0 }).toArray();

                    case 10:
                        res = _context5.sent;
                        return _context5.abrupt('return', res);

                    case 14:
                        _context5.prev = 14;
                        _context5.t0 = _context5['catch'](1);

                        debug(_context5.t0);
                        throw _context5.t0;

                    case 19:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this, [[1, 14]]);
    }));

    return function (_x4) {
        return _ref5.apply(this, arguments);
    };
}();

function dbCollection(db) {
    return db.collection("bundle");
}

module.exports = new Bundle();