'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videoviews');
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var ew_vdo_vws_tbl = "ew1.ew_vdo_vws";

function VideoViewsApi() {}

//pass user id to get all video notes added by that user
VideoViewsApi.prototype.incrementVideoViews = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var vdo_id, usr_id, qry, params, res;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        vdo_id = data.vdo_id;
                        usr_id = data.usr_id;
                        qry = "update " + ew_vdo_vws_tbl + " set vw_num = vw_num + 1 where vdo_id=?";
                        params = [vdo_id];
                        _context.next = 7;
                        return cassExecute(qry, params);

                    case 7:
                        res = _context.sent;
                        _context.prev = 8;

                        if (!usr_id) {
                            _context.next = 12;
                            break;
                        }

                        _context.next = 12;
                        return dashboardStatsAPI.incrementVideoViews({ usr_id: usr_id });

                    case 12:
                        _context.next = 17;
                        break;

                    case 14:
                        _context.prev = 14;
                        _context.t0 = _context['catch'](8);

                        debug(_context.t0);

                    case 17:
                        return _context.abrupt('return', res);

                    case 20:
                        _context.prev = 20;
                        _context.t1 = _context['catch'](0);

                        debug(_context.t1);
                        throw _context.t1;

                    case 24:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 20], [8, 14]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

VideoViewsApi.prototype.getVideoViews = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var vdo_id, qry, params, res;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        vdo_id = data.vdo_id;
                        qry = "select * from " + ew_vdo_vws_tbl + " where vdo_id=?";
                        params = [vdo_id];
                        _context2.next = 6;
                        return cassExecute(qry, params);

                    case 6:
                        res = _context2.sent;
                        return _context2.abrupt('return', res);

                    case 10:
                        _context2.prev = 10;
                        _context2.t0 = _context2['catch'](0);

                        debug(_context2.t0);
                        throw _context2.t0;

                    case 14:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 10]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

module.exports = new VideoViewsApi();