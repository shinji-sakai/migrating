'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var shortId = require('shortid');
var courseApi = require(configPath.api.admin.course);
var logger = require(configPath.lib.log_to_file);
var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_last_vw_vdo_dt_table = "ew1.ew_last_vw_vdo_dt";

var RELATED_VIDEOS_FETCH_LIMIT = 3;

function VideoEntityApi() {}

VideoEntityApi.prototype.updateVideoEntity = function (data) {
    return getConnection().then(getCollection).then(updateVideoEntity).catch(globalFunctions.err);

    function updateVideoEntity(col) {
        if (!data.videoId) {
            data.videoId = shortId.generate();
        }
        return col.update({
            videoId: data.videoId
        }, data, {
            upsert: true
        });
    }
};
VideoEntityApi.prototype.removeVideoEntity = function (data) {
    return getConnection().then(getCollection).then(removeVideoEntity).catch(globalFunctions.err);

    function removeVideoEntity(col) {
        return col.remove({
            videoId: data.videoId
        });
    }
};
VideoEntityApi.prototype.getAllVideoEntity = function (data) {
    return getConnection().then(getCollection).then(getAll).catch(globalFunctions.err);

    function getAll(col) {
        return col.find({}).toArray();
    }
};
//
//
VideoEntityApi.prototype.findVideoEntityByQuery = function (data) {
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if (data[key].length > 0) {
                data[key] = {
                    $in: data[key]
                };
            } else {
                delete data[key];
            }
        }
    }

    logger.debug(data);

    return getConnection().then(getCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find(data).toArray();
    }
};

VideoEntityApi.prototype.findVideoEntityById = function (data) {
    return getConnection().then(getCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({
            videoId: data.videoId
        }).toArray();
    }
};

VideoEntityApi.prototype.findVideoEntitiesById = function (data) {
    return getConnection().then(getCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({
            videoId: {
                $in: data.videosId
            }
        }).toArray();
    }
};

VideoEntityApi.prototype.isVideoFilenameExist = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var connection, col, rows;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        _context.next = 3;
                        return getConnection();

                    case 3:
                        connection = _context.sent;
                        _context.next = 6;
                        return getCollection(connection);

                    case 6:
                        col = _context.sent;
                        _context.next = 9;
                        return col.find({
                            videoFilename: data.videoFilename
                        }).toArray();

                    case 9:
                        rows = _context.sent;

                        if (!(rows.length > 0)) {
                            _context.next = 14;
                            break;
                        }

                        return _context.abrupt('return', {
                            exist: true
                        });

                    case 14:
                        return _context.abrupt('return', {
                            exist: false
                        });

                    case 15:
                        _context.next = 20;
                        break;

                    case 17:
                        _context.prev = 17;
                        _context.t0 = _context['catch'](0);
                        throw _context.t0;

                    case 20:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 17]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

VideoEntityApi.prototype.getRelatedVideosByVideoId = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var startIndex, connection, collection, find_res, relatedVideos, fullVideosInfo;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        startIndex = data.startIndex;

                        startIndex = startIndex || 0;
                        _context2.next = 5;
                        return getConnection();

                    case 5:
                        connection = _context2.sent;
                        _context2.next = 8;
                        return getCollection(connection);

                    case 8:
                        collection = _context2.sent;
                        _context2.next = 11;
                        return collection.findOne({
                            videoId: data.videoId
                        });

                    case 11:
                        find_res = _context2.sent;

                        find_res = find_res || {};
                        console.log(find_res);
                        relatedVideos = find_res["relatedVideos"] || [];

                        if (!(relatedVideos.length > 0)) {
                            _context2.next = 23;
                            break;
                        }

                        relatedVideos = relatedVideos.splice(startIndex, RELATED_VIDEOS_FETCH_LIMIT);
                        _context2.next = 19;
                        return this.findVideoEntitiesById({
                            videosId: relatedVideos
                        });

                    case 19:
                        fullVideosInfo = _context2.sent;
                        return _context2.abrupt('return', fullVideosInfo || []);

                    case 23:
                        return _context2.abrupt('return', []);

                    case 24:
                        _context2.next = 30;
                        break;

                    case 26:
                        _context2.prev = 26;
                        _context2.t0 = _context2['catch'](0);

                        console.log(_context2.t0);
                        throw _context2.t0;

                    case 30:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 26]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

VideoEntityApi.prototype.getRelatedTopicsVideoByVideoId = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var startIndex, connection, collection, find_res, relatedTopics, fullVideosInfo;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        startIndex = data.startIndex;

                        startIndex = startIndex || 0;
                        _context3.next = 5;
                        return getConnection();

                    case 5:
                        connection = _context3.sent;
                        _context3.next = 8;
                        return getCollection(connection);

                    case 8:
                        collection = _context3.sent;
                        _context3.next = 11;
                        return collection.findOne({
                            videoId: data.videoId
                        });

                    case 11:
                        find_res = _context3.sent;
                        relatedTopics = find_res["relatedTopics"] || [];

                        if (!(relatedTopics.length > 0)) {
                            _context3.next = 21;
                            break;
                        }

                        _context3.next = 16;
                        return this.findVideoEntityByQuery({
                            topics: relatedTopics
                        });

                    case 16:
                        fullVideosInfo = _context3.sent;

                        fullVideosInfo = fullVideosInfo.splice(startIndex, RELATED_VIDEOS_FETCH_LIMIT);
                        return _context3.abrupt('return', fullVideosInfo || []);

                    case 21:
                        return _context3.abrupt('return', []);

                    case 22:
                        _context3.next = 27;
                        break;

                    case 24:
                        _context3.prev = 24;
                        _context3.t0 = _context3['catch'](0);
                        throw _context3.t0;

                    case 27:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 24]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

VideoEntityApi.prototype.getRelatedCoursesByVideoId = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var connection, collection, find_res, relatedCourses, fullCourses;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        _context4.next = 3;
                        return getConnection();

                    case 3:
                        connection = _context4.sent;
                        _context4.next = 6;
                        return getCollection(connection);

                    case 6:
                        collection = _context4.sent;
                        _context4.next = 9;
                        return collection.findOne({
                            videoId: data.videoId
                        });

                    case 9:
                        find_res = _context4.sent;
                        relatedCourses = find_res["relatedCourses"] || [];

                        if (!(relatedCourses.length > 0)) {
                            _context4.next = 18;
                            break;
                        }

                        _context4.next = 14;
                        return courseApi.getCoursesById({
                            ids: relatedCourses
                        });

                    case 14:
                        fullCourses = _context4.sent;
                        return _context4.abrupt('return', fullCourses || []);

                    case 18:
                        return _context4.abrupt('return', []);

                    case 19:
                        _context4.next = 24;
                        break;

                    case 21:
                        _context4.prev = 21;
                        _context4.t0 = _context4['catch'](0);
                        throw _context4.t0;

                    case 24:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 21]]);
    }));

    return function (_x4) {
        return _ref4.apply(this, arguments);
    };
}();

VideoEntityApi.prototype.updateVideoLastViewTime = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
        var user_id, crs_id, mdl_id, topic_id, last_vw_time, query, params;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _context5.prev = 0;
                        user_id = data.user_id, crs_id = data.crs_id, mdl_id = data.mdl_id, topic_id = data.topic_id, last_vw_time = data.last_vw_time;
                        query = 'update ' + ew_last_vw_vdo_dt_table + ' set last_vw_time=? where user_id=? and crs_id=? and mdl_id=? and topic_id=?';
                        params = [new Date(last_vw_time), user_id, crs_id, mdl_id, topic_id];
                        _context5.next = 6;
                        return cassExecute(query, params);

                    case 6:
                        return _context5.abrupt('return', data);

                    case 9:
                        _context5.prev = 9;
                        _context5.t0 = _context5['catch'](0);

                        logger.debug(_context5.t0);
                        throw new Error(_context5.t0);

                    case 13:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this, [[0, 9]]);
    }));

    return function (_x5) {
        return _ref5.apply(this, arguments);
    };
}();

VideoEntityApi.prototype.getVideoLastViewTime = function () {
    var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
        var user_id, crs_id, query, params, rows;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        _context6.prev = 0;
                        user_id = data.user_id, crs_id = data.crs_id;
                        query = 'select * from ' + ew_last_vw_vdo_dt_table + ' where user_id=? and crs_id=?';
                        params = [user_id, crs_id];
                        _context6.next = 6;
                        return cassExecute(query, params);

                    case 6:
                        rows = _context6.sent;
                        return _context6.abrupt('return', rows);

                    case 10:
                        _context6.prev = 10;
                        _context6.t0 = _context6['catch'](0);

                        logger.debug(_context6.t0);
                        throw new Error(_context6.t0);

                    case 14:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, this, [[0, 10]]);
    }));

    return function (_x6) {
        return _ref6.apply(this, arguments);
    };
}();

function getCollection(db) {
    var col = db.collection('videoentity');
    return col;
}

module.exports = new VideoEntityApi();