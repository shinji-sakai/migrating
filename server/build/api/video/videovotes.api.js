'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videovotes');

var ew_vdo_vws_tbl = "ew1.ew_vdo_vws";
var ew_vdo_rating_tbl = "ew1.ew_vdo_rating";

function VideoVotesApi() {}

//pass user id to get all video notes added by that user
VideoVotesApi.prototype.upvoteVideo = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var usr_id, vdo_id, upv_cnt, dwn_cnt, set_str, qry, params, qry_1, params_1;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        usr_id = data.usr_id;
                        vdo_id = data.vdo_id;
                        upv_cnt = data.upv_cnt;
                        dwn_cnt = data.dwn_cnt;
                        set_str = "upv_cnt = upv_cnt + " + upv_cnt + ",dwn_cnt = dwn_cnt + " + dwn_cnt;
                        qry = "update " + ew_vdo_vws_tbl + " set " + set_str + " where vdo_id=?";
                        params = [vdo_id];
                        _context.next = 10;
                        return cassExecute(qry, params);

                    case 10:
                        qry_1 = "update " + ew_vdo_rating_tbl + " set upv=1,dnv=0  where vdo_id=? and usr_id=?";
                        params_1 = [vdo_id, usr_id];
                        _context.next = 14;
                        return cassExecute(qry_1, params_1);

                    case 14:
                        return _context.abrupt('return', data);

                    case 17:
                        _context.prev = 17;
                        _context.t0 = _context['catch'](0);

                        debug(_context.t0);
                        throw _context.t0;

                    case 21:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 17]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

VideoVotesApi.prototype.downvoteVideo = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var usr_id, vdo_id, upv_cnt, dwn_cnt, set_str, qry, params, qry_1, params_1;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        usr_id = data.usr_id;
                        vdo_id = data.vdo_id;
                        upv_cnt = data.upv_cnt;
                        dwn_cnt = data.dwn_cnt;
                        set_str = "upv_cnt = upv_cnt + " + upv_cnt + ",dwn_cnt = dwn_cnt + " + dwn_cnt;
                        qry = "update " + ew_vdo_vws_tbl + " set " + set_str + " where vdo_id=?";
                        params = [vdo_id];
                        _context2.next = 10;
                        return cassExecute(qry, params);

                    case 10:
                        qry_1 = "update " + ew_vdo_rating_tbl + " set upv=0,dnv=1  where vdo_id=? and usr_id=?";
                        params_1 = [vdo_id, usr_id];
                        _context2.next = 14;
                        return cassExecute(qry_1, params_1);

                    case 14:
                        return _context2.abrupt('return', data);

                    case 17:
                        _context2.prev = 17;
                        _context2.t0 = _context2['catch'](0);

                        debug(_context2.t0);
                        throw _context2.t0;

                    case 21:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 17]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

// 
VideoVotesApi.prototype.getVideoVotes = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var usr_id, vdo_id, cnt_qry, cnt_params, cnt_res, bool_res, qry_1, params_1, obj;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        usr_id = data.usr_id;
                        vdo_id = data.vdo_id;
                        cnt_qry = "select  * from " + ew_vdo_vws_tbl + " where vdo_id=?";
                        cnt_params = [vdo_id];
                        _context3.next = 7;
                        return cassExecute(cnt_qry, cnt_params);

                    case 7:
                        cnt_res = _context3.sent;

                        cnt_res = cnt_res[0] || {};

                        bool_res = {};

                        if (!usr_id) {
                            _context3.next = 17;
                            break;
                        }

                        qry_1 = "select * from " + ew_vdo_rating_tbl + " where vdo_id=? and usr_id=?";
                        params_1 = [vdo_id, usr_id];
                        _context3.next = 15;
                        return cassExecute(qry_1, params_1);

                    case 15:
                        bool_res = _context3.sent;

                        bool_res = bool_res[0] || {};

                    case 17:
                        obj = {
                            usr_id: usr_id,
                            vdo_id: vdo_id,
                            upv_cnt: cnt_res.upv_cnt,
                            dwn_cnt: cnt_res.dwn_cnt,
                            upv: bool_res.upv,
                            dwn: bool_res.dnv
                        };
                        return _context3.abrupt('return', obj);

                    case 21:
                        _context3.prev = 21;
                        _context3.t0 = _context3['catch'](0);

                        debug(_context3.t0);
                        throw _context3.t0;

                    case 25:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 21]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

module.exports = new VideoVotesApi();