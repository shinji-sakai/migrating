'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videonotes');
var authorizeitemsAPI = require(configPath.api.authorizeitems);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var ew_vdo_note_mdls_tbl = "ew1.ew_vdo_note_mdls";
var ew_vdo_notes_tbl = "ew1.ew_vdo_notes";
var ew_vdo_notes_usr_tbl = "ew1.ew_vdo_notes_usr";

function VideoNoteApi() {}

//deprecated
VideoNoteApi.prototype.updateModuleNotes = function (data) {

    var col;
    return getConnection().then(removeData).then(insertData).catch(function (err) {
        console.log(err);
    });

    function removeData(db) {
        col = db.collection('videonotes');
        return col.updateOne({
            "moduleId": data.moduleId,
            "userId": data.userId
        }, {
            $set: { "moduleNotes": [] }
        });
    }

    function insertData() {
        return col.updateOne({
            "moduleId": data.moduleId,
            "userId": data.userId
        }, {
            $set: {
                "moduleNotes": data.moduleNotes,
                "courseId": data.courseId,
                "courseName": data.courseName,
                "moduleNumber": data.moduleNumber,
                "moduleName": data.moduleName,
                lastUpdated: data.lastUpdated
            }
        }, { upsert: true });
    }
};

//deprecated
VideoNoteApi.prototype.getModuleNotes = function (data) {
    var col;
    return getConnection().then(getModuleNotes).catch(function (err) {
        console.log(err);
    });

    function getModuleNotes(db) {
        col = db.collection('videonotes');
        return col.find({ userId: data.userId, moduleId: data.moduleId }, { _id: 0 }).toArray();
    }
};

//deprecated
VideoNoteApi.prototype.getNotesModuleNames = function (data) {
    var col;
    return getConnection().then(getNotesModuleNames).catch(function (err) {
        console.log(err);
    });

    function getNotesModuleNames(db) {
        col = db.collection('videonotes');
        return col.find({ userId: data.userId, courseId: data.courseId, 'moduleNotes.videoNotes.0': { $exists: true } }, { moduleId: 1, moduleNumber: 1, moduleName: 1, _id: 0 }).toArray();
    }
};

// Cassandra API
VideoNoteApi.prototype.getVideoNotesModules = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var defer, usr_id, crs_id, qry, params;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        crs_id = data.crs_id;
                        qry = "select * from " + ew_vdo_note_mdls_tbl + " where usr_id=? and crs_id=?";
                        params = [usr_id, crs_id];

                        cassconn.execute(qry, params, function (err, res) {
                            if (err) {
                                defer.reject(err);
                                return;
                            }
                            var jsonArr = JSON.parse(JSON.stringify(res.rows || []));
                            var modules = jsonArr.map(function (v, i) {
                                var obj = {
                                    mdl_id: v.mdl_id,
                                    mdl_nm: v.mdl_nm
                                };
                                return obj;
                            });
                            defer.resolve(modules || []);
                        });
                        return _context.abrupt('return', defer.promise);

                    case 7:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();
VideoNoteApi.prototype.insertVideoNotesModule = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var defer, usr_id, crs_id, crs_nm, mdl_id, mdl_nm, qry, params;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        crs_id = data.crs_id;
                        crs_nm = data.crs_nm;
                        mdl_id = data.mdl_id;
                        mdl_nm = data.mdl_nm;
                        qry = "insert into " + ew_vdo_note_mdls_tbl + " (usr_id,crs_id,crs_nm,mdl_id,mdl_nm) values (?,?,?,?,?)";
                        params = [usr_id, crs_id, crs_nm, mdl_id, mdl_nm];

                        cassconn.execute(qry, params, function (err, res) {
                            if (err) {
                                defer.reject(err);
                                return;
                            }
                            defer.resolve(data);
                        });
                        return _context2.abrupt('return', defer.promise);

                    case 10:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();
VideoNoteApi.prototype.deleteVideoNotesModules = function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var crs_id = data.crs_id;
    var mdl_id = data.mdl_id;
    var qry = "delete from " + ew_vdo_note_mdls_tbl + " where usr_id=? and crs_id=? and mdl_id=?";
    var params = [usr_id, crs_id, mdl_id];
    cassconn.execute(qry, params, function (err, res) {
        if (err) {
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    });
    return defer.promise;
};

//pass module id to get all video notes
VideoNoteApi.prototype.getVideoNotesForModules = function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var qry = "select * from " + ew_vdo_notes_tbl + " where usr_id=? and mdl_id=?";
    var params = [usr_id, mdl_id];
    cassconn.execute(qry, params, function (err, res) {
        if (err) {
            defer.reject(err);
            return;
        }
        var jsonArr = JSON.parse(JSON.stringify(res.rows || []));
        var videoNotes = jsonArr.map(function (v, i) {
            var obj = {
                vdo_id: v.vdo_id,
                vdo_note_id: v.vdo_note_id,
                vdo_note: v.vdo_note,
                vdo_tm: v.vdo_tm,
                crt_dt: v.crt_dt
            };
            return obj;
        });
        var result = {
            mdl_id: mdl_id,
            video_notes: videoNotes || []
        };
        defer.resolve(result || {});
    });
    return defer.promise;
};
VideoNoteApi.prototype.insertVideoNotes = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var _this = this;

        var defer, usr_id, crs_id, crs_nm, mdl_id, mdl_nm, vdo_id, vdo_note_id, vdo_note, vdo_tm, crt_dt, qry, params;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        crs_id = data.crs_id;
                        crs_nm = data.crs_nm;
                        mdl_id = data.mdl_id;
                        mdl_nm = data.mdl_nm;
                        vdo_id = data.vdo_id;
                        vdo_note_id = Uuid.random();
                        vdo_note = data.vdo_note;
                        vdo_tm = data.vdo_tm;
                        crt_dt = new Date();
                        _context4.next = 13;
                        return this.insertVideoNotesModule(data);

                    case 13:
                        qry = "insert into " + ew_vdo_notes_tbl + " (usr_id,crs_id,mdl_id,vdo_id,vdo_note_id,vdo_note,vdo_tm,crt_dt) values (?,?,?,?,?,?,?,?)";
                        params = [usr_id, crs_id, mdl_id, vdo_id, vdo_note_id, vdo_note, vdo_tm, crt_dt];


                        cassconn.execute(qry, params, function () {
                            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(err, res) {
                                return _regenerator2.default.wrap(function _callee3$(_context3) {
                                    while (1) {
                                        switch (_context3.prev = _context3.next) {
                                            case 0:
                                                if (!err) {
                                                    _context3.next = 3;
                                                    break;
                                                }

                                                defer.reject(err);
                                                return _context3.abrupt('return');

                                            case 3:
                                                data.vdo_note_id = vdo_note_id;
                                                data.crt_dt = crt_dt;

                                                _context3.prev = 5;
                                                _context3.next = 8;
                                                return dashboardStatsAPI.incrementNotesCounter({ usr_id: usr_id });

                                            case 8:
                                                _context3.next = 13;
                                                break;

                                            case 10:
                                                _context3.prev = 10;
                                                _context3.t0 = _context3['catch'](5);

                                                debug(_context3.t0);

                                            case 13:

                                                defer.resolve(data);

                                            case 14:
                                            case 'end':
                                                return _context3.stop();
                                        }
                                    }
                                }, _callee3, _this, [[5, 10]]);
                            }));

                            return function (_x4, _x5) {
                                return _ref4.apply(this, arguments);
                            };
                        }());
                        return _context4.abrupt('return', defer.promise);

                    case 17:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

VideoNoteApi.prototype.updateVideoNote = function (data) {
    var defer = Q.defer();

    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var vdo_note_id = data.vdo_note_id;
    var vdo_note = data.vdo_note;

    var qry = "update " + ew_vdo_notes_tbl + " set vdo_note=? where usr_id=? and mdl_id=? and vdo_note_id =?";
    var params = [vdo_note, usr_id, mdl_id, vdo_note_id];
    cassconn.execute(qry, params, function (err, res) {
        if (err) {
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    });
    return defer.promise;
};

VideoNoteApi.prototype.deleteVideoNote = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
        var _this2 = this;

        var defer, usr_id, mdl_id, vdo_note_id, qry, params;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        mdl_id = data.mdl_id;
                        vdo_note_id = data.vdo_note_id;
                        qry = "delete from " + ew_vdo_notes_tbl + " where usr_id=? and mdl_id=? and vdo_note_id =?";
                        params = [usr_id, mdl_id, vdo_note_id];

                        cassconn.execute(qry, params, function () {
                            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(err, res) {
                                var notesData;
                                return _regenerator2.default.wrap(function _callee5$(_context5) {
                                    while (1) {
                                        switch (_context5.prev = _context5.next) {
                                            case 0:
                                                if (!err) {
                                                    _context5.next = 3;
                                                    break;
                                                }

                                                defer.reject(err);
                                                return _context5.abrupt('return');

                                            case 3:
                                                _context5.next = 5;
                                                return _this2.getVideoNotesForModules(data);

                                            case 5:
                                                notesData = _context5.sent;

                                                if (!(notesData["video_notes"].length <= 0)) {
                                                    _context5.next = 9;
                                                    break;
                                                }

                                                _context5.next = 9;
                                                return _this2.deleteVideoNotesModules(data);

                                            case 9:
                                                _context5.prev = 9;
                                                _context5.next = 12;
                                                return dashboardStatsAPI.decrementNotesCounter({ usr_id: usr_id });

                                            case 12:
                                                _context5.next = 17;
                                                break;

                                            case 14:
                                                _context5.prev = 14;
                                                _context5.t0 = _context5['catch'](9);

                                                debug(_context5.t0);

                                            case 17:

                                                defer.resolve(data);

                                            case 18:
                                            case 'end':
                                                return _context5.stop();
                                        }
                                    }
                                }, _callee5, _this2, [[9, 14]]);
                            }));

                            return function (_x7, _x8) {
                                return _ref6.apply(this, arguments);
                            };
                        }());
                        return _context6.abrupt('return', defer.promise);

                    case 8:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, this);
    }));

    return function (_x6) {
        return _ref5.apply(this, arguments);
    };
}();

//pass user id to get all video notes added by that user
VideoNoteApi.prototype.getVideoNotesOfUser = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
        var usr_id, qry, params, res, video_ids, item_res, item_map;
        return _regenerator2.default.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        _context7.prev = 0;
                        usr_id = data.usr_id;
                        qry = "select * from " + ew_vdo_notes_usr_tbl + " where usr_id=?";
                        params = [usr_id];
                        _context7.next = 6;
                        return cassExecute(qry, params);

                    case 6:
                        res = _context7.sent;


                        //extract video id
                        video_ids = res.map(function (v, i) {
                            return v.vdo_id;
                        });

                        //all item info

                        _context7.next = 10;
                        return authorizeitemsAPI.getItemsUsingItemId({ itemId: video_ids });

                    case 10:
                        item_res = _context7.sent;


                        debug(item_res);

                        //all item map id->object
                        item_map = {};

                        item_res.forEach(function (v, i) {
                            item_map[v.itemId] = v;
                        });

                        //attach iteminfo to response obj
                        res = res.map(function (v, i) {
                            var item_info = item_map[v.vdo_id];
                            v["item_info"] = item_info;
                            return v;
                        });

                        return _context7.abrupt('return', res);

                    case 18:
                        _context7.prev = 18;
                        _context7.t0 = _context7['catch'](0);

                        debug(_context7.t0);
                        throw _context7.t0;

                    case 22:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, this, [[0, 18]]);
    }));

    return function (_x9) {
        return _ref7.apply(this, arguments);
    };
}();

module.exports = new VideoNoteApi();