'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videorating');

var ew_vdo_rating_tbl = "ew1.ew_vdo_rating";
var ew_vdo_avg_rating_tbl = "ew1.ew_vdo_avg_rating";

function VideoRatingApi() {}

VideoRatingApi.prototype.saveVideoRating = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var vdo_id, usr_id, rating, crt_dt, upd_dt, qry, params, avg_qry, avg_params;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        vdo_id = data.vdo_id;
                        usr_id = data.usr_id;
                        rating = data.rating;
                        crt_dt = new Date();
                        upd_dt = new Date();
                        qry = "insert into " + ew_vdo_rating_tbl + " (vdo_id,usr_id,rating,crt_dt,upd_dt) values (?,?,?,?,?)";
                        params = [vdo_id, usr_id, rating, crt_dt, upd_dt];
                        avg_qry = "update " + ew_vdo_avg_rating_tbl + " set vdo_rating=vdo_rating + " + rating + " , no_of_ratings=no_of_ratings + 1 where vdo_id=?";
                        avg_params = [vdo_id];
                        _context.next = 12;
                        return cassExecute(qry, params, { prepare: true });

                    case 12:
                        _context.next = 14;
                        return cassExecute(avg_qry, avg_params, { prepare: true });

                    case 14:
                        return _context.abrupt('return', data);

                    case 17:
                        _context.prev = 17;
                        _context.t0 = _context['catch'](0);

                        debug(_context.t0);
                        throw _context.t0;

                    case 21:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 17]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

VideoRatingApi.prototype.updateVideoRating = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var vdo_id, usr_id, rating, upd_dt, qry, params, avg_qry, avg_params;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        vdo_id = data.vdo_id;
                        usr_id = data.usr_id;
                        rating = new Number(data.rating);
                        upd_dt = new Date();
                        qry = "update " + ew_vdo_rating_tbl + " set rating=?,upd_dt=? where usr_id=? and vdo_id=?";
                        params = [rating, upd_dt, usr_id, vdo_id];
                        avg_qry = "update " + ew_vdo_avg_rating_tbl + " set vdo_rating=vdo_rating + " + rating + " , no_of_ratings=no_of_ratings + 1 where vdo_id=?";
                        avg_params = [vdo_id];
                        _context2.next = 11;
                        return cassExecute(qry, params, { prepare: true });

                    case 11:
                        _context2.next = 13;
                        return cassExecute(avg_qry, avg_params, { prepare: true });

                    case 13:
                        return _context2.abrupt('return', data);

                    case 16:
                        _context2.prev = 16;
                        _context2.t0 = _context2['catch'](0);

                        debug(_context2.t0);
                        throw _context2.t0;

                    case 20:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 16]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

VideoRatingApi.prototype.getVideoRatingForUser = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var vdo_id, usr_id, qry, params, res, avg_qry, avg_params, res_avg_rating, avg_rating, total_rating, total_no_rating;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        vdo_id = data.vdo_id;
                        usr_id = data.usr_id;
                        qry = "select * from  " + ew_vdo_rating_tbl + "  where usr_id=? and vdo_id=?";
                        params = [usr_id, vdo_id];
                        _context3.next = 7;
                        return cassExecute(qry, params);

                    case 7:
                        res = _context3.sent;

                        res[0] = res[0] || {};
                        //find avg rating
                        avg_qry = "select * from  " + ew_vdo_avg_rating_tbl + "  where vdo_id=?";
                        avg_params = [vdo_id];
                        _context3.next = 13;
                        return cassExecute(avg_qry, avg_params);

                    case 13:
                        res_avg_rating = _context3.sent;

                        debug("res_avg_rating.......", res_avg_rating);
                        avg_rating = 0;

                        if (res_avg_rating[0]) {
                            //total rating
                            total_rating = res_avg_rating[0].vdo_rating;
                            //total number of rating

                            total_no_rating = res_avg_rating[0].no_of_ratings;


                            avg_rating = total_rating / total_no_rating;
                        }

                        return _context3.abrupt('return', (0, _extends3.default)({}, res[0], {
                            avg_rating: avg_rating
                        }));

                    case 20:
                        _context3.prev = 20;
                        _context3.t0 = _context3['catch'](0);

                        debug(_context3.t0);
                        throw _context3.t0;

                    case 24:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 20]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

module.exports = new VideoRatingApi();