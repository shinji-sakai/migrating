'use strict';

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var uuid = require('uuid');

var Q = require('q');

function skill() {}

skill.prototype.getAll = function () {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({}, { _id: 0 }).toArray();
    }
};

skill.prototype.add = function (data) {
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        //insert/update in skill
        var skl_id = data.skl_id || '' + (Math.floor(Math.random() * 90000) + 10000);
        data.skl_id = skl_id;

        return col.update({ skl_id: skl_id }, data, { upsert: true, w: 1 });
    }
};
skill.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ skl_id: data.skl_id });
    }
};

function dbCollection(db) {
    return db.collection("skills");
}

module.exports = new skill();