'use strict';

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:forums");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var ew_emp_types_table = 'ew1.ew_emp_typs';

function EmployeeTypes() {}

EmployeeTypes.prototype.addType = function (data) {
	var defer = Q.defer();

	var typ_desc = data.typ_desc;
	var typ_id = data.typ_id || Uuid.random();

	var qry = "update " + ew_emp_types_table + " set typ_desc = ? where typ_id = ?";
	var params = [typ_desc, typ_id];

	cassconn.execute(qry, params, function (err, res, r) {
		if (err) {
			defer.reject(err);
			debug(err);
		}
		defer.resolve({});
	});

	return defer.promise;
};

EmployeeTypes.prototype.getAll = function (data) {
	var defer = Q.defer();

	var qry = "select * from  " + ew_emp_types_table;

	cassconn.execute(qry, null, function (err, res, r) {
		if (err) {
			defer.reject(err);
			debug(err);
		}
		var jsonRows = JSON.parse(JSON.stringify(res.rows));
		defer.resolve(jsonRows);
	});

	return defer.promise;
};

EmployeeTypes.prototype.delete = function (data) {
	var defer = Q.defer();

	var typ_id = data.typ_id;

	var qry = "delete from   " + ew_emp_types_table + " where typ_id = ?;";
	var params = [typ_id];
	cassconn.execute(qry, params, function (err, res, r) {
		if (err) {
			defer.reject(err);
			debug(err);
		}
		defer.resolve({});
	});

	return defer.promise;
};

module.exports = new EmployeeTypes();