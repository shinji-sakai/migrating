"use strict";

function docData(data) {
    console.log("new data", data);
    return data;
}

function err(err) {
    console.log(err);
    throw new Error("Error: Try again else contact Support");
}

module.exports = {
    docData: docData,
    err: err
};