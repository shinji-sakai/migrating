'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);

var videoUrlApi = require(configPath.api.videoUrl);
var videoEntityApi = require(configPath.api.video.videoentity);
var debug = require('debug')('app:api:authorizeitems');

var Q = require('q');

var AuthorizeItems = function () {
    function AuthorizeItems() {
        (0, _classCallCheck3.default)(this, AuthorizeItems);
    }

    (0, _createClass3.default)(AuthorizeItems, [{
        key: 'getAuthorizeItems',
        value: function getAuthorizeItems(data) {
            return getConnection().then(dbCollection).then(findAllItems).catch(globalFunctions.err);

            function findAllItems(col) {
                return col.find({
                    courseId: data.courseId
                }, {
                    _id: 0
                }).toArray();
            }
        }
    }, {
        key: 'updateItemType',
        value: function updateItemType(data) {
            return getConnection().then(dbCollection).then(updateAllItems).catch(globalFunctions.err);

            function updateAllItems(col) {
                var promise_arr = [];
                for (var i = 0; i < data.length; i++) {
                    promise_arr.push(col.updateOne({
                        itemId: data[i].itemId
                    }, data[i], {
                        upsert: true,
                        w: 1
                    }));
                }
                // console.log(promise_arr)
                return Q.all(promise_arr);
            }
            return getConnection().then(function (db) {
                var col = db.collection('coursemodule');
                return col.updateOne({
                    moduleId: data.moduleId
                }, data, {
                    upsert: true,
                    w: 1
                });
            }, function (err) {
                console.log(err);
            });
        }
    }, {
        key: 'isItemExist',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var conn, col, rows;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return getConnection();

                            case 3:
                                conn = _context.sent;
                                _context.next = 6;
                                return dbCollection(conn);

                            case 6:
                                col = _context.sent;
                                _context.next = 9;
                                return col.find({
                                    courseId: data.courseId,
                                    itemId: data.itemId
                                }, {
                                    _id: 0
                                }).toArray();

                            case 9:
                                rows = _context.sent;

                                if (!(rows.length > 0)) {
                                    _context.next = 14;
                                    break;
                                }

                                return _context.abrupt('return', {
                                    exist: true
                                });

                            case 14:
                                return _context.abrupt('return', {
                                    exist: false
                                });

                            case 15:
                                _context.next = 20;
                                break;

                            case 17:
                                _context.prev = 17;
                                _context.t0 = _context['catch'](0);
                                throw _context.t0;

                            case 20:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 17]]);
            }));

            function isItemExist(_x) {
                return _ref.apply(this, arguments);
            }

            return isItemExist;
        }()
    }, {
        key: 'addItem',
        value: function addItem(data) {
            return getConnection().then(dbCollection).then(insert).catch(globalFunctions.err);

            function insert(col) {
                return col.updateOne({
                    itemId: data.itemId
                }, data, {
                    upsert: true,
                    w: 1
                });
            }
        }
    }, {
        key: 'getVideoItemFullInfo',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                var conn, col, resItem, itemVideo, videoEntity, source, name, url;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                _context2.next = 3;
                                return getConnection();

                            case 3:
                                conn = _context2.sent;
                                _context2.next = 6;
                                return dbCollection(conn);

                            case 6:
                                col = _context2.sent;
                                _context2.next = 9;
                                return col.findOne({
                                    itemId: data.itemId
                                });

                            case 9:
                                resItem = _context2.sent;

                                //get item video id
                                resItem = resItem || {};
                                itemVideo = resItem.itemVideo;
                                //get video entity using video id

                                _context2.next = 14;
                                return videoEntityApi.findVideoEntityById({
                                    videoId: itemVideo
                                });

                            case 14:
                                videoEntity = _context2.sent;

                                videoEntity = videoEntity[0] || {};
                                source = videoEntity.videoSource || "harddrive";
                                name = videoEntity.videoUrl;
                                //get video url using item source and name

                                _context2.next = 20;
                                return videoUrlApi[source](name);

                            case 20:
                                url = _context2.sent;

                                videoEntity["fullUrl"] = url;
                                resItem["videoEntity"] = videoEntity;
                                return _context2.abrupt('return', resItem);

                            case 26:
                                _context2.prev = 26;
                                _context2.t0 = _context2['catch'](0);

                                debug(_context2.t0);
                                throw _context2.t0;

                            case 30:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[0, 26]]);
            }));

            function getVideoItemFullInfo(_x2) {
                return _ref2.apply(this, arguments);
            }

            return getVideoItemFullInfo;
        }()
    }, {
        key: 'getItemsUsingItemId',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var ids, conn, col, resItem;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                ids = [].concat(data.itemId);
                                _context3.next = 4;
                                return getConnection();

                            case 4:
                                conn = _context3.sent;
                                _context3.next = 7;
                                return dbCollection(conn);

                            case 7:
                                col = _context3.sent;
                                _context3.next = 10;
                                return col.find({
                                    itemId: {
                                        $in: ids
                                    }
                                }).toArray();

                            case 10:
                                resItem = _context3.sent;
                                return _context3.abrupt('return', resItem);

                            case 14:
                                _context3.prev = 14;
                                _context3.t0 = _context3['catch'](0);

                                debug(_context3.t0);
                                throw _context3.t0;

                            case 18:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 14]]);
            }));

            function getItemsUsingItemId(_x3) {
                return _ref3.apply(this, arguments);
            }

            return getItemsUsingItemId;
        }()
    }, {
        key: 'getVideoPermission',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var videoId, conn, col, resItem, type, i, item;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                videoId = data.videoId;
                                _context4.next = 4;
                                return getConnection();

                            case 4:
                                conn = _context4.sent;
                                _context4.next = 7;
                                return dbCollection(conn);

                            case 7:
                                col = _context4.sent;
                                _context4.next = 10;
                                return col.find({
                                    itemVideo: videoId
                                }).toArray();

                            case 10:
                                resItem = _context4.sent;

                                resItem = resItem || [];

                                if (!(resItem.length === 1)) {
                                    _context4.next = 16;
                                    break;
                                }

                                return _context4.abrupt('return', (0, _extends3.default)({
                                    videoId: videoId
                                }, resItem[0]));

                            case 16:
                                if (!(resItem.length > 1)) {
                                    _context4.next = 40;
                                    break;
                                }

                                type = 'paid';
                                i = 0;

                            case 19:
                                if (!(i < resItem.length)) {
                                    _context4.next = 37;
                                    break;
                                }

                                item = resItem[i];

                                if (!((type === "paid" || type === "demologin") && item.itemType === "demo")) {
                                    _context4.next = 26;
                                    break;
                                }

                                type = "demo";
                                return _context4.abrupt('break', 37);

                            case 26:
                                if (!(type === "paid" && item.itemType === "demo")) {
                                    _context4.next = 31;
                                    break;
                                }

                                type = "demologin";
                                return _context4.abrupt('break', 37);

                            case 31:
                                if (!(item.itemType === "paid")) {
                                    _context4.next = 34;
                                    break;
                                }

                                type = "paid";
                                return _context4.abrupt('break', 37);

                            case 34:
                                i++;
                                _context4.next = 19;
                                break;

                            case 37:
                                return _context4.abrupt('return', {
                                    videoId: videoId,
                                    itemType: type
                                });

                            case 40:
                                return _context4.abrupt('return', {
                                    videoId: videoId,
                                    itemType: "demo"
                                });

                            case 41:
                                _context4.next = 47;
                                break;

                            case 43:
                                _context4.prev = 43;
                                _context4.t0 = _context4['catch'](0);

                                debug(_context4.t0);
                                throw _context4.t0;

                            case 47:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 43]]);
            }));

            function getVideoPermission(_x4) {
                return _ref4.apply(this, arguments);
            }

            return getVideoPermission;
        }()
    }]);
    return AuthorizeItems;
}();

function dbCollection(db) {
    return db.collection("authorizeitems");
}

module.exports = new AuthorizeItems();