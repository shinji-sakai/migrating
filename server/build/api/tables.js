"use strict";

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MongoTables = function () {
  function MongoTables() {
    (0, _classCallCheck3.default)(this, MongoTables);
  }

  (0, _createClass3.default)(MongoTables, [{
    key: "practiceQuestions",

    /**
     * @param {string} questionNumber - Question Number 
     * @param {string} questionId - Question Id
     * @param {string} question - Question Description. This will be transformed plain html if user has entered MATH Code
     * @param {string} questionTime - Question Time.
     * @param {Object} options - Option contains id , original text without any transformation and text which will be transformed plain html if user has entered MATH Code
     * @param {string} textExplanation - Text Explanation
     * @param {Array}  ans - Encrypted answer id
     * @param {Array}  subjects - Subject in which this question comes
     * @param {Array}  authors - Related Authors
     * @param {Array}  publications - Related publications
     * @param {Array}  books - Related books
     * @param {Array}  exams - Related exams
     * @param {Array}  topics - Related topics
     * @param {Array}  topicGroup - Related topicGroup
     * @param {Array}  tags - Related tags
     * @param {string}  questionDifficulty - Question Difficulties
     * @param {string}  originalQuestion - Original Question without any transformation
     * @desc This Collection is used to store all practice questions.
     */
    value: function practiceQuestions() {
      var str = {
        "questionNumber": 5,
        "questionId": "",
        "question": "",
        "questionTime": 1,
        "options": [{
          "text": "",
          "id": "",
          "originalText": ""
        }, {
          "text": "",
          "id": "",
          "originalText": ""
        }],
        "textExplanation": "",
        "ans": [],
        "subjects": [],
        "authors": [],
        "publications": [],
        "books": [],
        "exams": [],
        "topics": [],
        "topicGroup": [],
        "tags": [],
        "questionDifficulty": "",
        "originalQuestion": ""
      };
    }
  }]);
  return MongoTables;
}();

var CassandraTables = function () {
  function CassandraTables() {
    (0, _classCallCheck3.default)(this, CassandraTables);
  }

  (0, _createClass3.default)(CassandraTables, [{
    key: "vt_forum_qrys",


    /**
        * @param {string} qry_id - Question Id 
        * @param {string} qry_title - Question Description
        * @param {string} usr_id - Id of user who posted the question
        * @param {Array} qry_tags - Question Categories
        * @param {Array} qry_txt - Question Description
        * @param {Array} qry_pst_id - Post Id , if question is posted from careerbook
        * @desc This Table is used to store all qa questions.
        */
    value: function vt_forum_qrys() {
      var str = "\t\n\t\t\tcreate table vt_forum_qrys\n\t\t\t(\n\t\t\t\tqry_id uuid,\n\t\t\t\tusr_id text ,\n\t\t\t\tqry_title text,\n\t\t\t\tqry_txt text,\n\t\t\t\tqry_ts timestamp,\n\t\t\t\tqry_tags SET<text>,\n\t\t\t\tqry_pst_id uuid,\n\t\t\t\tPRIMARY KEY (qry_id)\n\t\t\t);\n\t\t";
    }

    /**
     * @param {string} usr_id - User Id 
     * @param {string} del_typ - Delete type
     * @param {uuid} del_id - uuid
     * @param {string} del_rsn - Delete Reason , why user deleted the question
     * @param {timestamp} del_ts - Time when user delete the question
     * @desc This Table is used to store delete reason for deleted qa questions.
     */

  }, {
    key: "ew_del_rsn",
    value: function ew_del_rsn() {
      var str = "\n      create table ew_del_rsn\n      (\n        usr_id text,\n        del_typ text ,\n        del_id uuid,\n        del_rsn  text,\n        del_ts timestamp,\n        primary key(usr_id,del_ts) \n      ) with clustering order by(del_ts desc)\n    ";
    }

    /**
     * @param {string} usr_id - User Id 
     * @param {counter} no_of_bkmrks - Number Of Bookmarks
     * @param {counter} no_of_cb_cmnts - Number Of CB Comments
     * @param {counter} no_of_frds - Number Of Friends
     * @param {counter} no_of_notes - Number Of Notes
     * @param {counter} no_of_psts - Number Of Posts
     * @param {counter} no_of_qa_cmnts - Number Of QA Comments
     * @param {counter} no_of_qa_que_ans - Number Of QA Question Answered
     * @param {counter} no_of_qa_que_ask - Number Of QA Question Asked
     * @param {counter} no_of_qa_que_vws - Number Of QA Question Views
     * @param {counter} no_of_que_attempt_correct - Number Of Correct Question Attempt
     * @param {counter} no_of_que_attempt_tsts - Number Of Test Attempt
     * @param {counter} no_of_que_attempt_wrong - Number Of Wrong Question Attempt
     * @param {counter} no_of_que_learned - Number Of Question Learned
     * @param {counter} no_of_que_practiced - Number Of Question Practiced
     * @param {counter} no_of_tsts_taken - Number Of Test Taken
     * @param {counter} no_of_vdo_vws - Number Of Video Views
     * @desc This Table is used to store dashboard stats of user
     */

  }, {
    key: "ew_dashboard_stats",
    value: function ew_dashboard_stats() {
      var str = "\n      CREATE TABLE ew1.ew_dashboard_stats (\n          usr_id text,\n          no_of_bkmrks counter,\n          no_of_cb_cmnts counter,\n          no_of_frds counter,\n          no_of_notes counter,\n          no_of_psts counter,\n          no_of_qa_cmnts counter,\n          no_of_qa_que_ans counter,\n          no_of_qa_que_ask counter,\n          no_of_qa_que_vws counter,\n          no_of_que_attempt_correct counter,\n          no_of_que_attempt_tsts counter,\n          no_of_que_attempt_wrong counter,\n          no_of_que_learned counter,\n          no_of_que_practiced counter,\n          no_of_tsts_taken counter,\n          no_of_vdo_vws counter,\n          PRIMARY KEY (usr_id)\n      )\n    ";
    }

    /**
     * @param {uuid} qry_id - QA Question Id 
     * @param {counter} vw_cnt - Views counter
     * @desc This Table is used to store total views of qa questions.
     */

  }, {
    key: "vt_qry_trend",
    value: function vt_qry_trend() {
      var str = "\n      create table vt_qry_trend\n      (\n        qry_id uuid ,\n        vw_cnt  counter,\n        primary key ((qry_id) , vw_cnt)\n      )  WITH CLUSTERING ORDER BY (vw_cnt desc);\n    ";
    }

    /**
     * @param {uuid} qry_id - QA Question Id 
     * @param {string} usr_id - UserId
     * @param {timestamp} vw_ts - View Time
     * @desc This Table is used to store question view timestamp of user
     */

  }, {
    key: "vt_qry_vws",
    value: function vt_qry_vws() {
      var str = "\n      create table vt_qry_vws\n      (\n        qry_id uuid ,\n        usr_id text ,\n        vw_ts timestamp,\n        primary key (qry_id,usr_id,vw_ts)\n      );\n    ";
    }

    /**
     * @param {uuid} qry_id - Question Id 
     * @param {counter} qry_upv - Number Of Question Upvote
     * @param {counter} qry_dnv - Number Of Question Downvote
     * @param {counter} qry_ans - Number Of Question Answers
     * @param {counter} qry_shr - Number Of Question Share
     * @param {counter} qry_flw - Number Of Question Follower
     * @param {counter} qry_vws - Number Of Question Views
     * @desc This Table is used to store dashboard stats of user
     */

  }, {
    key: "ew_qry_stats",
    value: function ew_qry_stats() {
      var str = "\n      create table ew_qry_stats\n      (\n        qry_id  uuid,\n        qry_upv counter,\n        qry_dnv counter,\n        qry_ans counter ,\n        qry_shr counter,\n        qry_flw counter,\n        qry_vws counter,\n        primary key (qry_id) \n      );\n    ";
    }

    /**
     * @param {uuid} qry_id - Question Id 
     * @param {text} usr_id - user id
     * @param {boolean} like_dlike_flg - Like(true)/Dislike(false) Flag
     * @param {boolean} qry_ans - user has answered the question or not
     * @param {boolean} qry_shr - user has shared the question or not
     * @param {boolean} qry_flw - user is following question or not
     * @param {boolean} qry_vws - user has viewed the question or not
     * @param {timestamp} crt_ts - create timestamp
     * @desc This Table is used to store user stats for question
     */

  }, {
    key: "ew_qry_usr_stats",
    value: function ew_qry_usr_stats() {
      var str = "\n      create table ew_qry_usr_stats\n      (\n        qry_id uuid,\n        usr_id text,\n        like_dlike_flg  boolean,\n        qry_shr  boolean,\n        qry_flw boolean,\n        qry_vws  boolean,\n        qry_ans boolean,\n        crt_ts timestamp,\n        primary key(qry_id,usr_id)\n      );\n    ";
    }

    /**
     * @param {uuid} qry_id - Question Id 
     * @param {text} usr_id - user id
     * @param {timestamp} flw_ts - follow timestamp
     * @desc This Table is used to store followers of question
     */

  }, {
    key: "ew_qry_flw",
    value: function ew_qry_flw() {
      var str = "\n      create table ew_qry_flw\n      (\n        usr_id text,\n        qry_id uuid,\n        flw_ts timestamp,\n        primary key( (qry_id),usr_id)\n      )\n    ";
    }

    /**
     * @param {uuid} qry_id - Question Id 
     * @param {text} usr_id - user id
     * @param {uuid} ans_id - answer id
     * @param {text} ans_usr_id - id of user who answered the question
     * @param {timestamp} ans_ts - timestamp of answer
     * @param {boolean} ntf_vw_flg - notification has been viewed or not
     * @desc This Table is used to store answer notification for question followers
     */

  }, {
    key: "ew_qry_flw_ntf",
    value: function ew_qry_flw_ntf() {
      var str = " \n      create table ew_qry_flw_ntf\n      (\n        usr_id text,\n        qry_id uuid,\n        ans_id uuid,\n        ans_usr_id text,\n        ans_ts timestamp,\n        ntf_vw_flg boolean,\n        primary key((usr_id), ans_usr_id , ans_ts )\n      )\n    ";
    }

    /**
     * @param {uuid} qry_id - Question Id 
     * @param {text} usr_id - user id
     * @param {timestamp} crt_ts - create/updated timestamp
     * @param {boolean} ans_flg - question is marked it answer later or not
     * @desc This Table is used to store status of answer later for given user and question
     */

  }, {
    key: "ew_qry_ans_ltr",
    value: function ew_qry_ans_ltr() {
      var str = "\n      create table ew_qry_ans_ltr\n      (\n        usr_id text,\n        qry_id uuid,\n        crt_ts timestamp,\n        ans_flg boolean,\n        primary key((usr_id), qry_id)\n      );\n    ";
    }

    /**
     * @param {uuid} cmpln_id - Query Id for which user is submitting complaining
     * @param {text} usr_id - user id
     * @param {text} cmpln_typ - Type of Complaint For e.g. Wrong Question, Inappropriate Question, etc...
     * @param {text} cmpln_rsn - Complaint reason if any
     * @param {timestamp} cmpln_ts - Time of complaint
     * @desc This Table is used to store question complaints
     */

  }, {
    key: "ew_cmpln_rsn",
    value: function ew_cmpln_rsn() {
      var str = "\n      create table ew_cmpln_rsn\n      (\n        usr_id text,\n        cmpln_typ text ,\n        cmpln_id uuid,\n        cmpln_rsn text,\n        cmpln_ts timestamp,\n        primary key(usr_id,cmpln_ts) \n      ) with clustering order by(cmpln_ts desc)\n    ";
    }

    /**
     * @param {uuid} cat_id - Category Id 
     * @param {text} cat_name - Category Name
     * @param {text} cat_desc - Category Description
     * @param {timestamp} crt_ts - Create Timestamp
     * @desc This Table is used to store question categories
     */

  }, {
    key: "vt_qry_cats",
    value: function vt_qry_cats() {
      var str = "\n      create table vt_qry_cats\n      (\n        cat_id uuid,\n        cat_name text,\n        cat_desc text,\n        crt_ts timestamp,\n        primary key (cat_id)\n      );\n    ";
    }

    /**
     * @param {text} pst_crt_by - Post Author Id
     * @param {text} pst_tgt - Public , Only Me , Friends
     * @param {uuid} pst_id - Post Id
     * @param {timestamp} pst_dt - Posted Date
     * @param {text} pst_feel - happy,Sad, etc...
     * @param {Array} pst_img - array of url of post images
     * @param {timestamp} pst_mfy_date - Post Modify Date
     * @param {text} pst_msg - Post Msg
     * @param {text} pst_qry_desc - Post Query Descrioption if it has reply on QA
     * @param {text} pst_qry_id - Query ID if it has reply on QA
     * @param {text} pst_itm_id - If post is posted from some item like from video page ,practice page
     * @param {text} pst_shr_txt - If post is shared from some page
     * @param {Array} pst_tag_frs - Tagged Friends
     * @param {text} pst_typ - post Type
     * @param {Array} pst_vid - Post Video Attachments
     * @desc This Table is used to store CB Post Data
     */

  }, {
    key: "vt_posts",
    value: function vt_posts() {
      var str = "\n      CREATE TABLE ew1.vt_posts (\n          pst_id uuid,\n          pst_crt_by text,\n          pst_dt timestamp,\n          pst_feel text,\n          pst_img set<text>,\n          pst_itm_id text,\n          pst_mfy_date timestamp,\n          pst_msg text,\n          pst_qry_desc text,\n          pst_qry_id uuid,\n          pst_shr_txt text,\n          pst_tag_frs set<text>,\n          pst_tgt text,\n          pst_typ text,\n          pst_vid set<text>,\n          PRIMARY KEY (pst_id)\n      )\n    ";
    }

    /**
     * @param {text} pst_crt_by - Post Author Id
     * @param {text} pst_tgt - Public , Only Me , Friends
     * @param {uuid} pst_id - Post Id
     * @param {timestamp} pst_dt - Posted Date
     * @param {text} pst_feel - happy,Sad, etc...
     * @param {Array} pst_img - array of url of post images
     * @param {timestamp} pst_mfy_date - Post Modify Date
     * @param {text} pst_msg - Post Msg
     * @param {text} pst_qry_desc - Post Query Descrioption if it has reply on QA
     * @param {text} pst_qry_id - Query ID if it has reply on QA
     * @param {text} pst_itm_id - If post is posted from some item like from video page ,practice page
     * @param {text} pst_shr_txt - If post is shared from some page
     * @param {Array} pst_tag_frs - Tagged Friends
     * @param {text} pst_typ - post Type
     * @param {Array} pst_vid - Post Video Attachments
     * @desc This Table is used to store CB Post Data
     */

  }, {
    key: "vt_posts_crt_by",
    value: function vt_posts_crt_by() {
      var str = "\n      CREATE TABLE ew1.vt_posts_crt_by (\n          pst_crt_by text,\n          pst_tgt text,\n          pst_id uuid,\n          pst_dt timestamp,\n          pst_feel text,\n          pst_img set<text>,\n          pst_itm_id text,\n          pst_mfy_date timestamp,\n          pst_msg text,\n          pst_qry_desc text,\n          pst_qry_id uuid,\n          pst_shr_txt text,\n          pst_tag_frs set<text>,\n          pst_typ text,\n          pst_vid set<text>,\n          PRIMARY KEY ((pst_crt_by, pst_tgt), pst_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {text} dsp_nm - User Display Name
     * @param {text} usr_email - User Email
     * @param {text} usr_ph - User Phone Number
     * @param {text} usr_pic - User Display Pic
     * @param {text} usr_role - User Role
     * @desc This Table is used to store User short details
     */

  }, {
    key: "ew_usr_shrt_dtls",
    value: function ew_usr_shrt_dtls() {
      var str = "\n      CREATE TABLE ew1.ew_usr_shrt_dtls (\n          usr_id text PRIMARY KEY,\n          dsp_nm text,\n          usr_email text,\n          usr_ph text,\n          usr_pic text,\n          usr_role text\n      );\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {text} fr_id - id of friend
     * @desc This Table is used to store friends for CB
     */

  }, {
    key: "vt_user_frs",
    value: function vt_user_frs() {
      var str = "\n      create table vt_user_frs\n      (\n        usr_id     text,\n        fr_id  set<text>,\n        PRIMARY KEY (usr_id)\n      );\n    ";
    }

    /**
     * @param {text} user_id - User Id 
     * @param {text} fr_id - Friend id
     * @param {text} last_talked - last updated time
     * @param {text} typ - user type {User / Grp}
     * @desc This Table is used to store user's friends with whom he last taled/updated
     */

  }, {
    key: "vt_frs_chat_ts",
    value: function vt_frs_chat_ts() {
      var str = "\n      CREATE TABLE vt_frs_chat_ts (\n          user_id text,\n          fr_id text,\n          last_talked timestamp,\n          typ text,\n          PRIMARY KEY (user_id, fr_id, last_talked)\n      )\n    ";
    }

    /**
     * @param {text} user_id - User Id 
     * @param {text} display_name - display name
     * @param {text} pic50 - display pic url
     * @param {text} typ - user type {User / Grp}
     * @param {text} user_email - user Email
     * @desc This Table is used to store user short details for chat 
     */

  }, {
    key: "vt_usr_short",
    value: function vt_usr_short() {
      var str = "\n      CREATE TABLE vt_usr_short (\n          user_id text,\n          display_name text,\n          pic50 text,\n          typ text,\n          user_email text,\n          PRIMARY KEY (user_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {text} fr_id - Friend Id
     * @param {text} add_dt - Timestamp
     * @param {text} fr_req_snt - Friend Req sent or not
     * @desc This Table is used to store recommended friend list 
     */

  }, {
    key: "ew_rec_frs",
    value: function ew_rec_frs() {
      var str = "\n      CREATE TABLE ew_rec_frs (\n          usr_id text,\n          fr_id text,\n          add_dt timestamp,\n          fr_req_snt text,\n          PRIMARY KEY (usr_id, fr_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {text} fr_id - Friend Id
     * @param {text} req_dt - Friend Request Timestamp
     * @param {text} req_sts - Request Status (pending='p' , confirmed='f' , deleted='d' ....)
     * @desc This Table is used to store Sent Friend Requests
     */

  }, {
    key: "ew_fr_req_snt",
    value: function ew_fr_req_snt() {
      var str = "\n      CREATE TABLE ew1.ew_fr_req_snt (\n        usr_id text,\n        fr_id text,\n        req_dt timestamp,\n        req_sts text,\n        PRIMARY KEY (usr_id, fr_id)\n      ) \n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {text} fr_id - Friend Id
     * @param {text} req_dt - Friend Request Timestamp
     * @param {text} req_sts - Request Status (pending='p' , confirmed='f' , deleted='d' , ....)
     * @desc This Table is used to store Received Friend Requests
     */

  }, {
    key: "ew_fr_req_rcvd",
    value: function ew_fr_req_rcvd() {
      var str = "\n      CREATE TABLE ew1.ew_fr_req_rcvd (\n          fr_id text,\n          usr_id text,\n          req_dt timestamp,\n          req_sts text,\n          PRIMARY KEY (fr_id, usr_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {Array} flw_id - Array of followers id
     * @param {text} flw_dt - Follow Timestamp
     * @param {text} req_sts - Request Status (pending='p' , confirmed='f' , deleted='d' , ....)
     * @desc This Table is used to store Received Friend Requests
     */

  }, {
    key: "ew_cb_my_flw",
    value: function ew_cb_my_flw() {
      var str = "\n      CREATE TABLE ew_cb_my_flw (\n          usr_id text,\n          flw_dt timestamp,\n          flw_id set<text>,\n          PRIMARY KEY (usr_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {text} ctry - Country
     * @param {text} ctry_flg - Country restrition (Public , Only Me , Friends)
     * @param {text} cty - City
     * @param {text} cty_flg - City restrition (Public , Only Me , Friends)
     * @param {text} dob_dt - DOB
     * @param {text} dob_dt_flg - DOB restrition (Public , Only Me , Friends)
     * @param {text} dob_mn - DOB Month
     * @param {text} dob_mn_flg - DOB Month restrition (Public , Only Me , Friends)
     * @param {text} dob_yr - DOB Year
     * @param {text} dob_yr_flg - DOB Year restrition (Public , Only Me , Friends)
     * @param {text} dsp_nm - Display Name
     * @param {text} dsp_nm_flg - Display Name restrition (Public , Only Me , Friends)
     * @param {map} email - Emails
     * @param {map} email_nv - Non verified Emails
     * @param {text} fst_nm - Firstname
     * @param {text} fst_nm_flg - Firstname restrition (Public , Only Me , Friends)
     * @param {text} ht_ctry - Hometown Country
     * @param {text} ht_ctry_flg - Hometown Country restrition (Public , Only Me , Friends)
     * @param {text} ht_cty - HomeTown City
     * @param {text} ht_cty_flg - HomeTown City restrition (Public , Only Me , Friends)
     * @param {text} ht_zip_cd - Hometown zip code
     * @param {text} ht_zip_cd_flg - Hometown zip code restrition (Public , Only Me , Friends)
     * @param {text} lst_nm - Last name
     * @param {text} lst_nm_flg - Last name restrition (Public , Only Me , Friends)
     * @param {map} ph - Phone Numbers 
     * @param {map} ph_nv - non verified phone number
     * @param {text} role - Role
     * @param {text} usr_abt_me - about user
     * @param {text} usr_abt_me_flg - about user restrition (Public , Only Me , Friends)
     * @param {text} zip_cd - Zip Code
     * @param {text} zip_cd_flg - Zip Code restrition (Public , Only Me , Friends)
     * @desc This Table is used to store Personal Details of user
     */

  }, {
    key: "ew_usr_dtls",
    value: function ew_usr_dtls() {
      var str = "\n      CREATE TABLE ew_usr_dtls (\n          usr_id text,\n          ctry text,\n          ctry_flg text,\n          cty text,\n          cty_flg text,\n          dob_dt text,\n          dob_dt_flg text,\n          dob_mn text,\n          dob_mn_flg text,\n          dob_yr text,\n          dob_yr_flg text,\n          dsp_nm text,\n          dsp_nm_flg text,\n          email map<text, text>,\n          email_nv map<text, text>,\n          fst_nm text,\n          fst_nm_flg text,\n          ht_ctry text,\n          ht_ctry_flg text,\n          ht_cty text,\n          ht_cty_flg text,\n          ht_zip_cd text,\n          ht_zip_cd_flg text,\n          lst_nm text,\n          lst_nm_flg text,\n          ph map<text, text>,\n          ph_nv map<text, text>,\n          role text,\n          usr_abt_me text,\n          usr_abt_me_flg text,\n          zip_cd text,\n          zip_cd_flg text,\n          PRIMARY KEY (usr_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {map} usr_ph - List of user phone numbers for e.g. {<num1>:'p',<num2>:'s'....}. P=Primry , S = Secondary 
     * @param {map} usr_email - List of user emails for e.g. {<email1>:'p',<email2>:'s'....}. P=Primry , S = Secondary
     * @param {text} reg_dt - Timestamp
     * @desc This Table is used to store contact details (emails and phone numbers) of user
     */

  }, {
    key: "ew_usr_contact_dtls",
    value: function ew_usr_contact_dtls() {
      var str = "\n      CREATE TABLE ew1.ew_usr_contact_dtls (\n        usr_id text,\n        reg_dt timestamp,\n        usr_email map<text, text>,\n        usr_ph map<text, text>,\n        PRIMARY KEY (usr_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {uuid} usr_wrk_sk - unique id
     * @param {text} comp_desc - Company Desc
     * @param {text} comp_nm - Company Name
     * @param {text} frm_dt - From Date
     * @param {text} to_dt - To Date
     * @param {text} loc - Company Location
     * @param {text} role - Role in company,
     * @param {text} work_flg - (public,only me,friends)
     * @desc This Table is used to store users compny details
     */

  }, {
    key: "ew_usr_work_dtls",
    value: function ew_usr_work_dtls() {
      var str = "\n      CREATE TABLE ew_usr_work_dtls (\n        usr_id text,\n        usr_wrk_sk uuid,\n        comp_desc text,\n        comp_nm text,\n        frm_dt text,\n        loc text,\n        role text,\n        to_dt text,\n        work_flg text,\n        PRIMARY KEY (usr_id, usr_wrk_sk)\n      )\n    ";
    }

    /**
      * @param {text} usr_id - User Id 
      * @param {uuid} usr_wrk_sk - unique id
      * @param {text} edu_desc - Education Desc
      * @param {text} edu_lvl - Education level (primary school,clg,secondary school,etc...)
      * @param {text} frm_dt - From Date
      * @param {text} to_dt - To Date
      * @param {text} loc - School/Clg Location
      * @param {text} edu_nm - Clg/schl name
      * @param {text} edu_flg - (public,only me,friends)
      * @desc This Table is used to store users education details
      */

  }, {
    key: "ew_usr_edu_dtls",
    value: function ew_usr_edu_dtls() {
      var str = "\n      CREATE TABLE ew1.ew_usr_edu_dtls (\n          usr_id text,\n          usr_wrk_sk uuid,\n          edu_desc text,\n          edu_flg text,\n          edu_lvl text,\n          edu_nm text,\n          frm_dt text,\n          loc text,\n          to_dt text,\n          PRIMARY KEY (usr_id, usr_wrk_sk)\n      )\n    ";
    }

    /**
       * @param {text} usr_id - User Id 
       * @param {uuid} usr_exm_sk - unique id
       * @param {text} tgt_yr - Target Year
       * @param {text} exm_nm - Exam name
       * @param {text} exm_flg - (public,only me,friends)
       * @param {timestamp} crt_dt - Create Date
       * @desc This Table is used to store users exam details
       */

  }, {
    key: "ew_usr_exm_dtls",
    value: function ew_usr_exm_dtls() {
      var str = "\n      CREATE TABLE ew1.ew_usr_exm_dtls (\n          usr_id text,\n          usr_exm_sk uuid,\n          crt_dt timestamp,\n          exm_flg text,\n          exm_nm text,\n          tgt_yr text,\n          PRIMARY KEY (usr_id, usr_exm_sk)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {uuid} usr_crs_sk - unique id
     * @param {text} crs_nm - Course name
     * @param {text} crs_flg - (public,only me,friends)
     * @param {timestamp} crt_dt - Create Date
     * @desc This Table is used to store users interested course details in CB
     */

  }, {
    key: "ew_usr_crs_dtls",
    value: function ew_usr_crs_dtls() {
      var str = "\n      CREATE TABLE ew1.ew_usr_crs_dtls (\n          usr_id text,\n          usr_crs_sk uuid,\n          crs_flg text,\n          crs_nm text,\n          crt_dt timestamp,\n          PRIMARY KEY (usr_id, usr_crs_sk)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {uuid} usr_bk_sk - unique id
     * @param {text} author_desc - Author Desc
     * @param {text} crs_bk_desc - Course/Book Desc
     * @param {text} crs_bk_nm - Course/Book Name
     * @desc This Table is used to store authors details for CB
     */

  }, {
    key: "ew_usr_author_dtl",
    value: function ew_usr_author_dtl() {
      var str = "\n      CREATE TABLE ew1.ew_usr_author_dtl (\n          usr_id text,\n          usr_bk_sk uuid,\n          author_desc text,\n          crs_bk_desc text,\n          crs_bk_nm text,\n          PRIMARY KEY (usr_id, usr_bk_sk)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {uuid} tch_crs_sk - unique id
     * @param {text} tch_crs - Courses
     * @param {text} tch_desc - Desc
     * @param {text} edu_nm - Education Name
     * @desc This Table is used to store teachers details for CB
     */

  }, {
    key: "ew_usr_tch_dtl",
    value: function ew_usr_tch_dtl() {
      var str = "\n      CREATE TABLE ew1.ew_usr_tch_dtl (\n          usr_id text,\n          tch_crs_sk uuid,\n          edu_nm text,\n          tch_crs text,\n          tch_desc text,\n          PRIMARY KEY (usr_id, tch_crs_sk)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User Id 
     * @param {text} kid_usr_id - kid user id
     * @param {text} kid_edu_nm - kid eduction name
     * @param {text} kid_exm_nm - exam name
     * @desc This Table is used to store kids info for CB
     */

  }, {
    key: "ew_usr_parent_kid",
    value: function ew_usr_parent_kid() {
      var str = "\n      CREATE TABLE ew1.ew_usr_parent_kid (\n          usr_id text,\n          kid_usr_id text,\n          kid_edu_nm text,\n          kid_exm_nm set<text>,\n          PRIMARY KEY (usr_id, kid_usr_id)\n      )\n    ";
    }

    /**
     * @param {text} to_usr_id - User Id to whom we want to send notification
     * @param {text} frm_usr_id - User due to which notification generated
     * @param {text} noti_msg - Notification msg
     * @param {text} noti_vw_flg - Notification viewd or not
     * @param {text} snt_ts - Notification sent time
     * @desc This Table is used to store user's CB notifications
     */

  }, {
    key: "ew_fr_req_noti",
    value: function ew_fr_req_noti() {
      var str = "\n      CREATE TABLE ew1.ew_fr_req_noti (\n          to_usr_id text,\n          frm_usr_id text,\n          noti_msg text,\n          noti_vw_flg text,\n          snt_ts timestamp,\n          PRIMARY KEY (to_usr_id, frm_usr_id)\n      )\n    ";
    }

    /**
     * @param {text} to_usr_id - User Id to whom we want to send notification
     * @param {text} pst_id - Post Id in which user is tagged
     * @param {text} frm_usr_id - Userid who tagged his friends
     * @param {text} noti_vw_flg - Notification viewd or not
     * @param {text} noti_msg - Notification msg
     * @param {text} snt_ts - Notification sent time
     * @desc This Table is used to store user's CB notifications
     */

  }, {
    key: "ew_pst_tag_noti",
    value: function ew_pst_tag_noti() {
      var str = "\n      CREATE TABLE ew1.ew_pst_tag_noti (\n          to_usr_id text,\n          pst_id uuid,\n          frm_usr_id text,\n          noti_msg text,\n          noti_vw_flg text,\n          snt_ts timestamp,\n          PRIMARY KEY (to_usr_id, pst_id, frm_usr_id)\n      )\n    ";
    }

    /**
     * @param {uuid} pst_id - Post ID to which comment is related
     * @param {timestamp} cmnt_ts - Comment Timestamp
     * @param {uuid} cmnt_id - Comment Id
     * @param {set} cmnt_atch - Array Of Comment Attachment
     * @param {uuid} cmnt_cmnt_id - If comment is level 1 comment then this will be id of parent comment
     * @param {set} cmnt_img - Image attached to comment
     * @param {text} cmnt_lvl - Comment Level
     * @param {text} cmnt_txt - Comment Text
     * @param {set} cmnt_vid - Video attached to comment
     * @param {text} usr_id - User Id who posted the comment
     * @desc This Table is used to store CB Comments
     */

  }, {
    key: "vt_usr_comnt_lvl0",
    value: function vt_usr_comnt_lvl0() {
      var str = "\n      CREATE TABLE vt_usr_comnt_lvl0 (\n          pst_id uuid,\n          cmnt_ts timestamp,\n          cmnt_id uuid,\n          cmnt_atch set<text>,\n          cmnt_cmnt_id uuid,\n          cmnt_img set<text>,\n          cmnt_lvl text,\n          cmnt_txt text,\n          cmnt_vid set<text>,\n          usr_id text,\n          PRIMARY KEY (pst_id, cmnt_ts, cmnt_id)\n      )\n    ";
    }

    /**
    * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
    * @param {timestamp} crt_ts - Timestamp
    * @param {uuid} cmnt_id - Comment Id
    * @param {int} like_dlike_flg - Liked/Disliked
    * @param {text} usr_id - User Id liked the comment
    * @desc This Table is used to store Like/Disliked by user
    */

  }, {
    key: "vt_qry_like",
    value: function vt_qry_like() {
      var str = "\n      CREATE TABLE ew1.vt_qry_like (\n          qry_id uuid,\n          usr_id text,\n          cmnt_id uuid,\n          crt_ts timestamp,\n          like_dlike_flg int,\n          PRIMARY KEY ((qry_id, usr_id), cmnt_id)\n      )\n    ";
    }

    /**
     * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
     * @param {uuid} cmnt_id - Comment Id
     * @param {text} usr_id - User Id liked the comment
     * @desc This is materialized view
     */

  }, {
    key: "vt_qry_like_usrs",
    value: function vt_qry_like_usrs() {}

    /**
     * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
     * @param {uuid} cmnt_id - Comment Id
     * @param {counter} dlike_cnt - Dislike Count
     * @param {counter} like_cnt - Like Count
     * @desc This table is used to store like/dislike count for given comment
     */

  }, {
    key: "vt_qry_like_stats",
    value: function vt_qry_like_stats() {
      var str = "\n      CREATE TABLE ew1.vt_qry_like_stats (\n        qry_id uuid,\n        cmnt_id uuid,\n        dlike_cnt counter,\n        like_cnt counter,\n        PRIMARY KEY (qry_id, cmnt_id)\n      ) \n    ";
    }

    /**
     * @param {text} usr_id - User id
     * @param {timestamp} pst_dt - Post Date
     * @param {uuid} pst_id - Post ID
     * @desc This table is used to store users feed , like all posts from his friends and from he is following
     */

  }, {
    key: "vt_frs_posts",
    value: function vt_frs_posts() {
      var str = "\n      CREATE TABLE vt_frs_posts (\n        usr_id text,\n        pst_dt timestamp,\n        pst_id uuid,\n        PRIMARY KEY (usr_id, pst_dt, pst_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User id
     * @param {timestamp} pst_dt - Post Date
     * @param {uuid} pst_id - Post ID
     * @desc This table is used to store users own post ids
     */

  }, {
    key: "vt_my_posts",
    value: function vt_my_posts() {
      var str = "\n      CREATE TABLE ew1.vt_my_posts (\n        usr_id text,\n        pst_dt timestamp,\n        pst_id uuid,\n        PRIMARY KEY (usr_id, pst_dt, pst_id)\n      )\n    ";
    }

    /**
     * @param {text} pst_crt_by - Post Author Id
     * @param {text} pst_tgt - Public , Only Me , Friends
     * @param {uuid} pst_id - Post Id
     * @param {timestamp} pst_dt - Posted Date
     * @param {text} pst_feel - happy,Sad, etc...
     * @param {Array} pst_img - array of url of post images
     * @param {timestamp} pst_mfy_date - Post Modify Date
     * @param {text} pst_msg - Post Msg
     * @param {text} pst_qry_desc - Post Query Descrioption if it has reply on QA
     * @param {text} pst_qry_id - Query ID if it has reply on QA
     * @param {text} pst_itm_id - If post is posted from some item like from video page ,practice page
     * @param {text} pst_shr_txt - If post is shared from some page
     * @param {Array} pst_tag_frs - Tagged Friends
     * @param {text} pst_typ - post Type
     * @param {Array} pst_vid - Post Video Attachments
     * @desc This Table is used to store CB Post Data
     */

  }, {
    key: "vt_posts_crt_by",
    value: function vt_posts_crt_by() {
      var str = "\n      CREATE TABLE ew1.vt_posts_crt_by (\n          pst_crt_by text,\n          pst_tgt text,\n          pst_id uuid,\n          pst_dt timestamp,\n          pst_feel text,\n          pst_img set<text>,\n          pst_itm_id text,\n          pst_mfy_date timestamp,\n          pst_msg text,\n          pst_qry_desc text,\n          pst_qry_id uuid,\n          pst_shr_txt text,\n          pst_tag_frs set<text>,\n          pst_typ text,\n          pst_vid set<text>,\n          PRIMARY KEY ((pst_crt_by, pst_tgt), pst_id)\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User id
     * @param {timestamp} crt_dt - Post Date
     * @param {text} pst_id - This will be topic/item id from video page
     * @param {uuid} cb_pst_id - This will be Career-Book Post id
     * @desc This table is used to store references of CBPost which is posted from Video Page Comment Section
     */

  }, {
    key: "ew_usr_vdo_cmnt_m",
    value: function ew_usr_vdo_cmnt_m() {
      var str = "\n      CREATE TABLE ew_usr_vdo_cmnt_m (\n        usr_id text,\n        pst_id text,\n        cb_pst_id uuid,\n        crt_dt timestamp,\n        PRIMARY KEY ((usr_id, pst_id))\n      )\n    ";
    }

    /**
     * @param {text} usr_id - User id
     * @param {timestamp} crt_ts - Timestamp
     * @param {uuid} pst_id - This will be CBPost id
     * @param {text} like_typ - wow,sad,angry,liked,etc....
     * @desc This table is used to store like/dislike of post and user
     */

  }, {
    key: "ew_pst_like",
    value: function ew_pst_like() {
      var str = "\n      CREATE TABLE ew1.ew_pst_like (\n        pst_id uuid,\n        usr_id text,\n        crt_ts timestamp,\n        like_typ text,\n        PRIMARY KEY (pst_id, usr_id)\n      )\n    ";
    }

    /**
     * @param {uuid} pst_id - This will be CBPost id
     * @param {counter} angry_cnt - Angry Count
     * @param {counter} dlike_cnt - Dislike Count
     * @param {counter} haha_cnt - Haha Count
     * @param {counter} like_cnt - Like Count
     * @param {counter} love_cnt - Love Count
     * @param {counter} sad_cnt - Sad Count
     * @param {counter} wow_cnt - Wow Count
     * @desc This table is used to store various like type count of given post
     */

  }, {
    key: "ew_pst_like_stats",
    value: function ew_pst_like_stats() {
      var str = "\n      CREATE TABLE ew1.ew_pst_like_stats (\n        pst_id uuid,\n        angry_cnt counter,\n        dlike_cnt counter,\n        haha_cnt counter,\n        like_cnt counter,\n        love_cnt counter,\n        sad_cnt counter,\n        wow_cnt counter,\n        PRIMARY KEY (pst_id)\n      )\n    ";
    }
  }]);
  return CassandraTables;
}();