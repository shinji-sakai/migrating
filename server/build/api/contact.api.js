'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var Q = require('q');

var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:contact");
var cassconn = require(configPath.dbconn.cassconn);
var Uuid = cassandra.types.Uuid;
var mailAPI = require(configPath.api.mail);
var cassExecute = require(configPath.lib.utility).cassExecute;
var contactusTemplate = require(configPath.api.emailTemplateAPI.contactusTemplate);

var ew_contact_msg_table = 'ew1.ew_contact_msgs';

function Contact() {}

Contact.prototype.saveContactMsg = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var fst_nm, email, msg, ph_no, msg_id, query, params, res, template, obj;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						fst_nm = data.fst_nm;
						email = data.email;
						msg = data.msg;
						ph_no = data.ph_no;
						msg_id = Uuid.random();
						query = "insert into " + ew_contact_msg_table + " (fst_nm,email,msg,ph_no,msg_id,msg_ts) values (?,?,?,?,?,?)";
						params = [fst_nm, email, msg, ph_no, msg_id, new Date()];
						_context.next = 10;
						return cassExecute(query, params);

					case 10:
						res = _context.sent;
						_context.next = 13;
						return contactusTemplate({
							name: fst_nm,
							email: email,
							message: msg,
							ph_no: ph_no
						});

					case 13:
						template = _context.sent;


						//Send mail to ew team
						obj = {
							to: 'support@examwarrior.com',
							subject: 'Msg From ContactUs Form',
							text: template.html
						};
						_context.next = 17;
						return mailAPI.sendMail(obj);

					case 17:
						return _context.abrupt('return', data);

					case 20:
						_context.prev = 20;
						_context.t0 = _context['catch'](0);
						throw _context.t0;

					case 23:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 20]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

Contact.prototype.getContactUsDetails = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var find;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						find = function find(col) {
							return col.find({}).toArray();
						};

						return _context2.abrupt('return', getConnection().then(dbCollection).then(find).catch(globalFunctions.err));

					case 2:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

function dbCollection(db) {
	return db.collection("contactus");
}

module.exports = new Contact();