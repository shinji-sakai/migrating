'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var nodemailer = require('nodemailer');
var promisify = require("promisify-node");
var subscribeAPI = require('./subscribe.api.js');
var debug = require('debug')("app:api:auth:cass");
var buyTrainingTemplate = require('./buyTrainingTemplate.js');
var enrolledBatchTemplate = require('./enrolledBatchTemplate.js');
var mail_config = require('./mailConfig.js');
var smtpTransport = nodemailer.createTransport("SMTP", {
	host: "smtp.gmail.com", // hostname
	secureConnection: true, // use SSL
	port: 465,
	auth: {
		user: "support@examwarrior.com",
		pass: "dataxyz123"
	}
});

function Mail() {}

Mail.prototype.sendMail = function (data) {
	var mailOptions = {
		from: 'support@examwarrior.com',
		to: data.to,
		subject: data.subject,
		html: data.text
	};

	var sendMail = promisify(smtpTransport.sendMail);

	return sendMail(mailOptions);
};

Mail.prototype.sendBulkEmails = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var body, subject, subscribes, emails, email_str, d_obj;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						body = data.mail_body;
						subject = data.mail_subject;
						_context.next = 5;
						return subscribeAPI.getAllSubscribers();

					case 5:
						subscribes = _context.sent;

						subscribes = subscribes || [];
						emails = subscribes.map(function (v, i) {
							return v.usr_email;
						}) || [];
						email_str = emails.join(",");
						d_obj = {
							text: body,
							subject: subject,
							to: email_str
						};
						_context.next = 12;
						return this.sendMail(d_obj);

					case 12:
						return _context.abrupt('return', emails);

					case 15:
						_context.prev = 15;
						_context.t0 = _context['catch'](0);

						debug(_context.t0);
						throw _context.t0;

					case 19:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 15]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

Mail.prototype.sendSuccessfullBuyTrainingMail = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var dsp_nm, training, to, d, mail_data_obj;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						dsp_nm = data.dsp_nm;
						training = data.training;
						to = data.to;
						_context2.next = 6;
						return buyTrainingTemplate(data);

					case 6:
						d = _context2.sent;
						mail_data_obj = {
							text: d.html,
							subject: mail_config.BUY_TRAINING_MAIL_SUBJECT,
							to: data.to
						};
						_context2.next = 10;
						return this.sendMail(mail_data_obj);

					case 10:
						return _context2.abrupt('return', {});

					case 13:
						_context2.prev = 13;
						_context2.t0 = _context2['catch'](0);

						debug(_context2.t0);
						throw _context2.t0;

					case 17:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 13]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

Mail.prototype.sendEnrolledBatchMail = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var to, d, mail_data_obj;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						to = data.to;
						_context3.next = 4;
						return enrolledBatchTemplate(data);

					case 4:
						d = _context3.sent;
						mail_data_obj = {
							text: d.html,
							subject: mail_config.ENROLLED_BATCH_MAIL_SUBJECT,
							to: data.to
						};
						_context3.next = 8;
						return this.sendMail(mail_data_obj);

					case 8:
						return _context3.abrupt('return', {});

					case 11:
						_context3.prev = 11;
						_context3.t0 = _context3['catch'](0);

						debug(_context3.t0);
						throw _context3.t0;

					case 15:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 11]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

module.exports = new Mail();