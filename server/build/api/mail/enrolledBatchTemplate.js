'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var getTemplate = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var email_data, defer;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						email_data = {
							dsp_nm: data.dsp_nm,
							usr_id: data.usr_id,
							training: data.training,
							cls_start_dt: data.cls_start_dt,
							cls_frm_tm: data.cls_frm_tm,
							frm_wk_dy: data.frm_wk_dy,
							to_wk_dy: data.to_wk_dy
						};
						defer = Q.defer();

						template.render({ data: email_data }, function (err, result) {
							if (err) {
								defer.reject(err);
								return;
							}
							defer.resolve(result);
						});
						return _context.abrupt('return', defer.promise);

					case 7:
						_context.prev = 7;
						_context.t0 = _context['catch'](0);
						throw _context.t0;

					case 10:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 7]]);
	}));

	return function getTemplate(_x) {
		return _ref.apply(this, arguments);
	};
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var Q = require('q');
var Promise = require("bluebird");

var templateDir = path.join(__dirname, '../../../views/email-templates', 'enrolled-batch');
var template = new EmailTemplate(templateDir);

module.exports = getTemplate;