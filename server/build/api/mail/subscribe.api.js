'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var shortid = require('shortid');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var Q = require('q');

var subscribe_table = "ew1.ew_subs";
var unsubscribe_table = "ew1.ew_unsubs";

function API() {}

API.prototype.subscribeUser = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var subs_id, usr_id, subs_email, subs_typ, subs_dt, qry, params;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;

						console.log(data);
						subs_id = Uuid.random();
						usr_id = data.usr_id;
						subs_email = data.subs_email;
						subs_typ = data.subs_typ;
						subs_dt = new Date();
						qry = "insert into " + subscribe_table + " (subs_id,usr_id,subs_email,subs_typ,subs_dt) values (?,?,?,?,?)";
						params = [subs_id, usr_id, subs_email, subs_typ, subs_dt];
						_context.next = 11;
						return cassExecute(qry, params);

					case 11:
						return _context.abrupt('return', (0, _extends3.default)({}, data, { subs_id: subs_id }));

					case 14:
						_context.prev = 14;
						_context.t0 = _context['catch'](0);
						throw _context.t0;

					case 17:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 14]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

API.prototype.unsubscribeUser = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var usr_id, subs_email, unsubs_id, unsubs_dt, del_qry, del_params, qry, params, queries;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						usr_id = data.usr_id;
						subs_email = data.subs_email;
						unsubs_id = Uuid.random();
						unsubs_dt = new Date();
						del_qry = "delete from " + subscribe_table + " where subs_email=?";
						del_params = [subs_email];
						qry = "insert into " + unsubscribe_table + " (unsubs_id,usr_id,subs_email,unsubs_dt) values (?,?,?,?)";
						params = [unsubs_id, usr_id, subs_email, unsubs_dt];
						queries = [{
							query: del_qry,
							params: del_params
						}, {
							query: qry,
							params: params
						}];
						_context2.next = 12;
						return cassBatch(queries);

					case 12:
						return _context2.abrupt('return', (0, _extends3.default)({}, data, { unsubs_id: unsubs_id }));

					case 15:
						_context2.prev = 15;
						_context2.t0 = _context2['catch'](0);

						console.log(_context2.t0);
						throw _context2.t0;

					case 19:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 15]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

API.prototype.getAllSubscribers = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		return col.find({}).toArray();
	}
};

function dbCollection(db) {
	return db.collection("subscribe");
}

module.exports = new API();