'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var videoSourceConfig = require(configPath.config.videoSourceConfig);
var cloudFrontConfig = require(configPath.config.cloudFrontConfig);
var cloudfront = require('aws-cloudfront-sign');
var awsS3API = require(configPath.api.aws.s3);
var Q = require('q');

function VideoUrlApi() {}
VideoUrlApi.prototype._0_videoSourceGroup = function (videoName) {
    var url = cloudFrontSource(videoName);
    console.log(url);
    return Q.when(url);
};
VideoUrlApi.prototype._1_videoSourceGroup = function (videoName) {

    var url = hardDiskSource(videoName);
    return Q.when(url);
};

VideoUrlApi.prototype.cloud = function (videoName) {
    var url = cloudFrontSource(videoName);
    console.log(url);
    return Q.when(url);
};

VideoUrlApi.prototype.harddrive = function (videoName) {
    var url = hardDiskSource(videoName);
    return Q.when(url);
};

VideoUrlApi.prototype.s3 = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(videoName) {
        var url;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return awsS3API.getSignedUrl('video', videoName);

                    case 2:
                        url = _context.sent;
                        return _context.abrupt('return', url);

                    case 4:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

function hardDiskSource(videoName) {
    return videoSourceConfig._videoPath1 + videoName;
}

function cloudFrontSource(videoName) {
    var url = cloudFrontConfig.BASE_URL + videoName;

    var signedUrl = cloudfront.getSignedUrl(url, {
        keypairId: cloudFrontConfig.KEYPAIR_ID,
        expireTime: Date.now() + cloudFrontConfig.EXPIRE_TIME,
        privateKeyString: cloudFrontConfig.PRIVATE_KEY
    });
    return signedUrl;
}

VideoUrlApi.prototype.getVideoSource = function (data) {
    return getConnection().then(dbCollection).then(globalFunctions.docData).catch(globalFunctions.err);

    function dbCollection(db) {
        var col = db.collection('videoSource');
        var videoSource = col.find({ videoId: data.videoId }, { _id: 0 }).toArray();
        return videoSource;
    }
};

module.exports = new VideoUrlApi();