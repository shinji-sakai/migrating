'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videonotes');
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
// var authorizeitemsAPI = require(configPath.api.authorizeitems);
// var dashboardStatsAPI = require(configPath.api.dashboard.stats);

// var ew_vdo_note_mdls_tbl = "ew1.ew_vdo_note_mdls";
// var ew_vdo_notes_tbl = "ew1.ew_vdo_notes";
// var ew_vdo_notes_usr_tbl = "ew1.ew_vdo_notes_usr";
var ew_usr_notes_table = "ew1.ew_usr_notes";

function NotesAPI() {}

NotesAPI.prototype.getNotes = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var usr_id, src_id, qry, params, rows;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        usr_id = data.usr_id, src_id = data.src_id;
                        qry = 'select * from ' + ew_usr_notes_table + ' where usr_id = ? and src_id = ?';
                        params = [usr_id, src_id];
                        _context.next = 6;
                        return cassExecute(qry, params);

                    case 6:
                        rows = _context.sent;
                        return _context.abrupt('return', rows);

                    case 10:
                        _context.prev = 10;
                        _context.t0 = _context['catch'](0);

                        logger.debug(_context.t0);
                        throw new Error(_context.t0.toString());

                    case 14:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 10]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

NotesAPI.prototype.insertNote = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var usr_id, crs_id, mdl_id, src_id, note_id, note, vdo_tm, hlight_id, note_src, crt_dt, qry, params;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        usr_id = data.usr_id;
                        crs_id = data.crs_id;
                        mdl_id = data.mdl_id;
                        src_id = data.src_id;
                        note_id = Uuid.random();
                        note = data.note;
                        vdo_tm = data.vdo_tm;
                        hlight_id = data.hlight_id;
                        note_src = data.note_src;
                        crt_dt = new Date();
                        qry = "insert into " + ew_usr_notes_table + " (usr_id,crs_id,mdl_id,src_id,note_id,note,vdo_tm,hlight_id,note_src,crt_dt) values (?,?,?,?,?,?,?,?,?,?)";
                        params = [usr_id, crs_id, mdl_id, src_id, note_id, note, vdo_tm, hlight_id, note_src, crt_dt];
                        _context2.next = 15;
                        return cassExecute(qry, params);

                    case 15:
                        return _context2.abrupt('return', (0, _extends3.default)({}, data, {
                            note_id: note_id,
                            crt_dt: crt_dt
                        }));

                    case 18:
                        _context2.prev = 18;
                        _context2.t0 = _context2['catch'](0);
                        throw new Error(_context2.t0);

                    case 21:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 18]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

NotesAPI.prototype.updateNote = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var usr_id, src_id, note_id, note, qry, params;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        usr_id = data.usr_id, src_id = data.src_id, note_id = data.note_id, note = data.note;
                        qry = "update " + ew_usr_notes_table + " set note=? where usr_id=? and note_id=? and src_id =?";
                        params = [note, usr_id, note_id, src_id];
                        _context3.next = 6;
                        return cassExecute(qry, params);

                    case 6:
                        return _context3.abrupt('return', data);

                    case 9:
                        _context3.prev = 9;
                        _context3.t0 = _context3['catch'](0);
                        throw new Error(_context3.t0);

                    case 12:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 9]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

NotesAPI.prototype.deleteNote = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var usr_id, src_id, note_id, qry, params;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        usr_id = data.usr_id, src_id = data.src_id, note_id = data.note_id;
                        qry = "delete from " + ew_usr_notes_table + " where usr_id=? and note_id=? and src_id =?";
                        params = [usr_id, note_id, src_id];
                        _context4.next = 6;
                        return cassExecute(qry, params);

                    case 6:
                        return _context4.abrupt('return', data);

                    case 9:
                        _context4.prev = 9;
                        _context4.t0 = _context4['catch'](0);
                        throw new Error(_context4.t0);

                    case 12:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 9]]);
    }));

    return function (_x4) {
        return _ref4.apply(this, arguments);
    };
}();

module.exports = new NotesAPI();