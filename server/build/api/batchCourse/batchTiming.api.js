'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:batchTiming.api.js");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_bat_dtls_table = 'ew1.ew_bat_dtls';
var ew_bat_m_table = 'ew1.ew_bat_m';

function Timing() {}

Timing.prototype.add = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var defer, bat_id, bat_nm, bat_crs_price, discount, msg, faculty_id, last_class_video, cls_start_dt, cls_end_dt, discount_frm_dt, discount_to_dt, discount_rsn, no_wk_dy, frm_wk_dy, to_wk_dy, wk_dy, crs_id, cls_frm_tm, cls_to_tm, promiseRes, res, addToDetailsTable, addToMasterTable;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						addToMasterTable = function addToMasterTable() {
							var defer = Q.defer();

							var qry = "update " + ew_bat_m_table + " set crt_dt=?,cls_start_dt=? where bat_id = ? and crs_id=?";
							var params = [new Date(), cls_start_dt, bat_id, crs_id];

							cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
								if (err) {
									defer.reject(err);
									debug(err);
									return;
								}
								defer.resolve({});
							});
							return defer.promise;
						};

						addToDetailsTable = function addToDetailsTable() {
							var defer = Q.defer();
							var qry = "update " + ew_bat_dtls_table + " set bat_crs_price=?,discount=?,msg=?,faculty_id=?,last_class_video=?,cls_start_dt=?,cls_frm_tm=?,cls_to_tm=?,crs_id=?,discount_frm_dt=?,discount_to_dt=?,discount_rsn=?,no_wk_dy=?,wk_dy=?,frm_wk_dy=?,to_wk_dy=?,cls_end_dt=?,bat_nm=? where bat_id = ?";
							var params = [bat_crs_price, discount, msg, faculty_id, last_class_video, cls_start_dt, cls_frm_tm, cls_to_tm, crs_id, discount_frm_dt, discount_to_dt, discount_rsn, no_wk_dy, wk_dy, frm_wk_dy, to_wk_dy, cls_end_dt, bat_nm, bat_id];
							cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
								if (err) {
									defer.reject(err);
									debug(err);
								}
								defer.resolve({});
							});
							return defer.promise;
						};

						defer = Q.defer();
						_context.prev = 3;

						debug(data);
						bat_id = data.bat_id;
						bat_nm = data.bat_nm;
						bat_crs_price = data.bat_crs_price;
						discount = data.discount;
						msg = data.msg;
						faculty_id = data.faculty_id && data.faculty_id.map(function (v, i) {
							return parseInt(v);
						}) || [];
						last_class_video = data.last_class_video;
						cls_start_dt = data.cls_start_dt ? new Date(data.cls_start_dt) : undefined;
						cls_end_dt = data.cls_end_dt ? new Date(data.cls_end_dt) : undefined;
						discount_frm_dt = data.discount_frm_dt ? new Date(data.discount_frm_dt) : undefined;
						discount_to_dt = data.discount_to_dt ? new Date(data.discount_to_dt) : undefined;
						discount_rsn = data.discount_rsn;
						no_wk_dy = data.no_wk_dy;
						frm_wk_dy = data.frm_wk_dy;
						to_wk_dy = data.to_wk_dy;
						wk_dy = data.wk_dy;
						crs_id = data.crs_id;
						cls_frm_tm = data.cls_frm_tm;
						cls_to_tm = data.cls_to_tm;


						if (!bat_id) {
							bat_id = Long.fromNumber(Math.floor(Math.random() * 90000) + 10000);
						} else {
							bat_id = Long.fromNumber(bat_id);
						}

						promiseRes = Q.all([addToDetailsTable(), addToMasterTable()]);
						_context.next = 28;
						return promiseRes;

					case 28:
						res = _context.sent;

						defer.resolve({});
						_context.next = 35;
						break;

					case 32:
						_context.prev = 32;
						_context.t0 = _context['catch'](3);

						defer.reject(_context.t0);

					case 35:
						return _context.abrupt('return', defer.promise);

					case 36:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[3, 32]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

Timing.prototype.getAll = function (data) {
	var defer = Q.defer();

	var qry = "select * from  " + ew_bat_dtls_table;
	cassconn.execute(qry, null, function (err, res, r) {
		debug(err, res, r);
		if (err) {
			defer.reject(err);
			debug(err);
			return;
		}
		defer.resolve(res.rows);
	});

	return defer.promise;
};

Timing.prototype.delete = function (data) {
	var defer = Q.defer();

	var bat_id = data.bat_id;

	var qry = "delete from   " + ew_bat_dtls_table + " where bat_id = ?;";
	var params = [Long.fromNumber(parseInt(bat_id))];

	cassconn.execute(qry, params, function (err, res, r) {
		if (err) {
			defer.reject(err);
			debug(err);
			return;
		}
		defer.resolve({});
	});

	return defer.promise;
};

Timing.prototype.setActualEndDateOfBatchTiming = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var bat_id, cls_actual_end_dt, query, params, res;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						bat_id = data.bat_id;
						cls_actual_end_dt = data.cls_actual_end_dt ? new Date(data.cls_actual_end_dt) : undefined;
						query = "update " + ew_bat_dtls_table + " set cls_actual_end_dt=? where bat_id = ?";
						params = [cls_actual_end_dt, bat_id];
						_context2.next = 7;
						return cassExecute(query, params);

					case 7:
						res = _context2.sent;
						return _context2.abrupt('return', data);

					case 11:
						_context2.prev = 11;
						_context2.t0 = _context2['catch'](0);

						debug(_context2.t0);
						throw _context2.t0;

					case 15:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 11]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

Timing.prototype.getTimingByCourseId = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var crs_id, defer, db, col, find_res, no_of_days, masterRes, bat_obj_array, bat_id_array, res, getBatIDfromMaster;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						getBatIDfromMaster = function getBatIDfromMaster() {
							var defer = Q.defer();
							var qry = "select * from   " + ew_bat_m_table + " where crs_id = ?;";
							var params = [crs_id];

							cassconn.execute(qry, params, function (err, res, r) {
								if (err) {
									defer.reject(err);
									debug(err);
									return;
								}
								if (!res) {
									defer.resolve([]);
									return;
								}
								defer.resolve(res.rows);
							});
							return defer.promise;
						};

						crs_id = data.crs_id;
						defer = Q.defer();
						_context3.prev = 3;
						_context3.next = 6;
						return getConnection();

					case 6:
						db = _context3.sent;
						_context3.next = 9;
						return db.collection("batchcourse");

					case 9:
						col = _context3.sent;
						_context3.next = 12;
						return col.find({ crs_id: crs_id }, { _id: 0 }).toArray();

					case 12:
						find_res = _context3.sent;
						no_of_days = find_res[0] && find_res[0]["no_of_days"] || 0;

						//first get batch array from master table based on crs-id

						_context3.next = 16;
						return getBatIDfromMaster();

					case 16:
						masterRes = _context3.sent;

						if (!(masterRes && masterRes.length > 0)) {
							_context3.next = 27;
							break;
						}

						//get all batch id in array
						bat_obj_array = masterRes;
						// .filter((v,i)=>{
						// 	var now = new Date();
						// 	now.setDate(now.getDate() - no_of_days);
						// 	if(v.cls_start_dt >= now){
						// 		return true;
						// 	}else{
						// 		return false;
						// 	}
						// });


						bat_id_array = bat_obj_array.map(function (v, i) {
							return v.bat_id;
						});
						//get all batch details

						_context3.next = 22;
						return this.getTimingByBatId({ bat_id: bat_id_array });

					case 22:
						res = _context3.sent;


						res = res.map(function (v, i) {
							var today_date = new Date();
							var discount_frm_dt = new Date(v.discount_frm_dt);
							var discount_to_dt = new Date(v.discount_to_dt);

							if (today_date >= discount_frm_dt && today_date <= discount_to_dt) {
								v["should_apply_discount"] = true;
							}
							return v;
						});

						defer.resolve(res);
						_context3.next = 28;
						break;

					case 27:
						defer.resolve([]);

					case 28:
						_context3.next = 33;
						break;

					case 30:
						_context3.prev = 30;
						_context3.t0 = _context3['catch'](3);

						defer.reject(_context3.t0);

					case 33:
						return _context3.abrupt('return', defer.promise);

					case 34:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[3, 30]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

Timing.prototype.getTimingByBatId = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var bat_id, defer, res, get;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						get = function get() {
							var defer = Q.defer();
							var bat_id_str = '(' + bat_id.join(",") + ')';

							var qry = "select * from   " + ew_bat_dtls_table + " where bat_id in " + bat_id_str;

							cassconn.execute(qry, null, function (err, res) {
								if (err) {
									defer.reject(err);
									debug(err);
									return;
								}
								defer.resolve(res.rows);
							});
							return defer.promise;
						};

						bat_id = [].concat(data.bat_id);
						defer = Q.defer();
						_context4.prev = 3;
						_context4.next = 6;
						return get();

					case 6:
						res = _context4.sent;

						defer.resolve(res);
						_context4.next = 14;
						break;

					case 10:
						_context4.prev = 10;
						_context4.t0 = _context4['catch'](3);

						defer.reject(_context4.t0);
						return _context4.abrupt('return');

					case 14:
						return _context4.abrupt('return', defer.promise);

					case 15:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[3, 10]]);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

module.exports = new Timing();