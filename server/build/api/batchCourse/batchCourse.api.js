'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var timingAPI = require(configPath.api.batchCourse.batchTiming);
var gettingStartedAPI = require(configPath.api.batchCourse.gettingStarted);
var sessionsAPI = require(configPath.api.batchCourse.sessions);
var bundleAPI = require(configPath.api.bundle.bundle);
var uuid = require('uuid');
var debug = require('debug')("app:api:batchCourse.api.js");

var Q = require('q');

function BatchCourse() {}

BatchCourse.prototype.getAll = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
    var connection, collection, find_res, find;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    find = function find(col) {
                        return col.find({}, { _id: 0 }).toArray();
                    };

                    _context.prev = 1;
                    _context.next = 4;
                    return getConnection();

                case 4:
                    connection = _context.sent;
                    _context.next = 7;
                    return dbCollection(connection);

                case 7:
                    collection = _context.sent;
                    _context.next = 10;
                    return find(collection);

                case 10:
                    find_res = _context.sent;
                    return _context.abrupt('return', find_res);

                case 14:
                    _context.prev = 14;
                    _context.t0 = _context['catch'](1);

                    globalFunctions.err(_context.t0);

                case 17:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this, [[1, 14]]);
}));

BatchCourse.prototype.getBatchCourseById = function (data) {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({ crs_id: data.crs_id }, { _id: 0 }).toArray();
    }
};

BatchCourse.prototype.add = function (data) {
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        //insert/update in batchcourse
        var crs_id = data.crs_id || uuid.v4();
        data.crs_id = crs_id;

        return col.update({ crs_id: crs_id }, data, { upsert: true, w: 1 });
    }
};
BatchCourse.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ crs_id: data.crs_id });
    }
};

// Here We will get full details of included courses in given batch course
BatchCourse.prototype.getIncludedCoursesInBatchCourse = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var defer, crs_id, res_batch, batchcourse, includedBundlesId, bundleWithCourses, courses, i, bndl;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        defer = Q.defer();
                        crs_id = data.crs_id;
                        _context2.prev = 2;
                        _context2.next = 5;
                        return this.getBatchCourseById({ crs_id: crs_id });

                    case 5:
                        res_batch = _context2.sent;
                        batchcourse = res_batch[0];
                        includedBundlesId = batchcourse.includedBundles || [];
                        _context2.next = 10;
                        return bundleAPI.getFullBundlesDetail({ bndl_ids: includedBundlesId });

                    case 10:
                        bundleWithCourses = _context2.sent;
                        courses = [];

                        for (i = 0; i < bundleWithCourses.length; i++) {
                            bndl = bundleWithCourses[i];

                            courses.push.apply(courses, (0, _toConsumableArray3.default)(bndl.courses));
                        }
                        defer.resolve(courses);
                        _context2.next = 20;
                        break;

                    case 16:
                        _context2.prev = 16;
                        _context2.t0 = _context2['catch'](2);

                        console.log(_context2.t0);
                        defer.reject(_context2.t0);

                    case 20:
                        return _context2.abrupt('return', defer.promise);

                    case 21:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[2, 16]]);
    }));

    return function (_x) {
        return _ref2.apply(this, arguments);
    };
}();

// Get bundle course mapping 
// bndl_id -> [courses]
BatchCourse.prototype.getBundleCourseMappingInBatchCourse = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var defer, crs_id, res_batch, batchcourse, includedBundlesId, bundleWithCourses;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        defer = Q.defer();
                        crs_id = data.crs_id;
                        _context3.prev = 2;
                        _context3.next = 5;
                        return this.getBatchCourseById({ crs_id: crs_id });

                    case 5:
                        res_batch = _context3.sent;
                        batchcourse = res_batch[0] || {};
                        includedBundlesId = batchcourse.includedBundles || [];
                        _context3.next = 10;
                        return bundleAPI.getFullBundlesDetail({ bndl_ids: includedBundlesId });

                    case 10:
                        bundleWithCourses = _context3.sent;

                        defer.resolve(bundleWithCourses);
                        _context3.next = 18;
                        break;

                    case 14:
                        _context3.prev = 14;
                        _context3.t0 = _context3['catch'](2);

                        console.log(_context3.t0);
                        defer.reject(_context3.t0);

                    case 18:
                        return _context3.abrupt('return', defer.promise);

                    case 19:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[2, 14]]);
    }));

    return function (_x2) {
        return _ref3.apply(this, arguments);
    };
}();

// Here We will get full details of given batch course
BatchCourse.prototype.getBatchCourse = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var defer, crs_id, db, col, contactus, res, course, res_batch, batchcourse, res_timing, batchTiming, res_gs, gs_arr, res_sessions;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        defer = Q.defer();
                        crs_id = data.crs_id;
                        _context4.prev = 2;
                        _context4.next = 5;
                        return getConnection();

                    case 5:
                        db = _context4.sent;
                        _context4.next = 8;
                        return db.collection("contactus");

                    case 8:
                        col = _context4.sent;
                        _context4.next = 11;
                        return col.findOne({});

                    case 11:
                        contactus = _context4.sent;
                        _context4.next = 14;
                        return courseListAPI.getCoursesById({ ids: crs_id });

                    case 14:
                        res = _context4.sent;
                        course = res[0];
                        _context4.next = 18;
                        return this.getBatchCourseById({ crs_id: crs_id });

                    case 18:
                        res_batch = _context4.sent;
                        batchcourse = res_batch[0];
                        _context4.next = 22;
                        return timingAPI.getTimingByCourseId({ crs_id: crs_id });

                    case 22:
                        res_timing = _context4.sent;
                        batchTiming = res_timing;
                        _context4.next = 26;
                        return gettingStartedAPI.getByCourseId({ crs_id: crs_id });

                    case 26:
                        res_gs = _context4.sent;
                        gs_arr = res_gs && res_gs[0];
                        _context4.next = 30;
                        return sessionsAPI.getByCourseId({ crs_id: crs_id });

                    case 30:
                        res_sessions = _context4.sent;

                        defer.resolve((0, _extends3.default)({
                            contactus: contactus
                        }, course, batchcourse, {
                            batchTiming: batchTiming,
                            gettingStarted: gs_arr,
                            sessions: res_sessions
                        }));
                        _context4.next = 38;
                        break;

                    case 34:
                        _context4.prev = 34;
                        _context4.t0 = _context4['catch'](2);

                        console.log(_context4.t0);
                        defer.reject(_context4.t0);

                    case 38:
                        return _context4.abrupt('return', defer.promise);

                    case 39:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[2, 34]]);
    }));

    return function (_x3) {
        return _ref4.apply(this, arguments);
    };
}();

BatchCourse.prototype.getBatchCoursesByBundleIds = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
        var bundle_ids, connection, collection, find_res;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        //it can be array or one bundle
                        bundle_ids = [].concat(data.bundle_ids);
                        _context5.prev = 1;
                        _context5.next = 4;
                        return getConnection();

                    case 4:
                        connection = _context5.sent;
                        _context5.next = 7;
                        return dbCollection(connection);

                    case 7:
                        collection = _context5.sent;
                        _context5.next = 10;
                        return collection.find({
                            includedBundles: {
                                '$in': bundle_ids
                            }
                        }).toArray();

                    case 10:
                        find_res = _context5.sent;
                        return _context5.abrupt('return', find_res);

                    case 14:
                        _context5.prev = 14;
                        _context5.t0 = _context5['catch'](1);

                        console.log(_context5.t0);
                        throw _context5.t0;

                    case 18:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this, [[1, 14]]);
    }));

    return function (_x4) {
        return _ref5.apply(this, arguments);
    };
}();

function dbCollection(db) {
    return db.collection("batchcourse");
}

module.exports = new BatchCourse();