'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var uuid = require('uuid');

var Q = require('q');

function Currency() {}

Currency.prototype.getAll = function () {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({}, { _id: 0 }).toArray();
    }
};

Currency.prototype.add = function (data) {
    var insertData = function () {
        var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
            var obj, currency_id;
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            _context.next = 2;
                            return col.findOne({ currency_id: data.currency_id });

                        case 2:
                            obj = _context.sent;


                            if (!obj) {
                                data.create_dt = new Date();
                                data.update_dt = new Date();
                            } else {
                                data.update_dt = new Date();
                            }

                            currency_id = data.currency_id || uuid.v4();

                            data.currency_id = currency_id;

                            return _context.abrupt('return', col.update({ currency_id: currency_id }, { $set: data }, { upsert: true, w: 1 }));

                        case 7:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, this);
        }));

        return function insertData(_x) {
            return _ref.apply(this, arguments);
        };
    }();

    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });
};
Currency.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ currency_id: data.currency_id });
    }
};

function dbCollection(db) {
    return db.collection("currency");
}

module.exports = new Currency();