'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:batchCourse:sessionItems");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;

var ew_bat_sess_dtls = 'ew1.ew_bat_sess_dtls';

function SessionItem() {}

SessionItem.prototype.add = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var defer, bat_id, sess_id, sess_dt, sess_items, res, addToTable;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						addToTable = function addToTable() {
							var defer = Q.defer();

							var qry = "update " + ew_bat_sess_dtls + " set sess_items=?,sess_dt=? where bat_id = ? and sess_id=?";
							var params = [sess_items, sess_dt, bat_id, sess_id];

							cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
								if (err) {
									defer.reject(err);
									debug(err);
								}
								defer.resolve({});
							});
							return defer.promise;
						};

						defer = Q.defer();
						bat_id = data.bat_id;
						sess_id = Long.fromNumber(data.sess_id);
						sess_dt = data.sess_dt ? new Date(data.sess_dt) : undefined;
						sess_items = data.sess_items;
						_context.prev = 6;
						_context.next = 9;
						return addToTable();

					case 9:
						res = _context.sent;

						defer.resolve({});
						_context.next = 16;
						break;

					case 13:
						_context.prev = 13;
						_context.t0 = _context['catch'](6);

						defer.reject(_context.t0);

					case 16:
						return _context.abrupt('return', defer.promise);

					case 17:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[6, 13]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

SessionItem.prototype.getAll = function (data) {
	var defer = Q.defer();

	var qry = "select * from  " + ew_bat_sess_dtls;
	cassconn.execute(qry, null, function (err, res) {
		if (err) {
			defer.reject(err);
			debug(err);
		}
		defer.resolve(res.rows);
	});

	return defer.promise;
};

SessionItem.prototype.delete = function (data) {
	var defer = Q.defer();

	var bat_id = data.bat_id;
	var sess_id = data.sess_id;

	var qry = "delete from   " + ew_bat_dtls_table + " where bat_id = ? and sess_id = ?;";
	var params = [Long.fromNumber(parseInt(bat_id)), Long.fromNumber(parseInt(sess_id))];

	cassconn.execute(qry, params, function (err, res, r) {
		if (err) {
			defer.reject(err);
			debug(err);
		}
		defer.resolve({});
	});

	return defer.promise;
};

SessionItem.prototype.getSessionDetailsForBatchAndSession = function (data) {
	var defer = Q.defer();

	var bat_id = data.bat_id;
	var sess_id = data.sess_id;

	var qry = "select * from  " + ew_bat_sess_dtls + " where bat_id = ? and sess_id = ?;";
	var params = [Long.fromNumber(parseInt(bat_id)), Long.fromNumber(parseInt(sess_id))];

	cassconn.execute(qry, params, function (err, res) {
		if (err) {
			defer.reject(err);
			debug(err);
		}
		var jsonObj = JSON.parse(JSON.stringify(res.rows[0] || {}));
		var sess_items = jsonObj.sess_items || [];
		console.log(jsonObj, sess_items);
		var obj = [];
		for (var key in sess_items) {
			var type = key.substr(0, 1) === 'v' ? 'video' : 'download';
			var link = sess_items[key];
			var title = key.substr(2);
			obj.push({
				type: type,
				link: link,
				title: title
			});
		}
		jsonObj["sess_items_ui"] = obj;
		defer.resolve(jsonObj);
	});

	return defer.promise;
};

module.exports = new SessionItem();