'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:batchTiming.api.js");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batchTimingAPI = require(configPath.api.batchCourse.batchTiming);

var ew_bat_ds_timing_table = 'ew1.ew_bat_ds_timing';

function BatchTimingDashboard() {}

BatchTimingDashboard.prototype.getAllBatchTimingDashboard = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var query, res;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						query = "select * from " + ew_bat_ds_timing_table;
						_context.next = 4;
						return cassExecute(query);

					case 4:
						res = _context.sent;
						return _context.abrupt('return', res);

					case 8:
						_context.prev = 8;
						_context.t0 = _context['catch'](0);
						throw _context.t0;

					case 12:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 8]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

BatchTimingDashboard.prototype.getBatchTimingDashboardById = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var query, params, res;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						query = "select * from " + ew_bat_ds_timing_table + " where batch_id = ?";
						params = [Long.fromNumber(data.bat_id)];
						_context2.next = 5;
						return cassExecute(query, params);

					case 5:
						res = _context2.sent;
						return _context2.abrupt('return', res);

					case 9:
						_context2.prev = 9;
						_context2.t0 = _context2['catch'](0);
						throw _context2.t0;

					case 13:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 9]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

BatchTimingDashboard.prototype.setBatchTimingDashboardAsCompleted = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var bat_id, status, cls_end_dt, query, params, res;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						bat_id = Long.fromNumber(data.bat_id);
						status = data.status || "completed";
						cls_end_dt = data.cls_end_dt ? new Date(data.cls_end_dt) : undefined;
						query = "update " + ew_bat_ds_timing_table + " set status = ? , cls_end_dt =?  where batch_id =?";
						params = [status, cls_end_dt, bat_id];
						_context3.next = 8;
						return batchTimingAPI.setActualEndDateOfBatchTiming({ cls_actual_end_dt: cls_end_dt, bat_id: bat_id });

					case 8:
						_context3.next = 10;
						return cassExecute(query, params);

					case 10:
						res = _context3.sent;
						return _context3.abrupt('return', res);

					case 14:
						_context3.prev = 14;
						_context3.t0 = _context3['catch'](0);
						throw _context3.t0;

					case 18:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 14]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

BatchTimingDashboard.prototype.addBatchTimingDashboard = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var bat_id, status, cls_start_dt, nxt_cls_dt, cls_end_dt, cls_start_tm, nxt_cls_tm, training_id, query, params, res;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						bat_id = Long.fromNumber(data.bat_id);
						status = data.status;
						cls_start_dt = data.cls_start_dt ? new Date(data.cls_start_dt) : undefined;
						nxt_cls_dt = data.nxt_cls_dt ? new Date(data.nxt_cls_dt) : undefined;
						cls_end_dt = null;
						cls_start_tm = data.cls_start_tm;
						nxt_cls_tm = data.nxt_cls_tm;
						training_id = data.training_id;
						query = "update " + ew_bat_ds_timing_table + " set status = ? , cls_start_dt =? , nxt_cls_dt = ? , cls_start_tm = ? , nxt_cls_tm =? , training_id = ? where batch_id =?";
						params = [status, cls_start_dt, nxt_cls_dt, cls_start_tm, nxt_cls_tm, training_id, bat_id];


						logger.debug(query, params);

						_context4.next = 14;
						return cassExecute(query, params);

					case 14:
						res = _context4.sent;
						return _context4.abrupt('return', data);

					case 18:
						_context4.prev = 18;
						_context4.t0 = _context4['catch'](0);
						throw _context4.t0;

					case 22:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 18]]);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

BatchTimingDashboard.prototype.deleteBatchTimingDashboard = function () {
	var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
		var bat_id, query, params, res;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.prev = 0;
						bat_id = Long.fromNumber(data.bat_id);
						query = "delete from " + ew_bat_ds_timing_table + " where batch_id =?";
						params = [bat_id];
						_context5.next = 6;
						return cassExecute(query, params);

					case 6:
						res = _context5.sent;
						return _context5.abrupt('return', data);

					case 10:
						_context5.prev = 10;
						_context5.t0 = _context5['catch'](0);
						throw _context5.t0;

					case 14:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, this, [[0, 10]]);
	}));

	return function (_x5) {
		return _ref5.apply(this, arguments);
	};
}();

module.exports = new BatchTimingDashboard();