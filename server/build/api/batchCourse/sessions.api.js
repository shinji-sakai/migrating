'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var timingAPI = require(configPath.api.batchCourse.batchTiming);
var uuid = require('uuid');

var Q = require('q');

function Sessions() {}

Sessions.prototype.getAll = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
    var connection, collection, find_res, find;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    find = function find(col) {
                        return col.find({}, { _id: 0 }).toArray();
                    };

                    _context.prev = 1;
                    _context.next = 4;
                    return getConnection();

                case 4:
                    connection = _context.sent;
                    _context.next = 7;
                    return dbCollection(connection);

                case 7:
                    collection = _context.sent;
                    _context.next = 10;
                    return find(collection);

                case 10:
                    find_res = _context.sent;
                    return _context.abrupt('return', find_res);

                case 14:
                    _context.prev = 14;
                    _context.t0 = _context['catch'](1);

                    globalFunctions.err(_context.t0);

                case 17:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this, [[1, 14]]);
}));

Sessions.prototype.getByCourseId = function (data) {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({ crs_id: data.crs_id }, { _id: 0 }).toArray();
    }
};

Sessions.prototype.add = function (data) {
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        //insert/update in batchcourse
        var crs_id = data.crs_id || uuid.v4();
        data.crs_id = crs_id;

        return col.update({ crs_id: crs_id, sessionNumber: data.sessionNumber }, data, { upsert: true, w: 1 });
    }
};
Sessions.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ crs_id: data.crs_id, sessionNumber: data.sessionNumber });
    }
};

function dbCollection(db) {
    return db.collection("sessions");
}

module.exports = new Sessions();