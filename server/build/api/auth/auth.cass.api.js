'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var jwt = require('jsonwebtoken');
var authJWT = require("jwt-simple");
var request = require('request');
var qs = require('querystring');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);
var Base64 = require(configPath.lib.utility).Base64;
var shortid = require('shortid');
var mail = require(configPath.api.mail);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:auth:cass");
var crypto = require('crypto');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path');

var cassExecute = require(configPath.lib.utility).cassExecute;
var profileApi = require(configPath.api.dashboard.profile);
var adminAuthorApi = require(configPath.api.admin.author);
var chatFriendsApi = require(configPath.api.chat.friends);
var userCoursesAPI = require(configPath.api.usercourses);
var registerTemplate = require(configPath.api.emailTemplateAPI.registerTemplate);
var forgotpasswordTemplate = require(configPath.api.emailTemplateAPI.forgotpasswordTemplate);
var resendotpTemplate = require(configPath.api.emailTemplateAPI.resendotpTemplate);
var subscribeAPI = require(configPath.api.subscribe.subscribe);
var smsAPI = require(configPath.api.sms.sms);

var login_table = 'ew1.vt_usr_lgn';
var reg_table = 'ew1.vt_usr_reg';
var reg_otp_table = 'ew1.vt_usr_reg_otp';
var fpass_otp_table = 'ew1.vt_usr_fpass_otp';
var FORGOT_PASS_OTP_EXP_TM = 7200;

function Auth() {}

//Register User
//This function will be called when user will register first time in website
Auth.prototype.registerUser = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var defer, usr_id, dsp_nm, usr_email, usr_ph, usr_role, usr_profile_base64, pwd, reg_dt, isUserExists, targetProfilePicPath, base64Data, targetPath, otp, otp_exp_tm, otp_snt_dt, insertUser, insertUser_args, insertOTP, insertOTP_args, queries, queryOptions;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = (data.usr_id || "").trim().toLowerCase();
                        dsp_nm = data.dsp_nm;
                        usr_email = data.usr_email;
                        usr_ph = data.usr_ph || "";
                        usr_role = data.usr_role;
                        usr_profile_base64 = data.user_profile;
                        pwd = crypto.createHash('md5').update(data.pwd).digest("hex");
                        reg_dt = new Date();
                        _context2.next = 11;
                        return this.checkUserExistance({
                            usr_id: usr_id
                        });

                    case 11:
                        isUserExists = _context2.sent;

                        if (!isUserExists.userExists) {
                            _context2.next = 14;
                            break;
                        }

                        return _context2.abrupt('return', {
                            status: false,
                            message: userErrorsConfig.USER_EXISTS
                        });

                    case 14:
                        if (!usr_profile_base64) {
                            _context2.next = 26;
                            break;
                        }

                        base64Data = usr_profile_base64.replace(/^data:image\/\w+;base64,/, '');
                        targetPath = path.resolve(configPath.common_paths.user_pic_relative + '/' + usr_id + '.jpg');

                        targetProfilePicPath = path.normalize(configPath.common_paths.user_pic_absolute + '/' + usr_id + '.jpg');
                        _context2.prev = 18;
                        _context2.next = 21;
                        return fs.writeFileAsync(targetPath, base64Data, { encoding: 'base64' });

                    case 21:
                        _context2.next = 26;
                        break;

                    case 23:
                        _context2.prev = 23;
                        _context2.t0 = _context2['catch'](18);
                        return _context2.abrupt('return', {
                            status: false,
                            message: "Something went wrong in uploading profile pic.Please contact support."
                        });

                    case 26:
                        otp = '' + Math.floor(100000 + Math.random() * 900000);
                        otp_exp_tm = 172800; //in seconds 48 hr , 2 days

                        otp_snt_dt = reg_dt;

                        //Insert User into register table

                        insertUser = 'INSERT INTO ' + reg_table + ' (usr_id, dsp_nm, usr_email,usr_ph,reg_dt,pwd,role)  ' + 'VALUES(?, ?, ?, ?, ?,?,?);';
                        insertUser_args = [usr_id, dsp_nm, usr_email, usr_ph, reg_dt, pwd, usr_role];

                        //Insert entry for storing generated OTP

                        insertOTP = 'INSERT INTO ' + reg_otp_table + ' (usr_id, otp , otp_exp_tm , otp_snt_dt , usr_email,usr_ph,pwd)  ' + 'VALUES(?, ?, ?, ?, ?,?,?);';
                        insertOTP_args = [usr_id, otp, otp_exp_tm, otp_snt_dt, usr_email, usr_ph, pwd];
                        queries = [{
                            query: insertUser,
                            params: insertUser_args
                        }, {
                            query: insertOTP,
                            params: insertOTP_args
                        }];

                        // 

                        //save role to usr_dtls table

                        profileApi.saveUserBasicPersonalDetails({
                            usr_id: usr_id,
                            role: usr_role,
                            dsp_nm: dsp_nm
                        });

                        if (usr_role === 'author') {
                            //if registering user is author then add it to mongodb author table also
                            //this admin author table we are using in admin forms to add author to the course
                            //so to sync both the table we will add author to there also
                            adminAuthorApi.update({
                                authorId: usr_id,
                                authorName: dsp_nm
                            });
                        }

                        queryOptions = {
                            prepare: true,
                            consistency: cassandra.types.consistencies.quorum
                        };

                        cassconn.batch(queries, queryOptions, function () {
                            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(err, succ, res) {
                                var smsres, template, str, data;
                                return _regenerator2.default.wrap(function _callee$(_context) {
                                    while (1) {
                                        switch (_context.prev = _context.next) {
                                            case 0:
                                                if (!err) {
                                                    _context.next = 3;
                                                    break;
                                                }

                                                defer.reject(err);
                                                return _context.abrupt('return');

                                            case 3:
                                                _context.prev = 3;
                                                _context.next = 6;
                                                return chatFriendsApi.addFriend({
                                                    user_id: usr_id,
                                                    fr_id: 'support'
                                                });

                                            case 6:
                                                _context.next = 8;
                                                return chatFriendsApi.addUserShortDetails({
                                                    user_id: usr_id,
                                                    user_email: usr_email,
                                                    display_name: dsp_nm,
                                                    typ: 'usr',
                                                    pic: targetProfilePicPath
                                                });

                                            case 8:
                                                _context.next = 10;
                                                return subscribeAPI.subscribeUser({
                                                    subs_email: usr_email,
                                                    subs_typ: 'registration form',
                                                    usr_id: usr_id
                                                });

                                            case 10:
                                                _context.next = 12;
                                                return profileApi.saveShortDetails({
                                                    usr_id: (usr_id || "").trim().toLowerCase(),
                                                    usr_email: usr_email,
                                                    dsp_nm: dsp_nm,
                                                    usr_pic: targetProfilePicPath,
                                                    usr_ph: usr_ph
                                                });

                                            case 12:
                                                _context.next = 14;
                                                return profileApi.addEmailInContactDetails({
                                                    usr_id: usr_id,
                                                    email: usr_email,
                                                    isPrimary: true
                                                });

                                            case 14:
                                                if (!usr_ph) {
                                                    _context.next = 34;
                                                    break;
                                                }

                                                _context.prev = 15;
                                                _context.next = 18;
                                                return smsAPI.firstTimeLogin(usr_ph, usr_id, otp);

                                            case 18:
                                                smsres = _context.sent;

                                                // var smsres = await smsAPI.sendSMS({
                                                //     message: 'From examwarrior.com, OTP for 1st time login: ' + otp,
                                                //     numbers: usr_ph
                                                // });
                                                debug(smsres);
                                                _context.next = 34;
                                                break;

                                            case 22:
                                                _context.prev = 22;
                                                _context.t0 = _context['catch'](15);

                                                debug("Sms Sending error :", _context.t0);
                                                //try once again
                                                _context.prev = 25;
                                                _context.next = 28;
                                                return smsAPI.firstTimeLogin(usr_ph, usr_id, otp);

                                            case 28:
                                                smsres = _context.sent;
                                                _context.next = 34;
                                                break;

                                            case 31:
                                                _context.prev = 31;
                                                _context.t1 = _context['catch'](25);

                                                debug("2nd time Sms Sending error :", _context.t1);

                                            case 34:
                                                _context.next = 36;
                                                return registerTemplate({
                                                    username: usr_id,
                                                    otp: otp,
                                                    usr_email: usr_email,
                                                    usr_ph: usr_ph,
                                                    dsp_nm: dsp_nm
                                                });

                                            case 36:
                                                template = _context.sent;


                                                //Send otp to user email
                                                str = 'Hi,' + dsp_nm + "<br /> Thanks for Registration.<br />Your OTP for registration is : <strong>" + otp + "</strong>";
                                                data = {
                                                    to: usr_email,
                                                    subject: 'OTP for Registration - examwarrior.com',
                                                    text: template.html
                                                };

                                                mail.sendMail(data);
                                                _context.next = 46;
                                                break;

                                            case 42:
                                                _context.prev = 42;
                                                _context.t2 = _context['catch'](3);

                                                debug(_context.t2);
                                                defer.reject(_context.t2);

                                            case 46:

                                                defer.resolve({
                                                    status: 'success',
                                                    err: 'Registration Successfull'
                                                });

                                            case 47:
                                            case 'end':
                                                return _context.stop();
                                        }
                                    }
                                }, _callee, this, [[3, 42], [15, 22], [25, 31]]);
                            }));

                            return function (_x2, _x3, _x4) {
                                return _ref2.apply(this, arguments);
                            };
                        }());
                        return _context2.abrupt('return', defer.promise);

                    case 39:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[18, 23]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();
Auth.prototype.verifyOTP = function (data) {
    var defer = Q.defer();
    var usr_id = (data.usr_id || "").trim().toLowerCase();
    var otp = data.otp;
    var dsp_nm = '';
    var crt_dt = new Date();
    var otp_exp_tm = 172800; //in seconds 48 hr , 2 days

    var selectOtp = 'SELECT * from ' + reg_otp_table + ' where usr_id = ?';
    var selectOtp_args = [usr_id];

    cassconn.execute(selectOtp, selectOtp_args, function (err, res, r) {
        if (err) {
            defer.reject(err);
            return;
        }
        if (!res.rows) {
            defer.resolve({
                err: 'Something went wrong.'
            });
        } else {
            if (res.rows.length <= 0) {
                defer.resolve({
                    err: userErrorsConfig.USER_NOT_EXIST
                });
                return;
            }
            var row = res.rows[0];
            var real_otp = row.get('otp');
            var send_dt = row.get('otp_snt_dt');
            var usr_email = row.get('usr_email');
            var pwd = row.get('pwd');

            var dt = new Date(send_dt);
            var now_dt = new Date();

            var timeDiff = Math.abs(now_dt - dt); // in millisec
            var diff_secs = timeDiff / 1000;

            var exp_tm = row.get('otp_exp_tm');

            if (diff_secs > otp_exp_tm) {
                defer.resolve({
                    status: 'error',
                    err: 'OTP Expired'
                });
            } else {
                if (real_otp === otp) {
                    defer.resolve({
                        usr_email: usr_email,
                        pwd: pwd
                    });
                } else {
                    defer.resolve({
                        status: 'error',
                        err: "OTP doesn't match"
                    });
                }
            }
        }
    });
    return defer.promise;
};
Auth.prototype.resendOTP = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var defer, usr_email, usr_ph, usr_id, otp, otp_snt_dt, getUser, getUser_args, getUserRes, updateOTP, updateOTP_args, queries, queryOptions;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        defer = Q.defer();
                        usr_email = data.usr_email;
                        usr_id = (data.usr_id || "").trim().toLowerCase();
                        otp = '' + Math.floor(100000 + Math.random() * 900000);
                        otp_snt_dt = new Date();

                        // if (!usr_email) {

                        getUser = 'select * from ' + reg_table + ' where usr_id = ?';
                        getUser_args = [usr_id];
                        _context4.next = 9;
                        return cassExecute(getUser, getUser_args);

                    case 9:
                        getUserRes = _context4.sent;

                        if (getUserRes) {
                            usr_email = getUserRes[0]["usr_email"];
                            usr_ph = getUserRes[0]["usr_ph"];
                        }
                        // }


                        //Insert entry for storing generated OTP
                        updateOTP = 'Update ' + reg_otp_table + ' SET otp=?,otp_snt_dt=?  where usr_id = ?';
                        updateOTP_args = [otp, new Date(), usr_id];
                        queries = [{
                            query: updateOTP,
                            params: updateOTP_args
                        }];
                        queryOptions = {
                            prepare: true,
                            consistency: cassandra.types.consistencies.quorum
                        };

                        cassconn.batch(queries, queryOptions, function () {
                            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(err, succ, res) {
                                var template, str, data;
                                return _regenerator2.default.wrap(function _callee3$(_context3) {
                                    while (1) {
                                        switch (_context3.prev = _context3.next) {
                                            case 0:
                                                if (!err) {
                                                    _context3.next = 3;
                                                    break;
                                                }

                                                defer.reject(err);
                                                return _context3.abrupt('return');

                                            case 3:
                                                _context3.next = 5;
                                                return resendotpTemplate({
                                                    username: usr_id,
                                                    otp: otp
                                                });

                                            case 5:
                                                template = _context3.sent;


                                                //Send otp to user email
                                                str = 'Hi,' + "<br /> Thanks for Registration.<br />Your OTP for registration is : <strong>" + otp + "</strong>";
                                                data = {
                                                    to: usr_email,
                                                    subject: 'OTP for Registration - examwarrior.com',
                                                    text: template.html
                                                };

                                                mail.sendMail(data);

                                                if (!usr_ph) {
                                                    _context3.next = 12;
                                                    break;
                                                }

                                                _context3.next = 12;
                                                return smsAPI.firstTimeLogin(usr_ph, usr_id, otp);

                                            case 12:

                                                defer.resolve({
                                                    status: 'success',
                                                    message: 'OTP Sent Successfully'
                                                });

                                            case 13:
                                            case 'end':
                                                return _context3.stop();
                                        }
                                    }
                                }, _callee3, this);
                            }));

                            return function (_x6, _x7, _x8) {
                                return _ref4.apply(this, arguments);
                            };
                        }());
                        return _context4.abrupt('return', defer.promise);

                    case 17:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this);
    }));

    return function (_x5) {
        return _ref3.apply(this, arguments);
    };
}();
Auth.prototype.validateLoginDetails = function (data) {
    var self = this;
    return findUser().then(validate).then(checkForOTP).then(isFirstTime).catch(globalFunctions.err);

    function findUser() {
        var defer = Q.defer();
        var selectUser = 'select * from ' + login_table + ' where usr_id = ?;';
        var args = [(data.usr_id || "").trim().toLowerCase()];
        cassconn.execute(selectUser, args, function (err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            if (!res) {
                defer.resolve([]);
            } else {
                defer.resolve(res.rows);
            }
        });
        return defer.promise;
    }

    function validate(user) {
        var hashPwd = crypto.createHash('md5').update(data.pwd).digest("hex");
        if (user.length > 0 && user[0].get('pwd') === hashPwd) {
            return profileApi.getUserShortDetails({
                usr_id: user[0].get('usr_id')
            }).then(function (data) {
                // debug("validate.....",data);
                data.is_act = user[0].get('is_act');
                var obj = {
                    user: data,
                    firstTime: false
                };
                return obj;
            }).catch(function (err) {
                debug(err);
            });
        } else if (user.length == 0) {
            return {
                err: userErrorsConfig.USER_NOT_EXIST
            };
        } else {
            return {
                err: userErrorsConfig.PASSWORD_MISMATCH
            };
        }
    }

    function checkForOTP(res) {
        if (!res.err || res.err === userErrorsConfig.PASSWORD_MISMATCH) {
            //If no error or password missmatch then forward ahead 
            return res;
        }
        var d = {
            usr_id: (data.usr_id || "").trim().toLowerCase(),
            otp: data.pwd
        };
        return self.verifyOTP(d).then(function (verifiedStatus) {
            if (!verifiedStatus.err) {
                //if there is no erroe
                //means user is logging in first time and successfully entered otp
                return {
                    firstTime: true,
                    user: {
                        usr_id: (data.usr_id || "").trim().toLowerCase(),
                        pwd: verifiedStatus.pwd, // real password
                        usr_email: verifiedStatus.usr_email
                    }
                };
            } else if (verifiedStatus.err) {
                if (verifiedStatus.err !== userErrorsConfig.USER_NOT_EXIST) {
                    //user is exist but wrong otp
                    return {
                        firstTime: true,
                        err: verifiedStatus.err
                    };
                } else {
                    //user is not exist
                    return {
                        err: verifiedStatus.err
                    };
                }
            }
        });
    }

    function isFirstTime(obj) {
        if (obj.err) {
            return obj;
        }

        if (!obj.firstTime) {
            return obj;
        }

        //if it is first time
        //save details to login table
        var defer = Q.defer();

        var real_pwd = obj.user.pwd; //This Password is from registration form
        var email = obj.user.usr_email;

        //fetch query for reg table
        var reg_details = "select * from " + reg_table + ' where usr_id = ?';
        var reg_args = [(data.usr_id || "").trim().toLowerCase()];

        //first fetch info from reg table
        cassconn.execute(reg_details, reg_args, function (err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            //if there is no for user 
            if (!res.rows || res.rows.length <= 0) {
                defer.reject(new Error("Something went wrong"));
                return;
            }

            //user details from reg table
            var usr = res.rows[0];

            //insert query for login table
            var insertUser = 'insert into ' + login_table + ' (usr_id,pwd,is_act,crt_dt) values (?,?,?,?);';
            var insertUser_args = [(data.usr_id || "").trim().toLowerCase(), real_pwd, 'true', new Date()];

            //insert user into login table
            cassconn.execute(insertUser, insertUser_args, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                var d = {
                    firstTime: true,
                    user: {
                        usr_id: (data.usr_id || "").trim().toLowerCase(),
                        pwd: real_pwd,
                        dsp_nm: usr.get("dsp_nm"),
                        usr_email: email,
                        usr_ph: usr.get("usr_ph"),
                        is_act: true
                    }
                };

                defer.resolve(d);
            });
        });
        return defer.promise;
    }
};
Auth.prototype.checkUserExistance = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
        var findUser = function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5() {
                var defer, selectUser, args, rows;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                defer = Q.defer();
                                _context5.prev = 1;
                                selectUser = 'select * from ' + login_table + ' where usr_id = ?;';
                                args = [(data.usr_id || "").trim().toLowerCase()];
                                _context5.next = 6;
                                return cassExecute(selectUser, args);

                            case 6:
                                rows = _context5.sent;

                                if (!(rows.length > 0)) {
                                    _context5.next = 11;
                                    break;
                                }

                                return _context5.abrupt('return', rows);

                            case 11:
                                selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
                                args = [(data.usr_id || "").trim().toLowerCase()];
                                _context5.next = 15;
                                return cassExecute(selectUser, args);

                            case 15:
                                rows = _context5.sent;
                                return _context5.abrupt('return', rows);

                            case 17:
                                _context5.next = 23;
                                break;

                            case 19:
                                _context5.prev = 19;
                                _context5.t0 = _context5['catch'](1);

                                debug(_context5.t0);
                                throw _context5.t0;

                            case 23:
                                return _context5.abrupt('return', defer.promise);

                            case 24:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[1, 19]]);
            }));

            return function findUser() {
                return _ref6.apply(this, arguments);
            };
        }();

        var checkUser;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        checkUser = function checkUser(user) {
                            if (user.length > 0) {
                                return {
                                    userExists: true
                                };
                            }
                            return {
                                userExists: false
                            };
                        };

                        return _context6.abrupt('return', findUser().then(checkUser).catch(globalFunctions.err));

                    case 2:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, this);
    }));

    return function (_x9) {
        return _ref5.apply(this, arguments);
    };
}();
Auth.prototype.removeUser = function (data) {
    return removeUser().catch(globalFunctions.err);

    function removeUser(col) {
        var defer = Q.defer();
        var selectUser = 'delete from ' + login_table + ' where user_id = ?;';
        var args = [(data.usr_id || "").trim().toLowerCase()];
        cassconn.execute(selectUser, args, function (err, succ, res) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }
};
Auth.prototype.checkPassword = function (data) {
    return findUser().then(checkPassword).catch(globalFunctions.err);

    function findUser() {
        var defer = Q.defer();
        var selectUser = 'select * from ' + login_table + ' where usr_id = ?;';
        var args = [(data.usr_id || "").trim().toLowerCase()];
        cassconn.execute(selectUser, args, function (err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });
        return defer.promise;
    }

    function checkPassword(user) {
        if (user.length > 0) {
            var user = user[0];
            var hashPwd = crypto.createHash('md5').update(data.pwd).digest("hex");
            if (user.get('pwd') === hashPwd) {
                return {
                    err: null
                };
            } else {
                return {
                    err: userErrorsConfig.PASSWORD_MISMATCH
                };
            }
        }
        return {
            err: userErrorsConfig.PASSWORD_MISMATCH
        };
    }
};
Auth.prototype.changePassword = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
        var obj, updateUser;
        return _regenerator2.default.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        updateUser = function updateUser() {
                            var hashPwd = crypto.createHash('md5').update(data.pwd).digest("hex");

                            var defer = Q.defer();
                            var selectUser = 'update ' + login_table + ' set pwd = ? where usr_id = ?;';

                            var args = [hashPwd, (data.usr_id || "").trim().toLowerCase()];

                            debug(args);
                            cassconn.execute(selectUser, args, function (err, res, r) {
                                if (err) {
                                    defer.reject(err);
                                    return;
                                }
                                defer.resolve({});
                            });
                            return defer.promise;
                        };

                        _context7.prev = 1;

                        if (data.usr_id) {
                            _context7.next = 4;
                            break;
                        }

                        return _context7.abrupt('return', {
                            error: "usr_id field is required."
                        });

                    case 4:
                        if (data.pwd) {
                            _context7.next = 6;
                            break;
                        }

                        return _context7.abrupt('return', {
                            error: "pwd field is required."
                        });

                    case 6:
                        _context7.next = 8;
                        return this.checkUserExistance({ usr_id: data.usr_id });

                    case 8:
                        obj = _context7.sent;

                        if (!obj.userExists) {
                            _context7.next = 15;
                            break;
                        }

                        _context7.next = 12;
                        return updateUser();

                    case 12:
                        return _context7.abrupt('return', {
                            message: "Password successfully updated."
                        });

                    case 15:
                        return _context7.abrupt('return', {
                            error: "User doesn't exist"
                        });

                    case 16:
                        _context7.next = 22;
                        break;

                    case 18:
                        _context7.prev = 18;
                        _context7.t0 = _context7['catch'](1);

                        console.log(_context7.t0);
                        throw new Error(_context7.t0);

                    case 22:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, this, [[1, 18]]);
    }));

    return function (_x10) {
        return _ref7.apply(this, arguments);
    };
}();
Auth.prototype.forgotPassword_old = function (data) {
    var usr_id = (data.usr_id || "").trim().toLowerCase();
    return findUser().then(function () {
        var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(user) {
            var email, usr_id, date, time, obj, str, bs_en, link, template, mailData;
            return _regenerator2.default.wrap(function _callee8$(_context8) {
                while (1) {
                    switch (_context8.prev = _context8.next) {
                        case 0:
                            console.log(user);
                            email = user.get('usr_email');
                            usr_id = (data.usr_id || "").trim().toLowerCase();
                            date = new Date();
                            time = date.getTime();
                            obj = {
                                usr_email: email,
                                usr_id: usr_id,
                                time: time
                            };
                            str = JSON.stringify(obj);
                            bs_en = Base64.encode(str);
                            link = "https://www.examwarrior.com/changepassword/" + bs_en;
                            _context8.next = 11;
                            return forgotpasswordTemplate({
                                username: usr_id,
                                link: link
                            });

                        case 11:
                            template = _context8.sent;
                            str = 'Hi,' + usr_id + "<br /> Click on below link to change password.This link is valid only for one hour.<br /><a href='" + link + "' target='_blank' >Change Password</a>";
                            mailData = {
                                to: email,
                                subject: 'Forgot Password - examwarrior.com',
                                text: template.html
                            };

                            console.log(mailData);
                            mail.sendMail(mailData);
                            return _context8.abrupt('return', {
                                usr_email: email
                            });

                        case 17:
                        case 'end':
                            return _context8.stop();
                    }
                }
            }, _callee8, this);
        }));

        return function (_x11) {
            return _ref8.apply(this, arguments);
        };
    }()).catch(globalFunctions.err);

    function findUser() {
        var defer = Q.defer();
        var selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
        var args = [usr_id.trim()];

        cassconn.execute(selectUser, args, function (err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }

            if (!res) {
                defer.resolve([]);
            } else {
                defer.resolve(res.rows[0]);
            }
        });
        return defer.promise;
    }
};
Auth.prototype.forgotPassword = function () {
    var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(data) {
        var usr_id, userExistsRes, selectUser, args, rows, user, email, ph, date, otp, otp_exp_tm, otp_snt_dt, insertOTP, insertOTP_args;
        return _regenerator2.default.wrap(function _callee9$(_context9) {
            while (1) {
                switch (_context9.prev = _context9.next) {
                    case 0:
                        usr_id = (data.usr_id || "").trim().toLowerCase();

                        /////////////Check user existance

                        _context9.next = 3;
                        return this.checkUserExistance(data);

                    case 3:
                        userExistsRes = _context9.sent;

                        if (userExistsRes.userExists) {
                            _context9.next = 6;
                            break;
                        }

                        return _context9.abrupt('return', {
                            err: userErrorsConfig.USER_NOT_EXIST
                        });

                    case 6:
                        /////////////

                        selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
                        args = [usr_id.trim()];
                        _context9.next = 10;
                        return cassExecute(selectUser, args);

                    case 10:
                        rows = _context9.sent;
                        user = rows[0];
                        email = user['usr_email'];
                        ph = user['usr_ph'];
                        usr_id = (data.usr_id || "").trim().toLowerCase();
                        date = new Date();
                        otp = '' + Math.floor(100000 + Math.random() * 900000);
                        otp_exp_tm = FORGOT_PASS_OTP_EXP_TM; //in seconds 2 hr

                        otp_snt_dt = date;

                        ///////////////Insert entry for storing generated OTP

                        insertOTP = 'INSERT INTO ' + fpass_otp_table + ' (usr_id, otp  , otp_snt_dt,otp_exp_tm)  ' + 'VALUES(?, ?, ?,?);';
                        insertOTP_args = [usr_id, otp, otp_snt_dt, otp_exp_tm];
                        _context9.next = 23;
                        return cassExecute(insertOTP, insertOTP_args, {
                            prepare: true
                        });

                    case 23:
                        if (!email) {
                            _context9.next = 26;
                            break;
                        }

                        _context9.next = 26;
                        return this.sendForgotPasswordOTPMail({
                            usr_id: usr_id,
                            email: email,
                            otp: otp
                        });

                    case 26:

                        if (ph) {
                            this.sendForgotPasswordOTPSMS({
                                otp: otp,
                                ph: ph,
                                usr_id: usr_id
                            });
                        }

                        return _context9.abrupt('return', {
                            email: email,
                            ph: ph.substr(0, 3) + "xxxx" + ph.substr(-3)
                        });

                    case 28:
                    case 'end':
                        return _context9.stop();
                }
            }
        }, _callee9, this);
    }));

    return function (_x12) {
        return _ref9.apply(this, arguments);
    };
}();
Auth.prototype.sendForgotPasswordOTPMail = function () {
    var _ref10 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(data) {
        var template, mailData;
        return _regenerator2.default.wrap(function _callee10$(_context10) {
            while (1) {
                switch (_context10.prev = _context10.next) {
                    case 0:
                        _context10.prev = 0;
                        _context10.next = 3;
                        return forgotpasswordTemplate({
                            username: (data.usr_id || "").trim().toLowerCase(),
                            otp: data.otp
                        });

                    case 3:
                        template = _context10.sent;
                        mailData = {
                            to: data.email,
                            subject: 'Forgot Password OTP - examwarrior.com',
                            text: template.html
                        };
                        _context10.next = 7;
                        return mail.sendMail(mailData);

                    case 7:
                        _context10.next = 13;
                        break;

                    case 9:
                        _context10.prev = 9;
                        _context10.t0 = _context10['catch'](0);

                        debug(_context10.t0);
                        throw _context10.t0;

                    case 13:
                    case 'end':
                        return _context10.stop();
                }
            }
        }, _callee10, this, [[0, 9]]);
    }));

    return function (_x13) {
        return _ref10.apply(this, arguments);
    };
}();
Auth.prototype.sendForgotPasswordOTPSMS = function () {
    var _ref11 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11(data) {
        var smsres;
        return _regenerator2.default.wrap(function _callee11$(_context11) {
            while (1) {
                switch (_context11.prev = _context11.next) {
                    case 0:
                        _context11.prev = 0;
                        _context11.next = 3;
                        return smsAPI.sendForgotPasswordSMS(data.ph, (data.usr_id || "").trim().toLowerCase(), data.otp);

                    case 3:
                        smsres = _context11.sent;

                        debug(smsres);
                        _context11.next = 19;
                        break;

                    case 7:
                        _context11.prev = 7;
                        _context11.t0 = _context11['catch'](0);

                        debug("Sms Sending error :", _context11.t0);
                        //try once again
                        _context11.prev = 10;
                        _context11.next = 13;
                        return smsAPI.sendForgotPasswordSMS(data.ph, (data.usr_id || "").trim().toLowerCase(), data.otp);

                    case 13:
                        smsres = _context11.sent;
                        _context11.next = 19;
                        break;

                    case 16:
                        _context11.prev = 16;
                        _context11.t1 = _context11['catch'](10);

                        debug("2nd time Sms Sending error :", _context11.t1);

                    case 19:
                    case 'end':
                        return _context11.stop();
                }
            }
        }, _callee11, this, [[0, 7], [10, 16]]);
    }));

    return function (_x14) {
        return _ref11.apply(this, arguments);
    };
}();
Auth.prototype.resendForgotPasswordOTP = function () {
    var _ref12 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12(data) {
        var usr_id, otp, otp_snt_dt, selectUser, args, rows, user, email, ph, updateOTP, updateOTP_args;
        return _regenerator2.default.wrap(function _callee12$(_context12) {
            while (1) {
                switch (_context12.prev = _context12.next) {
                    case 0:
                        _context12.prev = 0;
                        usr_id = (data.usr_id || "").trim().toLowerCase();
                        otp = '' + Math.floor(100000 + Math.random() * 900000);
                        otp_snt_dt = new Date();

                        //find user email and phone number

                        selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
                        args = [usr_id.trim()];
                        _context12.next = 8;
                        return cassExecute(selectUser, args);

                    case 8:
                        rows = _context12.sent;
                        user = rows[0];
                        email = user["usr_email"];
                        ph = user['usr_ph'];

                        //Insert entry for storing generated OTP

                        updateOTP = 'Update ' + fpass_otp_table + ' SET otp=?,otp_snt_dt=?  where usr_id = ?';
                        updateOTP_args = [otp, otp_snt_dt, usr_id];
                        _context12.next = 16;
                        return cassExecute(updateOTP, updateOTP_args);

                    case 16:
                        if (!email) {
                            _context12.next = 19;
                            break;
                        }

                        _context12.next = 19;
                        return this.sendForgotPasswordOTPMail({
                            usr_id: usr_id,
                            email: email,
                            otp: otp
                        });

                    case 19:
                        if (ph) {
                            this.sendForgotPasswordOTPSMS({
                                otp: otp,
                                ph: ph,
                                usr_id: usr_id
                            });
                        }

                        return _context12.abrupt('return', {
                            email: email,
                            ph: ph.substr(0, 3) + "xxxx" + ph.substr(-3)
                        });

                    case 23:
                        _context12.prev = 23;
                        _context12.t0 = _context12['catch'](0);

                        debug(_context12.t0);
                        throw _context12.t0;

                    case 27:
                    case 'end':
                        return _context12.stop();
                }
            }
        }, _callee12, this, [[0, 23]]);
    }));

    return function (_x15) {
        return _ref12.apply(this, arguments);
    };
}();
Auth.prototype.verifyForgotPasswordOTP = function () {
    var _ref13 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee13(data) {
        var usr_id, otp, selectOtp, selectOtp_args, rows, row, real_otp, send_dt, dt, now_dt, timeDiff, diff_secs, exp_tm, selectUser, args, user;
        return _regenerator2.default.wrap(function _callee13$(_context13) {
            while (1) {
                switch (_context13.prev = _context13.next) {
                    case 0:
                        usr_id = (data.usr_id || "").trim().toLowerCase();
                        otp = data.otp;
                        selectOtp = 'SELECT * from ' + fpass_otp_table + ' where usr_id = ?';
                        selectOtp_args = [usr_id];
                        _context13.next = 6;
                        return cassExecute(selectOtp, selectOtp_args);

                    case 6:
                        rows = _context13.sent;
                        row = rows[0];
                        real_otp = row['otp'];
                        send_dt = row['otp_snt_dt'];
                        dt = new Date(send_dt);
                        now_dt = new Date();
                        timeDiff = Math.abs(now_dt - dt); // in millisec

                        diff_secs = timeDiff / 1000;
                        exp_tm = FORGOT_PASS_OTP_EXP_TM;

                        if (!(diff_secs > exp_tm)) {
                            _context13.next = 19;
                            break;
                        }

                        return _context13.abrupt('return', {
                            err: 'OTP Expired'
                        });

                    case 19:
                        if (!(real_otp === otp)) {
                            _context13.next = 30;
                            break;
                        }

                        //get user details
                        selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
                        args = [usr_id.trim()];
                        _context13.next = 24;
                        return cassExecute(selectUser, args);

                    case 24:
                        rows = _context13.sent;
                        user = rows[0];

                        delete user["pwd"];
                        return _context13.abrupt('return', user);

                    case 30:
                        return _context13.abrupt('return', {
                            err: "OTP doesn't match"
                        });

                    case 31:
                    case 'end':
                        return _context13.stop();
                }
            }
        }, _callee13, this);
    }));

    return function (_x16) {
        return _ref13.apply(this, arguments);
    };
}();

Auth.prototype.facebook = function (data) {
    var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'];
    var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
    var graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.FACEBOOK_SECRET,
        redirect_uri: data.redirectUri
    };
    var defer = Q.defer();
    request.get({
        url: accessTokenUrl,
        qs: params,
        json: true
    }, function (err, response, accessToken) {
        if (response.statusCode !== 200) {
            var e = new Error(accessToken.error.message);
            defer.reject(e);
        }
        console.log("accessToken......", accessToken);
        request.get({
            url: graphApiUrl,
            qs: accessToken,
            json: true
        }, function (err, response, profile) {

            if (response.statusCode !== 200) {
                var e = new Error(profile.error.message);
                defer.reject(e);
            }

            console.log(profile);
            defer.resolve(profile);
        });
    });
    return defer.promise;
};
Auth.prototype.google = function (data) {
    var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.GOOGLE_SECRET,
        redirect_uri: data.redirectUri,
        grant_type: 'authorization_code'
    };
    var defer = Q.defer();
    request.post(accessTokenUrl, {
        json: true,
        form: params
    }, function (err, response, token) {
        if (response.statusCode !== 200) {
            var e = new Error("Some Error Occurred");
            defer.reject(e);
        }
        var accessToken = token.access_token;
        var headers = {
            Authorization: 'Bearer ' + accessToken
        };

        request.get({
            url: peopleApiUrl,
            headers: headers,
            json: true
        }, function (err, response, profile) {
            if (profile.error) {
                var e = new Error(profile.error.message);
                defer.reject(e);
            }
            defer.resolve(profile);
        });
    });
    return defer.promise;
};
Auth.prototype.twitter = function (data) {
    var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
    var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
    var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';
    var defer = Q.defer();
    if (!data.oauth_token || !data.oauth_verifier) {
        var requestTokenOauth = {
            consumer_key: config.TWITTER_KEY,
            consumer_secret: config.TWITTER_SECRET,
            callback: data.redirectUri
        };

        // Step 1. Obtain request token for the authorization popup.
        request.post({
            url: requestTokenUrl,
            oauth: requestTokenOauth
        }, function (err, response, body) {
            var oauthToken = qs.parse(body);

            // Step 2. Send OAuth token back to open the authorization screen.
            defer.resolve(oauthToken);
        });
    } else {
        // Part 2 of 2: Second request after Authorize app is clicked.
        var accessTokenOauth = {
            consumer_key: config.TWITTER_KEY,
            consumer_secret: config.TWITTER_SECRET,
            token: data.oauth_token,
            verifier: data.oauth_verifier
        };
        console.log("accessTokenOauth.........", accessTokenOauth);

        // Step 3. Exchange oauth token and oauth verifier for access token.
        request.post({
            url: accessTokenUrl,
            oauth: accessTokenOauth
        }, function (err, response, accessToken) {
            if (response.statusCode !== 200) {
                var e = new Error("Some Error Occured");
                defer.reject(e);
            }
            accessToken = qs.parse(accessToken);
            console.log("accessToken.........", accessToken);

            var profileOauth = {
                consumer_key: config.TWITTER_KEY,
                consumer_secret: config.TWITTER_SECRET,
                oauth_token: accessToken.oauth_token
            };

            // Step 4. Retrieve profile information about the current user.
            request.get({
                url: profileUrl + accessToken.screen_name,
                oauth: profileOauth,
                json: true
            }, function (err, response, profile) {
                if (response.statusCode !== 200) {
                    var e = new Error("Some Error Occured");
                    defer.reject(e);
                }
                defer.resolve(profile);
            });
        });
    }
    return defer.promise;
};
Auth.prototype.linkedin = function (data) {
    var accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken';
    var peopleApiUrl = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url)';
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.LINKEDIN_SECRET,
        redirect_uri: data.redirectUri,
        grant_type: 'authorization_code'
    };
    var defer = Q.defer();
    request.post(accessTokenUrl, {
        form: params,
        json: true
    }, function (err, response, body) {
        if (response.statusCode !== 200) {
            var e = new Error(body.error_description);
            defer.reject(e);
        }
        var params = {
            oauth2_access_token: body.access_token,
            format: 'json'
        };

        // Step 2. Retrieve profile information about the current user.
        request.get({
            url: peopleApiUrl,
            qs: params,
            json: true
        }, function (err, response, profile) {
            if (response.statusCode !== 200) {
                var e = new Error("Some Error Occured");
                defer.reject(e);
            }
            defer.resolve(profile);
        });
    });
    return defer.promise;
};

Auth.prototype.updateSocialMediaDetails = function () {
    var _ref14 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee14(data) {
        var usr_id, dsp_nm, usr_email, usr_ph, usr_role, reg_dt;
        return _regenerator2.default.wrap(function _callee14$(_context14) {
            while (1) {
                switch (_context14.prev = _context14.next) {
                    case 0:

                        debug("updateSocialMediaDetails.....", data);

                        usr_id = (data.usr_id || "").trim().toLowerCase();
                        dsp_nm = data.dsp_nm;
                        usr_email = data.usr_email;
                        usr_ph = data.usr_ph || "";
                        usr_role = data.usr_role || "public";
                        reg_dt = new Date();
                        _context14.prev = 7;
                        _context14.next = 10;
                        return profileApi.saveUserBasicPersonalDetails({
                            usr_id: usr_id,
                            role: usr_role,
                            dsp_nm: dsp_nm
                        });

                    case 10:
                        _context14.next = 12;
                        return profileApi.saveShortDetails({
                            usr_id: usr_id,
                            pwd: '',
                            dsp_nm: dsp_nm,
                            usr_email: usr_email,
                            usr_ph: usr_ph
                        });

                    case 12:
                        _context14.next = 14;
                        return profileApi.addEmailInContactDetails({
                            usr_id: usr_id,
                            email: usr_email,
                            isPrimary: true
                        });

                    case 14:
                        _context14.next = 16;
                        return chatFriendsApi.addFriend({ user_id: usr_id, fr_id: 'support' });

                    case 16:
                        _context14.next = 18;
                        return chatFriendsApi.addUserShortDetails({
                            user_id: usr_id,
                            user_email: usr_email,
                            display_name: dsp_nm,
                            typ: 'usr',
                            pic: ''
                        });

                    case 18:
                        if (!usr_email) {
                            _context14.next = 21;
                            break;
                        }

                        _context14.next = 21;
                        return subscribeAPI.subscribeUser({
                            subs_email: usr_email,
                            subs_typ: 'from social media',
                            usr_id: usr_id
                        });

                    case 21:
                        return _context14.abrupt('return', {});

                    case 24:
                        _context14.prev = 24;
                        _context14.t0 = _context14['catch'](7);

                        debug(_context14.t0);
                        throw _context14.t0;

                    case 28:
                    case 'end':
                        return _context14.stop();
                }
            }
        }, _callee14, this, [[7, 24]]);
    }));

    return function (_x17) {
        return _ref14.apply(this, arguments);
    };
}();

Auth.prototype.generatePassportJWT = function () {
    var _ref15 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee15(data) {
        var payload, usercoursesRes, usercourses;
        return _regenerator2.default.wrap(function _callee15$(_context15) {
            while (1) {
                switch (_context15.prev = _context15.next) {
                    case 0:
                        if (!data.err) {
                            _context15.next = 2;
                            break;
                        }

                        return _context15.abrupt('return', data);

                    case 2:
                        if (!data.firstTime) {
                            data.firstTime = false;
                        }
                        delete data.user["pwd"];

                        payload = {
                            id: data.user.usr_id,
                            usr: data.user
                        };

                        data['token'] = authJWT.encode(payload, config.TOKEN_SECRET);

                        _context15.next = 8;
                        return userCoursesAPI.getPurchasedCourseOfUser({
                            usr_id: data.user.usr_id.trim().toLowerCase()
                        });

                    case 8:
                        usercoursesRes = _context15.sent;
                        usercourses = usercoursesRes.map(function (v, i) {
                            return v.crs_id;
                        });


                        data.user.usr_pic = data.user.usr_pic || "/assets/images/dummy-user-pic.png";

                        data['uc'] = usercourses;
                        debug(data);
                        return _context15.abrupt('return', data);

                    case 14:
                    case 'end':
                        return _context15.stop();
                }
            }
        }, _callee15, this);
    }));

    return function (_x18) {
        return _ref15.apply(this, arguments);
    };
}();

Auth.prototype.generateJWT = function () {
    var _ref16 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee16(data) {
        var payload, token, usercoursesRes, usercourses, tempUser, obj;
        return _regenerator2.default.wrap(function _callee16$(_context16) {
            while (1) {
                switch (_context16.prev = _context16.next) {
                    case 0:
                        _context16.prev = 0;

                        if (!data.err) {
                            _context16.next = 3;
                            break;
                        }

                        return _context16.abrupt('return', data);

                    case 3:
                        if (!data.firstTime) {
                            data.firstTime = false;
                        }

                        console.log("data....", data);

                        delete data.user["pwd"];
                        payload = {
                            userId: data.user.usr_id.trim().toLowerCase()
                        };
                        token = jwt.sign(payload, config.TOKEN_SECRET, {
                            expiresIn: config.TOKEN_EXPIRE
                        });
                        _context16.next = 10;
                        return userCoursesAPI.getPurchasedCourseOfUser({
                            usr_id: data.user.usr_id.trim().toLowerCase()
                        });

                    case 10:
                        usercoursesRes = _context16.sent;
                        usercourses = usercoursesRes.map(function (v, i) {
                            return v.crs_id;
                        });
                        _context16.next = 14;
                        return profileApi.getUserShortDetails({ usr_id: data.user.usr_id.trim().toLowerCase() });

                    case 14:
                        tempUser = _context16.sent;


                        data.user = (0, _extends3.default)({}, tempUser, {
                            is_act: data.user.is_act
                        });

                        data.user.usr_pic = data.user.usr_pic || "/assets/images/dummy-user-pic.png";

                        obj = {
                            token: token,
                            firstTime: data.firstTime,
                            user: data.user,
                            uc: usercourses
                        };
                        return _context16.abrupt('return', obj);

                    case 21:
                        _context16.prev = 21;
                        _context16.t0 = _context16['catch'](0);
                        throw new Error(_context16.t0);

                    case 24:
                    case 'end':
                        return _context16.stop();
                }
            }
        }, _callee16, this, [[0, 21]]);
    }));

    return function (_x19) {
        return _ref16.apply(this, arguments);
    };
}();
module.exports = new Auth();