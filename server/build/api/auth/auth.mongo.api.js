'use strict';

var configPath = require('../../configPath.js');
var config = require('../../config.js');
var jwt = require('jsonwebtoken');
var request = require('request');
var qs = require('querystring');
var Q = require('q');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);

function Auth() {}

Auth.prototype.validateLoginDetails = function (data) {
    return getConnection().then(dbCollection).then(findUser).then(validate).catch(globalFunctions.err);

    function findUser(col) {
        return col.find({
            userId: data.userId
        }, {
            _id: 0
        }).limit(2).toArray();
    }

    function validate(user) {
        if (user.length > 0 && user[0].password === data.password) {
            return {
                user: user[0]
            };
        } else if (user.length == 0) {
            return {
                err: userErrorsConfig.USER_NOT_EXIST
            };
        } else {
            return {
                err: userErrorsConfig.PASSWORD_MISMATCH
            };
        }
    }
};

Auth.prototype.checkUserExistance = function (data) {
    return getConnection().then(dbCollection).then(findUser).then(checkUser).catch(globalFunctions.err);

    function findUser(col) {
        return col.find({
            userId: data.userId
        }, {
            _id: 0
        }).limit(2).toArray();
    }

    function checkUser(user) {
        if (user.length > 0) {
            return {
                err: userErrorsConfig.USER_EXISTS
            };
        }
        return {
            err: null
        };
    }
};

Auth.prototype.registerUser = function (data) {
    return getConnection().then(dbCollection).then(insertUser).catch(globalFunctions.err);

    function insertUser(col) {
        return col.insertOne(data);
    }
};
//In case , 
//Mail not worked properly , but inserted user
Auth.prototype.removeUser = function (data) {
    return getConnection().then(dbCollection).then(removeUser).catch(globalFunctions.err);

    function removeUser(col) {
        return col.remove({
            userId: data.userId
        });
    }
};
Auth.prototype.checkPassword = function (data) {
    return getConnection().then(dbCollection).then(findUser).then(checkPassword).catch(globalFunctions.err);

    function findUser(col) {
        return col.find({
            userId: data.userId
        }, {
            _id: 0
        }).limit(2).toArray();
    }

    function checkPassword(user) {
        console.log(data, user);
        if (user.length > 0) {
            var user = user[0];
            if (user.password === data.password) {
                return {
                    err: null
                };
            } else {
                return {
                    err: userErrorsConfig.PASSWORD_MISMATCH
                };
            }
        }
        return {
            err: userErrorsConfig.PASSWORD_MISMATCH
        };
    }
};
Auth.prototype.changePassword = function (data) {
    return getConnection().then(dbCollection).then(updateUser).catch(globalFunctions.err);

    function updateUser(col) {
        return col.update({
            userId: data.userId
        }, {
            $set: {
                password: data.password
            }
        });
    }
};

Auth.prototype.facebook = function (data) {
    var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'];
    var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
    var graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.FACEBOOK_SECRET,
        redirect_uri: data.redirectUri
    };
    var defer = Q.defer();
    request.get({
        url: accessTokenUrl,
        qs: params,
        json: true
    }, function (err, response, accessToken) {
        if (response.statusCode !== 200) {
            var e = new Error(accessToken.error.message);
            defer.reject(e);
        }
        console.log("accessToken......", accessToken);
        request.get({
            url: graphApiUrl,
            qs: accessToken,
            json: true
        }, function (err, response, profile) {

            if (response.statusCode !== 200) {
                var e = new Error(profile.error.message);
                defer.reject(e);
            }

            console.log(profile);
            defer.resolve(profile);
        });
    });
    return defer.promise;
};

Auth.prototype.google = function (data) {
    var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.GOOGLE_SECRET,
        redirect_uri: data.redirectUri,
        grant_type: 'authorization_code'
    };
    var defer = Q.defer();
    request.post(accessTokenUrl, {
        json: true,
        form: params
    }, function (err, response, token) {
        if (response.statusCode !== 200) {
            var e = new Error("Some Error Occurred");
            defer.reject(e);
        }
        var accessToken = token.access_token;
        var headers = {
            Authorization: 'Bearer ' + accessToken
        };

        request.get({
            url: peopleApiUrl,
            headers: headers,
            json: true
        }, function (err, response, profile) {
            if (profile.error) {
                var e = new Error(profile.error.message);
                defer.reject(e);
            }
            defer.resolve(profile);
        });
    });
    return defer.promise;
};

Auth.prototype.twitter = function (data) {
    var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
    var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
    var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';
    var defer = Q.defer();
    if (!data.oauth_token || !data.oauth_verifier) {
        var requestTokenOauth = {
            consumer_key: config.TWITTER_KEY,
            consumer_secret: config.TWITTER_SECRET,
            callback: data.redirectUri
        };

        // Step 1. Obtain request token for the authorization popup.
        request.post({
            url: requestTokenUrl,
            oauth: requestTokenOauth
        }, function (err, response, body) {
            var oauthToken = qs.parse(body);

            // Step 2. Send OAuth token back to open the authorization screen.
            defer.resolve(oauthToken);
        });
    } else {
        // Part 2 of 2: Second request after Authorize app is clicked.
        var accessTokenOauth = {
            consumer_key: config.TWITTER_KEY,
            consumer_secret: config.TWITTER_SECRET,
            token: data.oauth_token,
            verifier: data.oauth_verifier
        };
        console.log("accessTokenOauth.........", accessTokenOauth);

        // Step 3. Exchange oauth token and oauth verifier for access token.
        request.post({
            url: accessTokenUrl,
            oauth: accessTokenOauth
        }, function (err, response, accessToken) {
            if (response.statusCode !== 200) {
                var e = new Error("Some Error Occured");
                defer.reject(e);
            }
            accessToken = qs.parse(accessToken);
            console.log("accessToken.........", accessToken);

            var profileOauth = {
                consumer_key: config.TWITTER_KEY,
                consumer_secret: config.TWITTER_SECRET,
                oauth_token: accessToken.oauth_token
            };

            // Step 4. Retrieve profile information about the current user.
            request.get({
                url: profileUrl + accessToken.screen_name,
                oauth: profileOauth,
                json: true
            }, function (err, response, profile) {
                if (response.statusCode !== 200) {
                    var e = new Error("Some Error Occured");
                    defer.reject(e);
                }
                defer.resolve(profile);
            });
        });
    }
    return defer.promise;
};

Auth.prototype.linkedin = function (data) {
    var accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken';
    var peopleApiUrl = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url)';
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.LINKEDIN_SECRET,
        redirect_uri: data.redirectUri,
        grant_type: 'authorization_code'
    };
    var defer = Q.defer();
    request.post(accessTokenUrl, { form: params, json: true }, function (err, response, body) {
        if (response.statusCode !== 200) {
            var e = new Error(body.error_description);
            defer.reject(e);
        }
        var params = {
            oauth2_access_token: body.access_token,
            format: 'json'
        };

        // Step 2. Retrieve profile information about the current user.
        request.get({ url: peopleApiUrl, qs: params, json: true }, function (err, response, profile) {
            if (response.statusCode !== 200) {
                var e = new Error("Some Error Occured");
                defer.reject(e);
            }
            defer.resolve(profile);
        });
    });
    return defer.promise;
};

Auth.prototype.generateJWT = function (data) {
    if (data.err) {
        return {
            err: data.err
        };
    }
    delete data.user["password"];
    var payload = {
        userId: data.user.userId
    };
    var token = jwt.sign(payload, config.TOKEN_SECRET, {
        expiresIn: config.TOKEN_EXPIRE
    });
    return {
        token: token,
        user: data.user
    };
};

function dbCollection(db) {
    return db.collection('userdetails');
}

module.exports = new Auth();