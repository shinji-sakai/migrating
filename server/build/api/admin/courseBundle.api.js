'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q = require('q');

function CourseBundle() {}

//Get all course bundles
CourseBundle.prototype.getAll = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		return col.find({}).toArray();
	}
};

//insert/Update course bundle
//if data will come with bundleId then update data otherwise insert it
CourseBundle.prototype.update = function (data) {
	var update = function () {
		var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
			var obj;
			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							_context.next = 2;
							return col.findOne({ bundleId: data.bundleId });

						case 2:
							obj = _context.sent;


							if (!obj) {
								data.create_dt = new Date();
								data.update_dt = new Date();
							} else {
								data.update_dt = new Date();
							}

							if (!data.bundleId) {
								data.bundleId = shortid.generate();
							}
							return _context.abrupt('return', col.update({ bundleId: data.bundleId }, { $set: data }, { upsert: true }));

						case 6:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, this);
		}));

		return function update(_x) {
			return _ref.apply(this, arguments);
		};
	}();

	return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);
};

//Remove bundle from db
CourseBundle.prototype.remove = function (data) {
	return getConnection().then(dbCollection).then(remove).catch(globalFunctions.err);

	function remove(col) {
		return col.remove({ bundleId: data.bundleId });
	}
};

function dbCollection(db) {
	return db.collection("coursebundle");
}

module.exports = new CourseBundle();