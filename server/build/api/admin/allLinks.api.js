'use strict';

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q = require('q');

function API() {}
API.prototype.getAll = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		if (data.link_id) {
			return col.find({ link_id: data.link_id }).toArray();
		} else {
			return col.find({}).toArray();
		}
	}
};

API.prototype.update = function (data) {
	return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);

	function update(col) {
		if (!data.link_id) {
			data.link_id = shortid.generate();
			data.create_dt = new Date();
			data.update_dt = new Date();
		} else {
			data.update_dt = new Date();
		}
		return col.update({ link_id: data.link_id }, { $set: data }, { upsert: true });
	}
};

API.prototype.remove = function (data) {
	return getConnection().then(dbCollection).then(remove).catch(globalFunctions.err);

	function remove(col) {
		return col.remove({ link_id: data.link_id });
	}
};

function dbCollection(db) {
	return db.collection("allLinks");
}

module.exports = new API();