'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q = require('q');

function Publication() {}
Publication.prototype.getAll = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		return col.find({}).toArray();
	}
};
Publication.prototype.fetchPublicationsUsingLimit = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var no_of_item_to_fetch, no_of_item_to_skip, connection, collection, find_res;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						no_of_item_to_fetch = data.no_of_item_to_fetch, no_of_item_to_skip = data.no_of_item_to_skip;

						if (typeof no_of_item_to_fetch === "undefined") {
							no_of_item_to_fetch = 5;
						}
						if (typeof no_of_item_to_skip === "undefined") {
							no_of_item_to_skip = 0;
						}
						console.log(no_of_item_to_fetch, no_of_item_to_skip);
						_context.next = 7;
						return getConnection();

					case 7:
						connection = _context.sent;
						_context.next = 10;
						return dbCollection(connection);

					case 10:
						collection = _context.sent;
						_context.next = 13;
						return collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();

					case 13:
						find_res = _context.sent;
						return _context.abrupt('return', find_res);

					case 17:
						_context.prev = 17;
						_context.t0 = _context['catch'](0);
						throw new Error(_context.t0);

					case 20:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 17]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();
Publication.prototype.update = function (data) {
	return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);

	function update(col) {
		if (!data.publicationId) {
			data.publicationId = shortid.generate();
		}
		return col.update({ publicationId: data.publicationId }, data, { upsert: true });
	}
};

Publication.prototype.remove = function (data) {
	return getConnection().then(dbCollection).then(remove).catch(globalFunctions.err);

	function remove(col) {
		return col.remove({ publicationId: data.publicationId });
	}
};

function dbCollection(db) {
	return db.collection("publication");
}

module.exports = new Publication();