'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q = require('q');

function API() {}

API.prototype.getAllFormsHelp = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		return col.find({}).toArray();
	}
};

API.prototype.getFormHelpById = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		if (data.form_id) {
			return col.findOne({ form_id: data.form_id });
		} else {
			return col.find({}).toArray();
		}
	}
};

API.prototype.updateFormHelp = function (data) {
	var update = function () {
		var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							_context.prev = 0;

							delete data["_id"];
							_context.next = 4;
							return col.update({ form_id: data.form_id }, data, { upsert: true });

						case 4:
							return _context.abrupt('return', data);

						case 7:
							_context.prev = 7;
							_context.t0 = _context['catch'](0);

							console.log(_context.t0);
							throw _context.t0;

						case 11:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, this, [[0, 7]]);
		}));

		return function update(_x) {
			return _ref.apply(this, arguments);
		};
	}();

	return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);
};

API.prototype.removeFormHelp = function (data) {
	var remove = function () {
		var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(col) {
			return _regenerator2.default.wrap(function _callee2$(_context2) {
				while (1) {
					switch (_context2.prev = _context2.next) {
						case 0:
							_context2.prev = 0;
							_context2.next = 3;
							return col.remove({ form_id: data.form_id });

						case 3:
							return _context2.abrupt('return', data);

						case 6:
							_context2.prev = 6;
							_context2.t0 = _context2['catch'](0);

							console.log(_context2.t0);
							throw _context2.t0;

						case 10:
						case 'end':
							return _context2.stop();
					}
				}
			}, _callee2, this, [[0, 6]]);
		}));

		return function remove(_x2) {
			return _ref2.apply(this, arguments);
		};
	}();

	return getConnection().then(dbCollection).then(remove).catch(globalFunctions.err);
};

function dbCollection(db) {
	return db.collection("helpForm");
}

module.exports = new API();