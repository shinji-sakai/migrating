'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');
var courseAPI = require(configPath.api.admin.course);

var Q = require('q');

function API() {}
API.prototype.getAll = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		if (data.page_id) {
			return col.find({ page_id: data.page_id }).toArray();
		} else {
			return col.find({}).toArray();
		}
	}
};

API.prototype.getById = function (data) {
	var getCourseDetails = function () {
		var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
			var res;
			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							if (!data.crs_id) {
								_context.next = 8;
								break;
							}

							_context.next = 3;
							return courseAPI.getCoursesById({ ids: data.crs_id });

						case 3:
							res = _context.sent;

							data["courseDetails"] = res[0];
							return _context.abrupt('return', data);

						case 8:
							return _context.abrupt('return', data);

						case 9:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, this);
		}));

		return function getCourseDetails(_x) {
			return _ref.apply(this, arguments);
		};
	}();

	return getConnection().then(dbCollection).then(find).then(getCourseDetails).catch(globalFunctions.err);

	function find(col) {
		if (data.page_id) {
			return col.findOne({ page_id: data.page_id });
		} else {
			return col.find({}).toArray();
		}
	}
};

API.prototype.getByIds = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		if (data.page_id) {
			return col.find({ page_id: { $in: [].concat(data.page_id) } }).toArray();
		} else {
			return [];
		}
	}
};

API.prototype.update = function (data) {
	return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);

	function update(col) {
		if (!data.page_id) {
			data.page_id = shortid.generate();
		}
		return col.update({ page_id: data.page_id }, data, { upsert: true });
	}
};

API.prototype.remove = function (data) {
	return getConnection().then(dbCollection).then(remove).catch(globalFunctions.err);

	function remove(col) {
		return col.remove({ page_id: data.page_id });
	}
};

function dbCollection(db) {
	return db.collection("sideLinksArticles");
}

module.exports = new API();