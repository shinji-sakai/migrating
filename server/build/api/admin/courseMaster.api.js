'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);

var courseModule = require(configPath.api.admin.courseModule);
var course = require(configPath.api.admin.course);
var courseApi = require(configPath.api.admin.course);
var moduleItems = require(configPath.api.admin.moduleItems);

var debug = require('debug')('app:api:admin:coursemaster');

var RELATED_COURSE_FETCH_LIMIT = 3;

var Q = require('q');

function CourseMaster() {}

/**
 * [getAll get all courses from coursemaster collection]
 * @return {[Promise]} [find Promise]
 */
CourseMaster.prototype.getAll = function () {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({}, { _id: 0 }).toArray();
    }
};

/**
 * [getAll get all courses from coursemaster collection]
 * @return {[Promise]} [find Promise]
 */
CourseMaster.prototype.getCourseByCourseId = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var find;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        find = function find(col) {
                            return col.findOne({ courseId: data.courseId }, { _id: 0 });
                        };

                        if (data.courseId) {
                            _context.next = 3;
                            break;
                        }

                        return _context.abrupt('return', {
                            error: "courseId field is required."
                        });

                    case 3:
                        return _context.abrupt('return', getConnection().then(dbCollection).then(find).catch(globalFunctions.err));

                    case 4:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

/**
 * [add add/update coursemaster , this will take effect in courselist,coursemodules and moduleitems]
 * @param {[type]} data [description]
 */
CourseMaster.prototype.add = function (data) {
    if (data.courseId) {
        data.courseId = data.courseId.toLowerCase();
    }
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData) // insert/update data in coursemaster
    .then(updateCourseList) // update in courselist
    .then(updateCourseModules) // update in course modules
    .then(updateModuleItems) // update moduleitems
    .catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        //insert/update in coursemaster
        return col.update({ courseId: data.courseId }, data, { upsert: true, w: 1 });
    }

    function updateCourseList() {
        //update in courselist
        var query = {
            $set: {
                courseName: data.courseName,
                courseType: data.courseType,
                courseSubGroup: data.courseSubGroup,
                author: data.author,
                isLiveClass: data.isLiveClass
            }
        };
        console.log(query);
        return course.updateUsingQuery({ courseId: data.courseId }, query);
    }

    function updateCourseModules() {
        //update in coursemodules
        var query = {
            $set: {
                courseName: data.courseName
            }
        };
        return courseModule.updateUsingQuery({ courseId: data.courseId }, query);
    }

    function updateModuleItems() {
        //insert/update in moduleItems
        var query = {
            $set: {
                courseName: data.courseName,
                courseType: data.courseType,
                courseSubGroup: data.courseSubGroup,
                author: data.author,
                isLiveClass: data.isLiveClass
            }
        };
        return moduleItems.updateUsingQuery({ courseId: data.courseId }, query);
    }
};
CourseMaster.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).then(removeDataFromCourseList).then(removeDataFromModules).then(removeDataFromModuleItems).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ courseId: data.courseId });
    }
    function removeDataFromCourseList(col) {
        //remove from courselist
        return course.delete({ courseId: data.courseId });
    }
    function removeDataFromModules(col) {
        //remove from coursemodules
        return courseModule.deleteUsingCourseId({ courseId: data.courseId });
    }
    function removeDataFromModuleItems(col) {
        //remove from moduleitems
        return moduleItems.deleteModuleItemsUsingCourseId({ courseId: data.courseId });
    }
};

// getRelatedCoursesByCourseId
CourseMaster.prototype.getRelatedCoursesByCourseId = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var startIndex, connection, collection, find_res, relatedCourses, fullCourses, i, crs, items, moduleI, allModules, firstModule, firstItem;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        startIndex = data.startIndex;

                        startIndex = startIndex || 0;
                        _context2.next = 5;
                        return getConnection();

                    case 5:
                        connection = _context2.sent;
                        _context2.next = 8;
                        return dbCollection(connection);

                    case 8:
                        collection = _context2.sent;
                        _context2.next = 11;
                        return collection.findOne({ courseId: data.courseId });

                    case 11:
                        find_res = _context2.sent;
                        relatedCourses = find_res["relatedCourses"] || [];

                        if (!(relatedCourses.length > 0)) {
                            _context2.next = 38;
                            break;
                        }

                        relatedCourses = relatedCourses.splice(startIndex, RELATED_COURSE_FETCH_LIMIT);
                        _context2.next = 17;
                        return courseApi.getCoursesById({ ids: relatedCourses });

                    case 17:
                        fullCourses = _context2.sent;
                        i = 0;

                    case 19:
                        if (!(i < fullCourses.length)) {
                            _context2.next = 35;
                            break;
                        }

                        crs = fullCourses[i];
                        //get all modules data for course

                        _context2.next = 23;
                        return moduleItems.getCourseItems({ courseId: crs.courseId });

                    case 23:
                        items = _context2.sent;
                        moduleI = items[0] || {};
                        allModules = moduleI["moduleDetail"] || [];
                        //sort all module based on weight

                        allModules.sort(function (a, b) {
                            return a.moduleWeight - b.moduleWeight;
                        });
                        //get first module
                        firstModule = allModules[0] || {};
                        // get first item of the first module

                        firstItem = firstModule.moduleItems ? firstModule.moduleItems[0] : {};
                        //attach it to course details

                        crs["firstModuleId"] = firstModule.moduleId;
                        crs["firstItemId"] = firstItem.itemId;
                        crs["firstItemType"] = firstItem.itemType;

                    case 32:
                        i++;
                        _context2.next = 19;
                        break;

                    case 35:
                        return _context2.abrupt('return', fullCourses || []);

                    case 38:
                        return _context2.abrupt('return', []);

                    case 39:
                        _context2.next = 44;
                        break;

                    case 41:
                        _context2.prev = 41;
                        _context2.t0 = _context2['catch'](0);
                        throw _context2.t0;

                    case 44:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 41]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

CourseMaster.prototype.fetchCourseMasterUsingLimit = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var no_of_item_to_fetch, no_of_item_to_skip, connection, collection, find_res;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        no_of_item_to_fetch = data.no_of_item_to_fetch, no_of_item_to_skip = data.no_of_item_to_skip;

                        if (typeof no_of_item_to_fetch === "undefined") {
                            no_of_item_to_fetch = 5;
                        }
                        if (typeof no_of_item_to_skip === "undefined") {
                            no_of_item_to_skip = 0;
                        }
                        console.log(no_of_item_to_fetch, no_of_item_to_skip);
                        _context3.next = 7;
                        return getConnection();

                    case 7:
                        connection = _context3.sent;
                        _context3.next = 10;
                        return dbCollection(connection);

                    case 10:
                        collection = _context3.sent;
                        _context3.next = 13;
                        return collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();

                    case 13:
                        find_res = _context3.sent;
                        return _context3.abrupt('return', find_res);

                    case 17:
                        _context3.prev = 17;
                        _context3.t0 = _context3['catch'](0);
                        throw new Error(_context3.t0);

                    case 20:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 17]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

CourseMaster.prototype.isIdExist = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var key, _collection, id_value, connection, obj, find_res;

        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        key = data.key, _collection = data.collection, id_value = data.id_value;

                        if (!(id_value && key)) {
                            _context4.next = 21;
                            break;
                        }

                        _context4.next = 5;
                        return getConnection();

                    case 5:
                        connection = _context4.sent;
                        _context4.next = 8;
                        return connection.collection(_collection);

                    case 8:
                        _collection = _context4.sent;
                        obj = {};

                        obj[key] = id_value.toLowerCase();
                        _context4.next = 13;
                        return _collection.find(obj).toArray();

                    case 13:
                        find_res = _context4.sent;

                        if (!(find_res && find_res[0])) {
                            _context4.next = 18;
                            break;
                        }

                        return _context4.abrupt('return', {
                            status: "success",
                            idExist: true,
                            data: find_res[0]
                        });

                    case 18:
                        return _context4.abrupt('return', {
                            status: "success",
                            idExist: false
                        });

                    case 19:
                        _context4.next = 22;
                        break;

                    case 21:
                        return _context4.abrupt('return', {
                            status: "error",
                            message: "id_value field is required"
                        });

                    case 22:
                        _context4.next = 27;
                        break;

                    case 24:
                        _context4.prev = 24;
                        _context4.t0 = _context4['catch'](0);
                        throw new Error(_context4.t0);

                    case 27:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 24]]);
    }));

    return function (_x4) {
        return _ref4.apply(this, arguments);
    };
}();

function dbCollection(db) {
    return db.collection("coursemaster");
}

module.exports = new CourseMaster();