'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseMasterAPI = require(configPath.api.admin.courseMaster);
var shortid = require('shortid');

var Q = require('q');

function API() {}
API.prototype.get = function (data) {
	var find = function () {
		var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
			var crses, i, course, obj;
			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							if (!data.course_id) {
								_context.next = 4;
								break;
							}

							return _context.abrupt('return', col.find({ course_id: data.course_id }).toArray());

						case 4:
							_context.next = 6;
							return col.find({}).toArray();

						case 6:
							crses = _context.sent;
							i = 0;

						case 8:
							if (!(i < crses.length)) {
								_context.next = 17;
								break;
							}

							_context.next = 11;
							return courseMasterAPI.getCourseByCourseId({ courseId: crses[i].course_id });

						case 11:
							course = _context.sent;
							obj = (0, _extends3.default)({}, crses[i], course);

							crses[i] = obj;

						case 14:
							i++;
							_context.next = 8;
							break;

						case 17:
							return _context.abrupt('return', crses);

						case 18:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, this);
		}));

		return function find(_x) {
			return _ref.apply(this, arguments);
		};
	}();

	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);
};

API.prototype.getCoursesByCourseType = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var conn, col, res, i, course, obj;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						if (data.courseType) {
							_context2.next = 2;
							break;
						}

						return _context2.abrupt('return', {
							error: "courseType is required"
						});

					case 2:
						_context2.prev = 2;
						_context2.next = 5;
						return getConnection();

					case 5:
						conn = _context2.sent;
						_context2.next = 8;
						return dbCollection(conn);

					case 8:
						col = _context2.sent;
						_context2.next = 11;
						return col.find({ courseType: data.courseType }).toArray();

					case 11:
						res = _context2.sent;
						i = 0;

					case 13:
						if (!(i < res.length)) {
							_context2.next = 22;
							break;
						}

						_context2.next = 16;
						return courseMasterAPI.getCourseByCourseId({ courseId: res[i].course_id });

					case 16:
						course = _context2.sent;
						obj = (0, _extends3.default)({}, res[i], course);

						res[i] = obj;

					case 19:
						i++;
						_context2.next = 13;
						break;

					case 22:
						return _context2.abrupt('return', res);

					case 25:
						_context2.prev = 25;
						_context2.t0 = _context2['catch'](2);

						console.log("getCoursesByCourseType err....", _context2.t0);
						throw new Error(_context2.t0);

					case 29:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[2, 25]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

API.prototype.update = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var update;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						update = function update(col) {
							return col.update({ course_id: data.course_id }, data, { upsert: true });
						};

						if (data.course_id) {
							_context3.next = 3;
							break;
						}

						return _context3.abrupt('return', {
							error: "course_id is required"
						});

					case 3:
						return _context3.abrupt('return', getConnection().then(dbCollection).then(update).catch(globalFunctions.err));

					case 4:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

API.prototype.remove = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var remove;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						remove = function remove(col) {
							return col.remove({ course_id: data.course_id });
						};

						if (data.course_id) {
							_context4.next = 3;
							break;
						}

						return _context4.abrupt('return', {
							error: "course_id is required"
						});

					case 3:
						return _context4.abrupt('return', getConnection().then(dbCollection).then(remove).catch(globalFunctions.err));

					case 4:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

function dbCollection(db) {
	return db.collection("boardCompetitiveCourses");
}

module.exports = new API();