'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var Q = require('q');
var moduleItems = require(configPath.api.admin.moduleItems);

function Course() {}

//get all courses
Course.prototype.getAll = function (data) {
    return getConnection() // get database connection
    .then(dbCollection) //get db collection
    .then(function (col) {

        if (data.courseType) {
            //if coursetype is set then fetch only matching records
            return col.find({ courseType: data.courseType }).toArray();
        } else {
            //else find all courses
            return col.find({}).toArray();
        }
    }).catch(globalFunctions.err); // error handling
};

//get all courses
Course.prototype.isCourseExist = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var conn, col, rows;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        _context.next = 3;
                        return getConnection();

                    case 3:
                        conn = _context.sent;
                        _context.next = 6;
                        return dbCollection(conn);

                    case 6:
                        col = _context.sent;
                        _context.next = 9;
                        return col.find({ courseId: data.courseId }, { _id: 0 }).toArray();

                    case 9:
                        rows = _context.sent;

                        if (!(rows.length > 0)) {
                            _context.next = 14;
                            break;
                        }

                        return _context.abrupt('return', { exist: true });

                    case 14:
                        return _context.abrupt('return', { exist: false });

                    case 15:
                        _context.next = 20;
                        break;

                    case 17:
                        _context.prev = 17;
                        _context.t0 = _context['catch'](0);
                        throw _context.t0;

                    case 20:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 17]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

//get courses by given ids
Course.prototype.getCoursesById = function (data) {
    var ids = [].concat(data.ids);
    return getConnection() // get database connection
    .then(dbCollection) //get db collection
    .then(function (col) {
        return col.find({ courseId: { $in: ids } }).toArray();
    }).catch(globalFunctions.err); // error handling
};

/**
 * [add insert/update course will take affect in courselist collection and moduleitems collection]
 * @param {[JSON]} data [insert/update json]
 */
Course.prototype.add = function (data) {
    return getConnection() // get database connection
    .then(dbCollection) // get db collection
    .then(updateCourseList) // update courselist
    .then(updateModuleItems) // update moduleitems
    .catch(globalFunctions.err); // error handling

    function updateCourseList(col) {
        //update in courselist
        return col.updateOne({ courseId: data.courseId }, data, { upsert: true, w: 1 });
    }

    function updateModuleItems() {
        //insert/update in moduleItems
        var query = {
            $set: {
                courseLongDesc: data.courseLongDesc,
                courseShortDesc: data.courseShortDesc,
                courseLevel: data.courseLevel,
                courseLevelDesc: data.courseLevelDesc,
                imageUrl: data.imageUrl
            }
        };
        return moduleItems.updateUsingQuery({ courseId: data.courseId }, query);
    }
};

/**
 * [delete Delete course from db]
 * @param  {JSON} data {}
 * @return {Promise} [delete promise from mongo api]
 */
Course.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //delete query and return promise
        return col.deleteOne({ courseId: data.courseId });
    }
};

/**
 * [updateUsingQuery update course]
 * @param  {[JSON]} data  {}
 * @param  {[JSON]} query ex : {
            $set : {
                courseName : '',
                courseType : ',
                author : ''
            }
        }
 * @return {[Promise]}       [Update Query Promise]
 */
Course.prototype.updateUsingQuery = function (data, query) {
    return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);
    function update(col) {
        //update query and return promise
        return col.update(data, query, { upsert: true });
    }
};

// this function will be used to get courses given by author
Course.prototype.getCoursesByAuthor = function (data, query) {
    return getConnection().then(dbCollection).then(get).catch(globalFunctions.err);

    function get(col) {
        //update query and return promise
        var qu = {
            author: {
                $elemMatch: {
                    $eq: data.authorId
                }
            }
        };
        return col.find(qu).toArray();
    }
};

function dbCollection(db) {
    return db.collection("courselist");
}

module.exports = new Course();