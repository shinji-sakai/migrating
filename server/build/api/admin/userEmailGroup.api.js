'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batTimingApi = require(configPath.api.batchCourse.batchTiming);
var profileAPI = require(configPath.api.dashboard.profile);

var ew_training_bat_users_template_table = 'ew1.ew_training_bat_users_template';

function UserEmailGroup() {}

UserEmailGroup.prototype.getAllUserEmailGroups = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var query, res, allBatIds, allBatches, allBatchesMap;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						query = "select * from " + ew_training_bat_users_template_table;
						_context.next = 4;
						return cassExecute(query);

					case 4:
						res = _context.sent;

						//get all batch ids from all templates
						allBatIds = res.map(function (v, i) {
							return v.bat_id;
						});
						//get batch details for all batch id

						_context.next = 8;
						return batTimingApi.getTimingByBatId({ bat_id: allBatIds });

					case 8:
						allBatches = _context.sent;
						allBatchesMap = {};
						//create map of all batches {bat_id -> batch_detail}

						allBatches.map(function (v, i) {
							allBatchesMap[v.bat_id] = v;
						});

						//attach batch details to correspondance template
						res = res.map(function (v, i) {
							v["batch_details"] = allBatchesMap[v.bat_id];
							return v;
						});

						// res = [{frm_nm , bat_id , training_id , usrs , batch_details}]
						return _context.abrupt('return', res);

					case 15:
						_context.prev = 15;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);
						throw _context.t0;

					case 19:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 15]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

UserEmailGroup.prototype.getUserEmailGroupById = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var pk_id, query, params, res, users, users_data, users_email;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						pk_id = data.pk_id;

						if (pk_id) {
							_context2.next = 4;
							break;
						}

						return _context2.abrupt('return', {
							err: 'pk_id is required'
						});

					case 4:
						query = "select * from " + ew_training_bat_users_template_table + " where pk_id = ? ";
						params = [pk_id];
						_context2.next = 8;
						return cassExecute(query, params);

					case 8:
						res = _context2.sent;

						res = res[0];

						if (!res) {
							_context2.next = 20;
							break;
						}

						users = res.usrs;
						_context2.next = 14;
						return profileAPI.getUsersShortDetails({ usr_ids: users });

					case 14:
						users_data = _context2.sent;
						users_email = users_data.map(function (v, i) {
							return v.usr_email;
						});

						res["users_email"] = users_email;
						return _context2.abrupt('return', res);

					case 20:
						return _context2.abrupt('return', {});

					case 21:
						_context2.next = 27;
						break;

					case 23:
						_context2.prev = 23;
						_context2.t0 = _context2['catch'](0);

						logger.debug(_context2.t0);
						throw _context2.t0;

					case 27:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 23]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

UserEmailGroup.prototype.addUserEmailGroup = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var pk_id, frm_nm, bat_id, training_id, usrs, crt_dt, query, params, res, batchData;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						pk_id = data.pk_id || Uuid.random();
						frm_nm = data.frm_nm;
						bat_id = data.bat_id;
						training_id = data.training_id;
						usrs = data.usrs;
						crt_dt = new Date();
						query = "update " + ew_training_bat_users_template_table + " set frm_nm=? , bat_id=? , training_id=? , usrs=? , crt_dt=? where pk_id = ?";
						// var query = "insert into " + ew_training_bat_users_template_table + " (pk_id,frm_nm , bat_id , training_id , usrs , crt_dt) values (?,?,?,?,?,?) " ;

						params = [frm_nm, bat_id, training_id, usrs, crt_dt, pk_id];
						//execute query

						_context3.next = 11;
						return cassExecute(query, params);

					case 11:
						res = _context3.sent;
						_context3.next = 14;
						return batTimingApi.getTimingByBatId({ bat_id: bat_id });

					case 14:
						batchData = _context3.sent;
						return _context3.abrupt('return', (0, _extends3.default)({ pk_id: pk_id }, data, { batch_details: batchData[0] }));

					case 18:
						_context3.prev = 18;
						_context3.t0 = _context3['catch'](0);

						logger.debug(_context3.t0);
						throw _context3.t0;

					case 22:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 18]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

UserEmailGroup.prototype.deleteUserEmailGroup = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var pk_id, query, params, res;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						pk_id = data.pk_id;

						if (pk_id) {
							_context4.next = 4;
							break;
						}

						return _context4.abrupt('return', {
							err: 'pk_id is required'
						});

					case 4:
						query = "delete from " + ew_training_bat_users_template_table + " where pk_id = ?";
						params = [pk_id];
						_context4.next = 8;
						return cassExecute(query, params);

					case 8:
						res = _context4.sent;
						return _context4.abrupt('return', { pk_id: pk_id });

					case 12:
						_context4.prev = 12;
						_context4.t0 = _context4['catch'](0);

						logger.debug(_context4.t0);
						throw _context4.t0;

					case 16:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 12]]);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

module.exports = new UserEmailGroup();