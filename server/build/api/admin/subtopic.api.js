'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q = require('q');

function SubTopic() {}
SubTopic.prototype.getAll = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		return col.find({ topicId: data.topicId }).toArray();
	}
};
SubTopic.prototype.update = function (data) {
	var update = function () {
		var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
			var obj;
			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							_context.next = 2;
							return col.findOne({ subTopicId: data.subTopicId });

						case 2:
							obj = _context.sent;


							if (!obj) {
								data.create_dt = new Date();
								data.update_dt = new Date();
							} else {
								data.update_dt = new Date();
							}

							if (!data.subTopicId) {
								data.subTopicId = shortid.generate();
							}
							return _context.abrupt('return', col.update({ subTopicId: data.subTopicId }, { $set: data }, { upsert: true }));

						case 6:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, this);
		}));

		return function update(_x) {
			return _ref.apply(this, arguments);
		};
	}();

	return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);
};

SubTopic.prototype.remove = function (data) {
	return getConnection().then(dbCollection).then(remove).catch(globalFunctions.err);

	function remove(col) {
		return col.remove({ subTopicId: data.subTopicId });
	}
};

function dbCollection(db) {
	return db.collection("subtopic");
}

module.exports = new SubTopic();