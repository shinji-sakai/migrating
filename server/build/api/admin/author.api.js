'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q = require('q');

function Author() {}

//Get All Authors 
Author.prototype.getAll = function (data) {

	return getConnection() // get mongodb connection
	.then(dbCollection) // get db collection
	.then(find) // find all author
	.catch(globalFunctions.err); // handle error

	function find(col) {
		//find all author from mongodb
		return col.find({}).toArray();
	}
};

//Update Author
Author.prototype.update = function (data) {
	// handle error

	//Update author
	var update = function () {
		var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
			var obj;
			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							_context.next = 2;
							return col.findOne({ authorId: data.authorId });

						case 2:
							obj = _context.sent;


							if (!obj) {
								data.create_dt = new Date();
								data.update_dt = new Date();
							} else {
								data.update_dt = new Date();
							}

							//if author id is not set 
							//then create new authorid
							if (!data.authorId) {
								data.authorId = shortid.generate();
							}
							//update/insert author
							return _context.abrupt('return', col.update({ authorId: data.authorId }, { $set: data }, { upsert: true }));

						case 6:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, this);
		}));

		return function update(_x) {
			return _ref.apply(this, arguments);
		};
	}();

	return getConnection() // get mongodb connection
	.then(dbCollection) // get db collection
	.then(update) // update/insert author
	.catch(globalFunctions.err);
};

//remove author by id
Author.prototype.remove = function (data) {
	return getConnection() // get mongodb connection
	.then(dbCollection) // get db collection
	.then(remove) // remove author
	.catch(globalFunctions.err); // handle error

	function remove(col) {

		return col.remove({ authorId: data.authorId });
	}
};
Author.prototype.fetchAuthorUsingLimit = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var no_of_item_to_fetch, no_of_item_to_skip, connection, collection, find_res;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						no_of_item_to_fetch = data.no_of_item_to_fetch, no_of_item_to_skip = data.no_of_item_to_skip;

						if (typeof no_of_item_to_fetch === "undefined") {
							no_of_item_to_fetch = 5;
						}
						if (typeof no_of_item_to_skip === "undefined") {
							no_of_item_to_skip = 0;
						}
						console.log(no_of_item_to_fetch, no_of_item_to_skip);
						_context2.next = 7;
						return getConnection();

					case 7:
						connection = _context2.sent;
						_context2.next = 10;
						return dbCollection(connection);

					case 10:
						collection = _context2.sent;
						_context2.next = 13;
						return collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();

					case 13:
						find_res = _context2.sent;
						return _context2.abrupt('return', find_res);

					case 17:
						_context2.prev = 17;
						_context2.t0 = _context2['catch'](0);
						throw new Error(_context2.t0);

					case 20:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 17]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();
Author.prototype.AuthorsIdIsExits = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var Id, connection, collection, find_res;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						Id = data.Id;
						_context3.next = 4;
						return getConnection();

					case 4:
						connection = _context3.sent;
						_context3.next = 7;
						return dbCollection(connection);

					case 7:
						collection = _context3.sent;
						_context3.next = 10;
						return collection.find({ authorId: Id }).toArray();

					case 10:
						find_res = _context3.sent;
						return _context3.abrupt('return', find_res);

					case 14:
						_context3.prev = 14;
						_context3.t0 = _context3['catch'](0);
						throw new Error(_context3.t0);

					case 17:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 14]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();
function dbCollection(db) {
	return db.collection("tag");
}

function dbCollection(db) {
	return db.collection("author");

	// author collection : {authorId , authorName}
}

module.exports = new Author();