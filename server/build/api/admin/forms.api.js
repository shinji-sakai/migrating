'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_forms_m_table = 'ew1.ew_forms_m';

function Forms() {}

Forms.prototype.getAllForms = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var query, res;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						query = "select * from " + ew_forms_m_table;
						_context.next = 4;
						return cassExecute(query);

					case 4:
						res = _context.sent;
						return _context.abrupt('return', res);

					case 8:
						_context.prev = 8;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);
						throw _context.t0;

					case 12:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 8]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

Forms.prototype.addForm = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var form_id, form_name, create_dt, query, params, res;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						form_id = data.form_id;
						form_name = data.form_name;
						create_dt = new Date();
						query = "update " + ew_forms_m_table + " set form_name=? , create_dt=? where form_id = ?";
						params = [form_name, create_dt, form_id];
						//execute query

						_context2.next = 8;
						return cassExecute(query, params);

					case 8:
						res = _context2.sent;
						return _context2.abrupt('return', (0, _extends3.default)({}, data));

					case 12:
						_context2.prev = 12;
						_context2.t0 = _context2['catch'](0);

						logger.debug(_context2.t0);
						throw _context2.t0;

					case 16:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 12]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

Forms.prototype.deleteForm = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var form_id, query, params, res;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						form_id = data.form_id;
						query = "delete from " + ew_forms_m_table + " where form_id = ?";
						params = [form_id];
						_context3.next = 6;
						return cassExecute(query, params);

					case 6:
						res = _context3.sent;
						return _context3.abrupt('return', { form_id: form_id });

					case 10:
						_context3.prev = 10;
						_context3.t0 = _context3['catch'](0);

						logger.debug(_context3.t0);
						throw _context3.t0;

					case 14:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 10]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

module.exports = new Forms();