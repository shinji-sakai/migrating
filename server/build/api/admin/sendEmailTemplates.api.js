'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batTimingApi = require(configPath.api.batchCourse.batchTiming);
var emailTemplatesApi = require(configPath.api.admin.emailTemplates);
var userEmailGroupApi = require(configPath.api.admin.userEmailGroup);
var sendNotificationApi = require(configPath.api.admin.sendNotification);
var mailAPI = require(configPath.api.mail);

var ew_training_bat_users_send_email_table = 'ew1.ew_training_bat_users_send_email';

function SendEmailTemplate() {}

SendEmailTemplate.prototype.getAllSendEmailTemplates = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var query, res, allBatIds, allBatches, allBatchesMap;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						query = "select * from " + ew_training_bat_users_send_email_table;
						_context.next = 4;
						return cassExecute(query);

					case 4:
						res = _context.sent;

						//get all batch ids from all templates
						allBatIds = res.map(function (v, i) {
							return v.bat_id;
						});
						//get batch details for all batch id

						_context.next = 8;
						return batTimingApi.getTimingByBatId({ bat_id: allBatIds });

					case 8:
						allBatches = _context.sent;
						allBatchesMap = {};
						//create map of all batches {bat_id -> batch_detail}

						allBatches.map(function (v, i) {
							allBatchesMap[v.bat_id] = v;
						});

						//attach batch details to correspondance template
						res = res.map(function (v, i) {
							v["batch_details"] = allBatchesMap[v.bat_id];
							return v;
						});

						return _context.abrupt('return', res);

					case 15:
						_context.prev = 15;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);
						throw _context.t0;

					case 19:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 15]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

SendEmailTemplate.prototype.getSendEmailTemplateById = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var pk_id, query, params, res;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						pk_id = data.pk_id;
						query = "select * from " + ew_training_bat_users_send_email_table + " where pk_id = ?";
						params = [pk_id];
						_context2.next = 6;
						return cassExecute(query, params);

					case 6:
						res = _context2.sent;
						return _context2.abrupt('return', res[0]);

					case 10:
						_context2.prev = 10;
						_context2.t0 = _context2['catch'](0);

						logger.debug(_context2.t0);
						throw _context2.t0;

					case 14:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 10]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

SendEmailTemplate.prototype.addSendEmailTemplate = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var pk_id, frm_nm, bat_id, training_id, users_template, email_template, crt_dt, query, params, res, batchData;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						pk_id = data.pk_id || Uuid.random();
						frm_nm = data.frm_nm;
						bat_id = data.bat_id;
						training_id = data.training_id;
						users_template = data.users_template;
						email_template = data.email_template;
						crt_dt = new Date();
						query = "update " + ew_training_bat_users_send_email_table + " set frm_nm=? , users_template=? , email_template=? , bat_id=? , training_id=? , crt_dt=? where pk_id=? ";
						// var query = "insert into " + ew_training_bat_users_send_email_table + " (pk_id,frm_nm , users_template , email_template,bat_id,training_id , crt_dt) values (?,?,?,?,?,?,?) " ;

						params = [frm_nm, users_template, email_template, bat_id, training_id, crt_dt, pk_id];
						//execute query

						_context3.next = 12;
						return cassExecute(query, params);

					case 12:
						res = _context3.sent;
						_context3.next = 15;
						return batTimingApi.getTimingByBatId({ bat_id: bat_id });

					case 15:
						batchData = _context3.sent;
						return _context3.abrupt('return', (0, _extends3.default)({ pk_id: pk_id }, data, { batch_details: batchData[0] }));

					case 19:
						_context3.prev = 19;
						_context3.t0 = _context3['catch'](0);

						logger.debug(_context3.t0);
						throw _context3.t0;

					case 23:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 19]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

SendEmailTemplate.prototype.sendEmailTemplatesToUsers = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var pk_id, frm_nm, bat_id, training_id, users_template, email_template, emailTemplate, userEmailGroup, email_body, email_subject, users_emails, email_str;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						pk_id = data.pk_id || Uuid.random();
						frm_nm = data.frm_nm;
						bat_id = data.bat_id;
						training_id = data.training_id;
						users_template = data.users_template;
						email_template = data.email_template;
						_context4.next = 9;
						return emailTemplatesApi.getEmailTemplateById({ pk_id: email_template });

					case 9:
						emailTemplate = _context4.sent;
						_context4.next = 12;
						return userEmailGroupApi.getUserEmailGroupById({ pk_id: users_template });

					case 12:
						userEmailGroup = _context4.sent;

						if (!(emailTemplate && userEmailGroup)) {
							_context4.next = 23;
							break;
						}

						email_body = emailTemplate.email_body;
						email_subject = emailTemplate.email_sub;
						users_emails = userEmailGroup.users_email || [];
						email_str = users_emails.join(",");
						_context4.next = 20;
						return mailAPI.sendMail({
							to: email_str,
							subject: email_subject,
							text: email_body
						});

					case 20:
						return _context4.abrupt('return', data);

					case 23:
						return _context4.abrupt('return', { err: 'Something wrong' });

					case 24:
						_context4.next = 30;
						break;

					case 26:
						_context4.prev = 26;
						_context4.t0 = _context4['catch'](0);

						logger.debug(_context4.t0);
						throw _context4.t0;

					case 30:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 26]]);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

SendEmailTemplate.prototype.sendNotificationOfEmail = function () {
	var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
		var pk_id, frm_nm, users_template, email_template, emailTemplate, userEmailGroup, email_body, email_subject, users_emails;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.prev = 0;
						pk_id = data.pk_id || Uuid.random();
						frm_nm = data.frm_nm;
						users_template = data.users_template;
						email_template = data.email_template;
						_context5.next = 7;
						return emailTemplatesApi.getEmailTemplateById({ pk_id: email_template });

					case 7:
						emailTemplate = _context5.sent;
						_context5.next = 10;
						return userEmailGroupApi.getUserEmailGroupById({ pk_id: users_template });

					case 10:
						userEmailGroup = _context5.sent;

						if (!(emailTemplate && userEmailGroup)) {
							_context5.next = 20;
							break;
						}

						email_body = emailTemplate.email_body;
						email_subject = emailTemplate.email_sub;
						users_emails = userEmailGroup.users_email || [];
						_context5.next = 17;
						return sendNotificationApi.addSendNotification({
							sub: email_subject,
							noti_msg: email_body,
							users: userEmailGroup.usrs
						});

					case 17:
						return _context5.abrupt('return', data);

					case 20:
						return _context5.abrupt('return', { err: 'Something wrong' });

					case 21:
						_context5.next = 27;
						break;

					case 23:
						_context5.prev = 23;
						_context5.t0 = _context5['catch'](0);

						logger.debug(_context5.t0);
						throw _context5.t0;

					case 27:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, this, [[0, 23]]);
	}));

	return function (_x5) {
		return _ref5.apply(this, arguments);
	};
}();

SendEmailTemplate.prototype.deleteSendEmailTemplate = function () {
	var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
		var pk_id, query, params, res;
		return _regenerator2.default.wrap(function _callee6$(_context6) {
			while (1) {
				switch (_context6.prev = _context6.next) {
					case 0:
						_context6.prev = 0;
						pk_id = data.pk_id;

						if (pk_id) {
							_context6.next = 4;
							break;
						}

						return _context6.abrupt('return', {
							err: 'pk_id is required'
						});

					case 4:
						query = "delete from " + ew_training_bat_users_send_email_table + " where pk_id = ?";
						params = [pk_id];
						_context6.next = 8;
						return cassExecute(query, params);

					case 8:
						res = _context6.sent;
						return _context6.abrupt('return', { pk_id: pk_id });

					case 12:
						_context6.prev = 12;
						_context6.t0 = _context6['catch'](0);

						logger.debug(_context6.t0);
						throw _context6.t0;

					case 16:
					case 'end':
						return _context6.stop();
				}
			}
		}, _callee6, this, [[0, 12]]);
	}));

	return function (_x6) {
		return _ref6.apply(this, arguments);
	};
}();

module.exports = new SendEmailTemplate();