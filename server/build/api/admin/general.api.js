"use strict";

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);

var GeneralAPI = function () {
    function GeneralAPI() {
        (0, _classCallCheck3.default)(this, GeneralAPI);
    }

    (0, _createClass3.default)(GeneralAPI, [{
        key: "isIdExist",
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var key, _collection, id_value, connection, obj, find_res;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                key = data.key, _collection = data.collection, id_value = data.id_value;

                                if (!(id_value && key)) {
                                    _context.next = 21;
                                    break;
                                }

                                _context.next = 5;
                                return getConnection();

                            case 5:
                                connection = _context.sent;
                                _context.next = 8;
                                return connection.collection(_collection);

                            case 8:
                                _collection = _context.sent;
                                obj = {};

                                obj[key] = id_value.toLowerCase();
                                _context.next = 13;
                                return _collection.find(obj).toArray();

                            case 13:
                                find_res = _context.sent;

                                if (!(find_res && find_res[0])) {
                                    _context.next = 18;
                                    break;
                                }

                                return _context.abrupt("return", {
                                    status: "success",
                                    idExist: true,
                                    data: find_res[0]
                                });

                            case 18:
                                return _context.abrupt("return", {
                                    status: "success",
                                    idExist: false
                                });

                            case 19:
                                _context.next = 22;
                                break;

                            case 21:
                                return _context.abrupt("return", {
                                    status: "error",
                                    message: "id_value field is required"
                                });

                            case 22:
                                _context.next = 27;
                                break;

                            case 24:
                                _context.prev = 24;
                                _context.t0 = _context["catch"](0);
                                throw new Error(_context.t0);

                            case 27:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 24]]);
            }));

            function isIdExist(_x) {
                return _ref.apply(this, arguments);
            }

            return isIdExist;
        }()
    }, {
        key: "fetchItemUsingLimit",
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                var no_of_item_to_fetch, no_of_item_to_skip, _collection2, query, update_dt, connection, find_res;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                no_of_item_to_fetch = data.no_of_item_to_fetch, no_of_item_to_skip = data.no_of_item_to_skip, _collection2 = data.collection, query = data.query, update_dt = data.update_dt;

                                if (_collection2) {
                                    _context2.next = 4;
                                    break;
                                }

                                return _context2.abrupt("return", {
                                    status: "error",
                                    message: "collection field is required"
                                });

                            case 4:
                                if (typeof no_of_item_to_fetch === "undefined") {
                                    no_of_item_to_fetch = 5;
                                }
                                if (typeof no_of_item_to_skip === "undefined") {
                                    no_of_item_to_skip = 0;
                                }
                                query = query || {};

                                if (update_dt) {
                                    // add update date to compare
                                    query["update_dt"] = {
                                        "$lt": new Date(update_dt)
                                    };
                                }

                                _context2.next = 10;
                                return getConnection();

                            case 10:
                                connection = _context2.sent;
                                _context2.next = 13;
                                return connection.collection(_collection2);

                            case 13:
                                _collection2 = _context2.sent;
                                _context2.next = 16;
                                return _collection2.find(query || {}).sort({ update_dt: -1 }).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();

                            case 16:
                                find_res = _context2.sent;
                                return _context2.abrupt("return", find_res);

                            case 20:
                                _context2.prev = 20;
                                _context2.t0 = _context2["catch"](0);
                                throw new Error(_context2.t0);

                            case 23:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[0, 20]]);
            }));

            function fetchItemUsingLimit(_x2) {
                return _ref2.apply(this, arguments);
            }

            return fetchItemUsingLimit;
        }()
    }]);
    return GeneralAPI;
}();

module.exports = new GeneralAPI();