'use strict';

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var uuid = require('uuid');

var Q = require('q');

function Subgroup() {}

Subgroup.prototype.getAll = function () {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({}, { _id: 0 }).toArray();
    }
};

Subgroup.prototype.add = function (data) {
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        //insert/update in Subgroup
        var sbgrp_id = data.sbgrp_id || '' + (Math.floor(Math.random() * 90000) + 10000);
        data.sbgrp_id = sbgrp_id;

        return col.update({ sbgrp_id: sbgrp_id }, data, { upsert: true, w: 1 });
    }
};
Subgroup.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ sbgrp_id: data.sbgrp_id });
    }
};

function dbCollection(db) {
    return db.collection("subgroup");
}

module.exports = new Subgroup();