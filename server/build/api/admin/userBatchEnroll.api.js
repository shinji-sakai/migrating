'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batTimingApi = require(configPath.api.batchCourse.batchTiming);
var paymentApi = require(configPath.api.payment.payment);
var profileAPI = require(configPath.api.dashboard.profile);
var mailAPI = require(configPath.api.mail);

function UserBatchEnroll() {}

UserBatchEnroll.prototype.getUserEnrollAllBatch = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var rows, allBatIds, allBatches, allBatchesMap;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						_context.next = 3;
						return paymentApi.getUserEnrolledBatchDetails({ usr_id: data.usr_id });

					case 3:
						rows = _context.sent;

						//get all batch ids
						allBatIds = rows.map(function (v, i) {
							return v.enroll_bat;
						});
						//get batch details for all batch id

						_context.next = 7;
						return batTimingApi.getTimingByBatId({ bat_id: allBatIds });

					case 7:
						allBatches = _context.sent;
						allBatchesMap = {};
						//create map of all batches {bat_id -> batch_detail}

						allBatches.map(function (v, i) {
							allBatchesMap[v.bat_id] = v;
						});
						//attach batch details to correspondance template
						rows = rows.map(function (v, i) {
							return (0, _extends3.default)({}, v, allBatchesMap[v.enroll_bat]);
						});
						return _context.abrupt('return', rows);

					case 14:
						_context.prev = 14;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);
						throw _context.t0;

					case 18:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 14]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

UserBatchEnroll.prototype.addUserBatchEnroll = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var usr_id, enroll_bat, enroll_dt, enroll_status, training, cls_start_dt, cls_frm_tm, frm_wk_dy, to_wk_dy, allBatIds, allBatches, allBatchesMap, user_data, user_email;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						usr_id = data.usr_id;
						enroll_bat = data.enroll_bat;
						enroll_dt = data.enroll_status ? new Date() : undefined;
						enroll_status = data.enroll_status;
						training = data.emailData.training;
						cls_start_dt = data.emailData.cls_start_dt;
						cls_frm_tm = data.emailData.cls_frm_tm;
						frm_wk_dy = data.emailData.frm_wk_dy;
						to_wk_dy = data.emailData.to_wk_dy;
						_context2.next = 12;
						return paymentApi.saveUserEnrolledBatchDetails({ usr_id: usr_id, enroll_bat: enroll_bat, enroll_dt: enroll_dt, enroll_status: enroll_status });

					case 12:
						//get all batch ids
						allBatIds = [enroll_bat];
						//get batch details for all batch id

						_context2.next = 15;
						return batTimingApi.getTimingByBatId({ bat_id: allBatIds });

					case 15:
						allBatches = _context2.sent;
						allBatchesMap = {};
						//create map of all batches {bat_id -> batch_detail}

						allBatches.map(function (v, i) {
							allBatchesMap[v.bat_id] = v;
						});

						if (!enroll_status) {
							_context2.next = 25;
							break;
						}

						_context2.next = 21;
						return profileAPI.getUserShortDetails({ usr_id: usr_id });

					case 21:
						user_data = _context2.sent;
						user_email = user_data.usr_email;
						_context2.next = 25;
						return mailAPI.sendEnrolledBatchMail((0, _extends3.default)({
							to: user_email,
							dsp_nm: user_data.dsp_nm,
							usr_id: usr_id
						}, data.emailData));

					case 25:
						return _context2.abrupt('return', (0, _extends3.default)({}, data, allBatchesMap[enroll_bat]));

					case 29:
						_context2.prev = 29;
						_context2.t0 = _context2['catch'](0);

						logger.debug(_context2.t0);
						throw _context2.t0;

					case 33:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 29]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

UserBatchEnroll.prototype.removeUserBatchEnroll = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var usr_id, enroll_bat, enroll_status, allBatIds, allBatches, allBatchesMap;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						usr_id = data.usr_id;
						enroll_bat = data.enroll_bat;
						enroll_status = false;
						_context3.next = 6;
						return paymentApi.saveUserEnrolledBatchDetails({ usr_id: usr_id, enroll_bat: enroll_bat, enroll_status: enroll_status });

					case 6:
						//get all batch ids
						allBatIds = [enroll_bat];
						//get batch details for all batch id

						_context3.next = 9;
						return batTimingApi.getTimingByBatId({ bat_id: allBatIds });

					case 9:
						allBatches = _context3.sent;
						allBatchesMap = {};
						//create map of all batches {bat_id -> batch_detail}

						allBatches.map(function (v, i) {
							allBatchesMap[v.bat_id] = v;
						});

						return _context3.abrupt('return', (0, _extends3.default)({}, data, {
							enroll_status: false
						}, allBatchesMap[enroll_bat]));

					case 16:
						_context3.prev = 16;
						_context3.t0 = _context3['catch'](0);

						logger.debug(_context3.t0);
						throw _context3.t0;

					case 20:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 16]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();
//

module.exports = new UserBatchEnroll();