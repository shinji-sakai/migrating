'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q = require('q');

function API() {}
API.prototype.getAll = function (data) {
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		return col.find({}).toArray();
	}
};

API.prototype.get = function (data) {
	if (!data.paper_id) {
		return {
			error: "paper_id field is required"
		};
	}
	return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

	function find(col) {
		return col.findOne({ paper_id: data.paper_id });
	}
};

API.prototype.update = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var update = function () {
			var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
				var alreadyExist;
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								_context.prev = 0;
								_context.next = 3;
								return col.findOne({ paper_id: data.paper_id });

							case 3:
								alreadyExist = _context.sent;

								if (alreadyExist) {
									data.updatedAt = new Date();
								} else {
									data.createdAt = new Date();
								}
								_context.next = 7;
								return col.update({ paper_id: data.paper_id }, { "$set": data }, { upsert: true });

							case 7:
								return _context.abrupt('return', data);

							case 10:
								_context.prev = 10;
								_context.t0 = _context['catch'](0);

								console.log(_context.t0);
								throw new Error(_context.t0);

							case 14:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this, [[0, 10]]);
			}));

			return function update(_x2) {
				return _ref2.apply(this, arguments);
			};
		}();

		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						if (data.paper_id) {
							_context2.next = 2;
							break;
						}

						return _context2.abrupt('return', {
							error: "paper_id field is required"
						});

					case 2:
						return _context2.abrupt('return', getConnection().then(dbCollection).then(update).catch(globalFunctions.err));

					case 3:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

API.prototype.remove = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var remove;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						remove = function remove(col) {
							return col.remove({ paper_id: data.paper_id });
						};

						if (data.paper_id) {
							_context3.next = 3;
							break;
						}

						return _context3.abrupt('return', {
							error: "paper_id field is required"
						});

					case 3:
						return _context3.abrupt('return', getConnection().then(dbCollection).then(remove).catch(globalFunctions.err));

					case 4:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

function dbCollection(db) {
	return db.collection("previousPapers");
}

module.exports = new API();