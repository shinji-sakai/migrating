'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var profileAPI = require(configPath.api.dashboard.profile);

var ew_usr_noti_table = 'ew1.ew_usr_noti';
var ew_noti_table = "ew1.ew_noti";
var ew_usr_noti_cnt_table = "ew1.ew_usr_noti_cnt";

function SendNotification() {}

SendNotification.prototype.getAllSendNotifications = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var query, res;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						query = "select * from " + ew_noti_table;
						_context.next = 4;
						return cassExecute(query);

					case 4:
						res = _context.sent;
						return _context.abrupt('return', res);

					case 8:
						_context.prev = 8;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);
						throw _context.t0;

					case 12:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 8]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

SendNotification.prototype.getSendNotificationById = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var usr_noti_pk, query, params, res;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						usr_noti_pk = data.usr_noti_pk;
						query = "select * from " + ew_noti + " where usr_noti_pk = ? ";
						params = [usr_noti_pk];
						_context2.next = 6;
						return cassExecute(query, params);

					case 6:
						res = _context2.sent;

						res = res[0] || {};
						return _context2.abrupt('return', {});

					case 11:
						_context2.prev = 11;
						_context2.t0 = _context2['catch'](0);

						logger.debug(_context2.t0);
						throw _context2.t0;

					case 15:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 11]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

SendNotification.prototype.getSendNotificationByUserId = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var usr_id, query, params, res;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						usr_id = data.usr_id;
						query = "select * from " + ew_usr_noti_table + " where usr_id = ? ";
						params = [usr_id];
						_context3.next = 6;
						return cassExecute(query, params);

					case 6:
						res = _context3.sent;
						return _context3.abrupt('return', res);

					case 10:
						_context3.prev = 10;
						_context3.t0 = _context3['catch'](0);

						logger.debug(_context3.t0);
						throw _context3.t0;

					case 14:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 10]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

SendNotification.prototype.markSendNotificationAsRead = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var usr_id, usr_noti_pk, query, params, res;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						usr_id = data.usr_id;
						usr_noti_pk = data.usr_noti_pk;
						query = "update " + ew_usr_noti_table + " set noti_read=? , noti_read_tm=? where usr_noti_pk = ? and usr_id = ?";
						params = ["true", new Date(), usr_noti_pk, usr_id];
						_context4.next = 7;
						return this.decrementSendNotificationCount({ usr_id: usr_id });

					case 7:
						_context4.next = 9;
						return cassExecute(query, params);

					case 9:
						res = _context4.sent;
						return _context4.abrupt('return', res);

					case 13:
						_context4.prev = 13;
						_context4.t0 = _context4['catch'](0);

						logger.debug(_context4.t0);
						throw _context4.t0;

					case 17:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 13]]);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

//
SendNotification.prototype.markAllSendNotificationAsRead = function () {
	var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
		var usr_id, query, params, res;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.prev = 0;
						usr_id = data.usr_id;
						query = "update " + ew_usr_noti_table + " set noti_read=? , noti_read_tm=? where usr_id = ?";
						params = ["true", new Date(), usr_id];
						_context5.next = 6;
						return this.decrementSendNotificationCount({ usr_id: usr_id });

					case 6:
						_context5.next = 8;
						return cassExecute(query, params);

					case 8:
						res = _context5.sent;
						return _context5.abrupt('return', res);

					case 12:
						_context5.prev = 12;
						_context5.t0 = _context5['catch'](0);

						logger.debug(_context5.t0);
						throw _context5.t0;

					case 16:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, this, [[0, 12]]);
	}));

	return function (_x5) {
		return _ref5.apply(this, arguments);
	};
}();

SendNotification.prototype.addSendNotification = function () {
	var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
		var noti_id, sub, users, noti_msg, snd_dt, queries, i, usr_id, query_m, params_m, res;
		return _regenerator2.default.wrap(function _callee6$(_context6) {
			while (1) {
				switch (_context6.prev = _context6.next) {
					case 0:
						_context6.prev = 0;
						noti_id = Uuid.random();
						sub = data.sub;
						users = data.users;
						noti_msg = data.noti_msg;
						snd_dt = new Date();
						queries = users.map(function (user_id, i) {
							var query = "update " + ew_usr_noti_table + " set sub=? , noti_msg=? , snd_dt=? , noti_id = ? where usr_noti_pk = ? and usr_id = ?";
							var params = [sub, noti_msg, snd_dt, noti_id, Uuid.random(), user_id];

							return {
								query: query,
								params: params
							};
						});
						i = users.length - 1;

					case 8:
						if (!(i >= 0)) {
							_context6.next = 15;
							break;
						}

						usr_id = users[i];
						_context6.next = 12;
						return this.incrementSendNotificationCount({ usr_id: usr_id });

					case 12:
						i--;
						_context6.next = 8;
						break;

					case 15:

						//query to master table
						query_m = "update " + ew_noti_table + " set sub=? , noti_msg=? , snd_dt=? where noti_id = ?";
						params_m = [sub, noti_msg, snd_dt, noti_id];


						queries.push({
							query: query_m,
							params: params_m
						});

						_context6.next = 20;
						return cassBatch(queries);

					case 20:
						res = _context6.sent;
						return _context6.abrupt('return', (0, _extends3.default)({}, data));

					case 24:
						_context6.prev = 24;
						_context6.t0 = _context6['catch'](0);

						logger.debug(_context6.t0);
						throw _context6.t0;

					case 28:
					case 'end':
						return _context6.stop();
				}
			}
		}, _callee6, this, [[0, 24]]);
	}));

	return function (_x6) {
		return _ref6.apply(this, arguments);
	};
}();

SendNotification.prototype.deleteSendNotification = function () {
	var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
		var usr_noti_pk, query, params, res;
		return _regenerator2.default.wrap(function _callee7$(_context7) {
			while (1) {
				switch (_context7.prev = _context7.next) {
					case 0:
						_context7.prev = 0;
						usr_noti_pk = data.usr_noti_pk;

						if (usr_noti_pk) {
							_context7.next = 4;
							break;
						}

						return _context7.abrupt('return', {
							err: 'usr_noti_pk is required'
						});

					case 4:
						query = "delete from " + ew_usr_noti_table + " where usr_noti_pk = ?";
						params = [usr_noti_pk];
						_context7.next = 8;
						return cassExecute(query, params);

					case 8:
						res = _context7.sent;
						return _context7.abrupt('return', { usr_noti_pk: usr_noti_pk });

					case 12:
						_context7.prev = 12;
						_context7.t0 = _context7['catch'](0);

						logger.debug(_context7.t0);
						throw _context7.t0;

					case 16:
					case 'end':
						return _context7.stop();
				}
			}
		}, _callee7, this, [[0, 12]]);
	}));

	return function (_x7) {
		return _ref7.apply(this, arguments);
	};
}();

SendNotification.prototype.getSendNotificationCount = function () {
	var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
		var usr_id, query_m, params_m, res;
		return _regenerator2.default.wrap(function _callee8$(_context8) {
			while (1) {
				switch (_context8.prev = _context8.next) {
					case 0:
						_context8.prev = 0;
						usr_id = data.usr_id;
						//query to master table

						query_m = "select * from " + ew_usr_noti_cnt_table + " where usr_id = ?";
						params_m = [usr_id];
						_context8.next = 6;
						return cassExecute(query_m, params_m);

					case 6:
						res = _context8.sent;
						return _context8.abrupt('return', res);

					case 10:
						_context8.prev = 10;
						_context8.t0 = _context8['catch'](0);

						logger.debug(_context8.t0);
						throw _context8.t0;

					case 14:
					case 'end':
						return _context8.stop();
				}
			}
		}, _callee8, this, [[0, 10]]);
	}));

	return function (_x8) {
		return _ref8.apply(this, arguments);
	};
}();

SendNotification.prototype.incrementSendNotificationCount = function () {
	var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(data) {
		var usr_id, type, query_m, params_m, res;
		return _regenerator2.default.wrap(function _callee9$(_context9) {
			while (1) {
				switch (_context9.prev = _context9.next) {
					case 0:
						_context9.prev = 0;
						usr_id = data.usr_id;
						type = data.type || "web";

						if (type === "web") {
							query_m = "update " + ew_usr_noti_cnt_table + " set web_noti_cnt=web_noti_cnt+1 where usr_id = ?";
						} else if (type === "qa") {
							query_m = "update " + ew_usr_noti_cnt_table + " set qa_noti_cnt=qa_noti_cnt+1 where usr_id = ?";
						} else if (type === "cb") {
							query_m = "update " + ew_usr_noti_cnt_table + " set cb_noti_cnt=cb_noti_cnt+1 where usr_id = ?";
						}

						params_m = [usr_id];
						_context9.next = 7;
						return cassExecute(query_m, params_m);

					case 7:
						res = _context9.sent;
						return _context9.abrupt('return', (0, _extends3.default)({}, data));

					case 11:
						_context9.prev = 11;
						_context9.t0 = _context9['catch'](0);

						logger.debug(_context9.t0);
						throw _context9.t0;

					case 15:
					case 'end':
						return _context9.stop();
				}
			}
		}, _callee9, this, [[0, 11]]);
	}));

	return function (_x9) {
		return _ref9.apply(this, arguments);
	};
}();

SendNotification.prototype.decrementSendNotificationCount = function () {
	var _ref10 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(data) {
		var usr_id, type, query_m, params_m, res;
		return _regenerator2.default.wrap(function _callee10$(_context10) {
			while (1) {
				switch (_context10.prev = _context10.next) {
					case 0:
						_context10.prev = 0;
						usr_id = data.usr_id;
						type = data.type || "web";

						if (type === "web") {
							query_m = "update " + ew_usr_noti_cnt_table + " set web_noti_cnt=web_noti_cnt-1 where usr_id = ?";
						} else if (type === "qa") {
							query_m = "update " + ew_usr_noti_cnt_table + " set qa_noti_cnt=qa_noti_cnt-1 where usr_id = ?";
						} else if (type === "cb") {
							query_m = "update " + ew_usr_noti_cnt_table + " set cb_noti_cnt=cb_noti_cnt-1 where usr_id = ?";
						}

						params_m = [usr_id];
						_context10.next = 7;
						return cassExecute(query_m, params_m);

					case 7:
						res = _context10.sent;
						return _context10.abrupt('return', (0, _extends3.default)({}, data));

					case 11:
						_context10.prev = 11;
						_context10.t0 = _context10['catch'](0);

						logger.debug(_context10.t0);
						throw _context10.t0;

					case 15:
					case 'end':
						return _context10.stop();
				}
			}
		}, _callee10, this, [[0, 11]]);
	}));

	return function (_x10) {
		return _ref10.apply(this, arguments);
	};
}();

module.exports = new SendNotification();