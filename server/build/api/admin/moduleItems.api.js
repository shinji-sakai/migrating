'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var getRedisClient = require(configPath.dbconn.redisconn);
var jwt_simple = require("jwt-simple");
var config = require('../../config.js');
var redisCli = getRedisClient();

var ew_usr_enroll_bat_uploads_table = 'ew1.ew_usr_enroll_bat_uploads';
var ew_crs_access_tbl = "ew1.ew_crs_access";

var Q = require('q');

function ModuleItems() {}
ModuleItems.prototype.getAll = function (data) {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({
            "moduleDetail.moduleId": data.moduleId
        }, {
            "moduleDetail.$": 1,
            _id: 0
        }).toArray();
    }
};

ModuleItems.prototype.getCourseItems = function (data) {

    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({
            "courseId": data.courseId
        }, {
            "moduleDetail.moduleItems.itemQuestions": 0
        }).toArray();
    }
};

ModuleItems.prototype.hasUserPurchasedTheCourse = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var usr_id, courseId, qry, params, rows, isCourseFound, i, row;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        usr_id = data.usr_id, courseId = data.courseId;
                        qry = "select * from " + ew_crs_access_tbl + " where usr_id = ?";
                        params = [usr_id];
                        _context.next = 6;
                        return cassExecute(qry, params);

                    case 6:
                        rows = _context.sent;
                        isCourseFound = false;
                        i = 0;

                    case 9:
                        if (!(i < rows.length)) {
                            _context.next = 17;
                            break;
                        }

                        row = rows[i];

                        if (!(row.crs_id === courseId)) {
                            _context.next = 14;
                            break;
                        }

                        isCourseFound = true;
                        return _context.abrupt('break', 17);

                    case 14:
                        i++;
                        _context.next = 9;
                        break;

                    case 17:
                        return _context.abrupt('return', isCourseFound);

                    case 20:
                        _context.prev = 20;
                        _context.t0 = _context['catch'](0);

                        console.log(_context.t0);
                        throw new Error(_context.t0);

                    case 24:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 20]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

ModuleItems.prototype.getFullCourseDetails = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req) {
        var courseId, isCoursePurchased, authorization, auth_jwt, decoded, usr_id, str_course, jsonCourse;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        courseId = req.body.courseId;
                        isCoursePurchased = false;
                        authorization = req.get("Authorization");

                        if (!(authorization && authorization.indexOf("JWT") === 0)) {
                            _context2.next = 17;
                            break;
                        }

                        auth_jwt = authorization.substr(4);
                        _context2.prev = 6;
                        decoded = jwt_simple.decode(auth_jwt, config.TOKEN_SECRET);
                        usr_id = decoded.userId;
                        _context2.next = 11;
                        return this.hasUserPurchasedTheCourse({ usr_id: usr_id, courseId: courseId });

                    case 11:
                        isCoursePurchased = _context2.sent;
                        _context2.next = 17;
                        break;

                    case 14:
                        _context2.prev = 14;
                        _context2.t0 = _context2['catch'](6);

                        console.log("error in decoding");

                    case 17:
                        _context2.next = 19;
                        return redisCli.getAsync(courseId);

                    case 19:
                        str_course = _context2.sent;

                        if (!(str_course && str_course != 'null')) {
                            _context2.next = 24;
                            break;
                        }

                        jsonCourse = JSON.parse(str_course);

                        jsonCourse.isCoursePurchased = isCoursePurchased;
                        return _context2.abrupt('return', jsonCourse);

                    case 24:
                        return _context2.abrupt('return', {});

                    case 27:
                        _context2.prev = 27;
                        _context2.t1 = _context2['catch'](0);
                        throw new Error(_context2.t1);

                    case 30:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 27], [6, 14]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

ModuleItems.prototype.getModuleItems = function (data) {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({
            "moduleDetail.moduleId": data.moduleId
        }, {
            "moduleDetail.$": 1,
            _id: 0
        }).toArray();
    }
};

ModuleItems.prototype.update = function (data) {
    var col;
    return getConnection().then(dbCollection).then(removeData).then(insertData).catch(globalFunctions.err);

    function removeData(collection) {
        col = collection;
        return col.updateOne({
            "courseId": data.courseId
        }, {
            $pull: {
                "moduleDetail": {
                    "moduleId": data.moduleDetail.moduleId
                }
            }
        });
    }

    function insertData() {
        return col.updateOne({
            "courseId": data.courseId
        }, {
            $addToSet: {
                "moduleDetail": data.moduleDetail
            },
            $set: {
                "courseName": data.courseName,
                "author": data.author,
                "courseLevel": data.courseLevel,
                courseLongDesc: data.courseLongDesc,
                courseShortDesc: data.courseShortDesc,
                LastUpdated: data.LastUpdated,
                isBatch: data.isBatch,
                training_id: data.training_id
            }
        }, {
            upsert: true
        });
    }
};

ModuleItems.prototype.deleteModuleItemsUsingCourseId = function (data) {
    return getConnection().then(dbCollection).then(deleteO).catch(globalFunctions.err);

    function deleteO(col) {
        return col.deleteOne({
            courseId: data.courseId
        });
    }
};

ModuleItems.prototype.updateUsingQuery = function (data, query) {
    return getConnection().then(dbCollection).then(update).catch(globalFunctions.err);

    function update(col) {
        return col.update(data, query, {
            upsert: true
        });
    }
};

ModuleItems.prototype.saveUserBatchEnrollUploads = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var usr_assign_upload_pk, usr_id, enroll_bat, upload_module, upload_item, upload_path, upload_nm, upload_tm, query, params;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        usr_assign_upload_pk = Uuid.random();
                        usr_id = data.usr_id;
                        enroll_bat = data.enroll_bat;
                        upload_module = data.upload_module;
                        upload_item = data.upload_item;
                        upload_path = data.upload_path;
                        upload_nm = data.upload_nm;
                        upload_tm = new Date();
                        query = "insert into " + ew_usr_enroll_bat_uploads_table + " (usr_assign_upload_pk,usr_id,enroll_bat,upload_module,upload_item,upload_path,upload_tm,upload_nm) values (?,?,?,?,?,?,?,?)";
                        params = [usr_assign_upload_pk, usr_id, enroll_bat, upload_module, upload_item, upload_path, upload_tm, upload_nm];
                        _context3.next = 13;
                        return cassExecute(query, params);

                    case 13:
                        return _context3.abrupt('return', data);

                    case 16:
                        _context3.prev = 16;
                        _context3.t0 = _context3['catch'](0);

                        logger.debug(_context3.t0);
                        throw _context3.t0;

                    case 20:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 16]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

ModuleItems.prototype.getUserBatchEnrollUploads = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var usr_id, query, params, rows;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        usr_id = data.usr_id;
                        query = "select * from " + ew_usr_enroll_bat_uploads_table + " where usr_id = ?";
                        params = [usr_id];
                        _context4.next = 6;
                        return cassExecute(query, params);

                    case 6:
                        rows = _context4.sent;
                        return _context4.abrupt('return', rows);

                    case 10:
                        _context4.prev = 10;
                        _context4.t0 = _context4['catch'](0);

                        logger.debug(_context4.t0);
                        throw _context4.t0;

                    case 14:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 10]]);
    }));

    return function (_x4) {
        return _ref4.apply(this, arguments);
    };
}();

function dbCollection(db) {
    return db.collection("moduleitems");
}

module.exports = new ModuleItems();