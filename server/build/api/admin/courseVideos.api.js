'use strict';

var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var Q = require('q');

function CourseVideos() {}

CourseVideos.prototype.add = function (data) {
    var col;
    return getConnection().then(dbCollection).then(removeData).then(insertData).catch(globalFunctions.err);

    function removeData(gcol) {
        col = gcol;
        return col.updateOne({
            "courseId": data.courseId,
            "courseName": data.courseName
        }, {
            $pull: { "moduleDetail": { "moduleId": data.moduleDetail.moduleId } }
        });
    }

    function insertData() {
        return col.updateOne({
            "courseId": data.courseId,
            "courseName": data.courseName
        }, {
            $addToSet: {
                "moduleDetail": data.moduleDetail
            }, $set: { "author": data.author, "courseLevel": data.courseLevel, courseLongDesc: data.courseLongDesc, courseShortDesc: data.courseShortDesc, LastUpdated: data.LastUpdated }
        }, { upsert: true });
    }
};
CourseVideos.prototype.getModuleVideos = function (data) {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({ "moduleDetail.moduleId": data.moduleId }, { "moduleDetail.$": 1, _id: 0 }).toArray();
    }
};
CourseVideos.prototype.getCourseVideos = function (data) {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({ "courseId": data.courseId }).toArray();
    }
};

function dbCollection(db) {
    return db.collection("coursevideos");
}

module.exports = new CourseVideos();