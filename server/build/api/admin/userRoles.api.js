'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var profileAPI = require(configPath.api.dashboard.profile);

var ew_usr_role_table = 'ew1.ew_usr_role';

function Roles() {}

Roles.prototype.getAllUserRoles = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var query, res, userSpecificRoles, keys, resArray;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						query = "select * from " + ew_usr_role_table;
						_context.next = 4;
						return cassExecute(query);

					case 4:
						res = _context.sent;
						userSpecificRoles = {};

						res.map(function (record, i) {

							userSpecificRoles[record.usr_id] = userSpecificRoles[record.usr_id] || {};

							userSpecificRoles[record.usr_id]["usr_id"] = record["usr_id"];

							userSpecificRoles[record.usr_id]["roles"] = userSpecificRoles[record.usr_id]["roles"] || [];

							userSpecificRoles[record.usr_id]["roles"].push({
								role_id: record["role_id"],
								form_access: record["form_access"]
							});
						});

						keys = Object.keys(userSpecificRoles || {});
						resArray = keys.map(function (key, i) {
							return userSpecificRoles[key];
						});
						return _context.abrupt('return', resArray);

					case 12:
						_context.prev = 12;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);
						throw _context.t0;

					case 16:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 12]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

Roles.prototype.getUserAdminRoleFormAccessList = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var usr_id, role_id, query, params, rows, row, form_access_obj, form_access_list;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						usr_id = data.usr_id;
						role_id = "admin";
						query = "select * from " + ew_usr_role_table + " where usr_id = ? and role_id = ?";
						params = [usr_id, role_id];
						_context2.next = 7;
						return cassExecute(query, params);

					case 7:
						rows = _context2.sent;

						if (!rows[0]) {
							_context2.next = 15;
							break;
						}

						row = rows[0];
						form_access_obj = row["form_access"];
						form_access_list = Object.keys(form_access_obj || {});
						return _context2.abrupt('return', form_access_list);

					case 15:
						return _context2.abrupt('return', []);

					case 16:
						_context2.next = 22;
						break;

					case 18:
						_context2.prev = 18;
						_context2.t0 = _context2['catch'](0);

						logger.debug(_context2.t0);
						throw _context2.t0;

					case 22:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 18]]);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

Roles.prototype.addUserRole = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var roles, usr_id, rolesForProfile, queries, res;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						roles = data.roles;
						usr_id = data.usr_id;
						rolesForProfile = roles.map(function (v, i) {
							return v.role_id;
						});
						_context3.next = 6;
						return this.deleteUserRole({ usr_id: usr_id });

					case 6:
						queries = roles.map(function (v, i) {
							var query = "insert into " + ew_usr_role_table + " (form_access,usr_id,role_id) values (?,?,?)";
							var params = [v.form_access, usr_id, v.role_id];
							logger.debug(query, params);
							return {
								query: query,
								params: params
							};
						});
						_context3.next = 9;
						return profileAPI.updateUserRole({ usr_id: usr_id, usr_role: rolesForProfile });

					case 9:
						_context3.next = 11;
						return cassBatch(queries, { prepare: true });

					case 11:
						res = _context3.sent;
						return _context3.abrupt('return', data);

					case 15:
						_context3.prev = 15;
						_context3.t0 = _context3['catch'](0);

						logger.debug(_context3.t0);
						throw _context3.t0;

					case 19:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 15]]);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

Roles.prototype.deleteUserRole = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var usr_id, query, params, res;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						usr_id = data.usr_id;
						query = "delete from " + ew_usr_role_table + " where usr_id = ?";
						params = [usr_id];
						_context4.next = 6;
						return cassExecute(query, params);

					case 6:
						res = _context4.sent;
						return _context4.abrupt('return', { usr_id: usr_id });

					case 10:
						_context4.prev = 10;
						_context4.t0 = _context4['catch'](0);

						logger.debug(_context4.t0);
						throw _context4.t0;

					case 14:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 10]]);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

module.exports = new Roles();