'use strict';

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var shortId = require('shortid');
var logger = require(configPath.lib.log_to_file);

function BookTopicAPI() {}

BookTopicAPI.prototype.updateBookTopic = function (data) {
    return getConnection().then(getCollection).then(updateBookTopic).catch(globalFunctions.err);

    function updateBookTopic(col) {
        if (!data.bookTopicId) {
            data.bookTopicId = shortId.generate();
        }
        return col.update({
            bookTopicId: data.bookTopicId
        }, data, {
            upsert: true
        });
    }
};

BookTopicAPI.prototype.removeBookTopic = function (data) {
    return getConnection().then(getCollection).then(removeBookTopic).catch(globalFunctions.err);

    function removeBookTopic(col) {
        return col.remove({
            bookTopicId: data.bookTopicId
        });
    }
};

BookTopicAPI.prototype.getAllBookTopics = function (data) {
    return getConnection().then(getCollection).then(getAll).catch(globalFunctions.err);

    function getAll(col) {
        return col.find({}).toArray();
    }
};

BookTopicAPI.prototype.findBookTopicsByQuery = function (data) {
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if (data[key].length > 0) {
                data[key] = {
                    $in: data[key]
                };
            } else {
                delete data[key];
            }
        }
    }

    return getConnection().then(getCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find(data).toArray();
    }
};

BookTopicAPI.prototype.findBookTopicById = function (data) {
    return getConnection().then(getCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({
            bookTopicId: data.bookTopicId
        }).toArray();
    }
};

BookTopicAPI.prototype.findBookTopicsById = function (data) {
    return getConnection().then(getCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({
            bookTopicId: {
                $in: data.bookTopicId
            }
        }).toArray();
    }
};

function getCollection(db) {
    var col = db.collection('booktopic');
    return col;
}

module.exports = new BookTopicAPI();