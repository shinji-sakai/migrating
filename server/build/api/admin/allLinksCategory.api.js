'use strict';

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var uuid = require('uuid');

var Q = require('q');

function AllLinksCategory() {}

AllLinksCategory.prototype.getAll = function () {
    return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

    function find(col) {
        return col.find({}, { _id: 0 }).toArray();
    }
};

AllLinksCategory.prototype.add = function (data) {
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        if (!data.cat_id) {
            data.create_dt = new Date();
            data.update_dt = new Date();
        } else {
            data.update_dt = new Date();
        }
        //insert/update in skill
        var cat_id = data.cat_id || '' + (Math.floor(Math.random() * 90000) + 10000);
        data.cat_id = cat_id;

        return col.update({ cat_id: cat_id }, { $set: data }, { upsert: true, w: 1 });
    }
};
AllLinksCategory.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ cat_id: data.cat_id });
    }
};

function dbCollection(db) {
    return db.collection("allLinksCategory");
}

module.exports = new AllLinksCategory();