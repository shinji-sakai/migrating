'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:cmnts");
var profileAPI = require(configPath.api.dashboard.profile);
var cassExecute = require(configPath.lib.utility).cassExecute;
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

// var comment_lvl0_table = 'ew1.vt_usr_comnt_lvl0';
var comment_lvl0_table = 'ew1.ew_usr_comnt_lvl0';
var comment_lvl1_table = 'ew1.ew_usr_comnt_lvl1';
var comment_like_table = 'ew1.vt_qry_like';
var comment_like_stats_table = 'ew1.vt_qry_like_stats';
var comment_users_table = "ew1.vt_qry_like_usrs";
var COMMENT_FETCH_LIMIT = 5;

var Comments = function () {
    function Comments() {
        (0, _classCallCheck3.default)(this, Comments);

        this.comments = [];
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.usr_id - Usrid who posted the comment
     * @param {uuid} data.pst_id - Post ID to which comment is related
     * @param {set} data.cmnt_atch - Array Of Comment Attachment
     * @param {uuid} data.cmnt_cmnt_id - If comment is level 1 comment then this will be id of parent comment
     * @param {set} data.cmnt_img - Image attached to comment
     * @param {text} data.cmnt_lvl - Comment Level
     * @param {text} data.cmnt_txt - Comment Text
     * @param {set} data.cmnt_vid - Video attached to comment.
     * @param {set} data.from - Source of comment.This will tell us , from where comment is coming for e.g. qa-ans,qa-comment,cb-comment
     * @returns {Promise}
     * @desc
     * - This function is used to insert new comment
     * - If Comment source is qa-ans then we will increment dashboard stats for qa questions answered. See {@link DashboardStats#incrementForumQuestionAnsweredCounter}
     * - Else we will increment dashboard stats for comments. See {@link DashboardStats#incrementCommentCounter}
     * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
     */


    (0, _createClass3.default)(Comments, [{
        key: 'save',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_cmnt_id, cmnt_lvl, usr_id, cmnt_from, insertPost, insertPost_args;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                pst_id = data.pst_id;
                                cmnt_id = TimeUuid.now();
                                cmnt_ts = new Date();
                                cmnt_txt = data.cmnt_txt;
                                cmnt_cmnt_id = data.cmnt_cmnt_id || null;
                                cmnt_lvl = data.cmnt_lvl;
                                usr_id = data.usr_id;
                                cmnt_from = data.from; // this will tell us , from where comment is coming for e.g. qa-ans,qa-comment,cb-comment

                                insertPost = void 0;
                                insertPost_args = void 0;


                                if (cmnt_lvl !== '1') {
                                    // if 0 level comment
                                    insertPost = 'INSERT INTO ' + comment_lvl0_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_lvl,usr_id)  ' + 'VALUES(?, ?, ?, ?, ?,?);';
                                    insertPost_args = [pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_lvl, usr_id];
                                } else {
                                    // if 1 level comment
                                    insertPost = 'INSERT INTO ' + comment_lvl1_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_lvl,usr_id,cmnt_cmnt_id)  ' + 'VALUES(?,?,?,?,?,?,?);';
                                    insertPost_args = [pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_lvl, usr_id, cmnt_cmnt_id];
                                }

                                _context.next = 14;
                                return cassExecute(insertPost, insertPost_args);

                            case 14:
                                _context.prev = 14;

                                if (!(cmnt_from === 'qa-ans')) {
                                    _context.next = 20;
                                    break;
                                }

                                _context.next = 18;
                                return dashboardStatsAPI.incrementForumQuestionAnsweredCounter({
                                    usr_id: usr_id
                                });

                            case 18:
                                _context.next = 22;
                                break;

                            case 20:
                                _context.next = 22;
                                return dashboardStatsAPI.incrementCommentCounter({
                                    usr_id: usr_id
                                });

                            case 22:
                                _context.next = 27;
                                break;

                            case 24:
                                _context.prev = 24;
                                _context.t0 = _context['catch'](14);

                                debug(_context.t0);

                            case 27:
                                return _context.abrupt('return', {
                                    cmnt_id: cmnt_id,
                                    cmnt_ts: cmnt_ts
                                });

                            case 30:
                                _context.prev = 30;
                                _context.t1 = _context['catch'](0);
                                throw new Error(_context.t1);

                            case 33:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 30], [14, 24]]);
            }));

            function save(_x) {
                return _ref.apply(this, arguments);
            }

            return save;
        }()

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - Post ID for which we want to get all comments
         * @returns {Promise}
         * @desc
         * - This function is used to get all comments for given postId
         * - After getting all comments we will get all users from those comments and we will get short details for those users using {@link CareerBook#getUsersShortDetails}
         * - Then merge the results.
         * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
         */

    }, {
        key: 'get',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                var pst_id, cmnt_lvl, cmnt_cmnt_id, last_cmnt_id, qry, params, jsonComments, usr_ids, users, userMapping, allCommentsdata;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                pst_id = data.pst_id, cmnt_lvl = data.cmnt_lvl, cmnt_cmnt_id = data.cmnt_cmnt_id, last_cmnt_id = data.last_cmnt_id;

                                if (!(!pst_id || !cmnt_lvl)) {
                                    _context2.next = 4;
                                    break;
                                }

                                return _context2.abrupt('return', {
                                    status: 'error',
                                    message: 'pst_id and cmnt_lvl are required'
                                });

                            case 4:
                                qry = void 0;
                                params = void 0;

                                if (cmnt_lvl === '0') {
                                    // if 0 level comment
                                    if (last_cmnt_id) {
                                        // if user want to fetch from last_cmnt_id
                                        qry = 'select * from ' + comment_lvl0_table + ' where pst_id = ? and cmnt_id < ? limit ' + COMMENT_FETCH_LIMIT;
                                        params = [pst_id, last_cmnt_id];
                                    } else {
                                        // if user want to fetch first few comments
                                        qry = 'select * from ' + comment_lvl0_table + ' where pst_id = ? limit ' + COMMENT_FETCH_LIMIT;
                                        params = [pst_id];
                                    }
                                } else {
                                    // if 1 level comment
                                    if (last_cmnt_id) {
                                        // if user want to fetch from last_cmnt_id
                                        qry = 'select * from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_cmnt_id = ? and  cmnt_id < ? limit ' + COMMENT_FETCH_LIMIT;
                                        params = [pst_id, cmnt_cmnt_id, last_cmnt_id];
                                    } else {
                                        // if user want to fetch first few comments
                                        qry = 'select * from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_cmnt_id = ? limit ' + COMMENT_FETCH_LIMIT;
                                        params = [pst_id, cmnt_cmnt_id];
                                    }
                                }

                                _context2.next = 9;
                                return cassExecute(qry, params);

                            case 9:
                                jsonComments = _context2.sent;


                                jsonComments = jsonComments.filter(function (v, i) {
                                    if (v.usr_id) {
                                        return true;
                                    }
                                    return false;
                                });

                                //get all userse
                                usr_ids = jsonComments.map(function (v, i) {
                                    return v.usr_id;
                                });
                                _context2.next = 14;
                                return profileAPI.getUsersShortDetails({ usr_ids: usr_ids });

                            case 14:
                                users = _context2.sent;
                                userMapping = {};
                                //generate user mapping so we can direct fetch from dict

                                users.map(function (v, i) {
                                    delete v.usr_email;
                                    delete v.usr_role;
                                    delete v.usr_ph;
                                    userMapping[v.usr_id] = v;
                                });

                                allCommentsdata = jsonComments.map(function (v, i) {
                                    //get post author
                                    var crt_by = v['usr_id'];
                                    //get user info
                                    var user = userMapping[crt_by];
                                    //attach user info to item
                                    v['usr_info'] = user;
                                    return v;
                                });
                                return _context2.abrupt('return', allCommentsdata);

                            case 21:
                                _context2.prev = 21;
                                _context2.t0 = _context2['catch'](0);
                                throw new Error(_context2.t0);

                            case 24:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[0, 21]]);
            }));

            function get(_x2) {
                return _ref2.apply(this, arguments);
            }

            return get;
        }()

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - Post ID
         * @param {uuid} data.cmnt_id - Comment Id which we want to edit
         * @param {timestamp} data.cmnt_ts - Comment Timestamp
         * @param {Array} data.cmnt_atch - Comment Attachments
         * @param {Array} data.cmnt_vid - Comment Video Attachments
         * @param {Array} data.cmnt_img - Comment Images Attachments
         * @param {string} data.cmnt_txt - Comment Text
         * @returns {Promise}
         * @desc
         * - This function is used to update comment data for given comment id
         * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
         */

    }, {
        key: 'edit',
        value: function edit(data) {
            var defer = Q.defer();
            var pst_id = data.pst_id;
            var cmnt_id = data.cmnt_id;

            var cmnt_ts = new Date(data.cmnt_ts);
            var cmnt_txt = data.cmnt_txt;

            var cmnt_lvl = data.cmnt_lvl;
            var cmnt_cmnt_id = data.cmnt_cmnt_id;

            var updatePost = void 0;
            var updatePost_args = void 0;

            if (cmnt_lvl === '0') {
                updatePost = 'update ' + comment_lvl0_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ?';
                updatePost_args = [cmnt_txt, cmnt_id, pst_id];
            } else {
                updatePost = 'update ' + comment_lvl1_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ? and cmnt_cmnt_id = ?';
                updatePost_args = [cmnt_txt, cmnt_id, pst_id, cmnt_cmnt_id];
            }

            cassconn.execute(updatePost, updatePost_args, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - Post ID
         * @param {uuid} data.cmnt_id - Comment Id which we want to edit
         * @param {timestamp} data.cmnt_ts - Comment Timestamp
         * @param {string} data.usr_id - Author of comment
         * @returns {Promise}
         * @desc
         * - This function is used to delete given comment id
         * - We will decrement dashboard stats comment counter. See {@link DashboardStats#decrementCommentCounter}
         * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
         */

    }, {
        key: 'delete',
        value: function _delete(data) {
            var defer = Q.defer();
            var pst_id = data.pst_id;
            var cmnt_id = data.cmnt_id;
            var cmnt_ts = new Date(data.cmnt_ts);
            var usr_id = data.usr_id;

            var cmnt_lvl = data.cmnt_lvl;
            var cmnt_cmnt_id = data.cmnt_cmnt_id;

            var deletePost = void 0;
            var deletePost_args = void 0;

            if (cmnt_lvl === '0') {
                deletePost = 'delete from ' + comment_lvl0_table + ' where pst_id = ? and cmnt_id = ?';
                deletePost_args = [pst_id, cmnt_id];
            } else {
                deletePost = 'delete from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_id = ? and cmnt_cmnt_id = ?';
                deletePost_args = [pst_id, cmnt_id, cmnt_cmnt_id];
            }

            cassconn.execute(deletePost, deletePost_args, function () {
                var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(err, res, r) {
                    return _regenerator2.default.wrap(function _callee3$(_context3) {
                        while (1) {
                            switch (_context3.prev = _context3.next) {
                                case 0:
                                    if (!err) {
                                        _context3.next = 4;
                                        break;
                                    }

                                    debug(err);
                                    defer.reject(err);
                                    return _context3.abrupt('return');

                                case 4:
                                    _context3.prev = 4;
                                    _context3.next = 7;
                                    return dashboardStatsAPI.decrementCommentCounter({
                                        usr_id: usr_id
                                    });

                                case 7:
                                    _context3.next = 12;
                                    break;

                                case 9:
                                    _context3.prev = 9;
                                    _context3.t0 = _context3['catch'](4);

                                    debug(_context3.t0);

                                case 12:

                                    defer.resolve([]);

                                case 13:
                                case 'end':
                                    return _context3.stop();
                            }
                        }
                    }, _callee3, this, [[4, 9]]);
                }));

                return function (_x3, _x4, _x5) {
                    return _ref3.apply(this, arguments);
                };
            }());

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Query ID
         * @param {text} data.usr_id - User id who did like/dislike
         * @param {uuid} data.cmnt_id - Comment Id
         * @param {int} data.like_dlike_flg - Like/Dislike Flag
         * @returns {Promise}
         * @desc
         * - This function is used to update like/dislike flag for given comment
         * - Tables Used : {@link CassandraTables#vt_qry_like}
         */

    }, {
        key: 'likeDislikeComment',
        value: function likeDislikeComment(data) {
            var defer = Q.defer();
            var qry_id = data.qry_id || null;
            var usr_id = data.usr_id || 'n/a';
            var cmnt_id = data.cmnt_id || null;
            var crt_ts = new Date();
            var like_dlike_flg = data.like_dlike_flg;

            var insert = 'insert into ' + comment_like_table + ' (qry_id,usr_id,cmnt_id,crt_ts,like_dlike_flg) values(?,?,?,?,?)';
            var insert_args = [qry_id, usr_id, cmnt_id, crt_ts, like_dlike_flg];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            cassconn.execute(insert, insert_args, queryOptions, function (err, res, r) {

                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Query ID
         * @param {text} data.usr_id - User id who did like/dislike
         * @returns {Promise}
         * @desc
         * - This function is used to get all like/dislike comment for given post and user
         * - Tables Used : {@link CassandraTables#vt_qry_like}
         */

    }, {
        key: 'getLikeDislikeComments',
        value: function getLikeDislikeComments(data) {
            var defer = Q.defer();

            var qry_id = data.qry_id || null;
            var usr_id = data.usr_id || 'n/a';

            var select = 'select * from ' + comment_like_table + ' where qry_id = ? and  usr_id = ?';
            var select_args = [qry_id, usr_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            cassconn.execute(select, select_args, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows || []);
            });
            return defer.promise;
        }
    }, {
        key: 'getLikeDislikeCommentsByCmntId',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var qry_id, usr_id, cmnt_id, select, select_args, queryOptions, res;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                qry_id = data.qry_id || null;
                                usr_id = data.usr_id || 'n/a';
                                cmnt_id = data.cmnt_id || null;

                                if (!(!qry_id || !cmnt_id)) {
                                    _context4.next = 6;
                                    break;
                                }

                                return _context4.abrupt('return', { err: "qry_id and cmnt_id parameters required" });

                            case 6:
                                select = 'select * from ' + comment_like_table + ' where qry_id = ? and  usr_id = ? and cmnt_id = ?';
                                select_args = [qry_id, usr_id, cmnt_id];
                                queryOptions = {
                                    prepare: true,
                                    consistency: cassandra.types.consistencies.quorum
                                };
                                _context4.next = 11;
                                return cassExecute(select, select_args, queryOptions);

                            case 11:
                                res = _context4.sent;
                                return _context4.abrupt('return', res);

                            case 15:
                                _context4.prev = 15;
                                _context4.t0 = _context4['catch'](0);
                                throw new Error(_context4.t0);

                            case 18:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 15]]);
            }));

            function getLikeDislikeCommentsByCmntId(_x6) {
                return _ref4.apply(this, arguments);
            }

            return getLikeDislikeCommentsByCmntId;
        }()

        /**
         * @param {JSON} data
         * @param {uuid} data.cmnt_id - Comment ID
         * @returns {Promise}
         * @desc
         * - This function is used to get all users id who liked the comment
         * - Then get short details for all users using {@link CareerBook#getUsersShortDetails}
         * - Remove email id from all users as we dont require it on client side and it is sensitive info
         * - Tables Used : {@link CassandraTables#vt_qry_like_usrs}
         */

    }, {
        key: 'getCommentUsers',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var cmnt_id, query, params, res, usersIdArr, usersDetails;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;
                                cmnt_id = data.cmnt_id;
                                query = "select * from " + comment_users_table + " where cmnt_id=?";
                                params = [cmnt_id];
                                _context5.next = 6;
                                return cassExecute(query, params);

                            case 6:
                                res = _context5.sent;

                                //get all user id
                                usersIdArr = res.map(function (v, i) {
                                    return v.usr_id;
                                });
                                //get users short details

                                _context5.next = 10;
                                return profileAPI.getUsersShortDetails({
                                    usr_ids: usersIdArr
                                });

                            case 10:
                                usersDetails = _context5.sent;

                                //remove email from all users and send it to client
                                usersDetails = usersDetails.map(function (v, i) {
                                    delete v["usr_email"];
                                    return v;
                                });
                                return _context5.abrupt('return', usersDetails);

                            case 15:
                                _context5.prev = 15;
                                _context5.t0 = _context5['catch'](0);

                                debug(_context5.t0);
                                throw _context5.t0;

                            case 19:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 15]]);
            }));

            function getCommentUsers(_x7) {
                return _ref5.apply(this, arguments);
            }

            return getCommentUsers;
        }()

        /**
         * @param {JSON} data
         * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
         * @param {uuid} cmnt_id - Comment Id
         * @param {counter} dlike_cnt - Dislike Count for e.g. -1/0/1
         * @param {counter} like_cnt - Like Count for e.g. -1/0/1
         * @returns {Promise}
         * @desc
         * - This function is used to increment/decrement like/dislike counts for given comment
         * - Tables Used : {@link CassandraTables#vt_qry_like_stats}
         */

    }, {
        key: 'updateCommentLikeDislikeCount',
        value: function updateCommentLikeDislikeCount(data) {
            var defer = Q.defer();

            //data = {qry_id : qry_id , cmnt_id : cmnt_id , like : -1/0/1 , dislike : -1/0/1}

            var qry_id = data.qry_id || null;
            var cmnt_id = data.cmnt_id || null;

            var like_cnt_str = ' like_cnt = like_cnt + ' + (data.like || 0);
            var dlike_cnt_str = ' dlike_cnt = dlike_cnt + ' + (data.dislike || 0);
            var where_clause = ' where qry_id = ? and cmnt_id = ?';

            var update_like = 'update ' + comment_like_stats_table + ' set ' + like_cnt_str + "," + dlike_cnt_str + where_clause;
            var update_args = [qry_id, cmnt_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            cassconn.execute(update_like, update_args, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
         * @returns {Promise}
         * @desc
         * - This function is used to get comment like/dislike count
         * - Tables Used : {@link CassandraTables#vt_qry_like_stats}
         */

    }, {
        key: 'getCommentLikeDislikeCount',
        value: function getCommentLikeDislikeCount(data) {
            var defer = Q.defer();

            //data = {qry_id : qry_id}

            var qry_id = data.qry_id || null;

            var select_like = 'select * from ' + comment_like_stats_table + ' where qry_id = ?';
            var select_args = [qry_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            cassconn.execute(select_like, select_args, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows || []);
            });
            return defer.promise;
        }
    }, {
        key: 'getCommentLikeDislikeCountByCmntID',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var qry_id, cmnt_id, select_like, select_args, queryOptions, res;
                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;

                                //data = {qry_id , cmnt_id}

                                qry_id = data.qry_id || null;
                                cmnt_id = data.cmnt_id || null;

                                if (!(!qry_id || !cmnt_id)) {
                                    _context6.next = 5;
                                    break;
                                }

                                return _context6.abrupt('return', { err: "qry_id and cmnt_id parameters required" });

                            case 5:
                                select_like = 'select * from ' + comment_like_stats_table + ' where qry_id = ? and cmnt_id = ?';
                                select_args = [qry_id, cmnt_id];
                                queryOptions = {
                                    prepare: true,
                                    consistency: cassandra.types.consistencies.quorum
                                };
                                _context6.next = 10;
                                return cassExecute(select_like, select_args, queryOptions);

                            case 10:
                                res = _context6.sent;
                                return _context6.abrupt('return', res);

                            case 14:
                                _context6.prev = 14;
                                _context6.t0 = _context6['catch'](0);
                                throw new Error(_context6.t0);

                            case 17:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 14]]);
            }));

            function getCommentLikeDislikeCountByCmntID(_x8) {
                return _ref6.apply(this, arguments);
            }

            return getCommentLikeDislikeCountByCmntID;
        }()
    }]);
    return Comments;
}();

module.exports = new Comments();