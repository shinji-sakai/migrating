'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:profile");
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var mailAPI = require(configPath.api.mail);
var chatFriendAPI = require(configPath.api.chat.friends);
var adminCourseAPI = require(configPath.api.admin.course);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var smsAPI = require(configPath.api.sms.sms);
var TimeUuid = cassandra.types.TimeUuid;
var adminSendNotificationAPI = require(configPath.api.admin.sendNotification);

var usr_shrt_dtls_table = "ew1.ew_usr_shrt_dtls";
var vt_user_friends = "ew1.vt_user_frs";
var vt_user_frs_read = "ew1.vt_user_frs_read";
var ew_rec_frs_table = "ew1.ew_rec_frs";
var ew_frs_req_table = "ew1.ew_fr_req_snt";
var ew_frs_req_rcvd_table = "ew1.ew_fr_req_rcvd";
var ew_cb_my_flw_table = "ew1.ew_cb_my_flw";
var ew_cb_usrs_i_flw_table = "ew1.ew_cb_usrs_i_flw";
var ew_usr_dtls_table = "ew1.ew_usr_dtls";
var ew_usr_contact_dtls_table = "ew1.ew_usr_contact_dtls";
var ew_usr_work_dtls_table = "ew1.ew_usr_work_dtls";
var ew_usr_edu_dtls_table = "ew1.ew_usr_edu_dtls";
var ew_usr_exm_dtls_table = "ew1.ew_usr_exm_dtls";
var ew_usr_crs_dtls_table = "ew1.ew_usr_crs_dtls";
var ew_usr_author_dtls_table = "ew1.ew_usr_author_dtl";
var ew_usr_tch_dtls_table = "ew1.ew_usr_tch_dtl";
var ew_usr_parent_kid_table = "ew1.ew_usr_parent_kid";

var ew_fr_req_noti_table = "ew1.ew_fr_req_noti";
var ew_pst_tag_noti_table = "ew1.ew_pst_tag_noti";

var ew_country_table = "ew1.ew_country";
var ew_country_city_table = "ew1.ew_country_city";

var ew_usr_email_table = "ew1.ew_usr_email";
var ew_usr_ph_table = "ew1.ew_usr_ph";

var FETCH_NOTIFICATION_LIMIT = 5;
var FETCH_FRIENDS_LIMIT = 5;

/**
 * Carrerbook API Class
 * @constructor
 */

var CareerBook = function () {
    function CareerBook() {
        (0, _classCallCheck3.default)(this, CareerBook);
    }

    (0, _createClass3.default)(CareerBook, [{
        key: 'saveProfilePic',

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - usr_id of the user who is uploading profile pic
         * @param {string} data.usr_pic - Path of the user's new profile pic
         * @returns {Promise}
         * @desc
         * - This method is used to save/update profile pic.
         * - We will also update profile pic in chatFriends table using {@link ChatFriends#updateUserProfilePic}
         * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
         */
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var self, usr_id, usr_pic, query, args;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                self = this;
                                usr_id = data.usr_id;
                                usr_pic = data.usr_pic;
                                query = "update " + usr_shrt_dtls_table + " set usr_pic=? where usr_id = ?";
                                args = [usr_pic, usr_id];
                                _context.next = 8;
                                return cassExecute(query, args);

                            case 8:
                                _context.next = 10;
                                return chatFriendAPI.updateUserProfilePic({
                                    usr_id: usr_id,
                                    usr_pic: usr_pic
                                });

                            case 10:
                                return _context.abrupt('return', data);

                            case 13:
                                _context.prev = 13;
                                _context.t0 = _context['catch'](0);

                                debug(_context.t0);
                                throw _context.t0;

                            case 17:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 13]]);
            }));

            function saveProfilePic(_x) {
                return _ref.apply(this, arguments);
            }

            return saveProfilePic;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - usr_id of the user who is updating the profile
         * @param {string} data.usr_pic - Path of the user's profile pic
         * @param {string} data.usr_email - Email of the user
         * @param {string} data.dsp_nm - Display name of the user
         * @returns {Promise}
         * @desc
         * - This method is used to save/update short details of the user
         * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
         */

    }, {
        key: 'saveShortDetails',
        value: function saveShortDetails(data) {
            var defer = Q.defer();
            var self = this;

            var usr_id = data.usr_id;
            var usr_email = data.usr_email;
            var dsp_nm = data.dsp_nm;
            var usr_pic = data.usr_pic;
            var usr_ph = data.usr_ph;

            //query
            var query = "update " + usr_shrt_dtls_table + " set usr_email=?,dsp_nm=?,usr_pic=?,usr_ph=? where usr_id = ?";
            var args = [usr_email, dsp_nm, usr_pic, usr_ph, usr_id];
            //cass execute query
            cassconn.execute(query, args, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - usr_id of the user who is updating the profile
         * @param {string} data.usr_role - user Role
         * @returns {Promise}
         * @desc
         * - This method is used to update role of the user
         * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
         */

    }, {
        key: 'updateUserRole',
        value: function updateUserRole(data) {
            var defer = Q.defer();
            var self = this;

            var usr_role = data.usr_role;
            var usr_id = data.usr_id;

            //query
            var query = "update " + usr_shrt_dtls_table + " set usr_role=? where usr_id = ?";
            var args = [usr_role, usr_id];
            //cass execute query
            cassconn.execute(query, args, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(data);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - usr_id of the user whose profile info we want to get
         * @param {string} data.usr_pic - Path of the user's profile pic
         * @param {string} data.usr_email - Email of the user
         * @param {string} data.dsp_nm - Display name of the user
         * @returns {Promise} - Promise will conain short details of the given user
         * @desc
         * - This method is used to get short details of the given user
         * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
         */

    }, {
        key: 'getUserShortDetails',
        value: function getUserShortDetails(data) {
            var defer = Q.defer();
            var self = this;

            var usr_id = data.usr_id;

            //query
            var query = "select * from " + usr_shrt_dtls_table + " where usr_id = ?";
            var args = [usr_id];
            //cass execute query
            cassconn.execute(query, args, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                //Row Object
                var row = res.rows.length > 0 && res.rows[0] || {};
                //Convert object to JSON
                var json_row = JSON.parse(JSON.stringify(row));
                defer.resolve(json_row);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string|Array} data.usr_ids - String/Array of userid
         * @returns {Promise} - Promise will conain short details of the given user(s)
         * @desc
         * - This method is used to get short details of the given user(s)
         * - Tables Used : {@link CassandraTables#ew_usr_shrt_dtls}
         */

    }, {
        key: 'getUsersShortDetails',
        value: function getUsersShortDetails(data) {

            //data = {usr_ids = ""/[]}
            var defer = Q.defer();
            var self = this;

            var usr_ids = [].concat(data.usr_ids);
            var str = '';
            usr_ids.forEach(function (v) {
                //join with ,(comma)
                str += '\'' + v + '\',';
            });

            str = '(' + str.substring(0, str.length - 1) + ')';

            //query
            var query = "select * from " + usr_shrt_dtls_table + " where usr_id in " + str;
            //cass execute query
            cassconn.execute(query, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var row = res.rows.length > 0 && res.rows || [];
                // Convert to JSON Object
                var json_row = JSON.parse(JSON.stringify(row));
                defer.resolve(json_row);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @returns {Promise} - Promise will conain short details all the users in system
         * @desc
         * - This method is used to get short details of all the user(s) in system
         * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
         */

    }, {
        key: 'getAllUsersShortDetails',
        value: function getAllUsersShortDetails(data) {

            var defer = Q.defer();
            var self = this;

            //query
            var query = "select * from " + usr_shrt_dtls_table;
            //cass execute query
            cassconn.execute(query, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var row = res.rows.length > 0 && res.rows || [];
                // Converting to JSON object
                var json_row = JSON.parse(JSON.stringify(row));
                defer.resolve(json_row);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User
         * @param {string} data.fr_id - User
         * @returns {Promise}
         * @desc
         * - Consider Two users A and B.Anyone can be usr_id and fr_id.We will add/update entry as follow.
         * - A will be added to friend list of B
         * - B will be added to friend list of A
         * - We will also add usr_id and fr_id to chat friends table using {@link ChatFriends#addFriend}
         * - Also update dashboard stats friend counter of usr_id and fr_id using {@link DashboardStats#incrementFriendCounter}
         * - Tables used : {@link CassandraTables#vt_user_frs}
         */

    }, {
        key: 'addConfirmedFriends',
        value: function addConfirmedFriends(data) {
            var defer = Q.defer();
            var self = this;

            var usr_id = data.usr_id; //A
            var fr_id = data.fr_id; //B

            //query
            // B will be added to friend list of A
            // var query = "update " + vt_user_friends + " set fr_id = fr_id + {'" + fr_id + "'} where usr_id = ?";
            // var params = [usr_id];
            var query = 'insert into ' + vt_user_friends + ' (fr_id,usr_id,fr_ts) values (?,?,?)';
            var params = [fr_id, usr_id, TimeUuid.now()];

            // A will be added to friend list of B
            // var query_rev = "update " + vt_user_friends + " set fr_id = fr_id + {'" + usr_id + "'} where usr_id = ?";
            // var params_rev = [fr_id];
            var query_rev = 'insert into ' + vt_user_friends + ' (fr_id,usr_id,fr_ts) values (?,?,?)';
            var params_rev = [usr_id, fr_id, TimeUuid.now()];

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_rev,
                params: params_rev
            }];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            //cass execute query
            cassconn.batch(queries, queryOptions, function () {
                var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(err, res, r) {
                    return _regenerator2.default.wrap(function _callee2$(_context2) {
                        while (1) {
                            switch (_context2.prev = _context2.next) {
                                case 0:
                                    if (!err) {
                                        _context2.next = 4;
                                        break;
                                    }

                                    debug(err);
                                    defer.reject(err);
                                    return _context2.abrupt('return');

                                case 4:
                                    _context2.next = 6;
                                    return chatFriendAPI.addFriend({
                                        user_id: usr_id,
                                        fr_id: fr_id
                                    });

                                case 6:
                                    _context2.next = 8;
                                    return chatFriendAPI.addFriend({
                                        user_id: fr_id,
                                        fr_id: usr_id
                                    });

                                case 8:
                                    _context2.prev = 8;

                                    debug("Updating dashboard stats counter for friends............");
                                    _context2.next = 12;
                                    return dashboardStatsAPI.incrementFriendCounter({
                                        usr_id: usr_id
                                    });

                                case 12:
                                    _context2.next = 14;
                                    return dashboardStatsAPI.incrementFriendCounter({
                                        usr_id: fr_id
                                    });

                                case 14:
                                    _context2.next = 19;
                                    break;

                                case 16:
                                    _context2.prev = 16;
                                    _context2.t0 = _context2['catch'](8);

                                    debug(_context2.t0);

                                case 19:

                                    defer.resolve({});

                                case 20:
                                case 'end':
                                    return _context2.stop();
                            }
                        }
                    }, _callee2, this, [[8, 16]]);
                }));

                return function (_x2, _x3, _x4) {
                    return _ref2.apply(this, arguments);
                };
            }());
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User
         * @param {string} data.fr_id - User
         * @returns {Promise}
         * @description
         * - Consider Two users A and B.Anyone can be usr_id and fr_id.We will add/update entry as follow.
         * - A will be removed from friend list of B
         * - B will be removed from friend list of A
         * - We will also update friend list of usr_id and fr_id in chat friends table using {@link ChatFriends#removeFriend}
         * - Also decrement dashboard stats friend counter of usr_id and fr_id using {@link DashboardStats#decrementFriendCounter}
         * - Tables used : {@link CassandraTables#vt_user_frs}
         */

    }, {
        key: 'removeFriend',
        value: function removeFriend(data) {
            var defer = Q.defer();
            var self = this;

            var usr_id = data.usr_id; //A
            var fr_id = data.fr_id; //B

            //query
            // B will be removed from friend list of A
            // var query = "update " + vt_user_friends + " set fr_id = fr_id - {'" + fr_id + "'} where usr_id = ?";
            // var params = [usr_id];
            var query = 'delete from ' + vt_user_friends + ' where fr_id = ? and usr_id = ?';
            var params = [fr_id, usr_id];

            // A will be removed from friend list of B
            // var query_rev = "update " + vt_user_friends + " set fr_id = fr_id - {'" + usr_id + "'} where usr_id = ?";
            // var params_rev = [fr_id];
            var query_rev = 'delete from ' + vt_user_friends + ' where fr_id = ? and usr_id = ?';
            var params_rev = [usr_id, fr_id];

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_rev,
                params: params_rev
            }];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            //cass execute query
            cassconn.batch(queries, queryOptions, function () {
                var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(err, res, r) {
                    return _regenerator2.default.wrap(function _callee3$(_context3) {
                        while (1) {
                            switch (_context3.prev = _context3.next) {
                                case 0:
                                    if (!err) {
                                        _context3.next = 4;
                                        break;
                                    }

                                    debug(err);
                                    defer.reject(err);
                                    return _context3.abrupt('return');

                                case 4:
                                    _context3.prev = 4;

                                    debug("Removing chat friends............");
                                    _context3.next = 8;
                                    return chatFriendAPI.removeFriend({
                                        user_id: usr_id,
                                        fr_id: fr_id
                                    });

                                case 8:
                                    _context3.next = 10;
                                    return chatFriendAPI.removeFriend({
                                        user_id: fr_id,
                                        fr_id: usr_id
                                    });

                                case 10:
                                    _context3.next = 15;
                                    break;

                                case 12:
                                    _context3.prev = 12;
                                    _context3.t0 = _context3['catch'](4);

                                    debug(_context3.t0);

                                case 15:
                                    _context3.prev = 15;

                                    debug("Updating dashboard stats counter for friends............");
                                    _context3.next = 19;
                                    return dashboardStatsAPI.decrementFriendCounter({
                                        usr_id: usr_id
                                    });

                                case 19:
                                    _context3.next = 21;
                                    return dashboardStatsAPI.decrementFriendCounter({
                                        usr_id: fr_id
                                    });

                                case 21:
                                    _context3.next = 26;
                                    break;

                                case 23:
                                    _context3.prev = 23;
                                    _context3.t1 = _context3['catch'](15);

                                    debug(_context3.t1);

                                case 26:

                                    defer.resolve({});

                                case 27:
                                case 'end':
                                    return _context3.stop();
                            }
                        }
                    }, _callee3, this, [[4, 12], [15, 23]]);
                }));

                return function (_x5, _x6, _x7) {
                    return _ref3.apply(this, arguments);
                };
            }());
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - usr_id of user whose friends we want to get
         * @returns {Promise}
         * @desc
         * - Get List of all friends(with their short details {@link CareerBook#getUsersShortDetails}) of given user
         * - Tables Used : {@link CassandraTables#vt_user_frs}
         */

    }, {
        key: 'getUserFriends',
        value: function getUserFriends(data) {

            var defer = Q.defer();
            var self = this;

            var usr_id = data.usr_id;
            var last_frnd_id = data.last_frnd_id;
            var query;
            var params;
            if (last_frnd_id) {
                // "select * from " + vt_user_friends + " where usr_id = ? "
                query = 'select * from ' + vt_user_frs_read + ' where usr_id = ? and fr_ts < ? limit ' + FETCH_FRIENDS_LIMIT;
                params = [usr_id, last_frnd_id];
            } else {
                query = 'select * from ' + vt_user_frs_read + ' where usr_id = ? limit ' + FETCH_FRIENDS_LIMIT;
                params = [usr_id];
            }

            //cass execute query
            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var row = res.rows.length > 0 && res.rows || [];
                //convert row to json object
                var json_row = JSON.parse(JSON.stringify(row));
                if (json_row.length > 0) {
                    //if user has friends
                    //then get user short details
                    self.getUsersShortDetails({
                        usr_ids: json_row[0].fr_id
                    }).then(function (friends) {
                        //send it to client
                        defer.resolve(friends);
                    }).catch(function (err) {
                        defer.reject(err);
                    });
                } else {
                    defer.resolve([]);
                }
            });
            return defer.promise;
        }
    }, {
        key: 'getUserFriendsAndFollowings',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var self, usr_id, last_frnd_id, query, params, rows, fr_ids, friends, query_fl, params_fl, fl_rows;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                self = this;

                                if (data.usr_id) {
                                    _context4.next = 4;
                                    break;
                                }

                                return _context4.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 4:
                                usr_id = data.usr_id;
                                last_frnd_id = data.last_frnd_id;


                                if (last_frnd_id) {
                                    // "select * from " + vt_user_friends + " where usr_id = ? "
                                    query = 'select * from ' + vt_user_frs_read + ' where usr_id = ? and fr_ts < ? limit ' + FETCH_FRIENDS_LIMIT;
                                    params = [usr_id, last_frnd_id];
                                } else {
                                    query = 'select * from ' + vt_user_frs_read + ' where usr_id = ? limit ' + FETCH_FRIENDS_LIMIT;
                                    params = [usr_id];
                                }

                                _context4.next = 9;
                                return cassExecute(query, params);

                            case 9:
                                rows = _context4.sent;

                                if (!(rows.length > 0)) {
                                    _context4.next = 30;
                                    break;
                                }

                                fr_ids = rows[0].fr_id;

                                if (fr_ids) {
                                    _context4.next = 14;
                                    break;
                                }

                                return _context4.abrupt('return', []);

                            case 14:
                                if (!(fr_ids && fr_ids.length <= 0)) {
                                    _context4.next = 16;
                                    break;
                                }

                                return _context4.abrupt('return', []);

                            case 16:
                                _context4.next = 18;
                                return self.getUsersShortDetails({
                                    usr_ids: fr_ids
                                });

                            case 18:
                                friends = _context4.sent;


                                //get all followings
                                query_fl = "select * from " + ew_cb_usrs_i_flw_table + " where usr_id = ? ";
                                params_fl = [usr_id];
                                _context4.next = 23;
                                return cassExecute(query_fl, params_fl);

                            case 23:
                                fl_rows = _context4.sent;


                                fl_rows = fl_rows || [];
                                if (fl_rows.length > 0) {
                                    fl_rows = fl_rows[0].flw_id || [];
                                }
                                friends = friends.map(function (v, i) {
                                    if (fl_rows.indexOf(v.usr_id) > -1) {
                                        v.isFollwing = true;
                                    }
                                    delete v["usr_email"];
                                    delete v["usr_ph"];
                                    delete v["usr_role"];
                                    return v;
                                });

                                return _context4.abrupt('return', friends);

                            case 30:
                                return _context4.abrupt('return', []);

                            case 31:
                                _context4.next = 37;
                                break;

                            case 33:
                                _context4.prev = 33;
                                _context4.t0 = _context4['catch'](0);

                                console.log(_context4.t0);
                                throw new Error(_context4.t0);

                            case 37:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 33]]);
            }));

            function getUserFriendsAndFollowings(_x8) {
                return _ref4.apply(this, arguments);
            }

            return getUserFriendsAndFollowings;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - usr_id of user whose recommended friends we want to get
         * @returns {Promise}
         * @desc
         * - Get recommended friends(with short details {@link CareerBook#getUsersShortDetails}) of given user
         * - Tables Used : {@link CassandraTables#ew_rec_frs}
         */

    }, {
        key: 'getRecommendFriendForUser',
        value: function getRecommendFriendForUser(data) {
            var defer = Q.defer();
            var self = this;
            var usr_id = data.usr_id;

            //query
            //get recommended friends
            var query = "select * from " + ew_rec_frs_table + " where usr_id = ? ";
            var params = [usr_id];

            //cass execute query
            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var jsonRows = JSON.parse(JSON.stringify(res.rows));
                //get recommended friends id list
                var fr_ids = jsonRows.map(function (v, i) {
                    return v.fr_id;
                });
                self.getUsersShortDetails({
                    usr_ids: fr_ids
                }).then(function (users) {
                    //users will be array of requested users

                    var frndMapping = {};
                    //generate user mapping from recommended friends list so we can direct fetch from dict
                    //Mapping : usr_id -> short details
                    jsonRows.map(function (v, i) {
                        frndMapping[v.fr_id] = v;
                    });

                    //go through all recommended friend and insert short details of the users
                    var allData = users.map(function (v, i) {
                        v['fr_req_snt'] = frndMapping[v.usr_id]['fr_req_snt'];
                        return v;
                    });

                    defer.resolve(allData);
                }).catch(function (err) {
                    defer.reject(err);
                });
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user who is the sender of the friend request
         * @param {string} data.fr_id - user who will receive the friend request
         * @returns {Promise}
         * @desc
         * - Consider two users A and B. When A click add friend to B.
         * - Insert Friend request notification for B {@link CareerBook#insertFriendRequestNotifications}
         * - Update recommendation table for A and change record to 'p'(Pending) state
         * - Update frnd request sent table for A and set request state to 'p'
         * - Update frnd request receive table for B and set request state to 'p'
         * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
         */

    }, {
        key: 'sendFriendRequest',
        value: function sendFriendRequest(data) {
            var defer = Q.defer();
            var self = this;
            var usr_id = data.usr_id; /// usr_id who sent the reqest
            var fr_id = data.fr_id; // usr_id who receive the request
            var req_dt = new Date();
            var req_sts = 'p'; // Pending

            //add notification to receiver
            //1st point in comment
            this.insertFriendRequestNotifications({
                frm_usr_id: usr_id,
                to_usr_id: fr_id,
                noti_vw_flg: "false"
            });

            //change recommend table
            //2nd point in comment
            var query_rec = "update " + ew_rec_frs_table + " set fr_req_snt = ? where usr_id = ? and fr_id = ? ";
            var params_rec = ['p', usr_id, fr_id];

            //query
            //3rd point in comment
            var query = "insert into " + ew_frs_req_table + " (usr_id,fr_id,req_dt,req_sts) values (?,?,?,?) ";
            var params = [usr_id, fr_id, req_dt, req_sts];

            var query_snt_reverse = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ?";
            var params_snt_reverse = [fr_id, usr_id];

            // receive table insert
            // 4th point in comment
            var query_rcv = "insert into " + ew_frs_req_rcvd_table + " (usr_id,fr_id,req_dt,req_sts) values (?,?,?,?) ";
            var params_rvc = [usr_id, fr_id, req_dt, req_sts];

            var query_rcv_reverse = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ?";
            var params_rcv_reverse = [fr_id, usr_id];

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_rcv,
                params: params_rvc
            }, {
                query: query_rec,
                params: params_rec
            }, {
                query: query_snt_reverse,
                params: params_snt_reverse
            }, {
                query: query_rcv_reverse,
                params: params_rcv_reverse
            }];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            //cass execute query
            cassconn.batch(queries, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user who accept the friend request
         * @param {string} data.fr_id - user who is sender of the friend request
         * @returns {Promise}
         * @desc
         * - Consider two users A and B.When B click confirm/accept friend to A.
         * - Update recommendation table for A and change record to 'f'(friends) state , {@link CassandraTables#ew_rec_frs}
         * - Update recommendation table for B and change record to 'f'(friends) state , {@link CassandraTables#ew_rec_frs}
         * - Update `frnd request sent table` for A and set request state to 'f' , {@link CassandraTables#ew_fr_req_snt}
         * - Update `frnd request receive table` for B and set request state to 'f' , {@link CassandraTables#ew_fr_req_rcvd}
         * - Add A to the follwer list of B , see {@link CareerBook#addFollower}
         * - Add B to the follwer list of A , see {@link CareerBook#addFollower}
         * - Add A and B as a confirmed friend {@link CareerBook#addConfirmedFriends}
         * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
         */

    }, {
        key: 'acceptFriendRequest',
        value: function acceptFriendRequest(data) {
            var defer = Q.defer();
            var self = this;
            var usr_id = data.usr_id;
            var fr_id = data.fr_id;
            var req_dt = new Date();
            var req_sts = 'f'; // Become Friends

            //change recommend table
            //1st point
            var query_rec = "update " + ew_rec_frs_table + " set fr_req_snt = ? where usr_id = ? and fr_id = ? ";
            var params_rec = ['f', usr_id, fr_id];

            //change recommend table
            //2nd point
            var query_reverse = "update " + ew_rec_frs_table + " set fr_req_snt = ? where usr_id = ? and fr_id = ? ";
            var params_reverse = ['f', fr_id, usr_id];

            //query
            //here A is sending Frnd req to B
            //so when B accept
            //B will be frnd in req sent table and A will be user
            //3rd point
            var query = "update " + ew_frs_req_table + " set req_dt = ?,req_sts = ? where usr_id = ? and fr_id = ? ";
            var params = [req_dt, req_sts, fr_id, usr_id];

            // receive table insert
            //here A is sending Frnd req to B
            //so when B accept
            //B will be frnd in req sent table and A will be user
            //4th point
            var query_rcv = "update " + ew_frs_req_rcvd_table + " set req_dt=? , req_sts = ? where usr_id = ? and fr_id = ? ";
            var params_rvc = [req_dt, req_sts, fr_id, usr_id];

            //also added both user as a follower
            //5th point
            self.addFollower({
                usr_id: usr_id,
                flw_id: fr_id
            });
            // 6th point
            self.addFollower({
                usr_id: fr_id,
                flw_id: usr_id
            });

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_rcv,
                params: params_rvc
            }, {
                query: query_rec,
                params: params_rec
            }, {
                query: query_reverse,
                params: params_reverse
            }];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            //cass execute query
            cassconn.batch(queries, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                // 7th point
                self.addConfirmedFriends({
                    usr_id: usr_id,
                    fr_id: fr_id
                });
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user who delete the friend request
         * @param {string} data.fr_id - user whose frnd request is being deleted
         * @returns {Promise}
         * @desc
         * - Consider two users A and B.When B is deleting frnd req of A.
         * - delete entry for A in recommendation table so he will not see B as a recommended frnd in future , {@link CassandraTables#ew_rec_frs}
         * - Update `frnd request sent table` for A and set request state to 'd' , {@link CassandraTables#ew_fr_req_snt}
         * - Update `frnd request receive table` for B and set request state to 'd' , {@link CassandraTables#ew_fr_req_rcvd}
         * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
         */

    }, {
        key: 'deleteFriendRequest',
        value: function deleteFriendRequest(data) {
            var defer = Q.defer();
            var self = this;
            var usr_id_from_argument = data.usr_id; //B is deleting requst of A so usr_id_from_argument = B
            var fr_id_from_argument = data.fr_id; //fr_id_from_argument = A
            var req_dt = new Date();
            var req_sts = 'd'; // deleted

            //now we want to take action for `A`(fr_id_from_argument) so in actual he will be usr_id in database
            var usr_id = fr_id_from_argument;
            var fr_id = usr_id_from_argument;

            //change recommend table
            //delete user and frnd combination so in future usr_id will not see fr_id in recommandation list
            //1st point
            var query_rec = "delete from " + ew_rec_frs_table + " where usr_id = ? and fr_id = ? ";
            var params_rec = [usr_id, fr_id];

            //query
            //here A is sending Frnd req to B  so in table : usr_id = A , fr_id = B , req_sts = pending(p)
            //so when B Delete
            //B will be frnd in req sent table and A will be user, so in table: usr_id = A , fr_id = B , req_sts = deleted(d)
            //2nd point
            var query = "update " + ew_frs_req_table + " set req_dt=?,req_sts = ? where usr_id = ? and fr_id = ? ";
            var params = [req_dt, req_sts, usr_id, fr_id];

            // receive table insert
            //here A is sending Frnd req to B,      usr_id = A , fr_id = B , req_sts = pending(p)
            //so when B accept
            //B will be frnd in req sent table and A will be user,  usr_id = A , fr_id = B , req_sts = deleted(d)
            //3rd point
            var query_rcv = "update " + ew_frs_req_rcvd_table + " set req_dt=? , req_sts = ? where usr_id = ? and fr_id = ? ";
            var params_rvc = [req_dt, req_sts, usr_id, fr_id];

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_rcv,
                params: params_rvc
            }, {
                query: query_rec,
                params: params_rec
            }];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            //cass execute query
            cassconn.batch(queries, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user who unfriend his friend
         * @param {string} data.fr_id - user who is going to be unfriend
         * @returns {Promise}
         * @desc
         * - Consider two users A and B. When B is unfriending A.
         * - Remove A and B from thier Friendlist , See {@link CareerBook#removeFriend}
         * - Remove A from follower list of B , See {@link CareerBook#removeFollower}
         * - Remove B from follower list of A , See {@link CareerBook#removeFollower}
         * - Remove B from Recommended friend list of A , {@link CassandraTables#ew_rec_frs}
         * - Update `frnd request sent table` for A and set request state to 'd' so in future A can't send request to B
         * - Delete entry for B(where B sent req to A,incase if any) `frnd request sent table` so in future B can send request to A
         * - Update `frnd request receive table` for A and set request state to 'd' so in future A can't send request to B
         * - Delete entry for B(where B sent req to A,incase if any) `frnd request receive table` so in future B can send request to A
         * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
         */

    }, {
        key: 'unfriendFriend',
        value: function unfriendFriend(data) {
            var defer = Q.defer();
            var self = this;

            var usr_id_from_argument = data.usr_id; // this usr_id has unfriened fr_id
            var fr_id_from_argument = data.fr_id;

            //1st step
            self.removeFriend({
                usr_id: usr_id_from_argument,
                fr_id: fr_id_from_argument
            });

            //also remove both user as a follower
            //2ndt step
            // self.removeFollower({
            //     usr_id: usr_id_from_argument,
            //     flw_id: fr_id_from_argument
            // });
            //3rd step
            // self.removeFollower({
            //     usr_id: fr_id_from_argument,
            //     flw_id: usr_id_from_argument
            // });

            //In future
            //fr_id can't see usr_id in people you may know
            //4th step
            var query_recommended = "delete from " + ew_rec_frs_table + " where usr_id = ? and fr_id = ?";
            var params_recommended = [fr_id_from_argument, usr_id_from_argument];

            //query
            //here A is unfriending B
            //In future , A can send friend request to B , but B can't
            //so in req sent table set B req status to deleted
            //5th step
            // var query_snt = "update " + ew_frs_req_table + " set req_sts = ?, req_dt=? where usr_id = ? and fr_id = ? ";
            // var params_snt_by_b = ['d',new Date(), fr_id_from_argument, usr_id_from_argument];

            //6th step
            var query_snt_dlt = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
            var params_snt_dlt = [usr_id_from_argument, fr_id_from_argument];

            var query_snt_dlt_b = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
            var params_snt_dlt_b = [fr_id_from_argument, usr_id_from_argument];

            // receive table insert
            //here A is unfriending B
            //In future , A can send friend request to B , but B can't
            //so in req rcv table set B req status to deleted
            //7th step
            // var query_rcv = "update " + ew_frs_req_rcvd_table + " set req_sts = ? , req_dt = ? where usr_id = ? and fr_id = ? ";
            // var params_rcv_by_a = ['d',new Date(), fr_id_from_argument, usr_id_from_argument];

            //8th step
            var query_rcv_dlt = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
            var params_rcv_dlt = [usr_id_from_argument, fr_id_from_argument];

            var query_rcv_dlt_b = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
            var params_rcv_dlt_b = [fr_id_from_argument, usr_id_from_argument];

            var queries = [{
                query: query_recommended,
                params: params_recommended
            }, {
                // query: query_snt,
                // params: params_snt_by_b
                query: query_snt_dlt_b,
                params: params_snt_dlt_b
            }, {
                query: query_snt_dlt,
                params: params_snt_dlt
            }, {
                // query: query_rcv,
                // params: params_rcv_by_a
                query: query_rcv_dlt_b,
                params: params_rcv_dlt_b
            }, {
                query: query_rcv_dlt,
                params: params_rcv_dlt
            }];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            //cass execute query
            cassconn.batch(queries, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }
    }, {
        key: 'cancelFriendRequest',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var usr_id_from_argument, fr_id_from_argument, query_snt_dlt, params_snt_dlt, query_rcv_dlt, params_rcv_dlt, queries;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;
                                usr_id_from_argument = data.usr_id;
                                fr_id_from_argument = data.fr_id;
                                query_snt_dlt = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
                                params_snt_dlt = [usr_id_from_argument, fr_id_from_argument];
                                query_rcv_dlt = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
                                params_rcv_dlt = [usr_id_from_argument, fr_id_from_argument];
                                queries = [{
                                    query: query_snt_dlt,
                                    params: params_snt_dlt
                                }, {
                                    query: query_rcv_dlt,
                                    params: params_rcv_dlt
                                }];
                                _context5.next = 10;
                                return cassBatch(queries);

                            case 10:
                                return _context5.abrupt('return', data);

                            case 13:
                                _context5.prev = 13;
                                _context5.t0 = _context5['catch'](0);
                                throw new Error(_context5.t0);

                            case 16:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 13]]);
            }));

            function cancelFriendRequest(_x9) {
                return _ref5.apply(this, arguments);
            }

            return cancelFriendRequest;
        }()
    }, {
        key: 'blockPerson',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var self, usr_id_from_argument, fr_id_from_argument, query_snt_dlt_reverse, params_snt_dlt_reverse, query_rcv_dlt_reverse, params_rcv_dlt_reverse, query_blck, params_blck, query_blck_rcv, params_blck_rcv, queries;
                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;
                                self = this;
                                usr_id_from_argument = data.usr_id;
                                fr_id_from_argument = data.fr_id;


                                self.removeFriend({
                                    usr_id: usr_id_from_argument,
                                    fr_id: fr_id_from_argument
                                });

                                //also remove both user as a follower
                                //2ndt step
                                self.removeFollower({
                                    usr_id: usr_id_from_argument,
                                    flw_id: fr_id_from_argument
                                });
                                //3rd step
                                self.removeFollower({
                                    usr_id: fr_id_from_argument,
                                    flw_id: usr_id_from_argument
                                });

                                query_snt_dlt_reverse = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
                                params_snt_dlt_reverse = [fr_id_from_argument, usr_id_from_argument];
                                query_rcv_dlt_reverse = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
                                params_rcv_dlt_reverse = [fr_id_from_argument, usr_id_from_argument];
                                query_blck = "update " + ew_frs_req_table + " set req_dt = ?,req_sts = ? where usr_id = ? and fr_id = ? ";
                                params_blck = [new Date(), 'b', usr_id_from_argument, fr_id_from_argument];
                                query_blck_rcv = "update " + ew_frs_req_rcvd_table + " set req_dt = ?,req_sts = ? where usr_id = ? and fr_id = ? ";
                                params_blck_rcv = [new Date(), 'b', usr_id_from_argument, fr_id_from_argument];
                                queries = [{
                                    query: query_snt_dlt_reverse,
                                    params: params_snt_dlt_reverse
                                }, {
                                    query: query_rcv_dlt_reverse,
                                    params: params_rcv_dlt_reverse
                                }, {
                                    query: query_blck,
                                    params: params_blck
                                }, {
                                    query: query_blck_rcv,
                                    params: params_blck_rcv
                                }];
                                _context6.next = 18;
                                return cassBatch(queries);

                            case 18:
                                return _context6.abrupt('return', data);

                            case 21:
                                _context6.prev = 21;
                                _context6.t0 = _context6['catch'](0);
                                throw new Error(_context6.t0);

                            case 24:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 21]]);
            }));

            function blockPerson(_x10) {
                return _ref6.apply(this, arguments);
            }

            return blockPerson;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user for whom we want to find all friend request
         * @returns {Promise}
         * @desc
         * - Get all friends request(with their short details {@link CareerBook#getUsersShortDetails}) which are 'p'(pending) for usr_id
         * - Tables Used : {@link CassandraTables#ew_fr_req_rcvd}
         */

    }, {
        key: 'getFriendRequest',
        value: function getFriendRequest(data) {
            var defer = Q.defer();
            var self = this;
            var usr_id = data.usr_id; // get friend request for this user id , so we will look for fr_id == this user

            //query
            var query = "select * from " + ew_frs_req_rcvd_table + " where fr_id = ?"; // reuest should be pending
            var params = [usr_id];

            //cass execute query
            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var jsonRows = JSON.parse(JSON.stringify(res.rows));
                //filter request which are pending
                jsonRows = jsonRows.filter(function (v, i) {
                    if (v.req_sts === 'p') return true;
                    return false;
                });
                var fr_ids = jsonRows.map(function (v, i) {
                    return v.usr_id;
                });
                self.getUsersShortDetails({
                    usr_ids: fr_ids
                }).then(function (res) {
                    defer.resolve(res);
                }).catch(function (err) {
                    defer.reject(err);
                });
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user
         * @param {string} data.fr_id - user
         * @returns {Promise}
         * @desc
         * - Two friends A and B.Here we will find friend status between them
         * - Check req sent table for usr_id, If request is not deleted then send it to client
         * - Then Check for req receive table for usr_id, If request is not deleted then send it to client
         * - if in both the table we have deleted request then send `req sent table record` to client
         * - Tables Used : {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
         */

    }, {
        key: 'getFriendStatus',
        value: function getFriendStatus(data) {
            var defer = Q.defer();
            var self = this;
            var usr_id = data.usr_id;
            var fr_id = data.fr_id;
            //query friend req sent table
            var query = "select * from " + ew_frs_req_table + " where usr_id = ?  and fr_id = ?";
            var params = [usr_id, fr_id];

            //query friend req receive table
            var query_rcv = "select * from " + ew_frs_req_rcvd_table + " where usr_id = ?  and fr_id = ?";
            var params_rcv = [fr_id, usr_id];

            //cass execute query
            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }

                var reqSentArray = JSON.parse(JSON.stringify(res.rows));
                if (reqSentArray.length > 0) {
                    if (reqSentArray[0]['req_sts'] === 'b' || reqSentArray[0]['req_sts'] === 'd') {
                        defer.resolve(reqSentArray[0]);
                        return;
                    } else if (reqSentArray[0]['req_sts'] !== 'd') {
                        reqSentArray[0]['req_action'] = 'send';
                        defer.resolve(reqSentArray[0]);
                        return;
                    }
                }

                cassconn.execute(query_rcv, params_rcv, function (err, res, r) {
                    if (err) {
                        debug(err);
                        defer.reject(err);
                        return;
                    }
                    var reqRcvArray = JSON.parse(JSON.stringify(res.rows));

                    if (reqRcvArray.length > 0) {
                        if (reqRcvArray[0]['req_sts'] === 'b') {
                            defer.resolve(reqRcvArray[0]);
                            return;
                        } else if (reqRcvArray[0]['req_sts'] !== 'd') {
                            reqRcvArray[0]['req_action'] = 'receive';
                            defer.resolve(reqRcvArray[0]);
                            return;
                        }
                        //if it is deleted rcv request then we will jump out if else
                        //and we will check whether both sent and recv req are deleted
                        // if deleted then we can't add as a friend
                    } else if (reqSentArray.length > 0) {
                        defer.resolve(reqSentArray[0]);
                        return;
                    }
                    if (reqRcvArray[0] && reqSentArray[0] && reqRcvArray[0]['req_sts'] === 'd' && reqSentArray[0]['req_sts'] === 'd') {
                        defer.resolve(reqSentArray[0]);
                        return;
                    }

                    defer.resolve({});
                });
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.flw_id - user who will be added to followers list array
         * @param {string} data.usr_id - user whose follower we are going to add
         * @returns {Promise}
         * @desc
         * - We will add Follower id to user_id's follower list
         * - Tables Used : {@link CassandraTables#ew_cb_my_flw}
         */

    }, {
        key: 'addFollower',
        value: function addFollower(data) {
            var defer = Q.defer();

            var flw_id = data.flw_id; // user who will be added to flw_id array
            var usr_id = data.usr_id; // user whose follower we are going to add
            var flw_dt = new Date();

            var query = "update " + ew_cb_my_flw_table + " set flw_id = flw_id + {'" + flw_id + "'},flw_dt=? where usr_id = ?";
            var params = [flw_dt, usr_id];

            var query_rev = "update " + ew_cb_usrs_i_flw_table + " set flw_id = flw_id + {'" + usr_id + "'},flw_dt=? where usr_id = ?";
            var params_rev = [flw_dt, flw_id];

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_rev,
                params: params_rev
            }];

            cassconn.batch(queries, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(data);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user whose follower we want to get
         * @returns {Promise}
         * @desc
         * - We will fetch all follower list from table for given user
         * - Tables Used : {@link CassandraTables#ew_cb_my_flw}
         */

    }, {
        key: 'getFollowers',
        value: function getFollowers(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id; // user whose follower we are going to get

            var query = "select * from  " + ew_cb_my_flw_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var jsonObj = JSON.parse(JSON.stringify(res.rows));
                defer.resolve(jsonObj);
            });
            return defer.promise;
        }
    }, {
        key: 'getFollowingUsers',
        value: function getFollowingUsers(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id; // user whose following we are going to get

            var query = "select * from  " + ew_cb_usrs_i_flw_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var jsonObj = JSON.parse(JSON.stringify(res.rows));
                defer.resolve(jsonObj);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user whose follower we want to get
         * @param {string} data.flw_id - user whom we want to remove from follower list
         * @returns {Promise}
         * @desc
         * - Remove given follower from follower list for given user
         * - Tables Used : {@link CassandraTables#ew_cb_my_flw}
         */

    }, {
        key: 'removeFollower',
        value: function removeFollower(data) {
            var defer = Q.defer();

            var flw_id = data.flw_id; // user who will be added to flw_id array
            var usr_id = data.usr_id; // user whose follower we are going to remove
            var flw_dt = new Date();

            var query = "update " + ew_cb_my_flw_table + " set flw_id = flw_id - {'" + flw_id + "'},flw_dt=? where usr_id = ?";
            var params = [flw_dt, usr_id];

            var query_rev = "update " + ew_cb_usrs_i_flw_table + " set flw_id = flw_id - {'" + usr_id + "'},flw_dt=? where usr_id = ?";
            var params_rev = [flw_dt, flw_id];

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_rev,
                params: params_rev
            }];

            cassconn.batch(queries, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user whose personal details we want to get
         * @returns {Promise}
         * @desc
         * - Get Personal Details of given user
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */

    }, {
        key: 'getUserPersonalDetails',
        value: function getUserPersonalDetails(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var query = "select * from " + ew_usr_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var jsonObj = JSON.parse(JSON.stringify(res.rows));
                defer.resolve(jsonObj);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} usr_id - User Id
         * @param {string} ctry - Country
         * @param {string} ctry_flg - Country restrition (Public , Only Me , Friends)
         * @param {string} cty - City
         * @param {string} cty_flg - City restrition (Public , Only Me , Friends)
         * @param {string} dob_dt - DOB
         * @param {string} dob_dt_flg - DOB restrition (Public , Only Me , Friends)
         * @param {string} dob_mn - DOB Month
         * @param {string} dob_mn_flg - DOB Month restrition (Public , Only Me , Friends)
         * @param {string} dob_yr - DOB Year
         * @param {string} dob_yr_flg - DOB Year restrition (Public , Only Me , Friends)
         * @param {string} dsp_nm - Display Name
         * @param {string} dsp_nm_flg - Display Name restrition (Public , Only Me , Friends)
         * @param {string} fst_nm - Firstname
         * @param {string} fst_nm_flg - Firstname restrition (Public , Only Me , Friends)
         * @param {string} ht_ctry - Hometown Country
         * @param {string} ht_ctry_flg - Hometown Country restrition (Public , Only Me , Friends)
         * @param {string} ht_cty - HomeTown City
         * @param {string} ht_cty_flg - HomeTown City restrition (Public , Only Me , Friends)
         * @param {string} ht_zip_cd - Hometown zip code
         * @param {string} ht_zip_cd_flg - Hometown zip code restrition (Public , Only Me , Friends)
         * @param {string} lst_nm - Last name
         * @param {string} lst_nm_flg - Last name restrition (Public , Only Me , Friends)
         * @param {string} role - Role
         * @param {string} usr_abt_me - about user
         * @param {string} usr_abt_me_flg - about user restrition (Public , Only Me , Friends)
         * @param {string} zip_cd - Zip Code
         * @param {string} zip_cd_flg - Zip Code restrition (Public , Only Me , Friends)
         * @returns {Promise}
         * @desc
         * - Save Personal Details of given user
         * - We will update user short details tables also if display name is changed {@link CareerBook#saveShortDetails}
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */

    }, {
        key: 'saveUserBasicPersonalDetails',
        value: function saveUserBasicPersonalDetails(data) {
            var defer = Q.defer();
            var self = this;

            var usr_id = data.usr_id;
            var update_str = "";
            var update_params = [];

            var columns = Object.keys(data || {});
            columns.forEach(function (v, i) {
                if (v !== 'usr_id' && data[v] && v !== 'email_nv' && v !== 'email' && v !== 'email_nv_permission' && v !== 'ph_nv' && v !== 'ph' && v !== 'ph_nv_permission') {
                    update_str += v + " = ?,";
                    update_params.push(data[v]);
                }
            });

            update_str = update_str.substr(0, update_str.length - 1);
            var query = "update " + ew_usr_dtls_table + " set " + update_str + " where usr_id = ?";
            var params = [].concat(update_params, usr_id);

            cassconn.execute(query, params, function () {
                var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(err, res, r) {
                    return _regenerator2.default.wrap(function _callee7$(_context7) {
                        while (1) {
                            switch (_context7.prev = _context7.next) {
                                case 0:
                                    if (!err) {
                                        _context7.next = 4;
                                        break;
                                    }

                                    debug(err);
                                    defer.reject(err);
                                    return _context7.abrupt('return');

                                case 4:
                                    if (!(data.usr_id && data.dsp_nm)) {
                                        _context7.next = 13;
                                        break;
                                    }

                                    _context7.prev = 5;
                                    _context7.next = 8;
                                    return self.saveShortDetails({
                                        usr_id: data.usr_id,
                                        dsp_nm: data.dsp_nm
                                    });

                                case 8:
                                    _context7.next = 13;
                                    break;

                                case 10:
                                    _context7.prev = 10;
                                    _context7.t0 = _context7['catch'](5);

                                    debug(_context7.t0);

                                case 13:

                                    defer.resolve({});

                                case 14:
                                case 'end':
                                    return _context7.stop();
                            }
                        }
                    }, _callee7, this, [[5, 10]]);
                }));

                return function (_x11, _x12, _x13) {
                    return _ref7.apply(this, arguments);
                };
            }());
            return defer.promise;
        }
    }, {
        key: 'sendMailForVerification',
        value: function () {
            var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(email, otp) {
                var obj;
                return _regenerator2.default.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                _context8.prev = 0;
                                obj = {
                                    to: email,
                                    subject: 'OTP For verifing Email address - ExamWarrior',
                                    text: 'Your OTP is ' + otp
                                };
                                _context8.next = 4;
                                return mailAPI.sendMail(obj);

                            case 4:
                                return _context8.abrupt('return', { email: email, otp: otp });

                            case 7:
                                _context8.prev = 7;
                                _context8.t0 = _context8['catch'](0);
                                throw new Error(_context8.t0);

                            case 10:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, this, [[0, 7]]);
            }));

            function sendMailForVerification(_x14, _x15) {
                return _ref8.apply(this, arguments);
            }

            return sendMailForVerification;
        }()
    }, {
        key: 'sendSMSForVerification',
        value: function () {
            var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(ph, otp) {
                return _regenerator2.default.wrap(function _callee9$(_context9) {
                    while (1) {
                        switch (_context9.prev = _context9.next) {
                            case 0:
                                _context9.prev = 0;
                                _context9.next = 3;
                                return smsAPI.sendSMS({
                                    message: 'From examwarrior.com, OTP for 1st time login: ' + otp,
                                    numbers: ph
                                });

                            case 3:
                                return _context9.abrupt('return', { ph: ph, otp: otp });

                            case 6:
                                _context9.prev = 6;
                                _context9.t0 = _context9['catch'](0);
                                throw new Error(_context9.t0);

                            case 9:
                            case 'end':
                                return _context9.stop();
                        }
                    }
                }, _callee9, this, [[0, 6]]);
            }));

            function sendSMSForVerification(_x16, _x17) {
                return _ref9.apply(this, arguments);
            }

            return sendSMSForVerification;
        }()
    }, {
        key: 'getUserEmails',
        value: function () {
            var _ref10 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(data) {
                var query, params, emails;
                return _regenerator2.default.wrap(function _callee10$(_context10) {
                    while (1) {
                        switch (_context10.prev = _context10.next) {
                            case 0:
                                _context10.prev = 0;

                                if (data.usr_id) {
                                    _context10.next = 3;
                                    break;
                                }

                                return _context10.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                query = "select * from " + ew_usr_email_table + " where usr_id = ?";
                                params = [data.usr_id];
                                _context10.next = 7;
                                return cassExecute(query, params);

                            case 7:
                                emails = _context10.sent;
                                return _context10.abrupt('return', emails);

                            case 11:
                                _context10.prev = 11;
                                _context10.t0 = _context10['catch'](0);
                                throw new Error(_context10.t0);

                            case 14:
                            case 'end':
                                return _context10.stop();
                        }
                    }
                }, _callee10, this, [[0, 11]]);
            }));

            function getUserEmails(_x18) {
                return _ref10.apply(this, arguments);
            }

            return getUserEmails;
        }()
    }, {
        key: 'getUserPhoneNumbers',
        value: function () {
            var _ref11 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11(data) {
                var query, params, phs;
                return _regenerator2.default.wrap(function _callee11$(_context11) {
                    while (1) {
                        switch (_context11.prev = _context11.next) {
                            case 0:
                                _context11.prev = 0;

                                if (data.usr_id) {
                                    _context11.next = 3;
                                    break;
                                }

                                return _context11.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                query = "select * from " + ew_usr_ph_table + " where usr_id = ?";
                                params = [data.usr_id];
                                _context11.next = 7;
                                return cassExecute(query, params);

                            case 7:
                                phs = _context11.sent;
                                return _context11.abrupt('return', phs);

                            case 11:
                                _context11.prev = 11;
                                _context11.t0 = _context11['catch'](0);
                                throw new Error(_context11.t0);

                            case 14:
                            case 'end':
                                return _context11.stop();
                        }
                    }
                }, _callee11, this, [[0, 11]]);
            }));

            function getUserPhoneNumbers(_x19) {
                return _ref11.apply(this, arguments);
            }

            return getUserPhoneNumbers;
        }()
    }, {
        key: 'addUserEmail',
        value: function () {
            var _ref12 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12(data) {
                var otp, otp_sent_dt, otp_status, email_flg, query, params;
                return _regenerator2.default.wrap(function _callee12$(_context12) {
                    while (1) {
                        switch (_context12.prev = _context12.next) {
                            case 0:
                                _context12.prev = 0;

                                if (data.usr_id) {
                                    _context12.next = 3;
                                    break;
                                }

                                return _context12.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.email) {
                                    _context12.next = 5;
                                    break;
                                }

                                return _context12.abrupt('return', {
                                    status: "error",
                                    message: "email field is required"
                                });

                            case 5:
                                otp = '' + Math.floor(1000 + Math.random() * 9000);
                                otp_sent_dt = new Date();
                                otp_status = 'n';
                                email_flg = data.email_flg || "public";
                                query = "insert into " + ew_usr_email_table + " (usr_id,email,email_flg,otp,otp_sent_dt,otp_status) values (?,?,?,?,?,?)";
                                params = [data.usr_id, data.email, email_flg, otp, otp_sent_dt, otp_status];
                                // save data to db

                                _context12.next = 13;
                                return cassExecute(query, params);

                            case 13:
                                return _context12.abrupt('return', data);

                            case 16:
                                _context12.prev = 16;
                                _context12.t0 = _context12['catch'](0);
                                throw new Error(_context12.t0);

                            case 19:
                            case 'end':
                                return _context12.stop();
                        }
                    }
                }, _callee12, this, [[0, 16]]);
            }));

            function addUserEmail(_x20) {
                return _ref12.apply(this, arguments);
            }

            return addUserEmail;
        }()
    }, {
        key: 'addUserPhoneNumber',
        value: function () {
            var _ref13 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee13(data) {
                var otp, otp_sent_dt, otp_status, ph_flg, query, params;
                return _regenerator2.default.wrap(function _callee13$(_context13) {
                    while (1) {
                        switch (_context13.prev = _context13.next) {
                            case 0:
                                _context13.prev = 0;

                                if (data.usr_id) {
                                    _context13.next = 3;
                                    break;
                                }

                                return _context13.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.ph) {
                                    _context13.next = 5;
                                    break;
                                }

                                return _context13.abrupt('return', {
                                    status: "error",
                                    message: "ph field is required"
                                });

                            case 5:
                                otp = '' + Math.floor(1000 + Math.random() * 9000);
                                otp_sent_dt = new Date();
                                otp_status = 'n';
                                ph_flg = data.ph_flg || "public";
                                query = "insert into " + ew_usr_ph_table + " (usr_id,ph,ph_flg,otp,otp_sent_dt,otp_status) values (?,?,?,?,?,?)";
                                params = [data.usr_id, data.ph, data.ph_flg, otp, otp_sent_dt, otp_status];
                                // save query

                                _context13.next = 13;
                                return cassExecute(query, params);

                            case 13:
                                return _context13.abrupt('return', data);

                            case 16:
                                _context13.prev = 16;
                                _context13.t0 = _context13['catch'](0);
                                throw new Error(_context13.t0);

                            case 19:
                            case 'end':
                                return _context13.stop();
                        }
                    }
                }, _callee13, this, [[0, 16]]);
            }));

            function addUserPhoneNumber(_x21) {
                return _ref13.apply(this, arguments);
            }

            return addUserPhoneNumber;
        }()
    }, {
        key: 'verifyUserEmail',
        value: function () {
            var _ref14 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee14(data) {
                var query, params, emailRes, updateRes;
                return _regenerator2.default.wrap(function _callee14$(_context14) {
                    while (1) {
                        switch (_context14.prev = _context14.next) {
                            case 0:
                                _context14.prev = 0;

                                if (data.usr_id) {
                                    _context14.next = 3;
                                    break;
                                }

                                return _context14.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.email) {
                                    _context14.next = 5;
                                    break;
                                }

                                return _context14.abrupt('return', {
                                    status: "error",
                                    message: "email field is required"
                                });

                            case 5:
                                if (data.otp) {
                                    _context14.next = 7;
                                    break;
                                }

                                return _context14.abrupt('return', {
                                    status: "error",
                                    message: "otp field is required"
                                });

                            case 7:
                                query = "select * from " + ew_usr_email_table + " where usr_id = ? and  email = ?";
                                params = [data.usr_id, data.email];
                                _context14.next = 11;
                                return cassExecute(query, params);

                            case 11:
                                emailRes = _context14.sent;

                                if (!(emailRes && emailRes[0])) {
                                    _context14.next = 24;
                                    break;
                                }

                                emailRes = emailRes[0];

                                if (!(emailRes.otp === data.otp)) {
                                    _context14.next = 23;
                                    break;
                                }

                                query = "update " + ew_usr_email_table + " set otp_status='y',otp_verify_dt=? where usr_id = ? and email = ?";
                                params = [new Date(), data.usr_id, data.email];
                                _context14.next = 19;
                                return cassExecute(query, params);

                            case 19:
                                updateRes = _context14.sent;
                                return _context14.abrupt('return', {
                                    status: "success",
                                    message: "Email is verified successfully",
                                    data: data
                                });

                            case 23:
                                return _context14.abrupt('return', {
                                    status: "error",
                                    message: "OTP is not correct"
                                });

                            case 24:
                                return _context14.abrupt('return', data);

                            case 27:
                                _context14.prev = 27;
                                _context14.t0 = _context14['catch'](0);
                                throw new Error(_context14.t0);

                            case 30:
                            case 'end':
                                return _context14.stop();
                        }
                    }
                }, _callee14, this, [[0, 27]]);
            }));

            function verifyUserEmail(_x22) {
                return _ref14.apply(this, arguments);
            }

            return verifyUserEmail;
        }()
    }, {
        key: 'verifyUserPhoneNumber',
        value: function () {
            var _ref15 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee15(data) {
                var query, params, phRes, updateRes;
                return _regenerator2.default.wrap(function _callee15$(_context15) {
                    while (1) {
                        switch (_context15.prev = _context15.next) {
                            case 0:
                                _context15.prev = 0;

                                if (data.usr_id) {
                                    _context15.next = 3;
                                    break;
                                }

                                return _context15.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.ph) {
                                    _context15.next = 5;
                                    break;
                                }

                                return _context15.abrupt('return', {
                                    status: "error",
                                    message: "ph field is required"
                                });

                            case 5:
                                if (data.otp) {
                                    _context15.next = 7;
                                    break;
                                }

                                return _context15.abrupt('return', {
                                    status: "error",
                                    message: "otp field is required"
                                });

                            case 7:
                                query = "select * from " + ew_usr_ph_table + " where usr_id = ? and  ph = ?";
                                params = [data.usr_id, data.ph];
                                _context15.next = 11;
                                return cassExecute(query, params);

                            case 11:
                                phRes = _context15.sent;

                                if (!(phRes && phRes[0])) {
                                    _context15.next = 24;
                                    break;
                                }

                                phRes = phRes[0];

                                if (!(phRes.otp === data.otp)) {
                                    _context15.next = 23;
                                    break;
                                }

                                query = "update " + ew_usr_ph_table + " set otp_status='y',otp_verify_dt=? where usr_id = ? and ph = ?";
                                params = [new Date(), data.usr_id, data.ph];
                                _context15.next = 19;
                                return cassExecute(query, params);

                            case 19:
                                updateRes = _context15.sent;
                                return _context15.abrupt('return', {
                                    status: "success",
                                    message: "Phone number is verified successfully",
                                    data: data
                                });

                            case 23:
                                return _context15.abrupt('return', {
                                    status: "error",
                                    message: "OTP is not correct"
                                });

                            case 24:
                                return _context15.abrupt('return', data);

                            case 27:
                                _context15.prev = 27;
                                _context15.t0 = _context15['catch'](0);
                                throw new Error(_context15.t0);

                            case 30:
                            case 'end':
                                return _context15.stop();
                        }
                    }
                }, _callee15, this, [[0, 27]]);
            }));

            function verifyUserPhoneNumber(_x23) {
                return _ref15.apply(this, arguments);
            }

            return verifyUserPhoneNumber;
        }()
    }, {
        key: 'deleteUserEmail',
        value: function () {
            var _ref16 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee16(data) {
                var query, params;
                return _regenerator2.default.wrap(function _callee16$(_context16) {
                    while (1) {
                        switch (_context16.prev = _context16.next) {
                            case 0:
                                _context16.prev = 0;

                                if (data.usr_id) {
                                    _context16.next = 3;
                                    break;
                                }

                                return _context16.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.email) {
                                    _context16.next = 5;
                                    break;
                                }

                                return _context16.abrupt('return', {
                                    status: "error",
                                    message: "email field is required"
                                });

                            case 5:
                                query = "delete from " + ew_usr_email_table + " where usr_id = ? and email = ?";
                                params = [data.usr_id, data.email];
                                // save data to db

                                _context16.next = 9;
                                return cassExecute(query, params);

                            case 9:
                                return _context16.abrupt('return', data);

                            case 12:
                                _context16.prev = 12;
                                _context16.t0 = _context16['catch'](0);
                                throw new Error(_context16.t0);

                            case 15:
                            case 'end':
                                return _context16.stop();
                        }
                    }
                }, _callee16, this, [[0, 12]]);
            }));

            function deleteUserEmail(_x24) {
                return _ref16.apply(this, arguments);
            }

            return deleteUserEmail;
        }()
    }, {
        key: 'deleteUserPhoneNumber',
        value: function () {
            var _ref17 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee17(data) {
                var query, params;
                return _regenerator2.default.wrap(function _callee17$(_context17) {
                    while (1) {
                        switch (_context17.prev = _context17.next) {
                            case 0:
                                _context17.prev = 0;

                                if (data.usr_id) {
                                    _context17.next = 3;
                                    break;
                                }

                                return _context17.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.ph) {
                                    _context17.next = 5;
                                    break;
                                }

                                return _context17.abrupt('return', {
                                    status: "error",
                                    message: "ph field is required"
                                });

                            case 5:
                                query = "delete from " + ew_usr_ph_table + " where usr_id = ? and ph = ?";
                                params = [data.usr_id, data.ph];
                                // save data to db

                                _context17.next = 9;
                                return cassExecute(query, params);

                            case 9:
                                return _context17.abrupt('return', data);

                            case 12:
                                _context17.prev = 12;
                                _context17.t0 = _context17['catch'](0);
                                throw new Error(_context17.t0);

                            case 15:
                            case 'end':
                                return _context17.stop();
                        }
                    }
                }, _callee17, this, [[0, 12]]);
            }));

            function deleteUserPhoneNumber(_x25) {
                return _ref17.apply(this, arguments);
            }

            return deleteUserPhoneNumber;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user id
         * @param {string} data.old_ph_nv - If user is updating existing phonenumber then this will be set to current old number
         * @param {string} data.ph_nv - new non verified Phone number
         * @param {string} data.ph_nv_permission - new number restriction (public, friends , only me)
         * @returns {Promise}
         * @desc
         * - If there is old number then we will first delete it
         * - Then update table with new number
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */

    }, {
        key: 'saveUserPhoneDetails',
        value: function () {
            var _ref18 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee18(data) {
                var defer, usr_id, update_str, queries, query_dlt, params_dlt, query_update, params_update;
                return _regenerator2.default.wrap(function _callee18$(_context18) {
                    while (1) {
                        switch (_context18.prev = _context18.next) {
                            case 0:
                                defer = Q.defer();
                                usr_id = data.usr_id;
                                update_str = "";
                                queries = [];


                                if (data.old_ph_nv) {
                                    //if old_ph_nv is set
                                    //then user is updating that phone number
                                    //so delete old one
                                    query_dlt = "delete ph_nv['" + data.old_ph_nv + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
                                    params_dlt = [usr_id];
                                }

                                //add new field to ph_nv
                                query_update = "update " + ew_usr_dtls_table + " set ph_nv['" + data.ph_nv + "']='" + data.ph_nv_permission + "' where usr_id = ?";
                                params_update = [usr_id];
                                _context18.prev = 7;

                                if (!query_dlt) {
                                    _context18.next = 11;
                                    break;
                                }

                                _context18.next = 11;
                                return cassExecute(query_dlt, params_dlt);

                            case 11:
                                _context18.next = 13;
                                return cassExecute(query_update, params_update);

                            case 13:
                                return _context18.abrupt('return', {});

                            case 16:
                                _context18.prev = 16;
                                _context18.t0 = _context18['catch'](7);
                                throw _context18.t0;

                            case 19:
                                return _context18.abrupt('return', defer.promise);

                            case 20:
                            case 'end':
                                return _context18.stop();
                        }
                    }
                }, _callee18, this, [[7, 16]]);
            }));

            function saveUserPhoneDetails(_x26) {
                return _ref18.apply(this, arguments);
            }

            return saveUserPhoneDetails;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user id
         * @param {string} data.old_email_nv - If user is updating existing email then this will be set to current old number
         * @param {string} data.email_nv - new non verified email
         * @param {string} data.email_nv_permission - new email restriction (public, friends , only me)
         * @returns {Promise}
         * @desc
         * - If there is old email then we will first delete it
         * - Then update table with new non verified email
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */

    }, {
        key: 'saveUserEmailDetails',
        value: function () {
            var _ref19 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee19(data) {
                var defer, usr_id, update_str, queries, query_dlt, params_dlt, query_update, params_update;
                return _regenerator2.default.wrap(function _callee19$(_context19) {
                    while (1) {
                        switch (_context19.prev = _context19.next) {
                            case 0:
                                defer = Q.defer();
                                usr_id = data.usr_id;
                                update_str = "";
                                queries = [];

                                if (data.old_email_nv) {
                                    //if old_email_nv is set
                                    //then user is updating that email
                                    //so delete old one
                                    query_dlt = "delete email_nv['" + data.old_email_nv + "'],email['" + data.old_email_nv + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
                                    params_dlt = [usr_id];
                                }

                                query_update = "update " + ew_usr_dtls_table + " set email_nv['" + data.email_nv + "']='" + data.email_nv_permission + "' where usr_id = ?";
                                params_update = [usr_id];
                                _context19.prev = 7;

                                if (!query_dlt) {
                                    _context19.next = 11;
                                    break;
                                }

                                _context19.next = 11;
                                return cassExecute(query_dlt, params_dlt);

                            case 11:
                                _context19.next = 13;
                                return cassExecute(query_update, params_update);

                            case 13:
                                return _context19.abrupt('return', {});

                            case 16:
                                _context19.prev = 16;
                                _context19.t0 = _context19['catch'](7);
                                throw _context19.t0;

                            case 19:
                            case 'end':
                                return _context19.stop();
                        }
                    }
                }, _callee19, this, [[7, 16]]);
            }));

            function saveUserEmailDetails(_x27) {
                return _ref19.apply(this, arguments);
            }

            return saveUserEmailDetails;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.to - Email Id which user want to verify
         * @returns {Promise}
         * @desc
         * - This function used to send OTP For verifing Email address
         * - this function will be called when user will click on verify email address client side
         * - user will be presented with textbox to enter otp code
         */

    }, {
        key: 'startVerifingEmailAddress',
        value: function () {
            var _ref20 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee20(data) {
                var query, params, emailRes;
                return _regenerator2.default.wrap(function _callee20$(_context20) {
                    while (1) {
                        switch (_context20.prev = _context20.next) {
                            case 0:
                                _context20.prev = 0;

                                if (data.usr_id) {
                                    _context20.next = 3;
                                    break;
                                }

                                return _context20.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.email) {
                                    _context20.next = 5;
                                    break;
                                }

                                return _context20.abrupt('return', {
                                    status: "error",
                                    message: "email field is required"
                                });

                            case 5:
                                query = "select * from " + ew_usr_email_table + " where usr_id = ? and  email = ?";
                                params = [data.usr_id, data.email];
                                _context20.next = 9;
                                return cassExecute(query, params);

                            case 9:
                                emailRes = _context20.sent;

                                if (emailRes && emailRes[0]) {
                                    emailRes = emailRes[0] || {};
                                    this.sendMailForVerification(data.email, emailRes.otp);
                                }
                                return _context20.abrupt('return', data);

                            case 14:
                                _context20.prev = 14;
                                _context20.t0 = _context20['catch'](0);
                                throw new Error(_context20.t0);

                            case 17:
                            case 'end':
                                return _context20.stop();
                        }
                    }
                }, _callee20, this, [[0, 14]]);
            }));

            function startVerifingEmailAddress(_x28) {
                return _ref20.apply(this, arguments);
            }

            return startVerifingEmailAddress;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.ph - Phone Number which user want to verify
         * @returns {Promise}
         * @desc
         * - This function used to send OTP SMS For verifing Phone number
         * - this function will be called when user will click on verify phonenumber client side
         * - user will be presented with textbox to enter otp code
         */

    }, {
        key: 'startVerifingPhoneNumber',
        value: function () {
            var _ref21 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee21(data) {
                var query, params, phRes;
                return _regenerator2.default.wrap(function _callee21$(_context21) {
                    while (1) {
                        switch (_context21.prev = _context21.next) {
                            case 0:
                                _context21.prev = 0;

                                if (data.usr_id) {
                                    _context21.next = 3;
                                    break;
                                }

                                return _context21.abrupt('return', {
                                    status: "error",
                                    message: "usr_id field is required"
                                });

                            case 3:
                                if (data.ph) {
                                    _context21.next = 5;
                                    break;
                                }

                                return _context21.abrupt('return', {
                                    status: "error",
                                    message: "ph field is required"
                                });

                            case 5:
                                query = "select * from " + ew_usr_ph_table + " where usr_id = ? and  ph = ?";
                                params = [data.usr_id, data.ph];
                                _context21.next = 9;
                                return cassExecute(query, params);

                            case 9:
                                phRes = _context21.sent;

                                if (phRes && phRes[0]) {
                                    phRes = phRes[0];
                                    this.sendSMSForVerification(data.ph, phRes.otp);
                                }
                                _context21.next = 16;
                                break;

                            case 13:
                                _context21.prev = 13;
                                _context21.t0 = _context21['catch'](0);
                                throw new Error(_context21.t0);

                            case 16:
                            case 'end':
                                return _context21.stop();
                        }
                    }
                }, _callee21, this, [[0, 13]]);
            }));

            function startVerifingPhoneNumber(_x29) {
                return _ref21.apply(this, arguments);
            }

            return startVerifingPhoneNumber;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.email - Email id which user has confirmed with otp
         * @param {string} data.permission - Email permission (public,only me , friends)
         * @returns {Promise}
         * @desc
         * - This function used to update user's verifed emails
         * - we will delete same email from non verifed list
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */

    }, {
        key: 'confirmVerificationOfEmail',
        value: function confirmVerificationOfEmail(data) {

            var defer = Q.defer();
            var queries = [];
            var usr_id = data.usr_id;
            var email = data.email;
            var email_flg = data.permission;

            var query_veri_add = "update " + ew_usr_dtls_table + " set email['" + email + "']='" + email_flg + "' where usr_id = ?";
            var query_veri_add_params = [usr_id];

            var query_dlt = "delete email_nv['" + email + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
            var params_dlt = [usr_id];

            queries.push({
                query: query_veri_add,
                params: query_veri_add_params
            });
            queries.push({
                query: query_dlt,
                params: params_dlt
            });

            cassconn.batch(queries, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.ph - Phone Number which user has confirmed with otp
         * @param {string} data.permission - Phone Number permission (public,only me , friends)
         * @returns {Promise}
         * @desc
         * - This function used to update user's verifed phone number
         * - we will delete same phone number from non verifed list
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */

    }, {
        key: 'confirmVerificationOfPhoneNumber',
        value: function confirmVerificationOfPhoneNumber(data) {

            var defer = Q.defer();
            var queries = [];
            var usr_id = data.usr_id;
            var ph = data.ph;
            var ph_flg = data.permission;

            var query_veri_add = "update " + ew_usr_dtls_table + " set ph['" + ph + "']='" + ph_flg + "' where usr_id = ?";
            var query_veri_add_params = [usr_id];

            var query_dlt = "delete ph_nv['" + ph + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
            var params_dlt = [usr_id];

            queries.push({
                query: query_veri_add,
                params: query_veri_add_params
            });
            queries.push({
                query: query_dlt,
                params: params_dlt
            });

            cassconn.batch(queries, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.email - Email id which user want to delete
         * @returns {Promise}
         * @desc
         * - This function used to delete given email from table
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */

    }, {
        key: 'deleteUserEmailAddress',
        value: function deleteUserEmailAddress(data) {
            var defer = Q.defer();
            var queries = [];
            var usr_id = data.usr_id;
            var email = data.email;

            var query_dlt = "delete email_nv['" + email + "'],email['" + email + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
            var params_dlt = [usr_id];

            cassconn.execute(query_dlt, params_dlt, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.ph - Phone number which user want to delete
         * @returns {Promise}
         * @desc
         * - This function used to delete given phone number from table
         * - Tables Used : {@link CassandraTables#ew_usr_dtls}
         */
        // deleteUserPhoneNumber(data) {
        //     var defer = Q.defer();
        //     var queries = [];
        //     var usr_id = data.usr_id;
        //     var ph = data.ph;

        //     var query_dlt = "delete ph_nv['" + ph + "'],ph['" + ph + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
        //     var params_dlt = [usr_id];

        //     cassconn.execute(query_dlt, params_dlt, function(err, res, r) {
        //         if (err) {
        //             debug(err);
        //             defer.reject(err);
        //             return;
        //         }
        //         defer.resolve({});
        //     });

        //     return defer.promise;
        // }

        //////////////////////////////////
        // Contact Details Form Helper //
        ////////////////////////////////


        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @returns {Promise}
         * @desc
         * - This function used to get contact details of given user
         * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
         */

    }, {
        key: 'getUserContactDetails',
        value: function getUserContactDetails(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            var query = "select * from " + ew_usr_contact_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.old_primary - Old Primary Email
         * @param {string} data.new_primary - New Primary Email
         * @returns {Promise}
         * @desc
         * - This function used to change primary email of given user
         * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
         */

    }, {
        key: 'changePrimaryEmailInContactDetails',
        value: function changePrimaryEmailInContactDetails(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            //this is prev primary email
            var old_primary = data.old_primary;
            //this will be the new primary email
            var new_primary = data.new_primary;

            var query = "update " + ew_usr_contact_dtls_table + " set usr_email['" + old_primary + "']='s',usr_email['" + new_primary + "']='p'" + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.email - Email to add
         * @param {string} data.isPrimary - indicate whether email is primary or not
         * @returns {Promise}
         * @desc
         * - This function is used to add primary/secondary email in contact details
         * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
         */

    }, {
        key: 'addEmailInContactDetails',
        value: function addEmailInContactDetails(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var primary = data.email;
            var isPrimary = data.isPrimary ? 'p' : 's';

            var query = "update " + ew_usr_contact_dtls_table + " set usr_email['" + primary + "']='" + isPrimary + "' " + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.email - Email which is going to be confirmed
         * @returns {Promise}
         * @desc
         * - First we will add email to verified email list
         * - then we will delete email from non verified emails
         * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
         */

    }, {
        key: 'confirmVerificationOfContactEmail',
        value: function confirmVerificationOfContactEmail(data) {
            var defer = Q.defer();
            var queries = [];

            var usr_id = data.usr_id;
            var email = data.email;

            var query_veri_add = "update " + ew_usr_contact_dtls_table + " set usr_email['" + email + "']='s' where usr_id = ?";
            var query_veri_add_params = [usr_id];

            var query_dlt = "delete usr_email_nv['" + email + "'] from " + ew_usr_contact_dtls_table + " where usr_id = ?";
            var params_dlt = [usr_id];

            queries.push({
                query: query_veri_add,
                params: query_veri_add_params
            });
            queries.push({
                query: query_dlt,
                params: params_dlt
            });

            cassconn.batch(queries, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.email - Email which is going to be added to non verfied email
         * @returns {Promise}
         * @desc
         * - we will add given email to non verified email list
         * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
         */

    }, {
        key: 'addNonVerifiedEmailInContactDetails',
        value: function addNonVerifiedEmailInContactDetails(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var primary = data.email;

            var query = "update " + ew_usr_contact_dtls_table + " set usr_email_nv['" + primary + "']='s' " + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.email - Email which is going to be delete
         * @returns {Promise}
         * @desc
         * - we will delete given email from verified and non verified email list
         * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
         */

    }, {
        key: 'deleteEmailInContactDetails',
        value: function deleteEmailInContactDetails(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var email = data.email;

            var query = "delete usr_email['" + email + "'],usr_email_nv['" + email + "'] from " + ew_usr_contact_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        //////////////////////
        // Work FOrm Helper //
        //////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_wrk_sk - unique id
         * @param {text} data.comp_desc - Company Desc
         * @param {text} data.comp_nm - Company Name
         * @param {text} data.frm_dt - From Date
         * @param {text} data.to_dt - To Date
         * @param {text} data.loc - Company Location
         * @param {text} data.role - Role in company,
         * @param {text} data.work_flg - (public,only me,friends)
         * @returns {Promise}
         * @desc
         * - This method will be used to save/update user work info
         * - Tables Used : {@link CassandraTables#ew_usr_work_dtls}
         */

    }, {
        key: 'saveWorkInfo',
        value: function saveWorkInfo(data) {
            var columns = ["city", "comp_desc", "comp_nm", "country", "frm_dt", "role", "to_dt", "work_flg", "zipcode"];
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_wrk_sk = data.usr_wrk_sk || Uuid.random();

            var keys = Object.keys(data || {});
            var update_str = "";
            var update_params = [];

            keys.forEach(function (v, i) {
                if (v !== 'usr_id' && v !== 'usr_wrk_sk') {
                    if (columns.indexOf(v) > -1) {
                        update_str += v + " = ?,";
                        update_params.push(data[v]);
                    }
                }
            });
            update_str = update_str.substr(0, update_str.length - 1);

            var query = "update " + ew_usr_work_dtls_table + " set " + update_str + " where usr_id = ? and usr_wrk_sk = ?";
            var params = [].concat(update_params, [usr_id, usr_wrk_sk]);

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @returns {Promise}
         * @desc
         * - This method will be used to get user work info of given user
         * - Tables Used : {@link CassandraTables#ew_usr_work_dtls}
         */

    }, {
        key: 'getWorkInfo',
        value: function getWorkInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            var query = "select * from " + ew_usr_work_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_wrk_sk
         * @returns {Promise}
         * @desc
         * - This method will be used to get user work info using usr_wrk_sk and usr_id
         * - Tables Used : {@link CassandraTables#ew_usr_work_dtls}
         */

    }, {
        key: 'deleteWorkInfo',
        value: function deleteWorkInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_wrk_sk = data.usr_wrk_sk;

            var query = "delete from " + ew_usr_work_dtls_table + " where usr_id = ? and usr_wrk_sk = ?";
            var params = [usr_id, usr_wrk_sk];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        //////////////////////
        // Edu FOrm Helper //
        //////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_wrk_sk - unique id
         * @param {text} data.edu_desc - Education Desc
         * @param {text} data.edu_lvl - Education level (primary school,clg,secondary school,etc...)
         * @param {text} data.frm_dt - From Date
         * @param {text} data.to_dt - To Date
         * @param {text} data.loc - School/Clg Location
         * @param {text} data.edu_nm - Clg/schl name
         * @param {text} data.edu_flg - (public,only me,friends)
         * @returns {Promise}
         * @desc
         * - This method will be used to save user edu info
         * - Tables Used : {@link CassandraTables#ew_usr_edu_dtls}
         */

    }, {
        key: 'saveEduInfo',
        value: function saveEduInfo(data) {
            var arr = ["city", "country", "edu_desc", "edu_flg", "edu_lvl", "edu_nm", "frm_dt", "to_dt", "zipcode"];
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_wrk_sk = data.usr_wrk_sk || Uuid.random();

            var keys = Object.keys(data || {});
            var update_str = "";
            var update_params = [];

            keys.forEach(function (v, i) {
                if (v !== 'usr_id' && v !== 'usr_wrk_sk') {
                    if (arr.indexOf(v) > -1) {
                        update_str += v + " = ?,";
                        update_params.push(data[v]);
                    }
                }
            });
            update_str = update_str.substr(0, update_str.length - 1);

            var query = "update " + ew_usr_edu_dtls_table + " set " + update_str + " where usr_id = ? and usr_wrk_sk = ?";
            var params = [].concat(update_params, [usr_id, usr_wrk_sk]);

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @returns {Promise}
         * @desc
         * - This method will be used to get user eduction info of given user
         * - Tables Used : {@link CassandraTables#ew_usr_edu_dtls}
         */

    }, {
        key: 'getEduInfo',
        value: function getEduInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            var query = "select * from " + ew_usr_edu_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.usr_wrk_sk - unique Id
         * @returns {Promise}
         * @desc
         * - This method will be used to delete user eduction info using usr_wrk_sk and usr_id
         * - Tables Used : {@link CassandraTables#ew_usr_edu_dtls}
         */

    }, {
        key: 'deleteEduInfo',
        value: function deleteEduInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_wrk_sk = data.usr_wrk_sk;

            var query = "delete from " + ew_usr_edu_dtls_table + " where usr_id = ? and usr_wrk_sk = ?";
            var params = [usr_id, usr_wrk_sk];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        ////////////////////////
        // Exam FOrm Helper ///
        //////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_exm_sk - unique id
         * @param {text} data.tgt_yr - Target Year
         * @param {text} data.exm_nm - Exam name
         * @param {text} data.exm_flg - (public,only me,friends)
         * @returns {Promise}
         * @desc
         * - This method will be used to save user edu info
         * - Tables Used : {@link CassandraTables#ew_usr_exm_dtls}
         */

    }, {
        key: 'saveExamInfo',
        value: function saveExamInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_exm_sk = data.usr_exm_sk || Uuid.random();
            var crt_dt = new Date();

            var keys = Object.keys(data || {});
            var update_str = "";
            var update_params = [];

            keys.forEach(function (v, i) {
                if (v !== 'usr_id' && v !== 'usr_exm_sk' && v !== 'crt_dt') {
                    update_str += v + " = ?,";
                    update_params.push(data[v]);
                }
            });
            update_str = update_str.substr(0, update_str.length - 1);

            var query = "update " + ew_usr_exm_dtls_table + " set " + update_str + ",crt_dt=? where usr_id = ? and usr_exm_sk = ?";
            var params = [].concat(update_params, [crt_dt, usr_id, usr_exm_sk]);

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @returns {Promise}
         * @desc
         * - This method will be used to get user exam info of given user
         * - Tables Used : {@link CassandraTables#ew_usr_exm_dtls}
         */

    }, {
        key: 'getExamInfo',
        value: function getExamInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            var query = "select * from " + ew_usr_exm_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_exm_sk - unique uuid
         * @returns {Promise}
         * @desc
         * - This method will be used to delete user exam info using usr_id and usr_exm_sk
         * - Tables Used : {@link CassandraTables#ew_usr_exm_dtls}
         */

    }, {
        key: 'deleteExamInfo',
        value: function deleteExamInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_exm_sk = data.usr_exm_sk;

            var query = "delete from " + ew_usr_exm_dtls_table + " where usr_id = ? and usr_exm_sk = ?";
            var params = [usr_id, usr_exm_sk];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /////////////////////////////////////
        // Interested Course Form Helper ///
        ///////////////////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_crs_sk - unique id
         * @param {text} data.crs_nm - Course name
         * @param {text} data.crs_flg - (public,only me,friends)
         * @returns {Promise}
         * @desc
         * - This method will be used to save user's interested course into {@link CassandraTables#ew_usr_crs_dtls}
         * - Tables Used : {@link CassandraTables#ew_usr_crs_dtls}
         */

    }, {
        key: 'saveInterestedCourseInfo',
        value: function saveInterestedCourseInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_crs_sk = data.usr_crs_sk || Uuid.random();
            var crt_dt = new Date();

            var keys = Object.keys(data || {});
            var update_str = "";
            var update_params = [];

            keys.forEach(function (v, i) {
                if (v !== 'usr_id' && v !== 'usr_crs_sk' && v !== 'crt_dt') {
                    update_str += v + " = ?,";
                    update_params.push(data[v]);
                }
            });
            update_str = update_str.substr(0, update_str.length - 1);

            var query = "update " + ew_usr_crs_dtls_table + " set " + update_str + ",crt_dt=? where usr_id = ? and usr_crs_sk = ?";
            var params = [].concat(update_params, [crt_dt, usr_id, usr_crs_sk]);

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @returns {Promise}
         * @desc
         * - This method will be used to get interested courses of given user
         * - Tables Used : {@link CassandraTables#ew_usr_crs_dtls}
         */

    }, {
        key: 'getInterestedCourseInfo',
        value: function getInterestedCourseInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            var query = "select * from " + ew_usr_crs_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_crs_sk - unique id
         * @returns {Promise}
         * @desc
         * - This method will be used to delete interested courses of given user using usr_crs_sk
         * - Tables Used : {@link CassandraTables#ew_usr_crs_dtls}
         */

    }, {
        key: 'deleteInterestedCourseInfo',
        value: function deleteInterestedCourseInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_crs_sk = data.usr_crs_sk;

            var query = "delete from " + ew_usr_crs_dtls_table + " where usr_id = ? and usr_crs_sk = ?";
            var params = [usr_id, usr_crs_sk];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        //////////////////////////////////
        // Author Profile Form Helper ///
        ////////////////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.usr_bk_sk - unique id
         * @param {text} data.author_desc - Author Desc
         * @param {text} data.crs_bk_desc - Course/Book Desc
         * @param {text} data.crs_bk_nm - Course/Book Name
         * @returns {Promise}
         * @desc
         * - This method will be used to save/update Author Info
         * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
         */

    }, {
        key: 'saveAuthorInfo',
        value: function saveAuthorInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_bk_sk = data.usr_bk_sk || Uuid.random();

            var keys = Object.keys(data || {});
            var update_str = "";
            var update_params = [];

            keys.forEach(function (v, i) {
                if (v !== 'usr_id' && v !== 'usr_bk_sk') {
                    update_str += v + " = ?,";
                    update_params.push(data[v]);
                }
            });
            update_str = update_str.substr(0, update_str.length - 1);

            var query = "update " + ew_usr_author_dtls_table + " set " + update_str + " where usr_id = ? and usr_bk_sk = ?";
            var params = [].concat(update_params, [usr_id, usr_bk_sk]);

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user/author Id
         * @returns {Promise}
         * @desc
         * - First we will fetch all courses given by author using {@link AdminCourse#getCoursesByAuthor}
         * - Then we will fetch info from {@link CassandraTables#ew_usr_author_dtl}
         * - Then merge those 2 results
         * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
         */

    }, {
        key: 'getAuthorInfo',
        value: function getAuthorInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            //first fetch courses that he has already given
            adminCourseAPI.getCoursesByAuthor({
                authorId: usr_id
            }).then(function (res) {
                //courses
                var temp_courses = res || [];

                var courses = temp_courses.map(function (v, i) {
                    var obj = {
                        editable: false,
                        crs_bk_nm: v.courseName,
                        crs_bk_desc: v.courseShortDesc || ''
                    };
                    return obj;
                });

                //fetch courses from author info details
                var query = "select * from " + ew_usr_author_dtls_table + " where usr_id = ?";
                var params = [usr_id];

                cassconn.execute(query, params, function (err, res, r) {
                    if (err) {
                        debug(err);
                        defer.reject(err);
                        return;
                    }
                    var resArr = JSON.parse(JSON.stringify(res.rows));
                    var finalArr = [].concat(courses, resArr);
                    defer.resolve(finalArr);
                });
            }).catch(function (err) {
                debug(err);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user/author Id
         * @param {uuid} data.usr_bk_sk - unique id
         * @returns {Promise}
         * @desc
         * - This method will be used to delete given author information using usr_id and usr_bk_sk
         * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
         */

    }, {
        key: 'deleteAuthorInfo',
        value: function deleteAuthorInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var usr_bk_sk = data.usr_bk_sk;

            var query = "delete from " + ew_usr_author_dtls_table + " where usr_id = ? and usr_bk_sk = ?";
            var params = [usr_id, usr_bk_sk];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user/author Id
         * @param {string} data.author_desc - Author Description
         * @returns {Promise}
         * @desc
         * - This method will be used to save/update author description using usr_id
         * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
         */

    }, {
        key: 'saveAuthorDescInfo',
        value: function saveAuthorDescInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var desc = data.author_desc;

            var query = "update " + ew_usr_author_dtls_table + " set author_desc=? where usr_id = ?";
            var params = [desc, usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        //////////////////////////////////
        // Teacher Profile Form Helper ///
        ////////////////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.tch_crs_sk - unique id
         * @param {text} data.tch_crs - Courses
         * @param {text} data.tch_desc - Desc
         * @param {text} data.edu_nm - Education Name
         * @returns {Promise}
         * @desc
         * - This method will be used to save/update Teacher Info
         * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
         */

    }, {
        key: 'saveTeacherInfo',
        value: function saveTeacherInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var tch_crs_sk = data.tch_crs_sk || Uuid.random();

            var keys = Object.keys(data || {});
            var update_str = "";
            var update_params = [];

            keys.forEach(function (v, i) {
                if (v !== 'usr_id' && v !== 'tch_crs_sk') {
                    update_str += v + " = ?,";
                    update_params.push(data[v]);
                }
            });
            update_str = update_str.substr(0, update_str.length - 1);

            var query = "update " + ew_usr_tch_dtls_table + " set " + update_str + " where usr_id = ? and tch_crs_sk = ?";
            var params = [].concat(update_params, [usr_id, tch_crs_sk]);

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @returns {Promise}
         * @desc
         * - This method will be used to get Teacher Info
         * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
         */

    }, {
        key: 'getTeacherInfo',
        value: function getTeacherInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            //fetch courses from author info details
            var query = "select * from " + ew_usr_tch_dtls_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {uuid} data.tch_crs_sk - unique Id
         * @returns {Promise}
         * @desc
         * - This method will be used to delete Teacher Info using usr_id and tch_crs_sk
         * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
         */

    }, {
        key: 'deleteTeacherInfo',
        value: function deleteTeacherInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var tch_crs_sk = data.tch_crs_sk;

            var query = "delete from " + ew_usr_tch_dtls_table + " where usr_id = ? and tch_crs_sk = ?";
            var params = [usr_id, tch_crs_sk];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.tch_desc - techer description
         * @returns {Promise}
         * @desc
         * - This method will be used to save/update teacher description
         * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
         */

    }, {
        key: 'saveTeacherDescInfo',
        value: function saveTeacherDescInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var desc = data.tch_desc;

            var query = "update " + ew_usr_tch_dtls_table + " set tch_desc=? where usr_id = ?";
            var params = [desc, usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        //////////////////////////////////
        // Kid info Form Helper /////////
        ////////////////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.kid_usr_id - kid user id
         * @param {text} data.kid_edu_nm - Kid Eduction name
         * @param {text} data.kid_exm_nm - Kid Exam Name
         * @returns {Promise}
         * @desc
         * - This method will be used to save/update Kid Info
         * - Tables Used : {@link CassandraTables#ew_usr_parent_kid}
         */

    }, {
        key: 'saveKidInfo',
        value: function saveKidInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var kid_usr_id = data.kid_usr_id;
            var kid_edu_nm = data.kid_edu_nm;
            var kid_exm_nm = data.kid_exm_nm;

            var query = "update " + ew_usr_parent_kid_table + " set kid_exm_nm = kid_exm_nm + {'" + kid_exm_nm + "'},kid_edu_nm=? where usr_id = ? and kid_usr_id = ?";
            var params = [kid_edu_nm, usr_id, kid_usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @returns {Promise}
         * @desc
         * - This method will be used to get Kid Info
         * - Tables Used : {@link CassandraTables#ew_usr_parent_kid}
         */

    }, {
        key: 'getKidInfo',
        value: function getKidInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;

            //fetch courses from author info details
            var query = "select * from " + ew_usr_parent_kid_table + " where usr_id = ?";
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - user Id
         * @param {string} data.kid_usr_id - kid user Id
         * @returns {Promise}
         * @desc
         * - This method will be used to delete get Kid Info
         * - Tables Used : {@link CassandraTables#ew_usr_parent_kid}
         */

    }, {
        key: 'deleteKidInfo',
        value: function deleteKidInfo(data) {
            var defer = Q.defer();

            var usr_id = data.usr_id;
            var kid_usr_id = data.kid_usr_id;

            var query = "delete from " + ew_usr_parent_kid_table + " where usr_id = ? and kid_usr_id = ?";
            var params = [usr_id, kid_usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        ///////////////////////////////////////
        // Friend Request notification table //
        ///////////////////////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.to_usr_id - user Id for which we want to get friend request notifications
         * @returns {Promise}
         * @desc
         * - This method will be used to get all friend request notifications for given user
         * - Then for all that notification users(user by whom this notificaitions are generated ) we will get short details using {@link CareerBook#getUsersShortDetails}
         * - Them Merge the results
         * - Tables Used : {@link CassandraTables#ew_fr_req_noti}
         */

    }, {
        key: 'getFriendRequestNotifications',
        value: function () {
            var _ref22 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee22(data) {
                var to_usr_id, query, params, rows, frm_usr_ids, users, userMapping, allNotifications;
                return _regenerator2.default.wrap(function _callee22$(_context22) {
                    while (1) {
                        switch (_context22.prev = _context22.next) {
                            case 0:
                                _context22.prev = 0;

                                if (data.to_usr_id) {
                                    _context22.next = 3;
                                    break;
                                }

                                return _context22.abrupt('return', {
                                    status: "error",
                                    message: "to_usr_id field is required"
                                });

                            case 3:
                                to_usr_id = data.to_usr_id; // Usr_id who received the request

                                if (data.last_notification) {
                                    query = 'select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,frm_usr_id,noti_msg,noti_vw_flg from ' + ew_fr_req_noti_table + ' where to_usr_id=? and snt_ts < ? limit ' + FETCH_NOTIFICATION_LIMIT;
                                    params = [to_usr_id, data.last_notification];
                                } else {
                                    query = 'select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,frm_usr_id,noti_msg,noti_vw_flg from ' + ew_fr_req_noti_table + ' where to_usr_id=? limit ' + FETCH_NOTIFICATION_LIMIT;
                                    params = [to_usr_id];
                                }

                                _context22.next = 7;
                                return cassExecute(query, params);

                            case 7:
                                rows = _context22.sent;
                                frm_usr_ids = rows.map(function (v, i) {
                                    return v.frm_usr_id;
                                });

                                if (!(frm_usr_ids.length > 0)) {
                                    _context22.next = 17;
                                    break;
                                }

                                _context22.next = 12;
                                return this.getUsersShortDetails({
                                    usr_ids: frm_usr_ids
                                });

                            case 12:
                                users = _context22.sent;
                                userMapping = {};
                                //generate user mapping so we can direct fetch from dict

                                users.map(function (v, i) {
                                    delete v.usr_role;
                                    delete v.usr_email;
                                    delete v.usr_ph;
                                    userMapping[v.usr_id] = v;
                                });

                                allNotifications = rows.map(function (v, i) {
                                    //get sender
                                    var frm_usr_id = v['frm_usr_id'];
                                    //get user info
                                    var user = userMapping[frm_usr_id];
                                    //attach user info to item
                                    v['usr_info'] = user;
                                    return v;
                                });
                                return _context22.abrupt('return', allNotifications);

                            case 17:
                                return _context22.abrupt('return', rows);

                            case 20:
                                _context22.prev = 20;
                                _context22.t0 = _context22['catch'](0);
                                throw new Error(_context22.t0);

                            case 23:
                            case 'end':
                                return _context22.stop();
                        }
                    }
                }, _callee22, this, [[0, 20]]);
            }));

            function getFriendRequestNotifications(_x30) {
                return _ref22.apply(this, arguments);
            }

            return getFriendRequestNotifications;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.frm_usr_id - user Id who sent the request
         * @param {string} data.to_usr_id - user Id who will receive the frnd request
         * @param {string} data.noti_vw_flg - notification viewed or not
         * @returns {Promise}
         * @desc
         * - This method will be used to update frnd request notification for given user
         * - Tables Used : {@link CassandraTables#ew_fr_req_noti}
         */

    }, {
        key: 'updateFriendRequestNotifications',
        value: function () {
            var _ref23 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee23(data) {
                var frm_usr_id, to_usr_id, noti_vw_flg, snt_ts, query, params;
                return _regenerator2.default.wrap(function _callee23$(_context23) {
                    while (1) {
                        switch (_context23.prev = _context23.next) {
                            case 0:
                                _context23.prev = 0;

                                if (data.frm_usr_id) {
                                    _context23.next = 3;
                                    break;
                                }

                                return _context23.abrupt('return', {
                                    status: "error",
                                    message: "frm_usr_id field is required"
                                });

                            case 3:
                                if (data.to_usr_id) {
                                    _context23.next = 5;
                                    break;
                                }

                                return _context23.abrupt('return', {
                                    status: "error",
                                    message: "to_usr_id field is required"
                                });

                            case 5:
                                if (data.snt_ts) {
                                    _context23.next = 7;
                                    break;
                                }

                                return _context23.abrupt('return', {
                                    status: "error",
                                    message: "snt_ts field is required"
                                });

                            case 7:
                                frm_usr_id = data.frm_usr_id; // Usr_id who sent the request

                                to_usr_id = data.to_usr_id; // Usr_id who received the request

                                noti_vw_flg = data.noti_vw_flg;
                                snt_ts = data.snt_ts;
                                query = 'update ' + ew_fr_req_noti_table + ' set noti_vw_flg=? where frm_usr_id=? and to_usr_id=? and snt_ts = ?';
                                params = [noti_vw_flg, frm_usr_id, to_usr_id, snt_ts];
                                _context23.next = 15;
                                return cassExecute(query, params);

                            case 15:
                                if (!(noti_vw_flg === 'true')) {
                                    _context23.next = 18;
                                    break;
                                }

                                _context23.next = 18;
                                return adminSendNotificationAPI.decrementSendNotificationCount({
                                    usr_id: to_usr_id,
                                    type: 'cb'
                                });

                            case 18:
                                return _context23.abrupt('return', data);

                            case 21:
                                _context23.prev = 21;
                                _context23.t0 = _context23['catch'](0);
                                throw new Error(_context23.t0);

                            case 24:
                            case 'end':
                                return _context23.stop();
                        }
                    }
                }, _callee23, this, [[0, 21]]);
            }));

            function updateFriendRequestNotifications(_x31) {
                return _ref23.apply(this, arguments);
            }

            return updateFriendRequestNotifications;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.frm_usr_id - user Id who sent the request
         * @param {string} data.to_usr_id - user Id who will receive the frnd request
         * @param {string} data.noti_vw_flg - notification viewed or not
         * @returns {Promise}
         * @desc
         * - This method will be used to insert new frnd request notification for given user
         * - Tables Used : {@link CassandraTables#ew_fr_req_noti}
         */

    }, {
        key: 'insertFriendRequestNotifications',
        value: function () {
            var _ref24 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee24(data) {
                var frm_usr_id, to_usr_id, noti_msg, noti_vw_flg, snt_ts, query, params;
                return _regenerator2.default.wrap(function _callee24$(_context24) {
                    while (1) {
                        switch (_context24.prev = _context24.next) {
                            case 0:
                                _context24.prev = 0;

                                if (data.frm_usr_id) {
                                    _context24.next = 3;
                                    break;
                                }

                                return _context24.abrupt('return', {
                                    status: "error",
                                    message: "frm_usr_id field is required"
                                });

                            case 3:
                                if (data.to_usr_id) {
                                    _context24.next = 5;
                                    break;
                                }

                                return _context24.abrupt('return', {
                                    status: "error",
                                    message: "to_usr_id field is required"
                                });

                            case 5:
                                frm_usr_id = data.frm_usr_id; // Usr_id who sent the request

                                to_usr_id = data.to_usr_id; // Usr_id who received the request

                                noti_msg = "sent friend request.";
                                noti_vw_flg = data.noti_vw_flg || "false";
                                snt_ts = TimeUuid.now();
                                query = 'insert into ' + ew_fr_req_noti_table + ' (noti_msg,noti_vw_flg,frm_usr_id,to_usr_id,snt_ts) values (?,?,?,?,?) ';
                                params = [noti_msg, noti_vw_flg, frm_usr_id, to_usr_id, snt_ts];
                                _context24.next = 14;
                                return cassExecute(query, params);

                            case 14:
                                _context24.next = 16;
                                return adminSendNotificationAPI.incrementSendNotificationCount({
                                    usr_id: to_usr_id,
                                    type: 'cb'
                                });

                            case 16:
                                return _context24.abrupt('return', data);

                            case 19:
                                _context24.prev = 19;
                                _context24.t0 = _context24['catch'](0);
                                throw new Error(_context24.t0);

                            case 22:
                            case 'end':
                                return _context24.stop();
                        }
                    }
                }, _callee24, this, [[0, 19]]);
            }));

            function insertFriendRequestNotifications(_x32) {
                return _ref24.apply(this, arguments);
            }

            return insertFriendRequestNotifications;
        }()

        ///////////////////////////////////////
        // Tagged Friends notification table //
        ///////////////////////////////////////

        /**
         * @param {JSON} data
         * @param {string} data.to_usr_id - user Id for which we want to get tagged frnds notifications
         * @returns {Promise}
         * @desc
         * - This method will be used to get all tagged notifications for given user
         * - Then for all that notification users(user by whom this notificaitions are generated), we will get short details using {@link CareerBook#getUsersShortDetails}
         * - Them Merge the results
         * - Tables Used : {@link CassandraTables#ew_pst_tag_noti}
         */

    }, {
        key: 'getTaggedFriendsNotifications',
        value: function () {
            var _ref25 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee25(data) {
                var to_usr_id, query, params, jsonArr, frm_usr_ids, users, userMapping, allNotifications;
                return _regenerator2.default.wrap(function _callee25$(_context25) {
                    while (1) {
                        switch (_context25.prev = _context25.next) {
                            case 0:
                                _context25.prev = 0;

                                if (data.to_usr_id) {
                                    _context25.next = 3;
                                    break;
                                }

                                return _context25.abrupt('return', {
                                    status: "error",
                                    message: "to_usr_id field is required"
                                });

                            case 3:
                                to_usr_id = data.to_usr_id; // Usr_id who received the tag frnd noti

                                if (data.last_notification) {
                                    query = 'select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,pst_id,frm_usr_id,noti_msg,noti_vw_flg from ' + ew_pst_tag_noti_table + ' where to_usr_id=? and snt_ts < ? limit ' + FETCH_NOTIFICATION_LIMIT;
                                    params = [to_usr_id, data.last_notification];
                                } else {
                                    query = 'select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,pst_id,frm_usr_id,noti_msg,noti_vw_flg from ' + ew_pst_tag_noti_table + ' where to_usr_id=? limit ' + FETCH_NOTIFICATION_LIMIT;
                                    params = [to_usr_id];
                                }

                                _context25.next = 7;
                                return cassExecute(query, params);

                            case 7:
                                jsonArr = _context25.sent;
                                frm_usr_ids = jsonArr.map(function (v, i) {
                                    return v.frm_usr_id;
                                });

                                if (!(frm_usr_ids.length > 0)) {
                                    _context25.next = 17;
                                    break;
                                }

                                _context25.next = 12;
                                return this.getUsersShortDetails({
                                    usr_ids: frm_usr_ids
                                });

                            case 12:
                                users = _context25.sent;
                                userMapping = {};
                                //generate user mapping so we can direct fetch from dict

                                users.map(function (v, i) {
                                    delete v.usr_role;
                                    delete v.usr_email;
                                    delete v.usr_ph;
                                    userMapping[v.usr_id] = v;
                                });
                                allNotifications = jsonArr.map(function (v, i) {
                                    //get sender
                                    var frm_usr_id = v['frm_usr_id'];
                                    //get user info
                                    var user = userMapping[frm_usr_id];
                                    //attach user info to item
                                    v['usr_info'] = user;
                                    return v;
                                });
                                return _context25.abrupt('return', allNotifications);

                            case 17:
                                return _context25.abrupt('return', jsonArr);

                            case 20:
                                _context25.prev = 20;
                                _context25.t0 = _context25['catch'](0);
                                throw new Error(_context25.t0);

                            case 23:
                            case 'end':
                                return _context25.stop();
                        }
                    }
                }, _callee25, this, [[0, 20]]);
            }));

            function getTaggedFriendsNotifications(_x33) {
                return _ref25.apply(this, arguments);
            }

            return getTaggedFriendsNotifications;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.frm_usr_id - user Id who tag the frnd
         * @param {string} data.to_usr_id - user Id who is being tagged
         * @param {string} data.noti_vw_flg - notification viewed or not
         * @returns {Promise}
         * @desc
         * - This function is used to update tagged notifications for given user and frnd_id
         * - Tables Used : {@link CassandraTables#ew_pst_tag_noti}
         */

    }, {
        key: 'updateTaggedFriendNotifications',
        value: function () {
            var _ref26 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee26(data) {
                var frm_usr_id, to_usr_id, noti_vw_flg, pst_id, snt_ts, query, params;
                return _regenerator2.default.wrap(function _callee26$(_context26) {
                    while (1) {
                        switch (_context26.prev = _context26.next) {
                            case 0:
                                _context26.prev = 0;

                                if (data.frm_usr_id) {
                                    _context26.next = 3;
                                    break;
                                }

                                return _context26.abrupt('return', {
                                    status: "error",
                                    message: "frm_usr_id field is required"
                                });

                            case 3:
                                if (data.to_usr_id) {
                                    _context26.next = 5;
                                    break;
                                }

                                return _context26.abrupt('return', {
                                    status: "error",
                                    message: "to_usr_id field is required"
                                });

                            case 5:
                                if (data.pst_id) {
                                    _context26.next = 7;
                                    break;
                                }

                                return _context26.abrupt('return', {
                                    status: "error",
                                    message: "pst_id field is required"
                                });

                            case 7:
                                if (data.snt_ts) {
                                    _context26.next = 9;
                                    break;
                                }

                                return _context26.abrupt('return', {
                                    status: "error",
                                    message: "snt_ts field is required"
                                });

                            case 9:
                                frm_usr_id = data.frm_usr_id; // Usr_id who tag the frnd

                                to_usr_id = data.to_usr_id; // Usr_id who is being tagged

                                noti_vw_flg = data.noti_vw_flg;
                                pst_id = data.pst_id;
                                snt_ts = data.snt_ts;
                                query = 'update ' + ew_pst_tag_noti_table + ' set noti_vw_flg=? where frm_usr_id=? and to_usr_id=? and pst_id = ? and snt_ts = ?';
                                params = [noti_vw_flg, frm_usr_id, to_usr_id, pst_id, snt_ts];
                                _context26.next = 18;
                                return cassExecute(query, params);

                            case 18:
                                if (!(noti_vw_flg === "true")) {
                                    _context26.next = 21;
                                    break;
                                }

                                _context26.next = 21;
                                return adminSendNotificationAPI.decrementSendNotificationCount({
                                    usr_id: to_usr_id,
                                    type: 'cb'
                                });

                            case 21:
                                return _context26.abrupt('return', data);

                            case 24:
                                _context26.prev = 24;
                                _context26.t0 = _context26['catch'](0);
                                throw new Error(_context26.t0);

                            case 27:
                            case 'end':
                                return _context26.stop();
                        }
                    }
                }, _callee26, this, [[0, 24]]);
            }));

            function updateTaggedFriendNotifications(_x34) {
                return _ref26.apply(this, arguments);
            }

            return updateTaggedFriendNotifications;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.frm_usr_id - user Id who tag the frnd
         * @param {string} data.to_usr_ids - all Usr_id who received the tag frnd notification
         * @param {string} data.noti_vw_flg - notification viewed or not
         * @param {string} data.pst_id - post id in which frnds are tagged
         * @returns {Promise}
         * @desc
         * - This function is used to insert new tagged notifications for all tagged user(s)
         * - Tables Used : {@link CassandraTables#ew_pst_tag_noti}
         */

    }, {
        key: 'insertTaggedFriendNotifications',
        value: function () {
            var _ref27 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee27(data) {
                var frm_usr_id, to_usr_ids, noti_msg, noti_vw_flg, pst_id, snt_ts, queries, i, usr_id;
                return _regenerator2.default.wrap(function _callee27$(_context27) {
                    while (1) {
                        switch (_context27.prev = _context27.next) {
                            case 0:
                                _context27.prev = 0;

                                if (data.frm_usr_id) {
                                    _context27.next = 3;
                                    break;
                                }

                                return _context27.abrupt('return', {
                                    status: "error",
                                    message: "frm_usr_id field is required"
                                });

                            case 3:
                                if (data.to_usr_ids) {
                                    _context27.next = 5;
                                    break;
                                }

                                return _context27.abrupt('return', {
                                    status: "error",
                                    message: "to_usr_ids field is required"
                                });

                            case 5:
                                if (!(to_usr_ids && to_usr_ids.length <= 0)) {
                                    _context27.next = 7;
                                    break;
                                }

                                return _context27.abrupt('return', {
                                    status: "error",
                                    message: "Atleast one usr_id required in to_usr_ids field"
                                });

                            case 7:
                                frm_usr_id = data.frm_usr_id; // Usr_id who sent the tag frnd notification

                                to_usr_ids = data.to_usr_ids; // all Usr_id who received the tag frnd notification

                                noti_msg = "tagged you in the post";
                                noti_vw_flg = data.noti_vw_flg || "false";
                                pst_id = data.pst_id;
                                snt_ts = TimeUuid.now();
                                queries = [];


                                to_usr_ids.forEach(function (v, i) {
                                    var query = 'insert into ' + ew_pst_tag_noti_table + ' (noti_msg,noti_vw_flg,frm_usr_id,to_usr_id,snt_ts,pst_id) values (?,?,?,?,?,?) ';
                                    var params = [noti_msg, noti_vw_flg, frm_usr_id, v, snt_ts, pst_id];

                                    queries.push({
                                        query: query,
                                        params: params
                                    });
                                });

                                _context27.next = 17;
                                return cassBatch(queries, null);

                            case 17:
                                i = 0;

                            case 18:
                                if (!(i < to_usr_ids.length)) {
                                    _context27.next = 25;
                                    break;
                                }

                                usr_id = to_usr_ids[i];
                                // update notification counter

                                _context27.next = 22;
                                return adminSendNotificationAPI.incrementSendNotificationCount({
                                    usr_id: usr_id,
                                    type: 'cb'
                                });

                            case 22:
                                i++;
                                _context27.next = 18;
                                break;

                            case 25:
                                return _context27.abrupt('return', data);

                            case 28:
                                _context27.prev = 28;
                                _context27.t0 = _context27['catch'](0);
                                throw new Error(_context27.t0);

                            case 31:
                            case 'end':
                                return _context27.stop();
                        }
                    }
                }, _callee27, this, [[0, 28]]);
            }));

            function insertTaggedFriendNotifications(_x35) {
                return _ref27.apply(this, arguments);
            }

            return insertTaggedFriendNotifications;
        }()
    }, {
        key: 'getAllCountry',
        value: function () {
            var _ref28 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee28() {
                var query, data;
                return _regenerator2.default.wrap(function _callee28$(_context28) {
                    while (1) {
                        switch (_context28.prev = _context28.next) {
                            case 0:
                                _context28.prev = 0;
                                query = 'select * from ' + ew_country_table;
                                _context28.next = 4;
                                return cassExecute(query, null);

                            case 4:
                                data = _context28.sent;
                                return _context28.abrupt('return', data);

                            case 8:
                                _context28.prev = 8;
                                _context28.t0 = _context28['catch'](0);
                                throw new Error(_context28.t0);

                            case 11:
                            case 'end':
                                return _context28.stop();
                        }
                    }
                }, _callee28, this, [[0, 8]]);
            }));

            function getAllCountry() {
                return _ref28.apply(this, arguments);
            }

            return getAllCountry;
        }()
    }, {
        key: 'getAllCitiesOfCountry',
        value: function () {
            var _ref29 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee29(data) {
                var query, params;
                return _regenerator2.default.wrap(function _callee29$(_context29) {
                    while (1) {
                        switch (_context29.prev = _context29.next) {
                            case 0:
                                _context29.prev = 0;

                                if (data.cntry_id) {
                                    _context29.next = 3;
                                    break;
                                }

                                return _context29.abrupt('return', {
                                    status: "error",
                                    message: "cntry_id field is required to fetch cities"
                                });

                            case 3:
                                query = 'select * from ' + ew_country_city_table + " where cntry_id = ? ";
                                params = [data.cntry_id];
                                _context29.next = 7;
                                return cassExecute(query, params);

                            case 7:
                                data = _context29.sent;
                                return _context29.abrupt('return', data);

                            case 11:
                                _context29.prev = 11;
                                _context29.t0 = _context29['catch'](0);
                                throw new Error(_context29.t0);

                            case 14:
                            case 'end':
                                return _context29.stop();
                        }
                    }
                }, _callee29, this, [[0, 11]]);
            }));

            function getAllCitiesOfCountry(_x36) {
                return _ref29.apply(this, arguments);
            }

            return getAllCitiesOfCountry;
        }()
    }]);
    return CareerBook;
}();

module.exports = new CareerBook();