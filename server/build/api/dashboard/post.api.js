'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:posts");
var profileAPI = require(configPath.api.dashboard.profile);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var post_table = 'ew1.vt_posts';
var post_crt_by_table = 'ew1.vt_posts_crt_by';
var user_posts_table = 'ew1.vt_frs_posts';
var my_posts_table = 'ew1.vt_my_posts';
var vt_my_posts_flg = "ew1.vt_my_posts_flg";
var user_frs_table = 'ew1.vt_user_frs';
// Like Tables
var ew_pst_like_table = "ew1.ew_pst_like";
var ew_pst_like_stats_table = "ew1.ew_pst_like_stats";

var ew_usr_vdo_cmnt_m_table = 'ew1.ew_usr_vdo_cmnt_m';
var ew_cb_cmpln_rsn_table = 'ew1.ew_cb_cmpln_rsn';
var FETCH_POST_LIMIT = 5;

var CBPost = function () {
    function CBPost() {
        (0, _classCallCheck3.default)(this, CBPost);

        this.posts = [];
    }

    /**
     * @param {JSON} data
     * @param {string} data.pst_msg - Post Msg
     * @param {Array} data.pst_img - Post Images
     * @param {Array} data.pst_vid - Post Videos
     * @param {string} data.postFeeling - Post Feeling (Happy,Sad...)
     * @param {string} data.pst_tgt - Post Target (Public,Only Me,Friends)
     * @param {uuid} data.pst_qry_id - Post Query Id if that post is answered on QA
     * @param {string} data.pst_qry_desc - Post Query Description
     * @param {string} data.pst_crt_by - Post Author id
     * @param {string} data.postRestriction - (Public,Only Me,Friends)
     * @param {string} data.pst_shr_txt - if post is shared using share widget
     * @param {string} data.pst_typ - share,..etc...
     * @param {Array} data.pst_tag_frs - Tagged Friends id
     * @param {String} data.pst_itm_id - If post is posted from video page from comment section
     * @returns {Promise}
     * @desc
     * - If there is tagged friends then insert tagged friend notification using {@link CareerBook#insertTaggedFriendNotifications}
     * - Insert data into {@link CassandraTables#vt_my_posts} and {@link CassandraTables#vt_posts_crt_by}
     * - Insert data into {@link CassandraTables#vt_posts}
     * - if post is only me then dont insert data in {@link CassandraTables#vt_frs_posts} table
     * - Increment dashboard stats post counter using {@link DashboardStats#incrementPostCounter}
     * - if post is public/friends then get all follower of author using {@link CareerBook#getFollowers} and insert data for all those follower {@link CassandraTables#vt_frs_posts} table
     * - Tables Used : {@link CassandraTables#vt_posts} and {@link CassandraTables#vt_posts_crt_by} , {@link CassandraTables#vt_frs_posts} , {@link CassandraTables#vt_my_posts}
     */


    (0, _createClass3.default)(CBPost, [{
        key: 'save',
        value: function save(data) {
            var defer = Q.defer();
            var self = this;
            var pst_id = uuid.v4();
            var pst_dt = new Date();
            var post_author = data.pst_crt_by;
            var pst_tgt = data.postRestriction.toLowerCase() || "public";
            var pst_shr_txt = data.pst_shr_txt || '';
            var pst_typ = data.pst_typ || '';
            var pst_tag_frs = data.pst_tag_frs;
            var pst_itm_id = data.pst_itm_id;

            if (pst_tag_frs && pst_tag_frs.length > 0) {
                //set notification for all tag frnds
                var o = {
                    to_usr_ids: pst_tag_frs,
                    frm_usr_id: post_author,
                    pst_id: pst_id,
                    noti_vw_flg: 'false'
                };
                profileAPI.insertTaggedFriendNotifications(o);
            }

            // Insert into main post table with all details
            var insertPost = 'INSERT INTO ' + post_table + ' (pst_id, pst_dt, pst_msg,pst_img,pst_vid,pst_crt_by,pst_feel,pst_tgt,pst_mfy_date,pst_typ,pst_shr_txt,pst_tag_frs,pst_itm_id)  VALUES(?, ?, ?, ?, ?,?,?,?,?,?,?,?,?);';
            var insertPost_args = [pst_id, pst_dt, data.pst_msg, data.pst_img, data.pst_vid, data.pst_crt_by, data.postFeeling, pst_tgt, pst_dt, pst_typ, pst_shr_txt, pst_tag_frs, pst_itm_id];

            // Insert into post crt by table with all details
            var insertPostCrtBy = 'INSERT INTO ' + post_crt_by_table + ' (pst_id, pst_dt, pst_msg,pst_img,pst_vid,pst_crt_by,pst_feel,pst_tgt,pst_mfy_date,pst_typ,pst_shr_txt,pst_tag_frs,pst_itm_id)  VALUES(?, ?, ?, ?, ?,?,?,?,?,?,?,?,?);';
            var insertPostCrtBy_args = [pst_id, pst_dt, data.pst_msg, data.pst_img, data.pst_vid, data.pst_crt_by, data.postFeeling, pst_tgt, pst_dt, pst_typ, pst_shr_txt, pst_tag_frs, pst_itm_id];

            // Insert into user's own post table to show in profile
            var my_posts = 'insert into ' + my_posts_table + ' (usr_id,pst_id, pst_dt, pst_flg)  ' + 'VALUES(?, ?, ?,?);';
            var my_posts_args = [data.pst_crt_by, pst_id, cassandra.types.TimeUuid.now(), pst_tgt];

            cassconn.execute(my_posts, my_posts_args, function (err, res, r) {
                if (err) {
                    debug("error in saving post into my_posts_table.....", err);
                    return;
                }
            });

            cassconn.execute(insertPostCrtBy, insertPostCrtBy_args, function (err, res, r) {
                if (err) {
                    debug("error in saving post into post_crt_by_table.....", err);
                    return;
                }
            });

            //Find user's friendlist
            var user_frs = 'select * from ' + user_frs_table + ' where usr_id = ?';
            var user_frs_args = [data.pst_crt_by];

            cassconn.execute(insertPost, insertPost_args, function () {
                var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(err, res, r) {
                    return _regenerator2.default.wrap(function _callee$(_context) {
                        while (1) {
                            switch (_context.prev = _context.next) {
                                case 0:
                                    if (!err) {
                                        _context.next = 4;
                                        break;
                                    }

                                    debug(err);
                                    defer.reject(err);
                                    return _context.abrupt('return');

                                case 4:
                                    if (!(pst_tgt.toLowerCase() === 'onlyme')) {
                                        _context.next = 7;
                                        break;
                                    }

                                    //dont save to frnds table
                                    defer.resolve((0, _extends3.default)({}, data, {
                                        pst_id: pst_id,
                                        pst_dt: pst_dt,
                                        pst_tgt: pst_tgt
                                    }));
                                    return _context.abrupt('return');

                                case 7:
                                    _context.prev = 7;
                                    _context.next = 10;
                                    return dashboardStatsAPI.incrementPostCounter({
                                        usr_id: post_author
                                    });

                                case 10:
                                    _context.next = 15;
                                    break;

                                case 12:
                                    _context.prev = 12;
                                    _context.t0 = _context['catch'](7);

                                    debug(_context.t0);

                                case 15:

                                    // add post to all followers
                                    profileAPI.getFollowers({
                                        usr_id: post_author
                                    }).then(function (followers) {
                                        // debug("followers.......",followers);
                                        var flw_ids = [];
                                        if (followers.length > 0) {
                                            flw_ids = followers[0].flw_id || [];
                                        }

                                        if (flw_ids.length <= 0) {
                                            defer.resolve((0, _extends3.default)({}, data, {
                                                pst_id: pst_id,
                                                pst_dt: pst_dt,
                                                pst_tgt: pst_tgt
                                            }));
                                            return;
                                        }

                                        var queries = [];
                                        //create queries to add post for every friends
                                        flw_ids.forEach(function (v) {
                                            var user_frs = 'insert into ' + user_posts_table + ' (usr_id,pst_id, pst_dt)  ' + 'VALUES(?, ?, ?);';
                                            var user_frs_args = [v, pst_id, cassandra.types.TimeUuid.now()];
                                            queries.push({
                                                query: user_frs,
                                                params: user_frs_args
                                            });
                                        });

                                        var queryOptions = {
                                            prepare: true,
                                            consistency: cassandra.types.consistencies.quorum
                                        };
                                        cassconn.batch(queries, queryOptions, function (err, succ, res) {
                                            if (err) {
                                                debug(err);
                                                defer.reject(err);
                                                return;
                                            }
                                            defer.resolve((0, _extends3.default)({}, data, {
                                                pst_id: pst_id,
                                                pst_dt: pst_dt,
                                                pst_tgt: pst_tgt
                                            }));
                                        });
                                    });

                                case 16:
                                case 'end':
                                    return _context.stop();
                            }
                        }
                    }, _callee, this, [[7, 12]]);
                }));

                return function (_x, _x2, _x3) {
                    return _ref.apply(this, arguments);
                };
            }());
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.pst_msg - Post Msg
         * @param {Array} data.pst_img - Post Images
         * @param {Array} data.pst_vid - Post Videos
         * @param {string} data.postFeeling - Post Feeling (Happy,Sad...)
         * @param {string} data.pst_tgt - Post Target (Public,Only Me,Friends)
         * @param {uuid} data.pst_qry_id - Post Query Id if that post is answered on QA
         * @param {string} data.pst_qry_desc - Post Query Description
         * @param {string} data.pst_crt_by - Post Author id
         * @returns {Promise}
         * @desc
         * - This method is used to edit post details for given post id
         * - Tables Used : {@link CassandraTables#vt_posts} and {@link CassandraTables#vt_posts_crt_by}
         */

    }, {
        key: 'edit',
        value: function edit(data) {
            var _this = this;

            var defer = Q.defer();
            var self = this;

            var query = 'Update ' + post_table + ' set pst_mfy_date=?,pst_msg=?,pst_img=?,pst_vid=?,pst_feel=?,pst_tgt=?,pst_qry_id=?,pst_qry_desc=? where pst_id=?  ';
            var params = [new Date(), data.pst_msg, data.pst_img, data.pst_vid, data.postFeeling, data.pst_tgt, data.pst_qry_id, data.pst_qry_desc, data.pst_id];

            var query_crt_by = 'Update ' + post_crt_by_table + ' set pst_mfy_date=?,pst_msg=?,pst_img=?,pst_vid=?,pst_feel=?,pst_qry_id=?,pst_qry_desc=? where pst_id=? and pst_crt_by=?  and pst_tgt = ? ';
            var params_crt_by = [new Date(), data.pst_msg, data.pst_img, data.pst_vid, data.postFeeling, data.pst_qry_id, data.pst_qry_desc, data.pst_id, data.pst_crt_by, data.pst_tgt];

            var queries = [{
                query: query,
                params: params
            }, {
                query: query_crt_by,
                params: params_crt_by
            }];

            cassconn.batch(queries, null, function () {
                var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(err, res, r) {
                    var postdata;
                    return _regenerator2.default.wrap(function _callee2$(_context2) {
                        while (1) {
                            switch (_context2.prev = _context2.next) {
                                case 0:
                                    if (!err) {
                                        _context2.next = 3;
                                        break;
                                    }

                                    defer.reject(err);
                                    return _context2.abrupt('return');

                                case 3:
                                    _context2.next = 5;
                                    return _this.getPostData({ postId: data.pst_id });

                                case 5:
                                    postdata = _context2.sent;

                                    defer.resolve(postdata[0]);

                                case 7:
                                case 'end':
                                    return _context2.stop();
                            }
                        }
                    }, _callee2, _this);
                }));

                return function (_x4, _x5, _x6) {
                    return _ref2.apply(this, arguments);
                };
            }());

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {text} data.usr_id - user Id for which we want to get his post feeds
         * @returns {Promise}
         * @desc
         * - First we will get all posts id from {@link CassandraTables#vt_frs_posts}.
         * - Those post ids will be basically filled when one of the user's friend or following will submit the post
         * - After getting all post ids we will get post data using {@link CBPost#getPostData}
         * - Tables Used : {@link CassandraTables#vt_frs_posts}
         */

    }, {
        key: 'getFeed',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var usr_id, user_posts, user_posts_args, jsonArr, pst_id_dt_uuid_mapping, postsId, res;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;

                                if (data.usr_id) {
                                    _context3.next = 3;
                                    break;
                                }

                                return _context3.abrupt('return', {
                                    status: 'error',
                                    message: "usr_id is required"
                                });

                            case 3:
                                usr_id = data.usr_id;

                                if (data.last_post) {
                                    user_posts = 'select * from ' + user_posts_table + '  where usr_id = ? and pst_dt < ? limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id, data.last_post];
                                } else {
                                    user_posts = 'select * from ' + user_posts_table + '  where usr_id = ? limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id];
                                }

                                _context3.next = 7;
                                return cassExecute(user_posts, user_posts_args);

                            case 7:
                                jsonArr = _context3.sent;
                                pst_id_dt_uuid_mapping = {};
                                //get all posts id

                                postsId = jsonArr.map(function (v, i) {
                                    pst_id_dt_uuid_mapping[v.pst_id] = v.pst_dt;
                                    return v.pst_id;
                                });

                                if (!(postsId && postsId.length > 0)) {
                                    _context3.next = 16;
                                    break;
                                }

                                _context3.next = 13;
                                return this.getPostData({
                                    postId: postsId
                                });

                            case 13:
                                res = _context3.sent;

                                if (res && res.length > 0) {
                                    res = res.map(function (v, i) {
                                        v["dt_uuid"] = pst_id_dt_uuid_mapping[v.pst_id];
                                        return v;
                                    });
                                }
                                return _context3.abrupt('return', res || []);

                            case 16:
                                return _context3.abrupt('return', []);

                            case 19:
                                _context3.prev = 19;
                                _context3.t0 = _context3['catch'](0);
                                throw new Error(_context3.t0);

                            case 22:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 19]]);
            }));

            function getFeed(_x7) {
                return _ref3.apply(this, arguments);
            }

            return getFeed;
        }()

        /**
         * @param {JSON} data
         * @param {text} data.usr_id - user Id for which we want to get his all posts
         * @returns {Promise}
         * @desc
         * - First we will get all posts id from {@link CassandraTables#vt_my_posts}.
         * - Those post ids will be basically filled when user submit the post
         * - After getting all post ids we will get post data using {@link CBPost#getPostData}
         * - Tables Used : {@link CassandraTables#vt_my_posts}
         */

    }, {
        key: 'getUserPosts',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var usr_id, user_posts, user_posts_args, jsonArr, pst_id_dt_uuid_mapping, postsId, res;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;

                                if (data.usr_id) {
                                    _context4.next = 3;
                                    break;
                                }

                                return _context4.abrupt('return', {
                                    status: 'error',
                                    message: "usr_id is required"
                                });

                            case 3:
                                usr_id = data.usr_id;

                                if (data.last_post) {
                                    user_posts = 'select * from ' + my_posts_table + '  where usr_id = ? and pst_dt < ? limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id, data.last_post];
                                } else {
                                    user_posts = 'select * from ' + my_posts_table + '  where usr_id = ? limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id];
                                }

                                _context4.next = 7;
                                return cassExecute(user_posts, user_posts_args);

                            case 7:
                                jsonArr = _context4.sent;

                                //get all posts id
                                pst_id_dt_uuid_mapping = {};
                                postsId = jsonArr.map(function (v, i) {
                                    pst_id_dt_uuid_mapping[v.pst_id] = v.pst_dt;
                                    return v.pst_id;
                                });

                                if (!(postsId && postsId.length > 0)) {
                                    _context4.next = 16;
                                    break;
                                }

                                _context4.next = 13;
                                return this.getPostData({
                                    postId: postsId
                                });

                            case 13:
                                res = _context4.sent;

                                if (res && res.length > 0) {
                                    res = res.map(function (v, i) {
                                        v["dt_uuid"] = pst_id_dt_uuid_mapping[v.pst_id];
                                        return v;
                                    });
                                }
                                return _context4.abrupt('return', res);

                            case 16:
                                return _context4.abrupt('return', []);

                            case 19:
                                _context4.prev = 19;
                                _context4.t0 = _context4['catch'](0);
                                throw new Error(_context4.t0);

                            case 22:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 19]]);
            }));

            function getUserPosts(_x8) {
                return _ref4.apply(this, arguments);
            }

            return getUserPosts;
        }()

        /**
         * @param {JSON} data
         * @param {text} data.usr_id - user Id for which we want to get his public posts
         * @returns {Promise}
         * @desc
         * - This method is used to get all public post of given user
         * - First we will get all public posts details from {@link CassandraTables#vt_posts_crt_by}.
         * - Then we will get given user short details using {@link CareerBook#getUserShortDetails}
         * - Tables Used : {@link CassandraTables#vt_posts_crt_by}
         */

    }, {
        key: 'getUserPublicPosts',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var usr_id, user_posts, user_posts_args, jsonArr, pst_id_dt_uuid_mapping, postsId, res;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;

                                if (data.usr_id) {
                                    _context5.next = 3;
                                    break;
                                }

                                return _context5.abrupt('return', {
                                    status: 'error',
                                    message: "usr_id is required"
                                });

                            case 3:
                                usr_id = data.usr_id;

                                if (data.last_post) {
                                    user_posts = 'select * from ' + vt_my_posts_flg + '  where usr_id = ? and pst_flg = \'public\' and pst_dt < ? limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id, data.last_post];
                                } else {
                                    user_posts = 'select * from ' + vt_my_posts_flg + '  where usr_id = ? and pst_flg = \'public\' limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id];
                                }
                                _context5.next = 7;
                                return cassExecute(user_posts, user_posts_args);

                            case 7:
                                jsonArr = _context5.sent;

                                //get all posts id
                                pst_id_dt_uuid_mapping = {};
                                postsId = jsonArr.map(function (v, i) {
                                    pst_id_dt_uuid_mapping[v.pst_id] = v.pst_dt;
                                    return v.pst_id;
                                });

                                if (!(postsId && postsId.length > 0)) {
                                    _context5.next = 16;
                                    break;
                                }

                                _context5.next = 13;
                                return this.getPostData({
                                    postId: postsId
                                });

                            case 13:
                                res = _context5.sent;

                                if (res && res.length > 0) {
                                    res = res.map(function (v, i) {
                                        v["dt_uuid"] = pst_id_dt_uuid_mapping[v.pst_id];
                                        return v;
                                    });
                                }
                                return _context5.abrupt('return', res);

                            case 16:
                                return _context5.abrupt('return', []);

                            case 19:
                                _context5.prev = 19;
                                _context5.t0 = _context5['catch'](0);
                                throw new Error(_context5.t0);

                            case 22:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 19]]);
            }));

            function getUserPublicPosts(_x9) {
                return _ref5.apply(this, arguments);
            }

            return getUserPublicPosts;
        }()

        /**
         * @param {JSON} data
         * @param {text} data.usr_id - user Id for which we want to get his friends posts
         * @returns {Promise}
         * @desc
         * - This method is used to get all posts which is restricted to friends and public
         * - First we will get all public posts details from {@link CassandraTables#vt_posts_crt_by}.
         * - Then we will get given user short details using {@link CareerBook#getUserShortDetails}
         * - Tables Used : {@link CassandraTables#vt_posts_crt_by}
         */

    }, {
        key: 'getUserPostsForFriends',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var usr_id, user_posts, user_posts_args, jsonArr, pst_id_dt_uuid_mapping, postsId, res;
                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;

                                if (data.usr_id) {
                                    _context6.next = 3;
                                    break;
                                }

                                return _context6.abrupt('return', {
                                    status: 'error',
                                    message: "usr_id is required"
                                });

                            case 3:
                                usr_id = data.usr_id;

                                if (data.last_post) {
                                    user_posts = 'select * from ' + vt_my_posts_flg + '  where usr_id = ? and pst_flg = \'friends\' and pst_dt < ? limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id, data.last_post];
                                } else {
                                    user_posts = 'select * from ' + vt_my_posts_flg + '  where usr_id = ? and pst_flg = \'friends\' limit ' + FETCH_POST_LIMIT;
                                    user_posts_args = [usr_id];
                                }
                                _context6.next = 7;
                                return cassExecute(user_posts, user_posts_args);

                            case 7:
                                jsonArr = _context6.sent;

                                //get all posts id
                                pst_id_dt_uuid_mapping = {};
                                postsId = jsonArr.map(function (v, i) {
                                    pst_id_dt_uuid_mapping[v.pst_id] = v.pst_dt;
                                    return v.pst_id;
                                });

                                if (!(postsId && postsId.length > 0)) {
                                    _context6.next = 16;
                                    break;
                                }

                                _context6.next = 13;
                                return this.getPostData({
                                    postId: postsId
                                });

                            case 13:
                                res = _context6.sent;

                                if (res && res.length > 0) {
                                    res = res.map(function (v, i) {
                                        v["dt_uuid"] = pst_id_dt_uuid_mapping[v.pst_id];
                                        return v;
                                    });
                                }
                                return _context6.abrupt('return', res);

                            case 16:
                                return _context6.abrupt('return', []);

                            case 19:
                                _context6.prev = 19;
                                _context6.t0 = _context6['catch'](0);
                                throw new Error(_context6.t0);

                            case 22:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 19]]);
            }));

            function getUserPostsForFriends(_x10) {
                return _ref6.apply(this, arguments);
            }

            return getUserPostsForFriends;
        }()

        /**
         * @param {JSON} data
         * @param {string|Array} data.postId - Post(s) ID
         * @returns {Promise}
         * @desc
         * - This method is used to get details of given post id(s)
         * - Then collect user id from this post
         * - Then Get User Short Details from using those uesr id. See {@link CareerBook#getUsersShortDetails}
         * - Merge the results
         * - Tables Used : {@link CassandraTables#vt_posts}
         */

    }, {
        key: 'getPostData',
        value: function getPostData(data) {
            // data = {postId : (it can be anything string/array of postids) }
            var defer = Q.defer();
            var postId = [].concat(data.postId);

            //join all ids with commma
            var postsIdStr = '(' + postId.join(",") + ")";
            var posts_query = 'select * from ' + post_table + ' where pst_id in ' + postsIdStr;

            cassconn.execute(posts_query, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                }

                //all post array
                var jsonArr = JSON.parse(JSON.stringify(res.rows));

                if (jsonArr.length <= 0) {
                    //if there is no post
                    defer.resolve([]);
                    return;
                }

                //all post users id
                var postUsers = jsonArr.map(function (v, i) {
                    return v.pst_crt_by;
                });

                //get post user details
                profileAPI.getUsersShortDetails({
                    usr_ids: postUsers
                }).then(function (users) {

                    var userMapping = {};
                    //generate user mapping so we can direct fetch from dict
                    users.map(function (v, i) {
                        delete v.usr_email;
                        delete v.usr_ph;
                        delete v.usr_role;
                        userMapping[v.usr_id] = v;
                    });

                    var allPostdata = jsonArr.map(function (v, i) {
                        //get post author
                        var crt_by = v['pst_crt_by'];
                        //get user info
                        var user = userMapping[crt_by];
                        //attach user info to item
                        v['usr_info'] = user;
                        return v;
                    });

                    defer.resolve(allPostdata);
                }).catch(function (err) {
                    defer.reject(err);
                });
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - post id which we want to delete
         * @param {text} data.pst_crt_by - author of the post
         * @param {text} data.pst_tgt - post targeted audiance (public,onlyme , friends)
         * @returns {Promise}
         * @desc
         * - This method is used to delete post by given post id and user
         * - First select post data related to pst_id from {@link CassandraTables#vt_posts}
         * - Delete Post from {@link CassandraTables#vt_posts}
         * - Delete Post from {@link CassandraTables#vt_posts_crt_by}
         * - Decrement Dashboard Stats Post Counter using {@link DashboardStats#decrementPostCounter}
         * - Post Contains Post Item ID means if that post is posted from video page, then remove reference of this post from {@link CassandraTables#ew_usr_vdo_cmnt_m}
         * - Tables Used : {@link CassandraTables#vt_posts_crt_by} , {@link CassandraTables#vt_posts} , {@link CassandraTables#ew_usr_vdo_cmnt_m}
         */

    }, {
        key: 'delete',
        value: function () {
            var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
                var defer, self, pst_id, pst_crt_by, pst_tgt, pst_itm_id, posts, posts_args, posts_crt_by, posts_crt_by_args, qry_sel, params_sel, res, queries, del_res, qry, params;
                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                defer = Q.defer();
                                _context7.prev = 1;
                                self = this;
                                self = this;
                                pst_id = data.pst_id;
                                pst_crt_by = data.pst_crt_by;
                                pst_tgt = data.pst_tgt;
                                pst_itm_id = "";
                                posts = 'delete from ' + post_table + ' where pst_id = ?';
                                posts_args = [pst_id];
                                posts_crt_by = 'delete from ' + post_crt_by_table + ' where pst_id = ? and pst_crt_by = ? and pst_tgt=?';
                                posts_crt_by_args = [pst_id, pst_crt_by, pst_tgt];
                                qry_sel = "select * from " + post_table + " where pst_id=?";
                                params_sel = [pst_id];
                                _context7.next = 16;
                                return cassExecute(qry_sel, params_sel);

                            case 16:
                                res = _context7.sent;

                                res = res[0] || {};
                                pst_itm_id = res.pst_itm_id;

                                queries = [{
                                    query: posts,
                                    params: posts_args
                                }, {
                                    query: posts_crt_by,
                                    params: posts_crt_by_args
                                }];
                                _context7.next = 22;
                                return cassBatch(queries, null);

                            case 22:
                                del_res = _context7.sent;
                                _context7.prev = 23;
                                _context7.next = 26;
                                return dashboardStatsAPI.decrementPostCounter({
                                    usr_id: pst_crt_by
                                });

                            case 26:
                                _context7.next = 31;
                                break;

                            case 28:
                                _context7.prev = 28;
                                _context7.t0 = _context7['catch'](23);

                                debug(_context7.t0);

                            case 31:
                                if (!pst_itm_id) {
                                    _context7.next = 37;
                                    break;
                                }

                                qry = "delete from " + ew_usr_vdo_cmnt_m_table + " where usr_id=? and pst_id=?";
                                params = [pst_crt_by, pst_itm_id];
                                _context7.next = 36;
                                return cassExecute(qry, params);

                            case 36:
                                res = _context7.sent;

                            case 37:
                                return _context7.abrupt('return', {});

                            case 40:
                                _context7.prev = 40;
                                _context7.t1 = _context7['catch'](1);

                                debug(_context7.t1);
                                throw _context7.t1;

                            case 45:
                                return _context7.abrupt('return', defer.promise);

                            case 46:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this, [[1, 40], [23, 28]]);
            }));

            function _delete(_x11) {
                return _ref7.apply(this, arguments);
            }

            return _delete;
        }()

        /**
         * @param {JSON} data
         * @param {uuid|Array} data.pst_ids - post id(s)
         * @param {string} data.usr_id - usr id whose likes we want to fetch
         * @returns {Promise}
         * @desc
         * - This function is used to get like given by user
         * - Tables Used : {@link CassandraTables#ew_pst_like}
         */

    }, {
        key: 'getLikeByUser',
        value: function getLikeByUser(data) {
            var defer = Q.defer();
            var usr_id = data.usr_id;
            var pst_ids = [].concat(data.pst_ids); //data.pst_ids can be string or array
            var crt_ts = new Date();

            var str = '(' + pst_ids.join(',') + ')';

            var query = "select * from " + ew_pst_like_table + ' where usr_id =? and pst_id in ' + str;
            var params = [usr_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - post id
         * @param {string} data.usr_id - usr id whose likes we want to fetch
         * @param {string} data.like_typ - wow,sad,angry,liked,etc....
         * @returns {Promise}
         * @desc
         * - This function is used to update like given by user
         * - Tables Used : {@link CassandraTables#ew_pst_like}
         */

    }, {
        key: 'updateLikeByUser',
        value: function updateLikeByUser(data) {
            var defer = Q.defer();
            var usr_id = data.usr_id;
            var pst_id = data.pst_id;
            var like_typ = data.like_typ || "none";
            var crt_ts = new Date();

            var query = "update " + ew_pst_like_table + ' set like_typ = ?,crt_ts=? where usr_id =? and pst_id=?';
            var params = [like_typ, crt_ts, usr_id, pst_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid|Array} data.pst_ids - post id(s)
         * @returns {Promise}
         * @desc
         * - This function is used to get all type of like count of given posts
         * - Tables Used : {@link CassandraTables#ew_pst_like_stats}
         */

    }, {
        key: 'getLikeStatsForPosts',
        value: function getLikeStatsForPosts(data) {
            var defer = Q.defer();

            var pst_ids = [].concat(data.pst_ids); //data.pst_ids can be string or array

            var str = '(' + pst_ids.join(',') + ')';

            var query = "select * from " + ew_pst_like_stats_table + ' where pst_id in ' + str;

            cassconn.execute(query, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - post id
         * @param {counter} data.angry_cnt - Angry Count for e.g. (-1,0,+1)
         * @param {counter} data.dlike_cnt - Dislike Count for e.g. (-1,0,+1)
         * @param {counter} data.haha_cnt - Haha Count for e.g. (-1,0,+1)
         * @param {counter} data.like_cnt - Like Count for e.g. (-1,0,+1)
         * @param {counter} data.love_cnt - Love Count for e.g. (-1,0,+1)
         * @param {counter} data.sad_cnt - Sad Count for e.g. (-1,0,+1)
         * @param {counter} data.wow_cnt - Wow Count for e.g. (-1,0,+1)
         * @returns {Promise}
         * @desc
         * - This function is used to update count of given posts
         * - Tables Used : {@link CassandraTables#ew_pst_like_stats}
         */

    }, {
        key: 'updateLikeStatsForPosts',
        value: function updateLikeStatsForPosts(data) {
            var defer = Q.defer();

            var pst_id = data.pst_id;

            var keys = Object.keys(data);
            var update_str = '';
            keys.forEach(function (key, index) {
                if (key !== 'pst_id') {
                    update_str += '' + key + '=' + key + '+ ' + data[key] + ',';
                }
            });

            update_str = update_str.substr(0, update_str.length - 1);

            var query = "update " + ew_pst_like_stats_table + ' set ' + update_str + ' where pst_id = ?';
            debug(query);
            var params = [pst_id];

            cassconn.execute(query, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });

            return defer.promise;
        }
    }, {
        key: 'saveComplain',
        value: function saveComplain(data) {
            // data = {usr_id,cmpln_id,cmpln_typ,cmpln_rsn}
            var defer = Q.defer();
            var self = this;
            var qry = "insert into " + ew_cb_cmpln_rsn_table + ' (usr_id,cmpln_id,cmpln_typ,cmpln_rsn,cmpln_ts) values (?,?,?,?,?)';
            var qry_params = [data.usr_id, data.cmpln_id, data.cmpln_typ, data.cmpln_rsn, new Date()];
            console.log(data);
            cassconn.execute(qry, qry_params, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(data);
            });
            return defer.promise;
        }
    }]);
    return CBPost;
}();

module.exports = new CBPost();