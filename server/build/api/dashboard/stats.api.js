'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:dashboard:stats');

var ew_dashboard_stats_tbl = "ew1.ew_dashboard_stats";

var DashboardStats = function () {
    function DashboardStats() {
        (0, _classCallCheck3.default)(this, DashboardStats);
    }

    (0, _createClass3.default)(DashboardStats, [{
        key: 'getAll',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var usr_id, qry, params, res;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                usr_id = data.usr_id;
                                qry = "select * from " + ew_dashboard_stats_tbl + " where usr_id = ?";
                                params = [usr_id];
                                _context.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                res = _context.sent;

                                if (res.length === 1) {
                                    res = res[0];
                                }
                                return _context.abrupt('return', res);

                            case 11:
                                _context.prev = 11;
                                _context.t0 = _context['catch'](0);

                                debug(_context.t0);
                                throw _context.t0;

                            case 15:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 11]]);
            }));

            function getAll(_x) {
                return _ref.apply(this, arguments);
            }

            return getAll;
        }()
    }, {
        key: 'updateBookmarkCounter',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                var usr_id, no_of_bookmarks, qry, params;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                usr_id = data.usr_id;
                                no_of_bookmarks = data.no_of_bkmrks; //can be positive or negitive

                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_bkmrks=no_of_bkmrks+ " + no_of_bookmarks + " where usr_id = ?";
                                params = [usr_id];
                                _context2.next = 7;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 7:
                                return _context2.abrupt('return', data);

                            case 10:
                                _context2.prev = 10;
                                _context2.t0 = _context2['catch'](0);

                                debug(_context2.t0);
                                throw _context2.t0;

                            case 14:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[0, 10]]);
            }));

            function updateBookmarkCounter(_x2) {
                return _ref2.apply(this, arguments);
            }

            return updateBookmarkCounter;
        }()
    }, {
        key: 'incrementNotesCounter',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_notes=no_of_notes+1 where usr_id = ?";
                                params = [usr_id];
                                _context3.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context3.abrupt('return', data);

                            case 9:
                                _context3.prev = 9;
                                _context3.t0 = _context3['catch'](0);

                                debug(_context3.t0);
                                throw _context3.t0;

                            case 13:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 9]]);
            }));

            function incrementNotesCounter(_x3) {
                return _ref3.apply(this, arguments);
            }

            return incrementNotesCounter;
        }()
    }, {
        key: 'decrementNotesCounter',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_notes=no_of_notes-1 where usr_id = ?";
                                params = [usr_id];
                                _context4.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context4.abrupt('return', data);

                            case 9:
                                _context4.prev = 9;
                                _context4.t0 = _context4['catch'](0);

                                debug(_context4.t0);
                                throw _context4.t0;

                            case 13:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 9]]);
            }));

            function decrementNotesCounter(_x4) {
                return _ref4.apply(this, arguments);
            }

            return decrementNotesCounter;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose question asked count we want to increment
         * @returns {Promise}
         * @desc
         * - Increment Dashboard Question Asked counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'incrementForumQuestionAskedCounter',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ask=no_of_qa_que_ask+1 where usr_id = ?";
                                params = [usr_id];
                                _context5.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context5.abrupt('return', data);

                            case 9:
                                _context5.prev = 9;
                                _context5.t0 = _context5['catch'](0);

                                debug(_context5.t0);
                                throw _context5.t0;

                            case 13:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 9]]);
            }));

            function incrementForumQuestionAskedCounter(_x5) {
                return _ref5.apply(this, arguments);
            }

            return incrementForumQuestionAskedCounter;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose question asked count we want to decrement
         * @returns {Promise}
         * @desc
         * - Decrement Dashboard Question Asked counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'decrementForumQuestionAskedCounter',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ask=no_of_qa_que_ask-1 where usr_id = ?";
                                params = [usr_id];
                                _context6.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context6.abrupt('return', data);

                            case 9:
                                _context6.prev = 9;
                                _context6.t0 = _context6['catch'](0);

                                debug(_context6.t0);
                                throw _context6.t0;

                            case 13:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 9]]);
            }));

            function decrementForumQuestionAskedCounter(_x6) {
                return _ref6.apply(this, arguments);
            }

            return decrementForumQuestionAskedCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose question answered count we want to increment
         * @returns {Promise}
         * @desc
         * - Increment Dashboard Question Answered counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'incrementForumQuestionAnsweredCounter',
        value: function () {
            var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                _context7.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ans=no_of_qa_que_ans+1 where usr_id = ?";
                                params = [usr_id];
                                _context7.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context7.abrupt('return', data);

                            case 9:
                                _context7.prev = 9;
                                _context7.t0 = _context7['catch'](0);

                                debug(_context7.t0);
                                throw _context7.t0;

                            case 13:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this, [[0, 9]]);
            }));

            function incrementForumQuestionAnsweredCounter(_x7) {
                return _ref7.apply(this, arguments);
            }

            return incrementForumQuestionAnsweredCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose question answered count we want to decrement
         * @returns {Promise}
         * @desc
         * - decrement Dashboard Question Answered counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'decrementForumQuestionAnsweredCounter',
        value: function () {
            var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                _context8.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ans=no_of_qa_que_ans-1 where usr_id = ?";
                                params = [usr_id];
                                _context8.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context8.abrupt('return', data);

                            case 9:
                                _context8.prev = 9;
                                _context8.t0 = _context8['catch'](0);

                                debug(_context8.t0);
                                throw _context8.t0;

                            case 13:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, this, [[0, 9]]);
            }));

            function decrementForumQuestionAnsweredCounter(_x8) {
                return _ref8.apply(this, arguments);
            }

            return decrementForumQuestionAnsweredCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose QA question views count we want to increment
         * @returns {Promise}
         * @desc
         * - Increment Dashboard QA Question views counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'incrementForumQuestionViewsCounter',
        value: function () {
            var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee9$(_context9) {
                    while (1) {
                        switch (_context9.prev = _context9.next) {
                            case 0:
                                _context9.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_vws=no_of_qa_que_vws+1 where usr_id = ?";
                                params = [usr_id];
                                _context9.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context9.abrupt('return', data);

                            case 9:
                                _context9.prev = 9;
                                _context9.t0 = _context9['catch'](0);

                                debug(_context9.t0);
                                throw _context9.t0;

                            case 13:
                            case 'end':
                                return _context9.stop();
                        }
                    }
                }, _callee9, this, [[0, 9]]);
            }));

            function incrementForumQuestionViewsCounter(_x9) {
                return _ref9.apply(this, arguments);
            }

            return incrementForumQuestionViewsCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose QA question Comment count we want to increment
         * @returns {Promise}
         * @desc
         * - Increment Dashboard QA Question Comment counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'incrementForumQuestionCommentCounter',
        value: function () {
            var _ref10 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee10$(_context10) {
                    while (1) {
                        switch (_context10.prev = _context10.next) {
                            case 0:
                                _context10.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_cmnts=no_of_qa_cmnts+1 where usr_id = ?";
                                params = [usr_id];
                                _context10.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context10.abrupt('return', data);

                            case 9:
                                _context10.prev = 9;
                                _context10.t0 = _context10['catch'](0);

                                debug(_context10.t0);
                                throw _context10.t0;

                            case 13:
                            case 'end':
                                return _context10.stop();
                        }
                    }
                }, _callee10, this, [[0, 9]]);
            }));

            function incrementForumQuestionCommentCounter(_x10) {
                return _ref10.apply(this, arguments);
            }

            return incrementForumQuestionCommentCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose QA question Comment count we want to decrement
         * @returns {Promise}
         * @desc
         * - decrement Dashboard QA Question Comment counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'decrementForumQuestionCommentCounter',
        value: function () {
            var _ref11 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee11$(_context11) {
                    while (1) {
                        switch (_context11.prev = _context11.next) {
                            case 0:
                                _context11.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_cmnts=no_of_qa_cmnts-1 where usr_id = ?";
                                params = [usr_id];
                                _context11.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context11.abrupt('return', data);

                            case 9:
                                _context11.prev = 9;
                                _context11.t0 = _context11['catch'](0);

                                debug(_context11.t0);
                                throw _context11.t0;

                            case 13:
                            case 'end':
                                return _context11.stop();
                        }
                    }
                }, _callee11, this, [[0, 9]]);
            }));

            function decrementForumQuestionCommentCounter(_x11) {
                return _ref11.apply(this, arguments);
            }

            return decrementForumQuestionCommentCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose CB Comment count we want to increment
         * @returns {Promise}
         * @desc
         * - Increment Dashboard CB Comment counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'incrementCommentCounter',
        value: function () {
            var _ref12 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee12$(_context12) {
                    while (1) {
                        switch (_context12.prev = _context12.next) {
                            case 0:
                                _context12.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_cmnts=no_of_cmnts+1 where usr_id = ?";
                                params = [usr_id];
                                _context12.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context12.abrupt('return', data);

                            case 9:
                                _context12.prev = 9;
                                _context12.t0 = _context12['catch'](0);

                                debug(_context12.t0);
                                throw _context12.t0;

                            case 13:
                            case 'end':
                                return _context12.stop();
                        }
                    }
                }, _callee12, this, [[0, 9]]);
            }));

            function incrementCommentCounter(_x12) {
                return _ref12.apply(this, arguments);
            }

            return incrementCommentCounter;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose CB Comment count we want to Decrement
         * @returns {Promise}
         * @desc
         * - Decrement Dashboard CB Comment counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'decrementCommentCounter',
        value: function () {
            var _ref13 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee13(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee13$(_context13) {
                    while (1) {
                        switch (_context13.prev = _context13.next) {
                            case 0:
                                _context13.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_cmnts=no_of_cmnts-1 where usr_id = ?";
                                params = [usr_id];
                                _context13.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context13.abrupt('return', data);

                            case 9:
                                _context13.prev = 9;
                                _context13.t0 = _context13['catch'](0);

                                debug(_context13.t0);
                                throw _context13.t0;

                            case 13:
                            case 'end':
                                return _context13.stop();
                        }
                    }
                }, _callee13, this, [[0, 9]]);
            }));

            function decrementCommentCounter(_x13) {
                return _ref13.apply(this, arguments);
            }

            return decrementCommentCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose CB Post count we want to increment
         * @returns {Promise}
         * @desc
         * - Increment Dashboard CB Post counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'incrementPostCounter',
        value: function () {
            var _ref14 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee14(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee14$(_context14) {
                    while (1) {
                        switch (_context14.prev = _context14.next) {
                            case 0:
                                _context14.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_psts=no_of_psts+1 where usr_id = ?";
                                params = [usr_id];
                                _context14.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context14.abrupt('return', data);

                            case 9:
                                _context14.prev = 9;
                                _context14.t0 = _context14['catch'](0);

                                debug(_context14.t0);
                                throw _context14.t0;

                            case 13:
                            case 'end':
                                return _context14.stop();
                        }
                    }
                }, _callee14, this, [[0, 9]]);
            }));

            function incrementPostCounter(_x14) {
                return _ref14.apply(this, arguments);
            }

            return incrementPostCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose CB Post count we want to Decrement
         * @returns {Promise}
         * @desc
         * - Decrement Dashboard CB Post counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'decrementPostCounter',
        value: function () {
            var _ref15 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee15(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee15$(_context15) {
                    while (1) {
                        switch (_context15.prev = _context15.next) {
                            case 0:
                                _context15.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_psts=no_of_psts-1 where usr_id = ?";
                                params = [usr_id];
                                _context15.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context15.abrupt('return', data);

                            case 9:
                                _context15.prev = 9;
                                _context15.t0 = _context15['catch'](0);

                                debug(_context15.t0);
                                throw _context15.t0;

                            case 13:
                            case 'end':
                                return _context15.stop();
                        }
                    }
                }, _callee15, this, [[0, 9]]);
            }));

            function decrementPostCounter(_x15) {
                return _ref15.apply(this, arguments);
            }

            return decrementPostCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose friends count we want to increment
         * @returns {Promise}
         * @desc
         * - Increment Dashboard Stats Friends counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'incrementFriendCounter',
        value: function () {
            var _ref16 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee16(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee16$(_context16) {
                    while (1) {
                        switch (_context16.prev = _context16.next) {
                            case 0:
                                _context16.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_frds=no_of_frds+1 where usr_id = ?";
                                params = [usr_id];
                                _context16.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context16.abrupt('return', data);

                            case 9:
                                _context16.prev = 9;
                                _context16.t0 = _context16['catch'](0);

                                debug(_context16.t0);
                                throw _context16.t0;

                            case 13:
                            case 'end':
                                return _context16.stop();
                        }
                    }
                }, _callee16, this, [[0, 9]]);
            }));

            function incrementFriendCounter(_x16) {
                return _ref16.apply(this, arguments);
            }

            return incrementFriendCounter;
        }()
        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id whose friends count we want to decrement
         * @returns {Promise}
         * @desc
         * - Decrement Dashboard Stats Friends counter
         * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
         */

    }, {
        key: 'decrementFriendCounter',
        value: function () {
            var _ref17 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee17(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee17$(_context17) {
                    while (1) {
                        switch (_context17.prev = _context17.next) {
                            case 0:
                                _context17.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_frds=no_of_frds-1 where usr_id = ?";
                                params = [usr_id];
                                _context17.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context17.abrupt('return', data);

                            case 9:
                                _context17.prev = 9;
                                _context17.t0 = _context17['catch'](0);

                                debug(_context17.t0);
                                throw _context17.t0;

                            case 13:
                            case 'end':
                                return _context17.stop();
                        }
                    }
                }, _callee17, this, [[0, 9]]);
            }));

            function decrementFriendCounter(_x17) {
                return _ref17.apply(this, arguments);
            }

            return decrementFriendCounter;
        }()
    }, {
        key: 'incrementVideoViews',
        value: function () {
            var _ref18 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee18(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee18$(_context18) {
                    while (1) {
                        switch (_context18.prev = _context18.next) {
                            case 0:
                                _context18.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_vdo_vws=no_of_vdo_vws+1 where usr_id = ?";
                                params = [usr_id];
                                _context18.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context18.abrupt('return', data);

                            case 9:
                                _context18.prev = 9;
                                _context18.t0 = _context18['catch'](0);

                                debug(_context18.t0);
                                throw _context18.t0;

                            case 13:
                            case 'end':
                                return _context18.stop();
                        }
                    }
                }, _callee18, this, [[0, 9]]);
            }));

            function incrementVideoViews(_x18) {
                return _ref18.apply(this, arguments);
            }

            return incrementVideoViews;
        }()
    }, {
        key: 'incrementQuestionLearnedViews',
        value: function () {
            var _ref19 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee19(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee19$(_context19) {
                    while (1) {
                        switch (_context19.prev = _context19.next) {
                            case 0:
                                _context19.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_learned=no_of_que_learned+1 where usr_id = ?";
                                params = [usr_id];
                                _context19.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context19.abrupt('return', data);

                            case 9:
                                _context19.prev = 9;
                                _context19.t0 = _context19['catch'](0);

                                debug(_context19.t0);
                                throw _context19.t0;

                            case 13:
                            case 'end':
                                return _context19.stop();
                        }
                    }
                }, _callee19, this, [[0, 9]]);
            }));

            function incrementQuestionLearnedViews(_x19) {
                return _ref19.apply(this, arguments);
            }

            return incrementQuestionLearnedViews;
        }()
    }, {
        key: 'incrementTestTakenCounter',
        value: function () {
            var _ref20 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee20(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee20$(_context20) {
                    while (1) {
                        switch (_context20.prev = _context20.next) {
                            case 0:
                                _context20.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_tsts_taken=no_of_tsts_taken+1 where usr_id = ?";
                                params = [usr_id];
                                _context20.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context20.abrupt('return', data);

                            case 9:
                                _context20.prev = 9;
                                _context20.t0 = _context20['catch'](0);

                                debug(_context20.t0);
                                throw _context20.t0;

                            case 13:
                            case 'end':
                                return _context20.stop();
                        }
                    }
                }, _callee20, this, [[0, 9]]);
            }));

            function incrementTestTakenCounter(_x20) {
                return _ref20.apply(this, arguments);
            }

            return incrementTestTakenCounter;
        }()
    }, {
        key: 'incrementNoOfQuestionPracticedCounter',
        value: function () {
            var _ref21 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee21(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee21$(_context21) {
                    while (1) {
                        switch (_context21.prev = _context21.next) {
                            case 0:
                                _context21.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_practiced=no_of_que_practiced+1 where usr_id = ?";
                                params = [usr_id];
                                _context21.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context21.abrupt('return', data);

                            case 9:
                                _context21.prev = 9;
                                _context21.t0 = _context21['catch'](0);

                                debug(_context21.t0);
                                throw _context21.t0;

                            case 13:
                            case 'end':
                                return _context21.stop();
                        }
                    }
                }, _callee21, this, [[0, 9]]);
            }));

            function incrementNoOfQuestionPracticedCounter(_x21) {
                return _ref21.apply(this, arguments);
            }

            return incrementNoOfQuestionPracticedCounter;
        }()
    }, {
        key: 'incrementNoOfQuestionAttemptedInTestCounter',
        value: function () {
            var _ref22 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee22(data) {
                var usr_id, qry, params;
                return _regenerator2.default.wrap(function _callee22$(_context22) {
                    while (1) {
                        switch (_context22.prev = _context22.next) {
                            case 0:
                                _context22.prev = 0;
                                usr_id = data.usr_id;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_attempt_tsts=no_of_que_attempt_tsts+1 where usr_id = ?";
                                params = [usr_id];
                                _context22.next = 6;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 6:
                                return _context22.abrupt('return', data);

                            case 9:
                                _context22.prev = 9;
                                _context22.t0 = _context22['catch'](0);

                                debug(_context22.t0);
                                throw _context22.t0;

                            case 13:
                            case 'end':
                                return _context22.stop();
                        }
                    }
                }, _callee22, this, [[0, 9]]);
            }));

            function incrementNoOfQuestionAttemptedInTestCounter(_x22) {
                return _ref22.apply(this, arguments);
            }

            return incrementNoOfQuestionAttemptedInTestCounter;
        }()
    }, {
        key: 'updateNoOfCorrectQuestionAttemptedCounter',
        value: function () {
            var _ref23 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee23(data) {
                var usr_id, count, qry, params;
                return _regenerator2.default.wrap(function _callee23$(_context23) {
                    while (1) {
                        switch (_context23.prev = _context23.next) {
                            case 0:
                                _context23.prev = 0;
                                usr_id = data.usr_id;
                                count = data.count || 0;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_attempt_correct = no_of_que_attempt_correct + " + count + "where usr_id = ?";
                                params = [usr_id];
                                _context23.next = 7;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 7:
                                return _context23.abrupt('return', data);

                            case 10:
                                _context23.prev = 10;
                                _context23.t0 = _context23['catch'](0);

                                debug(_context23.t0);
                                throw _context23.t0;

                            case 14:
                            case 'end':
                                return _context23.stop();
                        }
                    }
                }, _callee23, this, [[0, 10]]);
            }));

            function updateNoOfCorrectQuestionAttemptedCounter(_x23) {
                return _ref23.apply(this, arguments);
            }

            return updateNoOfCorrectQuestionAttemptedCounter;
        }()
    }, {
        key: 'updateNoOfWrongQuestionAttemptedCounter',
        value: function () {
            var _ref24 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee24(data) {
                var usr_id, count, qry, params;
                return _regenerator2.default.wrap(function _callee24$(_context24) {
                    while (1) {
                        switch (_context24.prev = _context24.next) {
                            case 0:
                                _context24.prev = 0;
                                usr_id = data.usr_id;
                                count = data.count || 0;
                                qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_attempt_wrong = no_of_que_attempt_wrong + " + count + "where usr_id = ?";
                                params = [usr_id];
                                _context24.next = 7;
                                return cassExecute(qry, params, {
                                    prepare: true
                                });

                            case 7:
                                return _context24.abrupt('return', data);

                            case 10:
                                _context24.prev = 10;
                                _context24.t0 = _context24['catch'](0);

                                debug(_context24.t0);
                                throw _context24.t0;

                            case 14:
                            case 'end':
                                return _context24.stop();
                        }
                    }
                }, _callee24, this, [[0, 10]]);
            }));

            function updateNoOfWrongQuestionAttemptedCounter(_x24) {
                return _ref24.apply(this, arguments);
            }

            return updateNoOfWrongQuestionAttemptedCounter;
        }()
    }]);
    return DashboardStats;
}();

module.exports = new DashboardStats();