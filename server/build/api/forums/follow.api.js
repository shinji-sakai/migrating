'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:follow");
var uuid = require('uuid');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;
var async = require('async');
var cassExecute = require(configPath.lib.utility).cassExecute;
var adminSendNotificationAPI = require(configPath.api.admin.sendNotification);

var ew_qry_flw_tbl = 'ew1.ew_qry_flw';
var ew_qry_flw_ntf_tbl = 'ew1.ew_qry_flw_ntf';
var ew_qry_usr_flw_ = "ew1.ew_qry_usr_flw_";

var QAFollow = function () {
    function QAFollow() {
        (0, _classCallCheck3.default)(this, QAFollow);
    }

    (0, _createClass3.default)(QAFollow, [{
        key: 'getAllFollowers',


        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Question Id
         * @returns {Promise}
         * @desc
         * - This method is used to get all followers for the given question
         * - Tables Used : {@link CassandraTables#ew_qry_flw}
         */
        value: function getAllFollowers(data) {
            //data = {qry_id : ''}
            var defer = Q.defer();

            var select = 'select * from ' + ew_qry_flw_tbl + ' where qry_id = ?';
            var args = [data.qry_id];

            //cassandra query
            cassconn.execute(select, args, function (err, res, r) {
                if (err) {
                    //if error
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var rows = res.rows || [];
                var json_rows = JSON.parse(JSON.stringify(res.rows));
                //return all rows
                defer.resolve(json_rows);
            });
            return defer.promise;
        }
    }, {
        key: 'getUserFollowedQuestion',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var usr_id, last_qry_id, qry, params, res;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                usr_id = data.usr_id, last_qry_id = data.last_qry_id;
                                qry = void 0;
                                params = void 0;

                                if (last_qry_id) {
                                    query = 'select * from ' + ew_qry_usr_flw_ + ' where usr_id = ? and qry_id < ? limit 5';
                                    params = [usr_id, last_qry_id];
                                } else {
                                    query = 'select * from ' + ew_qry_usr_flw_ + ' where usr_id = ? limit 5';
                                    params = [usr_id];
                                }
                                _context.next = 7;
                                return cassExecute(qry, params);

                            case 7:
                                res = _context.sent;
                                return _context.abrupt('return', res);

                            case 11:
                                _context.prev = 11;
                                _context.t0 = _context['catch'](0);
                                throw new Error(_context.t0);

                            case 14:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 11]]);
            }));

            function getUserFollowedQuestion(_x) {
                return _ref.apply(this, arguments);
            }

            return getUserFollowedQuestion;
        }()

        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Question Id
         * @param {string} data.usr_id - User Id
         * @returns {Promise}
         * @desc
         * - This method is used to update follow status of given user for given question
         * - Tables Used : {@link CassandraTables#ew_qry_flw}
         */

    }, {
        key: 'updateFollower',
        value: function updateFollower(data) {
            var defer = Q.defer();
            var qry;
            var qry_params;

            qry = 'insert into ' + ew_qry_flw_tbl + ' (usr_id,qry_id,flw_ts) values (?,?,?)';
            qry_params = [data.usr_id, data.qry_id, new Date()];

            //query execute
            cassconn.execute(qry, qry_params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Question Id
         * @param {string} data.usr_id - User Id
         * @returns {Promise}
         * @desc
         * - This method is used to delete follower if user choose to unfollow the question
         * - Tables Used : {@link CassandraTables#ew_qry_flw}
         */

    }, {
        key: 'deleteFollower',
        value: function deleteFollower(data) {
            // data = {qry_id : '' , usr_id : ''}
            var defer = Q.defer();
            var qry;
            var qry_params;
            qry = 'delete from ' + ew_qry_flw_tbl + ' where qry_id=? and usr_id=?';
            qry_params = [data.qry_id, data.usr_id];

            //query execute
            cassconn.execute(qry, qry_params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Question Id
         * @param {string} data.ans_usr_id - Userid , who posted the answer
         * @param {uuid} data.ans_id - answer id
         * @returns {Promise}
         * @desc
         * - First we will get all follower using {@link QAFollow#getAllFollowers}
         * - Then for each follower we will add notification to {@link CassandraTables#ew_qry_flw_ntf}
         * - Tables Used : {@link CassandraTables#ew_qry_flw_ntf}
         */

    }, {
        key: 'updateNotifications',
        value: function updateNotifications(data) {
            // data = {qry_id,ans_usr_id,ans_id}
            var defer = Q.defer();
            var timeuuid_now = TimeUuid.now();
            var qry = "insert into " + ew_qry_flw_ntf_tbl + '(qry_id,usr_id,ans_usr_id,ans_id,ans_ts,ntf_vw_flg,ntf_typ,qry_text) values (?,?,?,?,?,?,?,?)';
            var qry_params;

            var queries = [];

            var followers_promise = this.getAllFollowers({
                qry_id: data.qry_id
            });

            //get all followers first
            followers_promise.then(function () {
                var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(followers) {
                    var queryOptions, i, follower;
                    return _regenerator2.default.wrap(function _callee2$(_context2) {
                        while (1) {
                            switch (_context2.prev = _context2.next) {
                                case 0:

                                    //iterate through all follower
                                    followers.forEach(function (v) {
                                        // prepare query params for all followers
                                        qry_params = [data.qry_id, v.usr_id, data.ans_usr_id, data.ans_id, timeuuid_now, false, 'follow', data.qry_title];
                                        if (v.usr_id !== data.ans_usr_id) {
                                            //push it to query array
                                            queries.push({
                                                query: qry,
                                                params: qry_params
                                            });
                                        }
                                    });

                                    //execute all queries all at once in batch
                                    queryOptions = {
                                        prepare: true,
                                        consistency: cassandra.types.consistencies.quorum
                                    };

                                    if (!(queries.length > 0)) {
                                        _context2.next = 16;
                                        break;
                                    }

                                    i = 0;

                                case 4:
                                    if (!(i < followers.length)) {
                                        _context2.next = 13;
                                        break;
                                    }

                                    follower = followers[i];

                                    if (!(follower.usr_id === data.ans_usr_id)) {
                                        _context2.next = 8;
                                        break;
                                    }

                                    return _context2.abrupt('continue', 10);

                                case 8:
                                    _context2.next = 10;
                                    return adminSendNotificationAPI.incrementSendNotificationCount({
                                        usr_id: follower.usr_id,
                                        type: 'qa'
                                    });

                                case 10:
                                    i++;
                                    _context2.next = 4;
                                    break;

                                case 13:

                                    cassconn.batch(queries, queryOptions, function (err, res, r) {
                                        if (err) {
                                            debug(err);
                                            defer.reject(err);
                                            return;
                                        }
                                        defer.resolve({});
                                    });
                                    _context2.next = 17;
                                    break;

                                case 16:
                                    defer.resolve({});

                                case 17:
                                case 'end':
                                    return _context2.stop();
                            }
                        }
                    }, _callee2, this);
                }));

                return function (_x2) {
                    return _ref2.apply(this, arguments);
                };
            }()).catch(function (err) {
                debug(err);
                defer.reject(err);
                return;
            });
            return defer.promise;
        }
    }, {
        key: 'getNotifications',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var usr_id, last_notif_id, _query, params, res;

                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                usr_id = data.usr_id, last_notif_id = data.last_notif_id;
                                _query = void 0;
                                params = void 0;

                                if (last_notif_id) {
                                    _query = 'select toTimestamp(ans_ts) as ans_timestamp,usr_id,ans_id,ans_ts,ans_usr_id,ntf_typ,ntf_vw_flg,qry_id,qry_text from ' + ew_qry_flw_ntf_tbl + ' where usr_id = ? and ans_ts < ? limit 5';
                                    params = [usr_id, last_notif_id];
                                } else {
                                    _query = 'select toTimestamp(ans_ts) as ans_timestamp,usr_id,ans_id,ans_ts,ans_usr_id,ntf_typ,ntf_vw_flg,qry_id,qry_text from ' + ew_qry_flw_ntf_tbl + ' where usr_id = ? limit 5';
                                    params = [usr_id];
                                }
                                _context3.next = 7;
                                return cassExecute(_query, params);

                            case 7:
                                res = _context3.sent;
                                return _context3.abrupt('return', res);

                            case 11:
                                _context3.prev = 11;
                                _context3.t0 = _context3['catch'](0);
                                throw new Error(_context3.t0.toString());

                            case 14:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 11]]);
            }));

            function getNotifications(_x3) {
                return _ref3.apply(this, arguments);
            }

            return getNotifications;
        }()
    }, {
        key: 'markFollowNotificationRead',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var ans_usr_id, ans_ts, usr_id, _query2, params, res;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                ans_usr_id = data.ans_usr_id, ans_ts = data.ans_ts, usr_id = data.usr_id;
                                _query2 = void 0;
                                params = void 0;


                                _query2 = 'update ' + ew_qry_flw_ntf_tbl + ' set ntf_vw_flg=true where usr_id = ? and ans_ts = ? and ans_usr_id = ?';
                                params = [usr_id, ans_ts, ans_usr_id];

                                _context4.next = 8;
                                return cassExecute(_query2, params);

                            case 8:
                                res = _context4.sent;
                                _context4.next = 11;
                                return adminSendNotificationAPI.decrementSendNotificationCount({
                                    type: 'qa',
                                    usr_id: usr_id
                                });

                            case 11:
                                return _context4.abrupt('return', res);

                            case 14:
                                _context4.prev = 14;
                                _context4.t0 = _context4['catch'](0);
                                throw new Error(_context4.t0.toString());

                            case 17:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 14]]);
            }));

            function markFollowNotificationRead(_x4) {
                return _ref4.apply(this, arguments);
            }

            return markFollowNotificationRead;
        }()
    }]);
    return QAFollow;
}();

module.exports = new QAFollow();