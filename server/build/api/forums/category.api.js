'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:forums");
var uuid = require('uuid');

var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var cassExecute = require(configPath.lib.utility).cassExecute;

var vt_qry_cats = 'ew1.vt_qry_cats';
var vt_qry_cats_crsmdl = 'ew1.vt_qry_cats_crsmdl';

/**
 * QA Category API
 */

var Categories = function () {
    function Categories() {
        (0, _classCallCheck3.default)(this, Categories);
    }

    (0, _createClass3.default)(Categories, [{
        key: 'getAllCategories',

        /**
         * @returns {Promise}
         * @desc
         * - This method is used to get all categories for qa
         * - Tables Used : {@link CassandraTables#vt_qry_cats}
         */
        value: function getAllCategories(data) {
            var defer = Q.defer();
            //select query
            var selectQry = 'select * from ' + vt_qry_cats;
            //execute
            cassconn.execute(selectQry, null, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.cat_name - Category Name
         * @param {string} data.cat_desc - Category Description
         * @param {uuid} data.cat_id - Category Id <Optional>
         * @returns {Promise}
         * @desc 
         * - This method is used to save/update category
         * - Tables Used : {@link CassandraTables#vt_qry_cats}
         */

    }, {
        key: 'saveCategory',
        value: function saveCategory(data) {
            var defer = Q.defer();
            var randomUUID = void 0;
            var qry;
            var qry_params;
            if (data.cat_id) {
                //update
                qry = 'update ' + vt_qry_cats + ' set crs_id=?,mdl_id=?,crs_mdl_id=?,cat_name=?,cat_desc=?,crt_ts=? where cat_id=?';
                qry_params = [data.cat_name, data.cat_desc, new Date(), data.cat_id];
            } else {
                randomUUID = Uuid.random();
                //insert
                qry = 'insert into ' + vt_qry_cats + ' (cat_id,cat_name,cat_desc,crs_id,mdl_id,crs_mdl_id,crt_ts) values (?,?,?,?,?,?,?)';
                qry_params = [randomUUID, data.cat_name, data.cat_desc, data.crs_id, data.mdl_id, data.crs_mdl_id, new Date()];
            }

            cassconn.execute(qry, qry_params, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve((0, _extends3.default)({}, data, {
                    cat_id: randomUUID
                }));
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.cat_id - Category Id
         * @returns {Promise}
         * @desc 
         * - This method is used to delete category using id
         * - Tables Used : {@link CassandraTables#vt_qry_cats}
         */

    }, {
        key: 'deleteCategory',
        value: function deleteCategory(data) {
            var defer = Q.defer();
            var qry;
            var qry_params;
            qry = 'delete from ' + vt_qry_cats + ' where cat_id=?';
            qry_params = [data.cat_id];

            cassconn.execute(qry, qry_params, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }
    }, {
        key: 'findAndSaveCategoryBasedOnCourseAndModuleId',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var findQuery, findParams, rows, res;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                findQuery = 'select * from ' + vt_qry_cats_crsmdl + ' where crs_mdl_id = ?';
                                findParams = [data.crs_mdl_id];
                                _context.next = 5;
                                return cassExecute(findQuery, findParams);

                            case 5:
                                rows = _context.sent;

                                if (!(rows && rows.length > 0)) {
                                    _context.next = 10;
                                    break;
                                }

                                return _context.abrupt('return', rows[0]);

                            case 10:
                                _context.next = 12;
                                return this.saveCategory(data);

                            case 12:
                                res = _context.sent;
                                return _context.abrupt('return', res);

                            case 14:
                                _context.next = 19;
                                break;

                            case 16:
                                _context.prev = 16;
                                _context.t0 = _context['catch'](0);
                                throw new Error(_context.t0);

                            case 19:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 16]]);
            }));

            function findAndSaveCategoryBasedOnCourseAndModuleId(_x) {
                return _ref.apply(this, arguments);
            }

            return findAndSaveCategoryBasedOnCourseAndModuleId;
        }()
    }]);
    return Categories;
}();

module.exports = new Categories();