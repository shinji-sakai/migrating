'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:cmnts");
var profileAPI = require(configPath.api.dashboard.profile);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassandra = require('cassandra-driver');
var TimeUuid = cassandra.types.TimeUuid;
var logger = require(configPath.lib.log_to_file);

var dashboardStatsAPI = require(configPath.api.dashboard.stats);
var profileAPI = require(configPath.api.dashboard.profile);
var adminSendNotificationAPI = require(configPath.api.admin.sendNotification);
// var commentAPI = require(configPath.api.dashboard.comment)

var forum_table = 'ew1.vt_forum_qrys';
var qa_ans_table = 'ew1.vt_usr_qry_ans';
var ew_qa_like_tbl = "ew1.ew_qa_like";
var ew_qa_like_stats_tbl = "ew1.ew_qa_like_stats";
var ew_qry_flw_ntf_tbl = 'ew1.ew_qry_flw_ntf';

var FETCH_ANS_LIMIT = 5;

var QAAnswer = function () {
    function QAAnswer() {
        (0, _classCallCheck3.default)(this, QAAnswer);
    }

    (0, _createClass3.default)(QAAnswer, [{
        key: 'saveAnswer',

        /**
         * @param {JSON} data
         * @param {uuid} data.usr_id - Usrid who posted the answer
         * @param {uuid} data.pst_id - QA Que ID to which answer is related
         * @param {set} data.cmnt_atch - Array Of Answer Attachment
         * @param {uuid} data.cmnt_cmnt_id - Not related
         * @param {set} data.cmnt_img - Image attached to answer
         * @param {text} data.cmnt_lvl - Not related
         * @param {text} data.cmnt_txt - Answer Text
         * @param {set} data.cmnt_vid - Video attached to answer.
         * @returns {Promise}
         * @desc
         * - This function is used to insert new qa answer
         * - If Comment source is qa-ans then we will increment dashboard stats for qa questions answered. See {@link DashboardStats#incrementForumQuestionAnsweredCounter}
         * - Else we will increment dashboard stats for comments. See {@link DashboardStats#incrementCommentCounter}
         * - Tables Used : {@link CassandraTables#vt_usr_qry_ans}
         */
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var defer, qry_id, ans_id, ans_ts, ans_txt, usr_id, timeuuid_now, insertPost, insertPost_args, getQuestionQry, getQuestionQryParams, que, author_id, notif_qry, notif_params;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                defer = Q.defer();
                                qry_id = data.pst_id;
                                ans_id = TimeUuid.now();
                                ans_ts = new Date();
                                ans_txt = data.cmnt_txt;
                                usr_id = data.usr_id;
                                timeuuid_now = TimeUuid.now();
                                insertPost = 'INSERT INTO ' + qa_ans_table + ' (qry_id,ans_id,ans_create_dt,ans_txt,usr_id)  ' + 'VALUES(?, ?, ?, ?, ?);';
                                insertPost_args = [qry_id, ans_id, ans_ts, ans_txt, usr_id];

                                //save answer

                                _context.next = 12;
                                return cassExecute(insertPost, insertPost_args);

                            case 12:
                                getQuestionQry = 'select * from ' + forum_table + ' where qry_id = ?';
                                getQuestionQryParams = [qry_id];

                                // get qa question to find author

                                _context.next = 16;
                                return cassExecute(getQuestionQry, getQuestionQryParams);

                            case 16:
                                que = _context.sent;
                                author_id = void 0;

                                que = que[0];
                                if (que) {
                                    author_id = que.usr_id;
                                }

                                if (!(usr_id !== author_id)) {
                                    _context.next = 27;
                                    break;
                                }

                                // notification query
                                notif_qry = "insert into " + ew_qry_flw_ntf_tbl + '(qry_id,usr_id,ans_usr_id,ans_id,ans_ts,ntf_vw_flg,ntf_typ,qry_text) values (?,?,?,?,?,?,?,?)';
                                notif_params = [qry_id, author_id, usr_id, ans_id, timeuuid_now, false, 'owner', que.qry_title];
                                // save notification

                                _context.next = 25;
                                return cassExecute(notif_qry, notif_params);

                            case 25:
                                _context.next = 27;
                                return adminSendNotificationAPI.incrementSendNotificationCount({
                                    usr_id: author_id,
                                    type: 'qa'
                                });

                            case 27:
                                _context.next = 29;
                                return dashboardStatsAPI.incrementForumQuestionAnsweredCounter({
                                    usr_id: usr_id
                                });

                            case 29:
                                return _context.abrupt('return', (0, _extends3.default)({
                                    cmnt_id: ans_id,
                                    ans_ts: ans_ts
                                }, data));

                            case 32:
                                _context.prev = 32;
                                _context.t0 = _context['catch'](0);

                                logger.debug(_context.t0);
                                throw new Error(_context.t0.toString());

                            case 36:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 32]]);
            }));

            function saveAnswer(_x) {
                return _ref.apply(this, arguments);
            }

            return saveAnswer;
        }()
    }, {
        key: 'getAllAnswersOfQuestion',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                var qry_id, usr_id, last_ans, query, params, res, result, i, ans, ans_details;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                qry_id = data.qry_id, usr_id = data.usr_id, last_ans = data.last_ans;
                                query = void 0;
                                params = void 0;


                                if (last_ans) {
                                    query = " select * from " + qa_ans_table + " where qry_id = ? and ans_id < ? limit 5";
                                    params = [qry_id, last_ans];
                                } else {
                                    query = " select * from " + qa_ans_table + " where qry_id = ? limit 5";
                                    params = [qry_id];
                                }

                                _context2.next = 7;
                                return cassExecute(query, params);

                            case 7:
                                res = _context2.sent;


                                res = res || [];

                                if (!(res.length <= 0)) {
                                    _context2.next = 11;
                                    break;
                                }

                                return _context2.abrupt('return', []);

                            case 11:
                                result = [];
                                i = 0;

                            case 13:
                                if (!(i < res.length)) {
                                    _context2.next = 22;
                                    break;
                                }

                                ans = res[i];
                                _context2.next = 17;
                                return this.getAnswerOfQuestionUsingId({
                                    qry_id: qry_id,
                                    ans_id: ans["ans_id"],
                                    usr_id: usr_id
                                });

                            case 17:
                                ans_details = _context2.sent;

                                if (ans_details && ans_details.length === 1) {
                                    result.push(ans_details[0]);
                                }

                            case 19:
                                i++;
                                _context2.next = 13;
                                break;

                            case 22:
                                return _context2.abrupt('return', result);

                            case 25:
                                _context2.prev = 25;
                                _context2.t0 = _context2['catch'](0);

                                logger.debug(_context2.t0);
                                throw new Error(_context2.t0);

                            case 29:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[0, 25]]);
            }));

            function getAllAnswersOfQuestion(_x2) {
                return _ref2.apply(this, arguments);
            }

            return getAllAnswersOfQuestion;
        }()
    }, {
        key: 'getOneLatestAnswerOfQuestion',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var qry_id, query, params, res, result, ans, ans_details;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                qry_id = data.qry_id;
                                query = " select * from " + qa_ans_table + " where qry_id = ?";
                                params = [qry_id];
                                _context3.next = 6;
                                return cassExecute(query, params);

                            case 6:
                                res = _context3.sent;


                                res = res || [];

                                if (!(res.length <= 0)) {
                                    _context3.next = 10;
                                    break;
                                }

                                return _context3.abrupt('return', {});

                            case 10:
                                result = {};
                                ans = res[0] || {};
                                _context3.next = 14;
                                return this.getAnswerOfQuestionUsingId({
                                    qry_id: qry_id,
                                    ans_id: ans["ans_id"]
                                });

                            case 14:
                                ans_details = _context3.sent;

                                if (ans_details && ans_details.length === 1) {
                                    result.ans_id = ans_details[0].ans_id;
                                    result.usr_details = ans_details[0].usr_details;
                                    result.ans_create_dt = ans_details[0].ans_create_dt;
                                }

                                return _context3.abrupt('return', (0, _extends3.default)({}, result, {
                                    totalAnswers: res.length
                                }));

                            case 19:
                                _context3.prev = 19;
                                _context3.t0 = _context3['catch'](0);

                                logger.debug(_context3.t0);
                                throw new Error(_context3.t0);

                            case 23:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 19]]);
            }));

            function getOneLatestAnswerOfQuestion(_x3) {
                return _ref3.apply(this, arguments);
            }

            return getOneLatestAnswerOfQuestion;
        }()
    }, {
        key: 'getAnswerOfQuestionUsingId',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var qry_id, ans_id, usr_id, query, params, res, author_id, users_detail, likeRes, ansLikeDislikeCount;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                qry_id = data.qry_id, ans_id = data.ans_id, usr_id = data.usr_id;
                                query = " select * from " + qa_ans_table + " where qry_id = ? and ans_id = ?";
                                params = [qry_id, ans_id];
                                _context4.next = 6;
                                return cassExecute(query, params);

                            case 6:
                                res = _context4.sent;

                                if (!(res && res.length > 0)) {
                                    _context4.next = 30;
                                    break;
                                }

                                // res = res.filter((v, i) => {
                                //     if (v.cmnt_id === ans_id)
                                //         return true;
                                //     return false;
                                // })
                                author_id = res[0]["usr_id"];
                                _context4.next = 11;
                                return profileAPI.getUsersShortDetails({
                                    usr_ids: [author_id]
                                });

                            case 11:
                                users_detail = _context4.sent;


                                users_detail = users_detail[0] || {};

                                delete users_detail["usr_email"];
                                delete users_detail["usr_role"];
                                delete users_detail["usr_ph"];

                                res[0]["usr_details"] = users_detail;

                                if (!usr_id) {
                                    _context4.next = 22;
                                    break;
                                }

                                _context4.next = 20;
                                return this.getLikeDislikeByCmntId({
                                    qry_id: qry_id,
                                    usr_id: usr_id,
                                    cmnt_id: ans_id
                                });

                            case 20:
                                likeRes = _context4.sent;

                                if (likeRes && likeRes.length > 0) {
                                    res[0]["usr_likes_dislike"] = likeRes[0] || {};
                                } else {
                                    res[0]["usr_likes_dislike"] = {};
                                }

                            case 22:
                                if (!ans_id) {
                                    _context4.next = 27;
                                    break;
                                }

                                _context4.next = 25;
                                return this.getLikeDislikeCountByCmntID({
                                    qry_id: qry_id,
                                    cmnt_id: ans_id
                                });

                            case 25:
                                ansLikeDislikeCount = _context4.sent;

                                if (ansLikeDislikeCount && ansLikeDislikeCount.length > 0) {
                                    res[0]["ans_like_dislike_count"] = ansLikeDislikeCount[0] || {};
                                } else {
                                    res[0]["ans_like_dislike_count"] = {};
                                }

                            case 27:
                                return _context4.abrupt('return', res);

                            case 30:
                                return _context4.abrupt('return', []);

                            case 31:
                                _context4.next = 37;
                                break;

                            case 33:
                                _context4.prev = 33;
                                _context4.t0 = _context4['catch'](0);

                                logger.debug(_context4.t0);
                                throw new Error(_context4.t0);

                            case 37:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 33]]);
            }));

            function getAnswerOfQuestionUsingId(_x4) {
                return _ref4.apply(this, arguments);
            }

            return getAnswerOfQuestionUsingId;
        }()

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - QA Query ID
         * @param {uuid} data.cmnt_id - Answer Id which we want to edit
         * @param {timestamp} data.ans_ts - Answer Timestamp
         * @param {Array} data.cmnt_atch - Answer Attachments
         * @param {Array} data.cmnt_vid - Answer Video Attachments
         * @param {Array} data.cmnt_img - Answer Images Attachments
         * @param {string} data.cmnt_txt - Answer Text
         * @returns {Promise}
         * @desc
         * - This function is used to update comment data for given comment id
         * - Tables Used : {@link CassandraTables#vt_usr_qry_ans}
         */

    }, {
        key: 'editAnswer',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var qry_id, ans_id, ans_ts, ans_txt, updatePost, updatePost_args;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;
                                qry_id = data.pst_id;
                                ans_id = data.cmnt_id;
                                ans_ts = new Date();
                                ans_txt = data.cmnt_txt;
                                updatePost = 'update ' + qa_ans_table + ' set ans_txt = ?,ans_update_dt = ? where ans_id = ? and qry_id = ?';
                                updatePost_args = [ans_txt, ans_ts, ans_id, qry_id];
                                _context5.next = 9;
                                return cassExecute(updatePost, updatePost_args);

                            case 9:
                                return _context5.abrupt('return', data);

                            case 12:
                                _context5.prev = 12;
                                _context5.t0 = _context5['catch'](0);

                                logger.debug(_context5.t0);
                                throw new Error(_context5.t0);

                            case 16:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 12]]);
            }));

            function editAnswer(_x5) {
                return _ref5.apply(this, arguments);
            }

            return editAnswer;
        }()

        /**
         * @param {JSON} data
         * @param {uuid} data.pst_id - QA Query ID
         * @param {uuid} data.cmnt_id - Answer Id which we want to delete
         * @param {timestamp} data.ans_ts - Answer Timestamp
         * @param {string} data.usr_id - Author of comment
         * @returns {Promise}
         * @desc
         * - This function is used to delete given answer id
         * - We will decrement dashboard stats answer counter. See {@link DashboardStats#decrementForumQuestionAnsweredCounter}
         * - Tables Used : {@link CassandraTables#vt_usr_qry_ans}
         */

    }, {
        key: 'deleteAnswer',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var qry_id, ans_id, usr_id, deletePost, deletePost_args;
                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;
                                qry_id = data.pst_id;
                                ans_id = data.cmnt_id;
                                usr_id = data.usr_id;
                                deletePost = 'delete from ' + qa_ans_table + ' where qry_id = ? and ans_id = ?';
                                deletePost_args = [qry_id, ans_id];
                                _context6.next = 8;
                                return cassExecute(deletePost, deletePost_args);

                            case 8:
                                _context6.next = 10;
                                return dashboardStatsAPI.decrementForumQuestionAnsweredCounter({
                                    usr_id: usr_id
                                });

                            case 10:
                                return _context6.abrupt('return', data);

                            case 13:
                                _context6.prev = 13;
                                _context6.t0 = _context6['catch'](0);

                                logger.debug(_context6.t0);
                                throw new Error(_context6.t0);

                            case 17:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 13]]);
            }));

            function deleteAnswer(_x6) {
                return _ref6.apply(this, arguments);
            }

            return deleteAnswer;
        }()
    }, {
        key: 'likeDislike',
        value: function likeDislike(data) {
            var defer = Q.defer();
            var qry_id = data.qry_id || null;
            var usr_id = data.usr_id || 'n/a';
            var cmnt_id = data.cmnt_id || null;
            var crt_ts = new Date();
            var like_dlike_flg = data.like_dlike_flg;

            if (!qry_id || !cmnt_id) {
                return { err: "qry_id and cmnt_id parameters required" };
            }

            var insert = 'insert into ' + ew_qa_like_tbl + ' (qry_id,usr_id,cmnt_id,crt_ts,like_dlike_flg) values(?,?,?,?,?)';
            var insert_args = [qry_id, usr_id, cmnt_id, crt_ts, like_dlike_flg];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            cassconn.execute(insert, insert_args, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }
    }, {
        key: 'getLikeDislikeByCmntId',
        value: function () {
            var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
                var qry_id, usr_id, cmnt_id, select, select_args, queryOptions, res;
                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                _context7.prev = 0;
                                qry_id = data.qry_id || null;
                                usr_id = data.usr_id || 'n/a';
                                cmnt_id = data.cmnt_id || null;

                                if (!(!qry_id || !cmnt_id)) {
                                    _context7.next = 6;
                                    break;
                                }

                                return _context7.abrupt('return', { err: "qry_id and cmnt_id parameters required" });

                            case 6:
                                select = 'select * from ' + ew_qa_like_tbl + ' where qry_id = ? and  usr_id = ? and cmnt_id = ?';
                                select_args = [qry_id, usr_id, cmnt_id];
                                queryOptions = {
                                    prepare: true,
                                    consistency: cassandra.types.consistencies.quorum
                                };
                                _context7.next = 11;
                                return cassExecute(select, select_args, queryOptions);

                            case 11:
                                res = _context7.sent;
                                return _context7.abrupt('return', res);

                            case 15:
                                _context7.prev = 15;
                                _context7.t0 = _context7['catch'](0);
                                throw new Error(_context7.t0);

                            case 18:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this, [[0, 15]]);
            }));

            function getLikeDislikeByCmntId(_x7) {
                return _ref7.apply(this, arguments);
            }

            return getLikeDislikeByCmntId;
        }()
    }, {
        key: 'updateLikeDislikeCount',
        value: function updateLikeDislikeCount(data) {
            var defer = Q.defer();

            //data = {qry_id : qry_id , cmnt_id : cmnt_id , like : -1/0/1 , dislike : -1/0/1}

            var qry_id = data.qry_id || null;
            var cmnt_id = data.cmnt_id || null;

            var like_cnt_str = ' like_cnt = like_cnt + ' + (data.like || 0);
            var dlike_cnt_str = ' dlike_cnt = dlike_cnt + ' + (data.dislike || 0);
            var where_clause = ' where qry_id = ? and cmnt_id = ?';

            var update_like = 'update ' + ew_qa_like_stats_tbl + ' set ' + like_cnt_str + "," + dlike_cnt_str + where_clause;
            var update_args = [qry_id, cmnt_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };
            cassconn.execute(update_like, update_args, queryOptions, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }
    }, {
        key: 'getLikeDislikeCountByCmntID',
        value: function () {
            var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
                var qry_id, cmnt_id, select_like, select_args, queryOptions, res;
                return _regenerator2.default.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                _context8.prev = 0;

                                //data = {qry_id , cmnt_id}

                                qry_id = data.qry_id || null;
                                cmnt_id = data.cmnt_id || null;

                                if (!(!qry_id || !cmnt_id)) {
                                    _context8.next = 5;
                                    break;
                                }

                                return _context8.abrupt('return', { err: "qry_id and cmnt_id parameters required" });

                            case 5:
                                select_like = 'select * from ' + ew_qa_like_stats_tbl + ' where qry_id = ? and cmnt_id = ?';
                                select_args = [qry_id, cmnt_id];
                                queryOptions = {
                                    prepare: true,
                                    consistency: cassandra.types.consistencies.quorum
                                };
                                _context8.next = 10;
                                return cassExecute(select_like, select_args, queryOptions);

                            case 10:
                                res = _context8.sent;
                                return _context8.abrupt('return', res);

                            case 14:
                                _context8.prev = 14;
                                _context8.t0 = _context8['catch'](0);
                                throw new Error(_context8.t0);

                            case 17:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, this, [[0, 14]]);
            }));

            function getLikeDislikeCountByCmntID(_x8) {
                return _ref8.apply(this, arguments);
            }

            return getLikeDislikeCountByCmntID;
        }()
    }]);
    return QAAnswer;
}();

module.exports = new QAAnswer();