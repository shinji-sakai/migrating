'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:forums");
var uuid = require('uuid');

var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var asy_lib = require('async');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var TimeUuid = cassandra.types.TimeUuid;

var FollowAPI = require(configPath.api.forums.follow);
var profile_api = require(configPath.api.dashboard.profile);
var postAPI = require(configPath.api.dashboard.post);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var qaAnsAPI = require(configPath.api.forums.ans);

var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

var forum_table = 'ew1.vt_forum_qrys';
var forum_table_read = 'ew1.vt_forum_qrys_read';
var trend_cnt = 'ew1.vt_qry_trend';
var trend_vws = 'ew1.vt_qry_vws';
var qry_stats = 'ew1.ew_qry_stats';
var qry_usr_stats = 'ew1.ew_qry_usr_stats';
var ew_qry_ans_ltr_tbl = 'ew1.ew_qry_ans_ltr';
var ew_qry_flw_ntf_tbl = 'ew1.ew_qry_flw_ntf';

var ew_del_rsn_tbl = 'ew1.ew_del_rsn';
var ew_complain_tbl = 'ew1.ew_cmpln_rsn';

var ew_qa_qry_counter_tbl = "ew1.ew_qa_qry_counter";

var QA_QUESTION_IDS = function QA_QUESTION_IDS() {
    return "qa-question-ids";
};
var NO_OF_QA_QUESTION_TO_FETCH = 5;

/**
 * QA API
 */

var QA = function () {
    function QA() {
        (0, _classCallCheck3.default)(this, QA);
    }

    (0, _createClass3.default)(QA, [{
        key: 'saveQuestion',

        /**
         * @param {JSON} data
         * @param {string} data.qry_title - Question Description
         * @param {string} data.usr_id - Id of user who posted the question
         * @param {Array} data.qry_tags - Question Categories
         * @param {Array} data.qry_txt - Question Description
         * @param {Array} data.qry_pst_id - Post Id , if question is posted from careerbook
         * @returns {Promise}
         * @desc
         * - This method is used to save QA Question
         * - We will store new question id into `qa-question-ids` redis list also
         * - Increment dashboard stats counter for user {@link DashboardStats#incrementForumQuestionAskedCounter}
         * - Tables Used : {@link CassandraTables#vt_forum_qrys}
         */
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var defer, qry_id, qry_title, qry_ts, usr_id, qry_tags, qry_txt, qry_pst_id, insertQry, insertQry_args;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;

                                // var qa_count = await this.getQAQuestionCounter();

                                defer = Q.defer();
                                qry_id = TimeUuid.now();
                                qry_title = data.qry_title;
                                qry_ts = new Date();
                                usr_id = data.usr_id;
                                qry_tags = data.qry_tags;
                                qry_txt = data.qry_txt;
                                qry_pst_id = data.qry_pst_id;
                                //insert query

                                insertQry = 'insert into ' + forum_table + ' (qry_id,qry_tags,qry_title,qry_ts,qry_txt,usr_id,qry_pst_id,qry_bucket) values (?,?,?,?,?,?,?,?);';
                                //insert params

                                insertQry_args = [qry_id, qry_tags, qry_title, qry_ts, qry_txt, usr_id, qry_pst_id, '1'];
                                //execute

                                _context.next = 13;
                                return cassExecute(insertQry, insertQry_args);

                            case 13:
                                _context.next = 15;
                                return dashboardStatsAPI.incrementForumQuestionAskedCounter({
                                    usr_id: usr_id
                                });

                            case 15:
                                return _context.abrupt('return', {
                                    qry_id: qry_id
                                });

                            case 18:
                                _context.prev = 18;
                                _context.t0 = _context['catch'](0);

                                logger.debug(_context.t0);
                                throw new Error(_context.t0);

                            case 22:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 18]]);
            }));

            function saveQuestion(_x) {
                return _ref.apply(this, arguments);
            }

            return saveQuestion;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.qry_title - Question Description
         * @param {string} data.qry_id - Question Id which we want to update
         * @returns {Promise}
         * @desc
         * - This method is used to update already post Question
         * - Tables Used : {@link CassandraTables#vt_forum_qrys}
         */

    }, {
        key: 'updateQuestion',
        value: function updateQuestion(data) {
            var defer = Q.defer();
            var qry_title = data.qry_title;
            var qry_id = data.qry_id;
            var qry_ts = new Date();

            //update query
            var updateQry = 'update ' + forum_table + ' SET qry_title=?,qry_ts=? where qry_id=?;';
            //update params
            var updateQry_args = [qry_title, qry_ts, qry_id];
            //execute
            cassconn.execute(updateQry, updateQry_args, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.qry_id - Question Id which we want to delete
         * @param {string} data.usr_id - Id of user who posted the question
         * @param {string} data.qry_pst_id - this is id of post from which this question is shared For ex. CB
         * @param {string} data.del_rsn - Reason , why user want to delete the question
         * @returns {Promise}
         * @desc
         * - This method is used to delete Posted Question
         * - If there is any linked post with this question , it will update that also. See {@link CBPost#getPostData} and {@link CBPost#edit}
         * - We will delete `deleted question id` from `qa-question-ids` redis list also
         * - Decrement Dashboard Question Asked counter {@link DashboardStats#decrementForumQuestionAskedCounter}
         * - Tables Used : {@link CassandraTables#vt_forum_qrys} , {@link CassandraTables#ew_del_rsn}
         */

    }, {
        key: 'deleteQuestion',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                var defer, qry_id, qry_pst_id, deleteQry, deleteQry_args, qry, params, queries, queryOptions;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                defer = Q.defer();
                                qry_id = data.qry_id;
                                qry_pst_id = data.qry_pst_id; // this is id of post from which this question is shared

                                //update post in vt_posts and vt_posts_crt_by
                                // TODO

                                if (qry_pst_id) {
                                    // postAPI.getPostData({
                                    //     postId: qry_pst_id
                                    // })
                                    // .then(function(d) {
                                    //     if (d.length > 0) {
                                    //         var post = d[0];
                                    //         //remove qry id ref from post
                                    //         post.pst_qry_id = null;
                                    //         post.pst_qry_desc = null;
                                    //         postAPI.edit(post);
                                    //     }
                                    // })
                                    // .catch(function(err){
                                    //     logger.debug(err);
                                    // })
                                }

                                _context2.prev = 4;

                                //delete question from qry table
                                deleteQry = 'delete from ' + forum_table + ' where qry_id=?;';
                                deleteQry_args = [qry_id];

                                //insert reason of deleting question into delete reason table

                                qry = "insert into " + ew_del_rsn_tbl + '(usr_id,del_ts,del_id,del_rsn,del_typ) values (?,?,?,?,?)';
                                params = [data.usr_id, new Date(), data.qry_id, data.del_rsn, 'qry'];
                                queries = [{
                                    query: deleteQry,
                                    params: deleteQry_args
                                }, {
                                    query: qry,
                                    params: params
                                }];

                                //query execute

                                queryOptions = {
                                    prepare: true,
                                    consistency: cassandra.types.consistencies.quorum
                                };

                                // exectue dele query and insert del-rsn query

                                _context2.next = 13;
                                return cassBatch(queries, queryOptions);

                            case 13:
                                _context2.next = 15;
                                return dashboardStatsAPI.decrementForumQuestionAskedCounter({
                                    usr_id: data.usr_id
                                });

                            case 15:
                                return _context2.abrupt('return', {});

                            case 18:
                                _context2.prev = 18;
                                _context2.t0 = _context2['catch'](4);

                                logger.debug(_context2.t0);
                                throw new Error(_context2.t0);

                            case 22:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[4, 18]]);
            }));

            function deleteQuestion(_x2) {
                return _ref2.apply(this, arguments);
            }

            return deleteQuestion;
        }()

        /**
         * @param {JSON} data
         * @param {int} data.no_of_questions - No of question to fetch
         * @returns {Promise}
         * @desc
         * - This method is used to get first few requested number of question
         * - First we will get question id from redis
         * - then we will get those question details from cassandra using {@link QA#getQuestions}
         * - Generally this method will be called when user refresh the page
         */

    }, {
        key: 'getFirstFewQuestions',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var no_of_questions, query, rows, last_que_id, question_ids, requested_questions;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                no_of_questions = data.no_of_questions;

                                no_of_questions = parseInt(no_of_questions || NO_OF_QA_QUESTION_TO_FETCH);
                                // let question_ids = await redisCli.lrangeAsync(QA_QUESTION_IDS(),0, no_of_questions - 1);
                                // if(question_ids && question_ids.length <= 0){
                                //     return []
                                // }
                                query = 'select * from ' + forum_table_read + ' limit ' + no_of_questions;
                                _context3.next = 6;
                                return cassExecute(query, null);

                            case 6:
                                rows = _context3.sent;

                                if (!(rows && rows.length > 0)) {
                                    _context3.next = 16;
                                    break;
                                }

                                last_que_id = rows[rows.length - 1].qry_id;
                                question_ids = rows.map(function (v, i) {
                                    return v.qry_id;
                                });
                                _context3.next = 12;
                                return this.getQuestions({
                                    qry_ids: question_ids
                                });

                            case 12:
                                requested_questions = _context3.sent;
                                return _context3.abrupt('return', {
                                    questions: requested_questions || [],
                                    last_que_id: last_que_id
                                });

                            case 16:
                                return _context3.abrupt('return', {
                                    questions: []
                                });

                            case 17:
                                _context3.next = 23;
                                break;

                            case 19:
                                _context3.prev = 19;
                                _context3.t0 = _context3['catch'](0);

                                logger.debug(_context3.t0);
                                throw new Error(_context3.t0);

                            case 23:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 19]]);
            }));

            function getFirstFewQuestions(_x3) {
                return _ref3.apply(this, arguments);
            }

            return getFirstFewQuestions;
        }()

        /**
         * @param {JSON} data
         * @param {int} data.no_of_questions - No of question to fetch
         * @param {int} data.start_question_id - This will be the id of question from where we want to start fetching next question, start_question_id will not be taked in next questions
         * @returns {Promise}
         * @desc
         * - This method is used to get next few requested number of question from given question id
         * - First we will get question id from redis
         * - then we will get those question details from cassandra using {@link QA#getQuestions}
         * - Generally this method will be called when user is scrolling in qa page
         */

    }, {
        key: 'getNextFewQuestions',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var no_of_questions, start_question_id, last_que_id, query, params, rows, _last_que_id, question_ids, requested_questions;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                no_of_questions = data.no_of_questions, start_question_id = data.start_question_id, last_que_id = data.last_que_id;

                                no_of_questions = parseInt(no_of_questions || NO_OF_QA_QUESTION_TO_FETCH);
                                // let question_ids = await redisCli.lrangeAsync(QA_QUESTION_IDS(),0,-1);
                                // let pos = question_ids.indexOf(start_question_id);
                                // if(pos > -1){
                                //     question_ids = question_ids.slice(pos+1,pos+1+no_of_questions)
                                //     if(question_ids && question_ids.length <= 0){
                                //         return []
                                //     }
                                //     let requested_questions = await this.getQuestions({
                                //         qry_ids : question_ids
                                //     })
                                //     return requested_questions || []
                                // }else{
                                //     return []
                                // }
                                query = void 0;
                                params = void 0;

                                if (data.last_que_id) {
                                    query = 'select * from ' + forum_table_read + ' where qry_id < ? and qry_bucket = ? limit ' + no_of_questions;
                                    params = [last_que_id, '1'];
                                } else {
                                    query = 'select * from ' + forum_table_read + ' limit ' + no_of_questions;
                                    params = null;
                                }
                                _context4.next = 8;
                                return cassExecute(query, params);

                            case 8:
                                rows = _context4.sent;

                                if (!(rows && rows.length > 0)) {
                                    _context4.next = 18;
                                    break;
                                }

                                _last_que_id = rows[rows.length - 1].qry_id;
                                question_ids = rows.map(function (v, i) {
                                    return v.qry_id;
                                });
                                _context4.next = 14;
                                return this.getQuestions({
                                    qry_ids: question_ids
                                });

                            case 14:
                                requested_questions = _context4.sent;
                                return _context4.abrupt('return', {
                                    questions: requested_questions || [],
                                    last_que_id: _last_que_id
                                });

                            case 18:
                                return _context4.abrupt('return', {
                                    questions: []
                                });

                            case 19:
                                _context4.next = 25;
                                break;

                            case 21:
                                _context4.prev = 21;
                                _context4.t0 = _context4['catch'](0);

                                logger.debug(_context4.t0);
                                throw new Error(_context4.t0);

                            case 25:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 21]]);
            }));

            function getNextFewQuestions(_x4) {
                return _ref4.apply(this, arguments);
            }

            return getNextFewQuestions;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.qry_id - Question Id which we want to delete
         * @param {string} data.usr_id - Id of user who posted the question
         * @param {string} data.qry_pst_id - this is id of post from which this question is shared For ex. CB
         * @param {string} data.del_rsn - Reason , why user want to delete the question
         * @returns {Promise}
         * @desc
         * - This function will be called when user will see perticular question , we will update question view count
         * - Tables Used : {@link CassandraTables#vt_qry_trend}
         */

    }, {
        key: 'updateTrendingCounts',
        value: function updateTrendingCounts(data) {
            var defer = Q.defer();
            //update query
            var insertQry = 'update ' + trend_cnt + ' set vw_cnt=vw_cnt+1 where qry_id = ?;';
            //query params
            var insertQry_args = [data.qry_id];
            //execute
            cassconn.execute(insertQry, insertQry_args, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }

        /**
         * @returns {Promise}
         * @desc
         * - This function used to get all trending queries
         * - Tables Used : {@link CassandraTables#vt_qry_trend}
         */

    }, {
        key: 'getTrendingQrys',
        value: function getTrendingQrys(data) {
            var defer = Q.defer();
            var insertQry = 'select * from ' + trend_cnt;
            cassconn.execute(insertQry, null, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.qry_id - Question Id
         * @param {string} data.usr_id - Id of user
         * @returns {Promise}
         * @desc
         * - This method will update question view timestamp of user
         * - Tables Used : {@link CassandraTables#vt_qry_vws}
         */

    }, {
        key: 'updateTrendingViews',
        value: function updateTrendingViews(data) {
            var defer = Q.defer();
            var insertQry = 'insert into ' + trend_vws + ' (qry_id,usr_id,vw_ts) values(?,?,?)';
            var insertQry_args = [data.qry_id, data.usr_id, new Date()];
            cassconn.execute(insertQry, insertQry_args, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.qry_id - Question Id
         * @param {number} data.qry_ans - number of query answer
         * @param {number} data.qry_dnv - number of query downvote
         * @param {number} data.qry_flw - number of query follower
         * @param {number} data.qry_shr - number of query share
         * @param {number} data.qry_upv - number of query upvote
         * @param {number} data.qry_vws - number of query views
         * @returns {Promise}
         * @desc
         * - This method will update question stats
         * - Tables Used : {@link CassandraTables#ew_qry_stats}
         */

    }, {
        key: 'updateQuestionStatsCounter',
        value: function updateQuestionStatsCounter(data) {
            var defer = Q.defer();

            var qry_id = data.qry_id;
            var set_str_arr = [];

            if (data.qry_ans != undefined) {
                //if qry_ans is set
                set_str_arr.push('qry_ans = qry_ans + ' + data.qry_ans);
            }
            if (data.qry_dnv != undefined) {
                //if qry_dnv is set
                set_str_arr.push('qry_dnv = qry_dnv + ' + data.qry_dnv);
            }
            if (data.qry_flw != undefined) {
                //if qry_flw is set
                set_str_arr.push('qry_flw = qry_flw + ' + data.qry_flw);
            }
            if (data.qry_shr != undefined) {
                //if qry_shr is set
                set_str_arr.push('qry_shr = qry_shr + ' + data.qry_shr);
            }
            if (data.qry_upv != undefined) {
                //if qry_upv is set
                set_str_arr.push('qry_upv = qry_upv + ' + data.qry_upv);
            }
            if (data.qry_vws != undefined) {
                //if qry_vws is set
                set_str_arr.push('qry_vws = qry_vws + ' + data.qry_vws);
            }

            var updateQry = 'update ' + qry_stats + ' set ' + set_str_arr.join(',') + ' where qry_id = ?';
            var updateQry_args = [qry_id];
            cassconn.execute(updateQry, updateQry_args, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string|Array} data.qry_id - Question(s) Id
         * @returns {Promise}
         * @desc
         * - This method is used to get question(s) stats using question id
         * - Tables Used : {@link CassandraTables#ew_qry_stats}
         */

    }, {
        key: 'getQuestionStatsCounter',
        value: function getQuestionStatsCounter(data) {
            var defer = Q.defer();

            //query id
            var qry_id = data.qry_id; // can be array of query id

            // logger.debug(typeof qry_id)

            // qry_id can be array or string
            var qry_ids = [].concat(qry_id);

            //merge with ,(comma)
            var qry_ids_str = "(" + qry_ids.join(",") + ")";

            // logger.debug(qry_ids_str)

            var selectQry = 'select * from ' + qry_stats + ' where qry_id in ' + qry_ids_str;

            //query cassandra db
            cassconn.execute(selectQry, null, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.qry_id - Question Id
         * @returns {Promise}
         * @desc
         * - This method is used to get question details using question id
         * - It will also fetch question stats based on question id. See {@link QA#getQuestionStatsCounter}
         * - Tables Used : {@link CassandraTables#vt_forum_qrys}
         */

    }, {
        key: 'getQuestion',
        value: function getQuestion(data) {
            var defer = Q.defer();
            var self = this;

            var insertQry = 'select * from ' + forum_table + ' where qry_id = ?';
            var insertQry_args = [data.qry_id];

            cassconn.execute(insertQry, insertQry_args, function () {
                var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(err, res, r) {
                    var row, allUsersShortDetails, allUsersShortDetailsMap;
                    return _regenerator2.default.wrap(function _callee5$(_context5) {
                        while (1) {
                            switch (_context5.prev = _context5.next) {
                                case 0:
                                    if (!err) {
                                        _context5.next = 4;
                                        break;
                                    }

                                    debug(err);
                                    defer.reject(err);
                                    return _context5.abrupt('return');

                                case 4:
                                    if (!(res.rows && res.rows.length > 0)) {
                                        _context5.next = 8;
                                        break;
                                    }

                                    //if we have question
                                    row = JSON.parse(JSON.stringify(res.rows[0]));
                                    _context5.next = 10;
                                    break;

                                case 8:
                                    defer.resolve([]);
                                    return _context5.abrupt('return');

                                case 10:
                                    if (!row["usr_id"]) {
                                        _context5.next = 18;
                                        break;
                                    }

                                    _context5.next = 13;
                                    return profile_api.getUsersShortDetails({
                                        usr_ids: [row["usr_id"]]
                                    });

                                case 13:
                                    allUsersShortDetails = _context5.sent;

                                    allUsersShortDetails = allUsersShortDetails || [];
                                    allUsersShortDetailsMap = {};

                                    allUsersShortDetails.map(function (v, i) {
                                        delete v["usr_email"];
                                        delete v["usr_role"];
                                        delete v["usr_ph"];
                                        allUsersShortDetailsMap[v.usr_id] = v;
                                    });
                                    row["usr_details"] = allUsersShortDetailsMap[row["usr_id"]];

                                case 18:

                                    //get question stats
                                    //
                                    self.getQuestionStatsCounter({
                                        qry_id: row.qry_id
                                    }).then(function (resrow) {
                                        //convert to JSON
                                        if (resrow.length > 0) {
                                            var obj = JSON.parse(JSON.stringify(resrow[0]));
                                            //attach qry_stats to row obj
                                            row['qry_stats'] = obj;
                                        } else {
                                            row['qry_stats'] = {};
                                        }
                                        defer.resolve(row);
                                        return;
                                    }).catch(function (err) {
                                        debug("err....", err);
                                        defer.reject(err);
                                        return;
                                    });

                                case 19:
                                case 'end':
                                    return _context5.stop();
                            }
                        }
                    }, _callee5, this);
                }));

                return function (_x5, _x6, _x7) {
                    return _ref5.apply(this, arguments);
                };
            }());
            return defer.promise;
        }

        /**
         * @returns {Promise}
         * @desc
         * - This method is used to get all questions
         * - It will also fetch all question stats based on question id. See {@link QA#getQuestionStatsCounter}
         * - Tables Used : {@link CassandraTables#vt_forum_qrys}
         */

    }, {
        key: 'getAllQuestion',
        value: function getAllQuestion(data) {
            var self = this;

            var defer = Q.defer();
            var selectQry = 'select * from ' + forum_table;
            //select all questions
            cassconn.execute(selectQry, null, function (err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }

                //convert to json
                var allQuestions = JSON.parse(JSON.stringify(res.rows));

                //get all qurestion ids to fetch stats of those question
                var qry_ids = allQuestions.map(function (v) {
                    return v.qry_id;
                });

                //get all question stats
                self.getQuestionStatsCounter({
                    qry_id: qry_ids
                }).then(function (resrow) {
                    //convert to JSON
                    if (resrow.length > 0) {
                        //covert to json
                        var obj = JSON.parse(JSON.stringify(resrow));

                        //attach qry_stats to all questions
                        allQuestions.forEach(function (v, i) {
                            var stat = obj.filter(function (t) {
                                if (t.qry_id === v.qry_id) {
                                    return t;
                                };
                            })[0];
                            allQuestions[i]['qry_stats'] = stat;
                        });
                    }
                    defer.resolve(allQuestions);
                }).catch(function (err) {
                    debug("err getQuestionStats....", err);
                    defer.reject(err);
                });
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {Array} data.qry_ids - Questions Id
         * @returns {Promise}
         * @desc
         * - This method is used to get question details for given question ids
         * - It will also fetch question stats based on question id. See {@link QA#getQuestionStatsCounter}
         * - Tables Used : {@link CassandraTables#vt_forum_qrys}
         */

    }, {
        key: 'getQuestions',
        value: function getQuestions(data) {
            var self = this;

            var defer = Q.defer();
            var qry_ids = data.qry_ids || [];

            if (qry_ids.length <= 0) {
                return;
            }

            var str_qry_ids = "(" + qry_ids.join(",") + ")";
            var selectQry = 'select * from ' + forum_table + ' where qry_id in ' + str_qry_ids;
            //select all questions
            cassconn.execute(selectQry, null, function () {
                var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(err, res, r) {
                    var allQuestions, allUsersId, allUsersShortDetails, allUsersShortDetailsMap, i, id;
                    return _regenerator2.default.wrap(function _callee6$(_context6) {
                        while (1) {
                            switch (_context6.prev = _context6.next) {
                                case 0:
                                    if (!err) {
                                        _context6.next = 3;
                                        break;
                                    }

                                    defer.reject(err);
                                    return _context6.abrupt('return');

                                case 3:

                                    //convert to json
                                    allQuestions = JSON.parse(JSON.stringify(res.rows));


                                    allQuestions = allQuestions || [];

                                    allUsersId = allQuestions.map(function (v, i) {
                                        return v.usr_id;
                                    });

                                    if (!(allUsersId && allUsersId.length > 0)) {
                                        _context6.next = 15;
                                        break;
                                    }

                                    _context6.next = 9;
                                    return profile_api.getUsersShortDetails({
                                        usr_ids: allUsersId
                                    });

                                case 9:
                                    allUsersShortDetails = _context6.sent;

                                    allUsersShortDetails = allUsersShortDetails || [];
                                    allUsersShortDetailsMap = {};

                                    allUsersShortDetails.map(function (v, i) {
                                        delete v["usr_email"];
                                        delete v["usr_role"];
                                        delete v["usr_ph"];
                                        allUsersShortDetailsMap[v.usr_id] = v;
                                    });
                                    console.log(allUsersShortDetailsMap);
                                    allQuestions = allQuestions.map(function (v, i) {
                                        v["usr_details"] = allUsersShortDetailsMap[v["usr_id"]];
                                        return v;
                                    });

                                case 15:
                                    i = 0;

                                case 16:
                                    if (!(i < allQuestions.length)) {
                                        _context6.next = 25;
                                        break;
                                    }

                                    id = allQuestions[i]["qry_id"];
                                    _context6.next = 20;
                                    return qaAnsAPI.getOneLatestAnswerOfQuestion({
                                        qry_id: id
                                    });

                                case 20:
                                    r = _context6.sent;

                                    allQuestions[i]["latest_answer"] = r;

                                case 22:
                                    i++;
                                    _context6.next = 16;
                                    break;

                                case 25:

                                    self.getQuestionStatsCounter({
                                        qry_id: data.qry_ids
                                    }).then(function (resrow) {
                                        //convert to JSON
                                        if (resrow.length > 0) {
                                            //covert to json
                                            var obj = JSON.parse(JSON.stringify(resrow));

                                            //attach qry_stats to all questions
                                            allQuestions.forEach(function (v, i) {
                                                var stat = obj.filter(function (t) {
                                                    if (t.qry_id === v.qry_id) {
                                                        return t;
                                                    };
                                                })[0];
                                                allQuestions[i]['qry_stats'] = stat;
                                            });
                                        }
                                        defer.resolve(allQuestions);
                                    }).catch(function (err) {
                                        debug("err getQuestionStats....", err);
                                        defer.reject(err);
                                    });

                                case 26:
                                case 'end':
                                    return _context6.stop();
                            }
                        }
                    }, _callee6, this);
                }));

                return function (_x8, _x9, _x10) {
                    return _ref6.apply(this, arguments);
                };
            }());
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Question Id
         * @param {string} data.usr_id - User Id
         * @returns {Promise}
         * @desc
         * - This method is used to update question stats for particular user
         * - If user follow/unfollow question then we will update follow tables also. See {@link QAFollow#updateFollower} and {@link QAFollow#deleteFollower}
         * - It will also increment dashboard stats for question views. See {@link DashboardStats#incrementForumQuestionViewsCounter}
         * - Tables Used : {@link CassandraTables#ew_qry_usr_stats}
         */

    }, {
        key: 'updateUserQuestionStats',
        value: function () {
            var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
                var defer, usr_id, qry_id, followQueryPromise, crt_ts, set_str_arr, set_params, selectQry, selectQry_args;
                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                defer = Q.defer();

                                if (data) {
                                    _context7.next = 3;
                                    break;
                                }

                                return _context7.abrupt('return');

                            case 3:
                                usr_id = data.usr_id;
                                qry_id = data.qry_id;
                                crt_ts = new Date();
                                set_str_arr = [];
                                set_params = [];

                                if (!(data.like_dlike_flg != undefined)) {
                                    _context7.next = 13;
                                    break;
                                }

                                //if `like dislike flag` is set
                                set_str_arr.push('like_dlike_flg = ?');
                                set_params.push(data.like_dlike_flg);

                                _context7.next = 40;
                                break;

                            case 13:
                                if (!(data.qry_ans != undefined)) {
                                    _context7.next = 18;
                                    break;
                                }

                                //if `query ans` is set
                                set_str_arr.push('qry_ans = ?');
                                set_params.push(data.qry_ans);

                                _context7.next = 40;
                                break;

                            case 18:
                                if (!(data.qry_flw != undefined)) {
                                    _context7.next = 24;
                                    break;
                                }

                                //if `query follow` is set
                                set_str_arr.push('qry_flw = ?');
                                set_params.push(data.qry_flw);

                                if (data.qry_flw === true) {
                                    //user is following this question
                                    followQueryPromise = FollowAPI.updateFollower({
                                        qry_id: qry_id,
                                        usr_id: usr_id
                                    });
                                } else if (data.qry_flw === false) {
                                    //user is unfollowing this question
                                    followQueryPromise = FollowAPI.deleteFollower({
                                        qry_id: qry_id,
                                        usr_id: usr_id
                                    });
                                }

                                _context7.next = 40;
                                break;

                            case 24:
                                if (!(data.qry_shr != undefined)) {
                                    _context7.next = 29;
                                    break;
                                }

                                //if `query share` is set
                                set_str_arr.push('qry_shr = ?');
                                set_params.push(data.qry_shr);

                                _context7.next = 40;
                                break;

                            case 29:
                                if (!(data.qry_vws != undefined)) {
                                    _context7.next = 40;
                                    break;
                                }

                                //if `query views` is set
                                set_str_arr.push('qry_vws = ?');
                                set_params.push(data.qry_vws);

                                _context7.prev = 32;
                                _context7.next = 35;
                                return dashboardStatsAPI.incrementForumQuestionViewsCounter({
                                    usr_id: usr_id
                                });

                            case 35:
                                _context7.next = 40;
                                break;

                            case 37:
                                _context7.prev = 37;
                                _context7.t0 = _context7['catch'](32);

                                debug(_context7.t0);

                            case 40:

                                //`create timestamp` update
                                set_str_arr.push('crt_ts=?');
                                set_params.push(crt_ts);

                                //query
                                selectQry = 'update ' + qry_usr_stats + ' set ' + set_str_arr.join(',') + ' where usr_id = ? and qry_id = ?';
                                selectQry_args = set_params.concat([usr_id, qry_id]);

                                //query cassandra db

                                cassconn.execute(selectQry, selectQry_args, function (err, res, r) {
                                    if (err) {
                                        defer.reject(err);
                                        return;
                                    }
                                    if (followQueryPromise) {
                                        //if follow query is set then wait for it to resolve
                                        followQueryPromise.then(function () {
                                            defer.resolve([]);
                                        }).catch(function (err) {
                                            debug("updateUserQuestionStats...", err);
                                            defer.reject(err);
                                        });
                                    } else {
                                        // direct resolve if follow query is not set
                                        defer.resolve([]);
                                    }
                                });

                                return _context7.abrupt('return', defer.promise);

                            case 46:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this, [[32, 37]]);
            }));

            function updateUserQuestionStats(_x11) {
                return _ref7.apply(this, arguments);
            }

            return updateUserQuestionStats;
        }()

        /**
         * @param {JSON} data
         * @param {Array} data.qry_ids - Questions Id
         * @param {string} data.usr_id - User Id
         * @returns {Promise}
         * @desc
         * - Get User Question Stats for all given questions id
         * - Tables Used : {@link CassandraTables#ew_qry_usr_stats}
         */

    }, {
        key: 'getUserQuestionStats',
        value: function getUserQuestionStats(data) {

            var defer = Q.defer();
            var usr_id = data.usr_id;
            var qry_ids = data.qry_ids;

            function getStats(qry_id, cb) {
                //query cassandra db
                var selectQry = 'select * from ' + qry_usr_stats + ' where usr_id = ? and qry_id = ?';
                var params = [usr_id, qry_id];
                cassconn.execute(selectQry, params, function (err, res, r) {
                    if (err) {
                        cb(err, null);
                        return;
                    }
                    var parsed = JSON.parse(JSON.stringify(res.rows));
                    cb(null, parsed[0]);
                });
            }

            asy_lib.map(qry_ids, getStats, function (err, results) {

                if (err) {
                    defer.reject(err);
                    return;
                }
                var obj = {};
                var res = qry_ids.map(function (v, i) {
                    obj[v] = results[i];
                    return obj;
                });
                // obj = {
                //     qry_id_1 : <stats for user>,
                //     qry_id_2 : <stats for user>,
                //     ...
                // }
                defer.resolve(obj);
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {Object} data.usr_stats_update - Object
         * @param {Object} data.qry_stats_update - Object
         * @returns {Promise}
         * @desc
         * - Update Question stats using {@link QA#updateQuestionStatsCounter} and user stats using {@link QA#updateUserQuestionStats}
         */

    }, {
        key: 'updateQuestionStats',
        value: function updateQuestionStats(data) {
            //data = {usr_stats_update : {} , qry_stats_update : {}}
            var prms_arr = [this.updateQuestionStatsCounter(data.qry_stats_update), this.updateUserQuestionStats(data.usr_stats_update)];
            return Q.all(prms_arr);
        }

        /**
         * @param {JSON} data
         * @param {uuid} data.qry_id - Question Id
         * @param {string} data.usr_id - User Id
         * @param {boolean} data.ans_flg - Answer Later Flag
         * @returns {Promise}
         * @desc
         * - Update status of Answer later flag for given question and user
         * - Tables Used : {@link CassandraTables#ew_qry_ans_ltr}
         */

    }, {
        key: 'updateAnswerLater',
        value: function updateAnswerLater(data) {
            // data = {usr_id , qry_id , ans_flg , crt_ts}
            var defer = Q.defer();
            var usr_id = data.usr_id;
            var qry_id = data.qry_id;
            var ans_flg = data.ans_flg;
            var crt_ts = new Date();

            var qry = "update " + ew_qry_ans_ltr_tbl + ' set ans_flg = ? , crt_ts = ? where  usr_id = ? and  qry_id = ?';
            var params = [ans_flg, crt_ts, usr_id, qry_id];

            cassconn.execute(qry, params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.resolve(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id
         * @returns {Promise}
         * @desc
         * - First get all question ids which marked as answer later
         * - For all those question get question details and question stats using {@link QA#getQuestions}
         * - For all those question get user question stats using {@link QA#getUserQuestionStats}
         * - Merge all the results
         * - Tables Used : {@link CassandraTables#ew_qry_ans_ltr}
         */

    }, {
        key: 'getAnswerLater',
        value: function () {
            var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
                var usr_id, qry, params, rows, qry_ids, questions, questionMapping, userQuestionsStats, finalResult, i, ans_ltr_obj, question, userQuestionsStat, obj;
                return _regenerator2.default.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                _context8.prev = 0;
                                usr_id = data.usr_id;
                                qry = "select * from  " + ew_qry_ans_ltr_tbl + ' where  usr_id = ?';
                                params = [usr_id];
                                _context8.next = 6;
                                return cassExecute(qry, params);

                            case 6:
                                rows = _context8.sent;


                                rows = rows.filter(function (v, i) {
                                    return v.ans_flg;
                                });

                                if (!(rows && rows.length <= 0)) {
                                    _context8.next = 10;
                                    break;
                                }

                                return _context8.abrupt('return', []);

                            case 10:
                                qry_ids = rows.map(function (v, i) {
                                    return v.qry_id;
                                });

                                //get required questions

                                _context8.next = 13;
                                return this.getQuestions({
                                    qry_ids: qry_ids
                                });

                            case 13:
                                questions = _context8.sent;

                                //create question mapping
                                questionMapping = {};

                                questions.map(function (v, i) {
                                    questionMapping[v.qry_id] = v;
                                });

                                //get required user question stats
                                _context8.next = 18;
                                return this.getUserQuestionStats({
                                    usr_id: usr_id,
                                    qry_ids: qry_ids
                                });

                            case 18:
                                userQuestionsStats = _context8.sent;
                                finalResult = [];

                                for (i = 0; i < rows.length; i++) {
                                    ans_ltr_obj = rows[i];
                                    question = questionMapping[ans_ltr_obj.qry_id];
                                    userQuestionsStat = userQuestionsStats[ans_ltr_obj.qry_id];
                                    obj = (0, _extends3.default)({}, ans_ltr_obj, question, {
                                        userQuestionsStat: userQuestionsStat
                                    });

                                    finalResult.push(obj);
                                }

                                return _context8.abrupt('return', finalResult);

                            case 24:
                                _context8.prev = 24;
                                _context8.t0 = _context8['catch'](0);

                                debug(_context8.t0);
                                throw _context8.t0;

                            case 28:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, this, [[0, 24]]);
            }));

            function getAnswerLater(_x12) {
                return _ref8.apply(this, arguments);
            }

            return getAnswerLater;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id
         * @returns {Promise}
         * @desc
         * - Get all notifications for given user
         * - Get all Unique answer user id in those notification and get user short details for those users {@link CareerBook#getUsersShortDetails}
         * - Get question ids from those notification and for all those question get question details {@link QA#getQuestions}
         * - Merge all the results and send
         * - Tables Used : {@link CassandraTables#ew_qry_flw_ntf}
         */

    }, {
        key: 'getAllNotifications',
        value: function getAllNotifications(data) {
            // data = {usr_id : ''}
            var defer = Q.defer();
            var self = this;

            var qry = "select * from " + ew_qry_flw_ntf_tbl + ' where usr_id = ? ';
            var qry_params = [data.usr_id];
            debug("time start ........", Date.now());
            cassconn.execute(qry, qry_params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var rows = res.rows && res.rows.length > 0 && res.rows || [];
                var json_rows = JSON.parse(JSON.stringify(rows));

                // create array of users id
                var usr_ids = json_rows.map(function (v) {
                    return '' + v.ans_usr_id;
                });

                //create array of questions id
                var qry_ids = json_rows.map(function (v) {
                    return v.qry_id;
                });

                //get users short details like display name
                var user_promise = profile_api.getUsersShortDetails({
                    usr_ids: usr_ids
                });
                //get question details
                var question_promise = self.getQuestions({
                    qry_ids: qry_ids
                });

                // wait for both promise to resolve
                Q.all([user_promise, question_promise]).then(function (res) {
                    var usr_arr = res[0];
                    var qry_arr = res[1];
                    for (var i = 0; i < json_rows.length; i++) {
                        var que = json_rows[i];
                        //find question details
                        que["qry_details"] = qry_arr.filter(function (v) {
                            if (v.qry_id === que.qry_id) {
                                return true;
                            }
                            return false;
                        })[0];
                        //find ans user details
                        que["ans_usr_details"] = usr_arr.filter(function (v) {
                            if (v.usr_id === que.ans_usr_id) {
                                return true;
                            }
                            return false;
                        })[0];
                    }
                    // debug(que);
                    debug("time end ........", Date.now());
                    defer.resolve(json_rows);
                }).catch(function (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                });
            });

            return defer.promise;
        }

        /**
         * @param {JSON} data
         * @param {string} data.usr_id - User Id
         * @param {uuid} data.cmpln_id - Complain Id
         * @param {string} data.cmpln_typ - Complain Type (Wrong Question,etc...)
         * @param {string} data.cmpln_rsn - Complain Reason
         * @returns {Promise}
         * @desc
         * - Insert detail into table
         * - Tables Used : {@link CassandraTables#ew_cmpln_rsn}
         */

    }, {
        key: 'saveComplain',
        value: function saveComplain(data) {
            // data = {usr_id,cmpln_id,cmpln_typ,cmpln_rsn}
            var defer = Q.defer();
            var self = this;
            var qry = "insert into " + ew_complain_tbl + ' (usr_id,cmpln_id,cmpln_typ,cmpln_rsn,cmpln_ts) values (?,?,?,?,?)';
            var qry_params = [data.usr_id, data.cmpln_id, data.cmpln_typ, data.cmpln_rsn, new Date()];
            cassconn.execute(qry, qry_params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }

        /**
         * @returns {Promise}
         * @desc
         * - Get all complains
         * - Tables Used : {@link CassandraTables#ew_cmpln_rsn}
         */

    }, {
        key: 'getAllComplain',
        value: function getAllComplain(data) {
            // data = {}
            var defer = Q.defer();
            var self = this;

            var qry = "select * from " + ew_complain_tbl;
            cassconn.execute(qry, null, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });
            return defer.promise;
        }
    }, {
        key: 'incrementQAQuestionCounter',
        value: function () {
            var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9() {
                var updateQry;
                return _regenerator2.default.wrap(function _callee9$(_context9) {
                    while (1) {
                        switch (_context9.prev = _context9.next) {
                            case 0:
                                _context9.prev = 0;
                                updateQry = "update " + ew_qa_qry_counter_tbl + " set qry=qry + 1 where id = 'total_q_count'";
                                _context9.next = 4;
                                return cassExecute(updateQry);

                            case 4:
                                return _context9.abrupt('return');

                            case 7:
                                _context9.prev = 7;
                                _context9.t0 = _context9['catch'](0);

                                logger.debug(_context9.t0);
                                throw new Error(_context9.t0);

                            case 11:
                            case 'end':
                                return _context9.stop();
                        }
                    }
                }, _callee9, this, [[0, 7]]);
            }));

            function incrementQAQuestionCounter() {
                return _ref9.apply(this, arguments);
            }

            return incrementQAQuestionCounter;
        }()
    }, {
        key: 'decrementQAQuestionCounter',
        value: function () {
            var _ref10 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10() {
                var updateQry;
                return _regenerator2.default.wrap(function _callee10$(_context10) {
                    while (1) {
                        switch (_context10.prev = _context10.next) {
                            case 0:
                                _context10.prev = 0;
                                updateQry = "update " + ew_qa_qry_counter_tbl + " set qry=qry - 1 where id = 'total_q_count'";
                                _context10.next = 4;
                                return cassExecute(updateQry);

                            case 4:
                                return _context10.abrupt('return');

                            case 7:
                                _context10.prev = 7;
                                _context10.t0 = _context10['catch'](0);

                                logger.debug(_context10.t0);
                                throw new Error(_context10.t0);

                            case 11:
                            case 'end':
                                return _context10.stop();
                        }
                    }
                }, _callee10, this, [[0, 7]]);
            }));

            function decrementQAQuestionCounter() {
                return _ref10.apply(this, arguments);
            }

            return decrementQAQuestionCounter;
        }()
    }, {
        key: 'getQAQuestionCounter',
        value: function () {
            var _ref11 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11() {
                var selectQry, res;
                return _regenerator2.default.wrap(function _callee11$(_context11) {
                    while (1) {
                        switch (_context11.prev = _context11.next) {
                            case 0:
                                _context11.prev = 0;
                                selectQry = "select * from " + ew_qa_qry_counter_tbl + " where id = 'total_q_count'";
                                _context11.next = 4;
                                return cassExecute(selectQry);

                            case 4:
                                res = _context11.sent;

                                if (!(res && res.length > 0)) {
                                    _context11.next = 9;
                                    break;
                                }

                                return _context11.abrupt('return', res[0]["qry"]);

                            case 9:
                                return _context11.abrupt('return', 0);

                            case 10:
                                _context11.next = 16;
                                break;

                            case 12:
                                _context11.prev = 12;
                                _context11.t0 = _context11['catch'](0);

                                logger.debug(_context11.t0);
                                throw new Error(_context11.t0);

                            case 16:
                            case 'end':
                                return _context11.stop();
                        }
                    }
                }, _callee11, this, [[0, 12]]);
            }));

            function getQAQuestionCounter() {
                return _ref11.apply(this, arguments);
            }

            return getQAQuestionCounter;
        }()
    }, {
        key: 'getDetailedFollowNotifications',
        value: function () {
            var _ref12 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12(data) {
                var usr_id, last_notif_id, res, results, i, obj, ans_usr_info, r;
                return _regenerator2.default.wrap(function _callee12$(_context12) {
                    while (1) {
                        switch (_context12.prev = _context12.next) {
                            case 0:
                                _context12.prev = 0;
                                usr_id = data.usr_id, last_notif_id = data.last_notif_id;
                                _context12.next = 4;
                                return FollowAPI.getNotifications(data);

                            case 4:
                                res = _context12.sent;
                                results = [];

                                if (!(res && res.length > 0)) {
                                    _context12.next = 24;
                                    break;
                                }

                                i = 0;

                            case 8:
                                if (!(i < res.length)) {
                                    _context12.next = 21;
                                    break;
                                }

                                obj = res[i];
                                _context12.next = 12;
                                return profile_api.getUsersShortDetails({
                                    usr_ids: [obj.ans_usr_id]
                                });

                            case 12:
                                ans_usr_info = _context12.sent;

                                if (ans_usr_info[0]) {
                                    obj["ans_usr_info"] = ans_usr_info[0];
                                }
                                _context12.next = 16;
                                return this.getQuestion({
                                    qry_id: obj.qry_id
                                });

                            case 16:
                                r = _context12.sent;

                                if (r) {
                                    obj["details"] = r;
                                }

                            case 18:
                                i++;
                                _context12.next = 8;
                                break;

                            case 21:
                                return _context12.abrupt('return', res);

                            case 24:
                                return _context12.abrupt('return', []);

                            case 25:
                                _context12.next = 30;
                                break;

                            case 27:
                                _context12.prev = 27;
                                _context12.t0 = _context12['catch'](0);
                                throw new Error(_context12.t0);

                            case 30:
                            case 'end':
                                return _context12.stop();
                        }
                    }
                }, _callee12, this, [[0, 27]]);
            }));

            function getDetailedFollowNotifications(_x13) {
                return _ref12.apply(this, arguments);
            }

            return getDetailedFollowNotifications;
        }()
    }, {
        key: 'getDetailedUserFollowedQuestion',
        value: function () {
            var _ref13 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee13(data) {
                var usr_id, last_qry_id, res, qry_ids, results;
                return _regenerator2.default.wrap(function _callee13$(_context13) {
                    while (1) {
                        switch (_context13.prev = _context13.next) {
                            case 0:
                                _context13.prev = 0;
                                usr_id = data.usr_id, last_qry_id = data.last_qry_id;
                                _context13.next = 4;
                                return FollowAPI.getUserFollowedQuestion(data);

                            case 4:
                                res = _context13.sent;

                                if (!(res && res.length > 0)) {
                                    _context13.next = 13;
                                    break;
                                }

                                qry_ids = res.map(function (v, i) {
                                    return v.qry_id;
                                });
                                _context13.next = 9;
                                return this.getQuestions({
                                    qry_ids: qry_ids
                                });

                            case 9:
                                results = _context13.sent;
                                return _context13.abrupt('return', results);

                            case 13:
                                return _context13.abrupt('return', []);

                            case 14:
                                _context13.next = 19;
                                break;

                            case 16:
                                _context13.prev = 16;
                                _context13.t0 = _context13['catch'](0);
                                throw new Error(_context13.t0);

                            case 19:
                            case 'end':
                                return _context13.stop();
                        }
                    }
                }, _callee13, this, [[0, 16]]);
            }));

            function getDetailedUserFollowedQuestion(_x14) {
                return _ref13.apply(this, arguments);
            }

            return getDetailedUserFollowedQuestion;
        }()
    }]);
    return QA;
}();

module.exports = new QA();