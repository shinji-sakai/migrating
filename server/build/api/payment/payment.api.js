"use strict";

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var debug = require('debug')("app:api:payment:payment");
var Q = require("q");
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var logger = require(configPath.lib.log_to_file);
var moment = require('moment');

var userCoursesAPI = require(configPath.api.usercourses);
var paypalAPI = require(configPath.api.payment.paypal);
var profileAPI = require(configPath.api.dashboard.profile);
var smsAPI = require(configPath.api.sms.sms);
var mailAPI = require(configPath.api.mail);
var batchTimingAPI = require(configPath.api.batchCourse.batchTiming);
var batchTimingDashboardAPI = require(configPath.api.batchCourse.batchTimingDashboard);

var pricelistAPI = require(configPath.api.pricelist.pricelist);
var batchCourseAPI = require(configPath.api.batchCourse.batchCourse);

var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_payment_dtl_tbl = "ew1.ew_payment_dtl";
var ew_usr_enroll_bat_dtls_tbl = "ew1.ew_usr_enroll_bat_dtls";

function Payment() {}

//This method will be called once user payment is done(completed / pending)
Payment.prototype.savePaymentDetails = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var defer, usr_id, training_id, bat_id, buy_dt, pay_id, pay_method, pay_status, userShortDetails, courses, smsres, invoice_data, mail_res, resData, saveTODB;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						saveTODB = function saveTODB() {
							var defer = Q.defer();
							var qry = "insert into " + ew_payment_dtl_tbl + " (usr_id,training_id,bat_id,buy_dt,pay_id,pay_method,pay_status) values (?,?,?,?,?,?,?)";
							var params = [usr_id, training_id, bat_id, buy_dt, pay_id, pay_method, pay_status];
							console.log(params);
							cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
								if (err) {
									defer.reject(err);
									debug(err);
									return;
								}
								defer.resolve({});
							});
							return defer.promise;
						};

						defer = Q.defer();
						usr_id = data.usr_id;
						training_id = data.training_id;
						bat_id = data.bat_id;
						buy_dt = new Date();
						pay_id = data.pay_id;
						pay_method = data.pay_method;
						pay_status = data.pay_status;
						_context.prev = 9;
						_context.next = 12;
						return saveTODB();

					case 12:
						_context.next = 14;
						return profileAPI.getUserShortDetails({ usr_id: usr_id });

					case 14:
						userShortDetails = _context.sent;

						if (!(pay_status === 'completed')) {
							_context.next = 48;
							break;
						}

						_context.next = 18;
						return userCoursesAPI.savePurchasedCourse({ usr_id: usr_id, training_id: training_id });

					case 18:
						courses = _context.sent;
						_context.next = 21;
						return this.saveUserEnrolledBatchDetails({
							usr_id: usr_id,
							enroll_bat: bat_id,
							enroll_dt: new Date(),
							enroll_status: true
						});

					case 21:
						if (!userShortDetails.usr_ph) {
							_context.next = 31;
							break;
						}

						_context.prev = 22;
						_context.next = 25;
						return smsAPI.sendSuccessfulBuySMS(userShortDetails.usr_ph, training_id, usr_id);

					case 25:
						smsres = _context.sent;
						_context.next = 31;
						break;

					case 28:
						_context.prev = 28;
						_context.t0 = _context["catch"](22);

						logger.debug(_context.t0);

					case 31:
						if (!userShortDetails.usr_email) {
							_context.next = 46;
							break;
						}

						_context.prev = 32;
						_context.next = 35;
						return this.generateInvoiceDetails({
							training_id: training_id,
							bat_id: bat_id
						});

					case 35:
						invoice_data = _context.sent;

						invoice_data["to"] = userShortDetails.usr_email;
						invoice_data["dsp_nm"] = userShortDetails.dsp_nm;
						_context.next = 40;
						return mailAPI.sendSuccessfullBuyTrainingMail(invoice_data);

					case 40:
						mail_res = _context.sent;
						_context.next = 46;
						break;

					case 43:
						_context.prev = 43;
						_context.t1 = _context["catch"](32);

						logger.debug(_context.t1);

					case 46:
						_context.next = 59;
						break;

					case 48:
						if (!(pay_status === "fail")) {
							_context.next = 59;
							break;
						}

						if (!userShortDetails.usr_ph) {
							_context.next = 59;
							break;
						}

						_context.prev = 50;
						_context.next = 53;
						return smsAPI.paymentFailureSMS(userShortDetails.usr_ph, training_id);

					case 53:
						smsres = _context.sent;
						_context.next = 59;
						break;

					case 56:
						_context.prev = 56;
						_context.t2 = _context["catch"](50);

						logger.debug(_context.t2);

					case 59:
						resData = (0, _extends3.default)({}, data, {
							purchasedCourses: courses
						});

						defer.resolve(resData);
						_context.next = 66;
						break;

					case 63:
						_context.prev = 63;
						_context.t3 = _context["catch"](9);

						defer.reject(_context.t3);

					case 66:
						return _context.abrupt("return", defer.promise);

					case 67:
					case "end":
						return _context.stop();
				}
			}
		}, _callee, this, [[9, 63], [22, 28], [32, 43], [50, 56]]);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

Payment.prototype.savePurchasedTrainingDetails = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var defer, usr_id, training_id, bat_id, buy_dt, pay_id, pay_method, pay_status, qry, params;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						defer = Q.defer();
						usr_id = data.usr_id;
						training_id = data.training_id;
						bat_id = data.bat_id;
						buy_dt = new Date();
						pay_id = data.pay_id;
						pay_method = data.pay_method;
						pay_status = data.pay_status;
						qry = "insert into " + ew_payment_dtl_tbl + " (usr_id,training_id,bat_id,buy_dt,pay_id,pay_method,pay_status) values (?,?,?,?,?,?,?)";
						params = [usr_id, training_id, bat_id, buy_dt, pay_id, pay_method, pay_status];

						cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
							if (err) {
								defer.reject(err);
								debug(err);
								return;
							}
							defer.resolve({});
						});
						return _context2.abrupt("return", defer.promise);

					case 12:
					case "end":
						return _context2.stop();
				}
			}
		}, _callee2, this);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

Payment.prototype.saveUserEnrolledBatchDetails = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var defer, usr_id, enroll_bat, enroll_dt, enroll_status, qry, params;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						defer = Q.defer();
						usr_id = data.usr_id;
						enroll_bat = data.enroll_bat;
						enroll_dt = data.enroll_dt;
						enroll_status = data.enroll_status;
						qry = "insert into " + ew_usr_enroll_bat_dtls_tbl + " (usr_id,enroll_bat,enroll_dt,enroll_status) values (?,?,?,?)";
						params = [usr_id, enroll_bat, enroll_dt, enroll_status];

						cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
							if (err) {
								defer.reject(err);
								debug(err);
								return;
							}
							defer.resolve({});
						});
						return _context3.abrupt("return", defer.promise);

					case 9:
					case "end":
						return _context3.stop();
				}
			}
		}, _callee3, this);
	}));

	return function (_x3) {
		return _ref3.apply(this, arguments);
	};
}();

Payment.prototype.getUserEnrolledBatchDetails = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var defer, usr_id, qry, params, rows;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						defer = Q.defer();
						_context4.prev = 1;
						usr_id = data.usr_id;
						qry = "select * from " + ew_usr_enroll_bat_dtls_tbl + " where usr_id = ?";
						params = [usr_id];
						_context4.next = 7;
						return cassExecute(qry, params);

					case 7:
						rows = _context4.sent;
						return _context4.abrupt("return", rows);

					case 11:
						_context4.prev = 11;
						_context4.t0 = _context4["catch"](1);

						logger.debug(_context4.t0);
						throw _context4.t0;

					case 15:
					case "end":
						return _context4.stop();
				}
			}
		}, _callee4, this, [[1, 11]]);
	}));

	return function (_x4) {
		return _ref4.apply(this, arguments);
	};
}();

Payment.prototype.getPurchasedTrainingDetails = function () {
	var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
		var usr_id, qry, params, rows, i, v, batch_details, batch_dashboard_details;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.prev = 0;
						usr_id = data.usr_id;
						qry = "select * from " + ew_payment_dtl_tbl + " where usr_id = ?";
						params = [usr_id];
						_context5.next = 6;
						return cassExecute(qry, params);

					case 6:
						rows = _context5.sent;
						i = 0;

					case 8:
						if (!(i < rows.length)) {
							_context5.next = 21;
							break;
						}

						v = rows[i];
						_context5.next = 12;
						return batchTimingAPI.getTimingByBatId({ bat_id: v.bat_id });

					case 12:
						batch_details = _context5.sent;

						v["batch_details"] = batch_details[0];

						_context5.next = 16;
						return batchTimingDashboardAPI.getBatchTimingDashboardById({ bat_id: v.bat_id });

					case 16:
						batch_dashboard_details = _context5.sent;

						v["batch_dashboard_details"] = batch_dashboard_details[0];

					case 18:
						i++;
						_context5.next = 8;
						break;

					case 21:
						return _context5.abrupt("return", rows);

					case 24:
						_context5.prev = 24;
						_context5.t0 = _context5["catch"](0);

						debug(_context5.t0);
						throw _context5.t0;

					case 28:
					case "end":
						return _context5.stop();
				}
			}
		}, _callee5, this, [[0, 24]]);
	}));

	return function (_x5) {
		return _ref5.apply(this, arguments);
	};
}();

Payment.prototype.getPaymentDetails = function () {
	var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
		var defer, usr_id, training_id, bat_id, res, getFromDB;
		return _regenerator2.default.wrap(function _callee6$(_context6) {
			while (1) {
				switch (_context6.prev = _context6.next) {
					case 0:
						getFromDB = function getFromDB() {
							var defer = Q.defer();
							var qry = "select * from " + ew_payment_dtl_tbl + " where usr_id = ? and training_id =? and bat_id= ?";
							var params = [usr_id, training_id, bat_id];
							cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
								if (err) {
									defer.reject(err);
									debug(err);
									return;
								}
								var row = res.rows && res.rows[0] || {};
								var jsonRow = JSON.parse(JSON.stringify(row));
								defer.resolve(jsonRow);
							});
							return defer.promise;
						};

						defer = Q.defer();
						usr_id = data.usr_id;
						training_id = data.training_id;
						bat_id = data.bat_id;
						_context6.prev = 5;
						_context6.next = 8;
						return getFromDB();

					case 8:
						res = _context6.sent;

						defer.resolve(res);
						_context6.next = 15;
						break;

					case 12:
						_context6.prev = 12;
						_context6.t0 = _context6["catch"](5);

						defer.reject(_context6.t0);

					case 15:
						return _context6.abrupt("return", defer.promise);

					case 16:
					case "end":
						return _context6.stop();
				}
			}
		}, _callee6, this, [[5, 12]]);
	}));

	return function (_x6) {
		return _ref6.apply(this, arguments);
	};
}();

Payment.prototype.updatePaymentStatus = function () {
	var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
		var defer, usr_id, training_id, bat_id, pay_status, res, updateRecord;
		return _regenerator2.default.wrap(function _callee7$(_context7) {
			while (1) {
				switch (_context7.prev = _context7.next) {
					case 0:
						updateRecord = function updateRecord() {
							var defer = Q.defer();
							var qry = "update " + ew_payment_dtl_tbl + " set pay_status=? where usr_id = ? and training_id =? and bat_id= ?";
							var params = [pay_status, usr_id, training_id, bat_id];
							cassconn.execute(qry, params, { prepare: true }, function (err, res, r) {
								if (err) {
									defer.reject(err);
									debug(err);
									return;
								}
								defer.resolve({});
							});
							return defer.promise;
						};

						defer = Q.defer();
						usr_id = data.usr_id;
						training_id = data.training_id;
						bat_id = data.bat_id;
						pay_status = data.pay_status;
						_context7.prev = 6;
						_context7.next = 9;
						return updateRecord();

					case 9:
						res = _context7.sent;

						defer.resolve(data);
						_context7.next = 16;
						break;

					case 13:
						_context7.prev = 13;
						_context7.t0 = _context7["catch"](6);

						defer.reject(_context7.t0);

					case 16:
						return _context7.abrupt("return", defer.promise);

					case 17:
					case "end":
						return _context7.stop();
				}
			}
		}, _callee7, this, [[6, 13]]);
	}));

	return function (_x7) {
		return _ref7.apply(this, arguments);
	};
}();

// checkPaymentStatus
Payment.prototype.checkPaymentStatus = function () {
	var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
		var defer, usr_id, training_id, bat_id, obj, pay_details, payment_status_details;
		return _regenerator2.default.wrap(function _callee8$(_context8) {
			while (1) {
				switch (_context8.prev = _context8.next) {
					case 0:
						defer = Q.defer();
						usr_id = data.usr_id;
						training_id = data.training_id;
						bat_id = data.bat_id;
						_context8.prev = 4;
						obj = {
							usr_id: usr_id,
							training_id: training_id,
							bat_id: bat_id
						};
						_context8.next = 8;
						return this.getPaymentDetails(obj);

					case 8:
						pay_details = _context8.sent;
						payment_status_details = paypalAPI.checkPaymentStatus(pay_details.pay_id);
						// TODO
						// obj.pay_status = "";
						// this.updatePaymentStatus(obj);

						defer.resolve(payment_status_details);
						_context8.next = 16;
						break;

					case 13:
						_context8.prev = 13;
						_context8.t0 = _context8["catch"](4);

						defer.reject(_context8.t0);

					case 16:
						return _context8.abrupt("return", defer.promise);

					case 17:
					case "end":
						return _context8.stop();
				}
			}
		}, _callee8, this, [[4, 13]]);
	}));

	return function (_x8) {
		return _ref8.apply(this, arguments);
	};
}();

Payment.prototype.generateInvoiceDetails = function () {
	var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(data) {
		var vm, training_id, bat_id, batchTimings, batch, data_to_send, getDiscount, getNetPrice, getServiceTax, getTotalPrice, convertTimeTo_AM_PM;
		return _regenerator2.default.wrap(function _callee9$(_context9) {
			while (1) {
				switch (_context9.prev = _context9.next) {
					case 0:
						convertTimeTo_AM_PM = function convertTimeTo_AM_PM(time) {
							// time = 10:10
							var split_time = time.split(":");
							var hour = parseInt(split_time[0]);
							if (hour < 12) {
								return split_time[0] + ":" + split_time[1] + " AM";
							} else if (hour === 12) {
								return split_time[0] + ":" + split_time[1] + " PM";
							} else if (hour > 12) {
								return 12 - split_time[0] + ":" + split_time[1] + " PM";
							}
						};

						getTotalPrice = function getTotalPrice() {
							return getNetPrice() + getServiceTax();
						};

						getServiceTax = function getServiceTax() {
							var total = getNetPrice();
							var tax = getNetPrice() * 15 / 100;
							return tax;
						};

						getNetPrice = function getNetPrice() {
							var dis = getDiscount();
							var total = vm.pricelist.crs_price[vm.selectedCurrency];
							return Math.round(total - dis);
						};

						getDiscount = function getDiscount() {
							if (!vm.batch.should_apply_discount) {
								return 0;
							}
							var total = vm.pricelist.crs_price[vm.selectedCurrency];
							var discount = vm.pricelist.crs_price[vm.selectedCurrency] * parseInt(vm.batch.discount) / 100;
							return discount;
						};

						vm = {};

						vm.selectedCurrency = 'INR';
						_context9.prev = 7;

						data = data || {};
						training_id = data.training_id;
						bat_id = data.bat_id;
						_context9.next = 13;
						return pricelistAPI.getPricelistByCourseId({ crs_id: training_id });

					case 13:
						vm.pricelist = _context9.sent;
						_context9.next = 16;
						return batchCourseAPI.getBatchCourse({ crs_id: training_id });

					case 16:
						vm.courseInfo = _context9.sent;
						batchTimings = JSON.parse(JSON.stringify(vm.courseInfo.batchTiming));
						batch = batchTimings.filter(function (v, i) {
							if (v.bat_id === bat_id) {
								return true;
							}
							return false;
						})[0];

						vm.batch = batch;
						vm.discount = getDiscount();
						vm.netPrice = getNetPrice();
						vm.serviceTax = getServiceTax();
						vm.totalAmount = getTotalPrice();

						data_to_send = {
							training: vm.courseInfo.courseId,
							training_name: vm.courseInfo.courseName,
							training_start_date: moment(vm.batch.cls_start_dt).format('DD-MMM-YYYY'),
							training_from_week_day: vm.batch.frm_wk_dy,
							training_to_week_day: vm.batch.to_wk_dy,
							training_duration_count: vm.batch.no_wk_dy,
							training_duration_unit: vm.batch.wk_dy,
							training_start_time: convertTimeTo_AM_PM(vm.batch.cls_frm_tm),
							training_end_time: convertTimeTo_AM_PM(vm.batch.cls_to_tm),
							training_price: vm.pricelist.crs_price[vm.selectedCurrency],
							training_discount: vm.discount,
							training_net_price: vm.netPrice,
							training_total_saving: vm.discount,
							training_service_tax: vm.serviceTax,
							training_total_amount: vm.totalAmount
						};
						return _context9.abrupt("return", data_to_send);

					case 28:
						_context9.prev = 28;
						_context9.t0 = _context9["catch"](7);

						logger.debug(_context9.t0);
						throw _context9.t0;

					case 32:
					case "end":
						return _context9.stop();
				}
			}
		}, _callee9, this, [[7, 28]]);
	}));

	return function (_x9) {
		return _ref9.apply(this, arguments);
	};
}();

module.exports = new Payment();