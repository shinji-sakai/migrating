'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('./config.js');
var configPath = require("../../configPath.js");
var paypal = require('paypal-rest-sdk');
var debug = require('debug')("app:api:payment:paypal");
var Q = require("q");
var smsAPI = require(configPath.api.sms.sms);
var logger = require(configPath.lib.log_to_file);
var profileAPI = require(configPath.api.dashboard.profile);

function Paypal() {}

function getCreatePaymentRequestFormat(amount, currency, item_name, return_url) {
    var obj = {
        "intent": "sale",
        // "experience_profile_id": "XP-4Z2Q-NQVW-2ETU-22HT",
        "payer": {
            "payment_method": "paypal"
        },
        "transactions": [{
            "amount": {
                "total": amount,
                "currency": currency
            },
            "payment_options": {
                "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
            },
            "item_list": {
                "items": [{
                    "name": item_name,
                    "quantity": "1",
                    "price": amount,
                    "currency": currency
                }]
            }
        }],
        "note_to_payer": "Contact us for any questions on your order.",
        "redirect_urls": {
            "return_url": return_url,
            "cancel_url": return_url
        }
    };
    return obj;
}

/**
 * @param {JSON} data
 * @param {string} data
 * @returns {Promise}
 * @description 
 *     <ul>
 *         <li>Create payment and submit it to paypal api</li>
 *     </ul>
 */
Paypal.prototype.createPayment = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var defer, paypal_data, usr_id, training_id, user, smsres;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        defer = Q.defer();
                        paypal_data = getCreatePaymentRequestFormat(data.amount, data.currency, data.item_name, data.return_url);
                        usr_id = data.usr_id;
                        training_id = data.training_id;
                        _context.prev = 4;
                        _context.next = 7;
                        return profileAPI.getUserShortDetails({
                            usr_id: usr_id
                        });

                    case 7:
                        user = _context.sent;

                        if (!user.usr_ph) {
                            _context.next = 12;
                            break;
                        }

                        _context.next = 11;
                        return smsAPI.paymentStartedSMS(user.usr_ph);

                    case 11:
                        smsres = _context.sent;

                    case 12:
                        _context.next = 17;
                        break;

                    case 14:
                        _context.prev = 14;
                        _context.t0 = _context['catch'](4);

                        logger.debug(_context.t0);

                    case 17:

                        paypal.payment.create(paypal_data, function (error, payment) {
                            if (error) {
                                console.log(error);
                                defer.reject(error);
                            } else {
                                console.log("Create Payment Response");
                                debug(payment);
                                defer.resolve(payment);
                            }
                        });
                        return _context.abrupt('return', defer.promise);

                    case 19:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[4, 14]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

/**
 * @param {JSON} data
 * @param {string} data
 * @returns {Promise}
 * @description 
 *     <ul>
 *         <li>Execute given payment once payer approve the order</li>
 *     </ul>
 */
Paypal.prototype.executePayment = function (paymentId, data) {
    var defer = Q.defer();

    paypal.payment.execute(paymentId, {
        payer_id: data.payer_id
    }, function (error, payment) {
        if (error) {
            debug(error);
            defer.reject(error);
        } else {
            console.log("Execute Payment Response");
            debug(JSON.stringify(payment));
            defer.resolve(payment);
        }
    });
    return defer.promise;
};

/**
 * @param {JSON} data
 * @param {string} data
 * @returns {Promise}
 * @description 
 *     <ul>
 *         <li>Check payment status for given id</li>
 *     </ul>
 */
Paypal.prototype.checkPaymentStatus = function (paymentId) {
    var defer = Q.defer();
    paypal.payment.get(paymentId, function (error, payment) {
        if (error) {
            debug(error);
            defer.reject(error);
        } else {
            // console.log("Create Payment Response");
            // debug(payment);
            defer.resolve(payment);
        }
    });
    return defer.promise;
};

module.exports = new Paypal();