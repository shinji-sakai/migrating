'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var kafka = require('kafka-node');
var config = require('../../config.js');
var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);

function UserStats() {
    console.log("Producer Started ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
}

UserStats.prototype.sendStats = function (data) {
    var _this = this;

    if (!this.producer) {
        var Producer = kafka.Producer,
            KeyedMessage = kafka.KeyedMessage,
            client = new kafka.Client(config.ZOOKEEPER_KEY),
            km = new KeyedMessage('key', 'message');
        this.producer = new Producer(client);

        var payloads = [{ topic: data.topic, messages: data.stats, partition: 0 }];
        this.producer.on('ready', function () {
            _this.producer.send(payloads, function (err, data) {
                console.log(data);
            });
        });

        this.producer.on('error', function (err) {
            console.log('error', err);
        });
    } else {
        var payloads = [{ topic: data.topic, messages: data.stats, partition: 0 }];
        this.producer.send(payloads, function (err, data) {
            console.log(data);
        });
    }
};

UserStats.prototype.shouldFetchIdleEvents = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var page_url, connection, col, rows, found, i, pattern;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        page_url = data.page_url;
                        //get mongodb connetion

                        _context.next = 4;
                        return getConnection();

                    case 4:
                        connection = _context.sent;
                        _context.next = 7;
                        return connection.collection('idle_evt_pages');

                    case 7:
                        col = _context.sent;
                        _context.next = 10;
                        return col.find({}).toArray();

                    case 10:
                        rows = _context.sent;
                        found = false;
                        i = 0;

                    case 13:
                        if (!(i < rows.length)) {
                            _context.next = 21;
                            break;
                        }

                        //take pattern from mongo
                        pattern = rows[i].page_url;
                        //check pattern with page url

                        found = pattern.test(page_url);

                        if (!found) {
                            _context.next = 18;
                            break;
                        }

                        return _context.abrupt('break', 21);

                    case 18:
                        i++;
                        _context.next = 13;
                        break;

                    case 21:
                        return _context.abrupt('return', { shouldFetch: found });

                    case 24:
                        _context.prev = 24;
                        _context.t0 = _context['catch'](0);

                        logger.debug("shouldFetchIdleEvents...", _context.t0);
                        throw new Error(_context.t0);

                    case 28:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 24]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

UserStats.prototype.testPerformance = function () {
    var _this2 = this;

    var sub = getRedisClient("subscriber");
    sub.subscribe("start-test-performance");
    sub.on('message', function (channel, socketid) {
        var i = 0;
        var intervalId = setInterval(function () {
            i++;
            if (i > 100000) {
                clearInterval(intervalId);
            }
            var obj = {
                topic: 'test-perf3',
                stats: JSON.stringify({
                    number: i
                })
            };
            _this2.sendStats(obj);
        });
    });
};

// var t = new UserStats();
// t.testPerformance();

module.exports = new UserStats();