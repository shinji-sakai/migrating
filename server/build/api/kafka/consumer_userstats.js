'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var insertIntoOracleDb = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(columns, value) {
        var query, conn, res;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;

                        //example query: 
                        query = "insert into " + oracle_userstats_table + " " + columns + " values " + value;
                        // logger.debug(query);

                        _context2.next = 4;
                        return oracledb.getConnection();

                    case 4:
                        conn = _context2.sent;
                        _context2.next = 7;
                        return conn.execute(query, [], { autoCommit: true });

                    case 7:
                        res = _context2.sent;
                        _context2.next = 10;
                        return oracledb.releaseConnection(conn);

                    case 10:
                        return _context2.abrupt('return');

                    case 13:
                        _context2.prev = 13;
                        _context2.t0 = _context2['catch'](0);

                        logger.debug("insertIntoOracleDb error ..." + _context2.t0);
                        throw _context2.t0;

                    case 17:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 13]]);
    }));

    return function insertIntoOracleDb(_x2, _x3) {
        return _ref2.apply(this, arguments);
    };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var Q = require('q');
var debug = require('debug')('app:api:kafka:consumer_userstats');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var oracledb = require(configPath.dbconn.oracledb);
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);

var getRedisClient = require(configPath.dbconn.redisconn);
var pub = getRedisClient("publisher");

var userstats_table = 'ew1.usr_pg_stats';
var oracle_userstats_table = 'usr_pg_stats';

var groupId = "";
if (process.env.MODE != "DEV") {
    groupId = 'my-group1';
} else {
    groupId = "userstats-group2";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(client, [{ topic: 'userstats' }], {
    groupId: groupId
});

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

console.log("Consumer UserStats Started ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
consumer.on('message', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(message) {
        var columns, values, values_mapping, oracledb_values, oracledb_values_mapping, random_uuid, msg, parentKeys, i, key, val, childObject, childKeys, column_str, oracledb_values_str, value_str, _msg;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        // logger.debug("In userstats Consumer");
                        console.log(message);
                        columns = [];


                        columns = ["pg_vw_id", "browser_desc", "browser_nm", "browser_ver", "city", "country", "ctry_cd", "curr_pg", "device_pixel_ratio", "flash_install", "geo_loc_info", "hostname", "ip", "ip_gather_status", "isp", "key", "loc", "login_tkn", "offset", "org", "os_archi", "os_family", "os_ver", "partition", "pg_vw_tm", "prev_pg", "region", "res_height", "res_width", "tkn", "tm_zone", "usr_id", "zip_cd", "express_sess"];

                        values = [];
                        values_mapping = {};
                        oracledb_values = [];
                        oracledb_values_mapping = {};
                        random_uuid = Uuid.random();

                        // columns.push("pg_vw_id");

                        values.push(random_uuid);
                        values_mapping["pg_vw_id"] = random_uuid;
                        oracledb_values_mapping["pg_vw_id"] = '\'' + random_uuid + '\'';
                        oracledb_values.push('\'' + random_uuid + '\'');

                        if (!message.value) {
                            _context.next = 18;
                            break;
                        }

                        //convert to json
                        msg = JSON.parse(message.value);

                        if (!(msg.typ === "channel")) {
                            _context.next = 16;
                            break;
                        }

                        return _context.abrupt('return');

                    case 16:

                        //get all parent keys
                        parentKeys = Object.keys(msg || {});
                        //iterate through parent keys

                        for (i = 0; i < parentKeys.length; i++) {
                            key = parentKeys[i];
                            val = msg[key];


                            if ((typeof val === 'undefined' ? 'undefined' : (0, _typeof3.default)(val)) === "object") {
                                //parent key value is object
                                childObject = val;
                                //get all child keys

                                childKeys = Object.keys(childObject || {});
                                //concat into columns array
                                // columns = columns.concat(childKeys);
                                //push all columns(childkeys) value

                                childKeys.forEach(function (k, i) {
                                    var v = childObject[k];
                                    values.push('' + v);
                                    values_mapping[k] = v || '';
                                    oracledb_values.push('\'' + v + '\'');
                                    oracledb_values_mapping[k] = '\'' + v + '\'';
                                });
                            } else {
                                //if parent key value is other than object
                                // columns.push(key);
                                values.push('' + val);
                                values_mapping[key] = val || '';

                                if (key === "login_tkn") {
                                    oracledb_values.push('\'' + val.substring(1, val.length - 1) + '\'');
                                    oracledb_values_mapping[key] = '\'' + val.substring(1, val.length - 1) + '\'';
                                } else {
                                    oracledb_values.push('\'' + val + '\'');
                                    oracledb_values_mapping[key] = '\'' + val + '\'';
                                }
                            }
                        }

                    case 18:

                        // columns.push("offset");
                        values.push('' + message.offset);
                        values_mapping["offset"] = message.offset;
                        oracledb_values.push('\'' + message.offset + '\'');
                        oracledb_values_mapping["offset"] = '\'' + message.offset + '\'';

                        // columns.push("partition");
                        values.push('' + message.partition);
                        values_mapping["partition"] = message.partition;
                        oracledb_values.push('\'' + message.partition + '\'');
                        oracledb_values_mapping["partition"] = '\'' + message.partition + '\'';

                        // columns.push("key");
                        values.push('' + message.key);
                        values_mapping["key"] = message.key;
                        oracledb_values.push('\'' + message.key + '\'');
                        oracledb_values_mapping["key"] = '\'' + message.key + '\'';

                        //convert column into (col1,col2,....)
                        column_str = '(' + columns.join(",") + ')';

                        logger.debug(column_str);

                        oracledb_values_str = '(';

                        //convert value into ('val1','val2',......)

                        value_str = '(';

                        columns.forEach(function (v, i) {
                            if (i === 0) {
                                value_str += '' + (values_mapping[v] || '') + ',';
                                oracledb_values_str += '' + (oracledb_values_mapping[v] || "'-'") + ',';
                            } else {
                                value_str += '\'' + (values_mapping[v] || '') + '\',';
                                oracledb_values_str += (oracledb_values_mapping[v] || "'-'") + ',';
                            }
                        });
                        value_str = value_str.substring(0, value_str.length - 1);
                        oracledb_values_str = oracledb_values_str.substring(0, oracledb_values_str.length - 1);
                        value_str += ')';
                        oracledb_values_str += ')';

                        try {
                            pub.publish("page-stats", JSON.stringify(values_mapping));
                            _msg = JSON.parse(message.value);

                            if (_msg.usr_id) {
                                pub.publish("page-stats-" + _msg.usr_id, JSON.stringify(values_mapping));
                            } else {
                                pub.publish("page-stats-ew-anon", JSON.stringify(values_mapping));
                            }
                        } catch (err) {
                            logger.debug("page-stats redis error in pagestats", err);
                        }

                        _context.prev = 40;
                        _context.next = 43;
                        return insertIntoDb(column_str, value_str);

                    case 43:
                        _context.next = 45;
                        return insertIntoOracleDb(column_str, oracledb_values_str);

                    case 45:
                        logger.debug("Added to userstats");
                        _context.next = 51;
                        break;

                    case 48:
                        _context.prev = 48;
                        _context.t0 = _context['catch'](40);

                        logger.debug("Error in Adding to userstats", _context.t0);

                    case 51:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[40, 48]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}());

function insertIntoDb(columns, value) {
    var query = "insert into " + userstats_table + " " + columns + " values " + value;

    var defer = Q.defer();
    cassconn.execute(query, null, function (err, res) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve({});
    });
    return defer.promise;
}