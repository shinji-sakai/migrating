'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var insertIntoOracleDb = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(columns, value) {
        var query, conn, res;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;

                        //example query: 
                        // var query = "insert into usr_pg_stats (pg_vw_id,usr_id,browser_nm,browser_ver,browser_desc,os_family,os_ver,os_archi,ip,hostname,city,region,country,loc,org,flash_install,geo_loc_info,prev_pg,curr_pg,res_width,res_height,device_pixel_ratio,tkn,login_tkn,pg_vw_tm,offset,partition,key) values ('909d2b2d-8965-4473-ab74-3e4d87a91f4e','sarju','Chrome','55.0.2883.95','Chrome 55.0.2883.95 on OS X 10.11.6','OS X','10.11.6','32','103.236.194.54','No Hostname','Hoodi','Karnataka','IN','12.9944,77.7164','AS134308 CIT Solutions Pvt. Ltd','true','true','http://localhost/','http://localhost/','1920','1080','1','bj8-9LFkpoiMf2G-6loZf-iONtZBBi3j','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJzYXJqdSIsImlhdCI6MTQ4NDU1MDQ3MCwiZXhwIjoxNDg1MTU1MjcwfQ.hWA9uaJIKEE_x8RySEMWvl6-wnzzULApQeMXRTNj1QM','10-JUN-2016 10:10:10','5353','0','-1')";
                        query = "insert into " + oracledb_question_stats_table + " " + columns + " values " + value;

                        logger.debug(query);
                        _context2.next = 5;
                        return oracledb.getConnection();

                    case 5:
                        conn = _context2.sent;
                        _context2.next = 8;
                        return conn.execute(query, [], { autoCommit: true });

                    case 8:
                        res = _context2.sent;
                        _context2.next = 11;
                        return oracledb.releaseConnection(conn);

                    case 11:
                        logger.debug("questionstats insertIntoOracleDb......", res);
                        return _context2.abrupt('return');

                    case 15:
                        _context2.prev = 15;
                        _context2.t0 = _context2['catch'](0);

                        logger.debug(_context2.t0);
                        throw _context2.t0;

                    case 19:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 15]]);
    }));

    return function insertIntoOracleDb(_x2, _x3) {
        return _ref2.apply(this, arguments);
    };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var Q = require('q');
var debug = require('debug')('app:api:kafka:consumer_videostats');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var oracledb = require(configPath.dbconn.oracledb);
var getRedisClient = require(configPath.dbconn.redisconn);
var moment = require("moment");
var question_stats_table = 'ew1.usr_que_vw_stats ';
var oracledb_question_stats_table = 'usr_que_vw_stats';
var pub = getRedisClient("publisher");

var groupId = "";
if (process.env.MODE != "DEV") {
    groupId = 'my-group';
} else {
    groupId = "question-group2";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(client, [{ topic: 'questionstats' }], {
    groupId: groupId
});

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

console.log("Consumer VideoStats Started ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
consumer.on('message', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(message) {
        var columns, values, oracledb_values, random_uuid, msg, parentKeys, i, key, val, column_str, value_str, oracledb_values_str, _msg;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        // console.log(message);
                        columns = [];
                        values = [];
                        oracledb_values = [];
                        random_uuid = Uuid.random();


                        columns.push("que_stat_id");
                        values.push(random_uuid);
                        oracledb_values.push("'" + random_uuid + "'");

                        if (message.value) {
                            //convert to json
                            msg = JSON.parse(message.value);

                            delete msg["typ"];
                            //get all parent keys
                            parentKeys = Object.keys(msg || {});
                            //iterate through parent keys

                            for (i = 0; i < parentKeys.length; i++) {
                                key = parentKeys[i];
                                // if(key === 'tkn')
                                // continue

                                val = msg[key];

                                columns.push(key);
                                // if(key === "vdo_stats"){
                                //     values.push(''+val);    
                                //     oracledb_values.push("'" + val + "'");
                                // }else{
                                values.push('\'' + val + '\'');
                                oracledb_values.push("'" + val + "'");
                                // }
                            }
                        }

                        columns.push("q_vw_dt");
                        values.push("'" + moment().format() + "'");
                        oracledb_values.push("to_date('" + moment().format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')");

                        // columns.push("offset");
                        // values.push('\''+message.offset+'\'');

                        // columns.push("partition");
                        // values.push('\''+message.partition+'\'');

                        // columns.push("key");
                        // values.push('\''+message.key+'\'');

                        //convert column into (col1,col2,....)
                        column_str = '(' + columns.join(",") + ')';

                        //convert value into ('val1','val2',......)

                        value_str = '(' + values.join(",") + ')';
                        oracledb_values_str = '(' + oracledb_values.join(",") + ')';
                        _context.prev = 14;
                        _context.next = 17;
                        return insertIntoDb(column_str, value_str);

                    case 17:
                        _context.next = 19;
                        return insertIntoOracleDb(column_str, oracledb_values_str);

                    case 19:
                        logger.debug("Added to question stats");
                        _context.next = 25;
                        break;

                    case 22:
                        _context.prev = 22;
                        _context.t0 = _context['catch'](14);

                        logger.debug("err in question stats", _context.t0);
                        // throw err;

                    case 25:

                        try {
                            pub.publish("practice-stats", message.value);
                            _msg = JSON.parse(message.value);

                            if (_msg.usr_id) {
                                pub.publish("practice-stats-" + _msg.usr_id, message.value);
                            } else {
                                pub.publish("practice-stats-ew-anon", message.value);
                            }
                        } catch (err) {
                            logger.debug("practice-stats redis error in practicestats", err);
                        }

                    case 26:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[14, 22]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}());

function insertIntoDb(columns, value) {
    var query = "insert into " + question_stats_table + " " + columns + " values " + value;
    var defer = Q.defer();
    cassconn.execute(query, null, function (err, res) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        logger.debug("questionstats insertIntoCassDb......", res);
        defer.resolve({});
    });
    return defer.promise;
}