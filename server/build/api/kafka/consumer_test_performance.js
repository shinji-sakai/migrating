'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var Q = require('q');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var oracledb = require(configPath.dbconn.oracledb);
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);
var pub = getRedisClient("publisher");

var groupId = "";
if (process.env.MODE != "DEV") {
    groupId = "test-perf-group-prod";
} else {
    groupId = "test-perf-group-dev";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),

// rtdb_kafka = require('../../rtdb_kafka.js'),
HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(client, [{
    topic: 'test-perf3'
}], {
    groupId: groupId
});

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

console.log("Consumer Test Performance started : ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
var user_click = {};
consumer.on('message', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(message) {
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        try {
                            logger.debug(message.value);
                            try {
                                pub.incrAsync("total_count");
                            } catch (err) {
                                logger.debug("Redis start-test-performance error ...", err);
                            }
                        } catch (err) {
                            logger.debug("Redis userclick consumer error ...", err);
                        }

                    case 1:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}());