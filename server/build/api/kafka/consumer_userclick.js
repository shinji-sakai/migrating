'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var insertIntoOracleDb = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(columns, value) {
        var query, conn, res;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;

                        //example query
                        // var query = "insert into usr_click_stats (clk_id,link_nm,tag_typ,curr_pg,go_to_pg,login_tkn,usr_id,clk_tm,offset,partition,key) values ('24139de4-458e-4beb-9e7d-6dc1ec31af84','IT  Courses','A','http://localhost/','/about-it-training','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJzYXJqdSIsImlhdCI6MTQ4NDU1MDQ3MCwiZXhwIjoxNDg1MTU1MjcwfQ.hWA9uaJIKEE_x8RySEMWvl6-wnzzULApQeMXRTNj1QM','sarju','2017-01-17T13:58:36.678Z','10393','0','-1')";
                        query = "insert into " + oracle_userclick_table + " " + columns + " values " + value;
                        // logger.debug(query);

                        _context2.next = 4;
                        return oracledb.getConnection();

                    case 4:
                        conn = _context2.sent;
                        _context2.next = 7;
                        return conn.execute(query, [], { autoCommit: true });

                    case 7:
                        res = _context2.sent;
                        _context2.next = 10;
                        return oracledb.releaseConnection(conn);

                    case 10:
                        return _context2.abrupt('return');

                    case 13:
                        _context2.prev = 13;
                        _context2.t0 = _context2['catch'](0);

                        logger.debug(_context2.t0);
                        throw _context2.t0;

                    case 17:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 13]]);
    }));

    return function insertIntoOracleDb(_x2, _x3) {
        return _ref2.apply(this, arguments);
    };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var Q = require('q');
var debug = require('debug')('app:api:kafka:consumer_userclick');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var oracledb = require(configPath.dbconn.oracledb);
var Uuid = cassandra.types.Uuid;
var webSocket = require(configPath.api.websocket);
var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);
var pub = getRedisClient("publisher");

var userclick_table = 'ew1.usr_click_stats';
var oracle_userclick_table = 'usr_click_stats';

var groupId = "";
if (process.env.MODE != "DEV") {
    groupId = "click-group1";
} else {
    groupId = "click-group2";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),

// rtdb_kafka = require('../../rtdb_kafka.js'),
HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(client, [{
    topic: 'userclick'
}], {
    groupId: groupId
});

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

console.log("Consumer UserClick started : ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
var user_click = {};
consumer.on('message', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(message) {
        var columns, values, oracledb_values, random_uuid, msg, parentKeys, i, key, val, column_str, value_str, oracledb_values_str, clicks, _msg, jsonMsg, user, obj;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        logger.debug("In userclick Consumer", message);
                        columns = [];
                        values = [];
                        oracledb_values = [];
                        random_uuid = Uuid.random();


                        columns.push("clk_id");
                        values.push(random_uuid);
                        oracledb_values.push('\'' + random_uuid + '\'');

                        if (message.value) {
                            //convert to json
                            msg = JSON.parse(message.value);
                            // logger.debug("msg........",msg);
                            //get all parent keys

                            parentKeys = Object.keys(msg || {});
                            //iterate through parent keys

                            for (i = 0; i < parentKeys.length; i++) {
                                key = parentKeys[i];
                                // if (key === 'tkn')
                                //     continue;

                                val = msg[key];

                                columns.push(key);

                                if (key === "clk_id") {
                                    values.push('' + val);
                                    oracledb_values.push('\'' + clk_id + '\'');
                                } else {
                                    values.push('\'' + val + '\'');

                                    if (key === "login_tkn") {
                                        oracledb_values.push('\'' + val.substring(1, val.length - 1) + '\'');
                                    } else {
                                        oracledb_values.push('\'' + val + '\'');
                                    }
                                }
                            }
                        }

                        columns.push("offset");
                        values.push('\'' + message.offset + '\'');
                        oracledb_values.push('\'' + message.offset + '\'');

                        columns.push("partition");
                        values.push('\'' + message.partition + '\'');
                        oracledb_values.push('\'' + message.partition + '\'');

                        columns.push("key");
                        values.push('\'' + message.key + '\'');
                        oracledb_values.push('\'' + message.key + '\'');

                        //convert column into (col1,col2,....)
                        column_str = '(' + columns.join(",") + ')';

                        //convert value into ('val1','val2',......)

                        value_str = '(' + values.join(",") + ')';

                        //convert oracledb_values into ('val1','val2',......)

                        oracledb_values_str = '(' + oracledb_values.join(",") + ')';
                        _context.prev = 21;
                        _context.next = 24;
                        return insertIntoDb(column_str, value_str);

                    case 24:
                        _context.next = 26;
                        return insertIntoOracleDb(column_str, oracledb_values_str);

                    case 26:
                        _context.next = 31;
                        break;

                    case 28:
                        _context.prev = 28;
                        _context.t0 = _context['catch'](21);

                        logger.debug("error Adding to userclick table", _context.t0);

                    case 31:
                        _context.prev = 31;
                        _context.next = 34;
                        return pub.getAsync("noOfClicks");

                    case 34:
                        clicks = _context.sent;

                        if (clicks) {
                            clicks = parseInt(clicks);
                            clicks++;
                            pub.setAsync("noOfClicks", clicks);
                        } else {
                            pub.setAsync("noOfClicks", 1);
                        }
                        _context.next = 41;
                        break;

                    case 38:
                        _context.prev = 38;
                        _context.t1 = _context['catch'](31);

                        logger.debug("Redis userclick consumer error ...", _context.t1);

                    case 41:

                        try {
                            pub.publish("click-stats", message.value);
                            _msg = JSON.parse(message.value);

                            if (_msg.usr_id) {
                                pub.publish("click-stats-" + _msg.usr_id, message.value);
                            } else {
                                pub.publish("click-stats-ew-anon", message.value);
                            }
                        } catch (err) {
                            logger.debug("click-stats redis error in clickstats", err);
                        }

                        //adding into rethink db

                        jsonMsg = JSON.parse(message.value);
                        _context.prev = 43;
                        user = jsonMsg["usr_id"];

                        if (user) {
                            _context.next = 47;
                            break;
                        }

                        return _context.abrupt('return');

                    case 47:
                        if (user_click[user] >= 0) {
                            clicks = user_click[user]++;
                            obj = {
                                name: user,
                                clickcount: clicks
                            };

                            webSocket.sendClickStatsOfUser(user, obj);
                            // webSocket.sendMsgFromRethinkDB("message", obj);

                            // rtdb_kafka.update(user, clicks);
                        } else {
                            clicks = 0;

                            user_click[user] = clicks++;
                            obj = {
                                name: user,
                                clickcount: clicks
                            };

                            webSocket.sendClickStatsOfUser(user, obj);
                            // webSocket.sendMsgFromRethinkDB("message", obj);
                            // rtdb_kafka.fetch(user, function(clicks) {
                            //     user_click[user] = clicks++;
                            //     var obj = {
                            //         name: user,
                            //         clickcount: clicks
                            //     };
                            //     webSocket.sendClickStatsOfUser(user, obj);
                            //     webSocket.sendMsgFromRethinkDB("message", obj);
                            // });
                        }
                        _context.next = 53;
                        break;

                    case 50:
                        _context.prev = 50;
                        _context.t2 = _context['catch'](43);

                        debug(_context.t2);

                    case 53:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined, [[21, 28], [31, 38], [43, 50]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}());

function insertIntoDb(columns, value) {
    var query = "insert into " + userclick_table + " " + columns + " values " + value;
    var defer = Q.defer();
    cassconn.execute(query, null, function (err, res) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        // logger.debug("userclick into cass....",res);
        defer.resolve({});
    });
    return defer.promise;
}