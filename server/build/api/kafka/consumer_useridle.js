'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var insertIntoOracleDb = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(columns, value) {
        var query, conn, res;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;

                        // var query = 'insert into usr_click_stats (clk_id,link_nm,tag_typ,curr_pg,go_to_pg,login_tkn,usr_id,clk_tm,offset,partition,key) values ("24139de4-458e-4beb-9e7d-6dc1ec31af84","IT  Courses","A","http://localhost/","/about-it-training","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJzYXJqdSIsImlhdCI6MTQ4NDU1MDQ3MCwiZXhwIjoxNDg1MTU1MjcwfQ.hWA9uaJIKEE_x8RySEMWvl6-wnzzULApQeMXRTNj1QM,"sarju","2017-01-17T13:58:36.678Z","10393","0","-1")'
                        query = "insert into " + oracle_useridle_table + " " + columns + " values " + value;

                        debug(query);
                        _context2.next = 5;
                        return oracledb.getConnection();

                    case 5:
                        conn = _context2.sent;
                        _context2.next = 8;
                        return conn.execute(query, [], { autoCommit: true });

                    case 8:
                        res = _context2.sent;
                        _context2.next = 11;
                        return oracledb.releaseConnection(conn);

                    case 11:
                        debug("insertIntoOracleDb......", res);
                        return _context2.abrupt('return');

                    case 15:
                        _context2.prev = 15;
                        _context2.t0 = _context2['catch'](0);

                        debug(_context2.t0);
                        throw _context2.t0;

                    case 19:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 15]]);
    }));

    return function insertIntoOracleDb(_x2, _x3) {
        return _ref2.apply(this, arguments);
    };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var Q = require('q');
var debug = require('debug')('app:api:kafka:consumer_useridle');
var cassconn = require(configPath.dbconn.cassconn);
var oracledb = require(configPath.dbconn.oracledb);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var useridle_table = 'ew1.usr_idle_stats';
var oracle_useridle_table = 'usr_idle_stats';

var groupId = "";
if (process.env.MODE != "DEV") {
    groupId = 'my-group3';
} else {
    groupId = "idle-group2";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(client, [{ topic: 'useridle' }], {
    groupId: groupId
});

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

consumer.on('message', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(message) {
        var columns, values, oracledb_values, random_uuid, msg, parentKeys, i, key, val, column_str, value_str, oracledb_values_str;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:

                        // console.log(message);
                        columns = [];
                        values = [];
                        oracledb_values = [];
                        random_uuid = Uuid.random();


                        columns.push("idl_id");
                        values.push(random_uuid);
                        oracledb_values.push("'" + random_uuid + "'");

                        if (message.value) {
                            //convert to json
                            msg = JSON.parse(message.value);
                            //get all parent keys

                            parentKeys = Object.keys(msg || {});
                            //iterate through parent keys

                            for (i = 0; i < parentKeys.length; i++) {
                                key = parentKeys[i];
                                val = msg[key];

                                columns.push(key);
                                if (key === "clk_id") {
                                    values.push('' + val);
                                    oracledb_values.push("'" + val + "'");
                                } else {
                                    values.push('\'' + val + '\'');
                                    if (key === "login_tkn") {
                                        oracledb_values.push('\'' + val.substring(1, val.length - 1) + '\'');
                                    } else {
                                        oracledb_values.push('\'' + val + '\'');
                                    }
                                }
                            }
                        }

                        columns.push("offset");
                        values.push('\'' + message.offset + '\'');
                        oracledb_values.push("'" + message.offset + "'");

                        columns.push("partition");
                        values.push('\'' + message.partition + '\'');
                        oracledb_values.push("'" + message.partition + "'");

                        columns.push("key");
                        values.push('\'' + message.key + '\'');
                        oracledb_values.push("'" + message.key + "'");

                        //convert column into (col1,col2,....)
                        column_str = '(' + columns.join(",") + ')';

                        //convert value into ('val1','val2',......)

                        value_str = '(' + values.join(",") + ')';
                        oracledb_values_str = '(' + oracledb_values.join(",") + ')';
                        _context.prev = 20;
                        _context.next = 23;
                        return insertIntoDb(column_str, value_str);

                    case 23:
                        _context.next = 25;
                        return insertIntoOracleDb(column_str, oracledb_values_str);

                    case 25:
                        debug("Added to useridle table");
                        _context.next = 31;
                        break;

                    case 28:
                        _context.prev = 28;
                        _context.t0 = _context['catch'](20);

                        debug("error Adding to useridle table", _context.t0);

                    case 31:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[20, 28]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}());

function insertIntoDb(columns, value) {
    var query = "insert into " + useridle_table + " " + columns + " values " + value;
    var defer = Q.defer();
    cassconn.execute(query, null, function (err, res) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve({});
    });
    return defer.promise;
}