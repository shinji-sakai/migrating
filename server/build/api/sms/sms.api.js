'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var do_request = require('request');
var debug = require('debug')("app:api:sms:sms");
var configPath = require('../../configPath.js');
var textLocalConfig = require(configPath.config.textLocalConfig);
var logger = require(configPath.lib.log_to_file);
var Q = require("q");

function SMS() {
	this.sendSMSAPI = textLocalConfig.API.SEND_SMS;
	this.apiKey = textLocalConfig.API_KEY;
	this.sender = textLocalConfig.SENDER;
	this.isTestMode = false;
	this.format = "json";
}

SMS.prototype.sendSMS = function (data) {
	var defer = Q.defer();

	//data = {message , numbers}

	var obj = {
		apiKey: this.apiKey,
		sender: this.sender,
		message: data.message,
		numbers: data.numbers,
		format: this.format,
		test: this.isTestMode
	};

	do_request.post({
		url: this.sendSMSAPI,
		form: obj
	}, function (err, msg, res) {
		res = JSON.parse(res);
		if (err) {
			logger.debug(err);
			defer.reject(err);
		} else {
			logger.debug(res);
			if (res.status === "failure") {
				defer.reject(res);
			} else {
				defer.resolve(res);
			}
		}
	});

	return defer.promise;
};

SMS.prototype.firstTimeLogin = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(numbers, usr_nm, otp) {
		var message, smsres;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						message = 'Dear User, I\'m so glad you decided to try out examwarrior.com. \nUse below OTP for first time login.\nUsername: ' + usr_nm + '\nOTP: ' + otp + '\nHappy learning. \nFor any questions, please contact us on support@examwarrior.com.';
						_context.next = 4;
						return this.sendSMS({
							message: message,
							numbers: numbers
						});

					case 4:
						smsres = _context.sent;
						_context.next = 11;
						break;

					case 7:
						_context.prev = 7;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);
						throw _context.t0;

					case 11:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 7]]);
	}));

	return function (_x, _x2, _x3) {
		return _ref.apply(this, arguments);
	};
}();

SMS.prototype.paymentFailureSMS = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(numbers, training) {
		var message, smsres;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						message = 'From examwarrior.com.Dear User, payment for ' + training + ' Failed. Can you please check your payment details. For any issues call us on 8197624885 or email us on support@examwarrior.com';
						_context2.next = 4;
						return this.sendSMS({
							message: message,
							numbers: numbers
						});

					case 4:
						smsres = _context2.sent;
						_context2.next = 11;
						break;

					case 7:
						_context2.prev = 7;
						_context2.t0 = _context2['catch'](0);

						logger.debug(_context2.t0);
						throw _context2.t0;

					case 11:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 7]]);
	}));

	return function (_x4, _x5) {
		return _ref2.apply(this, arguments);
	};
}();

SMS.prototype.paymentStartedSMS = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(numbers) {
		var message, smsres;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						message = 'From examwarrior.com. Dear Customer , If any issue in doing payment call us on 918197624885 or email us on support@examwarrior.com';
						_context3.next = 4;
						return this.sendSMS({
							message: message,
							numbers: numbers
						});

					case 4:
						smsres = _context3.sent;
						_context3.next = 11;
						break;

					case 7:
						_context3.prev = 7;
						_context3.t0 = _context3['catch'](0);

						logger.debug(_context3.t0);
						throw _context3.t0;

					case 11:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 7]]);
	}));

	return function (_x6) {
		return _ref3.apply(this, arguments);
	};
}();

SMS.prototype.sendSuccessfulBuySMS = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(numbers, training, usr_id) {
		var message, smsres;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						message = "From examwarrior.com. Dear Customer , Payment Successful.Thank you for purchasing " + training + "\nStart Learning now. \nYour UserID : " + usr_id;
						_context4.next = 4;
						return this.sendSMS({
							message: message,
							numbers: numbers
						});

					case 4:
						smsres = _context4.sent;
						_context4.next = 11;
						break;

					case 7:
						_context4.prev = 7;
						_context4.t0 = _context4['catch'](0);

						logger.debug(_context4.t0);
						throw _context4.t0;

					case 11:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 7]]);
	}));

	return function (_x7, _x8, _x9) {
		return _ref4.apply(this, arguments);
	};
}();

SMS.prototype.sendForgotPasswordSMS = function () {
	var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(numbers, usr_id, otp) {
		var msg, smsres;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.prev = 0;
						msg = "From examwarrior.com . OTP to change password is " + otp + "\nYour UserID : " + usr_id;
						_context5.next = 4;
						return this.sendSMS({
							message: msg,
							numbers: numbers
						});

					case 4:
						smsres = _context5.sent;
						_context5.next = 11;
						break;

					case 7:
						_context5.prev = 7;
						_context5.t0 = _context5['catch'](0);

						logger.debug(_context5.t0);
						throw _context5.t0;

					case 11:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, this, [[0, 7]]);
	}));

	return function (_x10, _x11, _x12) {
		return _ref5.apply(this, arguments);
	};
}();

module.exports = new SMS();