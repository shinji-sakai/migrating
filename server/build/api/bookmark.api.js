'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:bookmarks");

var dashboardStatsApi = require(configPath.api.dashboard.stats);

var bookmarks_tbl = 'ew1.vt_usr_crs_bmrk';
var bookmarks_mv = 'ew1.vt_usr_crs_bmrk_mv';
var authorizeItemAPI = require(configPath.api.authorizeitems);

var BookmarkApi = function () {
    function BookmarkApi() {
        (0, _classCallCheck3.default)(this, BookmarkApi);
    }

    (0, _createClass3.default)(BookmarkApi, [{
        key: 'addBookmark_mongo',
        value: function addBookmark_mongo(data) {
            var col;
            return getConnection().then(findBookmark).then(insertData).then(this.getBookmarks).catch(function (err) {
                console.log(err);
            });

            function findBookmark(db) {
                col = db.collection('bookmarks');
                return col.find({
                    userId: data.userId,
                    videoId: data.videoId
                }, {
                    _id: 1
                }).toArray();
            }

            function insertData(d) {
                if (d.length == 0) {
                    col.insertOne(data);
                }
                console.log(d, data);
                return data;
            }
        }
    }, {
        key: 'getBookmarks_mongo',
        value: function getBookmarks_mongo(data) {
            return getConnection().then(dbCollection).then(globalFunctions.docData).catch(globalFunctions.err);

            function dbCollection(db) {
                console.log("req data....", data);
                var col = db.collection('bookmarks');
                var bookmarks;
                if (data.courseId && !data.fullbookmark) {
                    bookmarks = col.find({
                        courseId: data.courseId,
                        userId: data.userId
                    }, {
                        _id: 0
                    }).sort({
                        _id: -1
                    }).limit(globalConfig.BOOKMARKS_LIMIT).toArray();
                } else if (data.courseId && data.fullbookmark) {
                    bookmarks = col.find({
                        courseId: data.courseId,
                        userId: data.userId
                    }, {
                        _id: 0
                    }).toArray();
                } else {

                    bookmarks = col.find({
                        userId: data.userId
                    }, {
                        _id: 0
                    }).toArray();
                }

                return bookmarks;
            }
        }
    }, {
        key: 'deleteBookmarks_mongo',
        value: function deleteBookmarks_mongo(data) {
            var col;
            return getConnection().then(removeData).catch(globalFunctions.err);

            function removeData(db) {
                col = db.collection('bookmarks');
                return col.deleteOne(data);
            }
        }
    }, {
        key: 'addBookmark',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var defer, query, query_params, total_no_of_affected_bookmarks;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                defer = Q.defer();
                                query = "update " + bookmarks_tbl + ' SET bmrk_flg=?,itm_typ=?,bmrk_ts=? where usr_id = ? and crs_id = ? and itm_id = ?';
                                query_params = [data.bmrk_flg, data.itm_typ, new Date(), data.usr_id, data.crs_id, data.itm_id];

                                //change bookmark counter in dashboard stats table

                                total_no_of_affected_bookmarks = data.bmrk_flg ? 1 : -1;
                                _context.next = 6;
                                return dashboardStatsApi.updateBookmarkCounter({
                                    usr_id: data.usr_id,
                                    no_of_bkmrks: total_no_of_affected_bookmarks
                                });

                            case 6:

                                cassconn.execute(query, query_params, function (err, res, r) {
                                    if (err) {
                                        debug(err);
                                        defer.reject(err);
                                        return;
                                    }
                                    defer.resolve([]);
                                });
                                return _context.abrupt('return', defer.promise);

                            case 8:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function addBookmark(_x) {
                return _ref.apply(this, arguments);
            }

            return addBookmark;
        }()
    }, {
        key: 'bookmarkMultipleItems',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
                var defer, crs_id, items, usr_id, time, queries, no_of_added_bookmarks, no_of_removed_bookmarks, total_no_of_affected_bookmarks, queryOptions;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                defer = Q.defer();
                                crs_id = data.crs_id;
                                items = data.items; // {itm_id,itm_typ,bmrk_flg}

                                usr_id = data.usr_id;
                                time = new Date();
                                queries = [];
                                no_of_added_bookmarks = 0;
                                no_of_removed_bookmarks = 0;


                                items.forEach(function (v) {
                                    var query = "update " + bookmarks_tbl + ' SET bmrk_flg=?,itm_typ=?,bmrk_ts=? where usr_id = ? and crs_id = ? and itm_id = ?';
                                    var query_params = [v.bmrk_flg, v.itm_typ, time, usr_id, crs_id, v.itm_id];
                                    queries.push({
                                        query: query,
                                        params: query_params
                                    });
                                    v.bmrk_flg ? no_of_added_bookmarks++ : no_of_removed_bookmarks++;
                                });

                                //change bookmark counter in dashboard stats table
                                total_no_of_affected_bookmarks = no_of_added_bookmarks - no_of_removed_bookmarks;
                                _context2.next = 12;
                                return dashboardStatsApi.updateBookmarkCounter({
                                    usr_id: usr_id,
                                    no_of_bkmrks: total_no_of_affected_bookmarks
                                });

                            case 12:
                                queryOptions = {
                                    prepare: true,
                                    consistency: cassandra.types.consistencies.quorum
                                };

                                cassconn.batch(queries, queryOptions, function (err, res, r) {
                                    if (err) {
                                        debug(err);
                                        defer.reject(err);
                                        return;
                                    }
                                    defer.resolve([]);
                                });
                                return _context2.abrupt('return', defer.promise);

                            case 15:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function bookmarkMultipleItems(_x2) {
                return _ref2.apply(this, arguments);
            }

            return bookmarkMultipleItems;
        }()
    }, {
        key: 'getBookmarks',
        value: function getBookmarks(data) {
            var defer = Q.defer();

            var query = "select * from " + bookmarks_tbl + ' where usr_id = ? and crs_id = ?';
            var query_params = [data.usr_id, data.crs_id];

            cassconn.execute(query, query_params, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve(res.rows);
            });
            return defer.promise;
        }
    }, {
        key: 'getUserBookmarks',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var query, query_params, rows, result, i, row, itemInfo, obj;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                query = "select * from " + bookmarks_mv + ' where usr_id = ?';
                                query_params = [data.usr_id];
                                _context3.next = 5;
                                return cassExecute(query, query_params);

                            case 5:
                                rows = _context3.sent;

                                if (!(rows && rows.length > 0)) {
                                    _context3.next = 24;
                                    break;
                                }

                                result = [];
                                i = 0;

                            case 9:
                                if (!(i < rows.length)) {
                                    _context3.next = 21;
                                    break;
                                }

                                row = rows[i];

                                if (!row.bmrk_flg) {
                                    _context3.next = 18;
                                    break;
                                }

                                _context3.next = 14;
                                return authorizeItemAPI.getItemsUsingItemId({
                                    itemId: row.itm_id
                                });

                            case 14:
                                itemInfo = _context3.sent;

                                itemInfo = itemInfo && itemInfo[0];
                                obj = (0, _extends3.default)({}, row, itemInfo);

                                result.push(obj);

                            case 18:
                                i++;
                                _context3.next = 9;
                                break;

                            case 21:
                                return _context3.abrupt('return', result);

                            case 24:
                                return _context3.abrupt('return', []);

                            case 25:
                                _context3.next = 31;
                                break;

                            case 27:
                                _context3.prev = 27;
                                _context3.t0 = _context3['catch'](0);

                                debug(_context3.t0);
                                throw _context3.t0;

                            case 31:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 27]]);
            }));

            function getUserBookmarks(_x3) {
                return _ref3.apply(this, arguments);
            }

            return getUserBookmarks;
        }()
    }]);
    return BookmarkApi;
}();

module.exports = new BookmarkApi();