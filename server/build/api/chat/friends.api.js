'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var jwt = require('jsonwebtoken');
var request = require('request');
var qs = require('querystring');
var Q = require('q');
var debug = require('debug')('app:api:chat:friends');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);
var cassExecute = require(configPath.lib.utility).cassExecute;
var path = require("path");
var fs = require("fs");

var frnd_table = 'ew1.vt_frs_chat_ts';
var user_short_table = 'ew1.vt_usr_short';
var grp_md = 'ew1.vt_grp_chat_md';

var ChatFriends = function () {
    function ChatFriends() {
        (0, _classCallCheck3.default)(this, ChatFriends);
    }

    (0, _createClass3.default)(ChatFriends, [{
        key: 'findFriends',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var findFriends = function () {
                    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
                        var selectUser, rows, userIds, usersRes;
                        return _regenerator2.default.wrap(function _callee$(_context) {
                            while (1) {
                                switch (_context.prev = _context.next) {
                                    case 0:
                                        _context.prev = 0;
                                        selectUser = 'select * from ' + frnd_table + ' where user_id = ?;';
                                        _context.next = 4;
                                        return cassExecute(selectUser, [data.user_id]);

                                    case 4:
                                        rows = _context.sent;
                                        userIds = rows.map(function (v, i) {
                                            return v.fr_id;
                                        });
                                        _context.next = 8;
                                        return self.fetchUserShortInfo(userIds);

                                    case 8:
                                        usersRes = _context.sent;
                                        _context.next = 11;
                                        return findGrpMembers(usersRes || []);

                                    case 11:
                                        usersRes = _context.sent;
                                        return _context.abrupt('return', usersRes);

                                    case 15:
                                        _context.prev = 15;
                                        _context.t0 = _context['catch'](0);

                                        debug(_context.t0);
                                        throw _context.t0;

                                    case 19:
                                    case 'end':
                                        return _context.stop();
                                }
                            }
                        }, _callee, this, [[0, 15]]);
                    }));

                    return function findFriends() {
                        return _ref2.apply(this, arguments);
                    };
                }();

                var findGrpMembers = function () {
                    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(userData) {
                        var defer, ids, ids_str, selectUser, result;
                        return _regenerator2.default.wrap(function _callee2$(_context2) {
                            while (1) {
                                switch (_context2.prev = _context2.next) {
                                    case 0:
                                        defer = Q.defer();
                                        _context2.prev = 1;

                                        userData = userData || [];
                                        ids = userData.map(function (v, i) {
                                            return v.user_id;
                                        });

                                        if (ids) {
                                            _context2.next = 6;
                                            break;
                                        }

                                        return _context2.abrupt('return', []);

                                    case 6:
                                        if (!(ids && ids.length <= 0)) {
                                            _context2.next = 8;
                                            break;
                                        }

                                        return _context2.abrupt('return', []);

                                    case 8:

                                        //create string like ('user1','user2',.....)
                                        ids_str = '(';

                                        ids.forEach(function (v, i) {
                                            ids_str += '\'' + v + '\',';
                                        });
                                        ids_str = ids_str.substring(0, ids_str.length - 1);
                                        ids_str += ')';

                                        selectUser = 'select * from ' + grp_md + ' where grp_id in ' + ids_str;
                                        _context2.next = 15;
                                        return cassExecute(selectUser, undefined);

                                    case 15:
                                        result = _context2.sent;

                                        userData.forEach(function (usrinfo) {
                                            result.forEach(function (grpinfo) {
                                                if (grpinfo.grp_id == usrinfo.user_id) {
                                                    usrinfo["grp_mem"] = grpinfo.grp_mem;
                                                    if (usrinfo.pic50) {
                                                        var parsed = path.parse(usrinfo.pic50);
                                                        if (parsed.base) {
                                                            var rel_path = configPath.common_paths.grp_pic_relative + "/" + parsed.base;
                                                            var norm_rel_path = path.normalize(rel_path);
                                                            var isExist = fs.existsSync(norm_rel_path);
                                                            if (!isExist) {
                                                                usrinfo.pic50 = "/assets/images/dummy-user-pic.png";
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        });
                                        return _context2.abrupt('return', userData);

                                    case 20:
                                        _context2.prev = 20;
                                        _context2.t0 = _context2['catch'](1);

                                        debug(_context2.t0);
                                        throw _context2.t0;

                                    case 24:
                                        return _context2.abrupt('return', defer.promise);

                                    case 25:
                                    case 'end':
                                        return _context2.stop();
                                }
                            }
                        }, _callee2, this, [[1, 20]]);
                    }));

                    return function findGrpMembers(_x2) {
                        return _ref3.apply(this, arguments);
                    };
                }();

                var self;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                self = this;

                                if (data.user_id) {
                                    _context3.next = 3;
                                    break;
                                }

                                return _context3.abrupt('return', {
                                    status: "error",
                                    message: "user_id field is required"
                                });

                            case 3:
                                return _context3.abrupt('return', findFriends().catch(globalFunctions.err));

                            case 4:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function findFriends(_x) {
                return _ref.apply(this, arguments);
            }

            return findFriends;
        }()
    }, {
        key: 'fetchUserShortInfo',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(userIds) {
                var promiseArr, resArr, ids_str, selectUser, result;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                promiseArr = [];
                                resArr = [];
                                _context4.prev = 2;

                                if (userIds) {
                                    _context4.next = 5;
                                    break;
                                }

                                return _context4.abrupt('return', {
                                    message: "userIds required"
                                });

                            case 5:
                                if (!(userIds && userIds.length <= 0)) {
                                    _context4.next = 7;
                                    break;
                                }

                                return _context4.abrupt('return', []);

                            case 7:
                                //create string like ('user1','user2',.....)
                                ids_str = "('" + userIds.join("','") + "')";
                                selectUser = 'select * from ' + user_short_table + ' where user_id in ' + ids_str;
                                _context4.next = 11;
                                return cassExecute(selectUser, undefined);

                            case 11:
                                result = _context4.sent;
                                return _context4.abrupt('return', result);

                            case 15:
                                _context4.prev = 15;
                                _context4.t0 = _context4['catch'](2);

                                debug(_context4.t0);
                                throw _context4.t0;

                            case 19:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[2, 15]]);
            }));

            function fetchUserShortInfo(_x3) {
                return _ref4.apply(this, arguments);
            }

            return fetchUserShortInfo;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.user_id - User Id
         * @param {string} data.fr_id - Friend Id
         * @returns {Promise}
         * @desc
         * - This method is used to add fr_id to friend list of usr_id
         * - Tables Used : {@link CassandraTables#vt_frs_chat_ts}
         */

    }, {
        key: 'addFriend',
        value: function addFriend(data) {
            return addFriend().catch(globalFunctions.err);

            function addFriend() {
                var defer = Q.defer();
                var insertUser = 'insert into ' + frnd_table + ' (user_id,fr_id,last_talked) values (?,?,?);';
                var arr = [data.user_id, data.fr_id, new Date()];
                cassconn.execute(insertUser, arr, function (err, res, r) {
                    if (err) {
                        defer.reject(err);
                        return;
                    }
                    defer.resolve([]);
                });
                return defer.promise;
            }
        }

        /**
         * @param {JSON} data
         * @param {string} data.user_id - User Id
         * @param {string} data.fr_id - Friend Id
         * @returns {Promise}
         * @desc
         * - This method is used to remove fr_id from friend list of usr_id
         * - Tables Used : {@link CassandraTables#vt_frs_chat_ts}
         */

    }, {
        key: 'removeFriend',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var deleteFrnd, params, res;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;
                                deleteFrnd = 'delete from ' + frnd_table + ' where user_id=? and fr_id=?;';
                                params = [data.user_id, data.fr_id];
                                _context5.next = 5;
                                return cassExecute(deleteFrnd, params);

                            case 5:
                                res = _context5.sent;
                                return _context5.abrupt('return', data);

                            case 9:
                                _context5.prev = 9;
                                _context5.t0 = _context5['catch'](0);

                                debug(_context5.t0);
                                throw _context5.t0;

                            case 13:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 9]]);
            }));

            function removeFriend(_x4) {
                return _ref5.apply(this, arguments);
            }

            return removeFriend;
        }()
    }, {
        key: 'findFriendWithId',
        value: function findFriendWithId(data) {
            var defer = Q.defer();
            var selectUser = 'select * from ' + user_short_table + ' where user_id = ?;';
            var arr = [data.user_id];
            cassconn.execute(selectUser, arr, function (err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                if (!res) {
                    defer.resolve([]);
                } else {
                    defer.resolve(res.rows[0]);
                }
            });
            return defer.promise;
        }
    }, {
        key: 'addUserShortDetails',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var user_id, user_email, display_name, typ, pic50, selectUser, arr, result;
                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;

                                debug(data);
                                user_id = data.user_id;
                                user_email = data.user_email;
                                display_name = data.display_name;
                                typ = data.typ;
                                pic50 = data.pic;

                                //query

                                selectUser = 'insert into ' + user_short_table + ' (user_id,display_name,pic50,typ,user_email) values (?,?,?,?,?)';
                                arr = [user_id, display_name, pic50, typ, user_email];

                                debug(arr);
                                //execute query
                                _context6.next = 12;
                                return cassExecute(selectUser, arr);

                            case 12:
                                result = _context6.sent;
                                return _context6.abrupt('return', data);

                            case 16:
                                _context6.prev = 16;
                                _context6.t0 = _context6['catch'](0);

                                debug(_context6.t0);
                                throw _context6.t0;

                            case 20:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 16]]);
            }));

            function addUserShortDetails(_x5) {
                return _ref6.apply(this, arguments);
            }

            return addUserShortDetails;
        }()

        /**
         * @param {JSON} data
         * @param {string} data.user_id - User Id
         * @param {string} data.usr_pic - Display Pic Url
         * @returns {Promise}
         * @desc
         * - This method is used to update display pic url of given user id in chat user short details table
         * - Tables Used : {@link CassandraTables#vt_usr_short}
         */

    }, {
        key: 'updateUserProfilePic',
        value: function () {
            var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
                var user_id, pic50, selectUser, arr, result;
                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                _context7.prev = 0;
                                user_id = data.usr_id;
                                pic50 = data.usr_pic;

                                //query

                                selectUser = 'update ' + user_short_table + ' set pic50=? where user_id=?';
                                arr = [pic50, user_id];
                                //execute query

                                _context7.next = 7;
                                return cassExecute(selectUser, arr);

                            case 7:
                                result = _context7.sent;
                                return _context7.abrupt('return', data);

                            case 11:
                                _context7.prev = 11;
                                _context7.t0 = _context7['catch'](0);

                                debug(_context7.t0);
                                throw _context7.t0;

                            case 15:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this, [[0, 11]]);
            }));

            function updateUserProfilePic(_x6) {
                return _ref7.apply(this, arguments);
            }

            return updateUserProfilePic;
        }()
    }]);
    return ChatFriends;
}();

module.exports = new ChatFriends();