'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var jwt = require('jsonwebtoken');
var request = require('request');
var qs = require('querystring');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);

var uuid = require('uuid');
// var time = require('time')(Date);
var cassandra = require('cassandra-driver');
var debug = require('debug')('app:api:chat:grp');
var cassExecute = require(configPath.lib.utility).cassExecute;
var chatFrndsAPI = require(configPath.api.chat.friends);

var chat_ts_table = 'ew1.vt_frs_chat_ts';
var chat_m_table = 'ew1.vt_frs_chat_m';
var user_short_table = 'ew1.vt_usr_short';
var grp_md = 'ew1.vt_grp_chat_md';

function ChatGroups() {}

ChatGroups.prototype.createGroup = function (data) {
    var defer = Q.defer();
    var d = new Date();
    // d.setTimezone('UTC');

    var grp_id = data.grp_id;
    // console.log(grp_id);
    var created_dt = d;
    var grp_nm = data.grp_nm;
    var grp_own = data.grp_own;
    var pic50 = data.pic50;
    var typ = 'grp';
    var grp_mem = {};
    var members = data.members;

    var frs_chat_m_queries = [];
    var frs_chat_ts_queries = [];
    members.forEach(function (v) {
        grp_mem[v] = d;

        // no of people new entry in vt_frs_chat_m
        var insert_chat_m = "insert into " + chat_m_table + '(user_id , fr_id , typ , comb_id) values (?,?,?,?);';
        var insert_chat_m_params = [v, grp_id, 'grp', grp_id];

        frs_chat_m_queries.push({
            query: insert_chat_m,
            params: insert_chat_m_params
        });

        // no of people new entry in vt_frs_chat_ts
        var insert_chat_ts = "insert into " + chat_ts_table + '(user_id , fr_id , typ , last_talked) values (?,?,?,?);';
        var insert_chat_ts_params = [v, grp_id, 'grp', d];

        frs_chat_ts_queries.push({
            query: insert_chat_ts,
            params: insert_chat_ts_params
        });
    });

    // 1 new entry in vt_grp_chat_md
    var insertGrp = "insert into " + grp_md + '(grp_id , grp_nm , grp_own , grp_mem , created_dt) values (?,?,?,?,?);';
    var insertGrp_params = [grp_id, grp_nm, grp_own, grp_mem, created_dt];

    // 1 new entry in vt_usr_short_m
    var insertShortInfo = "insert into " + user_short_table + '(user_id , display_name , typ , user_email,pic50) values (?,?,?,?,?);';
    var insertShortInfo_params = [grp_id, grp_nm, 'grp', grp_id, pic50];

    var queries = [];
    queries.push({
        query: insertGrp,
        params: insertGrp_params
    });
    queries.push({
        query: insertShortInfo,
        params: insertShortInfo_params
    });
    queries = queries.concat(frs_chat_m_queries, frs_chat_ts_queries);
    // console.log(queries);
    var queryOptions = {
        prepare: true,
        consistency: cassandra.types.consistencies.quorum
    };
    cassconn.batch(queries, queryOptions, function (err, res, r) {
        // console.log(err,res,r);
        if (err) {
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
};

ChatGroups.prototype.findMembers = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var select, select_params, rows, grp_members, members_id, members_details;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        if (data.grp_id) {
                            _context.next = 2;
                            break;
                        }

                        return _context.abrupt('return', {
                            status: "error",
                            message: "grp_id field required"
                        });

                    case 2:
                        _context.prev = 2;
                        select = "select * from " + grp_md + ' where grp_id = ?;';
                        select_params = [data.grp_id];
                        _context.next = 7;
                        return cassExecute(select, select_params);

                    case 7:
                        rows = _context.sent;

                        if (!(rows && rows.length > 0)) {
                            _context.next = 17;
                            break;
                        }

                        grp_members = rows[0].grp_mem;
                        members_id = Object.keys(grp_members);
                        _context.next = 13;
                        return chatFrndsAPI.fetchUserShortInfo(members_id);

                    case 13:
                        members_details = _context.sent;
                        return _context.abrupt('return', members_details);

                    case 17:
                        return _context.abrupt('return', []);

                    case 18:
                        _context.next = 24;
                        break;

                    case 20:
                        _context.prev = 20;
                        _context.t0 = _context['catch'](2);

                        console.error(_context.t0);
                        throw new Error(_context.t0);

                    case 24:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[2, 20]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

ChatGroups.prototype.addMembers = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var defer, d, frs_chat_m_queries, frs_chat_ts_queries, members, grp_id, grp_mem, select, select_params;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        defer = Q.defer();
                        d = new Date();
                        // d.setTimezone('UTC');

                        frs_chat_m_queries = [];
                        frs_chat_ts_queries = [];
                        members = data.members;
                        grp_id = data.grp_id;
                        grp_mem = {};

                        if (data.grp_id) {
                            _context2.next = 9;
                            break;
                        }

                        return _context2.abrupt('return', {
                            status: "error",
                            message: "grp_id field is required"
                        });

                    case 9:
                        if (data.members) {
                            _context2.next = 11;
                            break;
                        }

                        return _context2.abrupt('return', {
                            status: "error",
                            message: "members field is required"
                        });

                    case 11:
                        if (!(data.members.length <= 0)) {
                            _context2.next = 13;
                            break;
                        }

                        return _context2.abrupt('return', {
                            status: "error",
                            message: "Atleast one member is required"
                        });

                    case 13:

                        members.forEach(function (v) {
                            grp_mem[v] = d;

                            // no of people new entry in vt_frs_chat_m
                            var insert_chat_m = "insert into " + chat_m_table + '(user_id , fr_id , typ , comb_id) values (?,?,?,?);';
                            var insert_chat_m_params = [v, grp_id, 'grp', grp_id];

                            frs_chat_m_queries.push({
                                query: insert_chat_m,
                                params: insert_chat_m_params
                            });

                            // no of people new entry in vt_frs_chat_ts
                            var insert_chat_ts = "insert into " + chat_ts_table + '(user_id , fr_id , typ , last_talked) values (?,?,?,?);';
                            var insert_chat_ts_params = [v, grp_id, 'grp', d];

                            frs_chat_ts_queries.push({
                                query: insert_chat_ts,
                                params: insert_chat_ts_params
                            });
                        });

                        select = "select * from " + grp_md + ' where grp_id = ?;';
                        select_params = [data.grp_id];

                        cassconn.execute(select, select_params, function (err, res, r) {
                            if (err) {
                                defer.reject(err);
                                return;
                            }

                            var newMem = {};
                            if (res.rows.length > 0) {
                                var mem = res.rows[0].get('grp_mem');

                                for (var k in mem) {
                                    newMem[k] = mem[k];
                                }
                                for (var _k in grp_mem) {
                                    newMem[_k] = grp_mem[_k];
                                }
                            }

                            var qu = 'UPDATE ' + grp_md + ' SET grp_mem = ? WHERE grp_id = ?;';
                            var qu_params = [newMem, grp_id];

                            var queries = [];
                            queries = queries.concat(frs_chat_m_queries, frs_chat_ts_queries);
                            queries.push({
                                query: qu,
                                params: qu_params
                            });
                            var queryOptions = {
                                prepare: true,
                                consistency: cassandra.types.consistencies.quorum
                            };
                            cassconn.batch(queries, queryOptions, function (err, res, r) {
                                if (err) {
                                    debug(err);
                                    defer.reject(err);
                                    return;
                                }
                                defer.resolve({});
                            });
                        });

                        return _context2.abrupt('return', defer.promise);

                    case 18:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

// var d = new ChatGroups();
// var obj = {
// 	grp_nm : 'Test Group',
// 	grp_own : 'sarju@filmsmiles.com',
// 	members : ['sarju@filmsmiles.com','jeevan@filmsmiles.com','dvbydt@filmsmiles.com']
// }
// d.createGroup(obj);

module.exports = new ChatGroups();