'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();
var logger = require(configPath.lib.log_to_file);
var Q = require('q');

var OFFLINE_STATUS = "offline";
var ONLINE_STATUS = "online";
var AWAY_STATUS = "away";
var ACTIVE_STATUS = "active";
var INACTIVE_STATUS = "inactive";

var chat_redis = function () {
    function chat_redis() {
        (0, _classCallCheck3.default)(this, chat_redis);
    }

    (0, _createClass3.default)(chat_redis, [{
        key: 'setChatUserInfo',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
                var defer, getUserInfo;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                defer = Q.defer();
                                _context.next = 4;
                                return redisCli.saddAsync(data.user_id + ":socketIds", data.socketId);

                            case 4:
                                _context.next = 6;
                                return redisCli.setAsync(data.user_id + ":display_name", data.display_name);

                            case 6:
                                _context.next = 8;
                                return redisCli.smembersAsync(data.user_id + ":socketIds");

                            case 8:
                                getUserInfo = _context.sent;
                                return _context.abrupt('return', {
                                    getUserInfo: getUserInfo
                                });

                            case 12:
                                _context.prev = 12;
                                _context.t0 = _context['catch'](0);

                                logger.debug(_context.t0);
                                throw new Error(_context.t0);

                            case 16:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 12]]);
            }));

            function setChatUserInfo(_x) {
                return _ref.apply(this, arguments);
            }

            return setChatUserInfo;
        }()
    }, {
        key: 'getUserSocketIds',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(usr_Ids) {
                var defer, multiObj, getUserSocketIds, responseObj, i;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                defer = Q.defer();
                                multiObj = redisCli.multi();

                                usr_Ids.forEach(function (usrSocketId) {
                                    multiObj.smembers(usrSocketId + ":socketIds");
                                });

                                _context2.next = 6;
                                return multiObj.execAsync();

                            case 6:
                                getUserSocketIds = _context2.sent;
                                responseObj = [];


                                for (i = 0; i < usr_Ids.length; i++) {
                                    responseObj.push({
                                        user_id: usr_Ids[i],
                                        socketIds: getUserSocketIds[i]
                                    });
                                }
                                return _context2.abrupt('return', responseObj);

                            case 12:
                                _context2.prev = 12;
                                _context2.t0 = _context2['catch'](0);

                                logger.debug(_context2.t0);
                                throw new Error(_context2.t0);

                            case 16:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[0, 12]]);
            }));

            function getUserSocketIds(_x2) {
                return _ref2.apply(this, arguments);
            }

            return getUserSocketIds;
        }()
    }, {
        key: 'removeChatUserInfo',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var defer, getUserInfo;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                defer = Q.defer();
                                _context3.next = 4;
                                return redisCli.sremAsync(data.user_id + ":socketIds", data.socketId);

                            case 4:
                                _context3.next = 6;
                                return redisCli.smembersAsync(data.user_id + ":socketIds");

                            case 6:
                                getUserInfo = _context3.sent;

                                if (!(getUserInfo.length == 0)) {
                                    _context3.next = 10;
                                    break;
                                }

                                _context3.next = 10;
                                return redisCli.delAsync(data.user_id + ":display_name");

                            case 10:
                                return _context3.abrupt('return', {
                                    getUserInfo: getUserInfo
                                });

                            case 13:
                                _context3.prev = 13;
                                _context3.t0 = _context3['catch'](0);

                                logger.debug(_context3.t0);
                                throw new Error(_context3.t0);

                            case 17:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 13]]);
            }));

            function removeChatUserInfo(_x3) {
                return _ref3.apply(this, arguments);
            }

            return removeChatUserInfo;
        }()
    }, {
        key: 'setUserManualStatus',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var defer;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                defer = Q.defer();
                                _context4.next = 4;
                                return redisCli.setAsync(data.user_id + ":manual_chat_status", data.manualChatStatus);

                            case 4:
                                return _context4.abrupt('return', data);

                            case 7:
                                _context4.prev = 7;
                                _context4.t0 = _context4['catch'](0);

                                logger.debug(_context4.t0);
                                throw new Error(_context4.t0);

                            case 11:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 7]]);
            }));

            function setUserManualStatus(_x4) {
                return _ref4.apply(this, arguments);
            }

            return setUserManualStatus;
        }()
    }, {
        key: 'getUserManualStatus',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var defer, status, user_sock;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;
                                defer = Q.defer();
                                _context5.next = 4;
                                return redisCli.getAsync(data.user_id + ":manual_chat_status");

                            case 4:
                                status = _context5.sent;

                                if (!(status && status !== 'null')) {
                                    _context5.next = 20;
                                    break;
                                }

                                if (!(status === OFFLINE_STATUS)) {
                                    _context5.next = 10;
                                    break;
                                }

                                return _context5.abrupt('return', status);

                            case 10:
                                _context5.next = 12;
                                return this.getUserSocketIds([data.user_id]);

                            case 12:
                                user_sock = _context5.sent;

                                if (!(user_sock[0] && user_sock[0].socketIds.length > 0)) {
                                    _context5.next = 17;
                                    break;
                                }

                                return _context5.abrupt('return', status);

                            case 17:
                                return _context5.abrupt('return', OFFLINE_STATUS);

                            case 18:
                                _context5.next = 28;
                                break;

                            case 20:
                                _context5.next = 22;
                                return this.getUserSocketIds([data.user_id]);

                            case 22:
                                user_sock = _context5.sent;

                                if (!(user_sock[0] && user_sock[0].socketIds.length > 0)) {
                                    _context5.next = 27;
                                    break;
                                }

                                return _context5.abrupt('return', status);

                            case 27:
                                return _context5.abrupt('return', OFFLINE_STATUS);

                            case 28:
                                _context5.next = 34;
                                break;

                            case 30:
                                _context5.prev = 30;
                                _context5.t0 = _context5['catch'](0);

                                logger.debug(_context5.t0);
                                throw new Error(_context5.t0);

                            case 34:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 30]]);
            }));

            function getUserManualStatus(_x5) {
                return _ref5.apply(this, arguments);
            }

            return getUserManualStatus;
        }()
    }, {
        key: 'getUserChatActivity',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var redis_chat_activity, parsedObj;
                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;
                                _context6.next = 3;
                                return redisCli.getAsync(data.user_id + ":chat_activity");

                            case 3:
                                redis_chat_activity = _context6.sent;

                                if (!(redis_chat_activity && redis_chat_activity !== 'null')) {
                                    _context6.next = 9;
                                    break;
                                }

                                parsedObj = JSON.parse(redis_chat_activity);
                                return _context6.abrupt('return', parsedObj);

                            case 9:
                                return _context6.abrupt('return', {
                                    activity: OFFLINE_STATUS,
                                    time: new Date()
                                });

                            case 10:
                                _context6.next = 16;
                                break;

                            case 12:
                                _context6.prev = 12;
                                _context6.t0 = _context6['catch'](0);

                                logger.debug(_context6.t0);
                                throw new Error(_context6.t0);

                            case 16:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 12]]);
            }));

            function getUserChatActivity(_x6) {
                return _ref6.apply(this, arguments);
            }

            return getUserChatActivity;
        }()
    }, {
        key: 'setUserChatActivity',
        value: function () {
            var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
                var obj, redis_chat_activity, parsedObj, lastTime, currentTime, _obj;

                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                _context7.prev = 0;

                                if (!(data.chatActivity && data.chatActivity === ONLINE_STATUS)) {
                                    _context7.next = 7;
                                    break;
                                }

                                // if chat activity is online then directly set that
                                obj = {
                                    activity: data.chatActivity,
                                    time: new Date()
                                };
                                _context7.next = 5;
                                return redisCli.setAsync(data.user_id + ":chat_activity", JSON.stringify(obj));

                            case 5:
                                _context7.next = 19;
                                break;

                            case 7:
                                if (!(data.chatActivity && data.chatActivity === AWAY_STATUS)) {
                                    _context7.next = 19;
                                    break;
                                }

                                _context7.next = 10;
                                return redisCli.getAsync(data.user_id + ":chat_activity");

                            case 10:
                                redis_chat_activity = _context7.sent;

                                if (!(redis_chat_activity && redis_chat_activity !== 'null')) {
                                    _context7.next = 19;
                                    break;
                                }

                                parsedObj = JSON.parse(redis_chat_activity);
                                lastTime = new Date(parsedObj.time);
                                currentTime = new Date();

                                if (!(currentTime - lastTime > 1000 * 5 * 60)) {
                                    _context7.next = 19;
                                    break;
                                }

                                _obj = {
                                    activity: AWAY_STATUS,
                                    time: currentTime
                                };
                                _context7.next = 19;
                                return redisCli.setAsync(data.user_id + ":chat_activity", JSON.stringify(_obj));

                            case 19:
                                return _context7.abrupt('return', data);

                            case 22:
                                _context7.prev = 22;
                                _context7.t0 = _context7['catch'](0);

                                logger.debug(_context7.t0);
                                throw new Error(_context7.t0);

                            case 26:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this, [[0, 22]]);
            }));

            function setUserChatActivity(_x7) {
                return _ref7.apply(this, arguments);
            }

            return setUserChatActivity;
        }()
    }, {
        key: 'getUserChatStatusToShow',
        value: function () {
            var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
                var manual_status, chat_activity_status;
                return _regenerator2.default.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                _context8.prev = 0;
                                _context8.next = 3;
                                return this.getUserManualStatus(data);

                            case 3:
                                manual_status = _context8.sent;

                                if (!(manual_status === OFFLINE_STATUS)) {
                                    _context8.next = 8;
                                    break;
                                }

                                return _context8.abrupt('return', manual_status);

                            case 8:
                                _context8.next = 10;
                                return this.getUserChatActivity(data);

                            case 10:
                                chat_activity_status = _context8.sent;
                                return _context8.abrupt('return', chat_activity_status.activity);

                            case 12:
                                _context8.next = 18;
                                break;

                            case 14:
                                _context8.prev = 14;
                                _context8.t0 = _context8['catch'](0);

                                logger.debug(_context8.t0);
                                throw new Error(_context8.t0);

                            case 18:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, this, [[0, 14]]);
            }));

            function getUserChatStatusToShow(_x8) {
                return _ref8.apply(this, arguments);
            }

            return getUserChatStatusToShow;
        }()
    }, {
        key: 'getMultipleUsersChatStatusToShow',
        value: function () {
            var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(data) {
                var res, i, usr_id, status;
                return _regenerator2.default.wrap(function _callee9$(_context9) {
                    while (1) {
                        switch (_context9.prev = _context9.next) {
                            case 0:
                                _context9.prev = 0;

                                if (data.users) {
                                    _context9.next = 3;
                                    break;
                                }

                                return _context9.abrupt('return', []);

                            case 3:
                                res = [];
                                i = 0;

                            case 5:
                                if (!(i < data.users.length)) {
                                    _context9.next = 14;
                                    break;
                                }

                                usr_id = data.users[i];
                                _context9.next = 9;
                                return this.getUserChatStatusToShow({ user_id: usr_id });

                            case 9:
                                status = _context9.sent;

                                res.push({
                                    user_id: usr_id,
                                    status: status
                                });

                            case 11:
                                i++;
                                _context9.next = 5;
                                break;

                            case 14:
                                return _context9.abrupt('return', res);

                            case 17:
                                _context9.prev = 17;
                                _context9.t0 = _context9['catch'](0);

                                logger.debug(_context9.t0);
                                throw new Error(_context9.t0);

                            case 21:
                            case 'end':
                                return _context9.stop();
                        }
                    }
                }, _callee9, this, [[0, 17]]);
            }));

            function getMultipleUsersChatStatusToShow(_x9) {
                return _ref9.apply(this, arguments);
            }

            return getMultipleUsersChatStatusToShow;
        }()
    }]);
    return chat_redis;
}();

module.exports = new chat_redis();