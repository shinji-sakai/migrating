'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var request = require('request');
var qs = require('querystring');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);
var crypto = require('crypto');
var cassandra = require('cassandra-driver');
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = cassandra.types.TimeUuid;
// var time = require('time')(Date);
var debug = require('debug')("app:api:chat:msgs");
var detail_table = 'ew1.vt_frs_chat_d';
var read_detail_table = 'ew1.vt_frs_chat_d_read';
var master_table = 'ew1.vt_frs_chat_m';

function ChatMsgs() {}

ChatMsgs.prototype.addUsersToMaster = function (data) {
	return checkUserExistance().then(addUsers).catch(globalFunctions.err);

	function checkUserExistance() {
		var defer = Q.defer();
		var checkUser = 'select * from ' + master_table + ' where user_id = ? and fr_id = ?;';

		var params = [data.user_id, data.fr_id];
		cassconn.execute(checkUser, params, function (err, res, r) {
			if (err) {
				defer.reject(err);
				return;
			}
			defer.resolve(res.rows);
		});
		return defer.promise;
	}

	function addUsers(rows) {
		var defer = Q.defer();
		//if rows will be there then dont add user
		if (rows.length > 0) {
			defer.resolve([]);
		} else {
			var usr_str = data.user_id + data.fr_id;
			// var frnd_str  = data.fr_id   + data.user_id;
			var usr_hash = crypto.createHash('md5').update(usr_str).digest('hex');
			// var frnd_hash = crypto.createHash('md5').update(frnd_str).digest('hex');

			var insertUser = 'insert into ' + master_table + ' (user_id,fr_id,comb_id) values (?,?,?);';
			var queries = [{
				query: insertUser,
				params: [data.user_id, data.fr_id, usr_hash]
			}, {
				query: insertUser,
				params: [data.fr_id, data.user_id, usr_hash]
			}];
			var queryOptions = { prepare: true, consistency: cassandra.types.consistencies.quorum };
			cassconn.batch(queries, queryOptions, function (err, res, r) {
				if (err) {
					defer.reject(err);
					return;
				}
				defer.resolve([]);
			});
		}

		return defer.promise;
	}
};

ChatMsgs.prototype.findCombIdFromMaster = function (data) {

	return findId().catch(globalFunctions.err);

	function findId() {
		var defer = Q.defer();
		var checkUser = 'select * from ' + master_table + ' where user_id = ? and fr_id = ?;';
		var params = [data.user_id, data.fr_id];
		cassconn.execute(checkUser, params, function (err, res, r) {
			if (err) {
				defer.reject(err);
				return;
			}
			// debug(res.rows[0]);
			defer.resolve(res.rows[0]);
		});
		return defer.promise;
	}
};

ChatMsgs.prototype.addMsg = function (data) {

	// data : {
	// 		from : '',
	// 		user_id : '',
	// 		fr_id : '',
	// 		msg : '',
	// }
	var self = this;
	return this.addUsersToMaster(data).then(function () {
		return self.findCombIdFromMaster(data);
	}).then(addMsg).catch(globalFunctions.err);

	function addMsg(row) {
		var defer = Q.defer();

		// debug(row,data);
		var insertMsg = 'insert into ' + detail_table + ' (comb_id,msg,msg_ts,msg_id,frm_usr,read,msg_del,msg_edit) values (?,?,?,?,?,?,?,?);';
		// var insertMsg = 'insert into ' + detail_table + ' (comb_id,msg,msg_ts,msg_id,frm_usr) values (?,?,?,?,?);';
		var date = new Date();
		// date.setTimezone('UTC');

		// var arr = [row.comb_id,data.msg,date,TimeUuid.now(),data.frm_usr];
		var arr = [row.comb_id, data.msg, date, TimeUuid.now(), data.frm_usr, false, false, false];
		// debug(arr);
		cassconn.execute(insertMsg, arr, function (err, res, r) {
			if (err) {
				defer.reject(err);
				return;
			}
			defer.resolve([]);
		});
		return defer.promise;
	}
};

ChatMsgs.prototype.findMsgs = function (data) {

	// data : {
	// 		user_id : '',
	// 		fr_id : ''
	// }
	var self = this;
	return this.findCombIdFromMaster(data).then(findMsg).catch(globalFunctions.err);

	function findMsg(row) {
		if (!row) {
			return [];
		}
		var defer = Q.defer();
		var selectMsg = 'select * from  ' + read_detail_table + ' where comb_id = ?;';
		cassconn.execute(selectMsg, [row.comb_id], function (err, res, r) {
			if (err) {
				defer.reject(err);
				return;
			}
			console.log(res.rows);
			defer.resolve(res.rows);
		});
		return defer.promise;
	}
};

ChatMsgs.prototype.clearMsgs = function (data) {
	return this.findCombIdFromMaster(data).then(clearMsg).catch(globalFunctions.err);

	function clearMsg(row) {
		var defer = Q.defer();
		var selectMsg = 'delete from  ' + detail_table + ' where comb_id = ?;';
		cassconn.execute(selectMsg, [row.comb_id], function (err, res, r) {
			if (err) {
				defer.reject(err);
				return;
			}
			// console.log(res.rows);
			defer.resolve(res.rows);
		});
		return defer.promise;
	}
};

ChatMsgs.prototype.deleteMsgs = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var deleteMsg;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						deleteMsg = function deleteMsg(data) {
							var defer = Q.defer();
							var updateDelFlag = 'UPDATE ' + detail_table + ' SET msg_del = ? WHERE msg_id = ? and comb_id = ? and msg_ts = ?;';
							var date = new Date(data.msg_ts);
							var params = [true, data.msg_id, data.comb_id, date];
							cassconn.execute(updateDelFlag, params, function (err, res, r) {
								if (err) {
									defer.reject(err);
									return;
								};
								defer.resolve({
									status: "success",
									message: "Successfully deleted."
								});
							});
							return defer.promise;
						};

						if (!(!data.msg_ts || !data.msg_id || !data.comb_id)) {
							_context.next = 3;
							break;
						}

						return _context.abrupt('return', {
							status: "error",
							message: "msg_ts ,  msg_id,  comb_id fields are required"
						});

					case 3:
						return _context.abrupt('return', deleteMsg(data).catch(globalFunctions.err));

					case 4:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this);
	}));

	return function (_x) {
		return _ref.apply(this, arguments);
	};
}();

ChatMsgs.prototype.editMsgs = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var editMsgs;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						editMsgs = function editMsgs(data) {
							var defer = Q.defer();
							var updateMsg = 'UPDATE ' + detail_table + ' SET msg_edit = ? , msg = ? WHERE msg_id = ? and comb_id = ? and msg_ts = ?;';
							var date = new Date(data.msg_ts);
							var params = [true, data.msg, data.msg_id, data.comb_id, date];

							cassconn.execute(updateMsg, params, function (err, res, r) {
								if (err) {
									defer.reject(err);
									return;
								}
								defer.resolve({
									status: "success",
									message: "Successfully edited."
								});
							});
							return defer.promise;
						};

						if (!(!data.msg_ts || !data.msg || !data.msg_id || !data.comb_id)) {
							_context2.next = 3;
							break;
						}

						return _context2.abrupt('return', {
							status: "error",
							message: "msg_ts , msg,  msg_id,  comb_id fields are required"
						});

					case 3:
						return _context2.abrupt('return', editMsgs(data).catch(globalFunctions.err));

					case 4:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this);
	}));

	return function (_x2) {
		return _ref2.apply(this, arguments);
	};
}();

module.exports = new ChatMsgs();