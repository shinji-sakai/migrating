'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var mongodb = require('mongodb');
var authorizedItemsAPI = require(configPath.api.authorizeitems);
var debug = require('debug')('app:api:qa:HighlightText');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var NotesAPI = require(configPath.api.notes.notes);

var ew_usr_hlights_table = "ew1.ew_usr_hlights";

function HighlightText() {}

HighlightText.prototype.saveHighlightText = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var usr_id, crs_id, mdl_id, src_id, hlight_id, hlight_src, select_txt, range, note, crt_dt, noteInfo, query, params;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        usr_id = data.usr_id, crs_id = data.crs_id, mdl_id = data.mdl_id, src_id = data.src_id, hlight_id = data.hlight_id, hlight_src = data.hlight_src, select_txt = data.select_txt, range = data.range, note = data.note, crt_dt = data.crt_dt;


                        if (!hlight_id) {
                            hlight_id = Uuid.random();
                        }

                        noteInfo = {};

                        if (!note) {
                            _context.next = 8;
                            break;
                        }

                        _context.next = 7;
                        return NotesAPI.insertNote({
                            usr_id: usr_id,
                            crs_id: crs_id,
                            mdl_id: mdl_id,
                            src_id: src_id,
                            note: note,
                            hlight_id: hlight_id,
                            note_src: hlight_src
                        });

                    case 7:
                        noteInfo = _context.sent;

                    case 8:
                        query = 'insert into ' + ew_usr_hlights_table + ' (usr_id,crs_id,mdl_id,src_id,hlight_id,hlight_src,select_txt,range,crt_dt,note,note_id) values (?,?,?,?,?,?,?,?,?,?,?)';
                        params = [usr_id, crs_id, mdl_id, src_id, hlight_id, hlight_src, select_txt, range, new Date(), note, noteInfo.note_id];
                        _context.next = 12;
                        return cassExecute(query, params);

                    case 12:
                        return _context.abrupt('return', (0, _extends3.default)({}, data, {
                            hlight_id: hlight_id,
                            note_id: noteInfo.note_id
                        }));

                    case 15:
                        _context.prev = 15;
                        _context.t0 = _context['catch'](0);
                        throw new Error(_context.t0);

                    case 18:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 15]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

HighlightText.prototype.removeHighlightText = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var usr_id, src_id, hlight_id, query, params;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        usr_id = data.usr_id, src_id = data.src_id, hlight_id = data.hlight_id;
                        query = 'delete from ' + ew_usr_hlights_table + ' where usr_id = ? and src_id = ? and hlight_id = ?';
                        params = [usr_id, src_id, hlight_id];
                        _context2.next = 6;
                        return cassExecute(query, params);

                    case 6:
                        return _context2.abrupt('return', data);

                    case 9:
                        _context2.prev = 9;
                        _context2.t0 = _context2['catch'](0);
                        throw new Error(_context2.t0);

                    case 12:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 9]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

HighlightText.prototype.updateHighlightNote = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var usr_id, src_id, hlight_id, note_id, note, query, params;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        usr_id = data.usr_id, src_id = data.src_id, hlight_id = data.hlight_id, note_id = data.note_id, note = data.note;
                        _context3.next = 4;
                        return NotesAPI.updateNote({
                            usr_id: usr_id,
                            src_id: src_id,
                            note_id: note_id,
                            note: note
                        });

                    case 4:
                        query = 'update ' + ew_usr_hlights_table + ' set note = ? where usr_id = ? and src_id = ? and hlight_id = ?';
                        params = [note, usr_id, src_id, hlight_id];
                        _context3.next = 8;
                        return cassExecute(query, params);

                    case 8:
                        return _context3.abrupt('return', data);

                    case 11:
                        _context3.prev = 11;
                        _context3.t0 = _context3['catch'](0);
                        throw new Error(_context3.t0);

                    case 14:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 11]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

HighlightText.prototype.getQuestionHighlights = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var usr_id, src_id, query, params, res;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        usr_id = data.usr_id, src_id = data.src_id;
                        query = 'select * from ' + ew_usr_hlights_table + ' where usr_id = ? and src_id = ?';
                        params = [usr_id, src_id];
                        _context4.next = 6;
                        return cassExecute(query, params);

                    case 6:
                        res = _context4.sent;
                        return _context4.abrupt('return', res);

                    case 10:
                        _context4.prev = 10;
                        _context4.t0 = _context4['catch'](0);
                        throw new Error(_context4.t0);

                    case 13:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 10]]);
    }));

    return function (_x4) {
        return _ref4.apply(this, arguments);
    };
}();

HighlightText.prototype.getQuestionHighlights = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
        var usr_id, src_id, query, params, res;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _context5.prev = 0;
                        usr_id = data.usr_id, src_id = data.src_id;
                        query = 'select * from ' + ew_usr_hlights_table + ' where usr_id = ? and src_id = ?';
                        params = [usr_id, src_id];
                        _context5.next = 6;
                        return cassExecute(query, params);

                    case 6:
                        res = _context5.sent;
                        return _context5.abrupt('return', res);

                    case 10:
                        _context5.prev = 10;
                        _context5.t0 = _context5['catch'](0);
                        throw new Error(_context5.t0);

                    case 13:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this, [[0, 10]]);
    }));

    return function (_x5) {
        return _ref5.apply(this, arguments);
    };
}();

HighlightText.prototype.getUserHighlights = function () {
    var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
        var connection, col, resArr, items, itemsDetails, itemMapping, finalArr;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        _context6.prev = 0;
                        _context6.next = 3;
                        return getConnection();

                    case 3:
                        connection = _context6.sent;
                        _context6.next = 6;
                        return dbCollection(connection);

                    case 6:
                        col = _context6.sent;
                        _context6.next = 9;
                        return col.find({ userId: data.usr_id }).toArray();

                    case 9:
                        resArr = _context6.sent;
                        items = resArr.map(function (v, i) {
                            return v.itemId;
                        });
                        _context6.next = 13;
                        return authorizedItemsAPI.getItemsUsingItemId({ itemId: items });

                    case 13:
                        itemsDetails = _context6.sent;
                        itemMapping = {};

                        itemsDetails.map(function (v, i) {
                            itemMapping[v.itemId] = v;
                        });

                        finalArr = resArr.map(function (v, i) {
                            var obj = {};
                            obj = Object.assign(obj, v, itemMapping[v.itemId]);
                            return obj;
                        });
                        return _context6.abrupt('return', finalArr);

                    case 20:
                        _context6.prev = 20;
                        _context6.t0 = _context6['catch'](0);

                        debug(_context6.t0);
                        throw _context6.t0;

                    case 24:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, this, [[0, 20]]);
    }));

    return function (_x6) {
        return _ref6.apply(this, arguments);
    };
}();

function dbCollection(db) {
    return db.collection('highlightedText');
}

module.exports = new HighlightText();