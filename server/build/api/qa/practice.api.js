'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var config = require('../../config.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var utility = require(configPath.lib.utility);
var Base64 = require(configPath.lib.utility).Base64;
var mongodb = require('mongodb');
var shortid = require('shortid');
var logger = require(configPath.lib.log_to_file);
var cassandra = require('cassandra-driver');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;
var oracledb = require(configPath.dbconn.oracledb);
var moment = require("moment");
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

var ew_verify_questions_table = "ew1.ew_verify_questions";
var ew_verify_questions_oracle = "ew_verify_questions";

//this will store exam_id , exam status and exam start time
var USER_EXAM_REDIS_KEY = function USER_EXAM_REDIS_KEY(usr_id, topic_id) {
    return usr_id + "-" + topic_id;
};

// this will store original sequesnce of question in topic
var ORIGINAL_QUE_SEQ_REDIS_KEY = function ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id) {
    return topic_id + "-qid";
};

// this will store user question sequesnce for topic
var USER_QUE_SEQ_REDIS_KEY = function USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id) {
    return usr_id + "-" + topic_id + "-qid";
};

// this will store user exam question data for exam
var USER_EXAM_QUESTION_DATA_REDIS_KEY = function USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, time) {
    time = moment(time).format("DD-MMM-YYYY-h:mm:ss");
    return usr_id + "-" + topic_id + "-" + time;
};

//this will store skipped question ids
var USER_EXAM_SKIPPED_QUESTION_REDIS_KEY = function USER_EXAM_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id, time) {
    time = moment(time).format("DD-MMM-YYYY-h:mm:ss");
    return usr_id + "-" + topic_id + "-" + time + "-skipped";
};

//this will store review question ids
var USER_EXAM_REVIEW_QUESTION_REDIS_KEY = function USER_EXAM_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id, time) {
    time = moment(time).format("DD-MMM-YYYY-h:mm:ss");
    return usr_id + "-" + topic_id + "-" + time + "-review";
};

var USER_PRACTICE_QUESTION_DATA_REDIS_KEY = function USER_PRACTICE_QUESTION_DATA_REDIS_KEY(usr_id, topic_id) {
    return usr_id + "-" + topic_id;
};

//this will store skipped question ids
var USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY = function USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id) {
    return usr_id + "-" + topic_id + "-skipped";
};

//this will store review question ids
var USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY = function USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id) {
    return usr_id + "-" + topic_id + "-review";
};

var PRACTICE_TOPIC_QUESTIONS_REDIS_KEY = function PRACTICE_TOPIC_QUESTIONS_REDIS_KEY(topic_id) {
    return topic_id + "-qids";
};

var PracticeQuestion = function () {
    function PracticeQuestion() {
        (0, _classCallCheck3.default)(this, PracticeQuestion);
    }

    (0, _createClass3.default)(PracticeQuestion, [{
        key: 'saveQuestion',


        /**
         * @param {JSON} data
         * @param {string} data.questionNumber - Question Number 
         * @param {string} data.questionId - Question Id
         * @param {string} data.question - Question Description.
         * @param {string} data.questionTime - Question Time.
         * @param {Object} data.options - Option contains id , text which will be option html/text
         * @param {string} data.textExplanation - Text Explanation
         * @param {Array}  data.ans - Encrypted answer id
         * @param {Array}  data.subjects - Subject in which this question comes
         * @param {Array}  data.authors - Related Authors
         * @param {Array}  data.publications - Related publications
         * @param {Array}  data.books - Related books
         * @param {Array}  data.exams - Related exams
         * @param {Array}  data.topics - Related topics
         * @param {Array}  data.topicGroup - Related topicGroup
         * @param {Array}  data.tags - Related tags
         * @param {string}  data.questionDifficulty - Question Difficulties
         * @returns {Promise}
         * @desc
         * - This method is used to save/update practice question
         * - If question contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text question to normal html, we will store original question html also for future editing {@link Utility#mathTypeSet}
         * - If any options contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text option to normal html, we will store original option html also for future editing {@link Utility#mathTypeSet}
         * - We will update/insert question to {@link MongoTables#practiceQuestions}
         * - Tables Used : {@link MongoTables#practiceQuestions}     
         */
        value: function saveQuestion(data) {
            var saveQue = function () {
                var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(col) {
                    var currentData, nextQuestionId, nextQuestionNumber, originalQuestion, mathRes, originalTextExplanation, processed, i, opt, _processed;

                    return _regenerator2.default.wrap(function _callee$(_context) {
                        while (1) {
                            switch (_context.prev = _context.next) {
                                case 0:
                                    if (!data.questionId) {
                                        data.questionId = shortid.generate();
                                    }

                                    if (data.isNewQuestion) {
                                        data.create_ts = new Date();
                                    } else {
                                        data.update_ts = new Date();
                                    }

                                    if (!data.isNewQuestion) {
                                        _context.next = 12;
                                        break;
                                    }

                                    _context.next = 5;
                                    return col.find({ "topicGroup": { "$in": [data.topicGroup[0]] } }).sort({ "questionNumber": -1 }).toArray();

                                case 5:
                                    currentData = _context.sent;
                                    nextQuestionId = "";
                                    nextQuestionNumber = "";

                                    if (currentData && currentData[0]) {
                                        nextQuestionId = data.topicGroup[0] + "-" + (currentData[0].questionNumber + 1);
                                        nextQuestionNumber = currentData[0].questionNumber + 1;
                                    } else {
                                        nextQuestionId = data.topicGroup[0] + "-" + 1;
                                        nextQuestionNumber = 1;
                                    }
                                    data.questionId = nextQuestionId;
                                    data.questionNumber = nextQuestionNumber;
                                    logger.debug("question going to be saved as : ", nextQuestionId);

                                case 12:
                                    _context.prev = 12;
                                    originalQuestion = data.question;

                                    data.originalQuestion = originalQuestion;
                                    _context.next = 17;
                                    return utility.mathTypeSet(data.question);

                                case 17:
                                    mathRes = _context.sent;

                                    data.question = mathRes || data.question;
                                    _context.next = 24;
                                    break;

                                case 21:
                                    _context.prev = 21;
                                    _context.t0 = _context['catch'](12);

                                    logger.debug(_context.t0);

                                case 24:
                                    _context.prev = 24;
                                    originalTextExplanation = data.textExplanation;

                                    data.originalTextExplanation = originalTextExplanation;
                                    _context.next = 29;
                                    return utility.mathTypeSet(data.textExplanation);

                                case 29:
                                    processed = _context.sent;

                                    data.textExplanation = processed || data.textExplanation;
                                    _context.next = 36;
                                    break;

                                case 33:
                                    _context.prev = 33;
                                    _context.t1 = _context['catch'](24);

                                    logger.debug(_context.t1);

                                case 36:
                                    _context.prev = 36;
                                    i = 0;

                                case 38:
                                    if (!(i < data.options.length)) {
                                        _context.next = 48;
                                        break;
                                    }

                                    opt = data.options[i];
                                    _context.next = 42;
                                    return utility.mathTypeSet(opt.text);

                                case 42:
                                    _processed = _context.sent;

                                    data.options[i].originalText = opt.text;
                                    data.options[i].text = _processed || opt.text;

                                case 45:
                                    i++;
                                    _context.next = 38;
                                    break;

                                case 48:
                                    _context.next = 53;
                                    break;

                                case 50:
                                    _context.prev = 50;
                                    _context.t2 = _context['catch'](36);

                                    logger.debug(_context.t2);

                                case 53:

                                    delete data["isNewQuestion"];
                                    return _context.abrupt('return', col.update({
                                        questionId: data.questionId
                                    }, { "$set": data }, {
                                        upsert: true
                                    }));

                                case 55:
                                case 'end':
                                    return _context.stop();
                            }
                        }
                    }, _callee, this, [[12, 21], [24, 33], [36, 50]]);
                }));

                return function saveQue(_x) {
                    return _ref.apply(this, arguments);
                };
            }();

            return getConnection().then(dbCollection).then(saveQue).catch(globalFunctions.err);
        }

        /**
         * @param {JSON} data
         * @param {string} data.questionNumber - Question Number 
         * @param {string} data.questionId - Question Id
         * @param {string} data.question - Question Description.
         * @param {string} data.questionTime - Question Time.
         * @param {Object} data.options - Option contains id , text which will be option html/text
         * @param {string} data.textExplanation - Text Explanation
         * @param {Array}  data.ans - Encrypted answer id
         * @param {Array}  data.subjects - Subject in which this question comes
         * @param {Array}  data.authors - Related Authors
         * @param {Array}  data.publications - Related publications
         * @param {Array}  data.books - Related books
         * @param {Array}  data.exams - Related exams
         * @param {Array}  data.topics - Related topics
         * @param {Array}  data.topicGroup - Related topicGroup
         * @param {Array}  data.tags - Related tags
         * @param {string}  data.questionDifficulty - Question Difficulties
         * @returns {Promise}
         * @desc
         * - This method is used to save/update preview practice question
         * - If question contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text question to normal html, we will store original question html also for future editing {@link Utility#mathTypeSet}
         * - If any options contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text option to normal html, we will store original option html also for future editing {@link Utility#mathTypeSet}
         * - We will update/insert question to {@link MongoTables#practiceQuestions}
         * - Tables Used : {@link MongoTables#practiceQuestions}     
         */

    }, {
        key: 'savePreviewQuestion',
        value: function savePreviewQuestion(data) {
            var saveQue = function () {
                var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(col) {
                    var currentData, nextQuestionId, originalQuestion, mathRes, originalTextExplanation, processed, i, opt, _processed2;

                    return _regenerator2.default.wrap(function _callee2$(_context2) {
                        while (1) {
                            switch (_context2.prev = _context2.next) {
                                case 0:
                                    if (!data.questionId) {
                                        data.questionId = shortid.generate();
                                    }

                                    if (data.isNewQuestion) {
                                        data.create_ts = new Date();
                                    } else {
                                        data.update_ts = new Date();
                                    }

                                    if (!data.isNewQuestion) {
                                        _context2.next = 9;
                                        break;
                                    }

                                    _context2.next = 5;
                                    return col.find({ "topicGroup": { "$in": [data.topicGroup[0]] } }).sort({ "questionNumber": -1 }).toArray();

                                case 5:
                                    currentData = _context2.sent;
                                    nextQuestionId = "";

                                    if (currentData && currentData[0]) {
                                        nextQuestionId = data.topicGroup[0] + "-" + (currentData[0].questionNumber + 1);
                                    } else {
                                        nextQuestionId = data.topicGroup[0] + "-" + 1;
                                    }
                                    data.questionId = nextQuestionId;

                                case 9:
                                    _context2.prev = 9;
                                    originalQuestion = data.question;

                                    data.originalQuestion = originalQuestion;
                                    _context2.next = 14;
                                    return utility.mathTypeSet(data.question);

                                case 14:
                                    mathRes = _context2.sent;

                                    data.question = mathRes || data.question;
                                    _context2.next = 21;
                                    break;

                                case 18:
                                    _context2.prev = 18;
                                    _context2.t0 = _context2['catch'](9);

                                    logger.debug(_context2.t0);

                                case 21:
                                    _context2.prev = 21;
                                    originalTextExplanation = data.textExplanation;

                                    data.originalTextExplanation = originalTextExplanation;
                                    _context2.next = 26;
                                    return utility.mathTypeSet(data.textExplanation);

                                case 26:
                                    processed = _context2.sent;

                                    data.textExplanation = processed || data.textExplanation;
                                    _context2.next = 33;
                                    break;

                                case 30:
                                    _context2.prev = 30;
                                    _context2.t1 = _context2['catch'](21);

                                    logger.debug(_context2.t1);

                                case 33:
                                    _context2.prev = 33;
                                    i = 0;

                                case 35:
                                    if (!(i < data.options.length)) {
                                        _context2.next = 45;
                                        break;
                                    }

                                    opt = data.options[i];
                                    _context2.next = 39;
                                    return utility.mathTypeSet(opt.text);

                                case 39:
                                    _processed2 = _context2.sent;

                                    data.options[i].originalText = opt.text;
                                    data.options[i].text = _processed2 || opt.text;

                                case 42:
                                    i++;
                                    _context2.next = 35;
                                    break;

                                case 45:
                                    _context2.next = 50;
                                    break;

                                case 47:
                                    _context2.prev = 47;
                                    _context2.t2 = _context2['catch'](33);

                                    logger.debug(_context2.t2);

                                case 50:

                                    delete data["isNewQuestion"];
                                    return _context2.abrupt('return', col.update({
                                        questionId: data.questionId
                                    }, { "$set": data }, {
                                        upsert: true
                                    }));

                                case 52:
                                case 'end':
                                    return _context2.stop();
                            }
                        }
                    }, _callee2, this, [[9, 18], [21, 30], [33, 47]]);
                }));

                return function saveQue(_x2) {
                    return _ref2.apply(this, arguments);
                };
            }();

            return getConnection().then(dbColl).then(saveQue).catch(globalFunctions.err);

            function dbColl(db) {
                return db.collection('practicePreviewQuestions');
            }
        }
    }, {
        key: 'findPreviewQuestionsByQuery',
        value: function findPreviewQuestionsByQuery(data) {
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    if (data[key].length > 0) {
                        data[key] = {
                            $in: data[key]
                        };
                    } else {
                        delete data[key];
                    }
                }
            }
            //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}

            function dbColl(db) {
                return db.collection('practicePreviewQuestions');
            }

            return getConnection().then(dbColl).then(find).catch(globalFunctions.err);

            function find(col) {
                return col.find(data).toArray();
            }
        }
    }, {
        key: 'findQuestionsByQuery',
        value: function findQuestionsByQuery(data) {

            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    if (data[key].length > 0) {
                        data[key] = {
                            $in: data[key]
                        };
                    } else {
                        delete data[key];
                    }
                }
            }
            //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}

            return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

            function find(col) {
                return col.find(data).toArray();
            }
        }
    }, {
        key: 'findQuestionsTextByQuery',
        value: function findQuestionsTextByQuery(data) {
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    if (data[key].length > 0) {
                        data[key] = {
                            $in: data[key]
                        };
                    } else {
                        delete data[key];
                    }
                }
            }
            //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}

            return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

            function find(col) {
                return col.find(data, { questionNumber: 1, questionId: 1, question: 1 }).toArray();
            }
        }

        // find questions id and name by query

    }, {
        key: 'findQuestionsIdByQuery',
        value: function findQuestionsIdByQuery(data) {

            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    if (data[key].length > 0) {
                        data[key] = {
                            $in: data[key]
                        };
                    } else {
                        delete data[key];
                    }
                }
            }
            //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}

            return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

            function find(col) {
                return col.find(data, { questionNumber: 1, questionId: 1 }).toArray();
            }
        }
    }, {
        key: 'removeQuestion',
        value: function removeQuestion(data) {
            return getConnection().then(dbCollection).then(removeQue).catch(globalFunctions.err);

            function removeQue(col) {
                return col.remove({
                    questionId: data.questionId
                });
            }
        }
    }, {
        key: 'getAllQuestions',
        value: function getAllQuestions(data) {
            return getConnection().then(dbCollection).then(getAllQues).catch(globalFunctions.err);

            function getAllQues(col) {
                return col.find({}).toArray();
            }
        }
    }, {
        key: 'getPracticeQuestionsById',
        value: function getPracticeQuestionsById(data) {
            return getConnection().then(dbCollection).then(getAllQues).catch(globalFunctions.err);

            function getAllQues(col) {
                return col.find({
                    questionId: {
                        $in: data.questionsId
                    }
                }).toArray();
            }
        }
    }, {
        key: 'getPracticeQuestionsByLimit',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
                var start, no_of_question, questionsId, db, col, cursor, questions, getAllQues;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                getAllQues = function getAllQues(col) {
                                    return col.find({
                                        questionId: {
                                            $in: data.questionsId
                                        }
                                    }).toArray();
                                };

                                _context3.prev = 1;
                                start = data.start, no_of_question = data.no_of_question, questionsId = data.questionsId;
                                _context3.next = 5;
                                return getConnection();

                            case 5:
                                db = _context3.sent;
                                _context3.next = 8;
                                return dbCollection(db);

                            case 8:
                                col = _context3.sent;
                                _context3.next = 11;
                                return col.find({
                                    questionId: {
                                        $in: questionsId || []
                                    }
                                });

                            case 11:
                                cursor = _context3.sent;

                                if (start) {
                                    cursor = cursor.skip(start || 0);
                                }
                                _context3.next = 15;
                                return cursor.limit(no_of_question || 0).toArray();

                            case 15:
                                questions = _context3.sent;
                                return _context3.abrupt('return', questions);

                            case 19:
                                _context3.prev = 19;
                                _context3.t0 = _context3['catch'](1);

                                logger.debug(_context3.t0);
                                throw new Error(_context3.t0);

                            case 23:
                                return _context3.abrupt('return', getConnection().then(dbCollection).then(getAllQues).catch(globalFunctions.err));

                            case 24:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[1, 19]]);
            }));

            function getPracticeQuestionsByLimit(_x3) {
                return _ref3.apply(this, arguments);
            }

            return getPracticeQuestionsByLimit;
        }()
    }, {
        key: 'updateMultiplePracticeQuestions',
        value: function updateMultiplePracticeQuestions(data) {
            return getConnection().then(dbCollection).then(updateAllQues).catch(globalFunctions.err);

            function updateAllQues(col) {
                for (var i = data.length - 1; i >= 0; i--) {
                    var question = data[i];
                    console.log(data[i]);
                    col.update({
                        questionId: question.questionId
                    }, {
                        $set: {
                            topics: data[i].topics
                        }
                    });
                }
                return [];
            }
        }
    }, {
        key: 'updateVerifyPracticeQuestion',
        value: function () {
            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
                var previous_verify_id, verify_id, q_id, tester_id, tested_dt, test_status, test_comments, q_state_start_dt, q_state_end_dt, queries, cass_query, cass_params, prev_rows, prev_uuid, oracle_query, conn, res, oracle_params;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                previous_verify_id = data.previous_verify_id;
                                verify_id = data.verify_id || TimeUuid.now();
                                q_id = data.q_id;
                                tester_id = data.tester_id || '';
                                tested_dt = data.tested_dt ? new Date(data.tested_dt) : undefined;
                                test_status = data.test_status || '';
                                test_comments = data.test_comments || '';
                                q_state_start_dt = data.q_state_start_dt ? new Date(data.q_state_start_dt) : undefined;
                                q_state_end_dt = data.q_state_end_dt ? new Date(data.q_state_end_dt) : undefined;
                                queries = [];
                                cass_query = "select max(verify_id) from " + ew_verify_questions_table + " where q_id=?";
                                cass_params = [q_id];
                                _context4.next = 15;
                                return cassExecute(cass_query, cass_params);

                            case 15:
                                prev_rows = _context4.sent;

                                if (!(prev_rows && prev_rows[0] && prev_rows[0]["system.max(verify_id)"] !== null)) {
                                    _context4.next = 22;
                                    break;
                                }

                                prev_uuid = prev_rows[0]["system.max(verify_id)"];

                                cass_query = "update " + ew_verify_questions_table + " set q_state_end_dt=? where verify_id=? and q_id=?";
                                cass_params = [new Date(), prev_uuid, q_id];
                                _context4.next = 22;
                                return cassExecute(cass_query, cass_params);

                            case 22:

                                cass_query = "update " + ew_verify_questions_table + " set tester_id=?,tested_dt=?,test_status=?,test_comments=?,q_state_start_dt=? where verify_id=? and q_id=?";
                                cass_params = [tester_id, tested_dt, test_status, test_comments, q_state_start_dt, verify_id, q_id];
                                queries.push({
                                    query: cass_query,
                                    params: cass_params
                                });
                                _context4.next = 27;
                                return cassBatch(queries);

                            case 27:
                                oracle_query = "select max(verify_id) from " + ew_verify_questions_oracle + " where q_id='" + q_id + "'";
                                _context4.next = 30;
                                return oracledb.getConnection();

                            case 30:
                                conn = _context4.sent;
                                _context4.next = 33;
                                return conn.execute(oracle_query, [], {
                                    autoCommit: true
                                });

                            case 33:
                                res = _context4.sent;

                                if (!(res.rows && res.rows[0] && res.rows[0][0])) {
                                    _context4.next = 39;
                                    break;
                                }

                                prev_uuid = res.rows[0][0];

                                oracle_query = 'update ' + ew_verify_questions_oracle + ' set q_state_end_dt=' + ("to_date('" + moment().format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')") + ' where verify_id=' + ("'" + prev_uuid + "'") + ' and q_id=' + ("'" + q_id + "'");
                                _context4.next = 39;
                                return conn.execute(oracle_query, [], {
                                    autoCommit: true
                                });

                            case 39:
                                _context4.next = 41;
                                return oracledb.releaseConnection(conn);

                            case 41:

                                oracle_query = 'insert into ' + ew_verify_questions_oracle + ' (verify_id,tester_id,test_status,test_comments,q_state_start_dt,q_id) values (' + ("'" + verify_id + "'") + ',' + ("'" + tester_id + "'") + ',' + ("'" + test_status + "'") + ',' + ("'" + test_comments + "'") + ',' + ("to_date('" + moment(q_state_start_dt).format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')") + ',' + ("'" + q_id + "'") + ')';
                                oracle_params = ['' + verify_id, tester_id, tested_dt, '' + test_status, '' + test_comments, q_state_start_dt, q_id];
                                _context4.next = 45;
                                return oracledb.getConnection();

                            case 45:
                                conn = _context4.sent;
                                _context4.next = 48;
                                return conn.execute(oracle_query, [], {
                                    autoCommit: true
                                });

                            case 48:
                                res = _context4.sent;
                                _context4.next = 51;
                                return oracledb.releaseConnection(conn);

                            case 51:
                                return _context4.abrupt('return', data);

                            case 54:
                                _context4.prev = 54;
                                _context4.t0 = _context4['catch'](0);

                                logger.debug(_context4.t0);
                                throw new Error(_context4.t0);

                            case 58:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[0, 54]]);
            }));

            function updateVerifyPracticeQuestion(_x4) {
                return _ref4.apply(this, arguments);
            }

            return updateVerifyPracticeQuestion;
        }()
    }, {
        key: 'getAllVerifyStatusOfPracticeQuestion',
        value: function () {
            var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
                var q_id, cass_query, cass_params, res;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.prev = 0;
                                q_id = data.q_id;
                                cass_query = "select * from " + ew_verify_questions_table + " where q_id=?";
                                cass_params = [q_id];
                                _context5.next = 6;
                                return cassExecute(cass_query, cass_params);

                            case 6:
                                res = _context5.sent;
                                return _context5.abrupt('return', res);

                            case 10:
                                _context5.prev = 10;
                                _context5.t0 = _context5['catch'](0);

                                logger.debug(_context5.t0);
                                throw new Error(_context5.t0);

                            case 14:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[0, 10]]);
            }));

            function getAllVerifyStatusOfPracticeQuestion(_x5) {
                return _ref5.apply(this, arguments);
            }

            return getAllVerifyStatusOfPracticeQuestion;
        }()
    }, {
        key: 'getLatestVerifyStatusOfGivenPracticeQuestions',
        value: function () {
            var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
                var q_id, id_map, i, que_id, cass_query, cass_params, res, rows, q, _cass_query, _cass_params, row;

                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _context6.prev = 0;
                                q_id = [].concat(data.q_id);
                                id_map = {};
                                i = 0;

                            case 4:
                                if (!(i < q_id.length)) {
                                    _context6.next = 15;
                                    break;
                                }

                                que_id = q_id[i];
                                cass_query = "select max(verify_id) from " + ew_verify_questions_table + " where q_id=?";
                                cass_params = [que_id];
                                _context6.next = 10;
                                return cassExecute(cass_query, cass_params);

                            case 10:
                                res = _context6.sent;

                                if (res && res[0] && res[0]["system.max(verify_id)"] !== null) {
                                    id_map[que_id] = res[0]["system.max(verify_id)"];
                                }

                            case 12:
                                i++;
                                _context6.next = 4;
                                break;

                            case 15:

                                logger.debug(id_map);

                                rows = [];
                                _context6.t0 = _regenerator2.default.keys(id_map);

                            case 18:
                                if ((_context6.t1 = _context6.t0()).done) {
                                    _context6.next = 28;
                                    break;
                                }

                                q = _context6.t1.value;
                                _cass_query = "select * from " + ew_verify_questions_table + " where q_id=? and verify_id=?";
                                _cass_params = [q, id_map[q]];
                                _context6.next = 24;
                                return cassExecute(_cass_query, _cass_params);

                            case 24:
                                row = _context6.sent;

                                rows.push(row[0]);
                                _context6.next = 18;
                                break;

                            case 28:
                                return _context6.abrupt('return', rows);

                            case 31:
                                _context6.prev = 31;
                                _context6.t2 = _context6['catch'](0);

                                logger.debug(_context6.t2);
                                throw new Error(_context6.t2);

                            case 35:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[0, 31]]);
            }));

            function getLatestVerifyStatusOfGivenPracticeQuestions(_x6) {
                return _ref6.apply(this, arguments);
            }

            return getLatestVerifyStatusOfGivenPracticeQuestions;
        }()
    }, {
        key: 'startUserExam',
        value: function () {
            var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
                var usr_id, usr_exam_id, topic_id, exam_start_time, status, seqOrRand, prv_exam, questionSeq, shuffledUserQueSeq, firstQuestion, q_data;
                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                _context7.prev = 0;
                                usr_id = data.usr_id, usr_exam_id = data.usr_exam_id, topic_id = data.topic_id, exam_start_time = data.exam_start_time, status = data.status, seqOrRand = data.seqOrRand;
                                _context7.next = 4;
                                return this.checkForPreviousExam({
                                    usr_id: usr_id,
                                    topic_id: topic_id
                                });

                            case 4:
                                prv_exam = _context7.sent;

                                logger.debug("prv_exam...", prv_exam);

                                if (!(prv_exam && prv_exam.previousExamRunning)) {
                                    _context7.next = 9;
                                    break;
                                }

                                _context7.next = 9;
                                return this.finishUserExam({
                                    usr_id: usr_id,
                                    topic_id: topic_id,
                                    usr_exam_id: prv_exam.exam_id
                                });

                            case 9:
                                _context7.next = 11;
                                return redisCli.hmsetAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id), {
                                    "exam_id": "" + usr_exam_id,
                                    "status": status || "running",
                                    "start_time": "" + exam_start_time
                                });

                            case 11:
                                _context7.next = 13;
                                return redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);

                            case 13:
                                questionSeq = _context7.sent;

                                if (!seqOrRand || seqOrRand === "random") {
                                    // if we want random question sequence
                                    shuffledUserQueSeq = utility.shuffleArray([].concat(questionSeq || []));
                                } else if (seqOrRand === "sequence") {
                                    // if we want sequential question sequence as it is
                                    shuffledUserQueSeq = questionSeq;
                                }
                                //delete user quesion seq
                                _context7.next = 17;
                                return redisCli.delAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id));

                            case 17:
                                _context7.next = 19;
                                return redisCli.rpushAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), shuffledUserQueSeq || []);

                            case 19:
                                _context7.next = 21;
                                return redisCli.hgetallAsync(shuffledUserQueSeq[0]);

                            case 21:
                                firstQuestion = _context7.sent;

                                if (firstQuestion && firstQuestion != 'null' && firstQuestion.json) {
                                    firstQuestion = JSON.parse(firstQuestion.json);
                                } else {
                                    firstQuestion = {};
                                }
                                _context7.next = 25;
                                return this.getUserExamQuestionData({
                                    usr_id: usr_id,
                                    exam_id: usr_exam_id,
                                    questionId: firstQuestion.questionId,
                                    topic_id: topic_id
                                });

                            case 25:
                                q_data = _context7.sent;

                                q_data = q_data || {};

                                return _context7.abrupt('return', {
                                    totalQuestions: shuffledUserQueSeq.length,
                                    exam_id: usr_exam_id,
                                    usr_id: usr_id,
                                    firstQuestion: (0, _extends3.default)({}, firstQuestion, q_data)
                                });

                            case 30:
                                _context7.prev = 30;
                                _context7.t0 = _context7['catch'](0);

                                logger.debug(_context7.t0);
                                throw new Error(_context7.t0);

                            case 34:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this, [[0, 30]]);
            }));

            function startUserExam(_x7) {
                return _ref7.apply(this, arguments);
            }

            return startUserExam;
        }()
    }, {
        key: 'finishUserExam',
        value: function () {
            var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
                var usr_id, usr_exam_id, topic_id, exam, user_question_data, exam_question_ids, ques, i, q_id, question, result_exam;
                return _regenerator2.default.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                _context8.prev = 0;
                                usr_id = data.usr_id, usr_exam_id = data.usr_exam_id, topic_id = data.topic_id;
                                _context8.next = 4;
                                return this.getUserExamDetails({
                                    usr_id: usr_id,
                                    topic_id: topic_id
                                });

                            case 4:
                                exam = _context8.sent;
                                _context8.next = 7;
                                return redisCli.hgetallAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, exam.start_time));

                            case 7:
                                user_question_data = _context8.sent;
                                _context8.next = 10;
                                return redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);

                            case 10:
                                exam_question_ids = _context8.sent;

                                logger.debug("exam_question_ids...", exam_question_ids);
                                ques = {};
                                i = 0;

                            case 14:
                                if (!(i < exam_question_ids.length)) {
                                    _context8.next = 25;
                                    break;
                                }

                                q_id = exam_question_ids[i];

                                logger.debug("q_id...", q_id);
                                _context8.next = 19;
                                return redisCli.hgetallAsync(q_id);

                            case 19:
                                question = _context8.sent;

                                if (question === 'null' || !question) {
                                    question = {};
                                }
                                ques[q_id] = JSON.parse(question.json || {});

                            case 22:
                                i++;
                                _context8.next = 14;
                                break;

                            case 25:
                                result_exam = this.checkQuestionAnswers(ques, user_question_data);

                                //delete exam data for user

                                _context8.next = 28;
                                return redisCli.delAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id));

                            case 28:
                                _context8.next = 30;
                                return redisCli.delAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id));

                            case 30:
                                return _context8.abrupt('return', result_exam);

                            case 33:
                                _context8.prev = 33;
                                _context8.t0 = _context8['catch'](0);

                                logger.debug(_context8.t0);
                                throw new Error(_context8.t0);

                            case 37:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, this, [[0, 33]]);
            }));

            function finishUserExam(_x8) {
                return _ref8.apply(this, arguments);
            }

            return finishUserExam;
        }()
    }, {
        key: 'checkQuestionAnswers',
        value: function () {
            var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(questions, user_question_data) {
                var wrong_ans, correct_ans, skipped_que, review_que, attempted_que, totalQuestions, id, question, usr_data, ans, userAnswers, isRightAnswer, i, t_ans;
                return _regenerator2.default.wrap(function _callee9$(_context9) {
                    while (1) {
                        switch (_context9.prev = _context9.next) {
                            case 0:
                                _context9.prev = 0;
                                wrong_ans = [];
                                correct_ans = [];
                                skipped_que = [];
                                review_que = [];
                                attempted_que = Object.keys(user_question_data || {});
                                totalQuestions = Object.keys(questions || {});
                                _context9.t0 = _regenerator2.default.keys(user_question_data);

                            case 8:
                                if ((_context9.t1 = _context9.t0()).done) {
                                    _context9.next = 40;
                                    break;
                                }

                                id = _context9.t1.value;
                                question = questions[id];
                                usr_data = JSON.parse(user_question_data[id]);
                                ans = (question.ans || []).map(function (v, i) {
                                    return Base64.decode(v);
                                });
                                userAnswers = usr_data.userAnswers || [];


                                logger.debug("ans...userAnswers..", ans, userAnswers);

                                if (!usr_data.isSkipped) {
                                    _context9.next = 18;
                                    break;
                                }

                                skipped_que.push(question);
                                return _context9.abrupt('continue', 8);

                            case 18:
                                if (usr_data.markForReview) {
                                    review_que.push(question);
                                }

                                isRightAnswer = true;

                                if (!(userAnswers.length != ans.length)) {
                                    _context9.next = 24;
                                    break;
                                }

                                isRightAnswer = false;
                                _context9.next = 37;
                                break;

                            case 24:
                                if (!(ans.length === 1)) {
                                    _context9.next = 28;
                                    break;
                                }

                                //if there is only one ans
                                isRightAnswer = userAnswers[0] === ans[0];
                                _context9.next = 37;
                                break;

                            case 28:
                                i = 0;

                            case 29:
                                if (!(i < userAnswers.length)) {
                                    _context9.next = 37;
                                    break;
                                }

                                //check for all user answers to match real answers
                                t_ans = userAnswers[i];

                                if (!(ans.indexOf(t_ans) <= -1)) {
                                    _context9.next = 34;
                                    break;
                                }

                                isRightAnswer = false;
                                return _context9.abrupt('break', 37);

                            case 34:
                                i++;
                                _context9.next = 29;
                                break;

                            case 37:
                                if (isRightAnswer) {
                                    correct_ans.push(question);
                                } else {
                                    wrong_ans.push(question);
                                }
                                _context9.next = 8;
                                break;

                            case 40:
                                return _context9.abrupt('return', {
                                    wrong_ans: wrong_ans,
                                    correct_ans: correct_ans,
                                    attempted_que: attempted_que,
                                    skipped_que: skipped_que,
                                    review_que: review_que,
                                    totalQuestions: totalQuestions
                                });

                            case 43:
                                _context9.prev = 43;
                                _context9.t2 = _context9['catch'](0);

                                logger.debug(_context9.t2);
                                throw new Error(_context9.t2);

                            case 47:
                            case 'end':
                                return _context9.stop();
                        }
                    }
                }, _callee9, this, [[0, 43]]);
            }));

            function checkQuestionAnswers(_x9, _x10) {
                return _ref9.apply(this, arguments);
            }

            return checkQuestionAnswers;
        }()
    }, {
        key: 'getUserExamDetails',
        value: function () {
            var _ref10 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(data) {
                var usr_id, topic_id, exam;
                return _regenerator2.default.wrap(function _callee10$(_context10) {
                    while (1) {
                        switch (_context10.prev = _context10.next) {
                            case 0:
                                _context10.prev = 0;
                                usr_id = data.usr_id, topic_id = data.topic_id;
                                _context10.next = 4;
                                return redisCli.hgetallAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id));

                            case 4:
                                exam = _context10.sent;

                                exam = exam || {};
                                if (exam === 'null') {
                                    exam = {};
                                }
                                return _context10.abrupt('return', exam);

                            case 10:
                                _context10.prev = 10;
                                _context10.t0 = _context10['catch'](0);

                                logger.debug(_context10.t0);
                                throw new Error(_context10.t0);

                            case 14:
                            case 'end':
                                return _context10.stop();
                        }
                    }
                }, _callee10, this, [[0, 10]]);
            }));

            function getUserExamDetails(_x11) {
                return _ref10.apply(this, arguments);
            }

            return getUserExamDetails;
        }()
    }, {
        key: 'checkForPreviousExam',
        value: function () {
            var _ref11 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11(data) {
                var usr_id, topic_id, res, questionSeq, userQuestionSeq, userExamQuestionData, attemptedQuestions, nextQuestionIndex, nextQuestionId, que, q_data;
                return _regenerator2.default.wrap(function _callee11$(_context11) {
                    while (1) {
                        switch (_context11.prev = _context11.next) {
                            case 0:
                                _context11.prev = 0;
                                usr_id = data.usr_id, topic_id = data.topic_id;
                                _context11.next = 4;
                                return redisCli.hgetallAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id));

                            case 4:
                                res = _context11.sent;

                                if (!(res && res.exam_id && res.status)) {
                                    _context11.next = 31;
                                    break;
                                }

                                _context11.next = 8;
                                return redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);

                            case 8:
                                questionSeq = _context11.sent;
                                _context11.next = 11;
                                return redisCli.lrangeAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), 0, -1);

                            case 11:
                                userQuestionSeq = _context11.sent;
                                _context11.next = 14;
                                return redisCli.hgetallAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, res.start_time));

                            case 14:
                                userExamQuestionData = _context11.sent;
                                attemptedQuestions = Object.keys(userExamQuestionData || {});
                                nextQuestionIndex = attemptedQuestions.length || 0;


                                if (attemptedQuestions.length === userQuestionSeq.length) {
                                    nextQuestionIndex = userQuestionSeq.length - 1;
                                }

                                nextQuestionId = userQuestionSeq[nextQuestionIndex];
                                //get question

                                _context11.next = 21;
                                return redisCli.hgetallAsync(nextQuestionId);

                            case 21:
                                que = _context11.sent;

                                logger.debug(".....", que);
                                if (que && que !== 'null' && que.json) {
                                    que = JSON.parse(que.json);
                                } else {
                                    que = {};
                                }

                                //get question data
                                _context11.next = 26;
                                return this.getUserExamQuestionData({
                                    usr_id: usr_id,
                                    exam_id: res.exam_id,
                                    questionId: que.questionId,
                                    topic_id: topic_id
                                });

                            case 26:
                                q_data = _context11.sent;

                                q_data = q_data || {};

                                return _context11.abrupt('return', {
                                    previousExamRunning: true,
                                    exam_id: res.exam_id,
                                    totalQuestions: (userQuestionSeq || []).length,
                                    question: (0, _extends3.default)({}, que, q_data),
                                    questionIndex: nextQuestionIndex
                                });

                            case 31:
                                return _context11.abrupt('return', {
                                    previousExamRunning: false
                                });

                            case 32:
                                _context11.next = 38;
                                break;

                            case 34:
                                _context11.prev = 34;
                                _context11.t0 = _context11['catch'](0);

                                logger.debug(_context11.t0);
                                throw new Error(_context11.t0);

                            case 38:
                            case 'end':
                                return _context11.stop();
                        }
                    }
                }, _callee11, this, [[0, 34]]);
            }));

            function checkForPreviousExam(_x12) {
                return _ref11.apply(this, arguments);
            }

            return checkForPreviousExam;
        }()
    }, {
        key: 'saveUserExamQuestionData',
        value: function () {
            var _ref12 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12(data) {
                var usr_id, topic_id, questionId, exam_id, questionData, exam, jsonQData, isReview, isSkipped;
                return _regenerator2.default.wrap(function _callee12$(_context12) {
                    while (1) {
                        switch (_context12.prev = _context12.next) {
                            case 0:
                                _context12.prev = 0;
                                usr_id = data.usr_id, topic_id = data.topic_id, questionId = data.questionId, exam_id = data.exam_id, questionData = data.questionData;

                                if (questionId) {
                                    _context12.next = 4;
                                    break;
                                }

                                return _context12.abrupt('return');

                            case 4:
                                _context12.next = 6;
                                return this.getUserExamDetails({
                                    usr_id: usr_id,
                                    topic_id: topic_id
                                });

                            case 6:
                                exam = _context12.sent;
                                jsonQData = JSON.parse(questionData || {});
                                isReview = jsonQData["markForReview"];
                                isSkipped = jsonQData["isSkipped"];

                                if (!isSkipped) {
                                    _context12.next = 15;
                                    break;
                                }

                                _context12.next = 13;
                                return redisCli.saddAsync(USER_EXAM_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);

                            case 13:
                                _context12.next = 17;
                                break;

                            case 15:
                                _context12.next = 17;
                                return redisCli.sremAsync(USER_EXAM_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);

                            case 17:
                                if (!isReview) {
                                    _context12.next = 22;
                                    break;
                                }

                                _context12.next = 20;
                                return redisCli.saddAsync(USER_EXAM_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);

                            case 20:
                                _context12.next = 24;
                                break;

                            case 22:
                                _context12.next = 24;
                                return redisCli.sremAsync(USER_EXAM_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);

                            case 24:
                                _context12.next = 26;
                                return redisCli.hmsetAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId, questionData);

                            case 26:
                                return _context12.abrupt('return', {
                                    questionId: questionId,
                                    usr_id: usr_id,
                                    exam_id: exam_id
                                });

                            case 29:
                                _context12.prev = 29;
                                _context12.t0 = _context12['catch'](0);

                                logger.debug(_context12.t0);
                                throw new Error(_context12.t0);

                            case 33:
                            case 'end':
                                return _context12.stop();
                        }
                    }
                }, _callee12, this, [[0, 29]]);
            }));

            function saveUserExamQuestionData(_x13) {
                return _ref12.apply(this, arguments);
            }

            return saveUserExamQuestionData;
        }()
    }, {
        key: 'getUserExamQuestionData',
        value: function () {
            var _ref13 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee13(data) {
                var usr_id, questionId, exam_id, topic_id, exam, q_data;
                return _regenerator2.default.wrap(function _callee13$(_context13) {
                    while (1) {
                        switch (_context13.prev = _context13.next) {
                            case 0:
                                _context13.prev = 0;
                                usr_id = data.usr_id, questionId = data.questionId, exam_id = data.exam_id, topic_id = data.topic_id;
                                _context13.next = 4;
                                return this.getUserExamDetails({
                                    usr_id: usr_id,
                                    topic_id: topic_id
                                });

                            case 4:
                                exam = _context13.sent;
                                _context13.next = 7;
                                return redisCli.hmgetAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);

                            case 7:
                                q_data = _context13.sent;

                                logger.debug(q_data[0]);
                                if (!q_data[0] || q_data[0] === 'null') {
                                    q_data = {};
                                } else {
                                    q_data = JSON.parse(q_data[0]);
                                }
                                logger.debug(q_data);
                                return _context13.abrupt('return', q_data);

                            case 14:
                                _context13.prev = 14;
                                _context13.t0 = _context13['catch'](0);

                                logger.debug(_context13.t0);
                                throw new Error(_context13.t0);

                            case 18:
                            case 'end':
                                return _context13.stop();
                        }
                    }
                }, _callee13, this, [[0, 14]]);
            }));

            function getUserExamQuestionData(_x14) {
                return _ref13.apply(this, arguments);
            }

            return getUserExamQuestionData;
        }()
    }, {
        key: 'getUserExamQuestion',
        value: function () {
            var _ref14 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee14(data) {
                var usr_id, topic_id, questionIndex, exam_id, type, exam, userQuestionSeq, ret_res, question, q_data;
                return _regenerator2.default.wrap(function _callee14$(_context14) {
                    while (1) {
                        switch (_context14.prev = _context14.next) {
                            case 0:
                                _context14.prev = 0;
                                usr_id = data.usr_id, topic_id = data.topic_id, questionIndex = data.questionIndex, exam_id = data.exam_id, type = data.type;
                                _context14.next = 4;
                                return this.getUserExamDetails({
                                    usr_id: usr_id,
                                    topic_id: topic_id
                                });

                            case 4:
                                exam = _context14.sent;

                                if (!(type === "skipped")) {
                                    _context14.next = 11;
                                    break;
                                }

                                _context14.next = 8;
                                return redisCli.smembersAsync(USER_EXAM_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time));

                            case 8:
                                userQuestionSeq = _context14.sent;
                                _context14.next = 20;
                                break;

                            case 11:
                                if (!(type === "review")) {
                                    _context14.next = 17;
                                    break;
                                }

                                _context14.next = 14;
                                return redisCli.smembersAsync(USER_EXAM_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time));

                            case 14:
                                userQuestionSeq = _context14.sent;
                                _context14.next = 20;
                                break;

                            case 17:
                                _context14.next = 19;
                                return redisCli.lrangeAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), 0, -1);

                            case 19:
                                userQuestionSeq = _context14.sent;

                            case 20:
                                ret_res = {};

                                if (!(userQuestionSeq != 'null' && userQuestionSeq && userQuestionSeq[questionIndex])) {
                                    _context14.next = 34;
                                    break;
                                }

                                _context14.next = 24;
                                return redisCli.hgetallAsync(userQuestionSeq[questionIndex]);

                            case 24:
                                question = _context14.sent;

                                question = question || {};
                                question = JSON.parse(question.json || {});
                                _context14.next = 29;
                                return this.getUserExamQuestionData({
                                    usr_id: usr_id,
                                    exam_id: exam_id,
                                    questionId: question.questionId,
                                    topic_id: topic_id
                                });

                            case 29:
                                q_data = _context14.sent;

                                q_data = q_data || {};
                                ret_res = {
                                    question: (0, _extends3.default)({}, question, q_data),
                                    questionIndex: questionIndex,
                                    totalQuestions: (userQuestionSeq || []).length
                                };
                                _context14.next = 35;
                                break;

                            case 34:
                                ret_res = {
                                    totalQuestions: 0,
                                    questionIndex: questionIndex,
                                    question: {}
                                };

                            case 35:
                                return _context14.abrupt('return', ret_res);

                            case 38:
                                _context14.prev = 38;
                                _context14.t0 = _context14['catch'](0);

                                logger.debug(_context14.t0);
                                throw new Error(_context14.t0);

                            case 42:
                            case 'end':
                                return _context14.stop();
                        }
                    }
                }, _callee14, this, [[0, 38]]);
            }));

            function getUserExamQuestion(_x15) {
                return _ref14.apply(this, arguments);
            }

            return getUserExamQuestion;
        }()
    }, {
        key: 'getQuestionUsingIdFromRedis',
        value: function () {
            var _ref15 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee15(data) {
                var q_id, res;
                return _regenerator2.default.wrap(function _callee15$(_context15) {
                    while (1) {
                        switch (_context15.prev = _context15.next) {
                            case 0:
                                _context15.prev = 0;
                                q_id = data.q_id;

                                if (q_id) {
                                    _context15.next = 4;
                                    break;
                                }

                                return _context15.abrupt('return');

                            case 4:
                                _context15.next = 6;
                                return redisCli.hgetallAsync(q_id);

                            case 6:
                                res = _context15.sent;

                                if (!(res !== 'null' && res)) {
                                    _context15.next = 10;
                                    break;
                                }

                                res.json = JSON.parse(res.json);
                                return _context15.abrupt('return', res);

                            case 10:
                                return _context15.abrupt('return');

                            case 13:
                                _context15.prev = 13;
                                _context15.t0 = _context15['catch'](0);

                                logger.debug(_context15.t0);

                            case 16:
                            case 'end':
                                return _context15.stop();
                        }
                    }
                }, _callee15, this, [[0, 13]]);
            }));

            function getQuestionUsingIdFromRedis(_x16) {
                return _ref15.apply(this, arguments);
            }

            return getQuestionUsingIdFromRedis;
        }()
    }, {
        key: 'startUserPractice',
        value: function () {
            var _ref16 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee16(data) {
                var usr_id, topic_id, no_of_question, seqOrRand, no_of_attempted_questions, totalQuestions, questionSeq, shuffledUserQueSeq, questions, i, question, q_data, j;
                return _regenerator2.default.wrap(function _callee16$(_context16) {
                    while (1) {
                        switch (_context16.prev = _context16.next) {
                            case 0:
                                usr_id = data.usr_id, topic_id = data.topic_id, no_of_question = data.no_of_question, seqOrRand = data.seqOrRand;
                                _context16.prev = 1;

                                seqOrRand = seqOrRand || "sequence";
                                no_of_attempted_questions = 0; //await this.noOfAlreadyAttemptedUserPracticeQuestion(data);

                                _context16.next = 6;
                                return redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);

                            case 6:
                                totalQuestions = _context16.sent;

                                if (no_of_attempted_questions >= totalQuestions.length) {
                                    no_of_attempted_questions--;
                                }
                                _context16.next = 10;
                                return redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), no_of_attempted_questions || 0, -1);

                            case 10:
                                questionSeq = _context16.sent;

                                if (seqOrRand === "random") {
                                    // if we want random question sequence
                                    shuffledUserQueSeq = utility.shuffleArray([].concat(questionSeq || []));
                                } else if (seqOrRand === "sequence") {
                                    // if we want sequential question sequence as it is
                                    shuffledUserQueSeq = questionSeq;
                                }
                                //delete user quesion seq
                                _context16.next = 14;
                                return redisCli.delAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id));

                            case 14:
                                _context16.next = 16;
                                return redisCli.rpushAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), shuffledUserQueSeq || []);

                            case 16:
                                questions = [];
                                i = 0;

                            case 18:
                                if (!(i < (no_of_question || 5))) {
                                    _context16.next = 29;
                                    break;
                                }

                                _context16.next = 21;
                                return this.getQuestionUsingIdFromRedis({
                                    q_id: shuffledUserQueSeq[i]
                                });

                            case 21:
                                question = _context16.sent;
                                _context16.next = 24;
                                return this.getUserPracticeQuestionData({
                                    usr_id: usr_id,
                                    topic_id: topic_id,
                                    questionId: shuffledUserQueSeq[i]
                                });

                            case 24:
                                q_data = _context16.sent;

                                if (question) {
                                    j = question.json || {};

                                    q_data = q_data || {};
                                    questions.push((0, _extends3.default)({}, j, q_data));
                                }

                            case 26:
                                i++;
                                _context16.next = 18;
                                break;

                            case 29:
                                return _context16.abrupt('return', {
                                    attemptedQuestions: no_of_attempted_questions,
                                    questions: questions
                                });

                            case 32:
                                _context16.prev = 32;
                                _context16.t0 = _context16['catch'](1);

                                logger.debug(_context16.t0);
                                throw new Error(_context16.t0);

                            case 36:
                            case 'end':
                                return _context16.stop();
                        }
                    }
                }, _callee16, this, [[1, 32]]);
            }));

            function startUserPractice(_x17) {
                return _ref16.apply(this, arguments);
            }

            return startUserPractice;
        }()
    }, {
        key: 'fetchNextUserPracticeQuestion',
        value: function () {
            var _ref17 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee17(data) {
                var usr_id, topic_id, start, no_of_question, question_type, questionSeq, questions, i, question, q_data, j;
                return _regenerator2.default.wrap(function _callee17$(_context17) {
                    while (1) {
                        switch (_context17.prev = _context17.next) {
                            case 0:
                                usr_id = data.usr_id, topic_id = data.topic_id, start = data.start, no_of_question = data.no_of_question, question_type = data.question_type;
                                _context17.prev = 1;

                                if (!(question_type === "skipped")) {
                                    _context17.next = 8;
                                    break;
                                }

                                _context17.next = 5;
                                return redisCli.smembersAsync(USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id));

                            case 5:
                                questionSeq = _context17.sent;
                                _context17.next = 17;
                                break;

                            case 8:
                                if (!(question_type === "review")) {
                                    _context17.next = 14;
                                    break;
                                }

                                _context17.next = 11;
                                return redisCli.smembersAsync(USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id));

                            case 11:
                                questionSeq = _context17.sent;
                                _context17.next = 17;
                                break;

                            case 14:
                                _context17.next = 16;
                                return redisCli.lrangeAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), start || 0, -1);

                            case 16:
                                questionSeq = _context17.sent;

                            case 17:
                                if (!(questionSeq === 'null' || !questionSeq)) {
                                    _context17.next = 19;
                                    break;
                                }

                                return _context17.abrupt('return', []);

                            case 19:
                                questions = [];
                                i = 0;

                            case 21:
                                if (!(i < (no_of_question || 5))) {
                                    _context17.next = 32;
                                    break;
                                }

                                _context17.next = 24;
                                return this.getQuestionUsingIdFromRedis({
                                    q_id: questionSeq[i]
                                });

                            case 24:
                                question = _context17.sent;
                                _context17.next = 27;
                                return this.getUserPracticeQuestionData({
                                    usr_id: usr_id,
                                    topic_id: topic_id,
                                    questionId: questionSeq[i]
                                });

                            case 27:
                                q_data = _context17.sent;

                                if (question) {
                                    j = question.json || {};

                                    q_data = q_data || {};
                                    questions.push((0, _extends3.default)({}, j, q_data));
                                }

                            case 29:
                                i++;
                                _context17.next = 21;
                                break;

                            case 32:
                                return _context17.abrupt('return', {
                                    questions: questions,
                                    totalQuestions: questions.length
                                });

                            case 35:
                                _context17.prev = 35;
                                _context17.t0 = _context17['catch'](1);

                                logger.debug(_context17.t0);
                                throw new Error(_context17.t0);

                            case 39:
                            case 'end':
                                return _context17.stop();
                        }
                    }
                }, _callee17, this, [[1, 35]]);
            }));

            function fetchNextUserPracticeQuestion(_x18) {
                return _ref17.apply(this, arguments);
            }

            return fetchNextUserPracticeQuestion;
        }()
    }, {
        key: 'noOfAlreadyAttemptedUserPracticeQuestion',
        value: function () {
            var _ref18 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee18(data) {
                var usr_id, topic_id, q_data, arr;
                return _regenerator2.default.wrap(function _callee18$(_context18) {
                    while (1) {
                        switch (_context18.prev = _context18.next) {
                            case 0:
                                _context18.prev = 0;
                                usr_id = data.usr_id, topic_id = data.topic_id;
                                _context18.next = 4;
                                return redisCli.hgetallAsync(USER_PRACTICE_QUESTION_DATA_REDIS_KEY(usr_id, topic_id));

                            case 4:
                                q_data = _context18.sent;

                                if (!(q_data && q_data !== 'null')) {
                                    _context18.next = 8;
                                    break;
                                }

                                arr = Object.keys(q_data || {});
                                return _context18.abrupt('return', arr.length);

                            case 8:
                                return _context18.abrupt('return', 0);

                            case 11:
                                _context18.prev = 11;
                                _context18.t0 = _context18['catch'](0);

                                logger.debug(_context18.t0);
                                throw new Error(_context18.t0);

                            case 15:
                            case 'end':
                                return _context18.stop();
                        }
                    }
                }, _callee18, this, [[0, 11]]);
            }));

            function noOfAlreadyAttemptedUserPracticeQuestion(_x19) {
                return _ref18.apply(this, arguments);
            }

            return noOfAlreadyAttemptedUserPracticeQuestion;
        }()
    }, {
        key: 'getUserPracticeQuestionData',
        value: function () {
            var _ref19 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee19(data) {
                var usr_id, questionId, topic_id, q_data;
                return _regenerator2.default.wrap(function _callee19$(_context19) {
                    while (1) {
                        switch (_context19.prev = _context19.next) {
                            case 0:
                                _context19.prev = 0;
                                usr_id = data.usr_id, questionId = data.questionId, topic_id = data.topic_id;
                                _context19.next = 4;
                                return redisCli.hmgetAsync(USER_PRACTICE_QUESTION_DATA_REDIS_KEY(usr_id, topic_id), questionId);

                            case 4:
                                q_data = _context19.sent;

                                q_data = q_data[0];
                                if (!q_data || q_data === 'null') {
                                    q_data = undefined;
                                } else {
                                    q_data = JSON.parse(q_data);
                                }
                                return _context19.abrupt('return', q_data);

                            case 10:
                                _context19.prev = 10;
                                _context19.t0 = _context19['catch'](0);

                                logger.debug(_context19.t0);
                                throw new Error(_context19.t0);

                            case 14:
                            case 'end':
                                return _context19.stop();
                        }
                    }
                }, _callee19, this, [[0, 10]]);
            }));

            function getUserPracticeQuestionData(_x20) {
                return _ref19.apply(this, arguments);
            }

            return getUserPracticeQuestionData;
        }()
    }, {
        key: 'getPracticeNodeQuestions',
        value: function () {
            var _ref20 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee20(data) {
                var node, topic_id, nodeContent, questionIDs, questions, i, ids, j, id, que;
                return _regenerator2.default.wrap(function _callee20$(_context20) {
                    while (1) {
                        switch (_context20.prev = _context20.next) {
                            case 0:
                                _context20.prev = 0;
                                node = data.node, topic_id = data.topic_id;
                                _context20.next = 4;
                                return redisCli.hgetAsync(PRACTICE_TOPIC_QUESTIONS_REDIS_KEY(topic_id), '#' + node);

                            case 4:
                                nodeContent = _context20.sent;

                                if (!(nodeContent === 'null')) {
                                    _context20.next = 7;
                                    break;
                                }

                                return _context20.abrupt('return', []);

                            case 7:
                                nodeContent = nodeContent.split(","); // [Q1,Q2,Q3,G1]
                                _context20.next = 10;
                                return redisCli.hmgetAsync(PRACTICE_TOPIC_QUESTIONS_REDIS_KEY(topic_id), nodeContent);

                            case 10:
                                questionIDs = _context20.sent;
                                // ['id','id','id','id,id,id']
                                questions = [];
                                i = 0;

                            case 13:
                                if (!(i < questionIDs.length)) {
                                    _context20.next = 40;
                                    break;
                                }

                                ids = questionIDs[i].split(",");

                                if (!(ids && ids.length > 1)) {
                                    _context20.next = 30;
                                    break;
                                }

                                j = 0;

                            case 17:
                                if (!(j < ids.length)) {
                                    _context20.next = 28;
                                    break;
                                }

                                id = ids[j];

                                if (!(id === 'null')) {
                                    _context20.next = 21;
                                    break;
                                }

                                return _context20.abrupt('continue', 25);

                            case 21:
                                _context20.next = 23;
                                return this.getQuestionUsingIdFromRedis({
                                    q_id: id
                                });

                            case 23:
                                que = _context20.sent;

                                if (que != 'null' && que.json) {
                                    que.json.group = true;
                                    que.json.groupId = nodeContent[i];
                                    questions.push(que.json);
                                }

                            case 25:
                                j++;
                                _context20.next = 17;
                                break;

                            case 28:
                                _context20.next = 37;
                                break;

                            case 30:
                                id = ids[0];

                                if (!(id === 'null')) {
                                    _context20.next = 33;
                                    break;
                                }

                                return _context20.abrupt('continue', 37);

                            case 33:
                                _context20.next = 35;
                                return this.getQuestionUsingIdFromRedis({
                                    q_id: id
                                });

                            case 35:
                                que = _context20.sent;

                                if (que && que != 'null') {
                                    questions.push(que.json);
                                }

                            case 37:
                                i++;
                                _context20.next = 13;
                                break;

                            case 40:
                                return _context20.abrupt('return', {
                                    questions: questions
                                });

                            case 43:
                                _context20.prev = 43;
                                _context20.t0 = _context20['catch'](0);

                                logger.debug(_context20.t0);

                            case 46:
                            case 'end':
                                return _context20.stop();
                        }
                    }
                }, _callee20, this, [[0, 43]]);
            }));

            function getPracticeNodeQuestions(_x21) {
                return _ref20.apply(this, arguments);
            }

            return getPracticeNodeQuestions;
        }()
    }, {
        key: 'saveUserPracticeQuestionData',
        value: function () {
            var _ref21 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee21(data) {
                var usr_id, topic_id, questionId, questionData, jsonQData, isReview, isSkipped;
                return _regenerator2.default.wrap(function _callee21$(_context21) {
                    while (1) {
                        switch (_context21.prev = _context21.next) {
                            case 0:
                                _context21.prev = 0;
                                usr_id = data.usr_id, topic_id = data.topic_id, questionId = data.questionId, questionData = data.questionData;

                                if (questionId) {
                                    _context21.next = 4;
                                    break;
                                }

                                return _context21.abrupt('return');

                            case 4:
                                jsonQData = JSON.parse(questionData || {});
                                isReview = jsonQData["markForReview"];
                                isSkipped = jsonQData["isSkipped"];

                                if (!isSkipped) {
                                    _context21.next = 12;
                                    break;
                                }

                                _context21.next = 10;
                                return redisCli.saddAsync(USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);

                            case 10:
                                _context21.next = 14;
                                break;

                            case 12:
                                _context21.next = 14;
                                return redisCli.sremAsync(USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);

                            case 14:
                                if (!isReview) {
                                    _context21.next = 19;
                                    break;
                                }

                                _context21.next = 17;
                                return redisCli.saddAsync(USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);

                            case 17:
                                _context21.next = 21;
                                break;

                            case 19:
                                _context21.next = 21;
                                return redisCli.sremAsync(USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);

                            case 21:
                                _context21.next = 23;
                                return redisCli.hmsetAsync(USER_PRACTICE_QUESTION_DATA_REDIS_KEY(usr_id, topic_id), questionId, questionData);

                            case 23:
                                return _context21.abrupt('return', {
                                    questionId: questionId,
                                    usr_id: usr_id
                                });

                            case 26:
                                _context21.prev = 26;
                                _context21.t0 = _context21['catch'](0);

                                logger.debug(_context21.t0);
                                throw new Error(_context21.t0);

                            case 30:
                            case 'end':
                                return _context21.stop();
                        }
                    }
                }, _callee21, this, [[0, 26]]);
            }));

            function saveUserPracticeQuestionData(_x22) {
                return _ref21.apply(this, arguments);
            }

            return saveUserPracticeQuestionData;
        }()
    }]);
    return PracticeQuestion;
}();

function dbCollection(db) {
    return db.collection('practiceQuestions');
}

module.exports = new PracticeQuestion();