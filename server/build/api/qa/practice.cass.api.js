'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:qa:practice:cass");
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var uuid = require('uuid');
var logger = require(configPath.lib.log_to_file);
var oracledb = require(configPath.dbconn.oracledb);
var moment = require("moment");

var prtc_stats_table = 'ew1.vt_usr_prtc_stat';
var oracle_prtc_stats_table = 'vt_usr_prtc_stat';

var ew_usr_tests_dtl_table = 'ew1.ew_usr_tests_dtl';
var oracle_usr_tests_dtl_table = 'ew_usr_tests_dtl';

var ew_usr_tests_m_table = "ew1.ew_usr_tests_m";
var oracle_ew_usr_tests_m_table = "ew_usr_tests_m";
var ew_usr_tests_review_table = "ew1.ew_usr_tests_review";

var ew_avg_perf_stats_table = "ew1.ew_avg_perf_stats";
var ew_usr_perf_stats_table = "ew1.ew_usr_perf_stats";

var ew_usr_perf_month_stats_table = "ew1.ew_usr_perf_month_stats";
var ew_usr_perf_month_avg_stats_table = "ew1.ew_usr_perf_month_avg_stats";

var usr_q_attempt_a_table = "ew1.usr_q_attempt_a";

function Practice() {}

Practice.prototype.saveQuestionStats = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var defer, usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm, que_ans, que_ans_tm, no_of_clks, revu, lrn, skip, wrg_rsn, que_diff, insertStats, insertStats_params, oracle_insertStats_params, oracle_insertStats_params_str, oracle_insertStats, conn, res;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id || "";
                        que_id = data.que_id || "";
                        tpc_id = data.tpc_id || "";
                        mdl_id = data.mdl_id || "";
                        crs_id = data.crs_id || "";
                        que_strt_tm = data.que_strt_tm;
                        que_end_tm = data.que_end_tm;
                        que_ans = data.que_ans || {};
                        que_ans_tm = data.que_ans_tm || {};
                        no_of_clks = data.no_of_clks || 0;
                        revu = data.revu || false;
                        lrn = data.lrn || false;
                        skip = data.skip || false;
                        wrg_rsn = data.wrg_rsn || ["Not Selected"];
                        que_diff = data.que_diff || "Not Selected";

                        //insert query for cassandra table

                        insertStats = "insert into " + prtc_stats_table + " (usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm,que_ans,que_ans_tm,no_of_clks,revu,lrn,skip,wrg_rsn,que_diff) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        insertStats_params = [usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm, que_ans, que_ans_tm, no_of_clks, revu, lrn, skip, wrg_rsn, que_diff];

                        //insert into oracle stats table also

                        oracle_insertStats_params = [usr_id, que_id, tpc_id, mdl_id, crs_id, "to_date('" + moment(que_strt_tm).format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')", "to_date('" + moment(que_end_tm).format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')", no_of_clks, revu, lrn, skip, que_diff];

                        oracle_insertStats_params = oracle_insertStats_params.map(function (v, i) {
                            if (("" + v).indexOf("to_date") === 0) {
                                return v;
                            } else {
                                return "'" + v + "'";
                            }
                        });
                        oracle_insertStats_params_str = '(' + oracle_insertStats_params.join(",") + ')';
                        oracle_insertStats = "insert into " + oracle_prtc_stats_table + " (usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm,no_of_clks,revu,lrn,skip,que_diff) values " + oracle_insertStats_params_str;
                        _context.prev = 22;

                        logger.debug(oracle_insertStats);
                        _context.next = 26;
                        return oracledb.getConnection();

                    case 26:
                        conn = _context.sent;
                        _context.next = 29;
                        return conn.execute(oracle_insertStats, [], { autoCommit: true });

                    case 29:
                        res = _context.sent;
                        _context.next = 32;
                        return oracledb.releaseConnection(conn);

                    case 32:
                        logger.debug("insert into...oracle_prtc_stats_table....", res);
                        _context.next = 38;
                        break;

                    case 35:
                        _context.prev = 35;
                        _context.t0 = _context['catch'](22);

                        logger.debug(_context.t0);

                    case 38:

                        ///////////////////////////////

                        cassconn.execute(insertStats, insertStats_params, { prepare: true }, function (err, res, r) {
                            // debug(err,res,r);
                            if (err) {
                                defer.reject(err);
                                return;
                            }
                            defer.resolve([]);
                        });
                        return _context.abrupt('return', defer.promise);

                    case 40:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[22, 35]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

Practice.prototype.saveTestQuestionStats = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var tst_id, usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm, que_ans, oracle_insertStats_params, oracle_insertStats_params_str, oracle_insertStats, conn, res, insertStats, insertStats_params, saveRes;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        tst_id = data.tst_id || uuid.v4();
                        usr_id = data.usr_id || "";
                        que_id = data.que_id || "";
                        tpc_id = data.tpc_id || "";
                        mdl_id = data.mdl_id || "";
                        crs_id = data.crs_id || "";
                        que_strt_tm = data.que_strt_tm;
                        que_end_tm = data.que_end_tm;
                        que_ans = data.que_ans || {};

                        //insert into oracle stats table also

                        oracle_insertStats_params = [tst_id, usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm];

                        oracle_insertStats_params = oracle_insertStats_params.map(function (v, i) {
                            return "'" + v + "'";
                        });
                        oracle_insertStats_params_str = '(' + oracle_insertStats_params.join(",") + ')';
                        oracle_insertStats = "insert into " + oracle_usr_tests_dtl_table + " (tst_id,usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm) values " + oracle_insertStats_params_str;
                        _context2.prev = 14;

                        logger.debug(oracle_insertStats);
                        _context2.next = 18;
                        return oracledb.getConnection();

                    case 18:
                        conn = _context2.sent;
                        _context2.next = 21;
                        return conn.execute(oracle_insertStats, [], { autoCommit: true });

                    case 21:
                        res = _context2.sent;
                        _context2.next = 24;
                        return oracledb.releaseConnection(conn);

                    case 24:
                        logger.debug("insert into...oracle_usr_tests_dtl_table....", res);
                        _context2.next = 30;
                        break;

                    case 27:
                        _context2.prev = 27;
                        _context2.t0 = _context2['catch'](14);

                        logger.debug(_context2.t0);

                    case 30:

                        ///////////////////////////////

                        insertStats = "insert into " + ew_usr_tests_dtl_table + " (tst_id,usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm,que_ans) values (?,?,?,?,?,?,?,?,?)";
                        insertStats_params = [tst_id, usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm, que_ans];
                        _context2.next = 34;
                        return this.saveTestToMasterTable(data);

                    case 34:
                        saveRes = _context2.sent;
                        _context2.next = 37;
                        return cassExecute(insertStats, insertStats_params, { prepare: true });

                    case 37:
                        res = _context2.sent;
                        return _context2.abrupt('return', data);

                    case 41:
                        _context2.prev = 41;
                        _context2.t1 = _context2['catch'](0);

                        debug(_context2.t1);
                        throw _context2.t1;

                    case 45:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 41], [14, 27]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

Practice.prototype.saveTestToMasterTable = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var tst_id, usr_id, tst_strt_tm, tst_end_tm, insertStats, insertStats_params, res, oracle_insertStats_params, oracle_insertStats_params_str, oracle_insertStats, conn;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        tst_id = data.tst_id || uuid.v4();
                        usr_id = data.usr_id || "";
                        tst_strt_tm = data.tst_strt_tm;
                        tst_end_tm = data.tst_end_tm;
                        insertStats = "insert into " + ew_usr_tests_m_table + " (tst_id,usr_id,tst_strt_tm,tst_end_tm) values (?,?,?,?)";
                        insertStats_params = [tst_id, usr_id, tst_strt_tm, tst_end_tm];
                        _context3.next = 9;
                        return cassExecute(insertStats, insertStats_params, { prepare: true });

                    case 9:
                        res = _context3.sent;
                        _context3.prev = 10;
                        oracle_insertStats_params = [tst_id, usr_id, tst_strt_tm, tst_end_tm];

                        oracle_insertStats_params = oracle_insertStats_params.map(function (v, i) {
                            return "'" + v + "'";
                        });
                        oracle_insertStats_params_str = '(' + oracle_insertStats_params.join(",") + ')';
                        oracle_insertStats = "insert into " + oracle_ew_usr_tests_m_table + " (tst_id,usr_id,tst_strt_tm,tst_end_tm) values " + oracle_insertStats_params_str;

                        logger.debug(oracle_insertStats);
                        _context3.next = 18;
                        return oracledb.getConnection();

                    case 18:
                        conn = _context3.sent;
                        _context3.next = 21;
                        return conn.execute(oracle_insertStats, [], { autoCommit: true });

                    case 21:
                        res = _context3.sent;
                        _context3.next = 24;
                        return oracledb.releaseConnection(conn);

                    case 24:
                        logger.debug("insert into...oracle_ew_usr_tests_m_table....", res);
                        _context3.next = 30;
                        break;

                    case 27:
                        _context3.prev = 27;
                        _context3.t0 = _context3['catch'](10);

                        logger.debug(_context3.t0);

                    case 30:
                        return _context3.abrupt('return', data);

                    case 33:
                        _context3.prev = 33;
                        _context3.t1 = _context3['catch'](0);

                        debug(_context3.t1);
                        throw _context3.t1;

                    case 37:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 33], [10, 27]]);
    }));

    return function (_x3) {
        return _ref3.apply(this, arguments);
    };
}();

Practice.prototype.saveTestReviews = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(dataArr) {
        var queries, i, data, tst_id, usr_id, que_id, tpc_id, mdl_id, crs_id, wrg_rsn, usr_cmnt, qry, prm, res;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        queries = [];

                        for (i = 0; i < dataArr.length; i++) {
                            data = dataArr[i];
                            tst_id = data.tst_id || uuid.v4();
                            usr_id = data.usr_id || "";
                            que_id = data.que_id || "";
                            tpc_id = data.tpc_id || "";
                            mdl_id = data.mdl_id || "";
                            crs_id = data.crs_id || "";
                            wrg_rsn = data.wrg_rsn || ["Not Selected"];
                            usr_cmnt = data.usr_cmnt || ["Not Selected"];
                            qry = "insert into " + ew_usr_tests_review_table + " (tst_id,usr_id,que_id,tpc_id,mdl_id,crs_id,wrg_rsn,usr_cmnt) values (?,?,?,?,?,?,?,?)";
                            prm = [tst_id, usr_id, que_id, tpc_id, mdl_id, crs_id, wrg_rsn, usr_cmnt];


                            queries.push({
                                query: qry,
                                params: prm
                            });
                        }
                        _context4.next = 5;
                        return cassBatch(queries, { prepare: true });

                    case 5:
                        res = _context4.sent;
                        return _context4.abrupt('return', data);

                    case 9:
                        _context4.prev = 9;
                        _context4.t0 = _context4['catch'](0);

                        debug(_context4.t0);
                        throw _context4.t0;

                    case 13:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 9]]);
    }));

    return function (_x4) {
        return _ref4.apply(this, arguments);
    };
}();

Practice.prototype.getAllAvgQuestionPerfStats = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
        var query, rows;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _context5.prev = 0;
                        query = "select * from " + ew_avg_perf_stats_table;
                        _context5.next = 4;
                        return cassExecute(query);

                    case 4:
                        rows = _context5.sent;
                        return _context5.abrupt('return', rows);

                    case 8:
                        _context5.prev = 8;
                        _context5.t0 = _context5['catch'](0);

                        logger.debug(_context5.t0);
                        throw new Error(_context5.t0);

                    case 12:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this, [[0, 8]]);
    }));

    return function (_x5) {
        return _ref5.apply(this, arguments);
    };
}();

Practice.prototype.getAllUsersQuestionPerfStats = function () {
    var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
        var query, rows;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        _context6.prev = 0;
                        query = "select * from " + ew_usr_perf_stats_table;
                        _context6.next = 4;
                        return cassExecute(query);

                    case 4:
                        rows = _context6.sent;
                        return _context6.abrupt('return', rows);

                    case 8:
                        _context6.prev = 8;
                        _context6.t0 = _context6['catch'](0);

                        logger.debug(_context6.t0);
                        throw new Error(_context6.t0);

                    case 12:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, this, [[0, 8]]);
    }));

    return function (_x6) {
        return _ref6.apply(this, arguments);
    };
}();

Practice.prototype.getAllMonthAvgQuestionPerfStats = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
        var query, rows;
        return _regenerator2.default.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        _context7.prev = 0;
                        query = "select * from " + ew_usr_perf_month_avg_stats_table;
                        _context7.next = 4;
                        return cassExecute(query);

                    case 4:
                        rows = _context7.sent;
                        return _context7.abrupt('return', rows);

                    case 8:
                        _context7.prev = 8;
                        _context7.t0 = _context7['catch'](0);

                        logger.debug(_context7.t0);
                        throw new Error(_context7.t0);

                    case 12:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, this, [[0, 8]]);
    }));

    return function (_x7) {
        return _ref7.apply(this, arguments);
    };
}();

Practice.prototype.getAllUsersMonthQuestionPerfStats = function () {
    var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
        var query, rows;
        return _regenerator2.default.wrap(function _callee8$(_context8) {
            while (1) {
                switch (_context8.prev = _context8.next) {
                    case 0:
                        _context8.prev = 0;
                        query = "select * from " + ew_usr_perf_month_stats_table;
                        _context8.next = 4;
                        return cassExecute(query);

                    case 4:
                        rows = _context8.sent;
                        return _context8.abrupt('return', rows);

                    case 8:
                        _context8.prev = 8;
                        _context8.t0 = _context8['catch'](0);

                        logger.debug(_context8.t0);
                        throw new Error(_context8.t0);

                    case 12:
                    case 'end':
                        return _context8.stop();
                }
            }
        }, _callee8, this, [[0, 8]]);
    }));

    return function (_x8) {
        return _ref8.apply(this, arguments);
    };
}();

Practice.prototype.getUserMonthQuestionAttemptStats = function () {
    var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(data) {
        var usr_id, month, year, no_of_months, MONTHS, NO_OF_MONTHS, today, desireMonth, desireMonthStr, cass_month_arr, i, _month, monthStr, month_id_str, query, params, rows, res;

        return _regenerator2.default.wrap(function _callee9$(_context9) {
            while (1) {
                switch (_context9.prev = _context9.next) {
                    case 0:
                        _context9.prev = 0;
                        usr_id = data.usr_id, month = data.month, year = data.year, no_of_months = data.no_of_months;
                        MONTHS = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
                        NO_OF_MONTHS = no_of_months || 3;
                        today = new Date();

                        if (!year) {
                            year = today.getFullYear();
                        }

                        if (!month || month > 12) {
                            month = today.getMonth();
                        } else {
                            // Javascript month start from 0
                            month = month - 1;
                        }

                        desireMonth = new Date(year, month);
                        desireMonthStr = desireMonth.getFullYear() + ("00" + (desireMonth.getMonth() + 1)).slice(-2);
                        cass_month_arr = ["'" + desireMonthStr + "'"];


                        for (i = 0; i < NO_OF_MONTHS - 1; i++) {
                            _month = new Date(desireMonth);

                            _month.setMonth(_month.getMonth() - (i + 1));
                            monthStr = _month.getFullYear() + ("00" + (_month.getMonth() + 1)).slice(-2);

                            cass_month_arr.push("'" + monthStr + "'");
                        }

                        month_id_str = "(" + cass_month_arr.join(",") + ")";
                        query = "select * from " + usr_q_attempt_a_table + " where usr_id=? and month_id in " + month_id_str;
                        params = [usr_id];
                        _context9.next = 16;
                        return cassExecute(query, params);

                    case 16:
                        rows = _context9.sent;
                        res = {
                            "usr_id": usr_id,
                            "months": [],
                            "no_q_attempt": [],
                            "no_q_correct_1": [],
                            "no_q_correct_2": [],
                            "no_q_correct_3": [],
                            "no_q_learned": []
                        };


                        if (rows.length > 0) {
                            rows.forEach(function (v, i) {
                                res["months"].push(MONTHS[parseInt(v.month_id.slice(-2) || "1") - 1]);
                                res["no_q_attempt"].push(parseInt(v.no_q_attempt || 0));
                                res["no_q_correct_1"].push(parseInt(v.no_q_correct_1 || 0));
                                res["no_q_correct_2"].push(parseInt(v.no_q_correct_2 || 0));
                                res["no_q_correct_3"].push(parseInt(v.no_q_correct_3 || 0));
                                res["no_q_learned"].push(parseInt(v.no_q_learned || 0));
                            });
                        }

                        return _context9.abrupt('return', res);

                    case 22:
                        _context9.prev = 22;
                        _context9.t0 = _context9['catch'](0);

                        logger.debug(_context9.t0);
                        throw new Error(_context9.t0);

                    case 26:
                    case 'end':
                        return _context9.stop();
                }
            }
        }, _callee9, this, [[0, 22]]);
    }));

    return function (_x9) {
        return _ref9.apply(this, arguments);
    };
}();

module.exports = new Practice();