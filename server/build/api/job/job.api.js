'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var config = require('../../config.js');
var Q = require('q');
var uuid = require('uuid');
var shortid = require('shortid');
var debug = require('debug')('app:api:job:job');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var ew_apply_job_tbl = "ew1.ew_apply_job";

function Job() {}

Job.prototype.applyForJob = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var job_pk, usr_nm, usr_email, usr_ph, usr_sal, usr_edu, usr_skill, job_id, usr_about_exp, create_dt, qry, params;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        job_pk = Uuid.random();
                        usr_nm = data.usr_nm || "";
                        usr_email = data.usr_email || "";
                        usr_ph = data.usr_ph || "";
                        usr_sal = data.usr_sal || "";
                        usr_edu = data.usr_edu || "";
                        usr_skill = data.usr_skill || "";
                        job_id = data.job_id || "";
                        usr_about_exp = data.usr_about_exp || "";
                        create_dt = new Date();
                        qry = "insert into " + ew_apply_job_tbl + " (job_pk,job_id,usr_nm,usr_email,usr_ph,usr_sal,usr_edu,usr_skill,usr_about_exp,create_dt) values (?,?,?,?,?,?,?,?,?,?)";
                        params = [job_pk, job_id, usr_nm, usr_email, usr_ph, usr_sal, usr_edu, usr_skill, usr_about_exp, create_dt];
                        _context.next = 15;
                        return cassExecute(qry, params);

                    case 15:
                        return _context.abrupt('return', data);

                    case 18:
                        _context.prev = 18;
                        _context.t0 = _context['catch'](0);

                        debug(_context.t0);
                        throw _context.t0;

                    case 22:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 18]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

module.exports = new Job();