'use strict';

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);

var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:usercourses:usercourses");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;

var courseAPI = require(configPath.api.admin.course);
var batchCourseAPI = require(configPath.api.batchCourse.batchCourse);
var bundleAPI = require(configPath.api.bundle.bundle);
var ew_crs_access_tbl = "ew1.ew_crs_access";

function UserCourses() {}
//Deprecated
UserCourses.prototype.getPurchasedCourses = function (data) {
	return new getConnection().then(dbCollection).then(getCourses).then(getCoursesDetails).catch(globalFunctions.err);

	function getCourses(col) {
		return col.findOne({ userId: data.userId }, { _id: 0 });
	}

	function getCoursesDetails() {
		var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

		var courses = obj ? obj.courses : [];
		var courseIds = courses.map(function (v, i) {
			return v.courseId;
		});
		return courseAPI.getCoursesById({ ids: courseIds });
	}
};
//Deprecated
UserCourses.prototype.updatePurchasedCourses = function (data) {
	return new getConnection().then(dbCollection).then(updateCourses).catch(globalFunctions.err);

	function updateCourses(col) {
		return col.update({ userId: data.userId }, { $addToSet: { "courses": { $each: data.courses } } }, { upsert: true });
	}
};

//Deprecated
UserCourses.prototype.addPurchasedCourse = function (data) {
	return new getConnection().then(dbCollection).then(updateCourses).catch(globalFunctions.err);

	function updateCourses(col) {
		var obj = {
			courseId: data.course,
			bat_id: data.bat_id,
			purchaseDate: new Date()
		};
		return col.update({ userId: data.userId }, { $addToSet: { "courses": obj } }, { upsert: true });
	}
};

UserCourses.prototype.savePurchasedCourse = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
		var defer, usr_id, training_id, comments, src_usr, now, now_1_yr, bundles, queries, allCoursesRes, i, b, j, crs, str, params, queryOptions;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						defer = Q.defer();
						usr_id = data.usr_id;
						training_id = data.training_id;
						comments = data.comments || "";
						src_usr = data.src_usr || "";
						now = new Date();
						now_1_yr = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
						_context.prev = 7;
						_context.next = 10;
						return batchCourseAPI.getBundleCourseMappingInBatchCourse({ crs_id: training_id });

					case 10:
						bundles = _context.sent;
						queries = [];
						allCoursesRes = [];

						if (!(!bundles || bundles.length <= 0)) {
							_context.next = 15;
							break;
						}

						return _context.abrupt('return', []);

					case 15:
						i = 0;

					case 16:
						if (!(i < bundles.length)) {
							_context.next = 24;
							break;
						}

						b = bundles[i];

						if (!(!b || !b.includedCourses || b && b.includedCourses.length <= 0)) {
							_context.next = 20;
							break;
						}

						return _context.abrupt('continue', 21);

					case 20:
						//iterate through all course in this bundle
						for (j = 0; j < b.includedCourses.length; j++) {
							crs = b.includedCourses[j];

							allCoursesRes.push(crs);
							str = "insert into " + ew_crs_access_tbl + " (usr_id,training_id,bndl_id,crs_id,privilege,start_dt,end_dt,comments,src_usr) values (?,?,?,?,?,?,?,?,?)";
							params = [usr_id, training_id, b.bndl_id, crs, "all", now, now_1_yr, comments, src_usr];

							queries.push({ query: str, params: params });
						}

					case 21:
						i++;
						_context.next = 16;
						break;

					case 24:
						if (queries && queries.length > 0) {
							//execte all query at once in batch
							queryOptions = { prepare: true, consistency: cassandra.types.consistencies.quorum };

							cassconn.batch(queries, queryOptions, function (err, res) {
								if (err) {
									defer.resolve(err);
								}
								defer.resolve(allCoursesRes);
							});
						}
						_context.next = 31;
						break;

					case 27:
						_context.prev = 27;
						_context.t0 = _context['catch'](7);

						console.log(_context.t0);
						defer.reject(_context.t0);

					case 31:
						return _context.abrupt('return', defer.promise);

					case 32:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[7, 27]]);
	}));

	return function (_x2) {
		return _ref.apply(this, arguments);
	};
}();

UserCourses.prototype.saveCoursesBasedOnCourseId = function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
		var defer, usr_id, crs_id, comments, src_usr, queries, j, crs, str, params;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						defer = Q.defer();
						usr_id = data.usr_id;
						crs_id = [].concat(data.crs_id);
						comments = data.comments;
						src_usr = data.src_usr;
						_context2.prev = 5;
						queries = [];

						for (j = 0; j < crs_id.length; j++) {
							crs = crs_id[j];
							str = "insert into " + ew_crs_access_tbl + " (usr_id,crs_id,privilege,comments,src_usr) values (?,?,?,?,?)";
							params = [usr_id, crs, "all", comments, src_usr];

							queries.push({ query: str, params: params });
						}

						_context2.next = 10;
						return cassBatch(queries);

					case 10:
						return _context2.abrupt('return', {
							status: true,
							message: "Saved successfully.",
							data: data
						});

					case 13:
						_context2.prev = 13;
						_context2.t0 = _context2['catch'](5);

						console.log(_context2.t0);
						defer.reject(_context2.t0);

					case 17:
						return _context2.abrupt('return', defer.promise);

					case 18:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[5, 13]]);
	}));

	return function (_x3) {
		return _ref2.apply(this, arguments);
	};
}();

UserCourses.prototype.saveCoursesBasedOnBundleId = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
		var defer, usr_id, bundles, comments, src_usr, queries, i, bndl, courses, j, crs, str, params;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						defer = Q.defer();
						usr_id = data.usr_id;
						bundles = [].concat(data.bundles);
						comments = data.comments;
						src_usr = data.src_usr;
						_context3.prev = 5;
						queries = [];
						i = 0;

					case 8:
						if (!(i < bundles.length)) {
							_context3.next = 17;
							break;
						}

						bndl = bundles[i];
						_context3.next = 12;
						return bundleAPI.getCoursesInBundle({
							bndl_id: bndl
						});

					case 12:
						courses = _context3.sent;


						for (j = 0; j < courses.length; j++) {
							crs = courses[j];
							str = "insert into " + ew_crs_access_tbl + " (usr_id,crs_id,privilege,comments,src_usr,bndl_id) values (?,?,?,?,?,?)";
							params = [usr_id, crs, "all", comments, src_usr, bndl.bndl_id];

							queries.push({ query: str, params: params });
						}

					case 14:
						i++;
						_context3.next = 8;
						break;

					case 17:
						_context3.next = 19;
						return cassBatch(queries);

					case 19:
						return _context3.abrupt('return', {
							status: true,
							message: "Saved successfully.",
							data: data
						});

					case 22:
						_context3.prev = 22;
						_context3.t0 = _context3['catch'](5);

						console.log(_context3.t0);
						defer.reject(_context3.t0);

					case 26:
						return _context3.abrupt('return', defer.promise);

					case 27:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[5, 22]]);
	}));

	return function (_x4) {
		return _ref3.apply(this, arguments);
	};
}();

UserCourses.prototype.saveCoursesBasedOnTrainingId = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
		var defer, usr_id, trainings, comments, src_usr, queries, i, training;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						defer = Q.defer();
						usr_id = data.usr_id;
						trainings = [].concat(data.trainings);
						comments = data.comments;
						src_usr = data.src_usr;
						_context4.prev = 5;
						queries = [];
						i = 0;

					case 8:
						if (!(i < trainings.length)) {
							_context4.next = 15;
							break;
						}

						training = trainings[i];
						_context4.next = 12;
						return this.savePurchasedCourse((0, _extends3.default)({
							training_id: training
						}, data));

					case 12:
						i++;
						_context4.next = 8;
						break;

					case 15:
						return _context4.abrupt('return', {
							status: true,
							message: "Saved successfully.",
							data: data
						});

					case 18:
						_context4.prev = 18;
						_context4.t0 = _context4['catch'](5);

						console.log(_context4.t0);
						defer.reject(_context4.t0);

					case 22:
						return _context4.abrupt('return', defer.promise);

					case 23:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[5, 18]]);
	}));

	return function (_x5) {
		return _ref4.apply(this, arguments);
	};
}();

UserCourses.prototype.getPurchasedCourseOfUser = function (data) {
	var defer = Q.defer();

	var usr_id = data.usr_id;
	try {
		var qry = "select * from " + ew_crs_access_tbl + " where usr_id = ?";
		var params = [usr_id];
		cassconn.execute(qry, params, function (err, res) {
			if (err) {
				defer.reject(err);
				return;
			}
			var jsonRows = JSON.parse(JSON.stringify(res.rows || []));
			defer.resolve(jsonRows);
		});
	} catch (e) {
		console.log(e);
		defer.reject(e);
	}
	return defer.promise;
};

UserCourses.prototype.getPurchasedCourseDetails = function () {
	var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
		var usr_id, courses, coursesId, fullCourseDetails;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.prev = 0;
						usr_id = data.usr_id;
						_context5.next = 4;
						return this.getPurchasedCourseOfUser(data);

					case 4:
						courses = _context5.sent;
						coursesId = courses.map(function (v, i) {
							return v.crs_id;
						});
						_context5.next = 8;
						return courseAPI.getCoursesById({ ids: coursesId });

					case 8:
						fullCourseDetails = _context5.sent;
						return _context5.abrupt('return', fullCourseDetails);

					case 12:
						_context5.prev = 12;
						_context5.t0 = _context5['catch'](0);
						throw _context5.t0;

					case 15:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, this, [[0, 12]]);
	}));

	return function (_x6) {
		return _ref5.apply(this, arguments);
	};
}();

function dbCollection(db) {
	return db.collection('usercourses');
}

module.exports = new UserCourses();