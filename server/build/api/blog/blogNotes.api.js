'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videonotes');
var authorizeitemsAPI = require(configPath.api.authorizeitems);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var ew_vdo_note_mdls_tbl = "ew1.ew_vdo_note_mdls";
var ew_vdo_notes_tbl = "ew1.ew_vdo_notes";
var ew_vdo_notes_usr_tbl = "ew1.ew_vdo_notes_usr";

function BlogNoteApi() {}

BlogNoteApi.prototype.insertBlogNote = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var _this = this;

        var defer, usr_id, crs_id, crs_nm, mdl_id, mdl_nm, vdo_id, vdo_note_id, vdo_note, vdo_tm, crt_dt, qry, params;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        crs_id = data.crs_id;
                        crs_nm = data.crs_nm;
                        mdl_id = data.mdl_id;
                        mdl_nm = data.mdl_nm;
                        vdo_id = data.vdo_id;
                        vdo_note_id = Uuid.random();
                        vdo_note = data.vdo_note;
                        vdo_tm = data.vdo_tm;
                        crt_dt = new Date();
                        qry = "insert into " + ew_vdo_notes_tbl + " (usr_id,crs_id,mdl_id,vdo_id,vdo_note_id,vdo_note,vdo_tm,crt_dt) values (?,?,?,?,?,?,?,?)";
                        params = [usr_id, crs_id, mdl_id, vdo_id, vdo_note_id, vdo_note, vdo_tm, crt_dt];


                        cassconn.execute(qry, params, function () {
                            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(err, res) {
                                return _regenerator2.default.wrap(function _callee$(_context) {
                                    while (1) {
                                        switch (_context.prev = _context.next) {
                                            case 0:
                                                if (!err) {
                                                    _context.next = 3;
                                                    break;
                                                }

                                                defer.reject(err);
                                                return _context.abrupt('return');

                                            case 3:
                                                data.vdo_note_id = vdo_note_id;
                                                data.crt_dt = crt_dt;
                                                defer.resolve(data);

                                            case 6:
                                            case 'end':
                                                return _context.stop();
                                        }
                                    }
                                }, _callee, _this);
                            }));

                            return function (_x2, _x3) {
                                return _ref2.apply(this, arguments);
                            };
                        }());
                        return _context2.abrupt('return', defer.promise);

                    case 15:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

BlogNoteApi.prototype.updateBlogNote = function (data) {
    var defer = Q.defer();

    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var vdo_note_id = data.vdo_note_id;
    var vdo_note = data.vdo_note;

    var qry = "update " + ew_vdo_notes_tbl + " set vdo_note=? where usr_id=? and mdl_id=? and vdo_note_id =?";
    var params = [vdo_note, usr_id, mdl_id, vdo_note_id];
    cassconn.execute(qry, params, function (err, res) {
        if (err) {
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    });
    return defer.promise;
};

BlogNoteApi.prototype.deleteBlogNote = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var _this2 = this;

        var defer, usr_id, mdl_id, vdo_note_id, qry, params;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        mdl_id = data.mdl_id;
                        vdo_note_id = data.vdo_note_id;
                        qry = "delete from " + ew_vdo_notes_tbl + " where usr_id=? and mdl_id=? and vdo_note_id =?";
                        params = [usr_id, mdl_id, vdo_note_id];

                        cassconn.execute(qry, params, function () {
                            var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(err, res) {
                                return _regenerator2.default.wrap(function _callee3$(_context3) {
                                    while (1) {
                                        switch (_context3.prev = _context3.next) {
                                            case 0:
                                                if (!err) {
                                                    _context3.next = 3;
                                                    break;
                                                }

                                                defer.reject(err);
                                                return _context3.abrupt('return');

                                            case 3:
                                                defer.resolve(data);

                                            case 4:
                                            case 'end':
                                                return _context3.stop();
                                        }
                                    }
                                }, _callee3, _this2);
                            }));

                            return function (_x5, _x6) {
                                return _ref4.apply(this, arguments);
                            };
                        }());
                        return _context4.abrupt('return', defer.promise);

                    case 8:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this);
    }));

    return function (_x4) {
        return _ref3.apply(this, arguments);
    };
}();

module.exports = new BlogNoteApi();