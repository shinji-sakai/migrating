'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:itemComments:itemQueries");
var cassExecute = require(configPath.lib.utility).cassExecute;
var forumsAPI = require(configPath.api.forums.forums);
var qaCategoryAPI = require(configPath.api.forums.category);

var ew_itm_qrys_tbl = 'ew1.ew_itm_qrys';

function Query() {}

Query.prototype.save = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var qry_title, usr_id, itm_id, mdl_id, crs_id, crs_mdl_id, cat_name, cat_desc, cat_res, saveQuestionResult, query, params;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        qry_title = data.qry_title;
                        usr_id = data.usr_id;
                        itm_id = data.itm_id;

                        // find and save category

                        mdl_id = data.mdl_id;
                        crs_id = data.crs_id;
                        crs_mdl_id = data.crs_id + "-" + data.mdl_id;
                        cat_name = data.crs_nm + "-" + data.mdl_nm;
                        cat_desc = data.crs_nm + "-" + data.mdl_nm;
                        _context.next = 11;
                        return qaCategoryAPI.findAndSaveCategoryBasedOnCourseAndModuleId({
                            mdl_id: mdl_id,
                            crs_id: crs_id,
                            crs_mdl_id: crs_mdl_id,
                            cat_name: cat_name,
                            cat_desc: cat_desc
                        });

                    case 11:
                        cat_res = _context.sent;


                        cat_res = cat_res || {};

                        // save qa question with category
                        _context.next = 15;
                        return forumsAPI.saveQuestion({
                            qry_title: qry_title,
                            usr_id: usr_id,
                            qry_tags: [cat_res["cat_id"]]
                        });

                    case 15:
                        saveQuestionResult = _context.sent;
                        query = "insert into " + ew_itm_qrys_tbl + " (itm_id,qry_id)  values (?,?)";
                        params = [itm_id, saveQuestionResult.qry_id];
                        _context.next = 20;
                        return cassExecute(query, params);

                    case 20:
                        return _context.abrupt('return', {
                            qry_id: saveQuestionResult.qry_id,
                            itm_id: itm_id,
                            qry_title: qry_title
                        });

                    case 23:
                        _context.prev = 23;
                        _context.t0 = _context['catch'](0);

                        debug(_context.t0);
                        throw _context.t0;

                    case 27:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 23]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

Query.prototype.getAll = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var itm_id, qry, params, res, qry_ids, allQuestions;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        itm_id = data.itm_id;
                        qry = "select * from " + ew_itm_qrys_tbl + " where itm_id = ?";
                        params = [itm_id];
                        _context2.next = 6;
                        return cassExecute(qry, params);

                    case 6:
                        res = _context2.sent;
                        qry_ids = res.map(function (v, i) {
                            return v.qry_id;
                        });

                        debug(qry_ids);
                        _context2.next = 11;
                        return forumsAPI.getQuestions({ qry_ids: qry_ids });

                    case 11:
                        allQuestions = _context2.sent;

                        debug(allQuestions);
                        return _context2.abrupt('return', allQuestions);

                    case 16:
                        _context2.prev = 16;
                        _context2.t0 = _context2['catch'](0);

                        debug(_context2.t0);
                        throw _context2.t0;

                    case 20:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 16]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

module.exports = new Query();