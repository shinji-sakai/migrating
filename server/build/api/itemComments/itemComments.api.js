'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:itemComments.api.js");
var profileAPI = require(configPath.api.dashboard.profile);
var cassExecute = require(configPath.lib.utility).cassExecute;
var postAPI = require(configPath.api.dashboard.post);
var dateFormat = require('dateformat');

var comment_lvl0_table = 'ew1.ew_usr_vdo_cmnt_lvl0';
var comment_lvl1_table = 'ew1.ew_usr_vdo_cmnt_lvl1';
var comment_like_table = 'ew1.vt_itm_pst_like';
var comment_users_table = 'ew1.vt_itm_pst_like_usrs';
var comment_like_stats_table = 'ew1.vt_itm_like_stats';
var ew_usr_vdo_cmnt_m_table = 'ew1.ew_usr_vdo_cmnt_m';

var COMMENT_FETCH_LIMIT = 5;

function Comment() {
    this.comments = [];
}

Comment.prototype.save = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        var defer, pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_cmnt_id, cmnt_lvl, usr_id, pst_typ, insertPost, insertPost_args, isUserCommentedBeforeRes, date, pst_msg, pst_crt_by, pst_itm_id, obj, res, cbPostRes, cb_pst_id, post_res, post;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        defer = Q.defer();
                        pst_id = data.pst_id;
                        cmnt_id = TimeUuid.now();
                        cmnt_ts = new Date();
                        cmnt_txt = data.cmnt_txt;
                        cmnt_cmnt_id = data.cmnt_cmnt_id || null;
                        cmnt_lvl = data.cmnt_lvl;
                        usr_id = data.usr_id;
                        pst_typ = data.pst_typ;

                        if (!(cmnt_lvl === "0")) {
                            _context.next = 48;
                            break;
                        }

                        insertPost = 'INSERT INTO ' + comment_lvl0_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_lvl,usr_id)  ' + 'VALUES(?,?,?,?,?,?);';
                        insertPost_args = [pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_lvl, usr_id];

                        _context.next = 15;
                        return this.isUserCommentedBefore(data);

                    case 15:
                        isUserCommentedBeforeRes = _context.sent;

                        if (isUserCommentedBeforeRes) {
                            _context.next = 30;
                            break;
                        }

                        //Not Commented
                        //add to the master table and create post also
                        date = new Date();
                        pst_msg = "<tr><td>" + dateFormat(date, "mmm dS, yyyy") + "</td><td>" + cmnt_txt + "</td></tr>";
                        pst_crt_by = usr_id;
                        pst_itm_id = pst_id;
                        obj = {
                            pst_msg: pst_msg,
                            pst_crt_by: pst_crt_by,
                            postRestriction: "public",
                            pst_typ: pst_typ,
                            pst_itm_id: pst_itm_id
                        };
                        _context.next = 24;
                        return postAPI.save(obj);

                    case 24:
                        res = _context.sent;

                        data.cb_pst_id = res.pst_id;
                        _context.next = 28;
                        return this.saveToMasterTable(data);

                    case 28:
                        _context.next = 46;
                        break;

                    case 30:
                        _context.next = 32;
                        return this.getCBPostIdFromMasterTable(data);

                    case 32:
                        cbPostRes = _context.sent;
                        cb_pst_id = cbPostRes[0]["cb_pst_id"];
                        _context.next = 36;
                        return postAPI.getPostData({ postId: cb_pst_id });

                    case 36:
                        post_res = _context.sent;
                        post = post_res[0];
                        date = new Date();


                        post.pst_msg += "<tr><td>" + dateFormat(date, "mmm dS, yyyy") + "</td><td>" + cmnt_txt + "</td></tr>";
                        _context.next = 42;
                        return postAPI.edit(post);

                    case 42:
                        _context.next = 44;
                        return postAPI.getPostData({ postId: cb_pst_id });

                    case 44:
                        post_res = _context.sent;

                        debug(post_res);

                    case 46:
                        _context.next = 49;
                        break;

                    case 48:
                        if (cmnt_lvl === '1') {
                            insertPost = 'INSERT INTO ' + comment_lvl1_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_cmnt_id,cmnt_lvl,usr_id)  ' + 'VALUES(?, ?,?,?,?,?,?);';
                            insertPost_args = [pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_cmnt_id, cmnt_lvl, usr_id];
                        }

                    case 49:
                        _context.next = 51;
                        return cassExecute(insertPost, insertPost_args);

                    case 51:
                        res = _context.sent;
                        return _context.abrupt('return', {
                            cmnt_id: cmnt_id,
                            cmnt_ts: cmnt_ts
                        });

                    case 55:
                        _context.prev = 55;
                        _context.t0 = _context['catch'](0);

                        debug(_context.t0);
                        throw _context.t0;

                    case 59:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 55]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

Comment.prototype.get = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var pst_id, cmnt_lvl, cmnt_cmnt_id, last_cmnt_id, qry, params, jsonComments, usr_ids, users, userMapping, allCommentsdata;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        pst_id = data.pst_id, cmnt_lvl = data.cmnt_lvl, cmnt_cmnt_id = data.cmnt_cmnt_id, last_cmnt_id = data.last_cmnt_id;

                        if (!(!pst_id || !cmnt_lvl)) {
                            _context2.next = 4;
                            break;
                        }

                        return _context2.abrupt('return', {
                            status: 'error',
                            message: 'pst_id and cmnt_lvl are required'
                        });

                    case 4:
                        qry = void 0;
                        params = void 0;

                        if (cmnt_lvl === '0') {
                            // if 0 level comment
                            if (last_cmnt_id) {
                                // if user want to fetch from last_cmnt_id
                                qry = 'select * from ' + comment_lvl0_table + ' where pst_id = ? and cmnt_id < ? limit ' + COMMENT_FETCH_LIMIT;
                                params = [pst_id, last_cmnt_id];
                            } else {
                                // if user want to fetch first few comments
                                qry = 'select * from ' + comment_lvl0_table + ' where pst_id = ? limit ' + COMMENT_FETCH_LIMIT;
                                params = [pst_id];
                            }
                        } else {
                            // if 1 level comment
                            if (last_cmnt_id) {
                                // if user want to fetch from last_cmnt_id
                                qry = 'select * from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_cmnt_id = ? and  cmnt_id < ? limit ' + COMMENT_FETCH_LIMIT;
                                params = [pst_id, cmnt_cmnt_id, last_cmnt_id];
                            } else {
                                // if user want to fetch first few comments
                                qry = 'select * from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_cmnt_id = ? limit ' + COMMENT_FETCH_LIMIT;
                                params = [pst_id, cmnt_cmnt_id];
                            }
                        }

                        _context2.next = 9;
                        return cassExecute(qry, params);

                    case 9:
                        jsonComments = _context2.sent;


                        jsonComments = jsonComments.filter(function (v, i) {
                            if (v.usr_id) {
                                return true;
                            }
                            return false;
                        });

                        //get all userse
                        usr_ids = jsonComments.map(function (v, i) {
                            return v.usr_id;
                        });
                        _context2.next = 14;
                        return profileAPI.getUsersShortDetails({ usr_ids: usr_ids });

                    case 14:
                        users = _context2.sent;
                        userMapping = {};
                        //generate user mapping so we can direct fetch from dict

                        users.map(function (v, i) {
                            delete v.usr_email;
                            delete v.usr_role;
                            delete v.usr_ph;
                            userMapping[v.usr_id] = v;
                        });

                        allCommentsdata = jsonComments.map(function (v, i) {
                            //get post author
                            var crt_by = v['usr_id'];
                            //get user info
                            var user = userMapping[crt_by];
                            //attach user info to item
                            v['usr_info'] = user;
                            return v;
                        });
                        return _context2.abrupt('return', allCommentsdata);

                    case 21:
                        _context2.prev = 21;
                        _context2.t0 = _context2['catch'](0);
                        throw new Error(_context2.t0);

                    case 24:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 21]]);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

Comment.prototype.edit = function (data) {
    var defer = Q.defer();
    var pst_id = data.pst_id;
    var cmnt_id = data.cmnt_id;

    var cmnt_ts = new Date(data.cmnt_ts);
    var cmnt_txt = data.cmnt_txt;

    var cmnt_lvl = data.cmnt_lvl;
    var cmnt_cmnt_id = data.cmnt_cmnt_id;

    var updatePost = void 0;
    var updatePost_args = void 0;

    if (cmnt_lvl === '0') {
        updatePost = 'update ' + comment_lvl0_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ?';
        updatePost_args = [cmnt_txt, cmnt_id, pst_id];
    } else {
        updatePost = 'update ' + comment_lvl1_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ? and cmnt_cmnt_id = ?';
        updatePost_args = [cmnt_txt, cmnt_id, pst_id, cmnt_cmnt_id];
    }

    cassconn.execute(updatePost, updatePost_args, function (err, res, r) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
};

Comment.prototype.delete = function (data) {
    var defer = Q.defer();
    var pst_id = data.pst_id;
    var cmnt_id = data.cmnt_id;
    var cmnt_ts = new Date(data.cmnt_ts);
    var usr_id = data.usr_id;

    var cmnt_lvl = data.cmnt_lvl;
    var cmnt_cmnt_id = data.cmnt_cmnt_id;

    var deletePost = void 0;
    var deletePost_args = void 0;

    if (!cmnt_lvl) {
        return {
            status: 'error',
            message: 'cmnt_lvl is required'
        };
    }

    if (cmnt_lvl === '0') {
        deletePost = 'delete from ' + comment_lvl0_table + ' where pst_id = ? and cmnt_id = ?';
        deletePost_args = [pst_id, cmnt_id];
    } else if (cmnt_lvl === '1') {
        deletePost = 'delete from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_id = ? and cmnt_cmnt_id = ?';
        deletePost_args = [pst_id, cmnt_id, cmnt_cmnt_id];
    }

    cassconn.execute(deletePost, deletePost_args, function () {
        var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(err, res, r) {
            return _regenerator2.default.wrap(function _callee3$(_context3) {
                while (1) {
                    switch (_context3.prev = _context3.next) {
                        case 0:
                            if (!err) {
                                _context3.next = 4;
                                break;
                            }

                            debug(err);
                            defer.reject(err);
                            return _context3.abrupt('return');

                        case 4:
                            defer.resolve([]);

                        case 5:
                        case 'end':
                            return _context3.stop();
                    }
                }
            }, _callee3, this);
        }));

        return function (_x3, _x4, _x5) {
            return _ref3.apply(this, arguments);
        };
    }());

    return defer.promise;
};

Comment.prototype.saveToMasterTable = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var defer, usr_id, pst_id, cb_pst_id, qry, params, res;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        pst_id = data.pst_id;
                        cb_pst_id = data.cb_pst_id;
                        qry = "insert into " + ew_usr_vdo_cmnt_m_table + " (usr_id,pst_id,cb_pst_id,crt_dt) values (?,?,?,?)";
                        params = [usr_id, pst_id, cb_pst_id, new Date()];
                        _context4.prev = 6;
                        _context4.next = 9;
                        return cassExecute(qry, params);

                    case 9:
                        res = _context4.sent;
                        return _context4.abrupt('return', data);

                    case 13:
                        _context4.prev = 13;
                        _context4.t0 = _context4['catch'](6);

                        debug(_context4.t0);
                        throw _context4.t0;

                    case 17:
                        return _context4.abrupt('return', defer.promise);

                    case 18:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[6, 13]]);
    }));

    return function (_x6) {
        return _ref4.apply(this, arguments);
    };
}();

Comment.prototype.deleteFromMasterTable = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(data) {
        var defer, usr_id, pst_id, qry, params, res;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        pst_id = data.pst_id;
                        qry = "delete from " + ew_usr_vdo_cmnt_m_table + " where usr_id=? and pst_id=?";
                        params = [usr_id, pst_id, cb_pst_id];
                        _context5.prev = 5;
                        _context5.next = 8;
                        return cassExecute(qry, params);

                    case 8:
                        res = _context5.sent;
                        return _context5.abrupt('return', data);

                    case 12:
                        _context5.prev = 12;
                        _context5.t0 = _context5['catch'](5);

                        debug(_context5.t0);
                        throw _context5.t0;

                    case 16:
                        return _context5.abrupt('return', defer.promise);

                    case 17:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this, [[5, 12]]);
    }));

    return function (_x7) {
        return _ref5.apply(this, arguments);
    };
}();

Comment.prototype.getCBPostIdFromMasterTable = function () {
    var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(data) {
        var usr_id, pst_id, qry, params, res;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        _context6.prev = 0;
                        usr_id = data.usr_id;
                        pst_id = data.pst_id;
                        qry = "select * from " + ew_usr_vdo_cmnt_m_table + " where usr_id =? and pst_id = ?";
                        params = [usr_id, pst_id];
                        _context6.next = 7;
                        return cassExecute(qry, params);

                    case 7:
                        res = _context6.sent;
                        return _context6.abrupt('return', res);

                    case 11:
                        _context6.prev = 11;
                        _context6.t0 = _context6['catch'](0);

                        debug(_context6.t0);
                        throw _context6.t0;

                    case 15:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, this, [[0, 11]]);
    }));

    return function (_x8) {
        return _ref6.apply(this, arguments);
    };
}();

Comment.prototype.isUserCommentedBefore = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(data) {
        var defer, usr_id, pst_id, qry, params, res;
        return _regenerator2.default.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        defer = Q.defer();
                        usr_id = data.usr_id;
                        pst_id = data.pst_id;
                        qry = "select * from " + ew_usr_vdo_cmnt_m_table + " where usr_id =? and pst_id = ?";
                        params = [usr_id, pst_id];
                        _context7.prev = 5;
                        _context7.next = 8;
                        return cassExecute(qry, params);

                    case 8:
                        res = _context7.sent;

                        if (!(res.length > 0)) {
                            _context7.next = 13;
                            break;
                        }

                        return _context7.abrupt('return', true);

                    case 13:
                        return _context7.abrupt('return', false);

                    case 14:
                        _context7.next = 20;
                        break;

                    case 16:
                        _context7.prev = 16;
                        _context7.t0 = _context7['catch'](5);

                        debug(_context7.t0);
                        return _context7.abrupt('return', false);

                    case 20:
                        return _context7.abrupt('return', defer.promise);

                    case 21:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, this, [[5, 16]]);
    }));

    return function (_x9) {
        return _ref7.apply(this, arguments);
    };
}();

Comment.prototype.likeDislikeComment = function (data) {
    var defer = Q.defer();
    var pst_id = data.pst_id || null;
    var usr_id = data.usr_id || 'n/a';
    var cmnt_id = data.cmnt_id || null;
    var crt_ts = new Date();
    var like_dlike_flg = data.like_dlike_flg;

    var insert = 'insert into ' + comment_like_table + ' (pst_id,usr_id,cmnt_id,crt_ts,like_dlike_flg) values(?,?,?,?,?)';
    var insert_args = [pst_id, usr_id, cmnt_id, crt_ts, like_dlike_flg];

    var queryOptions = { prepare: true, consistency: cassandra.types.consistencies.quorum };
    cassconn.execute(insert, insert_args, queryOptions, function (err, res, r) {

        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
};

Comment.prototype.getLikeDislikeComments = function (data) {
    var defer = Q.defer();

    var pst_id = data.pst_id || null;
    var usr_id = data.usr_id || 'n/a';

    var select = 'select * from ' + comment_like_table + ' where pst_id = ? and  usr_id = ?';
    var select_args = [pst_id, usr_id];

    var queryOptions = { prepare: true, consistency: cassandra.types.consistencies.quorum };
    cassconn.execute(select, select_args, queryOptions, function (err, res, r) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve(res.rows || []);
    });
    return defer.promise;
};

Comment.prototype.getCommentUsers = function () {
    var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(data) {
        var cmnt_id, query, params, res, usersIdArr, usersDetails;
        return _regenerator2.default.wrap(function _callee8$(_context8) {
            while (1) {
                switch (_context8.prev = _context8.next) {
                    case 0:
                        _context8.prev = 0;
                        cmnt_id = data.cmnt_id;
                        query = "select * from " + comment_users_table + " where cmnt_id=?";
                        params = [cmnt_id];
                        _context8.next = 6;
                        return cassExecute(query, params);

                    case 6:
                        res = _context8.sent;

                        //get all user id
                        usersIdArr = res.map(function (v, i) {
                            return v.usr_id;
                        });
                        //get users short details

                        _context8.next = 10;
                        return profileAPI.getUsersShortDetails({ usr_ids: usersIdArr });

                    case 10:
                        usersDetails = _context8.sent;

                        //remove email from all users and send it to client
                        usersDetails = usersDetails.map(function (v, i) {
                            delete v["usr_email"];
                            return v;
                        });
                        return _context8.abrupt('return', usersDetails);

                    case 15:
                        _context8.prev = 15;
                        _context8.t0 = _context8['catch'](0);

                        debug(_context8.t0);
                        throw _context8.t0;

                    case 19:
                    case 'end':
                        return _context8.stop();
                }
            }
        }, _callee8, this, [[0, 15]]);
    }));

    return function (_x10) {
        return _ref8.apply(this, arguments);
    };
}();

// updateCommentLikeDislikeCount
Comment.prototype.updateCommentLikeDislikeCount = function (data) {
    var defer = Q.defer();

    //data = {pst_id : pst_id , cmnt_id : cmnt_id , like : -1/0/1 , dislike : -1/0/1}

    var pst_id = data.pst_id || null;
    var cmnt_id = data.cmnt_id || null;

    var like_cnt_str = ' like_cnt = like_cnt + ' + (data.like || 0);
    var dlike_cnt_str = ' dlike_cnt = dlike_cnt + ' + (data.dislike || 0);
    var where_clause = ' where pst_id = ? and cmnt_id = ?';

    var update_like = 'update ' + comment_like_stats_table + ' set ' + like_cnt_str + "," + dlike_cnt_str + where_clause;
    var update_args = [pst_id, cmnt_id];

    var queryOptions = { prepare: true, consistency: cassandra.types.consistencies.quorum };
    cassconn.execute(update_like, update_args, queryOptions, function (err, res, r) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
};

Comment.prototype.getCommentLikeDislikeCount = function (data) {
    var defer = Q.defer();

    //data = {pst_id : pst_id}

    var pst_id = data.pst_id || null;

    var select_like = 'select * from ' + comment_like_stats_table + ' where pst_id = ?';
    var select_args = [pst_id];

    var queryOptions = { prepare: true, consistency: cassandra.types.consistencies.quorum };
    cassconn.execute(select_like, select_args, queryOptions, function (err, res, r) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve(res.rows || []);
    });
    return defer.promise;
};

module.exports = new Comment();