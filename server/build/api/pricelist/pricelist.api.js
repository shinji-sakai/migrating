'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var batchCourseAPI = require(configPath.api.batchCourse.batchCourse);
var batchTimingAPI = require(configPath.api.batchCourse.batchTiming);
var bundleAPI = require(configPath.api.bundle.bundle);
var uuid = require('uuid');
var debug = require('debug')("app:api:pricelist.api.js");

var Q = require('q');

function Pricelist() {}

Pricelist.prototype.getAll = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
    var connection, collection, find_res, find;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    find = function find(col) {
                        return col.find({}, { _id: 0 }).toArray();
                    };

                    _context.prev = 1;
                    _context.next = 4;
                    return getConnection();

                case 4:
                    connection = _context.sent;
                    _context.next = 7;
                    return dbCollection(connection);

                case 7:
                    collection = _context.sent;
                    _context.next = 10;
                    return find(collection);

                case 10:
                    find_res = _context.sent;
                    return _context.abrupt('return', find_res);

                case 14:
                    _context.prev = 14;
                    _context.t0 = _context['catch'](1);

                    globalFunctions.err(_context.t0);

                case 17:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this, [[1, 14]]);
}));

Pricelist.prototype.getPricelistByCourseId = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(data) {
        var connection, collection, find_res, find;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        find = function find(col) {
                            return col.find({ crs_id: data.crs_id }, { _id: 0 }).toArray();
                        };

                        _context2.prev = 1;
                        _context2.next = 4;
                        return getConnection();

                    case 4:
                        connection = _context2.sent;
                        _context2.next = 7;
                        return dbCollection(connection);

                    case 7:
                        collection = _context2.sent;
                        _context2.next = 10;
                        return find(collection);

                    case 10:
                        find_res = _context2.sent;
                        return _context2.abrupt('return', find_res[0]);

                    case 14:
                        _context2.prev = 14;
                        _context2.t0 = _context2['catch'](1);

                        globalFunctions.err(_context2.t0);

                    case 17:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[1, 14]]);
    }));

    return function (_x) {
        return _ref2.apply(this, arguments);
    };
}();

Pricelist.prototype.add = function (data) {
    return getConnection() //get mongodb connection
    .then(dbCollection) //get database collection
    .then(insertData).catch(function (err) {
        console.log(err);
    });

    function insertData(col) {
        //insert/update in batchcourse
        return col.update({ crs_id: data.crs_id }, data, { upsert: true, w: 1 });
    }
};
Pricelist.prototype.delete = function (data) {
    return getConnection().then(dbCollection).then(removeData).catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({ crs_id: data.crs_id });
    }
};

Pricelist.prototype.getDetailedPricelist = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
        var allPrices, res, i, v, includedCoursesDetails, batchTimings, maxBatchDiscount, discount_rsn;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        _context3.next = 3;
                        return this.getAll();

                    case 3:
                        allPrices = _context3.sent;
                        res = [];
                        i = allPrices.length - 1;

                    case 6:
                        if (!(i >= 0)) {
                            _context3.next = 25;
                            break;
                        }

                        v = allPrices[i];
                        _context3.next = 10;
                        return batchCourseAPI.getIncludedCoursesInBatchCourse({ crs_id: v.crs_id });

                    case 10:
                        includedCoursesDetails = _context3.sent;
                        _context3.next = 13;
                        return batchTimingAPI.getTimingByCourseId({ crs_id: v.crs_id });

                    case 13:
                        batchTimings = _context3.sent;

                        debug(batchTimings);
                        maxBatchDiscount = 0;
                        discount_rsn = "";

                        batchTimings.map(function (v, i) {
                            var discount = parseInt(v.discount);
                            var should_apply_discount = v.should_apply_discount;
                            if (should_apply_discount && maxBatchDiscount <= discount) {
                                maxBatchDiscount = discount;
                                discount_rsn = v.discount_rsn;
                            }
                        });
                        v["discount"] = maxBatchDiscount;
                        v["discount_rsn"] = discount_rsn;
                        v["includedCourses"] = includedCoursesDetails;
                        res.push(v);

                    case 22:
                        i--;
                        _context3.next = 6;
                        break;

                    case 25:
                        return _context3.abrupt('return', res || []);

                    case 28:
                        _context3.prev = 28;
                        _context3.t0 = _context3['catch'](0);
                        throw _context3.t0;

                    case 31:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 28]]);
    }));

    return function (_x2) {
        return _ref3.apply(this, arguments);
    };
}();

Pricelist.prototype.getDetailedPricelistByCourseId = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(data) {
        var bundles, batchTrainings, batchTrainingsId, connection, collection, allPrices, res, i, v, includedCoursesDetails, batchTimings, maxBatchDiscount, discount_rsn;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        _context4.next = 3;
                        return bundleAPI.getBundlesByCourseId({ crs_id: data.crs_id });

                    case 3:
                        bundles = _context4.sent;

                        bundles = bundles.map(function (v, i) {
                            return v.bndl_id;
                        });
                        _context4.next = 7;
                        return batchCourseAPI.getBatchCoursesByBundleIds({ bundle_ids: bundles });

                    case 7:
                        batchTrainings = _context4.sent;
                        batchTrainingsId = batchTrainings.map(function (v, i) {
                            return v.crs_id;
                        });

                        debug(batchTrainingsId);
                        _context4.next = 12;
                        return getConnection();

                    case 12:
                        connection = _context4.sent;
                        _context4.next = 15;
                        return dbCollection(connection);

                    case 15:
                        collection = _context4.sent;
                        _context4.next = 18;
                        return collection.find({
                            crs_id: {
                                $in: batchTrainingsId
                            }
                        }).toArray();

                    case 18:
                        allPrices = _context4.sent;
                        res = [];
                        i = allPrices.length - 1;

                    case 21:
                        if (!(i >= 0)) {
                            _context4.next = 39;
                            break;
                        }

                        v = allPrices[i];
                        _context4.next = 25;
                        return batchCourseAPI.getIncludedCoursesInBatchCourse({ crs_id: v.crs_id });

                    case 25:
                        includedCoursesDetails = _context4.sent;
                        _context4.next = 28;
                        return batchTimingAPI.getTimingByCourseId({ crs_id: v.crs_id });

                    case 28:
                        batchTimings = _context4.sent;
                        maxBatchDiscount = 0;
                        discount_rsn = "";

                        batchTimings.map(function (v, i) {
                            var discount = parseInt(v.discount);
                            var should_apply_discount = v.should_apply_discount;
                            if (should_apply_discount && maxBatchDiscount <= discount) {
                                maxBatchDiscount = discount;
                                discount_rsn = v.discount_rsn;
                            }
                        });
                        v["discount"] = maxBatchDiscount;
                        v["discount_rsn"] = discount_rsn;
                        v["includedCourses"] = includedCoursesDetails;
                        res.push(v);

                    case 36:
                        i--;
                        _context4.next = 21;
                        break;

                    case 39:
                        return _context4.abrupt('return', res || []);

                    case 42:
                        _context4.prev = 42;
                        _context4.t0 = _context4['catch'](0);
                        throw _context4.t0;

                    case 45:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 42]]);
    }));

    return function (_x3) {
        return _ref4.apply(this, arguments);
    };
}();

function dbCollection(db) {
    return db.collection("pricelist");
}

module.exports = new Pricelist();