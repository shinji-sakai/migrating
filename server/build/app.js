'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var subdomain = require('express-subdomain');
//var authPassport = require("./auth.js")();
var configPath = require('./configPath.js');
var logs = require('./logger/log');
// var express_session = require('express-session');
// var RedisStore = require('connect-redis')(express_session);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
// let examwarrior_session = require(configPath.lib.session);
var lib_passport = require(configPath.lib.passport);
//let singleLoginMiddleware = require(configPath.lib.singleLogin)


var baseUrlConfig = require(configPath.config.baseUrlConfig);
// var admin = require(configPath.routes.admin);
var test = require(configPath.routes.test);
var qa = require(configPath.routes.qa);
var ejs = require(configPath.routes.ejsRoutes);
var videoUrl = require(configPath.routes.videoUrl);
var bookmarks = require(configPath.routes.bookmark);

var auth = require(configPath.routes.auth);
var usercourses = require(configPath.routes.usercourses);
var mail = require(configPath.routes.mail.mail);
var authorizeitems = require(configPath.routes.authorizeitems);
var userstats = require(configPath.routes.kafka.userstats);
var practice = require(configPath.routes.qa_dir.practice);
var highlight = require(configPath.routes.qa_dir.highlight);
var createtest = require(configPath.routes.qa_dir.createtest);

var posts = require(configPath.routes.dashboard.post);
var comments = require(configPath.routes.dashboard.comment);
var profile = require(configPath.routes.dashboard.profile);
var dashboardStats = require(configPath.routes.dashboard.stats);

var chatFriends = require(configPath.routes.chat.friends);
var chatMsgs = require(configPath.routes.chat.msgs);
var chatGrp = require(configPath.routes.chat.grp);

var adminGeneral = require(configPath.routes.admin_dir.general);
var adminSubgroup = require(configPath.routes.admin_dir.subgroup);
var author = require(configPath.routes.admin_dir.author);
var publication = require(configPath.routes.admin_dir.publication);
var book = require(configPath.routes.admin_dir.book);
var subject = require(configPath.routes.admin_dir.subject);
var exam = require(configPath.routes.admin_dir.exam);
var topic = require(configPath.routes.admin_dir.topic);
var subtopic = require(configPath.routes.admin_dir.subtopic);
var tag = require(configPath.routes.admin_dir.tag);
var moduleItems = require(configPath.routes.admin_dir.moduleItems);
var adminCourse = require(configPath.routes.admin_dir.course);
var adminCourseModule = require(configPath.routes.admin_dir.courseModule);
var adminCourseMaster = require(configPath.routes.admin_dir.courseMaster);
var adminCourseVideos = require(configPath.routes.admin_dir.courseVideos);
var adminVideoSlide = require(configPath.routes.admin_dir.videoSlide);
var adminCourseBundle = require(configPath.routes.admin_dir.courseBundle);
var adminSideLinks = require(configPath.routes.admin_dir.sideLinks);
var adminAllLinksCategory = require(configPath.routes.admin_dir.allLinksCategory);
var adminAllLinks = require(configPath.routes.admin_dir.allLinks);
var adminEmailTemplates = require(configPath.routes.admin_dir.emailTemplates);
var adminUserEmailGroup = require(configPath.routes.admin_dir.userEmailGroup);
var adminSendEmailTemplates = require(configPath.routes.admin_dir.sendEmailTemplates);
var adminSendNotification = require(configPath.routes.admin_dir.sendNotification);
var adminFormsRoles = require(configPath.routes.admin_dir.roles);
var adminFormsForms = require(configPath.routes.admin_dir.forms);
var adminFormsHelp = require(configPath.routes.admin_dir.helpForm);
var adminUserRoles = require(configPath.routes.admin_dir.userRoles);
var adminUserBatchEnroll = require(configPath.routes.admin_dir.userBatchEnroll);
var adminBookTopic = require(configPath.routes.admin_dir.booktopic);
var adminJobOpenings = require(configPath.routes.admin_dir.jobOpenings);
var adminTopicGroup = require(configPath.routes.admin_dir.topicGroup);
var adminBoardCompetitive = require(configPath.routes.admin_dir.boardCompetitive);
var adminPreviousPapers = require(configPath.routes.admin_dir.previousPapers);

var forums = require(configPath.routes.forums.forums);
var categories = require(configPath.routes.forums.category);
var qry_follow = require(configPath.routes.forums.follow);
var qa_ans = require(configPath.routes.forums.ans);

var videostats = require(configPath.routes.videostats);

var ckeditor = require(configPath.routes.ckeditor);

var contact = require(configPath.routes.contact);

var paymentRoutes = require(configPath.routes.payment.index);
var employeeRoutes = require(configPath.routes.employee.index);
var batchCourseRoutes = require(configPath.routes.batchCourse.index);
var pricelistRoutes = require(configPath.routes.pricelist.index);
var bundleRoutes = require(configPath.routes.bundle.index);
var itemCommentsRoutes = require(configPath.routes.itemComments.index);
var awsRoutes = require(configPath.routes.aws.index);
var videoRoutes = require(configPath.routes.video.index);
var jobRoutes = require(configPath.routes.job.index);
var smsRoutes = require(configPath.routes.sms.index);
var bookRoutes = require(configPath.routes.book.index);

var authorizeitemsAPI = require(configPath.api.authorizeitems);
var courseAPI = require(configPath.api.admin.course);
var notesRoutes = require(configPath.routes.notes.index);

var app = express();
app.use(lib_passport.initialize());

var helpers = require('express-helpers')();
app.locals = helpers;

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(compression({
    level: 1
}));
// app.use(examwarrior_session);
app.use(logger('dev'));
app.use(bodyParser.json());
//app.use(authPassport.initialize());
// app.use(singleLoginMiddleware());
app.use(function (req, res, next) {
    var index = configPath.privateRoutes.indexOf(req.url);
    if (index > -1) {
        console.log("privateRoutes.....", req.url);
    }
    next();
});

if (process.env.MODE != "DEV") {
    // Always route to secure path
    app.use(function (req, res, next) {
        if (req.secure) {
            next();
        } else {
            // request was via http, so redirect to https
            res.redirect('https://' + req.headers.host + req.url);
        }
    });
}

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-session-id, x-track-session");
    next();
});

app.use(function (req, res, next) {
    res.append('imageBaseUrl', baseUrlConfig.MAIN_IMAGE_BASE_URL);
    next();
});

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser('ew'));
// This will change in production since we'll be using the dist folder
app.use(express.static(path.join(__dirname, '../../client/build/'), { maxAge: 0 }));
app.use('/resources', express.static(path.join(__dirname, '../../client/resources/'), { maxAge: 0 }));
app.use(express.static(path.join(__dirname, '../../server/views/'), { maxAge: 0 }));
app.use('/authApp', express.static(path.join(__dirname, '../../client/build/authApp/'), { maxAge: 0 }));
app.use('/dashboardApp', express.static(path.join(__dirname, '../../client/build/dashboardApp/'), { maxAge: 0 }));
app.use('/adminApp', express.static(path.join(__dirname, '../../client/build/adminApp/'), { maxAge: 0 }));
app.use('/app', express.static(path.join(__dirname, '../../client/build/app/'), { maxAge: 0 }));
app.use('/forums', express.static(path.join(__dirname, '../../client/build/forums/'), { maxAge: 0 }));
app.use('/books', express.static(path.join(__dirname, '../../client/build/books/'), { maxAge: 0 }));
app.use('/courses', express.static(path.join(__dirname, '../../client/build/courses/'), { maxAge: 0 }));
app.use('/buy', express.static(path.join(__dirname, '../../client/build/buy/'), { maxAge: 0 }));
app.use('/training', express.static(path.join(__dirname, '../../client/build/training/'), { maxAge: 0 }));
app.use('/blog', express.static(path.join(__dirname, '../../client/build/blog/'), { maxAge: 0 }));
app.use('/search', express.static(path.join(__dirname, '../../client/build/search/'), { maxAge: 0 }));
app.use('/career-book', express.static(path.join(__dirname, '../../client/build/career-book/'), { maxAge: 0 }));
app.use('/docs', express.static(path.join(__dirname, '../../api_doc'), { maxAge: 0 }));

// app.use('/logout', function(req, res,next) {
//     if(req.session){
//         // console.log("logout.......",req.session.loginData)
//         // if(req.session.loginData && req.session.loginData.usr_id){
//         //     redisClient.del(req.session.loginData.usr_id + "-session")
//         // }
//         req.session.destroy();
//     }
//     next()
// });
// app.use(['/login/out','/login/session-expired'], function(req, res,next) {
//     if(req.session){
//         req.session.destroy(function(){
//             next()
//         });
//     }

// });

app.use(['/all-links'], function (req, res) {
    res.sendFile(path.join(__dirname, '../../client/build/responsive-misc-pages/index.html'), function (err) {
        if (err) {
            render404(res);
        }
    });
});

app.use(['/question-answers', '/question-answers/question/:id', '/question-answers/question/new/:id', '/question-answers/question/:id/answer/:ans_id', '/question-answers/question/new/:id/answer/:ans_id'], function (req, res) {
    console.log("in qa routes");
    res.sendFile(path.join(__dirname, '../../client/build/forums/index.html'), function (err) {
        if (err) {
            render404(res);
        }
    });
});

app.use(['/login', '/register', '/forgotpassword', '/changepassword', '/logout'], function (req, res) {
    res.sendFile(path.join(__dirname, '../../client/build/responsive-auth-pages/index.html'), function (err) {
        if (err) {
            render404(res);
        }
    });
});

app.use(['/it-trainings-old', '/board-trainings-old', '/competitive-trainings-old'], function (req, res) {
    res.sendFile(path.join(__dirname, '../../client/build/courses/index.html'), function (err) {
        if (err) {
            render404(res);
        }
    });
});

//Courselist page
app.use(['/it-trainings', '/board-trainings', '/competitive-trainings', '/previous-papers', '/board-exams', '/competitive-exams'], function (req, res) {
    res.sendFile(path.join(__dirname, '../../client/build/responsive-misc-pages/index.html'), function (err) {
        if (err) {
            render404(res);
        }
    });
});

app.use(['/practice/new', '/video/new'], function (req, res) {
    res.sendFile(path.join(__dirname, '../../client/build/courses/index.html'), function (err) {
        if (err) {
            render404(res);
        }
    });
});

// Course Page
app.use('/:main_type/*-trainings/:crs_id', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var main_type, courseId, api_res;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        main_type = req.params.main_type;

                        if (!(main_type === "it" || main_type === "board" || main_type === "competitive")) {
                            _context.next = 16;
                            break;
                        }

                        courseId = req.params.crs_id;
                        _context.prev = 3;
                        _context.next = 6;
                        return courseAPI.isCourseExist({
                            courseId: courseId
                        });

                    case 6:
                        api_res = _context.sent;

                        //check whether course exist or not
                        //if not then throw 404 page
                        if (api_res.exist) {
                            res.sendFile(path.join(__dirname, '../../client/build/responsive-misc-pages/index.html'), function (err) {
                                if (err) {
                                    render404(res);
                                }
                            });
                        } else {
                            render404(res);
                        }
                        _context.next = 14;
                        break;

                    case 10:
                        _context.prev = 10;
                        _context.t0 = _context['catch'](3);

                        console.log(_context.t0);
                        render404(res);

                    case 14:
                        _context.next = 17;
                        break;

                    case 16:
                        render404(res);

                    case 17:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[3, 10]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());
//Video Page
app.use('/:courseid/video/:itemid?', function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req, res) {
        var courseId, itemId, api_res;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        courseId = req.params.courseid;
                        itemId = req.params.itemid;
                        _context2.prev = 2;
                        _context2.next = 5;
                        return authorizeitemsAPI.isItemExist({
                            courseId: courseId,
                            itemId: itemId
                        });

                    case 5:
                        api_res = _context2.sent;

                        //check whether item and course exist or not
                        //if not then throw 404 page
                        if (api_res.exist) {
                            res.sendFile(path.join(__dirname, '../../client/build/courses/index.html'), function (err) {
                                if (err) {
                                    render404(res);
                                }
                            });
                        } else {
                            render404(res);
                        }
                        _context2.next = 13;
                        break;

                    case 9:
                        _context2.prev = 9;
                        _context2.t0 = _context2['catch'](2);

                        console.log(_context2.t0);
                        render404(res);

                    case 13:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[2, 9]]);
    }));

    return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
    };
}());
// Question Page
app.use('/:courseid/questions/:itemid?/:question?', function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(req, res) {
        var courseId, itemId, api_res;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        courseId = req.params.courseid;
                        itemId = req.params.itemid;
                        _context3.prev = 2;
                        _context3.next = 5;
                        return authorizeitemsAPI.isItemExist({
                            courseId: courseId,
                            itemId: itemId
                        });

                    case 5:
                        api_res = _context3.sent;

                        //check whether item and course exist or not
                        //if not then throw 404 page
                        if (api_res.exist) {
                            res.sendFile(path.join(__dirname, '../../client/build/courses/index.html'), function (err) {
                                if (err) {
                                    render404(res);
                                }
                            });
                        } else {
                            render404(res);
                        }
                        _context3.next = 13;
                        break;

                    case 9:
                        _context3.prev = 9;
                        _context3.t0 = _context3['catch'](2);

                        console.log(_context3.t0);
                        render404(res);

                    case 13:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[2, 9]]);
    }));

    return function (_x5, _x6) {
        return _ref3.apply(this, arguments);
    };
}());
//Book Topic Page
app.use('/:courseid/book-topic/:itemid?', function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(req, res) {
        var courseId, itemId, api_res;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        courseId = req.params.courseid;
                        itemId = req.params.itemid;
                        _context4.prev = 2;
                        _context4.next = 5;
                        return authorizeitemsAPI.isItemExist({
                            courseId: courseId,
                            itemId: itemId
                        });

                    case 5:
                        api_res = _context4.sent;

                        //check whether item and course exist or not
                        //if not then throw 404 page
                        if (api_res.exist) {
                            res.sendFile(path.join(__dirname, '../../client/build/courses/index.html'), function (err) {
                                if (err) {
                                    render404(res);
                                }
                            });
                        } else {
                            render404(res);
                        }
                        _context4.next = 13;
                        break;

                    case 9:
                        _context4.prev = 9;
                        _context4.t0 = _context4['catch'](2);

                        console.log(_context4.t0);
                        render404(res);

                    case 13:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[2, 9]]);
    }));

    return function (_x7, _x8) {
        return _ref4.apply(this, arguments);
    };
}());

//buy home page
// app.use('/buy/:pricing_type', function(req, res) {
//     var pricing_type = req.params.pricing_type;

//     if(pricing_type === "it" || pricing_type === "board" || pricing_type === "competitive"){
//         res.sendFile(path.join(__dirname, '../../client/build/buy/index.html'), function(err) {
//             if (err) {
//                 render404(res);
//             }
//         });
//     }else{
//         render404(res);
//     }
// });

// app.use('/buy/course/:course', function(req, res) {
//     res.sendFile(path.join(__dirname, '../../client/build/buy/index.html'), function(err) {
//         if (err) {
//             render404(res);
//         }
//     });
// });


app.use('/', ejs);
// app.use('/admin', admin);
app.use('/test', test);
app.use('/qa', qa);
app.use('/videoUrl', videoUrl);
app.use('/bookmarks', bookmarks);
app.use('/auth', auth);
app.use('/mail', mail);
app.use('/ai', authorizeitems); //ai = authorizeitems
app.use('/uc', usercourses); //uc = usercourses
app.use('/userstats', userstats);
app.use('/practice', practice);
app.use('/highlight', highlight);

app.use('/dashboard/post', posts);
app.use('/dashboard/comment', comments);
app.use('/dashboard/profile', profile);
app.use('/dashboard/stats', dashboardStats);

//root pages
app.use(['/login-old', '/register-old', '/logout-old', '/changepassword-old', '/forgotpassword-old', '/sitemap', '/sitemap/', '/user-profile', '/dashboard', '/video-stats', '/video-stats/', '/practice-stats', '/click-stats', '/page-stats', '/test-performance', '/notifications', '/whiteboard', '/redis-java', '/youtube-test', '/admin-stats-monitor', '/current-openings', '/apply-for-job', '/messages'], function (req, res) {
    res.sendFile(path.join(__dirname, '../../client/build/homeApp/index.html'), function (err) {
        if (err) {
            render404(res);
        }
    });
});

app.use('/admin/general', adminGeneral);
app.use('/author', author);
app.use('/course', adminCourse);
app.use('/courseModule', adminCourseModule);
app.use('/courseMaster', adminCourseMaster);
app.use('/subgroup', adminSubgroup);
app.use('/courseVideos', adminCourseVideos);
app.use('/videoSlide', adminVideoSlide);
app.use('/sideLinksArticle', adminSideLinks);
app.use('/allLinksCategory', adminAllLinksCategory);
app.use('/allLinks', adminAllLinks);
app.use('/emailTemplates', adminEmailTemplates);
app.use('/userEmailGroup', adminUserEmailGroup);
app.use('/sendEmailTemplate', adminSendEmailTemplates);
app.use('/sendNotification', adminSendNotification);
app.use('/adminRoles', adminFormsRoles);
app.use('/adminForms', adminFormsForms);
app.use('/userRoles', adminUserRoles);
app.use('/formsHelp', adminFormsHelp);
app.use('/userBatchEnroll', adminUserBatchEnroll);
app.use('/booktopic', adminBookTopic);
app.use('/jobOpenings', adminJobOpenings);
app.use('/topicGroup', adminTopicGroup);
app.use('/boardCompetitive', adminBoardCompetitive);
app.use('/previousPapers', adminPreviousPapers);

//

app.use('/publication', publication);
app.use('/book', book);
app.use('/subject', subject);
app.use('/exam', exam);
app.use('/topic', topic);
app.use('/subtopic', subtopic);
app.use('/tag', tag);
app.use('/courseBundle', adminCourseBundle);
app.use('/moduleItems', moduleItems);
app.use('/chat/friends', chatFriends);
app.use('/chat/msgs', chatMsgs);
app.use('/chat/grp', chatGrp);

app.use('/createtest', createtest);
app.use('/forums', forums);
app.use('/forums/category', categories);
app.use('/forums/follow', qry_follow);
app.use('/forums/ans', qa_ans);

app.use('/videostats', videostats);

//for saving images from editor
app.use('/ckeditor', ckeditor);
app.use('/contact', contact);

// Paypal
paymentRoutes(app);
// Employee
employeeRoutes(app);
// Batch course routes
batchCourseRoutes(app);
// Pricelist routes
pricelistRoutes(app);
// bundle routes
bundleRoutes(app);
// Item comments routes
itemCommentsRoutes(app);

// aws routes
awsRoutes(app);

//video routes
videoRoutes(app);

//job routes
jobRoutes(app);

//sms routes
smsRoutes(app);

//book routes
bookRoutes(app);

// notes routes
notesRoutes(app);

app.get("/*", function (req, res, next) {
    var pos = req.originalUrl.indexOf('/', 1);
    var substr = req.originalUrl.substr(0, pos);
    res.sendFile(path.join(__dirname, '../../client/build/' + substr + '/index.html'), function (err) {
        if (err) {
            // res.status(err.status).send("Not Found");
            render404(res);
        }
    });
});

// Error Handling

app.use(function (err, req, res, next) {

    if (err && err.page === '404') {
        res.render('pages/404', function (err, html) {
            if (err) {
                res.status(err.status || 500);
            } else {
                res.status(200).send({
                    err: 400,
                    html: html
                });
            }
        });
    } else if (err) {
        console.log('err.  ', err);
        var date = new Date();
        var err_obj = {
            'timestamp': date,
            'user_name': req.body && req.body.usr_id || 'Unknown',
            'url': req.url,
            'error': err
        };

        logs.logInfo("error", JSON.stringify(err_obj));
        res.status(err.status || 500);
        res.json({ "statusCode": 0, "err": err_obj });
    }
});

function render404(res) {
    res.render('pages/404', function (err, html) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(400).send(html);
        }
    });
}

// app.use(function(err,req,res){
//   console.log("error handle............",err);
// })
module.exports = app;