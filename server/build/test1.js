'use strict';

var configPath = require('./configPath.js');
var cassconn = require(configPath.dbconn.cassconn);

function test() {
    var query = "select * from ew1.ew_usr_vdo_cmnt";
    var params = [];
    cassconn.stream(query, params, {}).on('readable', function () {
        // readable is emitted as soon a row is received and parsed
        var row;
        while (row = this.read()) {
            console.log(row);
        }
    }).on('end', function () {
        console.log("end");
        // emitted when all rows have been retrieved and read
    });
}

function test2() {
    var query = "select * from ew1.ew_usr_vdo_cmnt";
    var params = [];
    var options = { prepare: true, fetchSize: 2 };
    cassconn.eachRow(query, params, options, function (n, row) {
        // Invoked per each row in all the pages
        console.log(n, row);
    }, function (err, result) {
        console.log(result);
        // Called once the page has been retrieved.
        if (result.nextPage) {
            // Retrieve the following pages:
            // the same row handler from above will be used
            result.nextPage();
        }
    });
}

test2();