'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var transfer = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
		var db, moduleItemsCol, moduleItems, authItemsCol, authItems, authItemsMap, i, row, courseId, moduleDetail, j, module, items, k, item, itemId, itemPermission;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						_context.next = 3;
						return getConnection();

					case 3:
						db = _context.sent;
						_context.next = 6;
						return db.collection('moduleitems');

					case 6:
						moduleItemsCol = _context.sent;
						_context.next = 9;
						return moduleItemsCol.find({}).toArray();

					case 9:
						moduleItems = _context.sent;
						_context.next = 12;
						return db.collection('authorizeitems');

					case 12:
						authItemsCol = _context.sent;
						_context.next = 15;
						return authItemsCol.find({}).toArray();

					case 15:
						authItems = _context.sent;
						authItemsMap = {};

						authItems.forEach(function (v, i) {
							authItemsMap[v.itemId] = v;
						});

						i = 0;

					case 19:
						if (!(i < moduleItems.length)) {
							_context.next = 29;
							break;
						}

						row = moduleItems[i];
						courseId = row.courseId;
						moduleDetail = row.moduleDetail || [];

						for (j = 0; j < moduleDetail.length; j++) {
							module = moduleDetail[j];
							items = module.moduleItems || [];

							for (k = 0; k < items.length; k++) {
								item = items[k];
								itemId = item.itemId;
								itemPermission = authItemsMap[itemId].itemType || 'paid';

								item.itemPermission = itemPermission;
							}
						}
						_context.next = 26;
						return redisCli.setAsync(courseId, JSON.stringify(row));

					case 26:
						i++;
						_context.next = 19;
						break;

					case 29:

						process.exit();
						_context.next = 36;
						break;

					case 32:
						_context.prev = 32;
						_context.t0 = _context['catch'](0);

						console.log(_context.t0);
						return _context.abrupt('return', 0);

					case 36:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 32]]);
	}));

	return function transfer() {
		return _ref.apply(this, arguments);
	};
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

transfer();