'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var allPracticeQuestonToRedis = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
		var _this = this;

		var db, col, cursor, total, k;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						_context2.next = 3;
						return getConnection();

					case 3:
						db = _context2.sent;
						_context2.next = 6;
						return db.collection('practiceQuestions');

					case 6:
						col = _context2.sent;
						_context2.next = 9;
						return col.find({});

					case 9:
						cursor = _context2.sent;
						_context2.next = 12;
						return cursor.count();

					case 12:
						total = _context2.sent;

						console.log('Total ' + total + ' questions to transfer');
						k = 0;

						cursor.forEach(function () {
							var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(question, i) {
								var id, json, ans, r;
								return _regenerator2.default.wrap(function _callee$(_context) {
									while (1) {
										switch (_context.prev = _context.next) {
											case 0:
												id = question.questionId;
												json = JSON.stringify(question);
												ans = "" + (question.ans || []).join(",");
												_context.next = 5;
												return redisCli.hmsetAsync(id, {
													"json": json,
													"ans": ans
												});

											case 5:
												r = _context.sent;

												k++;
												console.log(k + " / " + total);
												if (k === total) {
													console.log("All questions transferred");
												}

											case 9:
											case 'end':
												return _context.stop();
										}
									}
								}, _callee, _this);
							}));

							return function (_x, _x2) {
								return _ref2.apply(this, arguments);
							};
						}());

						// console.log(`Total ${arr_que.length} questions to transfer`)
						// let promises = [];
						// for (var i = 0; i < arr_que.length; i++) {
						// 	let question = arr_que[i];
						// 	let id = question.questionId;
						// 	let json = JSON.stringify(question);
						// 	let ans = "" + (question.ans || []).join(",");
						// 	let r = await redisCli.hmsetAsync(id,{
						// 		"json" : json,
						// 		"ans" : ans
						// 	})
						// 	promises.push(r);
						// }

						// await Q.all(promises);

						return _context2.abrupt('return');

					case 19:
						_context2.prev = 19;
						_context2.t0 = _context2['catch'](0);

						console.log(_context2.t0);
						return _context2.abrupt('return', 0);

					case 23:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 19]]);
	}));

	return function allPracticeQuestonToRedis() {
		return _ref.apply(this, arguments);
	};
}();

var transferItemQuestions = function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3() {
		var db, moduleItemsCol, moduleItems, i, row, courseId, moduleDetail, j, module, items, k, item, itemId;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						_context3.next = 3;
						return getConnection();

					case 3:
						db = _context3.sent;
						_context3.next = 6;
						return db.collection('moduleitems');

					case 6:
						moduleItemsCol = _context3.sent;
						_context3.next = 9;
						return moduleItemsCol.find({}).toArray();

					case 9:
						moduleItems = _context3.sent;
						i = 0;

					case 11:
						if (!(i < moduleItems.length)) {
							_context3.next = 37;
							break;
						}

						row = moduleItems[i];
						courseId = row.courseId;
						moduleDetail = row.moduleDetail || [];
						j = 0;

					case 16:
						if (!(j < moduleDetail.length)) {
							_context3.next = 34;
							break;
						}

						module = moduleDetail[j];
						items = module.moduleItems || [];
						k = 0;

					case 20:
						if (!(k < items.length)) {
							_context3.next = 31;
							break;
						}

						item = items[k];
						itemId = item.itemId;

						if (!item.itemQuestions) {
							_context3.next = 28;
							break;
						}

						_context3.next = 26;
						return redisCli.delAsync(itemId + '-qid');

					case 26:
						_context3.next = 28;
						return redisCli.rpushAsync(itemId + '-qid', item.itemQuestions);

					case 28:
						k++;
						_context3.next = 20;
						break;

					case 31:
						j++;
						_context3.next = 16;
						break;

					case 34:
						i++;
						_context3.next = 11;
						break;

					case 37:
						return _context3.abrupt('return');

					case 40:
						_context3.prev = 40;
						_context3.t0 = _context3['catch'](0);

						console.log(_context3.t0);
						return _context3.abrupt('return', 0);

					case 44:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 40]]);
	}));

	return function transferItemQuestions() {
		return _ref3.apply(this, arguments);
	};
}();

var init = function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4() {
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;

						console.log("Script Started");
						_context4.next = 4;
						return transferItemQuestions();

					case 4:
						_context4.next = 6;
						return allPracticeQuestonToRedis();

					case 6:
						_context4.next = 12;
						break;

					case 8:
						_context4.prev = 8;
						_context4.t0 = _context4['catch'](0);

						console.log(_context4.t0);
						return _context4.abrupt('return', 0);

					case 12:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 8]]);
	}));

	return function init() {
		return _ref4.apply(this, arguments);
	};
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();
var Q = require('q');

init();