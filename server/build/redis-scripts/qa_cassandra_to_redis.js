'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var transferQAQuestionsIdToRedis = function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(argument) {
		var selectQry, res, q_ids;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						selectQry = 'select * from ' + forum_table;
						_context.next = 4;
						return cassExecute(selectQry);

					case 4:
						res = _context.sent;

						if (!(res && res.length > 0)) {
							_context.next = 15;
							break;
						}

						res.sort(function (a, b) {
							var a_date = new Date(a.qry_ts);
							var b_date = new Date(b.qry_ts);
							return a_date < b_date;
						});
						console.log('Found ' + res.length + ' QA Question in cassandra');
						q_ids = res.map(function (v, i) {
							return v.qry_id;
						});
						_context.next = 11;
						return redisCli.delAsync("qa-question-ids");

					case 11:
						_context.next = 13;
						return redisCli.rpushAsync("qa-question-ids", q_ids);

					case 13:
						console.log('Added ' + res.length + ' QA Question to redis');
						process.exit(0);

					case 15:
						_context.next = 20;
						break;

					case 17:
						_context.prev = 17;
						_context.t0 = _context['catch'](0);

						logger.debug(_context.t0);

					case 20:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 17]]);
	}));

	return function transferQAQuestionsIdToRedis(_x) {
		return _ref.apply(this, arguments);
	};
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var config = require('../config.js');
var Q = require('q');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

var forum_table = 'ew1.vt_forum_qrys';

transferQAQuestionsIdToRedis();