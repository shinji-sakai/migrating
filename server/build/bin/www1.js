#!/usr/bin/env node
'use strict';

/**
 * Module dependencies.
 */

var log4js = require('log4js');
log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file('/tmp/nodejs/examwarrior.log'), 'ew');

var app = require('../app');
var configPath = require('../configPath.js');
var debug = require('debug')('app:server');
var http = require('http');
var logger = require(configPath.lib.log_to_file);

var https = require('https');
var fs = require('fs');

var port = normalizePort(process.env.PORT || '8085');
app.set('port', port);

var httpserver = http.createServer(app);
httpserver.listen(port);

process.env.MODE = "DEV";

if (process.env.MODE === "DEV") {
	console.log("Starting Dev server on port " + port);
	// var pem = require('pem');
	// pem.createCertificate({days:1, selfSigned:true}, function(err, keys){
	// https.createServer({key: keys.serviceKey, cert: keys.certificate}, app).listen(443);
	// });
} else {
	var privateKey = fs.readFileSync('/opt/certs/examwarrior.key').toString();
	var certificate = fs.readFileSync('/opt/certs/4f0da29986d18188.crt').toString();
	var ca = fs.readFileSync('/opt/certs/gd_bundle-g2-g1.crt').toString();
	var httpsserver = https.createServer({ key: privateKey, cert: certificate, ca: ca }, app);
	httpsserver.listen(443);
}

var webSocket = require(configPath.api.websocket);
if (process.env.MODE === "DEV") {
	webSocket.create(httpserver);
	global.ewWebSocket = webSocket.wsServer;
} else {
	webSocket.create(httpsserver);
	global.ewWebSocket = webSocket.wsServer;
}

//Start WebSocet and listen to new requests

// var kalfka_rethinkdb = require('../kafka1.js');


// httpserver.on('error', onError);
// httpserver.on('listening', onListening);

process.on('uncaughtException', function (err) {
	logger.debug(err);
	console.log(err);
});

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
	var port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
	var addr = server.address();
	var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
	debug('Listening on ' + bind);
}