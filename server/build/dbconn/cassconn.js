'use strict';

var cassandra = require('cassandra-driver');
var config = require('../config.js');
var singleton = null;
var client = new cassandra.Client({
    contactPoints: [config.CASS_DB_SERVER_URL]
});
// 139.59.1.89
// '52.76.154.91',
// ,'139.59.2.237'
if (!singleton) {
    singleton = client;
}

module.exports = singleton;