'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (!process.env.NO_ORACLE) {
    var getConnection = function () {
        var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
            var connection;
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            _context.prev = 0;
                            _context.next = 3;
                            return oracledb.getConnection({ user: "scott", password: "jeevan", connectString: "139.59.41.144/pdbedw" });

                        case 3:
                            connection = _context.sent;
                            return _context.abrupt('return', connection);

                        case 7:
                            _context.prev = 7;
                            _context.t0 = _context['catch'](0);

                            logger.error("oracle connection error...", _context.t0);
                            throw _context.t0;

                        case 11:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, this, [[0, 7]]);
        }));

        return function getConnection() {
            return _ref.apply(this, arguments);
        };
    }();

    var releaseConnection = function () {
        var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(conn) {
            return _regenerator2.default.wrap(function _callee2$(_context2) {
                while (1) {
                    switch (_context2.prev = _context2.next) {
                        case 0:
                            _context2.prev = 0;
                            _context2.next = 3;
                            return conn.close();

                        case 3:
                            return _context2.abrupt('return');

                        case 6:
                            _context2.prev = 6;
                            _context2.t0 = _context2['catch'](0);

                            logger.error("oracle releaseConnection error...", _context2.t0);
                            throw _context2.t0;

                        case 10:
                        case 'end':
                            return _context2.stop();
                    }
                }
            }, _callee2, this, [[0, 6]]);
        }));

        return function releaseConnection(_x) {
            return _ref2.apply(this, arguments);
        };
    }();

    var oracledb = require('oracledb');
    var configPath = require('../configPath.js');
    var logger = require(configPath.lib.log_to_file);

    module.exports = {
        getConnection: getConnection,
        releaseConnection: releaseConnection
    };
} else {
    module.exports = {
        getConnection: function getConnection() {},
        releaseConnection: function releaseConnection() {}
    };
}