'use strict';

var bundle = require('./bundle.route.js');

module.exports = function (app) {
	app.use('/bundle', bundle);
};