'use strict';

//Not Used
//Deprecated

var express = require('express');
// Initialize the Router
var router = express.Router();
var jwt = require('jsonwebtoken');
var moment = require('moment');
var requestIp = require('request-ip');
var fs = require('fs');

var config = require('../config.js');
var configPath = require('../configPath.js');

var datafile = 'data/books.json';

function getBookData() {
    var data = fs.readFileSync(datafile, 'utf8');
    return JSON.parse(data);
}

// Setup the Route

router.use(function (req, res, next) {

    if (!req.get("Authorization")) {
        res.status(401).send({ err: "Auth failed" });
    } else {
        //get token
        var token = req.get("Authorization").split(' ')[1];
        //verify it
        jwt.verify(token, config.TOKEN_SECRET, function (err, decoded) {
            if (err) {
                //if error
                res.status(401).send(err);
            } else {
                //if sucess , proceed further
                next();
            }
        });
    }
});

router.get('/', function (req, res) {

    var data = getBookData();
    res.send(data);
});
router.post('/', function (req, res) {

    var payload = {
        iss: req.hostname,
        sub: req.body.email,
        iat: moment().format('LLL'),
        exp: moment().add(10, 'days').calendar()
    };

    var token = jwt.sign(payload, config.TOKEN_SECRET, { expiresIn: "2 days" });

    res.status(200).json({ token: token });

    /*
      var token = encodejwt(payload,'jeee');
      mongoapi.validateLoginDetails()
     .then(function(doc){
     console.log("validate login details...............");
     console.log(doc[0].email);
      if(doc[0].password=req.body.password){
     res.status(200).json({ success: 'Login Success!!' });
     }
      },function(err){
     console.log(err);
     });
      */
});
router.post('/abc', function (req, res) {
    console.log("in api");res.send({ data: "" });
});
router.post('/xyz', function (req, res) {
    res.send({ data: "" });
});
router.get('/data', function (req, res) {
    console.log("in api");res.send(200, { data: "" });
});

// Expose the module
module.exports = router;