'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');

var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');

var configPath = require('../configPath.js');
var utility = require(configPath.lib.utility);
var contactApi = require(configPath.api.contact);

router.post('/saveContactMsg', multipartyMiddleware, function (req, res, next) {
    contactApi.saveContactMsg(req.body).then(function (d) {
        res.status(200).send();
    }).catch(function (err) {
        next(err);
    });
});

router.post('/getContactUsDetails', function (req, res, next) {
    contactApi.getContactUsDetails(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        next(err);
    });
});

module.exports = router;