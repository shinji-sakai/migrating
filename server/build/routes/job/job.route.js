'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');
var Q = require('q');

var configPath = require('../../configPath.js');
var api = require(configPath.api.job.job);
var utility = require(configPath.lib.utility);

router.post('/applyForJob', function (req, res) {
    api.applyForJob(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

module.exports = router;