'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();
var path = require('path');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');
var configPath = require('../../configPath.js');
var PostApi = require(configPath.api.dashboard.post);
var utility = require(configPath.lib.utility);
var passporAuth = require(configPath.lib.passport);

router.post('/save', multipartyMiddleware, passporAuth.authenticate(), function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res, next) {
        var arr, photoUrls, videoUrls, videoArr, key, file, dotPos, onlyName, ext, targetFileName, targetPath, data;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        arr = [];
                        photoUrls = [];
                        videoUrls = [];
                        videoArr = [];

                        for (key in req.files.photoAttachments) {
                            file = req.files.photoAttachments[key];
                            dotPos = file.originalFilename.lastIndexOf(".");
                            onlyName = file.originalFilename.substr(0, dotPos);
                            ext = file.originalFilename.substr(dotPos + 1);
                            targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
                            targetPath = path.resolve(configPath.common_paths.postattachment_relative);

                            arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
                            photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
                        }

                        for (key in req.files.videoAttachments) {
                            file = req.files.videoAttachments[key];
                            dotPos = file.originalFilename.lastIndexOf(".");
                            onlyName = file.originalFilename.substr(0, dotPos);
                            ext = file.originalFilename.substr(dotPos + 1);
                            targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
                            targetPath = path.resolve(configPath.common_paths.postattachment_relative);

                            videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
                            videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
                        }

                        data = req.body;

                        data.pst_img = photoUrls;
                        data.pst_vid = videoUrls;

                        // arr.push();
                        arr.concat(videoArr);

                        _context.next = 12;
                        return Q.all(arr);

                    case 12:

                        PostApi.save(data).then(function (d) {
                            res.status(200).send(d);
                        }).catch(function (err) {
                            return next(err);
                        });

                    case 13:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
    };
}());

router.post('/getFeed', passporAuth.authenticate(), function (req, res, next) {
    PostApi.getFeed(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getPosts', passporAuth.authenticate(), function (req, res, next) {
    PostApi.getUserPosts(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// 
router.post('/getUserPublicPosts', passporAuth.authenticate(), function (req, res, next) {
    PostApi.getUserPublicPosts(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getUserPostsForFriends', passporAuth.authenticate(), function (req, res, next) {
    PostApi.getUserPostsForFriends(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/edit', multipartyMiddleware, passporAuth.authenticate(), function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req, res, next) {
        var incomingData, arr, photoUrls, videoUrls, videoArr, key, file, dotPos, onlyName, ext, targetFileName, targetPath, data;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        incomingData = req.body;
                        arr = [];
                        photoUrls = incomingData.alreadyAddedPhotos || [];
                        videoUrls = incomingData.alreadyAddedVideos || [];
                        videoArr = [];


                        for (key in req.files.photoAttachments) {
                            file = req.files.photoAttachments[key];
                            dotPos = file.originalFilename.lastIndexOf(".");
                            onlyName = file.originalFilename.substr(0, dotPos);
                            ext = file.originalFilename.substr(dotPos + 1);
                            targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
                            targetPath = path.resolve(configPath.common_paths.postattachment_relative);

                            arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
                            photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
                        }

                        for (key in req.files.videoAttachments) {
                            file = req.files.videoAttachments[key];
                            dotPos = file.originalFilename.lastIndexOf(".");
                            onlyName = file.originalFilename.substr(0, dotPos);
                            ext = file.originalFilename.substr(dotPos + 1);
                            targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
                            targetPath = path.resolve(configPath.common_paths.postattachment_relative);

                            videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
                            videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
                        }

                        data = req.body;

                        data.pst_img = photoUrls;
                        data.pst_vid = videoUrls;
                        // arr.push();
                        arr.concat(videoArr);
                        _context2.next = 13;
                        return Q.all(arr);

                    case 13:

                        PostApi.edit(data).then(function (d) {
                            res.status(200).send(d);
                        }).catch(function (err) {
                            console.log(err);
                            res.status(500).send({ 'err': err });
                        });

                    case 14:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
    };
}());
router.post('/delete', passporAuth.authenticate(), function (req, res, next) {
    PostApi.delete(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// getLikeByUser
router.post('/getLikeByUser', passporAuth.authenticate(), function (req, res, next) {
    PostApi.getLikeByUser(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// updateLikeByUser
router.post('/updateLikeByUser', passporAuth.authenticate(), function (req, res, next) {
    PostApi.updateLikeByUser(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// getLikeStatsForPosts
router.post('/getLikeStatsForPosts', passporAuth.authenticate(), function (req, res, next) {
    PostApi.getLikeStatsForPosts(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// updateLikeStatsForPosts
router.post('/updateLikeStatsForPosts', passporAuth.authenticate(), function (req, res, next) {
    PostApi.updateLikeStatsForPosts(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/saveComplain', passporAuth.authenticate(), function (req, res, next) {
    PostApi.saveComplain(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

module.exports = router;