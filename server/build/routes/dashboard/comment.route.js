'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');
var configPath = require('../../configPath.js');
var CommentApi = require(configPath.api.dashboard.comment);
var utility = require(configPath.lib.utility);
var passporAuth = require(configPath.lib.passport);

// multipartyMiddleware
router.post('/save', multipartyMiddleware, passporAuth.authenticate(), function (req, res, next) {

    var arr = [];
    var photoUrls = [];
    var videoUrls = [];
    var videoArr = [];
    if (req.files) {
        for (var key in req.files.cmnt_img) {
            var file = req.files.cmnt_img[key];
            var dotPos = file.originalFilename.lastIndexOf(".");
            var onlyName = file.originalFilename.substr(0, dotPos);
            var ext = file.originalFilename.substr(dotPos + 1);
            var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
            var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
            arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
            photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
        }

        for (var key in req.files.cmnt_vid) {
            var file = req.files.cmnt_vid[key];
            var dotPos = file.originalFilename.lastIndexOf(".");
            var onlyName = file.originalFilename.substr(0, dotPos);
            var ext = file.originalFilename.substr(dotPos + 1);
            var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
            var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
            videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
            videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
        }
    }

    var data = req.body;
    data.cmnt_img = photoUrls;
    data.cmnt_vid = videoUrls;
    data.usr_id = req.user.usr_id;
    arr.concat(videoArr);
    arr.push(CommentApi.save(req.body));
    Q.all(arr).then(function (d) {
        res.status(200).send(d[0]);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/get', function (req, res, next) {
    CommentApi.get(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/edit', multipartyMiddleware, passporAuth.authenticate(), function (req, res, next) {
    var incomingData = req.body;

    var arr = [];
    var photoUrls = incomingData.alreadyAddedPhotos || [];
    var videoUrls = incomingData.alreadyAddedVideos || [];
    var videoArr = [];
    for (var key in req.files.cmnt_img) {
        var file = req.files.cmnt_img[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0, dotPos);
        var ext = file.originalFilename.substr(dotPos + 1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }

    for (var key in req.files.cmnt_vid) {
        var file = req.files.cmnt_vid[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0, dotPos);
        var ext = file.originalFilename.substr(dotPos + 1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }

    var data = req.body;
    data.cmnt_img = photoUrls;
    data.cmnt_vid = videoUrls;
    arr.concat(videoArr);
    arr.push(CommentApi.edit(data));
    Q.all(arr).then(function (d) {
        res.status(200).send({});
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/delete', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    CommentApi.delete(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/like_dislike', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    CommentApi.likeDislikeComment(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});
// getLikeDislikeComments
router.post('/getLikeDislikeComments', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    CommentApi.getLikeDislikeComments(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// getCommentUsers
router.post('/getCommentUsers', function (req, res, next) {
    CommentApi.getCommentUsers(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// updateCommentLikeDislikeCount
router.post('/updateCommentLikeDislikeCount', passporAuth.authenticate(), function (req, res, next) {
    CommentApi.updateCommentLikeDislikeCount(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});
// getCommentLikeDislikeCount
router.post('/getCommentLikeDislikeCount', function (req, res, next) {
    CommentApi.getCommentLikeDislikeCount(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

module.exports = router;