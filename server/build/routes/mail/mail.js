'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var mail = require(configPath.api.mail);
var registerTemplate = require('../../api/mail/registerTemplate.js');
var forgotpasswordTemplate = require('../../api/mail/forgotpasswordTemplate.js');
var resendotpTemplate = require('../../api/mail/resendotpTemplate.js');
var enrollbtnTemplate = require('../../api/mail/enrollBtnTemplate.js');
var paySecurelyBtnTemplate = require('../../api/mail/paySecurelyBtnTemplate.js');
var subscribeAPI = require(configPath.api.subscribe.subscribe);

router.post('/send', function (req, res) {
	mail.sendMail(req.body).then(function success(data) {
		res.status(200).send(data);
	}).catch(function (err) {
		console.error(err);
		res.status(500).send(err);
	});
});

router.post('/registerTemplate', function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
		var d, html, r;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						_context.next = 3;
						return registerTemplate(req.body);

					case 3:
						d = _context.sent;
						html = d.html;

						req.body.text = html;
						_context.next = 8;
						return mail.sendMail(req.body);

					case 8:
						r = _context.sent;

						res.status(200).send(html);
						_context.next = 16;
						break;

					case 12:
						_context.prev = 12;
						_context.t0 = _context['catch'](0);

						console.error(_context.t0);
						res.status(500).send(_context.t0);

					case 16:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 12]]);
	}));

	return function (_x, _x2) {
		return _ref.apply(this, arguments);
	};
}());

router.post('/forgotpasswordTemplate', function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req, res) {
		var d, html, r;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.prev = 0;
						_context2.next = 3;
						return forgotpasswordTemplate(req.body);

					case 3:
						d = _context2.sent;
						html = d.html;

						req.body.text = html;
						_context2.next = 8;
						return mail.sendMail(req.body);

					case 8:
						r = _context2.sent;

						res.status(200).send(html);
						_context2.next = 16;
						break;

					case 12:
						_context2.prev = 12;
						_context2.t0 = _context2['catch'](0);

						console.error(_context2.t0);
						res.status(500).send(_context2.t0);

					case 16:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this, [[0, 12]]);
	}));

	return function (_x3, _x4) {
		return _ref2.apply(this, arguments);
	};
}());

router.post('/resendotpTemplate', function () {
	var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(req, res) {
		var d, html, r;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.prev = 0;
						_context3.next = 3;
						return resendotpTemplate(req.body);

					case 3:
						d = _context3.sent;
						html = d.html;

						req.body.text = html;
						_context3.next = 8;
						return mail.sendMail(req.body);

					case 8:
						r = _context3.sent;

						res.status(200).send(html);
						_context3.next = 16;
						break;

					case 12:
						_context3.prev = 12;
						_context3.t0 = _context3['catch'](0);

						console.error(_context3.t0);
						res.status(500).send(_context3.t0);

					case 16:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, this, [[0, 12]]);
	}));

	return function (_x5, _x6) {
		return _ref3.apply(this, arguments);
	};
}());

router.post('/sendBulkEmails', function () {
	var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(req, res) {
		var r;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.prev = 0;
						_context4.next = 3;
						return mail.sendBulkEmails(req.body);

					case 3:
						r = _context4.sent;

						res.status(200).send(r);
						_context4.next = 11;
						break;

					case 7:
						_context4.prev = 7;
						_context4.t0 = _context4['catch'](0);

						console.error(_context4.t0);
						res.status(500).send(_context4.t0);

					case 11:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, this, [[0, 7]]);
	}));

	return function (_x7, _x8) {
		return _ref4.apply(this, arguments);
	};
}());

router.post('/subscribeUser', function () {
	var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(req, res) {
		var r;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.prev = 0;
						_context5.next = 3;
						return subscribeAPI.subscribeUser(req.body);

					case 3:
						r = _context5.sent;

						res.status(200).send(r);
						_context5.next = 11;
						break;

					case 7:
						_context5.prev = 7;
						_context5.t0 = _context5['catch'](0);

						console.error(_context5.t0);
						res.status(500).send(_context5.t0);

					case 11:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, this, [[0, 7]]);
	}));

	return function (_x9, _x10) {
		return _ref5.apply(this, arguments);
	};
}());

router.post('/unsubscribeUser', function () {
	var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(req, res) {
		var r;
		return _regenerator2.default.wrap(function _callee6$(_context6) {
			while (1) {
				switch (_context6.prev = _context6.next) {
					case 0:
						_context6.prev = 0;
						_context6.next = 3;
						return subscribeAPI.unsubscribeUser(req.body);

					case 3:
						r = _context6.sent;

						res.status(200).send(r);
						_context6.next = 11;
						break;

					case 7:
						_context6.prev = 7;
						_context6.t0 = _context6['catch'](0);

						console.error(_context6.t0);
						res.status(500).send(_context6.t0);

					case 11:
					case 'end':
						return _context6.stop();
				}
			}
		}, _callee6, this, [[0, 7]]);
	}));

	return function (_x11, _x12) {
		return _ref6.apply(this, arguments);
	};
}());

router.post('/getAllSubscribers', function () {
	var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(req, res) {
		var r;
		return _regenerator2.default.wrap(function _callee7$(_context7) {
			while (1) {
				switch (_context7.prev = _context7.next) {
					case 0:
						_context7.prev = 0;
						_context7.next = 3;
						return subscribeAPI.getAllSubscribers(req.body);

					case 3:
						r = _context7.sent;

						res.status(200).send(r);
						_context7.next = 11;
						break;

					case 7:
						_context7.prev = 7;
						_context7.t0 = _context7['catch'](0);

						console.error(_context7.t0);
						res.status(500).send(_context7.t0);

					case 11:
					case 'end':
						return _context7.stop();
				}
			}
		}, _callee7, this, [[0, 7]]);
	}));

	return function (_x13, _x14) {
		return _ref7.apply(this, arguments);
	};
}());

router.post('/sendMailToSupportForEnrollBtn', function () {
	var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(req, res) {
		var d, html, r;
		return _regenerator2.default.wrap(function _callee8$(_context8) {
			while (1) {
				switch (_context8.prev = _context8.next) {
					case 0:
						_context8.prev = 0;
						_context8.next = 3;
						return enrollbtnTemplate(req.body);

					case 3:
						d = _context8.sent;
						html = d.html;

						req.body.text = html;
						req.body.to = "support@examwarrior.com";
						_context8.next = 9;
						return mail.sendMail(req.body);

					case 9:
						r = _context8.sent;

						res.status(200).send(html);
						_context8.next = 17;
						break;

					case 13:
						_context8.prev = 13;
						_context8.t0 = _context8['catch'](0);

						console.error(_context8.t0);
						res.status(500).send(_context8.t0);

					case 17:
					case 'end':
						return _context8.stop();
				}
			}
		}, _callee8, this, [[0, 13]]);
	}));

	return function (_x15, _x16) {
		return _ref8.apply(this, arguments);
	};
}());

router.post('/sendMailToSupportForPaySecurelyBtn', function () {
	var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(req, res) {
		var d, html;
		return _regenerator2.default.wrap(function _callee9$(_context9) {
			while (1) {
				switch (_context9.prev = _context9.next) {
					case 0:
						_context9.prev = 0;
						_context9.next = 3;
						return paySecurelyBtnTemplate(req.body);

					case 3:
						d = _context9.sent;
						html = d.html;

						req.body.text = html;
						req.body.to = "support@examwarrior.com";
						mail.sendMail(req.body);
						res.status(200).send(html);
						_context9.next = 15;
						break;

					case 11:
						_context9.prev = 11;
						_context9.t0 = _context9['catch'](0);

						console.error(_context9.t0);
						res.status(500).send(_context9.t0);

					case 15:
					case 'end':
						return _context9.stop();
				}
			}
		}, _callee9, this, [[0, 11]]);
	}));

	return function (_x17, _x18) {
		return _ref9.apply(this, arguments);
	};
}());

module.exports = router;