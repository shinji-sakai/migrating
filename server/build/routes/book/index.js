'use strict';

var highlights = require('./highlights.route.js');

module.exports = function (app) {
	app.use('/book/highlights', highlights);
};