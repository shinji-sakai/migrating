'use strict';

var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var highlightApi = require(configPath.api.book.highlights);

router.post('/saveBookHighlight', function (req, res) {
    highlightApi.saveBookHighlight(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});
router.post('/removeBookHighlight', function (req, res) {
    highlightApi.removeBookHighlight(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});
router.post('/getBookItemHighlights', function (req, res) {
    highlightApi.getBookItemHighlights(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

module.exports = router;