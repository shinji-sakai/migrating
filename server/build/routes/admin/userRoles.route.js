'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.userRoles);

router.post('/addUserRole', function (req, res) {
    api.addUserRole(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getAllUserRoles', function (req, res) {
    api.getAllUserRoles(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/deleteUserRole', function (req, res) {
    api.deleteUserRole(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getUserAdminRoleFormAccessList', function (req, res) {
    api.getUserAdminRoleFormAccessList(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

module.exports = router;