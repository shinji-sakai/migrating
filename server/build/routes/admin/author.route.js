'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var authorApi = require(configPath.api.admin.author);

router.post('/getAll', function (req, res) {
    authorApi.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/update', function (req, res) {
    authorApi.update(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/remove', function (req, res) {
    authorApi.remove(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/fetchAuthorUsingLimit', function (req, res) {
    authorApi.fetchAuthorUsingLimit(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/AuthorsIdIsExits', function (req, res) {
    authorApi.AuthorsIdIsExits(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        //debug(err);
        res.status(500).json({ err: err.message });
    });
});

module.exports = router;