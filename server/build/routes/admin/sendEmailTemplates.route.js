'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.sendEmailTemplates);

router.post('/addSendEmailTemplate', function (req, res) {
    api.addSendEmailTemplate(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getAllSendEmailTemplates', function (req, res) {
    api.getAllSendEmailTemplates(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/deleteSendEmailTemplate', function (req, res) {
    api.deleteSendEmailTemplate(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/sendEmailTemplatesToUsers', function (req, res) {
    api.sendEmailTemplatesToUsers(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/sendNotificationOfEmail', function (req, res) {
    api.sendNotificationOfEmail(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

// 

module.exports = router;