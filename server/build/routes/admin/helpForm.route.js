'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.helpForm);

router.post('/getAllFormsHelp', function (req, res) {
    api.getAllFormsHelp(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/getFormHelp', function (req, res) {
    api.getFormHelp(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/updateFormHelp', function (req, res) {
    api.updateFormHelp(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/removeFormHelp', function (req, res) {
    api.removeFormHelp(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

module.exports = router;