'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var subjectApi = require(configPath.api.admin.subject);

router.post('/getAll', function (req, res) {
    subjectApi.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/update', function (req, res) {
    subjectApi.update(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/fetchSubjectMasterUsingLimit', function (req, res) {
    subjectApi.fetchSubjectMasterUsingLimit(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/SubjectIdIsExits', function (req, res) {
    subjectApi.SubjectIdIsExits(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/remove', function (req, res) {
    subjectApi.remove(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

module.exports = router;