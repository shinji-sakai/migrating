'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.sendNotification);

router.post('/addSendNotification', function (req, res) {
    api.addSendNotification(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getAllSendNotifications', function (req, res) {
    api.getAllSendNotifications(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/deleteSendNotification', function (req, res) {
    api.deleteSendNotification(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getSendNotificationByUserId', function (req, res) {
    api.getSendNotificationByUserId(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/markSendNotificationAsRead', function (req, res) {
    api.markSendNotificationAsRead(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/markAllSendNotificationAsRead', function (req, res) {
    api.markAllSendNotificationAsRead(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getSendNotificationCount', function (req, res) {
    api.getSendNotificationCount(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

// 

// 

module.exports = router;