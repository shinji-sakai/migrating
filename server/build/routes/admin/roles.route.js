'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.roles);

router.post('/addRole', function (req, res) {
    api.addRole(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getAllRoles', function (req, res) {
    api.getAllRoles(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/deleteRole', function (req, res) {
    api.deleteRole(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

// deleteEmailTemplate

module.exports = router;