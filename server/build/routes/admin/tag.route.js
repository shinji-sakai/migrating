'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var tagApi = require(configPath.api.admin.tag);

router.post('/getAll', function (req, res) {
    tagApi.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/update', function (req, res) {
    tagApi.update(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/remove', function (req, res) {
    tagApi.remove(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/fetchTagsUsingLimit', function (req, res) {
    tagApi.fetchTagsUsingLimit(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        // debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/TagsIdIsExits', function (req, res) {
    tagApi.TagsIdIsExits(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        // debug(err);
        res.status(500).json({ err: err.message });
    });
});
module.exports = router;