'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var moduleItemsApi = require(configPath.api.admin.moduleItems);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var path = require('path');
var utility = require(configPath.lib.utility);

router.post('/uploadUserMaterial', multipartyMiddleware, function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var usr_id, file, parsedFileObj, targetFileName, targetPath, uploadRes, res_data;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;

                        if (req.files.file) {
                            _context.next = 4;
                            break;
                        }

                        res.status(500).json({ err: 'File is required' });
                        return _context.abrupt('return');

                    case 4:
                        usr_id = req.body.usr_id;
                        file = req.files && req.files.file || {};
                        parsedFileObj = path.parse(file.path);
                        targetFileName = path.parse(file.originalFilename).name + "_" + usr_id + "_" + new Date().getTime() + parsedFileObj.ext;

                        if (req.body.fileName) {
                            targetFileName = req.body.fileName + "_" + usr_id + "_" + new Date().getTime() + parsedFileObj.ext;
                        }
                        targetPath = path.normalize(configPath.common_paths.usr_material_file_relative + "/" + targetFileName);
                        _context.next = 12;
                        return utility.copyFile(file.path, targetPath);

                    case 12:
                        uploadRes = _context.sent;


                        req.body["upload_path"] = targetPath;
                        req.body["upload_nm"] = targetFileName;

                        _context.next = 17;
                        return moduleItemsApi.saveUserBatchEnrollUploads(req.body);

                    case 17:
                        res_data = _context.sent;


                        res.status(200).send((0, _extends3.default)({}, res_data, {
                            Key: targetFileName,
                            path: targetPath
                        }));
                        _context.next = 24;
                        break;

                    case 21:
                        _context.prev = 21;
                        _context.t0 = _context['catch'](0);

                        res.status(500).json({ err: _context.t0.message });

                    case 24:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 21]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

router.post('/getUserBatchEnrollUploads', function (req, res) {
    moduleItemsApi.getUserBatchEnrollUploads(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/getAll', function (req, res) {
    moduleItemsApi.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/update', function (req, res) {
    moduleItemsApi.update(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/getCourseItems', function (req, res, next) {
    moduleItemsApi.getCourseItems(req.body).then(function (doc) {
        if (doc && doc.length <= 0) {
            console.log(".....getCourseItems", doc);
            next({ page: '404' });
        } else {
            res.status(200).send(doc);
        }
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/getFullCourseDetails', function (req, res, next) {
    moduleItemsApi.getFullCourseDetails(req).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        next(err);
    });
});

router.post('/getModuleItems', function (req, res) {
    moduleItemsApi.getModuleItems(req.body).then(function (doc) {
        if (doc.length > 0) {
            res.status(200).send(doc[0].moduleDetail[0].moduleItems);
        } else {
            res.status(200).send(doc);
        }
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/remove', function (req, res) {
    moduleItemsApi.remove(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(501).json({ err: err.message });
    });
});

module.exports = router;