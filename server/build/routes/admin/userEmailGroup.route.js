'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.userEmailGroup);

router.post('/addUserEmailGroup', function (req, res) {
    api.addUserEmailGroup(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getAllUserEmailGroups', function (req, res) {
    api.getAllUserEmailGroups(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/deleteUserEmailGroup', function (req, res) {
    api.deleteUserEmailGroup(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.error(err);
        res.status(500).json({ err: err.message });
    });
});

// deleteEmailTemplate

module.exports = router;