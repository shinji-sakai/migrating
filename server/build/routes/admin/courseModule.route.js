'use strict';

var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var courseModuleApi = require(configPath.api.admin.courseModule);

//Course Module Routes
router.post('/getAll', function (req, res) {
    courseModuleApi.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/add', function (req, res) {
    courseModuleApi.add(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/delete', function (req, res) {
    courseModuleApi.delete(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});

module.exports = router;