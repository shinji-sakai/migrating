'use strict';

var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var courseApi = require(configPath.api.admin.course);
var debug = require('debug')('app:admin:route:course');

//Course Routes
router.post('/getAll', function (req, res) {
    courseApi.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/add', function (req, res) {
    courseApi.add(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/delete', function (req, res) {
    courseApi.delete(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});

module.exports = router;