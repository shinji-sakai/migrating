'use strict';

var awsS3Routes = require('./s3.route.js');

module.exports = function (app) {
	app.use('/aws/s3', awsS3Routes);
};