'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.aws.s3);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({});
var debug = require('debug')("app:routes:aws:s3.route.js");
var path = require('path');
var uuid = require('uuid');

router.post('/createBucket', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var apiRes;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        _context.next = 3;
                        return api.createBucket(req.body);

                    case 3:
                        apiRes = _context.sent;

                        res.status(200).send(apiRes);
                        _context.next = 10;
                        break;

                    case 7:
                        _context.prev = 7;
                        _context.t0 = _context['catch'](0);

                        res.status(500).json({ err: _context.t0.message });

                    case 10:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 7]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

router.get('/getBuckets', function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req, res) {
        var apiRes;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        _context2.next = 3;
                        return api.getBuckets();

                    case 3:
                        apiRes = _context2.sent;

                        res.status(200).send(apiRes);
                        _context2.next = 10;
                        break;

                    case 7:
                        _context2.prev = 7;
                        _context2.t0 = _context2['catch'](0);

                        res.status(500).json({ err: _context2.t0.message });

                    case 10:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 7]]);
    }));

    return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
    };
}());

router.post('/uploadFile', multipartyMiddleware, function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(req, res) {
        var file, parsedFileObj, targetFileName, apiRes;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;

                        if (req.files.file) {
                            _context3.next = 4;
                            break;
                        }

                        res.status(500).json({ err: 'File is required' });
                        return _context3.abrupt('return');

                    case 4:
                        file = req.files && req.files['file'] || {};
                        parsedFileObj = path.parse(file.path);
                        targetFileName = path.parse(file.originalFilename).name + parsedFileObj.ext;

                        if (req.body.fileName) {
                            targetFileName = req.body.fileName + parsedFileObj.ext;
                        }
                        _context3.next = 10;
                        return api.uploadFile(req.body.type, targetFileName, file.path, req.body.folderPath);

                    case 10:
                        apiRes = _context3.sent;

                        res.status(200).send(apiRes);
                        _context3.next = 17;
                        break;

                    case 14:
                        _context3.prev = 14;
                        _context3.t0 = _context3['catch'](0);

                        res.status(500).json({ err: _context3.t0.message });

                    case 17:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 14]]);
    }));

    return function (_x5, _x6) {
        return _ref3.apply(this, arguments);
    };
}());

router.post('/getSignedUrl', function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(req, res) {
        var apiRes;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        _context4.next = 3;
                        return api.getSignedUrl(req.body.type, req.body.key);

                    case 3:
                        apiRes = _context4.sent;

                        res.status(200).send(apiRes);
                        _context4.next = 10;
                        break;

                    case 7:
                        _context4.prev = 7;
                        _context4.t0 = _context4['catch'](0);

                        res.status(500).json({ err: _context4.t0.message });

                    case 10:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 7]]);
    }));

    return function (_x7, _x8) {
        return _ref4.apply(this, arguments);
    };
}());

module.exports = router;