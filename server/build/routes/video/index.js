'use strict';

var entity = require('./videoentity.route.js');
var note = require('./videonote.route.js');
var rating = require('./videorating.route.js');
var views = require('./videoviews.route.js');
var votes = require('./videovotes.route.js');

module.exports = function (app) {
	app.use('/videoentity', entity);
	app.use('/videonotes', note);
	app.use('/videoviews', views);
	app.use('/videorating', rating);
	app.use('/videovotes', votes);
};