'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.video.videorating);

router.post('/saveVideoRating', function (req, res) {
    api.saveVideoRating(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/updateVideoRating', function (req, res) {
    api.updateVideoRating(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/getVideoRatingForUser', function (req, res) {
    api.getVideoRatingForUser(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

module.exports = router;