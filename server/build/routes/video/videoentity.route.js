'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var config = require('../../config.js');
var VideoEntityApi = require(configPath.api.video.videoentity);
var videoSourceConfig = require(configPath.config.videoSourceConfig);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var path = require('path');
var utility = require(configPath.lib.utility);
var logger = require(configPath.lib.log_to_file);
var Promise = require("bluebird");
var thumbgen = Promise.promisify(require('thumbnails-webvtt'));
var awsS3API = require(configPath.api.aws.s3);
var tinify = require("tinify");
tinify.key = config.TINYPNG_API_KEY;
var exec = Promise.promisify(require('child_process').exec);

router.post('/uploadVideoEntity', multipartyMiddleware, function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var destTarget, file, parsedFileObj, targetFileName, targetPath, videoThumbsAbsolutePath, videoThumbsRelativePath, duration, videoDurationOp, d, thumbPath, source;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;

                        if (req.files.file) {
                            _context.next = 4;
                            break;
                        }

                        res.status(500).json({
                            err: 'File is required'
                        });
                        return _context.abrupt('return');

                    case 4:
                        destTarget = req.body.target || "harddrive";
                        file = req.files && req.files.file || {};
                        parsedFileObj = path.parse(file.path);
                        targetFileName = path.parse(file.originalFilename).name + parsedFileObj.ext;

                        if (req.body.fileName) {
                            targetFileName = req.body.fileName + parsedFileObj.ext;
                        }
                        targetPath = path.normalize(videoSourceConfig.videoRelativePath + "/" + targetFileName);

                        if (!(destTarget === "harddrive")) {
                            _context.next = 15;
                            break;
                        }

                        _context.next = 13;
                        return utility.copyFile(file.path, targetPath);

                    case 13:
                        _context.next = 17;
                        break;

                    case 15:
                        _context.next = 17;
                        return awsS3API.uploadFile(req.body.type, targetFileName, file.path, req.body.folderPath);

                    case 17:
                        videoThumbsAbsolutePath = path.normalize(videoSourceConfig.videoThumbnailAbsolutePath + "/" + targetFileName + ".vtt");
                        videoThumbsRelativePath = videoSourceConfig.videoThumbnailRelativePath + "/" + targetFileName + ".vtt";
                        duration = "";

                        if (!req.body.generateThumbs) {
                            _context.next = 36;
                            break;
                        }

                        _context.prev = 21;
                        _context.next = 24;
                        return exec('ffprobe -i ' + file.path + ' -show_format -v quiet | grep duration');

                    case 24:
                        videoDurationOp = _context.sent;

                        if (videoDurationOp && videoDurationOp[0]) {
                            d = Math.floor(parseFloat(videoDurationOp[0].split("=")[1] || 0));

                            logger.debug(d);
                            duration = utility.changeTimeFormat(d);
                        }

                        _context.next = 28;
                        return thumbgen(file.path, {
                            output: path.normalize(videoThumbsRelativePath),
                            size: {
                                width: 200,
                                height: 100
                            },
                            assetsDirectory: targetFileName,
                            secondsPerThumbnail: 1,
                            spritesheet: true
                        });

                    case 28:
                        thumbPath = path.normalize(videoSourceConfig.videoThumbnailRelativePath + "/" + targetFileName + "/thumbnails.png");
                        source = tinify.fromFile(thumbPath);

                        source.toFile(thumbPath);
                        _context.next = 36;
                        break;

                    case 33:
                        _context.prev = 33;
                        _context.t0 = _context['catch'](21);

                        logger.debug("Error in generating thumbnail of video...", _context.t0);

                    case 36:

                        res.status(200).send({
                            Key: targetFileName,
                            path: targetPath,
                            thumbnailPath: req.body.generateThumbs ? videoThumbsAbsolutePath : undefined,
                            duration: req.body.generateThumbs ? duration : undefined
                        });
                        _context.next = 42;
                        break;

                    case 39:
                        _context.prev = 39;
                        _context.t1 = _context['catch'](0);

                        res.status(500).json({
                            err: _context.t1.message
                        });

                    case 42:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 39], [21, 33]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

router.post('/update', function (req, res) {
    VideoEntityApi.updateVideoEntity(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(401).json({
            err: err.message
        });
    });
});

router.post('/remove', function (req, res) {
    VideoEntityApi.removeVideoEntity(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(401).json({
            err: err.message
        });
    });
});

router.post('/getAll', function (req, res) {
    VideoEntityApi.getAllVideoEntity(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(401).json({
            err: err.message
        });
    });
});

router.post('/isVideoFilenameExist', function (req, res) {
    VideoEntityApi.isVideoFilenameExist(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(401).json({
            err: err.message
        });
    });
});

router.post('/findVideoEntityByQuery', function (req, res) {
    try {
        VideoEntityApi.findVideoEntityByQuery(req.body).then(function (doc) {
            res.status(200).send(doc);
        }).catch(function (err) {
            console.log(err);
            res.status(401).json({
                err: err.message
            });
        });
    } catch (err) {
        console.log(err);
    }
});

router.post('/findVideoEntityById', function (req, res) {
    VideoEntityApi.findVideoEntityById(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(401).json({
            err: err.message
        });
    });
});

router.post('/findVideoEntitiesById', function (req, res) {
    VideoEntityApi.findVideoEntitiesById(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(401).json({
            err: err.message
        });
    });
});

// getRelatedVideosByVideoId
router.post('/getRelatedVideosByVideoId', function (req, res) {
    VideoEntityApi.getRelatedVideosByVideoId(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        res.status(500).json({
            err: err.message
        });
    });
});

// getRelatedTopicsVideoByVideoId
router.post('/getRelatedTopicsVideoByVideoId', function (req, res) {
    VideoEntityApi.getRelatedTopicsVideoByVideoId(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        res.status(500).json({
            err: err.message
        });
    });
});

// getRelatedCoursesByVideoId
router.post('/getRelatedCoursesByVideoId', function (req, res) {
    VideoEntityApi.getRelatedCoursesByVideoId(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        res.status(500).json({
            err: err.message
        });
    });
});

// updateVideoLastViewTime
router.post('/updateVideoLastViewTime', function (req, res) {
    VideoEntityApi.updateVideoLastViewTime(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        res.status(500).json({
            err: err.message
        });
    });
});

// getVideoLastViewTime
router.post('/getVideoLastViewTime', function (req, res) {
    VideoEntityApi.getVideoLastViewTime(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        res.status(500).json({
            err: err.message
        });
    });
});
module.exports = router;