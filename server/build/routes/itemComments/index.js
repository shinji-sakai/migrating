'use strict';

var itemComments = require('./itemComments.route.js');
var itemQueries = require('./itemQueries.route.js');

module.exports = function (app) {
	app.use('/itemComments/', itemComments);
	app.use('/itemQueries/', itemQueries);
};