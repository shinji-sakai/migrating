'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');

var configPath = require('../../configPath.js');
var CommentApi = require(configPath.api.itemComments.itemComments);
var utility = require(configPath.lib.utility);

// multipartyMiddleware
router.post('/save', multipartyMiddleware, function (req, res) {

    var arr = [];
    var photoUrls = [];
    var videoUrls = [];
    var videoArr = [];
    if (req.files) {
        for (var key in req.files.cmnt_img) {
            var file = req.files.cmnt_img[key];
            var dotPos = file.originalFilename.lastIndexOf(".");
            var onlyName = file.originalFilename.substr(0, dotPos);
            var ext = file.originalFilename.substr(dotPos + 1);
            var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
            var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
            arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
            photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
        }

        for (var key in req.files.cmnt_vid) {
            var file = req.files.cmnt_vid[key];
            var dotPos = file.originalFilename.lastIndexOf(".");
            var onlyName = file.originalFilename.substr(0, dotPos);
            var ext = file.originalFilename.substr(dotPos + 1);
            var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
            var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
            videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
            videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
        }
    }

    var data = req.body;
    data.cmnt_img = photoUrls;
    data.cmnt_vid = videoUrls;
    arr.concat(videoArr);
    arr.push(CommentApi.save(req.body));
    Q.all(arr).then(function (d) {
        res.status(200).send(d[0]);
    }).catch(function (err) {
        res.status(500).send({
            'err': err
        });
    });
});

router.post('/get', function (req, res) {
    CommentApi.get(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});
router.post('/edit', multipartyMiddleware, function (req, res) {
    console.log("body.....", req.body);
    console.log("files.....", req.files);

    var incomingData = req.body;

    var arr = [];
    var photoUrls = incomingData.alreadyAddedPhotos || [];
    var videoUrls = incomingData.alreadyAddedVideos || [];
    var videoArr = [];
    for (var key in req.files.cmnt_img) {
        var file = req.files.cmnt_img[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0, dotPos);
        var ext = file.originalFilename.substr(dotPos + 1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }

    for (var key in req.files.cmnt_vid) {
        var file = req.files.cmnt_vid[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0, dotPos);
        var ext = file.originalFilename.substr(dotPos + 1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }

    var data = req.body;
    data.cmnt_img = photoUrls;
    data.cmnt_vid = videoUrls;
    arr.concat(videoArr);
    arr.push(CommentApi.edit(data));
    Q.all(arr).then(function (d) {
        res.status(200).send({});
    }).catch(function (err) {
        res.status(500).send({
            'err': err
        });
    });
});
router.post('/delete', function (req, res) {
    CommentApi.delete(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/like_dislike', function (req, res) {
    CommentApi.likeDislikeComment(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});
// getLikeDislikeComments
router.post('/getLikeDislikeComments', function (req, res) {
    CommentApi.getLikeDislikeComments(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});
// getCommentUsers
router.post('/getCommentUsers', function (req, res) {
    CommentApi.getCommentUsers(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

// updateCommentLikeDislikeCount
router.post('/updateCommentLikeDislikeCount', function (req, res) {
    CommentApi.updateCommentLikeDislikeCount(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});
// getCommentLikeDislikeCount
router.post('/getCommentLikeDislikeCount', function (req, res) {
    CommentApi.getCommentLikeDislikeCount(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

module.exports = router;