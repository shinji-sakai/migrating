'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.batchCourse.sessions);

router.post('/getAll', function (req, res) {
    api.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/add', function (req, res) {
    api.add(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/delete', function (req, res) {
    api.delete(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});

router.post('/getByCourseId', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var doc;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        _context.next = 3;
                        return api.getByCourseId(req.body);

                    case 3:
                        doc = _context.sent;

                        res.status(200).send(doc);
                        _context.next = 11;
                        break;

                    case 7:
                        _context.prev = 7;
                        _context.t0 = _context['catch'](0);

                        debug(_context.t0);
                        res.status(500).json({ err: _context.t0.message });

                    case 11:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined, [[0, 7]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

module.exports = router;