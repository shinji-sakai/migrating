'use strict';

var express = require('express');
var router = express.Router();
var Q = require('q');

var configPath = require('../../configPath.js');
var MsgsApi = require(configPath.api.chat.msgs);

router.post('/find', function (req, res) {
    MsgsApi.findMsgs(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/clear', function (req, res) {
    MsgsApi.clearMsgs(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/addMsg', function (req, res) {
    MsgsApi.addMsg(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/editMsgs', function (req, res) {
    MsgsApi.editMsgs(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/deleteMsgs', function (req, res) {
    MsgsApi.deleteMsgs(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

module.exports = router;