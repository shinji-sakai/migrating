'use strict';

var express = require('express');
var router = express.Router();
var Q = require('q');

var configPath = require('../../configPath.js');
var FrndApi = require(configPath.api.chat.friends);

router.post('/find', function (req, res) {
    FrndApi.findFriends(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/findFriendWithId', function (req, res) {
    FrndApi.findFriendWithId(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

module.exports = router;