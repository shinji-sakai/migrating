'use strict';

var express = require('express');
var router = express.Router();
var Q = require('q');
var path = require('path');
var debug = require('debug')('app:routes:chat:grp');
var configPath = require('../../configPath.js');
var GrpApi = require(configPath.api.chat.grp);
var utility = require(configPath.lib.utility);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({});

router.post('/create', multipartyMiddleware, function (req, res) {

    var promise = Q.when("");
    var path_image = "";
    var relativePath = "";
    if (req.files) {
        var file = req.files.file;
        if (file) {
            // __dirname ,"../../../client/build/upload/"
            var targetPath = path.resolve(configPath.common_paths.grp_pic_relative);
            var targetFileName = file.name;
            promise = utility.copyFile(file.path, targetPath + "/" + targetFileName);
            path_image = configPath.common_paths.grp_pic_absolute + targetFileName;
            relativePath = targetPath + "/" + targetFileName;
        }
    }

    promise.then(function () {
        if (path_image.length > 0) {
            return utility.resizeImage(relativePath, relativePath, '50x50');
        } else {
            Q.when("");
        }
    }).then(function () {
        var d = req.body;
        d.pic50 = path_image;
        debug(d);
        GrpApi.createGroup(d).then(function (doc) {
            res.status(200).send(doc);
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        });
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/addMembers', function (req, res) {
    GrpApi.addMembers(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/findMembers', function (req, res) {
    GrpApi.findMembers(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

module.exports = router;