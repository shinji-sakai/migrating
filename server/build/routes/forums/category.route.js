'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');

var Q = require('q');

var configPath = require('../../configPath.js');
var CategoryApi = require(configPath.api.forums.category);
var utility = require(configPath.lib.utility);
var passporAuth = require(configPath.lib.passport);

router.post('/getAllCategories', function (req, res) {
    CategoryApi.getAllCategories(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/saveCategory', passporAuth.authenticate(), function (req, res) {
    CategoryApi.saveCategory(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/deleteCategory', passporAuth.authenticate(), function (req, res) {
    CategoryApi.deleteCategory(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

module.exports = router;