'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');

var Q = require('q');

var configPath = require('../../configPath.js');
var ForumsApi = require(configPath.api.forums.forums);
var utility = require(configPath.lib.utility);
var debug = require('debug')("app:routes:forums:forums");
var lib_passport_auth = require(configPath.lib.passport).authenticate;
var passporAuth = require(configPath.lib.passport);

router.post('/saveQuestion', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.saveQuestion(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/updateQuestion', passporAuth.authenticate(), function (req, res, next) {
    ForumsApi.updateQuestion(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/deleteQuestion', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.deleteQuestion(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getFirstFewQuestions', function (req, res, next) {
    ForumsApi.getFirstFewQuestions(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getNextFewQuestions', function (req, res, next) {
    ForumsApi.getNextFewQuestions(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/updateTrendingViews', function (req, res, next) {
    ForumsApi.updateTrendingViews(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/updateTrendingCounts', function (req, res, next) {
    ForumsApi.updateTrendingCounts(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getTrendingQrys', function (req, res, next) {
    ForumsApi.getTrendingQrys(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/updateQuestionStatsCounter', function (req, res, next) {

    ForumsApi.updateQuestionStatsCounter(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getQuestionStatsCounter', function (req, res, next) {
    ForumsApi.getQuestionStatsCounter(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getQuestion', function (req, res, next) {
    ForumsApi.getQuestion(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getAllQuestion', function (req, res, next) {
    ForumsApi.getAllQuestion(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

//getUserQuestionStats
router.post('/getUserQuestionStats', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.getUserQuestionStats(req.body).then(function (d) {
        // debug(d);
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

//updateUserQuestionStats
router.post('/updateUserQuestionStats', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.updateUserQuestionStats(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// updateQuestionStats
router.post('/updateQuestionStats', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.updateQuestionStats(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

// updateAnswerLater
router.post('/updateAnswerLater', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.updateAnswerLater(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

//get answer later for user
router.post('/getAnswerLater', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.getAnswerLater(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

//getAllNotifications
router.post('/getAllNotifications', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.getAllNotifications(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

//save complain
// saveComplain
router.post('/saveComplain', function (req, res, next) {
    ForumsApi.saveComplain(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/getDetailedFollowNotifications', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.getDetailedFollowNotifications(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err.toString());
    });
});

router.post('/getDetailedUserFollowedQuestion', passporAuth.authenticate(), function (req, res, next) {
    req.body.usr_id = req.user.usr_id;
    ForumsApi.getDetailedUserFollowedQuestion(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        return next(err);
    });
});

module.exports = router;