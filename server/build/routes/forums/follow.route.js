'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');

var Q = require('q');

var configPath = require('../../configPath.js');
var FollowApi = require(configPath.api.forums.follow);
var utility = require(configPath.lib.utility);
var passporAuth = require(configPath.lib.passport);

router.post('/getAllFollowers', function (req, res) {
  FollowApi.getAllFollowers(req.body).then(function (d) {
    res.status(200).send(d);
  }).catch(function (err) {
    console.error(err);
    res.status(500).send(err);
  });
});

router.post('/updateFollower', passporAuth.authenticate(), function (req, res) {
  req.body.usr_id = req.user.usr_id;
  FollowApi.updateFollower(req.body).then(function (d) {
    res.status(200).send(d);
  }).catch(function (err) {
    console.error(err);
    res.status(500).send(err);
  });
});

router.post('/deleteFollower', passporAuth.authenticate(), function (req, res) {
  req.body.usr_id = req.user.usr_id;
  FollowApi.deleteFollower(req.body).then(function (d) {
    res.status(200).send(d);
  }).catch(function (err) {
    console.error(err);
    res.status(500).send(err);
  });
});

router.post('/updateNotifications', passporAuth.authenticate(), function (req, res) {
  req.body.usr_id = req.user.usr_id;
  FollowApi.updateNotifications(req.body).then(function (d) {
    res.status(200).send(d);
  }).catch(function (err) {
    console.error(err);
    res.status(500).send(err);
  });
});

router.post('/markFollowNotificationRead', passporAuth.authenticate(), function (req, res) {
  req.body.usr_id = req.user.usr_id;
  FollowApi.markFollowNotificationRead(req.body).then(function (d) {
    res.status(200).send(d);
  }).catch(function (err) {
    res.status(500).send(err.toString());
  });
});

module.exports = router;