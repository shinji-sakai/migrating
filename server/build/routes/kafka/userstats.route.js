'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var StatsApi = require(configPath.api.kafka.userstats);

router.post('/', function (req, res) {
	try {
		//convert to json object
		var jsonParsed = JSON.parse(req.body.stats);
		//change hash token to express session id
		// jsonParsed["express_sess"] = req.session.id;
		// console.log("setting token as ..." , req.session.id);
		//set it back to stats
		req.body.stats = JSON.stringify(jsonParsed);

		StatsApi.sendStats(req.body);
		res.status(200).send({});
	} catch (err) {
		console.log(err);
		res.status(500).send(err);
	}
});

router.post('/shouldFetchIdleEvents', function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
		var data;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						_context.next = 3;
						return StatsApi.shouldFetchIdleEvents(req.body);

					case 3:
						data = _context.sent;

						res.status(200).send(data);
						_context.next = 11;
						break;

					case 7:
						_context.prev = 7;
						_context.t0 = _context['catch'](0);

						console.log(_context.t0);
						res.status(500).send(_context.t0);

					case 11:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this, [[0, 7]]);
	}));

	return function (_x, _x2) {
		return _ref.apply(this, arguments);
	};
}());

module.exports = router;