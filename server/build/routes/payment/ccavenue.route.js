'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var http = require('http'),
    fs = require('fs'),
    qs = require('querystring');
var configPath = require("../../configPath.js");
var ccav = require(configPath.lib.ccavutil);
var do_req = require('request');
var express = require('express');
var router = express.Router();
var path = require('path');
var Q = require('q');
var shortid = require('shortid');
var ccavConfig = require(configPath.config.ccavConfig);
var profileAPI = require(configPath.api.dashboard.profile);
var smsAPI = require(configPath.api.sms.sms);
var debug = require('debug')("app:routes:payment:ccav");
var ccavAPI = require(configPath.api.payment.ccavenue);
var userCoursesAPI = require(configPath.api.usercourses);
var mailAPI = require(configPath.api.mail);
var logger = require(configPath.lib.log_to_file);
var paymentAPI = require(configPath.api.payment.payment);

router.post('/ccavRequestHandler', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(request, response) {
        var body, workingKey, accessCode, encRequest, formbody, d, usr_id, training_id, bat_id, userDetails, smsres, request_str, keys;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        body = '', workingKey = ccavConfig.WORKING_KEY, accessCode = ccavConfig.ACCESS_CODE, encRequest = '', formbody = '';
                        d = request.body;

                        d["order_id"] = shortid.generate();
                        d["merchant_id"] = ccavConfig.MERCHANT_ID;
                        d["language"] = "EN";

                        usr_id = d["merchant_param4"];
                        training_id = d["merchant_param2"];
                        bat_id = d["merchant_param3"];
                        _context.next = 10;
                        return profileAPI.getUserShortDetails({ usr_id: usr_id });

                    case 10:
                        userDetails = _context.sent;

                        if (!userDetails.usr_ph) {
                            _context.next = 21;
                            break;
                        }

                        _context.prev = 12;
                        _context.next = 15;
                        return smsAPI.paymentStartedSMS(userDetails.usr_ph);

                    case 15:
                        smsres = _context.sent;
                        _context.next = 21;
                        break;

                    case 18:
                        _context.prev = 18;
                        _context.t0 = _context['catch'](12);

                        logger.debug(_context.t0);

                    case 21:
                        request_str = "";
                        keys = Object.keys(request.body || {});

                        keys.forEach(function (v, i) {
                            var key = v;
                            var val = d[key];
                            request_str = request_str + key + "=" + val + "&";
                        });

                        encRequest = ccav.encrypt(request_str, workingKey);
                        console.log(request_str);
                        formbody = '<form id="nonseamless" method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> <input type="hidden" id="encRequest" name="encRequest" value="' + encRequest + '"><input type="hidden" name="access_code" id="access_code" value="' + accessCode + '"><script language="javascript">document.redirect.submit();</script></form>';
                        response.writeHeader(200, {
                            "Content-Type": "text/html"
                        });
                        response.write(formbody);
                        response.end();
                        return _context.abrupt('return');

                    case 31:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[12, 18]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

router.post('/ccavResponseHandler', function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(request, response) {
        var ccavEncResponse, ccavResponse, workingKey, ccavPOST, ccavJSONResponse, usr_id, training_id, bat_id, userShortDetails, courses, smsres, invoice_data, mail_res, url;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        ccavEncResponse = '', ccavResponse = '', workingKey = ccavConfig.WORKING_KEY, ccavPOST = '';
                        _context2.prev = 1;

                        ccavResponse = ccav.decrypt(request.body.encResp, workingKey);

                        ccavJSONResponse = qs.parse(ccavResponse);


                        logger.debug(ccavJSONResponse);
                        //save payment response info
                        _context2.next = 7;
                        return ccavAPI.savePaymentDetails(ccavJSONResponse);

                    case 7:
                        usr_id = ccavJSONResponse["merchant_param4"];
                        training_id = ccavJSONResponse["merchant_param2"];
                        bat_id = ccavJSONResponse["merchant_param3"];
                        _context2.next = 12;
                        return profileAPI.getUserShortDetails({ usr_id: usr_id });

                    case 12:
                        userShortDetails = _context2.sent;

                        if (!(ccavJSONResponse["order_status"] === "Success")) {
                            _context2.next = 56;
                            break;
                        }

                        _context2.prev = 14;
                        _context2.next = 17;
                        return userCoursesAPI.savePurchasedCourse({ usr_id: usr_id, training_id: training_id });

                    case 17:
                        courses = _context2.sent;
                        _context2.next = 20;
                        return paymentAPI.savePurchasedTrainingDetails({
                            usr_id: usr_id,
                            training_id: training_id,
                            bat_id: bat_id,
                            pay_method: 'ccavenue',
                            pay_status: 'completed',
                            pay_id: ccavJSONResponse["tracking_id"]
                        });

                    case 20:
                        _context2.next = 22;
                        return paymentAPI.saveUserEnrolledBatchDetails({
                            usr_id: usr_id,
                            enroll_bat: bat_id,
                            enroll_dt: new Date(),
                            enroll_status: true
                        });

                    case 22:
                        if (!userShortDetails.usr_ph) {
                            _context2.next = 32;
                            break;
                        }

                        _context2.prev = 23;
                        _context2.next = 26;
                        return smsAPI.sendSuccessfulBuySMS(userShortDetails.usr_ph, training_id, usr_id);

                    case 26:
                        smsres = _context2.sent;
                        _context2.next = 32;
                        break;

                    case 29:
                        _context2.prev = 29;
                        _context2.t0 = _context2['catch'](23);

                        logger.debug(_context2.t0);

                    case 32:
                        if (!userShortDetails.usr_email) {
                            _context2.next = 47;
                            break;
                        }

                        _context2.prev = 33;
                        _context2.next = 36;
                        return paymentAPI.generateInvoiceDetails({
                            training_id: training_id,
                            bat_id: bat_id
                        });

                    case 36:
                        invoice_data = _context2.sent;

                        invoice_data["to"] = userShortDetails.usr_email;
                        invoice_data["dsp_nm"] = userShortDetails.dsp_nm;
                        _context2.next = 41;
                        return mailAPI.sendSuccessfullBuyTrainingMail(invoice_data);

                    case 41:
                        mail_res = _context2.sent;
                        _context2.next = 47;
                        break;

                    case 44:
                        _context2.prev = 44;
                        _context2.t1 = _context2['catch'](33);

                        logger.debug(_context2.t1);

                    case 47:
                        _context2.next = 52;
                        break;

                    case 49:
                        _context2.prev = 49;
                        _context2.t2 = _context2['catch'](14);

                        debug(_context2.t2);

                    case 52:
                        url = "https://" + ccavJSONResponse["merchant_param1"] + "?ccav_status=success";

                        response.redirect(decodeURIComponent(url));

                        _context2.next = 68;
                        break;

                    case 56:
                        if (!userShortDetails.usr_ph) {
                            _context2.next = 66;
                            break;
                        }

                        _context2.prev = 57;
                        _context2.next = 60;
                        return smsAPI.paymentFailureSMS(userShortDetails.usr_ph, training_id);

                    case 60:
                        smsres = _context2.sent;
                        _context2.next = 66;
                        break;

                    case 63:
                        _context2.prev = 63;
                        _context2.t3 = _context2['catch'](57);

                        logger.debug(_context2.t3);

                    case 66:

                        // if(userShortDetails.usr_email){
                        //     try{
                        //         var mail_res = await mailAPI.sendSuccessfullBuyTrainingMail({
                        //             to : userShortDetails.usr_email,
                        //             dsp_nm : userShortDetails.dsp_nm,
                        //             training : training_id
                        //         })
                        //     }catch(err){
                        //         logger.debug(err);
                        //     }
                        // }

                        url = "https://" + ccavJSONResponse["merchant_param1"] + "?ccav_status=failure&msg=" + ccavJSONResponse["status_message"];

                        response.redirect(decodeURIComponent(url));

                    case 68:
                        _context2.next = 74;
                        break;

                    case 70:
                        _context2.prev = 70;
                        _context2.t4 = _context2['catch'](1);

                        logger.debug(_context2.t4);
                        throw _context2.t4;

                    case 74:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[1, 70], [14, 49], [23, 29], [33, 44], [57, 63]]);
    }));

    return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
    };
}());

router.post('/orderStatus', function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(request, response) {
        var command, data, api_res;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        command = request.body.command;
                        data = request.body.data;
                        _context3.next = 5;
                        return ccavAPI.doAPICall(command, data);

                    case 5:
                        api_res = _context3.sent;

                        response.status(200).send(api_res);
                        _context3.next = 12;
                        break;

                    case 9:
                        _context3.prev = 9;
                        _context3.t0 = _context3['catch'](0);

                        response.status(500).send({ err: _context3.t0 });

                    case 12:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[0, 9]]);
    }));

    return function (_x5, _x6) {
        return _ref3.apply(this, arguments);
    };
}());

router.post('/apiCall/:command', function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(request, response) {
        var command, data, api_res;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;

                        debug(request.params);
                        command = request.params["command"];
                        data = request.body;
                        _context4.next = 6;
                        return ccavAPI.doAPICall(command, data);

                    case 6:
                        api_res = _context4.sent;

                        response.status(200).send(api_res);
                        _context4.next = 13;
                        break;

                    case 10:
                        _context4.prev = 10;
                        _context4.t0 = _context4['catch'](0);

                        response.status(500).send({ err: _context4.t0 });

                    case 13:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 10]]);
    }));

    return function (_x7, _x8) {
        return _ref4.apply(this, arguments);
    };
}());

module.exports = router;