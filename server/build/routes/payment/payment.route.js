'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');
var Q = require('q');

var configPath = require('../../configPath.js');
var API = require(configPath.api.payment.payment);
var utility = require(configPath.lib.utility);

router.post('/savePaymentDetails', function (req, res) {
    API.savePaymentDetails(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

// getPaymentDetails
router.post('/getPaymentDetails', function (req, res) {
    API.getPaymentDetails(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/getPurchasedTrainingDetails', function (req, res) {
    API.getPurchasedTrainingDetails(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/saveUserEnrolledBatchDetails', function (req, res) {
    API.saveUserEnrolledBatchDetails(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/getUserEnrolledBatchDetails', function (req, res) {
    API.getUserEnrolledBatchDetails(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

// checkPaymentStatus
router.post('/checkPaymentStatus', function (req, res) {
    API.checkPaymentStatus(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

module.exports = router;