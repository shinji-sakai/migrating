'use strict';

var paypal = require('./paypal.js');
var payment = require('./payment.route.js');
var ccav = require('./ccavenue.route.js');

module.exports = function (app) {
	app.use('/payment/paypal', paypal);
	app.use('/payment/ccavenue', ccav);
	app.use('/payment', payment);
};