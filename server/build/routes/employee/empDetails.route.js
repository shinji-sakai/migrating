'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');
var Q = require('q');

var configPath = require('../../configPath.js');
var EmpDetailsAPI = require(configPath.api.employee.empDetails);
var utility = require(configPath.lib.utility);

router.post('/add', function (req, res) {
    EmpDetailsAPI.add(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/getAll', function (req, res) {
    EmpDetailsAPI.getAll(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/delete', function (req, res) {
    EmpDetailsAPI.delete(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

module.exports = router;