'use strict';

var emptypes = require('./empTypes.routes.js');
var empdetails = require('./empDetails.route.js');
var empskills = require('./empSkills.route.js');

module.exports = function (app) {
	app.use('/employee', empdetails);
	app.use('/employee/types', emptypes);
	app.use('/employee/skills', empskills);
};