'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var saveSessionInfo = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(req, doc) {
        var loginData;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;
                        loginData = {
                            token: doc.token,
                            usr_id: doc.user.usr_id
                        };
                        _context4.next = 4;
                        return redisClient.setAsync(doc.user.usr_id, JSON.stringify(doc.user));

                    case 4:
                        _context4.next = 6;
                        return redisClient.setAsync("session-" + doc.user.usr_id, doc.token);

                    case 6:
                        _context4.next = 11;
                        break;

                    case 8:
                        _context4.prev = 8;
                        _context4.t0 = _context4['catch'](0);

                        console.log("auth.route.js saveSessionInfo err...", _context4.t0);

                    case 11:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[0, 8]]);
    }));

    return function saveSessionInfo(_x6, _x7) {
        return _ref4.apply(this, arguments);
    };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();
var Q = require('q');

var configPath = require('../../configPath.js');
var AuthApi = require(configPath.api.auth.authCass);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
var lib_passport_auth = require(configPath.lib.passport).authenticate;

router.post('/login', function (req, res, next) {
    AuthApi.validateLoginDetails(req.body).then(AuthApi.generateJWT).then(function () {
        var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(doc) {
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            if (!doc.err) {
                                saveSessionInfo(req, doc);
                            }
                            res.status(200).send(doc);

                        case 2:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, this);
        }));

        return function (_x) {
            return _ref.apply(this, arguments);
        };
    }()).catch(function (err) {
        return next(err);
    });
});

router.post('/test', lib_passport_auth(), function (req, res, next) {
    res.status(200).send(req.user);
});

router.post('/logout', function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req, res, next) {
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        _context2.next = 3;
                        return redisClient.delAsync("session-" + req.body.usr_id);

                    case 3:
                        res.status(200).send({});
                        _context2.next = 9;
                        break;

                    case 6:
                        _context2.prev = 6;
                        _context2.t0 = _context2['catch'](0);
                        return _context2.abrupt('return', next(_context2.t0));

                    case 9:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 6]]);
    }));

    return function (_x2, _x3, _x4) {
        return _ref2.apply(this, arguments);
    };
}());

router.post('/resendOTP', function (req, res, next) {
    AuthApi.resendOTP(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/checkUserExistance', function (req, res, next) {
    AuthApi.checkUserExistance(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/register', function (req, res, next) {
    AuthApi.registerUser(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/verifyOTP', function (req, res, next) {
    console.log(req.body);
    AuthApi.verifyOTP(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/removeUser', function (req, res, next) {
    AuthApi.removeUser(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/checkPassword', function (req, res, next) {
    AuthApi.checkPassword(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/changePassword', function (req, res, next) {
    AuthApi.changePassword(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/forgotPassword', function (req, res, next) {
    AuthApi.forgotPassword(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});
router.post('/resendForgotPasswordOTP', function (req, res, next) {
    AuthApi.resendForgotPasswordOTP(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});
router.post('/verifyForgotPasswordOTP', function (req, res, next) {
    AuthApi.verifyForgotPasswordOTP(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        return next(err);
    });
});
// 


router.post('/facebook', function (req, res, next) {
    var user_profile;
    AuthApi.facebook(req.body).then(function (profile) {
        profile.id = profile.id + "-fb";
        user_profile = profile;
        var data = {
            usr_id: profile.id
        };
        console.log("fb profile........", user_profile);
        // return AuthApi.checkUserExistance(data);
        return data;
    }).then(function (res) {
        var user = {
            usr_id: user_profile.id,
            dsp_nm: user_profile.name,
            usr_email: user_profile.email,
            usr_ph: '',
            usr_role: ''
        };
        return AuthApi.updateSocialMediaDetails(user);
    }).then(function () {
        var data = {};
        data.user = {
            usr_id: user_profile.id,
            dsp_nm: user_profile.name,
            usr_email: user_profile.email,
            pwd: ""
        };
        return AuthApi.generateJWT(data);
    }).then(function (d) {
        if (d.token) {
            saveSessionInfo(req, d);
            res.status(200).send(d);
        } else {
            res.status(200).send({});
        }
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/google', function (req, res, next) {
    var user_profile;
    AuthApi.google(req.body).then(function (profile) {
        user_profile = profile;
        var data = {
            usr_id: profile.email
        };
        console.log("google profile........", user_profile);
        // return AuthApi.checkUserExistance(data);
        return data;
    }).then(function (res) {
        var user = {
            usr_id: user_profile.email,
            dsp_nm: user_profile.name,
            usr_email: user_profile.email,
            usr_ph: '',
            usr_role: ''
        };
        return AuthApi.updateSocialMediaDetails(user);
    }).then(function () {
        var data = {};
        data.user = {
            usr_id: user_profile.email,
            dsp_nm: user_profile.name,
            usr_pic: user_profile.picture,
            usr_email: user_profile.email,
            pwd: ""
        };
        return AuthApi.generatePassportJWT(data);
    }).then(function () {
        var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(doc) {
            return _regenerator2.default.wrap(function _callee3$(_context3) {
                while (1) {
                    switch (_context3.prev = _context3.next) {
                        case 0:
                            res.status(200).send(doc);

                        case 1:
                        case 'end':
                            return _context3.stop();
                    }
                }
            }, _callee3, this);
        }));

        return function (_x5) {
            return _ref3.apply(this, arguments);
        };
    }()).catch(function (err) {
        return next(err);
    });
});

router.post('/twitter', function (req, res, next) {
    var user_profile;
    AuthApi.twitter(req.body).then(function (token) {
        if (token.oauth_token || token.oauth_verifier) {
            res.status(200).send(token);
            return;
        }
        console.log("res.................", token);
        token.id_str = token.id_str + "-twitter";
        user_profile = token;
        var data = {
            usr_id: user_profile.id_str
        };
        return data;
    }).then(function (res) {
        var user = {
            usr_id: user_profile.id_str,
            dsp_nm: user_profile.name,
            usr_email: user_profile.email,
            usr_ph: '',
            usr_role: ''
        };
        return AuthApi.updateSocialMediaDetails(user);
    }).then(function () {
        var data = {};
        data.user = {
            usr_id: user_profile.id_str,
            dsp_nm: user_profile.name,
            pwd: ""
        };
        return AuthApi.generateJWT(data);
    }).then(function (d) {
        if (d.token) {
            saveSessionInfo(req, d);
            res.status(200).send(d);
        } else {
            res.status(200).send({});
        }
    }).catch(function (err) {
        return next(err);
    });
});

router.post('/linkedin', function (req, res, next) {
    var user_profile;
    AuthApi.linkedin(req.body).then(function (profile) {
        profile.id = profile.id + "-linkedin";
        user_profile = profile;
        var data = {
            usr_id: profile.id
        };
        console.log("linkedin profile........", user_profile);
        return data;

        // return AuthApi.checkUserExistance(data);
    }).then(function (res) {
        var user = {
            usr_id: '' + user_profile.id,
            dsp_nm: user_profile.firstName + " " + user_profile.lastName,
            usr_email: user_profile.emailAddress,
            usr_ph: '',
            usr_role: ''
        };
        return AuthApi.updateSocialMediaDetails(user);
    }).then(function () {
        var data = {};
        data.user = {
            usr_id: user_profile.id,
            dsp_nm: user_profile.firstName + " " + user_profile.lastName,
            usr_pic: user_profile.pictureUrl,
            usr_email: user_profile.emailAddress,
            pwd: ""
        };
        return AuthApi.generateJWT(data);
    }).then(function (d) {
        if (d.token) {
            saveSessionInfo(req, d);
            res.status(200).send(d);
        } else {
            res.status(200).send({});
        }
    }).catch(function (err) {
        return next(err);
    });
});

module.exports = router;