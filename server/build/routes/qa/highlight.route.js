'use strict';

var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var highlightApi = require(configPath.api.qa.highlight);

router.post('/save', function (req, res) {
    highlightApi.saveHighlightText(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});
router.post('/delete', function (req, res) {
    highlightApi.removeHighlightText(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/updateHighlightNote', function (req, res) {
    highlightApi.updateHighlightNote(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/getQuestionHighlights', function (req, res) {
    highlightApi.getQuestionHighlights(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/getUserHighlights', function (req, res) {
    highlightApi.getUserHighlights(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.post('/test/', function (req, res) {
    console.log("post..............");
    var data = req.body;
    var random = Math.floor(Math.random() * 90000) + 10000;
    data.id = 'highlight_' + random;
    highlightApi.saveHighlightText(data).then(function (doc) {
        res.status(200).send(data);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.put('/test/:id', function (req, res) {
    console.log("put..............");
    var data = req.body;
    data.id = req.params.id;
    delete data._id;
    highlightApi.saveHighlightText(data).then(function (doc) {
        res.status(200).send(data);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.delete('/test/:id', function (req, res) {
    console.log("delete..............");
    var data = {};
    data.id = req.params.id;
    delete data._id;
    highlightApi.removeHighlightText(data).then(function (doc) {
        res.status(200).send(data);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.get('/search', function (req, res) {
    console.log("searcj..............");
    // var jsonQuery = JSON.parse(req.query);

    highlightApi.getQuestionHighlights(req.query).then(function (doc) {
        var data = {
            total: doc.length,
            rows: doc
        };
        res.status(200).send(data);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

router.get('/annotations', function (req, res) {
    console.log("annotations..............");
    var data = {
        user: 'dummy',
        questionId: 'corejava_1_38'
    };
    highlightApi.getQuestionHighlights(data).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        console.log(err);
        res.status(500).json({
            err: err.message
        });
    });
});

module.exports = router;