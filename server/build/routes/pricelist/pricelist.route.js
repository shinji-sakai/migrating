'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.pricelist.pricelist);
var debug = require('debug')("app:api:pricelist.route.js");

router.post('/getAll', function (req, res) {
    api.getAll(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/add', function (req, res) {
    api.add(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/delete', function (req, res) {
    api.delete(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
// getDetailedPricelist
router.post('/getDetailedPricelist', function (req, res) {
    api.getDetailedPricelist(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
// getPricelistByCourseId
router.post('/getPricelistByCourseId', function (req, res) {
    api.getPricelistByCourseId(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});

// getDetailedPricelistByCourseId
router.post('/getDetailedPricelistByCourseId', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var api_res;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;

                        console.log("getDetailedPricelistByCourseId");
                        _context.next = 4;
                        return api.getDetailedPricelistByCourseId(req.body);

                    case 4:
                        api_res = _context.sent;

                        res.status(200).send(api_res);
                        _context.next = 12;
                        break;

                    case 8:
                        _context.prev = 8;
                        _context.t0 = _context['catch'](0);

                        debug(_context.t0);
                        res.status(500).json({ err: _context.t0.message });

                    case 12:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 8]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

module.exports = router;