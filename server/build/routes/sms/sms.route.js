'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');
var Q = require('q');

var configPath = require('../../configPath.js');
var API = require(configPath.api.sms.sms);
var utility = require(configPath.lib.utility);

router.post('/send', function (req, res) {
    API.sendSMS(req.body).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/sendSuccessfulBuySMS', function (req, res) {
    API.sendSuccessfulBuySMS(req.body.numbers, req.body.training, req.body.usr_id).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

router.post('/paymentFailureSMS', function (req, res) {
    API.paymentFailureSMS(req.body.numbers, req.body.training).then(function (d) {
        res.status(200).send(d);
    }).catch(function (err) {
        console.error(err);
        res.status(500).send(err);
    });
});

module.exports = router;