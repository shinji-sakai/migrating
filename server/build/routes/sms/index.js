'use strict';

var send = require('./sms.route.js');

module.exports = function (app) {
	app.use('/sms', send);
};