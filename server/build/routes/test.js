'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
// Initialize the Router
var router = express.Router();
var fs = require('fs');
var sanitizeHtml = require('sanitize-html');
var configPath = require('../configPath.js');
var cassExecute = require(configPath.lib.utility).cassExecute;

router.get('/getHtml', function (req, res) {

    fs.readFile('data/test.html', 'utf8', function (err, data) {

        if (err) res.status(500).send({ err: err });

        data = data.replace(/\"/g, '\'');
        res.status(200).send({ d: data });
    });
});

router.post('/setHtml', function (req, res) {

    console.log(req.body);

    var data = sanitizeHtml(req.body.d, {
        allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img'])
    });
    console.log(data);
    fs.writeFile('data/test.html', data, 'utf8', function (err) {
        if (err) {
            res.status(500).send({ err: err });return;
        }
        res.status(200).send(data);
    });
});

router.post('/insertData', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var data, obj, qry, params;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        data = req.body;

                        if (!data.c1) {
                            obj = {
                                status: "error",
                                message: "c1 field is required"
                            };

                            res.status(200).send(obj);
                        }
                        if (!data.c2) {
                            obj = {
                                status: "error",
                                message: "c2 field is required"
                            };

                            res.status(200).send(obj);
                        }
                        qry = "insert into ew1.dummy (c1,c2) values (?,?)";
                        params = [data.c1, data.c2];
                        _context.next = 8;
                        return cassExecute(qry, params);

                    case 8:
                        obj = {
                            status: "success",
                            message: "Data is inserted"
                        };

                        res.status(200).send(obj);
                        _context.next = 15;
                        break;

                    case 12:
                        _context.prev = 12;
                        _context.t0 = _context['catch'](0);

                        res.status(500).send({
                            status: "error",
                            message: _context.t0.toString()
                        });

                    case 15:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined, [[0, 12]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

// Expose the module
module.exports = router;