'use strict';

var notes = require('./notes.route.js');

module.exports = function (app) {
	app.use('/api/notes', notes);
};