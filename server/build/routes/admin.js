'use strict';

//Not Used
//Deprecated

var express = require('express');
// Initialize the Router
var router = express.Router();
var jwt = require('jsonwebtoken');
var moment = require('moment');
var requestIp = require('request-ip');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({
    // uploadDir: __dirname + "../client/assets/images/upload/"
});
var Q = require('q');
var fs = require('fs');
var debug = require('debug')('app:adminapi');
var config = require('../config.js');
var configPath = require('../configPath.js');
var mongoapi = require(configPath.routes.mongoapi);
var utility = require(configPath.lib.utility);

//Course Routes
router.post('/addcourse', function (req, res) {
    new mongoapi().addcourse(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});
router.post('/removeCourse', function (req, res) {
    debug(req.body);
    new mongoapi().removeCourse(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function (err) {
        debug(err);
        res.status(500).json({ err: err.message });
    });
});

//Course Module Routes
router.post('/getCourseModules', function (req, res) {
    new mongoapi().getCourseModules(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/addCourseModule', function (req, res) {
    new mongoapi().addCourseModule(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/deleteCourseModule', function (req, res) {
    new mongoapi().deleteCourseModule(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});

//Course Master Routes
router.post('/addCourseMaster', function (req, res) {
    new mongoapi().addCourseMaster(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/deleteCourseMaster', function (req, res) {
    new mongoapi().deleteCourseMaster(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.get('/coursemaster', function (req, res) {
    new mongoapi().displayCourseMaster().then(function (doc) {
        console.log("doc", doc);
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});

//Video Slide Routes
router.post('/addVideoSlide', function (req, res) {
    new mongoapi().addVideoSlide(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/deleteVideoSlide', function (req, res) {
    new mongoapi().deleteVideoSlide(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/getVideoSlides', function (req, res) {
    new mongoapi().getVideoSlides(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/uploadVideoSlideImages', multipartyMiddleware, function (req, res) {
    var videoId = req.body.videoId;
    var arr = [];
    for (var key in req.files.files) {
        var file = req.files.files[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0, dotPos);
        var ext = file.originalFilename.substr(dotPos + 1);
        var targetFileName = videoId + "_" + onlyName + "." + ext;
        var targetPath = __dirname + "/../../client/assets/upload/";
        arr[key] = utility.copyFile(file.path, targetPath + targetFileName);
    }
    Q.all(arr).then(function () {
        res.status(200).send({});
    }).catch(function (err) {
        res.status(500).send({ 'err': err });
    });
});

//Course Video Routes
router.post('/addVideos', function (req, res) {
    new mongoapi().addVideos(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/getModuleVideos', function (req, res) {
    new mongoapi().getModuleVideos(req.body).then(function (doc) {
        if (doc.length > 0) {
            res.status(200).send(doc[0].moduleDetail[0].moduleItems);
        } else {
            res.status(200).send(doc);
        }
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});
router.post('/getCourseVideos', function (req, res) {
    new mongoapi().getCourseVideos(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});

router.post('/getFullDoc', function (req, res) {
    new mongoapi().getFullDoc(req.body).then(function (doc) {
        res.status(200).send(doc);
    }).catch(function sendErrorResponse501(err) {
        debug(err);
        res.status(501).json({ err: err.message });
    });
});

// Expose the module
module.exports = router;