'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();

var configPath = require('../configPath.js');
var videoUrlAPI = require(configPath.api.videoUrl);

router.post('/fetch', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(req, res) {
        var source, name, subtitle, url, subtitleUrl;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        source = req.body.videoSource || "harddrive";
                        name = req.body.videoUrl;
                        subtitle = req.body.subtitleUrl;
                        _context.next = 6;
                        return videoUrlAPI[source](name);

                    case 6:
                        url = _context.sent;
                        subtitleUrl = "";

                        if (!subtitle) {
                            _context.next = 13;
                            break;
                        }

                        console.log("Subtitle.....", subtitle);
                        _context.next = 12;
                        return videoUrlAPI[source](subtitle);

                    case 12:
                        subtitleUrl = _context.sent;

                    case 13:
                        res.status(200).send({
                            url: url,
                            subtitleUrl: subtitleUrl
                        });
                        _context.next = 19;
                        break;

                    case 16:
                        _context.prev = 16;
                        _context.t0 = _context['catch'](0);

                        res.status(500).send({
                            err: _context.t0
                        });

                    case 19:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 16]]);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}());

module.exports = router;