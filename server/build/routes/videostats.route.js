'use strict';

var express = require('express');
// Initialize the Router
var router = express.Router();

var configPath = require('../configPath.js');
var videoStatsApi = require(configPath.api.videostats);

router.post('/insertStats', function (req, res) {
    videoStatsApi.insertStats(req.body).then(function (doc) {
        res.status(200).send({});
    }).catch(function (err) {
        console.error(err);
        res.status(401).json({ err: err.message });
    });
});

module.exports = router;