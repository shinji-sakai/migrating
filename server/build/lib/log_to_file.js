'use strict';

var log4js = require('log4js');
var logger = log4js.getLogger('ew');

module.exports = logger;