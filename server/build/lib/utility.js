'use strict';

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
var Q = require('q');
var im = require('imagemagick');
var configPath = require('../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var debug = require('debug')("app:api:utility");
var mjAPI = require("mathjax-node/lib/mj-page.js");
var jsdom = require("jsdom").jsdom;

var Utility = function () {
    function Utility() {
        (0, _classCallCheck3.default)(this, Utility);

        this.Base64 = {
            _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            encode: function encode(e) {
                var t = "";
                var n, r, i, s, o, u, a;
                var f = 0;
                e = Base64._utf8_encode(e);
                while (f < e.length) {
                    n = e.charCodeAt(f++);
                    r = e.charCodeAt(f++);
                    i = e.charCodeAt(f++);
                    s = n >> 2;
                    o = (n & 3) << 4 | r >> 4;
                    u = (r & 15) << 2 | i >> 6;
                    a = i & 63;
                    if (isNaN(r)) {
                        u = a = 64;
                    } else if (isNaN(i)) {
                        a = 64;
                    }
                    t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
                }
                return t;
            },
            decode: function decode(e) {
                var t = "";
                var n, r, i;
                var s, o, u, a;
                var f = 0;
                e = e.replace(/[^A-Za-z0-9+/=]/g, "");
                while (f < e.length) {
                    s = this._keyStr.indexOf(e.charAt(f++));
                    o = this._keyStr.indexOf(e.charAt(f++));
                    u = this._keyStr.indexOf(e.charAt(f++));
                    a = this._keyStr.indexOf(e.charAt(f++));
                    n = s << 2 | o >> 4;
                    r = (o & 15) << 4 | u >> 2;
                    i = (u & 3) << 6 | a;
                    t = t + String.fromCharCode(n);
                    if (u != 64) {
                        t = t + String.fromCharCode(r);
                    }
                    if (a != 64) {
                        t = t + String.fromCharCode(i);
                    }
                }
                t = Base64._utf8_decode(t);
                return t;
            },
            _utf8_encode: function _utf8_encode(e) {
                e = e.replace(/rn/g, "n");
                var t = "";
                for (var n = 0; n < e.length; n++) {
                    var r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r);
                    } else if (r > 127 && r < 2048) {
                        t += String.fromCharCode(r >> 6 | 192);
                        t += String.fromCharCode(r & 63 | 128);
                    } else {
                        t += String.fromCharCode(r >> 12 | 224);
                        t += String.fromCharCode(r >> 6 & 63 | 128);
                        t += String.fromCharCode(r & 63 | 128);
                    }
                }
                return t;
            },
            _utf8_decode: function _utf8_decode(e) {
                var t = "";
                var n = 0;
                var r = 0;
                var c2 = 0;
                var c1 = 0;
                while (n < e.length) {
                    r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r);
                        n++;
                    } else if (r > 191 && r < 224) {
                        c2 = e.charCodeAt(n + 1);
                        t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                        n += 2;
                    } else {
                        c2 = e.charCodeAt(n + 1);
                        c3 = e.charCodeAt(n + 2);
                        t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                        n += 3;
                    }
                }
                return t;
            }
        };
    }

    (0, _createClass3.default)(Utility, [{
        key: 'copyFile',
        value: function copyFile(source, target, cb) {
            var cbCalled = false;
            var defer = Q.defer();
            var rd = fs.createReadStream(source);
            rd.on("error", function (err) {
                done(err);
            });
            var wr = fs.createWriteStream(target);
            wr.on("error", function (err) {
                done(err);
            });
            wr.on("close", function (ex) {
                done();
            });
            rd.pipe(wr);

            function done(err) {
                if (!cbCalled) {
                    if (err) {
                        defer.reject(err);
                    } else {
                        defer.resolve({});
                    }
                    cbCalled = true;
                }
            }
            return defer.promise;
        }
    }, {
        key: 'resizeImage',
        value: function resizeImage(srcPath, destPath, xxy) {
            var defer = Q.defer();
            im.convert([srcPath, '-resize', xxy, destPath], function (err, stdout) {
                if (err) {
                    defer.reject(err);
                }
                defer.resolve(destPath);
            });
            return defer.promise;
        }
    }, {
        key: 'cassExecute',
        value: function cassExecute(query, params, options) {
            var defer = Q.defer();
            cassconn.execute(query, params, options || {}, function (err, res) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var jsonRes = JSON.parse(JSON.stringify(res.rows || {}));
                defer.resolve(jsonRes);
            });
            return defer.promise;
        }
    }, {
        key: 'cassBatch',
        value: function cassBatch(queries, options) {
            var defer = Q.defer();
            cassconn.batch(queries, options || {}, function (err, res) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                defer.resolve({});
            });
            return defer.promise;
        }

        /**
         * @param {string|int} sec - Seconds
         * @returns {String}
         * @desc
         * - This method is used convert given seconds into Properly formatted time "HH:MM:SS"
         */

    }, {
        key: 'changeTimeFormat',
        value: function changeTimeFormat(sec) {
            var time = sec || 0;
            var hours = parseInt(time / 3600) % 24;
            var minutes = parseInt(time / 60) % 60;
            var seconds = parseInt(time % 60);

            hours = ("0" + hours).slice(-2);
            minutes = ("0" + minutes).slice(-2);
            seconds = ("0" + seconds).slice(-2);

            var ret_time;
            if (hours == "00") {
                ret_time = minutes + ":" + seconds;
            } else {
                ret_time = hours + ":" + minutes + ":" + seconds;
            }

            return ret_time;
        }

        /**
         * @param {string} mathml
         * @returns {Promise}
         * @desc
         * - This method is used to convert mathml/latex/tex code to plain html
         */

    }, {
        key: 'mathTypeSet',
        value: function mathTypeSet(mathml) {
            var defer = Q.defer();
            var document = jsdom('<!DOCTYPE html><html><body>' + mathml + '</body></html>');
            var mathRes = mjAPI.typeset({
                html: document.body.innerHTML,
                renderer: "CommonHTML"
            }, function (res) {
                defer.resolve(res.html);
            });
            return defer.promise;
        }

        /**
         * @param {Array} arr - Any Array
         * @returns {Array}
         * @desc
         * - This method is used to shuffle given array
         */

    }, {
        key: 'shuffleArray',
        value: function shuffleArray(arr) {
            var o = arr;
            for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) {}
            return o;
        }
    }]);
    return Utility;
}();

module.exports = new Utility();
// module.exports = {
//     copyFile: copyFile,
//     resizeImage: resizeImage,
//     Base64: Base64,
//     cassExecute: cassExecute,
//     cassBatch: cassBatch,
//     changeTimeFormat: changeTimeFormat,
//     mathTypeSet: mathTypeSet,
//     shuffleArray : shuffleArray
// }