'use strict';

var configPath = require('../configPath.js');
var express_session = require('express-session');
var RedisStore = require('connect-redis')(express_session);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");

var session = express_session({
    resave: false,
    store: new RedisStore({
        cline: redisClient
    }),
    rolling: true,
    saveUninitialized: false,
    secret: 'ew',
    cookie: {
        httpOnly: false,
        maxAge: null
    }
});

module.exports = session;