'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');
var websocketApi = require(configPath.api.websocket);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
module.exports = function () {
    return function (req, res, next) {
        var shouldTrackSession = req.header('x-track-session');
        if (!shouldTrackSession) {
            // if request is not coming from api
            next();
            return;
        }

        var sessionId = req.header('x-session-id');
        console.log("First Track session..............", sessionId);
        if (sessionId && sessionId != null) {

            console.log("Track session..............", sessionId, req.url);

            //try to load a sessionId
            req.sessionStore.get(sessionId, function (err, sess) {
                if (!err) {
                    var shouldCheckForAuth = false;
                    console.log("getting from redis....", sessionId, sess);
                    res.set('sess-id', req.session.id);
                    // create session using sessionstore
                    req.sessionStore.set(sessionId, sess || { cookie: { httpOnly: false, maxAge: null } }, function () {
                        var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(err) {
                            var current_user_id, session_id_from_redis, isUserLoggedIn;
                            return _regenerator2.default.wrap(function _callee$(_context) {
                                while (1) {
                                    switch (_context.prev = _context.next) {
                                        case 0:
                                            if (err) {
                                                _context.next = 17;
                                                break;
                                            }

                                            if (!req.session.loginData) {
                                                _context.next = 15;
                                                break;
                                            }

                                            _context.prev = 2;

                                            // get current user from session
                                            current_user_id = req.session.loginData.usr_id;
                                            // get user session id which is already stored in redis

                                            _context.next = 6;
                                            return redisClient.getAsync(current_user_id + "-session");

                                        case 6:
                                            session_id_from_redis = _context.sent;

                                            // check session id in redis exist or not
                                            if (session_id_from_redis && session_id_from_redis != 'null') {
                                                if (session_id_from_redis != sessionId) {
                                                    req.sessionStore.get(session_id_from_redis, function (err, _sess) {
                                                        if (!err) {
                                                            // send logout msg to other loggedin browser
                                                            websocketApi.logoutMsgToOtherBrowser(current_user_id, _sess.socketId);
                                                        }
                                                    });
                                                }
                                            } else {
                                                shouldCheckForAuth = true;
                                            }
                                            _context.next = 13;
                                            break;

                                        case 10:
                                            _context.prev = 10;
                                            _context.t0 = _context['catch'](2);

                                            console.log("single-login.js err...", _context.t0);

                                        case 13:
                                            _context.next = 16;
                                            break;

                                        case 15:
                                            shouldCheckForAuth = true;

                                        case 16:
                                            if (shouldCheckForAuth) {
                                                isUserLoggedIn = req.header('Authorization');

                                                if (isUserLoggedIn) {
                                                    res.set('shouldLogout', 'true');
                                                    console.log("User is not loggedin...............");
                                                }
                                            }

                                        case 17:
                                            next();

                                        case 18:
                                        case 'end':
                                            return _context.stop();
                                    }
                                }
                            }, _callee, this, [[2, 10]]);
                        }));

                        return function (_x) {
                            return _ref.apply(this, arguments);
                        };
                    }());
                } else {
                    console.log("Generate session..............", req.url);
                    req.session.regenerate(function (err) {
                        res.set('sess-id', req.session.id);
                        next();
                    });
                }
            });
        } else {
            req.session.regenerate(function (err) {
                res.set('sess-id', req.session.id);
                next();
            });
        }
    };
};