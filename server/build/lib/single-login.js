'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('../configPath.js');
var config = require('../config.js');
var jwt = require('jsonwebtoken');
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
module.exports = function () {
    return function (req, res, next) {
        var userToken = req.header('Authorization');
        if (!userToken) {
            // if user is not logged in or not api request
            next();
            return;
        } else {
            var jwtToken = userToken.substr(4);
            var token = jwt.verify(jwtToken, config.TOKEN_SECRET, function () {
                var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(err, decoded) {
                    var usr_sess_detail;
                    return _regenerator2.default.wrap(function _callee$(_context) {
                        while (1) {
                            switch (_context.prev = _context.next) {
                                case 0:
                                    if (!err) {
                                        _context.next = 4;
                                        break;
                                    }

                                    // if jwt token is not valid , if malformed , or expired
                                    res.set('shouldLogout', 'true');
                                    // console.log("error in decoding jwt token",err)
                                    _context.next = 12;
                                    break;

                                case 4:
                                    if (!decoded.userId) {
                                        _context.next = 11;
                                        break;
                                    }

                                    _context.next = 7;
                                    return redisClient.getAsync("session-" + decoded.userId);

                                case 7:
                                    usr_sess_detail = _context.sent;

                                    if (usr_sess_detail && usr_sess_detail != 'null') {
                                        if (jwtToken != usr_sess_detail) {
                                            res.set('shouldLogout', 'true');
                                        } else {
                                            // console.log("same user.....");
                                        }
                                    } else {
                                        res.set('shouldLogout', 'true');
                                    }
                                    _context.next = 12;
                                    break;

                                case 11:
                                    // if there is no user id in token
                                    res.set('shouldLogout', 'true');

                                case 12:
                                    next();

                                case 13:
                                case 'end':
                                    return _context.stop();
                            }
                        }
                    }, _callee, this);
                }));

                return function (_x, _x2) {
                    return _ref.apply(this, arguments);
                };
            }());
        }
    };
};