"use strict";

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var passport = require("passport");
var passportJWT = require("passport-jwt");
var config = require("../config.js");
var ExtractJwt = passportJWT.ExtractJwt;
var JWTStrategy = passportJWT.Strategy;

var params = {
    secretOrKey: config.TOKEN_SECRET,
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    ignoreExpiration: false
};
var configPath = require('../configPath.js');
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
var cassExecute = require(configPath.lib.utility).cassExecute;

module.exports = function () {
    var strategy = new JWTStrategy(params, function (payload, done) {
        var getUserDetailFromCassandra = function () {
            var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
                var qry, _params, rows, row;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                qry = "select * from ew1.vt_usr_lgn where usr_id = ?";
                                _params = [payload.userId];
                                _context2.next = 5;
                                return cassExecute(qry, _params);

                            case 5:
                                rows = _context2.sent;

                                if (!rows[0]) {
                                    _context2.next = 15;
                                    break;
                                }

                                row = rows[0];

                                if (!(row.is_act === 'true')) {
                                    _context2.next = 12;
                                    break;
                                }

                                return _context2.abrupt("return", done(null, rows[0]));

                            case 12:
                                return _context2.abrupt("return", done("User is inactive", false));

                            case 13:
                                _context2.next = 16;
                                break;

                            case 15:
                                return _context2.abrupt("return", done("No User Found", false));

                            case 16:
                                _context2.next = 21;
                                break;

                            case 18:
                                _context2.prev = 18;
                                _context2.t0 = _context2["catch"](0);
                                return _context2.abrupt("return", done("No User Found", false));

                            case 21:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[0, 18]]);
            }));

            return function getUserDetailFromCassandra() {
                return _ref2.apply(this, arguments);
            };
        }();

        // Here in our case, This strategy will check token expired or not because of ignoreExpiration: false option
        // If it is expired then it will return error
        // If not then it will go to next line
        // and check in redis db
        redisClient.get(payload.userId, function () {
            var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(err, usr) {
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                if (!err) {
                                    _context.next = 4;
                                    break;
                                }

                                getUserDetailFromCassandra();
                                _context.next = 14;
                                break;

                            case 4:
                                if (!(usr === null || usr === 'null')) {
                                    _context.next = 8;
                                    break;
                                }

                                getUserDetailFromCassandra();
                                _context.next = 14;
                                break;

                            case 8:
                                usr = JSON.parse(usr);

                                if (!(usr.is_act === 'true')) {
                                    _context.next = 13;
                                    break;
                                }

                                done(null, usr);
                                _context.next = 14;
                                break;

                            case 13:
                                return _context.abrupt("return", done("User is inactive", false));

                            case 14:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            return function (_x, _x2) {
                return _ref.apply(this, arguments);
            };
        }());
    });
    passport.use(strategy);
    return {
        initialize: function initialize() {
            return passport.initialize();
        },
        authenticate: function authenticate() {
            return function (req, res, next) {
                passport.authenticate('jwt', function (err, data, info) {
                    if (err || info || !data) {
                        res.status(401).send({
                            error: "Authorization Required."
                        });
                    } else {
                        req['user'] = data;
                        next();
                    }
                })(req, res, next);
            };
        }
    };
}();

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});