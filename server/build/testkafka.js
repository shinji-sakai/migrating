'use strict';

var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    KeyedMessage = kafka.KeyedMessage,
    client = new kafka.Client("ip-10-0-0-81.ap-southeast-1.compute.internal:2181,ip-10-0-0-126.ap-southeast-1.compute.internal:2181,ip-10-0-0-193.ap-southeast-1.compute.internal:2181"),
    producer = new Producer(client),

//    km = new KeyedMessage('key', 'message'),
payloads = [{ topic: 'userstats', messages: 'hi', partition: 0 }];
producer.on('ready', function () {
    producer.send(payloads, function (err, data) {
        console.log(data);
    });
});

producer.on('error', function (err) {});