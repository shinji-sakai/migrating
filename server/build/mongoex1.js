'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by jeevan on 1/19/2016.
 */

var MongoClient = require('mongodb').MongoClient,
    co = require('co'),
    assert = require('assert');

co(_regenerator2.default.mark(function _callee() {
    var db, r;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    _context.next = 2;
                    return MongoClient.connect('mongodb://localhost:27017/edw');

                case 2:
                    db = _context.sent;

                    console.log("Connected correctly to server");

                    // Insert a single document
                    _context.next = 6;
                    return db.collection('jeevan').insertOne({ a: 1 });

                case 6:
                    r = _context.sent;

                    assert.equal(2, r.insertedCount);

                    // Insert multiple documents
                    _context.next = 10;
                    return db.collection('jeevan').insertMany([{ a: 2 }, { a: 3 }]);

                case 10:
                    r = _context.sent;

                    assert.equal(2, r.insertedCount);

                    // Close connection
                    db.close();

                case 13:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this);
})).catch(function (err) {
    console.log(err.stack);
});