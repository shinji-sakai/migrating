'use strict';

require('./api/kafka/consumer_useridle.js');
// require('./api/kafka/consumer_test_performance.js');
require('./api/kafka/consumer_userstats.js');
require('./api/kafka/consumer_videostats.js');
require('./api/kafka/consumer_question_stats.js');
require('./api/kafka/consumer_userclick.js');