'use strict';

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configPath = require('./configPath.js');
var cassExecute = require(configPath.lib.utility).cassExecute;
var kafka = require('kafka-node'),
    config = require('./config.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(client, [{ topic: 'userstats' }], {
    groupId: 'my-group'
});
console.log("Consumer ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
consumer.on('message', function (message) {
    // console.log(message);
    var columns = [];
    var values = [];

    if (message.value) {
        //convert to json
        var msg = JSON.parse(message.value);
        //get all parent keys
        var parentKeys = Object.keys(msg || {});
        //iterate through parent keys
        for (var i = 0; i < parentKeys.length; i++) {
            var key = parentKeys[i];
            var val = msg[key];

            if ((typeof val === 'undefined' ? 'undefined' : (0, _typeof3.default)(val)) === "object") {
                //parent key value is object
                var childObject = val;
                //get all child keys
                var childKeys = Object.keys(childObject || {});
                //concat into columns array
                columns = columns.concat(childKeys);
                //push all columns(childkeys) value
                childKeys.forEach(function (v, i) {
                    var v = childObject[v];
                    values.push('' + v);
                });
            } else {
                //if parent key value is other than object
                columns.push(key);
                values.push('' + val);
            }
        }
    }

    columns.push("offset");
    values.push('' + message.offset);

    columns.push("partition");
    values.push('' + message.partition);

    columns.push("key");
    values.push('' + message.key);

    //convert column into (col1,col2,....)
    var column_str = columns.join(",");

    //convert value into ('val1','val2',......)
    var value_str = '(';
    values.forEach(function (v, i) {
        value_str += '\'' + v + '\',';
    });
    value_str = value_str.substring(0, value_str.length - 1);
    value_str += ')';

    insertIntoDb(column_str, value_str);
});

function insertIntoDb(columns, value) {}