
// var editor = window.contactMsgEditor = textboxio.replace('#msg123', {
//     css: {
//         stylesheets: [''],
//         styles: [{
//             rule: 'p',
//             text: 'Paragraph'
//         }, {
//             rule: 'h1',
//             text: 'H1'
//         }, {
//             rule: 'h2',
//             text: 'H2'
//         }, {
//             rule: 'h3',
//             text: 'H3'
//         }, {
//             rule: 'h4',
//             text: 'H4'
//         }, {
//             rule: 'div',
//             text: 'div'
//         }, {
//             rule: 'pre',
//             text: 'pre'
//         }]
//     },
//     codeview: {
//         enabled: false,
//         showButton: false
//     },
//     languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
//     paste: {
//         style: 'prompt'
//     },
//     ui: {
//         toolbar: {
//             items: ['undo', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
//         }
//     }
// });

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$("input[type=text]").on("input", function(e) {
    if ($(e.target).val().length <= 0) {
        $(e.target).parent().addClass("error");
    } else {
        $(e.target).parent().removeClass("error");
    }
})
$("input[type=email]").on("input", function(e) {
    if ($(e.target).val().length <= 0) {
        $(e.target).parent().addClass("error");
    } else {
        $(e.target).parent().removeClass("error");
    }
})

function validatePhoneNumber(no) {
    var re = /^\d+$/;
    return re.test(no);
}

window.submitForm = function submitForm(e) {
    $(".form").submit();
    return false;
}

$(function() {
    $('.form').submit(function(e) {
        e.preventDefault();
        var dataArr = $(".form").serializeArray();
        var data = {};
        dataArr.forEach(function(v, i) {
            data[v.name] = v.value;
        });

        $("#fst_nm").removeClass("error");
        $("#email").removeClass("error");
        $("#ph_no").removeClass("error");
        $("#msg").removeClass("error");

        var isError = false;
        if (!data.fst_nm) {
            $("#fst_nm").addClass("error");
            isError = true;
        }
        if (!data.email || !validateEmail(data.email)) {
            $("#email").addClass("error");
            isError = true;
        }
        if (!data.ph_no || !validatePhoneNumber(data.ph_no)) {
            $("#ph_no").addClass("error");
            isError = true;
        }

        // var html = window.contactMsgEditor.content.get();
        var msgText = data.msg;

        if (!msgText || msgText.length < 25) {
            $("#msg").addClass("error");
            isError = true;
        }

        if (isError) {
            return false;
        }

        $("#msg").removeClass("error");

        $("#submit").attr("disabled", "disabled");
        $(".submit-btn").addClass("disabled").find(".text").html("Submitting...")
        $.ajax({
            url: '/contact/saveContactMsg',
            data: data,
            method: 'post',
            success: function() {
                $(".form").trigger("reset");
                $("#success").show();
                $("#error").hide();
                $("#submit").removeAttr("disabled");
                $(".submit-btn").removeClass("disabled").find(".text").html("Submit")
                // editor.content.set('<p></p>');
                setTimeout(function() {
                    $("#success").hide();
                    $("#error").hide();
                }, 8000)
            },
            error: function() {
                $("#error").show();
                $("#success").hide();
                $("#submit").removeAttr("disabled");
                $(".submit-btn").removeClass("disabled").find(".text").html("Submit")
                setTimeout(function() {
                    $("#success").hide();
                    $("#error").hide();
                }, 8000)
            }
        });

        return false;
    });
})