$(function() {
    function isAuthenticated() {
        return window.ew.auth.isAuthenticated();
    }

    // Hide Unauthorized link
    (function() {
        var isLoggedIn = isAuthenticated();
        $("[data-auth='true']").each(function(i, block) {
            if (!isLoggedIn) {
                $(block).css("display", "none");
            }else{
                $(block).removeAttr("data-auth");
            }
        });
        $("[data-auth='false']").each(function(i, block) {
            if (isLoggedIn) {
                $(block).css("display", "none");
            }else{
                $(block).removeAttr("data-auth");
            }
        });
    })();

    function getUserId() {
        if(isAuthenticated()){
            return window.ew.auth.getUser()["usr_id"];
        }else{
            return '';
        }
    }

    (function() {
        //set user pic
        if(isAuthenticated()){
            var pic = window.ew.auth.getUser()["usr_pic"];
            if (pic) {
                $(".user-pic-img").attr("src", pic);
            }
        }
    })();

    function getUserName() {
        if(isAuthenticated()){
            return window.ew.auth.getUser()["dsp_nm"] || "";
        }else{
            return "";
        }
    }

    //Set Username
    (function() {
        var name = getUserName();
        var MAX_CHAR = 10;
        if (name.length > MAX_CHAR) {
            name = name.substr(0, MAX_CHAR) + "...";
        }
        $(".username").html(name);
    })();

    //getCoursesData
    (function() {
        var isAuthHeader = false;
        if(window.ew.auth.getToken()){
            isAuthHeader = true;
        }
        $.ajax({
            url: '/course/getAll',
            method: 'post',
            headers : {
                'x-session-id' : window.ew.auth.getSessionId(),
                'Authorization' : (isAuthHeader) ? 'JWT ' + window.ew.auth.getToken() : undefined,
                'x-track-session' : 'true'
            },
            success: function(courses,textStatus, request) {
                fillHTMLDOMWithCourses(courses, 'it');
                var shouldLogout = request.getResponseHeader('shouldLogout');
                if(shouldLogout){
                    window.location.href = "/logout/session-expired"
                }

                var sessId = request.getResponseHeader('sess-id');
                if(sessId){
                    window.ew.auth.setSessionId("\""+ sessId + "\"")
                }
                // fillHTMLDOMWithCourses(courses,'board');
                // fillHTMLDOMWithCourses(courses,'competitive');
            },
            error: function(err) {
                console.log(err);
            }
        })
    })();

    function fillHTMLDOMWithCourses(courses, type) {
        var arrDom = [];
        courses.forEach(function(v, i) {
            if (v.courseType === type) {
                var a = $("<a></a>");
                var link = '/' + type + '/' + v.courseSubGroup + '-trainings/' + v.courseId;
                a.attr("href", link);
                a.addClass("item white");
                a.html(v.courseName);
                var li = $("<li></li>").append(a);
                if (arrDom.length <= 5) {
                    arrDom.push(li);
                }
            }
        });
        if (arrDom.length > 0) {
            var a = $("<a></a>");
            var link = '/' + type + '-trainings';
            a.attr("href", link);
            a.addClass("item white text_right");
            a.html("All Courses");
            var li = $("<li></li>").append(a);
            arrDom.push(li);

            $("#" + type + "-courses").html(arrDom);
        }
    }
});
