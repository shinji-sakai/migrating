$(function() {
	window.ewSendIdleEvent = false;
	$.ajax({
        url: '/userstats/shouldFetchIdleEvents',
        method: 'POST',
        data: {
            page_url: window.location.pathname
        }
    })
    .done(function(data) {
    	window.ewSendIdleEvent = data.shouldFetch;
    })
    .fail(function(data) {
    	console.log(data);
    })
})