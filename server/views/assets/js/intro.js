(function() {
	function init() {
		var isLoggedIn = isAuthenticated();
		$("[data-auth='true']").each(function(i,block) {
			if(!isLoggedIn){
				$(block).css("display","none");
			}
		});
		$("[data-auth='false']").each(function(i,block) {
			if(isLoggedIn){
				$(block).css("display","none");
			}
		});
	}

	init();

	function isAuthenticated() {
		var tokenKey = "ls.dsToken";
		var userKey = "ls.dsUser";
		return !!localStorage[tokenKey] && !!localStorage[userKey];
	}
	
	$(".play-apple").on("click",function(e){
		var ele = $(".video-modal-wrapper");
		var isOpen = $(ele).hasClass('ds-open');
		if(isOpen){
			$(ele).removeClass('ds-open').addClass('ds-close');
		}else{
			$(ele).addClass('ds-open').removeClass('ds-close');
		}
	});
	$(".video-close-btn").on("click",function(){
		var ele = $(".video-modal-wrapper");
		$(ele).removeClass('ds-open').addClass('ds-close');

		var player_wrapper = $(ele).find(".custom-player-wrapper").children()[0];
		var player_id = $(player_wrapper).attr('id');
		var player = jwplayers[player_id];
		// console.log(,findParent($("#")));
		removeJWPlayer(player_id);

		//player current time
		var currentPosition = player.getPosition();
		//player duration
		var duration = player.getDuration();
		//percentage
		var percentage = (currentPosition/duration)*100;

		var user = JSON.parse(localStorage['ls.dsUser'] || {});
		var usr_id = "";
		if(user.usr_id){
			usr_id = user.usr_id;
		}
		var data = {
			type : 'close',
			data : percentage,
			hashToken : localStorage['ls.dsHashToken'],
			usr_id  : usr_id
		}
		$.ajax({
	        url : '/videostats/insertStats/',
	        method : 'POST',
	        data : data
	    });
	})
})();