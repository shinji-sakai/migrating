(function(){

	function init() {
		var isLoggedIn = isAuthenticated();
		$("[data-auth='true']").each(function(i,block) {
			if(!isLoggedIn){
				$(block).css("display","none");
			}
		});
		$("[data-auth='false']").each(function(i,block) {
			if(isLoggedIn){
				$(block).css("display","none");
			}
		});
	}

	init();

	function isAuthenticated() {
		var tokenKey = "ls.dsToken";
		var userKey = "ls.dsUser";
		return !!localStorage[tokenKey] && !!localStorage[userKey];
	}
})();