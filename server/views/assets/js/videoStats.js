function VideoStats(player){
	//player instance
	this.player = player;
	//play pause button click tracking
	this.play_pause_btn_clicked = [];

	//video seek event stats
	this.video_seek_events = [];

	//volume change event stats
	this.volume_change_events = [];

	//bind event callback
	this.bindEvents();
}

VideoStats.prototype.init = function(){

}

VideoStats.prototype.bindEvents = function(){
	var self = this;

	//play event callback
	this.player.on('play',function(){

		//player current time
		var currentPosition = self.player.getPosition();
		//player duration
		var duration = self.player.getDuration();
		//percentage
		var percentage = (currentPosition/duration)*100;

		self.play_pause_btn_clicked.push(percentage);

		var data = {
			type : 'play',
			data : percentage
		}
		self.sendStats(data);
	})

	//pause event callback
	this.player.on('pause',function(){

		//player current time
		var currentPosition = self.player.getPosition();
		//player duration
		var duration = self.player.getDuration();
		//percentage
		var percentage = (currentPosition/duration)*100;

		self.play_pause_btn_clicked.push(percentage);

		var data = {
			type : 'pause',
			data : percentage
		}
		self.sendStats(data);
	})

	//seek event callback
	this.player.on('seeked',function(){
		//player current time
		var currentPosition = self.player.getPosition();
		//player duration
		var duration = self.player.getDuration();
		//percentage
		var percentage = (currentPosition/duration)*100;
		//video seek event stats added to array
        self.video_seek_events.push(percentage);

        var data = {
			type : 'seek',
			data : percentage
		}
		self.sendStats(data);
	});

	//volume change event callback
	this.player.on("volume",function(){
		var volume = self.player.getVolume();
		//volume change event happen and added to array
		self.volume_change_events.push(volume);
		console.log("volume...",self.volume_change_events);

		var data = {
			type : 'volumechange',
			data: volume
		}
		self.sendStats(data);
	});
}

VideoStats.prototype.sendStats = function(data){
	data.hashToken = localStorage['ls.dsHashToken'];
	var user = JSON.parse(localStorage['ls.dsUser'] || {});
	if(user.usr_id){
		data.usr_id = user.usr_id;
	}
	$.ajax({
        url : '/videostats/insertStats/',
        method : 'POST',
        data : data
    });
}

window.VideoStats = VideoStats;
