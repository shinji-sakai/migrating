$( function () {
  $('body').on( 'click', '.jsTriggerMenu', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $('.jsMainNav').toggleClass('show_nav');
      $('body').removeClass('src_show');
      $('.jsSrcShow').slideUp();
  });
  $(document).on( 'click', function (e) {
      if($(e.target).closest('.jsMainNav, .jsTriggerMenu').length) {
        return;
      }
      $('.jsTriggerMenu').removeClass('active');
      $('.jsMainNav').removeClass('show_nav');
      e.stopPropagation();
  });

  $('body').on( 'click', '.jsTilteCollapse', function (e) {
      e.preventDefault();
      $(this).toggleClass('show_sub_nav');
      $(this).closest('.jsNavShow').find('.jsSubNav').slideToggle();
  });

  $('body').on( 'click', '.jsSrcBt', function (e) {
      e.preventDefault();
      $('body').toggleClass('src_show');
      $('.jsSrcShow').slideToggle();
      $('.jsMainNav').removeClass('show_nav');
  });

  $('body').on( 'click', '.jsToogleDetails, .jsCloseDetails', function (e) {
      e.preventDefault();
      $('.jsDetails').toggleClass('active');
  });
  $('.close_box').click(function(){
    $('.modal.show').modal('toggle');
  });

  $(document).on('click', '.jsSrcBtn', function () {
    $(this).closest('.wrap_search').addClass('active');
  })
  $(document).on('click', '.jsCloseSearch', function () {
    $(this).closest('.wrap_search').removeClass('active');
  })
});