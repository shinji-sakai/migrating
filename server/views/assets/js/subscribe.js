function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
var subs_typ;

function resetSubscribeModal() {
    $(".subscribe-input").show();
    $(".subscribe-btn").show();
    $(".success-msg").hide();
    $(".error-msg").hide();
    $(".modal-content input").val('');
    $(".modal-content input").removeClass("error");
    $(".input-error-msg").hide();
}

function subscribeUser(subs_typ,btn_ele) {
    
    var input_ele = $(btn_ele).closest(".modal-content").find("input");
    
    $(".success-msg").hide();
    $(".error-msg").hide();
    var input = $(input_ele).val();
    if (input.length <= 0 || !validateEmail(input)) {
        $(input_ele).addClass("error");
        $(".input-error-msg").show();
        return;
    }
    $(input_ele).removeClass("error");
    $(".input-error-msg").hide();
    $.ajax({
        url: '/mail/subscribeUser',
        method: 'post',
        data: {
            subs_email: input,
            subs_typ: subs_typ
        },
        success: function(res) {
            console.log(res);
            $(".success-msg").show();
            $(".subscribe-input").hide();
            $(".subscribe-btn").hide();
        },
        error: function(err) {
            console.error(err);
            $(".error-msg").show();
        }
    })
}

function onSubscribeInputChange(ele) {
    var input = $(ele).val();
    if (input.length <= 0 || !validateEmail(input)) {
        $(ele).addClass("error");
        $(".input-error-msg").show();
    } else {
        $(ele).removeClass("error");
        $(".input-error-msg").hide();
    }
}
// $(function() {
//     $(document).on('keyup',"#subscribe-tf", function() {
//         var input = $(":focus").val();
//         console.log(input)
//         if (input.length <= 0 || !validateEmail(input)) {
//             $(":focus").addClass("error");
//             $(".input-error-msg").show();
//         } else {
//             $(":focus").removeClass("error");
//             $(".input-error-msg").hide();
//         }
//     })
// })
