$(document).on('click',function(){
    if(isMenuModalOpen){
        closeMenu();
    }
});

function init() {
    var isLoggedIn = isAuthenticated();
    $("[data-auth='true']").each(function(i,block) {
        if(!isLoggedIn){
            $(block).css("display","none");
        }
    });
    $("[data-auth='false']").each(function(i,block) {
        if(isLoggedIn){
            $(block).css("display","none");
        }
    });
}

init();

$('.menu-modal-component').on('click',function(event){
    console.log(event);
    event.stopPropagation();
})

var isMenuModalOpen = false;
//toggle entire menu modal
function toggleMenuModal(event) {
    console.log("toggle");
    event.stopPropagation();
    if (!isMenuModalOpen) {
        //It is not opened
        //Now it is going to open
        openMenu()
    } else {
        //if it is opened
        //then close it
        closeMenu();
    }
}

function closeMenu(){
    $("#menu-modal").addClass('ds-close').removeClass('ds-open');
    isMenuModalOpen = false;
}
function openMenu(){
    $("#menu-modal").addClass('ds-open').removeClass('ds-close');
    isMenuModalOpen = true;
}




//toggle hidden menu
function toggleHiddenMenu(ele) {
    var target = $(ele);
    //find from clicked
    var menu = $(target).find('.hidden-menu');

    //if not found any hidden element
    if (menu.length === 0) {
        //try from finding from parent
        menu = $(target).parent().siblings('.hidden-menu');
    }

    if(target.tagName === 'A'){
        console.log("toggling",target);
        $event.stopPropagation();
        // don't toggle
        return;
    }
    
    //hide all menu with 'hidden-menu' class
    $('#menu-modal').find('.hidden-menu').each(function(i, v) {
        if ($(v)[0] != $(menu)[0]) {
            $(v).hide();
        }
    });
    
    $(menu).toggle();    
    
}


function isAuthenticated() {
    var tokenKey = "ls.dsToken";
    var userKey = "ls.dsUser";
    return !!localStorage[tokenKey] && !!localStorage[userKey];
}
function getUserId() {
    try{
        var userKey = "ls.dsUser";
        var obj = localStorage[userKey] || {};
        var jsonObj = JSON.parse(obj);
        return jsonObj["usr_id"];
    }catch(err){
        // console.log(err)
    }
}

function getCoursesData(type) {
    $.ajax({
        url : 'https://examwarrior.com/course/getAll',
        method : 'post',
        data : {userId : getUserId()},
        success : function(courses){
            fillHTMLDOMWithCourses(courses,'it');
            fillHTMLDOMWithCourses(courses,'board');
            fillHTMLDOMWithCourses(courses,'competitive');
        },
        error : function(err){
            console.log(err);
        }
    })
}
getCoursesData();

function fillHTMLDOMWithCourses(courses,type) {
    var arrDom = [];
    courses.forEach(function(v,i) {
        if(v.courseType === type){
            var a = $("<a></a>");
            var link = '/' + v.courseSubGroup + '-trainings/' + v.courseId;
            a.attr("href",link);
            a.addClass("item white");
            a.html(v.courseName);    
            arrDom.push(a);    
        }
    });
    if(arrDom.length > 0){
        $("#" + type + "-courses").html(arrDom);    
    }
}

function getUserCourses() {
    if(isAuthenticated()){
        $.ajax({
            url : '/uc/getPurchasedCourses',
            method : 'post',
            data : {userId : getUserId()},
            success : function(courses){
                var arrDom = [];
                courses.forEach(function(v,i) {
                    var a = $("<a></a>");
                    var link = '/courses/' + v.courseType + '/' + v.courseId;
                    a.attr("href",link);
                    a.html(v.courseName);    
                    arrDom.push(a);
                });
                $("#my-courses").html(arrDom);
            },
            error : function(err){
                console.log(err);
            }
        })
    }
}


getUserCourses();