/////////////////
// Platform JS //
/////////////////
(function() {
    "use strict";

    function m(a) {
        return a = String(a), a.charAt(0).toUpperCase() + a.slice(1)
    }

    function n(a, b, c) {
        var d = {
            "10.0": "10",
            6.4: "10 Technical Preview",
            6.3: "8.1",
            6.2: "8",
            6.1: "7 / Server 2008 R2",
            "6.0": "Vista / Server 2008",
            5.2: "XP 64-bit / Server 2003",
            5.1: "XP",
            5.01: "2000 SP1",
            "5.0": "2000",
            "4.0": "NT",
            "4.90": "ME"
        };
        return b && c && /^Win/i.test(a) && !/^Windows Phone /i.test(a) && (d = d[/[\d.]+$/.exec(a)]) && (a = "Windows " + d), a = String(a), b && c && (a = a.replace(RegExp(b, "i"), c)), a = p(a.replace(/ ce$/i, " CE").replace(/\bhpw/i, "web").replace(/\bMacintosh\b/, "Mac OS").replace(/_PowerPC\b/i, " OS").replace(/\b(OS X) [^ \d]+/i, "$1").replace(/\bMac (OS X)\b/, "$1").replace(/\/(\d)/, " $1").replace(/_/g, ".").replace(/(?: BePC|[ .]*fc[ \d.]+)$/i, "").replace(/\bx86\.64\b/gi, "x86_64").replace(/\b(Windows Phone) OS\b/, "$1").replace(/\b(Chrome OS \w+) [\d.]+\b/, "$1").split(" on ")[0])
    }

    function o(a, b) {
        var c = -1,
            d = a ? a.length : 0;
        if ("number" == typeof d && d > -1 && d <= g)
            for (; ++c < d;) b(a[c], c, a);
        else q(a, b)
    }

    function p(a) {
        return a = v(a), /^(?:webOS|i(?:OS|P))/.test(a) ? a : m(a)
    }

    function q(a, b) {
        for (var c in a) k.call(a, c) && b(a[c], c, a)
    }

    function r(a) {
        return null == a ? m(a) : l.call(a).slice(8, -1)
    }

    function s(a, b) {
        var c = null != a ? typeof a[b] : "number";
        return !(/^(?:boolean|number|string|undefined)$/.test(c) || "object" == c && !a[b])
    }

    function t(a) {
        return String(a).replace(/([ -])(?!$)/g, "$1?")
    }

    function u(a, b) {
        var c = null;
        return o(a, function(d, e) {
            c = b(c, d, e, a)
        }), c
    }

    function v(a) {
        return String(a).replace(/^ +| +$/g, "")
    }

    function w(a) {
        function T(b) {
            return u(b, function(b, c) {
                return b || RegExp("\\b" + (c.pattern || t(c)) + "\\b", "i").exec(a) && (c.label || c)
            })
        }

        function U(b) {
            return u(b, function(b, c, d) {
                return b || (c[Q] || c[/^[a-z]+(?: +[a-z]+\b)*/i.exec(Q)] || RegExp("\\b" + t(d) + "(?:\\b|\\w*\\d)", "i").exec(a)) && d
            })
        }

        function V(b) {
            return u(b, function(b, c) {
                return b || RegExp("\\b" + (c.pattern || t(c)) + "\\b", "i").exec(a) && (c.label || c)
            })
        }

        function W(b) {
            return u(b, function(b, c) {
                var d = c.pattern || t(c);
                return !b && (b = RegExp("\\b" + d + "(?:/[\\d.]+|[ \\w.]*)", "i").exec(a)) && (b = n(b, d, c.label || c)), b
            })
        }

        function X(b) {
            return u(b, function(b, c) {
                var d = c.pattern || t(c);
                return !b && (b = RegExp("\\b" + d + " *\\d+[.\\w_]*", "i").exec(a) || RegExp("\\b" + d + "(?:; *(?:[a-z]+[_-])?[a-z]+\\d+|[^ ();-]*)", "i").exec(a)) && ((b = String(c.label && !RegExp(d, "i").test(c.label) ? c.label : b).split("/"))[1] && !/[\d.]+/.test(b[0]) && (b[0] += " " + b[1]), c = c.label || c, b = p(b[0].replace(RegExp(d, "i"), c).replace(RegExp("; *(?:" + c + "[_-])?", "i"), " ").replace(RegExp("(" + c + ")[-_.]?(\\w)", "i"), "$1 $2"))), b
            })
        }

        function Y(b) {
            return u(b, function(b, c) {
                return b || (RegExp(c + "(?:-[\\d.]+/|(?: for [\\w-]+)?[ /-])([\\d.]+[^ ();/_-]*)", "i").exec(a) || 0)[1] || null
            })
        }

        function Z() {
            return this.description || ""
        }
        var d = b,
            e = a && "object" == typeof a && "String" != r(a);
        e && (d = a, a = null);
        var f = d.navigator || {},
            g = f.userAgent || "";
        a || (a = g);
        var H, N, j = e || i == c,
            k = e ? !!f.likeChrome : /\bChrome\b/.test(a) && !/internal|\n/i.test(l.toString()),
            m = "Object",
            o = e ? m : "ScriptBridgingProxyObject",
            x = e ? m : "Environment",
            y = e && d.java ? "JavaPackage" : r(d.java),
            z = e ? m : "RuntimeObject",
            A = /\bJava/.test(y) && d.java,
            B = A && r(d.environment) == x,
            C = A ? "a" : "α",
            D = A ? "b" : "β",
            E = d.document || {},
            F = d.operamini || d.opera,
            G = h.test(G = e && F ? F["[[Class]]"] : r(F)) ? G : F = null,
            I = a,
            J = [],
            K = null,
            L = a == g,
            M = L && F && "function" == typeof F.version && F.version(),
            O = T([{
                label: "EdgeHTML",
                pattern: "Edge"
            }, "Trident", {
                label: "WebKit",
                pattern: "AppleWebKit"
            }, "iCab", "Presto", "NetFront", "Tasman", "KHTML", "Gecko"]),
            P = V(["Adobe AIR", "Arora", "Avant Browser", "Breach", "Camino", "Epiphany", "Fennec", "Flock", "Galeon", "GreenBrowser", "iCab", "Iceweasel", "K-Meleon", "Konqueror", "Lunascape", "Maxthon", {
                label: "Microsoft Edge",
                pattern: "Edge"
            }, "Midori", "Nook Browser", "PaleMoon", "PhantomJS", "Raven", "Rekonq", "RockMelt", "SeaMonkey", {
                label: "Silk",
                pattern: "(?:Cloud9|Silk-Accelerated)"
            }, "Sleipnir", "SlimBrowser", {
                label: "SRWare Iron",
                pattern: "Iron"
            }, "Sunrise", "Swiftfox", "WebPositive", "Opera Mini", {
                label: "Opera Mini",
                pattern: "OPiOS"
            }, "Opera", {
                label: "Opera",
                pattern: "OPR"
            }, "Chrome", {
                label: "Chrome Mobile",
                pattern: "(?:CriOS|CrMo)"
            }, {
                label: "Firefox",
                pattern: "(?:Firefox|Minefield)"
            }, {
                label: "Firefox Mobile",
                pattern: "FxiOS"
            }, {
                label: "IE",
                pattern: "IEMobile"
            }, {
                label: "IE",
                pattern: "MSIE"
            }, "Safari"]),
            Q = X([{
                label: "BlackBerry",
                pattern: "BB10"
            }, "BlackBerry", {
                label: "Galaxy S",
                pattern: "GT-I9000"
            }, {
                label: "Galaxy S2",
                pattern: "GT-I9100"
            }, {
                label: "Galaxy S3",
                pattern: "GT-I9300"
            }, {
                label: "Galaxy S4",
                pattern: "GT-I9500"
            }, "Google TV", "Lumia", "iPad", "iPod", "iPhone", "Kindle", {
                label: "Kindle Fire",
                pattern: "(?:Cloud9|Silk-Accelerated)"
            }, "Nexus", "Nook", "PlayBook", "PlayStation 3", "PlayStation 4", "PlayStation Vita", "TouchPad", "Transformer", {
                label: "Wii U",
                pattern: "WiiU"
            }, "Wii", "Xbox One", {
                label: "Xbox 360",
                pattern: "Xbox"
            }, "Xoom"]),
            R = U({
                Apple: {
                    iPad: 1,
                    iPhone: 1,
                    iPod: 1
                },
                Amazon: {
                    Kindle: 1,
                    "Kindle Fire": 1
                },
                Asus: {
                    Transformer: 1
                },
                "Barnes & Noble": {
                    Nook: 1
                },
                BlackBerry: {
                    PlayBook: 1
                },
                Google: {
                    "Google TV": 1,
                    Nexus: 1
                },
                HP: {
                    TouchPad: 1
                },
                HTC: {},
                LG: {},
                Microsoft: {
                    Xbox: 1,
                    "Xbox One": 1
                },
                Motorola: {
                    Xoom: 1
                },
                Nintendo: {
                    "Wii U": 1,
                    Wii: 1
                },
                Nokia: {
                    Lumia: 1
                },
                Samsung: {
                    "Galaxy S": 1,
                    "Galaxy S2": 1,
                    "Galaxy S3": 1,
                    "Galaxy S4": 1
                },
                Sony: {
                    "PlayStation 4": 1,
                    "PlayStation 3": 1,
                    "PlayStation Vita": 1
                }
            }),
            S = W(["Windows Phone ", "Android", "CentOS", {
                label: "Chrome OS",
                pattern: "CrOS"
            }, "Debian", "Fedora", "FreeBSD", "Gentoo", "Haiku", "Kubuntu", "Linux Mint", "OpenBSD", "Red Hat", "SuSE", "Ubuntu", "Xubuntu", "Cygwin", "Symbian OS", "hpwOS", "webOS ", "webOS", "Tablet OS", "Linux", "Mac OS X", "Macintosh", "Mac", "Windows 98;", "Windows "]);
        if (O && (O = [O]), R && !Q && (Q = X([R])), (H = /\bGoogle TV\b/.exec(Q)) && (Q = H[0]), /\bSimulator\b/i.test(a) && (Q = (Q ? Q + " " : "") + "Simulator"), "Opera Mini" == P && /\bOPiOS\b/.test(a) && J.push("running in Turbo/Uncompressed mode"), /^iP/.test(Q) ? (P || (P = "Safari"), S = "iOS" + ((H = / OS ([\d_]+)/i.exec(a)) ? " " + H[1].replace(/_/g, ".") : "")) : "Konqueror" != P || /buntu/i.test(S) ? R && "Google" != R && (/Chrome/.test(P) && !/\bMobile Safari\b/i.test(a) || /\bVita\b/.test(Q)) ? (P = "Android Browser", S = /\bAndroid\b/.test(S) ? S : "Android") : "Silk" == P ? (/\bMobi/i.test(a) || (S = "Android", J.unshift("desktop mode")), /Accelerated *= *true/i.test(a) && J.unshift("accelerated")) : "PaleMoon" == P && (H = /\bFirefox\/([\d.]+)\b/.exec(a)) ? J.push("identifying as Firefox " + H[1]) : "Firefox" == P && (H = /\b(Mobile|Tablet|TV)\b/i.exec(a)) ? (S || (S = "Firefox OS"), Q || (Q = H[1])) : P && !(H = !/\bMinefield\b/i.test(a) && /\b(?:Firefox|Safari)\b/.exec(P)) || (P && !Q && /[\/,]|^[^(]+?\)/.test(a.slice(a.indexOf(H + "/") + 8)) && (P = null), (H = Q || R || S) && (Q || R || /\b(?:Android|Symbian OS|Tablet OS|webOS)\b/.test(S)) && (P = /[a-z]+(?: Hat)?/i.exec(/\bAndroid\b/.test(S) ? S : H) + " Browser")) : S = "Kubuntu", M || (M = Y(["(?:Cloud9|CriOS|CrMo|Edge|FxiOS|IEMobile|Iron|Opera ?Mini|OPiOS|OPR|Raven|Silk(?!/[\\d.]+$))", "Version", t(P), "(?:Firefox|Minefield|NetFront)"])), (H = "iCab" == O && parseFloat(M) > 3 && "WebKit" || /\bOpera\b/.test(P) && (/\bOPR\b/.test(a) ? "Blink" : "Presto") || /\b(?:Midori|Nook|Safari)\b/i.test(a) && !/^(?:Trident|EdgeHTML)$/.test(O) && "WebKit" || !O && /\bMSIE\b/i.test(a) && ("Mac OS" == S ? "Tasman" : "Trident") || "WebKit" == O && /\bPlayStation\b(?! Vita\b)/i.test(P) && "NetFront") && (O = [H]), "IE" == P && (H = (/; *(?:XBLWP|ZuneWP)(\d+)/i.exec(a) || 0)[1]) ? (P += " Mobile", S = "Windows Phone " + (/\+$/.test(H) ? H : H + ".x"), J.unshift("desktop mode")) : /\bWPDesktop\b/i.test(a) ? (P = "IE Mobile", S = "Windows Phone 8.x", J.unshift("desktop mode"), M || (M = (/\brv:([\d.]+)/.exec(a) || 0)[1])) : "IE" != P && "Trident" == O && (H = /\brv:([\d.]+)/.exec(a)) && (P && J.push("identifying as " + P + (M ? " " + M : "")), P = "IE", M = H[1]), L) {
            if (s(d, "global"))
                if (A && (H = A.lang.System, I = H.getProperty("os.arch"), S = S || H.getProperty("os.name") + " " + H.getProperty("os.version")), j && s(d, "system") && (H = [d.system])[0]) {
                    S || (S = H[0].os || null);
                    try {
                        H[1] = d.require("ringo/engine").version, M = H[1].join("."), P = "RingoJS"
                    } catch (a) {
                        H[0].global.system == d.system && (P = "Narwhal")
                    }
                } else "object" == typeof d.process && (H = d.process) ? (P = "Node.js", I = H.arch, S = H.platform, M = /[\d.]+/.exec(H.version)[0]) : B && (P = "Rhino");
            else r(H = d.runtime) == o ? (P = "Adobe AIR", S = H.flash.system.Capabilities.os) : r(H = d.phantom) == z ? (P = "PhantomJS", M = (H = H.version || null) && H.major + "." + H.minor + "." + H.patch) : "number" == typeof E.documentMode && (H = /\bTrident\/(\d+)/i.exec(a)) && (M = [M, E.documentMode], (H = +H[1] + 4) != M[1] && (J.push("IE " + M[1] + " mode"), O && (O[1] = ""), M[1] = H), M = "IE" == P ? String(M[1].toFixed(1)) : M[0]);
            S = S && p(S)
        }
        M && (H = /(?:[ab]|dp|pre|[ab]\d+pre)(?:\d+\+?)?$/i.exec(M) || /(?:alpha|beta)(?: ?\d)?/i.exec(a + ";" + (L && f.appMinorVersion)) || /\bMinefield\b/i.test(a) && "a") && (K = /b/i.test(H) ? "beta" : "alpha", M = M.replace(RegExp(H + "\\+?$"), "") + ("beta" == K ? D : C) + (/\d+\+?/.exec(H) || "")), "Fennec" == P || "Firefox" == P && /\b(?:Android|Firefox OS)\b/.test(S) ? P = "Firefox Mobile" : "Maxthon" == P && M ? M = M.replace(/\.[\d.]+/, ".x") : /\bXbox\b/i.test(Q) ? (S = null, "Xbox 360" == Q && /\bIEMobile\b/.test(a) && J.unshift("mobile mode")) : !/^(?:Chrome|IE|Opera)$/.test(P) && (!P || Q || /Browser|Mobi/.test(P)) || "Windows CE" != S && !/Mobi/i.test(a) ? "IE" == P && L && null === d.external ? J.unshift("platform preview") : (/\bBlackBerry\b/.test(Q) || /\bBB10\b/.test(a)) && (H = (RegExp(Q.replace(/ +/g, " *") + "/([.\\d]+)", "i").exec(a) || 0)[1] || M) ? (H = [H, /BB10/.test(a)], S = (H[1] ? (Q = null, R = "BlackBerry") : "Device Software") + " " + H[0], M = null) : this != q && "Wii" != Q && (L && F || /Opera/.test(P) && /\b(?:MSIE|Firefox)\b/i.test(a) || "Firefox" == P && /\bOS X (?:\d+\.){2,}/.test(S) || "IE" == P && (S && !/^Win/.test(S) && M > 5.5 || /\bWindows XP\b/.test(S) && M > 8 || 8 == M && !/\bTrident\b/.test(a))) && !h.test(H = w.call(q, a.replace(h, "") + ";")) && H.name && (H = "ing as " + H.name + ((H = H.version) ? " " + H : ""), h.test(P) ? (/\bIE\b/.test(H) && "Mac OS" == S && (S = null), H = "identify" + H) : (H = "mask" + H, P = G ? p(G.replace(/([a-z])([A-Z])/g, "$1 $2")) : "Opera", /\bIE\b/.test(H) && (S = null), L || (M = null)), O = ["Presto"], J.push(H)) : P += " Mobile", (H = (/\bAppleWebKit\/([\d.]+\+?)/i.exec(a) || 0)[1]) && (H = [parseFloat(H.replace(/\.(\d)$/, ".0$1")), H], "Safari" == P && "+" == H[1].slice(-1) ? (P = "WebKit Nightly", K = "alpha", M = H[1].slice(0, -1)) : M != H[1] && M != (H[2] = (/\bSafari\/([\d.]+\+?)/i.exec(a) || 0)[1]) || (M = null), H[1] = (/\bChrome\/([\d.]+)/i.exec(a) || 0)[1], 537.36 == H[0] && 537.36 == H[2] && parseFloat(H[1]) >= 28 && "WebKit" == O && (O = ["Blink"]), L && (k || H[1]) ? (O && (O[1] = "like Chrome"), H = H[1] || (H = H[0], H < 530 ? 1 : H < 532 ? 2 : H < 532.05 ? 3 : H < 533 ? 4 : H < 534.03 ? 5 : H < 534.07 ? 6 : H < 534.1 ? 7 : H < 534.13 ? 8 : H < 534.16 ? 9 : H < 534.24 ? 10 : H < 534.3 ? 11 : H < 535.01 ? 12 : H < 535.02 ? "13+" : H < 535.07 ? 15 : H < 535.11 ? 16 : H < 535.19 ? 17 : H < 536.05 ? 18 : H < 536.1 ? 19 : H < 537.01 ? 20 : H < 537.11 ? "21+" : H < 537.13 ? 23 : H < 537.18 ? 24 : H < 537.24 ? 25 : H < 537.36 ? 26 : "Blink" != O ? "27" : "28")) : (O && (O[1] = "like Safari"), H = H[0], H = H < 400 ? 1 : H < 500 ? 2 : H < 526 ? 3 : H < 533 ? 4 : H < 534 ? "4+" : H < 535 ? 5 : H < 537 ? 6 : H < 538 ? 7 : H < 601 ? 8 : "8"), O && (O[1] += " " + (H += "number" == typeof H ? ".x" : /[.+]/.test(H) ? "" : "+")), "Safari" == P && (!M || parseInt(M) > 45) && (M = H)), "Opera" == P && (H = /\bzbov|zvav$/.exec(S)) ? (P += " ", J.unshift("desktop mode"), "zvav" == H ? (P += "Mini", M = null) : P += "Mobile", S = S.replace(RegExp(" *" + H + "$"), "")) : "Safari" == P && /\bChrome\b/.exec(O && O[1]) && (J.unshift("desktop mode"), P = "Chrome Mobile", M = null, /\bOS X\b/.test(S) ? (R = "Apple", S = "iOS 4.3+") : S = null), M && 0 == M.indexOf(H = /[\d.]+$/.exec(S)) && a.indexOf("/" + H + "-") > -1 && (S = v(S.replace(H, ""))), O && !/\b(?:Avant|Nook)\b/.test(P) && (/Browser|Lunascape|Maxthon/.test(P) || "Safari" != P && /^iOS/.test(S) && /\bSafari\b/.test(O[1]) || /^(?:Adobe|Arora|Breach|Midori|Opera|Phantom|Rekonq|Rock|Sleipnir|Web)/.test(P) && O[1]) && (H = O[O.length - 1]) && J.push(H), J.length && (J = ["(" + J.join("; ") + ")"]), R && Q && Q.indexOf(R) < 0 && J.push("on " + R), Q && J.push((/^on /.test(J[J.length - 1]) ? "" : "on ") + Q), S && (H = / ([\d.+]+)$/.exec(S) || (N = /^[a-z]+ ([\d.+]+) \//i.exec(S)), S = {
            architecture: 32,
            family: H && !N ? S.replace(H[0], "") : S,
            version: H ? H[1] : null,
            toString: function() {
                var a = this.version;
                return this.family + (a && !N ? " " + a : "") + (64 == this.architecture ? " 64-bit" : "")
            }
        }), (H = /\b(?:AMD|IA|Win|WOW|x86_|x)64\b/i.exec(I)) && !/\bi686\b/i.test(I) && (S && (S.architecture = 64, S.family = S.family.replace(RegExp(" *" + H), "")), P && (/\bWOW64\b/i.test(a) || L && /\w(?:86|32)$/.test(f.cpuClass || f.platform) && !/\bWin64; x64\b/i.test(a)) && J.unshift("32-bit")), a || (a = null);
        var $ = {};
        return $.description = a, $.layout = O && O[0], $.manufacturer = R, $.name = P, $.prerelease = K, $.product = Q, $.ua = a, $.version = P && M, $.os = S || {
            architecture: null,
            family: null,
            version: null,
            toString: function() {
                return "null"
            }
        }, $.parse = w, $.toString = Z, $.version && J.unshift(M), $.name && J.unshift(P), S && P && (S != String(S).split(" ")[0] || S != P.split(" ")[0] && !Q) && J.push(Q ? "(" + S + ")" : "on " + S), J.length && ($.description = J.join(" ")), $
    }
    var a = {
            function: !0,
            object: !0
        },
        b = a[typeof window] && window || this,
        c = b,
        d = a[typeof exports] && exports,
        e = a[typeof module] && module && !module.nodeType && module,
        f = d && e && "object" == typeof global && global;
    !f || f.global !== f && f.window !== f && f.self !== f || (b = f);
    var g = Math.pow(2, 53) - 1,
        h = /\bOpera/,
        i = this,
        j = Object.prototype,
        k = j.hasOwnProperty,
        l = j.toString;
    "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function() {
        return w()
    }) : d && e ? q(w(), function(a, b) {
        d[b] = a
    }) : b.platform = w()
}).call(this);
////////////////
// Global JS //
//////////////
function isSupportsStorage() {
    try {
        window.localStorage.setItem('test', "test");
        window.localStorage.removeItem('test');
        return true;
    } catch (e) {
        return false;
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function deleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function setCookie(name, value) {
    document.cookie = name + '=' + value;
}


/*
 * Convert a 32-bit number to a hex string with ls-byte first
 */
var hex_chr = "0123456789abcdef";

function rhex(num) {
    str = "";
    for (j = 0; j <= 3; j++)
        str += hex_chr.charAt((num >> (j * 8 + 4)) & 0x0F) +
        hex_chr.charAt((num >> (j * 8)) & 0x0F);
    return str;
}

/*
 * Convert a string to a sequence of 16-word blocks, stored as an array.
 * Append padding bits and the length, as described in the MD5 standard.
 */
function str2blks_MD5(str) {
    nblk = ((str.length + 8) >> 6) + 1;
    blks = new Array(nblk * 16);
    for (i = 0; i < nblk * 16; i++) blks[i] = 0;
    for (i = 0; i < str.length; i++)
        blks[i >> 2] |= str.charCodeAt(i) << ((i % 4) * 8);
    blks[i >> 2] |= 0x80 << ((i % 4) * 8);
    blks[nblk * 16 - 2] = str.length * 8;
    return blks;
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally 
 * to work around bugs in some JS interpreters.
 */
function add(x, y) {
    var lsw = (x & 0xFFFF) + (y & 0xFFFF);
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left
 */
function rol(num, cnt) {
    return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * These functions implement the basic operation for each round of the
 * algorithm.
 */
function cmn(q, a, b, x, s, t) {
    return add(rol(add(add(a, q), add(x, t)), s), b);
}

function ff(a, b, c, d, x, s, t) {
    return cmn((b & c) | ((~b) & d), a, b, x, s, t);
}

function gg(a, b, c, d, x, s, t) {
    return cmn((b & d) | (c & (~d)), a, b, x, s, t);
}

function hh(a, b, c, d, x, s, t) {
    return cmn(b ^ c ^ d, a, b, x, s, t);
}

function ii(a, b, c, d, x, s, t) {
    return cmn(c ^ (b | (~d)), a, b, x, s, t);
}
/*
 * Take a string and return the hex representation of its MD5.
 */
function calcMD5(str) {
    x = str2blks_MD5(str);
    a = 1732584193;
    b = -271733879;
    c = -1732584194;
    d = 271733878;

    for (i = 0; i < x.length; i += 16) {
        olda = a;
        oldb = b;
        oldc = c;
        oldd = d;

        a = ff(a, b, c, d, x[i + 0], 7, -680876936);
        d = ff(d, a, b, c, x[i + 1], 12, -389564586);
        c = ff(c, d, a, b, x[i + 2], 17, 606105819);
        b = ff(b, c, d, a, x[i + 3], 22, -1044525330);
        a = ff(a, b, c, d, x[i + 4], 7, -176418897);
        d = ff(d, a, b, c, x[i + 5], 12, 1200080426);
        c = ff(c, d, a, b, x[i + 6], 17, -1473231341);
        b = ff(b, c, d, a, x[i + 7], 22, -45705983);
        a = ff(a, b, c, d, x[i + 8], 7, 1770035416);
        d = ff(d, a, b, c, x[i + 9], 12, -1958414417);
        c = ff(c, d, a, b, x[i + 10], 17, -42063);
        b = ff(b, c, d, a, x[i + 11], 22, -1990404162);
        a = ff(a, b, c, d, x[i + 12], 7, 1804603682);
        d = ff(d, a, b, c, x[i + 13], 12, -40341101);
        c = ff(c, d, a, b, x[i + 14], 17, -1502002290);
        b = ff(b, c, d, a, x[i + 15], 22, 1236535329);

        a = gg(a, b, c, d, x[i + 1], 5, -165796510);
        d = gg(d, a, b, c, x[i + 6], 9, -1069501632);
        c = gg(c, d, a, b, x[i + 11], 14, 643717713);
        b = gg(b, c, d, a, x[i + 0], 20, -373897302);
        a = gg(a, b, c, d, x[i + 5], 5, -701558691);
        d = gg(d, a, b, c, x[i + 10], 9, 38016083);
        c = gg(c, d, a, b, x[i + 15], 14, -660478335);
        b = gg(b, c, d, a, x[i + 4], 20, -405537848);
        a = gg(a, b, c, d, x[i + 9], 5, 568446438);
        d = gg(d, a, b, c, x[i + 14], 9, -1019803690);
        c = gg(c, d, a, b, x[i + 3], 14, -187363961);
        b = gg(b, c, d, a, x[i + 8], 20, 1163531501);
        a = gg(a, b, c, d, x[i + 13], 5, -1444681467);
        d = gg(d, a, b, c, x[i + 2], 9, -51403784);
        c = gg(c, d, a, b, x[i + 7], 14, 1735328473);
        b = gg(b, c, d, a, x[i + 12], 20, -1926607734);

        a = hh(a, b, c, d, x[i + 5], 4, -378558);
        d = hh(d, a, b, c, x[i + 8], 11, -2022574463);
        c = hh(c, d, a, b, x[i + 11], 16, 1839030562);
        b = hh(b, c, d, a, x[i + 14], 23, -35309556);
        a = hh(a, b, c, d, x[i + 1], 4, -1530992060);
        d = hh(d, a, b, c, x[i + 4], 11, 1272893353);
        c = hh(c, d, a, b, x[i + 7], 16, -155497632);
        b = hh(b, c, d, a, x[i + 10], 23, -1094730640);
        a = hh(a, b, c, d, x[i + 13], 4, 681279174);
        d = hh(d, a, b, c, x[i + 0], 11, -358537222);
        c = hh(c, d, a, b, x[i + 3], 16, -722521979);
        b = hh(b, c, d, a, x[i + 6], 23, 76029189);
        a = hh(a, b, c, d, x[i + 9], 4, -640364487);
        d = hh(d, a, b, c, x[i + 12], 11, -421815835);
        c = hh(c, d, a, b, x[i + 15], 16, 530742520);
        b = hh(b, c, d, a, x[i + 2], 23, -995338651);

        a = ii(a, b, c, d, x[i + 0], 6, -198630844);
        d = ii(d, a, b, c, x[i + 7], 10, 1126891415);
        c = ii(c, d, a, b, x[i + 14], 15, -1416354905);
        b = ii(b, c, d, a, x[i + 5], 21, -57434055);
        a = ii(a, b, c, d, x[i + 12], 6, 1700485571);
        d = ii(d, a, b, c, x[i + 3], 10, -1894986606);
        c = ii(c, d, a, b, x[i + 10], 15, -1051523);
        b = ii(b, c, d, a, x[i + 1], 21, -2054922799);
        a = ii(a, b, c, d, x[i + 8], 6, 1873313359);
        d = ii(d, a, b, c, x[i + 15], 10, -30611744);
        c = ii(c, d, a, b, x[i + 6], 15, -1560198380);
        b = ii(b, c, d, a, x[i + 13], 21, 1309151649);
        a = ii(a, b, c, d, x[i + 4], 6, -145523070);
        d = ii(d, a, b, c, x[i + 11], 10, -1120210379);
        c = ii(c, d, a, b, x[i + 2], 15, 718787259);
        b = ii(b, c, d, a, x[i + 9], 21, -343485551);

        a = add(a, olda);
        b = add(b, oldb);
        c = add(c, oldc);
        d = add(d, oldd);
    }
    return rhex(a) + rhex(b) + rhex(c) + rhex(d);
}

////////////////////////
// FlashDetect Script //
////////////////////////
var FlashDetect = new function() {
    var self = this;
    self.installed = false;
    self.raw = "";
    self.major = -1;
    self.minor = -1;
    self.revision = -1;
    self.revisionStr = "";
    var activeXDetectRules = [{
        "name": "ShockwaveFlash.ShockwaveFlash.7",
        "version": function(obj) {
            return getActiveXVersion(obj);
        }
    }, {
        "name": "ShockwaveFlash.ShockwaveFlash.6",
        "version": function(obj) {
            var version = "6,0,21";
            try {
                obj.AllowScriptAccess = "always";
                version = getActiveXVersion(obj);
            } catch (err) {}
            return version;
        }
    }, {
        "name": "ShockwaveFlash.ShockwaveFlash",
        "version": function(obj) {
            return getActiveXVersion(obj);
        }
    }];
    var getActiveXVersion = function(activeXObj) {
        var version = -1;
        try {
            version = activeXObj.GetVariable("$version");
        } catch (err) {}
        return version;
    };
    var getActiveXObject = function(name) {
        var obj = -1;
        try {
            obj = new ActiveXObject(name);
        } catch (err) {
            obj = {
                activeXError: true
            };
        }
        return obj;
    };
    var parseActiveXVersion = function(str) {
        var versionArray = str.split(",");
        return {
            "raw": str,
            "major": parseInt(versionArray[0].split(" ")[1], 10),
            "minor": parseInt(versionArray[1], 10),
            "revision": parseInt(versionArray[2], 10),
            "revisionStr": versionArray[2]
        };
    };
    var parseStandardVersion = function(str) {
        var descParts = str.split(/ +/);
        var majorMinor = descParts[2].split(/\./);
        var revisionStr = descParts[3];
        return {
            "raw": str,
            "major": parseInt(majorMinor[0], 10),
            "minor": parseInt(majorMinor[1], 10),
            "revisionStr": revisionStr,
            "revision": parseRevisionStrToInt(revisionStr)
        };
    };
    var parseRevisionStrToInt = function(str) {
        return parseInt(str.replace(/[a-zA-Z]/g, ""), 10) || self.revision;
    };
    self.majorAtLeast = function(version) {
        return self.major >= version;
    };
    self.minorAtLeast = function(version) {
        return self.minor >= version;
    };
    self.revisionAtLeast = function(version) {
        return self.revision >= version;
    };
    self.versionAtLeast = function(major) {
        var properties = [self.major, self.minor, self.revision];
        var len = Math.min(properties.length, arguments.length);
        for (i = 0; i < len; i++) {
            if (properties[i] >= arguments[i]) {
                if (i + 1 < len && properties[i] == arguments[i]) {
                    continue;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
    };
    self.FlashDetect = function() {
        if (navigator.plugins && navigator.plugins.length > 0) {
            var type = 'application/x-shockwave-flash';
            var mimeTypes = navigator.mimeTypes;
            if (mimeTypes && mimeTypes[type] && mimeTypes[type].enabledPlugin && mimeTypes[type].enabledPlugin.description) {
                var version = mimeTypes[type].enabledPlugin.description;
                var versionObj = parseStandardVersion(version);
                self.raw = versionObj.raw;
                self.major = versionObj.major;
                self.minor = versionObj.minor;
                self.revisionStr = versionObj.revisionStr;
                self.revision = versionObj.revision;
                self.installed = true;
            }
        } else if (navigator.appVersion.indexOf("Mac") == -1 && window.execScript) {
            var version = -1;
            for (var i = 0; i < activeXDetectRules.length && version == -1; i++) {
                var obj = getActiveXObject(activeXDetectRules[i].name);
                if (!obj.activeXError) {
                    self.installed = true;
                    version = activeXDetectRules[i].version(obj);
                    if (version != -1) {
                        var versionObj = parseActiveXVersion(version);
                        self.raw = versionObj.raw;
                        self.major = versionObj.major;
                        self.minor = versionObj.minor;
                        self.revision = versionObj.revision;
                        self.revisionStr = versionObj.revisionStr;
                    }
                }
            }
        }
    }();
};
FlashDetect.JS_RELEASE = "1.0.4";

////////////////
// AuthScript //
////////////////
function Auth() {
    this.tokenKey = "ls.dsToken";
    this.hashTokenKey = "ls.dsHashToken";
    this.userKey = "ls.dsUser";
    this.prevPageKey = "ls.dsPrevPage";
    this.sessionKey = "ls.ewSess";
    this.pageToRedirectKey = "dsPageToRedirect";
}
Auth.prototype.isAuthenticated = function() {
    return !!this.getToken() && !!this.getUser();
}
Auth.prototype.getToken = function() {
    var token = this.get(this.tokenKey);
    if(token){
        token = token.slice(1,-1)
    }
    return token
}
Auth.prototype.setToken = function(value) {
    return this.set(this.tokenKey, value);
}
Auth.prototype.getSessionId = function() {
    var sess = this.get(this.sessionKey);
    if(sess){
        sess = sess.slice(1,-1)
    }
    return sess
}
Auth.prototype.setSessionId = function(value) {
    return this.set(this.sessionKey, value);
}
Auth.prototype.removeToken = function() {
    this.remove(this.tokenKey);
}

Auth.prototype.setHashToken = function(value) {
    return this.set(this.hashTokenKey, value);
}
Auth.prototype.getHashToken = function() {
    return this.get(this.hashTokenKey);
}
Auth.prototype.removeHashToken = function() {
    this.remove(this.hashTokenKey);
}

Auth.prototype.getUser = function() {
    var str = this.get(this.userKey);
    if (str) {
        str = JSON.parse(str);
    }
    return str;
}
Auth.prototype.setUser = function(user) {
    return this.set(this.userKey, user);
}
Auth.prototype.removeUser = function() {
    this.remove(this.userKey);
}

Auth.prototype.setPrevPage = function(value) {
    return sessionStorage[this.prevPageKey] = value;
}
Auth.prototype.getPrevPage = function() {
    return sessionStorage[this.prevPageKey];
}

Auth.prototype.setPageToRedirect = function(value) {
    return sessionStorage[this.pageToRedirectKey] = value;
}
Auth.prototype.getPageToRedirect = function() {
    return sessionStorage[this.pageToRedirectKey];
}

Auth.prototype.set = function(key, value) {
    if (isSupportsStorage()) {
        localStorage.setItem(key, value);
    } else {
        setCookie(key, value);
    }
}
Auth.prototype.get = function(key) {
    var token;
    if (isSupportsStorage()) {
        token = localStorage[key];
    } else {
        token = getCookie(key);
    }
    return token;
}
Auth.prototype.remove = function(key) {
    if (isSupportsStorage()) {
        localStorage.removeItem(key);
    } else {
        deleteCookie(key);
    }
}
var auth = new Auth();

// document.addEventListener('DOMContentLoaded', function() {

// });
window.ew = window.ew || {};
window.ew.isSupportsStorage = isSupportsStorage,
    window.ew.getCookie = getCookie,
    window.ew.deleteCookie = deleteCookie,
    window.ew.setCookie = setCookie,
    window.ew.calcMD5 = calcMD5,
    window.ew.platform = platform,
    window.ew.FlashDetect = FlashDetect,
    window.ew.auth = auth

// window.ew = Object.assign({},window.ew,{
//     window.ew.isSupportsStorage : isSupportsStorage,
//     window.ew.getCookie : getCookie,
//     window.ew.deleteCookie : deleteCookie,
//     window.ew.setCookie : setCookie,
//     window.ew.calcMD5 : calcMD5,
//     window.ew.platform : platform,
//     window.ew.FlashDetect : FlashDetect,
//     window.ew.auth : auth
// });
CalculateStats();


function CalculateStats() {
    var auth = window.ew.auth;
    var platform = window.ew.platform;
    var FlashDetect = window.ew.FlashDetect;
    var calcMD5 = window.ew.calcMD5;

    function Stats() {
        var self = this;
        if (auth.getUser()) {
            this.usr_id = auth.getUser().usr_id;
        }
        this.GetFlashInfo();
        this.GetGeoLocationInfo();
        this.GetPrevPageInfo();
        this.GetCurrentPageInfo();
        this.GetBrowserInfo();
        this.GetOSInfo();
        this.GetResolution();
        this.GetIp().then(function() {
            self.GetLocation(self);
        }).then(function() {
            self.SendStats();
        });
        this.RegisterFocusAndBlurEvent();
        this.RegisterIdleTimer();
        this.FindAllClickableElements();
    }

    Stats.prototype.GetFlashInfo = function() {
        this.flashInfo = {
            flash_install: FlashDetect.installed,
        }

        // major: FlashDetect.major,
        // minor: FlashDetect.minor,
        // description: FlashDetect.raw,
        return this.flashInfo;
    }
    Stats.prototype.GetGeoLocationInfo = function() {
        if (navigator.geolocation) {
            this.isGeoLocationSupported = true;
        } else {
            this.isGeoLocationSupported = false;
        }
        return this.isGeoLocationSupported;
    }
    Stats.prototype.GetPrevPageInfo = function() {
        var prevPage = auth.getPrevPage();
        auth.setPrevPage(window.location.href);
        this.prev_pg = prevPage;

        return prevPage;
    }
    Stats.prototype.ChangeAngularPrevPageInfo = function() {
        var prevPage = auth.getPrevPage();
        auth.setPrevPage(window.location.href);
        this.prev_pg = prevPage;
    }
    Stats.prototype.GetCurrentPageInfo = function() {
        return window.location.href;
    }
    Stats.prototype.GetBrowserInfo = function() {
        this.browser = {
            browser_nm: platform.name,
            browser_ver: platform.version,
            browser_desc: platform.description
        }
        return this.browser;
    }
    Stats.prototype.GetOSInfo = function() {
        this.os = {
            os_family: platform.os.family,
            os_ver: platform.os.version,
            os_archi: platform.os.architecture
        }
        return this.os;
    }
    Stats.prototype.GetResolution = function() {
        this.resolution = {
            res_width: screen.width,
            res_height: screen.height,
            device_pixel_ratio: window.devicePixelRatio
        }
        return this.resolution;
    }
    Stats.prototype.GetIp = function() {
        var stats = this;
        if (this.ip) {
            return this.ip;
        }
        return $.ajax({
                // 
                // http://jsonip.com/
                url: 'https://api.ipify.org?format=json',
                method: 'GET'
            })
            .done(function success(d) {
                stats.ip = d.ip;
                return stats.ip;
            })
            .fail(function error(err) {
                console.error(err);
            })
    }
    window.ewLocation = undefined;
    Stats.prototype.GetLocation = function(self) {
        if (!this)
            return;

        //TODO Temporary
        //Find new site with https instead of http://ip-api.com/json
        var stats = this;
        // stats.location = {};
        // return {};


        if (window.ewLocation) {
            // console.log("location....",window.ewLocation);
            return window.ewLocation;
        }
        console.log("Fetching ip-api", new Date().getTime())
        return $.ajax({
                // url: 'http://ip-api.com/json/' + stats.ip,
                url: 'https://pro.ip-api.com/json?key=EkdeYkAQ5Y3wGG1',
                method: 'GET'
            })
            .done(function success(res) {
                // console.log("ip....",res);
                delete res["postal"];
                var t = res;
                delete t["postal"];
                t["ctry_cd"] = res["countryCode"] + "";
                t["hostname"] = res["as"] + "";
                t["ip"] = res["query"] + "";
                t["ip_gather_status"] = res["status"] + "";
                t["loc"] = res["lat"] + "|" + res["lon"];
                t["tm_zone"] = res["timezone"] + "";
                t["zip_cd"] = res["zip"] + "";
                window.ewLocation = t;
                stats.location = t;
                console.log("Done ip-api", new Date().getTime())
            })
            .fail(function error(err) {
                console.error(err);
            })
    }

    var IDLE_TIME = 60 * 1000;
    Stats.prototype.RegisterFocusAndBlurEvent = function() {
        var counter = 0;
        var self = this;
        window.onblur = function() {
            //Start Idle Timer
            this.timerId = setInterval(function() {
                counter++;
                if (counter >= 5) {

                    var data = {
                        idl_msg: 'User is Idle',
                        idl_rsn: 'By changing tab/minimizing',
                        curr_pg: window.location.href,
                        tkn: auth.getHashToken(),
                        login_tkn: auth.getToken(),
                        usr_id: self.usr_id,
                        clk_tm: new Date()
                    }
                    if (window.ewSendIdleEvent) {
                        self.SendData(data, 'useridle');
                    }

                    counter = 0;
                }
            }, IDLE_TIME);
        }
        window.onfocus = function() {
            //Stop Idle Timer
            if (this.timerId)
                clearInterval(this.timerId);
        }
    }
    Stats.prototype.RegisterIdleTimer = function() {
        var self = this;
        var idleTime = 0;
        var idleInterval = setInterval(function() {
            idleTime = idleTime + 1;
            if (idleTime >= 5) { // 5 minutes

                var data = {
                    idl_msg: 'User is Idle',
                    idl_rsn: 'With no mouse/keyboard activity',
                    curr_pg: window.location.href,
                    tkn: auth.getHashToken(),
                    login_tkn: auth.getToken(),
                    usr_id: self.usr_id,
                    clk_tm: new Date()
                }
                if (window.ewSendIdleEvent) {
                    self.SendData(data, 'useridle');
                }
                idleTime = 0;
            }
        }, IDLE_TIME);
        window.onmousemove = function(e) {
            idleTime = 0;
        };
        window.onkeypress = function(e) {
            idleTime = 0;
        };
    }
    Stats.prototype.FindAllClickableElements = function() {

        window.dsTarget = undefined;
        var self = this;
        document.onclick = function(e) {
            window.dsElements = $("a,button,select,input[type='reset'],input[type='button'],input[type='submit'],[onclick],[onmousedown],[onsubmit],[ondblclick]");
            var isClickableElementFound = false;
            var isParent = false;
            window.dsTarget = e.target;

            //First Match Element From Array `this.elements`
            //Try To find target/Parent Of Target from `this.elements`
            if (window.dsElements.index(window.dsTarget) > -1) {
                isClickableElementFound = true;
                // console.log("Element is Clickable from elements array");
            } else if (window.dsElements.index(window.dsTarget.parentNode) > -1) {
                isClickableElementFound = true;
                isParent = true;
                // window.dsTarget = window.dsTarget.parentNode;
                // console.log("Element Parent is Clickable from elements array");
            }

            //If Element is not exist in `this.elements`
            //Then retrieve DOM Node `events` using jquery functions
            //Ex : $._data($(ele).get(0),"events")
            if (!isClickableElementFound) {
                var events = $._data($(window.dsTarget).get(0), "events");
                if (events) {
                    if (events["click"] || events["dblclick"] || events["mousedown"] || events["mouseup"] || events["submit"]) {
                        //Element is Clickable
                        isClickableElementFound = true;
                        // console.log("Clickable element found");
                    }
                }
                //Check For Parent
                if (!isClickableElementFound) {
                    var events = $._data($(window.dsTarget).parent().get(0), "events");
                    if (events) {
                        if (events["click"] || events["dblclick"] || events["mousedown"] || events["mouseup"] || events["submit"]) {
                            //Element is Clickable
                            isClickableElementFound = true;
                            isParent = true;
                            // console.log("Clickable element found in parent");
                        }
                    }
                }
            }

            if (isClickableElementFound) {
                var ele = window.dsTarget;
                if (isParent) {
                    ele = window.dsTarget.parentNode;
                }

                var tag = $(ele).prop("tagName");
                var go_to_pg;
                if (tag.toLowerCase() === "a") {
                    go_to_pg = $(ele).attr("href");
                }


                var desc;
                if ($(window.dsTarget).data('title')) {
                    desc = $(ele).data('title');
                } else {
                    desc = $(ele).attr('value');
                    if (!desc)
                        desc = $(ele).text().trim();
                }
                if (!desc) {
                    var eleClass = $(ele).attr('class');
                    var dynEle = document.createElement(tag);
                    $(dynEle).attr('class', eleClass);
                    desc = dynEle.outerHTML;

                }


                var data = {
                        link_nm: desc,
                        tag_typ: tag,
                        curr_pg: window.location.href,
                        go_to_pg: go_to_pg,
                        tkn: auth.getHashToken(),
                        login_tkn: auth.getToken(),
                        usr_id: self.usr_id,
                        clk_tm: new Date()
                    }
                    // console.log("Click",data);
                self.SendData(data, 'userclick');
            }
        }
    }


    //Modern Browsers
    var BROWSERS = {
        CHROME: 30,
        FIREFOX: 35,
        SAFARI: 6,
        IE: 12,
        'MICROSOFT EDGE': 13
    }
    Stats.prototype.GetBrowserCompatibility = function() {
        var isIE = false;
        var isModern = false;
        var browser_name = stats.GetBrowserInfo().name.toUpperCase();
        var browser_version = parseInt(stats.GetBrowserInfo().version);
        var os = stats.GetOSInfo().family.toUpperCase();
        if (browser_name === "IE" || browser_name === "MICROSOFT EDGE") {
            isIE = true;
        }

        if (browser_version >= BROWSERS[browser_name]) {
            isModern = true;
        }
        var compability;
        if (os.indexOf("OS X") >= 0) {
            compability = "mac+";
        } else {
            compability = "windows+";

            if (isIE) {
                compability += 'ie+';
            }
        }
        if (isModern) {
            compability += 'modern';
        } else {
            compability += 'old';
        }

        return compability;
    }

    Stats.prototype.GenerateHash = function(str) {
        // var loginToken = auth.getToken();
        // var hashToken = auth.getHashToken();
        // if(loginToken)  return loginToken;
        // if(hashToken)   return hashToken;
        var hashToken = calcMD5(str);
        // auth.setHashToken(hashToken);
        return hashToken;
    }

    Stats.prototype.isSameHash = function(hashStr) {
        var hashToken = auth.getHashToken();
        if (hashToken && (hashToken === hashStr)) {
            return true;
        }
        return false;
    }

    Stats.prototype.SendStats = function() {


        var that = this;
        window.statsIneterval = setInterval(function() {
            if (window.statsSent) {
                clearInterval(window.statsIneterval);
                return;
            }
            if (window.ewLocation) {
                var data = {
                    browser: that.GetBrowserInfo(),
                    os: that.GetOSInfo(),
                    ip: that.GetIp(),
                    location: that.GetLocation(),
                }

                var loginToken = auth.getToken() || "";
                var token = that.GenerateHash(JSON.stringify(data));
                var dataToSend;
                if (that.isSameHash(token)) {
                    //Token is already Generated
                    //So we will not send all details again
                    dataToSend = {
                        stats: {
                            usr_id: that.usr_id,
                            browser: that.GetBrowserInfo(),
                            os: that.GetOSInfo(),
                            // ip : that.GetIp(),
                            flashInfo: that.GetFlashInfo(),
                            location: that.GetLocation(),
                            geo_loc_info: that.GetGeoLocationInfo(),
                            prev_pg: that.prev_pg,
                            curr_pg: that.GetCurrentPageInfo(),
                            resolution: that.GetResolution(),
                            tkn: token,
                            login_tkn: loginToken,
                            pg_vw_tm: new Date()
                        }
                    }
                } else {
                    //Token is not Generated
                    //So we will send all details
                    //Save token to storage
                    auth.setHashToken(token);
                    dataToSend = {
                        stats: {
                            usr_id: that.usr_id,
                            browser: that.GetBrowserInfo(),
                            os: that.GetOSInfo(),
                            // ip : that.GetIp(),
                            location: that.GetLocation(),
                            flashInfo: that.GetFlashInfo(),
                            geo_loc_info: that.GetGeoLocationInfo(),
                            prev_pg: that.prev_pg,
                            curr_pg: that.GetCurrentPageInfo(),
                            resolution: that.GetResolution(),
                            tkn: token,
                            login_tkn: loginToken,
                            pg_vw_tm: new Date()
                        }
                    }
                }
                that.SendData(dataToSend.stats, 'userstats');
                window.statsSent = true;
            }
        }, 20);
    }

    Stats.prototype.SendData = function(data, topic) {
        console.log("Sending Stats to kafka", new Date().getTime())
        $.ajax({
            url: '/userstats/',
            method: 'POST',
            data: {
                topic: topic,
                stats: JSON.stringify(data)
            }
        });
    }
    
    if(typeof angular === "undefined")
        new Stats();
}