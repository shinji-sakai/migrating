//default width
var DEFAULT_WIDTH = "960";
//default height
var DEFAULT_HEIGHT = "540";
//default volume
var DEFAULT_VOLUME = 40;
//store all jwplayer instances
//id -> instance mapping
var jwplayers = {};
//video seek event stats
var video_seek_events = [];
//volume button clicked stats
var volume_btn_clicked_events = 0;
//volume change event stats
var volume_change_events = [];
//play-pause button events
var play_pause_btn_clicked = [];
//dom structure
var HTML =
    [
        "<div class='custom-player-wrapper'>",
        "<div class='video-player-wrapper'>",
        "</div>",
        "<div class='video-middle-play-btn big-play-btn'>",
        "</div>",
        "<div class='video-control-wrapper'>",
        "<div class='control-item rewind-btn'>",
        "</div>",
        "<div class='control-item play-btn play'>",
        "</div>",
        "<div class='control-item forward-btn'>",
        "</div>",
        "<div class='control-item progress-wrapper'>",
        "<div class='real-progress'></div>",
        "<div class='buffer-progress'></div>",
        "</div>",

        "<div class='control-item video-name-wrapper'>",
        "Why we are special",
        "</div>",

        "<div class='control-item sound-wrapper'>",
        "<div class='sound-icon'>",
        "</div>",
        "<div class='mute-pre-load'>",
        "</div>",
        "<div class='sound-progress-wrapper'>",
        "<div class='sound-real-progress'></div>",
        "</div>",
        "</div>",

        // "<div class='control-item fullscreen-wrapper'>",
        // "<div class='fullscreen-icon full'>",
        // "</div>",
        // "</div>",
        "</div>",
        "</div>"
    ];

initializePlayers();

function initinitVideoStatsAPI(player) {
    new VideoStats(player);
}

function initializePlayers() {
    var players = $("[player^='true']");

    players.each(function(i, block) {
        //pour element with dom
        $(block).html(HTML.join(' '));

        //set height
        // var height = $(block).data("height");
        // (height) ? $(block).height(height): $(block).height(DEFAULT_HEIGHT);

        //set width
        // var width = $(block).data("width");
        // (width) ? $(block).width(width): $(block).width(DEFAULT_WIDTH);

        //video url set by user
        var videoURL = $(block).data("url");

        //random dom id
        var domId = generateDOMId();

        //player wrapper to set video player
        var playerWrapper = $(block).find(".video-player-wrapper");

        //set dom id to player wrapper
        $(playerWrapper).attr('id', domId);

        //set dom id to main parent wrapper
        $(block).attr('id', domId + "-wrapper");

        //set up jw player
        setupJWPlayer(domId, videoURL);
    })
}

//setup jw player
function setupJWPlayer(id, url) {
    jwplayers[id] = jwplayer(id).setup({
        playlist: [{
            sources: [{
                file: url
            }]
        }],
        width: '100%',
        height: '100%',
        primary: 'html5',
        controls: false
    });
    //init videostats api
    initinitVideoStatsAPI(jwplayers[id]);
    //set default volume
    jwplayers[id].setVolume(DEFAULT_VOLUME);
    //on buffer change event update buffer progress bar
    jwplayers[id].on('bufferChange', function() {
        var bufferbar_width = (jwplayers[id].getBuffer()) + '%';
        var parent = $("#" + id + "-wrapper");
        $(parent).find(".buffer-progress").css('width', bufferbar_width);
    });
    //On Time Update
    jwplayers[id].on('time', function(e) {
        //Fetch current time
        var time = this.getPosition();
        //Progress bar width
        var progressbar_width = ((this.getPosition() / this.getDuration()) * 100) + '%';
        //Set progress bar width
        var parent = $("#" + id + "-wrapper");
        $(parent).find(".real-progress").css('width', progressbar_width);
    });

    //events
    //
    // when user click on video player
    // toggle video player state to play/pause
    $(".custom-player-wrapper").on('click', function(e) {
        e.stopPropagation();
        var parent = $(this);
        var playerId = findVideoPlayerId($(this));
        var player = getPlayer(playerId);
        if (!player.isPlaying) {
            //change player state
            player.isPlaying = true;
            //play player
            player.play();
            //change dom
            $(parent).find('.play-btn').removeClass('play').addClass('pause');
            $(parent).find('.video-middle-play-btn').removeClass('big-play-btn').addClass('big-pause-btn');
        } else {
            //change player state
            player.isPlaying = false;
            //pause player
            player.pause();
            //change dom
            $(parent).find('.play-btn').removeClass('pause').addClass('play');
            $(parent).find('.video-middle-play-btn').removeClass('big-pause-btn').addClass('big-play-btn');

        }
    })

    $(".video-control-wrapper").on("click", function(e) {
        e.stopPropagation();
    })

    $('.video-middle-play-btn').on("click",function(e) {
        console.log("big button clicked")
        e.stopPropagation();
        var parent = findParent(e.currentTarget);
        var playerId = findVideoPlayerId(parent);
        var player = getPlayer(playerId);
        if (!player.isPlaying) {
            //change player state
            player.isPlaying = true;

            //change dom
            $(parent).find('.play-btn').removeClass('play').addClass('pause');
            $(parent).find('.video-middle-play-btn').removeClass('big-play-btn').addClass('big-pause-btn');


            //play player
            player.play();
        } else {
            //change player state
            player.isPlaying = false;

            //change dom
            $(parent).find('.play-btn').removeClass('pause').addClass('play');
            $(parent).find('.video-middle-play-btn').removeClass('big-pause-btn').addClass('big-play-btn');

            //pause player
            player.pause();
        }
    })

    //play button click
    $(".play-btn").on("click", function(e) {
        var parent = findParent(e.currentTarget);
        var playerId = findVideoPlayerId(parent);
        var player = getPlayer(playerId);
        if (!player.isPlaying) {
            //change player state
            player.isPlaying = true;

            //change dom
            $(parent).find('.play-btn').removeClass('play').addClass('pause');
            $(parent).find('.video-middle-play-btn').removeClass('big-play-btn').addClass('big-pause-btn');


            //play player
            player.play();
        } else {
            //change player state
            player.isPlaying = false;

            //change dom
            $(parent).find('.play-btn').removeClass('pause').addClass('play');
            $(parent).find('.video-middle-play-btn').removeClass('big-pause-btn').addClass('big-play-btn');

            //pause player
            player.pause();
        }
        // play_pause_btn_clicked.push(player.getDuration());
    });

    //rewind button click
    $(".rewind-btn").on("click", function(e) {
        var parent = findParent(e.currentTarget);
        var playerId = findVideoPlayerId(parent);
        var player = getPlayer(playerId);
        var currentTime = player.getPosition();
        var nextTime = currentTime - 10;
        player.seek(nextTime);
    });

    //fast forward button click
    $(".forward-btn").on("click", function(e) {
        var parent = findParent(e.currentTarget);
        var playerId = findVideoPlayerId(parent);
        var player = getPlayer(playerId);
        var currentTime = player.getPosition();
        if (currentTime < player.getDuration()) {
            var nextTime = currentTime + 10;
            player.seek(nextTime);
        }
    });

    //Click Listener For Progress Bar
    $('.progress-wrapper').on('click', function(e) {
        // e.stopPropagation();
        var parent = findParent($(this));
        var playerId = findVideoPlayerId(parent);
        var player = getPlayer(playerId);

        //Percent where user clicked on progressbar
        var percent = (e.offsetX / $(this).width()) * 100;

        $(this).find(".real-progress").width(percent + "%");

        //Get desire time from where user clicked
        var desireTime = player.getDuration() * percent / 100;
        //Set current time to desire time
        player.seek(desireTime);
        player.isPlaying = true;
        $(parent).find('.play-btn').removeClass('play').addClass('pause');
        // //video seek event stats added to array
        // video_seek_events.push(percent);
    });

    //toggle volume
    $(".sound-icon").on("click", function(e) {
        // e.stopPropagation();
        var parent = findParent($(this));
        var playerId = findVideoPlayerId(parent);
        var player = getPlayer(playerId);
        if (player.isMute) {
            //if it is already mute 
            //then unmute it
            var vol = player.previousVolume ? player.previousVolume : 100;
            $(parent).find(".sound-real-progress").width(vol + '%');
            player.setVolume(vol);
            $(this).removeClass('mute');
            player.isMute = false;
        } else {
            //if it is unmute 
            //then mute it
            player.previousVolume = player.getVolume();
            $(parent).find(".sound-real-progress").width('00%');
            player.setVolume(0);
            player.isMute = true;
            $(this).addClass('mute');
        }
        // //volume button click increment
        // volume_btn_clicked_events++;
    });

    //Click Listener For Sound Bar
    $('.sound-progress-wrapper').on('click', function(e) {

        var parent = findParent($(this));
        var playerId = findVideoPlayerId(parent);
        var player = getPlayer(playerId);

        //Percent where user clicked on sound progressbar
        var percent = (e.offsetX / $(this).width()) * 100;

        $(this).find(".sound-real-progress").width(percent + "%");

        //Set volume to desire volume
        player.setVolume(parseInt(percent));
        player.isMute = false;
        $(parent).find(".sound-icon").removeClass('mute');
        // //volume change event happen and added to array
        // volume_change_events.push(percent);

    });

    //Click Listener For Fullscreen
    $('.fullscreen-wrapper').on('click', function(e) {
        var parent = findParent($(this));
        var playerId = findVideoPlayerId(parent);
        var player = jwplayers[playerId];
        console.log(playerId);
        if (player.isFullScreen) {
            exitFullScreen();
        } else {
            launchFullScreen(playerId);
        }

    });
}

//generate random id
//it will use to assign random id to each player to avoid collision
function generateDOMId() {
    return Math.random().toString(36).substr(2, 5);
}

//find parent element of player
//this method will use to find parent element of player , for ex : (.custom-player-wrapper)
function findParent(element) {
    return $(element).closest('.custom-player-wrapper');
}

//find video player id using dom parent element
//this function is used for finding jwplyer id using given parent element
//parent element should contain video player wrapper
//we will take `id` attribute of that dom element
function findVideoPlayerId(element) {
    //refer `dom structure` on the top
    //always element will be '.custom-player-wrapper'
    var firstChild = $(element).children()[0];
    return $(firstChild).attr('id');
}

//get player instance from jwplayers
function getPlayer(id) {
    return jwplayers[id];
}

function pauseVideoPlayer(){
    var id = findVideoPlayerId(".custom-player-wrapper");
    removeJWPlayer(id);
}

function removeJWPlayer(id) {
    var player = jwplayers[id];
    var parent = findParent($("#" + id));
    console.log(id, parent);
    //change player state
    player.isPlaying = false;
    //pause player
    player.play(false);
    //change dom
    $(parent).find('.play-btn').removeClass('pause').addClass('play');
}

function launchFullScreen(id) {
    var parent = findParent($("#" + id));
    var player = jwplayers[id];
    var ele = $(parent).parent();
    var element = ele[0];
    var fullscreenElement = element;
    var fullscreenchange;
    if (element.requestFullscreen) {
        element.requestFullscreen();
        fullscreenchange = "fullscreenchange";
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
        fullscreenchange = "mozfullscreenchange";
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
        fullscreenchange = "webkitfullscreenchange";
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
        fullscreenchange = "msfullscreenchange";
    }
    element.addEventListener(fullscreenchange, function(e) {
        var fullscreenEnabled = document.isFullScreen || document.mozFullScreen || document.webkitIsFullScreen;
        if (fullscreenEnabled) {
            $(element).addClass('fullscreen-video');
            $(element).find('.fullscreen-icon').removeClass('full').addClass('small');
        } else {
            $(element).removeClass('fullscreen-video');
            $(element).find('.fullscreen-icon').removeClass('small').addClass('full');
        }
        player.isFullScreen = fullscreenEnabled;

        // $(element).trigger('fullscreenchange',[fullscreenEnabled]);
        // if (fullscreenEnabled) {
        //     vm.isFullScreen = true;
        // } else {
        //     vm.isFullScreen = false;
        // }
    });
}

function exitFullScreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}