var path = require('path');
var base = path.normalize(__dirname + '/');

var videoPath={
    _videoPath1: '/resources/video/',
    videoAbsolutePath : "/resources/video/",
    videoRelativePath : path.resolve(base + '../../../client/resources/video/'),
    videoThumbnailAbsolutePath : "/resources/video-thumbs/",
    videoThumbnailRelativePath : path.resolve(base + '../../../client/resources/video-thumbs/'),
}

module.exports =  videoPath;