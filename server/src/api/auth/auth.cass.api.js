var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var jwt = require('jsonwebtoken');
var authJWT = require("jwt-simple");
var request = require('request');
var qs = require('querystring');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);
var Base64 = require(configPath.lib.utility).Base64;
var shortid = require('shortid');
var mail = require(configPath.api.mail);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:auth:cass");
var crypto = require('crypto');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path')

var cassExecute = require(configPath.lib.utility).cassExecute;
var profileApi = require(configPath.api.dashboard.profile);
var adminAuthorApi = require(configPath.api.admin.author);
var chatFriendsApi = require(configPath.api.chat.friends);
var userCoursesAPI = require(configPath.api.usercourses);
var registerTemplate = require(configPath.api.emailTemplateAPI.registerTemplate);
var forgotpasswordTemplate = require(configPath.api.emailTemplateAPI.forgotpasswordTemplate);
var resendotpTemplate = require(configPath.api.emailTemplateAPI.resendotpTemplate);
var subscribeAPI = require(configPath.api.subscribe.subscribe);
var smsAPI = require(configPath.api.sms.sms);

var login_table = 'ew1.vt_usr_lgn';
var reg_table = 'ew1.vt_usr_reg';
var reg_otp_table = 'ew1.vt_usr_reg_otp';
var fpass_otp_table = 'ew1.vt_usr_fpass_otp';
var FORGOT_PASS_OTP_EXP_TM = 7200;

function Auth() {}

//Register User
//This function will be called when user will register first time in website
Auth.prototype.registerUser = async function(data) {
    var defer = Q.defer();

    var usr_id = (data.usr_id || "").trim().toLowerCase();
    var dsp_nm = data.dsp_nm;
    var usr_email = data.usr_email;
    var usr_ph = data.usr_ph || "";
    var usr_role = data.usr_role;
    var usr_profile_base64 = data.user_profile;



    var pwd = crypto.createHash('md5').update(data.pwd).digest("hex");
    var reg_dt = new Date();

    var isUserExists = await this.checkUserExistance({
        usr_id
    });

    if(isUserExists.userExists){
        return {
            status : false,
            message : userErrorsConfig.USER_EXISTS
        }
    }

    var targetProfilePicPath;
    if(usr_profile_base64){
        var base64Data = usr_profile_base64.replace(/^data:image\/\w+;base64,/, '');
        var targetPath = path.resolve(`${configPath.common_paths.user_pic_relative}/${usr_id}.jpg`);
        targetProfilePicPath = path.normalize(`${configPath.common_paths.user_pic_absolute}/${usr_id}.jpg`);
        try{
            await fs.writeFileAsync(targetPath, base64Data, {encoding: 'base64'});
        }catch(err){
            return {
                status : false,
                message : "Something went wrong in uploading profile pic.Please contact support."
            }
        }
    }


    var otp = '' + Math.floor(100000 + Math.random() * 900000);
    var otp_exp_tm = 172800; //in seconds 48 hr , 2 days
    var otp_snt_dt = reg_dt;

    //Insert User into register table
    var insertUser = 'INSERT INTO ' + reg_table + ' (usr_id, dsp_nm, usr_email,usr_ph,reg_dt,pwd,role)  ' + 'VALUES(?, ?, ?, ?, ?,?,?);';
    var insertUser_args = [usr_id, dsp_nm, usr_email, usr_ph, reg_dt, pwd, usr_role];

    //Insert entry for storing generated OTP
    var insertOTP = 'INSERT INTO ' + reg_otp_table + ' (usr_id, otp , otp_exp_tm , otp_snt_dt , usr_email,usr_ph,pwd)  ' + 'VALUES(?, ?, ?, ?, ?,?,?);';
    var insertOTP_args = [usr_id, otp, otp_exp_tm, otp_snt_dt, usr_email, usr_ph, pwd];

    var queries = [{
        query: insertUser,
        params: insertUser_args
    }, {
        query: insertOTP,
        params: insertOTP_args
    }];

    // 

    //save role to usr_dtls table
    profileApi.saveUserBasicPersonalDetails({
        usr_id: usr_id,
        role: usr_role,
        dsp_nm: dsp_nm
    });

    if (usr_role === 'author') {
        //if registering user is author then add it to mongodb author table also
        //this admin author table we are using in admin forms to add author to the course
        //so to sync both the table we will add author to there also
        adminAuthorApi.update({
            authorId: usr_id,
            authorName: dsp_nm
        });
    }

    var queryOptions = {
        prepare: true,
        consistency: cassandra.types.consistencies.quorum
    };
    cassconn.batch(queries, queryOptions, async function(err, succ, res) {
        if (err) {
            defer.reject(err);
            return;
        }

        try {

            //add support as a friend in all new registered user
            // await profileApi.addConfirmedFriends({
            //     usr_id: usr_id,
            //     fr_id: 'support'
            // });
            await chatFriendsApi.addFriend({
                user_id: usr_id,
                fr_id: 'support'
            });
            // await chatFriendsApi.addFriend({
            //     user_id: 'support',
            //     fr_id: usr_id
            // });
            //add user short details
            await chatFriendsApi.addUserShortDetails({
                    user_id: usr_id,
                    user_email: usr_email,
                    display_name: dsp_nm,
                    typ: 'usr',
                    pic: targetProfilePicPath
                })
                //add as a subscriber to the site
            await subscribeAPI.subscribeUser({
                subs_email: usr_email,
                subs_typ : 'registration form',
                usr_id : usr_id
            });

            //save details to profile api
            await profileApi.saveShortDetails({
                usr_id : (usr_id || "").trim().toLowerCase(),
                usr_email,
                dsp_nm,
                usr_pic : targetProfilePicPath,
                usr_ph
            });

            // add primary email to contact details
            await profileApi.addEmailInContactDetails({
                usr_id,
                email: usr_email,
                isPrimary: true
            });

            if (usr_ph) {
                //if user phone number is set then 
                //send registration sms
                try {
                    //send sms to user
                    var smsres = await smsAPI.firstTimeLogin(usr_ph,usr_id,otp);
                    // var smsres = await smsAPI.sendSMS({
                    //     message: 'From examwarrior.com, OTP for 1st time login: ' + otp,
                    //     numbers: usr_ph
                    // });
                    debug(smsres);
                } catch (err) {
                    debug("Sms Sending error :", err);
                    //try once again
                    try {
                        //send sms to user
                        var smsres = await smsAPI.firstTimeLogin(usr_ph,usr_id,otp);
                        // var smsres = await smsAPI.sendSMS({
                        //     message: 'From examwarrior.com, OTP for 1st time login: ' + otp,
                        //     numbers: usr_ph
                        // });
                    } catch (err_2) {
                        debug("2nd time Sms Sending error :", err_2);
                    }
                }
            }


            var template = await registerTemplate({
                username: usr_id,
                otp: otp,
                usr_email: usr_email,
                usr_ph: usr_ph,
                dsp_nm: dsp_nm
            });

            //Send otp to user email
            var str = 'Hi,' + dsp_nm + "<br /> Thanks for Registration.<br />Your OTP for registration is : <strong>" + otp + "</strong>";
            var data = {
                to: usr_email,
                subject: 'OTP for Registration - examwarrior.com',
                text: template.html
            }
            mail.sendMail(data);
        } catch (err) {
            debug(err);
            defer.reject(err);
        }

        defer.resolve({
            status : 'success',
            err: 'Registration Successfull'
        });
    });
    return defer.promise;
}
Auth.prototype.verifyOTP = function(data) {
    var defer = Q.defer();
    var usr_id = (data.usr_id || "").trim().toLowerCase();
    var otp = data.otp;
    var dsp_nm = '';
    var crt_dt = new Date();
    var otp_exp_tm = 172800; //in seconds 48 hr , 2 days

    var selectOtp = 'SELECT * from ' + reg_otp_table + ' where usr_id = ?';
    var selectOtp_args = [usr_id];

    cassconn.execute(selectOtp, selectOtp_args, function(err, res, r) {
        if (err) {
            defer.reject(err);
            return;
        }
        if (!res.rows) {
            defer.resolve({
                err: 'Something went wrong.'
            });
        } else {
            if (res.rows.length <= 0) {
                defer.resolve({
                    err: userErrorsConfig.USER_NOT_EXIST
                });
                return;
            }
            var row = res.rows[0];
            var real_otp = row.get('otp');
            var send_dt = row.get('otp_snt_dt');
            var usr_email = row.get('usr_email');
            var pwd = row.get('pwd');

            var dt = new Date(send_dt);
            var now_dt = new Date();

            var timeDiff = Math.abs(now_dt - dt); // in millisec
            var diff_secs = timeDiff / (1000);

            var exp_tm = row.get('otp_exp_tm');

            if (diff_secs > otp_exp_tm) {
                defer.resolve({
                    status : 'error',
                    err: 'OTP Expired'
                });
            } else {
                if (real_otp === otp) {
                    defer.resolve({
                        usr_email: usr_email,
                        pwd: pwd
                    });
                } else {
                    defer.resolve({
                        status : 'error',
                        err: "OTP doesn't match"
                    });
                }
            }
        }
    });
    return defer.promise;
}
Auth.prototype.resendOTP = async function(data) {
    var defer = Q.defer();

    var usr_email = data.usr_email;
    var usr_ph;
    var usr_id = (data.usr_id || "").trim().toLowerCase();
    var otp = '' + Math.floor(100000 + Math.random() * 900000);
    var otp_snt_dt = new Date();

    // if (!usr_email) {
        var getUser = 'select * from ' + reg_table + ' where usr_id = ?';
        var getUser_args = [usr_id];

        var getUserRes = await cassExecute(getUser, getUser_args);
        if (getUserRes) {
            usr_email = getUserRes[0]["usr_email"];
            usr_ph = getUserRes[0]["usr_ph"];
        }
    // }


    //Insert entry for storing generated OTP
    var updateOTP = 'Update ' + reg_otp_table + ' SET otp=?,otp_snt_dt=?  where usr_id = ?';
    var updateOTP_args = [otp,new Date(), usr_id];

    var queries = [{
        query: updateOTP,
        params: updateOTP_args
    }];

    var queryOptions = {
        prepare: true,
        consistency: cassandra.types.consistencies.quorum
    };
    cassconn.batch(queries, queryOptions, async function(err, succ, res) {
        if (err) {
            defer.reject(err);
            return;
        }

        var template = await resendotpTemplate({
            username: usr_id,
            otp: otp
        })

        //Send otp to user email
        var str = 'Hi,' + "<br /> Thanks for Registration.<br />Your OTP for registration is : <strong>" + otp + "</strong>";
        var data = {
            to: usr_email,
            subject: 'OTP for Registration - examwarrior.com',
            text: template.html
        }
        mail.sendMail(data);

        if(usr_ph){
            await smsAPI.firstTimeLogin(usr_ph,usr_id,otp);
        }

        defer.resolve({
            status : 'success',
            message : 'OTP Sent Successfully'
        });
    });
    return defer.promise;
}
Auth.prototype.validateLoginDetails = function(data) {
    var self = this;
    return findUser()
        .then(validate)
        .then(checkForOTP)
        .then(isFirstTime)
        .catch(globalFunctions.err);

    function findUser() {
        var defer = Q.defer();
        var selectUser = 'select * from ' + login_table + ' where usr_id = ?;';
        var args = [(data.usr_id || "").trim().toLowerCase()];
        cassconn.execute(selectUser, args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            if (!res) {
                defer.resolve([]);
            } else {
                defer.resolve(res.rows);
            }
        });
        return defer.promise;
    }

    function validate(user) {
        var hashPwd = crypto.createHash('md5').update(data.pwd).digest("hex");
        if (user.length > 0 && user[0].get('pwd') === hashPwd) {
            return profileApi.getUserShortDetails({
                    usr_id: user[0].get('usr_id')
                })
                .then(function(data) {
                    // debug("validate.....",data);
                    data.is_act = user[0].get('is_act')
                    var obj = {
                        user: data,
                        firstTime: false
                    }
                    return obj;
                })
                .catch(function(err) {
                    debug(err);
                })
        } else if (user.length == 0) {
            return {
                err: userErrorsConfig.USER_NOT_EXIST
            };
        } else {
            return {
                err: userErrorsConfig.PASSWORD_MISMATCH
            };
        }
    }

    function checkForOTP(res) {
        if (!res.err || res.err === userErrorsConfig.PASSWORD_MISMATCH) {
            //If no error or password missmatch then forward ahead 
            return res;
        }
        var d = {
            usr_id: (data.usr_id || "").trim().toLowerCase(),
            otp: data.pwd
        }
        return self.verifyOTP(d)
            .then(function(verifiedStatus) {
                if (!verifiedStatus.err) {
                    //if there is no erroe
                    //means user is logging in first time and successfully entered otp
                    return {
                        firstTime: true,
                        user: {
                            usr_id: (data.usr_id || "").trim().toLowerCase(),
                            pwd: verifiedStatus.pwd, // real password
                            usr_email: verifiedStatus.usr_email
                        }
                    };
                } else if (verifiedStatus.err) {
                    if (verifiedStatus.err !== userErrorsConfig.USER_NOT_EXIST) {
                        //user is exist but wrong otp
                        return {
                            firstTime: true,
                            err: verifiedStatus.err
                        };
                    } else {
                        //user is not exist
                        return {
                            err: verifiedStatus.err
                        };
                    }

                }
            });
    }

    function isFirstTime(obj) {
        if (obj.err) {
            return obj;
        }

        if (!obj.firstTime) {
            return obj;
        }

        //if it is first time
        //save details to login table
        var defer = Q.defer();

        var real_pwd = obj.user.pwd; //This Password is from registration form
        var email = obj.user.usr_email;

        //fetch query for reg table
        var reg_details = "select * from " + reg_table + ' where usr_id = ?';
        var reg_args = [(data.usr_id || "").trim().toLowerCase()];

        //first fetch info from reg table
        cassconn.execute(reg_details, reg_args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            //if there is no for user 
            if (!res.rows || res.rows.length <= 0) {
                defer.reject(new Error("Something went wrong"));
                return;
            }

            //user details from reg table
            var usr = res.rows[0];

            //insert query for login table
            var insertUser = 'insert into ' + login_table + ' (usr_id,pwd,is_act,crt_dt) values (?,?,?,?);';
            var insertUser_args = [(data.usr_id || "").trim().toLowerCase(), real_pwd, 'true', new Date()];

            //insert user into login table
            cassconn.execute(insertUser, insertUser_args, function(err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                var d = {
                    firstTime: true,
                    user: {
                        usr_id: (data.usr_id || "").trim().toLowerCase(),
                        pwd: real_pwd,
                        dsp_nm: usr.get("dsp_nm"),
                        usr_email: email,
                        usr_ph: usr.get("usr_ph"),
                        is_act : true
                    }
                };
                
                defer.resolve(d);
            });
        });
        return defer.promise;
    }
}
Auth.prototype.checkUserExistance = async function(data) {
    return findUser()
        .then(checkUser)
        .catch(globalFunctions.err);

    async function findUser() {
        var defer = Q.defer();
        try {
            var selectUser = 'select * from ' + login_table + ' where usr_id = ?;';
            var args = [(data.usr_id || "").trim().toLowerCase()];
            var rows = await cassExecute(selectUser, args);

            if (rows.length > 0) {
                return rows;
            } else {
                var selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
                var args = [(data.usr_id || "").trim().toLowerCase()];
                var rows = await cassExecute(selectUser, args);
                return rows;
            }
        } catch (err) {
            debug(err);
            throw err;
        }
        return defer.promise;
    }

    function checkUser(user) {
        if (user.length > 0) {
            return {
                userExists: true
            };
        }
        return {
            userExists: false
        };
    }
}
Auth.prototype.removeUser = function(data) {
    return removeUser()
        .catch(globalFunctions.err);

    function removeUser(col) {
        var defer = Q.defer();
        var selectUser = 'delete from ' + login_table + ' where user_id = ?;';
        var args = [(data.usr_id || "").trim().toLowerCase()];
        cassconn.execute(selectUser, args, function(err, succ, res) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }
}
Auth.prototype.checkPassword = function(data) {
    return findUser()
        .then(checkPassword)
        .catch(globalFunctions.err);

    function findUser() {
        var defer = Q.defer();
        var selectUser = 'select * from ' + login_table + ' where usr_id = ?;';
        var args = [(data.usr_id || "").trim().toLowerCase()];
        cassconn.execute(selectUser, args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });
        return defer.promise;
    }

    function checkPassword(user) {
        if (user.length > 0) {
            var user = user[0];
            var hashPwd = crypto.createHash('md5').update(data.pwd).digest("hex");
            if (user.get('pwd') === hashPwd) {
                return {
                    err: null
                };
            } else {
                return {
                    err: userErrorsConfig.PASSWORD_MISMATCH
                };
            }
        }
        return {
            err: userErrorsConfig.PASSWORD_MISMATCH
        };
    }
}
Auth.prototype.changePassword = async function(data) {

    try{
        if(!data.usr_id){
            return {
                error : "usr_id field is required."
            }
        }
        if(!data.pwd){
            return {
                error : "pwd field is required."
            }
        }
        var obj = await this.checkUserExistance({usr_id:data.usr_id});
        if(obj.userExists){
            await updateUser();
            return {
                message : "Password successfully updated."
            }
        }else{
            return {
                error : "User doesn't exist"
            }
        }
    }catch(err){
        console.log(err);
        throw new Error(err)
    }

    // return updateUser()
    //     .catch(globalFunctions.err);

    function updateUser() {
        var hashPwd = crypto.createHash('md5').update(data.pwd).digest("hex");

        var defer = Q.defer();
        var selectUser = 'update ' + login_table + ' set pwd = ? where usr_id = ?;';

        var args = [hashPwd, (data.usr_id || "").trim().toLowerCase()];

        debug(args);
        cassconn.execute(selectUser, args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }
}
Auth.prototype.forgotPassword_old = function(data) {
    var usr_id = (data.usr_id || "").trim().toLowerCase();
    return findUser()
        .then(async function(user) {
            console.log(user);
            var email = user.get('usr_email');
            var usr_id = (data.usr_id || "").trim().toLowerCase();
            var date = new Date();
            var time = date.getTime();
            var obj = {
                usr_email: email,
                usr_id: usr_id,
                time: time
            }
            var str = JSON.stringify(obj);
            var bs_en = Base64.encode(str);

            var link = "https://www.examwarrior.com/changepassword/" + bs_en;

            var template = await forgotpasswordTemplate({
                username: usr_id,
                link: link
            })


            var str = 'Hi,' + usr_id + "<br /> Click on below link to change password.This link is valid only for one hour.<br /><a href='" + link + "' target='_blank' >Change Password</a>";
            var mailData = {
                to: email,
                subject: 'Forgot Password - examwarrior.com',
                text: template.html
            }
            console.log(mailData);
            mail.sendMail(mailData);
            return {
                usr_email: email
            }
        })
        .catch(globalFunctions.err);

    function findUser() {
        var defer = Q.defer();
        var selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
        var args = [usr_id.trim()];

        cassconn.execute(selectUser, args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }

            if (!res) {
                defer.resolve([]);
            } else {
                defer.resolve(res.rows[0]);
            }
        });
        return defer.promise;
    }
}
Auth.prototype.forgotPassword = async function(data) {
    var usr_id = (data.usr_id || "").trim().toLowerCase();

    /////////////Check user existance
    var userExistsRes = await this.checkUserExistance(data);
    if (!userExistsRes.userExists) {
        return {
            err: userErrorsConfig.USER_NOT_EXIST
        }
    }
    /////////////

    var selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
    var args = [usr_id.trim()];
    var rows = await cassExecute(selectUser, args)
    var user = rows[0];

    var email = user['usr_email'];
    var ph = user['usr_ph'];
    var usr_id = (data.usr_id || "").trim().toLowerCase();
    var date = new Date();
    var otp = '' + Math.floor(100000 + Math.random() * 900000);
    var otp_exp_tm = FORGOT_PASS_OTP_EXP_TM; //in seconds 2 hr
    var otp_snt_dt = date;

    ///////////////Insert entry for storing generated OTP
    var insertOTP = 'INSERT INTO ' + fpass_otp_table + ' (usr_id, otp  , otp_snt_dt,otp_exp_tm)  ' + 'VALUES(?, ?, ?,?);';
    var insertOTP_args = [usr_id, otp, otp_snt_dt, otp_exp_tm];
    await cassExecute(insertOTP, insertOTP_args, {
        prepare: true
    });
    /////////////

    if (email) {
        //send email
        await this.sendForgotPasswordOTPMail({
            usr_id: usr_id,
            email: email,
            otp: otp
        })
    }

    if (ph) {
        this.sendForgotPasswordOTPSMS({
            otp: otp,
            ph: ph,
            usr_id : usr_id
        })
    }

    return {
        email: email,
        ph: ph.substr(0, 3) + "xxxx" + ph.substr(-3)
    }
}
Auth.prototype.sendForgotPasswordOTPMail = async function(data) {
    // data = {usr_id,otp,email}
    try {
        ///////////////send mail
        var template = await forgotpasswordTemplate({
            username: (data.usr_id || "").trim().toLowerCase(),
            otp: data.otp
        })
        var mailData = {
            to: data.email,
            subject: 'Forgot Password OTP - examwarrior.com',
            text: template.html
        }
        await mail.sendMail(mailData);
        /////////////
    } catch (err) {
        debug(err);
        throw err;
    }
}
Auth.prototype.sendForgotPasswordOTPSMS = async function(data) {
    // data = {otp,ph,usr_id}
    // var sms_obj = {
    //     message: 'From examwarrior.com, OTP for 1st time login: ' + data.otp,
    //     numbers: data.ph
    // }
    try {
        ///////////////send SMS
        var smsres = await smsAPI.sendForgotPasswordSMS(data.ph,(data.usr_id || "").trim().toLowerCase(),data.otp);
        debug(smsres);
    } catch (err) {
        debug("Sms Sending error :", err);
        //try once again
        try {
            //send sms to user
            var smsres = await smsAPI.sendForgotPasswordSMS(data.ph,(data.usr_id || "").trim().toLowerCase(),data.otp);
        } catch (err_2) {
            debug("2nd time Sms Sending error :", err_2);
        }
    }
}
Auth.prototype.resendForgotPasswordOTP = async function(data) {
    try {
        var usr_id = (data.usr_id || "").trim().toLowerCase();
        var otp = '' + Math.floor(100000 + Math.random() * 900000);
        var otp_snt_dt = new Date();

        //find user email and phone number
        var selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
        var args = [usr_id.trim()];
        var rows = await cassExecute(selectUser, args)
        var user = rows[0];
        var email = user["usr_email"];
        var ph = user['usr_ph'];

        //Insert entry for storing generated OTP
        var updateOTP = 'Update ' + fpass_otp_table + ' SET otp=?,otp_snt_dt=?  where usr_id = ?';
        var updateOTP_args = [otp, otp_snt_dt, usr_id];

        await cassExecute(updateOTP, updateOTP_args);

        if (email) {
            //send mail
            await this.sendForgotPasswordOTPMail({
                usr_id: usr_id,
                email: email,
                otp: otp
            })
        }
        if (ph) {
            this.sendForgotPasswordOTPSMS({
                otp: otp,
                ph: ph,
                usr_id : usr_id
            })
        }

        return {
            email: email,
            ph: ph.substr(0, 3) + "xxxx" + ph.substr(-3)
        }
    } catch (err) {
        debug(err);
        throw err;
    }
}
Auth.prototype.verifyForgotPasswordOTP = async function(data) {

    var usr_id = (data.usr_id || "").trim().toLowerCase();
    var otp = data.otp;

    var selectOtp = 'SELECT * from ' + fpass_otp_table + ' where usr_id = ?';
    var selectOtp_args = [usr_id];

    var rows = await cassExecute(selectOtp, selectOtp_args);
    var row = rows[0];
    var real_otp = row['otp'];
    var send_dt = row['otp_snt_dt'];

    var dt = new Date(send_dt);
    var now_dt = new Date();

    var timeDiff = Math.abs(now_dt - dt); // in millisec
    var diff_secs = timeDiff / (1000);

    var exp_tm = FORGOT_PASS_OTP_EXP_TM;

    if (diff_secs > exp_tm) {
        return {
            err: 'OTP Expired'
        };
    } else {
        if (real_otp === otp) {
            //get user details
            var selectUser = 'select * from ' + reg_table + ' where usr_id = ?;';
            var args = [usr_id.trim()];
            var rows = await cassExecute(selectUser, args)
            var user = rows[0];
            delete user["pwd"];
            return user;
        } else {
            return {
                err: "OTP doesn't match"
            };
        }
    }
}


Auth.prototype.facebook = function(data) {
    var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'];
    var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
    var graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.FACEBOOK_SECRET,
        redirect_uri: data.redirectUri
    };
    var defer = Q.defer();
    request.get({
        url: accessTokenUrl,
        qs: params,
        json: true
    }, function(err, response, accessToken) {
        if (response.statusCode !== 200) {
            var e = new Error(accessToken.error.message);
            defer.reject(e);
        }
        console.log("accessToken......", accessToken);
        request.get({
            url: graphApiUrl,
            qs: accessToken,
            json: true
        }, function(err, response, profile) {

            if (response.statusCode !== 200) {
                var e = new Error(profile.error.message);
                defer.reject(e);
            }

            console.log(profile);
            defer.resolve(profile);
        })
    });
    return defer.promise;
}
Auth.prototype.google = function(data) {
    var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.GOOGLE_SECRET,
        redirect_uri: data.redirectUri,
        grant_type: 'authorization_code'
    };
    var defer = Q.defer();
    request.post(accessTokenUrl, {
        json: true,
        form: params
    }, function(err, response, token) {
        if (response.statusCode !== 200) {
            var e = new Error("Some Error Occurred");
            defer.reject(e);
        }
        var accessToken = token.access_token;
        var headers = {
            Authorization: 'Bearer ' + accessToken
        };

        request.get({
            url: peopleApiUrl,
            headers: headers,
            json: true
        }, function(err, response, profile) {
            if (profile.error) {
                var e = new Error(profile.error.message);
                defer.reject(e);
            }
            defer.resolve(profile);
        });
    });
    return defer.promise;
}
Auth.prototype.twitter = function(data) {
    var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
    var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
    var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';
    var defer = Q.defer();
    if (!data.oauth_token || !data.oauth_verifier) {
        var requestTokenOauth = {
            consumer_key: config.TWITTER_KEY,
            consumer_secret: config.TWITTER_SECRET,
            callback: data.redirectUri
        };

        // Step 1. Obtain request token for the authorization popup.
        request.post({
            url: requestTokenUrl,
            oauth: requestTokenOauth
        }, function(err, response, body) {
            var oauthToken = qs.parse(body);

            // Step 2. Send OAuth token back to open the authorization screen.
            defer.resolve(oauthToken);
        });
    } else {
        // Part 2 of 2: Second request after Authorize app is clicked.
        var accessTokenOauth = {
            consumer_key: config.TWITTER_KEY,
            consumer_secret: config.TWITTER_SECRET,
            token: data.oauth_token,
            verifier: data.oauth_verifier
        };
        console.log("accessTokenOauth.........", accessTokenOauth);

        // Step 3. Exchange oauth token and oauth verifier for access token.
        request.post({
            url: accessTokenUrl,
            oauth: accessTokenOauth
        }, function(err, response, accessToken) {
            if (response.statusCode !== 200) {
                var e = new Error("Some Error Occured");
                defer.reject(e);
            }
            accessToken = qs.parse(accessToken);
            console.log("accessToken.........", accessToken);

            var profileOauth = {
                consumer_key: config.TWITTER_KEY,
                consumer_secret: config.TWITTER_SECRET,
                oauth_token: accessToken.oauth_token
            };

            // Step 4. Retrieve profile information about the current user.
            request.get({
                url: profileUrl + accessToken.screen_name,
                oauth: profileOauth,
                json: true
            }, function(err, response, profile) {
                if (response.statusCode !== 200) {
                    var e = new Error("Some Error Occured");
                    defer.reject(e);
                }
                defer.resolve(profile);
            });
        });
    }
    return defer.promise;
}
Auth.prototype.linkedin = function(data) {
    var accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken';
    var peopleApiUrl = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url)';
    var params = {
        code: data.code,
        client_id: data.clientId,
        client_secret: config.LINKEDIN_SECRET,
        redirect_uri: data.redirectUri,
        grant_type: 'authorization_code'
    };
    var defer = Q.defer();
    request.post(accessTokenUrl, {
        form: params,
        json: true
    }, function(err, response, body) {
        if (response.statusCode !== 200) {
            var e = new Error(body.error_description);
            defer.reject(e);
        }
        var params = {
            oauth2_access_token: body.access_token,
            format: 'json'
        };

        // Step 2. Retrieve profile information about the current user.
        request.get({
            url: peopleApiUrl,
            qs: params,
            json: true
        }, function(err, response, profile) {
            if (response.statusCode !== 200) {
                var e = new Error("Some Error Occured");
                defer.reject(e);
            }
            defer.resolve(profile);
        });
    });
    return defer.promise;
}

Auth.prototype.updateSocialMediaDetails = async function(data) {

    debug("updateSocialMediaDetails.....",data);

    var usr_id = (data.usr_id || "").trim().toLowerCase();
    var dsp_nm = data.dsp_nm;
    var usr_email = data.usr_email;
    var usr_ph = data.usr_ph || "";
    var usr_role = data.usr_role || "public";
    var reg_dt = new Date();

    try{
        //save role to usr_dtls table
        await profileApi.saveUserBasicPersonalDetails({
            usr_id: usr_id,
            role: usr_role,
            dsp_nm: dsp_nm
        });

        await profileApi.saveShortDetails({
            usr_id: usr_id,
            pwd: '',
            dsp_nm: dsp_nm,
            usr_email: usr_email,
            usr_ph: usr_ph,
        });

        // add primary email to contact details
        await profileApi.addEmailInContactDetails({
            usr_id : usr_id,
            email: usr_email,
            isPrimary: true
        });

        //add support as a friend in all new registered user
        // await profileApi.addConfirmedFriends({usr_id: usr_id,fr_id: 'support'});
        await chatFriendsApi.addFriend({user_id: usr_id,fr_id: 'support'});
        // await chatFriendsApi.addFriend({user_id: 'support',fr_id: usr_id});

        //add user chat short details
        await chatFriendsApi.addUserShortDetails({
            user_id: usr_id,
            user_email: usr_email,
            display_name: dsp_nm,
            typ: 'usr',
            pic: ''
        })

        if(usr_email){
            //add as a subscriber to the site
            await subscribeAPI.subscribeUser({
                subs_email: usr_email,
                subs_typ : 'from social media',
                usr_id : usr_id
            });     
        }
          
        return {}; 
    }catch(err){
        debug(err);
        throw err;
    }
}

Auth.prototype.generatePassportJWT =async function(data) {
    if (data.err) {
        return data;
    }
    if (!data.firstTime) {
        data.firstTime = false;
    }
    delete data.user["pwd"];

    var payload = {
        id: data.user.usr_id,
        usr:data.user
    };
    data['token'] = authJWT.encode(payload, config.TOKEN_SECRET);

    var usercoursesRes = await userCoursesAPI.getPurchasedCourseOfUser({
        usr_id: data.user.usr_id.trim().toLowerCase()
    });
    
    var usercourses = usercoursesRes.map((v, i) => {
        return v.crs_id;
    })

    data.user.usr_pic = data.user.usr_pic || "/assets/images/dummy-user-pic.png";

    data['uc'] = usercourses;
    debug(data);
    return data;

};

Auth.prototype.generateJWT = async function(data) {
    try{
        if (data.err) {
            return data;
        }
        if (!data.firstTime) {
            data.firstTime = false;
        }

        console.log("data....",data)

        delete data.user["pwd"];
        var payload = {
            userId: data.user.usr_id.trim().toLowerCase()
        };

        var token = jwt.sign(payload, config.TOKEN_SECRET, {
            expiresIn: config.TOKEN_EXPIRE
        });

        var usercoursesRes = await userCoursesAPI.getPurchasedCourseOfUser({
            usr_id: data.user.usr_id.trim().toLowerCase()
        });

        var usercourses = usercoursesRes.map((v, i) => {
            return v.crs_id;
        })

        let tempUser = await profileApi.getUserShortDetails({usr_id : data.user.usr_id.trim().toLowerCase()})

        data.user = {
            ...tempUser,
            is_act : data.user.is_act
        }

        data.user.usr_pic = data.user.usr_pic || "/assets/images/dummy-user-pic.png";

        var obj = {
            token: token,
            firstTime: data.firstTime,
            user: data.user,
            uc: usercourses
        };
        return obj;
    }catch(err){
        throw new Error(err);
    }
}
module.exports = new Auth();