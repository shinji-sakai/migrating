var configPath = require('../../configPath.js');
var shortid = require('shortid');
var Promise = require("bluebird");
var debug = require('debug')("app:api:aws:s3.api.js");
var fs = require('fs');
var path = require('path');

var awsConfig = require(configPath.config.awsConfig);
var AWS = require('aws-sdk');
AWS.config.region = 'ap-south-1';
var awsS3 = new AWS.S3();
awsS3 = Promise.promisifyAll(awsS3);

function S3(){}

S3.prototype.createBucket = async function(data){
	try{
		var bucketName = awsConfig["bucket"][data.type];
		var params = {
			Bucket : bucketName,
			CreateBucketConfiguration: {
			    LocationConstraint: 'ap-south-1'
			}
		}
		var res = await awsS3.createBucketAsync(params);
		debug(res);
		return res;
	}catch(err){
		debug(err);
		throw err;
	}
}

S3.prototype.getBuckets = async function(data){
	try{
		var res = await awsS3.listBucketsAsync();
		debug(res);
		return res;
	}catch(err){
		debug(err);
		throw err;
	}
}

S3.prototype.uploadFile = async function(type,fileName,filePath,folderPath){
	try{
		var bucketName = awsConfig["bucket"][type];
		var uploadParams = {
			Bucket: bucketName, 
			Key: '', 
			Body: '',
			ACL : awsConfig["ACL"][type]
		};
		var file = (folderPath || '') + filePath;
		var fileStream = fs.createReadStream(file);
		
		debug(bucketName,type,fileName,filePath);

		fileStream.on('error', function(err) {
		  console.log('File Error', err);
		});

		uploadParams.Body = fileStream;
		uploadParams.Key = fileName;

		var res = await awsS3.uploadAsync(uploadParams);
		return res;
	}catch(err){
		debug(err);
		throw err;
	}	
}

S3.prototype.getSignedUrl = async function(type,key){
	try{
		var bucketName = awsConfig["bucket"][type];
		var expiryTime = awsConfig.expiryTime[type] || 3600;
		var params = {Bucket: bucketName, Key: key,Expires:  expiryTime};
		var url = await awsS3.getSignedUrlAsync('getObject', params);
		return url;	
	}catch(err){
		debug(err);
		throw err;
	}
}

module.exports = new S3();