var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videonotes');
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
// var authorizeitemsAPI = require(configPath.api.authorizeitems);
// var dashboardStatsAPI = require(configPath.api.dashboard.stats);

// var ew_vdo_note_mdls_tbl = "ew1.ew_vdo_note_mdls";
// var ew_vdo_notes_tbl = "ew1.ew_vdo_notes";
// var ew_vdo_notes_usr_tbl = "ew1.ew_vdo_notes_usr";
var ew_usr_notes_table = "ew1.ew_usr_notes";

function NotesAPI() {}

NotesAPI.prototype.getNotes = async function (data) {
    try{
        let { usr_id , src_id } = data;
        let qry = `select * from ${ew_usr_notes_table} where usr_id = ? and src_id = ?`;
        let params = [usr_id,src_id];
        let rows = await cassExecute(qry,params);
        return rows
    }catch(err){
        logger.debug(err)
        throw new Error(err.toString())
    }
}

NotesAPI.prototype.insertNote = async function (data) {
    try{
        var usr_id = data.usr_id;
        var crs_id = data.crs_id;
        var mdl_id = data.mdl_id;
        var src_id = data.src_id;
        var note_id = Uuid.random();
        var note = data.note;
        var vdo_tm = data.vdo_tm;
        var hlight_id = data.hlight_id;
        var note_src = data.note_src;
        var crt_dt = new Date();


        var qry = "insert into " + ew_usr_notes_table + " (usr_id,crs_id,mdl_id,src_id,note_id,note,vdo_tm,hlight_id,note_src,crt_dt) values (?,?,?,?,?,?,?,?,?,?)" ;
        var params = [usr_id,crs_id,mdl_id,src_id,note_id,note,vdo_tm,hlight_id,note_src,crt_dt];

        await cassExecute(qry,params);

        return {
            ...data,
            note_id,
            crt_dt
        }
    }catch(err){
        throw new Error(err)
    }
}

NotesAPI.prototype.updateNote = async function (data) {
    try{
        let { usr_id , src_id , note_id, note } = data;
        var qry = "update " + ew_usr_notes_table + " set note=? where usr_id=? and note_id=? and src_id =?";
        var params = [note,usr_id,note_id,src_id];
        await cassExecute(qry,params);
        return data;
    }catch(err){
        throw new Error(err);
    }
}

NotesAPI.prototype.deleteNote = async function (data) {
    try{
        let { usr_id , src_id , note_id } = data;
        var qry = "delete from " + ew_usr_notes_table + " where usr_id=? and note_id=? and src_id =?";
        var params = [usr_id,note_id,src_id];
        await cassExecute(qry,params);
        return data;
    }catch(err){
        throw new Error(err);
    }
}

module.exports = new NotesAPI();