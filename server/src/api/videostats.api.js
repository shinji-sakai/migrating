var configPath = require('../configPath.js');

var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);

var Q=require('q');

function VideoStats(){

}

VideoStats.prototype.insertStats = function(data){
	return getConnection()
			.then(dbCollection)
			.then(insertStats)
			.catch(globalFunctions.err);

	function insertStats(col){
		return col.insert(data);
	}
}

function dbCollection(db){
	return db.collection("videostats");
}


module.exports = new VideoStats();