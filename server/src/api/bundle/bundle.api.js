var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var uuid = require('uuid');
var debug = require('debug')("app:api:bundle.api.js");

var Q=require('q');

function Bundle(){}

Bundle.prototype.getAll = async function () {
    try{
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await find(collection);
        return find_res;
    }catch(e){
        globalFunctions.err(e);
        throw e;
    }

    function find(col) {
        return col.find({}, { _id: 0}).toArray();
    }
}

Bundle.prototype.add = function (data) {
    return getConnection()      //get mongodb connection
        .then(dbCollection)     //get database collection
        .then(insertData)
        .catch(function(err) {
            console.log(err);
        });

    function insertData(col) {
        //insert/update in batchcourse
        var bndl_id = data.bndl_id || uuid.v4();
        data.bndl_id = bndl_id;

        return col.update({bndl_id : bndl_id},data,{upsert:true,w:1});
    }


}
Bundle.prototype.delete = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({bndl_id: data.bndl_id});
    }
}

Bundle.prototype.getFullBundleDetailById = async function (data) {
    var defer = Q.defer();
    var bndl_id = data.bndl_id;
    try{
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var res = await find(collection);
        var crs_ids = res.includedCourses;
        var res_crs = await courseListAPI.getCoursesById({ids : crs_ids});
        res["courses"] = res_crs;
        defer.resolve(res);
    }catch(e){
        defer.reject(e);
    }
    return defer.promise;


    function find(col) {
        return col.findOne({bndl_id:bndl_id}, { _id: 0});
    }
}

//Here you can pass multiple bundles
Bundle.prototype.getFullBundlesDetail = async function (data) {
    var defer = Q.defer();
    var bndl_ids = data.bndl_ids;
    console.log("getFullBundlesDetail..." + bndl_ids);
    try{
        var res = [];
        for (var i = bndl_ids.length - 1; i >= 0; i--) {
            var bndl_dtl = await this.getFullBundleDetailById({bndl_id : bndl_ids[i]});
            res.push(bndl_dtl);
        }
        defer.resolve(res);
    }catch(e){
        defer.reject(e);
    }
    return defer.promise;


    function find(col) {
        return col.findOne({bndl_id:bndl_id}, { _id: 0});
    }
}

Bundle.prototype.getCoursesInBundle = async function (data) {
    var bndl_id = data.bndl_id;
    try{
        var conn = await getConnection();
        var col = await dbCollection(conn);
        var res = await col.findOne({bndl_id : bndl_id}, { _id: 0});
        res = res || {};
        res.includedCourses = res.includedCourses || [];
        return res.includedCourses;
    }catch(e){
        debug(e);
        throw e;
        return;
    }
}

Bundle.prototype.getBundlesByCourseId = async function (data) {
    var crs_id = data.crs_id;
    try{
        var conn = await getConnection();
        var col = await dbCollection(conn);
        var res = await col.find({includedCourses:crs_id}, { _id: 0}).toArray();
        return res;
    }catch(e){
        debug(e);
        throw e;
        return;
    }
}



function dbCollection(db){
    return db.collection("bundle");
}

module.exports = new Bundle();