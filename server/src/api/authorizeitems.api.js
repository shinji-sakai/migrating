var configPath = require('../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);

var videoUrlApi = require(configPath.api.videoUrl);
var videoEntityApi = require(configPath.api.video.videoentity);
var debug = require('debug')('app:api:authorizeitems');

var Q = require('q');

class AuthorizeItems {
    getAuthorizeItems(data) {
        return getConnection()
            .then(dbCollection)
            .then(findAllItems)
            .catch(globalFunctions.err);

        function findAllItems(col) {
            return col.find({
                courseId: data.courseId
            }, {
                _id: 0
            }).toArray();
        }
    }
    updateItemType(data) {
        return getConnection()
            .then(dbCollection)
            .then(updateAllItems)
            .catch(globalFunctions.err);

        function updateAllItems(col) {
            var promise_arr = [];
            for (var i = 0; i < data.length; i++) {
                promise_arr.push(col.updateOne({
                    itemId: data[i].itemId
                }, data[i], {
                    upsert: true,
                    w: 1
                }));
            }
            // console.log(promise_arr)
            return Q.all(promise_arr);
        }
        return getConnection().then(function(db) {
            var col = db.collection('coursemodule');
            return col.updateOne({
                moduleId: data.moduleId
            }, data, {
                upsert: true,
                w: 1
            });
        }, function(err) {
            console.log(err);
        });
    }
    async isItemExist(data) {
        try {
            var conn = await getConnection();
            var col = await dbCollection(conn);
            var rows = await col.find({
                courseId: data.courseId,
                itemId: data.itemId
            }, {
                _id: 0
            }).toArray();
            if (rows.length > 0) {
                return {
                    exist: true
                }
            } else {
                return {
                    exist: false
                }
            }
        } catch (err) {
            throw err;
        }
    }
    addItem(data) {
        return getConnection()
            .then(dbCollection)
            .then(insert)
            .catch(globalFunctions.err);

        function insert(col) {
            return col.updateOne({
                itemId: data.itemId
            }, data, {
                upsert: true,
                w: 1
            });
        }
    }
    async getVideoItemFullInfo(data) {
        try {
            var conn = await getConnection();
            var col = await dbCollection(conn);
            var resItem = await col.findOne({
                itemId: data.itemId
            });
            //get item video id
            resItem = resItem || {};
            var itemVideo = resItem.itemVideo;
            //get video entity using video id
            var videoEntity = await videoEntityApi.findVideoEntityById({
                videoId: itemVideo
            });
            videoEntity = videoEntity[0] || {};
            var source = videoEntity.videoSource || "harddrive";
            var name = videoEntity.videoUrl;
            //get video url using item source and name
            var url = await videoUrlApi[source](name);
            videoEntity["fullUrl"] = url;
            resItem["videoEntity"] = videoEntity;
            return resItem;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async getItemsUsingItemId(data) {
        try {
            var ids = [].concat(data.itemId);
            var conn = await getConnection();
            var col = await dbCollection(conn);
            var resItem = await col.find({
                itemId: {
                    $in: ids
                }
            }).toArray();
            return resItem;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async getVideoPermission(data) {
        try {
            var videoId = data.videoId;
            var conn = await getConnection();
            var col = await dbCollection(conn);
            var resItem = await col.find({
                itemVideo: videoId
            }).toArray();
            resItem = resItem || [];
            if (resItem.length === 1) {
                return {
                    videoId: videoId,
                    ...resItem[0]
                    // itemType : resItem[0].itemType
                }
            } else if (resItem.length > 1) {
                var type = 'paid';
                for (var i = 0; i < resItem.length; i++) {
                    var item = resItem[i];
                    if ((type === "paid" || type === "demologin") && item.itemType === "demo") {
                        type = "demo";
                        break;
                    } else if ((type === "paid") && item.itemType === "demo") {
                        type = "demologin";
                        break;
                    } else if (item.itemType === "paid") {
                        type = "paid";
                        break;
                    }
                }
                return {
                    videoId: videoId,
                    itemType: type
                }
            } else {
                return {
                    videoId: videoId,
                    itemType: "demo"
                }
            }
        } catch (e) {
            debug(e);
            throw e;
        }
    }
}

function dbCollection(db) {
    return db.collection("authorizeitems");
}


module.exports = new AuthorizeItems();