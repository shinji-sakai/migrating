var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:follow");
var uuid = require('uuid');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;
var async = require('async');
var cassExecute = require(configPath.lib.utility).cassExecute;
var adminSendNotificationAPI = require(configPath.api.admin.sendNotification)

var ew_qry_flw_tbl = 'ew1.ew_qry_flw';
var ew_qry_flw_ntf_tbl = 'ew1.ew_qry_flw_ntf';
var ew_qry_usr_flw_ = "ew1.ew_qry_usr_flw_"

class QAFollow {

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Question Id
     * @returns {Promise}
     * @desc
     * - This method is used to get all followers for the given question
     * - Tables Used : {@link CassandraTables#ew_qry_flw}
     */
    getAllFollowers(data) {
        //data = {qry_id : ''}
        var defer = Q.defer();

        var select = 'select * from ' + ew_qry_flw_tbl + ' where qry_id = ?';
        var args = [data.qry_id];

        //cassandra query
        cassconn.execute(select, args, function(err, res, r) {
            if (err) {
                //if error
                debug(err);
                defer.reject(err);
                return;
            }
            var rows = res.rows || [];
            var json_rows = JSON.parse(JSON.stringify(res.rows));
            //return all rows
            defer.resolve(json_rows);
        });
        return defer.promise;
    }

    async getUserFollowedQuestion(data){
        try{
            let { usr_id , last_qry_id } = data;
            let qry;
            let params;
            if(last_qry_id){
                query = `select * from ${ew_qry_usr_flw_} where usr_id = ? and qry_id < ? limit 5`;
                params = [usr_id , last_qry_id]
            }else{
                query = `select * from ${ew_qry_usr_flw_} where usr_id = ? limit 5`;
                params = [usr_id]
            }
            var res = await cassExecute(qry , params);
            return res;
        }catch(err){
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Question Id
     * @param {string} data.usr_id - User Id
     * @returns {Promise}
     * @desc
     * - This method is used to update follow status of given user for given question
     * - Tables Used : {@link CassandraTables#ew_qry_flw}
     */
    updateFollower(data) {
        var defer = Q.defer();
        var qry;
        var qry_params;

        qry = 'insert into ' + ew_qry_flw_tbl + ' (usr_id,qry_id,flw_ts) values (?,?,?)';
        qry_params = [data.usr_id, data.qry_id, new Date()];

        //query execute
        cassconn.execute(qry, qry_params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Question Id
     * @param {string} data.usr_id - User Id
     * @returns {Promise}
     * @desc
     * - This method is used to delete follower if user choose to unfollow the question
     * - Tables Used : {@link CassandraTables#ew_qry_flw}
     */
    deleteFollower(data) {
        // data = {qry_id : '' , usr_id : ''}
        var defer = Q.defer();
        var qry;
        var qry_params;
        qry = 'delete from ' + ew_qry_flw_tbl + ' where qry_id=? and usr_id=?';
        qry_params = [data.qry_id, data.usr_id];


        //query execute
        cassconn.execute(qry, qry_params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Question Id
     * @param {string} data.ans_usr_id - Userid , who posted the answer
     * @param {uuid} data.ans_id - answer id
     * @returns {Promise}
     * @desc
     * - First we will get all follower using {@link QAFollow#getAllFollowers}
     * - Then for each follower we will add notification to {@link CassandraTables#ew_qry_flw_ntf}
     * - Tables Used : {@link CassandraTables#ew_qry_flw_ntf}
     */
    updateNotifications(data) {
        // data = {qry_id,ans_usr_id,ans_id}
        var defer = Q.defer();
        var timeuuid_now = TimeUuid.now();
        var qry = "insert into " + ew_qry_flw_ntf_tbl + '(qry_id,usr_id,ans_usr_id,ans_id,ans_ts,ntf_vw_flg,ntf_typ,qry_text) values (?,?,?,?,?,?,?,?)';
        var qry_params;

        var queries = [];

        var followers_promise = this.getAllFollowers({
            qry_id: data.qry_id
        });

        //get all followers first
        followers_promise
            .then(async function(followers) {

                //iterate through all follower
                followers.forEach(function(v) {
                    // prepare query params for all followers
                    qry_params = [data.qry_id, v.usr_id, data.ans_usr_id, data.ans_id, timeuuid_now, false , 'follow' , data.qry_title];
                    if(v.usr_id !== data.ans_usr_id){
                      //push it to query array
                      queries.push({
                          query: qry,
                          params: qry_params
                      });
                    }
                });

                //execute all queries all at once in batch
                var queryOptions = {
                    prepare: true,
                    consistency: cassandra.types.consistencies.quorum
                };
                if(queries.length > 0){

                    for (var i = 0; i < followers.length; i++) {
                      let follower = followers[i];
                      if(follower.usr_id === data.ans_usr_id){
                        continue;
                      }
                      // update notification counter
                      await adminSendNotificationAPI.incrementSendNotificationCount({
                        usr_id : follower.usr_id,
                        type : 'qa'
                      })
                    }

                    cassconn.batch(queries, queryOptions, function(err, res, r) {
                        if (err) {
                            debug(err);
                            defer.reject(err);
                            return;
                        }
                        defer.resolve({});
                    });
                }else{
                    defer.resolve({});
                }
            })
            .catch(function(err) {
                debug(err);
                defer.reject(err);
                return;
            });
        return defer.promise;
    }

    async getNotifications(data){
        try{
            let { usr_id , last_notif_id } = data;
            let query;
            let params;
            if(last_notif_id){
                query = `select toTimestamp(ans_ts) as ans_timestamp,usr_id,ans_id,ans_ts,ans_usr_id,ntf_typ,ntf_vw_flg,qry_id,qry_text from ${ew_qry_flw_ntf_tbl} where usr_id = ? and ans_ts < ? limit 5`;
                params = [usr_id , last_notif_id]
            }else{
                query = `select toTimestamp(ans_ts) as ans_timestamp,usr_id,ans_id,ans_ts,ans_usr_id,ntf_typ,ntf_vw_flg,qry_id,qry_text from ${ew_qry_flw_ntf_tbl} where usr_id = ? limit 5`;
                params = [usr_id]
            }
            var res = await cassExecute(query , params);
            return res;
        }catch(err){
            throw new Error(err.toString());
        }
    }

    async markFollowNotificationRead(data){
      try{
          let { ans_usr_id , ans_ts , usr_id } = data;
          let query;
          let params;

          query = `update ${ew_qry_flw_ntf_tbl} set ntf_vw_flg=true where usr_id = ? and ans_ts = ? and ans_usr_id = ?`;
          params = [usr_id , ans_ts , ans_usr_id]

          var res = await cassExecute(query , params);

          await adminSendNotificationAPI.decrementSendNotificationCount({
            type : 'qa',
            usr_id
          })

          return res;
      }catch(err){
          throw new Error(err.toString());
      }
    }
}


module.exports = new QAFollow();
