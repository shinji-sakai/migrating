var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:forums");
var uuid = require('uuid');

var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var cassExecute = require(configPath.lib.utility).cassExecute;

var vt_qry_cats = 'ew1.vt_qry_cats';
var vt_qry_cats_crsmdl = 'ew1.vt_qry_cats_crsmdl';

/**
 * QA Category API
 */
class Categories{
    /**
     * @returns {Promise}
     * @desc
     * - This method is used to get all categories for qa
     * - Tables Used : {@link CassandraTables#vt_qry_cats}
     */
    getAllCategories(data) {
        var defer = Q.defer();
        //select query
        var selectQry = 'select * from ' + vt_qry_cats;
        //execute
        cassconn.execute(selectQry,null,function(err,res,r) {
            if(err){
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });
        return defer.promise;
    }
    
    /**
     * @param {JSON} data
     * @param {string} data.cat_name - Category Name
     * @param {string} data.cat_desc - Category Description
     * @param {uuid} data.cat_id - Category Id <Optional>
     * @returns {Promise}
     * @desc 
     * - This method is used to save/update category
     * - Tables Used : {@link CassandraTables#vt_qry_cats}
     */    
    saveCategory(data) {
        var defer = Q.defer();
        let randomUUID;
        var qry;
        var qry_params;
        if(data.cat_id){
            //update
            qry = 'update ' + vt_qry_cats + ' set crs_id=?,mdl_id=?,crs_mdl_id=?,cat_name=?,cat_desc=?,crt_ts=? where cat_id=?';
            qry_params = [data.cat_name,data.cat_desc,new Date(),data.cat_id];
        }else{
            randomUUID = Uuid.random();
            //insert
            qry = 'insert into ' + vt_qry_cats + ' (cat_id,cat_name,cat_desc,crs_id,mdl_id,crs_mdl_id,crt_ts) values (?,?,?,?,?,?,?)';
            qry_params = [randomUUID,data.cat_name,data.cat_desc,data.crs_id,data.mdl_id,data.crs_mdl_id,new Date()];
        }
        
        cassconn.execute(qry,qry_params,function(err,res,r) {
            if(err){
                defer.reject(err);
                return;
            }
            defer.resolve({
                ...data,
                cat_id : randomUUID
            });
        });
        return defer.promise;
    }
    
    /**
     * @param {JSON} data
     * @param {uuid} data.cat_id - Category Id
     * @returns {Promise}
     * @desc 
     * - This method is used to delete category using id
     * - Tables Used : {@link CassandraTables#vt_qry_cats}
     */    
    deleteCategory(data) {
        var defer = Q.defer();
        var qry;
        var qry_params;
        qry = 'delete from ' + vt_qry_cats + ' where cat_id=?';
        qry_params = [data.cat_id];
        
        cassconn.execute(qry,qry_params,function(err,res,r) {
            if(err){
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    async findAndSaveCategoryBasedOnCourseAndModuleId(data){
        try{
            let findQuery = `select * from ${vt_qry_cats_crsmdl} where crs_mdl_id = ?`;
            let findParams = [data.crs_mdl_id];
            let rows = await cassExecute(findQuery,findParams);
            if(rows && rows.length > 0){
                return rows[0]
            }else{
                let res = await this.saveCategory(data);
                return res;
            }
        }catch(err){
            throw new Error(err)
        }
    }
}


module.exports = new Categories();