var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:cmnts");
var profileAPI = require(configPath.api.dashboard.profile);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassandra = require('cassandra-driver');
var TimeUuid = cassandra.types.TimeUuid;
var logger = require(configPath.lib.log_to_file);

var dashboardStatsAPI = require(configPath.api.dashboard.stats)
var profileAPI = require(configPath.api.dashboard.profile)
var adminSendNotificationAPI = require(configPath.api.admin.sendNotification)
// var commentAPI = require(configPath.api.dashboard.comment)

var forum_table = 'ew1.vt_forum_qrys';
var qa_ans_table = 'ew1.vt_usr_qry_ans';
var ew_qa_like_tbl = "ew1.ew_qa_like";
var ew_qa_like_stats_tbl = "ew1.ew_qa_like_stats"
var ew_qry_flw_ntf_tbl = 'ew1.ew_qry_flw_ntf';

const FETCH_ANS_LIMIT = 5

class QAAnswer {
    /**
     * @param {JSON} data
     * @param {uuid} data.usr_id - Usrid who posted the answer
     * @param {uuid} data.pst_id - QA Que ID to which answer is related
     * @param {set} data.cmnt_atch - Array Of Answer Attachment
     * @param {uuid} data.cmnt_cmnt_id - Not related
     * @param {set} data.cmnt_img - Image attached to answer
     * @param {text} data.cmnt_lvl - Not related
     * @param {text} data.cmnt_txt - Answer Text
     * @param {set} data.cmnt_vid - Video attached to answer.
     * @returns {Promise}
     * @desc
     * - This function is used to insert new qa answer
     * - If Comment source is qa-ans then we will increment dashboard stats for qa questions answered. See {@link DashboardStats#incrementForumQuestionAnsweredCounter}
     * - Else we will increment dashboard stats for comments. See {@link DashboardStats#incrementCommentCounter}
     * - Tables Used : {@link CassandraTables#vt_usr_qry_ans}
     */
    async saveAnswer(data) {
        try {
            var defer = Q.defer();
            var qry_id = data.pst_id;
            var ans_id = TimeUuid.now();
            var ans_ts = new Date();
            var ans_txt = data.cmnt_txt;
            var usr_id = data.usr_id;
            var timeuuid_now = TimeUuid.now()

            var insertPost = 'INSERT INTO ' + qa_ans_table + ' (qry_id,ans_id,ans_create_dt,ans_txt,usr_id)  ' + 'VALUES(?, ?, ?, ?, ?);';
            var insertPost_args = [qry_id, ans_id, ans_ts, ans_txt, usr_id];

            //save answer
            await cassExecute(insertPost, insertPost_args);

            var getQuestionQry = 'select * from ' + forum_table + ' where qry_id = ?';
            var getQuestionQryParams = [qry_id];

            // get qa question to find author
            let que = await cassExecute(getQuestionQry, getQuestionQryParams);
            let author_id;
            que = que[0];
            if(que){
              author_id = que.usr_id
            }

            if(usr_id !== author_id){
              // notification query
              var notif_qry = "insert into " + ew_qry_flw_ntf_tbl + '(qry_id,usr_id,ans_usr_id,ans_id,ans_ts,ntf_vw_flg,ntf_typ,qry_text) values (?,?,?,?,?,?,?,?)';
              var notif_params = [qry_id,author_id,usr_id,ans_id,timeuuid_now,false,'owner',que.qry_title];
              // save notification
              await cassExecute(notif_qry, notif_params);

              // update notification counter
              await adminSendNotificationAPI.incrementSendNotificationCount({
                usr_id : author_id,
                type : 'qa'
              })
            }

            await dashboardStatsAPI.incrementForumQuestionAnsweredCounter({
                usr_id: usr_id
            });

            return {
                cmnt_id: ans_id,
                ans_ts: ans_ts,
                ...data
            };
        } catch (err) {
            logger.debug(err);
            throw new Error(err.toString())
        }
    }

    async getAllAnswersOfQuestion(data) {
        try {
            let {
                qry_id,
                usr_id,
                last_ans
            } = data

            let query ;
            let params ;

            if(last_ans){
                query = " select * from " + qa_ans_table + " where qry_id = ? and ans_id < ? limit 5";
                params = [qry_id,last_ans];
            }else{
                query = " select * from " + qa_ans_table + " where qry_id = ? limit 5";
                params = [qry_id];
            }



            let res = await cassExecute(query, params);

            res = res || [];

            if (res.length <= 0) {
                return []
            }

            let result = []

            for (var i = 0; i < res.length; i++) {
                var ans = res[i]
                var ans_details = await this.getAnswerOfQuestionUsingId({
                    qry_id: qry_id,
                    ans_id: ans["ans_id"],
                    usr_id: usr_id
                })
                if (ans_details && ans_details.length === 1) {
                    result.push(ans_details[0])
                }
            }

            return result;
        } catch (err) {
            logger.debug(err);
            throw new Error(err)
        }
    }

    async getOneLatestAnswerOfQuestion(data) {
        try {
            let {
                qry_id
            } = data
            let query = " select * from " + qa_ans_table + " where qry_id = ?";
            let params = [qry_id];

            let res = await cassExecute(query, params);

            res = res || [];

            if (res.length <= 0) {
                return {}
            }

            let result = {}
            var ans = res[0] || {}
            var ans_details = await this.getAnswerOfQuestionUsingId({
                qry_id: qry_id,
                ans_id: ans["ans_id"]
            })
            if (ans_details && ans_details.length === 1) {
                result.ans_id  = ans_details[0].ans_id
                result.usr_details  = ans_details[0].usr_details
                result.ans_create_dt  = ans_details[0].ans_create_dt
            }

            return {
                ...result,
                totalAnswers : res.length
            };
        } catch (err) {
            logger.debug(err);
            throw new Error(err)
        }
    }

    async getAnswerOfQuestionUsingId(data) {
        try {
            let {
                qry_id,
                ans_id,
                usr_id
            } = data
            let query = " select * from " + qa_ans_table + " where qry_id = ? and ans_id = ?";
            let params = [qry_id,ans_id];

            let res = await cassExecute(query, params);

            if (res && res.length > 0) {
                // res = res.filter((v, i) => {
                //     if (v.cmnt_id === ans_id)
                //         return true;
                //     return false;
                // })
                let author_id = res[0]["usr_id"]

                let users_detail = await profileAPI.getUsersShortDetails({
                    usr_ids: [author_id]
                })

                users_detail = users_detail[0] || {}

                delete users_detail["usr_email"]
                delete users_detail["usr_role"]
                delete users_detail["usr_ph"]

                res[0]["usr_details"] = users_detail

                if (usr_id) {
                    var likeRes = await this.getLikeDislikeByCmntId({
                        qry_id: qry_id,
                        usr_id: usr_id,
                        cmnt_id : ans_id
                    })
                    if (likeRes && likeRes.length > 0) {
                        res[0]["usr_likes_dislike"] = likeRes[0] || {}
                    } else {
                        res[0]["usr_likes_dislike"] = {}
                    }
                }

                if (ans_id) {
                    let ansLikeDislikeCount = await this.getLikeDislikeCountByCmntID({
                        qry_id: qry_id,
                        cmnt_id : ans_id
                    })
                    if (ansLikeDislikeCount && ansLikeDislikeCount.length > 0) {
                        res[0]["ans_like_dislike_count"] = ansLikeDislikeCount[0] || {}
                    } else {
                        res[0]["ans_like_dislike_count"] = {}
                    }
                }
                return res
            } else {
                return [];
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err)
        }
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.pst_id - QA Query ID
     * @param {uuid} data.cmnt_id - Answer Id which we want to edit
     * @param {timestamp} data.ans_ts - Answer Timestamp
     * @param {Array} data.cmnt_atch - Answer Attachments
     * @param {Array} data.cmnt_vid - Answer Video Attachments
     * @param {Array} data.cmnt_img - Answer Images Attachments
     * @param {string} data.cmnt_txt - Answer Text
     * @returns {Promise}
     * @desc
     * - This function is used to update comment data for given comment id
     * - Tables Used : {@link CassandraTables#vt_usr_qry_ans}
     */
    async editAnswer(data) {
        try {
            var qry_id = data.pst_id;
            var ans_id = data.cmnt_id;
            var ans_ts = new Date();
            var ans_txt = data.cmnt_txt;

            var updatePost = 'update ' + qa_ans_table + ' set ans_txt = ?,ans_update_dt = ? where ans_id = ? and qry_id = ?';
            var updatePost_args = [ans_txt,ans_ts, ans_id, qry_id];

            await cassExecute(updatePost, updatePost_args);

            return data
        } catch (err) {
            logger.debug(err);
            throw new Error(err)
        }
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.pst_id - QA Query ID
     * @param {uuid} data.cmnt_id - Answer Id which we want to delete
     * @param {timestamp} data.ans_ts - Answer Timestamp
     * @param {string} data.usr_id - Author of comment
     * @returns {Promise}
     * @desc
     * - This function is used to delete given answer id
     * - We will decrement dashboard stats answer counter. See {@link DashboardStats#decrementForumQuestionAnsweredCounter}
     * - Tables Used : {@link CassandraTables#vt_usr_qry_ans}
     */
    async deleteAnswer(data) {
        try {
            var qry_id = data.pst_id;
            var ans_id = data.cmnt_id;
            var usr_id = data.usr_id;

            var deletePost = 'delete from ' + qa_ans_table + ' where qry_id = ? and ans_id = ?';
            var deletePost_args = [qry_id, ans_id];

            await cassExecute(deletePost, deletePost_args);

            await dashboardStatsAPI.decrementForumQuestionAnsweredCounter({
                usr_id: usr_id
            });

            return data
        } catch (err) {
            logger.debug(err);
            throw new Error(err)
        }

    }

    likeDislike(data) {
        var defer = Q.defer();
        var qry_id = data.qry_id || null;
        var usr_id = data.usr_id || 'n/a';
        var cmnt_id = data.cmnt_id || null;
        var crt_ts = new Date();
        var like_dlike_flg = data.like_dlike_flg;

        if(!qry_id || !cmnt_id){
            return {err : "qry_id and cmnt_id parameters required"}
        }

        var insert = 'insert into ' + ew_qa_like_tbl + ' (qry_id,usr_id,cmnt_id,crt_ts,like_dlike_flg) values(?,?,?,?,?)';
        const insert_args = [qry_id, usr_id, cmnt_id, crt_ts, like_dlike_flg];

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.execute(insert, insert_args, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    async getLikeDislikeByCmntId(data) {

        try{
            var qry_id = data.qry_id || null;
            var usr_id = data.usr_id || 'n/a';
            var cmnt_id = data.cmnt_id || null;

            if(!qry_id || !cmnt_id){
                return { err : "qry_id and cmnt_id parameters required"}
            }

            var select = 'select * from ' + ew_qa_like_tbl + ' where qry_id = ? and  usr_id = ? and cmnt_id = ?';
            const select_args = [qry_id, usr_id, cmnt_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };

            let res = await cassExecute(select, select_args, queryOptions)
            return res;
        }catch(err){
            throw new Error(err)
        }
    }

    updateLikeDislikeCount(data) {
        var defer = Q.defer();

        //data = {qry_id : qry_id , cmnt_id : cmnt_id , like : -1/0/1 , dislike : -1/0/1}

        var qry_id = data.qry_id || null;
        var cmnt_id = data.cmnt_id || null;

        var like_cnt_str = ' like_cnt = like_cnt + ' + (data.like || 0);
        var dlike_cnt_str = ' dlike_cnt = dlike_cnt + ' + (data.dislike || 0);
        var where_clause = ' where qry_id = ? and cmnt_id = ?';

        var update_like = 'update ' + ew_qa_like_stats_tbl + ' set ' + like_cnt_str + "," + dlike_cnt_str + where_clause;
        const update_args = [qry_id, cmnt_id];

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.execute(update_like, update_args, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    async getLikeDislikeCountByCmntID(data) {
        try {
            //data = {qry_id , cmnt_id}

            var qry_id = data.qry_id || null;
            var cmnt_id = data.cmnt_id || null;

            if(!qry_id || !cmnt_id){
                return { err : "qry_id and cmnt_id parameters required"}
            }

            var select_like = 'select * from ' + ew_qa_like_stats_tbl + ' where qry_id = ? and cmnt_id = ?';
            const select_args = [qry_id , cmnt_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };

            let res = await cassExecute(select_like, select_args, queryOptions);

            return res;
        } catch (err) {
            throw new Error(err);
        }
    }
}

module.exports = new QAAnswer();
