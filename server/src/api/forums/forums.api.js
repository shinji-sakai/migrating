var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:forums");
var uuid = require('uuid');

var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var asy_lib = require('async');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var TimeUuid = cassandra.types.TimeUuid;

var FollowAPI = require(configPath.api.forums.follow);
var profile_api = require(configPath.api.dashboard.profile);
var postAPI = require(configPath.api.dashboard.post);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var qaAnsAPI = require(configPath.api.forums.ans);

var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

var forum_table = 'ew1.vt_forum_qrys';
var forum_table_read = 'ew1.vt_forum_qrys_read';
var trend_cnt = 'ew1.vt_qry_trend';
var trend_vws = 'ew1.vt_qry_vws';
var qry_stats = 'ew1.ew_qry_stats';
var qry_usr_stats = 'ew1.ew_qry_usr_stats';
var ew_qry_ans_ltr_tbl = 'ew1.ew_qry_ans_ltr';
var ew_qry_flw_ntf_tbl = 'ew1.ew_qry_flw_ntf';

var ew_del_rsn_tbl = 'ew1.ew_del_rsn';
var ew_complain_tbl = 'ew1.ew_cmpln_rsn';

var ew_qa_qry_counter_tbl = "ew1.ew_qa_qry_counter"

const QA_QUESTION_IDS = () => {
    return "qa-question-ids"
}
const NO_OF_QA_QUESTION_TO_FETCH = 5;

/**
 * QA API
 */
class QA {
    /**
     * @param {JSON} data
     * @param {string} data.qry_title - Question Description
     * @param {string} data.usr_id - Id of user who posted the question
     * @param {Array} data.qry_tags - Question Categories
     * @param {Array} data.qry_txt - Question Description
     * @param {Array} data.qry_pst_id - Post Id , if question is posted from careerbook
     * @returns {Promise}
     * @desc
     * - This method is used to save QA Question
     * - We will store new question id into `qa-question-ids` redis list also
     * - Increment dashboard stats counter for user {@link DashboardStats#incrementForumQuestionAskedCounter}
     * - Tables Used : {@link CassandraTables#vt_forum_qrys}
     */
    async saveQuestion(data) {
        try {
            // var qa_count = await this.getQAQuestionCounter();

            var defer = Q.defer();
            var qry_id = TimeUuid.now();
            var qry_title = data.qry_title;
            var qry_ts = new Date();
            var usr_id = data.usr_id;
            var qry_tags = data.qry_tags;
            var qry_txt = data.qry_txt;
            var qry_pst_id = data.qry_pst_id;
            //insert query
            var insertQry = 'insert into ' + forum_table + ' (qry_id,qry_tags,qry_title,qry_ts,qry_txt,usr_id,qry_pst_id,qry_bucket) values (?,?,?,?,?,?,?,?);';
            //insert params
            var insertQry_args = [qry_id, qry_tags, qry_title, qry_ts, qry_txt, usr_id, qry_pst_id,'1'];
            //execute
            await cassExecute(insertQry, insertQry_args);

            // await this.incrementQAQuestionCounter();

            //insert question id into redis qa-question-ids list
            // await redisCli.lpushAsync(QA_QUESTION_IDS(), qry_id.toString())

            //increment stats in dashboard stats
            await dashboardStatsAPI.incrementForumQuestionAskedCounter({
                usr_id: usr_id
            });



            return {
                qry_id
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.qry_title - Question Description
     * @param {string} data.qry_id - Question Id which we want to update
     * @returns {Promise}
     * @desc
     * - This method is used to update already post Question
     * - Tables Used : {@link CassandraTables#vt_forum_qrys}
     */
    updateQuestion(data) {
        var defer = Q.defer();
        var qry_title = data.qry_title;
        var qry_id = data.qry_id;
        var qry_ts = new Date();

        //update query
        var updateQry = 'update ' + forum_table + ' SET qry_title=?,qry_ts=? where qry_id=?;';
        //update params
        var updateQry_args = [qry_title, qry_ts, qry_id];
        //execute
        cassconn.execute(updateQry, updateQry_args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.qry_id - Question Id which we want to delete
     * @param {string} data.usr_id - Id of user who posted the question
     * @param {string} data.qry_pst_id - this is id of post from which this question is shared For ex. CB
     * @param {string} data.del_rsn - Reason , why user want to delete the question
     * @returns {Promise}
     * @desc
     * - This method is used to delete Posted Question
     * - If there is any linked post with this question , it will update that also. See {@link CBPost#getPostData} and {@link CBPost#edit}
     * - We will delete `deleted question id` from `qa-question-ids` redis list also
     * - Decrement Dashboard Question Asked counter {@link DashboardStats#decrementForumQuestionAskedCounter}
     * - Tables Used : {@link CassandraTables#vt_forum_qrys} , {@link CassandraTables#ew_del_rsn}
     */
    async deleteQuestion(data) {
        var defer = Q.defer();
        var qry_id = data.qry_id;
        var qry_pst_id = data.qry_pst_id; // this is id of post from which this question is shared

        //update post in vt_posts and vt_posts_crt_by
        // TODO
        if(qry_pst_id){
            // postAPI.getPostData({
            //     postId: qry_pst_id
            // })
            // .then(function(d) {
            //     if (d.length > 0) {
            //         var post = d[0];
            //         //remove qry id ref from post
            //         post.pst_qry_id = null;
            //         post.pst_qry_desc = null;
            //         postAPI.edit(post);
            //     }
            // })
            // .catch(function(err){
            //     logger.debug(err);
            // })
        }

        try{
            //delete question from qry table
            var deleteQry = 'delete from ' + forum_table + ' where qry_id=?;';
            var deleteQry_args = [qry_id];

            //insert reason of deleting question into delete reason table

            var qry = "insert into " + ew_del_rsn_tbl + '(usr_id,del_ts,del_id,del_rsn,del_typ) values (?,?,?,?,?)';
            var params = [data.usr_id, new Date(), data.qry_id, data.del_rsn, 'qry'];

            var queries = [{
                query: deleteQry,
                params: deleteQry_args
            }, {
                query: qry,
                params: params
            }];

            //query execute
            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };

            // exectue dele query and insert del-rsn query
            await cassBatch(queries, queryOptions);

            // await this.decrementQAQuestionCounter();

            // await redisCli.lremAsync(QA_QUESTION_IDS(),1,qry_id.toString())

            //update stats(decrement question asked) in dashboard stats
            await dashboardStatsAPI.decrementForumQuestionAskedCounter({
                usr_id: data.usr_id
            });
            return {}
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }


    /**
     * @param {JSON} data
     * @param {int} data.no_of_questions - No of question to fetch
     * @returns {Promise}
     * @desc
     * - This method is used to get first few requested number of question
     * - First we will get question id from redis
     * - then we will get those question details from cassandra using {@link QA#getQuestions}
     * - Generally this method will be called when user refresh the page
     */
    async getFirstFewQuestions(data){
        try{
            let { no_of_questions } = data;
            no_of_questions = parseInt(no_of_questions || NO_OF_QA_QUESTION_TO_FETCH);
            // let question_ids = await redisCli.lrangeAsync(QA_QUESTION_IDS(),0, no_of_questions - 1);
            // if(question_ids && question_ids.length <= 0){
            //     return []
            // }
            let query = `select * from ${forum_table_read} limit ${no_of_questions}`;
            let rows = await cassExecute(query,null);
            if(rows && rows.length > 0){
                let last_que_id = rows[rows.length - 1].qry_id;
                let question_ids = rows.map((v,i)=>{
                    return v.qry_id
                })
                let requested_questions = await this.getQuestions({
                    qry_ids : question_ids
                })
                return {
                    questions : requested_questions || [],
                    last_que_id : last_que_id
                };
            }else{
                return {
                    questions : []
                }
            }
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {int} data.no_of_questions - No of question to fetch
     * @param {int} data.start_question_id - This will be the id of question from where we want to start fetching next question, start_question_id will not be taked in next questions
     * @returns {Promise}
     * @desc
     * - This method is used to get next few requested number of question from given question id
     * - First we will get question id from redis
     * - then we will get those question details from cassandra using {@link QA#getQuestions}
     * - Generally this method will be called when user is scrolling in qa page
     */
    async getNextFewQuestions(data){
        try{
            let { no_of_questions, start_question_id , last_que_id  } = data;
            no_of_questions = parseInt(no_of_questions || NO_OF_QA_QUESTION_TO_FETCH);
            // let question_ids = await redisCli.lrangeAsync(QA_QUESTION_IDS(),0,-1);
            // let pos = question_ids.indexOf(start_question_id);
            // if(pos > -1){
            //     question_ids = question_ids.slice(pos+1,pos+1+no_of_questions)
            //     if(question_ids && question_ids.length <= 0){
            //         return []
            //     }
            //     let requested_questions = await this.getQuestions({
            //         qry_ids : question_ids
            //     })
            //     return requested_questions || []
            // }else{
            //     return []
            // }
            let query;
            let params;
            if(data.last_que_id){
                query = `select * from ${forum_table_read} where qry_id < ? and qry_bucket = ? limit ${no_of_questions}`;
                params = [last_que_id,'1']
            }else{
                query = `select * from ${forum_table_read} limit ${no_of_questions}`;
                params = null
            }
            let rows = await cassExecute(query,params);
            if(rows && rows.length > 0){
                let last_que_id = rows[rows.length - 1].qry_id;
                let question_ids = rows.map((v,i)=>{
                    return v.qry_id
                })
                let requested_questions = await this.getQuestions({
                    qry_ids : question_ids
                })
                return {
                    questions : requested_questions || [],
                    last_que_id : last_que_id
                }
            }else{
                return {
                    questions : []
                }
            }
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.qry_id - Question Id which we want to delete
     * @param {string} data.usr_id - Id of user who posted the question
     * @param {string} data.qry_pst_id - this is id of post from which this question is shared For ex. CB
     * @param {string} data.del_rsn - Reason , why user want to delete the question
     * @returns {Promise}
     * @desc
     * - This function will be called when user will see perticular question , we will update question view count
     * - Tables Used : {@link CassandraTables#vt_qry_trend}
     */
    updateTrendingCounts(data) {
        var defer = Q.defer();
        //update query
        var insertQry = 'update ' + trend_cnt + ' set vw_cnt=vw_cnt+1 where qry_id = ?;';
        //query params
        var insertQry_args = [data.qry_id];
        //execute
        cassconn.execute(insertQry, insertQry_args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    /**
     * @returns {Promise}
     * @desc
     * - This function used to get all trending queries
     * - Tables Used : {@link CassandraTables#vt_qry_trend}
     */
    getTrendingQrys(data) {
        var defer = Q.defer();
        var insertQry = 'select * from ' + trend_cnt;
        cassconn.execute(insertQry, null, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.qry_id - Question Id
     * @param {string} data.usr_id - Id of user
     * @returns {Promise}
     * @desc
     * - This method will update question view timestamp of user
     * - Tables Used : {@link CassandraTables#vt_qry_vws}
     */
    updateTrendingViews(data) {
        var defer = Q.defer();
        var insertQry = 'insert into ' + trend_vws + ' (qry_id,usr_id,vw_ts) values(?,?,?)';
        var insertQry_args = [data.qry_id, data.usr_id, new Date()];
        cassconn.execute(insertQry, insertQry_args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.qry_id - Question Id
     * @param {number} data.qry_ans - number of query answer
     * @param {number} data.qry_dnv - number of query downvote
     * @param {number} data.qry_flw - number of query follower
     * @param {number} data.qry_shr - number of query share
     * @param {number} data.qry_upv - number of query upvote
     * @param {number} data.qry_vws - number of query views
     * @returns {Promise}
     * @desc
     * - This method will update question stats
     * - Tables Used : {@link CassandraTables#ew_qry_stats}
     */
    updateQuestionStatsCounter(data) {
        var defer = Q.defer();

        var qry_id = data.qry_id;
        var set_str_arr = [];



        if (data.qry_ans != undefined) {
            //if qry_ans is set
            set_str_arr.push('qry_ans = qry_ans + ' + data.qry_ans);
        }
        if (data.qry_dnv != undefined) {
            //if qry_dnv is set
            set_str_arr.push('qry_dnv = qry_dnv + ' + data.qry_dnv);
        }
        if (data.qry_flw != undefined) {
            //if qry_flw is set
            set_str_arr.push('qry_flw = qry_flw + ' + data.qry_flw);
        }
        if (data.qry_shr != undefined) {
            //if qry_shr is set
            set_str_arr.push('qry_shr = qry_shr + ' + data.qry_shr);
        }
        if (data.qry_upv != undefined) {
            //if qry_upv is set
            set_str_arr.push('qry_upv = qry_upv + ' + data.qry_upv);
        }
        if (data.qry_vws != undefined) {
            //if qry_vws is set
            set_str_arr.push('qry_vws = qry_vws + ' + data.qry_vws);
        }

        var updateQry = 'update ' + qry_stats + ' set ' + set_str_arr.join(',') + ' where qry_id = ?';
        var updateQry_args = [qry_id];
        cassconn.execute(updateQry, updateQry_args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string|Array} data.qry_id - Question(s) Id
     * @returns {Promise}
     * @desc
     * - This method is used to get question(s) stats using question id
     * - Tables Used : {@link CassandraTables#ew_qry_stats}
     */
    getQuestionStatsCounter(data) {
        var defer = Q.defer();

        //query id
        var qry_id = data.qry_id; // can be array of query id

        // logger.debug(typeof qry_id)

        // qry_id can be array or string
        var qry_ids = [].concat(qry_id);

        //merge with ,(comma)
        var qry_ids_str = "(" + qry_ids.join(",") + ")";

        // logger.debug(qry_ids_str)

        var selectQry = 'select * from ' + qry_stats + ' where qry_id in ' + qry_ids_str;

        //query cassandra db
        cassconn.execute(selectQry, null, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.qry_id - Question Id
     * @returns {Promise}
     * @desc
     * - This method is used to get question details using question id
     * - It will also fetch question stats based on question id. See {@link QA#getQuestionStatsCounter}
     * - Tables Used : {@link CassandraTables#vt_forum_qrys}
     */
    getQuestion(data) {
        var defer = Q.defer();
        var self = this;

        var insertQry = 'select * from ' + forum_table + ' where qry_id = ?';
        var insertQry_args = [data.qry_id];

        cassconn.execute(insertQry, insertQry_args, async function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var row;
            if (res.rows && res.rows.length > 0) {
                //if we have question
                row = JSON.parse(JSON.stringify(res.rows[0]));
            } else {
                defer.resolve([]);
                return;
            }

            if(row["usr_id"]){
                var allUsersShortDetails = await profile_api.getUsersShortDetails({
                    usr_ids: [row["usr_id"]]
                });
                allUsersShortDetails = allUsersShortDetails || [];
                let allUsersShortDetailsMap = {};
                allUsersShortDetails.map((v,i)=>{
                    delete v["usr_email"]
                    delete v["usr_role"]
                    delete v["usr_ph"]
                    allUsersShortDetailsMap[v.usr_id] = v;
                })
                row["usr_details"] = allUsersShortDetailsMap[row["usr_id"]]
            }

            //get question stats
            //
            self.getQuestionStatsCounter({
                    qry_id: row.qry_id
                })
                .then(function(resrow) {
                    //convert to JSON
                    if (resrow.length > 0) {
                        var obj = JSON.parse(JSON.stringify(resrow[0]));
                        //attach qry_stats to row obj
                        row['qry_stats'] = obj;
                    } else {
                        row['qry_stats'] = {};
                    }
                    defer.resolve(row);
                    return;
                })
                .catch(function(err) {
                    debug("err....", err);
                    defer.reject(err);
                    return;
                });
        });
        return defer.promise;
    }

    /**
     * @returns {Promise}
     * @desc
     * - This method is used to get all questions
     * - It will also fetch all question stats based on question id. See {@link QA#getQuestionStatsCounter}
     * - Tables Used : {@link CassandraTables#vt_forum_qrys}
     */
    getAllQuestion(data) {
        var self = this;

        var defer = Q.defer();
        var selectQry = 'select * from ' + forum_table;
        //select all questions
        cassconn.execute(selectQry, null, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }

            //convert to json
            var allQuestions = JSON.parse(JSON.stringify(res.rows));

            //get all qurestion ids to fetch stats of those question
            var qry_ids = allQuestions.map(function(v) {
                return v.qry_id
            });

            //get all question stats
            self.getQuestionStatsCounter({
                    qry_id: qry_ids
                })
                .then(function(resrow) {
                    //convert to JSON
                    if (resrow.length > 0) {
                        //covert to json
                        var obj = JSON.parse(JSON.stringify(resrow));

                        //attach qry_stats to all questions
                        allQuestions.forEach(function(v, i) {
                            var stat = obj.filter(function(t) {
                                if (t.qry_id === v.qry_id) {
                                    return t
                                };
                            })[0];
                            allQuestions[i]['qry_stats'] = stat;
                        });
                    }
                    defer.resolve(allQuestions);
                })
                .catch(function(err) {
                    debug("err getQuestionStats....", err);
                    defer.reject(err);
                })
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {Array} data.qry_ids - Questions Id
     * @returns {Promise}
     * @desc
     * - This method is used to get question details for given question ids
     * - It will also fetch question stats based on question id. See {@link QA#getQuestionStatsCounter}
     * - Tables Used : {@link CassandraTables#vt_forum_qrys}
     */
    getQuestions(data) {
        var self = this;

        var defer = Q.defer();
        var qry_ids = data.qry_ids || [];

        if(qry_ids.length <= 0){
            return;
        }

        var str_qry_ids = "(" + qry_ids.join(",") + ")";
        var selectQry = 'select * from ' + forum_table + ' where qry_id in ' + str_qry_ids;
        //select all questions
        cassconn.execute(selectQry, null, async function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }

            //convert to json
            var allQuestions = JSON.parse(JSON.stringify(res.rows));

            allQuestions = allQuestions || [];

            var allUsersId = allQuestions.map((v,i)=>v.usr_id)

            if(allUsersId && allUsersId.length > 0){
                var allUsersShortDetails = await profile_api.getUsersShortDetails({
                    usr_ids: allUsersId
                });
                allUsersShortDetails = allUsersShortDetails || [];
                let allUsersShortDetailsMap = {};
                allUsersShortDetails.map((v,i)=>{
                    delete v["usr_email"]
                    delete v["usr_role"]
                    delete v["usr_ph"]
                    allUsersShortDetailsMap[v.usr_id] = v;
                })
                console.log(allUsersShortDetailsMap)
                allQuestions = allQuestions.map(function(v, i) {
                    v["usr_details"] = allUsersShortDetailsMap[v["usr_id"]]
                    return v;
                });
            }

            for (var i = 0; i < allQuestions.length; i++) {
                var id = allQuestions[i]["qry_id"];
                var r = await qaAnsAPI.getOneLatestAnswerOfQuestion({
                    qry_id : id
                });
                allQuestions[i]["latest_answer"] = r;
            }




            self.getQuestionStatsCounter({
                    qry_id: data.qry_ids
                })
                .then(function(resrow) {
                    //convert to JSON
                    if (resrow.length > 0) {
                        //covert to json
                        var obj = JSON.parse(JSON.stringify(resrow));

                        //attach qry_stats to all questions
                        allQuestions.forEach(function(v, i) {
                            var stat = obj.filter(function(t) {
                                if (t.qry_id === v.qry_id) {
                                    return t
                                };
                            })[0];
                            allQuestions[i]['qry_stats'] = stat;
                        });
                    }
                    defer.resolve(allQuestions);
                })
                .catch(function(err) {
                    debug("err getQuestionStats....", err);
                    defer.reject(err);
                })
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Question Id
     * @param {string} data.usr_id - User Id
     * @returns {Promise}
     * @desc
     * - This method is used to update question stats for particular user
     * - If user follow/unfollow question then we will update follow tables also. See {@link QAFollow#updateFollower} and {@link QAFollow#deleteFollower}
     * - It will also increment dashboard stats for question views. See {@link DashboardStats#incrementForumQuestionViewsCounter}
     * - Tables Used : {@link CassandraTables#ew_qry_usr_stats}
     */
    async updateUserQuestionStats(data) {
        var defer = Q.defer();

        if(!data){
            return
        }

        var usr_id = data.usr_id;
        var qry_id = data.qry_id;
        var followQueryPromise;
        var crt_ts = new Date();

        var set_str_arr = [];
        var set_params = [];

        if (data.like_dlike_flg != undefined) {
            //if `like dislike flag` is set
            set_str_arr.push('like_dlike_flg = ?');
            set_params.push(data.like_dlike_flg);

        } else if (data.qry_ans != undefined) {
            //if `query ans` is set
            set_str_arr.push('qry_ans = ?');
            set_params.push(data.qry_ans);

        } else if (data.qry_flw != undefined) {
            //if `query follow` is set
            set_str_arr.push('qry_flw = ?');
            set_params.push(data.qry_flw);

            if (data.qry_flw === true) {
                //user is following this question
                followQueryPromise = FollowAPI.updateFollower({
                    qry_id: qry_id,
                    usr_id: usr_id
                });
            } else if (data.qry_flw === false) {
                //user is unfollowing this question
                followQueryPromise = FollowAPI.deleteFollower({
                    qry_id: qry_id,
                    usr_id: usr_id
                });
            }

        } else if (data.qry_shr != undefined) {
            //if `query share` is set
            set_str_arr.push('qry_shr = ?');
            set_params.push(data.qry_shr);

        } else if (data.qry_vws != undefined) {
            //if `query views` is set
            set_str_arr.push('qry_vws = ?');
            set_params.push(data.qry_vws);

            try {
                await dashboardStatsAPI.incrementForumQuestionViewsCounter({
                    usr_id: usr_id
                });
            } catch (err) {
                debug(err);
            }
        }

        //`create timestamp` update
        set_str_arr.push('crt_ts=?');
        set_params.push(crt_ts);


        //query
        var selectQry = 'update ' + qry_usr_stats + ' set ' + set_str_arr.join(',') + ' where usr_id = ? and qry_id = ?';
        var selectQry_args = set_params.concat([usr_id, qry_id]);

        //query cassandra db
        cassconn.execute(selectQry, selectQry_args, function(err, res, r) {
            if (err) {
                defer.reject(err);
                return;
            }
            if (followQueryPromise) {
                //if follow query is set then wait for it to resolve
                followQueryPromise.then(function() {
                        defer.resolve([]);
                    })
                    .catch(function(err) {
                        debug("updateUserQuestionStats...", err);
                        defer.reject(err);
                    })
            } else {
                // direct resolve if follow query is not set
                defer.resolve([]);
            }

        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {Array} data.qry_ids - Questions Id
     * @param {string} data.usr_id - User Id
     * @returns {Promise}
     * @desc
     * - Get User Question Stats for all given questions id
     * - Tables Used : {@link CassandraTables#ew_qry_usr_stats}
     */
    getUserQuestionStats(data) {

        var defer = Q.defer();
        var usr_id = data.usr_id;
        var qry_ids = data.qry_ids;

        function getStats(qry_id, cb) {
            //query cassandra db
            var selectQry = 'select * from ' + qry_usr_stats + ' where usr_id = ? and qry_id = ?';
            var params = [usr_id, qry_id];
            cassconn.execute(selectQry, params, function(err, res, r) {
                if (err) {
                    cb(err, null);
                    return;
                }
                var parsed = JSON.parse(JSON.stringify(res.rows));
                cb(null, parsed[0]);
            });
        }

        asy_lib.map(qry_ids, getStats, function(err, results) {

            if (err) {
                defer.reject(err);
                return;
            }
            var obj = {};
            var res = qry_ids.map(function(v, i) {
                obj[v] = results[i];
                return obj;
            });
            // obj = {
            //     qry_id_1 : <stats for user>,
            //     qry_id_2 : <stats for user>,
            //     ...
            // }
            defer.resolve(obj);
        })

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {Object} data.usr_stats_update - Object
     * @param {Object} data.qry_stats_update - Object
     * @returns {Promise}
     * @desc
     * - Update Question stats using {@link QA#updateQuestionStatsCounter} and user stats using {@link QA#updateUserQuestionStats}
     */
    updateQuestionStats(data) {
        //data = {usr_stats_update : {} , qry_stats_update : {}}
        var prms_arr = [this.updateQuestionStatsCounter(data.qry_stats_update), this.updateUserQuestionStats(data.usr_stats_update)];
        return Q.all(prms_arr);
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Question Id
     * @param {string} data.usr_id - User Id
     * @param {boolean} data.ans_flg - Answer Later Flag
     * @returns {Promise}
     * @desc
     * - Update status of Answer later flag for given question and user
     * - Tables Used : {@link CassandraTables#ew_qry_ans_ltr}
     */
    updateAnswerLater(data) {
        // data = {usr_id , qry_id , ans_flg , crt_ts}
        var defer = Q.defer();
        var usr_id = data.usr_id;
        var qry_id = data.qry_id;
        var ans_flg = data.ans_flg;
        var crt_ts = new Date();

        var qry = "update " + ew_qry_ans_ltr_tbl + ' set ans_flg = ? , crt_ts = ? where  usr_id = ? and  qry_id = ?';
        var params = [ans_flg, crt_ts, usr_id, qry_id];

        cassconn.execute(qry, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.resolve(err);
                return;
            }
            defer.resolve({});
        })
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id
     * @returns {Promise}
     * @desc
     * - First get all question ids which marked as answer later
     * - For all those question get question details and question stats using {@link QA#getQuestions}
     * - For all those question get user question stats using {@link QA#getUserQuestionStats}
     * - Merge all the results
     * - Tables Used : {@link CassandraTables#ew_qry_ans_ltr}
     */
    async getAnswerLater(data) {
        // data = {usr_id}
        try {
            var usr_id = data.usr_id;

            var qry = "select * from  " + ew_qry_ans_ltr_tbl + ' where  usr_id = ?';
            var params = [usr_id];

            var rows = await cassExecute(qry, params);

            rows = rows.filter((v, i) => {
                return v.ans_flg;
            })

            if(rows && rows.length <= 0){
                return [];
            }

            var qry_ids = rows.map((v, i) => v.qry_id);

            //get required questions
            var questions = await this.getQuestions({
                qry_ids: qry_ids
            });
            //create question mapping
            var questionMapping = {};
            questions.map((v, i) => {
                questionMapping[v.qry_id] = v;
            })


            //get required user question stats
            var userQuestionsStats = await this.getUserQuestionStats({
                usr_id: usr_id,
                qry_ids: qry_ids
            });

            var finalResult = [];
            for (var i = 0; i < rows.length; i++) {
                let ans_ltr_obj = rows[i];
                let question = questionMapping[ans_ltr_obj.qry_id];
                let userQuestionsStat = userQuestionsStats[ans_ltr_obj.qry_id];
                let obj = {...ans_ltr_obj,
                    ...question,
                    userQuestionsStat
                };
                finalResult.push(obj);
            }

            return finalResult;
        } catch (err) {
            debug(err);
            throw err;
        }
        // cassconn.execute(qry,params , function(err,res,r){
        //     if(err){
        //         debug(err);
        //         defer.resolve(err);
        //         return;
        //     }
        //     var row = (res.rows && res.rows.length > 0 && res.rows) || [];
        //     defer.resolve(row);
        // })
        // return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id
     * @returns {Promise}
     * @desc
     * - Get all notifications for given user
     * - Get all Unique answer user id in those notification and get user short details for those users {@link CareerBook#getUsersShortDetails}
     * - Get question ids from those notification and for all those question get question details {@link QA#getQuestions}
     * - Merge all the results and send
     * - Tables Used : {@link CassandraTables#ew_qry_flw_ntf}
     */
    getAllNotifications(data) {
        // data = {usr_id : ''}
        var defer = Q.defer();
        var self = this;

        var qry = "select * from " + ew_qry_flw_ntf_tbl + ' where usr_id = ? ';
        var qry_params = [data.usr_id];
        debug("time start ........", Date.now());
        cassconn.execute(qry, qry_params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var rows = (res.rows && (res.rows.length > 0) && res.rows) || [];
            var json_rows = JSON.parse(JSON.stringify(rows));

            // create array of users id
            var usr_ids = json_rows.map(function(v) {
                return '' + v.ans_usr_id
            });

            //create array of questions id
            var qry_ids = json_rows.map(function(v) {
                return v.qry_id
            });



            //get users short details like display name
            var user_promise = profile_api.getUsersShortDetails({
                usr_ids: usr_ids
            });
            //get question details
            var question_promise = self.getQuestions({
                qry_ids: qry_ids
            });

            // wait for both promise to resolve
            Q.all([user_promise, question_promise])
                .then(function(res) {
                    var usr_arr = res[0];
                    var qry_arr = res[1];
                    for (var i = 0; i < json_rows.length; i++) {
                        var que = json_rows[i];
                        //find question details
                        que["qry_details"] = qry_arr.filter(function(v) {
                            if (v.qry_id === que.qry_id) {
                                return true
                            }
                            return false;
                        })[0];
                        //find ans user details
                        que["ans_usr_details"] = usr_arr.filter(function(v) {
                            if (v.usr_id === que.ans_usr_id) {
                                return true
                            }
                            return false;
                        })[0];
                    }
                    // debug(que);
                    debug("time end ........", Date.now());
                    defer.resolve(json_rows);
                })
                .catch(function(err) {
                    debug(err);
                    defer.reject(err);
                    return;
                })

        })

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id
     * @param {uuid} data.cmpln_id - Complain Id
     * @param {string} data.cmpln_typ - Complain Type (Wrong Question,etc...)
     * @param {string} data.cmpln_rsn - Complain Reason
     * @returns {Promise}
     * @desc
     * - Insert detail into table
     * - Tables Used : {@link CassandraTables#ew_cmpln_rsn}
     */
    saveComplain(data) {
        // data = {usr_id,cmpln_id,cmpln_typ,cmpln_rsn}
        var defer = Q.defer();
        var self = this;
        var qry = "insert into " + ew_complain_tbl + ' (usr_id,cmpln_id,cmpln_typ,cmpln_rsn,cmpln_ts) values (?,?,?,?,?)';
        var qry_params = [data.usr_id, data.cmpln_id, data.cmpln_typ, data.cmpln_rsn, new Date()];
        cassconn.execute(qry, qry_params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        })
        return defer.promise;
    }

    /**
     * @returns {Promise}
     * @desc
     * - Get all complains
     * - Tables Used : {@link CassandraTables#ew_cmpln_rsn}
     */
    getAllComplain(data) {
        // data = {}
        var defer = Q.defer();
        var self = this;

        var qry = "select * from " + ew_complain_tbl;
        cassconn.execute(qry, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        })
        return defer.promise;
    }

    async incrementQAQuestionCounter(){
        try{
            var updateQry = "update " + ew_qa_qry_counter_tbl + " set qry=qry + 1 where id = 'total_q_count'";
            await cassExecute(updateQry);
            return;
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }

    async decrementQAQuestionCounter(){
        try{
            var updateQry = "update " + ew_qa_qry_counter_tbl + " set qry=qry - 1 where id = 'total_q_count'";
            await cassExecute(updateQry);
            return;
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getQAQuestionCounter(){
        try{
            var selectQry = "select * from " + ew_qa_qry_counter_tbl + " where id = 'total_q_count'";
            var res = await cassExecute(selectQry);
            if(res && res.length > 0){
                return res[0]["qry"]
            }else{
                return 0;
            }
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }


    async getDetailedFollowNotifications(data){
        try{
            let { usr_id , last_notif_id } = data;
            var res = await FollowAPI.getNotifications(data);
            let results = []
            if(res && res.length > 0){
                for(let i = 0 ; i < res.length ; i++){
                    let obj = res[i];
                    let ans_usr_info = await profile_api.getUsersShortDetails({
                      usr_ids : [obj.ans_usr_id]
                    })
                    if(ans_usr_info[0]){
                      obj["ans_usr_info"] = ans_usr_info[0];
                    }
                    var r = await this.getQuestion({
                        qry_id : obj.qry_id
                    })
                    if(r){
                        obj["details"] = r;
                    }
                }
                return res
            }else{
                return []
            }
        }catch(err){
            throw new Error(err)
        }
    }

    async getDetailedUserFollowedQuestion(data){
        try{
            let { usr_id , last_qry_id } = data;
            var res = await FollowAPI.getUserFollowedQuestion(data);
            if(res && res.length > 0){
                var qry_ids = res.map((v,i)=>v.qry_id);
                let results = await this.getQuestions({
                    qry_ids : qry_ids
                })
                return results;
            }else{
                return []
            }
        }catch(err){
            throw new Error(err)
        }
    }
}

module.exports = new QA();
