var redis = require("redis");
var configPath = require('../../configPath.js');
var getRedisClient = require(configPath.dbconn.redisconn);
var logger = require(configPath.lib.log_to_file);

function start() {
    var sub = getRedisClient("subscriber"),
        pub = getRedisClient("publisher");
    var msg_count = 0;

    sub.on("pmessage", function(channel, message) {
        console.log("sub channel " + channel + ": " + message);
    });

    sub.on("psubscribe", function(channel, message) {
        console.log("PSubscribed")
        pub.setAsync("testKey", "testVal").then(function(){
            console.log("published")
        })
    });

    sub.psubscribe("__key*__:*");

    
}

start();