var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:itemComments:itemQueries");
var cassExecute = require(configPath.lib.utility).cassExecute;
var forumsAPI = require(configPath.api.forums.forums);
var qaCategoryAPI = require(configPath.api.forums.category);

var ew_itm_qrys_tbl = 'ew1.ew_itm_qrys';

function Query() {
}



Query.prototype.save = async function(data) {
    try{
        var qry_title = data.qry_title;
        var usr_id = data.usr_id;
        var itm_id = data.itm_id;
        
        // find and save category
        var mdl_id = data.mdl_id;
        var crs_id = data.crs_id;
        var crs_mdl_id = data.crs_id + "-" + data.mdl_id;
        var cat_name = data.crs_nm + "-" + data.mdl_nm;
        var cat_desc = data.crs_nm + "-" + data.mdl_nm;

        var cat_res = await qaCategoryAPI.findAndSaveCategoryBasedOnCourseAndModuleId({
            mdl_id,
            crs_id,
            crs_mdl_id,
            cat_name,
            cat_desc
        });

        cat_res = cat_res || {}

        // save qa question with category
        var saveQuestionResult = await forumsAPI.saveQuestion({
            qry_title,
            usr_id,
            qry_tags : [cat_res["cat_id"]]
        });

        var query = "insert into " + ew_itm_qrys_tbl + " (itm_id,qry_id)  values (?,?)";
        var params = [itm_id,saveQuestionResult.qry_id];

        await cassExecute(query,params);
        return {
            qry_id : saveQuestionResult.qry_id,
            itm_id : itm_id,
            qry_title : qry_title
        }
    }catch(e){
        debug(e);
        throw e;
    }
}

Query.prototype.getAll = async function(data) {
    try{
        var itm_id = data.itm_id;

        var qry = "select * from " + ew_itm_qrys_tbl + " where itm_id = ?";
        var params = [itm_id];

        var res = await cassExecute(qry,params);
        
        var qry_ids = res.map((v,i)=>{return v.qry_id});
        debug(qry_ids);
        var allQuestions = await forumsAPI.getQuestions({qry_ids : qry_ids});
        debug(allQuestions);
        return allQuestions;
    }catch(e){
        debug(e);
        throw e;
    }
}

module.exports = new Query();