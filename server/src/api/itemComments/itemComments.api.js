var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:itemComments.api.js");
var profileAPI = require(configPath.api.dashboard.profile);
var cassExecute = require(configPath.lib.utility).cassExecute;
var postAPI = require(configPath.api.dashboard.post);
var dateFormat = require('dateformat');

var comment_lvl0_table = 'ew1.ew_usr_vdo_cmnt_lvl0';
var comment_lvl1_table = 'ew1.ew_usr_vdo_cmnt_lvl1';
var comment_like_table = 'ew1.vt_itm_pst_like';
var comment_users_table = 'ew1.vt_itm_pst_like_usrs';
var comment_like_stats_table = 'ew1.vt_itm_like_stats';
var ew_usr_vdo_cmnt_m_table = 'ew1.ew_usr_vdo_cmnt_m';

const COMMENT_FETCH_LIMIT = 5

function Comment() {
	this.comments = [];
}



Comment.prototype.save = async function(data) {
    try{
        var defer = Q.defer();
        var pst_id = data.pst_id;
        var cmnt_id = TimeUuid.now()
        var cmnt_ts = new Date();
        var cmnt_txt = data.cmnt_txt;
        var cmnt_cmnt_id = data.cmnt_cmnt_id || null;
        var cmnt_lvl = data.cmnt_lvl;
        var usr_id = data.usr_id;
        var pst_typ = data.pst_typ;


        var insertPost;
        var insertPost_args;
    
        if(cmnt_lvl === "0"){
            insertPost = 'INSERT INTO ' + comment_lvl0_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_lvl,usr_id)  '+ 'VALUES(?,?,?,?,?,?);';
            insertPost_args = [pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_lvl,usr_id];


            var isUserCommentedBeforeRes = await this.isUserCommentedBefore(data);
            if(!isUserCommentedBeforeRes){
                //Not Commented
                //add to the master table and create post also
                var date = new Date();
                var pst_msg = "<tr><td>" + dateFormat(date,"mmm dS, yyyy") + "</td><td>" + cmnt_txt + "</td></tr>";
                var pst_crt_by = usr_id;
                var pst_itm_id = pst_id;
                var obj = {
                    pst_msg : pst_msg,
                    pst_crt_by : pst_crt_by,
                    postRestriction : "public",
                    pst_typ : pst_typ,
                    pst_itm_id : pst_itm_id
                }
                var res = await postAPI.save(obj);
                data.cb_pst_id = res.pst_id;
                await this.saveToMasterTable(data);
            }else{
                //it is already there
                var cbPostRes = await this.getCBPostIdFromMasterTable(data);
                var cb_pst_id = cbPostRes[0]["cb_pst_id"];
                var post_res = await postAPI.getPostData({postId : cb_pst_id});
                var post = post_res[0];

                var date = new Date();

                post.pst_msg += "<tr><td>" + dateFormat(date,"mmm dS, yyyy") + "</td><td>" + cmnt_txt + "</td></tr>";
                await postAPI.edit(post);
                var post_res = await postAPI.getPostData({postId : cb_pst_id});
                debug(post_res);
            }
        }else if(cmnt_lvl === '1'){
            insertPost = 'INSERT INTO ' + comment_lvl1_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_cmnt_id,cmnt_lvl,usr_id)  '+ 'VALUES(?, ?,?,?,?,?,?);';
            insertPost_args = [pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_cmnt_id,cmnt_lvl,usr_id];
        }
        
        var res = await cassExecute(insertPost,insertPost_args);
        return {
            cmnt_id : cmnt_id,
            cmnt_ts : cmnt_ts
        };
    }catch(err){
        debug(err);
        throw err;
    }
}

Comment.prototype.get = async function(data) {
    try{
        let { pst_id , cmnt_lvl , cmnt_cmnt_id , last_cmnt_id } = data;
        if(!pst_id || !cmnt_lvl){
            return {
                status : 'error',
                message : 'pst_id and cmnt_lvl are required'
            }
        }
        let qry;
        let params;
        if(cmnt_lvl === '0'){
            // if 0 level comment
            if(last_cmnt_id){
                // if user want to fetch from last_cmnt_id
                qry = `select * from ${comment_lvl0_table} where pst_id = ? and cmnt_id < ? limit ${COMMENT_FETCH_LIMIT}`
                params = [pst_id,last_cmnt_id]
            }else{
                // if user want to fetch first few comments
                qry = `select * from ${comment_lvl0_table} where pst_id = ? limit ${COMMENT_FETCH_LIMIT}`
                params = [pst_id]
            }
        }else{
             // if 1 level comment
            if(last_cmnt_id){
                // if user want to fetch from last_cmnt_id
                qry = `select * from ${comment_lvl1_table} where pst_id = ? and cmnt_cmnt_id = ? and  cmnt_id < ? limit ${COMMENT_FETCH_LIMIT}`
                params = [pst_id , cmnt_cmnt_id , last_cmnt_id]
            }else{
                // if user want to fetch first few comments
                qry = `select * from ${comment_lvl1_table} where pst_id = ? and cmnt_cmnt_id = ? limit ${COMMENT_FETCH_LIMIT}`
                params = [pst_id,cmnt_cmnt_id]
            }
        }

        var jsonComments = await cassExecute(qry,params);

        jsonComments = jsonComments.filter(function(v, i) {
            if (v.usr_id) {
                return true
            }
            return false;
        });

        //get all userse
        var usr_ids = jsonComments.map((v, i)=>v.usr_id);
        var users = await profileAPI.getUsersShortDetails({ usr_ids: usr_ids })

        var userMapping = {};
        //generate user mapping so we can direct fetch from dict
        users.map(function(v, i) {
            delete v.usr_email;
            delete v.usr_role;
            delete v.usr_ph;
            userMapping[v.usr_id] = v;
        });

        var allCommentsdata = jsonComments.map(function(v, i) {
            //get post author
            var crt_by = v['usr_id'];
            //get user info
            var user = userMapping[crt_by];
            //attach user info to item
            v['usr_info'] = user;
            return v;
        })

        return allCommentsdata

    }catch(err){
        throw new Error(err);
    }
    // var defer = Q.defer();
    // var pst_id = data.pst_id.trim();
    // var select = 'select * from ' + comment_lvl0_table + ' where pst_id = ? ';
    // const select_args = [pst_id];
    // var queryOptions = { prepare: true, consistency:cassandra.types.consistencies.quorum };
    // cassconn.execute(select,select_args,function(err,res,r) {
    //     if(err){
    //         debug(err);
    //         defer.reject(err);
    //         return;
    //     }
    //     var rows = res.rows || [];
    //     //convert to json object
    //     var jsonComments = JSON.parse(JSON.stringify(rows));
    //     //get all userse
    //     var usr_ids = jsonComments.map(function(v,i){return v.usr_id});
    //     //get user short details
    //     profileAPI.getUsersShortDetails({usr_ids : usr_ids})
    //         .then(function(users) {

    //             var userMapping = {};
    //             //generate user mapping so we can direct fetch from dict
    //             users.map(function(v,i){
    //                 userMapping[v.usr_id] = v;
    //             });

    //             var allCommentsdata = jsonComments.map(function(v,i){
    //                 //get post author
    //                 var crt_by = v['usr_id'];
    //                 //get user info
    //                 var user = userMapping[crt_by];
    //                 //attach user info to item
    //                 v['usr_info'] = user;
    //                 return v;
    //             })

    //             defer.resolve(allCommentsdata);
    //         })
    //         .catch(function(err) {
    //             debug(err);
    //             defer.reject(err);
    //         })
    // });
    // return defer.promise;
}

Comment.prototype.edit = function(data) {
    var defer = Q.defer();
    var pst_id = data.pst_id;
    var cmnt_id = data.cmnt_id;
    
    var cmnt_ts = new Date(data.cmnt_ts);
    var cmnt_txt = data.cmnt_txt;

    var cmnt_lvl = data.cmnt_lvl;
    var cmnt_cmnt_id = data.cmnt_cmnt_id;

    let updatePost;
    let updatePost_args;

    if(cmnt_lvl === '0'){
        updatePost = 'update ' + comment_lvl0_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ?';
        updatePost_args = [cmnt_txt, cmnt_id, pst_id];
    }else{
        updatePost = 'update ' + comment_lvl1_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ? and cmnt_cmnt_id = ?';
        updatePost_args = [cmnt_txt, cmnt_id, pst_id, cmnt_cmnt_id];
    }

    cassconn.execute(updatePost, updatePost_args, function(err, res, r) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
}

Comment.prototype.delete = function(data) {
    var defer = Q.defer();
    var pst_id = data.pst_id;
    var cmnt_id = data.cmnt_id;
    var cmnt_ts = new Date(data.cmnt_ts);
    var usr_id = data.usr_id;

    var cmnt_lvl = data.cmnt_lvl;
    var cmnt_cmnt_id = data.cmnt_cmnt_id;

    let deletePost;
    let deletePost_args;

    if(!cmnt_lvl){
        return {
            status : 'error',
            message : 'cmnt_lvl is required'
        }
    }

    if(cmnt_lvl === '0'){
        deletePost = 'delete from ' + comment_lvl0_table + ' where pst_id = ? and cmnt_id = ?';
        deletePost_args = [pst_id, cmnt_id];
    }else if(cmnt_lvl === '1'){
        deletePost = 'delete from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_id = ? and cmnt_cmnt_id = ?';
        deletePost_args = [pst_id, cmnt_id, cmnt_cmnt_id];
    }

    cassconn.execute(deletePost, deletePost_args, async function(err, res, r) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });


    return defer.promise;
}

Comment.prototype.saveToMasterTable = async function(data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var pst_id = data.pst_id;
    var cb_pst_id = data.cb_pst_id;
    
    var qry = "insert into " + ew_usr_vdo_cmnt_m_table + " (usr_id,pst_id,cb_pst_id,crt_dt) values (?,?,?,?)";
    var params = [usr_id,pst_id,cb_pst_id,new Date()];
    try{
        var res = await cassExecute(qry,params);
        return data;
    }catch(err){
        debug(err);
        throw err;
    }
    return defer.promise;
}

Comment.prototype.deleteFromMasterTable = async function(data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var pst_id = data.pst_id;
    
    var qry = "delete from " + ew_usr_vdo_cmnt_m_table + " where usr_id=? and pst_id=?";
    var params = [usr_id,pst_id,cb_pst_id];
    try{
        var res = await cassExecute(qry,params);
        return data;
    }catch(err){
        debug(err);
        throw err;
    }
    return defer.promise;
}

Comment.prototype.getCBPostIdFromMasterTable = async function(data) {
    try{
        var usr_id = data.usr_id;
        var pst_id = data.pst_id;
        
        var qry = "select * from " + ew_usr_vdo_cmnt_m_table + " where usr_id =? and pst_id = ?";
        var params = [usr_id,pst_id];
    
        var res = await cassExecute(qry,params);
        return res;
    }catch(err){
        debug(err);
        throw err;
    }
}

Comment.prototype.isUserCommentedBefore = async function(data){
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var pst_id = data.pst_id;
    
    var qry = "select * from " + ew_usr_vdo_cmnt_m_table + " where usr_id =? and pst_id = ?";
    var params = [usr_id,pst_id];

    try{
        var res = await cassExecute(qry,params);
        if(res.length > 0){
            return true;
        }else{
            return false;
        }    
    }catch(err){
        debug(err);
        return false;
    }
    return defer.promise;
}

Comment.prototype.likeDislikeComment = function(data) {
    var defer = Q.defer();
    var pst_id = data.pst_id || null;
    var usr_id = data.usr_id || 'n/a';
    var cmnt_id = data.cmnt_id || null;
    var crt_ts  = new Date();
    var like_dlike_flg = data.like_dlike_flg;

    var insert = 'insert into ' + comment_like_table + ' (pst_id,usr_id,cmnt_id,crt_ts,like_dlike_flg) values(?,?,?,?,?)';
    const insert_args = [pst_id,usr_id,cmnt_id,crt_ts,like_dlike_flg];

    var queryOptions = { prepare: true, consistency:cassandra.types.consistencies.quorum };
    cassconn.execute(insert,insert_args,queryOptions,function(err,res,r) {
        
        if(err){
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
}

Comment.prototype.getLikeDislikeComments = function(data) {
    var defer = Q.defer();

    var pst_id = data.pst_id || null;
    var usr_id = data.usr_id || 'n/a';
    
    var select = 'select * from ' + comment_like_table + ' where pst_id = ? and  usr_id = ?';
    const select_args = [pst_id,usr_id];

    var queryOptions = { prepare: true, consistency:cassandra.types.consistencies.quorum };
    cassconn.execute(select,select_args,queryOptions,function(err,res,r) {
        if(err){
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve(res.rows || []);
    });
    return defer.promise;
}

Comment.prototype.getCommentUsers = async function(data) {
    try{
        var cmnt_id = data.cmnt_id;
        var query = "select * from " + comment_users_table + " where cmnt_id=?";
        var params = [cmnt_id];
        var res = await cassExecute(query,params);
        //get all user id
        var usersIdArr = res.map((v,i)=>v.usr_id);
        //get users short details
        var usersDetails = await profileAPI.getUsersShortDetails({usr_ids : usersIdArr})
        //remove email from all users and send it to client
        usersDetails = usersDetails.map((v,i)=>{
            delete v["usr_email"];
            return v;
        })
        return usersDetails;
    }catch(err){
        debug(err);
        throw err;
    }
}

// updateCommentLikeDislikeCount
Comment.prototype.updateCommentLikeDislikeCount = function(data) {
    var defer = Q.defer();

    //data = {pst_id : pst_id , cmnt_id : cmnt_id , like : -1/0/1 , dislike : -1/0/1}

    var pst_id = data.pst_id || null;
    var cmnt_id = data.cmnt_id || null;
    
    var like_cnt_str = ' like_cnt = like_cnt + ' + (data.like || 0);
    var dlike_cnt_str = ' dlike_cnt = dlike_cnt + ' + (data.dislike || 0);
    var where_clause = ' where pst_id = ? and cmnt_id = ?';

    var update_like = 'update ' + comment_like_stats_table + ' set ' + like_cnt_str + "," + dlike_cnt_str  + where_clause;
    const update_args = [pst_id,cmnt_id];

    var queryOptions = { prepare: true, consistency:cassandra.types.consistencies.quorum };
    cassconn.execute(update_like,update_args,queryOptions,function(err,res,r) {
        if(err){
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
}

Comment.prototype.getCommentLikeDislikeCount = function(data) {
    var defer = Q.defer();

    //data = {pst_id : pst_id}

    var pst_id = data.pst_id || null;
    
    var select_like = 'select * from ' + comment_like_stats_table + ' where pst_id = ?';
    const select_args = [pst_id];

    var queryOptions = { prepare: true, consistency:cassandra.types.consistencies.quorum };
    cassconn.execute(select_like,select_args,queryOptions,function(err,res,r) {
        if(err){
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve(res.rows || []);
    });
    return defer.promise;
}

module.exports = new Comment();