var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var config = require('../../config.js');
var Q = require('q');
var uuid = require('uuid');
var shortid = require('shortid');
var debug = require('debug')('app:api:job:job');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var ew_apply_job_tbl = "ew1.ew_apply_job";

function Job() {

}

Job.prototype.applyForJob = async function (data) {
    try{
        var job_pk = Uuid.random();
        var usr_nm = data.usr_nm || "";
        var usr_email = data.usr_email || "";
        var usr_ph = data.usr_ph || "";
        var usr_sal = data.usr_sal || "";
        var usr_edu = data.usr_edu || "";
        var usr_skill = data.usr_skill || "";
        var job_id = data.job_id || "";
        var usr_about_exp = data.usr_about_exp || "";
        var create_dt = new Date();
        
        var qry = "insert into " + ew_apply_job_tbl + " (job_pk,job_id,usr_nm,usr_email,usr_ph,usr_sal,usr_edu,usr_skill,usr_about_exp,create_dt) values (?,?,?,?,?,?,?,?,?,?)";
        var params = [job_pk,job_id,usr_nm,usr_email,usr_ph,usr_sal,usr_edu,usr_skill,usr_about_exp,create_dt];
        await cassExecute(qry,params);
        return data;
    }catch(e){
        debug(e);
        throw e;
    }
}

module.exports = new Job();