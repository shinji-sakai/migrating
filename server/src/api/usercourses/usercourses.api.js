var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);

var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:usercourses:usercourses");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;

var courseAPI = require(configPath.api.admin.course);
var batchCourseAPI = require(configPath.api.batchCourse.batchCourse);
var bundleAPI = require(configPath.api.bundle.bundle);
var ew_crs_access_tbl = "ew1.ew_crs_access";

function UserCourses(){

}
//Deprecated
UserCourses.prototype.getPurchasedCourses = function(data) {
	return new getConnection()
		.then(dbCollection)
		.then(getCourses)
		.then(getCoursesDetails)
		.catch(globalFunctions.err);

	function getCourses(col){
		return col.findOne({userId : data.userId} , {_id : 0});
	}

	function getCoursesDetails(obj = {}) {
		var courses = obj ? obj.courses : [];
		var courseIds = courses.map(function(v,i) {
			return v.courseId;
		})
		return courseAPI.getCoursesById({ids : courseIds});
	}
};
//Deprecated
UserCourses.prototype.updatePurchasedCourses = function(data) {
	return new getConnection()
		.then(dbCollection)
		.then(updateCourses)
		.catch(globalFunctions.err);

	function updateCourses(col){
		return col.update({userId : data.userId},{$addToSet: {"courses": { $each : data.courses }}},{upsert:true});
	}
};

//Deprecated
UserCourses.prototype.addPurchasedCourse = function(data) {
	return new getConnection()
		.then(dbCollection)
		.then(updateCourses)
		.catch(globalFunctions.err);

	function updateCourses(col){
		var obj = {
			courseId : data.course,
			bat_id : data.bat_id,
			purchaseDate : new Date()
		}
		return col.update({userId : data.userId},{$addToSet: {"courses": obj}},{upsert:true});
	}
};


UserCourses.prototype.savePurchasedCourse = async function(data) {
	var defer = Q.defer();

	var usr_id  = data.usr_id;
	var training_id = data.training_id;
	var comments = data.comments || "";
	var src_usr = data.src_usr || "";
	var now = new Date();
	var now_1_yr = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
	try{
		//get all included courses in batch training course
		var bundles = await batchCourseAPI.getBundleCourseMappingInBatchCourse({crs_id : training_id});
		var queries = [];
		var allCoursesRes = [];
		if(!bundles || bundles.length <= 0){
			return [];
		}
		//iterate through all bundles
		for (var i = 0; i < bundles.length; i++) {
			var b = bundles[i];
			if(!b || !b.includedCourses || (b && b.includedCourses.length <= 0)){
				continue
			}
			//iterate through all course in this bundle
			for (var j = 0; j < b.includedCourses.length; j++) {
				var crs = b.includedCourses[j];
				allCoursesRes.push(crs);
				var str = "insert into " + ew_crs_access_tbl + " (usr_id,training_id,bndl_id,crs_id,privilege,start_dt,end_dt,comments,src_usr) values (?,?,?,?,?,?,?,?,?)";
				var params = [usr_id,training_id,b.bndl_id,crs,"all",now,now_1_yr,comments,src_usr];
				queries.push({query : str,params:params});
			}
		}
		if(queries && queries.length > 0){
			//execte all query at once in batch
			var queryOptions = { prepare: true, consistency:cassandra.types.consistencies.quorum };
			cassconn.batch(queries,queryOptions,function(err,res){
				if(err){
					defer.resolve(err);
				}
				defer.resolve(allCoursesRes);
			});
		}
	}catch(e){
		console.log(e);
		defer.reject(e)
	}
	return defer.promise;
}

UserCourses.prototype.saveCoursesBasedOnCourseId = async function(data) {
	var defer = Q.defer();

	var usr_id  = data.usr_id;
	var crs_id = [].concat(data.crs_id);
	var comments = data.comments;
	var src_usr = data.src_usr
	try{
		let queries = [];
		for (var j = 0; j < crs_id.length; j++) {
			var crs = crs_id[j];
			var str = "insert into " + ew_crs_access_tbl + " (usr_id,crs_id,privilege,comments,src_usr) values (?,?,?,?,?)";
			var params = [usr_id,crs,"all",comments,src_usr];
			queries.push({query : str,params:params});
		}

		await cassBatch(queries);
		return {
			status : true,
			message : "Saved successfully.",
			data
		}
	}catch(e){
		console.log(e);
		defer.reject(e)
	}
	return defer.promise;
}

UserCourses.prototype.saveCoursesBasedOnBundleId = async function(data) {
	var defer = Q.defer();

	var usr_id  = data.usr_id;
	var bundles = [].concat(data.bundles);
	var comments = data.comments;
	var src_usr = data.src_usr
	try{
		let queries = [];
		for (var i = 0; i < bundles.length; i++) {
			let bndl = bundles[i]
			let courses = await bundleAPI.getCoursesInBundle({
				bndl_id : bndl
			});
			
			for (var j = 0; j < courses.length; j++) {
				var crs = courses[j];
				var str = "insert into " + ew_crs_access_tbl + " (usr_id,crs_id,privilege,comments,src_usr,bndl_id) values (?,?,?,?,?,?)";
				var params = [usr_id,crs,"all",comments,src_usr,bndl.bndl_id];
				queries.push({query : str,params:params});
			}
		}

		await cassBatch(queries);
		return {
			status : true,
			message : "Saved successfully.",
			data
		}
	}catch(e){
		console.log(e);
		defer.reject(e)
	}
	return defer.promise;
}

UserCourses.prototype.saveCoursesBasedOnTrainingId = async function(data) {
	var defer = Q.defer();
	var usr_id  = data.usr_id;
	var trainings = [].concat(data.trainings);
	var comments = data.comments;
	var src_usr = data.src_usr
	try{
		let queries = [];
		for (var i = 0; i < trainings.length; i++) {
			let training = trainings[i]

			await this.savePurchasedCourse({
				training_id : training,
				...data
			})
		}
		return {
			status : true,
			message : "Saved successfully.",
			data
		}
	}catch(e){
		console.log(e);
		defer.reject(e)
	}
	return defer.promise;
}


UserCourses.prototype.getPurchasedCourseOfUser = function(data) {
	var defer = Q.defer();

	var usr_id  = data.usr_id;
	try{
		var qry = "select * from " + ew_crs_access_tbl + " where usr_id = ?";
		var params = [usr_id];
		cassconn.execute(qry,params,function(err,res){
			if(err){
				defer.reject(err);
				return;
			}
			var jsonRows = JSON.parse(JSON.stringify(res.rows || []));
			defer.resolve(jsonRows);
		});
	}catch(e){
		console.log(e);
		defer.reject(e)
	}
	return defer.promise;
}

UserCourses.prototype.getPurchasedCourseDetails = async function(data) {
	try{
		var usr_id  = data.usr_id;
		var courses = await this.getPurchasedCourseOfUser(data);
		var coursesId = courses.map((v,i)=>{return v.crs_id});
		var fullCourseDetails = await courseAPI.getCoursesById({ids : coursesId});
		return fullCourseDetails;
	}catch(err){
		throw err;
	}
}

function dbCollection(db){
	return db.collection('usercourses');
}

module.exports = new UserCourses();