var configPath = require('../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var Q=require('q');

var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:contact");
var cassconn = require(configPath.dbconn.cassconn);
var Uuid = cassandra.types.Uuid;
var mailAPI = require(configPath.api.mail); 
var cassExecute = require(configPath.lib.utility).cassExecute;
var contactusTemplate = require(configPath.api.emailTemplateAPI.contactusTemplate);

var ew_contact_msg_table = 'ew1.ew_contact_msgs';


function Contact(){

}

Contact.prototype.saveContactMsg = async function(data){
	try{
		var fst_nm = data.fst_nm;
		var email = data.email;
		var msg = data.msg;
		var ph_no = data.ph_no;
		var msg_id = Uuid.random();

		var query = "insert into " + ew_contact_msg_table + " (fst_nm,email,msg,ph_no,msg_id,msg_ts) values (?,?,?,?,?,?)";
		var params = [fst_nm,email,msg,ph_no,msg_id,new Date()];

		var res = await cassExecute(query,params);

		var template = await contactusTemplate({
			name : fst_nm,
			email : email,
			message : msg,
			ph_no : ph_no
		});

		//Send mail to ew team
		var obj = {
			to : 'support@examwarrior.com',
			subject : 'Msg From ContactUs Form',
			text : template.html
		}
		await mailAPI.sendMail(obj)
		return data;
	}catch(err){
		throw err;
	}
}

Contact.prototype.getContactUsDetails = async function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}

function dbCollection(db){
	return db.collection("contactus");
}

module.exports = new Contact();