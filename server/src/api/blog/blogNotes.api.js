var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videonotes');
var authorizeitemsAPI = require(configPath.api.authorizeitems);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var ew_vdo_note_mdls_tbl = "ew1.ew_vdo_note_mdls";
var ew_vdo_notes_tbl = "ew1.ew_vdo_notes";
var ew_vdo_notes_usr_tbl = "ew1.ew_vdo_notes_usr";


function BlogNoteApi() {

}

BlogNoteApi.prototype.insertBlogNote = async function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var crs_id = data.crs_id;
    var crs_nm = data.crs_nm;
    var mdl_id = data.mdl_id;
    var mdl_nm = data.mdl_nm;
    var vdo_id = data.vdo_id;
    var vdo_note_id = Uuid.random();
    var vdo_note = data.vdo_note;
    var vdo_tm = data.vdo_tm;
    var crt_dt = new Date();


    var qry = "insert into " + ew_vdo_notes_tbl + " (usr_id,crs_id,mdl_id,vdo_id,vdo_note_id,vdo_note,vdo_tm,crt_dt) values (?,?,?,?,?,?,?,?)" ;
    var params = [usr_id,crs_id,mdl_id,vdo_id,vdo_note_id,vdo_note,vdo_tm,crt_dt];

    cassconn.execute(qry,params,async (err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        data.vdo_note_id = vdo_note_id;
        data.crt_dt = crt_dt;
        defer.resolve(data);
    })
    return defer.promise;
}

BlogNoteApi.prototype.updateBlogNote = function (data) {
    var defer = Q.defer();
    
    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var vdo_note_id = data.vdo_note_id;
    var vdo_note = data.vdo_note;

    var qry = "update " + ew_vdo_notes_tbl + " set vdo_note=? where usr_id=? and mdl_id=? and vdo_note_id =?";
    var params = [vdo_note,usr_id,mdl_id,vdo_note_id];
    cassconn.execute(qry,params,(err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    })
    return defer.promise;
}

BlogNoteApi.prototype.deleteBlogNote = async function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var vdo_note_id = data.vdo_note_id;

    var qry = "delete from " + ew_vdo_notes_tbl + " where usr_id=? and mdl_id=? and vdo_note_id =?";
    var params = [usr_id,mdl_id,vdo_note_id];
    cassconn.execute(qry,params,async (err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    })
    return defer.promise;
}

module.exports = new BlogNoteApi();