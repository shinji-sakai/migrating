var configPath = require('../../configPath.js');
var config = require('../../config.js');
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();
var logger = require(configPath.lib.log_to_file);
var Q = require('q');

const OFFLINE_STATUS = "offline"
const ONLINE_STATUS = "online"
const AWAY_STATUS = "away"
const ACTIVE_STATUS = "active"
const INACTIVE_STATUS = "inactive"

class chat_redis {
    async setChatUserInfo(data) {
        try {
            

            var defer = Q.defer();
            await redisCli.saddAsync( data.user_id+":socketIds", data.socketId);   
            await redisCli.setAsync( data.user_id+":display_name", data.display_name);    
            var getUserInfo = await redisCli.smembersAsync( data.user_id+":socketIds");    
            
            return {
                getUserInfo
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getUserSocketIds(usr_Ids) {
        try {

            var defer = Q.defer();          

            var multiObj = redisCli.multi();
            usr_Ids.forEach(function(usrSocketId){
                multiObj.smembers(usrSocketId+":socketIds");
            })

            var getUserSocketIds = await multiObj.execAsync();
            var responseObj = []

            for(var i=0;i<usr_Ids.length;i++) {
                responseObj.push({
                    user_id:usr_Ids[i],
                    socketIds:getUserSocketIds[i]
                })
            }
            return responseObj
            
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async removeChatUserInfo(data) {
        try {

            var defer = Q.defer();
            await redisCli.sremAsync( data.user_id+":socketIds", data.socketId);     
            var getUserInfo = await redisCli.smembersAsync( data.user_id+":socketIds");    
            if(getUserInfo.length == 0) {
               await redisCli.delAsync( data.user_id+":display_name");     
            }

            return {
               getUserInfo 
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async setUserManualStatus(data){
        try {
            var defer = Q.defer();
            await redisCli.setAsync( data.user_id + ":manual_chat_status",data.manualChatStatus);
            return data
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getUserManualStatus(data){
        try {
            var defer = Q.defer();
            var status = await redisCli.getAsync( data.user_id + ":manual_chat_status");
            if(status && status !== 'null'){
                if(status === OFFLINE_STATUS){
                    return status
                }else {
                    var user_sock = await this.getUserSocketIds([data.user_id]);
                    if(user_sock[0] && user_sock[0].socketIds.length > 0){
                        return status
                    }else{
                        return OFFLINE_STATUS
                    }
                }
            }else{
                var user_sock = await this.getUserSocketIds([data.user_id]);
                if(user_sock[0] && user_sock[0].socketIds.length > 0){
                    return status
                }else{
                    return OFFLINE_STATUS
                }
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getUserChatActivity(data){
        try{
            var redis_chat_activity = await redisCli.getAsync( data.user_id + ":chat_activity");
            if(redis_chat_activity && redis_chat_activity !== 'null'){
                let parsedObj = JSON.parse(redis_chat_activity);
                return parsedObj
            }else{
                return {
                    activity : OFFLINE_STATUS,
                    time : new Date()
                }
            }
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }

    async setUserChatActivity(data){
        try {
            if(data.chatActivity && data.chatActivity === ONLINE_STATUS){
                // if chat activity is online then directly set that
                var obj = {
                    activity : data.chatActivity,
                    time : new Date()
                }
                await redisCli.setAsync( data.user_id + ":chat_activity",JSON.stringify(obj));    
            }else if(data.chatActivity && data.chatActivity === AWAY_STATUS){
                // if chat activity is away 
                // then first check whether last activity time is more than 5 min
                // if it is more than 5 min then change activity to inactive
                // else keep as it is
                var redis_chat_activity = await redisCli.getAsync( data.user_id + ":chat_activity");
                if(redis_chat_activity && redis_chat_activity !== 'null'){
                    var parsedObj = JSON.parse(redis_chat_activity);
                    var lastTime = new Date(parsedObj.time);
                    var currentTime = new Date();
                    if((currentTime - lastTime) > (1000*5*60)){
                        let obj = {
                            activity : AWAY_STATUS,
                            time : currentTime
                        }
                        await redisCli.setAsync( data.user_id + ":chat_activity",JSON.stringify(obj));    
                    }
                }
            }
            return data
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getUserChatStatusToShow(data){
        try{
            let manual_status = await this.getUserManualStatus(data);
            if(manual_status === OFFLINE_STATUS){
                return manual_status
            }else{
                let chat_activity_status = await this.getUserChatActivity(data);
                return chat_activity_status.activity
            }
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getMultipleUsersChatStatusToShow(data){
        try{
            if(!data.users){
                return [];
            }
            let res = [];
            for (var i = 0; i < data.users.length; i++) {
                let usr_id = data.users[i];
                let status = await this.getUserChatStatusToShow({user_id : usr_id});
                res.push({
                    user_id : usr_id,
                    status : status
                })
            }
            return res;
        }catch(err){
            logger.debug(err);
            throw new Error(err);
        }
    }    
}


module.exports = new chat_redis();