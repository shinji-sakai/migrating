var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var jwt = require('jsonwebtoken');
var request = require('request');
var qs = require('querystring');
var Q = require('q');
var debug = require('debug')('app:api:chat:friends');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);
var cassExecute = require(configPath.lib.utility).cassExecute;
var path = require("path");
var fs = require("fs");

var frnd_table = 'ew1.vt_frs_chat_ts';
var user_short_table = 'ew1.vt_usr_short';
var grp_md = 'ew1.vt_grp_chat_md';


class ChatFriends {
    async findFriends(data) {
        var self = this;
        if (!data.user_id) {
            return {
                status: "error",
                message: "user_id field is required"
            }
        }
        return findFriends()
            .catch(globalFunctions.err);

        async function findFriends() {
            try {
                var selectUser = 'select * from ' + frnd_table + ' where user_id = ?;';
                var rows = await cassExecute(selectUser, [data.user_id]);
                var userIds = rows.map((v, i) => v.fr_id);
                var usersRes = await self.fetchUserShortInfo(userIds);
                usersRes = await findGrpMembers(usersRes || []);
                return usersRes;
            } catch (err) {
                debug(err);
                throw err;
            }
        }



        async function findGrpMembers(userData) {
            var defer = Q.defer();
            try {
                userData = userData || [];
                var ids = userData.map((v, i) => v.user_id);
                if(!ids){
                    return []
                }
                if(ids && ids.length <= 0){
                    return []
                }

                //create string like ('user1','user2',.....)
                var ids_str = '(';
                ids.forEach((v, i) => {
                    ids_str += '\'' + v + '\',';
                })
                ids_str = ids_str.substring(0, ids_str.length - 1);
                ids_str += ')';

                var selectUser = 'select * from ' + grp_md + ' where grp_id in ' + ids_str;
                var result = await cassExecute(selectUser, undefined);
                userData.forEach(function(usrinfo) {
                    result.forEach(function(grpinfo) {
                        if (grpinfo.grp_id == usrinfo.user_id) {
                            usrinfo["grp_mem"] = grpinfo.grp_mem;
                            if(usrinfo.pic50){
                                let parsed = path.parse(usrinfo.pic50);
                                if(parsed.base){
                                    let rel_path = configPath.common_paths.grp_pic_relative + "/" + parsed.base;
                                    let norm_rel_path = path.normalize(rel_path);
                                    let isExist = fs.existsSync(norm_rel_path)
                                    if(!isExist){
                                        usrinfo.pic50 = "/assets/images/dummy-user-pic.png"
                                    }    
                                }
                            }
                            
                        }
                    });
                });
                return userData;
            } catch (err) {
                debug(err);
                throw err;
            }
            return defer.promise;
        }
    }

    async fetchUserShortInfo(userIds) {
        var promiseArr = [];
        var resArr = [];
        try {
            if(!userIds){
                return {
                    message : "userIds required"
                }
            }
            if(userIds && userIds.length <= 0){
                return []
            }
            //create string like ('user1','user2',.....)
            var ids_str = "('" + userIds.join("','") + "')";

            var selectUser = 'select * from ' + user_short_table + ' where user_id in ' + ids_str;
            var result = await cassExecute(selectUser, undefined);

            return result;
        } catch (err) {
            debug(err);
            throw err;
        }
    }



    /**
     * @param {JSON} data
     * @param {string} data.user_id - User Id
     * @param {string} data.fr_id - Friend Id
     * @returns {Promise}
     * @desc
     * - This method is used to add fr_id to friend list of usr_id
     * - Tables Used : {@link CassandraTables#vt_frs_chat_ts}
     */
    addFriend(data) {
        return addFriend()
            .catch(globalFunctions.err);

        function addFriend() {
            var defer = Q.defer();
            var insertUser = 'insert into ' + frnd_table + ' (user_id,fr_id,last_talked) values (?,?,?);';
            var arr = [data.user_id, data.fr_id, new Date()];
            cassconn.execute(insertUser, arr, function(err, res, r) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve([]);
            });
            return defer.promise;
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.user_id - User Id
     * @param {string} data.fr_id - Friend Id
     * @returns {Promise}
     * @desc
     * - This method is used to remove fr_id from friend list of usr_id
     * - Tables Used : {@link CassandraTables#vt_frs_chat_ts}
     */
    async removeFriend(data) {
        try {
            var deleteFrnd = 'delete from ' + frnd_table + ' where user_id=? and fr_id=?;';
            var params = [data.user_id, data.fr_id];
            var res = await cassExecute(deleteFrnd, params);
            return data;
        } catch (err) {
            debug(err);
            throw err;
        }
    }

    findFriendWithId(data) {
        var defer = Q.defer();
        var selectUser = 'select * from ' + user_short_table + ' where user_id = ?;';
        var arr = [data.user_id];
        cassconn.execute(selectUser, arr, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            if (!res) {
                defer.resolve([]);
            } else {
                defer.resolve(res.rows[0]);
            }
        });
        return defer.promise;
    }

    async addUserShortDetails(data) {
        try {
            debug(data);
            var user_id = data.user_id;
            var user_email = data.user_email;
            var display_name = data.display_name;
            var typ = data.typ;
            var pic50 = data.pic;

            //query
            var selectUser = 'insert into ' + user_short_table + ' (user_id,display_name,pic50,typ,user_email) values (?,?,?,?,?)';
            var arr = [user_id, display_name, pic50, typ, user_email];
            debug(arr);
            //execute query
            var result = await cassExecute(selectUser, arr);

            return data;
        } catch (err) {
            debug(err);
            throw err;
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.user_id - User Id
     * @param {string} data.usr_pic - Display Pic Url
     * @returns {Promise}
     * @desc
     * - This method is used to update display pic url of given user id in chat user short details table
     * - Tables Used : {@link CassandraTables#vt_usr_short}
     */
    async updateUserProfilePic(data) {
        try {
            var user_id = data.usr_id;
            var pic50 = data.usr_pic;

            //query
            var selectUser = 'update ' + user_short_table + ' set pic50=? where user_id=?';
            var arr = [pic50, user_id];
            //execute query
            var result = await cassExecute(selectUser, arr);
            return data;
        } catch (err) {
            debug(err);
            throw err;
        }
    }
}

module.exports = new ChatFriends();