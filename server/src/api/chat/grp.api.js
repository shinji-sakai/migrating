var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var jwt = require('jsonwebtoken');
var request = require('request');
var qs = require('querystring');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var userErrorsConfig = require(configPath.config.userErrors);

var uuid = require('uuid');
// var time = require('time')(Date);
var cassandra = require('cassandra-driver');
var debug = require('debug')('app:api:chat:grp');
var cassExecute = require(configPath.lib.utility).cassExecute;
var chatFrndsAPI = require(configPath.api.chat.friends);

var chat_ts_table = 'ew1.vt_frs_chat_ts';
var chat_m_table = 'ew1.vt_frs_chat_m';
var user_short_table = 'ew1.vt_usr_short';
var grp_md = 'ew1.vt_grp_chat_md';

function ChatGroups() {

}

ChatGroups.prototype.createGroup = function(data) {
    var defer = Q.defer();
    var d = new Date();
    // d.setTimezone('UTC');

    var grp_id = data.grp_id;
    // console.log(grp_id);
    var created_dt = d;
    var grp_nm = data.grp_nm;
    var grp_own = data.grp_own;
    var pic50 = data.pic50;
    var typ = 'grp';
    var grp_mem = {};
    var members = data.members;

    var frs_chat_m_queries = [];
    var frs_chat_ts_queries = [];
    members.forEach(function(v) {
        grp_mem[v] = d;

        // no of people new entry in vt_frs_chat_m
        var insert_chat_m = "insert into " + chat_m_table + '(user_id , fr_id , typ , comb_id) values (?,?,?,?);';
        var insert_chat_m_params = [v, grp_id, 'grp', grp_id];

        frs_chat_m_queries.push({
            query: insert_chat_m,
            params: insert_chat_m_params
        });

        // no of people new entry in vt_frs_chat_ts
        var insert_chat_ts = "insert into " + chat_ts_table + '(user_id , fr_id , typ , last_talked) values (?,?,?,?);';
        var insert_chat_ts_params = [v, grp_id, 'grp', d];

        frs_chat_ts_queries.push({
            query: insert_chat_ts,
            params: insert_chat_ts_params
        });
    });

    // 1 new entry in vt_grp_chat_md
    var insertGrp = "insert into " + grp_md + '(grp_id , grp_nm , grp_own , grp_mem , created_dt) values (?,?,?,?,?);';
    var insertGrp_params = [grp_id, grp_nm, grp_own, grp_mem, created_dt];


    // 1 new entry in vt_usr_short_m
    var insertShortInfo = "insert into " + user_short_table + '(user_id , display_name , typ , user_email,pic50) values (?,?,?,?,?);';
    var insertShortInfo_params = [grp_id, grp_nm, 'grp', grp_id, pic50];


    var queries = [];
    queries.push({
        query: insertGrp,
        params: insertGrp_params
    });
    queries.push({
        query: insertShortInfo,
        params: insertShortInfo_params
    });
    queries = queries.concat(frs_chat_m_queries, frs_chat_ts_queries);
    // console.log(queries);
    var queryOptions = {
        prepare: true,
        consistency: cassandra.types.consistencies.quorum
    };
    cassconn.batch(queries, queryOptions, function(err, res, r) {
        // console.log(err,res,r);
        if (err) {
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    })
    return defer.promise;
}

ChatGroups.prototype.findMembers = async function(data) {
    if (!data.grp_id) {
        return {
            status: "error",
            message: "grp_id field required"
        }
    }
    try {
        var select = "select * from " + grp_md + ' where grp_id = ?;';
        var select_params = [data.grp_id];
        var rows = await cassExecute(select, select_params);
        if (rows && rows.length > 0) {
            let grp_members = rows[0].grp_mem;
            let members_id = Object.keys(grp_members);
            let members_details = await chatFrndsAPI.fetchUserShortInfo(members_id);
            return members_details;
        } else {
            return [];
        }
    } catch (err) {
        console.error(err);
        throw new Error(err)
    }
    // var defer = Q.defer();
    // cassconn.execute(select,select_params,function(err,res,r){
    // 	if(err){
    // 		defer.reject(err);
    // 		return;
    // 	}
    // 	if(!res.rows){
    // 		defer.resolve([]);	
    // 	}else{
    // 		defer.resolve(res.rows[0].get('grp_mem'));
    // 	}

    // });
    // return defer.promise;
}

ChatGroups.prototype.addMembers = async function(data) {
    var defer = Q.defer();

    var d = new Date();
    // d.setTimezone('UTC');
    var frs_chat_m_queries = [];
    var frs_chat_ts_queries = [];
    var members = data.members;
    var grp_id = data.grp_id;
    var grp_mem = {};

    if(!data.grp_id){
    	return {
    		status : "error",
    		message : "grp_id field is required"
    	}
    }
    if(!data.members){
    	return {
    		status : "error",
    		message : "members field is required"
    	}
    }

    if(data.members.length <= 0){
    	return {
    		status : "error",
    		message : "Atleast one member is required"
    	}
    }

    members.forEach(function(v) {
        grp_mem[v] = d;

        // no of people new entry in vt_frs_chat_m
        var insert_chat_m = "insert into " + chat_m_table + '(user_id , fr_id , typ , comb_id) values (?,?,?,?);';
        var insert_chat_m_params = [v, grp_id, 'grp', grp_id];

        frs_chat_m_queries.push({
            query: insert_chat_m,
            params: insert_chat_m_params
        });

        // no of people new entry in vt_frs_chat_ts
        var insert_chat_ts = "insert into " + chat_ts_table + '(user_id , fr_id , typ , last_talked) values (?,?,?,?);';
        var insert_chat_ts_params = [v, grp_id, 'grp', d];

        frs_chat_ts_queries.push({
            query: insert_chat_ts,
            params: insert_chat_ts_params
        });
    });

    var select = "select * from " + grp_md + ' where grp_id = ?;';
    var select_params = [data.grp_id];
    cassconn.execute(select, select_params, function(err, res, r) {
        if (err) {
            defer.reject(err);
            return;
        }

        var newMem = {};
        if (res.rows.length > 0) {
            var mem = res.rows[0].get('grp_mem');

            for (let k in mem) {
                newMem[k] = mem[k];
            }
            for (let k in grp_mem) {
                newMem[k] = grp_mem[k];
            }
        }

        var qu = 'UPDATE ' + grp_md + ' SET grp_mem = ? WHERE grp_id = ?;';
        var qu_params = [newMem, grp_id];

        var queries = [];
        queries = queries.concat(frs_chat_m_queries, frs_chat_ts_queries);
        queries.push({
            query: qu,
            params: qu_params
        });
        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.batch(queries, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
    });

    return defer.promise;
}

// var d = new ChatGroups();
// var obj = {
// 	grp_nm : 'Test Group',
// 	grp_own : 'sarju@filmsmiles.com',
// 	members : ['sarju@filmsmiles.com','jeevan@filmsmiles.com','dvbydt@filmsmiles.com']
// }
// d.createGroup(obj);

module.exports = new ChatGroups();