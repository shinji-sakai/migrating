var configPath = require('../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:bookmarks");

var dashboardStatsApi = require(configPath.api.dashboard.stats)

var bookmarks_tbl = 'ew1.vt_usr_crs_bmrk';
var bookmarks_mv = 'ew1.vt_usr_crs_bmrk_mv';
var authorizeItemAPI = require(configPath.api.authorizeitems);

class BookmarkApi {
    addBookmark_mongo(data) {
        var col;
        return getConnection()
            .then(findBookmark)
            .then(insertData)
            .then(this.getBookmarks)
            .catch(function(err) {
                console.log(err);
            });


        function findBookmark(db) {
            col = db.collection('bookmarks');
            return col.find({
                userId: data.userId,
                videoId: data.videoId
            }, {
                _id: 1
            }).toArray();
        }

        function insertData(d) {
            if (d.length == 0) {
                col.insertOne(data);
            }
            console.log(d, data);
            return data;
        }

    }
    getBookmarks_mongo(data) {
        return getConnection()
            .then(dbCollection)
            .then(globalFunctions.docData)
            .catch(globalFunctions.err);


        function dbCollection(db) {
            console.log("req data....", data);
            var col = db.collection('bookmarks');
            var bookmarks;
            if (data.courseId && !data.fullbookmark) {
                bookmarks = col.find({
                    courseId: data.courseId,
                    userId: data.userId
                }, {
                    _id: 0
                }).sort({
                    _id: -1
                }).limit(globalConfig.BOOKMARKS_LIMIT).toArray();
            } else if (data.courseId && data.fullbookmark) {
                bookmarks = col.find({
                    courseId: data.courseId,
                    userId: data.userId
                }, {
                    _id: 0
                }).toArray();
            } else {

                bookmarks = col.find({
                    userId: data.userId
                }, {
                    _id: 0
                }).toArray();
            }

            return bookmarks;
        }
    }
    deleteBookmarks_mongo(data) {
        var col;
        return getConnection()
            .then(removeData)
            .catch(globalFunctions.err);

        function removeData(db) {
            col = db.collection('bookmarks');
            return col.deleteOne(data);
        }

    }
    async addBookmark(data) {
        var defer = Q.defer();

        var query = "update " + bookmarks_tbl + ' SET bmrk_flg=?,itm_typ=?,bmrk_ts=? where usr_id = ? and crs_id = ? and itm_id = ?';
        var query_params = [data.bmrk_flg, data.itm_typ, new Date(), data.usr_id, data.crs_id, data.itm_id];

        //change bookmark counter in dashboard stats table
        var total_no_of_affected_bookmarks = (data.bmrk_flg) ? 1 : -1;
        await dashboardStatsApi.updateBookmarkCounter({
            usr_id: data.usr_id,
            no_of_bkmrks: total_no_of_affected_bookmarks
        });

        cassconn.execute(query, query_params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }
    async bookmarkMultipleItems(data) {
        var defer = Q.defer();

        var crs_id = data.crs_id;
        var items = data.items; // {itm_id,itm_typ,bmrk_flg}
        var usr_id = data.usr_id;
        var time = new Date();

        var queries = [];

        var no_of_added_bookmarks = 0;
        var no_of_removed_bookmarks = 0;

        items.forEach(function(v) {
            var query = "update " + bookmarks_tbl + ' SET bmrk_flg=?,itm_typ=?,bmrk_ts=? where usr_id = ? and crs_id = ? and itm_id = ?';
            var query_params = [v.bmrk_flg, v.itm_typ, time, usr_id, crs_id, v.itm_id];
            queries.push({
                query: query,
                params: query_params
            });
            v.bmrk_flg ? no_of_added_bookmarks++ : no_of_removed_bookmarks++;
        })

        //change bookmark counter in dashboard stats table
        var total_no_of_affected_bookmarks = no_of_added_bookmarks - no_of_removed_bookmarks;
        await dashboardStatsApi.updateBookmarkCounter({
            usr_id: usr_id,
            no_of_bkmrks: total_no_of_affected_bookmarks
        });


        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.batch(queries, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }
    getBookmarks(data) {
        var defer = Q.defer();

        var query = "select * from " + bookmarks_tbl + ' where usr_id = ? and crs_id = ?';
        var query_params = [data.usr_id, data.crs_id];

        cassconn.execute(query, query_params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });
        return defer.promise;
    }
    async getUserBookmarks(data) {
        try {
            var query = "select * from " + bookmarks_mv + ' where usr_id = ?';
            var query_params = [data.usr_id];
            var rows = await cassExecute(query, query_params);
            if (rows && rows.length > 0) {
                var result = [];
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.bmrk_flg) {
                        var itemInfo = await authorizeItemAPI.getItemsUsingItemId({
                            itemId: row.itm_id
                        });
                        itemInfo = itemInfo && itemInfo[0]
                        var obj = {
                            ...row,
                            ...itemInfo
                        }
                        result.push(obj);
                    }
                }
                return result;
            } else {
                return [];
            }
        } catch (err) {
            debug(err);
            throw err;
        }
    }
}

module.exports = new BookmarkApi();