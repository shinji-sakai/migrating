require('./config.js');
var configPath = require("../../configPath.js");
var paypal = require('paypal-rest-sdk');
var debug = require('debug')("app:api:payment:paypal");
var Q = require("q");
var smsAPI = require(configPath.api.sms.sms);
var logger = require(configPath.lib.log_to_file);
var profileAPI = require(configPath.api.dashboard.profile);

function Paypal() {

}

function getCreatePaymentRequestFormat(amount,currency,item_name,return_url) {
    var obj = {
        "intent": "sale",
        // "experience_profile_id": "XP-4Z2Q-NQVW-2ETU-22HT",
        "payer": {
            "payment_method": "paypal",
        },
        "transactions": [{
            "amount": {
                "total": amount,
                "currency": currency
            },
            "payment_options": {
                "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
            },
            "item_list": {
                "items": [{
                    "name": item_name,
                    "quantity": "1",
                    "price": amount,
                    "currency": currency
                }]
            }
        }],
        "note_to_payer": "Contact us for any questions on your order.",
        "redirect_urls": {
            "return_url": return_url,
            "cancel_url": return_url
        }
    }
    return obj;
}

/**
 * @param {JSON} data
 * @param {string} data
 * @returns {Promise}
 * @description 
 *     <ul>
 *         <li>Create payment and submit it to paypal api</li>
 *     </ul>
 */
Paypal.prototype.createPayment = async function(data) {
    var defer = Q.defer();

    var paypal_data = getCreatePaymentRequestFormat(data.amount,data.currency,data.item_name,data.return_url);
    var usr_id = data.usr_id;
    var training_id = data.training_id;

    try {
        //get user details
        var user = await profileAPI.getUserShortDetails({
            usr_id: usr_id
        });
        if (user.usr_ph) {
            //if user has given mobile number then
            var smsres = await smsAPI.paymentStartedSMS(user.usr_ph);
        }
    } catch (err) {
        logger.debug(err);
    }

    paypal.payment.create(paypal_data, function(error, payment) {
        if (error) {
            console.log(error);
            defer.reject(error);
        } else {
            console.log("Create Payment Response");
            debug(payment);
            defer.resolve(payment);
        }
    });
    return defer.promise;
}

/**
 * @param {JSON} data
 * @param {string} data
 * @returns {Promise}
 * @description 
 *     <ul>
 *         <li>Execute given payment once payer approve the order</li>
 *     </ul>
 */
Paypal.prototype.executePayment = function(paymentId, data) {
    var defer = Q.defer();

    paypal.payment.execute(paymentId, {
        payer_id: data.payer_id
    }, function(error, payment) {
        if (error) {
            debug(error);
            defer.reject(error);
        } else {
            console.log("Execute Payment Response");
            debug(JSON.stringify(payment));
            defer.resolve(payment);
        }
    });
    return defer.promise;
}

/**
 * @param {JSON} data
 * @param {string} data
 * @returns {Promise}
 * @description 
 *     <ul>
 *         <li>Check payment status for given id</li>
 *     </ul>
 */
Paypal.prototype.checkPaymentStatus = function(paymentId) {
    var defer = Q.defer();
    paypal.payment.get(paymentId, function(error, payment) {
        if (error) {
            debug(error);
            defer.reject(error);
        } else {
            // console.log("Create Payment Response");
            // debug(payment);
            defer.resolve(payment);
        }
    });
    return defer.promise;
}



module.exports = new Paypal();