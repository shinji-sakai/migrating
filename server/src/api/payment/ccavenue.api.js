var http = require('http'),
    fs = require('fs'),
    qs = require('querystring');
var configPath = require("../../configPath.js");
var ccav = require(configPath.lib.ccavutil);
var do_req = require('request');

var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var cassExecute = require(configPath.lib.utility).cassExecute;
var logger = require(configPath.lib.log_to_file);

var path = require('path');
var Q = require('q');
var shortid = require('shortid');
var ccavConfig = require(configPath.config.ccavConfig);

var debug = require('debug')("app:api:payment:CCAvenue");

var ew_ccavenue_pay_resp_table = "ew1.ew_ccavenue_pay_resp";

function CCAvenue() {
    this.testMode = false;
    this.testAPIUrl = "https://180.179.175.17/apis/servlet/DoWebTrans";
    this.liveAPIUrl = " https://login.ccavenue.com/apis/servlet/DoWebTrans";
}

CCAvenue.prototype.doAPICall = function(command, data) {
    var defer = Q.defer();
    var body = '',
        workingKey = ccavConfig.API_WORKING_KEY,
        accessCode = ccavConfig.API_ACCESS_CODE,
        encRequest = '',
        formbody = '';

    var d = data || {};
    d["merchant_id"] = ccavConfig.MERCHANT_ID;

    encRequest = ccav.encrypt(JSON.stringify(d), workingKey);

    var obj = {
            enc_request: encRequest,
            access_code: accessCode,
            command: command,
            request_type: 'JSON',
            response_type: 'JSON',
            version: '1.1'
        }
        // Test Server api : https://180.179.175.17/apis/servlet/DoWebTrans
        // Live Server api :  https://login.ccavenue.com/apis/servlet/DoWebTrans

    do_req.post({
        url: (this.testMode) ? this.testAPIUrl : this.liveAPIUrl,
        form: obj
    }, function(err, message, r) {
        if (message) {
            var api_res = qs.parse(r);
            if (api_res["status"] === "0") {
                var enc_response_str = api_res["enc_response"];
                var dec_response_str = ccav.decrypt(enc_response_str, workingKey);
                var dec_response_json = JSON.parse(dec_response_str);
                defer.resolve(dec_response_json);
            } else {

                defer.reject({
                    err: ccav.decrypt(api_res["enc_response"], workingKey),
                    errorCode: api_res["enc_error_code"]
                })
            }
        } else {
            defer.reject({
                err: err
            });
        }
    });
    return defer.promise;
}


function test_dec(enc_response_str) {
    var body = '',
        workingKey = ccavConfig.API_WORKING_KEY,
        accessCode = ccavConfig.API_ACCESS_CODE,
        encRequest = '',
        formbody = '';

    var dec_response_str = ccav.decrypt(enc_response_str, workingKey);
    var dec_response_json = JSON.parse(dec_response_str);

    console.log(dec_response_json);
    return dec_response_json;
}

CCAvenue.prototype.savePaymentDetails = async function(data) {
    try{
        var objectToStore = {
            cc_resp_id : Uuid.random(),
            order_id : data.order_id,
            tracking_id : data.tracking_id,
            bank_ref_no : data.bank_ref_no,
            order_status : data.order_status,
            failure_message : data.failure_message,
            payment_mode : data.payment_mode,
            card_name : data.card_name,
            status_code : data.status_code,
            status_message : data.status_message,
            currency : data.currency,
            amount : data.amount,
            billing_name : data.billing_name,
            billing_address : data.billing_address,
            billing_city : data.billing_city,
            billing_state : data.billing_state,
            billing_zip : data.billing_zip,
            billing_country : data.billing_country,
            billing_tel : data.billing_tel,
            billing_email : data.billing_email,
            delivery_name : data.delivery_name,
            delivery_address : data.delivery_address,
            delivery_city : data.delivery_city,
            delivery_state : data.delivery_state,
            delivery_zip : data.delivery_zip,
            delivery_country : data.delivery_country,
            delivery_tel : data.delivery_tel,
            merchant_param1 : data.merchant_param1,
            merchant_param2 : data.merchant_param2,
            merchant_param3 : data.merchant_param3,
            merchant_param4 : data.merchant_param4,
            merchant_param5 : data.merchant_param5,
            vault : data.vault,
            offer_type : data.offer_type,
            offer_code : data.offer_code,
            discount_value : data.discount_value,
            mer_amount : data.mer_amount,
            eci_value : data.eci_value,
            retry : data.retry,
            response_code : data.response_code,
            billing_notes : data.billing_notes,
            trans_date : data.trans_date,
            bin_country : data.bin_country
        }

        //create column string
        var columns = Object.keys(objectToStore);
        var columns_str = "(" + columns.join(",") + ")";

        //fetch values for all columns
        var values = columns.map((v,i)=>{
            if(v === "cc_resp_id"){
                return objectToStore[v];
            }
            var val = objectToStore[v];
            return "'" + val  + "'";
        })
        //create value string
        var values_str = "(" + values.join(",") + ")";

        var query = "insert into " + ew_ccavenue_pay_resp_table + " " + columns_str + " values " + values_str;
        var res = await cassExecute(query);
        logger.debug(res);
        return res;
    }catch(err){
        logger.debug(err);
        throw err;
    }   
}


module.exports = new CCAvenue();