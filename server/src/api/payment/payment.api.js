var debug = require('debug')("app:api:payment:payment");
var Q = require("q");
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var logger = require(configPath.lib.log_to_file);
var moment = require('moment');

var userCoursesAPI = require(configPath.api.usercourses);
var paypalAPI = require(configPath.api.payment.paypal);
var profileAPI = require(configPath.api.dashboard.profile);
var smsAPI = require(configPath.api.sms.sms);
var mailAPI = require(configPath.api.mail);
var batchTimingAPI = require(configPath.api.batchCourse.batchTiming);
var batchTimingDashboardAPI = require(configPath.api.batchCourse.batchTimingDashboard);

var pricelistAPI = require(configPath.api.pricelist.pricelist);
var batchCourseAPI = require(configPath.api.batchCourse.batchCourse);


var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_payment_dtl_tbl = "ew1.ew_payment_dtl";
var ew_usr_enroll_bat_dtls_tbl = "ew1.ew_usr_enroll_bat_dtls";

function Payment(){

}

//This method will be called once user payment is done(completed / pending)
Payment.prototype.savePaymentDetails = async function(data){
	var defer = Q.defer();
	var usr_id = data.usr_id;
	var training_id = data.training_id;
	var bat_id =  data.bat_id;
	var buy_dt =  new Date();
	var pay_id =  data.pay_id;
	var pay_method =  data.pay_method;
	var pay_status =  data.pay_status;
	try{
		//save info to db
		await saveTODB();
		var userShortDetails = await profileAPI.getUserShortDetails({usr_id : usr_id});
		if(pay_status === 'completed'){
			//if payment status is completed
			//save course to user purchase course
			var courses = await userCoursesAPI.savePurchasedCourse({usr_id : usr_id,training_id : training_id});
			//user batch enroll details
            await this.saveUserEnrolledBatchDetails({
                usr_id : usr_id,
                enroll_bat : bat_id,
                enroll_dt : new Date(),
                enroll_status : true                                     
            });

			if(userShortDetails.usr_ph){
				//if user has given mobile number then
				try{
                    var smsres = await smsAPI.sendSuccessfulBuySMS(userShortDetails.usr_ph,training_id,usr_id);
                }catch(err){
                    logger.debug(err);
                }
			}

			if(userShortDetails.usr_email){
                try{
                	var invoice_data = await this.generateInvoiceDetails({
                		training_id : training_id,
                		bat_id : bat_id
                	})
                	invoice_data["to"] = userShortDetails.usr_email;
                	invoice_data["dsp_nm"] = userShortDetails.dsp_nm;
                    var mail_res = await mailAPI.sendSuccessfullBuyTrainingMail(invoice_data);
                }catch(err){
                    logger.debug(err);
                }
			}
		}else if(pay_status === "fail"){
			if(userShortDetails.usr_ph){
				//if user has given mobile number then
				try{
                    var smsres = await smsAPI.paymentFailureSMS(userShortDetails.usr_ph,training_id);
                }catch(err){
                    logger.debug(err);
                }
			}
		}

		var resData = {
			...data,
			purchasedCourses : courses
		}
		defer.resolve(resData);
	}catch(e){
		defer.reject(e);
	}
	return defer.promise;

	function saveTODB(){
		var defer = Q.defer();
		var qry = "insert into " + ew_payment_dtl_tbl + " (usr_id,training_id,bat_id,buy_dt,pay_id,pay_method,pay_status) values (?,?,?,?,?,?,?)";
		var params = [usr_id,training_id,bat_id,buy_dt,pay_id,pay_method,pay_status];
		console.log(params);
		cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
				if(err){
					defer.reject(err);
					debug(err);
					return;
				}
				defer.resolve({});
			});	
		return defer.promise;
	}
}

Payment.prototype.savePurchasedTrainingDetails = async function(data){
	var defer = Q.defer();

	var usr_id = data.usr_id;
	var training_id = data.training_id;
	var bat_id =  data.bat_id;
	var buy_dt =  new Date();
	var pay_id =  data.pay_id;
	var pay_method =  data.pay_method;
	var pay_status =  data.pay_status;
	
	var qry = "insert into " + ew_payment_dtl_tbl + " (usr_id,training_id,bat_id,buy_dt,pay_id,pay_method,pay_status) values (?,?,?,?,?,?,?)";
	var params = [usr_id,training_id,bat_id,buy_dt,pay_id,pay_method,pay_status];
	cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
		if(err){
			defer.reject(err);
			debug(err);
			return;
		}
		defer.resolve({});
	});	
	return defer.promise;
}

Payment.prototype.saveUserEnrolledBatchDetails = async function(data){
	var defer = Q.defer();

	var usr_id = data.usr_id;
	var enroll_bat = data.enroll_bat;
	var enroll_dt =  data.enroll_dt;
	var enroll_status =  data.enroll_status;
	
	var qry = "insert into " + ew_usr_enroll_bat_dtls_tbl + " (usr_id,enroll_bat,enroll_dt,enroll_status) values (?,?,?,?)";
	var params = [usr_id,enroll_bat,enroll_dt,enroll_status];
	cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
		if(err){
			defer.reject(err);
			debug(err);
			return;
		}
		defer.resolve({});
	});	
	return defer.promise;
}

Payment.prototype.getUserEnrolledBatchDetails = async function(data){
	var defer = Q.defer();
	try{
		var usr_id = data.usr_id;
		var qry = "select * from " + ew_usr_enroll_bat_dtls_tbl + " where usr_id = ?";
		var params = [usr_id];
		var rows = await cassExecute(qry,params);
		return rows;	
	}catch(err){
		logger.debug(err);
		throw err;
	}
}



Payment.prototype.getPurchasedTrainingDetails = async function(data){
	try{
		var usr_id = data.usr_id;
		var qry = "select * from " + ew_payment_dtl_tbl + " where usr_id = ?";
		var params = [usr_id];
		var rows = await cassExecute(qry,params);
		for (var i = 0; i < rows.length; i++) {
			var v = rows[i]
			var batch_details = await batchTimingAPI.getTimingByBatId({bat_id : v.bat_id});
			v["batch_details"] = batch_details[0];

			var batch_dashboard_details = await batchTimingDashboardAPI.getBatchTimingDashboardById({bat_id : v.bat_id});
			v["batch_dashboard_details"] = batch_dashboard_details[0];

		}
		return rows;
	}catch(err){
		debug(err);
		throw err;
	}
}




Payment.prototype.getPaymentDetails = async function(data){
	var defer = Q.defer();
	var usr_id = data.usr_id;
	var training_id = data.training_id;
	var bat_id =  data.bat_id;
	try{
		//save info to db
		var res = await getFromDB();
		defer.resolve(res);
	}catch(e){
		defer.reject(e);
	}
	return defer.promise;

	function getFromDB(){
		var defer = Q.defer();
		var qry = "select * from " + ew_payment_dtl_tbl + " where usr_id = ? and training_id =? and bat_id= ?";
		var params = [usr_id,training_id,bat_id];
		cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
							if(err){
								defer.reject(err);
								debug(err);
								return;
							}
							var row = (res.rows && res.rows[0]) || {};
							var jsonRow = JSON.parse(JSON.stringify(row));
							defer.resolve(jsonRow);
						});	
		return defer.promise;
	}
}

Payment.prototype.updatePaymentStatus = async function(data){
	var defer = Q.defer();
	var usr_id = data.usr_id;
	var training_id = data.training_id;
	var bat_id =  data.bat_id;
	var pay_status = data.pay_status;
	try{
		//save info to db
		var res = await updateRecord();
		defer.resolve(data);
	}catch(e){
		defer.reject(e);
	}
	return defer.promise;

	function updateRecord(){
		var defer = Q.defer();
		var qry = "update " + ew_payment_dtl_tbl + " set pay_status=? where usr_id = ? and training_id =? and bat_id= ?";
		var params = [pay_status,usr_id,training_id,bat_id];
		cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
							if(err){
								defer.reject(err);
								debug(err);
								return;
							}
							defer.resolve({});
						});	
		return defer.promise;
	}
}

// checkPaymentStatus
Payment.prototype.checkPaymentStatus = async function(data){
	var defer = Q.defer();
	
	var usr_id = data.usr_id;
	var training_id = data.training_id;
	var bat_id =  data.bat_id;
	try{
		var obj = {
			usr_id : usr_id,
			training_id : training_id,
			bat_id : bat_id
		}
		var pay_details = await this.getPaymentDetails(obj);
		var payment_status_details = paypalAPI.checkPaymentStatus(pay_details.pay_id);
		// TODO
		// obj.pay_status = "";
		// this.updatePaymentStatus(obj);
		defer.resolve(payment_status_details);
	}catch(e){
		defer.reject(e);
	}
	return defer.promise;
}

Payment.prototype.generateInvoiceDetails = async function(data){
	var vm = {};
	vm.selectedCurrency = 'INR';
	try{
		data = data || {};
		var training_id  = data.training_id;
		var bat_id = data.bat_id;
		vm.pricelist = await pricelistAPI.getPricelistByCourseId({crs_id : training_id});
		vm.courseInfo = await batchCourseAPI.getBatchCourse({crs_id : training_id});
        var batchTimings = JSON.parse(JSON.stringify(vm.courseInfo.batchTiming));
        var batch = batchTimings.filter((v,i)=>{
        	if(v.bat_id === bat_id){
        		return true;
        	}
        	return false;
        })[0];
        vm.batch = batch;
        vm.discount = getDiscount();
        vm.netPrice = getNetPrice();
        vm.serviceTax = getServiceTax();
        vm.totalAmount = getTotalPrice();


        var data_to_send = {
        	training : vm.courseInfo.courseId,
			training_name : vm.courseInfo.courseName,
			training_start_date : moment(vm.batch.cls_start_dt).format('DD-MMM-YYYY'),
			training_from_week_day : vm.batch.frm_wk_dy,
			training_to_week_day : vm.batch.to_wk_dy,
			training_duration_count : vm.batch.no_wk_dy,
			training_duration_unit : vm.batch.wk_dy,
			training_start_time : convertTimeTo_AM_PM(vm.batch.cls_frm_tm),
			training_end_time : convertTimeTo_AM_PM(vm.batch.cls_to_tm),
			training_price : vm.pricelist.crs_price[vm.selectedCurrency],
			training_discount : vm.discount,
			training_net_price : vm.netPrice,
			training_total_saving : vm.discount,
			training_service_tax : vm.serviceTax,
			training_total_amount : vm.totalAmount,
        }

        return data_to_send;

	}catch(err){
		logger.debug(err);
		throw err;
	}

	function getDiscount() {
        if(!vm.batch.should_apply_discount){
            return 0;
        }
        var total = vm.pricelist.crs_price[vm.selectedCurrency];
        var discount = vm.pricelist.crs_price[vm.selectedCurrency] * parseInt(vm.batch.discount) / 100;
        return discount;
    }

    function getNetPrice() {
        var dis = getDiscount();
        var total = vm.pricelist.crs_price[vm.selectedCurrency];
        return Math.round(total - dis) ;
    }

    function getServiceTax() {
        var total = getNetPrice();
        var tax = getNetPrice() * 15 / 100;
        return tax;
    }

    function getTotalPrice() {
        return getNetPrice() + getServiceTax();
    }

    function convertTimeTo_AM_PM(time) {
        // time = 10:10
        var split_time = time.split(":");
        var hour = parseInt(split_time[0]);
        if(hour < 12){
            return split_time[0] + ":" + split_time[1] + " AM";
        }else if(hour === 12){
            return split_time[0] + ":" + split_time[1] + " PM";
        }else if(hour > 12){
            return 12 - split_time[0] + ":" + split_time[1] + " PM";
        }
    }
}

module.exports = new Payment();