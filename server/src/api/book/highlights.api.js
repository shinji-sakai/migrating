var configPath = require('../../configPath.js');
var config = require('../../config.js');

var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var mongodb = require('mongodb');
var logger = require(configPath.lib.log_to_file);

function BookHighlights(){

}

BookHighlights.prototype.saveBookHighlight = function(data){
	return getConnection()
        .then(dbCollection)
        .then(save)
        .catch(globalFunctions.err);

    function save(col) {
        return col.update({id : data.id},data,{upsert:true});
    }
}

BookHighlights.prototype.removeBookHighlight = function(data){
    return getConnection()
        .then(dbCollection)
        .then(remove)
        .catch(globalFunctions.err);

    function remove(col) {
        return col.remove({id : data.id,usr_id:data.usr_id});
    }
}

BookHighlights.prototype.getBookItemHighlights = function(data){
    return getConnection()
        .then(dbCollection)
        .then(get)
        .catch(globalFunctions.err);

    function get(col) {
        return col.find({bookItemId : data.bookItemId , usr_id : data.usr_id}).toArray();
    }
}

function dbCollection(db) {
    return db.collection('bookHighlights');
}

module.exports = new BookHighlights();
