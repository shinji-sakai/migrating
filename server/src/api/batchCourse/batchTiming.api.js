var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:batchTiming.api.js");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_bat_dtls_table = 'ew1.ew_bat_dtls';
var ew_bat_m_table = 'ew1.ew_bat_m';

function Timing() {

}

Timing.prototype.add = async function(data){
	var defer = Q.defer();
	try{
		debug(data);
		var bat_id = data.bat_id;
		var bat_nm = data.bat_nm;
		var bat_crs_price = data.bat_crs_price;
		var discount = data.discount;
		var msg = data.msg;
		var faculty_id = (data.faculty_id && data.faculty_id.map(function(v,i){return parseInt(v);})) || [];
		var last_class_video = data.last_class_video;
		var cls_start_dt = data.cls_start_dt ? new Date(data.cls_start_dt) : undefined;
		var cls_end_dt = data.cls_end_dt ? new Date(data.cls_end_dt) : undefined;

		var discount_frm_dt = data.discount_frm_dt ? new Date(data.discount_frm_dt) : undefined;
		var discount_to_dt = data.discount_to_dt ? new Date(data.discount_to_dt) : undefined;
		var discount_rsn  = data.discount_rsn;
		var no_wk_dy = data.no_wk_dy;
		var frm_wk_dy = data.frm_wk_dy;
		var to_wk_dy = data.to_wk_dy;
		var wk_dy = data.wk_dy;

		var crs_id = data.crs_id;
		var cls_frm_tm = data.cls_frm_tm;
		var cls_to_tm = data.cls_to_tm;
		
		if(!bat_id){
			bat_id = Long.fromNumber(Math.floor(Math.random()*90000) + 10000);
		}else{
			bat_id = Long.fromNumber(bat_id);
		}

	
		var promiseRes = Q.all([addToDetailsTable(),addToMasterTable()]);
		var res = await promiseRes;
		defer.resolve({});
	}catch(err){
		defer.reject(err);
	}
	return defer.promise;

	

	function addToDetailsTable(){
		var defer = Q.defer();
		var qry = "update " + ew_bat_dtls_table + " set bat_crs_price=?,discount=?,msg=?,faculty_id=?,last_class_video=?,cls_start_dt=?,cls_frm_tm=?,cls_to_tm=?,crs_id=?,discount_frm_dt=?,discount_to_dt=?,discount_rsn=?,no_wk_dy=?,wk_dy=?,frm_wk_dy=?,to_wk_dy=?,cls_end_dt=?,bat_nm=? where bat_id = ?";
		var params = [bat_crs_price,discount,msg,faculty_id,last_class_video,cls_start_dt,cls_frm_tm,cls_to_tm,crs_id,discount_frm_dt,discount_to_dt,discount_rsn,no_wk_dy,wk_dy,frm_wk_dy,to_wk_dy,cls_end_dt,bat_nm,bat_id];
		cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
						if(err){
							defer.reject(err);
							debug(err);
						}
						defer.resolve({});
					})
		return defer.promise;

	}

	function addToMasterTable(){	
		var defer = Q.defer();

		var qry = "update " + ew_bat_m_table + " set crt_dt=?,cls_start_dt=? where bat_id = ? and crs_id=?";
		var params = [new Date(),cls_start_dt,bat_id,crs_id];

		cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
				if(err){
					defer.reject(err);
					debug(err);
					return;
				}
				defer.resolve({});
			})
		return defer.promise;
	}
}

Timing.prototype.getAll = function(data){
	var defer = Q.defer();

	var qry = "select * from  " + ew_bat_dtls_table;
	cassconn.execute(qry,null,function(err,res,r){
		debug(err,res,r);
		if(err){
			defer.reject(err);
			debug(err);
			return;
		}
		defer.resolve(res.rows);
	})

	return defer.promise;
}


Timing.prototype.delete = function(data){
	var defer = Q.defer();

	var bat_id = data.bat_id;

	var qry = "delete from   " + ew_bat_dtls_table + " where bat_id = ?;";
	var params = [Long.fromNumber(parseInt(bat_id))];

	cassconn.execute(qry,params,function(err,res,r){
		if(err){
			defer.reject(err);
			debug(err);
			return;
		}
		defer.resolve({});
	})

	return defer.promise;
}

Timing.prototype.setActualEndDateOfBatchTiming = async function(data){
	try{
		var bat_id = data.bat_id;
		var cls_actual_end_dt = data.cls_actual_end_dt ? new Date(data.cls_actual_end_dt) : undefined;
		var query = "update " + ew_bat_dtls_table + " set cls_actual_end_dt=? where bat_id = ?";
		var params = [cls_actual_end_dt,bat_id];
		var res = await cassExecute(query,params);
		return data;
	}catch(err){
		debug(err)
		throw err;
	}
}

Timing.prototype.getTimingByCourseId = async function(data){
	var crs_id = data.crs_id;
	var defer = Q.defer();
	try{
	    var db = await getConnection();
        var col = await db.collection("batchcourse");
        var find_res = await col.find({crs_id : crs_id}, { _id: 0}).toArray();
        var no_of_days = (find_res[0] && find_res[0]["no_of_days"]) || 0;

		//first get batch array from master table based on crs-id
		var masterRes = await getBatIDfromMaster();
		if(masterRes && masterRes.length > 0){

			//get all batch id in array
			var bat_obj_array = masterRes;
			// .filter((v,i)=>{
			// 	var now = new Date();
			// 	now.setDate(now.getDate() - no_of_days);
			// 	if(v.cls_start_dt >= now){
			// 		return true;
			// 	}else{
			// 		return false;
			// 	}
			// });



			var bat_id_array = bat_obj_array.map((v,i)=>(v.bat_id));
			//get all batch details
			var res = await this.getTimingByBatId({bat_id : bat_id_array});

			res = res.map((v,i)=>{
				var today_date = new Date();
				var discount_frm_dt = new Date(v.discount_frm_dt);
				var discount_to_dt = new Date(v.discount_to_dt);

				if(today_date >= discount_frm_dt && today_date <= discount_to_dt){
					v["should_apply_discount"] = true;
				}
				return v;
			})

			defer.resolve(res);
		}else{
			defer.resolve([]);
		}
	}catch(e){
		defer.reject(e);
	}

	return defer.promise;

	function getBatIDfromMaster(){
		var defer = Q.defer();
		var qry = "select * from   " + ew_bat_m_table + " where crs_id = ?;";
		var params = [crs_id];

		cassconn.execute(qry,params,function(err,res,r){
			if(err){
				defer.reject(err);
				debug(err);
				return;
			}
			if(!res){
				defer.resolve([]);
				return;
			}
			defer.resolve(res.rows);
		})
		return defer.promise;
	}

}

Timing.prototype.getTimingByBatId = async function(data){
	var bat_id = [].concat(data.bat_id);
	var defer = Q.defer();
	try{
		var res = await get();
		defer.resolve(res);
	}catch(e){
		defer.reject(e);
		return;
	}

	return defer.promise;

	function get(){
		var defer = Q.defer();
		var bat_id_str = '(' + bat_id.join(",") + ')';

		var qry = "select * from   " + ew_bat_dtls_table + " where bat_id in " + bat_id_str;

		cassconn.execute(qry,null,function(err,res){
			if(err){
				defer.reject(err);
				debug(err);
				return;
			}
			defer.resolve(res.rows);
		})
		return defer.promise;
	}

}

module.exports = new Timing();