var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:batchCourse:sessionItems");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;

var ew_bat_sess_dtls = 'ew1.ew_bat_sess_dtls';

function SessionItem() {

}

SessionItem.prototype.add = async function(data){
	
	var defer = Q.defer();
	var bat_id = data.bat_id;
	var sess_id = Long.fromNumber(data.sess_id);
	var sess_dt = data.sess_dt ? new Date(data.sess_dt) : undefined;
	var sess_items = data.sess_items;

	try{
		var res = await addToTable();
		defer.resolve({});
	}catch(err){
		defer.reject(err);
	}
	return defer.promise;

	function addToTable(){	
		var defer = Q.defer();

		var qry = "update " + ew_bat_sess_dtls + " set sess_items=?,sess_dt=? where bat_id = ? and sess_id=?";
		var params = [sess_items,sess_dt,bat_id,sess_id];

		cassconn.execute(qry,params,{ prepare: true },function(err,res,r){
				if(err){
					defer.reject(err);
					debug(err);
				}
				defer.resolve({});
			})
		return defer.promise;
	}
}

SessionItem.prototype.getAll = function(data){
	var defer = Q.defer();

	var qry = "select * from  " + ew_bat_sess_dtls;
	cassconn.execute(qry,null,function(err,res){
		if(err){
			defer.reject(err);
			debug(err);
		}
		defer.resolve(res.rows);
	})

	return defer.promise;
}


SessionItem.prototype.delete = function(data){
	var defer = Q.defer();

	var bat_id = data.bat_id;
	var sess_id = data.sess_id;

	var qry = "delete from   " + ew_bat_dtls_table + " where bat_id = ? and sess_id = ?;";
	var params = [Long.fromNumber(parseInt(bat_id)),Long.fromNumber(parseInt(sess_id))];

	cassconn.execute(qry,params,function(err,res,r){
		if(err){
			defer.reject(err);
			debug(err);
		}
		defer.resolve({});
	})

	return defer.promise;
}

SessionItem.prototype.getSessionDetailsForBatchAndSession = function(data){
	var defer = Q.defer();

	var bat_id = data.bat_id;
	var sess_id = data.sess_id;

	var qry = "select * from  " + ew_bat_sess_dtls + " where bat_id = ? and sess_id = ?;";
	var params = [Long.fromNumber(parseInt(bat_id)),Long.fromNumber(parseInt(sess_id))];

	cassconn.execute(qry,params,function(err,res){
		if(err){
			defer.reject(err);
			debug(err);
		}
		var jsonObj = JSON.parse(JSON.stringify(res.rows[0] || {}));
		var sess_items = jsonObj.sess_items || [];
		console.log(jsonObj,sess_items);
		var obj = [];
		for(var key in sess_items){
			var type = key.substr(0,1) === 'v' ? 'video' : 'download';
			var link = sess_items[key];
			var title = key.substr(2);
			obj.push({
				type : type,
				link : link,
				title: title
			});
		}
		jsonObj["sess_items_ui"] = obj;
		defer.resolve(jsonObj);
	})

	return defer.promise;
}

module.exports = new SessionItem();