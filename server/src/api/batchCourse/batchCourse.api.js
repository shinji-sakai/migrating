var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var timingAPI = require(configPath.api.batchCourse.batchTiming);
var gettingStartedAPI = require(configPath.api.batchCourse.gettingStarted);
var sessionsAPI = require(configPath.api.batchCourse.sessions);
var bundleAPI = require(configPath.api.bundle.bundle);
var uuid = require('uuid');
var debug = require('debug')("app:api:batchCourse.api.js");

var Q=require('q');

function BatchCourse(){}

BatchCourse.prototype.getAll = async function () {
    try{
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await find(collection);
        return find_res;
    }catch(e){
        globalFunctions.err(e);
    }
    // return getConnection()
    //     .then(dbCollection)
    //     .then(find)
    //     .catch();

    function find(col) {
        return col.find({}, { _id: 0}).toArray();
    }
}

BatchCourse.prototype.getBatchCourseById = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({crs_id : data.crs_id}, { _id: 0}).toArray();
    }
}


BatchCourse.prototype.add = function (data) {
    return getConnection()      //get mongodb connection
        .then(dbCollection)     //get database collection
        .then(insertData)
        .catch(function(err) {
            console.log(err);
        });

    function insertData(col) {
        //insert/update in batchcourse
        var crs_id = data.crs_id || uuid.v4();
        data.crs_id = crs_id;

        return col.update({crs_id : crs_id},data,{upsert:true,w:1});
    }


}
BatchCourse.prototype.delete = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({crs_id: data.crs_id});
    }
}

// Here We will get full details of included courses in given batch course
BatchCourse.prototype.getIncludedCoursesInBatchCourse = async function (data) {
    var defer = Q.defer();

    var crs_id = data.crs_id;
    try{
        var res_batch = await this.getBatchCourseById({crs_id : crs_id});
        var batchcourse = res_batch[0];
        var includedBundlesId = batchcourse.includedBundles || [];
        var bundleWithCourses = await bundleAPI.getFullBundlesDetail({bndl_ids : includedBundlesId});
        var courses = [];
        for (var i = 0; i < bundleWithCourses.length; i++) {
            var bndl = bundleWithCourses[i];
            courses.push(...bndl.courses);
        }
        defer.resolve(courses);
    }catch(e){
        console.log(e);
        defer.reject(e);
    }
    return defer.promise;
}

// Get bundle course mapping 
// bndl_id -> [courses]
BatchCourse.prototype.getBundleCourseMappingInBatchCourse = async function (data) {
    var defer = Q.defer();

    var crs_id = data.crs_id;
    try{
        var res_batch = await this.getBatchCourseById({crs_id : crs_id});
        var batchcourse = res_batch[0] || {};
        var includedBundlesId = batchcourse.includedBundles || [];
        var bundleWithCourses = await bundleAPI.getFullBundlesDetail({bndl_ids : includedBundlesId});
        defer.resolve(bundleWithCourses);
    }catch(e){
        console.log(e);
        defer.reject(e);
    }
    return defer.promise;
}

// Here We will get full details of given batch course
BatchCourse.prototype.getBatchCourse = async function (data) {
    var defer = Q.defer();
    var crs_id = data.crs_id;
    try{

        var db = await getConnection();
        var col = await db.collection("contactus");
        var contactus = await col.findOne({});


        var res = await courseListAPI.getCoursesById({ids : crs_id});
        var course = res[0];



        var res_batch = await this.getBatchCourseById({crs_id : crs_id});
        var batchcourse = res_batch[0];
        var res_timing = await timingAPI.getTimingByCourseId({crs_id : crs_id});
        var batchTiming = res_timing;
        var res_gs = await gettingStartedAPI.getByCourseId({crs_id : crs_id});
        var gs_arr = res_gs && res_gs[0];
        var res_sessions = await sessionsAPI.getByCourseId({crs_id : crs_id});
        defer.resolve({
            contactus,
            ...course,
            ...batchcourse,
            batchTiming:batchTiming,
            gettingStarted : gs_arr ,
            sessions : res_sessions
        });
    }catch(e){
        console.log(e);
        defer.reject(e);
    }
    return defer.promise;
}

BatchCourse.prototype.getBatchCoursesByBundleIds = async function (data) {
    //it can be array or one bundle
    var bundle_ids = [].concat(data.bundle_ids);
    try{
        var connection = await getConnection();
        var collection = await dbCollection(connection);

        //this will be all training courses by matching criteria
        var find_res = await collection.find({
            includedBundles : {
                '$in' : bundle_ids
            }
        }).toArray();

        return find_res;
    }catch(e){
        console.log(e);
        throw e
    }
}



function dbCollection(db){
    return db.collection("batchcourse");
}

module.exports = new BatchCourse();