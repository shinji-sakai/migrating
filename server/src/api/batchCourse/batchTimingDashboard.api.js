var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var getConnection = require(configPath.dbconn.mongoconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:batchTiming.api.js");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batchTimingAPI = require(configPath.api.batchCourse.batchTiming);

var ew_bat_ds_timing_table = 'ew1.ew_bat_ds_timing';

function BatchTimingDashboard() {

}

BatchTimingDashboard.prototype.getAllBatchTimingDashboard = async function(data){
	try{
		var query = "select * from " + ew_bat_ds_timing_table;
		var res = await cassExecute(query);
		return res;
	}catch(err){
		throw err;
		logger.debug(err);
	}
}

BatchTimingDashboard.prototype.getBatchTimingDashboardById = async function(data){
	try{
		var query = "select * from " + ew_bat_ds_timing_table + " where batch_id = ?";
		var params = [Long.fromNumber(data.bat_id)];
		var res = await cassExecute(query,params);
		return res;
	}catch(err){
		throw err;
		logger.debug(err);
	}
}

BatchTimingDashboard.prototype.setBatchTimingDashboardAsCompleted = async function(data){
	try{
		var bat_id = Long.fromNumber(data.bat_id);
		var status = data.status || "completed";
		var cls_end_dt = data.cls_end_dt ? new Date(data.cls_end_dt) : undefined;

		var query = "update " + ew_bat_ds_timing_table + " set status = ? , cls_end_dt =?  where batch_id =?";
		var params = [status,cls_end_dt,bat_id];

		await batchTimingAPI.setActualEndDateOfBatchTiming({cls_actual_end_dt : cls_end_dt , bat_id : bat_id});

		var res = await cassExecute(query,params);
		return res;
	}catch(err){
		throw err;
		logger.debug(err);
	}
}

BatchTimingDashboard.prototype.addBatchTimingDashboard = async function(data){
	try{
		var bat_id = Long.fromNumber(data.bat_id);
		var status = data.status;
		var cls_start_dt = data.cls_start_dt ? new Date(data.cls_start_dt) : undefined;
		var nxt_cls_dt = data.nxt_cls_dt ? new Date(data.nxt_cls_dt) : undefined;
		var cls_end_dt = null;
		var cls_start_tm = data.cls_start_tm;
		var nxt_cls_tm = data.nxt_cls_tm;
		var training_id = data.training_id;

		var query = "update " + ew_bat_ds_timing_table + " set status = ? , cls_start_dt =? , nxt_cls_dt = ? , cls_start_tm = ? , nxt_cls_tm =? , training_id = ? where batch_id =?";
		var params = [status,cls_start_dt,nxt_cls_dt,cls_start_tm,nxt_cls_tm,training_id,bat_id];

		logger.debug(query , params)

		var res = await cassExecute(query,params);

		return data;
	}catch(err){
		throw err;
		logger.debug(err);
	}
}


BatchTimingDashboard.prototype.deleteBatchTimingDashboard = async function(data){
	try{
		var bat_id = Long.fromNumber(data.bat_id);
		
		var query = "delete from " + ew_bat_ds_timing_table + " where batch_id =?";
		var params = [bat_id];

		var res = await cassExecute(query,params);

		return data;
	}catch(err){
		throw err;
		logger.debug(err);
	}
}




module.exports = new BatchTimingDashboard();