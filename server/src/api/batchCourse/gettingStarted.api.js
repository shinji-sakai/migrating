var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var timingAPI = require(configPath.api.batchCourse.batchTiming);
var uuid = require('uuid');

var Q=require('q');

function GettingStarted(){}

GettingStarted.prototype.getAll = async function () {
    
    try{
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await find(collection);
        return find_res;
    }catch(e){
        globalFunctions.err(e);
    }

    function find(col) {
        return col.find({}, { _id: 0}).toArray();
    }
}

GettingStarted.prototype.getByCourseId = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({crs_id : data.crs_id}, { _id: 0}).toArray();
    }
}


GettingStarted.prototype.add = function (data) {
    return getConnection()      //get mongodb connection
        .then(dbCollection)     //get database collection
        .then(insertData)
        .catch(function(err) {
            console.log(err);
        });

    function insertData(col) {
        //insert/update in batchcourse
        var crs_id = data.crs_id || uuid.v4();
        data.crs_id = crs_id;

        return col.update({crs_id : crs_id},data,{upsert:true,w:1});
    }


}
GettingStarted.prototype.delete = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({crs_id: data.crs_id});
    }
}

function dbCollection(db){
    return db.collection("gettingStarted");
}

module.exports = new GettingStarted();