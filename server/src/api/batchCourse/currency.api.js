var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var uuid = require('uuid');

var Q=require('q');

function Currency(){}

Currency.prototype.getAll = function () {
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({}, { _id: 0}).toArray();
    }
}


Currency.prototype.add = function (data) {
    return getConnection()      //get mongodb connection
        .then(dbCollection)     //get database collection
        .then(insertData)
        .catch(function(err) {
            console.log(err);
        });

    async function insertData(col) {

        let obj = await col.findOne({currency_id:data.currency_id});
        
        if(!obj){
            data.create_dt = new Date();
            data.update_dt = new Date();
        }else{
            data.update_dt = new Date()
        }


        var currency_id = data.currency_id || uuid.v4();
        data.currency_id = currency_id;

        return col.update({currency_id : currency_id},{$set : data},{upsert:true,w:1});
    }
}
Currency.prototype.delete = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({currency_id: data.currency_id});
    }
}

function dbCollection(db){
    return db.collection("currency");
}

module.exports = new Currency();