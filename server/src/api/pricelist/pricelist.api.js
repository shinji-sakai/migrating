var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseListAPI = require(configPath.api.admin.course);
var batchCourseAPI = require(configPath.api.batchCourse.batchCourse);
var batchTimingAPI = require(configPath.api.batchCourse.batchTiming);
var bundleAPI = require(configPath.api.bundle.bundle);
var uuid = require('uuid');
var debug = require('debug')("app:api:pricelist.api.js");

var Q=require('q');

function Pricelist(){}

Pricelist.prototype.getAll = async function () {
    try{
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await find(collection);
        return find_res;
    }catch(e){
        globalFunctions.err(e);
    }

    function find(col) {
        return col.find({}, { _id: 0}).toArray();
    }
}

Pricelist.prototype.getPricelistByCourseId = async function (data) {
    try{
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await find(collection);
        return find_res[0];
    }catch(e){
        globalFunctions.err(e);
    }

    function find(col) {
        return col.find({crs_id:data.crs_id}, { _id: 0}).toArray();
    }
}

Pricelist.prototype.add = function (data) {
    return getConnection()      //get mongodb connection
        .then(dbCollection)     //get database collection
        .then(insertData)
        .catch(function(err) {
            console.log(err);
        });

    function insertData(col) {
        //insert/update in batchcourse
        return col.update({crs_id : data.crs_id},data,{upsert:true,w:1});
    }


}
Pricelist.prototype.delete = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({crs_id: data.crs_id});
    }
}

Pricelist.prototype.getDetailedPricelist = async function (data) {
    try{
        var allPrices = await this.getAll();

        var res = [];
        for (var i = allPrices.length - 1; i >= 0; i--) {
            var v = allPrices[i];
            var includedCoursesDetails = await batchCourseAPI.getIncludedCoursesInBatchCourse({crs_id:v.crs_id});
            var batchTimings = await batchTimingAPI.getTimingByCourseId({crs_id : v.crs_id});
            debug(batchTimings)
            var maxBatchDiscount = 0;
            var discount_rsn = "";
            batchTimings.map((v,i)=>{
                var discount = parseInt(v.discount);
                var should_apply_discount = v.should_apply_discount;
                if(should_apply_discount && maxBatchDiscount <= discount){
                    maxBatchDiscount = discount;
                    discount_rsn = v.discount_rsn;
                }
            });
            v["discount"] = maxBatchDiscount;
            v["discount_rsn"] = discount_rsn;
            v["includedCourses"] = includedCoursesDetails;
            res.push(v);
        }
        return res || [];
    }catch(e){
        throw e;
    }
}


Pricelist.prototype.getDetailedPricelistByCourseId = async function (data) {
    try{
        var bundles = await bundleAPI.getBundlesByCourseId({crs_id : data.crs_id});
        bundles = bundles.map((v,i)=>{
            return v.bndl_id
        })
        var batchTrainings = await batchCourseAPI.getBatchCoursesByBundleIds({bundle_ids : bundles});
        var batchTrainingsId = batchTrainings.map((v,i)=>{return v.crs_id});
        debug(batchTrainingsId);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var allPrices = await collection.find({
            crs_id : {
                $in : batchTrainingsId
            }
        }).toArray();

        var res = [];
        for (var i = allPrices.length - 1; i >= 0; i--) {
            var v = allPrices[i];
            var includedCoursesDetails = await batchCourseAPI.getIncludedCoursesInBatchCourse({crs_id:v.crs_id});
            var batchTimings = await batchTimingAPI.getTimingByCourseId({crs_id : v.crs_id});
            var maxBatchDiscount = 0;
            var discount_rsn = "";
            batchTimings.map((v,i)=>{
                var discount = parseInt(v.discount);
                var should_apply_discount = v.should_apply_discount;
                if(should_apply_discount && maxBatchDiscount <= discount){
                    maxBatchDiscount = discount;
                    discount_rsn = v.discount_rsn;
                }
            });
            v["discount"] = maxBatchDiscount;
            v["discount_rsn"] = discount_rsn;
            v["includedCourses"] = includedCoursesDetails;
            res.push(v);
        }
        return res || [];

    }catch(e){
        throw e;
    }
}




function dbCollection(db){
    return db.collection("pricelist");
}

module.exports = new Pricelist();