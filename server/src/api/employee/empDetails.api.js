var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:forums:forums");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var Long = require('cassandra-driver').types.Long;
var promisify = require("promisify-node");
var Promise = require("bluebird");


var ew_emp_dtls_table = 'ew1.ew_emp_dtls';

function Employee() {

}

Employee.prototype.add = function(data){
	var defer = Q.defer();

	var emp_id = data.emp_id;
	var fst_nm = data.fst_nm;
	var lst_nm = data.lst_nm;
	var mdl_nm = data.mdl_nm;
	var address = data.address;
	var typ = data.typ;
	var ph_no = [data.ph_no];
	var experties = data.experties;

	if(!emp_id){
		emp_id = Long.fromNumber(Math.floor(Math.random()*90000) + 10000);
	}

	var qry = "update " + ew_emp_dtls_table + " set fst_nm=?,lst_nm=?,mdl_nm=?,adress=?,ph_no=?,experties=?,typ_id=? where emp_id = ?";

	var params = [fst_nm,lst_nm,mdl_nm,address,ph_no,experties,typ,emp_id];
	debug(params);
	cassconn.execute(qry,params,function(err,res,r){
						if(err){
							defer.reject(err);
							debug(err);
						}
						defer.resolve({});
					})


	return defer.promise;
}

Employee.prototype.getAll = function(data){
	var defer = Q.defer();

	var qry = "select * from  " + ew_emp_dtls_table;
	
	cassconn.execute(qry,null,function(err,res,r){
		if(err){
			defer.reject(err);
			debug(err);
			return;
		}
		if(!res){
			defer.resolve({});
			return;	
		}
		var jsonRows = JSON.parse(JSON.stringify(res.rows));
		defer.resolve(jsonRows);
	})

	return defer.promise;
}


Employee.prototype.delete = function(data){
	var defer = Q.defer();

	var emp_id = data.emp_id;

	var qry = "delete from   " + ew_emp_dtls_table + " where emp_id = ?;";
	var params = [Long.fromNumber(parseInt(emp_id))];
	debug(qry,params);
	cassconn.execute(qry,params,function(err,res,r){
		if(err){
			defer.reject(err);
			debug(err);
		}
		defer.resolve({});
	})

	return defer.promise;
}

module.exports = new Employee();