class MongoTables{
  /**
   * @param {string} questionNumber - Question Number 
   * @param {string} questionId - Question Id
   * @param {string} question - Question Description. This will be transformed plain html if user has entered MATH Code
   * @param {string} questionTime - Question Time.
   * @param {Object} options - Option contains id , original text without any transformation and text which will be transformed plain html if user has entered MATH Code
   * @param {string} textExplanation - Text Explanation
   * @param {Array}  ans - Encrypted answer id
   * @param {Array}  subjects - Subject in which this question comes
   * @param {Array}  authors - Related Authors
   * @param {Array}  publications - Related publications
   * @param {Array}  books - Related books
   * @param {Array}  exams - Related exams
   * @param {Array}  topics - Related topics
   * @param {Array}  topicGroup - Related topicGroup
   * @param {Array}  tags - Related tags
   * @param {string}  questionDifficulty - Question Difficulties
   * @param {string}  originalQuestion - Original Question without any transformation
   * @desc This Collection is used to store all practice questions.
   */
  practiceQuestions(){
    let str = {
      "questionNumber" : 5,
      "questionId" : "",
      "question" : "",
      "questionTime" : 1,
      "options" : [ 
          {
              "text" : "",
              "id" : "",
              "originalText" : ""
          }, 
          {
              "text" : "",
              "id" : "",
              "originalText" : ""
          }
      ],
      "textExplanation" : "",
      "ans" : [],
      "subjects" : [],
      "authors" : [],
      "publications" : [],
      "books" : [],
      "exams" : [],
      "topics" : [],
      "topicGroup" : [],
      "tags" : [],
      "questionDifficulty" : "",
      "originalQuestion" : ""
    }
  }
}


class CassandraTables{
	
	/**
     * @param {string} qry_id - Question Id 
     * @param {string} qry_title - Question Description
     * @param {string} usr_id - Id of user who posted the question
     * @param {Array} qry_tags - Question Categories
     * @param {Array} qry_txt - Question Description
     * @param {Array} qry_pst_id - Post Id , if question is posted from careerbook
     * @desc This Table is used to store all qa questions.
     */
	vt_forum_qrys(){
		let str = `	
			create table vt_forum_qrys
			(
				qry_id uuid,
				usr_id text ,
				qry_title text,
				qry_txt text,
				qry_ts timestamp,
				qry_tags SET<text>,
				qry_pst_id uuid,
				PRIMARY KEY (qry_id)
			);
		`
	}
  
  /**
   * @param {string} usr_id - User Id 
   * @param {string} del_typ - Delete type
   * @param {uuid} del_id - uuid
   * @param {string} del_rsn - Delete Reason , why user deleted the question
   * @param {timestamp} del_ts - Time when user delete the question
   * @desc This Table is used to store delete reason for deleted qa questions.
   */
  ew_del_rsn(){
    let str = `
      create table ew_del_rsn
      (
        usr_id text,
        del_typ text ,
        del_id uuid,
        del_rsn  text,
        del_ts timestamp,
        primary key(usr_id,del_ts) 
      ) with clustering order by(del_ts desc)
    `
  }
  
  /**
   * @param {string} usr_id - User Id 
   * @param {counter} no_of_bkmrks - Number Of Bookmarks
   * @param {counter} no_of_cb_cmnts - Number Of CB Comments
   * @param {counter} no_of_frds - Number Of Friends
   * @param {counter} no_of_notes - Number Of Notes
   * @param {counter} no_of_psts - Number Of Posts
   * @param {counter} no_of_qa_cmnts - Number Of QA Comments
   * @param {counter} no_of_qa_que_ans - Number Of QA Question Answered
   * @param {counter} no_of_qa_que_ask - Number Of QA Question Asked
   * @param {counter} no_of_qa_que_vws - Number Of QA Question Views
   * @param {counter} no_of_que_attempt_correct - Number Of Correct Question Attempt
   * @param {counter} no_of_que_attempt_tsts - Number Of Test Attempt
   * @param {counter} no_of_que_attempt_wrong - Number Of Wrong Question Attempt
   * @param {counter} no_of_que_learned - Number Of Question Learned
   * @param {counter} no_of_que_practiced - Number Of Question Practiced
   * @param {counter} no_of_tsts_taken - Number Of Test Taken
   * @param {counter} no_of_vdo_vws - Number Of Video Views
   * @desc This Table is used to store dashboard stats of user
   */
  ew_dashboard_stats(){
    let str = `
      CREATE TABLE ew1.ew_dashboard_stats (
          usr_id text,
          no_of_bkmrks counter,
          no_of_cb_cmnts counter,
          no_of_frds counter,
          no_of_notes counter,
          no_of_psts counter,
          no_of_qa_cmnts counter,
          no_of_qa_que_ans counter,
          no_of_qa_que_ask counter,
          no_of_qa_que_vws counter,
          no_of_que_attempt_correct counter,
          no_of_que_attempt_tsts counter,
          no_of_que_attempt_wrong counter,
          no_of_que_learned counter,
          no_of_que_practiced counter,
          no_of_tsts_taken counter,
          no_of_vdo_vws counter,
          PRIMARY KEY (usr_id)
      )
    `
  }
  
  /**
   * @param {uuid} qry_id - QA Question Id 
   * @param {counter} vw_cnt - Views counter
   * @desc This Table is used to store total views of qa questions.
   */
  vt_qry_trend(){
    let str = `
      create table vt_qry_trend
      (
        qry_id uuid ,
        vw_cnt  counter,
        primary key ((qry_id) , vw_cnt)
      )  WITH CLUSTERING ORDER BY (vw_cnt desc);
    `
  }
  
  /**
   * @param {uuid} qry_id - QA Question Id 
   * @param {string} usr_id - UserId
   * @param {timestamp} vw_ts - View Time
   * @desc This Table is used to store question view timestamp of user
   */
  vt_qry_vws(){
    let str = `
      create table vt_qry_vws
      (
        qry_id uuid ,
        usr_id text ,
        vw_ts timestamp,
        primary key (qry_id,usr_id,vw_ts)
      );
    `
  }
  
  /**
   * @param {uuid} qry_id - Question Id 
   * @param {counter} qry_upv - Number Of Question Upvote
   * @param {counter} qry_dnv - Number Of Question Downvote
   * @param {counter} qry_ans - Number Of Question Answers
   * @param {counter} qry_shr - Number Of Question Share
   * @param {counter} qry_flw - Number Of Question Follower
   * @param {counter} qry_vws - Number Of Question Views
   * @desc This Table is used to store dashboard stats of user
   */
  ew_qry_stats(){
    let str = `
      create table ew_qry_stats
      (
        qry_id  uuid,
        qry_upv counter,
        qry_dnv counter,
        qry_ans counter ,
        qry_shr counter,
        qry_flw counter,
        qry_vws counter,
        primary key (qry_id) 
      );
    `
  }
	
  /**
   * @param {uuid} qry_id - Question Id 
   * @param {text} usr_id - user id
   * @param {boolean} like_dlike_flg - Like(true)/Dislike(false) Flag
   * @param {boolean} qry_ans - user has answered the question or not
   * @param {boolean} qry_shr - user has shared the question or not
   * @param {boolean} qry_flw - user is following question or not
   * @param {boolean} qry_vws - user has viewed the question or not
   * @param {timestamp} crt_ts - create timestamp
   * @desc This Table is used to store user stats for question
   */
  ew_qry_usr_stats(){
    let str = `
      create table ew_qry_usr_stats
      (
        qry_id uuid,
        usr_id text,
        like_dlike_flg  boolean,
        qry_shr  boolean,
        qry_flw boolean,
        qry_vws  boolean,
        qry_ans boolean,
        crt_ts timestamp,
        primary key(qry_id,usr_id)
      );
    `
  }
  
  /**
   * @param {uuid} qry_id - Question Id 
   * @param {text} usr_id - user id
   * @param {timestamp} flw_ts - follow timestamp
   * @desc This Table is used to store followers of question
   */
  ew_qry_flw(){
    let str = `
      create table ew_qry_flw
      (
        usr_id text,
        qry_id uuid,
        flw_ts timestamp,
        primary key( (qry_id),usr_id)
      )
    `
  }
  
  /**
   * @param {uuid} qry_id - Question Id 
   * @param {text} usr_id - user id
   * @param {uuid} ans_id - answer id
   * @param {text} ans_usr_id - id of user who answered the question
   * @param {timestamp} ans_ts - timestamp of answer
   * @param {boolean} ntf_vw_flg - notification has been viewed or not
   * @desc This Table is used to store answer notification for question followers
   */
  ew_qry_flw_ntf(){
    let str = ` 
      create table ew_qry_flw_ntf
      (
        usr_id text,
        qry_id uuid,
        ans_id uuid,
        ans_usr_id text,
        ans_ts timestamp,
        ntf_vw_flg boolean,
        primary key((usr_id), ans_usr_id , ans_ts )
      )
    `
  }
  
  /**
   * @param {uuid} qry_id - Question Id 
   * @param {text} usr_id - user id
   * @param {timestamp} crt_ts - create/updated timestamp
   * @param {boolean} ans_flg - question is marked it answer later or not
   * @desc This Table is used to store status of answer later for given user and question
   */
  ew_qry_ans_ltr(){
    let str = `
      create table ew_qry_ans_ltr
      (
        usr_id text,
        qry_id uuid,
        crt_ts timestamp,
        ans_flg boolean,
        primary key((usr_id), qry_id)
      );
    `
  }
  
  /**
   * @param {uuid} cmpln_id - Query Id for which user is submitting complaining
   * @param {text} usr_id - user id
   * @param {text} cmpln_typ - Type of Complaint For e.g. Wrong Question, Inappropriate Question, etc...
   * @param {text} cmpln_rsn - Complaint reason if any
   * @param {timestamp} cmpln_ts - Time of complaint
   * @desc This Table is used to store question complaints
   */
  ew_cmpln_rsn(){
    let str = `
      create table ew_cmpln_rsn
      (
        usr_id text,
        cmpln_typ text ,
        cmpln_id uuid,
        cmpln_rsn text,
        cmpln_ts timestamp,
        primary key(usr_id,cmpln_ts) 
      ) with clustering order by(cmpln_ts desc)
    `
  }
  
  /**
   * @param {uuid} cat_id - Category Id 
   * @param {text} cat_name - Category Name
   * @param {text} cat_desc - Category Description
   * @param {timestamp} crt_ts - Create Timestamp
   * @desc This Table is used to store question categories
   */
  vt_qry_cats(){
    let str = `
      create table vt_qry_cats
      (
        cat_id uuid,
        cat_name text,
        cat_desc text,
        crt_ts timestamp,
        primary key (cat_id)
      );
    `
  }
  
  /**
   * @param {text} pst_crt_by - Post Author Id
   * @param {text} pst_tgt - Public , Only Me , Friends
   * @param {uuid} pst_id - Post Id
   * @param {timestamp} pst_dt - Posted Date
   * @param {text} pst_feel - happy,Sad, etc...
   * @param {Array} pst_img - array of url of post images
   * @param {timestamp} pst_mfy_date - Post Modify Date
   * @param {text} pst_msg - Post Msg
   * @param {text} pst_qry_desc - Post Query Descrioption if it has reply on QA
   * @param {text} pst_qry_id - Query ID if it has reply on QA
   * @param {text} pst_itm_id - If post is posted from some item like from video page ,practice page
   * @param {text} pst_shr_txt - If post is shared from some page
   * @param {Array} pst_tag_frs - Tagged Friends
   * @param {text} pst_typ - post Type
   * @param {Array} pst_vid - Post Video Attachments
   * @desc This Table is used to store CB Post Data
   */
  vt_posts(){
    let str = `
      CREATE TABLE ew1.vt_posts (
          pst_id uuid,
          pst_crt_by text,
          pst_dt timestamp,
          pst_feel text,
          pst_img set<text>,
          pst_itm_id text,
          pst_mfy_date timestamp,
          pst_msg text,
          pst_qry_desc text,
          pst_qry_id uuid,
          pst_shr_txt text,
          pst_tag_frs set<text>,
          pst_tgt text,
          pst_typ text,
          pst_vid set<text>,
          PRIMARY KEY (pst_id)
      )
    `
  }
  
  /**
   * @param {text} pst_crt_by - Post Author Id
   * @param {text} pst_tgt - Public , Only Me , Friends
   * @param {uuid} pst_id - Post Id
   * @param {timestamp} pst_dt - Posted Date
   * @param {text} pst_feel - happy,Sad, etc...
   * @param {Array} pst_img - array of url of post images
   * @param {timestamp} pst_mfy_date - Post Modify Date
   * @param {text} pst_msg - Post Msg
   * @param {text} pst_qry_desc - Post Query Descrioption if it has reply on QA
   * @param {text} pst_qry_id - Query ID if it has reply on QA
   * @param {text} pst_itm_id - If post is posted from some item like from video page ,practice page
   * @param {text} pst_shr_txt - If post is shared from some page
   * @param {Array} pst_tag_frs - Tagged Friends
   * @param {text} pst_typ - post Type
   * @param {Array} pst_vid - Post Video Attachments
   * @desc This Table is used to store CB Post Data
   */
  vt_posts_crt_by(){
    let str = `
      CREATE TABLE ew1.vt_posts_crt_by (
          pst_crt_by text,
          pst_tgt text,
          pst_id uuid,
          pst_dt timestamp,
          pst_feel text,
          pst_img set<text>,
          pst_itm_id text,
          pst_mfy_date timestamp,
          pst_msg text,
          pst_qry_desc text,
          pst_qry_id uuid,
          pst_shr_txt text,
          pst_tag_frs set<text>,
          pst_typ text,
          pst_vid set<text>,
          PRIMARY KEY ((pst_crt_by, pst_tgt), pst_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {text} dsp_nm - User Display Name
   * @param {text} usr_email - User Email
   * @param {text} usr_ph - User Phone Number
   * @param {text} usr_pic - User Display Pic
   * @param {text} usr_role - User Role
   * @desc This Table is used to store User short details
   */
  ew_usr_shrt_dtls(){
    let str = `
      CREATE TABLE ew1.ew_usr_shrt_dtls (
          usr_id text PRIMARY KEY,
          dsp_nm text,
          usr_email text,
          usr_ph text,
          usr_pic text,
          usr_role text
      );
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {text} fr_id - id of friend
   * @desc This Table is used to store friends for CB
   */
  vt_user_frs(){
    let str = `
      create table vt_user_frs
      (
        usr_id     text,
        fr_id  set<text>,
        PRIMARY KEY (usr_id)
      );
    `
  }
  
  /**
   * @param {text} user_id - User Id 
   * @param {text} fr_id - Friend id
   * @param {text} last_talked - last updated time
   * @param {text} typ - user type {User / Grp}
   * @desc This Table is used to store user's friends with whom he last taled/updated
   */
  vt_frs_chat_ts(){
    let str = `
      CREATE TABLE vt_frs_chat_ts (
          user_id text,
          fr_id text,
          last_talked timestamp,
          typ text,
          PRIMARY KEY (user_id, fr_id, last_talked)
      )
    `
  }
  
  /**
   * @param {text} user_id - User Id 
   * @param {text} display_name - display name
   * @param {text} pic50 - display pic url
   * @param {text} typ - user type {User / Grp}
   * @param {text} user_email - user Email
   * @desc This Table is used to store user short details for chat 
   */
  vt_usr_short(){
    let str = `
      CREATE TABLE vt_usr_short (
          user_id text,
          display_name text,
          pic50 text,
          typ text,
          user_email text,
          PRIMARY KEY (user_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {text} fr_id - Friend Id
   * @param {text} add_dt - Timestamp
   * @param {text} fr_req_snt - Friend Req sent or not
   * @desc This Table is used to store recommended friend list 
   */
  ew_rec_frs(){
    let str = `
      CREATE TABLE ew_rec_frs (
          usr_id text,
          fr_id text,
          add_dt timestamp,
          fr_req_snt text,
          PRIMARY KEY (usr_id, fr_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {text} fr_id - Friend Id
   * @param {text} req_dt - Friend Request Timestamp
   * @param {text} req_sts - Request Status (pending='p' , confirmed='f' , deleted='d' ....)
   * @desc This Table is used to store Sent Friend Requests
   */
  ew_fr_req_snt(){
    let str = `
      CREATE TABLE ew1.ew_fr_req_snt (
        usr_id text,
        fr_id text,
        req_dt timestamp,
        req_sts text,
        PRIMARY KEY (usr_id, fr_id)
      ) 
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {text} fr_id - Friend Id
   * @param {text} req_dt - Friend Request Timestamp
   * @param {text} req_sts - Request Status (pending='p' , confirmed='f' , deleted='d' , ....)
   * @desc This Table is used to store Received Friend Requests
   */
  ew_fr_req_rcvd(){
    let str = `
      CREATE TABLE ew1.ew_fr_req_rcvd (
          fr_id text,
          usr_id text,
          req_dt timestamp,
          req_sts text,
          PRIMARY KEY (fr_id, usr_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {Array} flw_id - Array of followers id
   * @param {text} flw_dt - Follow Timestamp
   * @param {text} req_sts - Request Status (pending='p' , confirmed='f' , deleted='d' , ....)
   * @desc This Table is used to store Received Friend Requests
   */
  ew_cb_my_flw(){
    let str = `
      CREATE TABLE ew_cb_my_flw (
          usr_id text,
          flw_dt timestamp,
          flw_id set<text>,
          PRIMARY KEY (usr_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {text} ctry - Country
   * @param {text} ctry_flg - Country restrition (Public , Only Me , Friends)
   * @param {text} cty - City
   * @param {text} cty_flg - City restrition (Public , Only Me , Friends)
   * @param {text} dob_dt - DOB
   * @param {text} dob_dt_flg - DOB restrition (Public , Only Me , Friends)
   * @param {text} dob_mn - DOB Month
   * @param {text} dob_mn_flg - DOB Month restrition (Public , Only Me , Friends)
   * @param {text} dob_yr - DOB Year
   * @param {text} dob_yr_flg - DOB Year restrition (Public , Only Me , Friends)
   * @param {text} dsp_nm - Display Name
   * @param {text} dsp_nm_flg - Display Name restrition (Public , Only Me , Friends)
   * @param {map} email - Emails
   * @param {map} email_nv - Non verified Emails
   * @param {text} fst_nm - Firstname
   * @param {text} fst_nm_flg - Firstname restrition (Public , Only Me , Friends)
   * @param {text} ht_ctry - Hometown Country
   * @param {text} ht_ctry_flg - Hometown Country restrition (Public , Only Me , Friends)
   * @param {text} ht_cty - HomeTown City
   * @param {text} ht_cty_flg - HomeTown City restrition (Public , Only Me , Friends)
   * @param {text} ht_zip_cd - Hometown zip code
   * @param {text} ht_zip_cd_flg - Hometown zip code restrition (Public , Only Me , Friends)
   * @param {text} lst_nm - Last name
   * @param {text} lst_nm_flg - Last name restrition (Public , Only Me , Friends)
   * @param {map} ph - Phone Numbers 
   * @param {map} ph_nv - non verified phone number
   * @param {text} role - Role
   * @param {text} usr_abt_me - about user
   * @param {text} usr_abt_me_flg - about user restrition (Public , Only Me , Friends)
   * @param {text} zip_cd - Zip Code
   * @param {text} zip_cd_flg - Zip Code restrition (Public , Only Me , Friends)
   * @desc This Table is used to store Personal Details of user
   */
  ew_usr_dtls(){
    let str = `
      CREATE TABLE ew_usr_dtls (
          usr_id text,
          ctry text,
          ctry_flg text,
          cty text,
          cty_flg text,
          dob_dt text,
          dob_dt_flg text,
          dob_mn text,
          dob_mn_flg text,
          dob_yr text,
          dob_yr_flg text,
          dsp_nm text,
          dsp_nm_flg text,
          email map<text, text>,
          email_nv map<text, text>,
          fst_nm text,
          fst_nm_flg text,
          ht_ctry text,
          ht_ctry_flg text,
          ht_cty text,
          ht_cty_flg text,
          ht_zip_cd text,
          ht_zip_cd_flg text,
          lst_nm text,
          lst_nm_flg text,
          ph map<text, text>,
          ph_nv map<text, text>,
          role text,
          usr_abt_me text,
          usr_abt_me_flg text,
          zip_cd text,
          zip_cd_flg text,
          PRIMARY KEY (usr_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {map} usr_ph - List of user phone numbers for e.g. {<num1>:'p',<num2>:'s'....}. P=Primry , S = Secondary 
   * @param {map} usr_email - List of user emails for e.g. {<email1>:'p',<email2>:'s'....}. P=Primry , S = Secondary
   * @param {text} reg_dt - Timestamp
   * @desc This Table is used to store contact details (emails and phone numbers) of user
   */
  ew_usr_contact_dtls(){
    let str = `
      CREATE TABLE ew1.ew_usr_contact_dtls (
        usr_id text,
        reg_dt timestamp,
        usr_email map<text, text>,
        usr_ph map<text, text>,
        PRIMARY KEY (usr_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {uuid} usr_wrk_sk - unique id
   * @param {text} comp_desc - Company Desc
   * @param {text} comp_nm - Company Name
   * @param {text} frm_dt - From Date
   * @param {text} to_dt - To Date
   * @param {text} loc - Company Location
   * @param {text} role - Role in company,
   * @param {text} work_flg - (public,only me,friends)
   * @desc This Table is used to store users compny details
   */
  ew_usr_work_dtls(){
    let str = `
      CREATE TABLE ew_usr_work_dtls (
        usr_id text,
        usr_wrk_sk uuid,
        comp_desc text,
        comp_nm text,
        frm_dt text,
        loc text,
        role text,
        to_dt text,
        work_flg text,
        PRIMARY KEY (usr_id, usr_wrk_sk)
      )
    `
  }

   /**
     * @param {text} usr_id - User Id 
     * @param {uuid} usr_wrk_sk - unique id
     * @param {text} edu_desc - Education Desc
     * @param {text} edu_lvl - Education level (primary school,clg,secondary school,etc...)
     * @param {text} frm_dt - From Date
     * @param {text} to_dt - To Date
     * @param {text} loc - School/Clg Location
     * @param {text} edu_nm - Clg/schl name
     * @param {text} edu_flg - (public,only me,friends)
     * @desc This Table is used to store users education details
     */
  ew_usr_edu_dtls(){
    let str = `
      CREATE TABLE ew1.ew_usr_edu_dtls (
          usr_id text,
          usr_wrk_sk uuid,
          edu_desc text,
          edu_flg text,
          edu_lvl text,
          edu_nm text,
          frm_dt text,
          loc text,
          to_dt text,
          PRIMARY KEY (usr_id, usr_wrk_sk)
      )
    `
  }
  
  /**
     * @param {text} usr_id - User Id 
     * @param {uuid} usr_exm_sk - unique id
     * @param {text} tgt_yr - Target Year
     * @param {text} exm_nm - Exam name
     * @param {text} exm_flg - (public,only me,friends)
     * @param {timestamp} crt_dt - Create Date
     * @desc This Table is used to store users exam details
     */
  ew_usr_exm_dtls(){
    let str = `
      CREATE TABLE ew1.ew_usr_exm_dtls (
          usr_id text,
          usr_exm_sk uuid,
          crt_dt timestamp,
          exm_flg text,
          exm_nm text,
          tgt_yr text,
          PRIMARY KEY (usr_id, usr_exm_sk)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {uuid} usr_crs_sk - unique id
   * @param {text} crs_nm - Course name
   * @param {text} crs_flg - (public,only me,friends)
   * @param {timestamp} crt_dt - Create Date
   * @desc This Table is used to store users interested course details in CB
   */
  ew_usr_crs_dtls(){
    let str = `
      CREATE TABLE ew1.ew_usr_crs_dtls (
          usr_id text,
          usr_crs_sk uuid,
          crs_flg text,
          crs_nm text,
          crt_dt timestamp,
          PRIMARY KEY (usr_id, usr_crs_sk)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {uuid} usr_bk_sk - unique id
   * @param {text} author_desc - Author Desc
   * @param {text} crs_bk_desc - Course/Book Desc
   * @param {text} crs_bk_nm - Course/Book Name
   * @desc This Table is used to store authors details for CB
   */
  ew_usr_author_dtl(){
    let str = `
      CREATE TABLE ew1.ew_usr_author_dtl (
          usr_id text,
          usr_bk_sk uuid,
          author_desc text,
          crs_bk_desc text,
          crs_bk_nm text,
          PRIMARY KEY (usr_id, usr_bk_sk)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {uuid} tch_crs_sk - unique id
   * @param {text} tch_crs - Courses
   * @param {text} tch_desc - Desc
   * @param {text} edu_nm - Education Name
   * @desc This Table is used to store teachers details for CB
   */
  ew_usr_tch_dtl(){
    let str = `
      CREATE TABLE ew1.ew_usr_tch_dtl (
          usr_id text,
          tch_crs_sk uuid,
          edu_nm text,
          tch_crs text,
          tch_desc text,
          PRIMARY KEY (usr_id, tch_crs_sk)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User Id 
   * @param {text} kid_usr_id - kid user id
   * @param {text} kid_edu_nm - kid eduction name
   * @param {text} kid_exm_nm - exam name
   * @desc This Table is used to store kids info for CB
   */
  ew_usr_parent_kid(){
    let str = `
      CREATE TABLE ew1.ew_usr_parent_kid (
          usr_id text,
          kid_usr_id text,
          kid_edu_nm text,
          kid_exm_nm set<text>,
          PRIMARY KEY (usr_id, kid_usr_id)
      )
    `
  }
  
  /**
   * @param {text} to_usr_id - User Id to whom we want to send notification
   * @param {text} frm_usr_id - User due to which notification generated
   * @param {text} noti_msg - Notification msg
   * @param {text} noti_vw_flg - Notification viewd or not
   * @param {text} snt_ts - Notification sent time
   * @desc This Table is used to store user's CB notifications
   */
  ew_fr_req_noti(){
    let str = `
      CREATE TABLE ew1.ew_fr_req_noti (
          to_usr_id text,
          frm_usr_id text,
          noti_msg text,
          noti_vw_flg text,
          snt_ts timestamp,
          PRIMARY KEY (to_usr_id, frm_usr_id)
      )
    `
  }
  
  /**
   * @param {text} to_usr_id - User Id to whom we want to send notification
   * @param {text} pst_id - Post Id in which user is tagged
   * @param {text} frm_usr_id - Userid who tagged his friends
   * @param {text} noti_vw_flg - Notification viewd or not
   * @param {text} noti_msg - Notification msg
   * @param {text} snt_ts - Notification sent time
   * @desc This Table is used to store user's CB notifications
   */
  ew_pst_tag_noti(){
    let str = `
      CREATE TABLE ew1.ew_pst_tag_noti (
          to_usr_id text,
          pst_id uuid,
          frm_usr_id text,
          noti_msg text,
          noti_vw_flg text,
          snt_ts timestamp,
          PRIMARY KEY (to_usr_id, pst_id, frm_usr_id)
      )
    `
  }
  
  /**
   * @param {uuid} pst_id - Post ID to which comment is related
   * @param {timestamp} cmnt_ts - Comment Timestamp
   * @param {uuid} cmnt_id - Comment Id
   * @param {set} cmnt_atch - Array Of Comment Attachment
   * @param {uuid} cmnt_cmnt_id - If comment is level 1 comment then this will be id of parent comment
   * @param {set} cmnt_img - Image attached to comment
   * @param {text} cmnt_lvl - Comment Level
   * @param {text} cmnt_txt - Comment Text
   * @param {set} cmnt_vid - Video attached to comment
   * @param {text} usr_id - User Id who posted the comment
   * @desc This Table is used to store CB Comments
   */
  vt_usr_comnt_lvl0(){
    let str = `
      CREATE TABLE vt_usr_comnt_lvl0 (
          pst_id uuid,
          cmnt_ts timestamp,
          cmnt_id uuid,
          cmnt_atch set<text>,
          cmnt_cmnt_id uuid,
          cmnt_img set<text>,
          cmnt_lvl text,
          cmnt_txt text,
          cmnt_vid set<text>,
          usr_id text,
          PRIMARY KEY (pst_id, cmnt_ts, cmnt_id)
      )
    `
  }
  
   /**
   * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
   * @param {timestamp} crt_ts - Timestamp
   * @param {uuid} cmnt_id - Comment Id
   * @param {int} like_dlike_flg - Liked/Disliked
   * @param {text} usr_id - User Id liked the comment
   * @desc This Table is used to store Like/Disliked by user
   */
  vt_qry_like(){
    let str = `
      CREATE TABLE ew1.vt_qry_like (
          qry_id uuid,
          usr_id text,
          cmnt_id uuid,
          crt_ts timestamp,
          like_dlike_flg int,
          PRIMARY KEY ((qry_id, usr_id), cmnt_id)
      )
    `
  }
  
  /**
   * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
   * @param {uuid} cmnt_id - Comment Id
   * @param {text} usr_id - User Id liked the comment
   * @desc This is materialized view
   */
  vt_qry_like_usrs(){
  }
  
  /**
   * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
   * @param {uuid} cmnt_id - Comment Id
   * @param {counter} dlike_cnt - Dislike Count
   * @param {counter} like_cnt - Like Count
   * @desc This table is used to store like/dislike count for given comment
   */
  vt_qry_like_stats(){
    let str = `
      CREATE TABLE ew1.vt_qry_like_stats (
        qry_id uuid,
        cmnt_id uuid,
        dlike_cnt counter,
        like_cnt counter,
        PRIMARY KEY (qry_id, cmnt_id)
      ) 
    `
  }
  
  /**
   * @param {text} usr_id - User id
   * @param {timestamp} pst_dt - Post Date
   * @param {uuid} pst_id - Post ID
   * @desc This table is used to store users feed , like all posts from his friends and from he is following
   */
  vt_frs_posts(){
    let str = `
      CREATE TABLE vt_frs_posts (
        usr_id text,
        pst_dt timestamp,
        pst_id uuid,
        PRIMARY KEY (usr_id, pst_dt, pst_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User id
   * @param {timestamp} pst_dt - Post Date
   * @param {uuid} pst_id - Post ID
   * @desc This table is used to store users own post ids
   */
  vt_my_posts(){
    let str = `
      CREATE TABLE ew1.vt_my_posts (
        usr_id text,
        pst_dt timestamp,
        pst_id uuid,
        PRIMARY KEY (usr_id, pst_dt, pst_id)
      )
    `
  }
  
  /**
   * @param {text} pst_crt_by - Post Author Id
   * @param {text} pst_tgt - Public , Only Me , Friends
   * @param {uuid} pst_id - Post Id
   * @param {timestamp} pst_dt - Posted Date
   * @param {text} pst_feel - happy,Sad, etc...
   * @param {Array} pst_img - array of url of post images
   * @param {timestamp} pst_mfy_date - Post Modify Date
   * @param {text} pst_msg - Post Msg
   * @param {text} pst_qry_desc - Post Query Descrioption if it has reply on QA
   * @param {text} pst_qry_id - Query ID if it has reply on QA
   * @param {text} pst_itm_id - If post is posted from some item like from video page ,practice page
   * @param {text} pst_shr_txt - If post is shared from some page
   * @param {Array} pst_tag_frs - Tagged Friends
   * @param {text} pst_typ - post Type
   * @param {Array} pst_vid - Post Video Attachments
   * @desc This Table is used to store CB Post Data
   */
  vt_posts_crt_by(){
    let str = `
      CREATE TABLE ew1.vt_posts_crt_by (
          pst_crt_by text,
          pst_tgt text,
          pst_id uuid,
          pst_dt timestamp,
          pst_feel text,
          pst_img set<text>,
          pst_itm_id text,
          pst_mfy_date timestamp,
          pst_msg text,
          pst_qry_desc text,
          pst_qry_id uuid,
          pst_shr_txt text,
          pst_tag_frs set<text>,
          pst_typ text,
          pst_vid set<text>,
          PRIMARY KEY ((pst_crt_by, pst_tgt), pst_id)
      )
    `
  }
  
  /**
   * @param {text} usr_id - User id
   * @param {timestamp} crt_dt - Post Date
   * @param {text} pst_id - This will be topic/item id from video page
   * @param {uuid} cb_pst_id - This will be Career-Book Post id
   * @desc This table is used to store references of CBPost which is posted from Video Page Comment Section
   */
  ew_usr_vdo_cmnt_m(){
    let str = `
      CREATE TABLE ew_usr_vdo_cmnt_m (
        usr_id text,
        pst_id text,
        cb_pst_id uuid,
        crt_dt timestamp,
        PRIMARY KEY ((usr_id, pst_id))
      )
    `
  }
  
  /**
   * @param {text} usr_id - User id
   * @param {timestamp} crt_ts - Timestamp
   * @param {uuid} pst_id - This will be CBPost id
   * @param {text} like_typ - wow,sad,angry,liked,etc....
   * @desc This table is used to store like/dislike of post and user
   */
  ew_pst_like(){
    let str = `
      CREATE TABLE ew1.ew_pst_like (
        pst_id uuid,
        usr_id text,
        crt_ts timestamp,
        like_typ text,
        PRIMARY KEY (pst_id, usr_id)
      )
    `
  }
  
  /**
   * @param {uuid} pst_id - This will be CBPost id
   * @param {counter} angry_cnt - Angry Count
   * @param {counter} dlike_cnt - Dislike Count
   * @param {counter} haha_cnt - Haha Count
   * @param {counter} like_cnt - Like Count
   * @param {counter} love_cnt - Love Count
   * @param {counter} sad_cnt - Sad Count
   * @param {counter} wow_cnt - Wow Count
   * @desc This table is used to store various like type count of given post
   */
  ew_pst_like_stats(){
    let str = `
      CREATE TABLE ew1.ew_pst_like_stats (
        pst_id uuid,
        angry_cnt counter,
        dlike_cnt counter,
        haha_cnt counter,
        like_cnt counter,
        love_cnt counter,
        sad_cnt counter,
        wow_cnt counter,
        PRIMARY KEY (pst_id)
      )
    `
  }
}

