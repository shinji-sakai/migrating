var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videorating');

var ew_vdo_rating_tbl = "ew1.ew_vdo_rating";
var ew_vdo_avg_rating_tbl = "ew1.ew_vdo_avg_rating";

function VideoRatingApi() {

}

VideoRatingApi.prototype.saveVideoRating = async function (data) {
    try{
        var vdo_id = data.vdo_id;
        var usr_id = data.usr_id;
        var rating = data.rating;
        var crt_dt = new Date();
        var upd_dt = new Date();
        var qry = "insert into " + ew_vdo_rating_tbl + " (vdo_id,usr_id,rating,crt_dt,upd_dt) values (?,?,?,?,?)";
        var params = [vdo_id,usr_id,rating,crt_dt,upd_dt];

        var avg_qry = "update " + ew_vdo_avg_rating_tbl + " set vdo_rating=vdo_rating + " + rating + " , no_of_ratings=no_of_ratings + 1 where vdo_id=?";
        var avg_params = [vdo_id];

        await cassExecute(qry,params,{prepare:true});
        await cassExecute(avg_qry,avg_params,{prepare:true});

        return data;
    }catch(e){
        debug(e);
        throw e;
    }
}

VideoRatingApi.prototype.updateVideoRating = async function (data) {
    try{
        var vdo_id = data.vdo_id;
        var usr_id = data.usr_id;
        var rating = new Number(data.rating);
        var upd_dt = new Date();

        var qry = "update " + ew_vdo_rating_tbl + " set rating=?,upd_dt=? where usr_id=? and vdo_id=?";
        var params = [rating,upd_dt,usr_id,vdo_id];
        
        var avg_qry = "update " + ew_vdo_avg_rating_tbl + " set vdo_rating=vdo_rating + " + rating + " , no_of_ratings=no_of_ratings + 1 where vdo_id=?";
        var avg_params = [vdo_id];

        await cassExecute(qry,params,{prepare:true});
        await cassExecute(avg_qry,avg_params,{prepare:true});

        return data;
    }catch(e){
        debug(e);
        throw e;
    }
}

VideoRatingApi.prototype.getVideoRatingForUser = async function (data) {
     try{
        var vdo_id = data.vdo_id;
        var usr_id = data.usr_id;
        var qry = "select * from  " + ew_vdo_rating_tbl + "  where usr_id=? and vdo_id=?";
        var params = [usr_id,vdo_id];
        var res = await cassExecute(qry,params);
        res[0] = res[0] || {}
        //find avg rating
        var avg_qry = "select * from  " + ew_vdo_avg_rating_tbl + "  where vdo_id=?";
        var avg_params = [vdo_id];
        var res_avg_rating = await cassExecute(avg_qry,avg_params);  
        debug("res_avg_rating.......",res_avg_rating);
        var avg_rating = 0;
        if(res_avg_rating[0]){
            //total rating
            var total_rating = res_avg_rating[0].vdo_rating;
            //total number of rating
            var total_no_rating = res_avg_rating[0].no_of_ratings;

            avg_rating = total_rating/total_no_rating;
        }

        return {
            ...res[0],
            avg_rating
        };
    }catch(e){
        debug(e);
        throw e;
    }
}


module.exports = new VideoRatingApi();