var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videovotes');

var ew_vdo_vws_tbl = "ew1.ew_vdo_vws";
var ew_vdo_rating_tbl = "ew1.ew_vdo_rating";

function VideoVotesApi() {

}

//pass user id to get all video notes added by that user
VideoVotesApi.prototype.upvoteVideo = async function (data) {
    try{
        var usr_id = data.usr_id;
        var vdo_id = data.vdo_id;
        var upv_cnt = data.upv_cnt;
        var dwn_cnt = data.dwn_cnt;

        var set_str = "upv_cnt = upv_cnt + " + upv_cnt +  ",dwn_cnt = dwn_cnt + " + dwn_cnt;

        var qry = "update " + ew_vdo_vws_tbl + " set " + set_str  +  " where vdo_id=?";
        var params = [vdo_id];
        await cassExecute(qry,params);

        var qry_1 = "update " + ew_vdo_rating_tbl + " set upv=1,dnv=0  where vdo_id=? and usr_id=?";
        var params_1 = [vdo_id,usr_id];
        await cassExecute(qry_1,params_1);

        return data;
    }catch(e){
        debug(e);
        throw e;
    }
}

VideoVotesApi.prototype.downvoteVideo = async function (data) {
    try{
        var usr_id = data.usr_id;
        var vdo_id = data.vdo_id;
        var upv_cnt = data.upv_cnt;
        var dwn_cnt = data.dwn_cnt;

        var set_str = "upv_cnt = upv_cnt + " + upv_cnt +  ",dwn_cnt = dwn_cnt + " + dwn_cnt;

        var qry = "update " + ew_vdo_vws_tbl + " set " + set_str  +  " where vdo_id=?";
        var params = [vdo_id];
        await cassExecute(qry,params);

        var qry_1 = "update " + ew_vdo_rating_tbl + " set upv=0,dnv=1  where vdo_id=? and usr_id=?";
        var params_1 = [vdo_id,usr_id];
        await cassExecute(qry_1,params_1);

        return data;
    }catch(e){
        debug(e);
        throw e;
    }
}

// 
VideoVotesApi.prototype.getVideoVotes = async function (data) {
    try{
        var usr_id = data.usr_id;
        var vdo_id = data.vdo_id;

        var cnt_qry = "select  * from " + ew_vdo_vws_tbl + " where vdo_id=?";
        var cnt_params = [vdo_id];
        var cnt_res = await cassExecute(cnt_qry,cnt_params);
        cnt_res = cnt_res[0] || {};

        var bool_res = {};
        if(usr_id){
            var qry_1 = "select * from " + ew_vdo_rating_tbl + " where vdo_id=? and usr_id=?";
            var params_1 = [vdo_id,usr_id];
            bool_res = await cassExecute(qry_1,params_1);
            bool_res = bool_res[0] || {};    
        }
        

        var obj = {
            usr_id : usr_id,
            vdo_id : vdo_id,
            upv_cnt : cnt_res.upv_cnt, 
            dwn_cnt : cnt_res.dwn_cnt, 
            upv : bool_res.upv,
            dwn : bool_res.dnv,
        }

        return obj;
    }catch(e){
        debug(e);
        throw e;
    }
}

module.exports = new VideoVotesApi();