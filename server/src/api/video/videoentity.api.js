var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var shortId = require('shortid');
var courseApi = require(configPath.api.admin.course);
var logger = require(configPath.lib.log_to_file);
var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_last_vw_vdo_dt_table = "ew1.ew_last_vw_vdo_dt";

const RELATED_VIDEOS_FETCH_LIMIT = 3;

function VideoEntityApi() {

}

VideoEntityApi.prototype.updateVideoEntity = function(data) {
    return getConnection()
        .then(getCollection)
        .then(updateVideoEntity)
        .catch(globalFunctions.err);

    function updateVideoEntity(col) {
        if (!data.videoId) {
            data.videoId = shortId.generate()
        }
        return col.update({
            videoId: data.videoId
        }, data, {
            upsert: true
        });
    }
}
VideoEntityApi.prototype.removeVideoEntity = function(data) {
    return getConnection()
        .then(getCollection)
        .then(removeVideoEntity)
        .catch(globalFunctions.err);

    function removeVideoEntity(col) {
        return col.remove({
            videoId: data.videoId
        });
    }
}
VideoEntityApi.prototype.getAllVideoEntity = function(data) {
        return getConnection()
            .then(getCollection)
            .then(getAll)
            .catch(globalFunctions.err);

        function getAll(col) {
            return col.find({}).toArray();
        }
    }
    //
    //
VideoEntityApi.prototype.findVideoEntityByQuery = function(data) {
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if (data[key].length > 0) {
                data[key] = {
                    $in: data[key]
                }
            } else {
                delete data[key];
            }
        }
    }

    logger.debug(data);

    return getConnection()
        .then(getCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find(data).toArray();
    }
}

VideoEntityApi.prototype.findVideoEntityById = function(data) {
    return getConnection()
        .then(getCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({
            videoId: data.videoId
        }).toArray();
    }
}


VideoEntityApi.prototype.findVideoEntitiesById = function(data) {
    return getConnection()
        .then(getCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({
            videoId: {
                $in: data.videosId
            }
        }).toArray();
    }
}

VideoEntityApi.prototype.isVideoFilenameExist = async function(data) {
    try {
        var connection = await getConnection();
        var col = await getCollection(connection);
        var rows = await col.find({
            videoFilename: data.videoFilename
        }).toArray();
        if (rows.length > 0) {
            return {
                exist: true
            }
        } else {
            return {
                exist: false
            }
        }
    } catch (err) {
        throw err;
    }
}

VideoEntityApi.prototype.getRelatedVideosByVideoId = async function(data) {
    try {
        let {startIndex} = data;
        startIndex = startIndex || 0
        var connection = await getConnection();
        var collection = await getCollection(connection);
        var find_res = await collection.findOne({
            videoId: data.videoId
        });
        find_res = find_res || {};
        console.log(find_res)
        var relatedVideos = find_res["relatedVideos"] || [];
        if (relatedVideos.length > 0) {
            relatedVideos = relatedVideos.splice(startIndex,RELATED_VIDEOS_FETCH_LIMIT)
            var fullVideosInfo = await this.findVideoEntitiesById({
                videosId: relatedVideos
            });
            return (fullVideosInfo || []);
        } else {
            return [];
        }
    } catch (e) {
      console.log(e)
        throw e;
    }
}

VideoEntityApi.prototype.getRelatedTopicsVideoByVideoId = async function(data) {
    try {
        let {startIndex} = data;
        startIndex = startIndex || 0
        var connection = await getConnection();
        var collection = await getCollection(connection);
        var find_res = await collection.findOne({
            videoId: data.videoId
        });
        var relatedTopics = find_res["relatedTopics"] || [];
        if (relatedTopics.length > 0) {
            var fullVideosInfo = await this.findVideoEntityByQuery({
                topics: relatedTopics
            });
            fullVideosInfo = fullVideosInfo.splice(startIndex,RELATED_VIDEOS_FETCH_LIMIT)
            return (fullVideosInfo || []);
        } else {
            return [];
        }
    } catch (e) {
        throw e;
    }
}

VideoEntityApi.prototype.getRelatedCoursesByVideoId = async function(data) {
    try {
        var connection = await getConnection();
        var collection = await getCollection(connection);
        var find_res = await collection.findOne({
            videoId: data.videoId
        });
        var relatedCourses = find_res["relatedCourses"] || [];
        if (relatedCourses.length > 0) {
            var fullCourses = await courseApi.getCoursesById({
                ids: relatedCourses
            });
            return (fullCourses || []);
        } else {
            return [];
        }
    } catch (e) {
        throw e;
    }
}

VideoEntityApi.prototype.updateVideoLastViewTime = async function(data) {
    try {
        var {
            user_id,
            crs_id,
            mdl_id,
            topic_id,
            last_vw_time
        } = data;
        var query = `update ${ew_last_vw_vdo_dt_table} set last_vw_time=? where user_id=? and crs_id=? and mdl_id=? and topic_id=?`;
        var params = [new Date(last_vw_time), user_id, crs_id, mdl_id, topic_id];
        await cassExecute(query, params);
        return data;
    } catch (err) {
        logger.debug(err);
        throw new Error(err);
    }
}

VideoEntityApi.prototype.getVideoLastViewTime = async function(data) {
    try {
        var {
            user_id,
            crs_id
        } = data;
        var query = `select * from ${ew_last_vw_vdo_dt_table} where user_id=? and crs_id=?`;
        var params = [user_id, crs_id];
        var rows = await cassExecute(query, params);
        return rows;
    } catch (err) {
        logger.debug(err);
        throw new Error(err);
    }
}




function getCollection(db) {
    var col = db.collection('videoentity');
    return col;
}

module.exports = new VideoEntityApi();
