var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videoviews');
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var ew_vdo_vws_tbl = "ew1.ew_vdo_vws";

function VideoViewsApi() {

}

//pass user id to get all video notes added by that user
VideoViewsApi.prototype.incrementVideoViews = async function (data) {
    try{
        var vdo_id = data.vdo_id;
        var usr_id = data.usr_id;
        var qry = "update " + ew_vdo_vws_tbl + " set vw_num = vw_num + 1 where vdo_id=?";
        var params = [vdo_id];
        var res = await cassExecute(qry,params);

        try{
            //update video views in dashboard stats
            if(usr_id){
                await dashboardStatsAPI.incrementVideoViews({usr_id : usr_id});
            }    
        }catch(err){    
            debug(err)
        }
        

        return res;
    }catch(e){
        debug(e);
        throw e;
    }
}

VideoViewsApi.prototype.getVideoViews = async function (data) {
    try{
        var vdo_id = data.vdo_id;
        var qry = "select * from " + ew_vdo_vws_tbl + " where vdo_id=?";
        var params = [vdo_id];
        var res = await cassExecute(qry,params);
        return res;
    }catch(e){
        debug(e);
        throw e;
    }
}


module.exports = new VideoViewsApi();