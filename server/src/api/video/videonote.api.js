var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:video:videonotes');
var authorizeitemsAPI = require(configPath.api.authorizeitems);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);

var ew_vdo_note_mdls_tbl = "ew1.ew_vdo_note_mdls";
var ew_vdo_notes_tbl = "ew1.ew_vdo_notes";
var ew_vdo_notes_usr_tbl = "ew1.ew_vdo_notes_usr";


function VideoNoteApi() {

}

//deprecated
VideoNoteApi.prototype.updateModuleNotes = function (data) {

    var col;
    return getConnection()
        .then(removeData)
        .then(insertData)
        .catch(function (err) {
            console.log(err);
        });


    function removeData(db) {
        col = db.collection('videonotes');
        return col.updateOne({
            "moduleId": data.moduleId,
            "userId": data.userId
        }, {
            $set: {"moduleNotes": []}
        });
    }

    function insertData() {
        return col.updateOne(
            {
                "moduleId": data.moduleId,
                "userId": data.userId
            },
            {
                $set: {
                    "moduleNotes": data.moduleNotes,
                    "courseId": data.courseId,
                    "courseName":data.courseName,
                    "moduleNumber":data.moduleNumber,
                    "moduleName":data.moduleName,
                    lastUpdated:data.lastUpdated
                }
            }, {upsert: true});
    }
}

//deprecated
VideoNoteApi.prototype.getModuleNotes = function (data) {
    var col;
    return getConnection()
        .then(getModuleNotes)
        .catch(function (err) {
            console.log(err);
        });


    function getModuleNotes(db){
        col = db.collection('videonotes');
        return col.find({userId:data.userId, moduleId:data.moduleId},  {_id:0}).toArray();
    }
}


//deprecated
VideoNoteApi.prototype.getNotesModuleNames = function (data) {
    var col;
    return getConnection()
        .then(getNotesModuleNames)
        .catch(function (err) {
            console.log(err);
        });


    function getNotesModuleNames(db){
        col = db.collection('videonotes');
        return col.find({userId:data.userId, courseId:data.courseId, 'moduleNotes.videoNotes.0':{$exists:true} },  {moduleId:1,moduleNumber:1,moduleName:1,_id:0}).toArray();
    }
}

// Cassandra API
VideoNoteApi.prototype.getVideoNotesModules = async function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var crs_id = data.crs_id;
    var qry = "select * from " + ew_vdo_note_mdls_tbl + " where usr_id=? and crs_id=?";
    var params = [usr_id,crs_id];
    cassconn.execute(qry,params,(err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        var jsonArr = JSON.parse(JSON.stringify(res.rows || []));
        var modules =  jsonArr.map((v,i)=>{
            var obj = {
                mdl_id : v.mdl_id,
                mdl_nm : v.mdl_nm
            }
            return obj;
        })
        defer.resolve(modules || []);
    })
    return defer.promise;
}
VideoNoteApi.prototype.insertVideoNotesModule = async function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var crs_id = data.crs_id;
    var crs_nm = data.crs_nm;
    var mdl_id = data.mdl_id;
    var mdl_nm = data.mdl_nm;

    var qry = "insert into " + ew_vdo_note_mdls_tbl + " (usr_id,crs_id,crs_nm,mdl_id,mdl_nm) values (?,?,?,?,?)";
    var params = [usr_id,crs_id,crs_nm,mdl_id,mdl_nm];
    cassconn.execute(qry,params,(err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    })
    return defer.promise;
}
VideoNoteApi.prototype.deleteVideoNotesModules = function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var crs_id = data.crs_id;
    var mdl_id = data.mdl_id;
    var qry = "delete from " + ew_vdo_note_mdls_tbl + " where usr_id=? and crs_id=? and mdl_id=?";
    var params = [usr_id,crs_id,mdl_id];
    cassconn.execute(qry,params,(err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    })
    return defer.promise;
}


//pass module id to get all video notes
VideoNoteApi.prototype.getVideoNotesForModules = function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var qry = "select * from " + ew_vdo_notes_tbl + " where usr_id=? and mdl_id=?";
    var params = [usr_id,mdl_id];
    cassconn.execute(qry,params,(err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        var jsonArr = JSON.parse(JSON.stringify(res.rows || []));
        var videoNotes =  jsonArr.map((v,i)=>{
            var obj = {
                vdo_id : v.vdo_id,
                vdo_note_id : v.vdo_note_id,
                vdo_note : v.vdo_note,
                vdo_tm : v.vdo_tm,
                crt_dt : v.crt_dt,
            }
            return obj;
        })
        var result = {
            mdl_id : mdl_id,
            video_notes : videoNotes || []
        }
        defer.resolve(result || {});
    })
    return defer.promise;
}
VideoNoteApi.prototype.insertVideoNotes = async function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var crs_id = data.crs_id;
    var crs_nm = data.crs_nm;
    var mdl_id = data.mdl_id;
    var mdl_nm = data.mdl_nm;
    var vdo_id = data.vdo_id;
    var vdo_note_id = Uuid.random();
    var vdo_note = data.vdo_note;
    var vdo_tm = data.vdo_tm;
    var crt_dt = new Date();

    await this.insertVideoNotesModule(data);

    var qry = "insert into " + ew_vdo_notes_tbl + " (usr_id,crs_id,mdl_id,vdo_id,vdo_note_id,vdo_note,vdo_tm,crt_dt) values (?,?,?,?,?,?,?,?)" ;
    var params = [usr_id,crs_id,mdl_id,vdo_id,vdo_note_id,vdo_note,vdo_tm,crt_dt];

    cassconn.execute(qry,params,async (err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        data.vdo_note_id = vdo_note_id;
        data.crt_dt = crt_dt;

        try{
            await dashboardStatsAPI.incrementNotesCounter({usr_id : usr_id});    
        }catch(err){
            debug(err);
        }
        

        defer.resolve(data);
    })
    return defer.promise;
}

VideoNoteApi.prototype.updateVideoNote = function (data) {
    var defer = Q.defer();
    
    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var vdo_note_id = data.vdo_note_id;
    var vdo_note = data.vdo_note;

    var qry = "update " + ew_vdo_notes_tbl + " set vdo_note=? where usr_id=? and mdl_id=? and vdo_note_id =?";
    var params = [vdo_note,usr_id,mdl_id,vdo_note_id];
    cassconn.execute(qry,params,(err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        defer.resolve(data);
    })
    return defer.promise;
}

VideoNoteApi.prototype.deleteVideoNote = async function (data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var mdl_id = data.mdl_id;
    var vdo_note_id = data.vdo_note_id;

    var qry = "delete from " + ew_vdo_notes_tbl + " where usr_id=? and mdl_id=? and vdo_note_id =?";
    var params = [usr_id,mdl_id,vdo_note_id];
    cassconn.execute(qry,params,async (err,res) => {
        if(err){
            defer.reject(err);
            return;
        }
        //check if this is the last note in module
        //then delete module entry also
        var notesData = await this.getVideoNotesForModules(data);
        if(notesData["video_notes"].length <= 0){
            await this.deleteVideoNotesModules(data);
        }

        //decrement notes counter in dashboard stats 
        try{
            await dashboardStatsAPI.decrementNotesCounter({usr_id : usr_id});    
        }catch(err){
            debug(err);
        }

        defer.resolve(data);
    })
    return defer.promise;
}

//pass user id to get all video notes added by that user
VideoNoteApi.prototype.getVideoNotesOfUser = async function (data) {
    try{
        var usr_id = data.usr_id;
        var qry = "select * from " + ew_vdo_notes_usr_tbl + " where usr_id=?";
        var params = [usr_id];
        var res = await cassExecute(qry,params);

        //extract video id
        var video_ids = res.map((v,i)=>{
            return v.vdo_id
        });

        //all item info
        var item_res  = await authorizeitemsAPI.getItemsUsingItemId({itemId: video_ids});

        debug(item_res);

        //all item map id->object
        var item_map = {};
        item_res.forEach((v,i)=>{
            item_map[v.itemId] = v;
        });

        //attach iteminfo to response obj
        res = res.map((v,i)=>{
            var item_info = item_map[v.vdo_id];
            v["item_info"] = item_info;
            return v;
        });

        return res;
    }catch(e){
        debug(e);
        throw e;
    }
}



module.exports = new VideoNoteApi();