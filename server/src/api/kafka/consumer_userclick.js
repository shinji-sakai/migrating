var configPath = require('../../configPath.js');
var Q = require('q');
var debug = require('debug')('app:api:kafka:consumer_userclick');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var oracledb = require(configPath.dbconn.oracledb);
var Uuid = cassandra.types.Uuid;
var webSocket = require(configPath.api.websocket);
var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);
var pub = getRedisClient("publisher");

var userclick_table = 'ew1.usr_click_stats';
var oracle_userclick_table = 'usr_click_stats';

var groupId = "";
if(process.env.MODE != "DEV"){
    groupId = "click-group1";
}else{
    groupId = "click-group2";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),
    // rtdb_kafka = require('../../rtdb_kafka.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(
        client, [{
            topic: 'userclick'
        }], {
            groupId: groupId
        }
    );

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

console.log("Consumer UserClick started : ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
var user_click = {};
consumer.on('message', async (message)=> {
    logger.debug("In userclick Consumer",message);
    var columns = [];
    var values = [];
    var oracledb_values = [];

    var random_uuid = Uuid.random();

    columns.push("clk_id");
    values.push(random_uuid);
    oracledb_values.push('\''+random_uuid + '\'');

    if (message.value) {
        //convert to json
        var msg = JSON.parse(message.value);
        // logger.debug("msg........",msg);
        //get all parent keys
        var parentKeys = Object.keys(msg || {});
        //iterate through parent keys
        for (var i = 0; i < parentKeys.length; i++) {
            var key = parentKeys[i];
            // if (key === 'tkn')
            //     continue;
            var val = msg[key];
            columns.push(key);

            if (key === "clk_id") {
                values.push('' + val);
                oracledb_values.push('\''+clk_id+ '\'');
            } else {
                values.push('\'' + val + '\'');
                
                if(key === "login_tkn"){
                    oracledb_values.push('\''+val.substring(1,val.length - 1) +'\'');
                }else{
                    oracledb_values.push('\''+val+'\'');
                }
            }
        }
    }

    columns.push("offset");
    values.push('\'' + message.offset + '\'');
    oracledb_values.push('\''+message.offset+ '\'');

    columns.push("partition");
    values.push('\'' + message.partition + '\'');
    oracledb_values.push('\''+message.partition+ '\'');

    columns.push("key");
    values.push('\'' + message.key + '\'');
    oracledb_values.push('\''+message.key+ '\'');

    //convert column into (col1,col2,....)
    var column_str = '(' + columns.join(",") + ')';


    //convert value into ('val1','val2',......)
    var value_str = '(' + values.join(",") + ')';

    //convert oracledb_values into ('val1','val2',......)
    var oracledb_values_str = '(' + oracledb_values.join(",") + ')';

    try{
        // logger.debug("Userclick Event To Consumer");
        await insertIntoDb(column_str, value_str);
        await insertIntoOracleDb(column_str,oracledb_values_str);
        // logger.debug("Added to userclick table");
    }catch(err){
        logger.debug("error Adding to userclick table",err);
    }

    // insertIntoDb(column_str, value_str)
    //     .then(function(res) {
    //         debug("Added to userclick table");
    //     })
    //     .catch(function(err) {
    //         debug(err);
    //     })

    try{
        var clicks = await pub.getAsync("noOfClicks");
        if(clicks){
            clicks = parseInt(clicks);
            clicks++;
            pub.setAsync("noOfClicks",clicks);
        }else{
            pub.setAsync("noOfClicks",1);
        }
    }catch(err){
        logger.debug("Redis userclick consumer error ...",err)
    }

    try{
        pub.publish("click-stats",message.value)
        let msg = JSON.parse(message.value);
        if(msg.usr_id){
            pub.publish("click-stats-" + msg.usr_id,message.value)
        }else{
            pub.publish("click-stats-ew-anon",message.value)
        }
    }catch(err){
        logger.debug("click-stats redis error in clickstats",err);
    }

    //adding into rethink db

    var jsonMsg = JSON.parse(message.value);
    try {
        var user = jsonMsg["usr_id"];
        if(!user){
            return;
        }
        if (user_click[user] >= 0) {
            var clicks = user_click[user]++;
            var obj = {
                name: user,
                clickcount: clicks
            };
            webSocket.sendClickStatsOfUser(user, obj);
            // webSocket.sendMsgFromRethinkDB("message", obj);

            // rtdb_kafka.update(user, clicks);
        } else {
            var clicks = 0;
            user_click[user] = clicks++;
            var obj = {
                name: user,
                clickcount: clicks
            };
            webSocket.sendClickStatsOfUser(user, obj);
            // webSocket.sendMsgFromRethinkDB("message", obj);
            // rtdb_kafka.fetch(user, function(clicks) {
            //     user_click[user] = clicks++;
            //     var obj = {
            //         name: user,
            //         clickcount: clicks
            //     };
            //     webSocket.sendClickStatsOfUser(user, obj);
            //     webSocket.sendMsgFromRethinkDB("message", obj);
            // });
        }
    } catch (e) {
        debug(e);
    }
});


function insertIntoDb(columns, value) {
    var query = "insert into " + userclick_table + " " + columns + " values " + value;
    var defer = Q.defer();
    cassconn.execute(query, null, function(err, res) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        // logger.debug("userclick into cass....",res);
        defer.resolve({});
    });
    return defer.promise;
}


async function insertIntoOracleDb(columns, value) {
    try {
        //example query
        // var query = "insert into usr_click_stats (clk_id,link_nm,tag_typ,curr_pg,go_to_pg,login_tkn,usr_id,clk_tm,offset,partition,key) values ('24139de4-458e-4beb-9e7d-6dc1ec31af84','IT  Courses','A','http://localhost/','/about-it-training','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJzYXJqdSIsImlhdCI6MTQ4NDU1MDQ3MCwiZXhwIjoxNDg1MTU1MjcwfQ.hWA9uaJIKEE_x8RySEMWvl6-wnzzULApQeMXRTNj1QM','sarju','2017-01-17T13:58:36.678Z','10393','0','-1')";
        var query = "insert into " + oracle_userclick_table + " " + columns + " values " + value;
        // logger.debug(query);
        var conn = await oracledb.getConnection();
        var res = await conn.execute(query,[],{autoCommit: true});
        await oracledb.releaseConnection(conn);
        // logger.debug("userclick insertIntoOracleDb......", res);
        return;
    } catch (err) {
        logger.debug(err);
        throw err;
    }
}