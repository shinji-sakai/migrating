var configPath = require('../../configPath.js');
var Q = require('q');
var debug = require('debug')('app:api:kafka:consumer_useridle');
var cassconn = require(configPath.dbconn.cassconn);
var oracledb = require(configPath.dbconn.oracledb);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var useridle_table = 'ew1.usr_idle_stats';
var oracle_useridle_table = 'usr_idle_stats';

var groupId = "";
if(process.env.MODE != "DEV"){
    groupId = 'my-group3'
}else{
    groupId = "idle-group2";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(
        client,
        [
            { topic: 'useridle' }
        ],
        {
            groupId: groupId
        }
    );

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

consumer.on('message',async  function (message) {

    // console.log(message);
    var columns = [];
    var values = [];
    var oracledb_values = [];
    var random_uuid = Uuid.random();

    columns.push("idl_id");
    values.push(random_uuid);
    oracledb_values.push("'"+random_uuid+"'");

    if(message.value){
        //convert to json
        var msg = JSON.parse(message.value);
        //get all parent keys
        var parentKeys = Object.keys(msg || {});
        //iterate through parent keys
        for (var i = 0; i < parentKeys.length; i++) {
            var key = parentKeys[i];
            var val = msg[key];
            columns.push(key);
            if(key === "clk_id"){
                values.push(''+val);  
                oracledb_values.push("'"+val+"'");  
            }else{
                values.push('\''+val + '\'');
                if(key === "login_tkn"){
                    oracledb_values.push('\''+val.substring(1,val.length - 1) +'\'');
                }else{
                    oracledb_values.push('\''+val+'\'');
                }
            }
        }
    }

    columns.push("offset");
    values.push('\''+message.offset+'\'');
    oracledb_values.push("'"+message.offset+"'");

    columns.push("partition");
    values.push('\''+message.partition+'\'');
    oracledb_values.push("'"+message.partition+"'");

    columns.push("key");
    values.push('\''+message.key+'\'');
    oracledb_values.push("'"+message.key+"'");

    //convert column into (col1,col2,....)
    var column_str = '(' + columns.join(",") + ')';
    

    //convert value into ('val1','val2',......)
    var value_str = '(' + values.join(",") + ')';

    var oracledb_values_str = '(' + oracledb_values.join(",") + ')';

    try{
        await insertIntoDb(column_str,value_str)
        await insertIntoOracleDb(column_str,oracledb_values_str);
        debug("Added to useridle table");
    }catch(err){
        debug("error Adding to useridle table",err);
    }
    
    
        // .then(function(res){
        //     debug("Added to useridle table");
        // })
        // .catch(function(err){
        //     debug(err);
        // })
});

function insertIntoDb(columns,value){
    var query = "insert into " + useridle_table + " " + columns + " values " + value;
    var defer = Q.defer();
    cassconn.execute(query,null,function(err, res) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve({});
    });
    return defer.promise;
}

async function insertIntoOracleDb(columns, value) {
    try {
        // var query = 'insert into usr_click_stats (clk_id,link_nm,tag_typ,curr_pg,go_to_pg,login_tkn,usr_id,clk_tm,offset,partition,key) values ("24139de4-458e-4beb-9e7d-6dc1ec31af84","IT  Courses","A","http://localhost/","/about-it-training","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJzYXJqdSIsImlhdCI6MTQ4NDU1MDQ3MCwiZXhwIjoxNDg1MTU1MjcwfQ.hWA9uaJIKEE_x8RySEMWvl6-wnzzULApQeMXRTNj1QM,"sarju","2017-01-17T13:58:36.678Z","10393","0","-1")'
        var query = "insert into " + oracle_useridle_table + " " + columns + " values " + value;
        debug(query);
        var conn = await oracledb.getConnection();
        var res = await conn.execute(query,[],{autoCommit: true});
        await oracledb.releaseConnection(conn);
        debug("insertIntoOracleDb......", res);
        return;
    } catch (err) {
        debug(err);
        throw err;
    }
}
