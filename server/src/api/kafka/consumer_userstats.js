var configPath = require('../../configPath.js');
var Q = require('q');
var debug = require('debug')('app:api:kafka:consumer_userstats');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var oracledb = require(configPath.dbconn.oracledb);
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);

var getRedisClient = require(configPath.dbconn.redisconn);
var pub = getRedisClient("publisher");

var userstats_table = 'ew1.usr_pg_stats';
var oracle_userstats_table = 'usr_pg_stats';

var groupId = "";
if(process.env.MODE != "DEV"){
    groupId = 'my-group1';
}else{
    groupId = "userstats-group2";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(
        client,
        [
            { topic: 'userstats' }
        ],
        {
            groupId: groupId
        }
    );

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

console.log("Consumer UserStats Started ZOOKEEPER_KEY....",config.ZOOKEEPER_KEY);
consumer.on('message', async function (message) {
    // logger.debug("In userstats Consumer");
    console.log(message);
    var columns = [];

    columns = ["pg_vw_id","browser_desc","browser_nm","browser_ver","city","country","ctry_cd","curr_pg","device_pixel_ratio","flash_install","geo_loc_info","hostname","ip","ip_gather_status","isp","key","loc","login_tkn","offset","org","os_archi","os_family","os_ver","partition","pg_vw_tm","prev_pg","region","res_height","res_width","tkn","tm_zone","usr_id","zip_cd","express_sess"];

    var values = [];
    var values_mapping = {};
    var oracledb_values = [];
    var oracledb_values_mapping = {};
    var random_uuid = Uuid.random();

    // columns.push("pg_vw_id");
    values.push(random_uuid);
    values_mapping["pg_vw_id"] = random_uuid;
    oracledb_values_mapping["pg_vw_id"] = '\''+random_uuid+'\'';
    oracledb_values.push('\''+random_uuid+'\'');

    if(message.value){
        //convert to json
        var msg = JSON.parse(message.value);
        
        if(msg.typ === "channel"){
            return;
        }

        //get all parent keys
        var parentKeys = Object.keys(msg || {});
        //iterate through parent keys
        for (var i = 0; i < parentKeys.length; i++) {
            var key = parentKeys[i];
            var val = msg[key];

            if(typeof val === "object"){
                //parent key value is object
                var childObject = val;
                //get all child keys
                var childKeys = Object.keys(childObject || {});
                //concat into columns array
                // columns = columns.concat(childKeys);
                //push all columns(childkeys) value
                childKeys.forEach((k,i)=>{
                    var v = childObject[k];
                    values.push(''+v);  
                    values_mapping[k] = v || '';
                    oracledb_values.push('\''+v + '\'');  
                    oracledb_values_mapping[k] = '\''+v + '\'';
                })
            }else {
                //if parent key value is other than object
                // columns.push(key);
                values.push(''+val);
                values_mapping[key] = val || '';
                
                if(key === "login_tkn"){
                    oracledb_values.push('\''+val.substring(1,val.length - 1) +'\'');
                    oracledb_values_mapping[key] = '\''+val.substring(1,val.length - 1) +'\'';
                }else{
                    oracledb_values.push('\''+val+'\'');
                    oracledb_values_mapping[key] = '\''+val+'\'';
                }
            }
        }
    }

    // columns.push("offset");
    values.push(''+message.offset);
    values_mapping["offset"] = message.offset;
    oracledb_values.push('\''+message.offset+'\'');
    oracledb_values_mapping["offset"] = '\''+message.offset+'\'';

    // columns.push("partition");
    values.push(''+message.partition);
    values_mapping["partition"] = message.partition;
    oracledb_values.push('\''+message.partition+'\'');
    oracledb_values_mapping["partition"] = '\''+message.partition+'\'';

    // columns.push("key");
    values.push(''+message.key);
    values_mapping["key"] = message.key;
    oracledb_values.push('\''+message.key+'\'');
    oracledb_values_mapping["key"] = '\''+message.key+'\'';

    //convert column into (col1,col2,....)
    var column_str = '(' + columns.join(",") + ')';
    logger.debug(column_str);

    var oracledb_values_str = '(';

    //convert value into ('val1','val2',......)

    var value_str = '(';
    columns.forEach((v,i)=>{
        if(i===0){
            value_str += '' +  (values_mapping[v] || '') + ',';   
            oracledb_values_str += '' +  (oracledb_values_mapping[v] || "'-'") + ',';
        }else{
            value_str += '\'' + (values_mapping[v] || '') + '\',';   
            oracledb_values_str += (oracledb_values_mapping[v] || "'-'") + ',';
        }
    })
    value_str = value_str.substring(0,value_str.length - 1);
    oracledb_values_str = oracledb_values_str.substring(0,oracledb_values_str.length-1);
    value_str += ')';
    oracledb_values_str += ')';

    try{
        pub.publish("page-stats",JSON.stringify(values_mapping))
        let msg = JSON.parse(message.value);
        if(msg.usr_id){
            pub.publish("page-stats-" + msg.usr_id,JSON.stringify(values_mapping))
        }else{
            pub.publish("page-stats-ew-anon",JSON.stringify(values_mapping))
        }
    }catch(err){
        logger.debug("page-stats redis error in pagestats",err);
    }

    try{
        // logger.debug("UserStats Event To Consumer");
        await insertIntoDb(column_str,value_str);
        await insertIntoOracleDb(column_str,oracledb_values_str);
        logger.debug("Added to userstats");
    }catch(err){
        logger.debug("Error in Adding to userstats",err);
    }

    

    // insertIntoDb(column_str,value_str)
    //     .then(function(res){
    //         debug("Added to userstats");
    //     })
    //     .catch(function(err){
    //         debug(err);
    //     })

    
});

function insertIntoDb(columns,value){
    var query = "insert into " + userstats_table + " " + columns + " values " + value;
    
    var defer = Q.defer();
    cassconn.execute(query,null,function(err, res) {
        if (err) {
            debug(err);
            defer.reject(err);
            return;
        }
        defer.resolve({});
    });
    return defer.promise;
}

async function insertIntoOracleDb(columns,value){
    try{
        //example query: 
        var query = "insert into " + oracle_userstats_table + " " + columns + " values " + value;
        // logger.debug(query);
        var conn = await oracledb.getConnection();
        var res = await conn.execute(query,[],{autoCommit: true});
        await oracledb.releaseConnection(conn);
        // logger.debug("userstats insertIntoOracleDb......",res);
        return;
    }catch(err){
        logger.debug("insertIntoOracleDb error ..."+err);
        throw err;
    }
}
