var configPath = require('../../configPath.js');
var Q = require('q');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var oracledb = require(configPath.dbconn.oracledb);
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);
var pub = getRedisClient("publisher");

var groupId = "";
if (process.env.MODE != "DEV") {
    groupId = "test-perf-group-prod";
} else {
    groupId = "test-perf-group-dev";
}

var kafka = require('kafka-node'),
    config = require('../../config.js'),
    // rtdb_kafka = require('../../rtdb_kafka.js'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(
        client, [{
            topic: 'test-perf3'
        }], {
            groupId: groupId
        }
    );

process.on('SIGINT', function () {
    consumer.close(true, function () {});
});

console.log("Consumer Test Performance started : ZOOKEEPER_KEY....", config.ZOOKEEPER_KEY);
var user_click = {};
consumer.on('message', async function(message) {
    try {
        logger.debug(message.value);
        try {
            pub.incrAsync("total_count");
        } catch (err) {
            logger.debug("Redis start-test-performance error ...", err)
        }
    } catch (err) {
        logger.debug("Redis userclick consumer error ...", err)
    }
});