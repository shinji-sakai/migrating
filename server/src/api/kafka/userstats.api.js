var kafka = require('kafka-node');
var config = require('../../config.js');
var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var logger = require(configPath.lib.log_to_file);
var getRedisClient = require(configPath.dbconn.redisconn);

function UserStats() {
    console.log("Producer Started ZOOKEEPER_KEY....",config.ZOOKEEPER_KEY)
}

UserStats.prototype.sendStats = function (data) {
    if (!this.producer) {
        var Producer = kafka.Producer,
            KeyedMessage = kafka.KeyedMessage,
            client = new kafka.Client(config.ZOOKEEPER_KEY),
            km = new KeyedMessage('key', 'message');
        this.producer = new Producer(client);

        var payloads = [
            {topic: data.topic, messages: data.stats, partition: 0}
        ];
        this.producer.on('ready', () => {
            this.producer.send(payloads, (err, data)=> {
                console.log(data);
            });
        });

        this.producer.on('error', function (err) {
          console.log('error', err);
        });
    }
    else{
        var payloads = [
            {topic: data.topic, messages: data.stats, partition: 0}
        ];
        this.producer.send(payloads, (err, data) => {
            console.log(data);
        });
    }
}

UserStats.prototype.shouldFetchIdleEvents = async function(data) {
    // data = {page_url : "" }
    try{
        var page_url = data.page_url;
        //get mongodb connetion
        var connection = await getConnection();
        //get collection
        var col = await connection.collection('idle_evt_pages');
        //get all idle events page urlsro
        var rows = await col.find({}).toArray();   
        var found = false;
        for (var i = 0; i < rows.length; i++) {
            //take pattern from mongo
            var pattern = rows[i].page_url;
            //check pattern with page url
            found = pattern.test(page_url)
            if(found)
                break;
         } 
         return {shouldFetch : found};
    }catch(err){
        logger.debug("shouldFetchIdleEvents...",err);
        throw new Error(err);
    }
}

UserStats.prototype.testPerformance = function() {
    var sub = getRedisClient("subscriber");
    sub.subscribe("start-test-performance");
    sub.on('message', (channel, socketid) => {
        let i = 0;
        let intervalId = setInterval(()=>{
            i++;
            if(i > 100000){
                clearInterval(intervalId);
            }
            var obj = {
                topic : 'test-perf3',
                stats : JSON.stringify({
                    number : i
                })
            }
            this.sendStats(obj);    
        })
    });
}

// var t = new UserStats();
// t.testPerformance();

module.exports = new UserStats();