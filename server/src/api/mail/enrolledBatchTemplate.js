var EmailTemplate = require('email-templates').EmailTemplate
var path = require('path');
var Q = require('q');
var Promise = require("bluebird");

var templateDir = path.join(__dirname,'../../../views/email-templates', 'enrolled-batch');
var template = new EmailTemplate(templateDir)

async function getTemplate(data) {
	try{
		var email_data = {
		    dsp_nm: data.dsp_nm,
		    usr_id : data.usr_id,
		    training : data.training,
		    cls_start_dt : data.cls_start_dt,
			cls_frm_tm : data.cls_frm_tm,
			frm_wk_dy : data.frm_wk_dy,
			to_wk_dy : data.to_wk_dy
		}
		var defer = Q.defer();
		template.render({data : email_data},function (err, result) {
			if(err){
				defer.reject(err);
				return;
			}
			defer.resolve(result);
		})
		return defer.promise;
	}catch(err){
		throw err;
	}
}

module.exports = getTemplate;
