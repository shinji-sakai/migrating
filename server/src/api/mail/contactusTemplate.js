var EmailTemplate = require('email-templates').EmailTemplate
var path = require('path');
var Q = require('q');
var Promise = require("bluebird");

var templateDir = path.join(__dirname,'../../../views/email-templates', 'contactus');
var template = new EmailTemplate(templateDir)

async function getTemplate(data) {
	try{
		var data = {
		    name: data.name,
		    email: data.email,
		    ph_no : data.ph_no,
		    message : data.message 
		}
		var defer = Q.defer();
		template.render(data,function (err, result) {
			if(err){
				defer.reject(err);
				return;
			}
			defer.resolve(result);
		})
		return defer.promise;
	}catch(err){
		throw err;
	}
}

module.exports = getTemplate;
