var EmailTemplate = require('email-templates').EmailTemplate
var path = require('path');
var Q = require('q');
var Promise = require("bluebird");

var templateDir = path.join(__dirname,'../../../views/email-templates', 'enroll-btn');
var template = new EmailTemplate(templateDir)

async function getTemplate(data) {
	try{
		var user = {
		    username: data.username,
		    training : data.training,
		    batch : data.batch
		}
		var defer = Q.defer();
		template.render(user,function (err, result) {
			if(err){
				defer.reject(err);
				return;
			}
			defer.resolve(result);
		})
		return defer.promise;
	}catch(err){
		throw err;
	}
}

module.exports = getTemplate;
