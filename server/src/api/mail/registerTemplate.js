var EmailTemplate = require('email-templates').EmailTemplate
var path = require('path');
var Q = require('q');
var Promise = require("bluebird");

var templateDir = path.join(__dirname,'../../../views/email-templates', 'register');
var register = new EmailTemplate(templateDir)

async function getTemplate(data) {
	try{
		var user = {
		    username: data.username,
		    otp: data.otp,
		    usr_ph : data.usr_ph,
		    usr_email : data.usr_email,
		    dsp_nm : data.dsp_nm,
		}
		var defer = Q.defer();
		register.render(user,function (err, result) {
			if(err){
				defer.reject(err);
				return;
			}
			defer.resolve(result);
		})
		return defer.promise;
	}catch(err){
		throw err;
	}
}

module.exports = getTemplate;
