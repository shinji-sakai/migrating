var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var shortid = require('shortid');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;

var Q=require('q');

var subscribe_table = "ew1.ew_subs";
var unsubscribe_table = "ew1.ew_unsubs";

function API(){}


API.prototype.subscribeUser = async function(data){
	try{
		console.log(data);
		var subs_id = Uuid.random();
		var usr_id = data.usr_id;
		var subs_email = data.subs_email;
		var subs_typ = data.subs_typ;
		var subs_dt = new Date();

		var qry = "insert into " + subscribe_table + " (subs_id,usr_id,subs_email,subs_typ,subs_dt) values (?,?,?,?,?)";
		var params = [subs_id,usr_id,subs_email,subs_typ,subs_dt];

		await cassExecute(qry,params);

		return {...data,subs_id};
	}catch(err){
		throw err;
	}
	
	// return getConnection()
	// 		.then(dbCollection)
	// 		.then(update)
	// 		.catch(globalFunctions.err);



	// function update(col){
	// 	return col.update({usr_email:data.usr_email},data,{upsert:true});
	// }
}

API.prototype.unsubscribeUser = async function(data){
	try{
		var usr_id = data.usr_id;
		var subs_email = data.subs_email;
		var unsubs_id = Uuid.random();
		var unsubs_dt = new Date();

		var del_qry = "delete from " + subscribe_table + " where subs_email=?";
		var del_params = [subs_email];

		var qry = "insert into " + unsubscribe_table + " (unsubs_id,usr_id,subs_email,unsubs_dt) values (?,?,?,?)";
		var params = [unsubs_id,usr_id,subs_email,unsubs_dt];

		var queries = [{
			query : del_qry,
			params : del_params
		},{
			query : qry,
			params : params
		}]

		await cassBatch(queries);
		return {...data,unsubs_id}
	}catch(err){
		console.log(err);
		throw err;
	}
	// return getConnection()
	// 		.then(dbCollection)
	// 		.then(update)
	// 		.catch(globalFunctions.err);
	

	// function update(col){
	// 	return col.remove({usr_email:data.usr_email});
	// }
}

API.prototype.getAllSubscribers = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}

function dbCollection(db){
	return db.collection("subscribe");
}

module.exports = new API();