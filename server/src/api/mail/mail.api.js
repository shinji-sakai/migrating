var nodemailer = require('nodemailer');
var promisify = require("promisify-node");
var subscribeAPI = require('./subscribe.api.js');
var debug = require('debug')("app:api:auth:cass");
var buyTrainingTemplate = require('./buyTrainingTemplate.js');
var enrolledBatchTemplate = require('./enrolledBatchTemplate.js')
var mail_config = require('./mailConfig.js');
var smtpTransport = nodemailer.createTransport("SMTP",{
    host: "smtp.gmail.com", // hostname
    secureConnection: true, // use SSL
    port: 465,
    auth: {
        user: "support@examwarrior.com",
        pass: "dataxyz123"
    }
});

function Mail(){

}

Mail.prototype.sendMail = function(data){
	var mailOptions = {
	    from: 'support@examwarrior.com',
	    to: data.to,
	    subject: data.subject,
	    html: data.text
	}

	var sendMail = promisify(smtpTransport.sendMail);

	return sendMail(mailOptions);
}


Mail.prototype.sendBulkEmails = async function(data){
	try{
		var body = data.mail_body;
		var subject = data.mail_subject;

		var subscribes = await subscribeAPI.getAllSubscribers();
		subscribes = subscribes || [];
		var emails  = subscribes.map((v,i)=>v.usr_email) || [];
		var email_str = emails.join(",");

		var d_obj = {
			text : body,
			subject : subject,
			to : email_str
		}

		await this.sendMail(d_obj);
		return emails;
	}catch(err){
		debug(err);
		throw err;
	}
}

Mail.prototype.sendSuccessfullBuyTrainingMail = async function(data){
	try{
		var dsp_nm = data.dsp_nm;
		var training = data.training;
		var to = data.to;

		var d = await buyTrainingTemplate(data);

		var mail_data_obj = {
			text : d.html,
			subject : mail_config.BUY_TRAINING_MAIL_SUBJECT,
			to : data.to
		}

		await this.sendMail(mail_data_obj);
		return {};
	}catch(err){
		debug(err);
		throw err;
	}
}


Mail.prototype.sendEnrolledBatchMail = async function(data){
	try{
		var to = data.to;

		var d = await enrolledBatchTemplate(data);

		var mail_data_obj = {
			text : d.html,
			subject : mail_config.ENROLLED_BATCH_MAIL_SUBJECT,
			to : data.to
		}

		await this.sendMail(mail_data_obj);
		return {};
	}catch(err){
		debug(err);
		throw err;
	}
}






module.exports = new Mail();