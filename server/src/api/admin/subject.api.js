var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function Subject(){

}
Subject.prototype.getAll = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}
Subject.prototype.update = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	async function update(col){
		let obj = await col.findOne({subjectId:data.subjectId});
		
		if(!obj){
            data.create_dt = new Date();
            data.update_dt = new Date();
        }else{
            data.update_dt = new Date()
        }

		if(!data.subjectId){
			data.subjectId = shortid.generate();
		}
		return col.update({subjectId:data.subjectId},{$set  : data},{upsert:true});
	}
}
Subject.prototype.fetchSubjectMasterUsingLimit = async function(data) {
    try{
        let { no_of_item_to_fetch , no_of_item_to_skip } = data;
        if(typeof no_of_item_to_fetch === "undefined"){
            no_of_item_to_fetch = 5;
        }
        if(typeof no_of_item_to_skip === "undefined"){
            no_of_item_to_skip = 0;
        }
        console.log(no_of_item_to_fetch , no_of_item_to_skip);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();
        return find_res
    }catch(err){
        throw new Error(err)
    }
}
Subject.prototype.remove = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({subjectId:data.subjectId});
	}
}
Subject.prototype.SubjectIdIsExits = async function(data)
{
	try{
		let{Id}=data;
		var connection = await getConnection();
		var collection= await dbCollection(connection);
		var find_res= await collection.find({subjectId:Id}).toArray();
		return find_res;
	}catch(err){
		throw new Error(err);
	}
}
function dbCollection(db){
	return db.collection("subject");
}

module.exports = new Subject();