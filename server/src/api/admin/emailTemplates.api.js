var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batTimingApi = require(configPath.api.batchCourse.batchTiming);

var ew_training_bat_email_template_table = 'ew1.ew_training_bat_email_template';

function EmailTemplates() {

}

EmailTemplates.prototype.getAllEmailTemplates = async function(data){
	try{
		var query = "select * from " + ew_training_bat_email_template_table;
		var res = await cassExecute(query);
		//get all batch ids from all templates
		var allBatIds = res.map((v,i)=>v.bat_id);
		//get batch details for all batch id
		var allBatches = await batTimingApi.getTimingByBatId({bat_id : allBatIds});
		var allBatchesMap = {};
		//create map of all batches {bat_id -> batch_detail}
		allBatches.map((v,i)=>{
			allBatchesMap[v.bat_id] = v;
		})


		//attach batch details to correspondance template
		res = res.map((v,i)=>{
			v["batch_details"] = allBatchesMap[v.bat_id];
			return v;
		})


		// res = [{frm_nm , bat_id , training_id , email_sub , email_body , batch_details}]
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

EmailTemplates.prototype.getEmailTemplateById = async function(data){
	try{
		var pk_id = data.pk_id;
		if(!pk_id){
			return {
				err : 'pk_id is required'
			}
		}
		var query = "select * from " + ew_training_bat_email_template_table + " where pk_id = ? ";
		var params = [pk_id]
		var res = await cassExecute(query,params);
		//get all batch ids from all templates
		var allBatIds = res.map((v,i)=>v.bat_id);
		//get batch details for all batch id
		var allBatches = await batTimingApi.getTimingByBatId({bat_id : allBatIds});
		var allBatchesMap = {};
		//create map of all batches {bat_id -> batch_detail}
		allBatches.map((v,i)=>{
			allBatchesMap[v.bat_id] = v;
		})


		//attach batch details to correspondance template
		res = res.map((v,i)=>{
			v["batch_details"] = allBatchesMap[v.bat_id];
			return v;
		})
		return res[0];
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


EmailTemplates.prototype.addEmailTemplates = async function(data){
	// data = {frm_nm , bat_id , training_id , email_sub , email_body}
	try{
		var pk_id = data.pk_id || Uuid.random();
		var frm_nm = data.frm_nm;
		var bat_id = data.bat_id;
		var training_id = data.training_id;
		var email_sub = data.email_sub;
		var email_body = data.email_body;
		var crt_dt  = new Date();

		var query = "update " + ew_training_bat_email_template_table + " set frm_nm=? , bat_id=? , training_id=? , email_sub=? , email_body=? , crt_dt=? where pk_id =?  " ;
		// var query = "insert into " + ew_training_bat_email_template_table + " (pk_id,frm_nm , bat_id , training_id , email_sub , email_body , crt_dt) values (?,?,?,?,?,?,?) " ;
		var params = [frm_nm , bat_id , training_id , email_sub , email_body , crt_dt,pk_id];
		//execute query
		var res = await cassExecute(query,params);
		//get batch data for this template
		var batchData = await batTimingApi.getTimingByBatId({bat_id : bat_id});

		return {pk_id,...data,batch_details : batchData[0]};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

EmailTemplates.prototype.deleteEmailTemplate = async function(data){
	// data = {pk_id}
	try{
		var pk_id = data.pk_id;
		if(!pk_id){
			return {
				err : 'pk_id is required'
			}
		}
		var query = "delete from " + ew_training_bat_email_template_table + " where pk_id = ?" ;
		var params = [pk_id];
		var res = await cassExecute(query,params);
		return {pk_id};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


module.exports = new EmailTemplates();