var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function API(){

}
API.prototype.getAll = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}

API.prototype.get = function(data){
	if(!data.paper_id){
		return {
			error : "paper_id field is required"
		}
	}
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.findOne({paper_id : data.paper_id});
	}
}

API.prototype.update = async function(data){
	if(!data.paper_id){
		return {
			error : "paper_id field is required"
		}
	}
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	async function update(col){
		try{
			var alreadyExist = await col.findOne({paper_id:data.paper_id});
			if(alreadyExist){
				data.updatedAt = new Date()
			}else{
				data.createdAt = new Date()
			}
			await col.update({paper_id:data.paper_id},{"$set" : data},{upsert:true});
			return data
		}catch(err){
			console.log(err);
			throw new Error(err)
		}
	}
}

API.prototype.remove = async function(data){
	if(!data.paper_id){
		return {
			error : "paper_id field is required"
		}
	}
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({paper_id : data.paper_id});
	}
}

function dbCollection(db){
	return db.collection("previousPapers");
}

module.exports = new API();