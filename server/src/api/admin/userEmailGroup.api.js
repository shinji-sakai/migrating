var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batTimingApi = require(configPath.api.batchCourse.batchTiming);
var profileAPI = require(configPath.api.dashboard.profile);

var ew_training_bat_users_template_table = 'ew1.ew_training_bat_users_template';

function UserEmailGroup() {

}

UserEmailGroup.prototype.getAllUserEmailGroups = async function(data){
	try{
		var query = "select * from " + ew_training_bat_users_template_table;
		var res = await cassExecute(query);
		//get all batch ids from all templates
		var allBatIds = res.map((v,i)=>v.bat_id);
		//get batch details for all batch id
		var allBatches = await batTimingApi.getTimingByBatId({bat_id : allBatIds});
		var allBatchesMap = {};
		//create map of all batches {bat_id -> batch_detail}
		allBatches.map((v,i)=>{
			allBatchesMap[v.bat_id] = v;
		})

		//attach batch details to correspondance template
		res = res.map((v,i)=>{
			v["batch_details"] = allBatchesMap[v.bat_id];
			return v;
		})


		// res = [{frm_nm , bat_id , training_id , usrs , batch_details}]
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

UserEmailGroup.prototype.getUserEmailGroupById = async function(data){
	try{
		var pk_id = data.pk_id;
		if(!pk_id){
			return {
				err : 'pk_id is required'
			}
		}
		var query = "select * from " + ew_training_bat_users_template_table + " where pk_id = ? ";
		var params = [pk_id];
		var res = await cassExecute(query,params);
		res = res[0];

		if(res){
			var users = res.usrs;
			var users_data = await profileAPI.getUsersShortDetails({usr_ids : users});
			var users_email = users_data.map((v,i)=>v.usr_email);
			res["users_email"] = users_email;
			return res;
		}else{
			return {}
		}
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


UserEmailGroup.prototype.addUserEmailGroup = async function(data){
	// data = {frm_nm , bat_id , training_id , usrs}
	try{
		var pk_id = data.pk_id || Uuid.random();
		var frm_nm = data.frm_nm;
		var bat_id = data.bat_id;
		var training_id = data.training_id;
		var usrs = data.usrs;
		var crt_dt  = new Date();
		var query = "update " + ew_training_bat_users_template_table + " set frm_nm=? , bat_id=? , training_id=? , usrs=? , crt_dt=? where pk_id = ?" ;
		// var query = "insert into " + ew_training_bat_users_template_table + " (pk_id,frm_nm , bat_id , training_id , usrs , crt_dt) values (?,?,?,?,?,?) " ;
		var params = [frm_nm , bat_id , training_id , usrs , crt_dt,pk_id];
		//execute query
		var res = await cassExecute(query,params);
		//get batch data for this template
		var batchData = await batTimingApi.getTimingByBatId({bat_id : bat_id});

		return {pk_id,...data,batch_details : batchData[0]};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

UserEmailGroup.prototype.deleteUserEmailGroup = async function(data){
	// data = {pk_id}
	try{
		var pk_id = data.pk_id;
		if(!pk_id){
			return {
				err : 'pk_id is required'
			}
		}
		var query = "delete from " + ew_training_bat_users_template_table + " where pk_id = ?" ;
		var params = [pk_id];
		var res = await cassExecute(query,params);
		return {pk_id};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


module.exports = new UserEmailGroup();