var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var profileAPI = require(configPath.api.dashboard.profile);

var ew_usr_role_table = 'ew1.ew_usr_role';

function Roles() {

}

Roles.prototype.getAllUserRoles = async function(data){
	try{
		var query = "select * from " + ew_usr_role_table;
		var res = await cassExecute(query);

		var userSpecificRoles = {};
		res.map((record,i)=>{
			
			userSpecificRoles[record.usr_id] = userSpecificRoles[record.usr_id] || {};
			
			userSpecificRoles[record.usr_id]["usr_id"] = record["usr_id"];
			
			userSpecificRoles[record.usr_id]["roles"] = userSpecificRoles[record.usr_id]["roles"] || [];

			userSpecificRoles[record.usr_id]["roles"].push({
				role_id : record["role_id"],
				form_access : record["form_access"]
			}) 

		})

		var keys = Object.keys(userSpecificRoles || {});

		var resArray = keys.map((key,i)=>{
			return userSpecificRoles[key];
		})
		return resArray;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

Roles.prototype.getUserAdminRoleFormAccessList = async function(data){
	try{
		var usr_id = data.usr_id;
		var role_id = "admin";
		var query = "select * from " + ew_usr_role_table + " where usr_id = ? and role_id = ?";
		var params = [usr_id,role_id];
		var rows = await cassExecute(query,params);
		if(rows[0]){
			var row = rows[0];
			var form_access_obj = row["form_access"];
			var form_access_list = Object.keys(form_access_obj || {});
			return form_access_list;
		}else{
			return [];
		}
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

Roles.prototype.addUserRole = async function(data){
	try{
		var roles = data.roles;
		var usr_id = data.usr_id;

		var rolesForProfile = roles.map((v,i)=>{
			return v.role_id;
		})

		await this.deleteUserRole({usr_id : usr_id});

		var queries = roles.map((v,i)=>{
			var query = "insert into " + ew_usr_role_table + " (form_access,usr_id,role_id) values (?,?,?)"  ;
			var params = [v.form_access , usr_id ,v.role_id] ;
			logger.debug(query,params);
			return{
				query : query,
				params : params
			}
		});

		await profileAPI.updateUserRole({usr_id : usr_id , usr_role : rolesForProfile});

		//execute query
		var res = await cassBatch(queries,{prepare : true});
		return data;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

Roles.prototype.deleteUserRole = async function(data){
	try{
		var usr_id = data.usr_id;
		var query = "delete from " + ew_usr_role_table + " where usr_id = ?" ;
		var params = [usr_id];
		var res = await cassExecute(query,params);
		return {usr_id};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


module.exports = new Roles();