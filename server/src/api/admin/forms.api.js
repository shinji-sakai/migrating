var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_forms_m_table = 'ew1.ew_forms_m';

function Forms() {

}

Forms.prototype.getAllForms = async function(data){
	try{
		var query = "select * from " + ew_forms_m_table;
		var res = await cassExecute(query);
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

Forms.prototype.addForm = async function(data){
	try{
		var form_id = data.form_id;
		var form_name = data.form_name;
		var create_dt  = new Date();

		var query = "update " + ew_forms_m_table + " set form_name=? , create_dt=? where form_id = ?" ;
		var params = [form_name , create_dt , form_id];
		//execute query
		var res = await cassExecute(query,params);
		
		return {...data};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

Forms.prototype.deleteForm = async function(data){
	try{
		var form_id = data.form_id;
		var query = "delete from " + ew_forms_m_table + " where form_id = ?" ;
		var params = [form_id];
		var res = await cassExecute(query,params);
		return {form_id};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


module.exports = new Forms();