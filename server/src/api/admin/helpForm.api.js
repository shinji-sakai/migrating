var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function API(){}

API.prototype.getAllFormsHelp = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}

API.prototype.getFormHelpById = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		if(data.form_id){
			return col.findOne({form_id : data.form_id});
		}else{
			return col.find({}).toArray();
		}
	}
}

API.prototype.updateFormHelp = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	async function update(col){
		try{
			delete data["_id"];
			await col.update({form_id:data.form_id},data,{upsert:true});
			return data;	
		}catch(err){
			console.log(err);
			throw err;
		}
	}
}

API.prototype.removeFormHelp = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	async function remove(col){
		try{
			await col.remove({form_id:data.form_id});	
			return data;
		}catch(err){
			console.log(err);
			throw err;
		}
		
	}
}

function dbCollection(db){
	return db.collection("helpForm");
}

module.exports = new API();