var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function TopicGroup(){

}

TopicGroup.prototype.getAllTopicGroups = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		if(data.topicId){
			return col.find({topicId : data.topicId}).toArray();
		}else{
			return col.find({}).toArray();
		}
	}
}
TopicGroup.prototype.updateTopicGroup = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	async function update(col){

		if(!data.groupId){
			var tempRes = await col.find({topicId:data.topicId}).toArray();
			if(tempRes && tempRes.length > 0){
            	data.groupNumber = tempRes[tempRes.length - 1]["groupNumber"] + 1;
            }else{
            	data.groupNumber = 1;
            }
            data.groupId = data.topicId + "-" + data.groupNumber;
		}

		if(!data.groupId){
			data.groupId = shortid.generate();
		}
		return col.update({groupId:data.groupId},data,{upsert:true});
	}
}

TopicGroup.prototype.removeTopicGroup = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({groupId:data.groupId});
	}
}
TopicGroup.prototype.fetchTopicGroupUsingLimit = async function(data) {
    try{
        let { no_of_item_to_fetch , no_of_item_to_skip } = data;
        if(typeof no_of_item_to_fetch === "undefined"){
            no_of_item_to_fetch = 5;
        }
        if(typeof no_of_item_to_skip === "undefined"){
            no_of_item_to_skip = 0;
        }
        console.log(no_of_item_to_fetch , no_of_item_to_skip);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();
        return find_res
    }catch(err){
        throw new Error(err)
    }
}
function dbCollection(db){
	return db.collection("topicGroup");
}

module.exports = new TopicGroup();