var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function Author(){}


//Get All Authors 
Author.prototype.getAll = function(data){

	return getConnection()					// get mongodb connection
			.then(dbCollection)				// get db collection
			.then(find)						// find all author
			.catch(globalFunctions.err);	// handle error

	function find(col){
		//find all author from mongodb
		return col.find({}).toArray();
	}
}

//Update Author
Author.prototype.update = function(data){

	return getConnection()					// get mongodb connection
			.then(dbCollection)				// get db collection
			.then(update)					// update/insert author
			.catch(globalFunctions.err);	// handle error

	//Update author
	async function update(col){

		let obj = await col.findOne({authorId:data.authorId});
		
		if(!obj){
            data.create_dt = new Date();
            data.update_dt = new Date();
        }else{
            data.update_dt = new Date()
        }


		//if author id is not set 
		//then create new authorid
		if(!data.authorId){
			data.authorId = shortid.generate();
		}
		//update/insert author
		return col.update({authorId:data.authorId},{$set : data},{upsert:true});
	}
}

//remove author by id
Author.prototype.remove = function(data){
	return getConnection()					// get mongodb connection
			.then(dbCollection)				// get db collection
			.then(remove)					// remove author
			.catch(globalFunctions.err);	// handle error

	function remove(col){

		return col.remove({authorId:data.authorId});
	}
}
Author.prototype.fetchAuthorUsingLimit = async function(data) {
    try{
        let { no_of_item_to_fetch , no_of_item_to_skip } = data;
        if(typeof no_of_item_to_fetch === "undefined"){
            no_of_item_to_fetch = 5;
        }
        if(typeof no_of_item_to_skip === "undefined"){
            no_of_item_to_skip = 0;
        }
        console.log(no_of_item_to_fetch , no_of_item_to_skip);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();
        return find_res
    }catch(err){
        throw new Error(err)
    }
}
Author.prototype.AuthorsIdIsExits = async function(data)
{
	try{
		let{Id}=data;
		var connection = await getConnection();
		var collection= await dbCollection(connection);
		var find_res= await collection.find({authorId:Id}).toArray();
		return find_res;
	}catch(err){
		throw new Error(err);
	}
}
function dbCollection(db){
	return db.collection("tag");
}

function dbCollection(db){
	return db.collection("author");

	// author collection : {authorId , authorName}
}

module.exports = new Author();