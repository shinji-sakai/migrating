var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var profileAPI = require(configPath.api.dashboard.profile);

var ew_usr_noti_table = 'ew1.ew_usr_noti';
var ew_noti_table = "ew1.ew_noti";
var ew_usr_noti_cnt_table = "ew1.ew_usr_noti_cnt";

function SendNotification() {

}

SendNotification.prototype.getAllSendNotifications = async function(data){
	try{
		var query = "select * from " + ew_noti_table;
		var res = await cassExecute(query);
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendNotification.prototype.getSendNotificationById = async function(data){
	try{
		var usr_noti_pk = data.usr_noti_pk;
		var query = "select * from " + ew_noti + " where usr_noti_pk = ? ";
		var params = [usr_noti_pk];
		var res = await cassExecute(query,params);
		res = res[0] || {};
		return {};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


SendNotification.prototype.getSendNotificationByUserId = async function(data){
	try{
		var usr_id = data.usr_id;
		var query = "select * from " + ew_usr_noti_table + " where usr_id = ? ";
		var params = [usr_id];
		var res = await cassExecute(query,params);
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendNotification.prototype.markSendNotificationAsRead = async function(data){
	try{
		var usr_id = data.usr_id;
		var usr_noti_pk = data.usr_noti_pk;
		var query = "update " + ew_usr_noti_table + " set noti_read=? , noti_read_tm=? where usr_noti_pk = ? and usr_id = ?" ;
		var params = ["true" , new Date() ,  usr_noti_pk , usr_id];

		await this.decrementSendNotificationCount({usr_id : usr_id});
		var res = await cassExecute(query,params);
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

//
SendNotification.prototype.markAllSendNotificationAsRead = async function(data){
	try{
		var usr_id = data.usr_id;

		var query = "update " + ew_usr_noti_table + " set noti_read=? , noti_read_tm=? where usr_id = ?" ;
		var params = ["true" , new Date() , usr_id];

		await this.decrementSendNotificationCount({usr_id : usr_id});
		var res = await cassExecute(query,params);
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendNotification.prototype.addSendNotification = async function(data){
	try{
		var noti_id = Uuid.random();
		var sub = data.sub;
		var users = data.users;
		var noti_msg = data.noti_msg;
		var snd_dt  = new Date();

		var queries = users.map((user_id,i)=>{
			var query = "update " + ew_usr_noti_table + " set sub=? , noti_msg=? , snd_dt=? , noti_id = ? where usr_noti_pk = ? and usr_id = ?" ;
			var params = [sub  , noti_msg , snd_dt , noti_id , Uuid.random() , user_id];

			return {
				query : query,
				params : params
			}
		});

		for (var i = users.length - 1; i >= 0; i--) {
			var usr_id = users[i];
			await this.incrementSendNotificationCount({usr_id : usr_id});
		}

		//query to master table
		var query_m = "update " + ew_noti_table + " set sub=? , noti_msg=? , snd_dt=? where noti_id = ?" ;
		var params_m = [sub  , noti_msg , snd_dt , noti_id];

		queries.push({
			query : query_m,
			params : params_m
		});


		var res = await cassBatch(queries);

		return {...data};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendNotification.prototype.deleteSendNotification = async function(data){
	// data = {usr_noti_pk}
	try{
		var usr_noti_pk = data.usr_noti_pk;
		if(!usr_noti_pk){
			return {
				err : 'usr_noti_pk is required'
			}
		}
		var query = "delete from " + ew_usr_noti_table + " where usr_noti_pk = ?" ;
		var params = [usr_noti_pk];
		var res = await cassExecute(query,params);
		return {usr_noti_pk};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


SendNotification.prototype.getSendNotificationCount = async function(data){
	try{
		var usr_id = data.usr_id;
		//query to master table
		var query_m = "select * from " + ew_usr_noti_cnt_table + " where usr_id = ?" ;
		var params_m = [usr_id];
		var res = await cassExecute(query_m,params_m);
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendNotification.prototype.incrementSendNotificationCount = async function(data){
	try{
		var usr_id = data.usr_id;
		var type = data.type || "web";

		var query_m;
		if(type === "web"){
			query_m = "update " + ew_usr_noti_cnt_table + " set web_noti_cnt=web_noti_cnt+1 where usr_id = ?" ;
		}else if(type === "qa"){
			query_m = "update " + ew_usr_noti_cnt_table + " set qa_noti_cnt=qa_noti_cnt+1 where usr_id = ?" ;
		}else if(type === "cb"){
			query_m = "update " + ew_usr_noti_cnt_table + " set cb_noti_cnt=cb_noti_cnt+1 where usr_id = ?" ;
		}

		var params_m = [usr_id];

		var res = await cassExecute(query_m,params_m);

		return {...data};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendNotification.prototype.decrementSendNotificationCount = async function(data){
	try{
		var usr_id = data.usr_id;
		var type = data.type || "web";

		var query_m;
		if(type === "web"){
			query_m = "update " + ew_usr_noti_cnt_table + " set web_noti_cnt=web_noti_cnt-1 where usr_id = ?" ;
		}else if(type === "qa"){
			query_m = "update " + ew_usr_noti_cnt_table + " set qa_noti_cnt=qa_noti_cnt-1 where usr_id = ?" ;
		}else if(type === "cb"){
			query_m = "update " + ew_usr_noti_cnt_table + " set cb_noti_cnt=cb_noti_cnt-1 where usr_id = ?" ;
		}

		var params_m = [usr_id];

		var res = await cassExecute(query_m,params_m);

		return {...data};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

module.exports = new SendNotification();
