var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var Q=require('q');

/**
 * Represents a CourseModule.
 * @constructor
 */
function CourseModule(){}

/**
 * getall course modules for given course id
 * @param  {JSON} data {courseId : ''}
 * @return {Promise}
 */
CourseModule.prototype.getAll = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(getCM)
        .catch(globalFunctions.err);

    function getCM(col) {
        //find query and return promise 
        return col.find({courseId: data.courseId}).toArray();
    }
}

/**
 * add/update coursemodule
 * @param {JSON} data {moduleId:'',....}
 */
CourseModule.prototype.add = function (data) {
    return getConnection()
            .then(dbCollection)
            .then(function (col) {
                return col.updateOne({moduleId: data.moduleId}, data, {upsert: true, w: 1});
            })
            .catch(globalFunctions.err);
}

/**
 * delete coursemodule using moduleId
 * @param  {JSON} data {moduleId}
 * @return {Promise}
 */
CourseModule.prototype.delete = function (data) {
    var col;
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        return col.deleteOne(data);
    }
}

/**
 * delete coursemodules using courseId
 * @param  {JSON} data {courseId}
 * @return {Promise}
 */
CourseModule.prototype.deleteUsingCourseId = function (data) {
    var col;
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        return col.deleteOne({courseId: data.courseId});
    }
}

/**
 * update document details
 * @param  {JSON} data  {courseId:''}
 * @param  {JSON} query {
            $set : {
                courseName : data.courseName,
            }
        }
 * @return {Promise}
 */
CourseModule.prototype.updateUsingQuery = function (data,query) {
    return getConnection()
        .then(dbCollection)
        .then(update)
        .catch(globalFunctions.err);
    function update(col) {
        return col.update(data,query);
    }
}

function dbCollection(db){
    return db.collection("coursemodule");
}

module.exports = new CourseModule();