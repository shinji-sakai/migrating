var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function JobOpenings(){}

JobOpenings.prototype.getAllJobOpenings = function(data){
	return getConnection()							
			.then(dbCollection)						
			.then(find)								
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}

//Update/insert jobOpening by id
JobOpenings.prototype.updateJobOpening = function(data){
	return getConnection()							
			.then(dbCollection)						
			.then(update)							 
			.catch(globalFunctions.err);			

	function update(col){
		if(!data.jobOpeningId){
			data.jobOpeningId = shortid.generate();
		}
		return col.update({jobOpeningId:data.jobOpeningId},data,{upsert:true});
	}
}

//Remove JobOpening By id
JobOpenings.prototype.removeJobOpening = function(data){
	return getConnection()							// get mongodb connection
			.then(dbCollection)						// get db collection
			.then(remove)							// update/insert jobOpening 
			.catch(globalFunctions.err);			// error handling

	function remove(col){
		return col.remove({jobOpeningId:data.jobOpeningId});
	}
}

function dbCollection(db){
	return db.collection("jobOpenings");

	// jobOpening collection : {jobOpeningId , jobName , jobDesc , jobStatus}
}

module.exports = new JobOpenings();