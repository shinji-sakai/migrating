var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batTimingApi = require(configPath.api.batchCourse.batchTiming);
var emailTemplatesApi = require(configPath.api.admin.emailTemplates);
var userEmailGroupApi = require(configPath.api.admin.userEmailGroup);
var sendNotificationApi = require(configPath.api.admin.sendNotification);
var mailAPI = require(configPath.api.mail);

var ew_training_bat_users_send_email_table = 'ew1.ew_training_bat_users_send_email';

function SendEmailTemplate() {

}

SendEmailTemplate.prototype.getAllSendEmailTemplates = async function(data){
	try{
		var query = "select * from " + ew_training_bat_users_send_email_table;
		var res = await cassExecute(query);
		//get all batch ids from all templates
		var allBatIds = res.map((v,i)=>v.bat_id);
		//get batch details for all batch id
		var allBatches = await batTimingApi.getTimingByBatId({bat_id : allBatIds});
		var allBatchesMap = {};
		//create map of all batches {bat_id -> batch_detail}
		allBatches.map((v,i)=>{
			allBatchesMap[v.bat_id] = v;
		})

		//attach batch details to correspondance template
		res = res.map((v,i)=>{
			v["batch_details"] = allBatchesMap[v.bat_id];
			return v;
		})

		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendEmailTemplate.prototype.getSendEmailTemplateById = async function(data){
	try{
		var pk_id = data.pk_id;
		var query = "select * from " + ew_training_bat_users_send_email_table + " where pk_id = ?";
		var params = [pk_id]
		var res = await cassExecute(query,params);
		return res[0];
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SendEmailTemplate.prototype.addSendEmailTemplate = async function(data){
	try{
		var pk_id = data.pk_id || Uuid.random();
		var frm_nm = data.frm_nm;
		var bat_id = data.bat_id;
		var training_id = data.training_id;
		
		var users_template = data.users_template;
		var email_template = data.email_template;

		var crt_dt  = new Date();

		var query = "update " + ew_training_bat_users_send_email_table + " set frm_nm=? , users_template=? , email_template=? , bat_id=? , training_id=? , crt_dt=? where pk_id=? " ;
		// var query = "insert into " + ew_training_bat_users_send_email_table + " (pk_id,frm_nm , users_template , email_template,bat_id,training_id , crt_dt) values (?,?,?,?,?,?,?) " ;
		var params = [frm_nm , users_template , email_template ,bat_id,training_id, crt_dt,pk_id];
		//execute query
		var res = await cassExecute(query,params);

		//get batch data for this template
		var batchData = await batTimingApi.getTimingByBatId({bat_id : bat_id});

		return {pk_id,...data,batch_details : batchData[0]};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


SendEmailTemplate.prototype.sendEmailTemplatesToUsers = async function(data){
	try{
		var pk_id = data.pk_id || Uuid.random();
		var frm_nm = data.frm_nm;
		var bat_id = data.bat_id;
		var training_id = data.training_id;
		
		var users_template = data.users_template;
		var email_template = data.email_template;



		var emailTemplate = await emailTemplatesApi.getEmailTemplateById({pk_id : email_template});
		var userEmailGroup = await userEmailGroupApi.getUserEmailGroupById({pk_id : users_template});

		if(emailTemplate && userEmailGroup){
			var email_body = emailTemplate.email_body;
			var email_subject = emailTemplate.email_sub;
			var users_emails = userEmailGroup.users_email || [];
			var email_str = users_emails.join(",");

			await mailAPI.sendMail({
				to : email_str,
				subject : email_subject,
				text : email_body
			});
			return data;
		}else{
			return {err : 'Something wrong'};
		}
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


SendEmailTemplate.prototype.sendNotificationOfEmail = async function(data){
	try{
		var pk_id = data.pk_id || Uuid.random();
		var frm_nm = data.frm_nm;
		
		var users_template = data.users_template;
		var email_template = data.email_template;


		var emailTemplate = await emailTemplatesApi.getEmailTemplateById({pk_id : email_template});
		var userEmailGroup = await userEmailGroupApi.getUserEmailGroupById({pk_id : users_template});

		if(emailTemplate && userEmailGroup){
			var email_body = emailTemplate.email_body;
			var email_subject = emailTemplate.email_sub;
			var users_emails = userEmailGroup.users_email || [];

			await sendNotificationApi.addSendNotification({
				sub : email_subject,
				noti_msg : email_body,
				users : userEmailGroup.usrs
			});
			return data;
		}else{
			return {err : 'Something wrong'};
		}
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


SendEmailTemplate.prototype.deleteSendEmailTemplate = async function(data){
	// data = {pk_id}
	try{
		var pk_id = data.pk_id;
		if(!pk_id){
			return {
				err : 'pk_id is required'
			}
		}
		var query = "delete from " + ew_training_bat_users_send_email_table + " where pk_id = ?" ;
		var params = [pk_id];
		var res = await cassExecute(query,params);
		return {pk_id};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


module.exports = new SendEmailTemplate();