var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function Topic(){

}
Topic.prototype.getAll = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		if(data.subjectId){
			return col.find({subjectId : data.subjectId}).toArray();
		}else{
			return col.find({}).toArray();
		}
	}
}
Topic.prototype.update = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	async function update(col){
		let obj = await col.findOne({topicId:data.topicId});
		
		if(!obj){
            data.create_dt = new Date();
            data.update_dt = new Date();
            var tempRes = await col.find({subjectId : data.subjectId}).toArray();
            if(tempRes && tempRes.length > 0){
            	data.topicNumber = tempRes[tempRes.length - 1]["topicNumber"] + 1;
            }else{
            	data.topicNumber = 1;
            }
        }else{
            data.update_dt = new Date()
        }

		if(!data.topicId){
			data.topicId = shortid.generate();
		}
		return col.update({topicId:data.topicId},{$set  : data},{upsert:true});
	}
}

Topic.prototype.remove = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({topicId:data.topicId});
	}
}

Topic.prototype.fetchTopicsUsingLimit = async function(data) {
    try{
        let { no_of_item_to_fetch , no_of_item_to_skip } = data;
        if(typeof no_of_item_to_fetch === "undefined"){
            no_of_item_to_fetch = 5;
        }
        if(typeof no_of_item_to_skip === "undefined"){
            no_of_item_to_skip = 0;
        }
        console.log(no_of_item_to_fetch , no_of_item_to_skip);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();
        return find_res
    }catch(err){
        throw new Error(err)
	}
}
    
function dbCollection(db){
	return db.collection("topic");
}

module.exports = new Topic();