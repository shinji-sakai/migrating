var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);

var courseModule = require(configPath.api.admin.courseModule);
var course = require(configPath.api.admin.course);
var courseApi = require(configPath.api.admin.course);
var moduleItems = require(configPath.api.admin.moduleItems);

var debug = require('debug')('app:api:admin:coursemaster');

const RELATED_COURSE_FETCH_LIMIT = 3;

var Q=require('q');

function CourseMaster(){}

/**
 * [getAll get all courses from coursemaster collection]
 * @return {[Promise]} [find Promise]
 */
CourseMaster.prototype.getAll = function () {
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({}, { _id: 0}).toArray();
    }
}

/**
 * [getAll get all courses from coursemaster collection]
 * @return {[Promise]} [find Promise]
 */
CourseMaster.prototype.getCourseByCourseId = async function (data) {
    if(!data.courseId){
        return {
            error : "courseId field is required."
        }
    }
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.findOne({courseId : data.courseId}, { _id: 0});
    }
}

/**
 * [add add/update coursemaster , this will take effect in courselist,coursemodules and moduleitems]
 * @param {[type]} data [description]
 */
CourseMaster.prototype.add = function (data) {
    if(data.courseId){
        data.courseId = data.courseId.toLowerCase();
    }
    return getConnection()      //get mongodb connection
        .then(dbCollection)     //get database collection
        .then(insertData)       // insert/update data in coursemaster
        .then(updateCourseList) // update in courselist
        .then(updateCourseModules)  // update in course modules
        .then(updateModuleItems)    // update moduleitems
        .catch(function(err) {
            console.log(err);
        });

    function insertData(col) {
        //insert/update in coursemaster
        return col.update({courseId : data.courseId},data,{upsert:true,w:1});
    }

    function updateCourseList() {
        //update in courselist
        var query = {
            $set : {
                courseName : data.courseName,
                courseType : data.courseType,
                courseSubGroup : data.courseSubGroup,
                author : data.author,
                isLiveClass : data.isLiveClass
            }
        }
        console.log(query);
        return course.updateUsingQuery({courseId : data.courseId},query);
    }

    function updateCourseModules() {
        //update in coursemodules
        var query = {
            $set : {
                courseName : data.courseName,
            }
        }
        return courseModule.updateUsingQuery({courseId : data.courseId},query);
    }

    function updateModuleItems() {
        //insert/update in moduleItems
        var query = {
            $set : {
                courseName : data.courseName,
                courseType : data.courseType,
                courseSubGroup : data.courseSubGroup,
                author : data.author,
                isLiveClass : data.isLiveClass
            }
        }
        return moduleItems.updateUsingQuery({courseId : data.courseId},query);
    }

}
CourseMaster.prototype.delete = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .then(removeDataFromCourseList)
        .then(removeDataFromModules)
        .then(removeDataFromModuleItems)
        .catch(globalFunctions.err);

    function removeData(col) {
        //remove from coursemaster
        return col.deleteOne({courseId: data.courseId});
    }
    function removeDataFromCourseList(col) {
        //remove from courselist
        return course.delete({courseId: data.courseId});
    }
    function removeDataFromModules(col) {
        //remove from coursemodules
        return courseModule.deleteUsingCourseId({courseId: data.courseId});
    }
    function removeDataFromModuleItems(col) {
        //remove from moduleitems
        return moduleItems.deleteModuleItemsUsingCourseId({courseId: data.courseId});
    }
}


// getRelatedCoursesByCourseId
CourseMaster.prototype.getRelatedCoursesByCourseId = async function(data) {
    try{
        let {startIndex} = data;
        startIndex = startIndex || 0
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.findOne({courseId:data.courseId});
        var relatedCourses = find_res["relatedCourses"] || [];
        if(relatedCourses.length > 0){
            relatedCourses = relatedCourses.splice(startIndex,RELATED_COURSE_FETCH_LIMIT)
            var fullCourses = await courseApi.getCoursesById({ids : relatedCourses});
            for (var i = 0; i < fullCourses.length; i++) {
                var crs = fullCourses[i]
                //get all modules data for course
                var items = await moduleItems.getCourseItems({courseId : crs.courseId});
                var moduleI = items[0] || {}

                let allModules = moduleI["moduleDetail"] || []
                //sort all module based on weight
                allModules.sort((a, b)=>  {
                    return a.moduleWeight - b.moduleWeight;
                });
                //get first module
                var firstModule = allModules[0] || {};
                // get first item of the first module
                var firstItem = firstModule.moduleItems ? firstModule.moduleItems[0] : {};
                //attach it to course details
                crs["firstModuleId"] = firstModule.moduleId;
                crs["firstItemId"] = firstItem.itemId;
                crs["firstItemType"] = firstItem.itemType;
            }
            return (fullCourses || []);
        }else{
            return [];
        }
    }catch(e){
        throw e;
    }
}

CourseMaster.prototype.fetchCourseMasterUsingLimit = async function(data) {
    try{
        let { no_of_item_to_fetch , no_of_item_to_skip } = data;
        if(typeof no_of_item_to_fetch === "undefined"){
            no_of_item_to_fetch = 5;
        }
        if(typeof no_of_item_to_skip === "undefined"){
            no_of_item_to_skip = 0;
        }
        console.log(no_of_item_to_fetch , no_of_item_to_skip);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();
        return find_res
    }catch(err){
        throw new Error(err)
    }
}

CourseMaster.prototype.isIdExist = async function(data) {
    try{
        let { key , collection , id_value } = data;
        if(id_value && key){
            var connection = await getConnection();
            var collection = await connection.collection(collection);
            var obj = {};
            obj[key] = id_value.toLowerCase();
            var find_res = await collection.find(obj).toArray();
            if(find_res && find_res[0]){
                return {
                    status : "success",
                    idExist : true,
                    data : find_res[0]
                }
            }else{
                return {
                    status : "success",
                    idExist : false
                }
            }
        }else{
            return {
                status : "error",
                message : "id_value field is required"
            }
        }
    }catch(err){
        throw new Error(err)
    }
}

function dbCollection(db){
    return db.collection("coursemaster");
}

module.exports = new CourseMaster();
