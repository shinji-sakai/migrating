var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var Q=require('q');

function VideoSlide(){}

VideoSlide.prototype.getAll = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({"videoId": data.videoId}).toArray();
    }
}
VideoSlide.prototype.add = function (data) {
    var col;
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .then(insertData)
        .catch(globalFunctions.err);

    function removeData(gcol) {
        col = gcol;
        return col.updateOne({
            "videoId": data.videoId
        }, {
            $pull: {"slideDetail": {"slideId": data.slideDetail.slideId}}
        });
    }

    function insertData() {
        return col.updateOne(
            {
                "videoId": data.videoId
            },
            {
                $addToSet: {
                    "slideDetail": data.slideDetail
                },
                $set: {
                    "topicTitle": data.topicTitle,
                    "slideDesc": data.slideDesc,
                    "lastUpdated":new Date()
                }
            }, {upsert: true});
    }
}
VideoSlide.prototype.delete = function (data) {
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        return col.updateOne({
            "videoId": data.videoId,
        }, {
            $pull: {"slideDetail": {"slideId": data.slideId}}
        });
    }
}

function dbCollection(db){
    return db.collection("coursevideoslides");
}

module.exports = new VideoSlide();