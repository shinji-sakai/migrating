var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;

var ew_roles_m_table = 'ew1.ew_roles_m';

function Roles() {

}

Roles.prototype.getAllRoles = async function(data){
	try{
		var query = "select * from " + ew_roles_m_table;
		var res = await cassExecute(query);
		return res;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

Roles.prototype.addRole = async function(data){
	try{
		var role_id = data.role_id;
		var role_name = data.role_name;
		var create_dt  = new Date();

		var query = "update " + ew_roles_m_table + " set role_name=? , create_dt=? where role_id = ?" ;
		var params = [role_name , create_dt , role_id];
		//execute query
		var res = await cassExecute(query,params);
		
		return {...data};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

Roles.prototype.deleteRole = async function(data){
	try{
		var role_id = data.role_id;
		var query = "delete from " + ew_roles_m_table + " where role_id = ?" ;
		var params = [role_id];
		var res = await cassExecute(query,params);
		return {role_id};
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


module.exports = new Roles();