var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);

class GeneralAPI{
    async isIdExist(data){
        try{
            let { key , collection , id_value } = data;
            if(id_value && key){
                var connection = await getConnection();
                var collection = await connection.collection(collection);
                var obj = {};
                obj[key] = id_value.toLowerCase();
                var find_res = await collection.find(obj).toArray();
                if(find_res && find_res[0]){
                    return {
                        status : "success",
                        idExist : true,
                        data : find_res[0]
                    }
                }else{
                    return {
                        status : "success",
                        idExist : false
                    }
                }
            }else{
                return {
                    status : "error",
                    message : "id_value field is required"
                }
            }
        }catch(err){
            throw new Error(err)
        }
    }

    async fetchItemUsingLimit(data){
        try{
            let { no_of_item_to_fetch , no_of_item_to_skip , collection, query , update_dt } = data;
            if(!collection){
                return {
                    status : "error",
                    message : "collection field is required"
                }
            }
            if(typeof no_of_item_to_fetch === "undefined"){
                no_of_item_to_fetch = 5;
            }
            if(typeof no_of_item_to_skip === "undefined"){
                no_of_item_to_skip = 0;
            }
            query = query || {};

            if(update_dt){
                // add update date to compare
                query["update_dt"] = {
                    "$lt" : new Date(update_dt)
                }
            }

            var connection = await getConnection();

            var collection = await connection.collection(collection);

            var find_res = await collection.find(query || {}).sort({update_dt : -1}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();

            return find_res

        }catch(err){
            throw new Error(err)
        }
    }
}


module.exports = new GeneralAPI();