var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var courseMasterAPI = require(configPath.api.admin.courseMaster);
var shortid = require('shortid');

var Q=require('q');

function API(){

}
API.prototype.get = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	async function find(col){
		if(data.course_id){
			return col.find({course_id : data.course_id}).toArray();
		}else{
			let crses =  await col.find({}).toArray();
			for (var i = 0; i < crses.length; i++) {
				let course = await courseMasterAPI.getCourseByCourseId({courseId : crses[i].course_id});
				let obj = {...crses[i],...course}
				crses[i] = obj
			}
			return crses
		}
	}
}

API.prototype.getCoursesByCourseType = async function(data){
	if(!data.courseType){
		return {
			error : "courseType is required"
		}
	}
	try{
		let conn = await getConnection();
		let col = await dbCollection(conn);
		let res = await col.find({courseType : data.courseType}).toArray();
		for (var i = 0; i < res.length; i++) {
			let course = await courseMasterAPI.getCourseByCourseId({courseId : res[i].course_id});
			let obj = {...res[i],...course}
			res[i] = obj
		}
		return res
	}catch(err){
		console.log("getCoursesByCourseType err....",err)
		throw new Error(err);
	}
}

API.prototype.update = async function(data){
	if(!data.course_id){
		return {
			error : "course_id is required"
		}
	}
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	function update(col){
		return col.update({course_id:data.course_id},data,{upsert:true});
	}
}

API.prototype.remove = async function(data){
	if(!data.course_id){
		return {
			error : "course_id is required"
		}
	}
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({course_id:data.course_id});
	}
}

function dbCollection(db){
	return db.collection("boardCompetitiveCourses");
}

module.exports = new API();