var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function CourseBundle(){

}

//Get all course bundles
CourseBundle.prototype.getAll = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}

//insert/Update course bundle
//if data will come with bundleId then update data otherwise insert it
CourseBundle.prototype.update = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	async function update(col){

		let obj = await col.findOne({bundleId:data.bundleId});
		
		if(!obj){
            data.create_dt = new Date();
            data.update_dt = new Date();
        }else{
            data.update_dt = new Date()
        }

		if(!data.bundleId){
			data.bundleId = shortid.generate();
		}
		return col.update({bundleId:data.bundleId},{$set : data},{upsert:true});
	}
}


//Remove bundle from db
CourseBundle.prototype.remove = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({bundleId:data.bundleId});
	}
}

function dbCollection(db){
	return db.collection("coursebundle");
}

module.exports = new CourseBundle();