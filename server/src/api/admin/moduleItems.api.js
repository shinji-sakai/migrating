var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var getRedisClient = require(configPath.dbconn.redisconn);
var jwt_simple = require("jwt-simple");
var config = require('../../config.js');
var redisCli = getRedisClient();

var ew_usr_enroll_bat_uploads_table = 'ew1.ew_usr_enroll_bat_uploads';
var ew_crs_access_tbl = "ew1.ew_crs_access";


var Q = require('q');

function ModuleItems() {

}
ModuleItems.prototype.getAll = function(data) {
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({
            "moduleDetail.moduleId": data.moduleId
        }, {
            "moduleDetail.$": 1,
            _id: 0
        }).toArray();
    }
}

ModuleItems.prototype.getCourseItems = function(data) {

    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({
            "courseId": data.courseId
        },{
            "moduleDetail.moduleItems.itemQuestions":0
        }).toArray();
    }
}

ModuleItems.prototype.hasUserPurchasedTheCourse = async function(data) {
    try{
        let { usr_id , courseId } = data;
        var qry = "select * from " + ew_crs_access_tbl + " where usr_id = ?";
        var params = [usr_id];
        var rows = await cassExecute(qry,params);
        var isCourseFound = false;
        for (var i = 0; i < rows.length; i++) {
            let row = rows[i];
            if(row.crs_id === courseId){
                isCourseFound = true;
                break;
            }
        }
        return isCourseFound;
    }catch(e){
        console.log(e);
        throw new Error(e)
    }
}

ModuleItems.prototype.getFullCourseDetails = async function(req) {
    try{
        let { courseId } = req.body;
        let isCoursePurchased = false;

        let authorization = req.get("Authorization");
        if(authorization && authorization.indexOf("JWT") === 0){
            let auth_jwt = authorization.substr(4);
            try{
                var decoded = jwt_simple.decode(auth_jwt,config.TOKEN_SECRET)
                var usr_id = decoded.userId;
                isCoursePurchased = await this.hasUserPurchasedTheCourse({ usr_id : usr_id , courseId })
            }catch(err){
                console.log("error in decoding")
            }
        }

        var str_course = await redisCli.getAsync(courseId);
        if(str_course && str_course != 'null'){
            var jsonCourse = JSON.parse(str_course)
            jsonCourse.isCoursePurchased = isCoursePurchased;
            return jsonCourse    
        }
        return {};
    }catch(err){
        throw new Error(err);
    }
}

ModuleItems.prototype.getModuleItems = function(data) {
    return getConnection()
        .then(dbCollection)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({
            "moduleDetail.moduleId": data.moduleId
        }, {
            "moduleDetail.$": 1,
            _id: 0
        }).toArray();
    }
}

ModuleItems.prototype.update = function(data) {
    var col;
    return getConnection()
        .then(dbCollection)
        .then(removeData)
        .then(insertData)
        .catch(globalFunctions.err);


    function removeData(collection) {
        col = collection;
        return col.updateOne({
            "courseId": data.courseId
        }, {
            $pull: {
                "moduleDetail": {
                    "moduleId": data.moduleDetail.moduleId
                }
            }
        });
    }

    function insertData() {
        return col.updateOne({
            "courseId": data.courseId
        }, {
            $addToSet: {
                "moduleDetail": data.moduleDetail
            },
            $set: {
                "courseName": data.courseName,
                "author": data.author,
                "courseLevel": data.courseLevel,
                courseLongDesc: data.courseLongDesc,
                courseShortDesc: data.courseShortDesc,
                LastUpdated: data.LastUpdated,
                isBatch : data.isBatch,
                training_id : data.training_id,
            }
        }, {
            upsert: true
        });
    }
}

ModuleItems.prototype.deleteModuleItemsUsingCourseId = function(data) {
    return getConnection()
        .then(dbCollection)
        .then(deleteO)
        .catch(globalFunctions.err);

    function deleteO(col) {
        return col.deleteOne({
            courseId: data.courseId
        });
    }
}

ModuleItems.prototype.updateUsingQuery = function(data, query) {
    return getConnection()
        .then(dbCollection)
        .then(update)
        .catch(globalFunctions.err);

    function update(col) {
        return col.update(data, query, {
            upsert: true
        });
    }
}

ModuleItems.prototype.saveUserBatchEnrollUploads = async function(data) {
    try {
        var usr_assign_upload_pk = Uuid.random();
        var usr_id = data.usr_id;
        var enroll_bat = data.enroll_bat;
        var upload_module = data.upload_module;
        var upload_item = data.upload_item;
        var upload_path = data.upload_path;
        var upload_nm = data.upload_nm;
        var upload_tm = new Date();

        var query = "insert into " + ew_usr_enroll_bat_uploads_table + " (usr_assign_upload_pk,usr_id,enroll_bat,upload_module,upload_item,upload_path,upload_tm,upload_nm) values (?,?,?,?,?,?,?,?)";
        var params = [usr_assign_upload_pk, usr_id, enroll_bat, upload_module, upload_item, upload_path, upload_tm, upload_nm];

        await cassExecute(query, params);

        return data;
    } catch (err) {
        logger.debug(err);
        throw err;
    }
}

ModuleItems.prototype.getUserBatchEnrollUploads = async function(data) {
    try {
        var usr_id = data.usr_id;

        var query = "select * from " + ew_usr_enroll_bat_uploads_table + " where usr_id = ?";
        var params = [usr_id];

        var rows = await cassExecute(query, params);

        return rows;
    } catch (err) {
        logger.debug(err);
        throw err;
    }
}

function dbCollection(db) {
    return db.collection("moduleitems");
}

module.exports = new ModuleItems();