var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function SubTopic(){

}
SubTopic.prototype.getAll = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({topicId : data.topicId}).toArray();
	}
}
SubTopic.prototype.update = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	async function update(col){

		let obj = await col.findOne({subTopicId:data.subTopicId});
		
		if(!obj){
            data.create_dt = new Date();
            data.update_dt = new Date();
        }else{
            data.update_dt = new Date()
        }

		if(!data.subTopicId){
			data.subTopicId = shortid.generate();
		}
		return col.update({subTopicId:data.subTopicId},{$set : data},{upsert:true});
	}
}

SubTopic.prototype.remove = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({subTopicId:data.subTopicId});
	}
}

function dbCollection(db){
	return db.collection("subtopic");
}

module.exports = new SubTopic();