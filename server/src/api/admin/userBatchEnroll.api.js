var configPath = require('../../configPath.js');
var config = require('../../config.js');

var cassconn = require(configPath.dbconn.cassconn);
var Q = require('q');
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var logger = require(configPath.lib.log_to_file);
var cassExecute = require(configPath.lib.utility).cassExecute;
var batTimingApi = require(configPath.api.batchCourse.batchTiming);
var paymentApi = require(configPath.api.payment.payment);
var profileAPI = require(configPath.api.dashboard.profile);
var mailAPI = require(configPath.api.mail);

function UserBatchEnroll() {

}

UserBatchEnroll.prototype.getUserEnrollAllBatch = async function(data){
	try{
		var rows = await paymentApi.getUserEnrolledBatchDetails({usr_id : data.usr_id});
		//get all batch ids
		var allBatIds = rows.map((v,i)=>v.enroll_bat);
		//get batch details for all batch id
		var allBatches = await batTimingApi.getTimingByBatId({bat_id : allBatIds});
		var allBatchesMap = {};
		//create map of all batches {bat_id -> batch_detail}
		allBatches.map((v,i)=>{
			allBatchesMap[v.bat_id] = v;
		})
		//attach batch details to correspondance template
		rows = rows.map((v,i)=>{
			return {
				...v,
				...allBatchesMap[v.enroll_bat]
			};
		})
		return rows;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

UserBatchEnroll.prototype.addUserBatchEnroll = async function(data){
	try{
		var usr_id = data.usr_id;
		var enroll_bat = data.enroll_bat;
		var enroll_dt =  data.enroll_status ? new Date() : undefined; 
		var enroll_status =  data.enroll_status;

		var training = data.emailData.training;
		var cls_start_dt = data.emailData.cls_start_dt
		var cls_frm_tm = data.emailData.cls_frm_tm;
		var frm_wk_dy = data.emailData.frm_wk_dy;
		var to_wk_dy = data.emailData.to_wk_dy;


		await paymentApi.saveUserEnrolledBatchDetails({usr_id , enroll_bat , enroll_dt , enroll_status});
		//get all batch ids
		var allBatIds = [enroll_bat];
		//get batch details for all batch id
		var allBatches = await batTimingApi.getTimingByBatId({bat_id : allBatIds});
		var allBatchesMap = {};
		//create map of all batches {bat_id -> batch_detail}
		allBatches.map((v,i)=>{
			allBatchesMap[v.bat_id] = v;
		})

		if(enroll_status){
			//send mail
			var user_data = await profileAPI.getUserShortDetails({usr_id : usr_id});
			var user_email = user_data.usr_email;
			await mailAPI.sendEnrolledBatchMail({
				to : user_email,
				dsp_nm : user_data.dsp_nm,
				usr_id : usr_id,
				...data.emailData
			})
		}

		return {
			...data,
			...allBatchesMap[enroll_bat]
		}
		return rows;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

UserBatchEnroll.prototype.removeUserBatchEnroll = async function(data){
	try{
		var usr_id = data.usr_id;
		var enroll_bat = data.enroll_bat;
		var enroll_status =  false;

		await paymentApi.saveUserEnrolledBatchDetails({usr_id , enroll_bat , enroll_status});
		//get all batch ids
		var allBatIds = [enroll_bat];
		//get batch details for all batch id
		var allBatches = await batTimingApi.getTimingByBatId({bat_id : allBatIds});
		var allBatchesMap = {};
		//create map of all batches {bat_id -> batch_detail}
		allBatches.map((v,i)=>{
			allBatchesMap[v.bat_id] = v;
		})

		return {
			...data,
			enroll_status : false,
			...allBatchesMap[enroll_bat]
		}
		return rows;
	}catch(err){
		logger.debug(err);
		throw err;
	}
}
//

module.exports = new UserBatchEnroll();