var configPath = require('../../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function Publication(){

}
Publication.prototype.getAll = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		return col.find({}).toArray();
	}
}
Publication.prototype.fetchPublicationsUsingLimit = async function(data) {
    try{
        let { no_of_item_to_fetch , no_of_item_to_skip } = data;
        if(typeof no_of_item_to_fetch === "undefined"){
            no_of_item_to_fetch = 5;
        }
        if(typeof no_of_item_to_skip === "undefined"){
            no_of_item_to_skip = 0;
        }
        console.log(no_of_item_to_fetch , no_of_item_to_skip);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();
        return find_res
    }catch(err){
        throw new Error(err)
    }
}
Publication.prototype.update = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	function update(col){
		if(!data.publicationId){
			data.publicationId = shortid.generate();
		}
		return col.update({publicationId:data.publicationId},data,{upsert:true});
	}
}

Publication.prototype.remove = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({publicationId:data.publicationId});
	}
}

function dbCollection(db){
	return db.collection("publication");
}

module.exports = new Publication();