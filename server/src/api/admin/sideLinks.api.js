var configPath = require('../../configPath.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');
var courseAPI = require(configPath.api.admin.course);

var Q=require('q');

function API(){

}
API.prototype.getAll = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		if(data.page_id){
			return col.find({page_id : data.page_id}).toArray();
		}else{
			return col.find({}).toArray();
		}
	}
}

API.prototype.getById = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.then(getCourseDetails)
			.catch(globalFunctions.err);

	function find(col){
		if(data.page_id){
			return col.findOne({page_id : data.page_id});
		}else{
			return col.find({}).toArray();
		}
	}

	async function getCourseDetails(data){
		if(data.crs_id){
			var res = await courseAPI.getCoursesById({ids : data.crs_id});
			data["courseDetails"] = res[0];
			return data;
		}else{
			return data;
		}
	}
}

API.prototype.getByIds = function(data){
	return getConnection()
			.then(dbCollection)
			.then(find)
			.catch(globalFunctions.err);

	function find(col){
		if(data.page_id){
			return col.find({page_id : {$in : [].concat(data.page_id)}}).toArray();
		}else{
			return [];
		}
	}
}



API.prototype.update = function(data){
	return getConnection()
			.then(dbCollection)
			.then(update)
			.catch(globalFunctions.err);

	function update(col){
		if(!data.page_id){
			data.page_id = shortid.generate();
		}
		return col.update({page_id:data.page_id},data,{upsert:true});
	}
}

API.prototype.remove = function(data){
	return getConnection()
			.then(dbCollection)
			.then(remove)
			.catch(globalFunctions.err);

	function remove(col){
		return col.remove({page_id:data.page_id});
	}
}

function dbCollection(db){
	return db.collection("sideLinksArticles");
}

module.exports = new API();