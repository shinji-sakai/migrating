var configPath = require('../../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var shortid = require('shortid');

var Q=require('q');

function Book(){}

//Get All Books from mongodb
Book.prototype.getAll = function(data){
	return getConnection()							// get mongodb connection
			.then(dbCollection)						// get db collection
			.then(find)								// find all books
			.catch(globalFunctions.err);			// error handling

	function find(col){
		return col.find({}).toArray();
	}
}

//Update/insert book by id
Book.prototype.update = function(data){
	return getConnection()							// get mongodb connection
			.then(dbCollection)						// get db collection
			.then(update)							// update/insert book 
			.catch(globalFunctions.err);			// error handling

	async function update(col){
		
		let obj = await col.findOne({bookId:data.bookId});
		
		if(!obj){
            data.create_dt = new Date();
            data.update_dt = new Date();
        }else{
            data.update_dt = new Date()
        }

		//if bookid is not there then create new id and insert it or else update it
		if(!data.bookId){
			data.bookId = shortid.generate();
		}
		return col.update({bookId:data.bookId},{$set : data},{upsert:true});
	}
}

//Remove Book By id
Book.prototype.remove = function(data){
	return getConnection()							// get mongodb connection
			.then(dbCollection)						// get db collection
			.then(remove)							// update/insert book 
			.catch(globalFunctions.err);			// error handling

	function remove(col){
		return col.remove({bookId:data.bookId});
	}
}
Book.prototype.fetchBooksUsingLimit = async function(data) {
    try{
        let { no_of_item_to_fetch , no_of_item_to_skip } = data;
        if(typeof no_of_item_to_fetch === "undefined"){
            no_of_item_to_fetch = 5;
        }
        if(typeof no_of_item_to_skip === "undefined"){
            no_of_item_to_skip = 0;
        }
        console.log(no_of_item_to_fetch , no_of_item_to_skip);
        var connection = await getConnection();
        var collection = await dbCollection(connection);
        var find_res = await collection.find({}).limit(no_of_item_to_fetch).skip(no_of_item_to_skip).toArray();
        return find_res
    }catch(err){
        throw new Error(err)
    }
}
function dbCollection(db){
	return db.collection("book");

	// book collection : {bookId , bookName}
}

module.exports = new Book();