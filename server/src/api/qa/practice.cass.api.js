var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:qa:practice:cass");
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var uuid = require('uuid');
var logger = require(configPath.lib.log_to_file);
var oracledb = require(configPath.dbconn.oracledb);
var moment = require("moment");


var prtc_stats_table = 'ew1.vt_usr_prtc_stat';
var oracle_prtc_stats_table = 'vt_usr_prtc_stat';

var ew_usr_tests_dtl_table = 'ew1.ew_usr_tests_dtl';
var oracle_usr_tests_dtl_table = 'ew_usr_tests_dtl';

var ew_usr_tests_m_table = "ew1.ew_usr_tests_m";
var oracle_ew_usr_tests_m_table = "ew_usr_tests_m";
var ew_usr_tests_review_table = "ew1.ew_usr_tests_review";

var ew_avg_perf_stats_table = "ew1.ew_avg_perf_stats";
var ew_usr_perf_stats_table = "ew1.ew_usr_perf_stats";

var ew_usr_perf_month_stats_table = "ew1.ew_usr_perf_month_stats";
var ew_usr_perf_month_avg_stats_table = "ew1.ew_usr_perf_month_avg_stats";

var usr_q_attempt_a_table = "ew1.usr_q_attempt_a";

function Practice() {}

Practice.prototype.saveQuestionStats = async function(data) {
	var defer = Q.defer();
    var usr_id = data.usr_id || "";
    var que_id = data.que_id || "";
    var tpc_id = data.tpc_id || "";
    var mdl_id = data.mdl_id || "";
    var crs_id = data.crs_id || "";
    var que_strt_tm = data.que_strt_tm;
    var que_end_tm = data.que_end_tm;
    var que_ans = data.que_ans || {};
    var que_ans_tm = data.que_ans_tm || {};
    var no_of_clks = data.no_of_clks || 0;
    var revu = data.revu || false;
    var lrn = data.lrn || false;
    var skip = data.skip || false;
    var wrg_rsn = data.wrg_rsn || ["Not Selected"];
    var que_diff = data.que_diff || "Not Selected";

    //insert query for cassandra table
    var insertStats = "insert into " + prtc_stats_table + " (usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm,que_ans,que_ans_tm,no_of_clks,revu,lrn,skip,wrg_rsn,que_diff) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    var insertStats_params = [usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm, que_ans, que_ans_tm, no_of_clks, revu, lrn,skip, wrg_rsn, que_diff];

    //insert into oracle stats table also
    var oracle_insertStats_params = [
        usr_id, 
        que_id, 
        tpc_id, 
        mdl_id, 
        crs_id,
        "to_date('" + moment(que_strt_tm).format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')",
        "to_date('" + moment(que_end_tm).format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')",
        no_of_clks, 
        revu, 
        lrn,
        skip, 
        que_diff
    ];
    oracle_insertStats_params = oracle_insertStats_params.map((v,i)=>{
        if(("" + v).indexOf("to_date") === 0){
            return v
        }else{
            return "'" + v + "'";
        }
    })
    var oracle_insertStats_params_str = '(' + oracle_insertStats_params.join(",") + ')';
    var oracle_insertStats = "insert into " + oracle_prtc_stats_table + " (usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm,no_of_clks,revu,lrn,skip,que_diff) values " + oracle_insertStats_params_str;
    try{
        logger.debug(oracle_insertStats);
        var conn = await oracledb.getConnection();
        var res = await conn.execute(oracle_insertStats,[],{autoCommit: true});
        await oracledb.releaseConnection(conn);
        logger.debug("insert into...oracle_prtc_stats_table....",res);    
    }catch(err){
        logger.debug(err);
    }
    

    ///////////////////////////////

    cassconn.execute(insertStats, insertStats_params,{ prepare : true }, function(err, res, r) {
    	// debug(err,res,r);
        if (err) {
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
}


Practice.prototype.saveTestQuestionStats = async function(data) {
    try{
        var tst_id = data.tst_id || uuid.v4();
        var usr_id = data.usr_id || "";
        var que_id = data.que_id || "";
        var tpc_id = data.tpc_id || "";
        var mdl_id = data.mdl_id || "";
        var crs_id = data.crs_id || "";
        var que_strt_tm = data.que_strt_tm;
        var que_end_tm = data.que_end_tm;
        var que_ans = data.que_ans || {};
        
        

        //insert into oracle stats table also
        var oracle_insertStats_params = [tst_id,usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm];
        oracle_insertStats_params = oracle_insertStats_params.map((v,i)=>{
            return "'" + v + "'";
        })
        var oracle_insertStats_params_str = '(' + oracle_insertStats_params.join(",") + ')';
        var oracle_insertStats = "insert into " + oracle_usr_tests_dtl_table + " (tst_id,usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm) values " + oracle_insertStats_params_str;
        try{
            logger.debug(oracle_insertStats);
            var conn = await oracledb.getConnection();
            var res = await conn.execute(oracle_insertStats,[],{autoCommit: true});
            await oracledb.releaseConnection(conn);
            logger.debug("insert into...oracle_usr_tests_dtl_table....",res);    
        }catch(err){
            logger.debug(err);
        }
        

        ///////////////////////////////

        var insertStats = "insert into " + ew_usr_tests_dtl_table + " (tst_id,usr_id,que_id,tpc_id,mdl_id,crs_id,que_strt_tm,que_end_tm,que_ans) values (?,?,?,?,?,?,?,?,?)";
        var insertStats_params = [tst_id,usr_id, que_id, tpc_id, mdl_id, crs_id, que_strt_tm, que_end_tm, que_ans];

        var saveRes = await this.saveTestToMasterTable(data);
        var res = await cassExecute(insertStats, insertStats_params,{ prepare : true });
        return data;
    }catch(err){
        debug(err);
        throw err;
    }
}

Practice.prototype.saveTestToMasterTable = async function(data) {
    try{
        var tst_id = data.tst_id || uuid.v4();
        var usr_id = data.usr_id || "";
        var tst_strt_tm = data.tst_strt_tm;
        var tst_end_tm = data.tst_end_tm;
        
        var insertStats = "insert into " + ew_usr_tests_m_table + " (tst_id,usr_id,tst_strt_tm,tst_end_tm) values (?,?,?,?)";
        var insertStats_params = [tst_id,usr_id,tst_strt_tm,tst_end_tm];
        var res = await cassExecute(insertStats, insertStats_params,{ prepare : true });

        //insert into oracle stats table also
        try{
            var oracle_insertStats_params = [tst_id,usr_id,tst_strt_tm,tst_end_tm];
            oracle_insertStats_params = oracle_insertStats_params.map((v,i)=>{
                return "'" + v + "'";
            })
            var oracle_insertStats_params_str = '(' + oracle_insertStats_params.join(",") + ')';
            var oracle_insertStats = "insert into " + oracle_ew_usr_tests_m_table + " (tst_id,usr_id,tst_strt_tm,tst_end_tm) values " + oracle_insertStats_params_str;
            logger.debug(oracle_insertStats);
            var conn = await oracledb.getConnection();
            var res = await conn.execute(oracle_insertStats,[],{autoCommit: true});
            await oracledb.releaseConnection(conn);
            logger.debug("insert into...oracle_ew_usr_tests_m_table....",res);    
        }catch(err){
            logger.debug(err);
        }
        //////////////////////////////

        return data;
    }catch(err){
        debug(err);
        throw err;
    }
}


Practice.prototype.saveTestReviews = async function(dataArr) {
    try{
        var queries = [];
        for (var i = 0; i < dataArr.length; i++) {
            var data  = dataArr[i];
            var tst_id = data.tst_id || uuid.v4();
            var usr_id = data.usr_id || "";
            var que_id = data.que_id || "";
            var tpc_id = data.tpc_id || "";
            var mdl_id = data.mdl_id || "";
            var crs_id = data.crs_id || "";
            var wrg_rsn = data.wrg_rsn || ["Not Selected"];
            var usr_cmnt = data.usr_cmnt || ["Not Selected"];

            var qry = "insert into " + ew_usr_tests_review_table + " (tst_id,usr_id,que_id,tpc_id,mdl_id,crs_id,wrg_rsn,usr_cmnt) values (?,?,?,?,?,?,?,?)";
            var prm = [tst_id,usr_id, que_id, tpc_id, mdl_id, crs_id,wrg_rsn,usr_cmnt];

            queries.push({
                query : qry,
                params : prm
            })
        }
        var res = await cassBatch(queries,{ prepare : true });
        return data;
    }catch(err){
        debug(err);
        throw err;
    }
}


Practice.prototype.getAllAvgQuestionPerfStats = async function(data) {
    try{
        var query = "select * from " + ew_avg_perf_stats_table;
        let rows = await cassExecute(query);
        return rows;
    }catch(err){
        logger.debug(err);
        throw new Error(err);
    }
}

Practice.prototype.getAllUsersQuestionPerfStats = async function(data) {
    try{
        var query = "select * from " + ew_usr_perf_stats_table;
        let rows = await cassExecute(query);
        return rows;
    }catch(err){
        logger.debug(err);
        throw new Error(err);
    }
}


Practice.prototype.getAllMonthAvgQuestionPerfStats = async function(data) {
    try{
        var query = "select * from " + ew_usr_perf_month_avg_stats_table;
        let rows = await cassExecute(query);
        return rows;
    }catch(err){
        logger.debug(err);
        throw new Error(err);
    }
}

Practice.prototype.getAllUsersMonthQuestionPerfStats = async function(data) {
    try{
        var query = "select * from " + ew_usr_perf_month_stats_table;
        let rows = await cassExecute(query);
        return rows;
    }catch(err){
        logger.debug(err);
        throw new Error(err);
    }
}


Practice.prototype.getUserMonthQuestionAttemptStats = async function(data) {
    try{
        var {usr_id , month , year , no_of_months} = data;
        var MONTHS = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var NO_OF_MONTHS = no_of_months || 3;
        var today = new Date();
        if(!year){
            year = today.getFullYear();
        }

        if(!month || month > 12){
            month = today.getMonth();
        }else{
            // Javascript month start from 0
            month = month - 1;
        }

        var desireMonth = new Date(year,month);
        let desireMonthStr = desireMonth.getFullYear() + ("00" + (desireMonth.getMonth() + 1)).slice(-2)
        
        var cass_month_arr = ["'" + desireMonthStr + "'"];
        
        for (var i = 0; i < (NO_OF_MONTHS - 1); i++) {
            let month = new Date(desireMonth);
            month.setMonth(month.getMonth() - (i + 1));
            let monthStr = month.getFullYear() + ("00" + (month.getMonth() + 1)).slice(-2);
            cass_month_arr.push("'" + monthStr + "'");
        }

        var month_id_str = "(" + cass_month_arr.join(",") + ")";

        var query = "select * from " + usr_q_attempt_a_table + " where usr_id=? and month_id in "+ month_id_str ;
        var params = [usr_id];

        let rows = await cassExecute(query,params);
        var res = {
            "usr_id" : usr_id,
            "months" : [],
            "no_q_attempt" : [],
            "no_q_correct_1" : [],
            "no_q_correct_2" : [],
            "no_q_correct_3" : [],
            "no_q_learned" : []
        }

        if(rows.length > 0){
            rows.forEach((v,i)=>{
                res["months"].push(MONTHS[parseInt(v.month_id.slice(-2) || "1") - 1]);
                res["no_q_attempt"].push(parseInt(v.no_q_attempt || 0));
                res["no_q_correct_1"].push(parseInt(v.no_q_correct_1 || 0));
                res["no_q_correct_2"].push(parseInt(v.no_q_correct_2 || 0));
                res["no_q_correct_3"].push(parseInt(v.no_q_correct_3 || 0));
                res["no_q_learned"].push(parseInt(v.no_q_learned || 0));
            })
        }

        return res;
    }catch(err){
        logger.debug(err);
        throw new Error(err);
    }
}



module.exports = new Practice();