var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var config = require('../../config.js');
var Q = require('q');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var cassandra = require('cassandra-driver');
var debug = require('debug')("app:api:qa:createst:cass");
var uuid = require('uuid');
var Uuid = cassandra.types.Uuid;

var createtest_table = 'ew1.vt_usr_created_tsts';
var vw_tst_table = 'ew1.vt_vw_tsts';

function CreateTest() {}

CreateTest.prototype.saveTest = function(data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var crt_dt = new Date();
    var que_book_id = data.que_book_id;
    var tst_nm = data.tst_nm;
    var tst_id = "";
    if(data.tst_id && data.tst_id.length > 0){
        tst_id = data.tst_id;    
    }else{
        tst_id = Uuid.random();
    }
    
    var insert_created_tst = "insert into " + createtest_table + " (usr_id,que_book_id,tst_nm,tst_id,crt_dt) values (?,?,?,?,?)";
    var insert_created_tst_params = [usr_id,que_book_id,tst_nm,tst_id,crt_dt];

    var insert_vw_tst = "insert into " + vw_tst_table + " (usr_id,que_book_id,tst_nm,tst_id,crt_dt) values (?,?,?,?,?)";
    var insert_vw_tst_params = [usr_id,que_book_id,tst_nm,tst_id,crt_dt];

    var queries = [{
        query : insert_created_tst,
        params : insert_created_tst_params
    },{
        query : insert_vw_tst,
        params : insert_vw_tst_params
    }];

    var queryOptions = { prepare: true, consistency:cassandra.types.consistencies.quorum };
    cassconn.batch(queries,queryOptions, function(err, res, r) {
        if (err) {
            defer.reject(err);
            return;
        }
        defer.resolve([]);
    });
    return defer.promise;
}


CreateTest.prototype.getTests = function(data) {
    var defer = Q.defer();
    var usr_id = data.usr_id;
    var insertStats = "select * from " + createtest_table + " where usr_id = ?;";
    var insertStats_params = [usr_id];
    cassconn.execute(insertStats, insertStats_params,{ prepare : true }, function(err, res, r) {
        if (err) {
            defer.reject(err);
            return;
        }
        if(res && res.rows){
            defer.resolve(res.rows);    
        }else{
            defer.resolve([]);
        }
    });
    return defer.promise;
}

CreateTest.prototype.getTestById = function(data) {
    var defer = Q.defer();
    var tst_id = data.tst_id;
    var usr_id = data.usr_id ? data.usr_id : '';
    var insertStats = "";
    var insertStats_params = [];
    if(usr_id){
        insertStats = "select * from " + createtest_table + " where tst_id = ? and usr_id = ?;";
        insertStats_params = [tst_id,usr_id];
    }else{
        insertStats = "select * from " + vw_tst_table + " where tst_id = ?;";
        insertStats_params = [tst_id];
    }
    
    cassconn.execute(insertStats, insertStats_params,{ prepare : true }, function(err, res, r) {
        if (err) {
            defer.reject(err);
            return;
        }
        if(res && res.rows){
            defer.resolve(res.rows);    
        }else{
            defer.resolve([]);
        }
    });
    return defer.promise;
}
module.exports = new CreateTest();