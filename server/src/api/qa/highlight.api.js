var configPath = require('../../configPath.js');
var config = require('../../config.js');

//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var mongodb = require('mongodb');
var authorizedItemsAPI = require(configPath.api.authorizeitems);
var debug = require('debug')('app:api:qa:HighlightText');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var NotesAPI = require(configPath.api.notes.notes);

let ew_usr_hlights_table = "ew1.ew_usr_hlights";

function HighlightText(){

}

HighlightText.prototype.saveHighlightText = async function(data){
    try{
        let { 
            usr_id,
            crs_id,
            mdl_id,
            src_id,
            hlight_id,
            hlight_src,
            select_txt,
            range,
            note,
            crt_dt 
        } = data;

        if(!hlight_id){
            hlight_id = Uuid.random()
        }

        var noteInfo = {}
        if(note){
            noteInfo = await NotesAPI.insertNote({
                usr_id,
                crs_id,
                mdl_id,
                src_id,
                note,
                hlight_id,
                note_src : hlight_src
            })
        }

        let query = `insert into ${ew_usr_hlights_table} (usr_id,crs_id,mdl_id,src_id,hlight_id,hlight_src,select_txt,range,crt_dt,note,note_id) values (?,?,?,?,?,?,?,?,?,?,?)`
        let params = [usr_id,crs_id,mdl_id,src_id,hlight_id,hlight_src,select_txt,range,new Date(),note,noteInfo.note_id];

        await cassExecute(query,params);

        return {
            ...data,
            hlight_id,
            note_id : noteInfo.note_id
        };

    }catch(err){
        throw new Error(err)
    }
}

HighlightText.prototype.removeHighlightText = async function(data){
    try{
        let { 
            usr_id,
            src_id,
            hlight_id
        } = data;

        let query = `delete from ${ew_usr_hlights_table} where usr_id = ? and src_id = ? and hlight_id = ?`
        let params = [usr_id,src_id,hlight_id];

        await cassExecute(query,params);

        return data;

    }catch(err){
        throw new Error(err);
    }
}

HighlightText.prototype.updateHighlightNote = async function(data){
    try{
        let { 
            usr_id,
            src_id,
            hlight_id,
            note_id,
            note
        } = data;

        await NotesAPI.updateNote({
            usr_id,
            src_id,
            note_id,
            note
        })

        let query = `update ${ew_usr_hlights_table} set note = ? where usr_id = ? and src_id = ? and hlight_id = ?`
        let params = [note,usr_id,src_id,hlight_id];

        await cassExecute(query,params);

        return data;

    }catch(err){
        throw new Error(err);
    }
}

HighlightText.prototype.getQuestionHighlights = async function(data){
    try{
        let { 
            usr_id,
            src_id
        } = data;

        let query = `select * from ${ew_usr_hlights_table} where usr_id = ? and src_id = ?`
        let params = [usr_id,src_id];

        let res = await cassExecute(query,params);

        return res;

    }catch(err){
        throw new Error(err);
    }
}

HighlightText.prototype.getQuestionHighlights = async function(data){
    try{
        let { 
            usr_id,
            src_id
        } = data;

        let query = `select * from ${ew_usr_hlights_table} where usr_id = ? and src_id = ?`
        let params = [usr_id,src_id];

        let res = await cassExecute(query,params);

        return res;

    }catch(err){
        throw new Error(err);
    }
}

HighlightText.prototype.getUserHighlights = async function(data){
    try{
        var connection = await getConnection();
        var col = await dbCollection(connection);
        var resArr = await col.find({userId : data.usr_id}).toArray();
        var items = resArr.map((v,i)=>v.itemId);
        var itemsDetails = await authorizedItemsAPI.getItemsUsingItemId({itemId : items});
        var itemMapping = {};
        itemsDetails.map((v,i)=>{
            itemMapping[v.itemId] = v;
        })

        var finalArr = resArr.map((v,i)=>{
            var obj = {};
            obj = Object.assign(obj,v,itemMapping[v.itemId]);
            return obj;
        })

        return finalArr;
    }catch(err){
        debug(err);
        throw err;
    }
}

function dbCollection(db) {
    return db.collection('highlightedText');
}

module.exports = new HighlightText();