var configPath = require('../../configPath.js');
var config = require('../../config.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var utility = require(configPath.lib.utility);
var Base64 = require(configPath.lib.utility).Base64;
var mongodb = require('mongodb');
var shortid = require('shortid');
var logger = require(configPath.lib.log_to_file);
var cassandra = require('cassandra-driver');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;
var oracledb = require(configPath.dbconn.oracledb);
var moment = require("moment");
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

var ew_verify_questions_table = "ew1.ew_verify_questions";
var ew_verify_questions_oracle = "ew_verify_questions";

//this will store exam_id , exam status and exam start time
const USER_EXAM_REDIS_KEY = (usr_id, topic_id) => {
    return usr_id + "-" + topic_id
}

// this will store original sequesnce of question in topic
const ORIGINAL_QUE_SEQ_REDIS_KEY = (topic_id) => {
    return topic_id + "-qid"
}

// this will store user question sequesnce for topic
const USER_QUE_SEQ_REDIS_KEY = (usr_id, topic_id) => {
    return usr_id + "-" + topic_id + "-qid"
}

// this will store user exam question data for exam
const USER_EXAM_QUESTION_DATA_REDIS_KEY = (usr_id, topic_id, time) => {
    time = moment(time).format("DD-MMM-YYYY-h:mm:ss")
    return usr_id + "-" + topic_id + "-" + time
}

//this will store skipped question ids
const USER_EXAM_SKIPPED_QUESTION_REDIS_KEY = (usr_id, topic_id, time) => {
    time = moment(time).format("DD-MMM-YYYY-h:mm:ss")
    return usr_id + "-" + topic_id + "-" + time + "-skipped"
}

//this will store review question ids
const USER_EXAM_REVIEW_QUESTION_REDIS_KEY = (usr_id, topic_id, time) => {
    time = moment(time).format("DD-MMM-YYYY-h:mm:ss")
    return usr_id + "-" + topic_id + "-" + time + "-review"
}

const USER_PRACTICE_QUESTION_DATA_REDIS_KEY = (usr_id, topic_id) => {
    return usr_id + "-" + topic_id
}

//this will store skipped question ids
const USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY = (usr_id, topic_id) => {
    return usr_id + "-" + topic_id + "-skipped"
}

//this will store review question ids
const USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY = (usr_id, topic_id) => {
    return usr_id + "-" + topic_id + "-review"
}

const PRACTICE_TOPIC_QUESTIONS_REDIS_KEY = (topic_id) => {
    return topic_id + "-qids"
}

class PracticeQuestion {
    
    /**
     * @param {JSON} data
     * @param {string} data.questionNumber - Question Number 
     * @param {string} data.questionId - Question Id
     * @param {string} data.question - Question Description.
     * @param {string} data.questionTime - Question Time.
     * @param {Object} data.options - Option contains id , text which will be option html/text
     * @param {string} data.textExplanation - Text Explanation
     * @param {Array}  data.ans - Encrypted answer id
     * @param {Array}  data.subjects - Subject in which this question comes
     * @param {Array}  data.authors - Related Authors
     * @param {Array}  data.publications - Related publications
     * @param {Array}  data.books - Related books
     * @param {Array}  data.exams - Related exams
     * @param {Array}  data.topics - Related topics
     * @param {Array}  data.topicGroup - Related topicGroup
     * @param {Array}  data.tags - Related tags
     * @param {string}  data.questionDifficulty - Question Difficulties
     * @returns {Promise}
     * @desc
     * - This method is used to save/update practice question
     * - If question contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text question to normal html, we will store original question html also for future editing {@link Utility#mathTypeSet}
     * - If any options contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text option to normal html, we will store original option html also for future editing {@link Utility#mathTypeSet}
     * - We will update/insert question to {@link MongoTables#practiceQuestions}
     * - Tables Used : {@link MongoTables#practiceQuestions}     
     */
    saveQuestion(data) {
        return getConnection()
                .then(dbCollection)
                .then(saveQue)
                .catch(globalFunctions.err);

        async function saveQue(col) {
            if (!data.questionId) {
                data.questionId = shortid.generate();
                
            }

            if(data.isNewQuestion){
                data.create_ts = new Date()
            }else{
                data.update_ts = new Date()
            }
            
            
            if(data.isNewQuestion){
                var currentData = await col.find({"topicGroup" : {"$in" : [data.topicGroup[0]] }}).sort({"questionNumber":-1}).toArray();
                var nextQuestionId = "";
                var nextQuestionNumber = "";
                if(currentData && currentData[0]){
                    nextQuestionId = data.topicGroup[0] + "-" + (currentData[0].questionNumber + 1)
                    nextQuestionNumber = currentData[0].questionNumber + 1
                }else{
                    nextQuestionId = data.topicGroup[0] + "-" + 1
                    nextQuestionNumber = 1
                }
                data.questionId = nextQuestionId;
                data.questionNumber = nextQuestionNumber
                logger.debug("question going to be saved as : ",nextQuestionId)
            }
            

            

            try {
                var originalQuestion = data.question;
                data.originalQuestion = originalQuestion;
                var mathRes = await utility.mathTypeSet(data.question);
                data.question = mathRes || data.question;
            } catch (err) {
                logger.debug(err);
            }

            try {
                var originalTextExplanation = data.textExplanation;
                data.originalTextExplanation = originalTextExplanation;
                var processed = await utility.mathTypeSet(data.textExplanation);
                data.textExplanation = processed || data.textExplanation;
            } catch (err) {
                logger.debug(err);
            }

            try {
                for (var i = 0; i < data.options.length; i++) {
                    let opt = data.options[i];
                    let processed = await utility.mathTypeSet(opt.text);
                    data.options[i].originalText = opt.text;
                    data.options[i].text = processed || opt.text;
                }
            } catch (err) {
                logger.debug(err);
            }
            

            delete data["isNewQuestion"]
            return col.update({
                questionId: data.questionId
            }, { "$set" : data } , {
                upsert: true
            });
        }
    }
    
    /**
     * @param {JSON} data
     * @param {string} data.questionNumber - Question Number 
     * @param {string} data.questionId - Question Id
     * @param {string} data.question - Question Description.
     * @param {string} data.questionTime - Question Time.
     * @param {Object} data.options - Option contains id , text which will be option html/text
     * @param {string} data.textExplanation - Text Explanation
     * @param {Array}  data.ans - Encrypted answer id
     * @param {Array}  data.subjects - Subject in which this question comes
     * @param {Array}  data.authors - Related Authors
     * @param {Array}  data.publications - Related publications
     * @param {Array}  data.books - Related books
     * @param {Array}  data.exams - Related exams
     * @param {Array}  data.topics - Related topics
     * @param {Array}  data.topicGroup - Related topicGroup
     * @param {Array}  data.tags - Related tags
     * @param {string}  data.questionDifficulty - Question Difficulties
     * @returns {Promise}
     * @desc
     * - This method is used to save/update preview practice question
     * - If question contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text question to normal html, we will store original question html also for future editing {@link Utility#mathTypeSet}
     * - If any options contains MathML/Latex/Tex code then we will convert MathMl/Latex/Text option to normal html, we will store original option html also for future editing {@link Utility#mathTypeSet}
     * - We will update/insert question to {@link MongoTables#practiceQuestions}
     * - Tables Used : {@link MongoTables#practiceQuestions}     
     */
    savePreviewQuestion(data) {
        return getConnection()
                .then(dbColl)
                .then(saveQue)
                .catch(globalFunctions.err);
        
        function dbColl(db){
            return db.collection('practicePreviewQuestions')
        }

        async function saveQue(col) {
            if (!data.questionId) {
                data.questionId = shortid.generate();
                
            }

            if(data.isNewQuestion){
                data.create_ts = new Date()
            }else{
                data.update_ts = new Date()
            }
            
            
            if(data.isNewQuestion){
                var currentData = await col.find({"topicGroup" : {"$in" : [data.topicGroup[0]] }}).sort({"questionNumber":-1}).toArray();
                var nextQuestionId = "";
                if(currentData && currentData[0]){
                    nextQuestionId = data.topicGroup[0] + "-" + (currentData[0].questionNumber + 1)
                }else{
                    nextQuestionId = data.topicGroup[0] + "-" + 1
                }
                data.questionId = nextQuestionId;
            }
            
            try {
                var originalQuestion = data.question;
                data.originalQuestion = originalQuestion;
                var mathRes = await utility.mathTypeSet(data.question);
                data.question = mathRes || data.question;
            } catch (err) {
                logger.debug(err);
            }

            try {
                var originalTextExplanation = data.textExplanation;
                data.originalTextExplanation = originalTextExplanation;
                var processed = await utility.mathTypeSet(data.textExplanation);
                data.textExplanation = processed || data.textExplanation;
            } catch (err) {
                logger.debug(err);
            }

            try {
                for (var i = 0; i < data.options.length; i++) {
                    let opt = data.options[i];
                    let processed = await utility.mathTypeSet(opt.text);
                    data.options[i].originalText = opt.text;
                    data.options[i].text = processed || opt.text;
                }
            } catch (err) {
                logger.debug(err);
            }
            

            delete data["isNewQuestion"]
            return col.update({
                questionId: data.questionId
            }, { "$set" : data } , {
                upsert: true
            });
        }
    }

    findPreviewQuestionsByQuery(data) {
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                if (data[key].length > 0) {
                    data[key] = {
                        $in: data[key]
                    }
                } else {
                    delete data[key];
                }
            }
        }
        //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}
        
        function dbColl(db){
            return db.collection('practicePreviewQuestions')
        }

        return getConnection().then(dbColl).then(find).catch(globalFunctions.err);

        function find(col) {
            return col.find(data).toArray();
        }
    }

    findQuestionsByQuery(data) {

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                if (data[key].length > 0) {
                    data[key] = {
                        $in: data[key]
                    }
                } else {
                    delete data[key];
                }
            }
        }
        //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}

        return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

        function find(col) {
            return col.find(data).toArray();
        }
    }


    findQuestionsTextByQuery(data) {
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                if (data[key].length > 0) {
                    data[key] = {
                        $in: data[key]
                    }
                } else {
                    delete data[key];
                }
            }
        }
        //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}

        return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

        function find(col) {
            return col.find(data, {questionNumber : 1, questionId : 1 , question : 1 }).toArray();
        }
    }


    // find questions id and name by query
    findQuestionsIdByQuery(data) {

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                if (data[key].length > 0) {
                    data[key] = {
                        $in: data[key]
                    }
                } else {
                    delete data[key];
                }
            }
        }
        //example : {'publications':{ "$in" :['S1-5MnzX']},'subjects':{ "$in" :['angularjs']}}

        return getConnection().then(dbCollection).then(find).catch(globalFunctions.err);

        function find(col) {
            return col.find(data, {questionNumber : 1, questionId : 1 }).toArray();
        }
    }



    removeQuestion(data) {
        return getConnection().then(dbCollection).then(removeQue).catch(globalFunctions.err);

        function removeQue(col) {
            return col.remove({
                questionId: data.questionId
            });
        }
    }

    getAllQuestions(data) {
        return getConnection().then(dbCollection).then(getAllQues).catch(globalFunctions.err);

        function getAllQues(col) {
            return col.find({}).toArray();
        }
    }

    getPracticeQuestionsById(data) {
        return getConnection().then(dbCollection).then(getAllQues).catch(globalFunctions.err);

        function getAllQues(col) {
            return col.find({
                questionId: {
                    $in: data.questionsId
                }
            }).toArray();
        }
    }

    async getPracticeQuestionsByLimit(data) {

        try {
            var {
                start,
                no_of_question,
                questionsId
            } = data;
            var db = await getConnection();
            var col = await dbCollection(db);
            var cursor = await col.find({
                questionId: {
                    $in: questionsId || []
                }
            });
            if (start) {
                cursor = cursor.skip(start || 0);
            }
            var questions = await cursor.limit(no_of_question || 0).toArray();
            return questions
        } catch (err) {
            logger.debug(err)
            throw new Error(err);
        }

        return getConnection().then(dbCollection).then(getAllQues).catch(globalFunctions.err);

        function getAllQues(col) {
            return col.find({
                questionId: {
                    $in: data.questionsId
                }
            }).toArray();
        }
    }

    updateMultiplePracticeQuestions(data) {
        return getConnection().then(dbCollection).then(updateAllQues).catch(globalFunctions.err);

        function updateAllQues(col) {
            for (var i = data.length - 1; i >= 0; i--) {
                var question = data[i];
                console.log(data[i]);
                col.update({
                    questionId: question.questionId
                }, {
                    $set: {
                        topics: data[i].topics
                    }
                });
            }
            return [];
        }
    }

    async updateVerifyPracticeQuestion(data) {
        try {
            let previous_verify_id = data.previous_verify_id;
            let verify_id = data.verify_id || TimeUuid.now();
            let q_id = data.q_id;
            let tester_id = data.tester_id || '';
            let tested_dt = data.tested_dt ?
                new Date(data.tested_dt) :
                undefined;
            let test_status = data.test_status || '';
            let test_comments = data.test_comments || '';
            let q_state_start_dt = data.q_state_start_dt ?
                new Date(data.q_state_start_dt) :
                undefined;
            let q_state_end_dt = data.q_state_end_dt ?
                new Date(data.q_state_end_dt) :
                undefined;

            var queries = [];

            let cass_query = "select max(verify_id) from " + ew_verify_questions_table + " where q_id=?";
            let cass_params = [q_id];

            var prev_rows = await cassExecute(cass_query, cass_params);
            if (prev_rows && prev_rows[0] && prev_rows[0]["system.max(verify_id)"] !== null) {
                var prev_uuid = prev_rows[0]["system.max(verify_id)"];
                cass_query = "update " + ew_verify_questions_table + " set q_state_end_dt=? where verify_id=? and q_id=?";
                cass_params = [new Date(), prev_uuid, q_id];
                await cassExecute(cass_query, cass_params);
            }

            cass_query = "update " + ew_verify_questions_table + " set tester_id=?,tested_dt=?,test_status=?,test_comments=?,q_state_start_dt=? where verify_id=? and q_id=?";
            cass_params = [
                tester_id,
                tested_dt,
                test_status,
                test_comments,
                q_state_start_dt,
                verify_id,
                q_id
            ];
            queries.push({
                query: cass_query,
                params: cass_params
            })
            await cassBatch(queries);

            let oracle_query = "select max(verify_id) from " + ew_verify_questions_oracle + " where q_id='" + q_id + "'";
            var conn = await oracledb.getConnection();
            var res = await conn.execute(oracle_query, [], {
                autoCommit: true
            });
            if (res.rows && res.rows[0] && res.rows[0][0]) {
                var prev_uuid = res.rows[0][0];
                oracle_query = `update ${ew_verify_questions_oracle} set q_state_end_dt=${ "to_date('" + moment().format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')"} where verify_id=${ "'" + prev_uuid + "'"} and q_id=${ "'" + q_id + "'"}`;
                await conn.execute(oracle_query, [], {
                    autoCommit: true
                });
            }
            await oracledb.releaseConnection(conn);

            oracle_query = `insert into ${ew_verify_questions_oracle} (verify_id,tester_id,test_status,test_comments,q_state_start_dt,q_id) values (${ "'" + verify_id + "'"},${ "'" + tester_id + "'"},${ "'" + test_status + "'"},${ "'" + test_comments + "'"},${ "to_date('" + moment(q_state_start_dt).format('DD MMM YYYY, hh:mm:ss') + "','DD MON YYYY, HH24:MI:SS')"},${ "'" + q_id + "'"})`
            let oracle_params = [
                '' + verify_id,
                tester_id,
                tested_dt,
                '' + test_status,
                '' + test_comments,
                q_state_start_dt,
                q_id
            ];
            var conn = await oracledb.getConnection();
            var res = await conn.execute(oracle_query, [], {
                autoCommit: true
            });
            await oracledb.releaseConnection(conn);

            return data;

        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getAllVerifyStatusOfPracticeQuestion(data) {
        try {
            let q_id = data.q_id;

            let cass_query = "select * from " + ew_verify_questions_table + " where q_id=?";
            let cass_params = [q_id];

            let res = await cassExecute(cass_query, cass_params);

            return res;

        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getLatestVerifyStatusOfGivenPracticeQuestions(data) {
        try {
            let q_id = [].concat(data.q_id);

            var id_map = {};
            for (var i = 0; i < q_id.length; i++) {
                var que_id = q_id[i];
                let cass_query = "select max(verify_id) from " + ew_verify_questions_table + " where q_id=?";
                let cass_params = [que_id];
                let res = await cassExecute(cass_query, cass_params);
                if (res && res[0] && res[0]["system.max(verify_id)"] !== null) {
                    id_map[que_id] = res[0]["system.max(verify_id)"];
                }
            }

            logger.debug(id_map);

            var rows = [];

            for (let q in id_map) {
                let cass_query = "select * from " + ew_verify_questions_table + " where q_id=? and verify_id=?";
                let cass_params = [q, id_map[q]];
                var row = await cassExecute(cass_query, cass_params);
                rows.push(row[0]);
            }

            return rows;
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async startUserExam(data) {
        try {
            var {
                usr_id,
                usr_exam_id,
                topic_id,
                exam_start_time,
                status,
                seqOrRand
            } = data;

            var prv_exam = await this.checkForPreviousExam({
                usr_id,
                topic_id
            });
            logger.debug("prv_exam...", prv_exam)
            if (prv_exam && prv_exam.previousExamRunning) {
                //if prev exam running and now user want to start new exam
                //delete all exam data
                await this.finishUserExam({
                    usr_id: usr_id,
                    topic_id: topic_id,
                    usr_exam_id: prv_exam.exam_id
                })
            }

            //set status of exam is running
            await redisCli.hmsetAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id), {
                "exam_id": "" + usr_exam_id,
                "status": status || "running",
                "start_time": "" + exam_start_time
            });

            //get original question sequence for this topic
            var questionSeq = await redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);
            var shuffledUserQueSeq;
            if (!seqOrRand || seqOrRand === "random") {
                // if we want random question sequence
                shuffledUserQueSeq = utility.shuffleArray([].concat(questionSeq || []));
            } else if (seqOrRand === "sequence") {
                // if we want sequential question sequence as it is
                shuffledUserQueSeq = questionSeq
            }
            //delete user quesion seq
            await redisCli.delAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id));
            //set shuffled user question sequence for this topic
            await redisCli.rpushAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), (shuffledUserQueSeq || []));
            //get first question for user
            var firstQuestion = await redisCli.hgetallAsync(shuffledUserQueSeq[0]);
            if (firstQuestion && firstQuestion != 'null' && firstQuestion.json) {
                firstQuestion = JSON.parse(firstQuestion.json);
            } else {
                firstQuestion = {}
            }
            let q_data = await this.getUserExamQuestionData({
                usr_id: usr_id,
                exam_id: usr_exam_id,
                questionId: firstQuestion.questionId,
                topic_id: topic_id
            })
            q_data = q_data || {}

            return {
                totalQuestions: shuffledUserQueSeq.length,
                exam_id: usr_exam_id,
                usr_id: usr_id,
                firstQuestion: {
                    ...firstQuestion,
                    ...q_data
                }
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async finishUserExam(data) {
        try {
            var {
                usr_id,
                usr_exam_id,
                topic_id
            } = data;

            var exam = await this.getUserExamDetails({
                usr_id,
                topic_id
            });
            var user_question_data = await redisCli.hgetallAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, exam.start_time));
            var exam_question_ids = await redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);
            logger.debug("exam_question_ids...", exam_question_ids)
            var ques = {};
            for (var i = 0; i < exam_question_ids.length; i++) {
                var q_id = exam_question_ids[i];
                logger.debug("q_id...", q_id)
                var question = await redisCli.hgetallAsync(q_id);
                if (question === 'null' || !question) {
                    question = {};
                }
                ques[q_id] = JSON.parse(question.json || {});
            }
            var result_exam = this.checkQuestionAnswers(ques, user_question_data);

            //delete exam data for user
            await redisCli.delAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id));
            //delete user quesion seq
            await redisCli.delAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id));
            // //delete all user exam question data
            // await redisCli.delAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id,usr_exam_id))
            return result_exam
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async checkQuestionAnswers(questions, user_question_data) {
        try {
            var wrong_ans = [];
            var correct_ans = [];
            var skipped_que = [];
            var review_que = [];
            var attempted_que = Object.keys(user_question_data || {});
            var totalQuestions = Object.keys(questions || {});
            for (let id in user_question_data) {
                var question = questions[id];
                var usr_data = JSON.parse(user_question_data[id]);
                var ans = (question.ans || []).map((v, i) => {
                    return Base64.decode(v)
                });
                var userAnswers = usr_data.userAnswers || [];

                logger.debug("ans...userAnswers..", ans, userAnswers)

                if (usr_data.isSkipped) {
                    skipped_que.push(question);
                    continue;
                }
                if (usr_data.markForReview) {
                    review_que.push(question);
                }

                var isRightAnswer = true;
                if (userAnswers.length != ans.length) {
                    isRightAnswer = false;
                } else {
                    if (ans.length === 1) {
                        //if there is only one ans
                        isRightAnswer = (userAnswers[0] === ans[0])
                    } else {
                        //if there are multiple answers
                        for (var i = 0; i < userAnswers.length; i++) {
                            //check for all user answers to match real answers
                            var t_ans = userAnswers[i]
                            if (ans.indexOf(t_ans) <= -1) {
                                isRightAnswer = false;
                                break
                            }
                        }
                    }
                }
                if (isRightAnswer) {
                    correct_ans.push(question)
                } else {
                    wrong_ans.push(question)
                }
            }
            return {
                wrong_ans: wrong_ans,
                correct_ans: correct_ans,
                attempted_que: attempted_que,
                skipped_que: skipped_que,
                review_que: review_que,
                totalQuestions: totalQuestions
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getUserExamDetails(data) {
        try {
            var {
                usr_id,
                topic_id
            } = data;
            var exam = await redisCli.hgetallAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id));
            exam = exam || {}
            if (exam === 'null') {
                exam = {}
            }
            return exam;
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async checkForPreviousExam(data) {
        try {
            var {
                usr_id,
                topic_id
            } = data;
            var res = await redisCli.hgetallAsync(USER_EXAM_REDIS_KEY(usr_id, topic_id));

            if (res && res.exam_id && res.status) {
                //if prev exam existing
                //get original question sequence for this topic
                var questionSeq = await redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);
                //get user question sequence for this topic
                var userQuestionSeq = await redisCli.lrangeAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), 0, -1);
                //get user exam question data for exam
                var userExamQuestionData = await redisCli.hgetallAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, res.start_time));

                var attemptedQuestions = Object.keys(userExamQuestionData || {});

                var nextQuestionIndex = attemptedQuestions.length || 0;

                if (attemptedQuestions.length === userQuestionSeq.length) {
                    nextQuestionIndex = userQuestionSeq.length - 1
                }

                var nextQuestionId = userQuestionSeq[nextQuestionIndex];
                //get question
                var que = await redisCli.hgetallAsync(nextQuestionId);
                logger.debug(".....", que)
                if (que && que !== 'null' && que.json) {
                    que = JSON.parse(que.json);
                } else {
                    que = {}
                }

                //get question data
                let q_data = await this.getUserExamQuestionData({
                    usr_id: usr_id,
                    exam_id: res.exam_id,
                    questionId: que.questionId,
                    topic_id: topic_id
                })
                q_data = q_data || {}

                return {
                    previousExamRunning: true,
                    exam_id: res.exam_id,
                    totalQuestions: (userQuestionSeq || []).length,
                    question: {
                        ...que,
                        ...q_data
                    },
                    questionIndex: nextQuestionIndex
                }
            } else {
                return {
                    previousExamRunning: false
                }
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async saveUserExamQuestionData(data) {
        try {
            var {
                usr_id,
                topic_id,
                questionId,
                exam_id,
                questionData
            } = data;
            if (!questionId) {
                return;
            }
            var exam = await this.getUserExamDetails({
                usr_id,
                topic_id
            })

            var jsonQData = JSON.parse(questionData || {});
            var isReview = jsonQData["markForReview"];
            var isSkipped = jsonQData["isSkipped"];

            if (isSkipped) {
                await redisCli.saddAsync(USER_EXAM_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);
            } else {
                await redisCli.sremAsync(USER_EXAM_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);
            }

            if (isReview) {
                await redisCli.saddAsync(USER_EXAM_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);
            } else {
                await redisCli.sremAsync(USER_EXAM_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);
            }
            await redisCli.hmsetAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId, questionData);
            return {
                questionId,
                usr_id,
                exam_id
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getUserExamQuestionData(data) {
        try {
            var {
                usr_id,
                questionId,
                exam_id,
                topic_id
            } = data;
            var exam = await this.getUserExamDetails({
                usr_id,
                topic_id
            })

            var q_data = await redisCli.hmgetAsync(USER_EXAM_QUESTION_DATA_REDIS_KEY(usr_id, topic_id, exam.start_time), questionId);
            logger.debug(q_data[0])
            if (!q_data[0] || q_data[0] === 'null') {
                q_data = {};
            } else {
                q_data = JSON.parse(q_data[0])
            }
            logger.debug(q_data)
            return q_data
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getUserExamQuestion(data) {
        try {
            var {
                usr_id,
                topic_id,
                questionIndex,
                exam_id,
                type
            } = data;
            var exam = await this.getUserExamDetails({
                usr_id,
                topic_id
            })
            var userQuestionSeq;
            if (type === "skipped") {
                //get skipped user question sequence for this topic
                userQuestionSeq = await redisCli.smembersAsync(USER_EXAM_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time));
            } else if (type === "review") {
                //get user review question sequence for this topic
                userQuestionSeq = await redisCli.smembersAsync(USER_EXAM_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id, exam.start_time));
            } else {
                //get user question sequence for this topic
                userQuestionSeq = await redisCli.lrangeAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), 0, -1);
            }

            let ret_res = {};
            if (userQuestionSeq != 'null' && userQuestionSeq && userQuestionSeq[questionIndex]) {
                let question = await redisCli.hgetallAsync(userQuestionSeq[questionIndex]);
                question = question || {};
                question = JSON.parse(question.json || {});
                let q_data = await this.getUserExamQuestionData({
                    usr_id: usr_id,
                    exam_id: exam_id,
                    questionId: question.questionId,
                    topic_id: topic_id
                })
                q_data = q_data || {}
                ret_res = {
                    question: {
                        ...question,
                        ...q_data
                    },
                    questionIndex: questionIndex,
                    totalQuestions: (userQuestionSeq || []).length
                }
            } else {
                ret_res = {
                    totalQuestions: 0,
                    questionIndex: questionIndex,
                    question: {}
                };
            }
            return ret_res
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getQuestionUsingIdFromRedis(data) {
        try {
            let {
                q_id
            } = data;
            if (!q_id) {
                return
            }
            var res = await redisCli.hgetallAsync(q_id);
            if (res !== 'null' && res) {
                res.json = JSON.parse(res.json);
                return res;
            }
            return;
        } catch (err) {
            logger.debug(err)
        }
    }

    async startUserPractice(data) {
        var {
            usr_id,
            topic_id,
            no_of_question,
            seqOrRand
        } = data;

        try {
            seqOrRand = seqOrRand || "sequence"
            var no_of_attempted_questions = 0;//await this.noOfAlreadyAttemptedUserPracticeQuestion(data);
            var totalQuestions = await redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), 0, -1);
            if (no_of_attempted_questions >= totalQuestions.length) {
                no_of_attempted_questions--;
            }
            var questionSeq = await redisCli.lrangeAsync(ORIGINAL_QUE_SEQ_REDIS_KEY(topic_id), no_of_attempted_questions || 0, -1);
            var shuffledUserQueSeq;
            if (seqOrRand === "random") {
                // if we want random question sequence
                shuffledUserQueSeq = utility.shuffleArray([].concat(questionSeq || []));
            } else if (seqOrRand === "sequence") {
                // if we want sequential question sequence as it is
                shuffledUserQueSeq = questionSeq
            }
            //delete user quesion seq
            await redisCli.delAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id));
            //set shuffled user question sequence for this topic
            await redisCli.rpushAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), (shuffledUserQueSeq || []));
            let questions = []
            for (var i = 0; i < (no_of_question || 5); i++) {
                var question = await this.getQuestionUsingIdFromRedis({
                    q_id: shuffledUserQueSeq[i]
                });
                var q_data = await this.getUserPracticeQuestionData({
                    usr_id,
                    topic_id,
                    questionId: shuffledUserQueSeq[i]
                })
                if (question) {
                    var j = question.json || {}
                    q_data = q_data || {}
                    questions.push({
                        ...j,
                        ...q_data
                    })
                }
            }
            return {
                attemptedQuestions: no_of_attempted_questions,
                questions
            }
        } catch (err) {
            logger.debug(err)
            throw new Error(err);
        }
    }

    async fetchNextUserPracticeQuestion(data) {
        var {
            usr_id,
            topic_id,
            start,
            no_of_question,
            question_type
        } = data;

        try {
            var questionSeq;
            if (question_type === "skipped") {
                questionSeq = await redisCli.smembersAsync(USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id));
            } else if (question_type === "review") {
                questionSeq = await redisCli.smembersAsync(USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id));
            } else {
                questionSeq = await redisCli.lrangeAsync(USER_QUE_SEQ_REDIS_KEY(usr_id, topic_id), start || 0, -1);
            }

            if (questionSeq === 'null' || !questionSeq) {
                return [];
            }

            let questions = []
            for (var i = 0; i < (no_of_question || 5); i++) {
                var question = await this.getQuestionUsingIdFromRedis({
                    q_id: questionSeq[i]
                });
                var q_data = await this.getUserPracticeQuestionData({
                    usr_id,
                    topic_id,
                    questionId: questionSeq[i]
                })
                if (question) {
                    var j = question.json || {}
                    q_data = q_data || {}
                    questions.push({
                        ...j,
                        ...q_data
                    })
                }
            }
            return {
                questions,
                totalQuestions: questions.length
            }
        } catch (err) {
            logger.debug(err)
            throw new Error(err);
        }
    }

    async noOfAlreadyAttemptedUserPracticeQuestion(data) {
        try {
            let {
                usr_id,
                topic_id
            } = data;
            var q_data = await redisCli.hgetallAsync(USER_PRACTICE_QUESTION_DATA_REDIS_KEY(usr_id, topic_id));
            if (q_data && q_data !== 'null') {
                var arr = Object.keys(q_data || {});
                return arr.length
            }
            return 0;
        } catch (err) {
            logger.debug(err);
            throw new Error(err)
        }
    }

    async getUserPracticeQuestionData(data) {
        try {
            var {
                usr_id,
                questionId,
                topic_id
            } = data;

            var q_data = await redisCli.hmgetAsync(USER_PRACTICE_QUESTION_DATA_REDIS_KEY(usr_id, topic_id), questionId);
            q_data = q_data[0]
            if (!q_data || q_data === 'null') {
                q_data = undefined;
            } else {
                q_data = JSON.parse(q_data)
            }
            return q_data
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }

    async getPracticeNodeQuestions(data) {
        try {
            let {
                node,
                topic_id
            } = data;
            var nodeContent = await redisCli.hgetAsync(PRACTICE_TOPIC_QUESTIONS_REDIS_KEY(topic_id), '#' + node); // Q1,Q2,Q3,G1...
            if (nodeContent === 'null') {
                return [];
            }
            nodeContent = nodeContent.split(","); // [Q1,Q2,Q3,G1]
            let questionIDs = await redisCli.hmgetAsync(PRACTICE_TOPIC_QUESTIONS_REDIS_KEY(topic_id), nodeContent); // ['id','id','id','id,id,id']
            var questions = []
            for (var i = 0; i < questionIDs.length; i++) {
                let ids = questionIDs[i].split(",");
                if (ids && ids.length > 1) {
                    for (let j = 0; j < ids.length; j++) {
                        var id = ids[j];
                        if (id === 'null')
                            continue
                        var que = await this.getQuestionUsingIdFromRedis({
                            q_id: id
                        });
                        if (que != 'null' && que.json) {
                            que.json.group = true;
                            que.json.groupId = nodeContent[i]
                            questions.push(que.json);
                        }
                    }
                } else {
                    var id = ids[0];
                    if (id === 'null')
                        continue
                    var que = await this.getQuestionUsingIdFromRedis({
                        q_id: id
                    });
                    if (que && que != 'null') {
                        questions.push(que.json);
                    }
                }
            }
            return {
                questions
            }
        } catch (err) {
            logger.debug(err);
        }
    }

    async saveUserPracticeQuestionData(data) {
        try {
            var {
                usr_id,
                topic_id,
                questionId,
                questionData
            } = data;
            if (!questionId) {
                return;
            }

            var jsonQData = JSON.parse(questionData || {});
            var isReview = jsonQData["markForReview"];
            var isSkipped = jsonQData["isSkipped"];

            if (isSkipped) {
                await redisCli.saddAsync(USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);
            } else {
                await redisCli.sremAsync(USER_PRACTICE_SKIPPED_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);
            }

            if (isReview) {
                await redisCli.saddAsync(USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);
            } else {
                await redisCli.sremAsync(USER_PRACTICE_REVIEW_QUESTION_REDIS_KEY(usr_id, topic_id), questionId);
            }

            await redisCli.hmsetAsync(USER_PRACTICE_QUESTION_DATA_REDIS_KEY(usr_id, topic_id), questionId, questionData);
            return {
                questionId,
                usr_id
            }
        } catch (err) {
            logger.debug(err);
            throw new Error(err);
        }
    }
}

function dbCollection(db) {
    return db.collection('practiceQuestions');
}

module.exports = new PracticeQuestion();