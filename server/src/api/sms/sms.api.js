
var do_request = require('request');
var debug = require('debug')("app:api:sms:sms");
var configPath = require('../../configPath.js');
var textLocalConfig = require(configPath.config.textLocalConfig);
var logger = require(configPath.lib.log_to_file);
var Q = require("q");

function SMS(){
	this.sendSMSAPI = textLocalConfig.API.SEND_SMS;
	this.apiKey = textLocalConfig.API_KEY;
	this.sender = textLocalConfig.SENDER;
	this.isTestMode = false;
	this.format = "json";
}

SMS.prototype.sendSMS = function(data){
	var defer = Q.defer();

	//data = {message , numbers}
	
	var obj = {
		apiKey : this.apiKey,
		sender : this.sender,
		message : data.message,
		numbers : data.numbers,
		format : this.format,
		test : this.isTestMode
	}

	do_request.post({
		url : this.sendSMSAPI,
		form : obj
	},function(err,msg,res){
		res = JSON.parse(res);
		if(err){
			logger.debug(err);
			defer.reject(err);
		}else{
			logger.debug(res);
			if(res.status === "failure"){
				defer.reject(res);	
			}else{
				defer.resolve(res);	
			}
		}
	})

	return defer.promise;
}

SMS.prototype.firstTimeLogin = async function(numbers,usr_nm,otp){
	try{
		var message = `Dear User, I'm so glad you decided to try out examwarrior.com. 
Use below OTP for first time login.
Username: ${usr_nm}
OTP: ${otp}
Happy learning. 
For any questions, please contact us on support@examwarrior.com.`;

		var smsres = await this.sendSMS({
			message : message,
			numbers : numbers
		});
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SMS.prototype.paymentFailureSMS = async function(numbers,training){
	try{
		var message = `From examwarrior.com.Dear User, payment for ${training} Failed. Can you please check your payment details. For any issues call us on 8197624885 or email us on support@examwarrior.com`;

		var smsres = await this.sendSMS({
			message : message,
			numbers : numbers
		});
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SMS.prototype.paymentStartedSMS = async function(numbers){
	try{
		var message = `From examwarrior.com. Dear Customer , If any issue in doing payment call us on 918197624885 or email us on support@examwarrior.com`;
		var smsres = await this.sendSMS({
			message : message,
			numbers : numbers
		});
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SMS.prototype.sendSuccessfulBuySMS = async function(numbers,training,usr_id){
	try{
		var message = "From examwarrior.com. Dear Customer , Payment Successful.Thank you for purchasing " + training + "\nStart Learning now. \nYour UserID : " + usr_id;
		var smsres = await this.sendSMS({
			message : message,
			numbers : numbers
		});
	}catch(err){
		logger.debug(err);
		throw err;
	}
}

SMS.prototype.sendForgotPasswordSMS = async function(numbers,usr_id,otp){
	try{
		var msg = "From examwarrior.com . OTP to change password is " + otp + "\nYour UserID : " + usr_id;
		var smsres = await this.sendSMS({
			message : msg,
			numbers : numbers
		});
	}catch(err){
		logger.debug(err);
		throw err;
	}
}


module.exports = new SMS();