var configPath = require('../../configPath.js');

var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);

var cassconn = require(configPath.dbconn.cassconn);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var config = require('../../config.js');
var Q = require('q');
var debug = require('debug')('app:api:dashboard:stats');

var ew_dashboard_stats_tbl = "ew1.ew_dashboard_stats";

class DashboardStats {
    async getAll(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "select * from " + ew_dashboard_stats_tbl + " where usr_id = ?";
            var params = [usr_id];
            var res = await cassExecute(qry, params, {
                prepare: true
            });
            if (res.length === 1) {
                res = res[0];
            }
            return res;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async updateBookmarkCounter(data) {
        try {
            var usr_id = data.usr_id;
            var no_of_bookmarks = data.no_of_bkmrks; //can be positive or negitive
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_bkmrks=no_of_bkmrks+ " + no_of_bookmarks + " where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async incrementNotesCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_notes=no_of_notes+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async decrementNotesCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_notes=no_of_notes-1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose question asked count we want to increment
     * @returns {Promise}
     * @desc
     * - Increment Dashboard Question Asked counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async incrementForumQuestionAskedCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ask=no_of_qa_que_ask+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose question asked count we want to decrement
     * @returns {Promise}
     * @desc
     * - Decrement Dashboard Question Asked counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async decrementForumQuestionAskedCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ask=no_of_qa_que_ask-1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose question answered count we want to increment
     * @returns {Promise}
     * @desc
     * - Increment Dashboard Question Answered counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async incrementForumQuestionAnsweredCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ans=no_of_qa_que_ans+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose question answered count we want to decrement
     * @returns {Promise}
     * @desc
     * - decrement Dashboard Question Answered counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async decrementForumQuestionAnsweredCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_ans=no_of_qa_que_ans-1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose QA question views count we want to increment
     * @returns {Promise}
     * @desc
     * - Increment Dashboard QA Question views counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async incrementForumQuestionViewsCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_que_vws=no_of_qa_que_vws+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose QA question Comment count we want to increment
     * @returns {Promise}
     * @desc
     * - Increment Dashboard QA Question Comment counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async incrementForumQuestionCommentCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_cmnts=no_of_qa_cmnts+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose QA question Comment count we want to decrement
     * @returns {Promise}
     * @desc
     * - decrement Dashboard QA Question Comment counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async decrementForumQuestionCommentCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_qa_cmnts=no_of_qa_cmnts-1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose CB Comment count we want to increment
     * @returns {Promise}
     * @desc
     * - Increment Dashboard CB Comment counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async incrementCommentCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_cmnts=no_of_cmnts+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose CB Comment count we want to Decrement
     * @returns {Promise}
     * @desc
     * - Decrement Dashboard CB Comment counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async decrementCommentCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_cmnts=no_of_cmnts-1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose CB Post count we want to increment
     * @returns {Promise}
     * @desc
     * - Increment Dashboard CB Post counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async incrementPostCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_psts=no_of_psts+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose CB Post count we want to Decrement
     * @returns {Promise}
     * @desc
     * - Decrement Dashboard CB Post counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async decrementPostCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_psts=no_of_psts-1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose friends count we want to increment
     * @returns {Promise}
     * @desc
     * - Increment Dashboard Stats Friends counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async incrementFriendCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_frds=no_of_frds+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User Id whose friends count we want to decrement
     * @returns {Promise}
     * @desc
     * - Decrement Dashboard Stats Friends counter
     * - Tables Used : {@link CassandraTables#ew_dashboard_stats}
     */
    async decrementFriendCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_frds=no_of_frds-1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async incrementVideoViews(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_vdo_vws=no_of_vdo_vws+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async incrementQuestionLearnedViews(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_learned=no_of_que_learned+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async incrementTestTakenCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_tsts_taken=no_of_tsts_taken+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async incrementNoOfQuestionPracticedCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_practiced=no_of_que_practiced+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async incrementNoOfQuestionAttemptedInTestCounter(data) {
        try {
            var usr_id = data.usr_id;
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_attempt_tsts=no_of_que_attempt_tsts+1 where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async updateNoOfCorrectQuestionAttemptedCounter(data) {
        try {
            var usr_id = data.usr_id;
            var count = data.count || 0
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_attempt_correct = no_of_que_attempt_correct + " + count + "where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
    async updateNoOfWrongQuestionAttemptedCounter(data) {
        try {
            var usr_id = data.usr_id;
            var count = data.count || 0
            var qry = "update " + ew_dashboard_stats_tbl + " set no_of_que_attempt_wrong = no_of_que_attempt_wrong + " + count + "where usr_id = ?";
            var params = [usr_id];
            await cassExecute(qry, params, {
                prepare: true
            });
            return data;
        } catch (e) {
            debug(e);
            throw e;
        }
    }
}
module.exports = new DashboardStats();