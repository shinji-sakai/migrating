var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');

var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:profile");
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var mailAPI = require(configPath.api.mail);
var chatFriendAPI = require(configPath.api.chat.friends)
var adminCourseAPI = require(configPath.api.admin.course);
var dashboardStatsAPI = require(configPath.api.dashboard.stats);
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var smsAPI = require(configPath.api.sms.sms);
var TimeUuid = cassandra.types.TimeUuid;
var adminSendNotificationAPI = require(configPath.api.admin.sendNotification)

var usr_shrt_dtls_table = "ew1.ew_usr_shrt_dtls";
var vt_user_friends = "ew1.vt_user_frs";
var vt_user_frs_read = "ew1.vt_user_frs_read";
var ew_rec_frs_table = "ew1.ew_rec_frs";
var ew_frs_req_table = "ew1.ew_fr_req_snt";
var ew_frs_req_rcvd_table = "ew1.ew_fr_req_rcvd";
var ew_cb_my_flw_table = "ew1.ew_cb_my_flw";
var ew_cb_usrs_i_flw_table = "ew1.ew_cb_usrs_i_flw";
var ew_usr_dtls_table = "ew1.ew_usr_dtls";
var ew_usr_contact_dtls_table = "ew1.ew_usr_contact_dtls";
var ew_usr_work_dtls_table = "ew1.ew_usr_work_dtls";
var ew_usr_edu_dtls_table = "ew1.ew_usr_edu_dtls";
var ew_usr_exm_dtls_table = "ew1.ew_usr_exm_dtls";
var ew_usr_crs_dtls_table = "ew1.ew_usr_crs_dtls";
var ew_usr_author_dtls_table = "ew1.ew_usr_author_dtl";
var ew_usr_tch_dtls_table = "ew1.ew_usr_tch_dtl";
var ew_usr_parent_kid_table = "ew1.ew_usr_parent_kid";

var ew_fr_req_noti_table = "ew1.ew_fr_req_noti";
var ew_pst_tag_noti_table = "ew1.ew_pst_tag_noti";

var ew_country_table = "ew1.ew_country";
var ew_country_city_table = "ew1.ew_country_city";

var ew_usr_email_table = "ew1.ew_usr_email";
var ew_usr_ph_table = "ew1.ew_usr_ph";

var FETCH_NOTIFICATION_LIMIT = 5;
const FETCH_FRIENDS_LIMIT = 5;

/**
 * Carrerbook API Class
 * @constructor
 */
class CareerBook {
    /**
     * @param {JSON} data
     * @param {string} data.usr_id - usr_id of the user who is uploading profile pic
     * @param {string} data.usr_pic - Path of the user's new profile pic
     * @returns {Promise}
     * @desc
     * - This method is used to save/update profile pic.
     * - We will also update profile pic in chatFriends table using {@link ChatFriends#updateUserProfilePic}
     * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
     */
    async saveProfilePic(data) {
        // var defer = Q.defer();
        try {
            var self = this;
            var usr_id = data.usr_id;
            var usr_pic = data.usr_pic;

            var query = "update " + usr_shrt_dtls_table + " set usr_pic=? where usr_id = ?";
            var args = [usr_pic, usr_id];

            await cassExecute(query, args);
            await chatFriendAPI.updateUserProfilePic({
                usr_id: usr_id,
                usr_pic: usr_pic
            });
            return data;
        } catch (err) {
            debug(err);
            throw err;
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - usr_id of the user who is updating the profile
     * @param {string} data.usr_pic - Path of the user's profile pic
     * @param {string} data.usr_email - Email of the user
     * @param {string} data.dsp_nm - Display name of the user
     * @returns {Promise}
     * @desc
     * - This method is used to save/update short details of the user
     * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
     */
    saveShortDetails(data) {
        var defer = Q.defer();
        var self = this;

        var usr_id = data.usr_id;
        var usr_email = data.usr_email;
        var dsp_nm = data.dsp_nm;
        var usr_pic = data.usr_pic;
        var usr_ph = data.usr_ph;

        //query
        var query = "update " + usr_shrt_dtls_table + " set usr_email=?,dsp_nm=?,usr_pic=?,usr_ph=? where usr_id = ?";
        var args = [usr_email, dsp_nm, usr_pic, usr_ph, usr_id];
        //cass execute query
        cassconn.execute(query, args, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - usr_id of the user who is updating the profile
     * @param {string} data.usr_role - user Role
     * @returns {Promise}
     * @desc
     * - This method is used to update role of the user
     * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
     */
    updateUserRole(data) {
        var defer = Q.defer();
        var self = this;

        var usr_role = data.usr_role;
        var usr_id = data.usr_id;

        //query
        var query = "update " + usr_shrt_dtls_table + " set usr_role=? where usr_id = ?";
        var args = [usr_role, usr_id];
        //cass execute query
        cassconn.execute(query, args, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(data);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - usr_id of the user whose profile info we want to get
     * @param {string} data.usr_pic - Path of the user's profile pic
     * @param {string} data.usr_email - Email of the user
     * @param {string} data.dsp_nm - Display name of the user
     * @returns {Promise} - Promise will conain short details of the given user
     * @desc
     * - This method is used to get short details of the given user
     * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
     */
    getUserShortDetails(data) {
        var defer = Q.defer();
        var self = this;

        var usr_id = data.usr_id;

        //query
        var query = "select * from " + usr_shrt_dtls_table + " where usr_id = ?";
        var args = [usr_id];
        //cass execute query
        cassconn.execute(query, args, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            //Row Object
            var row = (res.rows.length > 0 && res.rows[0]) || {};
            //Convert object to JSON
            var json_row = JSON.parse(JSON.stringify(row));
            defer.resolve(json_row);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string|Array} data.usr_ids - String/Array of userid
     * @returns {Promise} - Promise will conain short details of the given user(s)
     * @desc
     * - This method is used to get short details of the given user(s)
     * - Tables Used : {@link CassandraTables#ew_usr_shrt_dtls}
     */
    getUsersShortDetails(data) {

        //data = {usr_ids = ""/[]}
        var defer = Q.defer();
        var self = this;

        var usr_ids = [].concat(data.usr_ids);
        var str = '';
        usr_ids.forEach(function(v) {
            //join with ,(comma)
            str += '\'' + v + '\',';
        });

        str = '(' + str.substring(0, str.length - 1) + ')';

        //query
        var query = "select * from " + usr_shrt_dtls_table + " where usr_id in " + str;
        //cass execute query
        cassconn.execute(query, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var row = (res.rows.length > 0 && res.rows) || [];
            // Convert to JSON Object
            var json_row = JSON.parse(JSON.stringify(row));
            defer.resolve(json_row);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @returns {Promise} - Promise will conain short details all the users in system
     * @desc
     * - This method is used to get short details of all the user(s) in system
     * - Tables used : {@link CassandraTables#ew_usr_shrt_dtls}
     */
    getAllUsersShortDetails(data) {

        var defer = Q.defer();
        var self = this;

        //query
        var query = "select * from " + usr_shrt_dtls_table;
        //cass execute query
        cassconn.execute(query, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var row = (res.rows.length > 0 && res.rows) || [];
            // Converting to JSON object
            var json_row = JSON.parse(JSON.stringify(row));
            defer.resolve(json_row);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User
     * @param {string} data.fr_id - User
     * @returns {Promise}
     * @desc
     * - Consider Two users A and B.Anyone can be usr_id and fr_id.We will add/update entry as follow.
     * - A will be added to friend list of B
     * - B will be added to friend list of A
     * - We will also add usr_id and fr_id to chat friends table using {@link ChatFriends#addFriend}
     * - Also update dashboard stats friend counter of usr_id and fr_id using {@link DashboardStats#incrementFriendCounter}
     * - Tables used : {@link CassandraTables#vt_user_frs}
     */
    addConfirmedFriends(data) {
        var defer = Q.defer();
        var self = this;

        var usr_id = data.usr_id; //A
        var fr_id = data.fr_id; //B

        //query
        // B will be added to friend list of A
        // var query = "update " + vt_user_friends + " set fr_id = fr_id + {'" + fr_id + "'} where usr_id = ?";
        // var params = [usr_id];
        var query = `insert into ${vt_user_friends} (fr_id,usr_id,fr_ts) values (?,?,?)`
        var params = [fr_id,usr_id,TimeUuid.now()];

        // A will be added to friend list of B
        // var query_rev = "update " + vt_user_friends + " set fr_id = fr_id + {'" + usr_id + "'} where usr_id = ?";
        // var params_rev = [fr_id];
        var query_rev = `insert into ${vt_user_friends} (fr_id,usr_id,fr_ts) values (?,?,?)`
        var params_rev = [usr_id,fr_id,TimeUuid.now()];

        var queries = [{
            query: query,
            params: params
        }, {
            query: query_rev,
            params: params_rev
        }]

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        //cass execute query
        cassconn.batch(queries, queryOptions, async function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            await chatFriendAPI.addFriend({
                user_id: usr_id,
                fr_id: fr_id
            })
            await chatFriendAPI.addFriend({
                user_id: fr_id,
                fr_id: usr_id
            })

            try {
                debug("Updating dashboard stats counter for friends............")
                await dashboardStatsAPI.incrementFriendCounter({
                    usr_id: usr_id
                });
                await dashboardStatsAPI.incrementFriendCounter({
                    usr_id: fr_id
                });
            } catch (err) {
                debug(err);
            }


            defer.resolve({});
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - User
     * @param {string} data.fr_id - User
     * @returns {Promise}
     * @description
     * - Consider Two users A and B.Anyone can be usr_id and fr_id.We will add/update entry as follow.
     * - A will be removed from friend list of B
     * - B will be removed from friend list of A
     * - We will also update friend list of usr_id and fr_id in chat friends table using {@link ChatFriends#removeFriend}
     * - Also decrement dashboard stats friend counter of usr_id and fr_id using {@link DashboardStats#decrementFriendCounter}
     * - Tables used : {@link CassandraTables#vt_user_frs}
     */
    removeFriend(data) {
        var defer = Q.defer();
        var self = this;

        var usr_id = data.usr_id; //A
        var fr_id = data.fr_id; //B

        //query
        // B will be removed from friend list of A
        // var query = "update " + vt_user_friends + " set fr_id = fr_id - {'" + fr_id + "'} where usr_id = ?";
        // var params = [usr_id];
        var query = `delete from ${vt_user_friends} where fr_id = ? and usr_id = ?`;
        var params = [fr_id,usr_id];

        // A will be removed from friend list of B
        // var query_rev = "update " + vt_user_friends + " set fr_id = fr_id - {'" + usr_id + "'} where usr_id = ?";
        // var params_rev = [fr_id];
        var query_rev = `delete from ${vt_user_friends} where fr_id = ? and usr_id = ?`;
        var params_rev = [usr_id,fr_id];

        var queries = [{
            query: query,
            params: params
        }, {
            query: query_rev,
            params: params_rev
        }]

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        //cass execute query
        cassconn.batch(queries, queryOptions, async function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }

            try {
                debug("Removing chat friends............")
                await chatFriendAPI.removeFriend({
                    user_id: usr_id,
                    fr_id: fr_id
                })
                await chatFriendAPI.removeFriend({
                    user_id: fr_id,
                    fr_id: usr_id
                })
            } catch (err) {
                debug(err);
            }

            try {
                debug("Updating dashboard stats counter for friends............")
                await dashboardStatsAPI.decrementFriendCounter({
                    usr_id: usr_id
                });
                await dashboardStatsAPI.decrementFriendCounter({
                    usr_id: fr_id
                });
            } catch (err) {
                debug(err);
            }

            defer.resolve({});

        });
        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - usr_id of user whose friends we want to get
     * @returns {Promise}
     * @desc
     * - Get List of all friends(with their short details {@link CareerBook#getUsersShortDetails}) of given user
     * - Tables Used : {@link CassandraTables#vt_user_frs}
     */
    getUserFriends(data) {

        var defer = Q.defer();
        var self = this;

        var usr_id = data.usr_id;
        var last_frnd_id = data.last_frnd_id;
        var query;
        var params;
        if(last_frnd_id){
            // "select * from " + vt_user_friends + " where usr_id = ? "
            query = `select * from ${vt_user_frs_read} where usr_id = ? and fr_ts < ? limit ${FETCH_FRIENDS_LIMIT}`;
            params = [usr_id , last_frnd_id];
        }else{
            query = `select * from ${vt_user_frs_read} where usr_id = ? limit ${FETCH_FRIENDS_LIMIT}`;
            params = [usr_id];
        }

        //cass execute query
        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var row = (res.rows.length > 0 && res.rows) || [];
            //convert row to json object
            var json_row = JSON.parse(JSON.stringify(row));
            if (json_row.length > 0) {
                //if user has friends
                //then get user short details
                self.getUsersShortDetails({
                        usr_ids: json_row[0].fr_id
                    })
                    .then(function(friends) {
                        //send it to client
                        defer.resolve(friends);
                    })
                    .catch(function(err) {
                        defer.reject(err);
                    });
            } else {
                defer.resolve([]);
            }
        });
        return defer.promise;
    }

    async getUserFriendsAndFollowings(data) {
        try {
            var self = this;

            if (!data.usr_id) {
                return {
                    status: "error",
                    message: "usr_id field is required"
                }
            }

            var usr_id = data.usr_id;

            var last_frnd_id = data.last_frnd_id;
            var query;
            var params;

            if(last_frnd_id){
                // "select * from " + vt_user_friends + " where usr_id = ? "
                query = `select * from ${vt_user_frs_read} where usr_id = ? and fr_ts < ? limit ${FETCH_FRIENDS_LIMIT}`;
                params = [usr_id , last_frnd_id];
            }else{
                query = `select * from ${vt_user_frs_read} where usr_id = ? limit ${FETCH_FRIENDS_LIMIT}`;
                params = [usr_id];
            }

            var rows = await cassExecute(query, params);
            if (rows.length > 0) {
                var fr_ids = rows[0].fr_id;
                if (!fr_ids) {
                    return [];
                }
                if (fr_ids && fr_ids.length <= 0) {
                    return [];
                }
                let friends = await self.getUsersShortDetails({
                    usr_ids: fr_ids
                })

                //get all followings
                var query_fl = "select * from " + ew_cb_usrs_i_flw_table + " where usr_id = ? ";
                var params_fl = [usr_id];

                var fl_rows = await cassExecute(query_fl, params_fl);

                fl_rows = fl_rows || []
                if (fl_rows.length > 0) {
                    fl_rows = fl_rows[0].flw_id || []
                }
                friends = friends.map((v, i) => {
                    if (fl_rows.indexOf(v.usr_id) > -1) {
                        v.isFollwing = true;
                    }
                    delete v["usr_email"]
                    delete v["usr_ph"]
                    delete v["usr_role"]
                    return v
                })

                return friends
            } else {
                return []
            }
        } catch (err) {
            console.log(err);
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - usr_id of user whose recommended friends we want to get
     * @returns {Promise}
     * @desc
     * - Get recommended friends(with short details {@link CareerBook#getUsersShortDetails}) of given user
     * - Tables Used : {@link CassandraTables#ew_rec_frs}
     */
    getRecommendFriendForUser(data) {
        var defer = Q.defer();
        var self = this;
        var usr_id = data.usr_id;

        //query
        //get recommended friends
        var query = "select * from " + ew_rec_frs_table + " where usr_id = ? ";
        var params = [usr_id];

        //cass execute query
        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var jsonRows = JSON.parse(JSON.stringify(res.rows));
            //get recommended friends id list
            var fr_ids = jsonRows.map(function(v, i) {
                return v.fr_id
            });
            self.getUsersShortDetails({
                    usr_ids: fr_ids
                })
                .then(function(users) {
                    //users will be array of requested users

                    var frndMapping = {};
                    //generate user mapping from recommended friends list so we can direct fetch from dict
                    //Mapping : usr_id -> short details
                    jsonRows.map(function(v, i) {
                        frndMapping[v.fr_id] = v;
                    });

                    //go through all recommended friend and insert short details of the users
                    var allData = users.map(function(v, i) {
                        v['fr_req_snt'] = frndMapping[v.usr_id]['fr_req_snt'];
                        return v;
                    })

                    defer.resolve(allData);
                })
                .catch(function(err) {
                    defer.reject(err);
                });
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user who is the sender of the friend request
     * @param {string} data.fr_id - user who will receive the friend request
     * @returns {Promise}
     * @desc
     * - Consider two users A and B. When A click add friend to B.
     * - Insert Friend request notification for B {@link CareerBook#insertFriendRequestNotifications}
     * - Update recommendation table for A and change record to 'p'(Pending) state
     * - Update frnd request sent table for A and set request state to 'p'
     * - Update frnd request receive table for B and set request state to 'p'
     * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
     */
    sendFriendRequest(data) {
        var defer = Q.defer();
        var self = this;
        var usr_id = data.usr_id; /// usr_id who sent the reqest
        var fr_id = data.fr_id; // usr_id who receive the request
        var req_dt = new Date();
        var req_sts = 'p'; // Pending

        //add notification to receiver
        //1st point in comment
        this.insertFriendRequestNotifications({
            frm_usr_id: usr_id,
            to_usr_id: fr_id,
            noti_vw_flg: "false"
        });

        //change recommend table
        //2nd point in comment
        var query_rec = "update " + ew_rec_frs_table + " set fr_req_snt = ? where usr_id = ? and fr_id = ? ";
        var params_rec = ['p', usr_id, fr_id];

        //query
        //3rd point in comment
        var query = "insert into " + ew_frs_req_table + " (usr_id,fr_id,req_dt,req_sts) values (?,?,?,?) ";
        var params = [usr_id, fr_id, req_dt, req_sts];

        var query_snt_reverse = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ?";
        var params_snt_reverse = [fr_id, usr_id];

        // receive table insert
        // 4th point in comment
        var query_rcv = "insert into " + ew_frs_req_rcvd_table + " (usr_id,fr_id,req_dt,req_sts) values (?,?,?,?) ";
        var params_rvc = [usr_id, fr_id, req_dt, req_sts];

        var query_rcv_reverse = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ?";
        var params_rcv_reverse = [fr_id, usr_id];

        var queries = [{
            query: query,
            params: params
        }, {
            query: query_rcv,
            params: params_rvc
        }, {
            query: query_rec,
            params: params_rec
        }, {
            query: query_snt_reverse,
            params: params_snt_reverse
        }, {
            query: query_rcv_reverse,
            params: params_rcv_reverse
        }]

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        //cass execute query
        cassconn.batch(queries, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user who accept the friend request
     * @param {string} data.fr_id - user who is sender of the friend request
     * @returns {Promise}
     * @desc
     * - Consider two users A and B.When B click confirm/accept friend to A.
     * - Update recommendation table for A and change record to 'f'(friends) state , {@link CassandraTables#ew_rec_frs}
     * - Update recommendation table for B and change record to 'f'(friends) state , {@link CassandraTables#ew_rec_frs}
     * - Update `frnd request sent table` for A and set request state to 'f' , {@link CassandraTables#ew_fr_req_snt}
     * - Update `frnd request receive table` for B and set request state to 'f' , {@link CassandraTables#ew_fr_req_rcvd}
     * - Add A to the follwer list of B , see {@link CareerBook#addFollower}
     * - Add B to the follwer list of A , see {@link CareerBook#addFollower}
     * - Add A and B as a confirmed friend {@link CareerBook#addConfirmedFriends}
     * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
     */
    acceptFriendRequest(data) {
        var defer = Q.defer();
        var self = this;
        var usr_id = data.usr_id;
        var fr_id = data.fr_id;
        var req_dt = new Date();
        var req_sts = 'f'; // Become Friends

        //change recommend table
        //1st point
        var query_rec = "update " + ew_rec_frs_table + " set fr_req_snt = ? where usr_id = ? and fr_id = ? ";
        var params_rec = ['f', usr_id, fr_id];

        //change recommend table
        //2nd point
        var query_reverse = "update " + ew_rec_frs_table + " set fr_req_snt = ? where usr_id = ? and fr_id = ? ";
        var params_reverse = ['f', fr_id, usr_id];

        //query
        //here A is sending Frnd req to B
        //so when B accept
        //B will be frnd in req sent table and A will be user
        //3rd point
        var query = "update " + ew_frs_req_table + " set req_dt = ?,req_sts = ? where usr_id = ? and fr_id = ? ";
        var params = [req_dt, req_sts, fr_id, usr_id];

        // receive table insert
        //here A is sending Frnd req to B
        //so when B accept
        //B will be frnd in req sent table and A will be user
        //4th point
        var query_rcv = "update " + ew_frs_req_rcvd_table + " set req_dt=? , req_sts = ? where usr_id = ? and fr_id = ? ";
        var params_rvc = [req_dt, req_sts, fr_id, usr_id];

        //also added both user as a follower
        //5th point
        self.addFollower({
            usr_id: usr_id,
            flw_id: fr_id
        });
        // 6th point
        self.addFollower({
            usr_id: fr_id,
            flw_id: usr_id
        });

        var queries = [{
            query: query,
            params: params
        }, {
            query: query_rcv,
            params: params_rvc
        }, {
            query: query_rec,
            params: params_rec
        }, {
            query: query_reverse,
            params: params_reverse
        }]

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        //cass execute query
        cassconn.batch(queries, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            // 7th point
            self.addConfirmedFriends({
                usr_id: usr_id,
                fr_id: fr_id
            });
            defer.resolve({});
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user who delete the friend request
     * @param {string} data.fr_id - user whose frnd request is being deleted
     * @returns {Promise}
     * @desc
     * - Consider two users A and B.When B is deleting frnd req of A.
     * - delete entry for A in recommendation table so he will not see B as a recommended frnd in future , {@link CassandraTables#ew_rec_frs}
     * - Update `frnd request sent table` for A and set request state to 'd' , {@link CassandraTables#ew_fr_req_snt}
     * - Update `frnd request receive table` for B and set request state to 'd' , {@link CassandraTables#ew_fr_req_rcvd}
     * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
     */
    deleteFriendRequest(data) {
        var defer = Q.defer();
        var self = this;
        var usr_id_from_argument = data.usr_id; //B is deleting requst of A so usr_id_from_argument = B
        var fr_id_from_argument = data.fr_id; //fr_id_from_argument = A
        var req_dt = new Date();
        var req_sts = 'd'; // deleted

        //now we want to take action for `A`(fr_id_from_argument) so in actual he will be usr_id in database
        var usr_id = fr_id_from_argument;
        var fr_id = usr_id_from_argument;

        //change recommend table
        //delete user and frnd combination so in future usr_id will not see fr_id in recommandation list
        //1st point
        var query_rec = "delete from " + ew_rec_frs_table + " where usr_id = ? and fr_id = ? ";
        var params_rec = [usr_id, fr_id];

        //query
        //here A is sending Frnd req to B  so in table : usr_id = A , fr_id = B , req_sts = pending(p)
        //so when B Delete
        //B will be frnd in req sent table and A will be user, so in table: usr_id = A , fr_id = B , req_sts = deleted(d)
        //2nd point
        var query = "update " + ew_frs_req_table + " set req_dt=?,req_sts = ? where usr_id = ? and fr_id = ? ";
        var params = [req_dt, req_sts, usr_id, fr_id];

        // receive table insert
        //here A is sending Frnd req to B,      usr_id = A , fr_id = B , req_sts = pending(p)
        //so when B accept
        //B will be frnd in req sent table and A will be user,  usr_id = A , fr_id = B , req_sts = deleted(d)
        //3rd point
        var query_rcv = "update " + ew_frs_req_rcvd_table + " set req_dt=? , req_sts = ? where usr_id = ? and fr_id = ? ";
        var params_rvc = [req_dt, req_sts, usr_id, fr_id];

        var queries = [{
            query: query,
            params: params
        }, {
            query: query_rcv,
            params: params_rvc
        }, {
            query: query_rec,
            params: params_rec
        }]

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        //cass execute query
        cassconn.batch(queries, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user who unfriend his friend
     * @param {string} data.fr_id - user who is going to be unfriend
     * @returns {Promise}
     * @desc
     * - Consider two users A and B. When B is unfriending A.
     * - Remove A and B from thier Friendlist , See {@link CareerBook#removeFriend}
     * - Remove A from follower list of B , See {@link CareerBook#removeFollower}
     * - Remove B from follower list of A , See {@link CareerBook#removeFollower}
     * - Remove B from Recommended friend list of A , {@link CassandraTables#ew_rec_frs}
     * - Update `frnd request sent table` for A and set request state to 'd' so in future A can't send request to B
     * - Delete entry for B(where B sent req to A,incase if any) `frnd request sent table` so in future B can send request to A
     * - Update `frnd request receive table` for A and set request state to 'd' so in future A can't send request to B
     * - Delete entry for B(where B sent req to A,incase if any) `frnd request receive table` so in future B can send request to A
     * - Tables Used : {@link CassandraTables#ew_rec_frs} , {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
     */
    unfriendFriend(data) {
        var defer = Q.defer();
        var self = this;

        var usr_id_from_argument = data.usr_id; // this usr_id has unfriened fr_id
        var fr_id_from_argument = data.fr_id;

        //1st step
        self.removeFriend({
            usr_id: usr_id_from_argument,
            fr_id: fr_id_from_argument
        });

        //also remove both user as a follower
        //2ndt step
        // self.removeFollower({
        //     usr_id: usr_id_from_argument,
        //     flw_id: fr_id_from_argument
        // });
        //3rd step
        // self.removeFollower({
        //     usr_id: fr_id_from_argument,
        //     flw_id: usr_id_from_argument
        // });

        //In future
        //fr_id can't see usr_id in people you may know
        //4th step
        var query_recommended = "delete from " + ew_rec_frs_table + " where usr_id = ? and fr_id = ?";
        var params_recommended = [fr_id_from_argument, usr_id_from_argument];

        //query
        //here A is unfriending B
        //In future , A can send friend request to B , but B can't
        //so in req sent table set B req status to deleted
        //5th step
        // var query_snt = "update " + ew_frs_req_table + " set req_sts = ?, req_dt=? where usr_id = ? and fr_id = ? ";
        // var params_snt_by_b = ['d',new Date(), fr_id_from_argument, usr_id_from_argument];

        //6th step
        var query_snt_dlt = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
        var params_snt_dlt = [usr_id_from_argument, fr_id_from_argument];

        var query_snt_dlt_b = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
        var params_snt_dlt_b = [fr_id_from_argument, usr_id_from_argument];


        // receive table insert
        //here A is unfriending B
        //In future , A can send friend request to B , but B can't
        //so in req rcv table set B req status to deleted
        //7th step
        // var query_rcv = "update " + ew_frs_req_rcvd_table + " set req_sts = ? , req_dt = ? where usr_id = ? and fr_id = ? ";
        // var params_rcv_by_a = ['d',new Date(), fr_id_from_argument, usr_id_from_argument];

        //8th step
        var query_rcv_dlt = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
        var params_rcv_dlt = [usr_id_from_argument, fr_id_from_argument];

        var query_rcv_dlt_b = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
        var params_rcv_dlt_b = [fr_id_from_argument, usr_id_from_argument];

        var queries = [{
            query: query_recommended,
            params: params_recommended
        }, {
            // query: query_snt,
            // params: params_snt_by_b
            query: query_snt_dlt_b,
            params: params_snt_dlt_b
        }, {
            query: query_snt_dlt,
            params: params_snt_dlt
        }, {
            // query: query_rcv,
            // params: params_rcv_by_a
            query: query_rcv_dlt_b,
            params: params_rcv_dlt_b
        }, {
            query: query_rcv_dlt,
            params: params_rcv_dlt
        }]

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        //cass execute query
        cassconn.batch(queries, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    async cancelFriendRequest(data) {
        try {
            var usr_id_from_argument = data.usr_id;
            var fr_id_from_argument = data.fr_id;

            var query_snt_dlt = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
            var params_snt_dlt = [usr_id_from_argument, fr_id_from_argument];

            var query_rcv_dlt = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
            var params_rcv_dlt = [usr_id_from_argument, fr_id_from_argument];


            var queries = [{
                query: query_snt_dlt,
                params: params_snt_dlt
            }, {
                query: query_rcv_dlt,
                params: params_rcv_dlt
            }]

            await cassBatch(queries);
            return data;
        } catch (err) {
            throw new Error(err);
        }
    }

    async blockPerson(data) {
        try {
            var self = this;
            var usr_id_from_argument = data.usr_id;
            var fr_id_from_argument = data.fr_id;

            self.removeFriend({
                usr_id: usr_id_from_argument,
                fr_id: fr_id_from_argument
            });

            //also remove both user as a follower
            //2ndt step
            self.removeFollower({
                usr_id: usr_id_from_argument,
                flw_id: fr_id_from_argument
            });
            //3rd step
            self.removeFollower({
                usr_id: fr_id_from_argument,
                flw_id: usr_id_from_argument
            });

            var query_snt_dlt_reverse = "delete from " + ew_frs_req_table + " where usr_id = ? and fr_id = ? ";
            var params_snt_dlt_reverse = [fr_id_from_argument, usr_id_from_argument];

            var query_rcv_dlt_reverse = "delete from " + ew_frs_req_rcvd_table + " where usr_id = ? and fr_id = ? ";
            var params_rcv_dlt_reverse = [fr_id_from_argument, usr_id_from_argument];

            var query_blck = "update " + ew_frs_req_table + " set req_dt = ?,req_sts = ? where usr_id = ? and fr_id = ? ";
            var params_blck = [new Date(), 'b', usr_id_from_argument, fr_id_from_argument];

            var query_blck_rcv = "update " + ew_frs_req_rcvd_table + " set req_dt = ?,req_sts = ? where usr_id = ? and fr_id = ? ";
            var params_blck_rcv = [new Date(), 'b', usr_id_from_argument, fr_id_from_argument];


            var queries = [{
                query: query_snt_dlt_reverse,
                params: params_snt_dlt_reverse
            }, {
                query: query_rcv_dlt_reverse,
                params: params_rcv_dlt_reverse
            }, {
                query: query_blck,
                params: params_blck
            }, {
                query: query_blck_rcv,
                params: params_blck_rcv
            }]

            await cassBatch(queries);
            return data;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user for whom we want to find all friend request
     * @returns {Promise}
     * @desc
     * - Get all friends request(with their short details {@link CareerBook#getUsersShortDetails}) which are 'p'(pending) for usr_id
     * - Tables Used : {@link CassandraTables#ew_fr_req_rcvd}
     */
    getFriendRequest(data) {
        var defer = Q.defer();
        var self = this;
        var usr_id = data.usr_id; // get friend request for this user id , so we will look for fr_id == this user

        //query
        var query = "select * from " + ew_frs_req_rcvd_table + " where fr_id = ?"; // reuest should be pending
        var params = [usr_id];

        //cass execute query
        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var jsonRows = JSON.parse(JSON.stringify(res.rows));
            //filter request which are pending
            jsonRows = jsonRows.filter(function(v, i) {
                if (v.req_sts === 'p')
                    return true;
                return false;
            })
            var fr_ids = jsonRows.map(function(v, i) {
                return v.usr_id
            });
            self.getUsersShortDetails({
                    usr_ids: fr_ids
                })
                .then(function(res) {
                    defer.resolve(res);
                })
                .catch(function(err) {
                    defer.reject(err);
                });
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user
     * @param {string} data.fr_id - user
     * @returns {Promise}
     * @desc
     * - Two friends A and B.Here we will find friend status between them
     * - Check req sent table for usr_id, If request is not deleted then send it to client
     * - Then Check for req receive table for usr_id, If request is not deleted then send it to client
     * - if in both the table we have deleted request then send `req sent table record` to client
     * - Tables Used : {@link CassandraTables#ew_fr_req_snt} , {@link CassandraTables#ew_fr_req_rcvd}
     */
    getFriendStatus(data) {
        var defer = Q.defer();
        var self = this;
        var usr_id = data.usr_id;
        var fr_id = data.fr_id;
        //query friend req sent table
        var query = "select * from " + ew_frs_req_table + " where usr_id = ?  and fr_id = ?";
        var params = [usr_id, fr_id];

        //query friend req receive table
        var query_rcv = "select * from " + ew_frs_req_rcvd_table + " where usr_id = ?  and fr_id = ?";
        var params_rcv = [fr_id, usr_id];

        //cass execute query
        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }

            var reqSentArray = JSON.parse(JSON.stringify(res.rows));
            if (reqSentArray.length > 0) {
                if (reqSentArray[0]['req_sts'] === 'b' || reqSentArray[0]['req_sts'] === 'd') {
                    defer.resolve(reqSentArray[0]);
                    return;
                } else if (reqSentArray[0]['req_sts'] !== 'd') {
                    reqSentArray[0]['req_action'] = 'send';
                    defer.resolve(reqSentArray[0]);
                    return;
                }
            }

            cassconn.execute(query_rcv, params_rcv, function(err, res, r) {
                if (err) {
                    debug(err);
                    defer.reject(err);
                    return;
                }
                var reqRcvArray = JSON.parse(JSON.stringify(res.rows));

                if (reqRcvArray.length > 0) {
                    if (reqRcvArray[0]['req_sts'] === 'b') {
                        defer.resolve(reqRcvArray[0]);
                        return;
                    } else if (reqRcvArray[0]['req_sts'] !== 'd') {
                        reqRcvArray[0]['req_action'] = 'receive';
                        defer.resolve(reqRcvArray[0]);
                        return;
                    }
                    //if it is deleted rcv request then we will jump out if else
                    //and we will check whether both sent and recv req are deleted
                    // if deleted then we can't add as a friend
                } else if (reqSentArray.length > 0) {
                    defer.resolve(reqSentArray[0]);
                    return;
                }
                if (reqRcvArray[0] && reqSentArray[0] && (reqRcvArray[0]['req_sts'] === 'd' && reqSentArray[0]['req_sts'] === 'd')) {
                    defer.resolve(reqSentArray[0]);
                    return;
                }

                defer.resolve({});
            });
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.flw_id - user who will be added to followers list array
     * @param {string} data.usr_id - user whose follower we are going to add
     * @returns {Promise}
     * @desc
     * - We will add Follower id to user_id's follower list
     * - Tables Used : {@link CassandraTables#ew_cb_my_flw}
     */
    addFollower(data) {
        var defer = Q.defer();

        var flw_id = data.flw_id; // user who will be added to flw_id array
        var usr_id = data.usr_id; // user whose follower we are going to add
        var flw_dt = new Date();


        var query = "update " + ew_cb_my_flw_table + " set flw_id = flw_id + {'" + flw_id + "'},flw_dt=? where usr_id = ?";
        var params = [flw_dt, usr_id];

        var query_rev = "update " + ew_cb_usrs_i_flw_table + " set flw_id = flw_id + {'" + usr_id + "'},flw_dt=? where usr_id = ?";
        var params_rev = [flw_dt, flw_id];

        var queries = [{
            query: query,
            params: params
        }, {
            query: query_rev,
            params: params_rev
        }]

        cassconn.batch(queries, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(data);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user whose follower we want to get
     * @returns {Promise}
     * @desc
     * - We will fetch all follower list from table for given user
     * - Tables Used : {@link CassandraTables#ew_cb_my_flw}
     */
    getFollowers(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id; // user whose follower we are going to get

        var query = "select * from  " + ew_cb_my_flw_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var jsonObj = JSON.parse(JSON.stringify(res.rows));
            defer.resolve(jsonObj);
        });
        return defer.promise;
    }


    getFollowingUsers(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id; // user whose following we are going to get

        var query = "select * from  " + ew_cb_usrs_i_flw_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var jsonObj = JSON.parse(JSON.stringify(res.rows));
            defer.resolve(jsonObj);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user whose follower we want to get
     * @param {string} data.flw_id - user whom we want to remove from follower list
     * @returns {Promise}
     * @desc
     * - Remove given follower from follower list for given user
     * - Tables Used : {@link CassandraTables#ew_cb_my_flw}
     */
    removeFollower(data) {
        var defer = Q.defer();

        var flw_id = data.flw_id; // user who will be added to flw_id array
        var usr_id = data.usr_id; // user whose follower we are going to remove
        var flw_dt = new Date();


        var query = "update " + ew_cb_my_flw_table + " set flw_id = flw_id - {'" + flw_id + "'},flw_dt=? where usr_id = ?";
        var params = [flw_dt, usr_id];

        var query_rev = "update " + ew_cb_usrs_i_flw_table + " set flw_id = flw_id - {'" + usr_id + "'},flw_dt=? where usr_id = ?";
        var params_rev = [flw_dt, flw_id];

        var queries = [{
            query: query,
            params: params
        }, {
            query: query_rev,
            params: params_rev
        }]

        cassconn.batch(queries, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });
        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user whose personal details we want to get
     * @returns {Promise}
     * @desc
     * - Get Personal Details of given user
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    getUserPersonalDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var query = "select * from " + ew_usr_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            var jsonObj = JSON.parse(JSON.stringify(res.rows));
            defer.resolve(jsonObj);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} usr_id - User Id
     * @param {string} ctry - Country
     * @param {string} ctry_flg - Country restrition (Public , Only Me , Friends)
     * @param {string} cty - City
     * @param {string} cty_flg - City restrition (Public , Only Me , Friends)
     * @param {string} dob_dt - DOB
     * @param {string} dob_dt_flg - DOB restrition (Public , Only Me , Friends)
     * @param {string} dob_mn - DOB Month
     * @param {string} dob_mn_flg - DOB Month restrition (Public , Only Me , Friends)
     * @param {string} dob_yr - DOB Year
     * @param {string} dob_yr_flg - DOB Year restrition (Public , Only Me , Friends)
     * @param {string} dsp_nm - Display Name
     * @param {string} dsp_nm_flg - Display Name restrition (Public , Only Me , Friends)
     * @param {string} fst_nm - Firstname
     * @param {string} fst_nm_flg - Firstname restrition (Public , Only Me , Friends)
     * @param {string} ht_ctry - Hometown Country
     * @param {string} ht_ctry_flg - Hometown Country restrition (Public , Only Me , Friends)
     * @param {string} ht_cty - HomeTown City
     * @param {string} ht_cty_flg - HomeTown City restrition (Public , Only Me , Friends)
     * @param {string} ht_zip_cd - Hometown zip code
     * @param {string} ht_zip_cd_flg - Hometown zip code restrition (Public , Only Me , Friends)
     * @param {string} lst_nm - Last name
     * @param {string} lst_nm_flg - Last name restrition (Public , Only Me , Friends)
     * @param {string} role - Role
     * @param {string} usr_abt_me - about user
     * @param {string} usr_abt_me_flg - about user restrition (Public , Only Me , Friends)
     * @param {string} zip_cd - Zip Code
     * @param {string} zip_cd_flg - Zip Code restrition (Public , Only Me , Friends)
     * @returns {Promise}
     * @desc
     * - Save Personal Details of given user
     * - We will update user short details tables also if display name is changed {@link CareerBook#saveShortDetails}
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    saveUserBasicPersonalDetails(data) {
        var defer = Q.defer();
        var self = this;

        var usr_id = data.usr_id;
        var update_str = "";
        var update_params = [];

        var columns = Object.keys(data || {});
        columns.forEach(function(v, i) {
            if (v !== 'usr_id' &&
                data[v] &&
                v !== 'email_nv' &&
                v !== 'email' &&
                v !== 'email_nv_permission' &&
                v !== 'ph_nv' &&
                v !== 'ph' &&
                v !== 'ph_nv_permission') {
                update_str += v + " = ?,";
                update_params.push(data[v]);
            }
        });

        update_str = update_str.substr(0, update_str.length - 1);
        var query = "update " + ew_usr_dtls_table + " set " + update_str + " where usr_id = ?";
        var params = [].concat(update_params, usr_id);

        cassconn.execute(query, params, async function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }

            if (data.usr_id && data.dsp_nm) {
                try {
                    await self.saveShortDetails({
                        usr_id: data.usr_id,
                        dsp_nm: data.dsp_nm
                    });
                } catch (err) {
                    debug(err);
                }

            }

            defer.resolve({});
        });
        return defer.promise;
    }

    async sendMailForVerification(email,otp){
        try{
            var obj = {
                to: email,
                subject: 'OTP For verifing Email address - ExamWarrior',
                text: 'Your OTP is ' + otp
            }
            await mailAPI.sendMail(obj);
            return { email, otp }
        }catch(err){
            throw new Error(err)
        }
    }

    async sendSMSForVerification(ph,otp){
        try{
            await smsAPI.sendSMS({
                message: 'From examwarrior.com, OTP for 1st time login: ' + otp,
                numbers: ph
            });
            return { ph, otp }
        }catch(err){
            throw new Error(err)
        }
    }

    async getUserEmails(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            var query = "select * from " + ew_usr_email_table + " where usr_id = ?";
            var params = [data.usr_id]
            var emails = await cassExecute(query,params);
            return emails;
        }catch(err){
            throw new Error(err)
        }
    }

    async getUserPhoneNumbers(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            var query = "select * from " + ew_usr_ph_table + " where usr_id = ?";
            var params = [data.usr_id]
            var phs = await cassExecute(query,params);
            return phs;
        }catch(err){
            throw new Error(err)
        }
    }

    async addUserEmail(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.email){
                return {
                    status : "error",
                    message : "email field is required"
                }
            }
            var otp = '' + Math.floor(1000 + Math.random() * 9000);
            var otp_sent_dt = new Date();
            var otp_status = 'n';
            var email_flg = data.email_flg || "public"
            var query = "insert into " + ew_usr_email_table + " (usr_id,email,email_flg,otp,otp_sent_dt,otp_status) values (?,?,?,?,?,?)";
            var params = [data.usr_id,data.email,email_flg,otp,otp_sent_dt,otp_status]
            // save data to db
            await cassExecute(query,params);
            // send mail for otp
            // this.sendMailForVerification(data.email,otp);
            return data;
        }catch(err){
            throw new Error(err)
        }
    }

    async addUserPhoneNumber(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.ph){
                return {
                    status : "error",
                    message : "ph field is required"
                }
            }
            var otp = '' + Math.floor(1000 + Math.random() * 9000);
            var otp_sent_dt = new Date();
            var otp_status = 'n';
            var ph_flg = data.ph_flg || "public";
            var query = "insert into " + ew_usr_ph_table + " (usr_id,ph,ph_flg,otp,otp_sent_dt,otp_status) values (?,?,?,?,?,?)";
            var params = [data.usr_id,data.ph,data.ph_flg,otp,otp_sent_dt,otp_status]
            // save query
            await cassExecute(query,params);
            // send sms for verification
            // this.sendSMSForVerification(data.ph,otp)
            return data;
        }catch(err){
            throw new Error(err)
        }
    }

    async verifyUserEmail(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.email){
                return {
                    status : "error",
                    message : "email field is required"
                }
            }

            if(!data.otp){
                return {
                    status : "error",
                    message : "otp field is required"
                }
            }
            var query = "select * from " + ew_usr_email_table + " where usr_id = ? and  email = ?";
            var params = [data.usr_id,data.email]


            var emailRes = await cassExecute(query,params);
            if(emailRes && emailRes[0]){
                emailRes = emailRes[0];
                if(emailRes.otp === data.otp){
                    var query = "update " + ew_usr_email_table + " set otp_status='y',otp_verify_dt=? where usr_id = ? and email = ?";
                    var params = [new Date(),data.usr_id,data.email];
                    var updateRes = await cassExecute(query,params);
                    return {
                        status : "success",
                        message : "Email is verified successfully",
                        data : data
                    }
                }else{
                    return {
                        status : "error",
                        message : "OTP is not correct"
                    }
                }
            }
            return data;
        }catch(err){
            throw new Error(err)
        }
    }

    async verifyUserPhoneNumber(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.ph){
                return {
                    status : "error",
                    message : "ph field is required"
                }
            }

            if(!data.otp){
                return {
                    status : "error",
                    message : "otp field is required"
                }
            }
            var query = "select * from " + ew_usr_ph_table + " where usr_id = ? and  ph = ?";
            var params = [data.usr_id,data.ph]


            var phRes = await cassExecute(query,params);
            if(phRes && phRes[0]){
                phRes = phRes[0];
                if(phRes.otp === data.otp){
                    var query = "update " + ew_usr_ph_table + " set otp_status='y',otp_verify_dt=? where usr_id = ? and ph = ?";
                    var params = [new Date(),data.usr_id,data.ph];
                    var updateRes = await cassExecute(query,params);
                    return {
                        status : "success",
                        message : "Phone number is verified successfully",
                        data : data
                    }
                }else{
                    return {
                        status : "error",
                        message : "OTP is not correct"
                    }
                }
            }
            return data;
        }catch(err){
            throw new Error(err)
        }
    }

    async deleteUserEmail(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.email){
                return {
                    status : "error",
                    message : "email field is required"
                }
            }
            var query = "delete from " + ew_usr_email_table + " where usr_id = ? and email = ?";
            var params = [data.usr_id,data.email]
            // save data to db
            await cassExecute(query,params);
            return data;
        }catch(err){
            throw new Error(err)
        }
    }

    async deleteUserPhoneNumber(data){
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.ph){
                return {
                    status : "error",
                    message : "ph field is required"
                }
            }
            var query = "delete from " + ew_usr_ph_table + " where usr_id = ? and ph = ?";
            var params = [data.usr_id,data.ph]
            // save data to db
            await cassExecute(query,params);
            return data;
        }catch(err){
            throw new Error(err)
        }
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user id
     * @param {string} data.old_ph_nv - If user is updating existing phonenumber then this will be set to current old number
     * @param {string} data.ph_nv - new non verified Phone number
     * @param {string} data.ph_nv_permission - new number restriction (public, friends , only me)
     * @returns {Promise}
     * @desc
     * - If there is old number then we will first delete it
     * - Then update table with new number
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    async saveUserPhoneDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var update_str = "";
        var queries = [];
        var query_dlt, params_dlt;

        if (data.old_ph_nv) {
            //if old_ph_nv is set
            //then user is updating that phone number
            //so delete old one
            query_dlt = "delete ph_nv['" + data.old_ph_nv + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
            params_dlt = [usr_id];
        }

        //add new field to ph_nv
        var query_update = "update " + ew_usr_dtls_table + " set ph_nv['" + data.ph_nv + "']='" + data.ph_nv_permission + "' where usr_id = ?";
        var params_update = [usr_id];

        try {
            if (query_dlt) {
                //if delete query is there then
                //first delete and then update record
                await cassExecute(query_dlt, params_dlt);
            }
            await cassExecute(query_update, params_update);
            return {}
        } catch (err) {
            throw err;
        }

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user id
     * @param {string} data.old_email_nv - If user is updating existing email then this will be set to current old number
     * @param {string} data.email_nv - new non verified email
     * @param {string} data.email_nv_permission - new email restriction (public, friends , only me)
     * @returns {Promise}
     * @desc
     * - If there is old email then we will first delete it
     * - Then update table with new non verified email
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    async saveUserEmailDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var update_str = "";
        var queries = [];
        var query_dlt, params_dlt;
        if (data.old_email_nv) {
            //if old_email_nv is set
            //then user is updating that email
            //so delete old one
            query_dlt = "delete email_nv['" + data.old_email_nv + "'],email['" + data.old_email_nv + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
            params_dlt = [usr_id];
        }

        var query_update = "update " + ew_usr_dtls_table + " set email_nv['" + data.email_nv + "']='" + data.email_nv_permission + "' where usr_id = ?";
        var params_update = [usr_id];

        try {
            if (query_dlt) {
                //if delete query is there then
                //first delete and then update record
                await cassExecute(query_dlt, params_dlt);
            }
            await cassExecute(query_update, params_update);
            return {};
        } catch (err) {
            throw err;
        }
    }


    /**
     * @param {JSON} data
     * @param {string} data.to - Email Id which user want to verify
     * @returns {Promise}
     * @desc
     * - This function used to send OTP For verifing Email address
     * - this function will be called when user will click on verify email address client side
     * - user will be presented with textbox to enter otp code
     */
    async startVerifingEmailAddress(data) {
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.email){
                return {
                    status : "error",
                    message : "email field is required"
                }
            }
            var query = "select * from " + ew_usr_email_table + " where usr_id = ? and  email = ?";
            var params = [data.usr_id,data.email]
            var emailRes = await cassExecute(query,params);
            if(emailRes && emailRes[0]){
                emailRes = emailRes[0] || {};
                this.sendMailForVerification(data.email,emailRes.otp)
            }
            return data;
        }catch(err){
            throw new Error(err)
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.ph - Phone Number which user want to verify
     * @returns {Promise}
     * @desc
     * - This function used to send OTP SMS For verifing Phone number
     * - this function will be called when user will click on verify phonenumber client side
     * - user will be presented with textbox to enter otp code
     */
    async startVerifingPhoneNumber(data) {
        try{
            if(!data.usr_id){
                return {
                    status : "error",
                    message : "usr_id field is required"
                }
            }
            if(!data.ph){
                return {
                    status : "error",
                    message : "ph field is required"
                }
            }
            var query = "select * from " + ew_usr_ph_table + " where usr_id = ? and  ph = ?";
            var params = [data.usr_id,data.ph]


            var phRes = await cassExecute(query,params);
            if(phRes && phRes[0]){
                phRes = phRes[0];
                this.sendSMSForVerification(data.ph,phRes.otp)
            }
        }catch(err){
            throw new Error(err)
        }
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.email - Email id which user has confirmed with otp
     * @param {string} data.permission - Email permission (public,only me , friends)
     * @returns {Promise}
     * @desc
     * - This function used to update user's verifed emails
     * - we will delete same email from non verifed list
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    confirmVerificationOfEmail(data) {

        var defer = Q.defer();
        var queries = [];
        var usr_id = data.usr_id;
        var email = data.email;
        var email_flg = data.permission;

        var query_veri_add = "update " + ew_usr_dtls_table + " set email['" + email + "']='" + email_flg + "' where usr_id = ?";
        var query_veri_add_params = [usr_id];

        var query_dlt = "delete email_nv['" + email + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
        var params_dlt = [usr_id];

        queries.push({
            query: query_veri_add,
            params: query_veri_add_params
        });
        queries.push({
            query: query_dlt,
            params: params_dlt
        });

        cassconn.batch(queries, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.ph - Phone Number which user has confirmed with otp
     * @param {string} data.permission - Phone Number permission (public,only me , friends)
     * @returns {Promise}
     * @desc
     * - This function used to update user's verifed phone number
     * - we will delete same phone number from non verifed list
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    confirmVerificationOfPhoneNumber(data) {

        var defer = Q.defer();
        var queries = [];
        var usr_id = data.usr_id;
        var ph = data.ph;
        var ph_flg = data.permission;

        var query_veri_add = "update " + ew_usr_dtls_table + " set ph['" + ph + "']='" + ph_flg + "' where usr_id = ?";
        var query_veri_add_params = [usr_id];

        var query_dlt = "delete ph_nv['" + ph + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
        var params_dlt = [usr_id];

        queries.push({
            query: query_veri_add,
            params: query_veri_add_params
        });
        queries.push({
            query: query_dlt,
            params: params_dlt
        });

        cassconn.batch(queries, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.email - Email id which user want to delete
     * @returns {Promise}
     * @desc
     * - This function used to delete given email from table
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    deleteUserEmailAddress(data) {
        var defer = Q.defer();
        var queries = [];
        var usr_id = data.usr_id;
        var email = data.email;

        var query_dlt = "delete email_nv['" + email + "'],email['" + email + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
        var params_dlt = [usr_id];

        cassconn.execute(query_dlt, params_dlt, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.ph - Phone number which user want to delete
     * @returns {Promise}
     * @desc
     * - This function used to delete given phone number from table
     * - Tables Used : {@link CassandraTables#ew_usr_dtls}
     */
    // deleteUserPhoneNumber(data) {
    //     var defer = Q.defer();
    //     var queries = [];
    //     var usr_id = data.usr_id;
    //     var ph = data.ph;

    //     var query_dlt = "delete ph_nv['" + ph + "'],ph['" + ph + "'] from " + ew_usr_dtls_table + " where usr_id = ?";
    //     var params_dlt = [usr_id];

    //     cassconn.execute(query_dlt, params_dlt, function(err, res, r) {
    //         if (err) {
    //             debug(err);
    //             defer.reject(err);
    //             return;
    //         }
    //         defer.resolve({});
    //     });

    //     return defer.promise;
    // }

    //////////////////////////////////
    // Contact Details Form Helper //
    ////////////////////////////////


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @returns {Promise}
     * @desc
     * - This function used to get contact details of given user
     * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
     */
    getUserContactDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        var query = "select * from " + ew_usr_contact_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.old_primary - Old Primary Email
     * @param {string} data.new_primary - New Primary Email
     * @returns {Promise}
     * @desc
     * - This function used to change primary email of given user
     * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
     */
    changePrimaryEmailInContactDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        //this is prev primary email
        var old_primary = data.old_primary;
        //this will be the new primary email
        var new_primary = data.new_primary;

        var query = "update " + ew_usr_contact_dtls_table + " set usr_email['" + old_primary + "']='s',usr_email['" + new_primary + "']='p'" + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.email - Email to add
     * @param {string} data.isPrimary - indicate whether email is primary or not
     * @returns {Promise}
     * @desc
     * - This function is used to add primary/secondary email in contact details
     * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
     */
    addEmailInContactDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var primary = data.email;
        var isPrimary = (data.isPrimary) ? 'p' : 's';

        var query = "update " + ew_usr_contact_dtls_table + " set usr_email['" + primary + "']='" + isPrimary + "' " + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.email - Email which is going to be confirmed
     * @returns {Promise}
     * @desc
     * - First we will add email to verified email list
     * - then we will delete email from non verified emails
     * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
     */
    confirmVerificationOfContactEmail(data) {
        var defer = Q.defer();
        var queries = [];

        var usr_id = data.usr_id;
        var email = data.email;

        var query_veri_add = "update " + ew_usr_contact_dtls_table + " set usr_email['" + email + "']='s' where usr_id = ?";
        var query_veri_add_params = [usr_id];

        var query_dlt = "delete usr_email_nv['" + email + "'] from " + ew_usr_contact_dtls_table + " where usr_id = ?";
        var params_dlt = [usr_id];

        queries.push({
            query: query_veri_add,
            params: query_veri_add_params
        });
        queries.push({
            query: query_dlt,
            params: params_dlt
        });

        cassconn.batch(queries, null, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.email - Email which is going to be added to non verfied email
     * @returns {Promise}
     * @desc
     * - we will add given email to non verified email list
     * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
     */
    addNonVerifiedEmailInContactDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var primary = data.email;

        var query = "update " + ew_usr_contact_dtls_table + " set usr_email_nv['" + primary + "']='s' " + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.email - Email which is going to be delete
     * @returns {Promise}
     * @desc
     * - we will delete given email from verified and non verified email list
     * - Tables Used : {@link CassandraTables#ew_usr_contact_dtls}
     */
    deleteEmailInContactDetails(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var email = data.email;

        var query = "delete usr_email['" + email + "'],usr_email_nv['" + email + "'] from " + ew_usr_contact_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }


    //////////////////////
    // Work FOrm Helper //
    //////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_wrk_sk - unique id
     * @param {text} data.comp_desc - Company Desc
     * @param {text} data.comp_nm - Company Name
     * @param {text} data.frm_dt - From Date
     * @param {text} data.to_dt - To Date
     * @param {text} data.loc - Company Location
     * @param {text} data.role - Role in company,
     * @param {text} data.work_flg - (public,only me,friends)
     * @returns {Promise}
     * @desc
     * - This method will be used to save/update user work info
     * - Tables Used : {@link CassandraTables#ew_usr_work_dtls}
     */
    saveWorkInfo(data) {
        var columns = ["city","comp_desc","comp_nm","country","frm_dt","role","to_dt","work_flg","zipcode"]
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_wrk_sk = data.usr_wrk_sk || Uuid.random();

        var keys = Object.keys(data || {});
        var update_str = "";
        var update_params = [];

        keys.forEach(function(v, i) {
            if (v !== 'usr_id' && v !== 'usr_wrk_sk') {
                if(columns.indexOf(v) > -1){
                     update_str += v + " = ?,";
                        update_params.push(data[v]);
                }
            }
        });
        update_str = update_str.substr(0, update_str.length - 1);

        var query = "update " + ew_usr_work_dtls_table + " set " + update_str + " where usr_id = ? and usr_wrk_sk = ?";
        var params = [].concat(update_params, [usr_id, usr_wrk_sk]);

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @returns {Promise}
     * @desc
     * - This method will be used to get user work info of given user
     * - Tables Used : {@link CassandraTables#ew_usr_work_dtls}
     */
    getWorkInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        var query = "select * from " + ew_usr_work_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_wrk_sk
     * @returns {Promise}
     * @desc
     * - This method will be used to get user work info using usr_wrk_sk and usr_id
     * - Tables Used : {@link CassandraTables#ew_usr_work_dtls}
     */
    deleteWorkInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_wrk_sk = data.usr_wrk_sk;

        var query = "delete from " + ew_usr_work_dtls_table + " where usr_id = ? and usr_wrk_sk = ?";
        var params = [usr_id, usr_wrk_sk];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }


    //////////////////////
    // Edu FOrm Helper //
    //////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_wrk_sk - unique id
     * @param {text} data.edu_desc - Education Desc
     * @param {text} data.edu_lvl - Education level (primary school,clg,secondary school,etc...)
     * @param {text} data.frm_dt - From Date
     * @param {text} data.to_dt - To Date
     * @param {text} data.loc - School/Clg Location
     * @param {text} data.edu_nm - Clg/schl name
     * @param {text} data.edu_flg - (public,only me,friends)
     * @returns {Promise}
     * @desc
     * - This method will be used to save user edu info
     * - Tables Used : {@link CassandraTables#ew_usr_edu_dtls}
     */
    saveEduInfo(data) {
        var arr = ["city","country","edu_desc","edu_flg","edu_lvl","edu_nm","frm_dt","to_dt","zipcode"]
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_wrk_sk = data.usr_wrk_sk || Uuid.random();

        var keys = Object.keys(data || {});
        var update_str = "";
        var update_params = [];

        keys.forEach(function(v, i) {
            if (v !== 'usr_id' && v !== 'usr_wrk_sk') {
                if(arr.indexOf(v) > -1){
                    update_str += v + " = ?,";
                    update_params.push(data[v]);
                }
            }
        });
        update_str = update_str.substr(0, update_str.length - 1);

        var query = "update " + ew_usr_edu_dtls_table + " set " + update_str + " where usr_id = ? and usr_wrk_sk = ?";
        var params = [].concat(update_params, [usr_id, usr_wrk_sk]);

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @returns {Promise}
     * @desc
     * - This method will be used to get user eduction info of given user
     * - Tables Used : {@link CassandraTables#ew_usr_edu_dtls}
     */
    getEduInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        var query = "select * from " + ew_usr_edu_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.usr_wrk_sk - unique Id
     * @returns {Promise}
     * @desc
     * - This method will be used to delete user eduction info using usr_wrk_sk and usr_id
     * - Tables Used : {@link CassandraTables#ew_usr_edu_dtls}
     */
    deleteEduInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_wrk_sk = data.usr_wrk_sk;

        var query = "delete from " + ew_usr_edu_dtls_table + " where usr_id = ? and usr_wrk_sk = ?";
        var params = [usr_id, usr_wrk_sk];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    ////////////////////////
    // Exam FOrm Helper ///
    //////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_exm_sk - unique id
     * @param {text} data.tgt_yr - Target Year
     * @param {text} data.exm_nm - Exam name
     * @param {text} data.exm_flg - (public,only me,friends)
     * @returns {Promise}
     * @desc
     * - This method will be used to save user edu info
     * - Tables Used : {@link CassandraTables#ew_usr_exm_dtls}
     */
    saveExamInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_exm_sk = data.usr_exm_sk || Uuid.random();
        var crt_dt = new Date();

        var keys = Object.keys(data || {});
        var update_str = "";
        var update_params = [];

        keys.forEach(function(v, i) {
            if (v !== 'usr_id' && v !== 'usr_exm_sk' && v !== 'crt_dt') {
                update_str += v + " = ?,";
                update_params.push(data[v]);
            }
        });
        update_str = update_str.substr(0, update_str.length - 1);

        var query = "update " + ew_usr_exm_dtls_table + " set " + update_str + ",crt_dt=? where usr_id = ? and usr_exm_sk = ?";
        var params = [].concat(update_params, [crt_dt, usr_id, usr_exm_sk]);

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @returns {Promise}
     * @desc
     * - This method will be used to get user exam info of given user
     * - Tables Used : {@link CassandraTables#ew_usr_exm_dtls}
     */
    getExamInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        var query = "select * from " + ew_usr_exm_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_exm_sk - unique uuid
     * @returns {Promise}
     * @desc
     * - This method will be used to delete user exam info using usr_id and usr_exm_sk
     * - Tables Used : {@link CassandraTables#ew_usr_exm_dtls}
     */
    deleteExamInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_exm_sk = data.usr_exm_sk;

        var query = "delete from " + ew_usr_exm_dtls_table + " where usr_id = ? and usr_exm_sk = ?";
        var params = [usr_id, usr_exm_sk];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /////////////////////////////////////
    // Interested Course Form Helper ///
    ///////////////////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_crs_sk - unique id
     * @param {text} data.crs_nm - Course name
     * @param {text} data.crs_flg - (public,only me,friends)
     * @returns {Promise}
     * @desc
     * - This method will be used to save user's interested course into {@link CassandraTables#ew_usr_crs_dtls}
     * - Tables Used : {@link CassandraTables#ew_usr_crs_dtls}
     */
    saveInterestedCourseInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_crs_sk = data.usr_crs_sk || Uuid.random();
        var crt_dt = new Date();

        var keys = Object.keys(data || {});
        var update_str = "";
        var update_params = [];

        keys.forEach(function(v, i) {
            if (v !== 'usr_id' && v !== 'usr_crs_sk' && v !== 'crt_dt') {
                update_str += v + " = ?,";
                update_params.push(data[v]);
            }
        });
        update_str = update_str.substr(0, update_str.length - 1);

        var query = "update " + ew_usr_crs_dtls_table + " set " + update_str + ",crt_dt=? where usr_id = ? and usr_crs_sk = ?";
        var params = [].concat(update_params, [crt_dt, usr_id, usr_crs_sk]);

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }


    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @returns {Promise}
     * @desc
     * - This method will be used to get interested courses of given user
     * - Tables Used : {@link CassandraTables#ew_usr_crs_dtls}
     */
    getInterestedCourseInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        var query = "select * from " + ew_usr_crs_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_crs_sk - unique id
     * @returns {Promise}
     * @desc
     * - This method will be used to delete interested courses of given user using usr_crs_sk
     * - Tables Used : {@link CassandraTables#ew_usr_crs_dtls}
     */
    deleteInterestedCourseInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_crs_sk = data.usr_crs_sk;

        var query = "delete from " + ew_usr_crs_dtls_table + " where usr_id = ? and usr_crs_sk = ?";
        var params = [usr_id, usr_crs_sk];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }



    //////////////////////////////////
    // Author Profile Form Helper ///
    ////////////////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.usr_bk_sk - unique id
     * @param {text} data.author_desc - Author Desc
     * @param {text} data.crs_bk_desc - Course/Book Desc
     * @param {text} data.crs_bk_nm - Course/Book Name
     * @returns {Promise}
     * @desc
     * - This method will be used to save/update Author Info
     * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
     */
    saveAuthorInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_bk_sk = data.usr_bk_sk || Uuid.random();

        var keys = Object.keys(data || {});
        var update_str = "";
        var update_params = [];

        keys.forEach(function(v, i) {
            if (v !== 'usr_id' && v !== 'usr_bk_sk') {
                update_str += v + " = ?,";
                update_params.push(data[v]);
            }
        });
        update_str = update_str.substr(0, update_str.length - 1);

        var query = "update " + ew_usr_author_dtls_table + " set " + update_str + " where usr_id = ? and usr_bk_sk = ?";
        var params = [].concat(update_params, [usr_id, usr_bk_sk]);

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user/author Id
     * @returns {Promise}
     * @desc
     * - First we will fetch all courses given by author using {@link AdminCourse#getCoursesByAuthor}
     * - Then we will fetch info from {@link CassandraTables#ew_usr_author_dtl}
     * - Then merge those 2 results
     * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
     */
    getAuthorInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        //first fetch courses that he has already given
        adminCourseAPI.getCoursesByAuthor({
                authorId: usr_id
            })
            .then(function(res) {
                //courses
                var temp_courses = res || [];

                var courses = temp_courses.map(function(v, i) {
                    var obj = {
                        editable: false,
                        crs_bk_nm: v.courseName,
                        crs_bk_desc: v.courseShortDesc || ''
                    }
                    return obj;
                })

                //fetch courses from author info details
                var query = "select * from " + ew_usr_author_dtls_table + " where usr_id = ?";
                var params = [usr_id];

                cassconn.execute(query, params, function(err, res, r) {
                    if (err) {
                        debug(err);
                        defer.reject(err);
                        return;
                    }
                    var resArr = JSON.parse(JSON.stringify(res.rows));
                    var finalArr = [].concat(courses, resArr);
                    defer.resolve(finalArr);
                });
            })
            .catch(function(err) {
                debug(err);
            })
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user/author Id
     * @param {uuid} data.usr_bk_sk - unique id
     * @returns {Promise}
     * @desc
     * - This method will be used to delete given author information using usr_id and usr_bk_sk
     * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
     */
    deleteAuthorInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var usr_bk_sk = data.usr_bk_sk;

        var query = "delete from " + ew_usr_author_dtls_table + " where usr_id = ? and usr_bk_sk = ?";
        var params = [usr_id, usr_bk_sk];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user/author Id
     * @param {string} data.author_desc - Author Description
     * @returns {Promise}
     * @desc
     * - This method will be used to save/update author description using usr_id
     * - Tables Used : {@link CassandraTables#ew_usr_author_dtl}
     */
    saveAuthorDescInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var desc = data.author_desc;

        var query = "update " + ew_usr_author_dtls_table + " set author_desc=? where usr_id = ?";
        var params = [desc, usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    //////////////////////////////////
    // Teacher Profile Form Helper ///
    ////////////////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.tch_crs_sk - unique id
     * @param {text} data.tch_crs - Courses
     * @param {text} data.tch_desc - Desc
     * @param {text} data.edu_nm - Education Name
     * @returns {Promise}
     * @desc
     * - This method will be used to save/update Teacher Info
     * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
     */
    saveTeacherInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var tch_crs_sk = data.tch_crs_sk || Uuid.random();

        var keys = Object.keys(data || {});
        var update_str = "";
        var update_params = [];

        keys.forEach(function(v, i) {
            if (v !== 'usr_id' && v !== 'tch_crs_sk') {
                update_str += v + " = ?,";
                update_params.push(data[v]);
            }
        });
        update_str = update_str.substr(0, update_str.length - 1);

        var query = "update " + ew_usr_tch_dtls_table + " set " + update_str + " where usr_id = ? and tch_crs_sk = ?";
        var params = [].concat(update_params, [usr_id, tch_crs_sk]);

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @returns {Promise}
     * @desc
     * - This method will be used to get Teacher Info
     * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
     */
    getTeacherInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        //fetch courses from author info details
        var query = "select * from " + ew_usr_tch_dtls_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {uuid} data.tch_crs_sk - unique Id
     * @returns {Promise}
     * @desc
     * - This method will be used to delete Teacher Info using usr_id and tch_crs_sk
     * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
     */
    deleteTeacherInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var tch_crs_sk = data.tch_crs_sk;

        var query = "delete from " + ew_usr_tch_dtls_table + " where usr_id = ? and tch_crs_sk = ?";
        var params = [usr_id, tch_crs_sk];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.tch_desc - techer description
     * @returns {Promise}
     * @desc
     * - This method will be used to save/update teacher description
     * - Tables Used : {@link CassandraTables#ew_usr_tch_dtl}
     */
    saveTeacherDescInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var desc = data.tch_desc;

        var query = "update " + ew_usr_tch_dtls_table + " set tch_desc=? where usr_id = ?";
        var params = [desc, usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }


    //////////////////////////////////
    // Kid info Form Helper /////////
    ////////////////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.kid_usr_id - kid user id
     * @param {text} data.kid_edu_nm - Kid Eduction name
     * @param {text} data.kid_exm_nm - Kid Exam Name
     * @returns {Promise}
     * @desc
     * - This method will be used to save/update Kid Info
     * - Tables Used : {@link CassandraTables#ew_usr_parent_kid}
     */
    saveKidInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var kid_usr_id = data.kid_usr_id;
        var kid_edu_nm = data.kid_edu_nm;
        var kid_exm_nm = data.kid_exm_nm;


        var query = "update " + ew_usr_parent_kid_table + " set kid_exm_nm = kid_exm_nm + {'" + kid_exm_nm + "'},kid_edu_nm=? where usr_id = ? and kid_usr_id = ?";
        var params = [kid_edu_nm, usr_id, kid_usr_id]

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve({});
        });

        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @returns {Promise}
     * @desc
     * - This method will be used to get Kid Info
     * - Tables Used : {@link CassandraTables#ew_usr_parent_kid}
     */
    getKidInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;

        //fetch courses from author info details
        var query = "select * from " + ew_usr_parent_kid_table + " where usr_id = ?";
        var params = [usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {string} data.usr_id - user Id
     * @param {string} data.kid_usr_id - kid user Id
     * @returns {Promise}
     * @desc
     * - This method will be used to delete get Kid Info
     * - Tables Used : {@link CassandraTables#ew_usr_parent_kid}
     */
    deleteKidInfo(data) {
        var defer = Q.defer();

        var usr_id = data.usr_id;
        var kid_usr_id = data.kid_usr_id;

        var query = "delete from " + ew_usr_parent_kid_table + " where usr_id = ? and kid_usr_id = ?";
        var params = [usr_id, kid_usr_id];

        cassconn.execute(query, params, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows);
        });

        return defer.promise;
    }

    ///////////////////////////////////////
    // Friend Request notification table //
    ///////////////////////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.to_usr_id - user Id for which we want to get friend request notifications
     * @returns {Promise}
     * @desc
     * - This method will be used to get all friend request notifications for given user
     * - Then for all that notification users(user by whom this notificaitions are generated ) we will get short details using {@link CareerBook#getUsersShortDetails}
     * - Them Merge the results
     * - Tables Used : {@link CassandraTables#ew_fr_req_noti}
     */
    async getFriendRequestNotifications(data) {
        try {
            if (!data.to_usr_id) {
                return {
                    status: "error",
                    message: "to_usr_id field is required"
                }
            }
            var to_usr_id = data.to_usr_id; // Usr_id who received the request
            var query;
            var params;
            if(data.last_notification){
                query  = `select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,frm_usr_id,noti_msg,noti_vw_flg from ${ew_fr_req_noti_table} where to_usr_id=? and snt_ts < ? limit ${FETCH_NOTIFICATION_LIMIT}`;
                params  = [to_usr_id,data.last_notification];
            }else{
                query = `select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,frm_usr_id,noti_msg,noti_vw_flg from ${ew_fr_req_noti_table} where to_usr_id=? limit ${FETCH_NOTIFICATION_LIMIT}`;
                params = [to_usr_id];
            }

            var rows = await cassExecute(query, params);
            var frm_usr_ids = rows.map(function(v, i) {
                return v.frm_usr_id;
            })
            if (frm_usr_ids.length > 0) {
                var users = await this.getUsersShortDetails({
                    usr_ids: frm_usr_ids
                });
                var userMapping = {};
                //generate user mapping so we can direct fetch from dict
                users.map(function(v, i) {
                    delete v.usr_role
                    delete v.usr_email
                    delete v.usr_ph
                    userMapping[v.usr_id] = v;
                });

                var allNotifications = rows.map(function(v, i) {
                    //get sender
                    var frm_usr_id = v['frm_usr_id'];
                    //get user info
                    var user = userMapping[frm_usr_id];
                    //attach user info to item
                    v['usr_info'] = user;
                    return v;
                })
                return allNotifications
            }
            return rows;
        } catch (err) {
            throw new Error(err)
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.frm_usr_id - user Id who sent the request
     * @param {string} data.to_usr_id - user Id who will receive the frnd request
     * @param {string} data.noti_vw_flg - notification viewed or not
     * @returns {Promise}
     * @desc
     * - This method will be used to update frnd request notification for given user
     * - Tables Used : {@link CassandraTables#ew_fr_req_noti}
     */
    async updateFriendRequestNotifications(data) {
        try {
            if (!data.frm_usr_id) {
                return {
                    status: "error",
                    message: "frm_usr_id field is required"
                }
            }
            if (!data.to_usr_id) {
                return {
                    status: "error",
                    message: "to_usr_id field is required"
                }
            }
            if (!data.snt_ts) {
                return {
                    status: "error",
                    message: "snt_ts field is required"
                }
            }
            var frm_usr_id = data.frm_usr_id; // Usr_id who sent the request
            var to_usr_id = data.to_usr_id; // Usr_id who received the request
            var noti_vw_flg = data.noti_vw_flg;
            var snt_ts = data.snt_ts;

            var query = 'update ' + ew_fr_req_noti_table + ' set noti_vw_flg=? where frm_usr_id=? and to_usr_id=? and snt_ts = ?';
            var params = [noti_vw_flg, frm_usr_id, to_usr_id, snt_ts];
            await cassExecute(query, params)

            if(noti_vw_flg === 'true'){
              // update notification counter
              await adminSendNotificationAPI.decrementSendNotificationCount({
                usr_id : to_usr_id,
                type : 'cb'
              })
            }


            return data;
        } catch (err) {
            throw new Error(err)
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.frm_usr_id - user Id who sent the request
     * @param {string} data.to_usr_id - user Id who will receive the frnd request
     * @param {string} data.noti_vw_flg - notification viewed or not
     * @returns {Promise}
     * @desc
     * - This method will be used to insert new frnd request notification for given user
     * - Tables Used : {@link CassandraTables#ew_fr_req_noti}
     */
    async insertFriendRequestNotifications(data) {
        try {
            if (!data.frm_usr_id) {
                return {
                    status: "error",
                    message: "frm_usr_id field is required"
                }
            }
            if (!data.to_usr_id) {
                return {
                    status: "error",
                    message: "to_usr_id field is required"
                }
            }
            var frm_usr_id = data.frm_usr_id; // Usr_id who sent the request
            var to_usr_id = data.to_usr_id; // Usr_id who received the request
            var noti_msg = "sent friend request."
            var noti_vw_flg = data.noti_vw_flg || "false";
            var snt_ts = TimeUuid.now();

            var query = 'insert into ' + ew_fr_req_noti_table + ' (noti_msg,noti_vw_flg,frm_usr_id,to_usr_id,snt_ts) values (?,?,?,?,?) ';
            var params = [noti_msg, noti_vw_flg, frm_usr_id, to_usr_id, snt_ts];

            await cassExecute(query, params)

            await adminSendNotificationAPI.incrementSendNotificationCount({
              usr_id : to_usr_id,
              type : 'cb'
            })

            return data
        } catch (err) {
            throw new Error(err);
        }
    }

    ///////////////////////////////////////
    // Tagged Friends notification table //
    ///////////////////////////////////////

    /**
     * @param {JSON} data
     * @param {string} data.to_usr_id - user Id for which we want to get tagged frnds notifications
     * @returns {Promise}
     * @desc
     * - This method will be used to get all tagged notifications for given user
     * - Then for all that notification users(user by whom this notificaitions are generated), we will get short details using {@link CareerBook#getUsersShortDetails}
     * - Them Merge the results
     * - Tables Used : {@link CassandraTables#ew_pst_tag_noti}
     */
    async getTaggedFriendsNotifications(data) {
        try {
            if (!data.to_usr_id) {
                return {
                    status: "error",
                    message: "to_usr_id field is required"
                }
            }
            var to_usr_id = data.to_usr_id; // Usr_id who received the tag frnd noti
            var query;
            var params;
            if(data.last_notification){
                query  = `select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,pst_id,frm_usr_id,noti_msg,noti_vw_flg from ${ew_pst_tag_noti_table} where to_usr_id=? and snt_ts < ? limit ${FETCH_NOTIFICATION_LIMIT}`;
                params  = [to_usr_id,data.last_notification];
            }else{
                query = `select toTimestamp(snt_ts) as timestamp ,snt_ts,to_usr_id,pst_id,frm_usr_id,noti_msg,noti_vw_flg from ${ew_pst_tag_noti_table} where to_usr_id=? limit ${FETCH_NOTIFICATION_LIMIT}`;
                params = [to_usr_id];
            }

            var jsonArr = await cassExecute(query, params);
            var frm_usr_ids = jsonArr.map(function(v, i) {
                return v.frm_usr_id;
            })
            if (frm_usr_ids.length > 0) {
                var users = await this.getUsersShortDetails({
                    usr_ids: frm_usr_ids
                });
                var userMapping = {};
                //generate user mapping so we can direct fetch from dict
                users.map(function(v, i) {
                    delete v.usr_role
                    delete v.usr_email
                    delete v.usr_ph
                    userMapping[v.usr_id] = v;
                });
                var allNotifications = jsonArr.map(function(v, i) {
                    //get sender
                    var frm_usr_id = v['frm_usr_id'];
                    //get user info
                    var user = userMapping[frm_usr_id];
                    //attach user info to item
                    v['usr_info'] = user;
                    return v;
                })
                return allNotifications
            }
            return jsonArr
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.frm_usr_id - user Id who tag the frnd
     * @param {string} data.to_usr_id - user Id who is being tagged
     * @param {string} data.noti_vw_flg - notification viewed or not
     * @returns {Promise}
     * @desc
     * - This function is used to update tagged notifications for given user and frnd_id
     * - Tables Used : {@link CassandraTables#ew_pst_tag_noti}
     */
    async updateTaggedFriendNotifications(data) {
        try {
            if (!data.frm_usr_id) {
                return {
                    status: "error",
                    message: "frm_usr_id field is required"
                }
            }
            if (!data.to_usr_id) {
                return {
                    status: "error",
                    message: "to_usr_id field is required"
                }
            }
            if (!data.pst_id) {
                return {
                    status: "error",
                    message: "pst_id field is required"
                }
            }
            if (!data.snt_ts) {
                return {
                    status: "error",
                    message: "snt_ts field is required"
                }
            }
            var frm_usr_id = data.frm_usr_id; // Usr_id who tag the frnd
            var to_usr_id = data.to_usr_id; // Usr_id who is being tagged
            var noti_vw_flg = data.noti_vw_flg;
            var pst_id = data.pst_id;
            var snt_ts = data.snt_ts;

            var query = 'update ' + ew_pst_tag_noti_table + ' set noti_vw_flg=? where frm_usr_id=? and to_usr_id=? and pst_id = ? and snt_ts = ?';
            var params = [noti_vw_flg, frm_usr_id, to_usr_id, pst_id, snt_ts];
            await cassExecute(query, params);

            if(noti_vw_flg === "true"){
              await adminSendNotificationAPI.decrementSendNotificationCount({
                usr_id : to_usr_id,
                type : 'cb'
              })
            }

            return data
        } catch (err) {
            throw new Error(err)
        }
    }

    /**
     * @param {JSON} data
     * @param {string} data.frm_usr_id - user Id who tag the frnd
     * @param {string} data.to_usr_ids - all Usr_id who received the tag frnd notification
     * @param {string} data.noti_vw_flg - notification viewed or not
     * @param {string} data.pst_id - post id in which frnds are tagged
     * @returns {Promise}
     * @desc
     * - This function is used to insert new tagged notifications for all tagged user(s)
     * - Tables Used : {@link CassandraTables#ew_pst_tag_noti}
     */
    async insertTaggedFriendNotifications(data) {
        try {
            if (!data.frm_usr_id) {
                return {
                    status: "error",
                    message: "frm_usr_id field is required"
                }
            }
            if (!data.to_usr_ids) {
                return {
                    status: "error",
                    message: "to_usr_ids field is required"
                }
            }
            if (to_usr_ids && to_usr_ids.length <= 0) {
                return {
                    status: "error",
                    message: "Atleast one usr_id required in to_usr_ids field"
                }
            }
            var frm_usr_id = data.frm_usr_id; // Usr_id who sent the tag frnd notification
            var to_usr_ids = data.to_usr_ids; // all Usr_id who received the tag frnd notification
            var noti_msg = "tagged you in the post";
            var noti_vw_flg = data.noti_vw_flg || "false";
            var pst_id = data.pst_id;
            var snt_ts = TimeUuid.now();
            var queries = [];

            to_usr_ids.forEach(function(v, i) {
                var query = 'insert into ' + ew_pst_tag_noti_table + ' (noti_msg,noti_vw_flg,frm_usr_id,to_usr_id,snt_ts,pst_id) values (?,?,?,?,?,?) ';
                var params = [noti_msg, noti_vw_flg, frm_usr_id, v, snt_ts, pst_id];

                queries.push({
                    query: query,
                    params: params
                });
            })

            await cassBatch(queries, null)

            for (var i = 0; i < to_usr_ids.length; i++) {
              let usr_id = to_usr_ids[i]
              // update notification counter
              await adminSendNotificationAPI.incrementSendNotificationCount({
                usr_id : usr_id,
                type : 'cb'
              })
            }

            return data;
        } catch (err) {
            throw new Error(err)
        }
    }

    async getAllCountry(){
        try{
            var query = 'select * from ' + ew_country_table;
            var data = await cassExecute(query, null)
            return data;
        }catch(err){
            throw new Error(err)
        }
    }

    async getAllCitiesOfCountry(data){
        try{
            if(!data.cntry_id){
                return {
                    status  : "error",
                    message : "cntry_id field is required to fetch cities"
                }
            }
            var query = 'select * from ' + ew_country_city_table + " where cntry_id = ? ";
            var params = [data.cntry_id]
            var data = await cassExecute(query, params)
            return data;
        }catch(err){
            throw new Error(err)
        }
    }
}


module.exports = new CareerBook();
