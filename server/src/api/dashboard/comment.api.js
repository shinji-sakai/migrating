var Q = require('q');
var fs = require('fs');
var path = require('path');
var shortid = require('shortid');
var uuid = require('uuid');
var configPath = require('../../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var cassandra = require('cassandra-driver');
var Uuid = cassandra.types.Uuid;
var TimeUuid = cassandra.types.TimeUuid;

var globalFunctions = require(configPath.api.globalFunctions);
var globalConfig = require(configPath.config.globalConfig);
var debug = require('debug')("app:api:dashboard:cmnts");
var profileAPI = require(configPath.api.dashboard.profile);
var cassExecute = require(configPath.lib.utility).cassExecute;
var dashboardStatsAPI = require(configPath.api.dashboard.stats)

// var comment_lvl0_table = 'ew1.vt_usr_comnt_lvl0';
var comment_lvl0_table = 'ew1.ew_usr_comnt_lvl0';
var comment_lvl1_table = 'ew1.ew_usr_comnt_lvl1';
var comment_like_table = 'ew1.vt_qry_like';
var comment_like_stats_table = 'ew1.vt_qry_like_stats';
var comment_users_table = "ew1.vt_qry_like_usrs";
const COMMENT_FETCH_LIMIT = 5

class Comments {
    constructor() {
        this.comments = [];
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.usr_id - Usrid who posted the comment
     * @param {uuid} data.pst_id - Post ID to which comment is related
     * @param {set} data.cmnt_atch - Array Of Comment Attachment
     * @param {uuid} data.cmnt_cmnt_id - If comment is level 1 comment then this will be id of parent comment
     * @param {set} data.cmnt_img - Image attached to comment
     * @param {text} data.cmnt_lvl - Comment Level
     * @param {text} data.cmnt_txt - Comment Text
     * @param {set} data.cmnt_vid - Video attached to comment.
     * @param {set} data.from - Source of comment.This will tell us , from where comment is coming for e.g. qa-ans,qa-comment,cb-comment
     * @returns {Promise}
     * @desc
     * - This function is used to insert new comment
     * - If Comment source is qa-ans then we will increment dashboard stats for qa questions answered. See {@link DashboardStats#incrementForumQuestionAnsweredCounter}
     * - Else we will increment dashboard stats for comments. See {@link DashboardStats#incrementCommentCounter}
     * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
     */
    async save(data) {
        try{
            var pst_id = data.pst_id;
            var cmnt_id = TimeUuid.now();
            var cmnt_ts = new Date();
            var cmnt_txt = data.cmnt_txt;
            var cmnt_cmnt_id = data.cmnt_cmnt_id || null;
            var cmnt_lvl = data.cmnt_lvl;
            var usr_id = data.usr_id;

            var cmnt_from = data.from; // this will tell us , from where comment is coming for e.g. qa-ans,qa-comment,cb-comment

            let insertPost;
            let insertPost_args;

            if(cmnt_lvl !== '1'){
                // if 0 level comment
                insertPost = 'INSERT INTO ' + comment_lvl0_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_lvl,usr_id)  ' + 'VALUES(?, ?, ?, ?, ?,?);';
                insertPost_args = [pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_lvl, usr_id];
            }else{
                // if 1 level comment
                insertPost = 'INSERT INTO ' + comment_lvl1_table + ' (pst_id, cmnt_id,cmnt_ts,cmnt_txt,cmnt_lvl,usr_id,cmnt_cmnt_id)  ' + 'VALUES(?,?,?,?,?,?,?);';
                insertPost_args = [pst_id, cmnt_id, cmnt_ts, cmnt_txt, cmnt_lvl, usr_id,cmnt_cmnt_id];
            }

            await cassExecute(insertPost, insertPost_args);

            try {
                if (cmnt_from === 'qa-ans') {
                    await dashboardStatsAPI.incrementForumQuestionAnsweredCounter({
                        usr_id: usr_id
                    });
                } else {
                    await dashboardStatsAPI.incrementCommentCounter({
                        usr_id: usr_id
                    });
                }
            } catch (err) {
                debug(err);
            }

            return {
                cmnt_id: cmnt_id,
                cmnt_ts: cmnt_ts
            }
        }catch(err){
            throw new Error(err);
        }
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.pst_id - Post ID for which we want to get all comments
     * @returns {Promise}
     * @desc
     * - This function is used to get all comments for given postId
     * - After getting all comments we will get all users from those comments and we will get short details for those users using {@link CareerBook#getUsersShortDetails}
     * - Then merge the results.
     * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
     */
    async get(data) {

        try{
            let { pst_id , cmnt_lvl , cmnt_cmnt_id , last_cmnt_id } = data;
            if(!pst_id || !cmnt_lvl){
                return {
                    status : 'error',
                    message : 'pst_id and cmnt_lvl are required'
                }
            }
            let qry;
            let params;
            if(cmnt_lvl === '0'){
                // if 0 level comment
                if(last_cmnt_id){
                    // if user want to fetch from last_cmnt_id
                    qry = `select * from ${comment_lvl0_table} where pst_id = ? and cmnt_id < ? limit ${COMMENT_FETCH_LIMIT}`
                    params = [pst_id,last_cmnt_id]
                }else{
                    // if user want to fetch first few comments
                    qry = `select * from ${comment_lvl0_table} where pst_id = ? limit ${COMMENT_FETCH_LIMIT}`
                    params = [pst_id]
                }
            }else{
                 // if 1 level comment
                if(last_cmnt_id){
                    // if user want to fetch from last_cmnt_id
                    qry = `select * from ${comment_lvl1_table} where pst_id = ? and cmnt_cmnt_id = ? and  cmnt_id < ? limit ${COMMENT_FETCH_LIMIT}`
                    params = [pst_id , cmnt_cmnt_id , last_cmnt_id]
                }else{
                    // if user want to fetch first few comments
                    qry = `select * from ${comment_lvl1_table} where pst_id = ? and cmnt_cmnt_id = ? limit ${COMMENT_FETCH_LIMIT}`
                    params = [pst_id,cmnt_cmnt_id]
                }
            }

            var jsonComments = await cassExecute(qry,params);

            jsonComments = jsonComments.filter(function(v, i) {
                if (v.usr_id) {
                    return true
                }
                return false;
            });

            //get all userse
            var usr_ids = jsonComments.map((v, i)=>v.usr_id);
            var users = await profileAPI.getUsersShortDetails({ usr_ids: usr_ids })

            var userMapping = {};
            //generate user mapping so we can direct fetch from dict
            users.map(function(v, i) {
                delete v.usr_email;
                delete v.usr_role;
                delete v.usr_ph;
                userMapping[v.usr_id] = v;
            });

            var allCommentsdata = jsonComments.map(function(v, i) {
                //get post author
                var crt_by = v['usr_id'];
                //get user info
                var user = userMapping[crt_by];
                //attach user info to item
                v['usr_info'] = user;
                return v;
            })

            return allCommentsdata

        }catch(err){
            throw new Error(err);
        }
        // var defer = Q.defer();
        // var pst_id = Uuid.fromString(data.pst_id.trim());
        // var select = 'select * from ' + comment_lvl0_table + ' where pst_id = ? ';
        // const select_args = [pst_id];
        // var queryOptions = {
        //     prepare: true,
        //     consistency: cassandra.types.consistencies.quorum
        // };
        // cassconn.execute(select, select_args, {
        //     hint: ['uuid']
        // }, function(err, res, r) {
        //     if (err) {
        //         debug(err);
        //         defer.reject(err);
        //         return;
        //     }
        //     var rows = res.rows || [];
        //     //convert to json object
        //     var jsonComments = JSON.parse(JSON.stringify(rows));
        //     jsonComments = jsonComments.filter(function(v, i) {
        //         if (v.usr_id) {
        //             return true
        //         }
        //         return false;
        //     });
        //     //get all userse
        //     var usr_ids = jsonComments.map(function(v, i) {
        //         return v.usr_id
        //     });
        //     //get user short details
        //     profileAPI.getUsersShortDetails({
        //             usr_ids: usr_ids
        //         })
        //         .then(function(users) {

        //             var userMapping = {};
        //             //generate user mapping so we can direct fetch from dict
        //             users.map(function(v, i) {
        //                 userMapping[v.usr_id] = v;
        //             });

        //             var allCommentsdata = jsonComments.map(function(v, i) {
        //                 //get post author
        //                 var crt_by = v['usr_id'];
        //                 //get user info
        //                 var user = userMapping[crt_by];
        //                 //attach user info to item
        //                 v['usr_info'] = user;
        //                 return v;
        //             })

        //             defer.resolve(allCommentsdata);
        //         })
        //         .catch(function(err) {
        //             debug(err);
        //             defer.reject(err);
        //         })
        // });
        // return defer.promise;
    }



    /**
     * @param {JSON} data
     * @param {uuid} data.pst_id - Post ID
     * @param {uuid} data.cmnt_id - Comment Id which we want to edit
     * @param {timestamp} data.cmnt_ts - Comment Timestamp
     * @param {Array} data.cmnt_atch - Comment Attachments
     * @param {Array} data.cmnt_vid - Comment Video Attachments
     * @param {Array} data.cmnt_img - Comment Images Attachments
     * @param {string} data.cmnt_txt - Comment Text
     * @returns {Promise}
     * @desc
     * - This function is used to update comment data for given comment id
     * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
     */
    edit(data) {
        var defer = Q.defer();
        var pst_id = data.pst_id;
        var cmnt_id = data.cmnt_id;
        
        var cmnt_ts = new Date(data.cmnt_ts);
        var cmnt_txt = data.cmnt_txt;

        var cmnt_lvl = data.cmnt_lvl;
        var cmnt_cmnt_id = data.cmnt_cmnt_id;

        let updatePost;
        let updatePost_args;

        if(cmnt_lvl === '0'){
            updatePost = 'update ' + comment_lvl0_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ?';
            updatePost_args = [cmnt_txt, cmnt_id, pst_id];
        }else{
            updatePost = 'update ' + comment_lvl1_table + ' set cmnt_txt = ? where cmnt_id = ? and pst_id = ? and cmnt_cmnt_id = ?';
            updatePost_args = [cmnt_txt, cmnt_id, pst_id, cmnt_cmnt_id];
        }

        cassconn.execute(updatePost, updatePost_args, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.pst_id - Post ID
     * @param {uuid} data.cmnt_id - Comment Id which we want to edit
     * @param {timestamp} data.cmnt_ts - Comment Timestamp
     * @param {string} data.usr_id - Author of comment
     * @returns {Promise}
     * @desc
     * - This function is used to delete given comment id
     * - We will decrement dashboard stats comment counter. See {@link DashboardStats#decrementCommentCounter}
     * - Tables Used : {@link CassandraTables#vt_usr_comnt_lvl0}
     */
    delete(data) {
        var defer = Q.defer();
        var pst_id = data.pst_id;
        var cmnt_id = data.cmnt_id;
        var cmnt_ts = new Date(data.cmnt_ts);
        var usr_id = data.usr_id;

        var cmnt_lvl = data.cmnt_lvl;
        var cmnt_cmnt_id = data.cmnt_cmnt_id;

        let deletePost;
        let deletePost_args;

        if(cmnt_lvl === '0'){
            deletePost = 'delete from ' + comment_lvl0_table + ' where pst_id = ? and cmnt_id = ?';
            deletePost_args = [pst_id, cmnt_id];
        }else{
            deletePost = 'delete from ' + comment_lvl1_table + ' where pst_id = ? and cmnt_id = ? and cmnt_cmnt_id = ?';
            deletePost_args = [pst_id, cmnt_id, cmnt_cmnt_id];
        }

        cassconn.execute(deletePost, deletePost_args, async function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }

            try {
                await dashboardStatsAPI.decrementCommentCounter({
                    usr_id: usr_id
                });
            } catch (err) {
                debug(err);
            }

            defer.resolve([]);
        });


        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Query ID
     * @param {text} data.usr_id - User id who did like/dislike
     * @param {uuid} data.cmnt_id - Comment Id
     * @param {int} data.like_dlike_flg - Like/Dislike Flag
     * @returns {Promise}
     * @desc
     * - This function is used to update like/dislike flag for given comment
     * - Tables Used : {@link CassandraTables#vt_qry_like}
     */
    likeDislikeComment(data) {
        var defer = Q.defer();
        var qry_id = data.qry_id || null;
        var usr_id = data.usr_id || 'n/a';
        var cmnt_id = data.cmnt_id || null;
        var crt_ts = new Date();
        var like_dlike_flg = data.like_dlike_flg;

        var insert = 'insert into ' + comment_like_table + ' (qry_id,usr_id,cmnt_id,crt_ts,like_dlike_flg) values(?,?,?,?,?)';
        const insert_args = [qry_id, usr_id, cmnt_id, crt_ts, like_dlike_flg];

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.execute(insert, insert_args, queryOptions, function(err, res, r) {

            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.qry_id - Query ID
     * @param {text} data.usr_id - User id who did like/dislike
     * @returns {Promise}
     * @desc
     * - This function is used to get all like/dislike comment for given post and user
     * - Tables Used : {@link CassandraTables#vt_qry_like}
     */
    getLikeDislikeComments(data) {
        var defer = Q.defer();

        var qry_id = data.qry_id || null;
        var usr_id = data.usr_id || 'n/a';

        var select = 'select * from ' + comment_like_table + ' where qry_id = ? and  usr_id = ?';
        const select_args = [qry_id, usr_id];

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.execute(select, select_args, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows || []);
        });
        return defer.promise;
    }

    async getLikeDislikeCommentsByCmntId(data) {

        try{
            var qry_id = data.qry_id || null;
            var usr_id = data.usr_id || 'n/a';
            var cmnt_id = data.cmnt_id || null;

            if(!qry_id || !cmnt_id){
                return { err : "qry_id and cmnt_id parameters required"}
            }

            var select = 'select * from ' + comment_like_table + ' where qry_id = ? and  usr_id = ? and cmnt_id = ?';
            const select_args = [qry_id, usr_id, cmnt_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };

            let res = await cassExecute(select, select_args, queryOptions)
            return res;
        }catch(err){
            throw new Error(err)
        }
    }

    /**
     * @param {JSON} data
     * @param {uuid} data.cmnt_id - Comment ID
     * @returns {Promise}
     * @desc
     * - This function is used to get all users id who liked the comment
     * - Then get short details for all users using {@link CareerBook#getUsersShortDetails}
     * - Remove email id from all users as we dont require it on client side and it is sensitive info
     * - Tables Used : {@link CassandraTables#vt_qry_like_usrs}
     */
    async getCommentUsers(data) {
        try {
            var cmnt_id = data.cmnt_id;
            var query = "select * from " + comment_users_table + " where cmnt_id=?";
            var params = [cmnt_id];
            var res = await cassExecute(query, params);
            //get all user id
            var usersIdArr = res.map((v, i) => v.usr_id);
            //get users short details
            var usersDetails = await profileAPI.getUsersShortDetails({
                    usr_ids: usersIdArr
                })
                //remove email from all users and send it to client
            usersDetails = usersDetails.map((v, i) => {
                delete v["usr_email"];
                return v;
            })
            return usersDetails;
        } catch (err) {
            debug(err);
            throw err;
        }
    }


    /**
     * @param {JSON} data
     * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
     * @param {uuid} cmnt_id - Comment Id
     * @param {counter} dlike_cnt - Dislike Count for e.g. -1/0/1
     * @param {counter} like_cnt - Like Count for e.g. -1/0/1
     * @returns {Promise}
     * @desc
     * - This function is used to increment/decrement like/dislike counts for given comment
     * - Tables Used : {@link CassandraTables#vt_qry_like_stats}
     */
    updateCommentLikeDislikeCount(data) {
        var defer = Q.defer();

        //data = {qry_id : qry_id , cmnt_id : cmnt_id , like : -1/0/1 , dislike : -1/0/1}

        var qry_id = data.qry_id || null;
        var cmnt_id = data.cmnt_id || null;

        var like_cnt_str = ' like_cnt = like_cnt + ' + (data.like || 0);
        var dlike_cnt_str = ' dlike_cnt = dlike_cnt + ' + (data.dislike || 0);
        var where_clause = ' where qry_id = ? and cmnt_id = ?';

        var update_like = 'update ' + comment_like_stats_table + ' set ' + like_cnt_str + "," + dlike_cnt_str + where_clause;
        const update_args = [qry_id, cmnt_id];

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.execute(update_like, update_args, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve([]);
        });
        return defer.promise;
    }

    /**
     * @param {JSON} data
     * @param {uuid} qry_id - (it can be CB Post Id / QA Query ID)
     * @returns {Promise}
     * @desc
     * - This function is used to get comment like/dislike count
     * - Tables Used : {@link CassandraTables#vt_qry_like_stats}
     */
    getCommentLikeDislikeCount(data) {
        var defer = Q.defer();

        //data = {qry_id : qry_id}

        var qry_id = data.qry_id || null;

        var select_like = 'select * from ' + comment_like_stats_table + ' where qry_id = ?';
        const select_args = [qry_id];

        var queryOptions = {
            prepare: true,
            consistency: cassandra.types.consistencies.quorum
        };
        cassconn.execute(select_like, select_args, queryOptions, function(err, res, r) {
            if (err) {
                debug(err);
                defer.reject(err);
                return;
            }
            defer.resolve(res.rows || []);
        });
        return defer.promise;
    }

    async getCommentLikeDislikeCountByCmntID(data) {
        try {
            //data = {qry_id , cmnt_id}

            var qry_id = data.qry_id || null;
            var cmnt_id = data.cmnt_id || null;

            if(!qry_id || !cmnt_id){
                return { err : "qry_id and cmnt_id parameters required"}
            }

            var select_like = 'select * from ' + comment_like_stats_table + ' where qry_id = ? and cmnt_id = ?';
            const select_args = [qry_id , cmnt_id];

            var queryOptions = {
                prepare: true,
                consistency: cassandra.types.consistencies.quorum
            };

            let res = await cassExecute(select_like, select_args, queryOptions);

            return res;
        } catch (err) {
            throw new Error(err);
        }


    }
}

module.exports = new Comments();