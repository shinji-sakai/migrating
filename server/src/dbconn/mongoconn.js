var MongoClient = require('mongodb').MongoClient;
var Q = require('q');
var debug = require('debug')('app:dbconn:mongoconn');
var config = require('../config.js')
var db_singleton = null;

var getConnection = function() {
    // if (db_singleton) {
        // return Q.when(db_singleton);
    // } else {
        //52.91.86.191
        //52.74.36.254
        //52.4.84.210
        //54.169.164.206
        //52.76.154.91
        //139.59.1.89
    var connURL = "mongodb://" + config.MONGO_DB_SERVER_URL + ":7070/dvbydt";
    return MongoClient.connect(connURL)
        .then(function(db) {
            db_singleton = db;
            return db;
        })
        .catch(function(err) {
            debug(err);
            throw err;
        })
    // }
};

module.exports = getConnection;