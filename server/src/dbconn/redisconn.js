var redis = require("redis");
var bluebird = require("bluebird");
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var configPath = require('../configPath.js');
var config = require('../config.js')
var logger = require(configPath.lib.log_to_file);
let singleton = null;
let CON = {}; // store redis connections as Object

function getRedisClient(type){
	let c_type = type || 'DEFAULT'; // allow infinite types of connections
	if(!CON[c_type] || !CON[c_type].connected){
    	CON[c_type] = redisConnection();
  	}
  	return CON[c_type];	
}

function redisConnection() {
  	let client = redis.createClient({
		// host : "127.0.0.1",
		host : config.DB_SERVER_URL,
		port : "6060"
	});
	client.on("error", function (err) {
	    logger.debug("Redis Client Error",err);
	});
	return client;
}

module.exports = getRedisClient;