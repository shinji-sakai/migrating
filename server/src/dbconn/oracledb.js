if (!process.env.NO_ORACLE) {
    var oracledb = require('oracledb');
    var configPath = require('../configPath.js');
    var logger = require(configPath.lib.log_to_file);

    async function getConnection() {
        try {
            var connection = await oracledb.getConnection({user: "scott", password: "jeevan", connectString: "139.59.41.144/pdbedw"})
            return connection;
        } catch (err) {
            logger.error("oracle connection error...", err);
            throw err;
        }
    }

    async function releaseConnection(conn) {
        try {
            await conn.close();
            return;
        } catch (err) {
            logger.error("oracle releaseConnection error...", err);
            throw err;
        }
    }

    module.exports = {
        getConnection,
        releaseConnection
    };
} else {
    module.exports = {
        getConnection: () => {},
        releaseConnection: () => {}
    };
}
