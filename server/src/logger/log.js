var log4js = require('log4js'); // include log4js
var logsConf = require("../config").LOGS_CONF;
var logsPath = logsConf.logs_path;
var logsEnabled = logsConf,logs_enabled;
var fs = require('fs');

const dir = `${__dirname}/${logsPath}`;

// create logs directory if not exist.
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}


log4js.configure({ // configure to use all types in different files.
    appenders: [
        {   type: 'dateFile',
            filename: (`${__dirname}/${logsPath}/error.log`), // specify the path where u want logs folder error.log
            category: 'error',
            pattern: "-dd-MM",
            maxLogSize: 20480,
            backups: 10,
            alwaysIncludePattern:false
        },
        {   type: "dateFile",
            filename: (`${__dirname}/${logsPath}/info.log`), // specify the path where u want logs folder info.log
            category: "info",
            pattern: "-dd--MM",
            maxLogSize: 20480,
            backups: 10,
            "alwaysIncludePattern":false
        },
        {   type: 'dateFile',
            filename: (`${__dirname}/${logsPath}/debug.log`), // specify the path where u want logs folder debug.log
            category: 'debug',
            pattern: "-dd--MM",
            maxLogSize: 20480,
            backups: 10,
            alwaysIncludePattern: false
        },
        {   type: 'dateFile',
            filename: (`${__dirname}/${logsPath}/warn.log`), // specify the path where u want logs folder error.log
            category: 'warn',
            pattern: "-dd-MM",
            maxLogSize: 20480,
            backups: 10,
            alwaysIncludePattern:false
        },
        {   type: 'dateFile',
            filename: (`${__dirname}/${logsPath}/fatal.log`), // specify the path where u want logs folder error.log
            category: 'fatal',
            pattern: "-dd-MM",
            maxLogSize: 20480,
            backups: 10,
            alwaysIncludePattern:false
        }
    ]
});

var loggerinfo = log4js.getLogger('info'); // initialize the var to use.
var loggererror = log4js.getLogger('error'); // initialize the var to use.
var loggerdebug = log4js.getLogger('debug'); // initialize the var to use.
var loggerwarn = log4js.getLogger('warn'); // initialize the var to use.
var loggerfatal = log4js.getLogger('fatal'); // initialize the var to use.



var logInfo = function(level,message){
	
    if(level=='error' && logsEnabled.error=="true"){
		loggererror.error(message);
    }else if(level == "info" && logsEnabled.info=="true"){
        loggerinfo.info(message);
    }else if(level == "debug" && logsEnabled.debug=="true"){
        loggerdebug.debug(message);
    }else if(level == "warn" && logsEnabled.warn=="true"){
        loggerwarn.warn(message);
    }else{
       loggererror.error(message); 
    }   
    return;
}


exports.logInfo = logInfo;