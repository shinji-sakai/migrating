var kafka = require('kafka-node'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client("ip-10-0-0-81.ap-southeast-1.compute.internal:2181,ip-10-0-0-126.ap-southeast-1.compute.internal:2181,ip-10-0-0-193.ap-southeast-1.compute.internal:2181"),
    consumer = new HighLevelConsumer(
        client,
        [
            { topic: 'useridle' }
        ],
        {
            groupId: 'my-group3'
        }
    );
consumer.on('message', function (message) {
    console.log(message);
});
