var mongodb = require('mongodb');

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
var url = 'mongodb://localhost:27017/edw';

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);

        // Get the documents collection
        var collection = db.collection('users');

        //Create some users
        var user1 = {name: 'modulus admin', age: 42, roles: ['admin', 'moderator', 'user']};
        var user2 = {name: 'modulus jeevan', age: 22, roles: ['user']};
        var user3 = {name: 'modulus super admin', age: 92, roles: ['super-admin', 'admin', 'moderator', 'user']};

        // Insert some users
        collection.insertMany([user1, user2, user3], function (err, result) {
            if (err) {
                console.log(err);
            } else {
               // console.log(" Inserted Documents :"+ result.length,result);
                console.log("Inserted");
            }
            //Close connection
          //  db.close();
        });

        collection.find({name: 'modulus jeevan'}).toArray(function (err, result) {
            if (err) {
                console.log(err);
            } else if (result.length) {
                console.log('Found:', result);
            } else {
                console.log('No document(s) found with defined "find" criteria!');
            }
            db.close();
        });


    }
});