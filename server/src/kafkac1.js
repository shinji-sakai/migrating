var kafka = require('kafka-node'),
     config = require('./config.js'),

    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(
        client,
        [
            { topic: 'userclick' }
        ],
        {
            groupId: 'my-group1'
        }
    );

console.log("Consumer ZOOKEEPER_KEY....",config.ZOOKEEPER_KEY);
consumer.on('message', function (message) {
    console.log(message);
});
