var configPath = require('../configPath.js');
var cassconn = require(configPath.dbconn.cassconn);
var config = require('../config.js');
var Q = require('q');
var cassExecute = require(configPath.lib.utility).cassExecute;
var cassBatch = require(configPath.lib.utility).cassBatch;
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

var forum_table = 'ew1.vt_forum_qrys';

async function transferQAQuestionsIdToRedis(argument) {
	try{
		var selectQry = 'select * from ' + forum_table;
		var res = await cassExecute(selectQry);
		if(res && res.length > 0){
			res.sort((a,b)=>{
				let a_date = new Date(a.qry_ts)
				let b_date = new Date(b.qry_ts)
				return a_date < b_date
			})
			console.log(`Found ${res.length} QA Question in cassandra`)
			let q_ids = res.map((v,i)=>v.qry_id);
			await redisCli.delAsync("qa-question-ids");
			await redisCli.rpushAsync("qa-question-ids",q_ids)
			console.log(`Added ${res.length} QA Question to redis`)
			process.exit(0);
		}
	}catch(err){
		logger.debug(err);
	}
}

transferQAQuestionsIdToRedis();