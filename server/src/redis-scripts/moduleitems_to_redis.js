var configPath = require('../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();

async function transfer() {
	try{
		var db = await getConnection();
		var moduleItemsCol = await db.collection('moduleitems');
		var moduleItems = await moduleItemsCol.find({}).toArray();
		
		var authItemsCol = await db.collection('authorizeitems');
		var authItems = await authItemsCol.find({}).toArray();
		var authItemsMap = {};
		authItems.forEach((v,i)=>{
			authItemsMap[v.itemId] = v;
		})

		for (let i = 0; i < moduleItems.length; i++) {
			var row = moduleItems[i];
			let courseId = row.courseId;
			var moduleDetail = row.moduleDetail || [];
			for (let j = 0; j < moduleDetail.length; j++) {
				let module = moduleDetail[j];
				let items = module.moduleItems || [];
				for (let k = 0; k < items.length; k++) {
					var item = items[k];
					var itemId = item.itemId;
					var itemPermission = authItemsMap[itemId].itemType || 'paid';
					item.itemPermission = itemPermission
				}
			}
			await redisCli.setAsync(courseId , JSON.stringify(row))
		}

		
		process.exit()
	}catch(err){
		console.log(err)
		return 0;
	}
}

transfer();