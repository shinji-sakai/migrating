var configPath = require('../configPath.js');
var getConnection = require(configPath.dbconn.mongoconn);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisCli = getRedisClient();
var Q=require('q');

async function allPracticeQuestonToRedis() {
	try{
		var db = await getConnection();
		var col = await db.collection('practiceQuestions');
		var cursor = await col.find({});
		let total = await cursor.count();
		console.log(`Total ${total} questions to transfer`)
		let k = 0;
		cursor.forEach(async  (question,i)=>{
			let id = question.questionId;
			let json = JSON.stringify(question);
			let ans = "" + (question.ans || []).join(",");
			let r = await redisCli.hmsetAsync(id,{
				"json" : json,
				"ans" : ans
			})
			k++;
			console.log(k + " / " + total)
			if(k === total){
				console.log("All questions transferred")
			}
		})

		// console.log(`Total ${arr_que.length} questions to transfer`)
		// let promises = [];
		// for (var i = 0; i < arr_que.length; i++) {
		// 	let question = arr_que[i];
		// 	let id = question.questionId;
		// 	let json = JSON.stringify(question);
		// 	let ans = "" + (question.ans || []).join(",");
		// 	let r = await redisCli.hmsetAsync(id,{
		// 		"json" : json,
		// 		"ans" : ans
		// 	})
		// 	promises.push(r);
		// }

		// await Q.all(promises);

		return;
	}catch(err){
		console.log(err)
		return 0;
	}
}

async function transferItemQuestions() {
	try{
		var db = await getConnection();
		var moduleItemsCol = await db.collection('moduleitems');
		var moduleItems = await moduleItemsCol.find({}).toArray();
		
		for (let i = 0; i < moduleItems.length; i++) {
			var row = moduleItems[i];
			let courseId = row.courseId;
			var moduleDetail = row.moduleDetail || [];
			for (let j = 0; j < moduleDetail.length; j++) {
				let module = moduleDetail[j];
				let items = module.moduleItems || [];
				for (let k = 0; k < items.length; k++) {
					var item = items[k];
					var itemId = item.itemId;
					if(item.itemQuestions){
						await redisCli.delAsync(itemId + '-qid')
						await redisCli.rpushAsync(itemId + '-qid' , item.itemQuestions)
					}
				}
			}	
		}
		return
	}catch(err){
		console.log(err)
		return 0;
	}
}

async function init(){
	try{
		console.log("Script Started")
		await transferItemQuestions();
		await allPracticeQuestonToRedis();
		// process.exit()
	}catch(err){
		console.log(err)
		return 0;
	}
	
}

init();