var configPath = require('../configPath.js');
var websocketApi = require(configPath.api.websocket);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
module.exports = function() {
    return (req, res, next) => {
        var shouldTrackSession = req.header('x-track-session');
        if (!shouldTrackSession) {
            // if request is not coming from api
            next();
            return
        }
        
        var sessionId = req.header('x-session-id');
        console.log("First Track session..............",sessionId)
        if (sessionId && sessionId != null) {

            console.log("Track session..............",sessionId,req.url)
        	
            //try to load a sessionId
            req.sessionStore.get(sessionId, function(err, sess) {
                if (!err ) {
                    var shouldCheckForAuth = false;
                    console.log("getting from redis....",sessionId,sess)
        			res.set('sess-id', req.session.id);
                    // create session using sessionstore
                    req.sessionStore.set(sessionId, sess || {cookie: {httpOnly: false,maxAge: null }}, async function(err) {
                        if (!err) {
                            if (req.session.loginData) {
                                try {
                                    // get current user from session
                                    var current_user_id = req.session.loginData.usr_id
                                        // get user session id which is already stored in redis
                                    var session_id_from_redis = await redisClient.getAsync(current_user_id + "-session");
                                    // check session id in redis exist or not
                                    if (session_id_from_redis && session_id_from_redis != 'null') {
                                        if (session_id_from_redis != sessionId) {
                                            req.sessionStore.get(session_id_from_redis, function(err, _sess) {
												if(!err){
													// send logout msg to other loggedin browser
                                                	websocketApi.logoutMsgToOtherBrowser(current_user_id,_sess.socketId)
												}
                                            })
                                        }
                                    }else{
                                        shouldCheckForAuth = true;

                                    }
                                } catch (err) {
                                    console.log("single-login.js err...", err)
                                }
                            }else{
                                shouldCheckForAuth = true;
                            }
                            if(shouldCheckForAuth){
                                var isUserLoggedIn = req.header('Authorization');
                                if(isUserLoggedIn){
                                    res.set('shouldLogout','true');
                                    console.log("User is not loggedin...............")    
                                }
                            }
                        }
                        next();
                    });
                } else {
                    console.log("Generate session..............",req.url)
                    req.session.regenerate(function(err) {
                    	res.set('sess-id', req.session.id);
                        next();
                    });
                }
            });

        } else {
            req.session.regenerate(function(err) {
            	res.set('sess-id', req.session.id);
                next();
            });
        }
    };
}