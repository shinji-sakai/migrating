var configPath = require('../configPath.js');
var config = require('../config.js');
var jwt = require('jsonwebtoken');
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
module.exports = () => {
    return (req, res, next) => {
        var userToken = req.header('Authorization');
        if (!userToken) {
            // if user is not logged in or not api request
            next();
            return
        }else{
            var jwtToken = userToken.substr(4)
            var token = jwt.verify(jwtToken, config.TOKEN_SECRET, async function(err, decoded) {
                if(err){
                    // if jwt token is not valid , if malformed , or expired
                    res.set('shouldLogout','true');
                    // console.log("error in decoding jwt token",err)
                }else{
                    if(decoded.userId){
                        var usr_sess_detail = await redisClient.getAsync("session-" + decoded.userId);
                        if(usr_sess_detail && usr_sess_detail != 'null'){
                            if(jwtToken != usr_sess_detail){
                                res.set('shouldLogout','true');
                            }else{
                                // console.log("same user.....");
                            }
                        }else{
                            res.set('shouldLogout','true');
                        }
                    }else{
                        // if there is no user id in token
                        res.set('shouldLogout','true');
                    }
                }
                next();
            });
        }
    }
}