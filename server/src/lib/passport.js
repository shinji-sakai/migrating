var passport = require("passport");
var passportJWT = require("passport-jwt");
var config = require("../config.js");
var ExtractJwt = passportJWT.ExtractJwt;
var JWTStrategy = passportJWT.Strategy;

var params = {
    secretOrKey: config.TOKEN_SECRET,
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    ignoreExpiration: false
};
var configPath = require('../configPath.js');
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
var cassExecute = require(configPath.lib.utility).cassExecute;

module.exports = function() {
    var strategy = new JWTStrategy(params, function(payload, done) {
        // Here in our case, This strategy will check token expired or not because of ignoreExpiration: false option
        // If it is expired then it will return error
        // If not then it will go to next line
        // and check in redis db
        redisClient.get(payload.userId,async function(err,usr){
            if(err){
                getUserDetailFromCassandra();
            }else if(usr === null || usr === 'null'){
                getUserDetailFromCassandra();
            }else{
                usr = JSON.parse(usr);
                if(usr.is_act === 'true'){
                    done(null, usr);    
                }else{
                    return done("User is inactive", false);
                }
            }
        })

        async function getUserDetailFromCassandra(){
            try{
                let qry = `select * from ew1.vt_usr_lgn where usr_id = ?`;
                let params = [payload.userId]
                let rows = await cassExecute(qry,params)
                if(rows[0]){
                    let row = rows[0];
                    if(row.is_act === 'true'){
                        return done(null, rows[0]);    
                    }else{
                        return done("User is inactive", false);
                    }
                }else{
                    return done("No User Found", false);
                }
            }catch(err){
                return done("No User Found", false);
            }
        }
    });
    passport.use(strategy);
    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function() {
       return (req, res, next) => {
                passport.authenticate('jwt', (err, data, info) => {
                    if (err || info || !data) {
                        res.status(401).send({
                            error : "Authorization Required."
                        });
                    } else {
                        req['user'] = data;
                        next();
                    }
                })(req, res, next);
            };
        }
    };
}();

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});