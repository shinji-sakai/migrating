var configPath = require('./configPath.js');

var kafka = require('kafka-node'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    config = require('./config.js'),
    rtdb_kafka = require('./rtdb_kafka'),
    client = new kafka.Client(config.ZOOKEEPER_KEY),
    consumer = new HighLevelConsumer(
        client,
        [
            { topic: 'userclick' }
        ],
        {
            groupId: 'my-group1'
        }
    );

var broadcast = global.ewWebSocket;
var user_click = {};
consumer.on('message', function (message) {
    //console.log(message);
    var jsonMsg = JSON.parse(message.value);
    try{
        console.log("User Click happen , inserting into rtdb");
    }catch(e){
        console.log(e);
    }
    var user = jsonMsg["usr_id"];
    if(user_click[user]>=0){
        console.log("Found User in arr",user_click[user]);
        var clicks = user_click[user]++;
        broadcast.emit("message",{name:user,clickcount:clicks});
        rtdb_kafka.update(user,clicks);
    }else{
        rtdb_kafka.fetch(user,function(clicks){
            user_click[user] = clicks++;
            broadcast.emit("message",{name:user,clickcount:clicks});
            console.log("users",user_click);
        });

    }
});


