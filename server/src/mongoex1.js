/**
 * Created by jeevan on 1/19/2016.
 */

var MongoClient = require('mongodb').MongoClient,
    co = require('co'),
    assert = require('assert');


co(function*() {
    // Connection URL
    var db = yield MongoClient.connect('mongodb://localhost:27017/edw');
    console.log("Connected correctly to server");

    // Insert a single document
    var r = yield db.collection('jeevan').insertOne({a:1});
    assert.equal(2, r.insertedCount);

    // Insert multiple documents
    var r = yield db.collection('jeevan').insertMany([{a:2}, {a:3}]);
    assert.equal(2, r.insertedCount);

    // Close connection
    db.close();
}).catch(function(err) {
    console.log(err.stack);
});
