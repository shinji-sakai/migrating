var express = require('express');
var router = express.Router();
var path = require('path');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');

var configPath = require('../../configPath.js');
var api = require(configPath.api.forums.ans);
var utility = require(configPath.lib.utility);
let lib_passport_auth = require(configPath.lib.passport).authenticate; 
let passporAuth = require(configPath.lib.passport);

router.post('/saveAnswer',  passporAuth.authenticate() , function(req, res) {
    req.body.usr_id = req.user.usr_id;
    api.saveAnswer(req.body)
        .then(function(d) {
            res.status(200).send(d);
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
        });
});

router.post('/getAllAnswersOfQuestion', function(req, res) {
    api.getAllAnswersOfQuestion(req.body)
        .then(function(d) {
            res.status(200).send(d);
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
        });
});

router.post('/getAnswerOfQuestionUsingId', function(req, res) {
    api.getAnswerOfQuestionUsingId(req.body)
        .then(function(d) {
            res.status(200).send(d);
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
        });
});

router.post('/editAnswer',passporAuth.authenticate(), function(req, res) {
    req.body.usr_id = req.user.usr_id;
    api.editAnswer(req.body)
        .then(function(d) {
            res.status(200).send(d);
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
        });
});

router.post('/deleteAnswer',passporAuth.authenticate(), function(req, res) {
    req.body.usr_id = req.user.usr_id;
    api.deleteAnswer(req.body)
        .then(function(d) {
            res.status(200).send(d);
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
        });
});

router.post('/likeDislike',passporAuth.authenticate(), function(req, res) {
    req.body.usr_id = req.user.usr_id;
    api.likeDislike(req.body)
        .then(function(d) {
            res.status(200).send(d);
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
        });
});

// updateCommentLikeDislikeCount
router.post('/updateLikeDislikeCount',passporAuth.authenticate(), function(req, res) {
    api.updateLikeDislikeCount(req.body)
        .then(function(d) {
            res.status(200).send(d);
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
        });
});

module.exports = router;