var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var mail = require(configPath.api.mail);
var registerTemplate = require('../../api/mail/registerTemplate.js');
var forgotpasswordTemplate = require('../../api/mail/forgotpasswordTemplate.js');
var resendotpTemplate = require('../../api/mail/resendotpTemplate.js');
var enrollbtnTemplate = require('../../api/mail/enrollBtnTemplate.js');
var paySecurelyBtnTemplate = require('../../api/mail/paySecurelyBtnTemplate.js');
var subscribeAPI = require(configPath.api.subscribe.subscribe);

router.post('/send',function(req,res){
	mail.sendMail(req.body)
		.then(function success(data) {
			res.status(200).send(data);
		})
		.catch(function(err){
			console.error(err);
			res.status(500).send(err);
		})
})

router.post('/registerTemplate',async function(req,res){
	try{	
		var d = await registerTemplate(req.body);
		var html = d.html;
		req.body.text = html;
		var r = await mail.sendMail(req.body);
		res.status(200).send(html);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/forgotpasswordTemplate',async function(req,res){
	try{	
		var d = await forgotpasswordTemplate(req.body);
		var html = d.html;
		req.body.text = html;
		var r = await mail.sendMail(req.body);
		res.status(200).send(html);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/resendotpTemplate',async function(req,res){
	try{	
		var d = await resendotpTemplate(req.body);
		var html = d.html;
		req.body.text = html;
		var r = await mail.sendMail(req.body);
		res.status(200).send(html);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/sendBulkEmails',async function(req,res){
	try{
		var r = await mail.sendBulkEmails(req.body)
		res.status(200).send(r);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/subscribeUser',async function(req,res){
	try{
		var r = await subscribeAPI.subscribeUser(req.body)
		res.status(200).send(r);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/unsubscribeUser',async function(req,res){
	try{
		var r = await subscribeAPI.unsubscribeUser(req.body)
		res.status(200).send(r);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/getAllSubscribers',async function(req,res){
	try{
		var r = await subscribeAPI.getAllSubscribers(req.body)
		res.status(200).send(r);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/sendMailToSupportForEnrollBtn',async function(req,res){
	try{	
		var d = await enrollbtnTemplate(req.body);
		var html = d.html;
		req.body.text = html;
		req.body.to = "support@examwarrior.com";
		var r = await mail.sendMail(req.body);
		res.status(200).send(html);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})

router.post('/sendMailToSupportForPaySecurelyBtn',async function(req,res){
	try{	
		var d = await paySecurelyBtnTemplate(req.body);
		var html = d.html;
		req.body.text = html;
		req.body.to = "support@examwarrior.com";
		mail.sendMail(req.body);
		res.status(200).send(html);
	}catch(err){
		console.error(err);
		res.status(500).send(err);
	}
})


module.exports = router;
