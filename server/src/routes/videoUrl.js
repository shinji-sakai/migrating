var express = require('express');
var router = express.Router();

var configPath = require('../configPath.js');
var videoUrlAPI = require(configPath.api.videoUrl);


router.post('/fetch', async function(req, res) {

    try {
        var source = req.body.videoSource || "harddrive";
        var name = req.body.videoUrl;
        var subtitle = req.body.subtitleUrl;
        var url = await videoUrlAPI[source](name);
        var subtitleUrl = "";
        if (subtitle) {
        	console.log("Subtitle.....",subtitle);
            subtitleUrl = await videoUrlAPI[source](subtitle);
        }
        res.status(200).send({
            url: url,
            subtitleUrl: subtitleUrl
        });
    } catch (err) {
        res.status(500).send({
            err: err
        });
    }
	  // videoUrl.getVideoSource(req.body)
    //     .then(function(data){
    //       var url = "";
    //       if(data.length > 0){
    //           url = videoUrl[data[0].videoSourceGroup](data[0].videoName);
    //       }
    //       res.status(200).send({url:url});
    //     })
    //     .catch(function(err){
    //         console.log("errr.....",err);
    //         res.status(500).json({err:err});
    //     });
});



module.exports = router;