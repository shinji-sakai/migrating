var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var UserCoursesApi = require(configPath.api.usercourses);
var passporAuth = require(configPath.lib.passport);

router.post('/getPurchasedCourses', function(req, res) {
    UserCoursesApi.getPurchasedCourses(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

// updatePurchasedCourses
router.post('/updatePurchasedCourses', function(req, res) {
    UserCoursesApi.updatePurchasedCourses(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});
// addPurchasedCourse
router.post('/addPurchasedCourse', function(req, res) {
    UserCoursesApi.addPurchasedCourse(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

// savePurchasedCourse
router.post('/savePurchasedCourse', function(req, res) {
    UserCoursesApi.savePurchasedCourse(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/saveCoursesBasedOnCourseId',passporAuth.authenticate(), function(req, res) {
    req.body.src_usr = req.user.usr_id
    UserCoursesApi.saveCoursesBasedOnCourseId(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/saveCoursesBasedOnBundleId',passporAuth.authenticate(), function(req, res) {
    req.body.src_usr = req.user.usr_id
    UserCoursesApi.saveCoursesBasedOnBundleId(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


router.post('/saveCoursesBasedOnTrainingId',passporAuth.authenticate(), function(req, res) {
    req.body.src_usr = req.user.usr_id
    UserCoursesApi.saveCoursesBasedOnTrainingId(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});




// getPurchasedCourseOfUser
router.post('/getPurchasedCourseOfUser', function(req, res) {
    UserCoursesApi.getPurchasedCourseOfUser(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

//getPurchasedCourseDetails
router.post('/getPurchasedCourseDetails', function(req, res) {
    UserCoursesApi.getPurchasedCourseDetails(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            res.status(500).json({
                err: err.message
            });
        })
});


module.exports = router;