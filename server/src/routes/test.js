

var express = require('express');
// Initialize the Router
var router = express.Router();
var fs = require('fs');
var sanitizeHtml = require('sanitize-html');
var configPath = require('../configPath.js');
var cassExecute = require(configPath.lib.utility).cassExecute;

router.get('/getHtml', function (req, res) {

    fs.readFile('data/test.html','utf8', function(err,data){

        if(err) res.status(500).send({err:err});

        data=data.replace(/\"/g, '\'');
        res.status(200).send({d:data});

    });


});

router.post('/setHtml', function (req, res) {

    console.log(req.body);

    var data = sanitizeHtml(req.body.d,{
          allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
        });
    console.log(data);
    fs.writeFile('data/test.html',data,'utf8',function(err){
        if(err) {res.status(500).send({err:err}); return;}
        res.status(200).send(data);
    })

});

router.post('/insertData', async (req, res) => {
    try{
        var data = req.body;
        if(!data.c1){
            var obj =  {
                status : "error",
                message : "c1 field is required"
            }
            res.status(200).send(obj);
        }
        if(!data.c2){
            var obj = {
                status : "error",
                message : "c2 field is required"
            }
            res.status(200).send(obj);
        }
        var qry = "insert into ew1.dummy (c1,c2) values (?,?)";
        var params = [data.c1,data.c2];
        await cassExecute(qry,params);
        var obj =  {
            status : "success",
            message : "Data is inserted"
        }
        res.status(200).send(obj);
    }catch(err){
        res.status(500).send({
            status  : "error",
            message : err.toString()
        });
    }
});

// Expose the module
module.exports = router;