
var express = require('express');
// Initialize the Router
var router = express.Router();

var configPath = require('../../configPath.js');
var VideoNoteApi = require(configPath.api.video.videonote);


router.post('/updateModuleNotes',function (req, res) {
    console.log(req.body);

      VideoNoteApi.updateModuleNotes(req.body)
        .then(function(doc){

            res.status(200).send(doc);

        }).catch(function(err){
        console.log(err);
        res.status(401).json({err:err.message});
    })
});

router.post('/getNotesModuleNames',function (req, res) {
    console.log(req.body);

     VideoNoteApi.getNotesModuleNames(req.body)
        .then(function(doc){

            res.status(200).send(doc);

        }).catch(function(err){
        console.log(err);
        res.status(401).json({err:err.message});
    })
});

router.post('/getModuleNotes',function (req, res) {
    console.log(req.body);

    VideoNoteApi.getModuleNotes(req.body)
        .then(function(doc){

            res.status(200).send(doc);

        }).catch(function(err){
        console.log(err);
        res.status(401).json({err:err.message});
    })
});

// getVideoNotesModules
router.post('/getVideoNotesModules',function (req, res) {
    VideoNoteApi.getVideoNotesModules(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});

// getVideoNotesForModules
router.post('/getVideoNotesForModules',function (req, res) {
    VideoNoteApi.getVideoNotesForModules(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});

// deleteVideoNote
router.post('/deleteVideoNote',function (req, res) {
    VideoNoteApi.deleteVideoNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});


// updateVideoNote
router.post('/updateVideoNote',function (req, res) {
    VideoNoteApi.updateVideoNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});

// insertVideoNotes
router.post('/insertVideoNotes',function (req, res) {
    VideoNoteApi.insertVideoNotes(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});

// getVideoNotesOfUser
router.post('/getVideoNotesOfUser',function (req, res) {
    VideoNoteApi.getVideoNotesOfUser(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            console.log(err);
            res.status(500).json({err:err.message});
        })
});
module.exports=router;