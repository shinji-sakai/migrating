var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.video.videovotes);


router.post('/upvoteVideo', function(req, res) {
    api.upvoteVideo(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/downvoteVideo', function(req, res) {
    api.downvoteVideo(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/getVideoVotes', function(req, res) {
    api.getVideoVotes(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});



module.exports = router;