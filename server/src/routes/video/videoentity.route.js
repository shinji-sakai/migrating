var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var config = require('../../config.js');
var VideoEntityApi = require(configPath.api.video.videoentity);
var videoSourceConfig = require(configPath.config.videoSourceConfig);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var path = require('path');
var utility = require(configPath.lib.utility);
var logger = require(configPath.lib.log_to_file);
var Promise = require("bluebird");
var thumbgen = Promise.promisify(require('thumbnails-webvtt'));
var awsS3API = require(configPath.api.aws.s3);
var tinify = require("tinify");
tinify.key = config.TINYPNG_API_KEY;
let exec = Promise.promisify(require('child_process').exec);

router.post('/uploadVideoEntity', multipartyMiddleware, async function(req, res) {
    try {
        if (!req.files.file) {
            res.status(500).json({
                err: 'File is required'
            });
            return;
        }
        var destTarget = req.body.target || "harddrive";
        var file = (req.files && req.files.file) || {};
        var parsedFileObj = path.parse(file.path);
        var targetFileName = path.parse(file.originalFilename).name + parsedFileObj.ext;
        if (req.body.fileName) {
            targetFileName = req.body.fileName + parsedFileObj.ext;
        }
        var targetPath = path.normalize(videoSourceConfig.videoRelativePath + "/" + targetFileName);
        if(destTarget === "harddrive"){
            await utility.copyFile(file.path, targetPath);
        }else{
            await awsS3API.uploadFile(req.body.type,targetFileName,file.path,req.body.folderPath);
        }
        
        var videoThumbsAbsolutePath = path.normalize(videoSourceConfig.videoThumbnailAbsolutePath + "/" + targetFileName + ".vtt");
        var videoThumbsRelativePath = videoSourceConfig.videoThumbnailRelativePath + "/" + targetFileName + ".vtt";
        var duration = "";
        if(req.body.generateThumbs){
            try {

                var videoDurationOp = await exec(`ffprobe -i ${file.path} -show_format -v quiet | grep duration`);
                if(videoDurationOp && videoDurationOp[0]){
                    let d = Math.floor(parseFloat(videoDurationOp[0].split("=")[1] || 0));
                    logger.debug(d);
                    duration = utility.changeTimeFormat(d);
                }
                

                await thumbgen(file.path, {
                    output: path.normalize(videoThumbsRelativePath),
                    size: {
                        width: 200,
                        height : 100
                    },
                    assetsDirectory : targetFileName,
                    secondsPerThumbnail: 1,
                    spritesheet: true
                });
                var thumbPath = path.normalize(videoSourceConfig.videoThumbnailRelativePath +  "/" + targetFileName + "/thumbnails.png");
                var source = tinify.fromFile(thumbPath);
                source.toFile(thumbPath);
            } catch (err) {
                logger.debug("Error in generating thumbnail of video...", err);
            }
        }

        res.status(200).send({
            Key: targetFileName,
            path: targetPath,
            thumbnailPath : (req.body.generateThumbs) ? videoThumbsAbsolutePath : undefined,
            duration : (req.body.generateThumbs) ? duration : undefined,
        });
    } catch (err) {
        res.status(500).json({
            err: err.message
        });
    }
})


router.post('/update', function(req, res) {
    VideoEntityApi.updateVideoEntity(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(401).json({
                err: err.message
            });
        })
});

router.post('/remove', function(req, res) {
    VideoEntityApi.removeVideoEntity(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(401).json({
                err: err.message
            });
        })
});

router.post('/getAll', function(req, res) {
    VideoEntityApi.getAllVideoEntity(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(401).json({
                err: err.message
            });
        })
});


router.post('/isVideoFilenameExist', function(req, res) {
    VideoEntityApi.isVideoFilenameExist(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(401).json({
                err: err.message
            });
        })
});

router.post('/findVideoEntityByQuery', function(req, res) {
    try {
        VideoEntityApi.findVideoEntityByQuery(req.body)
            .then(function(doc) {
                res.status(200).send(doc);
            }).catch(function(err) {
                console.log(err);
                res.status(401).json({
                    err: err.message
                });
            })
    } catch (err) {
        console.log(err)
    }

});

router.post('/findVideoEntityById', function(req, res) {
    VideoEntityApi.findVideoEntityById(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(401).json({
                err: err.message
            });
        })
});

router.post('/findVideoEntitiesById', function(req, res) {
    VideoEntityApi.findVideoEntitiesById(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(401).json({
                err: err.message
            });
        })
});

// getRelatedVideosByVideoId
router.post('/getRelatedVideosByVideoId', function(req, res) {
    VideoEntityApi.getRelatedVideosByVideoId(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            res.status(500).json({
                err: err.message
            });
        })
});

// getRelatedTopicsVideoByVideoId
router.post('/getRelatedTopicsVideoByVideoId', function(req, res) {
    VideoEntityApi.getRelatedTopicsVideoByVideoId(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            res.status(500).json({
                err: err.message
            });
        })
});

// getRelatedCoursesByVideoId
router.post('/getRelatedCoursesByVideoId', function(req, res) {
    VideoEntityApi.getRelatedCoursesByVideoId(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            res.status(500).json({
                err: err.message
            });
        })
});

// updateVideoLastViewTime
router.post('/updateVideoLastViewTime', function(req, res) {
    VideoEntityApi.updateVideoLastViewTime(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            res.status(500).json({
                err: err.message
            });
        })
})

// getVideoLastViewTime
router.post('/getVideoLastViewTime', function(req, res) {
    VideoEntityApi.getVideoLastViewTime(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            res.status(500).json({
                err: err.message
            });
        })
})
module.exports = router;