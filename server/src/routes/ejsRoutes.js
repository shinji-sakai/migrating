var express = require('express');

// Initialize the Router
var router = express.Router();

var configPath = require('../configPath.js');
var mongoapi = require(configPath.routes.mongoapi);
var authorizeitems = require(configPath.api.authorizeitems);

var getConnection = require(configPath.dbconn.mongoconn);
var globalFunctions = require(configPath.api.globalFunctions);


router.get('/webrtc', function(req, res) {
	res.render('pages/webrtc');
});

router.get('/', function(req, res) {
	res.render('pages/home');
});

// router.get('/home-new', function(req, res) {
// 	res.render('pages/home_res');
// });

router.get('/404', function(req, res) {
	res.render('pages/404');
});

router.get('/home_old', function(req, res) {
	res.render('pages/home');
});
router.get('/home_1', function(req, res) {
	res.render('pages/home_res_1');
});

router.get('/test-redis', function(req, res) {
	res.render('pages/redis-test');
});


router.get('/email-buy-training', function(req, res) {
	res.render('email-templates/buy-training/html',{dsp_nm : 'sarju',training : "informatica"});
});
router.get('/email-enrolled-batch', function(req, res) {
	res.render('email-templates/enrolled-batch/html',{data : {dsp_nm : 'sarju',training : "informatica"}});
});


router.get('/pricing', function(req, res) {
	res.render('pages/pricing');
});
router.get('/about-*-training-old', function(req, res) {
	res.render('pages/intro',{type : req.params['0'] || ''});
});
router.get('/about-it-training', function(req, res) {
	res.render('pages/intro_new',{type : req.params['0'] || ''});
});

router.get('/about-board-exams', function(req, res) {
	res.render('pages/about-board-exams');
});
router.get('/about-competitive-exams', function(req, res) {
	res.render('pages/about-competitive-exams');
});

router.get('/1', function(req, res) {
	res.render('pages/testsocket');
});
router.get('/2', function(req, res) {
	res.render('pages/testsocket_2');
});
// router.get('/whiteboard', function(req, res) {
// 	res.render('pages/whiteboard_new');
// });
router.get('/charts', function(req, res) {
	res.render('pages/chartsdemo');
});

router.get('/ccav', function(req, res) {
	res.render('pages/ccavform');
});

router.get('/contactus', function(req, res) {
	getConnection()
			.then(function(db) {
				return db.collection("contactus");
			})
			.then(function(col) {
				return col.findOne({});
			})
			.then(function(data) {
				console.log('........................',data);
				res.render('pages/contactus-new',{data : data || {}});
			})
			.catch(globalFunctions.err);
});

router.get('/contactus-old', function(req, res) {
	getConnection()
			.then(function(db) {
				return db.collection("contactus");
			})
			.then(function(col) {
				return col.findOne({});
			})
			.then(function(data) {
				res.render('pages/contactus',{data : data || {}});
			})
			.catch(globalFunctions.err);
});

router.get('/contactus_new', function(req, res) {
	console.log("Contact Us Page")
	res.render('pages/contactus_new');
});

router.get('/courselist-1', function(req, res) {
	new mongoapi().getFullDoc('courselist')
        .then(function(doc){
        	res.render('pages/courselist-1',{data:doc});
        }).catch(function(err){
        console.log(err);
        res.status(401).json({err:err.message});
    })
});
router.get('/courselist-2', function(req, res) {
	new mongoapi().getFullDoc('courselist')
        .then(function(doc){
        	res.render('pages/courselist-2',{data:doc});
        }).catch(function(err){
        console.log(err);
        res.status(401).json({err:err.message});
    })
});

router.get('/course/:id', function(req, res) {
	var courseData;
	// var authorizeitems;
	var ConfigItemTypes = {
		itemTypes :{
			"demo" : {
				color : "#87D37C",
				authenticate : false,
				requirePurchase : false,
			},
			"demologin" : {
				color : "#4183D7",
				authenticate : true,
				requirePurchase : false,
			},
			"paid" : {
				color : "#D24D57",
				authenticate : true,
				requirePurchase : true,
			}
		}
	}
	new mongoapi().getCourseVideos({courseId:req.params.id})
				.then(function(data){
					courseData = data[0];
					return getAuthorizeItems();
				})
				.then(function(authorizeitems){
					var itemIds = authorizeitems.map(function(v){return v.itemId});
					var itemTypes = authorizeitems.map(function(v){return v.itemType});
					var authorizedItemsMap = {};
					authorizeitems = authorizeitems.map(function(v){
	                    if(ConfigItemTypes.itemTypes[v.itemType].authenticate){
	                        authorizedItemsMap[v.itemId] = v.itemType;
	                    }
	                    return v;
	                });

					console.log("hhhhhhhh" , authorizedItemsMap);
					res.render('pages/course',{data:courseData,itemIds:itemIds,itemTypes:itemTypes,authorizedItemsMap:authorizedItemsMap,Config:ConfigItemTypes});
				})
				.catch(function(err){
					console.error(err);
					res.status(401).json({err:err.message});
				})

	function getAuthorizeItems(){
		return authorizeitems.getAuthorizeItems({courseId:req.params.id});
	}
});

module.exports = router;
