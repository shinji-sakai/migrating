var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.pricelist.pricelist);
var debug = require('debug')("app:api:pricelist.route.js");


router.post('/getAll', function (req, res) {
    api.getAll(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/add', function (req, res) {
    api.add(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/delete',function (req, res) {
    api.delete(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
// getDetailedPricelist
router.post('/getDetailedPricelist',function (req, res) {
    api.getDetailedPricelist(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
// getPricelistByCourseId
router.post('/getPricelistByCourseId',function (req, res) {
    api.getPricelistByCourseId(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});

// getDetailedPricelistByCourseId
router.post('/getDetailedPricelistByCourseId',async function (req, res) {
    try{
        console.log("getDetailedPricelistByCourseId");
        var api_res = await api.getDetailedPricelistByCourseId(req.body);   
        res.status(200).send(api_res);
    }catch(err){
        debug(err);
        res.status(500).json({err:err.message});
    }
});

module.exports = router;