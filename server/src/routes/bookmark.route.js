
var express = require('express');
// Initialize the Router
var router = express.Router();

var configPath = require('../configPath.js');
var bookmarkApi = require(configPath.api.bookmark);



router.post('/addBookmark', function (req, res) {
    bookmarkApi.addBookmark(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
        console.error(err);
        res.status(500).json({err:err.message});
    })
});

// bookmarkMultipleItems
router.post('/bookmarkMultipleItems', function (req, res) {
    bookmarkApi.bookmarkMultipleItems(req.body)
        .then(function(doc) {
             res.status(200).send(doc);
        }).catch(function(err){
        console.error(err);
        res.status(500).json({err:err.message});
    })
});

router.post('/getBookmarks', function (req, res) {
    bookmarkApi.getBookmarks(req.body)
        .then(function(doc) {
             res.status(200).send(doc);
        }).catch(function(err){
        console.error(err);
        res.status(500).json({err:err.message});
    })
});


router.post('/getUserBookmarks', function (req, res) {
    bookmarkApi.getUserBookmarks(req.body)
        .then(function(doc) {
             res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        })
});


router.post('/deleteBookmarks', function (req, res) {
    bookmarkApi.deleteBookmarks(req.body)
        .then(function(doc) {
             res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        })
});


module.exports = router;
