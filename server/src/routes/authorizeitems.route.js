var express = require('express');
var router = express.Router();

var configPath = require('../configPath.js');
var authorizeitems = require(configPath.api.authorizeitems);

router.post('/getItems', function (req, res) {
    authorizeitems.getAuthorizeItems(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(401).json({err:err.message});
        })
});

router.post('/updateItems', function (req, res) {
    authorizeitems.updateItemType(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
        console.error(err);
        res.status(401).json({err:err.message});
    })
});

router.post('/addItem', function (req, res) {
    authorizeitems.addItem(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
        console.error(err);
        res.status(401).json({err:err.message});
    })
});
// getVideoItemFullInfo
router.post('/getVideoItemFullInfo', function (req, res) {
    authorizeitems.getVideoItemFullInfo(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
        console.error(err);
        res.status(401).json({err:err.message});
    })
});


// getVideoPermission
router.post('/getVideoPermission', function (req, res) {
    authorizeitems.getVideoPermission(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
        console.error(err);
        res.status(401).json({err:err.message});
    })
});

module.exports = router;