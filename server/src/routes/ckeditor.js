var express = require('express');
var router = express.Router();
var path = require('path');

var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');

var configPath = require('../configPath.js');
var utility = require(configPath.lib.utility);

router.post('/save',multipartyMiddleware,function(req,res){
	var file = req.files.upload;
    var dotPos = file.originalFilename.lastIndexOf(".");
    var onlyName = file.originalFilename.substr(0, dotPos);
    var ext = file.originalFilename.substr(dotPos + 1);
    var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
    // __dirname, "./../../client/build/upload/postattachments/"
    var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
    var promise = utility.copyFile(file.path, targetPath + '/' + targetFileName);
    var uploadPath = configPath.common_paths.postattachment_absolute + targetFileName;
    promise
    	.then(function(d){
			res.status(200).send({link:uploadPath});
    	})
    	.catch(function(err){
    		console.log(err);
    		res.status(500).send({
                'err': err
            });
    	})

})

router.post('/uploadTextboxIOImage',multipartyMiddleware,function(req,res){
    var file = req.files.image;
    var dotPos = file.originalFilename.lastIndexOf(".");
    var onlyName = file.originalFilename.substr(0, dotPos);
    var ext = file.originalFilename.substr(dotPos + 1);
    var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
    var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
    var promise = utility.copyFile(file.path, targetPath + '/' + targetFileName);
    var uploadPath = configPath.common_paths.postattachment_absolute + targetFileName;
    promise
        .then(function(d){
            res.status(200).send(uploadPath);
        })
        .catch(function(err){
            console.log(err);
            res.status(500).send({
                'err': err
            });
        })

})

module.exports = router;