var express = require('express');
var router = express.Router();


var configPath = require('../../configPath.js');
var practiceApi = require(configPath.api.qa.practice);
var practiceCassApi = require(configPath.api.qa.practiceCass);

router.post('/saveQuestion',function(req,res){
	practiceApi.saveQuestion(req.body)
		.then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/savePreviewQuestion',function(req,res){
    practiceApi.savePreviewQuestion(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/saveQuestionStats',function(req,res){
    practiceCassApi.saveQuestionStats(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/findQuestionsByQuery',function(req,res){
    // console.log(req.body);
    practiceApi.findQuestionsByQuery(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});
router.post('/findQuestionsTextByQuery',function(req,res){
    practiceApi.findQuestionsTextByQuery(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


// find questions id and name by query
router.post('/findQuestionsIdByQuery',function(req,res){
    // console.log(req.body);
    practiceApi.findQuestionsIdByQuery(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


router.post('/findPreviewQuestionsByQuery',function(req,res){
    // console.log(req.body);
    practiceApi.findPreviewQuestionsByQuery(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});	

router.post('/removeQuestion',function(req,res){
    practiceApi.removeQuestion(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
}); 

router.post('/getAllQuestions',function(req,res){
	practiceApi.getAllQuestions(req.body)
		.then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});
//
router.post('/getPracticeQuestionsById',function(req,res){
    practiceApi.getPracticeQuestionsById(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


router.post('/getPracticeQuestionsByLimit',function(req,res){
    practiceApi.getPracticeQuestionsByLimit(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});
// 

// updateMultiplePracticeQuestions
router.post('/updateMultiplePracticeQuestions',function(req,res){
    practiceApi.updateMultiplePracticeQuestions(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

// saveTestQuestionStats
router.post('/saveTestQuestionStats',function(req,res){
    practiceCassApi.saveTestQuestionStats(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


// saveTestReviews
router.post('/saveTestReviews',function(req,res){
    practiceCassApi.saveTestReviews(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});



router.post('/getAllAvgQuestionPerfStats',function(req,res){
    practiceCassApi.getAllAvgQuestionPerfStats(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


router.post('/getAllUsersQuestionPerfStats',function(req,res){
    practiceCassApi.getAllUsersQuestionPerfStats(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


router.post('/getAllMonthAvgQuestionPerfStats',function(req,res){
    practiceCassApi.getAllMonthAvgQuestionPerfStats(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});


router.post('/getAllUsersMonthQuestionPerfStats',function(req,res){
    practiceCassApi.getAllUsersMonthQuestionPerfStats(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/updateVerifyPracticeQuestion',function(req,res){
    practiceApi.updateVerifyPracticeQuestion(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/getAllVerifyStatusOfPracticeQuestion',function(req,res){
    practiceApi.getAllVerifyStatusOfPracticeQuestion(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/getLatestVerifyStatusOfGivenPracticeQuestions',function(req,res){
    practiceApi.getLatestVerifyStatusOfGivenPracticeQuestions(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/getUserMonthQuestionAttemptStats',function(req,res){
    practiceCassApi.getUserMonthQuestionAttemptStats(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/startUserExam',function(req,res){
    practiceApi.startUserExam(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});
router.post('/finishUserExam',function(req,res){
    practiceApi.finishUserExam(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});
router.post('/checkForPreviousExam',function(req,res){
    practiceApi.checkForPreviousExam(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});



router.post('/saveUserExamQuestionData',function(req,res){
    practiceApi.saveUserExamQuestionData(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/getUserExamQuestion',function(req,res){
    practiceApi.getUserExamQuestion(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/startUserPractice',function(req,res){
    practiceApi.startUserPractice(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/saveUserPracticeQuestionData',function(req,res){
    practiceApi.saveUserPracticeQuestionData(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/fetchNextUserPracticeQuestion',function(req,res){
    practiceApi.fetchNextUserPracticeQuestion(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/getPracticeNodeQuestions',function(req,res){
    practiceApi.getPracticeNodeQuestions(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

module.exports = router;