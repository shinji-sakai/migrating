var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var createtestApi = require(configPath.api.qa.createtest);

router.post('/save',function(req,res){
	createtestApi.saveTest(req.body)
		.then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});

router.post('/getAll',function(req,res){
	createtestApi.getTests(req.body)
		.then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
});	

router.post('/getById',function(req,res){
    createtestApi.getTestById(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({
                err: err.message
            });
        })
}); 

module.exports = router;