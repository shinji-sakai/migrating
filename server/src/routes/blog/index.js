var blogNotes = require('./blogNotes.route.js');

module.exports = function(app){
	app.use('/blogApi/blogNotes',blogNotes);
}