
var express = require('express');
// Initialize the Router
var router = express.Router();

var configPath = require('../../configPath.js');
var BlogNoteApi = require(configPath.api.blog.blogNotes);

router.post('/deleteBlogNote',function (req, res) {
    BlogNoteApi.deleteBlogNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});

router.post('/updateBlogNote',function (req, res) {
    BlogNoteApi.updateBlogNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});

router.post('/insertBlogNote',function (req, res) {
    BlogNoteApi.insertBlogNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
        console.log(err);
        res.status(500).json({err:err.message});
    })
});

module.exports=router;