var express = require('express');
var router = express.Router();
var Q = require('q');

var configPath = require('../../configPath.js');
var AuthApi = require(configPath.api.auth.authCass);
var getRedisClient = require(configPath.dbconn.redisconn);
var redisClient = getRedisClient("sessions");
let lib_passport_auth = require(configPath.lib.passport).authenticate; 


router.post('/login', function(req, res,next) {
    AuthApi.validateLoginDetails(req.body)
        .then(AuthApi.generateJWT)
        .then(async function(doc) {
            if(!doc.err){
                saveSessionInfo(req,doc)
            }
            res.status(200).send(doc);
        }).catch(function(err) {
           return next(err);
        })
});

router.post('/test',lib_passport_auth(), function(req, res,next) {
    res.status(200).send(req.user);
});

router.post('/logout', async function(req, res,next) {

    try{
        await redisClient.delAsync("session-" + req.body.usr_id);
        res.status(200).send({});
    }catch(err){
       return next(err);
    }
});

router.post('/resendOTP', function(req, res,next) {
    AuthApi.resendOTP(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
           return next(err);
        })
});

router.post('/checkUserExistance', function(req, res,next) {
    AuthApi.checkUserExistance(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});

router.post('/register', function(req, res,next) {
    AuthApi.registerUser(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});

router.post('/verifyOTP', function(req, res,next) {
    console.log(req.body);
    AuthApi.verifyOTP(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});

router.post('/removeUser', function(req, res,next) {
    AuthApi.removeUser(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});

router.post('/checkPassword', function(req, res,next) {
    AuthApi.checkPassword(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});

router.post('/changePassword', function(req, res,next) {
    AuthApi.changePassword(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});

router.post('/forgotPassword', function(req, res,next) {
    AuthApi.forgotPassword(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});
router.post('/resendForgotPasswordOTP', function(req, res,next) {
    AuthApi.resendForgotPasswordOTP(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
            return next(err);
        })
});
router.post('/verifyForgotPasswordOTP', function(req, res,next) {
    AuthApi.verifyForgotPasswordOTP(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err) {
           return next(err);
        })
});
// 


router.post('/facebook', function(req, res,next) {
        var user_profile;
        AuthApi.facebook(req.body)
            .then(function (profile) {
                profile.id = profile.id + "-fb";
                user_profile = profile;
                var data = {
                    usr_id : profile.id
                }
                console.log("fb profile........",user_profile);
                // return AuthApi.checkUserExistance(data);
                return data;
            })
            .then(function(res){
                var user = {
                    usr_id : user_profile.id,
                    dsp_nm : user_profile.name,
                    usr_email : user_profile.email,
                    usr_ph : '',
                    usr_role : ''
                }
                return AuthApi.updateSocialMediaDetails(user);
            })
            .then(function(){
                var data = {};
                data.user = {
                    usr_id : user_profile.id,
                    dsp_nm : user_profile.name,
                    usr_email : user_profile.email,
                    pwd : ""
                }
                return AuthApi.generateJWT(data);
            })
            .then(function(d) {
                if(d.token){
                    saveSessionInfo(req,d);
                    res.status(200).send(d);
                }
                else{
                    res.status(200).send({});
                }
            })
            .catch(function(err) {
                return next(err);
            })
});

router.post('/google', function(req, res, next) {
        var user_profile;
        AuthApi.google(req.body)
            .then(function (profile) {
                user_profile = profile;
                var data = {
                    usr_id : profile.email
                }
                console.log("google profile........",user_profile);
                // return AuthApi.checkUserExistance(data);
                return data;
            })
            .then(function(res){
                var user = {
                    usr_id : user_profile.email,
                    dsp_nm : user_profile.name,
                    usr_email : user_profile.email,
                    usr_ph : '',
                    usr_role : ''
                }
                return AuthApi.updateSocialMediaDetails(user);
            })
            .then(function(){
                var data = {};
                data.user = {
                    usr_id : user_profile.email,
                    dsp_nm : user_profile.name,
                    usr_pic : user_profile.picture,
                    usr_email : user_profile.email,
                    pwd : ""
                }
                return AuthApi.generatePassportJWT(data);
            })
           .then(async function(doc) {
            res.status(200).send(doc); 
            }).catch(function(err) {
                return next(err);
            })
});

router.post('/twitter', function(req, res,next) {
        var user_profile;
        AuthApi.twitter(req.body)
            .then(function (token) {
                if(token.oauth_token || token.oauth_verifier){
                    res.status(200).send(token);  
                    return;  
                }
                console.log("res.................",token);
                token.id_str = token.id_str + "-twitter";
                user_profile = token;
                var data = {
                    usr_id : user_profile.id_str
                }
                return data;
            })
            .then(function(res){
                var user = {
                    usr_id : user_profile.id_str,
                    dsp_nm : user_profile.name,
                    usr_email : user_profile.email,
                    usr_ph : '',
                    usr_role : ''
                }
                return AuthApi.updateSocialMediaDetails(user);
            })
            .then(function() {
                var data = {};
                data.user = {
                    usr_id : user_profile.id_str,
                    dsp_nm : user_profile.name,
                    pwd : ""
                }
                return AuthApi.generateJWT(data);
            })
            .then(function(d) {
                if(d.token){
                    saveSessionInfo(req,d);
                    res.status(200).send(d);
                }
                else{
                    res.status(200).send({});
                }
            })
            .catch(function(err) {
                return next(err);
            })
});

router.post('/linkedin', function(req, res,next) {
        var user_profile;
        AuthApi.linkedin(req.body)
            .then(function (profile) {
                profile.id = profile.id + "-linkedin"
                user_profile = profile;
                var data = {
                    usr_id : profile.id
                }
                console.log("linkedin profile........",user_profile);
                return data;
                
                // return AuthApi.checkUserExistance(data);
            })
            .then(function(res){
                var user = {
                    usr_id : '' + user_profile.id,
                    dsp_nm : user_profile.firstName + " " + user_profile.lastName,
                    usr_email : user_profile.emailAddress,
                    usr_ph : '',
                    usr_role : ''
                }
                return AuthApi.updateSocialMediaDetails(user);
            })
            .then(function() {
                var data = {};
                data.user = {
                    usr_id : user_profile.id,
                    dsp_nm : user_profile.firstName + " " + user_profile.lastName,
                    usr_pic : user_profile.pictureUrl,
                    usr_email : user_profile.emailAddress,
                    pwd : ""
                }
                return AuthApi.generateJWT(data);
            })
            .then(function(d) {
                if(d.token){
                    saveSessionInfo(req,d);
                    res.status(200).send(d);
                }
                else{
                    res.status(200).send({});
                }
            })
            .catch(function(err) {
                return next(err);
            })
});

async function saveSessionInfo(req,doc) {
    
    try{
        var loginData = {
            token : doc.token,
            usr_id : doc.user.usr_id
        };

        await redisClient.setAsync(doc.user.usr_id,JSON.stringify(doc.user));
        await redisClient.setAsync("session-" + doc.user.usr_id,doc.token);
        // let sessionId = await redisClient.getAsync(doc.user.usr_id + "-session");
        // if(sessionId != req.session.id){
        //     await redisClient.delAsync("sess:"+sessionId);
        // }
        // await redisClient.setAsync(doc.user.usr_id + "-session",req.session.id)
    }catch(err){
        console.log("auth.route.js saveSessionInfo err...",err)
    }
    
}

        
module.exports = router;