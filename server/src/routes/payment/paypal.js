var express = require('express');
var router = express.Router();
var path  = require('path');
var Q = require('q');

var configPath = require('../../configPath.js');
var PaypalPaymentAPI = require(configPath.api.payment.paypal);
var utility = require(configPath.lib.utility);

router.post('/create', function(req, res) {
    console.log("create paypal payment");
    PaypalPaymentAPI.createPayment(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                console.error(err);
                res.status(500).send(err);
            });
});

router.post('/:id/execute', function(req, res) {
    PaypalPaymentAPI.executePayment(req.params.id,req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                console.error(err);
                res.status(500).send(err);
            });
});

module.exports = router;