var http = require('http'),
    fs = require('fs'),
    qs = require('querystring');
var configPath = require("../../configPath.js");
var ccav = require(configPath.lib.ccavutil);
var do_req = require('request');
var express = require('express');
var router = express.Router();
var path = require('path');
var Q = require('q');
var shortid = require('shortid');
var ccavConfig = require(configPath.config.ccavConfig);
var profileAPI = require(configPath.api.dashboard.profile);
var smsAPI = require(configPath.api.sms.sms);
var debug = require('debug')("app:routes:payment:ccav");
var ccavAPI = require(configPath.api.payment.ccavenue);
var userCoursesAPI = require(configPath.api.usercourses);
var mailAPI = require(configPath.api.mail);
var logger = require(configPath.lib.log_to_file);
var paymentAPI = require(configPath.api.payment.payment);

router.post('/ccavRequestHandler', async function(request, response) {
    var body = '',
        workingKey = ccavConfig.WORKING_KEY, //'58B1F90DAE0277FA78545BB481941117', //Put in the 32-Bit key shared by CCAvenues.
        accessCode = ccavConfig.ACCESS_CODE , //Put in the Access Code shared by CCAvenues.
        encRequest = '',
        formbody = '';

    var d = request.body;
    d["order_id"] = shortid.generate();
    d["merchant_id"] = ccavConfig.MERCHANT_ID;
    d["language"] = "EN";

    var usr_id = d["merchant_param4"];
    var training_id = d["merchant_param2"];
    var bat_id = d["merchant_param3"];
    var userDetails = await profileAPI.getUserShortDetails({usr_id : usr_id});
    //get user details
    if(userDetails.usr_ph){
        //if user has given mobile number then
        try{
            var smsres = await smsAPI.paymentStartedSMS(userDetails.usr_ph);
        }catch(err){
            logger.debug(err);
        }
    }   

    var request_str = "";

    var keys = Object.keys(request.body || {});
    keys.forEach((v, i) => {
        var key = v;
        var val = d[key];
        request_str = request_str + key + "=" + val + "&";
    })


    encRequest = ccav.encrypt(request_str, workingKey);
    console.log(request_str);
    formbody = '<form id="nonseamless" method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> <input type="hidden" id="encRequest" name="encRequest" value="' + encRequest + '"><input type="hidden" name="access_code" id="access_code" value="' + accessCode + '"><script language="javascript">document.redirect.submit();</script></form>';
    response.writeHeader(200, {
        "Content-Type": "text/html"
    });
    response.write(formbody);
    response.end();
    return;
});

router.post('/ccavResponseHandler', async function(request, response) {
    var ccavEncResponse = '',
        ccavResponse = '',
        workingKey = ccavConfig.WORKING_KEY,//Put in the 32-Bit key shared by CCAvenues.
        ccavPOST = '';

    try{
        ccavResponse = ccav.decrypt(request.body.encResp, workingKey);

        var ccavJSONResponse = qs.parse(ccavResponse);

        logger.debug(ccavJSONResponse);
        //save payment response info
        await ccavAPI.savePaymentDetails(ccavJSONResponse);

        var usr_id = ccavJSONResponse["merchant_param4"];
        var training_id = ccavJSONResponse["merchant_param2"];
        var bat_id = ccavJSONResponse["merchant_param3"];
        var userShortDetails = await profileAPI.getUserShortDetails({usr_id : usr_id});

        if(ccavJSONResponse["order_status"] === "Success"){
            try{
                //save purchased courses
                var courses = await userCoursesAPI.savePurchasedCourse({usr_id : usr_id,training_id : training_id});
                //payment details store
                await paymentAPI.savePurchasedTrainingDetails({
                    usr_id : usr_id,
                    training_id : training_id,
                    bat_id : bat_id,
                    pay_method : 'ccavenue',
                    pay_status : 'completed',
                    pay_id : ccavJSONResponse["tracking_id"]                                        
                })

                //user batch enroll details
                await paymentAPI.saveUserEnrolledBatchDetails({
                    usr_id : usr_id,
                    enroll_bat : bat_id,
                    enroll_dt : new Date(),
                    enroll_status : true                                     
                });



                //get user details
                if(userShortDetails.usr_ph){
                    //if user has given mobile number then
                    try{
                        var smsres = await smsAPI.sendSuccessfulBuySMS(userShortDetails.usr_ph,training_id,usr_id);
                    }catch(err){
                        logger.debug(err);
                    }
                }    

                if(userShortDetails.usr_email){
                    try{
                        var invoice_data = await paymentAPI.generateInvoiceDetails({
                            training_id : training_id,
                            bat_id : bat_id
                        })
                        invoice_data["to"] = userShortDetails.usr_email;
                        invoice_data["dsp_nm"] = userShortDetails.dsp_nm;
                        var mail_res = await mailAPI.sendSuccessfullBuyTrainingMail(invoice_data);                        
                        // var mail_res = await mailAPI.sendSuccessfullBuyTrainingMail({
                        //     to : userShortDetails.usr_email,
                        //     dsp_nm : userShortDetails.dsp_nm,
                        //     training : training_id
                        // })
                    }catch(err){
                        logger.debug(err);
                    }
                }

            }catch(err){
                debug(err);
            }

            var url = "https://" + ccavJSONResponse["merchant_param1"] + "?ccav_status=success";
            response.redirect(decodeURIComponent(url));

        }else{
            //get user details
            if(userShortDetails.usr_ph){
                //if user has given mobile number then
                try{
                    var smsres = await smsAPI.paymentFailureSMS(userShortDetails.usr_ph,training_id);
                }catch(err){
                    logger.debug(err);
                }
            }    

            // if(userShortDetails.usr_email){
            //     try{
            //         var mail_res = await mailAPI.sendSuccessfullBuyTrainingMail({
            //             to : userShortDetails.usr_email,
            //             dsp_nm : userShortDetails.dsp_nm,
            //             training : training_id
            //         })
            //     }catch(err){
            //         logger.debug(err);
            //     }
            // }

            var url = "https://" + ccavJSONResponse["merchant_param1"] + "?ccav_status=failure&msg=" + ccavJSONResponse["status_message"];
            response.redirect(decodeURIComponent(url));
        }
    }catch(err){
        logger.debug(err);
        throw err;
    }
    
});

router.post('/orderStatus', async function(request, response) {
    try{
        var command = request.body.command;
        var data  = request.body.data;
        var api_res = await ccavAPI.doAPICall(command,data);
        response.status(200).send(api_res); 
    }catch(err){
        response.status(500).send({err});
    }
});

router.post('/apiCall/:command', async function(request, response) {
    try{
        debug(request.params);
        var command = request.params["command"];
        var data  = request.body;
        var api_res = await ccavAPI.doAPICall(command,data);
        response.status(200).send(api_res); 
    }catch(err){
        response.status(500).send({err});
    }
});

module.exports = router;