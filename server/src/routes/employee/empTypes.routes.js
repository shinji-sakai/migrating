var express = require('express');
var router = express.Router();
var path  = require('path');
var Q = require('q');

var configPath = require('../../configPath.js');
var EmpTypesAPI = require(configPath.api.employee.empTypes);
var utility = require(configPath.lib.utility);

router.post('/add', function(req, res) {
    EmpTypesAPI.addType(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                console.error(err);
                res.status(500).send(err);
            });
});

router.post('/getAll', function(req, res) {
    EmpTypesAPI.getAll(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                console.error(err);
                res.status(500).send(err);
            });
});

router.post('/delete', function(req, res) {
    EmpTypesAPI.delete(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                console.error(err);
                res.status(500).send(err);
            });
});

module.exports = router;