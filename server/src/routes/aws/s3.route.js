var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.aws.s3);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({});
var debug = require('debug')("app:routes:aws:s3.route.js");
var path = require('path');
var uuid = require('uuid');

router.post('/createBucket', async function (req, res) {
    try{
        var apiRes = await api.createBucket(req.body);
        res.status(200).send(apiRes);
    }catch(err){
        res.status(500).json({err:err.message});
    }
});

router.get('/getBuckets', async function (req, res) {
    try{
        var apiRes = await api.getBuckets();
        res.status(200).send(apiRes);
    }catch(err){
        res.status(500).json({err:err.message});
    }
});

router.post('/uploadFile',multipartyMiddleware, async function (req, res) {
    try{
        if(!req.files.file){
            res.status(500).json({err:'File is required'});
            return;
        }
        var file  = (req.files && req.files['file']) || {};
        var parsedFileObj = path.parse(file.path);
        var targetFileName = path.parse(file.originalFilename).name + parsedFileObj.ext;
        if(req.body.fileName){
            targetFileName = req.body.fileName + parsedFileObj.ext;
        }
        var apiRes = await api.uploadFile(req.body.type,targetFileName,file.path,req.body.folderPath);
        res.status(200).send(apiRes);
    }catch(err){
        res.status(500).json({err:err.message});
    }
});

router.post('/getSignedUrl', async function (req, res) {
    try{
        var apiRes = await api.getSignedUrl(req.body.type,req.body.key);
        res.status(200).send(apiRes);
    }catch(err){
        res.status(500).json({err:err.message});
    }
});

module.exports = router;