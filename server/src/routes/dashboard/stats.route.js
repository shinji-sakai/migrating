var express = require('express');
var router = express.Router();
var path  = require('path');
var Q = require('q');
//var passporAuth = require('../../auth.js')();
var configPath = require('../../configPath.js');
var api = require(configPath.api.dashboard.stats);
var utility = require(configPath.lib.utility);

var passporAuth = require(configPath.lib.passport);

router.post('/getAllDashboardStatsOfUser', passporAuth.authenticate(), function(req, res, next) {
    console.log('session ----->   ',req.user);
    api.getAll(req.user)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

router.post('/incrementQuestionLearnedViews', passporAuth.authenticate(), function(req, res, next) {
    api.incrementQuestionLearnedViews(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

router.post('/incrementTestTakenCounter', passporAuth.authenticate(), function(req, res, next) {
    api.incrementTestTakenCounter(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
               return next(err);
            });
});

router.post('/incrementNoOfQuestionPracticedCounter', passporAuth.authenticate(),function(req, res, next) {
    api.incrementNoOfQuestionPracticedCounter(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

router.post('/incrementNoOfQuestionAttemptedInTestCounter', passporAuth.authenticate(),function(req, res, next) {
    api.incrementNoOfQuestionAttemptedInTestCounter(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

// 
router.post('/updateNoOfCorrectQuestionAttemptedCounter', passporAuth.authenticate(),function(req, res, next) {
    api.updateNoOfCorrectQuestionAttemptedCounter(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});


router.post('/updateNoOfWrongQuestionAttemptedCounter', passporAuth.authenticate(),function(req, res, next) {
    api.updateNoOfWrongQuestionAttemptedCounter(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
               return next(err);
            });
});
module.exports = router;