var express = require('express');
var router = express.Router();
var path  = require('path');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');
var configPath = require('../../configPath.js');
var PostApi = require(configPath.api.dashboard.post);
var utility = require(configPath.lib.utility);
var passporAuth = require(configPath.lib.passport);

router.post('/save',multipartyMiddleware, passporAuth.authenticate(),async function(req, res, next) {
	var arr = []; 
	var photoUrls = [];
	var videoUrls = [];
	var videoArr = [];
    for (var key in req.files.photoAttachments) {
        var file = req.files.photoAttachments[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0,dotPos);
        var ext = file.originalFilename.substr(dotPos+1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }

    for (var key in req.files.videoAttachments) {
        var file = req.files.videoAttachments[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0,dotPos);
        var ext = file.originalFilename.substr(dotPos+1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }


    var data = req.body;
    data.pst_img = photoUrls;
    data.pst_vid = videoUrls;

    // arr.push();
    arr.concat(videoArr);

    await Q.all(arr)

    PostApi.save(data)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/getFeed', passporAuth.authenticate(),function(req, res, next) {
    PostApi.getFeed(req.body)
			.then(function(d){
				res.status(200).send(d);
			})    	
			.catch(function(err){
				return next(err);
			});
});

router.post('/getPosts', passporAuth.authenticate(),function(req, res, next) {
    PostApi.getUserPosts(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

// 
router.post('/getUserPublicPosts', passporAuth.authenticate(), function(req, res, next) {
    PostApi.getUserPublicPosts(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

router.post('/getUserPostsForFriends', passporAuth.authenticate(),function(req, res,next) {
    PostApi.getUserPostsForFriends(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

router.post('/edit',multipartyMiddleware, passporAuth.authenticate(),async (req, res,next) => {
    
    var incomingData = req.body;

    var arr = []; 
    var photoUrls = incomingData.alreadyAddedPhotos || [];
    var videoUrls = incomingData.alreadyAddedVideos || [];
    var videoArr = [];

    for (var key in req.files.photoAttachments) {
        var file = req.files.photoAttachments[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0,dotPos);
        var ext = file.originalFilename.substr(dotPos+1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        photoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }

    for (var key in req.files.videoAttachments) {
        var file = req.files.videoAttachments[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0,dotPos);
        var ext = file.originalFilename.substr(dotPos+1);
        var targetFileName = onlyName + "_" + new Date().getTime() + "." + ext;
        var targetPath = path.resolve(configPath.common_paths.postattachment_relative);
        videoArr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
        videoUrls.push(configPath.common_paths.postattachment_absolute + targetFileName);
    }

    var data = req.body;
    data.pst_img = photoUrls;
    data.pst_vid = videoUrls;
    // arr.push();
    arr.concat(videoArr);
    await Q.all(arr)

    PostApi.edit(data)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            console.log(err);
            res.status(500).send({'err':err});
        });
});
router.post('/delete', passporAuth.authenticate(),function(req, res,next) {
    PostApi.delete(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
               return next(err);
            });
});


// getLikeByUser
router.post('/getLikeByUser',passporAuth.authenticate(), function(req, res,next) {
    PostApi.getLikeByUser(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
               return next(err);
            });
});

// updateLikeByUser
router.post('/updateLikeByUser', passporAuth.authenticate(),function(req, res,next) {
    PostApi.updateLikeByUser(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

// getLikeStatsForPosts
router.post('/getLikeStatsForPosts', passporAuth.authenticate(),function(req, res, next) {
    PostApi.getLikeStatsForPosts(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

// updateLikeStatsForPosts
router.post('/updateLikeStatsForPosts', passporAuth.authenticate(), function(req, res,next) {
    PostApi.updateLikeStatsForPosts(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

router.post('/saveComplain', passporAuth.authenticate(), function(req, res,next) {
    PostApi.saveComplain(req.body)
            .then(function(d){
                res.status(200).send(d);
            })      
            .catch(function(err){
                return next(err);
            });
});

module.exports = router;