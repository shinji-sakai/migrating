var express = require('express');
var router = express.Router();
var path  = require('path');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var Q = require('q');
var debug = require('debug')("app:route:dashboard:profile");

var configPath = require('../../configPath.js');
var utility = require(configPath.lib.utility);
var ProfileApi = require(configPath.api.dashboard.profile);
var passporAuth = require(configPath.lib.passport);

router.post('/saveProfilePic',multipartyMiddleware,  passporAuth.authenticate(), function(req, res, next) {
	var arr = [];

    //image file
    var file = req.files.pic;
    //find dot position for extension
    var dotPos = file.originalFilename.lastIndexOf(".");
    //only name without extension
    var onlyName = file.originalFilename.substr(0,dotPos);
    //extension
    var ext = file.originalFilename.substr(dotPos+1);
    //final image name with current time
    var targetFileName = onlyName + "." + ext;
    //final image target path
    var targetPath = path.resolve(configPath.common_paths.user_pic_relative);
    console.log("uploading....",file.path,targetPath + '/' + targetFileName)
    //promise of copying file
    var promise = utility.copyFile(file.path, targetPath + '/' + targetFileName);

    //absolute path
    var pic_path = configPath.common_paths.user_pic_absolute + targetFileName;

    arr.push(promise);

    var data = req.body;
    data.usr_pic = pic_path;
    arr.push(ProfileApi.saveProfilePic(data));

    Q.all(arr)
        .then(function(d){
            res.status(200).send(data);
        })
        .catch(function(err){
           return next(err);
        });
});

//getUserFriends
router.post('/getUserFriends',  passporAuth.authenticate(), function(req, res,next) {
    ProfileApi.getUserFriends(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});


router.post('/getUserFriendsAndFollowings',  passporAuth.authenticate(), function(req, res,next) {
    ProfileApi.getUserFriendsAndFollowings(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});


//get user short details using usr_id
router.post('/getUserShortDetails',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getUserShortDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
})

//get all users short details
router.post('/getAllUsersShortDetails',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getAllUsersShortDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
})

// get all recommended friend
router.post('/getRecommendFriendForUser',  passporAuth.authenticate(), function(req,res,next){
    ProfileApi.getRecommendFriendForUser(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
})

// sendFriendRequest
router.post('/sendFriendRequest',  passporAuth.authenticate(), function(req,res,next){
    ProfileApi.sendFriendRequest(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
})

// acceptFriendRequest
router.post('/acceptFriendRequest',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.acceptFriendRequest(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
})


// deleteFriendRequest
router.post('/deleteFriendRequest',  passporAuth.authenticate(), function(req,res,next){
    ProfileApi.deleteFriendRequest(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
})

// unfriendFriend
router.post('/unfriendFriend',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.unfriendFriend(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
})

router.post('/cancelFriendRequest', passporAuth.authenticate(), function(req,res,next){
    ProfileApi.cancelFriendRequest(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
})

router.post('/blockPerson',  passporAuth.authenticate(), function(req,res,next){
    ProfileApi.blockPerson(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
})

// getFriendRequest
router.post('/getFriendRequest', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getFriendRequest(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
})

// getFriendStatus
router.post('/getFriendStatus',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getFriendStatus(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

// addFollower
router.post('/addFollower', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.addFollower(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
         return next(err);
        });
});

// getFollowers
router.post('/getFollowers',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getFollowers(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});
// removeFollower
router.post('/removeFollower', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.removeFollower(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});


//getUserPersonalDetails
router.post('/getUserPersonalDetails', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getUserPersonalDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

//saveUserPersonalDetails
router.post('/saveUserBasicPersonalDetails', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveUserBasicPersonalDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});
// saveUserPhoneDetails
router.post('/saveUserPhoneDetails', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveUserPhoneDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

router.post('/getUserEmails',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getUserEmails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
         return next(err);
        });
});

router.post('/getUserPhoneNumbers', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getUserPhoneNumbers(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

router.post('/addUserEmail',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.addUserEmail(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/addUserPhoneNumber', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.addUserPhoneNumber(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/verifyUserEmail', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.verifyUserEmail(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/verifyUserPhoneNumber', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.verifyUserPhoneNumber(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/deleteUserEmail',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteUserEmail(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});

// deleteUserPhoneNumber
router.post('/deleteUserPhoneNumber', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteUserPhoneNumber(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});




// saveUserEmailDetails
router.post('/saveUserEmailDetails', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveUserEmailDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
         return next(err);
        });
});

// startVerifingEmailAddress
router.post('/startVerifingEmailAddress',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.startVerifingEmailAddress(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

// startVerifingEmailAddress
router.post('/startVerifingPhoneNumber', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.startVerifingPhoneNumber(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});

// confirmVerificationOfEmail
router.post('/confirmVerificationOfEmail', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.confirmVerificationOfEmail(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});

// confirmVerificationOfPhoneNumber
router.post('/confirmVerificationOfPhoneNumber', passporAuth.authenticate(), function(req,res,next){
    ProfileApi.confirmVerificationOfPhoneNumber(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

// deleteUserEmailAddress
router.post('/deleteUserEmailAddress', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteUserEmailAddress(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});



//getUserContactDetails
router.post('/getUserContactDetails', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getUserContactDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});
// changePrimaryEmailInContactDetails
router.post('/changePrimaryEmailInContactDetails',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.changePrimaryEmailInContactDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});
//addNonVerifiedEmailInContactDetails
router.post('/addNonVerifiedEmailInContactDetails',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.addNonVerifiedEmailInContactDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});
// confirmVerificationOfContactEmail
router.post('/confirmVerificationOfContactEmail',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.confirmVerificationOfContactEmail(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});

//deleteEmailInContactDetails
router.post('/deleteEmailInContactDetails',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteEmailInContactDetails(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});


// Work Form Helpper
router.post('/saveWorkInfo',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveWorkInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/getWorkInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getWorkInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/deleteWorkInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteWorkInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});


// Edu Form Helpper
router.post('/saveEduInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveEduInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/getEduInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getEduInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/deleteEduInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteEduInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});


// Exam Form Helpper
router.post('/saveExamInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveExamInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

router.post('/getExamInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getExamInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/deleteExamInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteExamInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

// Interested Course Form Helpper
router.post('/saveInterestedCourseInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveInterestedCourseInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

router.post('/getInterestedCourseInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getInterestedCourseInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/deleteInterestedCourseInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteInterestedCourseInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});


// Author Profile Form Helpper
router.post('/saveAuthorInfo',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveAuthorInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/getAuthorInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getAuthorInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/deleteAuthorInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteAuthorInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});
router.post('/saveAuthorDescInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveAuthorDescInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});


// Teacher Profile Form Helpper
router.post('/saveTeacherInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveTeacherInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/getTeacherInfo',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getTeacherInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/deleteTeacherInfo',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteTeacherInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});

router.post('/saveTeacherDescInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveTeacherDescInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});


// Kid Info Form Helpper
router.post('/saveKidInfo',  passporAuth.authenticate(),function(req,res,next){
    ProfileApi.saveKidInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/getKidInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getKidInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

router.post('/deleteKidInfo', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.deleteKidInfo(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});

// getFriendRequestNotifications
router.post('/getFriendRequestNotifications', passporAuth.authenticate(),function(req,res,next){
		req.body.to_usr_id = req.user.usr_id;
    ProfileApi.getFriendRequestNotifications(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});


// updateFriendRequestNotifications
router.post('/updateFriendRequestNotifications', passporAuth.authenticate(), function(req,res,next){
		req.body.to_usr_id = req.user.usr_id;
    ProfileApi.updateFriendRequestNotifications(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/insertFriendRequestNotifications', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.insertFriendRequestNotifications(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

router.post('/insertTaggedFriendNotifications', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.insertTaggedFriendNotifications(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});

// getTaggedFriendsNotifications
router.post('/getTaggedFriendsNotifications',  passporAuth.authenticate(),function(req,res,next){
		req.body.to_usr_id = req.user.usr_id;
    ProfileApi.getTaggedFriendsNotifications(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
          return next(err);
        });
});


// updateTaggedFriendNotifications
router.post('/updateTaggedFriendNotifications', passporAuth.authenticate(),function(req,res,next){
		req.body.to_usr_id = req.user.usr_id;
    ProfileApi.updateTaggedFriendNotifications(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            console.log(err);
            res.status(500).send({'err':err});
        });
});

router.get('/getAllCountry',  passporAuth.authenticate(), function(req,res,next){
    ProfileApi.getAllCountry(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
           return next(err);
        });
});

router.post('/getAllCitiesOfCountry', passporAuth.authenticate(),function(req,res,next){
    ProfileApi.getAllCitiesOfCountry(req.body)
        .then(function(d){
            res.status(200).send(d);
        })
        .catch(function(err){
            return next(err);
        });
});



module.exports = router;
