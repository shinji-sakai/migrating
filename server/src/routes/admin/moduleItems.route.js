var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var moduleItemsApi = require(configPath.api.admin.moduleItems);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var path = require('path');
var utility = require(configPath.lib.utility);

router.post('/uploadUserMaterial',multipartyMiddleware,async function(req,res){
     try{
        if(!req.files.file){
            res.status(500).json({err:'File is required'});
            return;
        }
        var usr_id = req.body.usr_id;
        var file  = (req.files && req.files.file) || {};
        var parsedFileObj = path.parse(file.path);
        var targetFileName = path.parse(file.originalFilename).name + "_" + usr_id + "_" + new Date().getTime() + parsedFileObj.ext;
        if(req.body.fileName){
            targetFileName = req.body.fileName + "_" + usr_id + "_" + new Date().getTime() + parsedFileObj.ext;
        }
        var targetPath = path.normalize(configPath.common_paths.usr_material_file_relative + "/" +  targetFileName); 
        var uploadRes = await utility.copyFile(file.path, targetPath);

        req.body["upload_path"] = targetPath;
        req.body["upload_nm"] = targetFileName;
        

        var res_data = await moduleItemsApi.saveUserBatchEnrollUploads(req.body);

        res.status(200).send({
            ...res_data,
            Key : targetFileName,
            path : targetPath
        });
    }catch(err){
        res.status(500).json({err:err.message});
    }
})

router.post('/getUserBatchEnrollUploads', function (req, res) {
    moduleItemsApi.getUserBatchEnrollUploads(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/getAll', function (req, res) {
    moduleItemsApi.getAll(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});
router.post('/update', function (req, res) {
    moduleItemsApi.update(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/getCourseItems', function (req, res,next) {
    moduleItemsApi.getCourseItems(req.body)
        .then(function(doc) {
            if(doc && doc.length <= 0){
                console.log(".....getCourseItems",doc);
                next({page:'404'});
            }else{
                res.status(200).send(doc);    
            }
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/getFullCourseDetails', function (req, res,next) {
    moduleItemsApi.getFullCourseDetails(req)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            next(err);
        });
});



router.post('/getModuleItems', function (req, res) {
    moduleItemsApi.getModuleItems(req.body)
        .then(function(doc) {
            if (doc.length > 0) {
                res.status(200).send((doc[0].moduleDetail)[0].moduleItems);
            } else {  
                res.status(200).send(doc); 
            }
        })
        .catch(function sendErrorResponse501(err){
            debug(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/remove', function (req, res) {
    moduleItemsApi.remove(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

module.exports = router;
