var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var examApi = require(configPath.api.admin.exam);

router.post('/getAll', function (req, res) {
    examApi.getAll(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});
router.post('/update', function (req, res) {
    examApi.update(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/remove', function (req, res) {
    examApi.remove(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});
router.post('/fetchExamUsingLimit',function (req, res) {
    examApi.fetchExamUsingLimit(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});


module.exports = router;
