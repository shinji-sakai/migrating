var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var generalApi = require(configPath.api.admin.general);

router.post('/isIdExist',function (req, res) {
    generalApi.isIdExist(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
});

router.post('/fetchItemUsingLimit',function (req, res) {
    generalApi.fetchItemUsingLimit(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
});



module.exports = router;