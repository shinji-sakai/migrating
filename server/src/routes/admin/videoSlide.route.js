var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var videoSlideApi = require(configPath.api.admin.videoSlide);
var utility = require(configPath.lib.utility);
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({
    // uploadDir: __dirname + "../client/assets/images/upload/"
});
var path = require('path');
var Q = require('q');

//Course Master Routes
router.post('/getAll', function (req, res) {
    videoSlideApi.getAll(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/add', function (req, res) {
    videoSlideApi.add(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/delete',function (req, res) {
    videoSlideApi.delete(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/uploadVideoSlideImages',multipartyMiddleware,function (req, res) {
    var videoId = req.body.videoId;
    var arr = []; 
    for (var key in req.files.files) {
        var file = req.files.files[key];
        var dotPos = file.originalFilename.lastIndexOf(".");
        var onlyName = file.originalFilename.substr(0,dotPos);
        var ext = file.originalFilename.substr(dotPos+1);
        var targetFileName = videoId + "_" + onlyName + "." + ext;
        // __dirname ,"./../../../client/build/upload/"
        var targetPath = path.resolve(configPath.common_paths.slide_relative);
        arr[key] = utility.copyFile(file.path, targetPath + '/' + targetFileName);
    }
    Q.all(arr)
        .then(function(){
            res.status(200).send({});
        })
        .catch(function(err){
            res.status(500).send({'err':err});
        })
});

module.exports = router;