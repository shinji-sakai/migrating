var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.forms);

router.post('/addForm', function (req, res) {
    api.addForm(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/getAllForms', function (req, res) {
    api.getAllForms(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/deleteForm', function (req, res) {
    api.deleteForm(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

// deleteEmailTemplate

module.exports = router;
