var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var courseMasterApi = require(configPath.api.admin.courseMaster);

//Course Master Routes
router.post('/getAll', function (req, res) {
    courseMasterApi.getAll(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
});
router.post('/add', function (req, res) {
    courseMasterApi.add(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
});
router.post('/delete',function (req, res) {
    courseMasterApi.delete(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
});

// getRelatedCoursesByCourseId
router.post('/getRelatedCoursesByCourseId',function (req, res) {
    courseMasterApi.getRelatedCoursesByCourseId(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
});


router.post('/fetchCourseMasterUsingLimit',function (req, res) {
    courseMasterApi.fetchCourseMasterUsingLimit(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
}); 
router.post('/isIdExist',function (req, res) {
    courseMasterApi.isIdExist(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            //debug(err);
            res.status(500).json({err:err.message});
        });
});


router.post('/isIdExist',function (req, res) {
    courseMasterApi.isIdExist(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            res.status(500).json({err:err.message});
        });
});


module.exports = router;