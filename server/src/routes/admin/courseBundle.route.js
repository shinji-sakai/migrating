var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var courseBundleApi = require(configPath.api.admin.courseBundle);

router.post('/getAll', function (req, res) {
    courseBundleApi.getAll(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});
router.post('/update', function (req, res) {
    courseBundleApi.update(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/remove', function (req, res) {
    courseBundleApi.remove(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

module.exports = router;
