var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var courseVideosApi = require(configPath.api.admin.courseVideos);

//Course Master Routes
router.post('/add', function (req, res) {
    courseVideosApi.add(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/getModuleVideos', function (req, res) {
    courseVideosApi.getModuleVideos(req.body)
        .then(function(doc) {
            if (doc.length > 0) {
                res.status(200).send((doc[0].moduleDetail)[0].moduleItems);
            } else {  
                res.status(200).send(doc); 
            }
        })
        .catch(function sendErrorResponse501(err){
            debug(err);
            res.status(501).json({err:err.message});
        });
});
router.post('/getCourseVideos',function (req, res) {
    courseVideosApi.getCourseVideos(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});


module.exports = router;