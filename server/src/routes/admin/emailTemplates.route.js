var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.emailTemplates);

router.post('/addEmailTemplates', function (req, res) {
    api.addEmailTemplates(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/getAllEmailTemplates', function (req, res) {
    api.getAllEmailTemplates(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/deleteEmailTemplate', function (req, res) {
    api.deleteEmailTemplate(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

// deleteEmailTemplate

module.exports = router;
