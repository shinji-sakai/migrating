var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var bookApi = require(configPath.api.admin.book);

router.post('/getAll', function (req, res) {
    bookApi.getAll(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});
router.post('/update', function (req, res) {
    bookApi.update(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/remove', function (req, res) {
    bookApi.remove(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/fetchBooksUsingLimit',function (req, res) {
    bookApi.fetchBooksUsingLimit(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});

module.exports = router;
