var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var subTopicApi = require(configPath.api.admin.subtopic);

router.post('/getAll', function (req, res) {
    subTopicApi.getAll(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});
router.post('/update', function (req, res) {
    subTopicApi.update(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

router.post('/remove', function (req, res) {
    subTopicApi.remove(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(501).json({err:err.message});
        });
});

module.exports = router;
