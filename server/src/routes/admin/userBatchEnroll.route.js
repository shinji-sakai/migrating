var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.userBatchEnroll);

router.post('/getUserEnrollAllBatch', function (req, res) {
    api.getUserEnrollAllBatch(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/removeUserBatchEnroll', function (req, res) {
    api.removeUserBatchEnroll(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/addUserBatchEnroll', function (req, res) {
    api.addUserBatchEnroll(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

module.exports = router;
