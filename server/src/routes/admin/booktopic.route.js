var express = require('express');
// Initialize the Router
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.admin.booktopic);

router.post('/updateBookTopic', function (req, res) {
    api.updateBookTopic(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/removeBookTopic', function (req, res) {
    api.removeBookTopic(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/getAllBookTopics', function (req, res) {
    api.getAllBookTopics(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/findBookTopicsByQuery', function (req, res) {
    api.findBookTopicsByQuery(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/findBookTopicById', function (req, res) {
    api.findBookTopicById(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/findBookTopicsById', function (req, res) {
    api.findBookTopicsById(req.body)
        .then(function(doc) {
            res.status(200).send(doc);
        }).catch(function(err){
            console.error(err);
            res.status(500).json({err:err.message});
        });
});

module.exports = router;