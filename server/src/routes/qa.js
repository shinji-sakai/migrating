var express = require('express');
// Initialize the Router
var router = express.Router();
var jwt= require('jsonwebtoken');
var moment = require('moment');
var requestIp = require('request-ip');
var fs = require('fs');

var configPath = require('../configPath.js');
var config = require('../config.js');


router.post('/question', function (req, res) {
    var number = req.body.start;
    console.log(number);
    if(number == 0){
        fs.readFile('data/qa.json', 'utf8', function(err,data){
            if(err){
                res.status(500).json({err:err});
                return;
            }
            res.status(200).send(data);
        });
    }else{
        fs.readFile('data/qa.other.json', 'utf8', function(err,data){
            if(err){
                res.status(500).send({err:err});
                return;
            }
            res.status(200).send(data);
        });
    }
});
router.get('/onlyQues', function (req, res) {
    fs.readFile('data/onlyQue.json', 'utf8', function(err,data){
        if(err){
            res.status(500).send({err:err});
            return;
        }
        res.status(200).send(JSON.parse(data));
    });
});

// Expose the module
module.exports = router;