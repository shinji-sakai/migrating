var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.bundle.bundle);


router.post('/getAll', function (req, res) {
    api.getAll(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/add', function (req, res) {
    api.add(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/delete',function (req, res) {
    api.delete(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});

// getBundlesByCourseId
router.post('/getBundlesByCourseId',async function (req, res) {
    try{
        var api_res = await api.getBundlesByCourseId(req.body);    
        res.status(200).send(api_res);
    }catch(err){
        debug(err);
        res.status(500).json({err:err.message});
    }
});
module.exports = router;