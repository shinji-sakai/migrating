var express = require('express');
var router = express.Router();

var configPath = require('../../configPath.js');
var StatsApi = require(configPath.api.kafka.userstats);

router.post('/', function(req, res) {
	try{
		//convert to json object
		var jsonParsed = JSON.parse(req.body.stats);
		//change hash token to express session id
		// jsonParsed["express_sess"] = req.session.id;
		// console.log("setting token as ..." , req.session.id);
		//set it back to stats
		req.body.stats = JSON.stringify(jsonParsed);

	    StatsApi.sendStats(req.body);
	    res.status(200).send({});
	}catch(err){
		console.log(err);
		res.status(500).send(err);
	}
	
});


router.post('/shouldFetchIdleEvents', async function(req, res) {
	try{
	    var data = await StatsApi.shouldFetchIdleEvents(req.body);
	    res.status(200).send(data);
	}catch(err){
		console.log(err);
		res.status(500).send(err);
	}
	
});

module.exports = router;