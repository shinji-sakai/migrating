var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.batchCourse.batchCourse);


router.post('/getAll', function (req, res) {
    api.getAll(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/add', function (req, res) {
    api.add(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/delete',function (req, res) {
    api.delete(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/getBatchCourse',async(req, res) => {
    try{
        var doc = await api.getBatchCourse(req.body);
        res.status(200).send(doc);
    }catch(err){
        debug(err);
        res.status(500).json({err:err.message});
    }
});


module.exports = router;