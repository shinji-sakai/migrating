var express = require('express');
var router = express.Router();
var configPath = require('../../configPath.js');
var api = require(configPath.api.batchCourse.batchTimingDashboard);
var debug = require('debug')("app:route:batchCourse:batchTimingDashboard.route.js");


router.post('/getAllBatchTimingDashboard', function (req, res) {
    api.getAllBatchTimingDashboard(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/addBatchTimingDashboard', function (req, res) {
    api.addBatchTimingDashboard(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});
router.post('/deleteBatchTimingDashboard',function (req, res) {
    api.deleteBatchTimingDashboard(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});

router.post('/setBatchTimingDashboardAsCompleted',function (req, res) {
    api.setBatchTimingDashboardAsCompleted(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            debug(err);
            res.status(500).json({err:err.message});
        });
});


module.exports = router;