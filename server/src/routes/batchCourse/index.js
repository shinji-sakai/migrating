var batchCourse = require('./batchCourse.route.js');
var batchTiming = require('./batchTiming.route.js');
var batchTimingDashboard = require('./batchTimingDashboard.route.js');
var currency = require('./currency.route.js');
var getting_started = require('./gettingStarted.route.js');
var sessions = require('./sessions.route.js');
var sessionItems = require('./sessionItems.route.js');

module.exports = function(app){
	app.use('/batch_course',batchCourse);
	app.use('/batch_course/timing',batchTiming);
	app.use('/batch_course/timingDashboard',batchTimingDashboard);
	app.use('/batch_course/currency',currency);
	app.use('/batch_course/gettingStarted',getting_started);
	app.use('/batch_course/sessions',sessions);
	app.use('/batch_course/sessionItems',sessionItems);
}