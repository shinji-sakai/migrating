
var express = require('express');
// Initialize the Router
var router = express.Router();

var configPath = require('../../configPath.js');
var NotesAPI = require(configPath.api.notes.notes);

router.post('/deleteNote',function (req, res, next) {
    NotesAPI.deleteNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            return next(err)
        })
});

router.post('/updateNote',function (req, res, next) {
    NotesAPI.updateNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            return next(err)
        })
});

router.post('/getNotes',function (req, res, next) {
    NotesAPI.getNotes(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            console.log(err)
            return next(err.toString())
        })
});

router.post('/insertNote',function (req, res, next) {
    NotesAPI.insertNote(req.body)
        .then(function(doc){
            res.status(200).send(doc);
        }).catch(function(err){
            return next(err)
        })
});

module.exports=router;