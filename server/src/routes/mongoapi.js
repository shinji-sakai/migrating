//Not Used
//Deprecated

var configPath = require('../configPath.js');
//var getConnection = require(configPath.routes.mongoconnect);
var getConnection = require(configPath.dbconn.mongoconn);
var bcrypt = require('bcrypt-nodejs');
var debug = require('debug')('app:mongoapi');
var globalFunctions = require(configPath.api.globalFunctions);

function MongoApi() {}


MongoApi.prototype.addcourse = function (data) {
    return getConnection()
            .then(courseListCol)
            .then(function (col) {
                return col.updateOne({courseId: data.courseId}, data, {upsert: true, w: 1});
            })
            .catch(globalFunctions.err);
}
MongoApi.prototype.removeCourse = function (data) {
    return getConnection()
        .then(courseListCol)
        .then(removeData)
        .catch(globalFunctions.err);
    function removeData(col) {
        return col.deleteOne({courseId: data.courseId});
    }
}


MongoApi.prototype.getCourseModules = function (data) {
    return getConnection()
        .then(courseModuleCol)
        .then(getCM)
        .catch(globalFunctions.err);

    function getCM(col) {
        return col.find({courseId: data.courseId}).toArray();
    }
}
MongoApi.prototype.addCourseModule = function (data) {
    return getConnection()
            .then(courseModuleCol)
            .then(function (col) {
                return col.updateOne({moduleId: data.moduleId}, data, {upsert: true, w: 1});
            })
            .catch(globalFunctions.err);
}
MongoApi.prototype.deleteCourseModule = function (data) {
    var col;
    return getConnection()
        .then(courseModuleCol)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        return col.deleteOne(data);
    }
}



MongoApi.prototype.addCourseMaster = function (data) {

    var gcol;
    return getConnection()
        .then(courseMasterCol)
        .then(removeData)
        .then(insertData)
        .catch(globalFunctions.err);
    function removeData(col) {
        gcol = col;
        return col.deleteOne({courseId: data.courseId});
    }
    function insertData() {
        return gcol.insertOne({courseId: data.courseId, courseName: data.courseName, author: data.author});
    }
}
MongoApi.prototype.deleteCourseMaster = function (data) {
    return getConnection()
        .then(courseMasterCol)
        .then(removeData)
        .catch(globalFunctions.err);
    function removeData(col) {
        return col.deleteOne({courseId: data.courseId});
    }
}
MongoApi.prototype.displayCourseMaster = function () {
    return getConnection()
        .then(courseMasterCol)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({}, {courseId: 1, courseName: 1, author: 1, _id: 0}).toArray();
    }
}




MongoApi.prototype.addVideoSlide = function (data) {
    var col;
    return getConnection()
        .then(courseVideoSlideCol)
        .then(removeData)
        .then(insertData)
        .catch(globalFunctions.err);

    function removeData(gcol) {
        col = gcol;
        return col.updateOne({
            "videoId": data.videoId
        }, {
            $pull: {"slideDetail": {"slideId": data.slideDetail.slideId}}
        });
    }

    function insertData() {
        return col.updateOne(
            {
                "videoId": data.videoId
            },
            {
                $addToSet: {
                    "slideDetail": data.slideDetail
                },
                $set: {
                    "topicTitle": data.topicTitle,
                    "slideDesc": data.slideDesc,
                    "lastUpdated":new Date()
                }
            }, {upsert: true});
    }
}
MongoApi.prototype.deleteVideoSlide = function (data) {
    return getConnection()
        .then(courseVideoSlideCol)
        .then(removeData)
        .catch(globalFunctions.err);

    function removeData(col) {
        return col.updateOne({
            "videoId": data.videoId,
        }, {
            $pull: {"slideDetail": {"slideId": data.slideId}}
        });
    }
}
MongoApi.prototype.getVideoSlides = function (data) {
    return getConnection()
        .then(courseVideoSlideCol)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({"videoId": data.videoId}).toArray();
    }
}




MongoApi.prototype.addVideos = function (data) {
    var col;
    return getConnection()
        .then(courseVideoCol)
        .then(removeData)
        .then(insertData)
        .catch(globalFunctions.err);

    function removeData(gcol) {
        col = gcol;
        return col.updateOne({
            "courseId": data.courseId,
            "courseName": data.courseName
        }, {
            $pull: {"moduleDetail": {"moduleId": data.moduleDetail.moduleId}}
        });
    }

    function insertData() {
        return col.updateOne(
            {
                "courseId": data.courseId,
                "courseName": data.courseName
            },
            {
                $addToSet: {
                    "moduleDetail": data.moduleDetail
                }, $set: {"author": data.author,"courseLevel":data.courseLevel,courseLongDesc:data.courseLongDesc,courseShortDesc:data.courseShortDesc,LastUpdated:data.LastUpdated}
            }, {upsert: true});
    }
}
MongoApi.prototype.getModuleVideos = function (data) {
    return getConnection()
        .then(courseVideoCol)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({"moduleDetail.moduleId": data.moduleId}, {"moduleDetail.$": 1, _id: 0}).toArray();
    }
}
MongoApi.prototype.getCourseVideos = function (data) {
    return getConnection()
        .then(courseVideoCol)
        .then(find)
        .catch(globalFunctions.err);

    function find(col) {
        return col.find({"courseId": data.courseId}).toArray();
    }
}


MongoApi.prototype.getFullDoc = function (data) {
    return getConnection()
        .then(dbCollection)
        .catch(globalFunctions.err);

    function dbCollection(db) {
        var col = db.collection(data.docname);
        return col.find({}, {_id: 0}).toArray();
    }
}


/* Global Methods    */
function courseListCol(db){
    return db.collection("courselist");
}
function courseModuleCol(db){
    return db.collection("coursemodule");
}
function courseMasterCol(db){
    return db.collection("coursemaster");
}
function courseVideoSlideCol(db){
    return db.collection('coursevideoslides');
}
function courseVideoCol(db){
    return db.collection('coursevideos');
}
module.exports = MongoApi;