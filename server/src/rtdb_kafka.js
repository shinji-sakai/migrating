 var r = require('rethinkdb');

console.log("Trying to connect RThinkDB on 139.59.1.89:28015")
var rdb_conn = r.connect({
        host: '139.59.1.89',
        port: 28015,
        db : 'ew1'
    });
rdb_conn.then(function(conn) {
    console.log("Connected rthinkdb.");
});
var rdb_fetch_data = function(displayname, cb) {
    rdb_conn.then(function(conn) {
        var clicks = 0;
        r.table('user_stats').filter({
            name: displayname
        }).run(conn, function(err, cursor) {
            if (cursor) {
                cursor.each(function(err, row) {
                    if (err) {
                        throw err;
                        console.log(err);
                        return;
                    }
                    //console.log(row.clickcount);
                    clicks = row.clickcount;
                },function finished(){
                    cb(clicks);
                });
            }
            
        })

    });
};
var rdb_update_data = function(displayname, clicks) {
    return rdb_conn.then(function(conn) {
        //  console.log(clicks);
        return r.table('user_stats').filter({
            name: displayname
        }).update({
            clickcount: clicks
        }).run(conn);
    });
};
module.exports = {
    fetch: rdb_fetch_data,
    update: rdb_update_data
};
//rdb_fetch_data("jeevan4");