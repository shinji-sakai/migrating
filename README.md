# dscode

On Client Side
-------------------
```sh
$ npm install -g bower
$ npm install
$ bower install
$ ./node_modules/.bin/webpack --watch
```
On server side
--------------------
- If using windows install python 2.7.
```sh
$ npm install -g nodemon
$ npm install babel-cli
$ npm install --no-optional //provide --no-optional argument if you dont want to install oracledb package
$ sudo nodemon ./build/bin/www.js --dev --logs // if mac
$ nodemon ./build/bin/www.js --no-oracle --dev --logs // recommended : if dont want to use oracle
$ nodemon ./build/bin/www.js --dev --logs // if windows and want to use oracle
```

- if build folder is empty then do ```babel -d ./build ./src``` first.

Cass Tables
----------------
For Login
-----
'ew1.vt_usr_lgn'

'ew1.vt_usr_reg'

'ew1.vt_usr_reg_otp'


For Chat
-----
'ew1.vt_frs_chat_ts'

'ew1.vt_usr_short'

'ew1.vt_frs_chat_ts'

'ew1.vt_frs_chat_m'

'ew1.vt_grp_chat_md'

'ew1.vt_frs_chat_d'


For CB/Comments
------

'ew1.vt_usr_comnt_lvl0'

'ew1.vt_qry_like'

'ew1.vt_qry_like_stats'


'ew1.vt_posts'

'ew1.vt_posts_crt_by'

'ew1.vt_frs_posts'

'ew1.vt_my_posts'

"ew1.ew_pst_like"

"ew1.ew_pst_like_stats"


"ew1.ew_usr_shrt_dtls"

'ew1.vt_user_frs'

"ew1.ew_rec_frs"

"ew1.ew_fr_req_snt"

"ew1.ew_fr_req_rcvd"

"ew1.ew_cb_my_flw"


"ew1.ew_usr_dtls"

"ew1.ew_usr_contact_dtls"

"ew1.ew_usr_work_dtls"

"ew1.ew_usr_edu_dtls"

"ew1.ew_usr_exm_dtls"

"ew1.ew_usr_crs_dtls"

"ew1.ew_usr_author_dtl"

"ew1.ew_usr_tch_dtl"

"ew1.ew_usr_parent_kid"


"ew1.ew_fr_req_noti"

"ew1.ew_pst_tag_noti"



For Forums
------------------
'ew1.vt_qry_cats'

'ew1.ew_qry_flw'

'ew1.ew_qry_flw_ntf'

'ew1.vt_forum_qrys'

'ew1.vt_qry_trend'

'ew1.vt_qry_vws'

'ew1.ew_qry_stats'

'ew1.ew_qry_usr_stats'

'ew1.ew_qry_ans_ltr'

'ew1.ew_qry_flw_ntf'

'ew1.ew_del_rsn'

'ew1.ew_cmpln_rsn'


For Tests
---------
'ew1.vt_usr_created_tsts'

'ew1.vt_vw_tsts'

'ew1.vt_usr_prtc_stat'


Misc
---------
'ew1.vt_usr_crs_bmrk'

'ew1.ew_contact_msgs'
