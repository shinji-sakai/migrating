var gulp = require('gulp'),
  connect = require('gulp-connect');
  sass = require('gulp-sass');
 
gulp.task('connect', function() {
  connect.server({
  	root:['app','.'],
    livereload: true,
    port : 8989
  });
});

gulp.task('styles',function(){
	return gulp.src('assets/scss/base.scss')
		.pipe(sass())
		.pipe(gulp.dest("assets/css/"));
});
 
gulp.task('reload', function () {
  gulp.src('./app/**/*')
    .pipe(connect.reload());
});
 
gulp.task('watch', function () {
  gulp.watch(['./app/**/*','./assets/**/*'], ['styles','reload']);
});
 
gulp.task('default', ['connect' , 'styles' , 'watch']);