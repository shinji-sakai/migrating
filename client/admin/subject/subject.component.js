	angular.module(window.moduleName)
        .component('subjectPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./subject.html'),
            controller: AdminSubjectController,
            controllerAs: 'vm'
        });

	AdminSubjectController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminSubjectController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;
        vm.no_of_item_already_fetch=0;
        vm.checkValidityOfID = checkValidityOfID;
    	vm.updateSubject = updateSubject;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removeSubject = removeSubject;
    	vm.editSubject = editSubject;

    	vm.$onInit = function() {
        	//updateSubjectsList();
	    };

        function checkValidityOfID() {
            vm.form.subjectId = vm.form.subjectId || "";
            vm.form.subjectId = vm.form.subjectId.replace(/\s/g , "-");
				admin.isIdExist({
				collection :'subject',
				key:"subjectId",
				id_value:vm.form.subjectId			
			}).then(function(data)
			{				
              console.log(data.data);
			  if(data.data.idExist)
			  {
				    vm.idExists = true;
			  }
			  else
			  {
				   vm.idExists = false;
			  }
			}).catch(function(err){
             console.log(err);
			})
			
			
         
        }

	    function updateSubjectsList(){
	    	pageLoader.show();
	    	admin.getAllSubject()
        		.then(function(data){
        			if(data.data.length > 0)
        				vm.subjects = data.data;
                    else
                        vm.subjects = [];
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }

    	function updateSubject(){
    		var data = {
    			subjectId : vm.form.subjectId,
    			subjectName : vm.form.subjectName,
    			subjectDesc : vm.form.subjectDesc 
    		}
			if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }

    		pageLoader.show();
    		admin.updateSubject(data)
    			.then(function(res){
					vm.fetchSubject();
    				vm.form.error = false;
    				vm.form.subjectName = "";
    				vm.form.subjectDesc = "";
                    vm.form.subjectId = "";
                    vm.idExists = false;
                    vm.formIsInEditMode = false;
    			//	updateSubjectsList();
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}

    	function resetFormDetail(){
    		vm.submitted = false;
            vm.idExists = false;
            vm.formIsInEditMode = false;
    		vm.form = {};
    	}
		vm.fetchMoreSubject = function(){
			vm.isSubjectLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				   collection : 'subject',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				$log.log('res.data'+res.data);
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.subjects , {subjectId: v.subjectId})[0];
						if(!item){
								vm.subjects.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					vm.last_update_dt = vm.subjects[vm.subjects.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isSubjectLoading = false;
			})
		}
vm.fetchSubject=function(){
	vm.isSubjectLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
			 collection : 'subject'
			})
			.then(function(res){
				$log.debug(res)
					vm.subjects = res.data;
			    	vm.no_of_item_already_fetch = 5;
					vm.last_update_dt = vm.subjects[vm.subjects.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isSubjectLoading = false;
			})
}
    	function removeSubject(id){
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
    		admin.removeSubject({subjectId:id})
    			.then(function(d){
    				console.log(d);
						var item = $filter('filter')(vm.subjects || [], {subjectId: id})[0];								
					 if (vm.subjects.indexOf(item) > -1) {
                        var pos = vm.subjects.indexOf(item);
                       vm.subjects.splice(pos, 1);
                    }
    				//updateSubjectsList();
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}

    	function editSubject(id){
            console.log(id);
    		var subject = $filter('filter')(vm.subjects,{subjectId:id})[0];
    		vm.form = {};
    		vm.form.subjectId = subject.subjectId;
    		vm.form.subjectName = subject.subjectName;
    		vm.form.subjectDesc = subject.subjectDesc;
            vm.formIsInEditMode = true;
    	}
    }