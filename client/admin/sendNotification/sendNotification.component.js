require('angular-textbox-io');
require('service.admin');
require('service.profile');
angular.module(window.moduleName)
    .component('sendNotificationPage', {
        template: require('./sendNotification.html'),
        controller: SendNotificationPageController,
        controllerAs: 'vm'
    })

SendNotificationPageController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin','profile'];

function SendNotificationPageController($log,$filter, $scope, $state, EditorConfig, $timeout,admin,profile) {
    var vm = this;

    vm.selectedTraining = [];
    vm.selectedBatch = [];
    vm.selectedUsers = [];

    vm.onUserChange = onUserChange;
    vm.addSendNotification = addSendNotification;
    vm.editSendNotification = editSendNotification;
    vm.deleteSendNotification = deleteSendNotification;
    
    vm.$onInit = function() {
        getAllSendNotifications();
        getAllUsers();
    }

    function getAllSendNotifications(){
        admin.getAllSendNotifications()
            .then(function(res){
                vm.allSendNotificaions = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }


    function getAllUsers(){
        profile.getAllUsersShortDetails()
            .then(function(d){
                vm.allUsers = d.data;
            })
            .catch(function(err){
                console.log(err);
            })
    }

    function onUserChange(){
        vm.form = vm.form || {};
        vm.form.users = vm.selectedUsers || [];
    }

    function addSendNotification(){
        vm.formSuccess = undefined;
        admin.addSendNotification(vm.form)
            .then(function(res){

                // var usr_noti_pk = res.data.usr_noti_pk;
                // var group = $filter("filter")(vm.allSendNotificaions,{usr_noti_pk : usr_noti_pk})[0];
                // if(group){
                //     var pos = vm.allSendNotificaions.indexOf(group);
                //     vm.allSendNotificaions.splice(pos,1);    
                // }

                vm.allSendNotificaions.push(res.data);
                vm.formSuccess = true;
                vm.form = {};
                vm.selectedUsers = [];
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function editSendNotification(usr_noti_pk){
        vm.formSuccess = undefined;
        var group = $filter("filter")(vm.allSendNotificaions,{usr_noti_pk : usr_noti_pk});
        if(group && group[0]){
            vm.form = group[0];
            if(!vm.allUsers){
                getAllUsers();
            }
            vm.selectedUsers = vm.form.users;
        }
    }

    function deleteSendNotification(usr_noti_pk){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteSendNotification({usr_noti_pk : usr_noti_pk})
            .then(function(res){
                var usr_noti_pk = res.data.usr_noti_pk;
                var group = $filter("filter")(vm.allSendNotificaions,{usr_noti_pk : usr_noti_pk})[0];
                var pos = vm.allSendNotificaions.indexOf(group);
                vm.allSendNotificaions.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}