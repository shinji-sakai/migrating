require('service.admin');
require('./_sideLinks.scss');
require('service.aws');
angular.module(window.moduleName)
    .component('blogSideLinksArticle', {
    	require: {
            parent : '^adminPage'
        },
        template: require('./sideLinks.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig','awsService'];
function Controller(admin,$filter,$scope,$state,EditorConfig,awsService){
	var vm = this;


	vm.selectedCourse = [];
	vm.selectedTopics = [];
	vm.selectedFilteredTopics = [];

	vm.EditorConfig = EditorConfig;
	EditorConfig.heightMin = 500;
	EditorConfig.imageDefaultWidth = 1024;

	vm.savePage = savePage;
	vm.scrollDown = scrollDown;
	vm.scrollUp = scrollUp;
	vm.editPage = editPage;
	vm.deletePage = deletePage;
	vm.onIdFieldChange = onIdFieldChange;
	vm.onCourseChange = onCourseChange;
	vm.onTopicChange = onTopicChange;
	vm.shouldShowArticleInList  = shouldShowArticleInList;
	vm.uploadMaterialFile = uploadMaterialFile;

	vm.$onInit = function(){
		$scope.$watch(function() {
            return vm.parent.courselistdata;
        }, function(v) {
            vm.allCourses = v;
        });

        //Observer files
		$scope.$watch('vm.materialFile',function(newValue,oldValue){
			if(newValue){
				vm.materialFileName = newValue.name;	
				vm.isMaterialUploadInProgress = false;
				vm.isMaterialUploaded = false;
			}
			
		})


		vm.parent.hideSidebar = true;
		getAllPages();
		getAllSubgroup();
		getAllTopics();
	}

	function getAllSubgroup(){
			admin.getAllSubgroup()
				.then(function(res){
					vm.allSubgroup = res.data
				})
		}

	function getAllPages(){
		admin.getAllSideLinksArticlePage()
			.then(function(res){
				vm.allPages = res.data
				vm.articlesToShow = vm.allPages;
			})
	}

	function getAllTopics(){
    	admin.getAllTopic()
    		.then(function(d){
    			vm.allTopics = d.data;
    		});
    }

	function onCourseChange(){
        vm.form = vm.form || {};
        vm.form.crs_id = vm.selectedCourse[0] || "";
    }

    function onTopicChange(){
        vm.form = vm.form || {};
        vm.form.topics = vm.selectedTopics || [];
    }


    function shouldShowArticleInList(article){
    	var topics = article.topics || [];
    	var found = true;
    	for (var i = 0; i < vm.selectedFilteredTopics.length; i++) {
    		var selectedTopic = vm.selectedFilteredTopics[i];
    		if(topics.indexOf(selectedTopic) <= -1){
    			found = false;
    			break;
    		}
    	}
    	return found;
    }

	function onIdFieldChange(){
		vm.form.page_id = vm.form.page_id.replace(" ", "");
	}

	function scrollUp() {
        $('html,body').animate({scrollTop: 0},'slow');
    }

    function scrollDown() {
        $('html,body').animate({scrollTop: $(document).height()},'slow');
    }

    

	function savePage(){
		admin.addSideLinksArticlePage(vm.form)
			.then(function(res){
				getAllPages();
				vm.form = {};
				vm.materialFileName = undefined;	
				vm.isMaterialUploadInProgress = false;
				vm.isMaterialUploaded = false;
				vm.isPageEditing = false;
				vm.currentlyEditPage = undefined;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function editPage(id){
		var page = $filter('filter')(vm.allPages,{page_id : id})[0];
		delete page._id;
		vm.form = page;
		vm.selectedCourse = vm.form.crs_id ? [vm.form.crs_id] : [];
		vm.selectedTopics = vm.form.topics || [];
		vm.materialFileName = vm.form.materialInfo || "";
		vm.isPageEditing = true;
		vm.currentlyEditPage = page;
	}

	function deletePage(id){
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		admin.deleteSideLinksArticlePage({page_id : id})
			.then(function(res){
				getAllPages();
				vm.isPageEditing = false;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function uploadMaterialFile(){
    	var file = {
    		type : 'download',
    		file : vm.materialFile
    	}
    	vm.isMaterialUploadInProgress = true;
    	awsService.uploadFileToS3(file)
    		.then(function(d){
				var data = d.data;
				vm.form = vm.form || {};
				vm.form.materialInfo = data.Key;
				vm.isMaterialUploadInProgress = false;
				vm.isMaterialUploaded = true;
			},function(err){
				console.error(err);
			},function(evt){
				var width = parseInt(100.0 * evt.loaded / evt.total);
				console.log(width);
				vm.uploadProgress = width;
			})
    }
}