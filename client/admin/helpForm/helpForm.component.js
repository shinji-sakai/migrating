require('service.admin');
require('angular-textbox-io');
require('./_helpForm.scss')
angular.module(window.moduleName)
    .component('helpFormPage', {
        template: require('./helpForm.html'),
        controller: FormsController,
        controllerAs: 'vm'
    })

FormsController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin','$stateParams'];

function FormsController($log,$filter, $scope, $state, EditorConfig, $timeout,admin,$stateParams) {
    var vm = this;

    vm.selectedForm = [];
    vm.formToView = $stateParams.form;

    vm.onFormChange = onFormChange;
    vm.addFormHelp = addFormHelp;
    vm.editFormHelp = editFormHelp;
    vm.deleteFormHelp = deleteFormHelp;
    
    vm.$onInit = function() {
        getAllForms();
        getAllFormsHelp();
    }

    function getAllForms(){
        admin.getAllForms()
            .then(function(res){
                vm.allForms = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllFormsHelp(){
        admin.getAllFormsHelp()
            .then(function(res){
                vm.allFormsHelp = res.data;
                if(vm.formToView){
                    vm.formContent = $filter('filter')(vm.allFormsHelp,{form_id : vm.formToView})[0];
                }
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function onFormChange() {
        if(vm.selectedForm[0]){
            vm.form.form_id = vm.selectedForm[0]    
        }
    }

    function addFormHelp(){
        vm.formSuccess = undefined;
        admin.addFormHelp(vm.form)
            .then(function(res){
                var form_id = res.data.form_id;
                var group = $filter("filter")(vm.allFormsHelp,{form_id : form_id})[0];
                if(group){
                    var pos = vm.allFormsHelp.indexOf(group);
                    vm.allFormsHelp.splice(pos,1);    
                }

                vm.allFormsHelp.push(res.data);
                vm.formSuccess = true;
                vm.form = {};
                vm.form.help_content = "<p>&nbsp;</p>";
                vm.selectedForm = [];
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function editFormHelp(form_id){
        vm.formSuccess = undefined;
        var group = $filter("filter")(vm.allFormsHelp,{form_id : form_id});
        if(group && group[0]){
            vm.form = group[0];
            vm.selectedForm = [vm.form.form_id]
        }
    }

    function deleteFormHelp(form_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteFormHelp({form_id : form_id})
            .then(function(res){
                var form_id = res.data.form_id;
                var group = $filter("filter")(vm.allFormsHelp,{form_id : form_id})[0];
                var pos = vm.allFormsHelp.indexOf(group);
                vm.allFormsHelp.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}