require("./_styles.scss")
require("service.utility")
require('angular-froala');
angular.module(window.moduleName)
    .component('previousPapersPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./previousPapers.html'),
        controller: AdminPreviousPapersController,
        controllerAs: 'vm'
    })

AdminPreviousPapersController.$inject = ['admin', '$filter', '$scope', '$state','alertify','utility','EditorConfig'];

function AdminPreviousPapersController(admin, $filter, $scope, $state , alertify, utility,EditorConfig) {
    var vm = this;

	EditorConfig.toolbarButtons = EditorConfig.toolbarButtonsMD = EditorConfig.toolbarButtonsSM = EditorConfig.toolbarButtonsXS = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertImage', 'insertVideo', 'insertTable', 'html'],
    EditorConfig.width = screen.availWidth / 2;
    EditorConfig.height = screen.availHeight;
    EditorConfig.placeholderText = 'Type Previous Paper Format Here',
    EditorConfig.quickInsertButtons = []
    EditorConfig.pluginsEnabled = ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"]

    vm.EditorConfig = EditorConfig;

    vm.selectedCourse = [];
    vm.selectedChild = [];
    vm.isCourseExpanded = true;
    vm.isFilterExpanded = true;

    vm.selectedSubjects = [];
    vm.selectedAuthors = [];
    vm.selectedPublications = [];
    vm.selectedBooks = [];
    vm.selectedExams = [];
    vm.selectedTopics = [];
    vm.selectedTopicGroup = [];
    vm.selectedTags = [];

    vm.trustAsHTML = utility.trustAsHTML;
	
	vm.$onInit = function(){
		$scope.$watch(function() {
            return vm.parent.coursemaster;
        }, function(v) {
        	vm.allCourses = v;
        });
        getAllFilterValues();
        getAllPreviousPapers();
	}

    function getAllPreviousPapers(){
        admin.getAllPreviousPapers()
            .then(function(res){
                vm.allPreviousPapers = res.data
            })
            .catch(function(err){
                console.error(err)
            })
    }

	function getAllFilterValues(){
		admin.getAllSubject()
            .then(function(d) {
                vm.subjects = d.data;
            });
        admin.getAllAuthor()
            .then(function(d) {
                vm.authors = d.data;
            });
        admin.getAllPublication()
            .then(function(d) {
                vm.publications = d.data;
            });
        admin.getAllBook()
            .then(function(d) {
                vm.books = d.data;
            });
        admin.getAllExam()
            .then(function(d) {
                vm.exams = d.data;
            });
        admin.getAllTopic()
            .then(function(d) {
                vm.topics = d.data;
            });
        admin.getAllTopicGroups()
            .then(function(d) {
                vm.topicGroups = d.data;
            });
        admin.getAllTag()
            .then(function(d) {
                vm.tags = d.data;
            });
	}

	vm.collapseAllQuestions = function(){
		if(vm.questions && vm.questions.length > 0){
			vm.questions = vm.questions.map(function(v,i){
				v.isOpen = false;
				return v;
			})
		}
	}

	vm.openAllQuestions = function(){
		if(vm.questions && vm.questions.length > 0){
			vm.questions = vm.questions.map(function(v,i){
				v.isOpen = true;
				return v;
			})
		}
	}

	vm.onCourseChange = function(){
		if(vm.selectedCourse.length > 0){
			admin.getBoardCompetitiveCourse({
				course_id : vm.selectedCourse[0]
			})
			.then(function(res){
				if(res.data && res.data[0] && res.data[0].course_id){
					vm.fullCourse = res.data[0]
				}
			})
			.catch(function(err){
				console.log(err);
			})
		}else{
			vm.fullCourse = {};
		}
    }

    vm.onChildChange = function(){
    	if(vm.selectedChild.length > 0){
			admin.getPreviousPaper({
				paper_id : vm.selectedChild[0]
			})
			.then(function(res){
				if(res.data){
					vm.previousPaper = res.data.paper
				}
			})
			.catch(function(err){
				console.error(err);

			})
    	}else{
    		vm.previousPaper = "";
    	}
    }

    vm.onFilterChange = function(item, model) {
        if(vm.selectedSubjects.length === 0 &&
			vm.selectedAuthors.length === 0 &&
			vm.selectedPublications.length === 0 &&
			vm.selectedBooks.length === 0 &&
			vm.selectedExams.length === 0 &&
			vm.selectedTopics.length === 0 &&
			vm.selectedTopicGroup.length === 0 &&
			vm.selectedTags.length === 0)
        {
        	vm.questions = [];
			return;
        }

        var query = {
                subjects: vm.selectedSubjects,
                authors: vm.selectedAuthors,
                publications: vm.selectedPublications,
                books: vm.selectedBooks,
                exams: vm.selectedExams,
                topics: vm.selectedTopics,
                topicGroup: vm.selectedTopicGroup,
                tags: vm.selectedTags
            }
        vm.showLoader = true;
        admin.findPracticeQuestionsTextByQuery(query)
            .then(function(d) {
            	vm.showLoader = false;
                // console.log(d.data);
                if (d.data.length > 0) {
                    vm.questions = d.data;
                    vm.questions.sort(function(a, b) {
                        return a.questionNumber - b.questionNumber;
                    });
                } else {
                    vm.questions = [];
                }
            })
            .catch(function(err) {
            	vm.showLoader = false;
                console.log(err);
            });
    }

    vm.updatePreviousPaper = function(){
    	if(vm.isPaperSaving){
    		return;
    	}
    	var obj = {
    		paper_id : vm.selectedChild[0],
    		course_id : vm.selectedCourse[0],
    		paper : vm.previousPaper
    	}
		
		vm.isPaperSaving = true;
    	admin.updatePreviousPaper(obj)
    		.then(function(res){
    			console.log(res)
    			alertify.success("Successfully Saved")
    			vm.isPaperSaving = false;
                getAllPreviousPapers();
    		})
    		.catch(function(err){
    			console.log(err)
    			alertify.error("Error in updating paper")
    			vm.isPaperSaving = false;
    		})
    }

    vm.editPreviousPaper = function(paper_id){
        if(vm.allPreviousPapers){
            var paper = $filter('filter')(vm.allPreviousPapers,{paper_id : paper_id});

            if(paper && paper[0]){
                vm.previousPaper = paper[0].paper;
                vm.selectedCourse = [paper[0].course_id];
                vm.selectedChild = [paper[0].paper_id];
                vm.onCourseChange();
            }
        }
    }

    vm.removePreviousPaper = function(paper_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        admin.removePreviousPaper({paper_id : paper_id})
            .then(function(res){
                getAllPreviousPapers();
            })
            .catch(function(err){
                console.log(err)
                alertify.error("Error in deleting paper")
            })
    }

    vm.resetForm = function(){
    	var result = confirm("Are you sure you want to reset form");
        if (!result) {
            //Declined
            return;
        }
        vm.isPaperSaving = false;
        vm.previousPaper = "";
    }
}