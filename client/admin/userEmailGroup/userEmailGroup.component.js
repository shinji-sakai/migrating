require('angular-textbox-io');
require('service.admin');
require('service.profile')
angular.module(window.moduleName)
    .component('userEmailGroupPage', {
        template: require('./userEmailGroup.html'),
        controller: UserEmailGroupController,
        controllerAs: 'vm'
    })

UserEmailGroupController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin','profile'];

function UserEmailGroupController($log,$filter, $scope, $state, EditorConfig, $timeout,admin,profile) {
    var vm = this;

    vm.selectedTraining = [];
    vm.selectedBatch = [];
    vm.selectedUsers = [];

    vm.onTrainingChange = onTrainingChange;
    vm.onBatchChange = onBatchChange;
    vm.onUserChange = onUserChange;
    vm.addUserEmailGroup = addUserEmailGroup;
    vm.editUserEmailGroup = editUserEmailGroup;
    vm.deleteUserEmailGroup = deleteUserEmailGroup;
    
    vm.$onInit = function() {
        getAllUserEmailGroups();
        getAllBatchTraining();
        getAllUsers();
    }

    function getAllUserEmailGroups(){
        admin.getAllUserEmailGroups()
            .then(function(res){
                vm.allUserEmailGroup = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllUsers(){
        profile.getAllUsersShortDetails()
            .then(function(d){
                console.log(d.data);
                vm.allUsers = d.data;
            })
            .catch(function(err){
                console.log(err);
            })
    }

    function getAllBatchTraining() {
        admin.getAllBatchCourse()
            .then(function(res){
                vm.allTrainings = res.data;
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    

    function  getAllTimingByTrainingId(id) {
        admin.getTimingByCourseId({crs_id : id || vm.selectedTraining[0]})
            .then(function(res){
                vm.allBatches = res.data;
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function onTrainingChange(){
        vm.form = vm.form || {};
        vm.form.training_id = vm.selectedTraining[0] || "";
        getAllTimingByTrainingId();
    }

    function onBatchChange(){
        vm.form = vm.form || {};
        vm.form.bat_id = vm.selectedBatch[0] || "";
    }

    function onUserChange(){
        vm.form = vm.form || {};
        vm.form.usrs = vm.selectedUsers || [];
    }

    function addUserEmailGroup(){
        vm.formSuccess = undefined;
        admin.addUserEmailGroup(vm.form)
            .then(function(res){

                var pk_id = res.data.pk_id;
                var group = $filter("filter")(vm.allUserEmailGroup,{pk_id : pk_id})[0];
                if(group){
                    var pos = vm.allUserEmailGroup.indexOf(group);
                    vm.allUserEmailGroup.splice(pos,1);    
                }

                vm.allUserEmailGroup.push(res.data);
                vm.formSuccess = true;
                vm.form = {};
                vm.selectedTraining = [];
                vm.selectedBatch = [];
                vm.selectedUsers = [];
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function editUserEmailGroup(pk_id){
        vm.formSuccess = undefined;
        var group = $filter("filter")(vm.allUserEmailGroup,{pk_id : pk_id});
        if(group && group[0]){
            vm.form = group[0];
            vm.selectedTraining = [vm.form.training_id]
            vm.selectedBatch = [vm.form.bat_id]
            vm.selectedUsers = vm.form.usrs;
            getAllTimingByTrainingId(vm.form.training_id);
        }
    }

    function deleteUserEmailGroup(pk_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteUserEmailGroup({pk_id : pk_id})
            .then(function(res){
                var pk_id = res.data.pk_id;
                var group = $filter("filter")(vm.allUserEmailGroup,{pk_id : pk_id})[0];
                var pos = vm.allUserEmailGroup.indexOf(group);
                vm.allUserEmailGroup.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}