var Base64 = require('hash_js').Base64;
require('service.utility')
require('angular-textbox-io');
require('angular-froala');
require('alertify.js/dist/js/alertify.js');
require('alertify.js/dist/js/ngAlertify.js');
require('alertify.js/dist/css/alertify.css');

require("./_practice.scss")
angular.module(window.moduleName)
    .component('practicePage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./practice.html'),
        controller: AdminPracticeController,
        controllerAs: 'vm'
    });

AdminPracticeController.$inject = ['$timeout', '$interval', '$scope', '$stateParams', 'admin', '$filter', 'pageLoader', 'EditorConfig', 'utility', '$state', 'alertify'];

function AdminPracticeController($timeout, $interval, $scope, $stateParams, admin, $filter, pageLoader, EditorConfig, utility, $state, alertify) {
    // 

    var vm = this;

    EditorConfig.toolbarButtons = EditorConfig.toolbarButtonsMD = EditorConfig.toolbarButtonsSM = EditorConfig.toolbarButtonsXS = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertImage', 'insertVideo', 'insertTable', 'html'],
        EditorConfig.width = screen.availWidth / 2;
    EditorConfig.quickInsertButtons = []
    EditorConfig.pluginsEnabled = ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"]
    EditorConfig.events = {
        'froalaEditor.keyup': function() {
            $(".preview-div").each(function(v, ele) {
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, ele]);
            })
        }
    }

    vm.EditorConfig = EditorConfig;

    //////////////
    //Variables //
    //////////////
    vm.optionCount = 4;
    vm.form = {};
    vm.form.textExplanation = "";
    vm.form.questionTime = 1;
    vm.form.options = [];
    vm.questions  = [];
    // vm.form.textExplanation = "Enter Your Explanation Here";
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    vm.form.isMCQ = "true";
    // vm.form.questionNumber = 1;
    vm.topicDifficulty = [{
        id: 'easy',
        title: "Easy"
    }, {
        id: 'medium',
        title: "Medium"
    }, {
        id: 'hard',
        title: "Hard"
    }]


    vm.selectedSubjects = [];
    vm.selectedAuthors = [];
    vm.selectedPublications = [];
    vm.selectedBooks = [];
    vm.selectedExams = [];
    vm.selectedTopics = [];
    vm.selectedTopicGroup = [];
    vm.selectedTags = [];

    // Video Filter Data
    vm.selectedVideoSubjects = [];
    vm.selectedVideoAuthors = [];
    vm.selectedVideoPublications = [];
    vm.selectedVideoBooks = [];
    vm.selectedVideoExams = [];
    vm.selectedVideoTopics = [];
    vm.selectedVideoTags = [];


    /////////////
    //Function //
    /////////////
    vm.trustAsHTML = utility.trustAsHTML;
    vm.getNumber = getNumber;
    vm.addOption = addOption;
    vm.removeOption = removeOption;
    vm.toggleAns = toggleAns;
    vm.changeCourse = changeCourse;
    vm.changeModule = changeModule;
    vm.editQuestion = editQuestion;
    vm.removeQuestion = removeQuestion;
    vm.saveQuestion = saveQuestion;
    vm.pageChanged = pageChanged;
    // vm.newQuestion = newQuestion;
    vm.onFilterChange = onFilterChange;
    vm.getDifficultyLevels = getDifficultyLevels;
    vm.onVideoFilterChange = onVideoFilterChange;

    vm.styles = "<style>mo,mn,mi{font-size : 15px;}</style>";

    setTimeout(function() {
        // $('#questionDesc').froalaEditor(EditorConfig);
        // $('#textExplanation').froalaEditor(EditorConfig);
        // $('#opt_1').froalaEditor(EditorConfig);
    });


    vm.$onInit = function() {

        // $interval(function(){
        //     $(".preview-div").each(function(v,ele){
        //         if(vm.shouldShowQuestionModal){
        //             MathJax.Hub.Queue(["Typeset", MathJax.Hub, ele]);    
        //         }
        //     })
        // },500)

        $scope.$watch(function() {
            return vm.parent.courselistdata;
        }, function(v) {
            vm.courselist = v;
        });

        vm.questionToView = $stateParams.q_id;
        if (vm.questionToView) {
            getQuestionToView();
        }


        // autoSaveQuestion();

        admin.getAllSubject()
            .then(function(d) {
                vm.subjects = d.data;
            });
        admin.getAllAuthor()
            .then(function(d) {
                vm.authors = d.data;
            });
        admin.getAllPublication()
            .then(function(d) {
                vm.publications = d.data;
            });
        admin.getAllBook()
            .then(function(d) {
                vm.books = d.data;
            });
        admin.getAllExam()
            .then(function(d) {
                vm.exams = d.data;
            });
        admin.getAllTopic()
            .then(function(d) {
                vm.topics = d.data;
            });
        admin.getAllTopicGroups()
            .then(function(d) {
                vm.topicGroups = d.data;
            });
        admin.getAllTag()
            .then(function(d) {
                vm.tags = d.data;
            });

        // updateQuestionList();
        window.onbeforeunload = function (e) {
            e = e || window.event;
            if(isQuestionDataChanged()){
                // For IE and Firefox prior to version 4
                if (e) {
                    e.returnValue = 'Some data is unsaved, Are you Sure you want to close?';
                }

                // For Safari
                return 'Some data is unsaved, Are you Sure you want to close?';    
            }
        };
    }


    $scope.$on('$destroy', function() {
        if (vm.autoSaveInterval) {
            $interval.cancel(vm.autoSaveInterval);
        }
    });

    function getQuestionToView() {
        var query = {
            questionId: [vm.questionToView]
        }
        admin.findPracticeQuestionsByQuery(query)
            .then(function(res) {
                if (res.data && res.data[0]) {
                    var item = res.data[0];
                    vm.form.questionId = item.questionId;
                    vm.form.questionNumber = item.questionNumber;
                    vm.form.questionDifficulty = item.questionDifficulty || "";
                    vm.form.question = item.originalQuestion || item.question;
                    questionDescription(item.originalQuestion || item.question);

                    vm.form.questionOneLine = item.questionOneLine;
                    vm.form.questionTime = item.questionTime || 1;

                    vm.form.textExplanation = item.textExplanation;
                    textExplanation(item.textExplanation);

                    vm.form.videoURL = item.videoURL;
                    vm.form.videoDesc = item.videoDesc;
                    vm.selectedVideo = item.videoId;
                    vm.optionCount = item.options.length;

                    setTimeout(function() {
                        vm.form.options = item.options.map(function(v, i) {
                            var t = v.originalText || v.text;
                            v.originalText = v.text;
                            v.text = t;

                            if (item.ans.indexOf(Base64.encode(v.id)) > -1) {
                                v.ans = true;
                            }
                            return v;
                        });
                    });
                    vm.selectedSubjects = item.subjects || [];
                    vm.selectedAuthors = item.authors || [];
                    vm.selectedPublications = item.publications || [];
                    vm.selectedBooks = item.books || [];
                    vm.selectedExams = item.exams || [];
                    vm.selectedTopics = item.topics || [];
                    vm.selectedTopicGroup = item.topicGroup || [];
                    vm.selectedTags = item.tags || [];
                }
            })
            .catch(function(err) {
                console.error(err);
            })
    }


    function updateQuestionList(id) {
        pageLoader.show();
        admin.getAllPracticeQuestions()
            .then(function(d) {
                if (d.data.length > 0) {
                    vm.questions = d.data;

                    vm.questions.sort(function(a, b) {
                        return a.questionNumber - b.questionNumber;
                    });
                    vm.form.questionNumber = vm.questions[vm.questions.length - 1]['questionNumber'] + 1;
                    vm.form.questionId = vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber;

                    var base = ((vm.currentPage - 1) * vm.itemPerPage);
                    var last = (vm.currentPage * vm.itemPerPage) - 1;
                    vm.currentQuestions = vm.questions.slice(base, last + 1);
                } else {
                    //First Question
                    vm.form.questionNumber = 1;
                    vm.form.questionId = vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber;
                    vm.currentQuestions = [];
                }
            })
            .catch(function(err) {
                console.error(err);
            })
            .finally(function() {
                pageLoader.hide();
            });
    }

    function getNumber(num) {
        return Array.apply(null, {
            length: num
        });
    }

    function addOption() {
        vm.form.options = vm.form.options || [];
        vm.optionCount += 1;
    }

    vm.removeOptionConfirmation = function(index) {
        vm.resetAlertModal();
        vm.showRemoveOptionBtnInAlert = true;
        vm.alertMsg = "Are you sure you want to remove option?"
        vm.showAlertModal = true;
        vm.currentlyRemovingOption = "" + index;
    }

    function removeOption() {
        if (!vm.currentlyRemovingOption) {
            return;
        }
        vm.form.options.splice(parseInt(vm.currentlyRemovingOption), 1);
        vm.optionCount -= 1;
        if (vm.optionCount <= 0)
            vm.optionCount = 1;
        vm.resetAlertModal();
    }

    vm.resetAlertModal = function() {
        vm.showRemoveOptionBtnInAlert = false;
        vm.alertMsg = ""
        vm.showAlertModal = false;
        vm.currentlyRemovingOption = undefined;
        vm.showSaveQuestionBtnInAlert = false;
        vm.showNewQuestionBtnInAlert = false;
        vm.showSaveQuestionCloseModalBtnInAlert = false;
    }

    function toggleAns(index) {
        vm.form.options[index] = vm.form.options[index] || {};
        vm.form.options[index]['ans'] = !vm.form.options[index]['ans'];
    }

    function changeCourse() {
        vm.modulelist = {};
        vm.form.questionNumber = undefined;
        vm.form.questionId = undefined;
        vm.questions = [];
        vm.currentQuestions = [];
        pageLoader.show();
        admin.getCourseModules({
                'courseId': vm.form.course
            })
            .then(function(res) {
                vm.modulelist = res;
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function changeModule() {
        if (vm.form.module) {
            // updateQuestionList(vm.form.module);	
        }
    }

    // vm.previewQuestion = previewQuestion;

    vm.previewQuestion = function() {
        // var saveQueRes = vm.saveQuestion()
        // if(saveQueRes){
        //     saveQueRes.then(function(){
        //         console.log("in preview")
        //         var url = $state.href('practiceView', {
        //             topic: vm.selectedTopicGroup[0],
        //             question: vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber
        //         });
        //         window.open(url, 'previewQuestion');
        //     });
        // }
        vm.resetAlertModal();
        vm.error = undefined;
        vm.randomNumber = Math.floor(Math.random() * 90000) + 10000;
        var ansArray = [];

        if (!vm.form.question) {

            vm.showAlertModal = true;
            vm.alertMsg = "Plese enter question description."
            return;
        }

        vm.form.options = vm.form.options || [];
        var isAnyOptionEmpty = false;

        for (var i = 0; i < vm.optionCount; i++) {
            vm.form.options[i] = vm.form.options[i] || {};
            vm.form.options[i]['text'] = optionText(i);
            if (!vm.form.options[i]['text']) {
                isAnyOptionEmpty = true;
                break;
            }
        }

        vm.form.options = vm.form.options.map(function(v, index) {
            v.id = (index + 1) + "_" + vm.randomNumber;

            if (v.ans) {
                ansArray.push(v.id);
                // v.ans = undefined;
            }
            return v;
        });

        //Encrypt All Answers
        ansArray = ansArray.map(function(v) {
            return Base64.encode(v);
        })

        var ltRegEx = /</g;
        var gtRegEx = />/g;

        var data = {
                questionNumber: vm.form.questionNumber,
                questionId: vm.form.questionId || (vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber),
                question: questionDescription(),
                questionOneLine: vm.form.questionOneLine,
                questionTime: vm.form.questionTime || 1,
                options: vm.form.options,
                textExplanation: textExplanation(),
                videoId: vm.selectedVideo,
                ans: ansArray,
                subjects: vm.selectedSubjects,
                authors: vm.selectedAuthors,
                publications: vm.selectedPublications,
                books: vm.selectedBooks,
                exams: vm.selectedExams,
                topics: vm.selectedTopics,
                topicGroup: vm.selectedTopicGroup,
                tags: vm.selectedTags,
                questionDifficulty: vm.form.questionDifficulty,
                isNewQuestion: (vm.isFormInEditMode) ? false : true
            }
            // console.log(data);
        vm.isAutoSaving = true;
        return admin.savePreviewPracticeQuestion(data)
            .then(function() {
                vm.form.error = false;
                vm.isAutoSaving = false;
                var url = $state.href('practiceView', {
                    topic: vm.selectedTopicGroup[0],
                    question: vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber
                });
                window.open(url, 'previewQuestion');
            })
            .catch(function(err) {
                vm.form.error = true;
            })
            .finally(function() {})

    }

    function previewQuestion_old() {
        var question_length = $("<div></div>").html(vm.form.question).text().trim().length;
        if (vm.selectedTopicGroup[0] && question_length >= 10) {
            vm.randomNumber = Math.floor(Math.random() * 90000) + 10000;
            var ansArray = [];


            if(vm.form.isMCQ === "true"){
                vm.form.options = vm.form.options || [];

                for (var i = 0; i < vm.optionCount; i++) {
                    vm.form.options[i] = vm.form.options[i] || {};
                    vm.form.options[i]['text'] = optionText(i);
                }

                vm.form.options = vm.form.options.map(function(v, index) {
                    v.id = (index + 1) + "_" + vm.randomNumber;

                    if (v.ans) {
                        ansArray.push(v.id);
                        v.ans = undefined;
                    }
                    return v;
                });

                //Encrypt All Answers
                ansArray = ansArray.map(function(v) {
                    return Base64.encode(v);
                })
            }

            
            var data = {
                questionNumber: vm.form.questionNumber,
                questionId: vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber,
                question: questionDescription(),
                questionOneLine: vm.form.questionOneLine,
                questionTime: vm.form.questionTime || 1,
                options: vm.form.options || [],
                textExplanation: textExplanation(),
                videoId: vm.selectedVideo,
                ans: ansArray || [],
                subjects: vm.selectedSubjects,
                authors: vm.selectedAuthors,
                publications: vm.selectedPublications,
                books: vm.selectedBooks,
                exams: vm.selectedExams,
                topics: vm.selectedTopics,
                topicGroup: vm.selectedTopicGroup,
                tags: vm.selectedTags,
                questionDifficulty: vm.form.questionDifficulty
            }
            vm.isAutoSaving = true;
            admin.savePracticeQuestion(data)
                .then(function() {
                    vm.isAutoSaving = false;
                    var url = $state.href('practiceView', {
                        topic: vm.selectedTopicGroup[0],
                        question: vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber
                    });
                    window.open(url, 'previewQuestion');
                })
                .catch(function(err) {
                    vm.isAutoSaving = false;
                })
                .finally(function() {
                    vm.isAutoSaving = false;
                })
        } else {
            var url = $state.href('practiceView', {
                topic: vm.selectedTopicGroup[0]
            });
            window.open(url, 'previewQuestion');
        }
    }

    function isTwoObjectsOptionsSame(obj1, obj2) {
        if (obj1.length != obj2.length) {
            return false;
        }

        for (var i = 0; i < obj1.length; i++) {
            console.log("------------")
            console.log(obj1[i])
            console.log(obj2[i])
            console.log("------------")
            if ((obj1[i].text === obj2[i].originalText || obj1[i].text === obj2[i].text) &&
                obj1[i].ans === obj2[i].ans) {} else {
                return false;
            }
        }
        return true;
    }

    function isQuestionDataChanged() {
        var oldQuestion = $filter('filter')(vm.questions, {
            questionId: vm.form.questionId
        })[0];

        // oldQuestion = oldQuestion || [];
        // oldQuestion = oldQuestion[0]
        if (oldQuestion) {
            // console.log(vm.form.questionId === oldQuestion.questionId)
            // console.log(vm.form.questionDifficulty === oldQuestion.questionDifficulty)
            // console.log(vm.form.question === (oldQuestion.originalQuestion))
            // console.log(vm.form.questionTime === oldQuestion.questionTime)
            // console.log(vm.form.textExplanation === (oldQuestion.originalTextExplanation || oldQuestion.textExplanation))
            oldQuestion.options = oldQuestion.options.map(function(v, i) {
                if(v.id && oldQuestion.ans){
                    if (oldQuestion.ans && oldQuestion.ans.indexOf(Base64.encode(v.id)) > -1) {
                        v.ans = true;
                    }    
                }
                return v;
            });

            if (vm.form.questionId === oldQuestion.questionId &&
                vm.form.questionDifficulty === oldQuestion.questionDifficulty &&
                vm.form.question === (oldQuestion.originalQuestion) &&
                vm.form.questionTime === oldQuestion.questionTime &&
                vm.form.isMCQ === oldQuestion.isMCQ &&
                vm.form.textExplanation === (oldQuestion.originalTextExplanation) &&
                (oldQuestion.isMCQ === "true" ? isTwoObjectsOptionsSame(vm.form.options, oldQuestion.options) : true)) {
                console.log("everything same")
                return false;
            } else {
                console.log("some changes")
                return true
            }

        } else {
            console.log(vm.form.question.length > 2 || vm.form.textExplanation.length > 2)
            if (vm.form.question.length > 2 || vm.form.textExplanation.length > 2) {
                console.log("some changes")
                return true
            }
        }
    }

    vm.newQuestionConfirm = function() {
        vm.resetAlertModal();

        if (!isQuestionDataChanged()) {
            console.log("everything same")
                // vm.alertMsg = "Are you sure you want to start new question?";
            vm.directNewQuestion();
        } else {
            console.log("some changes")
            vm.alertMsg = "Some question data has been changed , you want to save that?";
            vm.showSaveQuestionBtnInAlert = true;
            vm.showNewQuestionBtnInAlert = true;
            vm.showAlertModal = true;
        }
    }

    vm.directNewQuestion = function() {
        vm.onFilterChange();
        vm.form.questionNumber = vm.questions[vm.questions.length - 1]['questionNumber'] + 1;
        vm.form.questionId = vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber;
        vm.form.question = "";
        vm.form.options = [];
        vm.form.textExplanation = "Enter Your Explanation Here";
        questionDescription(' ');
        textExplanation(' ');
        vm.isFormInEditMode = false;
        vm.optionCount = 4;
        vm.resetAlertModal();
    }

    vm.saveAndNewQuestion = function() {
        var saveQueRes = vm.saveQuestion();
        if (saveQueRes) {
            saveQueRes
                .then(function() {
                    vm.onFilterChange();
                    vm.form.question = "";
                    vm.form.options = [];
                    vm.form.textExplanation = "";
                    vm.isFormInEditMode = false;
                    vm.optionCount = 4;
                    vm.resetAlertModal();
                })
        }
    }

    vm.saveAndCloseQuestionModal = function() {
        var saveQueRes = vm.saveQuestion();
        if (saveQueRes) {
            saveQueRes
                .then(function() {
                    vm.onFilterChange();
                    vm.shouldShowQuestionModal = false;
                })
        }
    }

    vm.closeQuestionModalConfirm = function() {
        if(!isQuestionDataChanged()){
            vm.shouldShowQuestionModal = false;
            return;
        }
        
        vm.resetAlertModal();
        vm.alertMsg = "Some data is unsaved , do you want to save it?"
        vm.showSaveQuestionCloseModalBtnInAlert = true;
        vm.showAlertModal = true;      
    }

    vm.closeQuestionModal = function(){
        vm.shouldShowQuestionModal = false;
        vm.resetAlertModal();
    }

    vm.openModalForQuestion = function() {
        vm.resetAlertModal();
        var shouldOpenModal = vm.selectedSubjects.length >= 1 && vm.selectedAuthors.length >= 1 && vm.selectedPublications.length >= 1 && vm.selectedBooks.length >= 1 && vm.selectedExams.length >= 1 && vm.selectedTopics.length >= 1 && vm.selectedTopicGroup.length >= 1 && vm.selectedTags.length >= 1
        if (shouldOpenModal) {
            vm.shouldShowQuestionModal = true;
            vm.showAlertModal = false;
            $timeout(function() {
                $(".preview-div").each(function(v, ele) {
                    MathJax.Hub.Queue(["Typeset", MathJax.Hub, ele]);
                })
            }, 2000)
        } else {
            vm.shouldShowQuestionModal = false;
            vm.showAlertModal = true;
            vm.alertMsg = "Select atleast one value in all dropdown"
        }
    }

    function saveQuestion(fromUserClick) {
        vm.resetAlertModal();
        vm.error = undefined;
        vm.randomNumber = Math.floor(Math.random() * 90000) + 10000;
        var ansArray = [];

        if (!vm.form.question) {

            vm.showAlertModal = true;
            vm.alertMsg = "Plese enter question description."
            return;
        }
    
        

        if(vm.form.isMCQ === "true"){
            vm.form.options = vm.form.options || [];
            var isAnyOptionEmpty = false;
            for (var i = 0; i < vm.optionCount; i++) {
                vm.form.options[i] = vm.form.options[i] || {};
                vm.form.options[i]['text'] = optionText(i);
                if (!vm.form.options[i]['text']) {
                    isAnyOptionEmpty = true;
                    break;
                }
            }
            if (isAnyOptionEmpty) {
                vm.showAlertModal = true;
                vm.alertMsg = "Some Options are empty , please fill it."
                return;
            }
            vm.form.options = vm.form.options.map(function(v, index) {

                v.id = (index + 1) + "_" + vm.randomNumber;

                if (v.ans) {
                    ansArray.push(v.id);
                    // v.ans = undefined;
                }
                return v;
            });

            if (ansArray.length <= 0) {
                vm.submitted = true;
                vm.form.error = true;
                vm.error = "Plese select atleast one answer.";
                vm.showAlertModal = true;
                vm.alertMsg = "Plese select atleast one answer."
                return;
            }
            //Encrypt All Answers
            ansArray = ansArray.map(function(v) {
                return Base64.encode(v);
            })
        }

        var ltRegEx = /</g;
        var gtRegEx = />/g;

        var data = {
                questionNumber: vm.form.questionNumber,
                questionId: vm.form.questionId || (vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber),
                question: questionDescription(),
                questionOneLine: vm.form.questionOneLine,
                questionTime: vm.form.questionTime || 1,
                options: vm.form.options || [],
                textExplanation: textExplanation(),
                videoId: vm.selectedVideo,
                ans: ansArray || [],
                subjects: vm.selectedSubjects,
                authors: vm.selectedAuthors,
                publications: vm.selectedPublications,
                books: vm.selectedBooks,
                exams: vm.selectedExams,
                topics: vm.selectedTopics,
                topicGroup: vm.selectedTopicGroup,
                tags: vm.selectedTags,
                questionDifficulty: vm.form.questionDifficulty,
                isNewQuestion: (vm.isFormInEditMode) ? false : true,
                isMCQ : vm.form.isMCQ
            }
            // console.log(data);
        vm.isSavingQuestion = true;
        return admin.savePracticeQuestion(data)
            .then(function() {
                vm.form.error = false;
                vm.isFormInEditMode = false;
                
                if(fromUserClick){
                    var item_real = $filter('filter')(vm.questions, {
                        questionId: vm.form.questionId
                    })[0];
                    if(item_real){
                        item_real.originalQuestion = vm.form.question
                        item_real.originalTextExplanation = vm.form.textExplanation || ""
                        item_real.questionTime = vm.form.questionTime
                        item_real.questionDifficulty = vm.form.questionDifficulty
                        item_real.isMCQ = vm.form.isMCQ
                        item_real.options = vm.form.options.map(function(v,i){
                            var tmp = v.text
                            v.originalText = tmp
                            return v
                        })
                        item_real.ans = ansArray    
                    }else{
                        var newQue = {};
                        newQue.questionId = vm.form.questionId
                        newQue.originalQuestion = vm.form.question 
                        newQue.originalTextExplanation = vm.form.textExplanation || ""
                        newQue.questionTime = vm.form.questionTime
                        newQue.questionDifficulty = vm.form.questionDifficulty
                        newQue.isMCQ = vm.form.isMCQ
                        newQue.options = vm.form.options.map(function(v,i){
                            var tmp = v.text
                            v.originalText = tmp
                            return v
                        })
                        newQue.ans = ansArray  
                        vm.questions.push(newQue)
                    }
                }
                alertify.success("Question Saved successfully.");
            })
            .catch(function(err) {
                vm.form.error = true;
                vm.error = "Error in Submiting Data";
                alertify.error("Error in Submiting Data");
            })
            .finally(function() {
                vm.submitted = true;
                vm.isSavingQuestion = false;
            })

        admin.updateVerifyPracticeQuestion({
            q_id: vm.form.questionId || (vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber),
            test_status: "ready_to_test",
            q_state_start_dt: new Date()
        })
    }



    function editQuestion(id) {

        admin.getPracticeQuestionsById({
            questionsId: [id]
        })
        .then(function(questionData) {
            var item_real = $filter('filter')(vm.questions, {
                questionId: id
            })[0];
            var item = angular.copy(questionData.data[0]);
            var temp_copy = angular.copy(questionData.data[0]);

            item_real.questionId = temp_copy.questionId;
            item_real.questionNumber  = temp_copy.questionNumber;
            item_real.questionDifficulty  = temp_copy.questionDifficulty || "";
            item_real.originalQuestion  = temp_copy.originalQuestion || temp_copy.question;
            item_real.questionOneLine = temp_copy.questionOneLine;
            item_real.questionTime = temp_copy.questionTime || 1;
            item_real.originalTextExplanation = temp_copy.originalTextExplanation;
            item_real.videoURL = temp_copy.videoURL;
            item_real.videoDesc = temp_copy.videoDesc;
            item_real.isMCQ = temp_copy.isMCQ || "true"
            item_real.options = temp_copy.options;

            vm.form.questionId =  item.questionId;
            vm.form.questionNumber   = item.questionNumber;
            vm.form.questionDifficulty   = item.questionDifficulty || "";
            vm.form.question =  item.originalQuestion || item.question;
            questionDescription(item.originalQuestion || item.question);

            vm.form.questionOneLine  = item.questionOneLine;
            vm.form.questionTime  = item.questionTime || 1;
            
            vm.form.textExplanation = item.originalTextExplanation;
            textExplanation(item.originalTextExplanation);


            vm.form.videoURL = item.videoURL;
            vm.form.videoDesc = item.videoDesc;
            vm.selectedVideo = item.videoId;

            vm.form.isMCQ = item.isMCQ || "true"
            vm.optionCount = item.options.length;
            
            if(vm.form.isMCQ === "true"){
                vm.form.options = item.options.map(function(v, i) {
                    var t = v.originalText || v.text;
                    v.originalText = v.text;
                    v.text = t;

                    if (item.ans.indexOf(Base64.encode(v.id)) > -1) {
                        v.ans = true;
                    }
                    return v;
                });
            }else{
                vm.form.options = undefined
            }

            vm.isFormInEditMode = true;
            vm.shouldShowQuestionModal = true;
            $timeout(function() {
                $(".preview-div").each(function(v, ele) {
                    MathJax.Hub.Queue(["Typeset", MathJax.Hub, ele]);
                })
            }, 1000)

            vm.selectedSubjects = item.subjects || [];
            vm.selectedAuthors = item.authors || [];
            vm.selectedPublications = item.publications || [];
            vm.selectedBooks = item.books || [];
            vm.selectedExams = item.exams || [];
            vm.selectedTopics = item.topics || [];
            vm.selectedTopicGroup = item.topicGroup || [];
            vm.selectedTags = item.tags || [];
        })
        .catch(function(e) {
            vm.submitted = true;
            vm.form.error = true;
            vm.error = e;
        })
    }

    function removeQuestion(id) {

        var result = confirm("Want to delete?");
        if (!result) {
            //Declined
            return;
        }
        var item = $filter('filter')(vm.questions, {
            questionId: id
        })[0];
        admin.removePracticeQuestion({
                questionId: id
            })
            .then(function() {
                vm.submitted = true;
                vm.form.error = false;

                // updateQuestionList(vm.form.module);
                vm.onFilterChange();
            })
            .catch(function(e) {
                vm.submitted = true;
                vm.form.error = true;
                vm.error = e;
            })
    }

    function pageChanged() {
        console.log("Page Changed", vm.currentPage);
        var base = ((vm.currentPage - 1) * vm.itemPerPage);
        var last = (vm.currentPage * vm.itemPerPage) - 1;
        vm.currentQuestions = vm.questions.slice(base, last + 1);
    }


    function onFilterChange(item, model) {
        // if(vm.isFormInEditMode){
        //     return;
        // }

        if (vm.selectedSubjects.length > 0) {
            admin.getAllTopic({
                    subjectId: vm.selectedSubjects[0]
                })
                .then(function(d) {
                    vm.topics = d.data;
                });
        }
        var query = {
                subjects: vm.selectedSubjects,
                authors: vm.selectedAuthors,
                publications: vm.selectedPublications,
                books: vm.selectedBooks,
                exams: vm.selectedExams,
                topics: vm.selectedTopics,
                topicGroup: vm.selectedTopicGroup,
                tags: vm.selectedTags
            }
            // console.log(query);
        admin.findPracticeQuestionsIdByQuery(query)
            .then(function(d) {
                // console.log(d.data);
                if (d.data.length > 0) {
                    vm.questions = d.data;

                    vm.questions.sort(function(a, b) {
                        return a.questionNumber - b.questionNumber;
                    });
                    vm.form.questionNumber = vm.questions[vm.questions.length - 1]['questionNumber'] + 1;
                    vm.form.questionId = vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber;

                    var base = ((vm.currentPage - 1) * vm.itemPerPage);
                    var last = (vm.currentPage * vm.itemPerPage) - 1;
                    vm.currentQuestions = vm.questions.slice(base, last + 1);
                } else {
                    //First Question
                    vm.form.questionNumber = 1;
                    vm.form.questionId = vm.selectedTopicGroup[0] + "-" + vm.form.questionNumber;
                    vm.currentQuestions = [];
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    }

    function onVideoFilterChange(item, model) {
        if (vm.selectedSubjects.length > 0) {
            admin.getAllTopic({
                    subjectId: vm.selectedSubjects[0]
                })
                .then(function(d) {
                    vm.topics = d.data;
                });
        }
        var query = {
            subjects: vm.selectedVideoSubjects,
            authors: vm.selectedVideoAuthors,
            publications: vm.selectedVideoPublications,
            books: vm.selectedVideoBooks,
            exams: vm.selectedVideoExams,
            topics: vm.selectedVideoTopics,
            tags: vm.selectedVideoTags
        }
        admin.findVideoEntityByQuery(query)
            .then(function(data) {
                vm.filteredVideos = data.data;
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    //set or get
    function questionDescription(desc) {
        if (desc) {
            vm.form.question = desc
                // $('#questionDesc').froalaEditor('html.set', desc);
            return;
        }
        return vm.form.question; //$('#questionDesc').froalaEditor('html.get', true);
    }
    //set or get
    function textExplanation(desc) {
        if (desc) {
            vm.form.textExplanation = desc;
            // $('#textExplanation').froalaEditor('html.set', desc);
            return;
        }
        return vm.form.textExplanation; //$('#textExplanation').froalaEditor('html.get', true);
    }

    //set or get
    function optionText(id, desc) {
        if (desc) {
            // $('#opt_' + id).froalaEditor('html.set', desc);
            vm.form.options[id] = vm.form.options[id] || {};
            vm.form.options[id]['text'] = desc;
            return;
        }
        // return $('#opt_' + id).froalaEditor('html.get', true);
        return vm.form.options[id]['text'];
    }

    function getDifficultyLevels() {
        // if (vm.selectedTopics.length > 0) {
        //     var topic = $filter('filter')(vm.topics, {
        //         topicId: vm.selectedTopics[0]
        //     })[0];
        //     vm.topicDifficulty = topic.topicDifficulty;
        // }
        // vm.topicDifficulty = [{
        //     id: 'easy',
        //     title: "Easy"
        // }, {
        //     id: 'medium',
        //     title: "Medium"
        // }, {
        //     id: 'hard',
        //     title: "Hard"
        // }]
    }


}