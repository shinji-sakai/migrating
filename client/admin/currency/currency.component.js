angular.module(window.moduleName)
    .component('currencyPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./currency.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['$log','admin','$filter','$scope','$state'];
function Controller($log,admin,$filter,$scope,$state){
	var vm = this;
    vm.no_of_item_already_fetch=0;
	vm.saveCurrency = saveCurrency;
	vm.deleteCurrency = deleteCurrency;

	vm.$onInit = function(){
		//getAllCurrency();
	}

	function getAllCurrency(){
		admin.getAllCurrency()
			.then(function(res){
				vm.allCurrency = res.data
			})
	}
		vm.fetchCurrency = function(){
            vm.allCurrency = [];
			vm.isCurrencyLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : 0,
                collection : 'currency'
			})
			.then(function(res){
				$log.debug(res)
				vm.allCurrency = res.data;
				vm.no_of_item_already_fetch = 5;
                vm.last_update_dt = vm.allCurrency[vm.allCurrency.length - 1].update_dt
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isCurrencyLoading = false;
			})
		}

		
		vm.fetchMoreAuthors = function(){
			vm.isCurrencyLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : vm.no_of_item_already_fetch,
                collection : 'currency',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.allCurrency, {currency_id: v.currency_id})[0];
						if(!item){
							vm.allCurrency.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
                    vm.last_update_dt = vm.allCurrency[vm.allCurrency.length - 1].update_dt
					
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isAuthorsLoading = false;
			})
		}

	function saveCurrency(){
		
            if(!vm.formIsInEditMode){
                vm.form.create_dt = new Date();
                vm.form.update_dt = new Date();
            }else{
                vm.form.update_dt = new Date();
            }
		admin.addCurrency(vm.form)
			.then(function(res){
				// getAllCurrency();
				 vm.fetchCurrency();
				vm.form = {};
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function deleteCurrency(id){
		admin.deleteCurrency({currency_id : id})
			.then(function(res){
				var item = $filter('filter')(vm.allCurrency || [], {currency_id: id})[0];								
					 if (vm.allCurrency.indexOf(item) > -1) {
                        var pos = vm.allCurrency.indexOf(item);
                       vm.allCurrency.splice(pos, 1);
                    }
				//getAllCurrency();
			})
			.catch(function(err){
				console.log(err);
			})
	}
}