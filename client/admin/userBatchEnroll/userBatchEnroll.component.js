require('service.admin');
require('service.profile');
require('service.utility')
angular.module(window.moduleName)
    .component('userBatchEnrollPage', {
        template: require('./userBatchEnroll.html'),
        controller: UserEmailGroupController,
        controllerAs: 'vm'
    })

UserEmailGroupController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin','profile','utility'];

function UserEmailGroupController($log,$filter, $scope, $state, EditorConfig, $timeout,admin,profile,utility) {
    var vm = this;

    vm.selectedTraining = [];
    vm.selectedBatch = [];
    vm.selectedUsers = [];

    vm.onBatchChange = onBatchChange;
    vm.onUserChange = onUserChange;
    vm.addUserBatchEnroll = addUserBatchEnroll;
    vm.removeUserBatchEnroll = removeUserBatchEnroll;
    
    vm.$onInit = function() {
        getAllTiming();
        getAllUsers();
    }

    function getAllUsers(){
        profile.getAllUsersShortDetails()
            .then(function(d){
                console.log(d.data);
                vm.allUsers = d.data;
            })
            .catch(function(err){
                console.log(err);
            })
    }

    function getAllTiming(){
        admin.getAllBatchTiming()
            .then(function(res){
                vm.allBatches = res.data;
            })
            .catch(function(err){
                console.error(err);
            })
    }

    function onBatchChange(){
        vm.form = vm.form || {};
        vm.form.enroll_bat = vm.selectedBatch[0] || "";
    }

    function onUserChange(){
        vm.form = vm.form || {};
        vm.form.usr_id = vm.selectedUsers[0] || "";
        if(vm.form.usr_id){
            getUserEnrollAllBatch(vm.form.usr_id);    
        }else{
            vm.userEnrollAllBatch = []
        }
    }

    function getUserEnrollAllBatch(usr_id) {
        admin.getUserEnrollAllBatch({usr_id : usr_id})
            .then(function(res){
                vm.userEnrollAllBatch = res.data;
                // $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function addUserBatchEnroll(){
        vm.formSuccess = undefined;
        vm.form.enroll_status = true;

        var batch_item  = $filter('filter')(vm.allBatches,{bat_id : vm.form.enroll_bat})[0];
        var emailData = {
            training : batch_item.crs_id,
            cls_start_dt : $filter('date')(batch_item.cls_start_dt,'dd-MMM-yyyy'),
            cls_frm_tm : utility.convertTimeTo_AM_PM(batch_item.cls_frm_tm),
            frm_wk_dy : batch_item.frm_wk_dy,
            to_wk_dy : batch_item.to_wk_dy
        }

        var data_to_send = Object.assign({},vm.form,{emailData : emailData});

        admin.addUserBatchEnroll(data_to_send)
            .then(function(res){
                var res_dtls = res.data;
                var found = false;
                vm.userEnrollAllBatch = vm.userEnrollAllBatch.map(function(v,i) {
                    if(v.enroll_bat === res_dtls.enroll_bat){
                        found = true;
                        return res_dtls
                    }
                    return v;
                })
                if(!found){
                    vm.userEnrollAllBatch.push(res_dtls);
                }

                vm.selectedUsers = [];
                vm.selectedBatch = [];
                vm.form = {};
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function removeUserBatchEnroll(enroll_bat){
        var result = confirm("Are you sure you want to unenrolled?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        vm.form.enroll_status = false;
        admin.removeUserBatchEnroll({enroll_bat : enroll_bat,usr_id:vm.form.usr_id})
            .then(function(res){
                var res_dtls = res.data;
                vm.userEnrollAllBatch = vm.userEnrollAllBatch.map(function(v,i) {
                    if(v.enroll_bat === res_dtls.enroll_bat){
                        return res_dtls
                    }
                    return v;
                })
                vm.selectedBatch = [];
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}