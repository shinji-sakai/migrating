require('service.forums');
angular.module(window.moduleName)
    .component('forumsCategoryPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./forumsCategory.html'),
        controller: ForumsCategoryController,
        controllerAs: 'vm'
    });

ForumsCategoryController.$inject = ['forums', 'pageLoader', '$filter', '$scope'];

function ForumsCategoryController(forums, pageLoader, $filter, $scope) {
    var vm = this;

    vm.form = {};
    vm.buttonName = "Save";
    
    vm.$onInit = function() {
        updateData();
    }

    vm.saveCategory = saveCategory;
    vm.updateCategory = updateCategory;
    vm.deleteCategory = deleteCategory;
    vm.resetForm = resetForm;

    function saveCategory() {
        var catData = {
            cat_id : vm.form.cat_id,
            cat_name: vm.form.cat_name,
            cat_desc: vm.form.cat_desc
        };
        vm.buttonName = "Uploading....";
        forums.saveCategory(catData)
            .then(function success(data) {
                vm.buttonName = "Save";
                vm.submitted = true;
                vm.form.error = false;

                vm.form = {};
                updateData();
            })
            .catch(function error(err) {
                vm.buttonName = "Save";
                vm.submitted = true;
                vm.form.error = true;
                vm.form = {};
            });
    }
    
    function deleteCategory(id) {
        var result = confirm("Want to delete?");
        if (!result) {
            //Declined
            return;
        }
        forums.deleteCategory({cat_id : id})
            .then(function(res) {
                updateData();
            });
    }


    function updateCategory(id) {
        var item = $filter('filter')(vm.categories, {
            cat_id: id
        })[0];

        if (item) {
            vm.form = item;
        }
    }

    function updateData() {
        forums.getAllCategories()
            .then(function(d) {
                vm.categories = d.data;
            })
            .catch(function err(err) {
                vm.form.error = true;
            })
    }
    function resetForm(){
        vm.form = {};
    }
}