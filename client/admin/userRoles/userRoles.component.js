require('service.admin');
require('service.profile')
require('./_userRoles.scss');
angular.module(window.moduleName)
    .component('userRolesPage', {
        template: require('./userRoles.html'),
        controller: UserRolesController,
        controllerAs: 'vm'
    })

UserRolesController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin','profile'];

function UserRolesController($log,$filter, $scope, $state, EditorConfig, $timeout,admin,profile) {
    var vm = this;

    vm.selectedUsers = [];
    vm.selectedForms = [];
    vm.form = {};
    vm.form.roles = [];
    vm.form_access = {};

    vm.addUserRole = addUserRole;
    vm.editUserRole = editUserRole;
    vm.deleteUserRole = deleteUserRole;
    vm.onUserChange = onUserChange;
    vm.addRoleBtnClick = addRoleBtnClick;
    vm.addRoleToArray = addRoleToArray;
    vm.onFormChange = onFormChange;
    vm.getStringOfForms = getStringOfForms;
    vm.editRoleInArray = editRoleInArray;
    vm.deleteRoleFromArray = deleteRoleFromArray;
    
    vm.$onInit = function() {
        getAllUsers();
        getAllRoles();
        getAllForms();
        getAllUserRoles();

    }

    function getAllUserRoles(){
        admin.getAllUserRoles()
            .then(function(res){
                vm.allUserRoles = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllUsers(){
        profile.getAllUsersShortDetails()
            .then(function(d){
                vm.allUsers = d.data;
            })
            .catch(function(err){
                console.log(err);
            })
    }

    function getAllRoles(){
        admin.getAllRoles()
            .then(function(res){
                vm.allRoles = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllForms(){
        admin.getAllForms()
            .then(function(res){
                vm.allForms = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function onUserChange(){
        vm.form = vm.form || {};
        vm.form.usr_id = vm.selectedUsers[0] || "";
        editUserRole(vm.form.usr_id);
    }

    function onFormChange(){
        vm.form_access = {};
        vm.selectedForms.map(function(v,i){
            vm.form_access[v] = "yes";
        })
        console.log(vm.form_access)
    }

    function addUserRole(){
        vm.formSuccess = undefined;
        admin.addUserRole(vm.form)
            .then(function(res){

                var usr_id = res.data.usr_id;
                var group = $filter("filter")(vm.allUserRoles,{usr_id : usr_id})[0];
                if(group){
                    var pos = vm.allUserRoles.indexOf(group);
                    vm.allUserRoles.splice(pos,1);    
                }

                vm.allUserRoles.push(res.data);
                vm.formSuccess = true;
                vm.form = {};
                vm.form.roles = [];
                vm.form_access = {};
                vm.selectedForms = [];
                vm.selectedUsers = [];
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function editUserRole(usr_id){
        if(!usr_id || usr_id.length <= 0){
            vm.form = {};
            vm.form.roles = [];
            vm.selectedUsers = [];
            return;
        }
        vm.formSuccess = undefined;
        var group = $filter("filter")(vm.allUserRoles,{usr_id : usr_id});
        if(group && group[0]){
            vm.form = angular.copy(group[0]);
            vm.selectedUsers = [group[0]["usr_id"]];
        }
    }

    function deleteUserRole(usr_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteUserRole({usr_id : usr_id})
            .then(function(res){
                var usr_id = res.data.usr_id;
                var group = $filter("filter")(vm.allUserRoles,{usr_id : usr_id})[0];
                var pos = vm.allUserRoles.indexOf(group);
                vm.allUserRoles.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function addRoleBtnClick(){
        vm.addingRoleIsInProgress = true;
    }

    function addRoleToArray(){
        vm.form = vm.form || {};
        vm.form.roles = vm.form.roles || {};
        vm.form.roles.push({
            role_id : vm.role_id,
            form_access : vm.form_access
        }) 


        vm.role_id='';
        vm.selectedForms=[];
        vm.form_access={};
        vm.addingRoleIsInProgress = false;
    }

    function editRoleInArray(index){
        var role = vm.form.roles[index];
        vm.role_id=role.role_id;
        vm.selectedForms = Object.keys(role.form_access || {});
        vm.form_access=role.form_access;
        vm.addingRoleIsInProgress = true;
        vm.form.roles.splice(index,1);
    }

    function deleteRoleFromArray(index) {
        vm.form.roles.splice(index,1);
    }

    function getStringOfForms(obj){
        var allKeys = Object.keys(obj);
        return allKeys.join(" , ");
    }
}