require('angular-textbox-io');
require('service.admin');
angular.module(window.moduleName)
    .component('emailTemplatesPage', {
        template: require('./emailTemplates.html'),
        controller: EmailTemplatesController,
        controllerAs: 'vm'
    })

EmailTemplatesController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin'];

function EmailTemplatesController($log,$filter, $scope, $state, EditorConfig, $timeout,admin) {
    var vm = this;

    vm.selectedTraining = [];
    vm.selectedBatch = [];

    vm.onTrainingChange = onTrainingChange;
    vm.onBatchChange = onBatchChange;
    vm.addEmailTemplate = addEmailTemplate;
    vm.editEmailTemplate = editEmailTemplate;
    vm.deleteEmailTemplate = deleteEmailTemplate;
    
    vm.$onInit = function() {
        getAllEmailTemplates();
        getAllBatchTraining();
    }

    function getAllEmailTemplates(){
        admin.getAllEmailTemplates()
            .then(function(res){
                vm.allTemplates = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllBatchTraining() {
        admin.getAllBatchCourse()
            .then(function(res){
                vm.allTrainings = res.data;
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function onTrainingChange(){
        vm.form = vm.form || {};
        vm.form.training_id = vm.selectedTraining[0] || "";
        getAllTimingByTrainingId();
    }

    function onBatchChange(){
        vm.form = vm.form || {};
        vm.form.bat_id = vm.selectedBatch[0] || "";
    }

    function  getAllTimingByTrainingId(id) {
        admin.getTimingByCourseId({crs_id : id || vm.selectedTraining[0]})
            .then(function(res){
                vm.allBatches = res.data;
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function addEmailTemplate(){
        vm.formSuccess = undefined;
        admin.addEmailTemplates(vm.form)
            .then(function(res){

                var pk_id = res.data.pk_id;
                var group = $filter("filter")(vm.allTemplates,{pk_id : pk_id})[0];
                if(group){
                    var pos = vm.allTemplates.indexOf(group);
                    vm.allTemplates.splice(pos,1);    
                }

                
                vm.allTemplates.push(res.data);
                vm.formSuccess = true;
                vm.form = {};
                vm.form.email_body = "<p>&nbsp;</p>";
                vm.selectedTraining = [];
                vm.selectedBatch = [];
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function editEmailTemplate(pk_id){
        vm.formSuccess = undefined;
        var template = $filter("filter")(vm.allTemplates,{pk_id : pk_id});
        if(template && template[0]){
            vm.form = template[0];
            vm.selectedTraining = [vm.form.training_id]
            vm.selectedBatch = [vm.form.bat_id]
            getAllTimingByTrainingId(vm.form.training_id);
        }
    }

    function deleteEmailTemplate(pk_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteEmailTemplate({pk_id : pk_id})
            .then(function(res){
                var pk_id = res.data.pk_id;
                var template = $filter("filter")(vm.allTemplates,{pk_id : pk_id})[0];
                var pos = vm.allTemplates.indexOf(template);
                vm.allTemplates.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}