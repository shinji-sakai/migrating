angular.module(window.moduleName)
    .component('allLinksPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./allLinks.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['$log','admin','$filter','$scope','$state'];
function Controller($log,admin,$filter,$scope,$state){
	var vm = this;
	vm.form = vm.form || {};
    vm.no_of_item_already_fetch = 0;
	vm.form.link_show_modal = "no";
	vm.form.link_login_required="no";
	vm.form.link_modal_type = "all-links"

	vm.saveLink = saveLink;
	vm.editLink = editLink;
	vm.deleteLink = deleteLink;

	vm.$onInit = function(){
	//	getAllLinks();
		getAllLinksCategory();
	}

	function getAllLinksCategory(){
			admin.getAllLinksCategories()
				.then(function(res){
					vm.allLinksCategory = res.data
				})
		}

	function getAllLinks(){
		admin.getAllLinks()
			.then(function(res){
				vm.allLinks = res.data
			})
	}
	vm.fetchAllLinks = function(){
			vm.isLinksLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
			collection : 'allLinks'
			})
			.then(function(res){
				$log.debug(res)
				vm.allLinks= res.data;
				vm.no_of_item_already_fetch = 5;
				vm.last_update_dt = vm.allLinks[vm.allLinks.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isLinksLoading = false;
			})
		}
		
		vm.fetchMoreLinks = function(){
			vm.isLinksLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
			collection : 'allLinks',
				  update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.allLinks, {link_id: v.link_id})[0];
						if(!item){
							vm.allLinks.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					vm.last_update_dt = vm.allLinks[vm.allLinks.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isLinksLoading = false;
			})
		}

	function saveLink(){
       	if(!vm.formIsInEditMode){
                vm.form.create_dt = new Date();
                vm.form.update_dt = new Date();
            }else{
                vm.form.update_dt = new Date();
            }
		admin.addLinkToAllLinks(vm.form)
			.then(function(res){
	           	vm.fetchAllLinks();
				//getAllLinks();
				vm.form = {};
				vm.formIsInEditMode=false;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function editLink(id){
		var link = $filter('filter')(vm.allLinks,{link_id : id})[0];
		delete link._id;
		vm.form = link;
		vm.formIsInEditMode=true;
	}

	function deleteLink(id){
		admin.deleteLinkFromAllLinks({link_id : id})
			.then(function(res){
					var item = $filter('filter')(vm.allLinks || [], {link_id: id})[0];								
					 if (vm.allLinks.indexOf(item) > -1) {
                        var pos = vm.allLinks.indexOf(item);
                       vm.allLinks.splice(pos, 1);
                    }
				//getAllLinks();
			})
			.catch(function(err){
				console.log(err);
			})
	}
}