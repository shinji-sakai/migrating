require('service.admin');
angular.module(window.moduleName)
    .component('rolesPage', {
        template: require('./roles.html'),
        controller: RolesController,
        controllerAs: 'vm'
    })

RolesController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin'];

function RolesController($log,$filter, $scope, $state, EditorConfig, $timeout,admin) {
    var vm = this;


    vm.addRole = addRole;
    vm.editRole = editRole;
    vm.deleteRole = deleteRole;
    
    vm.$onInit = function() {
        getAllRoles();
    }

    function getAllRoles(){
        admin.getAllRoles()
            .then(function(res){
                vm.allRoles = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function addRole(){
        vm.formSuccess = undefined;
        admin.addRole(vm.form)
            .then(function(res){

                var role_id = res.data.role_id;
                var group = $filter("filter")(vm.allRoles,{role_id : role_id})[0];
                if(group){
                    var pos = vm.allRoles.indexOf(group);
                    vm.allRoles.splice(pos,1);    
                }

                vm.allRoles.push(res.data);
                vm.formSuccess = true;
                vm.form = {};
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function editRole(role_id){
        vm.formSuccess = undefined;
        var group = $filter("filter")(vm.allRoles,{role_id : role_id});
        if(group && group[0]){
            vm.form = group[0];
        }
    }

    function deleteRole(role_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteRole({role_id : role_id})
            .then(function(res){
                var role_id = res.data.role_id;
                var group = $filter("filter")(vm.allRoles,{role_id : role_id})[0];
                var pos = vm.allRoles.indexOf(group);
                vm.allRoles.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}