	angular.module(window.moduleName)
        .component('topicPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./topic.html'),
            controller: AdminTopicController,
            controllerAs: 'vm'
        });

	AdminTopicController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminTopicController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;

        vm.topics = [];
       vm.no_of_item_already_fetch = 0;
    	vm.updateTopic = updateTopic;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removeTopic = removeTopic;
    	vm.editTopic = editTopic;
        vm.changeSubject = changeSubject;
        vm.form = vm.form || {};
        vm.form.topicDifficulty = vm.form.topicDifficulty || []; 

        vm.difficultyLevel = [{
            id : 'easy',
            title : "Easy"
        },{
            id : 'medium',
            title : "Medium"
        },{
            id : 'hard',
            title : "Hard"
        }]

    	vm.$onInit = function() {
            admin.getAllSubject()
                .then(function(d){
                    vm.subjectList = d.data;
                });
        	// updateTopicsList();
	    };
          function checkValidityOfID() {
            vm.form.topicId = vm.form.topicId || "";
            vm.form.topicId = vm.form.topicId.replace(/\s/g , "-");
			admin.isIdExist({
				collection :'topic',
				key:"topicId",
				id_value:vm.form.topicId			
			}).then(function(data)
			{				
              console.log(data.data);
			  if(data.data.idExist)
			  {
				    vm.idExists = true;
			  }
			  else
			  {
				   vm.idExists = false;
			  }
			}).catch(function(err){
             console.log(err);
			})
        }

	    function updateTopicsList(){
	    	pageLoader.show();
            console.log(vm.subjectId);
	    	admin.getAllTopic({subjectId : vm.subjectId})
        		.then(function(data){
        			if(data.data.length > 0){
        				vm.topics = data.data;
                        vm.topics.sort(function(a,b){
                            return a.topicNumber - b.topicNumber;
                        });
                        vm.form = vm.form || {};
                        vm.form.topicNumber = vm.topics[vm.topics.length - 1]['topicNumber'] + 1;
                    }
                    else{
                        vm.topics = [];
                        vm.form = vm.form || {};
                        vm.form.topicNumber = 1;
                    }
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }

    	function updateTopic(){
    		var data = {
    			topicId : vm.form.topicId,
                topicNumber : vm.form.topicNumber,
    			topicName : vm.form.topicName,
    			topicDesc : vm.form.topicDesc,
                subjectId : vm.subjectId,
                topicDifficulty : vm.form.topicDifficulty
    		}
				if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }
    		pageLoader.show();
    		admin.updateTopic(data)
    			.then(function(res){
					var item = $filter('filter')(vm.topics || [], {topicId: vm.form.topicId})[0];
					// if(item){
					// 		item.topicId = vm.form.topicId,
					// 		item.topicNumber = vm.form.topicNumber,
					// 		item.topicName = vm.form.topicName,
					// 		item.topicDesc = vm.form.topicDesc,
					// 		item.subjectId = vm.subjectId,
					// 		item.topicDifficulty = vm.form.topicDifficulty
					// }else{
					// 	vm.topics = [data].concat(vm.topics || [])
					// }
					vm.fetchTopics();
    				vm.submitted = true;
    				vm.form.error = false;
    				vm.form.topicName = "";
    				vm.form.topicDesc = "";
                    vm.form.topicId = "";
                    vm.form.topicNumber += 1;
                    vm.form.topicDifficulty = [];
					vm.formIsInEditMode=false;
    				//updateTopicsList();
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}

    	function resetFormDetail(){
    		vm.submitted = false;
    		vm.form = {};
            vm.form.topicDifficulty = [];
            if(vm.topics.length > 0){
                vm.form.topicNumber = vm.topics[vm.topics.length - 1]['topicNumber'] + 1;
            }
    	}

    	function removeTopic(id){
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
    		admin.removeTopic({topicId:id})
    			.then(function(d){
    				console.log(d);
					var item = $filter('filter')(vm.topics || [], {topicId: id})[0];								
					 if (vm.topics.indexOf(item) > -1) {
                        var pos = vm.topics.indexOf(item);
                       vm.topics.splice(pos, 1);
                    }
    				//updateTopicsList();
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}

    	function editTopic(id){

    		var obj = $filter('filter')(vm.topics,{topicId:id})[0];
    		vm.form = {};
    		vm.form.topicId = obj.topicId;
    		vm.form.topicName = obj.topicName;
    		vm.form.topicDesc = obj.topicDesc;
            vm.form.topicNumber = obj.topicNumber;
            vm.form.topicDifficulty = obj.topicDifficulty;
			vm.formIsInEditMode=true;
    	}
		vm.fetchTopics = function(){
			vm.isTopicsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
			collection : 'topic'
			})
			.then(function(res){
				$log.debug(res)
				  vm.topics = res.data;
				vm.no_of_item_already_fetch = 5;
				vm.last_update_dt = vm.topics[vm.topics.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTopicsLoading = false;
			})
		}
		vm.fetchMoreTopics = function(){
			vm.isTopicsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				   collection : 'topic',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 0){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.topics, {topicId: v.topicId})[0];
						if(!item){
							  vm.topics.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					vm.last_update_dt = vm.topics[vm.topics.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTopicsLoading = false;
			})
		}
        function changeSubject(){
			//   if(vm.topics.length > 0){
			//  vm.form.topicNumber = vm.topics[vm.topics.length - 1]['topicNumber'] + 1;
			//   }
            //updateTopicsList();
        }
    }