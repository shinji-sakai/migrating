
	angular.module(window.moduleName)
        .component('authorPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./author.html'),
            controller: AdminAuthorController,
            controllerAs: 'vm'
        });

	AdminAuthorController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminAuthorController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;
        vm.authors = [];
        vm.no_of_item_already_fetch=0;
        vm.checkValidityOfAuthorID = checkValidityOfAuthorID;
    	vm.updateAuthor = updateAuthor;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removeAuthor = removeAuthor;
    	vm.editAuthor = editAuthor;

    	vm.$onInit = function() {
            //update author list on page load
        	//updateAuthorsList();
	    };

		vm.fetchAuthors = function(){
            vm.authors = [];
			vm.isAuthorsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : 0,
                collection : 'author'
			})
			.then(function(res){
				$log.debug(res)
				vm.authors = res.data;
				vm.no_of_item_already_fetch = 5;
                vm.last_update_dt = vm.authors[vm.authors.length - 1].update_dt
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isAuthorsLoading = false;
			})
		}
   
		vm.fetchMoreAuthors = function(){
			vm.isAuthorsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : vm.no_of_item_already_fetch,
                collection : 'author',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.authors, {authorId: v.authorId})[0];
						if(!item){
							vm.authors.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
                    vm.last_update_dt = vm.authors[vm.authors.length - 1].update_dt
					
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isAuthorsLoading = false;
			})
		}
        function checkValidityOfAuthorID() {
			vm.form.authorId = vm.form.authorId || "";
            vm.form.authorId = vm.form.authorId.replace(/\s/g , "-");
			admin.isIdExist({
				collection :'author',
				key:"authorId",
				id_value:vm.form.authorId			
			}).then(function(data)
			{				
              console.log(data.data);
			  if(data.data.idExist)
			  {
				    vm.authorIdExists = true;
			  }
			  else
			  {
				   vm.authorIdExists = false;
			  }
			}).catch(function(err){
             console.log(err);
			})
			
			
           
        }

        //update author list
	    function updateAuthorsList(){
            //show page laoder
	    	pageLoader.show();
            //get all author from db
	    	admin.getAllAuthor()
        		.then(function(data){
        			if(data.data.length > 0)
        				vm.authors = data.data;
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }

        //update author to db
    	function updateAuthor(){

    		var data = {
    			authorId : vm.form.authorId,
    			authorName : vm.form.authorName,
    			authorDesc : vm.form.authorDesc 
    		}

            if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }

            //show page laoder
    		pageLoader.show();
            //update authpr to db
    		admin.updateAuthor(data)
    			.then(function(res){

                    vm.fetchAuthors();
    				vm.submitted = true;
    				vm.form.error = false;
                    //resest form value to defaults
    				vm.form.authorName = "";
    				vm.form.authorDesc = "";
                    vm.form.authorId = "";
                    vm.formIsInEditMode = false;
    				//updateAuthorsList();
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}

    	function resetFormDetail(){
    		vm.submitted = false;
            vm.authorIdExists = false;
            vm.formIsInEditMode = false;
    		vm.form = {};
    	}

        //remove author
    	function removeAuthor(id){
            //ask for confirmation
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
            //remove author api to remove from db
    		admin.removeAuthor({authorId:id})
    			.then(function(d){
    				var item = $filter('filter')(vm.authors || [], {authorId: id})[0];								
					 if (vm.authors.indexOf(item) > -1) {
                        var pos = vm.authors.indexOf(item);
                       vm.authors.splice(pos, 1);
                    }					  
    			//	updateAuthorsList();
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}

        //this function will be called when user click on edit icon
    	function editAuthor(id){
            //find author using author id
    		var author = $filter('filter')(vm.authors,{authorId:id})[0];
    		vm.form = {};
            //set form value to edited author
    		vm.form.authorId = author.authorId;
    		vm.form.authorName = author.authorName;
    		vm.form.authorDesc = author.authorDesc;
            vm.formIsInEditMode = true;
    	}
    }