require('service.admin');
require('angular-textbox-io');
angular.module(window.moduleName)
    .component('blogSideLinksArticleTextboxIo', {
        require: {
            parent : '^adminPage'
        },
        template: require('./sideLinks.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin', '$filter', '$scope', '$state', 'EditorConfig', '$timeout'];

function Controller(admin, $filter, $scope, $state, EditorConfig, $timeout) {
    var vm = this;

    vm.EditorConfig = EditorConfig;
    EditorConfig.heightMin = 500;
    EditorConfig.imageDefaultWidth = 1024;

    vm.savePage = savePage;
    vm.editPage = editPage;
    vm.deletePage = deletePage;
    vm.onIdFieldChange = onIdFieldChange;

    vm.$onInit = function() {
        vm.parent.hideSidebar = true;
        getAllPages();
        getAllSubgroup();
    }

    function getAllSubgroup() {
        admin.getAllSubgroup()
            .then(function(res) {
                vm.allSubgroup = res.data
            })
    }

    function getAllPages() {
        admin.getAllSideLinksArticlePage()
            .then(function(res) {
                vm.allPages = res.data
            })
    }

    function onIdFieldChange() {
        vm.form.page_id = vm.form.page_id.replace(" ", "");
    }

    function savePage() {
    	textboxio.getActiveEditor().content.uploadImages(function(res){
    		console.log(res);
    		var isSuccess = true;
    		for (var i = 0; i < res.length; i++) {
    			if(res[i].success === false){
    				isSuccess = false;
    				break;
    			}
    		}
    		if(isSuccess){
    			$timeout(function(){
    				admin.addSideLinksArticlePage(vm.form)
			            .then(function(res) {
			                getAllPages();
			                vm.form = {};
			                $timeout(function() {
			                    vm.form.page_content = "<p>&nbsp;</p>";
			                })
			            })
			            .catch(function(err) {
			                console.log(err);
			            })			
    			},1000);
    		}
    	})
        
    }

    function editPage(id) {
        var page = $filter('filter')(vm.allPages, {
            page_id: id
        })[0];
        delete page._id;
        vm.form = page;
    }

    function deletePage(id) {
        admin.deleteSideLinksArticlePage({
                page_id: id
            })
            .then(function(res) {
                getAllPages();
            })
            .catch(function(err) {
                console.log(err);
            })
    }
}