require('service.sms');
angular.module(window.moduleName)
    .component('bulkSmsPage', {
        template: require('./bulkSMS.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['$log','$filter', '$scope', '$state', '$timeout','SMS'];

function Controller($log,$filter, $scope, $state, $timeout,SMS) {
    var vm = this;

    vm.sendBulkSMS = sendBulkSMS;
    
    vm.$onInit = function() {
     
    }
    function sendBulkSMS() {
    	SMS.sendSMS(vm.form)
            .then(function(res){
                $log.debug(res);
                vm.response = res.data;
                vm.form = {};
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}