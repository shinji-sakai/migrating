	angular.module(window.moduleName)
        .component('bookPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./book.html'),
            controller: AdminBookController,
            controllerAs: 'vm'
        });

	AdminBookController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminBookController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;

        vm.books = [];
       	vm.no_of_item_already_fetch = 0;
        vm.checkValidityOfID = checkValidityOfID;
    	vm.updateBook = updateBook;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removeBook = removeBook;
    	vm.editBook = editBook;

    	vm.$onInit = function() {
            //update books list
        	//updateBooksList();
	    };

        function checkValidityOfID() {
            vm.form.bookId = vm.form.bookId || "";
            vm.form.bookId = vm.form.bookId.replace(/\s/g , "-");
				admin.isIdExist({
				collection :'book',
				key:"bookId",
				id_value:vm.form.bookId			
			}).then(function(data)
			{				
              console.log(data.data);
			  if(data.data.idExist)
			  {
				    vm.idExists = true;
			  }
			  else
			  {
				   vm.idExists = false;
			  }
			}).catch(function(err){
             console.log(err);
			})
			
            
        }

        //get books from db
        //update books array to display at local
	    function updateBooksList(){
	    	pageLoader.show();
            //get all books from db
	    	admin.getAllBook()
        		.then(function(data){
        			if(data.data.length > 0)
        				vm.books = data.data;
                    else
                        vm.books = [];
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }
	vm.fetchBooks = function(){
			vm.isBooksLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				  collection : 'book'
			})
			.then(function(res){
				$log.debug(res)
				vm.books= res.data;
				vm.no_of_item_already_fetch = 5;
				vm.last_update_dt = vm.books[vm.books.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isBooksLoading = false;
			})
		}

		vm.fetchMoreBooks = function(){
			vm.isBooksLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				  collection : 'book',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.books, {bookId: v.bookId})[0];
						if(!item){
							vm.books.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					   vm.last_update_dt = vm.books[vm.books.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isBooksLoading = false;
			})
		}
        //update/insert book to db
    	function updateBook(){
    		var data = {
    			bookId : vm.form.bookId,
    			bookName : vm.form.bookName,
    			bookDesc : vm.form.bookDesc 
    		}
			 if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }

    		pageLoader.show();
            //update book object to server
            //if books is not there the it will insert new 
            //else it will update
    		admin.updateBook(data)
    			.then(function(res){
					// 	var item = $filter('filter')(vm.books || [], {bookId: vm.form.bookId})[0];
					// if(item){
					// 	item.bookId = vm.form.bookId,
					// 	item.bookName = vm.form.bookName,
					// 	item.bookDesc = vm.form.bookDesc 
					// }else{
					// 	vm.books = [data].concat(vm.books || [])
					// }
					vm.fetchBooks();
    				vm.submitted = true;
    				vm.form.error = false;
                    //reset from fields
    				vm.form.bookName = "";
    				vm.form.bookDesc = "";
                    vm.form.bookId = "";
                    vm.idExists = false;
                    vm.formIsInEditMode = false;
                    //updates books array locally
    			//	updateBooksList();
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}

    	function resetFormDetail(){
    		vm.submitted = false;
            vm.idExists = false;
            vm.formIsInEditMode = false;
    		vm.form = {};
    	}


        //this function will call api and remove book from db
    	function removeBook(id){
            //ask for confirmation
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
            //service to remove book from server
    		admin.removeBook({bookId:id})
    			.then(function(d){
    				//update array locally
					var item = $filter('filter')(vm.books || [], {bookId: id})[0];								
					 if (vm.books.indexOf(item) > -1) {
                        var pos = vm.books.indexOf(item);
                       vm.books.splice(pos, 1);
                    }
    				//updateBooksList();
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}

        //this function will be called when user will click on press button of book 
    	function editBook(id){
            //fetch book from book array
    		var book = $filter('filter')(vm.books,{bookId:id})[0];
    		vm.form = {};
            //update form field from table
    		vm.form.bookId = book.bookId;
    		vm.form.bookName = book.bookName;
    		vm.form.bookDesc = book.bookDesc;
            vm.formIsInEditMode = true;
    	}
    }