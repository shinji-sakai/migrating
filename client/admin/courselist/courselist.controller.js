// (function() {
//     angular
//         .module(window.moduleName)
//         .controller('AdminCourseListController', AdminCourseListController);

//     AdminCourseListController.inject = ['courselist','admin','$filter','courselistdata']


//     function AdminCourseListController(courselist,admin,$filter,courselistdata) {

//         var vm = this;

//         // console.log(courselist);
//         vm.courseLevel = "_";
//         vm.courselist=courselist;
//         vm.removeCourse=removeCourse;

//         vm.courselistdata=courselistdata;
//         vm.buttonName = "Save";
//         vm.submitted = false;


//         vm.updateFormWithValues = updateFormWithValues;

//         // console.log(vm.courseLevel);

//         vm.addCourse = function(){

//             vm.buttonName = "Uploading ...";
//             var courseListData= {
//                 courseId:vm.courseId,
//                 author : vm.author,
//                 courseName:vm.courseName,
//                 courseLongDesc: vm.courseLongDesc,
//                 courseShortDesc:vm.courseShortDesc,
//                 courseLevel:vm.courseLevel,
//                 courseLevelDesc:vm.courseLevelDesc,
//                 imageUrl : vm.imageUrl,
//                 "CommentsReviews" : vm.comments,
//                 "LastUpdated" : $filter('date')(vm.lastUpdated,"yyyy/MM/dd")
//             };

//             // console.log(courseListData);

//              admin.addCourse(courseListData)
//                 .then(function success(){
//                     vm.submitted = true;
//                     vm.error = false;
//                     vm.buttonName = "Save";

//                     vm.couse = "";
//                     vm.courseName = "";
//                     vm.author = "";
//                     vm.courseLevel = "";
//                     vm.courseLevelDesc = "";
//                     vm.courseLongDesc = "";
//                     vm.courseShortDesc = "";
//                     vm.imageUrl = "";
//                     vm.comments = "";
//                     vm.lastUpdated = "";

//                     updateData();
//                 })
//                 .catch(function err(err){
//                     vm.submitted = true;
//                     vm.error = true;
//                     vm.buttonName = "Save";
//                 });



//         }

//         vm.changeCourse= function(){
//             vm.updateFormWithValues(vm.course);
//         }

//         function updateFormWithValues(id){

//             vm.courseId = id;
//             vm.course = id;

//             // console.log(id);

//             // console.log(vm.course,vm.courselistdata);
//             var item = $filter('filter')(vm.courselistdata, {courseId: vm.course})[0];
//             var coursemaster = $filter('filter')(vm.courselist, {courseId: vm.course})[0];
            
//             if(item){
//                 vm.courseName = item.courseName;
//                 vm.author = coursemaster.author;
//                 vm.courseLevel = item.courseLevel;
//                 vm.courseLevelDesc = item.courseLevelDesc;
//                 vm.courseLongDesc = item.courseLongDesc;
//                 vm.courseShortDesc = item.courseShortDesc;
//                 vm.imageUrl = item.imageUrl;
//                 vm.comments = item["CommentsReviews"];
//                 vm.lastUpdated = new Date(item["LastUpdated"]);

//                 // console.log(item);
//                 vm.buttonName = "Update";
//             }else{

//                 var item = $filter('filter')(vm.courselist, {courseId: vm.course})[0];
//                 // console.log(item);
//                 if(item){
//                     vm.courseName = item.courseName;
//                     vm.author = item.author;
//                 }else{
//                     vm.courseName = "";
//                     vm.author = "";
//                 }
                
//                 vm.courseLevel = "";
//                 vm.courseLevelDesc = "";
//                 vm.courseLongDesc = "";
//                 vm.courseShortDesc = "";
//                 vm.imageUrl = "";
//                 vm.comments = "";
//                 vm.lastUpdated = new Date();
//                 vm.buttonName = "Save";

//             }
//         }




//         function removeCourse(id) {

//             var deleteCourseData= {
//                 courseId:id,
//             };


//             admin.removeCourse(deleteCourseData)
//                 .then(function(res){
//                     updateData();
//                 });

//         }

//         function  updateData(){
//             admin.courselistfull()
//                 .then(function(data){
//                     vm.courselistdata = data;
//                 })
//                 .catch(function err(err){

//                 });
//         }
// // 

//     }
//     //comment
// })();