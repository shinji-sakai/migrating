angular.module(window.moduleName)
    .component('courselistPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./courselist.html'),
        controller: AdminCourseListController,
        controllerAs: 'vm'
    })
AdminCourseListController.$inject = ['admin', '$filter','$scope','EditorConfig'];
function AdminCourseListController(admin, $filter,$scope,EditorConfig) {

    var vm = this;

    vm.$onInit = function() {
        //observe parent(admin master page) coursemaster
    	$scope.$watch(function(){return vm.parent.coursemaster;},function(v){
    		vm.courselist = v;	
    		// console.log(v);
    	});
        //observe parent(admin master page) courselist data
    	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
    		vm.courselistdata = v;
    	});
    };

    vm.courseLevel = "_";
    vm.buttonName = "Save";
    vm.submitted = false;


    vm.updateFormWithValues = updateFormWithValues;
    vm.removeCourse = removeCourse;
    //initialize froala editor in short description
    $('#shortdesc').froalaEditor(EditorConfig);
    //initialize froala editor in long description
    $('#longdesc').froalaEditor(EditorConfig);

    //this method will be called when user click add/update button
    vm.addCourse = function() {

        vm.buttonName = "Uploading ...";
        var courseListData = {
            courseId: vm.courseId,
            author: vm.author,
            courseName: vm.courseName,
            courseLongDesc: $('#longdesc').froalaEditor('html.get',true),
            courseShortDesc: $('#shortdesc').froalaEditor('html.get',true),
            courseLevel: vm.courseLevel,
            courseLevelDesc: vm.courseLevelDesc,
            courseType: vm.courseType,
            imageUrl: vm.imageUrl,
            courseSubGroup : vm.courseSubGroup
        };

        //add course to database
        admin.addCourse(courseListData)
            .then(function success() {
                vm.submitted = true;
                vm.error = false;
                vm.buttonName = "Save";

                vm.couse = "";
                vm.courseName = "";
                vm.author = "";
                vm.courseLevel = "";
                vm.courseLevelDesc = "";
                vm.courseLongDesc = "";
                vm.courseShortDesc = "";
                vm.courseType = '';
                vm.imageUrl = "";
                vm.comments = "";
                // vm.lastUpdated = "";
                $('#shortdesc').froalaEditor('html.set','');
                $('#longdesc').froalaEditor('html.set','');
                updateData();
            })
            .catch(function err(err) {
                vm.submitted = true;
                vm.error = true;
                vm.buttonName = "Save";
            });
    }

    vm.changeCourse = function() {
        vm.updateFormWithValues(vm.course);
    }

    function updateFormWithValues(id) {

        vm.courseId = id;
        vm.course = id;
        var item = $filter('filter')(vm.courselistdata, {
            courseId: vm.course
        })[0];
        var coursemaster = $filter('filter')(vm.courselist, {
            courseId: vm.course
        })[0];



        if (item) {
            vm.courseType = coursemaster.courseType;
            vm.courseSubGroup = coursemaster.courseSubGroup;
            vm.courseName = coursemaster.courseName;
            vm.author = coursemaster.author;
            vm.courseLevel = item.courseLevel;
            vm.courseLevelDesc = item.courseLevelDesc;
            $('#shortdesc').froalaEditor('html.set', item.courseShortDesc);
            $('#longdesc').froalaEditor('html.set', item.courseLongDesc);
            vm.imageUrl = item.imageUrl;
            // vm.comments = item["CommentsReviews"];
            // vm.lastUpdated = new Date(item["LastUpdated"]);
            vm.buttonName = "Update";
        } else {
            var item = $filter('filter')(vm.courselist, {
                courseId: vm.course
            })[0];
            if (item) {
                vm.courseName = coursemaster.courseName;
                vm.courseType = coursemaster.courseType;
                vm.author = item.author;
            } else {
                vm.courseName = "";
                vm.author = "";
            }
            vm.courseSubGroup = coursemaster.courseSubGroup;
            vm.courseLevel = "";
            vm.courseLevelDesc = "";
            vm.courseLongDesc = "";
            vm.courseShortDesc = "";
            vm.imageUrl = "";
            vm.comments = "";
            // vm.lastUpdated = new Date();
            vm.buttonName = "Save";
        }

    }

    function removeCourse(id) {
        var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
        var deleteCourseData = {
            courseId: id,
        };


        admin.removeCourse(deleteCourseData)
            .then(function(res) {
                updateData();
            });

    }

    function updateData() {
        admin.courselistfull()
            .then(function(data) {
                vm.courselistdata = data;
            })
            .catch(function err(err) {

            });
    }


}