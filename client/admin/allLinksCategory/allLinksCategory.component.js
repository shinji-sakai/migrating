angular.module(window.moduleName)
    .component('allLinksCategoryPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./allLinksCategory.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['$log','admin','$filter','$scope','$state'];
function Controller($log,admin,$filter,$scope,$state){
	var vm = this;
    vm.no_of_item_already_fetch = 0;
	vm.saveCategory = saveCategory;
	vm.deleteCategory = deleteCategory;
	vm.editCategory = editCategory;

	vm.$onInit = function(){
		//getAllCategories();
	}
    	vm.fetchCategories = function(){
			vm.isCategoriesLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'allLinksCategory'
			})
			.then(function(res){
				$log.debug(res)
				vm.allCategories= res.data;
				vm.no_of_item_already_fetch = 5;
				vm.last_update_dt = vm.allCategories[vm.allCategories.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isCategoriesLoading = false;
			})
		}

		vm.fetchMoreCategories = function(){
			vm.isCategoriesLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'allLinksCategory',
				  update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						$log.log('v.cat_id'+v.cat_id);
						var item = $filter('filter')(vm.allCategories, {cat_id: v.cat_id})[0];
						if(!item){
							vm.allCategories.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					vm.last_update_dt = vm.allCategories[vm.allCategories.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isCategoriesLoading = false;
			})
		}

	function getAllCategories(){
		admin.getAllLinksCategories()
			.then(function(res){
				vm.allCategories = res.data
			})
	}

	function saveCategory(){
			if(!vm.formIsInEditMode){
                vm.form.create_dt = new Date();
                vm.form.update_dt = new Date();
            }else{
                vm.form.update_dt = new Date();
            }
		admin.addAllLinksCategory(vm.form)
			.then(function(res){
				debugger;
				//getAllCategories();
			  vm.fetchCategories();
					
				vm.form = {};
				vm.formIsInEditMode=false;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function deleteCategory(id){
		admin.deleteAllLinksCategory({cat_id : id})
			.then(function(res){
				//getAllCategories();
				var item = $filter('filter')(vm.allCategories || [], {cat_id: id})[0];								
					 if (vm.allCategories.indexOf(item) > -1) {
                        var pos = vm.allCategories.indexOf(item);
                       vm.allCategories.splice(pos, 1);
                    }
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function editCategory(id){
		var cat = $filter('filter')(vm.allCategories,{cat_id : id})[0];
		vm.form = cat;
		vm.formIsInEditMode=true;
	}
}