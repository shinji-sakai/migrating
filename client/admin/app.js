require('jquery');
require('bootstrap_js');
require('bootstrap_css');
require('theme_css');
require('fontawesome');
require('prism_css');
require('angular-ui-select/dist/select.min.css');

require('prism_js');

// require('tinymce-dist/tinymce.min.js');
require('tinymce/tinymce.js');
require('angular');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-local-storage');
require('oclazyload');
require('angular-jwt');
require('ng-file-upload');
require('angular-sanitize');
require('angular-ui-tinymce/dist/tinymce.min.js');
require('angular-ui-select/dist/select.min.js');
require('angular-animate');
require('serviceModule');
require('directiveModule');
require('admin_scss');

require("angularjs-datepicker");
require("angularjs-datepicker/dist/angular-datepicker.min.css");



require('angular-froala');

var Stats = require('stats_js');

    angular.module(window.moduleName, [
        'ui.router',
        'ui.bootstrap',
        "oc.lazyLoad",
        'ngFileUpload',
        'ngSanitize',
        'ui.tinymce',
        'ui.select',
        'ngAnimate',
        'froala','720kb.datepicker',
        'LocalStorageModule',
        "angular-jwt",
        "service"
    ])
    .constant("EditorConfig",{
            toolbarButtons : ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html'],
            quickInsertButtons : ['image'],
            heightMin: 200,
            toolbarSticky: false,
            fontSizeDefaultSelection: '18',
            fontSizeSelection: true,
            placeholderText: 'Type Here',
            imageUploadParam: 'upload',
            imageUploadURL: '/ckeditor/save',
            imageUploadMethod: 'POST',
            imageMaxSize: 5 * 1024 * 1024,
            imageAllowedTypes: ['jpeg', 'jpg', 'png'],
            htmlAllowedTags : ['a', 'abbr', 'address', 'area', 'article', 'aside', 'audio', 'b', 'base', 'bdi', 'bdo', 'blockquote', 'br', 'button', 'canvas', 'caption', 'cite', 'code', 'col', 'colgroup', 'datalist', 'dd', 'del', 'details', 'dfn', 'dialog', 'div', 'dl', 'dt', 'em', 'embed', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'header', 'hgroup', 'hr', 'i', 'iframe', 'img', 'input', 'ins', 'kbd', 'keygen', 'label', 'legend', 'li', 'link', 'main', 'map', 'mark', 'menu', 'menuitem', 'meter', 'nav', 'noscript', 'object', 'ol', 'optgroup', 'option', 'output', 'p', 'param', 'pre', 'progress', 'queue', 'rp', 'rt', 'ruby', 's', 'samp', 'script', 'style', 'section', 'select', 'small', 'source', 'span', 'strike', 'strong', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'textarea', 'tfoot', 'th', 'thead', 'time', 'title', 'tr', 'track', 'u', 'ul', 'var', 'video', 'wbr','ew-img','side-links',"maction","math","menclose","merror","mfenced","mfrac","mglyph","mi","mlabeledtr","mmultiscripts","mn","mo","mover","mpadded","mphantom","mroot","mrow","ms","mspace","msqrt","mstyle","msub","msubsup","msup","mtable","mtd","mtext","mtr","munder","munderover","semantics"],
            htmlAllowedEmptyTags : ['textarea', 'a', 'iframe', 'object', 'video', 'style', 'script', '.fa' , 'ew-img',"maction","math","menclose","merror","mfenced","mfrac","mglyph","mi","mlabeledtr","mmultiscripts","mn","mo","mover","mpadded","mphantom","mroot","mrow","ms","mspace","msqrt","mstyle","msub","msubsup","msup","mtable","mtd","mtext","mtr","munder","munderover","semantics"],
            htmlAllowedAttrs : ['accept', 'accept-charset', 'accesskey', 'action', 'align', 'alt', 'async', 'autocomplete', 'autofocus', 'autoplay', 'autosave', 'background', 'bgcolor', 'border', 'charset', 'cellpadding', 'cellspacing', 'checked', 'cite', 'class', 'color', 'cols', 'colspan', 'content', 'contenteditable', 'contextmenu', 'controls', 'coords', 'data', 'data-.*', 'datetime', 'default', 'defer', 'dir', 'dirname', 'disabled', 'download', 'draggable', 'dropzone', 'enctype', 'for', 'form', 'formaction', 'headers', 'height', 'hidden', 'high', 'href', 'hreflang', 'http-equiv', 'icon', 'id', 'ismap', 'itemprop', 'keytype', 'kind', 'label', 'lang', 'language', 'list', 'loop', 'low', 'max', 'maxlength', 'media', 'method', 'min', 'multiple', 'name', 'novalidate', 'open', 'optimum', 'pattern', 'ping', 'placeholder', 'poster', 'preload', 'pubdate', 'radiogroup', 'readonly', 'rel', 'required', 'reversed', 'rows', 'rowspan', 'sandbox', 'scope', 'scoped', 'scrolling', 'seamless', 'selected', 'shape', 'size', 'sizes', 'span', 'src', 'srcdoc', 'srclang', 'srcset', 'start', 'step', 'summary', 'spellcheck', 'style', 'tabindex', 'target', 'title', 'type', 'translate', 'usemap', 'value', 'valign', 'width', 'wrap','side-links','ew-article','ew-section',"open","close","separators","scriptminsize","mathsize","displaystyle","stretchy","columnalign"]
        })
    .run(['$rootScope',function($rootScope) {
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
			new Stats();
	  	});
	  	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            setTimeout(function(){
                $('body, html').animate({ scrollTop: 0 }, 800);
            },10);
        });
	}])
        .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider','$ocLazyLoadProvider',function($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider,$ocLazyLoadProvider) {
  		$ocLazyLoadProvider.config({
		  debug: false
		});
            $stateProvider
                .state('admin',{
					abstract:true,
					url : "",
					template : "<admin-page></admin-page>",
					resolve : {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.AdminComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.coursemaster',{
					url : "/coursemaster",
					template: "<course-master-page></course-master-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.CourseMasterComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.courselist',{
					url : "/courselist",
					template : "<courselist-page></courselist-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.CourselistComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.modules',{
					url : "/modules",
					template: "<modules-page></modules-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.ModulesComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.moduleItems',{
					url : "/moduleItems",
					template : "<module-items-page></module-items-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./moduleItems/moduleItems.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.itemtransfer',{
					url : "/itemtransfer",
					template : "<itemtransfer-page></itemtransfer-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.ItemTransfer'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.demovideos',{
					url : "/demovideos",
					template : "<demo-videos-page></demo-videos-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.DemoVideosComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.slides',{
					url : "/slides",
					template: "<slides-page></slides-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.SlidesComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.practice',{
					url : "/practice/:q_id?",
					template: "<practice-page></practice-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.PracticeComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('practiceView',{
					url : "/:topic/questions/:question?",
					template: "<practice-view-page></practice-view-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./practice-view/practice.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					},
					params : {
						questionData : null
					}
				})
				.state('admin.videoEntity',{
					url : "/videoEntity",
					template : "<video-entity></video-entity>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.VideoEntityComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]
					}
				})
				.state('admin.bookTopic',{
					url : "/bookTopic",
					template : "<book-topic-page></book-topic-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./booktopic/bookTopic.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]
					}
				})
				.state('admin.author',{
					url : "/author",
					template : "<author-page></author-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.AuthorComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.publication',{
					url : "/publication",
					template : "<publication-page></publication-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.PublicationComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.purchasedCourses',{
					url : "/purchasedCourses",
					template : "<purchased-courses></purchased-courses>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
							var defer = $q.defer();
							$ocLazyLoad.toggleWatch(true);
							require(['./purchasedCourses/purchasedCourses.component.js'],function(){
								$ocLazyLoad.inject();
								$ocLazyLoad.toggleWatch(false);
								defer.resolve();
							});
							return defer.promise;
						}]
					}
				})
				.state('admin.book',{
					url : "/book",
					template : "<book-page></book-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.BookComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.subject',{
					url : "/subject",
					template : "<subject-page></subject-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.SubjectComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.exam',{
					url : "/exam",
					template : "<exam-page></exam-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.ExamComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.topic',{
					url : "/topic",
					template : "<topic-page></topic-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.TopicComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.tags',{
					url : "/tags",
					template : "<tags-page></tags-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.TagsComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.subtopic',{
					url : "/subtopic",
					template : "<subtopic-page></subtopic-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.SubtopicComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.questionAnalytics',{
					url : "/questionAnalytics",
					template : "<question-analytics-page></question-analytics-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.QuestionAnalyticsComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.forumsCategory',{
					url : "/forumsCategory",
					template : "<forums-category-page></forums-category-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['admin.ForumsCategoryComponent'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.courseBundle',{
					url : "/coursebundle",
					template : "<course-bundle-page></course-bundle-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./courseBundle/courseBundle.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.userCourses',{
					url : "/userCourses",
					template : "<user-courses-page></user-courses-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./userCourses/userCourses.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.empTypes',{
					url : "/empTypes",
					template : "<emp-types-page></emp-types-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./empTypes/empTypes.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.empDetails',{
					url : "/empDetails",
					template : "<emp-details-page></emp-details-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./empDetails/empDetails.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.batchCourse',{
					url : "/batchCourse",
					template : "<batch-course-page></batch-course-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./batchCourse/batchCourse.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.batchTimings',{
					url : "/batchTimings",
					template : "<batch-timings-page></batch-timings-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./batchTimings/batchTiming.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.skills',{
					url : "/skills",
					template : "<skills-page></skills-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./skills/skills.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.currency',{
					url : "/currency",
					template : "<currency-page></currency-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./currency/currency.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.gettingStarted',{
					url : "/gettingStarted",
					template : "<getting-started-page></getting-started-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./gettingStarted/gettingStarted.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.sessions',{
					url : "/sessions",
					template : "<sessions-page></sessions-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./sessions/sessions.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.sessionDetails',{
					url : "/sessionDetails",
					template : "<session-details-page></session-details-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./sessionDetails/sessionDetails.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.pricelist',{
					url : "/pricelist",
					template : "<pricelist-page></pricelist-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./pricelist/pricelist.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.bundle',{
					url : "/bundle",
					template : "<bundle-page></bundle-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./bundle/bundle.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.courseSubGroup',{
					url : "/courseSubGroup",
					template : "<course-subgroup-page></course-subgroup-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./courseSubGroup/courseSubGroup.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.blogSideLinksArticle',{
					url : "/blogSideLinksArticle",
					template : "<blog-side-links-article></blog-side-links-article>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./blogSideLinkArticle/sideLinks.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.blogSideLinksArticleTextboxIo',{
					url : "/blogSideLinksArticleTextboxIo",
					template : "<blog-side-links-article-textbox-io></blog-side-links-article-textbox-io>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./blogSideLinkArticle_textbox_io/sideLinks.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					},
					reloadOnSearch : false
				})
				.state('admin.allLinksCategory',{
					url : "/allLinksCategory",
					template : "<all-links-category-page></all-links-category-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./allLinksCategory/allLinksCategory.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.allLinks',{
					url : "/allLinks",
					template : "<all-links-page></all-links-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./allLinks/allLinks.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.sendBulkMails',{
					url : "/sendBulkMails",
					template : "<send-bulk-mails></send-bulk-mails>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./sendBulkMails/sendBulkMails.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.bulkSMS',{
					url : "/bulkSMS",
					template : "<bulk-sms-page></bulk-sms-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./bulkSMS/bulkSMS.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.emailTemplates',{
					url : "/emailTemplates",
					template : "<email-templates-page></email-templates-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./emailTemplates/emailTemplates.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.userEmailGroup',{
					url : "/userEmailGroup",
					template : "<user-email-group-page></user-email-group-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./userEmailGroup/userEmailGroup.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.sendEmailTemplate',{
					url : "/sendEmailTemplate",
					template : "<send-email-template-page></send-email-template-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./sendEmailTemplate/sendEmailTemplate.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.sendNotification',{
					url : "/sendNotification",
					template : "<send-notification-page></send-notification-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./sendNotification/sendNotification.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.roles',{
					url : "/roles",
					template : "<roles-page></roles-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./roles/roles.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.forms',{
					url : "/forms",
					template : "<forms-page></forms-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./forms/forms.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.userRoles',{
					url : "/userRoles",
					template : "<user-roles-page></user-roles-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./userRoles/userRoles.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.batchTimingsDashboard',{
					url : "/batchTimingsDashboard",
					template : "<batch-timings-dashboard-page></batch-timings-dashboard-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./batchTimingsDashboard/batchTimingsDashboard.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.helpForm',{
					url : "/helpForm/:form",
					template : "<help-form-page></help-form-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./helpForm/helpForm.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					},
					params : {
						form : {
						    value: null,
						    squash: true
						}
					},
				})
				.state('admin.userBatchEnroll',{
					url : "/userBatchEnroll",
					template : "<user-batch-enroll-page></user-batch-enroll-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./userBatchEnroll/userBatchEnroll.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.verifyPracticeQuestion',{
					url : "/verifyPracticeQuestionPage",
					template : "<verify-practice-question-page></verify-practice-question-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./verifyPracticeQuestion/verifyPracticeQuestion.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.jobOpenings',{
					url : "/jobOpenings",
					template : "<job-openings-page></job-openings-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./jobOpenings/jobOpenings.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.topicGroup',{
					url : "/topicGroup",
					template : "<topic-group-page></topic-group-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./topicGroup/topicGroup.component.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.boardCompetitiveCourses',{
					url : "/boardCompetitiveCourses",
					template : "<board-competitive-courses-page></board-competitive-courses-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./boardCompetitiveCourses/boardCompetitiveCourses.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})
				.state('admin.previousPapers',{
					url : "/previousPapers",
					template : "<previous-papers-page></previous-papers-page>",
					resolve :  {
						loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
	                        var defer = $q.defer();
	                        $ocLazyLoad.toggleWatch(true);
	                        require(['./previousPapers/previousPapers.js'],function(){
	                            $ocLazyLoad.inject();
	                            $ocLazyLoad.toggleWatch(false);
	                            defer.resolve();
	                        });
	                        return defer.promise;
	                    }]	
					}
				})


            $urlRouterProvider.otherwise("/coursemaster");
            $locationProvider.html5Mode({
	            enabled: true,
	            requireBase: false,
	            rewriteLinks: false
	        });
        }]);