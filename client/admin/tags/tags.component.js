	angular.module(window.moduleName)
        .component('tagsPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./tags.html'),
            controller: AdminTagController,
            controllerAs: 'vm'
        });

	AdminTagController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminTagController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;

        vm.checkValidityOfID = checkValidityOfID;
    	vm.updateTag = updateTag;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removeTag = removeTag;
    	vm.editTag = editTag;
        vm.no_of_item_already_fetch = 0;

    	vm.$onInit = function() {
        	//updateTagsList();
	    };

        function checkValidityOfID() {
			debugger;
            vm.form.tagId = vm.form.tagId || "";
            vm.form.tagId = vm.form.tagId.replace(/\s/g , "-");
			admin.isIdExist({
				collection :'tag',
				key:"tagId",
				id_value:vm.form.tagId 			
			}).then(function(data)
			{				
              console.log(data.data);
			  if(data.data.idExist)
			  {
				    vm.idExists = true;
			  }
			  else
			  {
				   vm.idExists = false;
			  }
			}).catch(function(err){
             console.log(err);
			})
			
		
             }

	    function updateTagsList(){
	    	pageLoader.show();
	    	admin.getAllTag()
        		.then(function(data){
        			if(data.data.length > 0)
        				vm.tags = data.data;
                    else
                        vm.tags = [];
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }

    	function updateTag(){
    		var data = {
    			tagId : vm.form.tagId,
    			tagName : vm.form.tagName,
    			tagDesc : vm.form.tagDesc 
    		}
			if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }
    		pageLoader.show();
    		admin.updateTag(data)
    			.then(function(res){
					// 	var item = $filter('filter')(vm.tags || [], {tagId: vm.form.tagId})[0];
					// if(item){
					// 		item.tagId = vm.form.tagId,
					// 		item.tagName = vm.form.tagName,
					// 		item.tagDesc = vm.form.tagDesc 
					// }else{
					// 	vm.tags = [data].concat(vm.tags || [])
					// }
					 vm.fetchTags();
    				vm.submitted = true;
    				vm.form.error = false;
    				vm.form.tagName = "";
    				vm.form.tagDesc = "";
                    vm.form.tagId = "";
                    vm.idExists = false;
                    vm.formIsInEditMode = false;
    				//updateTagsList();
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}

    	function resetFormDetail(){
    		vm.submitted = false;
            vm.idExists = false;
            vm.formIsInEditMode = false;
    		vm.form = {};
    	}

    	function removeTag(id){
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
    		admin.removeTag({tagId:id})
    			.then(function(d){
    				console.log(d);
					var item = $filter('filter')(vm.tags || [], {tagId: id})[0];								
					 if (vm.tags.indexOf(item) > -1) {
                        var pos = vm.tags.indexOf(item);
                       vm.tags.splice(pos, 1);
                    }
    				//updateTagsList();
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}

    	function editTag(id){
    		var obj = $filter('filter')(vm.tags,{tagId:id})[0];
    		vm.form = {};
    		vm.form.tagId = obj.tagId;
    		vm.form.tagName = obj.tagName;
    		vm.form.tagDesc = obj.tagDesc;
            vm.formIsInEditMode = true;
    	}

		vm.fetchTags = function(){
			vm.isTagsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				//no_of_item_to_skip : 0
				collection : 'tag'
			})
			.then(function(res){
				$log.debug(res)
				vm.tags = res.data;
				vm.no_of_item_already_fetch = 5;
				   vm.last_update_dt = vm.tags[vm.tags.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTagsLoading = false;
			})
		}

		vm.fetchMoreTags = function(){
			vm.isTagsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
			//	no_of_item_to_skip : vm.no_of_item_already_fetch
				collection : 'tag',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.tags,{tagId: v.tagId})[0];
						if(!item){
							vm.tags.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					  vm.last_update_dt = vm.tags[vm.tags.length - 1].update_dt;
					// vm.coursemasterdata = vm.coursemasterdata.concat(res.data)	
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTagsLoading = false;
			})
		}
    }