require('directive.videoplayer_jw');
require('service.admin');
require('service.pageLoader');
require('directive.pageLoading');
require('authToken');
require('UserCourses');
require('service.utility');
require('service.videoURL');
require('malihu-custom-scrollbar-plugin');

require('service.fileGenerator');
require('directive.menu');

require('service.fullscreen');
require('angular-socialshare');
require('directive.chat');

require('service.videoplayer');
require('service.videoNotes');
require('directive.itemComments');
require('service.itemQueries');
require('service.dashboardStats');
require('service.aws');

require('malihu-custom-scrollbar-plugin')($);
require('malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');

require('../../shared/directive/compile.directive.js')
require('../../shared/directive/imageComponent/image.component.js');

require("rangy");
require("rangy/lib/rangy-classapplier.js");
require("rangy/lib/rangy-serializer.js");
require("rangy/lib/rangy-textrange.js");
var rangy = window.rangy = require("rangy/lib/rangy-highlighter.js");


var uuid = require('uuid');

var Base64 = require('hash_js').Base64;
require('directive.SlidesComponent');
var XpathRange = require('xpath-range');

require('angular-froala');
require('angular-textbox-io');

require('./_practice.scss');

angular.module(window.moduleName)
    .component('practiceViewPage', {
        bindings: {
            module: '@',
            course: '@'
        },
        template: require('./practice.html'),
        controller: PracticeController,
        controllerAs: 'vm'
    })

PracticeController.$inject = ['$window', '$scope', 'admin', 'pageLoader', '$stateParams', '$filter', 'authToken', 'userCourses', 'utility', '$q', '$location', 'videoUrl', '$interval', '$timeout', '$sce', 'fileGenerator', 'fullScreen', 'Socialshare', 'videoNotes', 'videoPlayer', '$state', 'itemQueries', 'awsService', 'dashboardStatsService', 'EditorConfig'];

function PracticeController($window, $scope, admin, pageLoader, $stateParams, $filter, authToken, userCourses, utility, $q, $location, videoUrl, $interval, $timeout, $sce, fileGenerator, fullScreen, Socialshare, videoNotes, videoPlayer, $state, itemQueries, awsService, dashboardStatsService, EditorConfig) {
    var vm = this;
    vm.EditorConfig = EditorConfig;
    vm.testId = "00000000-0000-0000-0000-000000000000";

    vm.onlyPopoutVideo = $stateParams.popout;
    vm.isTestBeingTaken = ($stateParams.test_id) ? true : false;
    vm.test_id = $stateParams.test_id;
    vm.isVideoWrapper = false;


    //Hide Everything until all content is available to show
    vm.isPageContentLoaded = false;

    vm.isExpanded = true;
    var aspect = 16 / 9;

    vm.totalTimeStr = "00:00";
    vm.questionTimeStr = "00:00";

    vm.activeItem = $stateParams.item;
    vm.activeQuestion = $stateParams.question;
    vm.activeCourse = $stateParams.course;
    vm.activeModule = $stateParams.module;
    vm.activeTopic = $stateParams.topic;
    // vm.questionData = $stateParams.questionData;
    // console.log($stateParams)
    // if(vm.questionData){
    //     vm.questions = [vm.questionData];
    // }
    vm.isVideoPlaying = true;

    // $state.current.data.pageTitle = 'Questions - ' + vm.activeCourse;

    //Whther to show radio/checkbox or A,B,C,D.... as a option
    vm.optionAsACharacter = true;

    //////////////
    //Variables //
    //////////////
    vm.isUserAuthenticated = authToken.isAuthenticated();
    vm.activeRelatedTab = "";
    vm.displayedRelatedVideos = [];
    vm.isCoursePurchased = false;
    vm.currentQuestionIndex = 0;
    vm.showTextExaplanation = true;
    // vm.textExplanation = "<pre><code class='line-numbers language-javascript'>function Person(){\n  this.age = 0;\n\n  setInterval(() => {\n    this.age++; // |this| properly refers to the person object\n  }, 1000);\n}\n\nvar p = new Person();</code></pre>";
    vm.videoUrl = "http://vcdn.dvbydt.com/sanam.mp4?Expires=1459862078&Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cDovL3ZjZG4uZHZieWR0LmNvbS9zYW5hbS5tcDQiLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE0NTk4NjIwNzh9fX1dfQ__&Signature=HAB8s82-58UmPjX8ZfosXHje4yf8JnDKHrDpRv1ENVjsUULzR-RgAQh5vk6p4n4P-WETxzDhUpsLKNB7XYRykpm2wpRbS-qOInDm3eKZPK4oSvc0~Dmz5QYoVSWXZR5uONJhWVBsDmRmrcqZIL1~DAGcRwy1uubfsGouKXcp6HR91Iv6k6LxT1m29iDUu6NrnDyE2PQNKlf0A61UgvefxCbvE9QxnZ5PWagocV-5C4iudh9sovu63yFTQwjiItavxUiCxUC4C0jr~b1wFiY5KhWmqm5ziH63kJJgj8oQkj9yWRqpG5klx4aLg-sAoUDY5lgcveCxTbd7I8wrZpLZCA__&Key-Pair-Id=APKAIVOLA6ECXA4NTBPA";
    vm.wrongAnsFeedbackOptions = [
        'First time Learning',
        'Wasnt sure of concept',
        'Didnt read the Question Properly',
        'Calculation Error',
        'Silly Mistake'
    ];
    vm.rateQuestionOptions = ['Easy', 'Standard', 'Hard'];
    vm.userWrongAnsFeedbackOption = [];

    vm.userRightAnsList = [];
    vm.userSkipAnsList = [];
    vm.userWrongAnsList = [];
    vm.userLearnedQueList = [];
    vm.note = {};

    vm.MAX_FONT_SIZE = 40;
    vm.MIN_FONT_SIZE = 18;
    vm.MAX_WIDTH = 90;
    vm.MIN_WIDTH = 50;
    vm.shouldShowQuestionList = false;
    vm.shouldShowContentWrapper = true;
    vm.bigSidebarWidth = 25;
    vm.bigSidebarWidthMin = 25;
    vm.bigSidebarWidthMax = 45;
    vm.bigSidebarDefaultWidth = '400px';
    vm.userAuthorized = false;

    vm.isHorizontalLayout = true;

    //////////////
    //Functions //
    //////////////
    ///

    vm.goToDashboard = utility.goToDashboard;
    vm.goToCourse = goToCourse;
    vm.isUserLoggedIn = authToken.isAuthenticated;
    vm.goToURL = utility.goToURL;
    vm.getUser = authToken.getUser;
    vm.getCoursePricePageURL = utility.getCoursePricePageURL;
    vm.getLoginUrl = utility.getLoginUrl;
    vm.getAdminPracticePageUrl = utility.getAdminPracticePageUrl;

    vm.finishTest = finishTest;
    vm.changeTimeFormat = utility.changeTimeFormat;
    vm.setVideo = setVideo;
    vm.pauseVideo = pauseVideo;
    vm.getNumber = getNumber;
    vm.getNextQuestionId = getNextQuestionId;
    vm.getPrevQuestionId = getPrevQuestionId;
    vm.getCurrentQuestionId = getCurrentQuestionId;
    vm.prevQue = prevQue;
    vm.nextQue = nextQue;
    vm.radioOrCheckBox = radioOrCheckBox;
    vm.checkAns = checkAns;
    vm.selectExamAns = selectExamAns;
    vm.isRightAnswer = isRightAnswer;
    vm.submitExamReview = submitExamReview;
    vm.getNumberOfAttemptedQuestion = getNumberOfAttemptedQuestion;
    vm.getNumberOfRightQuestion = getNumberOfRightQuestion;
    vm.getNumberOfWrongQuestion = getNumberOfWrongQuestion;
    vm.learnMore = learnMore;
    vm.showFeedback = showFeedback;
    vm.showReport = showReport;
    vm.addNotes = addNotes;
    vm.showQuestionList = showQuestionList;
    vm.goToQuestion = goToQuestion;
    vm.markForReview = markForReview;
    vm.showShareWidget = showShareWidget;
    vm.resetAllSidebar = resetAllSidebar;
    vm.shouldShowCloseSidebarButton = shouldShowCloseSidebarButton;
    vm.showTextTab = showTextTab;
    vm.showVideoTab = showVideoTab;
    vm.showContentWrapper = showContentWrapper;

    vm.getCurrentActiveModuleDetails = getCurrentActiveModuleDetails;

    vm.goToLogin = utility.goToLogin;
    vm.goToLogout = utility.goToLogout;
    vm.shouldModuleNameGrayColored = shouldModuleNameGrayColored;
    vm.showSlideModal = showSlideModal;
    vm.closeSlideModal = closeSlideModal;

    vm.getQuestionsNotes = getQuestionsNotes;
    vm.shareHighlight = shareHighlight;
    vm.shareHighlightOnQA = shareHighlightOnQA;
    vm.addHighlightNote = addHighlightNote;
    vm.onlyDoHighlight = onlyDoHighlight;
    vm.saveNote = saveNote;
    vm.deleteNote = deleteNote;
    vm.deleteNoteConfirm = deleteNoteConfirm;
    vm.closeDeleteNoteModal = closeDeleteNoteModal;
    vm.editNote = editNote;
    vm.showNote = showNote;
    vm.doesHighlightContainNote = doesHighlightContainNote;

    vm.increaseFont = increaseFont;
    vm.decreaseFont = decreaseFont;
    vm.increaseWidth = increaseWidth;
    vm.decreaseWidth = decreaseWidth;
    vm.resetReadingPref = resetReadingPref;

    vm.increaseSidebarWidth = increaseSidebarWidth;
    vm.decreaseSidebarWidth = decreaseSidebarWidth;
    vm.resetBigSidebar = resetBigSidebar;
    vm.closeNoteSidebar = closeNoteSidebar;

    vm.openBigSidebar = openBigSidebar;
    vm.closeBigSidebar = closeBigSidebar;
    vm.toggleBigSidebar = toggleBigSidebar;

    vm.pauseQuestionTimer = pauseQuestionTimer;
    vm.submitQuestionState = submitQuestionState;

    vm.changeModeAndLoadItem = changeModeAndLoadItem;
    vm.loadItem = loadItem;
    vm.showStartExamModal = showStartExamModal;
    vm.showFinishExamModal = showFinishExamModal;
    vm.fetchAllQuestions = fetchAllQuestions;

    vm.trustAsHTML = trustAsHTML;

    vm.showOnlyReviewQuestion = showOnlyReviewQuestion;
    vm.showOnlySkipQuestion = showOnlySkipQuestion;
    vm.showOnlyRightQuestion = showOnlyRightQuestion;
    vm.showOnlyWrongQuestion = showOnlyWrongQuestion;
    vm.showOnlyLearnQuestion = showOnlyLearnQuestion;
    vm.noOfLearnedQuestion = noOfLearnedQuestion;
    vm.noOfReviewQuestion = noOfReviewQuestion;
    vm.noOfSkipQuestion = noOfSkipQuestion;

    vm.openSideBarInSmallScreen = openSideBarInSmallScreen;
    vm.closeSideBarInSmallScreen = closeSideBarInSmallScreen;
    vm.printQuestion = printQuestion;

    vm.getOptionCharacter = getOptionCharacter;

    vm.getLanguageForSyntaxHighlighting = getLanguageForSyntaxHighlighting;

    vm.toggleMenuModal = toggleMenuModal;

    vm.doFullScreen = doFullScreen;

    vm.isItemContainsSlides = isItemContainsSlides;

    // Share Helpers
    vm.shareToFB = shareToFB;
    vm.shareToTwitter = shareToTwitter;
    vm.shareToEWProfile = shareToEWProfile;
    vm.shareToQA = shareToQA;

    vm.shareExamStatsToFB = shareExamStatsToFB;
    vm.shareExamStatsToTwitter = shareExamStatsToTwitter;
    vm.shareExamStatsToEWProfile = shareExamStatsToEWProfile;
    vm.shareExamStatsToQA = shareExamStatsToQA;

    // Qa helpers
    vm.saveQueryForItem = saveQueryForItem;
    vm.getQueriesForItem = getQueriesForItem;

    vm.getSignedDownloadUrl = getSignedDownloadUrl;

    vm.randomizeQuestions = randomizeQuestions;
    vm.randomizeOptions = randomizeOptions;
    vm.sequentialQuestions = sequentialQuestions;

    vm.closeHighlightOver = closeHighlightOver;
    vm.closeDehighlightOver = closeDehighlightOver;

    vm.doSyntaxHighlighting = function(){
        // if(vm.fullCourse && vm.fullCourse.courseName.toLowerCase().indexOf("maths") <= -1){
        //     utility.doSyntaxHighlighting();
        // }
    }


    $(document).on('click', function(e) {
        var captureClick = $(e.target).attr("data-capture-document-click");

        if (!captureClick) {
            element = $(e.target).closest("[data-capture-document-click]");
            captureClick = $(element).attr("data-capture-document-click");
        }

        if (captureClick === "false") {
            return;
        }

        closeSideBarInSmallScreen();
        $scope.$apply();
    });



    // jQuery(window).ready(function() {

    // });

    vm.$onInit = function() {
        setTimeout(function() {
            $("body").css('width', '100%');
        }, 1000)

        // recordPageClickStats();
        // captureIdleEvents();
    }


    if (!vm.isTestBeingTaken) {
        //if test is not being taken
        //user is viewwing normal course
        init();
    } else if (vm.isTestBeingTaken) {
        //If test is going on
        pageLoader.show();
        admin.getTestById({
                tst_id: vm.test_id
            })
            .then(function(res) {
                if (res.data.length > 0) {
                    //get book id (string)
                    var q_data = res.data[0].que_book_id;
                    // json object
                    var json_obj_course = JSON.parse(q_data);
                    // array of questions id
                    var ques_ids = [];
                    //iterate through all various courses if any
                    for (course in json_obj_course) {
                        var c = json_obj_course[course];
                        //get questions for this course
                        ques_ids = ques_ids.concat(c.map(function(v, i) {
                            return v.que_id;
                        }));
                    }

                    vm.testQuestionsId = ques_ids;
                    console.log(vm.testQuestionsId);
                    initiateTest();
                }

            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function init() {
        doKeyBinding();
        vm.questions = [];
        // Flow 
        // 1. Get User Purchased Courses
        // 2. Get All Authorized Item for this course
        // 3. Get All Course Items / And Get Duration for all video items
        // 4. If activeItem is authorized show error dialog
        // 5. Get Bookmarks
        // 6. Get Notes
        pageLoader.show();
        vm.loadItem('', '', true);
        // getUserCourses()
        //     .then(getCourseItems)
        //     .then(getAuthorizedItems)
        //     .then(function() {
        //         vm.loadItem(vm.activeModule, vm.activeItem, true);
        //     })
        //     .catch(function(err) {
        //         console.error("Error in fetching data", err);
        //     })
        //     .finally(function() {})
    }

    function initiateTest() {
        vm.totalTime = 0;
        vm.isVideoWrapper = false;
        vm.isPageContentLoaded = true;
        vm.isVideoPlaying = true;
        vm.activeItemType = 'qGroup';

        setTimeout(function() {
            $('body').css('overflow-y', 'scroll');
            $('body').css('overflow-x', 'hidden');
        }, 800)
        vm.isPractice = false;
        pageLoader.show();
        admin.getPracticeQuestionsById({
                questionsId: vm.testQuestionsId
            })
            .then(function(d) {
                if (d.data.length > 0) {
                    vm.questions = d.data;
                    vm.allQuestions = d.data;

                    if (shouldFilterQuestion()) {
                        //if filter is set already then show only filter questions
                        doFilterQuestion();
                        return;
                    }

                    vm.examTime = 0;
                    vm.questions.forEach(function(v) {
                        v.options.shuffle();
                        vm.examTime += parseInt(v.questionTime || 1);
                    });

                    vm.examTime = vm.examTime * 60;

                    //Start From First Question
                    vm.currentQuestionIndex = 0;
                    setQueConfig();
                }
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    //Get User Purchased Courses
    function getUserCourses() {
        vm.isCoursePurchased = authToken.isCoursePurchased(vm.activeCourse);
        return $q.when([]);
    }

    //Get All Authorized Items for this course
    function getAuthorizedItems() {
        console.log("vm.isCoursePurchased..", vm.isUserAuthenticated);
        return admin.getAuthorizedItems({
                courseId: vm.activeCourse
            })
            .success(function(data) {
                var items = data;

                vm.allItems = data;
                var temp = $filter('filter')(vm.allItems, {
                    itemId: vm.activeItem
                })[0] || {};
                vm.activeModule = temp.moduleId;


                console.log("AItems.........", data);
                console.log("activeModule", vm.activeModule);

                //Only Authorized Item : itemId -> itemType Mapping
                vm.authorizedItemsMap = {};
                //All items : itemId -> itemType mapping 
                vm.authorizedFullItemsMap = {};
                //All items : moduleId -> itemType mapping 
                vm.authorizedModuleFullItemsMap = {};

                vm.configItemTypes = Config.itemTypes;
                items.map(function(v) {
                    vm.authorizedFullItemsMap[v.itemId] = v.itemType;
                    vm.authorizedModuleFullItemsMap[v.moduleId] = vm.authorizedModuleFullItemsMap[v.moduleId] || [];
                    vm.authorizedModuleFullItemsMap[v.moduleId].push(v.itemType);
                    return v;
                });

                if (!vm.isUserAuthenticated) {
                    //If user is not logged in
                    vm.authorizedModuleItems = items.map(function(v) {
                        //Reuire Login
                        if (vm.configItemTypes[v.itemType].authenticate) {
                            return v.moduleId;
                        }
                    });
                    vm.itemIds = items.map(function(v) {
                        if (vm.configItemTypes[v.itemType].authenticate) {
                            vm.authorizedItemsMap[v.itemId] = v.itemType;
                        }
                        return v;
                    });

                } else if (vm.isUserAuthenticated && !vm.isCoursePurchased) {

                    vm.authorizedModuleItems = items.map(function(v) {
                        //Reuire Login And Purchase
                        if (vm.configItemTypes[v.itemType].authenticate && vm.configItemTypes[v.itemType].requirePurchase) {
                            return v.moduleId;
                        }
                    });
                    vm.itemIds = items.map(function(v) {
                        if (vm.configItemTypes[v.itemType].authenticate && vm.configItemTypes[v.itemType].requirePurchase) {
                            vm.authorizedItemsMap[v.itemId] = v.itemType;
                        }
                        return v;
                    });
                } else if (vm.isUserAuthenticated && vm.isCoursePurchased) {
                    vm.authorizedModuleItems = [];
                    vm.authorizedItemsMap = {};
                }
            });
    }

    //Get All Course Items
    function getCourseItems() {
        return admin.getCourseItems({
                courseId: vm.activeCourse
            })
            .then(function(data) {
                vm.fullCourse = data[0] || {};

                if (vm.fullCourse.isBatch) {
                    vm.isCoursePurchased = authToken.isCoursePurchased(vm.fullCourse.training_id);
                }

                vm.fullCourse.moduleDetail = vm.fullCourse.moduleDetail || [];
                vm.fullCourse.moduleDetail.sort(function(a, b) {
                    return a.moduleWeight - b.moduleWeight;
                });
                var firstModule = vm.fullCourse.moduleDetail[0] || {};
                var firstItem = firstModule.moduleItems[0] || {};
                if (!vm.activeModule) {
                    vm.activeModule = firstModule.moduleId;
                }
                if (!vm.activeItem) {
                    vm.activeItem = firstItem.itemId;
                }
                vm.moduleName = getCurrentActiveModuleDetails().moduleName;
            });
    }

    // Get Current Active Module Details
    function getCurrentActiveModuleDetails() {
        return {}
    }

    //Show error modal if item is Authorized
    function showErrorModal(itemId) {
        var item = $filter('filter')(vm.itemIds, {
            itemId: itemId
        })[0];
        if (!vm.isUserAuthenticated) {
            if (item['itemType'] === Config.itemTypes["demologin"].type) {
                vm.errorModalMsg = "Login to get access to this video";
            }
            if (item['itemType'] === Config.itemTypes["paid"].type) {
                vm.errorModalMsg = "Login and Buy to get immediate access to this course and more";
            }
        } else if (vm.isUserAuthenticated) {
            if (item['itemType'] === Config.itemTypes["paid"].type) {
                vm.errorModalMsg = "Buy to get immediate access to this course and more";
            }
        }
        vm.userAuthorized = false;
        return;
    }

    function goToCourse(course) {
        // window.location.href = '/courses/' + course.courseType + "/" + course.courseId;
        $state.go("practice", {
            course: course.courseId,
            item: ''
        });
    }

    //Detemine whether one of the item of this module is not authorized
    //if authorized then colored it with gray(disabled item)
    function shouldModuleNameGrayColored(moduleId) {
        vm.authorizedModuleItems = vm.authorizedModuleItems || [];
        return (vm.authorizedModuleItems.indexOf(moduleId) > -1) &&
            (vm.authorizedModuleFullItemsMap[moduleId].indexOf('demo') < 0) &&
            (vm.authorizedModuleFullItemsMap[moduleId].indexOf('demologin') < 0);
    }

    //Open SLide Modal
    function showSlideModal(videoId, itemName) {
        vm.slides = undefined;
        vm.topicTitle = undefined;
        vm.slideDesc = undefined;
        vm.activeSlideItemName = itemName;
        vm.closeSideBarInSmallScreen();
        vm.activeRelatedTab = 'slides';


        //Pause Video Player
        $scope.$broadcast('pauseVideo');

        setTimeout(function() {
            $('html, body').animate({
                scrollTop: $("#slides").offset().top - 100
            }, 1000);
        })

        // pageLoader.show();
        admin.getVideoSlides({
                videoId: videoId
            })
            .then(function(data) {
                if (data.length > 0) {
                    vm.topicTitle = data[0].topicTitle;
                    vm.slideDesc = data[0].slideDesc;
                    vm.slides = data[0].slideDetail;
                    vm.currentSlide = 0;
                    // vm.doSyntaxHighlighting();
                }
                vm.shouldShowSlideModal = true;

            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                // pageLoader.hide();
            });
    }

    //Close Slide Modal
    function closeSlideModal() {
        vm.shouldShowSlideModal = false;
    }

    function startQuestionTimer(id) {
        if (!vm.userAuthorized) {
            return;
        }
        //For every question tracking of time
        vm.questionsTimer = vm.questionsTimer || {};
        if (vm.questionsTimer[id]) {
            // if already visited question
            vm.questionTime = vm.questionsTimer[id];
        } else {
            vm.questionTime = 0;
        }
        vm.questionTimeStr = utility.changeTimeFormat(vm.questionTime);

        //Total time
        vm.totalTime = vm.totalTime || 0;
        if (vm.questionTimerId) {
            // if already running $interval , cancel it
            $interval.cancel(vm.questionTimerId);
        }
        vm.questionTimerId = $interval(function() {
            if (!vm.isPracticePause) {
                //If practice is not paused
                //increment question time
                vm.questionTime++;
                vm.questionsTimer[id] = vm.questionTime;
                //increment total time
                vm.totalTime += 1;
                vm.questionTimeStr = utility.changeTimeFormat(vm.questionTime);
                vm.totalTimeStr = utility.changeTimeFormat(vm.totalTime);
            }
        }, 1000);
    }

    function startExamQuestionTimerCountDown(id) {
        if (!vm.userAuthorized) {
            return;
        }
        //For every question tracking of time
        vm.questionsTimer = vm.questionsTimer || {};
        if (vm.questionsTimer[id]) {
            // if already visited question
            vm.questionTime = vm.questionsTimer[id];
        } else {
            vm.questionTime = 0;
        }
        vm.questionTimeStr = utility.changeTimeFormat(vm.questionTime);

        //Total time
        if (!vm.totalTime) {
            vm.totalTime = vm.examTime || 0;
        }
        if (vm.questionTimerId) {
            // if already running $interval , cancel it
            $interval.cancel(vm.questionTimerId);
        }
        vm.questionTimerId = $interval(function() {
            //increment question time
            vm.questionTime++;
            vm.questionsTimer[id] = vm.questionTime;
            //increment total time
            vm.totalTime -= 1;
            if (vm.totalTime === 0) {
                //finish exam
                $interval.cancel(vm.questionTimerId);
                vm.finishTest();
            }
            vm.questionTimeStr = utility.changeTimeFormat(vm.questionTime);
            vm.totalTimeStr = utility.changeTimeFormat(vm.totalTime || 0);
        }, 1000);
    }

    function pauseQuestionTimer() {
        console.log("isPracticePause");
        vm.isPracticePause = !vm.isPracticePause;
    }

    function getNumber(num) {
        return Array.apply(null, {
            length: num
        });
    }



    function doKeyBinding() {
        $(".practice-page-wrapper-new").on('keydown keypress', function(e) {
            var keyCode = e.keyCode;
            // console.log(keyCode);
            if (keyCode === 37) {
                // ();
                $scope.$apply(vm.prevQue);
            } else if (keyCode === 39) {
                // vm.nextQue();
                $scope.$apply(vm.nextQue);
            }
        })
    }

    function setVideo(url) {
        if (vm.isAlreadySetup) {
            // $scope.$broadcast('loadVideo', url);
            // $scope.$broadcast('playVideo', args);
            console.log("Already Setup");
        } else {
            $scope.$broadcast('setupVideo', url);
            vm.isAlreadySetup = true;
        }
    }

    function pauseVideo() {
        if (vm.isAlreadySetup) {
            $scope.$broadcast('pauseVideo');
        }
    }

    function getPrevQuestionId() {
        if (vm.questions) {
            if (vm.questions[vm.currentQuestionIndex - 1]) {
                return vm.questions[vm.currentQuestionIndex - 1].questionId;
            } else {
                return ""
            }
        }
    }

    function getCurrentQuestionId() {
        if (vm.questions) {
            if (vm.questions[vm.currentQuestionIndex]) {
                return vm.questions[vm.currentQuestionIndex].questionId;
            } else {
                return ""
            }
        }
    }

    function getNextQuestionId() {
        if (vm.questions) {
            if (vm.questions[vm.currentQuestionIndex + 1]) {
                return vm.questions[vm.currentQuestionIndex + 1].questionId;
            } else {
                return ""
            }
        }
    }

    function prevQue() {
        $timeout(function() {
            if (!vm.isUserLoggedIn()) {
                vm.showLoginAlertModal = true;
                vm.needLoginErrModalMsg = "Login to go to previous question";
                return;
            }

            if (vm.currentQuestionIndex === 0) {
                vm.showGeneralMsgModal = true;
                vm.generalModalMsg = "You are viewing first question."
                return;
            }
            vm.showGeneralMsgModal = false;
            if (vm.currentQuestionIndex > 0)
                vm.currentQuestionIndex--;
            else
                vm.currentQuestionIndex = 0;
            setQueConfig();

            question_load_stats();
            // $timeout(function() {
            //     MathJax.Hub.Queue(["Typeset",MathJax.Hub,vm.questions[vm.currentQuestionIndex]['questionId']]);
            // })
        });


    }



    function nextQue() {
        $timeout(function() {
            if (!vm.isUserLoggedIn()) {
                vm.showLoginAlertModal = true;
                vm.needLoginErrModalMsg = "Login to go to next question";
                return;
            }
            if (!vm.isPractice) {
                //if it is exam then check answer
                vm.questions[vm.currentQuestionIndex]['userAnswers'].forEach(function(v, i) {
                    vm.checkAns(v, 'true');
                })
            }

            if (vm.currentQuestionIndex === (vm.questions.length - 1)) {
                vm.showLastQuestionMsgModal = true;
                // vm.generalModalMsg = "You are at the last question."
                return;
            }
            vm.showLastQuestionMsgModal = false;

            if (vm.currentQuestionIndex < (vm.questions.length - 1))
                vm.currentQuestionIndex++;
            else
                vm.currentQuestionIndex = vm.questions.length - 1;
            setQueConfig();

            question_load_stats();
            // $timeout(function() {
            //     MathJax.Hub.Queue(["Typeset",MathJax.Hub,vm.questions[vm.currentQuestionIndex]['questionId']]);
            // })
        })
    }


    //To send stats to server
    function submitQuestionState(index) {
        vm.statsSubmited = vm.statsSubmited || {};
        if (index >= 0) {
            var question = vm.questions[index];

            var usr_id = authToken.getUserId() || "";
            var que_id = question["questionId"];

            if (vm.statsSubmited[que_id]) {
                // if already stats submitted
                return;
            }

            var tst_id = vm.currentExamId ? vm.currentExamId : undefined;
            var tpc_id = vm.activeItem
            var mdl_id = vm.activeModule;
            var crs_id = (vm.fullCourse) ? vm.fullCourse.courseId : vm.test_id;
            var que_strt_tm = question["startTime"];
            var que_end_tm = new Date(); //Capture this time
            var que_ans = {};
            var que_ans_tm = {};
            var no_of_clks = question["noOfAttempts"];
            var revu = question["markForReview"];
            var lrn = question["isLearned"];
            var skip = question["isSkipped"];
            var wrg_rsn = [];
            var que_diff = "Not Selected";

            if (!question["startTime"]) {
                //If Question Doesn't have start time
                return;
            }

            question.optionTimeStats = question.optionTimeStats || [];
            question.optionTimeStats.forEach(function(v) {
                que_ans_tm[v.opt_id] = v.time;
            })
            que_ans_tm = que_ans_tm || [];

            question.optionBoolStats = question.optionBoolStats || [];
            question.optionBoolStats.forEach(function(v) {
                que_ans[v.opt_id] = v.isRight;
            });
            que_ans = que_ans || [];

            vm.userWrongAnsFeedbackOption.forEach(function(v, i) {
                wrg_rsn.push(vm.wrongAnsFeedbackOptions[i])
            });
            if (wrg_rsn.length <= 0) {
                wrg_rsn = ["Not Selected"];
            }
            for (k in vm.rateQuestionUserAns) {
                que_diff = vm.rateQuestionOptions[k];
            }

            var data = {
                tst_id: tst_id,
                usr_id: usr_id,
                que_id: que_id,
                tpc_id: tpc_id,
                mdl_id: mdl_id,
                crs_id: crs_id,
                que_strt_tm: que_strt_tm || new Date(),
                que_end_tm: que_end_tm || new Date(),
                que_ans_tm: que_ans_tm,
                que_ans: que_ans,
                no_of_clks: no_of_clks,
                revu: revu,
                lrn: lrn,
                skip: skip,
                wrg_rsn: wrg_rsn,
                que_diff: que_diff
            }
            if (vm.isPractice) {
                //Send Practice State
                admin.saveQuestionStats(data);
            } else if (!vm.isPractice) {
                //Send Exam(Test) State
                if (vm.testEndTime) {
                    data.tst_strt_tm = vm.testStartTime || new Date();
                    data.tst_end_tm = vm.testEndTime || new Date();
                }

                admin.saveTestQuestionStats(data)
                    .then(function() {

                        vm.testStartTime = undefined;
                        vm.testEndTime = undefined;
                    })
            }

            vm.statsSubmited[que_id] = true;
        }
    }

    function getPreviousQuestionInfo() {
        if (vm.currentQuestionIndex > 0) {
            submitQuestionState(vm.currentQuestionIndex - 1);
        }
    }

    function finishTest() {
        if (vm.isPractice) {
            //if it is practice
            vm.submitQuestionState(vm.currentQuestionIndex);
        } else {
            //if it is exam(test)

            //if it is exam then check last question answer
            vm.questions[vm.currentQuestionIndex]['userAnswers'].forEach(function(v, i) {
                vm.checkAns(v, 'true');
            })



            //update dashboard stats counter for number of correct/wrong attempted question
            if (vm.isUserLoggedIn()) {
                console.log(vm.getNumberOfRightQuestion());
                console.log(vm.getNumberOfWrongQuestion());
                dashboardStatsService.updateNoOfCorrectQuestionAttemptedCounter({
                    usr_id: authToken.getUserId(),
                    count: vm.getNumberOfRightQuestion()
                });
                dashboardStatsService.updateNoOfWrongQuestionAttemptedCounter({
                    usr_id: authToken.getUserId(),
                    count: vm.getNumberOfWrongQuestion()
                });
            }

            vm.testEndTime = new Date();
            vm.submitQuestionState(vm.currentQuestionIndex);
            vm.isCurrentTestFinish = true;
            vm.questions = [];
            vm.shouldShowFinishExamModal = false;
        }
    }

    function setQueConfig() {
        resetOptionTimer();

        vm.activeRelatedTab = "";

        var obj = {};
        angular.copy($stateParams, obj);
        obj.question = vm.questions[vm.currentQuestionIndex]['questionId'];
        $state.transitionTo($state.current, obj, {
            reload: false,
            notify: false,
            location: 'replace'
        });
        vm.userWrongAnsFeedbackOption = [];
        vm.userWrongAnsReportOption = false;
        vm.userWrongAnsReportOptionText = "";
        vm.userWrongAnsReportOptionTextError = "";

        vm.rateQuestionUserAns = {};
        vm.rateQuestionReport = false;
        vm.rateQuestionReportText = "";
        vm.rateQuestionReportTextError = "";
        vm.rateQuestionNotApplicable = false;


        vm.shouldShowExplanation = false;
        vm.isAlreadySetup = false;
        vm.shouldShowFeedback = false;
        vm.shouldShowShareWidget = false;
        vm.shouldShowQuestionList = false;
        vm.shouldShowContentWrapper = true;
        if (vm.isVideoWrapper) {
            vm.shouldShowContentWrapper = true;
            vm.shouldShowQuestionList = false;
        }

        vm.showTextExaplanation = true;
        vm.userOption = {};
        vm.ansArr = vm.questions[vm.currentQuestionIndex].ans;
        vm.ansArr = vm.ansArr.map(function(v) {
            return Base64.decode(v);
        })

        var i = vm.currentQuestionIndex;
        if (vm.questions[vm.currentQuestionIndex]['isAnswered'] === undefined && vm.questions[vm.currentQuestionIndex]['markForReview'] === undefined) {
            vm.questions[vm.currentQuestionIndex]['isSkipped'] = true;
        }
        setTimeout(function() {
            vm.doSyntaxHighlighting();
            $(".useroption").show();
            $(".useroption_color").hide();
            if (vm.questions[vm.currentQuestionIndex]['isAnswered']) {
                questionAlreadyAnswered();
            }
        }, 100);
        vm.questions[vm.currentQuestionIndex]["startTime"] = new Date();
        vm.questions[vm.currentQuestionIndex]["userAnswers"] = vm.questions[vm.currentQuestionIndex]["userAnswers"] || [];
        if (vm.isPractice) {
            startQuestionTimer(vm.questions[vm.currentQuestionIndex].questionId);
        } else {
            //If exam
            startExamQuestionTimerCountDown(vm.questions[vm.currentQuestionIndex].questionId);
        }
        $scope.$broadcast('pauseVideo');

        setTimeout(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 100);
        })

        ///Update dashboard stats counter for , no of question practiced and no of question attempted in test
        if (vm.isUserLoggedIn()) {
            if (vm.isPractice) {
                dashboardStatsService.incrementNoOfQuestionPracticedCounter({
                    usr_id: authToken.getUserId()
                });
            } else {
                dashboardStatsService.incrementNoOfQuestionAttemptedInTestCounter({
                    usr_id: authToken.getUserId()
                })
            }
        }
        // vm.isQuestionProcessing = true;
        // $timeout(function(){
        //     MathJax.Hub.Queue(["Typeset",MathJax.Hub,vm.questions[vm.currentQuestionIndex]['questionId']]);    
        // })
        vm.getLatestVerifyStatusOfGivenPracticeQuestions();
    }

    function resetOptionTimer() {
        vm.optionTime = 0;
        $interval.cancel(vm.optionTimerId);
        vm.optionTimerId = $interval(function() {
            vm.optionTime++;
        }, 1000);
    }

    //This function indicates whether
    //One ans, that is 'Radio'
    //Or
    //Multiple Answers, that is 'Checkbox'
    function radioOrCheckBox() {

        // if(vm.activeItemFullObject.itemOptionType === "radio"){
        //     return 'radio';
        // }else{
        //     return 'checkbox';
        // }


        var arr = vm.ansArr || [];
        if (arr.length > 1) {
            return 'checkbox';
        } else {
            return 'radio';
        }
    }

    function checkAns(id, isUserClicked) {
        vm.userOption[id] = !vm.userOption[id];

        //Assign userAnswers arr if it is already there or create new
        vm.questions[vm.currentQuestionIndex]['userAnswers'] = vm.questions[vm.currentQuestionIndex]['userAnswers'] || [];
        //Check Id is already added to userAnsers arr
        if (vm.questions[vm.currentQuestionIndex]['userAnswers'].indexOf(id) <= -1) {
            vm.questions[vm.currentQuestionIndex]['userAnswers'].push(id);
        }


        //Will return if answer is right/wrong
        var isRightAnswer = changeOptionColor(id);

        if (isUserClicked) {
            vm.questions[vm.currentQuestionIndex]['optionTimeStats'] = vm.questions[vm.currentQuestionIndex]['optionTimeStats'] || [];
            vm.questions[vm.currentQuestionIndex]['optionTimeStats'].push({
                opt_id: id,
                time: vm.optionTime
            });
            vm.questions[vm.currentQuestionIndex]['optionBoolStats'] = vm.questions[vm.currentQuestionIndex]['optionBoolStats'] || [];
            vm.questions[vm.currentQuestionIndex]['optionBoolStats'].push({
                opt_id: id,
                isRight: isRightAnswer
            });
        }
        resetOptionTimer();

        //If there is multiple answers
        //Check that all answers are selected by user
        if (vm.radioOrCheckBox() === 'checkbox') {
            var isFound = true;
            //User has checked all right answers or not
            var userAnswers = vm.questions[vm.currentQuestionIndex]['userAnswers'];
            for (var i = 0; i < userAnswers.length; i++) {
                if (vm.ansArr.indexOf(userAnswers[i]) <= -1) {
                    isFound = false;
                    break;
                }
            }
            var allAnsFound = false;
            var allAnsFoundCount = 0;
            for (var i = 0; i < vm.ansArr.length; i++) {
                if (userAnswers.indexOf(vm.ansArr[i]) > -1) {
                    allAnsFoundCount++;
                }
            }
            if (allAnsFoundCount === vm.ansArr.length) {
                allAnsFound = true;
            }

            //Mark isRightAnswerTicked variable with true/false to disable option click afterwords
            if (!vm.questions[vm.currentQuestionIndex]['isRightAnswerTicked']) {
                vm.questions[vm.currentQuestionIndex]['isRightAnswerTicked'] = allAnsFound;
                vm.questions[vm.currentQuestionIndex]['noOfAttempts'] = vm.questions[vm.currentQuestionIndex]['noOfAttempts'] || 0;
                vm.questions[vm.currentQuestionIndex]['noOfAttempts']++;
            }

            if (!vm.questions[vm.currentQuestionIndex]['isAnswered']) {
                if (!isFound) {
                    vm.questions[vm.currentQuestionIndex]['isAnswered'] = true;
                    vm.questions[vm.currentQuestionIndex]['isRightAnswer'] = false;

                    //Remove Ans From Skip Arr and Move to right/worng arr
                    vm.userWrongAnsList.push(vm.currentQuestionIndex);
                    // var pos = vm.userSkipAnsList.indexOf(vm.currentQuestionIndex);
                    // vm.userSkipAnsList.splice(pos, 1);
                    vm.questions[vm.currentQuestionIndex]['isSkipped'] = false;

                    //If It is not right answer in first attempt 
                    //Show feedback form
                    //Hide all other sidebar form
                    vm.showFeedback();

                } else if (isFound && (userAnswers.length === vm.ansArr.length)) {
                    vm.questions[vm.currentQuestionIndex]['isAnswered'] = true;
                    vm.questions[vm.currentQuestionIndex]['isRightAnswer'] = true;

                    //Remove Ans From Skip Arr and Move to right/worng arr
                    vm.userRightAnsList.push(vm.currentQuestionIndex);

                    // var pos = vm.userSkipAnsList.indexOf(vm.currentQuestionIndex);
                    // vm.userSkipAnsList.splice(pos, 1);
                    vm.questions[vm.currentQuestionIndex]['isSkipped'] = false;
                }
            }
        } else {
            //If it is radio button
            //Only one answer


            //Mark isRightAnswerTicked variable with true/false to disable option click afterwords
            if (!vm.questions[vm.currentQuestionIndex]['isRightAnswerTicked']) {
                vm.questions[vm.currentQuestionIndex]['isRightAnswerTicked'] = isRightAnswer;
                vm.questions[vm.currentQuestionIndex]['noOfAttempts'] = vm.questions[vm.currentQuestionIndex]['noOfAttempts'] || 0;
                vm.questions[vm.currentQuestionIndex]['noOfAttempts']++;
            }


            //Check If this question is not already added to
            //1. vm.userRightAnsList
            //2. vm.userWrongAnsList
            //If not then only add this question number
            if (vm.userRightAnsList.indexOf(vm.currentQuestionIndex) <= -1 && vm.userWrongAnsList.indexOf(vm.currentQuestionIndex)) {
                if (isRightAnswer) {
                    //Add to Right answer list array
                    vm.userRightAnsList.push(vm.currentQuestionIndex);
                } else {
                    //Add To Wrong Answer list array
                    vm.userWrongAnsList.push(vm.currentQuestionIndex);
                }
            }

            //If Question is not answered then
            if (!vm.questions[vm.currentQuestionIndex]['isAnswered']) {

                //Remove Ans From Skip Arr and Move to right/worng arr
                // var pos = vm.userSkipAnsList.indexOf(vm.currentQuestionIndex);
                // vm.userSkipAnsList.splice(pos, 1);
                vm.questions[vm.currentQuestionIndex]['isSkipped'] = false;

                //MArk isAnswered variable to true
                vm.questions[vm.currentQuestionIndex]['isAnswered'] = true;

                //Mark isRightAnswer variable with true/false
                vm.questions[vm.currentQuestionIndex]['isRightAnswer'] = isRightAnswer;


                if (!isRightAnswer) {
                    //If It is not right answer in first attempt 
                    //Show feedback form
                    //Hide all other sidebar form
                    vm.showFeedback();
                }
            }
        }
    }

    function selectExamAns(id) {
        vm.questions[vm.currentQuestionIndex]['userAnswers'] = vm.questions[vm.currentQuestionIndex]['userAnswers'] || [];
        if (vm.radioOrCheckBox() === "radio") {
            vm.questions[vm.currentQuestionIndex]['userAnswers'] = [id];
        } else {
            var useranswers = vm.questions[vm.currentQuestionIndex]['userAnswers'];
            if (useranswers.indexOf(id) > -1) {
                var pos = useranswers.indexOf(id);
                vm.questions[vm.currentQuestionIndex]['userAnswers'].splice(pos, 1);
            } else {
                vm.questions[vm.currentQuestionIndex]['userAnswers'].push(id);
            }
        }
    }

    function isRightAnswer(actualAnswers, selectedAns) {
        var ansArr = actualAnswers;
        ansArr = ansArr.map(function(v, i) {
            return Base64.decode(v);
        });
        return ansArr.indexOf(selectedAns) > -1;
    }

    function getNumberOfAttemptedQuestion() {
        var qs = $filter('filter')(vm.allQuestions, {
            'isSkipped': false
        }) || [];
        return qs.length;
    }

    function getNumberOfRightQuestion() {
        var qs = $filter('filter')(vm.allQuestions, {
            'isRightAnswer': true
        }) || [];
        return qs.length;
    }

    function getNumberOfWrongQuestion() {
        var qs = $filter('filter')(vm.allQuestions, {
            'isRightAnswer': false
        }) || [];
        return qs.length;
    }

    function submitExamReview() {
        if (!vm.isUserLoggedIn()) {
            vm.showLoginAlertModal = true;
            vm.needLoginErrModalMsg = "Login to submit the exam review.";
            return;
        }
        var questions = Object.keys(vm.userTestFinishReview || {});
        var dataToSend = [];

        questions.forEach(function(v, i) {
            var wrongRsn = vm.userTestFinishReview[v];
            var comment = vm.userTestFinishComments[v];
            wrongRsn = Object.keys(wrongRsn || {});
            var data = {
                tst_id: vm.currentExamId ? vm.currentExamId : undefined,
                usr_id: authToken.getUserId(),
                que_id: v,
                tpc_id: vm.activeItem,
                mdl_id: vm.activeModule,
                crs_id: vm.activeCourse,
                wrg_rsn: wrongRsn,
                usr_cmnt: comment
            }
            dataToSend.push(data);
        });



        if (dataToSend.length > 0) {
            admin.saveTestReviews(dataToSend)
                .then(function(res) {
                    alert("Submitted");
                })
        }
    }

    function changeOptionColor(id) {
        var arr = vm.ansArr;
        var isRightAnswer = false;
        vm.userOption[id] = true;
        // $('#' + id).find(".useroption").hide();
        // $('#' + id).find(".useroption_color").hide();
        if (vm.radioOrCheckBox() === 'checkbox') {
            if (arr.indexOf(id) > -1) {
                // $("#" + id).prepend('<span class="fa-stack fa-sm useroption_color" style="color:green;"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-check fa-stack-1x"></i></span>');
                isRightAnswer = true;
            } else {
                // $("#" + id).prepend('<span class="fa-stack fa-sm useroption_color" style="color:red;"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-times fa-stack-1x"></i></span>');
                isRightAnswer = false;
            }
        } else {
            if (arr.indexOf(id) > -1) {
                // $("#" + id).prepend('<div class="useroption_color"><i style="color:green;" class="fa fa-dot-circle-o fa-2x"></i></div>');
                isRightAnswer = true;
            } else {
                // $("#" + id).prepend('<div class="useroption_color"><i style="color:red;" class="fa fa-dot-circle-o fa-2x"></i></div>');
                isRightAnswer = false;
            }
        }
        //Keep track of radio/check box images
        vm.userOptionImg = vm.userOptionImg || {};
        vm.userOptionImg[id] = isRightAnswer;
        return isRightAnswer;
    }

    function questionAlreadyAnswered() {
        vm.questions[vm.currentQuestionIndex]['userAnswers'].forEach(function(v) {
                changeOptionColor(v);
            })
            // setTimeout(function() {
            //     Prism.highlightAll(true);
            // });
        vm.doSyntaxHighlighting();
    }

    function learnMore() {
        if (!vm.isUserLoggedIn()) {
            vm.showLoginAlertModal = true;
            vm.needLoginErrModalMsg = "Login to see explanation of question.";
            return;
        }

        if (!(vm.questions[vm.currentQuestionIndex]['textExplanation'] || vm.questions[vm.currentQuestionIndex]['videoId'])) {
            vm.showGeneralMsgModal = true;
            vm.generalModalMsg = "No explanation found";
            return;
        }
        vm.showGeneralMsgModal = false;

        //Toggle Explanation part
        vm.shouldShowExplanation = true;
        vm.activeRelatedTab = "explanation";

        $scope.$broadcast('pauseVideo');

        setTimeout(function() {
            $('html,body').animate({
                scrollTop: $("#explanation").offset().top - 400
            }, 'slow');
        }, 300)

        setTimeout(function() {
            //If big sidebar is open then
            if ($('.qa-wrapper').hasClass('has-big-sidebar')) {
                $('#explanation').removeClass('has-small-sidebar').addClass('has-big-sidebar');
            }
        }, 0);

        if (vm.shouldShowExplanation) {
            if (!authToken.isAuthenticated()) {
                // return;
            } else {

                getQuestionsNotes();
                setTimeout(function() {
                    $('#text-wrapper').on("mousedown", function(event) {
                        vm.mouseX = event.pageX - $("#text-wrapper").offset().left; //event.clientX;//event.offsetX; //event.pageX - event.currentTarget.offsetLeft;
                        vm.mouseY = event.pageY - $("#text-wrapper").offset().top - 20; //event.offsetY; //event.pageY - event.currentTarget.offsetTop - 50;
                        console.log(vm.mouseX);

                    });
                    $('#text-wrapper').on("mouseup", function(event) {
                        $timeout(function() {
                            if (getSelectionText().length > 0 && getSelectionText().split(" ").length >= 3) {
                                $('.highlight-over')
                                    .css('display', 'flex')
                                    // .css('top', vm.mouseY)
                                    // .css('left', vm.mouseX);
                                    // getSelectionRange();
                            } else {
                                $('.highlight-over')
                                    .css('display', 'none');
                            }
                            if (window.getSelection) {
                                var sel = window.getSelection();
                                if (sel.rangeCount > 0) {
                                    var r = sel.getRangeAt(0);
                                    var customRangeObj = XpathRange.fromRange(r, $("#text-wrapper-inner")[0]);
                                    console.log(customRangeObj);
                                    // console.log(getXPath(r.startContainer));
                                    // console.log(getXPath(r.endContainer));
                                    vm.currentHighlightedRange = customRangeObj;
                                    vm.currentHighlightedText = getSelectionText();


                                    // // console.log(rangy.serializeSelection(undefined,true,$("#text-wrapper-inner")[0]))
                                    // var hig = rangy.createHighlighter($("#text-wrapper-inner")[0], "TextRange");
                                    // var highlightApplier = rangy.createClassApplier("highlight",{
                                    //     onElementCreate : function(ele){
                                    //         console.log("ele created")
                                    //         $(ele).on("click",function(event){
                                    //             event.stopPropagation();
                                    //             vm.mouseOverId = parseInt($(this).attr('h-id'));
                                    //             $('.dehighlight-over').css('display', 'flex');
                                    //         })
                                    //     }
                                    // });
                                    // hig.addClassApplier(highlightApplier);
                                    // hig.highlightSelection("highlight")
                                    // console.log(hig.serialize())
                                    // // type:TextRange|2361$2480$2$highlight$
                                    // // type:TextRange|1504$1630$1$highlight$
                                    // console.log(hig.deserialize("type:TextRange|2361$2480$2$highlight$"))
                                    // // highlightRange(customRangeObj);    
                                }
                            }
                        })

                    });
                });
            }

        }

        vm.questions[vm.currentQuestionIndex]['isRightAnswerTicked'] = true;
        // vm.questions[vm.currentQuestionIndex]['noOfAttempts'] = 0;

        //If user hasn't answered this question 
        //then add this question to Learn Question list `vm.userLearnedQueList`
        // if (!vm.questions[vm.currentQuestionIndex]['isAnswered']) {
        //if it is not already added to `vm.userLearnedQueList`
        vm.questions[vm.currentQuestionIndex]['isLearned'] = true;
        console.log(vm.questions[vm.currentQuestionIndex]);
        // }

        //If this question is in skip question list 
        //then remove from that array
        // if (vm.userSkipAnsList.indexOf(vm.currentQuestionIndex) > -1) {
        //     var pos = vm.userSkipAnsList.indexOf(vm.currentQuestionIndex);
        //     vm.userSkipAnsList.splice(pos, 1);
        // }
        //if(vm.questions[vm.currentQuestionIndex]['isSkipped']){
        vm.questions[vm.currentQuestionIndex]['isSkipped'] = false;
        // }

        //Mark question as a Answered question
        vm.questions[vm.currentQuestionIndex]['isAnswered'] = true;

        //Add right answers to the useranswers array
        vm.questions[vm.currentQuestionIndex]['userAnswers'] = vm.questions[vm.currentQuestionIndex]['options'].map(function(v) {
            return v.id
        }); //vm.questions[vm.currentQuestionIndex]['userAnswers'] || angular.copy(vm.ansArr);

        //Change color for every right answer
        vm.questions[vm.currentQuestionIndex]['options'].forEach(function(v) {
            // console.log(v);
            changeOptionColor(v.id);
        });

        //Set color coding if 
        //There is any code in explanation part
        vm.doSyntaxHighlighting();

        if (vm.isUserLoggedIn()) {
            //update learned question dashboard stats
            dashboardStatsService.incrementQuestionLearnedViews({
                usr_id: authToken.getUserId()
            })
        }
    }

    $scope.$watch('vm.highlights', function(newValue, oldValue, scope) {});


    $(document).on('click', function cb(event) {
        if (event.target.tagName !== 'HL' && event.target.tagName !== 'SUP') {
            $('.dehighlight-over').css('display', 'none')
        }
    })

    function closeHighlightOver() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        $('.highlight-over')
            .css('display', 'none');
    }

    function closeDehighlightOver() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        $('.dehighlight-over')
            .css('display', 'none');
    }

    function getQuestionsNotes() {
        var query = {
            questionId: vm.questions[vm.currentQuestionIndex].questionId,
            userId: authToken.getUserId()
        }
        admin.getQuestionHighlights(query)
            .then(function(d) {
                if (d.data.length <= 0) {
                    vm.nextHighlightId = Math.floor(100000000 + Math.random() * 900000000);
                    vm.highlights = [];
                    return;
                }
                vm.highlights = d.data;
                vm.nextHighlightId = Math.floor(100000000 + Math.random() * 900000000);;
                highlightAllRanges(vm.highlights);
            });
    }

    function highlightRange(customRangeObj, id) {
        try {
            var r = XpathRange.toRange(customRangeObj.start, customRangeObj.startOffset, customRangeObj.end, customRangeObj.endOffset, $("#text-wrapper-inner")[0]);

            var rangyRange = rangy.createRangyRange();
            rangyRange.setStartAndEnd(r.startContainer, r.startOffset, r.endContainer, r.endOffset);
            // console.log(rangyRange);
            var highlightApplier = rangy.createClassApplier("highlight", {
                elementAttributes: {
                    "h-id": id
                },
                onElementCreate: function(ele) {
                    console.log("ele created")
                    $(ele).on("click", function(event) {
                        event.stopPropagation();
                        vm.mouseOverId = parseInt($(this).attr('h-id'));
                        $('.dehighlight-over').css('display', 'flex');
                    })
                }
            });
            highlightApplier.applyToRange(rangyRange)

            // var extractCont = r.cloneContents();

            // r.

            // console.log(r);


            // var span = $("<span></span>");
            // $(span).append(extractCont);

            // var allChild = $(span).find("*");
            // if(allChild.length <= 0){
            //     var hl = $("<hl></hl>");
            //     hl.addClass("highlight highlight-" + id);
            //     hl.attr('h-id', id);
            //     // hl.append(extractCont);
            //     hl.on('click', function(event) {
            //         event.stopPropagation();
            //         vm.mouseOverId = parseInt($(this).attr('h-id'));
            //         console.log("mouse id", vm.mouseOverId);
            //         var mouseX = event.pageX - $("#text-wrapper").offset().left;
            //         var mouseY = event.pageY - $("#text-wrapper").offset().top - 20;

            //         $('.dehighlight-over')
            //             .css('display', 'flex');
            //             // .css('top', mouseY)
            //             // .css('left', mouseX);
            //     })
            //     $(span).wrapInner(hl);
            // }else{
            //     allChild.each(function(i,ele){
            //         console.log(ele.tagName);
            //         if(ele.tagName !== 'BR'){
            //             var hl = $("<hl></hl>");
            //             hl.addClass("highlight highlight-" + id);
            //             hl.attr('h-id', id);
            //             // hl.append(extractCont);
            //             hl.on('click', function(event) {
            //                 event.stopPropagation();
            //                 vm.mouseOverId = parseInt($(this).attr('h-id'));
            //                 console.log("mouse id", vm.mouseOverId);
            //                 var mouseX = event.pageX - $("#text-wrapper").offset().left;
            //                 var mouseY = event.pageY - $("#text-wrapper").offset().top - 20;

            //                 $('.dehighlight-over')
            //                     .css('display', 'flex');
            //                     // .css('top', mouseY)
            //                     // .css('left', mouseX);
            //             })
            //             $(hl).html($(ele).text());
            //             $(ele).replaceWith(hl);
            //         }
            //     })
            // }
            // console.log(span.html());
            // r.insertNode($(span)[0]);


            // var hl = $("<hl></hl>");
            // hl.addClass("highlight");
            // hl.attr('id', id);
            // hl.append(extractCont);
            // hl.on('click', function(event) {
            //     event.stopPropagation();
            //     vm.mouseOverId = parseInt($(this).attr('id'));
            //     console.log("mouse id", vm.mouseOverId);
            //     var mouseX = event.pageX - $("#text-wrapper").offset().left;
            //     var mouseY = event.pageY - $("#text-wrapper").offset().top - 20;

            //     $('.dehighlight-over')
            //         .css('display', 'flex');
            //         // .css('top', mouseY)
            //         // .css('left', mouseX);
            // })
            // r.insertNode($(hl)[0]);
            if (window.getSelection) {
                window.getSelection().removeAllRanges();
            }
        } catch (err) {
            console.log(err);
        }
    }

    function highlightAllRanges(highlights) {
        $timeout(function() {
            highlights.forEach(function(v, i) {
                if (v.range) {
                    highlightRange(v.range, v.id);
                }
            })
        })
    }


    function getSelectionText() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        return text;
    }

    function getSelectionRange() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
            var selection = window.getSelection();
            var anchorNode = selection.anchorNode;

            var prevSib = element = nextSib = "";
            console.log(prevSib, element, nextSib);

            // console.log(anchorNode.previousSibling.tagName);

            prevSib = getXPath(anchorNode.previousSibling) || '';
            nextSib = getXPath(anchorNode.nextSibling) || '';
            element = getXPath(anchorNode.parentNode) || '';

            var realText = $(anchorNode.parentNode).text();
            var pos = realText.indexOf(text);


            vm.note = {
                    parentNode: element,
                    previousNode: prevSib,
                    nextNode: nextSib,
                    startOffset: pos
                }
                // console.log(vm.note)
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        return text;
    }


    function getXPath(element) {
        var xpath = '';
        console.log(element.nodeType, element.parentNode);
        // if(element && element.id === 'text-wrapper-inner'){
        //     return '/';
        // }
        for (; element && element.nodeType == 1 && element.id !== 'text-wrapper-inner' && element.tagName !== 'HL'; element = element.parentNode) {
            var id = $(element.parentNode).children(element.tagName).index(element) + 1;
            id > 1 ? (id = '[' + id + ']') : (id = '');
            xpath = '/' + element.tagName.toLowerCase() + id + xpath;
        }
        return xpath;
    }

    function getElementFromXpath(xpath) {
        var element = document.evaluate('./' + xpath, document.getElementById('text-wrapper-inner'), null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        return element;
    }

    function showHighlightedNotes(ele, offset, id, index) {

        vm.cloneElement = vm.cloneElement || $("#text-wrapper-inner").clone();
        var element = $(vm.cloneElement).children($(ele)[0])[0];

        console.log(element);
        if (element.nodeType !== 3) {
            var text = element.innerHTML;
            text = text.replace(/&nbsp;/g, " ");
            if (text.indexOf(" ", offset) > -1) {
                var start = text.substr(0, offset);
                var space_pos = text.indexOf(" ", offset);
                var word = "<hl class='highlight' id=" + id + "><sup>[" + index + "]</sup>" + text.substr(offset, (space_pos - offset)) + "</hl>";
                var end = text.substr(space_pos);
                element.innerHTML = start + word + end;
            } else {
                var start = "",
                    end = "";
                if (offset === 0) {
                    start = "<hl class='highlight' id=" + id + "><sup>[" + index + "]</sup>" + text.substr(offset) + "</hl>";
                } else {
                    start = text.substr(0, offset);
                    end = "<hl class='highlight' id=" + id + "><sup>[" + index + "]</sup>" + text.substr(offset) + "</hl>";
                }
                element.innerHTML = start + end;
            }
        } else if (element.nodeType === 3) {
            var text = element.nodeValue;
            if (text.indexOf(" ", offset) > -1) {
                var start = text.substr(0, offset);
                var space_pos = text.indexOf(" ", offset);
                var word = "<hl class='highlight' id=" + id + "><sup>[" + index + "]</sup>" + text.substr(offset, (space_pos - offset)) + "</hl>";
                var end = text.substr(space_pos);
                var span = document.createElement('hl');
                span.innerHTML = start + word + end;
                element.parentNode.replaceChild(span, element);
            } else {
                var start = "",
                    end = "";
                if (offset === 0) {
                    start = "<hl class='highlight' id=" + id + "><sup>[" + index + "]</sup>" + text.substr(offset) + "</hl>";
                } else {
                    start = text.substr(0, offset);
                    end = "<hl class='highlight' id=" + id + "><sup>[" + index + "]</sup>" + text.substr(offset) + "</hl>";
                }
                var span = document.createElement('hl');
                span.innerHTML = start + end;
                element.parentNode.replaceChild(span, element);
            }

        }
        //Unwrap all HL tag who doesn't have highlight class
        $('hl:not(".highlight")').contents().unwrap();

        $('hl.highlight sup').on('click', function cb(event) {
            vm.mouseOverId = parseInt($(this).parent().attr('id'));

            var mouseX = event.offsetX; //event.pageX - event.target.offsetParent.offsetLeft;
            var mouseY = event.offsetY; //event.pageY - event.target.offsetParent.offsetTop - 50;
            $('.dehighlight-over')
                .css('display', 'flex')
                // .css('top', mouseY)
                // .css('left', mouseX);
        })
        $(document).on('click', function cb(event) {
            if (event.target.tagName !== 'HL' && event.target.tagName !== 'SUP') {
                $('.dehighlight-over').css('display', 'none')
            }
        })


    }

    function saveNote_old() {
        var note = vm.form.highlightNote;

        vm.note.note = note;
        vm.note.id = vm.nextHighlightId;
        vm.note.questionId = vm.questions[vm.currentQuestionIndex].questionId;
        vm.note.userId = authToken.getUser()["usr_id"];

        vm.closeSideBarInSmallScreen();

        admin.saveHighlightedText(vm.note)
            .then(function(d) {
                $('#' + vm.note.id + ' sup').remove();
                $('#' + vm.note.id).contents().unwrap();

                vm.form.highlightNote = "";
                vm.nextHighlightId = Math.floor(100000000 + Math.random() * 900000000);
                var index = 0;
                var arr = $.grep(vm.highlights, function(e) {
                    return e.id === vm.note.id;
                });
                if (arr.length <= 0) {
                    //If this is the first time, we are adding this to arr
                    vm.highlights.push(vm.note);
                    index = vm.highlights.length;
                } else {
                    //If it is already there then just update it
                    vm.highlights = vm.highlights.map(function(v, i) {
                        if (v.id === vm.note.id) {
                            index = i + 1;
                            return vm.note;
                        }
                        return v;
                    })
                }


                var isFound = false;
                var element = undefined;
                if (vm.note.previousNode) {
                    var xpath = vm.note.previousNode;
                    var node = getElementFromXpath(xpath);
                    if (node && node.nextSibling) {
                        element = node.nextSibling;
                        isFound = true;
                    }
                }
                if (vm.note.nextNode && !isFound) {
                    var xpath = vm.note.nextNode;
                    var node = getElementFromXpath(xpath);
                    if (node && node.previousSibling) {
                        element = node.previousSibling;
                        isFound = true;
                    }
                }
                if (vm.note.parentNode && !isFound) {
                    var xpath = vm.note.parentNode;
                    var node = getElementFromXpath(xpath);
                    if (node) {
                        element = node;
                        isFound = true;
                    }
                }

                if (element && isFound) {
                    showHighlightedNotes(node, vm.note.startOffset, vm.note.id, index);
                }



            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function saveNote() {
        var note = vm.form.highlightNote;
        vm.note.note = note;
        vm.note.id = vm.nextHighlightId;
        vm.note.questionId = vm.questions[vm.currentQuestionIndex].questionId;
        vm.note.userId = authToken.getUserId();

        if (!vm.isHighlightedNoteInEdit) {
            vm.note.range = vm.currentHighlightedRange;
            vm.note.selectedText = vm.currentHighlightedText;
            vm.note.timestamp = new Date();
        } else {
            var note = ($filter('filter')(vm.highlights, {
                id: vm.nextHighlightId
            }) || [])[0];
            if (note) {
                vm.note.range = note.range;
                vm.note.selectedText = note.selectedText;
            }
        }

        vm.note.itemId = vm.activeItem;
        vm.note.courseId = vm.activeCourse;

        vm.closeSideBarInSmallScreen();

        admin.saveHighlightedText(vm.note)
            .then(function(d) {

                if (vm.currentHighlightedRange && !vm.isHighlightedNoteInEdit) {
                    highlightRange(vm.currentHighlightedRange, vm.nextHighlightId);

                }

                vm.isHighlightedNoteInEdit = false;

                var arr = $.grep(vm.highlights, function(e) {
                    return e.id === vm.note.id;
                });
                if (arr.length <= 0) {
                    //If this is the first time, we are adding this to arr
                    var d = {};
                    angular.copy(vm.note, d)
                    vm.highlights.push(d);
                    index = vm.highlights.length;
                } else {
                    //If it is already there then just update it
                    vm.highlights = vm.highlights.map(function(v, i) {
                        if (v.id === vm.note.id) {
                            index = i + 1;
                            return vm.note;
                        }
                        return v;
                    })
                }
                vm.form.highlightNote = "";
                vm.nextHighlightId = Math.floor(100000000 + Math.random() * 900000000);
            })
            .catch(function(err) {
                console.error(err);
            })
    }


    function deleteNote_old(id) {
        console.log(id);
        admin.deleteHighlightedText({
                id: id,
                userId: authToken.getUser()["usr_id"]
            })
            .then(function(d) {
                console.log(d);
                vm.highlights = vm.highlights.filter(function(v) {
                    if (v.id === id)
                        return false;
                    return true;
                });
                $('hl sup').remove();
                $('hl').contents().unwrap();

                vm.highlights.forEach(function(v, i) {
                    var isFound = false;
                    var element = undefined;
                    if (v.previousNode) {
                        var xpath = v.previousNode;
                        var node = getElementFromXpath(xpath);
                        if (node && node.nextSibling) {
                            element = node.nextSibling;
                            isFound = true;
                        }
                    }
                    if (v.nextNode && !isFound) {
                        var xpath = v.nextNode;
                        var node = getElementFromXpath(xpath);
                        if (node && node.previousSibling) {
                            element = node.previousSibling;
                            isFound = true;
                        }

                    }
                    if (v.parentNode && !isFound) {
                        var xpath = v.parentNode;
                        var node = getElementFromXpath(xpath);
                        if (node) {
                            element = node;
                            isFound = true;
                        }
                    }

                    if (element && isFound) {
                        console.log(element);
                        showHighlightedNotes(element, v.startOffset, v.id, i + 1);
                    }
                })
            })
            .catch(function(d) {
                console.error(d);
            })
    }

    function deleteNote(id) {
        vm.noteGoingToBeDelete = id;
        vm.showDeleteNoteModal = true;
    }

    function closeDeleteNoteModal(id) {
        vm.noteGoingToBeDelete = undefined;
        vm.showDeleteNoteModal = false;
    }

    function deleteNoteConfirm() {
        var id = vm.noteGoingToBeDelete;
        admin.deleteHighlightedText({
                id: id,
                userId: authToken.getUser()["usr_id"]
            })
            .then(function(d) {
                vm.noteGoingToBeDelete = undefined;
                vm.showDeleteNoteModal = false;
                vm.highlights = vm.highlights.filter(function(v) {
                    if (v.id === id) {
                        vm.deletedHighlight = v;
                        return false;
                    }
                    return true;
                });
                $("[h-id=" + id + "]").contents().unwrap();
            })
            .catch(function(d) {
                console.error(d);
            })
    }

    function editNote(id) {
        var note = vm.highlights.filter(function(v) {
            if (v.id === id)
                return true;
            return false;
        })[0];

        vm.note = note;
        if (note && note._id) {
            delete note._id;
        }
        console.log(note);
        vm.nextHighlightId = note.id;
        vm.form = vm.form || {};
        vm.form.highlightNote = note.note;
        vm.isHighlightedNoteInEdit = true;

        $('.note-list .note').removeAttr('style');
        vm.resetAllSidebar();
        vm.activeRelatedTab = "notes";
        vm.shouldShowContentWrapper = true;
        setTimeout(function() {
            $('.highlight-over').css('display', 'none');
            $('#highlightedNote').focus();
        });
    }

    vm.editNoteInPlace = editNoteInPlace;

    function editNoteInPlace(id) {
        var note = ($filter('filter')(vm.highlights, {
            id: id
        }) || [])[0];

        if (note) {
            if (note && note._id) {
                delete note._id;
            }
            vm.practiceNoteEditingStatus = vm.practiceNoteEditingStatus || {};
            vm.practiceNoteEditingStatus[id] = {
                editing: true
            }
        }
    }

    vm.saveNoteInPlace = saveNoteInPlace;

    function saveNoteInPlace(id) {
        var note = ($filter('filter')(vm.highlights, {
            id: id
        }) || [])[0];
        if (note) {
            admin.saveHighlightedText(note)
                .then(function(d) {
                    vm.practiceNoteEditingStatus = vm.practiceNoteEditingStatus || {};
                    vm.practiceNoteEditingStatus[id] = {
                        editing: false
                    }
                })
                .catch(function(err) {
                    console.error(err);
                })
        }
    }

    function doesHighlightContainNote(id) {
        var note = ($filter('filter')(vm.highlights, {
            id: id
        }) || [])[0];
        // var note = vm.highlights.filter(function(v) {
        //     if (v.id === id)
        //         return true;
        //     return false;
        // })[0];
        if (note && note.note) {
            return true;
        } else {
            return false;
        }
    }

    function showNote(id) {
        vm.addHighlightNote();
        setTimeout(function() {
            $('.note-list .note').removeAttr('style');
            $('#note_' + id).parent().css('background-color', 'yellow');
        })

    }

    $scope.$watch('vm.shouldShowNote', function(newValue, oldValue, scope) {
        // console.log('vm.shouldShowNote',vm.shouldShowNote);
        if (!vm.shouldShowNote) {
            $(".qa-wrapper").removeClass('with-add-note');
            $(".qa-wrapper").removeAttr('style');
        } else {
            $(".qa-wrapper").addClass('with-add-note');
        }
    });

    function addNotes() {
        vm.resetAllSidebar();

        vm.shouldShowNote = true;
        setTimeout(function() {
            vm.openSideBarInSmallScreen(null, false);
        })
    }

    function addHighlightNote() {
        vm.resetAllSidebar();
        // vm.shouldShowNote = true;
        vm.activeRelatedTab = "notes";
        vm.shouldShowContentWrapper = true;
        setTimeout(function() {
            // vm.openSideBarInSmallScreen();

            $('.highlight-over').css('display', 'none');
            $('#highlightedNote').focus();
            vm.form = vm.form || {};
            vm.form.highlightNote = vm.currentHighlightedText;
        });
    }


    //Only highlight selected text without noting down anything
    function onlyDoHighlight() {
        vm.resetAllSidebar();
        vm.shouldShowContentWrapper = true;
        setTimeout(function() {
            $('.highlight-over').css('display', 'none');
            vm.form = vm.form || {};
            vm.form.highlightNote = "";
            saveNote();
        });
    }

    $scope.$watch('vm.shouldShowFeedback', function(newValue, oldValue, scope) {
        // console.log('vm.shouldShowFeedback',vm.shouldShowFeedback);
        if (!vm.shouldShowFeedback) {
            $(".qa-wrapper").removeClass('with-feedback-form');
        } else {
            $(".qa-wrapper").addClass('with-feedback-form');
        }
    });

    function showFeedback(shouldOpenSidebar) {
        if (vm.shouldShowFeedback) {
            vm.resetAllSidebar();
            vm.shouldShowContentWrapper = true;
            vm.shouldShowFeedback = false;
            return;
        }
        if (vm.isPractice) {
            vm.resetAllSidebar();
            vm.shouldShowContentWrapper = false;
            vm.shouldShowFeedback = true;
            setTimeout(function() {
                if (shouldOpenSidebar != false) {
                    vm.openSideBarInSmallScreen();
                }
            })
        }
    }

    $scope.$watch('vm.shouldShowQuestionList', function(newValue, oldValue, scope) {
        // console.log('vm.shouldShowQuestionList',vm.shouldShowQuestionList);
        if (!vm.shouldShowQuestionList) {
            $(".qa-wrapper").removeClass('with-question-list');
        } else {
            $(".qa-wrapper").addClass('with-question-list');
        }
    });

    function showQuestionList() {
        var temp = vm.shouldShowQuestionList;
        vm.resetAllSidebar();
        vm.openBigSidebar();
        vm.shouldShowQuestionList = true;
    }

    $scope.$watch('vm.shouldShowShareWidget', function(newValue, oldValue, scope) {
        if (!vm.shouldShowShareWidget) {
            $(".qa-wrapper").removeClass('with-share-widget');
        } else {
            $(".qa-wrapper").addClass('with-share-widget');
        }
    });

    function showShareWidget() {
        var temp = vm.shouldShowShareWidget;
        vm.resetAllSidebar();
        vm.shouldShowShareWidget = temp;
        vm.doSyntaxHighlighting();
        // setTimeout(function() {
        //     Prism.highlightAll(true);
        // });
        vm.shouldShowShareWidget = true;
    }

    function showReport() {
        if (!vm.isUserLoggedIn()) {
            vm.showLoginAlertModal = true;
            vm.needLoginErrModalMsg = "Login to report the question.";
            return;
        }
        vm.resetAllSidebar();
        vm.shouldShowReport = true;
        setTimeout(function() {
            vm.openSideBarInSmallScreen();
        })
    }

    function showContentWrapper() {
        vm.resetAllSidebar();
        vm.shouldShowContentWrapper = true;
    }

    function shareHighlight() {
        vm.resetAllSidebar();
        vm.shouldShowShareWidget = true;
        $('.highlight-over').css('display', 'none');
        var text = getSelectionText();
        // var url = window.location.origin + '/app/share?title=' + text + "&desc=" + text + "&url=" + encodeURIComponent(window.location.href);
        utility.shareToEWProfile("Highlight", text);
        // window.open(url, "_blank", "height=500,width=620");
    }

    function shareHighlightOnQA() {
        vm.resetAllSidebar();
        vm.shouldShowShareWidget = true;
        $('.highlight-over').css('display', 'none');
        var text = getSelectionText();
        // var url = window.location.origin + '/app/share?title=' + text + "&desc=" + text + "&url=" + encodeURIComponent(window.location.href);
        utility.shareToQA("Highlight", text);
        // window.open(url, "_blank", "height=500,width=620");
    }

    //This will hide all sidebar
    function resetAllSidebar() {
        vm.shouldShowNote = false;
        vm.shouldShowFeedback = false;
        vm.shouldShowQuestionList = false;
        vm.shouldShowShareWidget = false;
        vm.shouldShowContentWrapper = false;
        vm.shouldShowReport = false;
    }

    function shouldShowCloseSidebarButton() {
        return (vm.shouldShowNote ||
            vm.shouldShowFeedback ||
            vm.shouldShowQuestionList ||
            vm.shouldShowShareWidget);
    }

    function goToQuestion(index) {
        var last = vm.currentQuestionIndex;
        for (var i = last; i <= index; i++) {
            // if (vm.userSkipAnsList.indexOf(i) <= -1 && vm.userRightAnsList.indexOf(i) <= -1 && vm.userWrongAnsList.indexOf(i) <= -1 && vm.userLearnedQueList.indexOf(i) <= -1) {
            //     vm.userSkipAnsList.push(i);
            //     vm.questions[vm.currentQuestionIndex]['isSkipped'] = true;
            // }
            if (vm.questions[vm.currentQuestionIndex]['isAnswered'] === undefined && vm.questions[vm.currentQuestionIndex]['markForReview'] === undefined) {
                vm.questions[vm.currentQuestionIndex]['isSkipped'] = true;
            }
        }
        vm.currentQuestionIndex = index;
        setQueConfig();
    }

    function markForReview() {
        if (!vm.isUserLoggedIn()) {
            vm.showLoginAlertModal = true;
            vm.needLoginErrModalMsg = "Login to mark question for review later.";
            return;
        }
        vm.questions[vm.currentQuestionIndex]['isSkipped'] = false;
        vm.questions[vm.currentQuestionIndex]['markForReview'] = !vm.questions[vm.currentQuestionIndex]['markForReview'];
    }

    function showTextTab() {
        vm.showTextExaplanation = true;
        $scope.$broadcast('pauseVideo');
    }

    function showVideoTab() {
        if (vm.isAlreadySetup) {
            vm.showTextExaplanation = false;
            $scope.$broadcast('playVideo');
            return;
        }
        vm.restartVideoPlayer = false;
        $timeout(function() {
            vm.restartVideoPlayer = true;
            vm.showTextExaplanation = false;
            $timeout(function() {
                var videoId = vm.questions[vm.currentQuestionIndex]['videoId'];
                admin.findVideoEntityById({
                        videoId: videoId
                    })
                    .then(function(res) {
                        var v = res.data[0];
                        videoUrl.fetchVideoUrl(v)
                            .then(function(video) {
                                console.log(video);
                                var url = video.url;
                                $scope.$broadcast('loadVideo', url);
                                vm.isAlreadySetup = true;
                            })
                            .catch(function(err) {
                                console.log(err);
                            });
                    });
            }, 100);
        }, 200);
    }

    function increaseFont() {
        var current = parseInt($('#text-wrapper').css('font-size'));
        if (current < vm.MAX_FONT_SIZE) {
            $('#text-wrapper').css('font-size', (current + 1) + 'px');
        }
    }

    function decreaseFont() {
        var current = parseInt($('#text-wrapper').css('font-size'));
        if (current > vm.MIN_FONT_SIZE) {
            $('#text-wrapper').css('font-size', (current - 1) + 'px');
        }
    }

    function increaseWidth() {
        var eleWidth = parseInt($('#text-wrapper').css('width'));
        var parentWidth = parseInt($('#text-wrapper').parent().css('width'));
        var percent = Math.round((eleWidth / parentWidth) * 100);
        if ((percent + 5) <= vm.MAX_WIDTH) {
            $('#text-wrapper').css('width', (percent + 5) + '%');
        }
    }

    function decreaseWidth() {
        var eleWidth = parseInt($('#text-wrapper').css('width'));
        var parentWidth = parseInt($('#text-wrapper').parent().css('width'));
        var percent = Math.round((eleWidth / parentWidth) * 100);
        if ((percent - 5) >= vm.MIN_WIDTH) {
            $('#text-wrapper').css('width', (percent - 5) + '%');
        }
    }

    function closeNoteSidebar() {
        resetAllSidebar();
        $('.qa-wrapper').removeAttr('style');
    }

    function openBigSidebar(sidebarItem) {
        $('.third-strip-wrapper').removeClass('closed').addClass('open');
        $('.head-bar-wrapper').removeClass('has-small-sidebar').addClass('has-big-sidebar');
        $('.qa-wrapper').removeClass('has-small-sidebar').addClass('has-big-sidebar');
        $('.video-wrapper').removeClass('has-small-sidebar').addClass('has-big-sidebar');
        $('.explanation-wrapper').removeClass('has-small-sidebar').addClass('has-big-sidebar');
        // if (sidebarItem === "question") {
        //     vm.showQuestionList();
        // } else if (sidebarItem === "note") {
        //     vm.showNote();
        // } else if (sidebarItem === "share") {
        //     vm.showShareWidget();
        // } else if (sidebarItem === "feedback") {
        //     vm.showFeedback();
        // }

        $('.sidebar').css('display', 'block');
        vm.isBigSidebarOpen = true;
        // $('#main-content-wrapper').css('padding-left','410px');
        // setTimeout(changeVideoHeight, 10);
    }

    function closeBigSidebar() {
        vm.resetBigSidebar();
        $('.third-strip-wrapper').addClass('closed').removeClass('open');
        // $('.head-bar-wrapper').addClass('has-small-sidebar').removeClass('has-big-sidebar');
        $('.qa-wrapper').addClass('has-small-sidebar').removeClass('has-big-sidebar');
        $('.video-wrapper').addClass('has-small-sidebar').removeClass('has-big-sidebar');
        $('.explanation-wrapper').addClass('has-small-sidebar').removeClass('has-big-sidebar');
        $('.sidebar').css('display', 'none');
        // setTimeout(changeVideoHeight, 10);
        // $('#main-content-wrapper').css('padding-left','60px');
        vm.isBigSidebarOpen = false;
    }

    function toggleBigSidebar() {
        if (vm.isBigSidebarOpen) {
            vm.closeBigSidebar();
        } else {
            vm.openBigSidebar();
        }
    }

    function resetBigSidebar() {
        vm.bigSidebarWidth = vm.bigSidebarWidthMin;
        $('.third-strip-wrapper').attr('style', '');
        // $('.head-bar-wrapper').attr('style','');
        $('.qa-wrapper').attr('style', '');
        $('.video-wrapper').attr('style', '');
        $('.explanation-wrapper').attr('style', '');
        // setTimeout(changeVideoHeight, 10);
    }



    function increaseSidebarWidth() {
        if (vm.bigSidebarWidth < vm.bigSidebarWidthMax) {
            vm.bigSidebarWidth += 10;
            $('.third-strip-wrapper').css('width', vm.bigSidebarWidth + '%');
            // $('.head-bar-wrapper').css('right',vm.bigSidebarWidth + '%');
            $('.qa-wrapper').css('margin-right', vm.bigSidebarWidth + '%');
            $('.video-wrapper').css('margin-right', vm.bigSidebarWidth + '%');
            $('.explanation-wrapper').css('margin-right', vm.bigSidebarWidth + '%');
        }
    }

    function decreaseSidebarWidth() {
        if (vm.bigSidebarWidth > vm.bigSidebarWidthMin) {
            vm.bigSidebarWidth -= 10;
            $('.third-strip-wrapper').css('width', vm.bigSidebarWidth + '%');
            // $('.head-bar-wrapper').css('right',vm.bigSidebarWidth + '%');
            $('.qa-wrapper').css('margin-right', vm.bigSidebarWidth + '%');
            $('.video-wrapper').css('margin-right', vm.bigSidebarWidth + '%');
            $('.explanation-wrapper').css('margin-right', vm.bigSidebarWidth + '%');
        }
    }

    function resetReadingPref() {
        vm.openWidthPref = false;
        vm.openFontPref = false;
        $('#text-wrapper').css('width', (70) + '%');
        $('#text-wrapper').css('font-size', (18) + 'px');
    }

    function changeVideoHeight() {
        var windowH = $(window).height() - 50 - 20;
        //50px header and 20px padding
        $('video-controls-jw').removeAttr('style');
        if ($('video-controls-jw').hasClass('expanded')) {
            var width = $('video-controls-jw').width();
            var height = width * (1 / aspect);
            // console.log("width...", width, "height.....", height);
            if (height > windowH) {
                // console.log('width');
                $('video-controls-jw').height(windowH);
                width = windowH * aspect;
                $('video-controls-jw').width(width);
                $('video-controls-jw').css('margin-right', 'auto');
                $('video-controls-jw').css('margin-left', 'auto');
            } else {
                // console.log('not width');
                $('video-controls-jw').height(height);
                $('video-controls-jw').css('margin-right', '');
                $('video-controls-jw').css('margin-left', '');
                $('video-controls-jw').css('width', '');
            }
            $('#video-left-sidebar').css('margin-top', '0px');
        } else {
            var width = $('video-controls-jw').width();
            var height = width * (1 / aspect);
            // console.log("width...", width, "height.....", height);
            if (height > windowH) {
                $('video-controls-jw').height(windowH);
                var width = windowH * aspect;
                $('video-controls-jw').width(width);

                $('video-controls-jw').css('margin-right', 'auto');
                $('video-controls-jw').css('margin-left', 'auto');

                $('#video-left-sidebar').css('margin-top', -windowH - 10);
            } else {
                $('video-controls-jw').height(height);
                $('#video-left-sidebar').css('margin-top', -height - 10);

                $('video-controls-jw').css('margin-right', '');
                $('video-controls-jw').css('margin-left', '');
                $('video-controls-jw').css('width', '');
            }
        }
    }

    //Change exam / practice mode and load item
    function changeModeAndLoadItem(isPractice) {
        vm.isPractice = isPractice;
        loadItem(vm.activeModule, vm.activeItem, false, true);
    }

    //Load question gropu/video item
    function loadItem(moduleId, itemId, firstPageLoad, shouldNotLoadAgain) {
        if (!shouldNotLoadAgain) {
            vm.closeSideBarInSmallScreen();
        }
        vm.fullCourse = {};
        vm.shouldShowSlideModal = false;
        var moduleItem = $filter('filter')(vm.fullCourse.moduleDetail, {
            moduleId: moduleId
        });
        moduleItem = moduleItem || {};
        var item = $filter('filter')(moduleItem.moduleItems, {
            itemId: itemId
        });
        item = item || {};

        moduleItem.isOpen = true;
        vm.activeItem = itemId;
        vm.activeModule = moduleId;
        vm.activeModuleName = moduleItem.moduleName;
        vm.activeItemName = item.itemName;
        vm.activeItemMaterialInfo = item.materialInfo;
        vm.activeItemFullObject = item;
        vm.activeItemFullObject.itemOptionType = vm.activeItemFullObject.itemOptionType || "capital-alpha";

        var obj = {};
        angular.copy($stateParams, obj);
        obj.item = vm.activeItem;
        // $state.transitionTo($state.current, obj, {
        //     reload: false,
        //     notify: false
        // });
        vm.userAuthorized = true;
        vm.isPageContentLoaded = true;
        
        setTimeout(function() {
            // $('body').removeAttr('style');
            $('body').css('overflow-y', 'scroll');
            $('body').css('overflow-x', 'hidden');
        }, 100)


        setTimeout(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 40);
        })

        if (item.itemType === "video") {
            // If it is video item , redirect user to video page
            var href = $state.href('video', {
                course: vm.activeCourse,
                item: vm.activeItem
            })
            window.open(href, "video");
        } else {
            //if it is questions group 
            vm.totalTime = undefined;
            vm.isVideoWrapper = false;
            vm.isPageContentLoaded = true;
            vm.isVideoPlaying = true;
            vm.activeItemType = 'qGroup';
            vm.isCurrentTestFinish = false;
            vm.questionsTimer = {};
            vm.isCurrentTestStarted = false;
            vm.questions = [];
            vm.allQuestions = [];

            vm.userRightAnsList = [];
            vm.userWrongAnsList = [];
            vm.userLearnedQueList = [];
            vm.userSkipAnsList = [];

            if (firstPageLoad) {
                vm.isPractice = true;
            }

            if (item.itemTakeTest) {

                //if it is exam
                vm.isPractice = false;
                vm.examTime = item.itemQuestionsTime * 60;
                vm.currentExamId = uuid.v4();
                vm.testStartTime = new Date();
                vm.allQuestionsLoaded = true;
                // if(vm.allThingsLoaded){
                pageLoader.hide();
                // }

                //we will return from here and show strt button and when user will click that button we will fetch questions 
                return;
            } else {
                vm.isPractice = true;

                fetchAllQuestions(item.itemQuestions);
            }

        }
    }

    function showStartExamModal() {
        vm.shouldShowStartExamModal = true;
    }

    function showFinishExamModal() {
        vm.shouldShowFinishExamModal = true;
    }
    // MathJax.Hub.signal.Interest(
    //   function (message) {console.log("Hub: "+message)}
    // );
    // MathJax.Hub.Register.MessageHook("End Process", function (message) {
    //   vm.isQuestionProcessing = false;
    // })
    // MathJax.Hub.Register.MessageHook("Begin Process", function (message) {
    //   vm.isQuestionProcessing = true;
    // })

    function fetchAllQuestions(questions) {
        // if(vm.questionData){
        //     vm.allQuestions = vm.questions;
        //     vm.allQuestionsLoaded = true;
        //     vm.currentQuestionIndex = 0;
        //     setQueConfig();
        //     return;
        // }
        vm.shouldShowStartExamModal = false;
        vm.isCurrentTestStarted = true;
        var query = {
            topicGroup: [vm.activeTopic],
        }
        admin.findPreviewPracticeQuestionsByQuery(query)
            .then(function(d) {
                if (d.data.length > 0) {
                    console.log("allQuestionsLoaded");
                    vm.questions = d.data;
                    vm.allQuestions = d.data;
                    vm.questionsCopy = angular.copy(d.data);
                    vm.allQuestionsLoaded = true;
                    if (shouldFilterQuestion()) {
                        //if filter is set already then show only filter questions
                        doFilterQuestion();
                        return;
                    }

                    // if(!vm.isPractice){
                    //     //if it is exam
                    //     //randomize all options

                    // }
                    vm.isQuestionsSequential = true;

                    if (vm.activeQuestion) {
                        //Direct go to  Question Number if passed in url
                        var q = $filter('filter')(vm.allQuestions, {
                            questionId: vm.activeQuestion
                        })[0];
                        if (q) {
                            //if question found then go to that question`
                            var pos = vm.allQuestions.indexOf(q);
                            goToQuestion(pos);
                            vm.activeQuestion = undefined;
                        } else {
                            //Start From First Question
                            vm.currentQuestionIndex = 0;
                            setQueConfig();
                        }
                    } else {
                        //Start From First Question
                        vm.currentQuestionIndex = 0;
                        setQueConfig();
                    }
                }
            })
            .finally(function() {
                // if(!vm.allThingsLoaded){
                //dont hide
                // }else{
                pageLoader.hide();
                // }
            })
    }

    function sequentialQuestions() {
        if (!vm.isUserLoggedIn()) {
            vm.showLoginAlertModal = true;
            vm.needLoginErrModalMsg = "Login to change order of the questions.";
            return;
        }
        vm.isQuestionsSequential = !vm.isQuestionsSequential;
        vm.currentQuestionIndex === 0;

        if (!vm.isQuestionsSequential) {
            vm.allQuestions.shuffle();
            setQueConfig();
        } else if (vm.isQuestionsSequential) {
            vm.questions = vm.allQuestions = angular.copy(vm.questionsCopy);
            vm.currentQuestionIndex = 0;
            setQueConfig();
        }
    }

    function randomizeQuestions() {
        vm.isQuestionsRandomize = !vm.isQuestionsRandomize;
        if (vm.currentQuestionIndex === 0 && vm.isQuestionsRandomize) {
            vm.questions.shuffle();
            setQueConfig();
        } else if (vm.currentQuestionIndex === 0 && !vm.isQuestionsRandomize) {
            vm.questions = angular.copy(vm.questionsCopy);
            vm.currentQuestionIndex = 0;
            setQueConfig();
        }
    }

    function randomizeOptions() {
        vm.isOptionsRandomize = !vm.isOptionsRandomize;
        if (vm.isOptionsRandomize) {
            vm.questions.forEach(function(v) {
                v.options.shuffle();
            });
        }
    }


    function trustAsHTML(str) {
        return $sce.trustAsHtml(str);
    }


    //Return Number of review questions selected by user
    function noOfReviewQuestion() {
        var qs = $filter('filter')(vm.allQuestions, {
            'markForReview': true
        }) || [];
        return qs.length;
    }

    //Return Number of questions learned by user
    function noOfLearnedQuestion() {
        var qs = $filter('filter')(vm.allQuestions, {
            'isLearned': true
        }) || [];
        return qs.length;
    }

    //Return Number of skipped questions by user
    function noOfSkipQuestion() {
        var qs = $filter('filter')(vm.allQuestions, {
            'isSkipped': true
        }) || [];
        return qs.length;
    }

    function resetToAllQuestion() {
        vm.currentQuestionIndex = 0;
        vm.questions = vm.allQuestions;
        setQueConfig();
    }


    function showOnlyReviewQuestion() {
        $timeout(function() {
            vm.isShowingLearnOnly = false;
            vm.isShowingSkipOnly = false;

            if (!vm.isShowingReviewOnly) {
                vm.lastActiveQuestionIndex = vm.currentQuestionIndex;
            }

            resetToAllQuestion();
            if (vm.isShowingReviewOnly) {
                //Already showing 
                vm.currentQuestionIndex = vm.lastActiveQuestionIndex;
                vm.questions = vm.allQuestions;
                setQueConfig();
                //Now close
                vm.isShowingReviewOnly = false;
            } else {
                var qs = $filter('filter')(vm.questions, {
                    'markForReview': true
                });
                // console.log(qs);
                vm.isShowingReviewOnly = true;
                vm.currentQuestionIndex = 0;
                vm.questions = qs;
                setQueConfig();
            }
            question_load_stats();
        })

    }

    function showOnlyLearnQuestion() {
        vm.isShowingReviewOnly = false;
        vm.isShowingSkipOnly = false;

        if (!vm.isShowingLearnOnly) {
            vm.lastActiveQuestionIndex = vm.currentQuestionIndex;
        }

        resetToAllQuestion();
        if (vm.isShowingLearnOnly) {
            //Already showing 
            vm.currentQuestionIndex = vm.lastActiveQuestionIndex;
            vm.questions = vm.allQuestions;
            setQueConfig();
            //Now close
            vm.isShowingLearnOnly = false;
        } else {
            var qs = $filter('filter')(vm.questions, {
                'isLearned': true
            });
            console.log(qs);
            vm.isShowingLearnOnly = true;
            vm.currentQuestionIndex = 0;
            vm.questions = qs;
            setQueConfig();
        }
    }

    function showOnlySkipQuestion() {
        $timeout(function() {
            vm.isShowingLearnOnly = false;
            vm.isShowingReviewOnly = false;

            if (!vm.isShowingSkipOnly) {
                vm.lastActiveQuestionIndex = vm.currentQuestionIndex;
                console.log(vm.lastActiveQuestionIndex)
            }


            resetToAllQuestion();
            if (vm.isShowingSkipOnly) {
                //Already showing 
                vm.currentQuestionIndex = vm.lastActiveQuestionIndex || 0;
                vm.questions = vm.allQuestions;
                setQueConfig();
                //Now close
                vm.isShowingSkipOnly = false;
            } else {
                var qs = $filter('filter')(vm.questions, {
                    'isSkipped': true
                });
                vm.isShowingSkipOnly = true;
                vm.currentQuestionIndex = 0;
                vm.questions = qs;
                setQueConfig();
            }
            question_load_stats();
        })
    }

    function showOnlyRightQuestion() {
        resetToAllQuestion();
        var qs = $filter('filter')(vm.questions, {
            'isRightAnswer': true
        });
        vm.currentQuestionIndex = 0;
        vm.questions = qs;
        setQueConfig();
    }

    function showOnlyWrongQuestion() {
        resetToAllQuestion();
        var qs = $filter('filter')(vm.questions, {
            'isRightAnswer': false
        });
        vm.currentQuestionIndex = 0;
        vm.questions = qs;
        setQueConfig();
    }

    //show only Wrong and skipped question
    function showOnlyWrongAndSkippedQuestion() {
        resetToAllQuestion();

        var qs = vm.questions.filter(function(v, i) {
            if (!v.isRightAnswer || v.isSkipped) {
                return true;
            }
            return false;
        })

        console.log(qs);
        vm.currentQuestionIndex = 0;
        vm.questions = qs;
        setQueConfig();
    }

    //Open Sidebar
    function openSideBarInSmallScreen(event, shouldClose) {
        if (event) {
            event.stopPropagation();
        }
        if (!vm.isSidebarOpenInSmallScreen) {
            // open it
            $("#course-sidebar").addClass('ds-open');
            vm.isSidebarOpenInSmallScreen = true;
            $scope.$broadcast("pauseVideo");
        } else if (shouldClose !== false) {
            //close it
            closeSideBarInSmallScreen(event);
        }
        vm.shouldCheckForFeedbackAndReportOnClose = true;
    }

    function closeSideBarInSmallScreen(event) {
        if (event) {
            event.stopPropagation();
        }
        $("#course-sidebar").removeClass('ds-open');
        vm.isSidebarOpenInSmallScreen = false;

        if (vm.shouldCheckForFeedbackAndReportOnClose) {
            submitFeedbackOfQuestion();
            submitRatingOfQuestion();
            vm.shouldCheckForFeedbackAndReportOnClose = false;
        }
    }

    vm.submitRatingOfQuestion = submitRatingOfQuestion;

    function submitRatingOfQuestion(isFromBtn) {
        var arr = [];
        var key = Object.keys(vm.rateQuestionUserAns || {});
        var sel = vm.rateQuestionOptions[key[0]];
        sel && arr.push(sel);
        vm.rateQuestionNotApplicable && arr.push("Not Applicable");
        vm.rateQuestionReport && arr.push("Report: " + (vm.rateQuestionReportText || ""));
        if (vm.rateQuestionReport) {
            vm.rateQuestionReportText = vm.rateQuestionReportText || "";
            if (vm.rateQuestionReportText.length < 25) {
                vm.rateQuestionReportTextError = "Message should be atleast 25 characters"
                return;
            }
        }
        if (arr.length > 0) {
            submitQuestionStats({
                usr_id: authToken.getUserId() || '',
                Q_ID: vm.questions[vm.currentQuestionIndex]["questionId"],
                Q_EVENT_NM: "REPORT_SEND",
                q_event_val1: "Clicked",
                q_event_val2: getStatsEventData().courseId,
                q_event_val3: getStatsEventData().courseName,
                q_event_val4: getStatsEventData().moduleId,
                q_event_val5: getStatsEventData().moduleName,
                q_event_val6: getStatsEventData().itemId,
                q_event_val7: getStatsEventData().itemName,
                q_event_val8: getStatsEventData().time,
                q_event_val9: getStatsEventData().timezone,

                Q_EVENT_VAL10: (sel && arr[0]) || "",
                Q_EVENT_VAL11: vm.rateQuestionNotApplicable ? "Not Applicable" : "",
                Q_EVENT_VAL12: vm.rateQuestionReport ? (vm.rateQuestionReportText || "") : "",

                q_event_val14: vm.isQuestionsSequential ? "SEQ" : "RAND",
                q_event_val15: vm.isFullScreen ? true : false,

            });
            resetAllSidebar();
            vm.shouldShowContentWrapper = true;

            vm.rateQuestionUserAns = {};
            vm.rateQuestionReport = false;
            vm.rateQuestionReportText = "";
            vm.rateQuestionReportTextError = "";
            vm.rateQuestionNotApplicable = false;

            closeSideBarInSmallScreen();
        }

    }

    vm.submitFeedbackOfQuestion = submitFeedbackOfQuestion;

    function submitFeedbackOfQuestion(isFromBtn) {
        var arr = [];
        var only5Arr = [];
        vm.userWrongAnsFeedbackOption.map(function(v, i) {
            var val = vm.wrongAnsFeedbackOptions[i];
            arr.push(val);
            only5Arr.push(val);
        })
        vm.userWrongAnsReportOption && arr.push("Report: " + (vm.userWrongAnsReportOptionText || ""));
        if (vm.userWrongAnsReportOption) {
            vm.userWrongAnsReportOptionText = vm.userWrongAnsReportOptionText || "";
            if (vm.userWrongAnsReportOptionText.length < 25) {
                vm.userWrongAnsReportOptionTextError = "Message should be atleast 25 characters"
                return;
            }
        }

        if (arr.length > 0) {
            submitQuestionStats({
                usr_id: authToken.getUserId() || '',
                Q_ID: vm.questions[vm.currentQuestionIndex]["questionId"],
                Q_EVENT_NM: "Q_WRONG_REASON",
                q_event_val1: "AUTO",
                q_event_val2: getStatsEventData().courseId,
                q_event_val3: getStatsEventData().courseName,
                q_event_val4: getStatsEventData().moduleId,
                q_event_val5: getStatsEventData().moduleName,
                q_event_val6: getStatsEventData().itemId,
                q_event_val7: getStatsEventData().itemName,
                q_event_val8: getStatsEventData().time,
                q_event_val9: getStatsEventData().timezone,
                Q_EVENT_VAL10: only5Arr.join("|"),
                Q_EVENT_VAL11: (vm.userWrongAnsReportOptionText || ""),
                q_event_val14: vm.isQuestionsSequential ? "SEQ" : "RAND",
                q_event_val15: vm.isFullScreen ? true : false,
            });
            resetAllSidebar();
            vm.shouldShowContentWrapper = true;

            vm.userWrongAnsFeedbackOption = [];
            vm.userWrongAnsReportOption = false;
            vm.userWrongAnsReportOptionText = "";
            vm.userWrongAnsReportOptionTextError = "";

            vm.closeSideBarInSmallScreen();
        }
    }

    //generate word file
    function printQuestion(id) {
        fileGenerator.generateWord(id);
    }

    //Get language for syntax highlighting
    function getLanguageForSyntaxHighlighting() {
        if (!vm.fullCourse) {
            return;
        }
        return utility.getLanguageForSyntaxHighlighting("");
    }

    //Return A,B,C,D... based on Index
    function getOptionCharacter(index) {
        // 65 - A , 66 - B ...
        return String.fromCharCode(65 + index);
    }

    //toggle menu
    function toggleMenuModal() {
        if (!vm.isMenuModalOpen) {
            //It is not opened
            //Now it is going to open
            $("#menu-modal").css('width', '40%');
            $timeout(function() {
                vm.isMenuModalOpen = true;
            }, 300);
        } else {
            $("#menu-modal").width(0);
            vm.isMenuModalOpen = false;
        }
    }

    vm.changeActiveRelatedTab = changeActiveRelatedTab;

    function changeActiveRelatedTab(tab) {

        if (!vm.isUserLoggedIn()) {
            vm.showLoginAlertModal = true;
            var msgType = tab;
            if (tab === 'qa') {
                msgType = "QA (Question and Answer)";
            }
            vm.needLoginErrModalMsg = "Login to view " + msgType;
            return;
        }

        vm.activeRelatedTab = tab;
        if (tab === 'qa') {
            vm.getQueriesForItem()
        } else if (tab === 'explanation') {
            vm.learnMore()
        } else if (tab === "notes") {
            vm.getQuestionsNotes();
        } else if (tab === "stats") {
            $timeout(initQuestionStatsChart);
            $timeout(getAllAvgQuestionPerfStats);
            $timeout(getAllUsersQuestionPerfStats);
        }
    }

    function getAllAvgQuestionPerfStats() {
        admin.getAllAvgQuestionPerfStats()
            .then(function(res) {
                if (res.data && res.data[0]) {
                    var d = res.data[0];
                    vm.chart.addSeries({
                        name: 'Avg',
                        data: [Math.round(d.avg_no_of_q_views || 0), Math.round(d.avg_no_of_fst_correct || 0), Math.round(d.avg_no_of_fst_wrong || 0), Math.round(d.avg_no_of_sndt_correct || 0), Math.round(d.avg_no_of_sndt_wrong || 0), Math.round(d.avg_no_of_third_correct || 0), Math.round(d.avg_no_of_third_wrong || 0), Math.round(d.avg_no_of_vdo_views || 0)]
                    })
                    vm.chart.hideLoading()
                }
            })
            .catch(function(err) {
                console.error(err)
            })
    }

    function getAllUsersQuestionPerfStats() {
        admin.getAllUsersQuestionPerfStats()
            .then(function(res) {
                if (res.data) {
                    res.data.forEach(function(d, i) {
                        vm.chart.addSeries({
                            name: d.usr_id || "",
                            data: [Math.round(d.no_of_q_views || 0), Math.round(d.no_of_fst_correct || 0), Math.round(d.no_of_fst_wrong || 0), Math.round(d.no_of_sndt_correct || 0), Math.round(d.no_of_sndt_wrong || 0), Math.round(d.no_of_third_correct || 0), Math.round(d.no_of_third_wrong || 0), Math.round(d.no_of_vdo_views || 0)]
                        })
                    })
                }
            })
            .catch(function(err) {
                console.error(err)
            })
    }


    function initQuestionStatsChart() {
        var options = {
            chart: {
                renderTo: 'question-stats-chart',
                type: "line"
            },
            loading: {
                labelStyle: {
                    color: 'white'
                },
                style: {
                    backgroundColor: 'gray'
                }
            },
            tooltip: {
                enabled: true,
                crosshairs: [true, true]
            },
            title: {
                text: "Question Stats"
            },
            xAxis: {
                categories: ["Views", "1st Correct", "1st Wrong", "2nd Correct", "2nd Wrong", "3rd Correct", "3rd Wrong", "Video Views"]
            },
            yAxis: {
                title: {
                    text: '',
                    margin: 10
                }
            },
            series: []
        }
        vm.chart = new Highcharts.Chart(options);

        var monthStatsChartOptions = angular.copy(options);
        monthStatsChartOptions.chart.renderTo = "question-month-stats-chart";

        vm.monthStatsChart = new Highcharts.Chart(monthStatsChartOptions);
        vm.chart.showLoading()
        vm.monthStatsChart.showLoading();
    }


    vm.openFilterModal = openFilterModal;
    vm.closeFilterModal = closeFilterModal;
    vm.doFilterQuestion = doFilterQuestion;


    //open filter modal
    function openFilterModal() {
        vm.showFilterModal = true;
        getAllBooks();
    }
    //close filter modal
    function closeFilterModal() {
        vm.showFilterModal = false;
    }

    function getAllBooks() {
        admin.getAllBook()
            .then(function(data) {
                if (data.data.length > 0)
                    vm.books = data.data;
                else
                    vm.books = undefined;
            })
            .catch(function(err) {
                console.log(err);
            });
    }

    //this function will filter question based on user selection criteria like books,author,exam etc....
    function doFilterQuestion() {
        var selectedBooks = [];
        vm.selectedFilter = vm.selectedFilter || {};
        //create selected book array
        for (book in vm.selectedFilter['books']) {
            //if book is selected using checkbox then it is true
            if (vm.selectedFilter['books'][book])
                selectedBooks.push(book);
        }

        //reset all question 
        resetToAllQuestion();

        if (!shouldFilterQuestion()) {
            //there is no filter set 
            //return with all question
            //close filter modal if open
            if (vm.showFilterModal) {
                vm.showFilterModal = false;
            }
            return;
        }

        //iterate through all question
        var qs = vm.questions.filter(function(v, i) {
            var isContain = v.books.some(function(v) {
                return selectedBooks.indexOf(v) >= 0;
            });
            return isContain;
        });
        console.log(qs);
        vm.currentQuestionIndex = 0;
        vm.questions = qs;
        setQueConfig();

        //close filter modal if open
        if (vm.showFilterModal) {
            vm.showFilterModal = false;
        }
    }

    //should filter question in future or not
    //check whether one of the filter is set or not
    function shouldFilterQuestion() {
        vm.selectedFilter = vm.selectedFilter || {};
        //check selected book array
        for (book in vm.selectedFilter['books']) {
            //if book is selected using checkbox then it is true
            if (vm.selectedFilter['books'][book])
                return true;
        }

        //if nonr of the filter is set
        return false;
    }

    $($('body')[0]).on('fullscreenchange', function(e, fullScreenEnabled) {
        console.log("practice page fullScreen...", fullScreenEnabled, vm.isFullScreen);
        if (fullScreenEnabled) {
            vm.isFullScreen = true;
        } else {
            console.log("doing exitFullScreen practice")
            vm.isFullScreen = false;
        }
        $scope.$apply();
    });

    function doFullScreen() {
        if (vm.isFullScreen) {
            fullScreen.exitFullScreen();
        } else {
            fullScreen.launchFullScreen($('body')[0]);
        }
        vm.isFullScreen = !vm.isFullScreen;
    }

    //is Item contains slides
    function isItemContainsSlides(module, item) {
        var moduleItem = $filter('filter')(vm.fullCourse.moduleDetail, {
            moduleId: module
        })[0];
        var item = $filter('filter')(moduleItem.moduleItems, {
            itemId: item
        })[0];
        return item.itemContainsSlide;
    }


    //share to fb
    function shareToFB(title, desc) {
        var tempDiv = $("<div></div>");
        tempDiv.html(desc);
        var desc = tempDiv.text();

        Socialshare.share({
            'provider': 'facebook',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText': title,
                'socialshareDescription': desc,
                'socialshareType': 'feed',
                'socialshareVia': '1680795622155218',
                'socialshareMedia': 'http://www.digitalplatforms.co.za/wp-content/uploads/2014/09/laptop-on-work-desk.jpg',
                'socialsharePopupHeight': '500',
                'socialsharePopupWidth': '500',
            }
        });
    }

    //Share to twitter
    function shareToTwitter(title, desc) {
        Socialshare.share({
            'provider': 'twitter',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText': title,
                'socialshareVia': 'hcPYSQevRhsR8016f0MllEWbU',
                'socialshareHashtags': title + ', examwarrior',
                'socialsharePopupHeight': '500',
                'socialsharePopupWidth': '500',
            }
        });
    }

    //Share TO EW Profile
    function shareToEWProfile(title, desc) {
        var desc = desc;
        var title = title + ' | ExamWarrior';

        utility.shareToEWProfile(title, desc);
    }

    function shareToQA(title, desc) {
        var desc = desc;
        var title = title + ' | ExamWarrior';

        utility.shareToQA(title, desc);
    }


    //share to fb
    function shareExamStatsToFB(title) {
        var desc = $(".only-for-sharing")[0].outerHTML;

        Socialshare.share({
            'provider': 'facebook',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText': title,
                'socialshareDescription': desc,
                'socialshareType': 'feed',
                'socialshareVia': '1680795622155218',
                'socialshareMedia': 'http://www.digitalplatforms.co.za/wp-content/uploads/2014/09/laptop-on-work-desk.jpg',
                'socialsharePopupHeight': '500',
                'socialsharePopupWidth': '500',
            }
        });
    }

    //Share to twitter
    function shareExamStatsToTwitter(title) {
        Socialshare.share({
            'provider': 'twitter',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText': title,
                'socialshareVia': 'hcPYSQevRhsR8016f0MllEWbU',
                'socialshareHashtags': title + ', examwarrior',
                'socialsharePopupHeight': '500',
                'socialsharePopupWidth': '500',
            }
        });
    }

    //Share TO EW Profile
    function shareExamStatsToEWProfile(title) {
        var desc = $(".only-for-sharing")[0].outerHTML;
        var title = title + ' | ExamWarrior';
        var href = window.location.origin + "/" + vm.activeCourse + "/questions/" + vm.activeItem + "/";
        utility.shareToEWProfile(title, desc, href);
    }

    function shareExamStatsToQA(title) {
        var desc = $(".only-for-sharing")[0].outerHTML;
        var title = title + ' | ExamWarrior';
        var href = window.location.origin + "/" + vm.activeCourse + "/questions/" + vm.activeItem + "/";
        utility.shareToQA(title, desc, href);
    }

    function saveQueryForItem() {
        var obj = {
            itm_id: vm.activeItem,
            usr_id: authToken.getUserId() || 'n/a',
            qry_title: vm.qaTitle
        }
        itemQueries.save(obj)
            .then(function(res) {
                vm.itemQueries = vm.itemQueries || [];
                vm.itemQueries.push(res.data);
                vm.qaTitle = "";
                getQueriesForItem();

                sendQuestionPostedEvent(res.data.qry_id);
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function sendQuestionPostedEvent(query_id) {
        var obj = {
            usr_id: authToken.getUserId() || '',
            q_id: vm.questions[vm.currentQuestionIndex]["questionId"],
            Q_EVENT_NM: "Q_POSTED",
            q_event_val1: "Clicked",
            q_event_val2: getStatsEventData().courseId,
            q_event_val3: getStatsEventData().courseName,
            q_event_val4: getStatsEventData().moduleId,
            q_event_val5: getStatsEventData().moduleName,
            q_event_val6: getStatsEventData().itemId,
            q_event_val7: getStatsEventData().itemName,
            q_event_val8: getStatsEventData().time,
            q_event_val9: getStatsEventData().timezone,
            q_event_val10: query_id,
            q_event_val14: vm.isQuestionsSequential ? "SEQ" : "RAND",
            q_event_val15: vm.isFullScreen ? true : false,
        }
        submitQuestionStats(obj);
    }

    function getQueriesForItem() {
        var obj = {
            itm_id: vm.activeItem,
        }
        itemQueries.getAll(obj)
            .then(function(res) {
                vm.itemQueries = res.data;
                console.log(res.data);
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    vm.onPostComment = function(cmnt) {
        var obj = {
            usr_id: authToken.getUserId() || '',
            q_id: vm.questions[vm.currentQuestionIndex]["questionId"],
            Q_EVENT_NM: "COMMENTED",
            q_event_val1: "Enter",
            q_event_val2: getStatsEventData().courseId,
            q_event_val3: getStatsEventData().courseName,
            q_event_val4: getStatsEventData().moduleId,
            q_event_val5: getStatsEventData().moduleName,
            q_event_val6: getStatsEventData().itemId,
            q_event_val7: getStatsEventData().itemName,
            q_event_val8: getStatsEventData().time,
            q_event_val9: getStatsEventData().timezone,
            q_event_val10: cmnt.cmnt_id,
            q_event_val14: vm.isQuestionsSequential ? "SEQ" : "RAND",
            q_event_val15: vm.isFullScreen ? true : false,
        }
        submitQuestionStats(obj);
    }

    function getSignedDownloadUrl(info) {
        var key = info;
        awsService.getS3SignedUrl({
                type: 'download',
                key: key
            }).then(function(res) {
                var url = res.data;
                window.open(url);
            })
            .catch(function(err) {
                console.log(err);
            })

    }

    function getTimeAndTimeZone() {
        var time = new Date().getTime();
        var timezone = new Date().toString().split(" ")[5];
        return {
            time: "" + time,
            timezone: timezone
        }
    }

    function getStatsEventData() {
        return {
            courseName: vm.fullCourse.courseName,
            courseId: vm.fullCourse.courseId,
            moduleId: vm.activeModule,
            moduleName: vm.activeModuleName,
            itemId: vm.activeItem,
            itemName: vm.activeItemName,
            time: getTimeAndTimeZone().time,
            timezone: getTimeAndTimeZone().timezone
        }
    }

    function question_load_stats() {
        var options = vm.questions[vm.currentQuestionIndex]["options"] || [];
        var ans_texts = vm.ansArr.map(function(v, i) {
            var option = $filter("filter")(options, {
                id: v
            });
            if (option[0]) {
                return option[0]["text"];
            } else {
                return "";
            }
        })

        var typeOfQuestion = (vm.isShowingReviewOnly) && "Review";
        typeOfQuestion = typeOfQuestion || (vm.isShowingSkipOnly) && "Skipped";
        typeOfQuestion = typeOfQuestion || "Normal";

        var obj = {
            usr_id: authToken.getUserId() || '',
            q_id: vm.questions[vm.currentQuestionIndex]["questionId"],
            Q_EVENT_NM: "QUESTION_LOAD",
            q_event_val1: "QLOAD",
            q_event_val2: getStatsEventData().courseId,
            q_event_val3: getStatsEventData().courseName,
            q_event_val4: getStatsEventData().moduleId,
            q_event_val5: getStatsEventData().moduleName,
            q_event_val6: getStatsEventData().itemId,
            q_event_val7: getStatsEventData().itemName,
            q_event_val8: getStatsEventData().time,
            q_event_val9: getStatsEventData().timezone,
            q_event_val10: (vm.ansArr.length > 1) ? "multi" : "single",
            q_event_val11: typeOfQuestion,
            q_event_val14: vm.isQuestionsSequential ? "SEQ" : "RAND",
            q_event_val15: vm.isFullScreen ? true : false,
        }
        submitQuestionStats(obj);
    }

    vm.sendMenuStats = function(menuType) {
        submitQuestionStats({
            usr_id: authToken.getUserId() || '',
            Q_ID: vm.questions[vm.currentQuestionIndex]["questionId"],
            Q_EVENT_NM: "COURSE_MENU_CLICKED",
            q_event_val1: "Clicked",
            q_event_val2: getStatsEventData().courseId,
            q_event_val3: getStatsEventData().courseName,
            q_event_val4: getStatsEventData().moduleId,
            q_event_val5: getStatsEventData().moduleName,
            q_event_val6: getStatsEventData().itemId,
            q_event_val7: getStatsEventData().itemName,
            q_event_val8: getStatsEventData().time,
            q_event_val9: getStatsEventData().timezone,
            q_event_val10: vm.isSidebarOpenInSmallScreen ? 'OPEN' : 'CLOSE',
            q_event_val14: vm.isQuestionsSequential ? "SEQ" : "RAND",
            q_event_val15: vm.isFullScreen ? true : false,
        });
    }

    vm.idleTime = 0;
    vm.IDLE_TIME_CONST = 1000 * 60;

    function captureIdleEvents() {
        observeMouseActivity();
        observeTabActivity();

        function observeMouseActivity() {
            $interval(function() {
                vm.idleTime++;
                if (vm.idleTime > 5) {
                    submitIdleStats("No mouse/keyboard Activity");
                    vm.idleTime = 0;
                }
            }, vm.IDLE_TIME_CONST)
            $window.onmousemove = function(e) {
                vm.idleTime = 0;
            };
            $window.onkeypress = function(e) {
                vm.idleTime = 0;
            };
        }

        function observeTabActivity() {
            $window.onblur = function() {
                //Start Idle Timer
                vm.isTabChaged = true;
                vm.tabTimerId = $interval(function() {
                    vm.idleTime++;
                    if (vm.idleTime >= 5) {
                        submitIdleStats("Changed The Tab");
                        vm.idleTime = 0;
                    }
                }, vm.IDLE_TIME_CONST);
            }
            $window.onfocus = function() {
                vm.isTabChaged = false
                    //Stop Idle Timer
                if (vm.tabTimerId)
                    $interval.cancel(vm.tabTimerId);
            }
        }
    }

    function submitIdleStats(idleReason) {
        var que_id = "";
        if (vm.questions && vm.questions[vm.currentQuestionIndex]) {
            que_id = vm.questions[vm.currentQuestionIndex].questionId;
        }
        submitQuestionStats({
            usr_id: authToken.getUserId() || '',
            Q_ID: que_id,
            Q_EVENT_NM: "USER_IDLE",
            q_event_val1: "idle",
            q_event_val2: getStatsEventData().courseId,
            q_event_val3: getStatsEventData().courseName,
            q_event_val4: getStatsEventData().moduleId,
            q_event_val5: getStatsEventData().moduleName,
            q_event_val6: getStatsEventData().itemId,
            q_event_val7: getStatsEventData().itemName,
            q_event_val8: getStatsEventData().time,
            q_event_val9: getStatsEventData().timezone,
            q_event_val10: idleReason
        });
    }

    vm.pageLoadStats = function(menuType) {
        submitQuestionStats({
            usr_id: authToken.getUserId() || '',
            Q_ID: "",
            Q_EVENT_NM: "PAGE_LOAD",
            q_event_val1: "P_LOAD",
            q_event_val2: getStatsEventData().courseId,
            q_event_val3: getStatsEventData().courseName,
            q_event_val4: getStatsEventData().moduleId,
            q_event_val5: getStatsEventData().moduleName,
            q_event_val6: getStatsEventData().itemId,
            q_event_val7: getStatsEventData().itemName,
            q_event_val8: getStatsEventData().time,
            q_event_val9: getStatsEventData().timezone
        });
    }

    function recordPageClickStats() {
        $("practice-page").on('click', function(e) {
            var element = e.target;
            var shouldCapture = $(element).attr("data-ew-practice-page-stats");

            if (!shouldCapture) {
                element = $(element).closest("[data-ew-practice-page-stats]");
                shouldCapture = $(element).attr("data-ew-practice-page-stats");
            }

            var que_id = "";
            if (vm.questions && vm.questions[vm.currentQuestionIndex]) {
                que_id = vm.questions[vm.currentQuestionIndex].questionId;
            }

            if (shouldCapture === "true") {
                var msg = $(element).attr("data-ew-click");
                // console.log(msg);  
                if (msg) {
                    var pos = msg.indexOf(":");
                    if (pos <= -1) return;
                    var key = msg.substring(0, pos);
                    var value = msg.substring(pos + 1);
                    var values = value.split(",");
                    var obj = {
                        usr_id: authToken.getUserId() || '',
                        Q_ID: que_id,
                        Q_EVENT_NM: key,
                        q_event_val1: "Clicked",
                        q_event_val2: getStatsEventData().courseId,
                        q_event_val3: getStatsEventData().courseName,
                        q_event_val4: getStatsEventData().moduleId,
                        q_event_val5: getStatsEventData().moduleName,
                        q_event_val6: getStatsEventData().itemId,
                        q_event_val7: getStatsEventData().itemName,
                        q_event_val8: getStatsEventData().time,
                        q_event_val9: getStatsEventData().timezone,
                        q_event_val10: values[0] ? values[0] : undefined,
                        q_event_val11: values[1],
                        q_event_val12: values[2],
                        q_event_val13: values[3],
                        q_event_val14: vm.isQuestionsSequential ? "SEQ" : "RAND",
                        q_event_val15: vm.isFullScreen ? true : false
                    }
                    submitQuestionStats(obj);
                }
            }
        })
    }

    function submitQuestionStats(data) {
        data.tkn = authToken.getHashToken()
        utility.submitQuestionStats({
            topic: "questionstats",
            stats: JSON.stringify(data)
        });
    }

    vm.showVerifyQuestionModal = function(){
        vm.shouldShowVerifyQuestionModal = true;
    }

    vm.closeVerifyQuestionModal = function(){
        vm.shouldShowVerifyQuestionModal = false;
    }

    vm.submitVerifyPracticeQuestion = function(){
        vm.erorrInSubmitVerifyPracticeQuestion = undefined
        admin.updateVerifyPracticeQuestion({
            q_id : vm.questions[vm.currentQuestionIndex].questionId,
            test_status : vm.verifyStatsOption,
            tested_dt : new Date(),
            tester_id  : authToken.getUserId(),
            test_comments : vm.verifyQuestionRejectedReason,
            q_state_start_dt : new Date()
        })
        .then(function(){
            vm.closeVerifyQuestionModal();
            vm.verifyStatsOption = '';
            vm.verifyQuestionRejectedReason = undefined;
            vm.getLatestVerifyStatusOfGivenPracticeQuestions();
        })
        .catch(function(err){
            vm.erorrInSubmitVerifyPracticeQuestion = err;
        })
    }

    vm.getLatestVerifyStatusOfGivenPracticeQuestions = function() {
            admin.getLatestVerifyStatusOfGivenPracticeQuestions({
                q_id: vm.questions[vm.currentQuestionIndex].questionId
            })
            .then(function(res) {
                if(res.data && res.data[0]){
                    vm.current_que_status = res.data[0].test_status;
                    vm.verifyStatsOption = res.data[0].test_status;
                    vm.current_que_status = vm.current_que_status.replace(/_/g," ");
                }
            })
            .catch(function(err) {
                console.error(err);
            })
    }
}