// (function() {
//     angular
//         .module(window.moduleName)
//         .controller('AdminDemoVideosController', AdminDemoVideosController);

// 	AdminDemoVideosController.$inject=['courselist','admin','pageLoader','$filter','$scope'];
// 	function AdminDemoVideosController(courselist,admin,pageLoader,$filter,$scope) {

// 		var vm = this;
// 		//////////////
// 		//Variables //
// 		//////////////
// 		vm.courselist = courselist;
// 		vm.formError = false;
// 		vm.formSuccess = false;

// 		//////////////
// 		//Functions //
// 		//////////////
// 		vm.changeCourse = changeCourse;
// 		vm.changeVideoType = changeVideoType;
// 		vm.makeAllPaid = makeAllPaid;
// 		vm.isItemDemo = isItemDemo;
// 		vm.isItemDemoLogin = isItemDemoLogin;
// 		vm.isItemPaid = isItemPaid;


// 		//////////////////
// 		// Declarations //
// 		//////////////////
// 		function changeCourse(){
// 			pageLoader.show();

// 			admin.getCourseVideos({'courseId':vm.form.course})
// 				.then(function(res){
// 					var tempData = res[0];
// 					getAuthorizedItems()
// 						.then(function(){
// 							vm.courseVideos = tempData;
// 							//Sort Module Based On ModuleNumber
// 							vm.courseVideos.moduleDetail.sort(function(a,b){return a.moduleNumber - b.moduleNumber});
// 							//Sort Videos Based On VideoNumber
// 							vm.courseVideos.moduleDetail.map(function(v){v.moduleItems.sort(function(a,b){return a.videoNumber - b.videoNumber;});return v;});
// 						});
					
// 				})
// 				.finally(function(){
// 					pageLoader.hide();
// 				})
// 		}

// 		function getAuthorizedItems(){
// 			return admin.getAuthorizedItems({courseId:vm.form.course})
// 				.then(function(res){
// 					vm.authorizedItems = res.data;
// 					vm.authorizedIdTypeMap = {};
// 					vm.authorizedItems.map(function(v){
// 						vm.authorizedIdTypeMap[v.itemId] = v.itemType;	
// 					});
// 				})
// 				.catch(function(err){
// 					console.error(err);
// 				})
// 		}

// 		function isItemDemo(id){
// 			return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'demo');
// 		}
// 		function isItemDemoLogin(id){
// 			return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'demologin');
// 		}
// 		function isItemPaid(id){
// 			return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'paid');
// 		}

// 		function makeAllPaid(){
// 			var allVideosArr = [];
// 			vm.courseVideos.moduleDetail.map(function(v){
// 				v.moduleItems.map(function(item){
// 					var obj = {
// 						courseId : vm.form.course,
// 						moduleId : v.moduleId,
// 						itemId : item.videoId,
// 						itemType : 'paid',
// 						itemDesc : 'video',
// 						lastUpdated : new Date()
// 					}
// 					allVideosArr.push(obj);
// 				});
// 			});
// 			updateAuthorizedItems(allVideosArr);
// 		}

// 		function changeVideoType(moduleId,videoId,videoType){
// 			var allVideosArr = [];
// 			var obj = {
// 				courseId : vm.form.course,
// 				moduleId : moduleId,
// 				itemId : videoId,
// 				itemType : videoType,
// 				itemDesc : 'video',
// 				lastUpdated : new Date()
// 			}
// 			allVideosArr.push(obj);
// 			updateAuthorizedItems(allVideosArr);
// 		}
    	
//     	function updateAuthorizedItems(videosArr){
//     		admin.updateAuthorizedItems(videosArr)
// 				.then(function(){
// 					vm.formSuccess = true;
// 					vm.formError = false;
// 					getAuthorizedItems();
// 				})
// 				.catch(function(err){
// 					vm.formError = true;
// 					vm.formSuccess = false;
// 				})
//     	}
//     }
// })();