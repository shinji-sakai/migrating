	angular.module(window.moduleName)
        .component('demoVideosPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./demovideos.html'),
            controller: AdminDemoVideosController,
            controllerAs: 'vm'
        });
    AdminDemoVideosController.$inject=['admin','pageLoader','$filter','$scope'];
	function AdminDemoVideosController(admin,pageLoader,$filter,$scope) {

		var vm = this;

		vm.$onInit = function() {
        	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
        		vm.courselist = v;
        	});
	    };

		//////////////
		//Variables //
		//////////////
		vm.formError = false;
		vm.formSuccess = false;

		//////////////
		//Functions //
		//////////////
		vm.changeCourse = changeCourse;
		vm.changeVideoType = changeVideoType;
		vm.makeAllPaid = makeAllPaid;
		vm.isItemDemo = isItemDemo;
		vm.isItemDemoLogin = isItemDemoLogin;
		vm.isItemPaid = isItemPaid;


		//////////////////
		// Declarations //
		//////////////////
		function changeCourse(){
			pageLoader.show();

			admin.getCourseItems({'courseId':vm.form.course})
				.then(function(res){
					var tempData = res[0];
					getAuthorizedItems()
						.then(function(){
							vm.courseVideos = tempData;
							//Sort Module Based On ModuleNumber
							vm.courseVideos.moduleDetail.sort(function(a,b){return a.moduleNumber - b.moduleNumber});
							//Sort Videos Based On VideoNumber
							vm.courseVideos.moduleDetail.map(function(v){v.moduleItems.sort(function(a,b){return a.itemWeight - b.itemWeight;});return v;});
						});
					
				})
				.finally(function(){
					pageLoader.hide();
				})
		}

		function getAuthorizedItems(){
			return admin.getAuthorizedItems({courseId:vm.form.course})
				.then(function(res){
					vm.authorizedItems = res.data;
					vm.authorizedIdTypeMap = {};
					vm.authorizedItems.map(function(v){
						vm.authorizedIdTypeMap[v.itemId] = v.itemType;	
					});
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function isItemDemo(id){
			return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'demo');
		}
		function isItemDemoLogin(id){
			return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'demologin');
		}
		function isItemPaid(id){
			return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'paid');
		}

		function makeAllPaid(){
			var allVideosArr = [];
			var courseObj = $filter('filter')(vm.courselist,{courseId : vm.form.course})[0];

			vm.courseVideos.moduleDetail.map(function(v){
				v.moduleItems.map(function(item){
					var obj = {
						courseId : vm.form.course,
						courseName : courseObj["courseName"],
						moduleId : v.moduleId,
						moduleName : v.moduleName,
						itemId : item.itemId,
						itemVideo : item.itemVideo,
						itemName : item.itemName,
						itemType : 'paid',
						lastUpdated : new Date()
					}
					allVideosArr.push(obj);
				});
			});
			updateAuthorizedItems(allVideosArr);
		}

		function changeVideoType(moduleId,moduleName,itemId,itemName,itemType,itemVideo){
			var allVideosArr = [];

			var courseObj = $filter('filter')(vm.courselist,{courseId : vm.form.course})[0];

			var obj = {
				courseId : vm.form.course,
				courseName : courseObj["courseName"],
				moduleId : moduleId,
				moduleName : moduleName,
				itemId : itemId,
				itemVideo : itemVideo,
				itemName : itemName,
				itemType : itemType,
				lastUpdated : new Date()
			}
			allVideosArr.push(obj);
			updateAuthorizedItems(allVideosArr);
		}
    	
    	function updateAuthorizedItems(videosArr){
    		admin.updateAuthorizedItems(videosArr)
				.then(function(){
					vm.formSuccess = true;
					vm.formError = false;
					getAuthorizedItems();
				})
				.catch(function(err){
					vm.formError = true;
					vm.formSuccess = false;
				})
    	}
    }