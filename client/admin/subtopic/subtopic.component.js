	angular.module(window.moduleName)
        .component('subtopicPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./subtopic.html'),
            controller: AdminSubTopicController,
            controllerAs: 'vm'
        });

	AdminSubTopicController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminSubTopicController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;

        vm.changeTopic = changeTopic;
    	vm.updateSubTopic = updateSubTopic;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removeSubTopic = removeSubTopic;
    	vm.editSubTopic = editSubTopic;
       vm.no_of_item_already_fetch=0;
    	vm.$onInit = function() {

            admin.getAllTopic()
                .then(function(data){
                    vm.topics = data.data;
                })
                .catch(function(err){
                    console.log(err);
                })

	    };

        function changeTopic(){
            //updateSubTopicsList(vm.form.topic);
        }

			vm.fetchSubTopics = function(){
            vm.subTopics = [];
			vm.isTopicsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'subtopic'
			})
			.then(function(res){
				$log.debug(res)
				vm.subTopics = res.data;
				vm.no_of_item_already_fetch = 5;
                vm.last_update_dt = vm.subTopics[vm.subTopics.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTopicsLoading = false;
			})
		}

		vm.fetchMoreSubTopics = function(){
			vm.isTopicsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : vm.no_of_item_already_fetch,
                collection : 'subTopic',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.subTopics, {subTopicId: v.subTopicId})[0];
						if(!item){
							vm.topics.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
                    vm.last_update_dt = vm.subTopics[vm.subTopics.length - 1].update_dt
					
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTopicsLoading = false;
			})
		}
   

	    function updateSubTopicsList(id){
	    	pageLoader.show();
	    	admin.getAllSubTopic({topicId : id})
        		.then(function(data){
        			if(data.data.length > 0)
        				vm.subTopics = data.data;
                    else
                        vm.subTopics = undefined;
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }

    	function updateSubTopic(){
            var topic = $filter('filter')(vm.topics,{topicId:vm.form.topic})[0];
    		var data = {
                topicId : topic.topicId,
                topicName : topic.topicName,
    			subTopicId : vm.form.subTopicId,
    			subTopicName : vm.form.subTopicName,
    			subTopicDesc : vm.form.subTopicDesc 
    		}
			
            if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }
    		pageLoader.show();
    		admin.updateSubTopic(data)
    			.then(function(res){
					vm.fetchSubTopics();
    				vm.submitted = true;
    				vm.form.error = false;
    				vm.form.subTopicName = "";
    				vm.form.subTopicDesc = "";
                    vm.form.subTopicId = "";
					vm.formIsInEditMode=false;
    				//updateSubTopicsList(vm.form.topic);
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}

    	function resetFormDetail(){
    		vm.submitted = false;
            var topic = vm.form.topic;
    		vm.form = {};
            vm.form.topic = topic;
    	}

    	function removeSubTopic(id){
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
    		admin.removeSubTopic({subTopicId:id})
    			.then(function(d){
    				console.log(d);
					var item = $filter('filter')(vm.subTopics || [], {subTopicId: id})[0];								
					 if (vm.subTopics.indexOf(item) > -1) {
                        var pos = vm.subTopics.indexOf(item);
                       vm.subTopics.splice(pos, 1);
                    }
    				//updateSubTopicsList(vm.form.topic);
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}

    	function editSubTopic(id){
    		var obj = $filter('filter')(vm.subTopics,{subTopicId:id})[0];
            console.log(obj);
    		vm.form = {};
            vm.form.topic = obj.topicId;
    		vm.form.subTopicId = obj.subTopicId;
    		vm.form.subTopicName = obj.subTopicName;
    		vm.form.subTopicDesc = obj.subTopicDesc;
			vm.formIsInEditMode=true;
    	}
    }