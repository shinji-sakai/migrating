require('angular-froala');
angular.module(window.moduleName)
    .component('jobOpeningsPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./jobOpenings.html'),
        controller: AdminJobOpeningsController,
        controllerAs: 'vm'
    });

AdminJobOpeningsController.$inject = ['$log','admin', 'pageLoader', '$filter', '$scope','EditorConfig'];

function AdminJobOpeningsController($log,admin, pageLoader, $filter, $scope,EditorConfig) {
    var vm = this;
    
    vm.form = {};
    vm.form.jobStatus = "open"

    vm.EditorConfig = EditorConfig;

    vm.jobOpenings = [];
  vm.no_of_item_already_fetch=0;
    vm.updateJobOpening = updateJobOpening;
    vm.resetFormDetail = resetFormDetail;
    vm.removeJobOpening = removeJobOpening;
    vm.editJobOpening = editJobOpening;

    vm.$onInit = function() {
      //  updateJobOpeningsList();
    };

    //get jobOpenings from db
    //update jobOpenings array to display at local
    function updateJobOpeningsList() {
        pageLoader.show();
        //get all jobOpenings from db
        admin.getAllJobOpenings()
            .then(function(data) {
                if (data.data.length > 0)
                    vm.jobOpenings = data.data;
                else
                    vm.jobOpenings = [];
            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    	vm.fetchjobOpenings = function(){
            vm.jobOpenings = [];
			vm.isjobOpeningsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : 0,
                collection : 'jobOpenings'
			})
			.then(function(res){
				$log.debug(res)
				vm.jobOpenings = res.data;
				vm.no_of_item_already_fetch = 5;
                vm.last_update_dt = vm.jobOpenings[vm.jobOpenings.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isjobOpeningsLoading = false;
			})
		}
   
		vm.fetchMorejobOpenings = function(){
			vm.isjobOpeningsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : vm.no_of_item_already_fetch,
                collection : 'jobOpenings',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.jobOpenings, {jobOpeningId: v.jobOpeningId})[0];
						if(!item){
							vm.jobOpenings.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
                    vm.last_update_dt = vm.jobOpenings[vm.jobOpenings.length - 1].update_dt
					
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isjobOpeningsLoading = false;
			})
		}

    //update/insert book to db
    function updateJobOpening() {
        
            if(!vm.formIsInEditMode){
                vm.form.create_dt = new Date();
                vm.form.update_dt = new Date();
            }else{
                vm.form.update_dt = new Date();
            }
        pageLoader.show();
        vm.submitted = false;
        admin.updateJobOpening(vm.form)
            .then(function(res) {
                vm.fetchjobOpenings();
                resetFormDetail();
               // updateJobOpeningsList();
            })
            .catch(function() {
                vm.submitted = true;
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function resetFormDetail() {
        vm.submitted = false;
        vm.formIsInEditMode = false;
        vm.form = {};
        vm.form.jobStatus = "open";
    }


    //this function will call api and remove book from db
    function removeJobOpening(id) {
        //ask for confirmation
        var result = confirm("Want to delete?");
        if (!result) {
            //Declined
            return;
        }
        pageLoader.show();
        //service to remove book from server
        admin.removeJobOpening({
                jobOpeningId: id
            })
            .then(function(d) {
                var item = $filter('filter')(vm.jobOpenings || [], {jobOpeningId: id})[0];								
					 if (vm.jobOpenings.indexOf(item) > -1) {
                        var pos = vm.jobOpenings.indexOf(item);
                       vm.jobOpenings.splice(pos, 1);
                    }
                //update array locally
                //updateJobOpeningsList();
            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    //this function will be called when user will click on press button of book 
    function editJobOpening(id) {
        var jobOpening = $filter('filter')(vm.jobOpenings, {
            jobOpeningId: id
        })[0];
        delete jobOpening["_id"];
        vm.form = jobOpening;
        vm.formIsInEditMode = true;
    }
}