angular.module(window.moduleName)
    .component('courseBundlePage', {
        template: require('./courseBundle.html'),
        controller: AdminCourseBundleController,
        controllerAs: 'vm'
    });

AdminCourseBundleController.$inject=['admin','pageLoader','$filter'];

function AdminCourseBundleController(admin,pageLoader,$filter) {

    var vm = this;
    vm.selectedCourses = [];

    //functions
    vm.init = init;
    vm.saveBundle = saveBundle;
    vm.updateFormDetails = updateFormDetails;
    vm.removeCourseBundle = removeCourseBundle;
    vm.resetFormDetails = resetFormDetails;

    //init data
    init();
    function init(){
        //get coursemaster from db
        admin.coursemaster()
        .then(function(d){
            vm.courses = d;
        })
        .catch(function(err){
            console.error(err);
        });
        getBundleList();
        
    }

    //update bundle list locally
    function getBundleList(){
        admin.getAllCourseBundle()
            .then(function(d){
                vm.bundles = d.data;
            })
            .catch(function(err){
                console.error(err);
            });
    }

    //Save bundle to database
    function saveBundle(){
        var data = {
            bundleId : vm.form.bundleId || undefined,
            bundleName : vm.form.bundleName,
            bundleCourses : vm.selectedCourses
        }

        pageLoader.show();
        vm.isSubmitted = true;
        admin.updateCourseBundle(data)
            .then(function(r){
                vm.form.success = true;
                vm.form.error = null;

                getBundleList();
                resetFormDetails();
            })
            .catch(function(e){
                vm.form.success = undefined;                
                vm.form.error = e;
            })
            .finally(function() {
                pageLoader.hide();
                vm.isSubmitted = true;
            })
    }

    //reset form details
    function resetFormDetails(){
        vm.form = {};
        vm.selectedCourses = [];
    }

    //update form details
    //this will be called when user click on edit button of bundle
    function updateFormDetails(id){
        var bundle = $filter('filter')(vm.bundles,{bundleId : id})[0];
        vm.form = {};
        vm.form = bundle;
        vm.selectedCourses = bundle.bundleCourses;
    }  

    //remove bundle from database
    function removeCourseBundle(id){
        pageLoader.show();
        vm.isSubmitted = false;
        admin.removeCourseBundle({bundleId : id})
            .then(function(r){
                getBundleList();
            })
            .catch(function(e){
                vm.form.success = undefined;
                vm.form.error = e;
            })
            .finally(function() {
                pageLoader.hide();
                vm.isSubmitted = true;
            })
    }
}