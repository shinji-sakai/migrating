require('mail');
require('angular-textbox-io');
angular.module(window.moduleName)
    .component('sendBulkMails', {
        template: require('./sendBulkMails.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','Mail'];

function Controller($log,$filter, $scope, $state, EditorConfig, $timeout,Mail) {
    var vm = this;

    vm.sendMails = sendMails;
    
    vm.$onInit = function() {
     
    }
    function sendMails() {
    	Mail.sendBulkEmails(vm.form)
            .then(function(res){
                $log.debug(res);
                vm.form = {};
                $timeout(function() {
                    vm.form.mail_body = "<p>&nbsp;</p>";
                })
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}