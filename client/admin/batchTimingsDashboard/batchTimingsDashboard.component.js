require('./_batchTimingsDashboard.scss');
require('service.utility');
angular.module(window.moduleName)
    .component('batchTimingsDashboardPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./batchTimingsDashboard.html'),
        controller: BatchTimingDashboardController,
        controllerAs: 'vm'
    })

BatchTimingDashboardController.$inject = ['admin', '$filter', '$scope', '$state','utility'];

function BatchTimingDashboardController(admin, $filter, $scope, $state,utility) {

    var vm = this;
    vm.form = vm.form || {};
    vm.form.faculty_id = vm.form.faculty_id || [];
    vm.selectedTraining = [];
    vm.selectedBatch = [];
    vm.nowDate = (new Date());
    vm.yesterday = new Date(vm.nowDate);
    vm.yesterday.setDate(vm.nowDate.getDate() - 1);
    vm.yesterday = vm.yesterday.toString();

    vm.daysOfWeek = ["Monday", 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']


    vm.saveTiming = saveTiming;
    vm.deleteTiming = deleteTiming;
    vm.editTiming = editTiming;

    vm.onTrainingChange = onTrainingChange;
    vm.onBatchChange = onBatchChange;

    vm.convertTimeTo_AM_PM = utility.convertTimeTo_AM_PM;

    vm.$onInit = function() {
        getAllBatchTraining();
        getAllTimingDashboard();
    }

    function getAllBatchTraining() {
        admin.getAllBatchCourse()
            .then(function(res) {
                vm.allTrainings = res.data;
            })
            .catch(function(err) {
                $log.error(err);
            })
    }

    function getAllTimingByTrainingId(id) {
        admin.getTimingByCourseId({
                crs_id: id || vm.selectedTraining[0]
            })
            .then(function(res) {
                vm.allBatches = res.data;
            })
            .catch(function(err) {
                $log.error(err);
            })
    }

    function getAllTimingDashboard(){
    	admin.getAllBatchTimingDashboard()
            .then(function(res) {
                vm.allBatchTimingsDashboard = res.data;
            })
            .catch(function(err) {
                $log.error(err);
            })
    }

    function onTrainingChange() {
        vm.form = vm.form || {};
        vm.form.training_id = vm.selectedTraining[0] || "";
        vm.allBatches = [];
        vm.selectedBatch = [];
        getAllTimingByTrainingId();
    }

    function onBatchChange() {
        vm.form = vm.form || {};
        vm.form.bat_id = vm.selectedBatch[0] || "";

        if (vm.selectedBatch.length <= 0) {
            vm.form = {};
            vm.bat_frm_tm_hr = 0;
            vm.bat_frm_tm_min = 0;

            vm.nxt_bat_frm_tm_hr = 0;
            vm.nxt_bat_frm_tm_min = 0;
            return;
        }

        var batch = $filter("filter")(vm.allBatches, {
            bat_id: vm.form.bat_id
        })[0];

        vm.originalBatch = angular.copy(batch);

        batch = angular.copy(batch);

        var cls_start_dt = batch.cls_start_dt;
        batch.cls_start_dt = getFormattedDate(batch.cls_start_dt);


        var tempArr = batch.cls_frm_tm.split(":");
        vm.bat_frm_tm_hr = tempArr[0] || 0;
        vm.bat_frm_tm_min = tempArr[1] || 0;

        vm.nxt_bat_frm_tm_hr = tempArr[0] || 0;
        vm.nxt_bat_frm_tm_min = tempArr[1] || 0;


        var frm_wk_dy = batch.frm_wk_dy;
        var to_wk_dy = batch.to_wk_dy;

        var next_training_date = utility.getNextTrainingDay(frm_wk_dy,to_wk_dy,cls_start_dt,vm.bat_frm_tm_hr,vm.bat_frm_tm_min)
        batch.nxt_cls_dt = getFormattedDate(next_training_date);


        vm.form.cls_start_dt = batch.cls_start_dt;
        vm.form.nxt_cls_dt = batch.nxt_cls_dt;

        var from_ds_batch = $filter('filter')(vm.allBatchTimingsDashboard, {
            batch_id: vm.form.bat_id
        })[0];

        if(from_ds_batch){
        	vm.form.status = from_ds_batch.status;
        }


    }

    function saveTiming() {

        if(vm.form.status === "completed"){
            var obj_to_send = {
                bat_id: vm.form.bat_id,
                status : "completed",
                cls_end_dt : ""
            }
            var batch_from_ds = $filter('filter')(vm.allBatchTimingsDashboard, {
                batch_id: vm.form.bat_id
            })[0];
            if(batch_from_ds.nxt_cls_dt){
                obj_to_send["cls_end_dt"] = batch_from_ds.nxt_cls_dt;
            }else{
                obj_to_send["cls_end_dt"] = vm.originalBatch.cls_end_dt;
            }

            admin.setBatchTimingDashboardAsCompleted(obj_to_send)
                    .then(function(res) {
                        getAllTimingDashboard();
                        vm.form = {};
                        vm.bat_mn = "";
                        vm.bat_dt = "";
                        vm.bat_yr = "";
                        vm.bat_frm_tm_hr = "";
                        vm.bat_frm_tm_min = "";
                        vm.nxt_bat_frm_tm_hr = "";
                        vm.nxt_bat_frm_tm_min = "";
                    })
                    .catch(function(err) {
                        console.error(err);
                    })

            return;
        }

        vm.form.cls_start_tm = vm.bat_frm_tm_hr + ":" + vm.bat_frm_tm_min;
        vm.form.nxt_cls_tm = vm.nxt_bat_frm_tm_hr + ":" + vm.nxt_bat_frm_tm_min;

        admin.addBatchTimingDashboard(vm.form)
            .then(function(res) {
                getAllTimingDashboard();
                vm.form = {};
                vm.bat_mn = "";
                vm.bat_dt = "";
                vm.bat_yr = "";
                vm.bat_frm_tm_hr = "";
                vm.bat_frm_tm_min = "";
                vm.nxt_bat_frm_tm_hr = "";
                vm.nxt_bat_frm_tm_min = "";
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function deleteTiming(id) {
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        admin.deleteBatchTimingDashboard({
                bat_id: id
            })
            .then(function(res) {
                getAllTimingDashboard();
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function editTiming(id) {
    	console.log(id)
        var batch = $filter('filter')(vm.allBatchTimingsDashboard, {
            batch_id: id
        })[0];

        batch = angular.copy(batch);

        batch.cls_start_dt = getFormattedDate(batch.cls_start_dt);
        batch.nxt_cls_dt = getFormattedDate(batch.nxt_cls_dt);


        var tempArr = batch.cls_start_tm.split(":");
        vm.bat_frm_tm_hr = tempArr[0] || 0;
        vm.bat_frm_tm_min = tempArr[1] || 0;

        tempArr = batch.nxt_cls_tm.split(":");
        vm.nxt_bat_frm_tm_hr = tempArr[0] || 0;
        vm.nxt_bat_frm_tm_min = tempArr[1] || 0;

        vm.selectedBatch = [batch.batch_id];
        vm.form = batch;
        vm.form.bat_id = batch.batch_id;
        vm.selectedTraining = [vm.form.training_id];
        vm.selectedBatch = [vm.form.bat_id];
        getAllTimingByTrainingId(vm.form.training_id);
    }


    function getFormattedDate(date){
    	var tempDate = new Date(date);
        vm.bat_mn = ("00" + (tempDate.getMonth() + 1)).slice(-2);
        vm.bat_dt = ("00" + (tempDate.getDate())).slice(-2);
        vm.bat_yr = "" + (tempDate.getFullYear());
        return vm.bat_mn + "-" + vm.bat_dt + "-" + vm.bat_yr;
    }
}