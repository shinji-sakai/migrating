var FILE = {

	'AdminController' : 'admin/admin/admin.controller.js',
	'CourselistController' : 'admin/courselist/courselist.controller.js',
	'CourseMasterController' : 'admin/coursemaster/coursemaster.controller.js',
	'ModulesController' : 'admin/modules/modules.controller.js',
	'SlidesController' : 'admin/slides/slides.controller.js',
	'VideosController' : 'admin/videos/videos.controller.js',
	'DemoVideosController' : 'admin/demovideos/demovideos.controller.js',

	'ServiceModule' : 'shared/services/serviceModule.js',
	'DirectiveModule' : 'shared/directive/directiveModule.js',

	'admin' : 'shared/services/admin.js',
	'pageLoader' : 'shared/services/pageLoader.service.js',

	'pageLoading' : 'shared/directive/pageloading.directive.js',

	'tagsinputCSS' : '../../bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css',
	'tagsinputJS' : '../../bower_components/jquery.tagsinput/src/jquery.tagsinput.js',

}