	angular.module(window.moduleName)
        .component('examPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./exam.html'),
            controller: AdminExamController,
            controllerAs: 'vm'
        });

	AdminExamController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminExamController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;
       	vm.no_of_item_already_fetch = 0;
        vm.checkValidityOfID = checkValidityOfID;
    	vm.updateExam = updateExam;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removeExam = removeExam;
    	vm.editExam = editExam;

    	vm.$onInit = function() {
        	//updateExamsList();
	    };


        function checkValidityOfID() {
            vm.form.examId = vm.form.examId || "";
            vm.form.examId = vm.form.examId.replace(/\s/g , "-");
			admin.isIdExist({
				collection :'exam',
				key:"examId",
				id_value:vm.form.examId			
			}).then(function(data)
			{				
              console.log(data.data);
			  if(data.data.idExist)
			  {
				    vm.idExists = true;
			  }
			  else
			  {
				   vm.idExists = false;
			  }
			}).catch(function(err){
             console.log(err);
			})
		
			
          
        }

	    function updateExamsList(){
	    	pageLoader.show();
	    	admin.getAllExam()
        		.then(function(data){
        			if(data.data.length > 0)
        				vm.exams = data.data;
                    else
                        vm.exams = [];
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }
      vm.fetchExams = function(){
			vm.isExamsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'exam'
			})
			.then(function(res){
				$log.debug(res)
				vm.exams = res.data;
				vm.no_of_item_already_fetch = 5;
				 vm.last_update_dt = vm.exams[vm.exams.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isExamsLoading = false;
			})
		}

		vm.fetchMoreExams = function(){
			debugger;
			vm.isExamsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'exam',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.exams, {examId: v.examId})[0];
						if(!item){
							vm.exams.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					 vm.last_update_dt = vm.exams[vm.exams.length - 1].update_dt;
					// vm.coursemasterdata = vm.coursemasterdata.concat(res.data)	
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isExamsLoading = false;
			})
		}
    	function updateExam(){
    		var data = {
    			examId : vm.form.examId,
    			examName : vm.form.examName,
    			examDesc : vm.form.examDesc 
    		}
			if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }
    		pageLoader.show();
    		admin.updateExam(data)
    			.then(function(res){
					// var item = $filter('filter')(vm.exams || [], {examId: vm.form.examId})[0];
					// if(item){
					// 	item.examId = vm.form.examId,
					// 	item.examName = vm.form.examName,
					// 	item.examDesc = vm.form.examDesc 
					// }else{
					// 	vm.exams = [data].concat(vm.exams || [])
					// }
					vm.fetchExams();
    				vm.submitted = true;
    				vm.form.error = false;
    				vm.form.examName = "";
    				vm.form.examDesc = "";
                    vm.form.examId = "";
                    vm.idExists = false;
                    vm.formIsInEditMode = false;
    				//updateExamsList();
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}

    	function resetFormDetail(){
    		vm.submitted = false;
            vm.idExists = false;
            vm.formIsInEditMode = false;
    		vm.form = {};
    	}

    	function removeExam(id){
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
    		admin.removeExam({examId:id})
    			.then(function(d){
    				console.log(d);
					var item = $filter('filter')(vm.exams || [], {examId: id})[0];								
					 if (vm.exams.indexOf(item) > -1) {
                        var pos = vm.exams.indexOf(item);
                       vm.exams.splice(pos, 1);
                    }
    				//updateExamsList();
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}

    	function editExam(id){

    		var book = $filter('filter')(vm.exams,{examId:id})[0];
    		vm.form = {};
    		vm.form.examId = book.examId;
    		vm.form.examName = book.examName;
    		vm.form.examDesc = book.examDesc;
            vm.formIsInEditMode = true;
    	}
    }