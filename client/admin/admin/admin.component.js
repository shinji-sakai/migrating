require('service.admin');
require('authToken');
require('service.pageLoader');
require('directive.pageLoading');
require('angular-ui-tinymce');
require('directive.menu');
require('alertify.js/dist/js/alertify.js');
require('alertify.js/dist/js/ngAlertify.js');
require('alertify.js/dist/css/alertify.css');

angular.module(window.moduleName)
    .component('adminPage', {
        template: require('./admin.html'),
        controller: AdminController,
        controllerAs: 'vm'
    })

AdminController.$inject = ['$scope','admin', 'authToken'];

function AdminController($scope,admin, authToken) {
    var vm = this;
    vm.isUserAuthorized = false;

    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
        vm.hideSidebar = false;
        vm.isUserAuthorized = false;
        checkForAuthorization(toState.url.substring(1));
        vm.form_url = toState.url.substring(1);
    });

    $scope.$watch(function(){
        return vm.hideSidebar;
    },function(v){
    });


    vm.$onInit = function() {
        vm.testing = "testing";
        vm.hideSidebar = true;


        var pos = window.location.pathname.indexOf("/",2);
        var string_path = window.location.pathname.substring(pos+1);
        vm.form_url = string_path;
        checkForAuthorization(string_path);

        //get coursemaster from db
        admin.coursemaster()
            .then(function(d) {
                vm.coursemaster = d;
            })
            .catch(function(err) {
                console.error(err);
            });

        //get courselist from db
        admin.courselistfull()
            .then(function(d) {
                vm.courselistdata = d;
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function  checkForAuthorization(path) {
        if(!authToken.isAuthenticated()){
            vm.isUserAuthorized = false;
            return;
        }
        var userRoles = authToken.getUserRoles();
        if(userRoles.indexOf("super-admin") > -1){
            vm.isUserAuthorized = true;
        }else if(userRoles.indexOf("admin") > -1){
            vm.isUserAuthorized = false;
            admin.getUserAdminRoleFormAccessList({usr_id : authToken.getUserId()})
                .then(function(res) {
                    var authorizedLinks = res.data;
                    if(authorizedLinks.indexOf(path) > -1){
                        vm.isUserAuthorized = true;            
                    }else{
                        vm.isUserAuthorized = false;
                    }
                })
                .catch(function(err) {
                    console.error(err);
                })
        }else if(userRoles.indexOf("developer") > -1){
            vm.isUserAuthorized = false;
        }
    }
}