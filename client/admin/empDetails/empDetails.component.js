angular.module(window.moduleName)
    .component('empDetailsPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./empDetails.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state'];
function Controller(admin,$filter,$scope,$state){

	var vm  = this;

	vm.form = {};
	vm.form.experties = vm.form.experties || [];

	vm.saveEmployee = saveEmployee;
	vm.deleteEmployee = deleteEmployee;

	vm.$onInit = function(){
		getAllTypes();
		getAllEmployees();
		getAllSkills();
	}

	function getAllSkills(){
		admin.getAllEmployeeSkills()
			.then(function(res){
				vm.allSkills = res.data
			})
	}

	function getAllTypes(){
		admin.getAllEmployeeTypes()
			.then(function(res){
				vm.allEmpTypes = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function getAllEmployees(){
		admin.getAllEmployees()
			.then(function(res){
				vm.allEmps = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function saveEmployee(){
		admin.addEmployee(vm.form)
			.then(function(res){
				getAllEmployees();
				vm.form = {};
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function deleteEmployee(id){
		admin.deleteEmployee({emp_id : parseInt(id)})
			.then(function(res){
				getAllEmployees();
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function resetForm(){
		vm.form = {};
	}
}