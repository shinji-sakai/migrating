angular.module(window.moduleName)
    .component('skillsPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./skills.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['$log','admin','$filter','$scope','$state'];
function Controller($log,admin,$filter,$scope,$state){
	var vm = this;

	vm.saveSkill = saveSkill;
	vm.deleteSkill = deleteSkill;

	vm.$onInit = function(){
		//getAllSkills();
	}

	function getAllSkills(){
		admin.getAllEmployeeSkills()
			.then(function(res){
				vm.allSkills = res.data
			})
	}
		vm.fetchSkills = function(){
            vm.allSkills = [];
			vm.isSkillsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : 0,
                collection : 'skills'
			})
			.then(function(res){
				$log.debug(res)
				vm.allSkills = res.data;
				vm.no_of_item_already_fetch = 5;
                vm.last_update_dt = vm.allSkills[vm.allSkills.length - 1].update_dt
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isSkillsLoading = false;
			})
		}


		vm.fetchMoreSkills = function(){
			vm.isSkillsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				// no_of_item_to_skip : vm.no_of_item_already_fetch,
                collection : 'skills',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.allSkills, {skl_id: v.skl_id})[0];
						if(!item){
							vm.allSkills.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
                    vm.last_update_dt = vm.allSkills[vm.allSkills.length - 1].update_dt;
					
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isSkillsLoading = false;
			})
		}


	function saveSkill(){
		    if(!vm.formIsInEditMode){
                vm.form.create_dt = new Date();
                vm.form.update_dt = new Date();
            }else{
                vm.form.update_dt = new Date();
            }
		admin.addEmployeeSkill(vm.form)
			.then(function(res){
				vm.fetchSkills();
			//	getAllSkills();
				vm.form = {};
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function deleteSkill(id){
		admin.deleteEmployeeSkill({skl_id : id})
			.then(function(res){
					var item = $filter('filter')(vm.allSkills || [], {skl_id: id})[0];								
					 if (vm.allSkills.indexOf(item) > -1) {
                        var pos = vm.allSkills.indexOf(item);
                       vm.allSkills.splice(pos, 1);
                    }	
				//getAllSkills();
			})
			.catch(function(err){
				console.log(err);
			})
	}
}