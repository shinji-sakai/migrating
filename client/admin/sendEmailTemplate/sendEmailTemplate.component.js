require('service.admin');
require('mail');
require('./_sendEmailTemplate.scss');
angular.module(window.moduleName)
    .component('sendEmailTemplatePage', {
        template: require('./sendEmailTemplate.html'),
        controller: SendEmailTemplatesController,
        controllerAs: 'vm'
    })

SendEmailTemplatesController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin'];

function SendEmailTemplatesController($log,$filter, $scope, $state, EditorConfig, $timeout,admin) {
    var vm = this;

    vm.selectedEmailTemplate = [];
    vm.selectedUserEmailGroup = [];
    vm.selectedTraining = [];
    vm.selectedBatch = [];

    vm.onTrainingChange = onTrainingChange;
    vm.onBatchChange = onBatchChange;
    vm.onUserEmailGroupChange = onUserEmailGroupChange;
    vm.onEmailTemplateChange = onEmailTemplateChange;

    vm.addSendEmailTemplate = addSendEmailTemplate;
    vm.editSendEmailTemplate = editSendEmailTemplate;
    vm.deleteSendEmailTemplate = deleteSendEmailTemplate;
    vm.resetFormDetails = resetFormDetails;
    vm.sendEmailTemplatesToUsers = sendEmailTemplatesToUsers;
    vm.sendNotificationOfEmail = sendNotificationOfEmail;


    vm.$onInit = function() {
        getAllSendEmailTemplates();
        getAllEmailTemplates();
        getAllUserEmailGroups();
        getAllBatchTraining();
    }

    function getAllSendEmailTemplates(){
        admin.getAllSendEmailTemplates()
            .then(function(res){
                vm.allSendEmailTemplates = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllEmailTemplates(){
        admin.getAllEmailTemplates()
            .then(function(res){
                vm.allTemplates = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllUserEmailGroups(){
        admin.getAllUserEmailGroups()
            .then(function(res){
                vm.allUserEmailGroup = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function getAllBatchTraining() {
        admin.getAllBatchCourse()
            .then(function(res){
                vm.allTrainings = res.data;
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function onTrainingChange(){
        vm.form = vm.form || {};
        vm.form.training_id = vm.selectedTraining[0] || "";
        getAllTimingByTrainingId();
    }

    function onBatchChange(){
        vm.form = vm.form || {};
        vm.form.bat_id = vm.selectedBatch[0] || "";
    }

    function  getAllTimingByTrainingId(id) {
        admin.getTimingByCourseId({crs_id : id || vm.selectedTraining[0]})
            .then(function(res){
                vm.allBatches = res.data;
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function onEmailTemplateChange(){
        vm.form = vm.form || {};
        vm.form.email_template = vm.selectedEmailTemplate[0] || undefined;
    }

    function onUserEmailGroupChange(){
        vm.form = vm.form || {};
        vm.form.users_template = vm.selectedUserEmailGroup[0] || undefined;
    }

    function addSendEmailTemplate(){
        vm.formSuccess = undefined;
        admin.addSendEmailTemplate(vm.form)
            .then(function(res){

                var pk_id = res.data.pk_id;
                var group = $filter("filter")(vm.allSendEmailTemplates,{pk_id : pk_id})[0];
                if(group){
                    var pos = vm.allSendEmailTemplates.indexOf(group);
                    vm.allSendEmailTemplates.splice(pos,1);    
                }

                vm.formSuccess = true;
                vm.allSendEmailTemplates.push(res.data);
                vm.form = {};
                vm.selectedTraining = [];
                vm.selectedBatch = [];
                vm.selectedEmailTemplate = [];
                vm.selectedUserEmailGroup = [];
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function sendEmailTemplatesToUsers(data) {
        vm.emailSent = vm.emailSent || {};
        vm.emailSentInProgress = vm.emailSentInProgress || {};
        vm.emailSent[data.pk_id] = undefined;
        vm.emailSentInProgress[data.pk_id] = true;
        admin.sendEmailTemplatesToUsers(data)
            .then(function(res){
                vm.emailSent[data.pk_id] = true;
                vm.emailSentInProgress[data.pk_id] = false;

            })
            .catch(function(err){
                vm.emailSent[data.pk_id] = false;
                $log.error(err);
            })
            .finally(function(){
                $timeout(function(){
                    vm.emailSent[data.pk_id] = undefined;
                    vm.emailSentInProgress[data.pk_id] = false;
                },1000*5);
            })
    }

    function sendNotificationOfEmail(data){
        vm.emailNotificationSent = vm.emailNotificationSent || {};
        vm.emailNotificationSentInProgress = vm.emailNotificationSentInProgress || {};
        vm.emailNotificationSent[data.pk_id] = undefined;
        vm.emailNotificationSentInProgress[data.pk_id] = true;
        admin.sendNotificationOfEmail(data)
            .then(function(res){
                vm.emailNotificationSent[data.pk_id] = true;
                vm.emailNotificationSentInProgress[data.pk_id] = false;
            })
            .catch(function(err){
                vm.emailNotificationSent[data.pk_id] = false;
                vm.emailNotificationSentInProgress[data.pk_id] = false;
                $log.error(err);
            })
            .finally(function(){
                $timeout(function(){
                    vm.emailNotificationSent[data.pk_id] = undefined;
                    vm.emailNotificationSentInProgress[data.pk_id] = false;
                },1000*5);
            })
    }

    function editSendEmailTemplate(pk_id){
        vm.formSuccess = undefined;
        var template = $filter("filter")(vm.allSendEmailTemplates,{pk_id : pk_id});
        if(template && template[0]){
            vm.form = template[0];

            vm.selectedTraining = [vm.form.training_id]
            vm.selectedBatch = [vm.form.bat_id]
            vm.selectedUserEmailGroup = [vm.form.users_template];
            vm.selectedEmailTemplate = [vm.form.email_template];
            getAllTimingByTrainingId(vm.form.training_id);
            if(!vm.allTemplates || vm.allTemplates.length <=0){
                getAllEmailTemplates();    
            }

            if(!vm.allUserEmailGroup || vm.allUserEmailGroup.length <=0){
                getAllUserEmailGroups();
            }
        }
    }



    function deleteSendEmailTemplate(pk_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteSendEmailTemplate({pk_id : pk_id})
            .then(function(res){
                var pk_id = res.data.pk_id;
                var template = $filter("filter")(vm.allSendEmailTemplates,{pk_id : pk_id})[0];
                var pos = vm.allSendEmailTemplates.indexOf(template);
                vm.allSendEmailTemplates.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function resetFormDetails(){
        vm.form = {};
        vm.selectedEmailTemplate = [];
        vm.selectedUserEmailGroup = [];
        vm.selectedTraining = [];
        vm.selectedBatch = [];       
    }
}