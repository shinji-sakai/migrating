require('./_sessionDetails.scss');
angular.module(window.moduleName)
    .component('sessionDetailsPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./sessionDetails.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
function Controller(admin,$filter,$scope,$state,EditorConfig){

	var vm = this;
	vm.form = {};
	vm.selectedBatch = [];

	vm.onBatchChange = onBatchChange;
	vm.addItemToList = addItemToList;
	vm.removeItemFromList = removeItemFromList;
	vm.addSessionItem = addSessionItem;
	vm.resetForm = resetForm;
	vm.onSessionChange = onSessionChange;

	vm.$onInit = function(){
		getAllTiming();
	}

	function getAllTiming(){
		admin.getAllBatchTiming()
			.then(function(res){
				vm.allTimings = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function onBatchChange(){
		vm.form.bat_id = vm.selectedBatch[0];
		var bat = $filter('filter')(vm.allTimings,{bat_id : vm.form.bat_id})[0];
		vm.crs_id = bat.crs_id;
		console.log(vm.crs_id);
		getAllSessions();
	}

	function onSessionChange(){
		getSessionDetailsForBatchAndSession();
	}

	function getAllSessions(){

		admin.getSessionsByCourseId({crs_id : vm.crs_id})
			.then(function(res){
				vm.allSessions = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function getSessionDetailsForBatchAndSession(){
		admin.getSessionDetailsForBatchAndSession({bat_id : vm.form.bat_id , sess_id : vm.form.sess_id})
			.then(function(res){
				var obj = res.data;
				if(!obj.bat_id || !obj.sess_id){
					return;
				}
				var dt = new Date(obj.sess_dt);
				vm.sess_dt = "" + (dt.getDate());
				vm.sess_mn = "" + (dt.getMonth() + 1);
				vm.sess_yr = "" + (dt.getFullYear());

				delete obj.sess_dt;

				vm.form = obj;
				vm.form.sess_id = parseInt(obj.sess_id);
			})
			.catch(function(err){
				console.error(err);
			})
	}


	function addItemToList(){
		vm.form = vm.form || {};
		vm.form.sess_items = vm.form.sess_items || {};
		if(vm.title && vm.link){
			vm.form.sess_items[vm.itemType + "_" + vm.title] = vm.link;
			vm.title = "";
			vm.link = "";
			vm.itemType = "";
		}
	}

	function removeItemFromList(key){
		delete vm.form.sess_items[key];
	}


	function addSessionItem(){
		var dt = vm.sess_mn + "-" + vm.sess_dt + "-" + vm.sess_yr;
		vm.form.sess_dt = new Date(dt);
		admin.addSessionItems(vm.form)
			.then(function(res){
				getSessionDetailsForBatchAndSession();
				resetForm();
			})
			.catch(function(err){
				console.error(err);
			})
	}
	
	function resetForm(){
		vm.form = {};
		vm.sess_mn = "";
		vm.sess_dt = "";
		vm.sess_yr = "";
	}

}