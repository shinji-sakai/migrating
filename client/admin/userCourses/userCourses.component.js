require('UserCourses');
require('service.profile')

angular.module(window.moduleName)
    .component('userCoursesPage', {
        template: require('./userCourses.html'),
        controller: AdminUserCoursesController,
        controllerAs: 'vm'
    });

AdminUserCoursesController.$inject=['admin','pageLoader','$filter','userCourses','profile'];

function AdminUserCoursesController(admin,pageLoader,$filter,userCourses,profile) {
	var vm = this;

	vm.selectedUser = [];
	vm.selectedBundles = [];
	vm.form = {};
	var today = new Date();
	vm.form.startDate  = (today.getMonth() + 1) + '-' + today.getDate() + '-' +  today.getFullYear();

	vm.saveUserBundle = saveUserBundle;
	vm.getUserPuchasedCourses = getUserPuchasedCourses;


	//initialize data 
	init();
	function init(){
		//get all users short details 
		//to display in drop down
		profile.getAllUsersShortDetails()
			.then(function(d){
				console.log(d.data);
				vm.users = d.data;
			})
			.catch(function(err){
				console.log(err);
			})

		getBundleList();
		getUserPuchasedCourses();
	}

	//update bundle list locally
    function getBundleList(){
        admin.getAllCourseBundle()
            .then(function(d){
                vm.bundles = d.data;
            })
            .catch(function(err){
                console.error(err);
            });
    }

    function getUserPuchasedCourses(){
    	if(vm.selectedUser.length > 0){
    		var user = vm.selectedUser[0].usr_id;
    		userCourses.getPurchasedCourses({userId : user})
    			.then(function(r){
    				vm.purchasedCourses = r.data;
    			})	
    			.catch(function(err){
    				console.log(err);
    			})
    	}
    	else{
    		vm.purchasedCourses = [];
    	}
    	
    }


    //save bundles to db
    function saveUserBundle(){
    	var usr_id = vm.selectedUser[0].usr_id;
    	var user_courses = [];
    	vm.selectedBundles.map(function(v,i){
    		user_courses = user_courses.concat(v.bundleCourses.map(function(v,i){
    			return {
    				courseId : v,
    				startDate : vm.form.startDate,
    				endDate : vm.form.endDate
    			}
    		}));

    		return v;
    	});
    	var bundl = {
    		userId : usr_id,
    		courses : user_courses
    	}
    	pageLoader.show();
    	vm.isSubmitted = false;
    	userCourses.updatePurchasedCourses(bundl)
    		.then(function(r){
    			getUserPuchasedCourses();
    			resetFormDetails();
    			vm.error = undefined;
    			vm.success = true;
    		})
    		.catch(function(err){
    			console.log(err);
    			vm.error = err;
    			vm.success = undefined;
    		})
    		.finally(function(){
    			pageLoader.hide();
    			vm.isSubmitted = true;

    		})
    }

    function resetFormDetails() {
    	vm.form = {};
    	var today = new Date();
		vm.form.startDate  = (today.getMonth() + 1) + '-' + today.getDate() + '-' +  today.getFullYear();
		vm.selectedUser = [];
		vm.selectedBundles = [];
    }
}