require('service.admin');
angular.module(window.moduleName)
    .component('formsPage', {
        template: require('./forms.html'),
        controller: FormsController,
        controllerAs: 'vm'
    })

FormsController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin'];

function FormsController($log,$filter, $scope, $state, EditorConfig, $timeout,admin) {
    var vm = this;


    vm.addForm = addForm;
    vm.editForm = editForm;
    vm.deleteForm = deleteForm;
    
    vm.$onInit = function() {
        getAllForms();
    }

    function getAllForms(){
        admin.getAllForms()
            .then(function(res){
                vm.allForms = res.data;
                $log.debug(res);
            })
            .catch(function(err){
                $log.error(err);
            })
    }

    function addForm(){
        vm.formSuccess = undefined;
        admin.addForm(vm.form)
            .then(function(res){

                var form_id = res.data.form_id;
                var group = $filter("filter")(vm.allForms,{form_id : form_id})[0];
                if(group){
                    var pos = vm.allForms.indexOf(group);
                    vm.allForms.splice(pos,1);    
                }

                vm.allForms.push(res.data);
                vm.formSuccess = true;
                vm.form = {};
            })
            .catch(function(err){
                $log.error(err);
                vm.formSuccess = false;
            })
    }

    function editForm(form_id){
        vm.formSuccess = undefined;
        var group = $filter("filter")(vm.allForms,{form_id : form_id});
        if(group && group[0]){
            vm.form = group[0];
        }
    }

    function deleteForm(form_id){
        var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
        vm.formSuccess = undefined;
        admin.deleteForm({form_id : form_id})
            .then(function(res){
                var form_id = res.data.form_id;
                var group = $filter("filter")(vm.allForms,{form_id : form_id})[0];
                var pos = vm.allForms.indexOf(group);
                vm.allForms.splice(pos,1);
            })
            .catch(function(err){
                $log.error(err);
            })
    }
}