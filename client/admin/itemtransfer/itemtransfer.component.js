var Base64 = require('hash_js').Base64;
angular.module(window.moduleName)
    .component('itemtransferPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./itemtransfer.html'),
        controller: AdminItemTransferController,
        controllerAs: 'vm'
    });

AdminItemTransferController.$inject = ['$scope', 'admin', '$filter', 'pageLoader'];

function AdminItemTransferController($scope, admin, $filter, pageLoader) {


    var vm = this;

    //////////////
    //Variables //
    //////////////
    vm.optionCount = 1;
    vm.form = {};
    vm.form.questionTime = 1;
    vm.form.action = "copy";
    vm.form.options = [];
    // vm.form.textExplanation = "Enter Your Explanation Here";
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    // vm.form.questionNumber = 1;


    vm.selectedSubjects = [];
    vm.selectedAuthors = [];
    vm.selectedPublications = [];
    vm.selectedBooks = [];
    vm.selectedExams = [];
    vm.selectedFromTopics = [];
    vm.selectedToTopics = [];
    vm.selectedTags = [];

    vm.filteredQuestionsPageNumber = 1;
    vm.filteredQuestionsPageCount = 1;
    vm.selectedQuestionsPageNumber = 1;
    vm.selectedQuestionsPageCount = 1;
    vm.questionsInOnePage = 3;


    /////////////
    //Function //
    /////////////
    vm.getNumber = getNumber;
    vm.onFilterChange = onFilterChange;

    vm.fecthFilteredQuestions = fecthFilteredQuestions;
    vm.filteredQuestionPrev = filteredQuestionPrev;
    vm.filteredQuestionNext = filteredQuestionNext;
    vm.addToSelectedQuestions = addToSelectedQuestions;

    vm.fecthSelectedQuestions = fecthSelectedQuestions;
    vm.selectedQuestionPrev = selectedQuestionPrev;
    vm.selectedQuestionNext = selectedQuestionNext;
    vm.removeFromSelectedQuestions = removeFromSelectedQuestions;

    vm.isQuestionSelected = isQuestionSelected;

    vm.updateQuestionTopics = updateQuestionTopics;

    vm.$onInit = function() {
        $scope.$watch(function() {
            return vm.parent.courselistdata;
        }, function(v) {
            vm.courselist = v;
        });

        admin.getAllSubject()
            .then(function(d) {
                vm.subjects = d.data;
            });
        admin.getAllAuthor()
            .then(function(d) {
                vm.authors = d.data;
            });
        admin.getAllPublication()
            .then(function(d) {
                vm.publications = d.data;
            });
        admin.getAllBook()
            .then(function(d) {
                vm.books = d.data;
            });
        admin.getAllExam()
            .then(function(d) {
                vm.exams = d.data;
            });
        admin.getAllTopic()
            .then(function(d) {
                vm.topics = d.data;
            });
        admin.getAllTag()
            .then(function(d) {
                vm.tags = d.data;
            });
        // updateQuestionList();
    }

    function getNumber(num) {
        return Array.apply(null, {
            length: num
        });
    }

    function onFilterChange(item, model) {
        if (vm.selectedSubjects.length > 0) {
            admin.getAllTopic({
                subjectId: vm.selectedSubjects[0]
            })
                .then(function(d) {
                    vm.topics = d.data;
                });
        }
        var query = {
            subjects: vm.selectedSubjects,
            authors: vm.selectedAuthors,
            publications: vm.selectedPublications,
            books: vm.selectedBooks,
            exams: vm.selectedExams,
            topics: vm.selectedFromTopics,
            tags: vm.selectedTags
        }
        console.log(query);
        admin.findPracticeQuestionsByQuery(query)
            .then(function(d) {
                console.log(d.data);
                if (d.data.length > 0) {
                    vm.filteredQuestions = d.data;
                    vm.filteredQuestionsPageNumber = 1;
				    vm.filteredQuestionsPageCount = 1;
				    vm.selectedQuestionsPageNumber = 1;
				    vm.selectedQuestionsPageCount = 1;
				    if (vm.filteredQuestions.length > vm.questionsInOnePage) {
	                    vm.filteredQuestionsPageCount = Math.ceil(vm.filteredQuestions.length / vm.questionsInOnePage);
	                }
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    }


    function fecthFilteredQuestions() {
        var start = (vm.filteredQuestionsPageNumber - 1) * vm.questionsInOnePage;
        if (vm.filteredQuestions) {
            return vm.filteredQuestions.slice(start, start + vm.questionsInOnePage);
        } else {
            return [];
        }
    }

    function filteredQuestionPrev() {
        if (vm.filteredQuestionsPageNumber > 1) {
            vm.filteredQuestionsPageNumber--;
        }
    }

    function filteredQuestionNext() {
        if (vm.filteredQuestionsPageNumber < vm.filteredQuestionsPageCount) {
            vm.filteredQuestionsPageNumber++;
        }
    }

    function fecthSelectedQuestions() {
        var start = (vm.selectedQuestionsPageNumber - 1) * vm.questionsInOnePage;
        if (vm.selectedQuestions) {
            return vm.selectedQuestions.slice(start, start + vm.questionsInOnePage);
        } else {
            return [];
        }
    }

    function selectedQuestionPrev() {
        if (vm.selectedQuestionsPageNumber > 1) {
            vm.selectedQuestionsPageNumber--;
        }
    }

    function selectedQuestionNext() {
        if (vm.selectedQuestionsPageNumber < vm.selectedQuestionsPageCount) {
            vm.selectedQuestionsPageNumber++;
        }
    }

    function addToSelectedQuestions(questionId) {
        var que = $filter('filter')(vm.filteredQuestions, {
            questionId: questionId
        })[0];
        vm.selectedQuestions = vm.selectedQuestions || [];
        vm.selectedQuestions.push(que);

        if (vm.selectedQuestions.length > vm.questionsInOnePage) {
            vm.selectedQuestionsPageCount = Math.ceil(vm.selectedQuestions.length / vm.questionsInOnePage);
        }
    }

    function removeFromSelectedQuestions(questionId) {
        var que = $filter('filter')(vm.selectedQuestions, {
            questionId: questionId
        })[0];
        var pos = vm.selectedQuestions.indexOf(que);
        vm.selectedQuestions.splice(pos, 1);
    }

    function isQuestionSelected(questionId) {
        var que = $filter('filter')(vm.selectedQuestions, {
            questionId: questionId
        }) || [];
        return (que.length > 0);
    }

    function updateQuestionTopics() {
    	var qs = vm.fecthSelectedQuestions();
    	console.log(vm.selectedToTopics);
    	if(vm.form.action === "copy"){
    		qs = qs.map(function(q) {
    			q.topics = q.topics || [];
    			q.topics = q.topics.concat(vm.selectedToTopics);
    			return q;
    		});
    	}else if(vm.form.action === "move"){
    		qs = qs.map(function(q) {
    			vm.selectedFromTopics.forEach(function(v) {
    				var pos = q.topics.indexOf(v);
    				if(pos >= -1){
    					q.topics.splice(pos,1);
    				}
    			});
    			q.topics = q.topics || [];
    			q.topics = q.topics.concat(vm.selectedToTopics);
    			return q;
    		});
    	}
    	console.log(qs);
    	vm.submitted = false;
    	admin.updateMultiplePracticeQuestions(qs)
    		.then(function(res) {
    			console.log(res);
    			vm.form.error = false;
    			vm.submitted = true;
    			vm.selectedSubjects = [];
			    vm.selectedAuthors = [];
			    vm.selectedPublications = [];
			    vm.selectedBooks = [];
			    vm.selectedExams = [];
			    vm.selectedFromTopics = [];
			    vm.selectedToTopics = [];
			    vm.selectedTags = [];
			    vm.selectedQuestions = [];
			    vm.filteredQuestions = [];
			    vm.filteredQuestionsPageNumber = 1;
			    vm.filteredQuestionsPageCount = 1;
			    vm.selectedQuestionsPageNumber = 1;
			    vm.selectedQuestionsPageCount = 1;
    		})
    		.catch(function(err) {
    			console.error(err);
    			vm.form.error = true;
    			vm.submitted = true;
    			vm.error = err;
    		})
    }
}