angular.module(window.moduleName)
    .component('empTypesPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./empTypes.html'),
        controller: EmployeeTypesController,
        controllerAs: 'vm'
    })

EmployeeTypesController.$inject = ['admin','$filter','$scope','$state'];
function EmployeeTypesController(admin,$filter,$scope,$state){

	var vm  = this;

	vm.form = {};

	vm.saveType = saveType;
	vm.deleteType = deleteType;

	vm.$onInit = function(){
		getAllTypes();
	}

	function getAllTypes(){
		admin.getAllEmployeeTypes()
			.then(function(res){
				resetForm();
				vm.allEmpTypes = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function saveType() {
		var obj = {
			typ_id : vm.form.typ_id,
			typ_desc : vm.form.typ_desc
		}
		admin.addEmployeeType(obj)
			.then(function(){
				getAllTypes();
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function resetForm(){
		vm.form = {};
	}

	function deleteType(typ_id){
		var result = confirm("Want to delete?");
		if (!result) {
		    //Declined
		   	return;
		}
		var obj = {
			typ_id : typ_id,
		}
		admin.deleteEmployeeType(obj)
			.then(function(){
				getAllTypes();
			})
			.catch(function(err){
				console.error(err);
			})
	}
}