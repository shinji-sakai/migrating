	require('angular-froala');
	angular.module(window.moduleName)
        .component('slidesPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./slides.html'),
            controller: AdminSlidesController,
            controllerAs: 'vm'
        });

	AdminSlidesController.$inject = ['admin','pageLoader','$filter','$scope','Upload','EditorConfig','$sce'];
	function AdminSlidesController(admin,pageLoader,$filter,$scope,Upload,EditorConfig,$sce){
		var vm = this;

		vm.EditorConfig = EditorConfig;
		vm.trustAsHTML = function(html){
			return $sce.trustAsHtml(html);
		}

		//Observer parent variable
		vm.$onInit = function() {
        	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
        		vm.courseList = v;
        	});
	    };



	    //Observer files
		$scope.$watch('vm.files',function(newValue,oldValue){
			vm.isUploadCompleted = false;
			vm.fileNames = [];
			vm.slidesImageProgress = undefined;
			$('.slides-image-progress').width('0' + "%");
			angular.forEach(newValue, function(value, key){
				vm.fileNames[key] = value.name;
			});
		})

		//Observer showUploadImageForm to change file array
		$scope.$watch('vm.showUploadImageForm', function(newValue, oldValue, scope) {
			vm.files = undefined;
		});

		//Observe videoSlides array to sort it when it change
		$scope.$watch('vm.videoSlides', function(newValue, oldValue, scope) {
			vm.videoSlides.sort(function(a,b){
				return a.slideWeight - b.slideWeight;
			});
		});

		$scope.$watch('vm.form.blocks', function(newValue, oldValue, scope) {
			setTimeout(function(){
				Prism.highlightAll(true);
			},10);
		},true);

		$scope.$watch('vm.form.noOfBlocks', function(newValue, oldValue, scope) {
			if(vm.form){
				vm.form.blocks = [];
			}
		});

		$scope.$watch('vm.form.slideContent', function(newValue, oldValue, scope) {
			if(newValue === 'image'){
				vm.form.slideImage = vm.form.videoId + "_" + vm.form.slideNumber + ".png";
			}
		});
		//Render Code Preview based on Language
		$scope.$watch('vm.form.codeLanguage', function(newValue, oldValue, scope) {
			if(vm.form){
				vm.form.slideCode += " ";
			}
		});

		vm.videoSlides = [];
		vm.showForm = false;
		vm.codeLanguages = [
			{name : 'Java',value : 'java'},
			{name : 'JavaScript',value : 'javascript'},
			{name : 'Python',value : 'python'},
			{name : 'Plain Text',value : 'plain'}
		]


		vm.changeCourse = changeCourse;
		vm.changeModule = changeModule;
		vm.changeVideo = changeVideo;
		vm.newSlide = newSlide;
		vm.addVideoSlide=addVideoSlide;


		vm.deleteVideoSlide  = deleteVideoSlide;
		vm.editVideoSlide = editVideoSlide;
		vm.getNumber = getNumber;
		vm.uploadSlideImages = uploadSlideImages;

		vm.submitted = false;

		function getNumber(num) {
		    return Array.apply(null,{length:num});   
		}

		function editVideoSlide(id){
			vm.showForm = true;
			var item = $filter('filter')(vm.videoSlides,{slideId:id})[0];
			vm.form.slideNumber = item.slideNumber;
			vm.form.slideWeight = item.slideWeight;
			
			if (item.slideImage) {
				vm.form.slideContent = "image";
				vm.form.slideImage = item.slideImage;
				vm.form.codeLanguage = 'javascript';	
			}else if(item.slideBlocks){
				vm.form.slideContent = "code";
				vm.form.noOfBlocks = item.slideBlocks.length;
				
				setTimeout(function(){
					vm.form.blocks = item.slideBlocks;	
				},10);
			}
			vm.form.slideNotes= item.slideNotes;
			vm.form.slideId = item.slideId;
			vm.form.slideTitle = item.slideTitle;
			setTimeout(function(){
				console.log("edit");
				$('#slidedesc').froalaEditor(EditorConfig);

				slideDescription('');
				slideDescription(item.slideDesc);
			})
			
		}

		function deleteVideoSlide(id){
			var result = confirm("Want to delete?");
			if (!result) {
			    //Declined
			   	return;
			}
			pageLoader.show();
			admin.deleteVideoSlide({slideId:id,videoId:vm.form.videoId})
				.then(function(){
					//Update Slides Data
					updateSlidesData(vm.form.videoId);
				})
				.catch(function(err){console.error(err);})
				.finally(function(){
					pageLoader.hide();
				})
		}


		function addVideoSlide() {
			if(vm.form.slideContent === 'image'){
				//If Slide Content is Image
				vm.form.blocks = undefined;
			}else{
				//If Slide Content is Code
				vm.form.slideImage = undefined;
			}
			var slideData = {
				videoId : vm.form.videoId,
				topicTitle: vm.form.topicTitle,
				slideDesc: slideDescription(),
				slideDetail: {
					slideNumber: vm.form.slideNumber,
					slideWeight : vm.form.slideWeight,
					slideId: vm.form.videoId + "_" + vm.form.slideNumber,
					slideImage: vm.form.slideImage,
					slideBlocks : vm.form.blocks,
					slideTitle : vm.form.slideTitle,
					slideNotes: vm.form.slideNotes
				}
			}

			console.log("slide data.............",slideData);
			pageLoader.show();
			admin.addVideoSlide(slideData)
				.then(function(data){

					vm.form.error = false;
					vm.videoSlides = [];
					updateSlidesData(vm.form.videoId);
				})
				.catch(function(err){
					vm.form.error = true;
					console.error(err);
				})
				.finally(function(){
					vm.submitted = true;
					vm.showForm = false;
					pageLoader.hide();
				})

		}
		function newSlide(){
			vm.showForm = true;
			if(vm.videoSlides){
				vm.form.slideNumber = vm.videoSlides.length + 1;
				vm.form.slideWeight = vm.form.slideNumber;
			}else{
				vm.form.slideNumber = 1;
				vm.form.slideWeight = vm.form.slideNumber;
			}
			vm.form.slideContent = "image";
			vm.form.slideImage = vm.form.videoId + "_" + vm.form.slideNumber + ".png";
			vm.form.slideCode = undefined;
			vm.form.slideTitle = "";
			vm.form.slideNotes = [];
			vm.form.noOfSlideNotes = 1;

			setTimeout(function(){
				$('#slidedesc').froalaEditor(EditorConfig);
				slideDescription(' ');
			})
		}

		function changeCourse(){
			vm.moduleList = [];
			vm.videoList = [];
			vm.videoSlides = [];
			vm.showForm = false;
			vm.showUploadImageForm = false;
			if(vm.form.courseId){
				pageLoader.show();
				admin.getCourseModules({courseId:vm.form.courseId})
					.then(function(data){
						vm.moduleList = data;
					})
					.catch(function(err){console.error(err);})
					.finally(function(){pageLoader.hide();})
			}
		}

		function changeModule(){
			vm.videoList = [];
			vm.videoSlides = [];
			vm.showForm = false;
			vm.showUploadImageForm = false;
			if(vm.form.moduleId){
				pageLoader.show();
				admin.getModuleItems({moduleId:vm.form.moduleId})
					.then(function(data){
						vm.videoList = data;
					})
					.catch(function(err){console.error(err);})
					.finally(function(){pageLoader.hide();})
			}
		}

		function changeVideo(){
			vm.videoSlides = [];
			vm.showForm = false;
			if(vm.form.videoId){
				updateSlidesData(vm.form.videoId);
			}
		}

		function updateSlidesData(id){
			pageLoader.show();
			admin.getVideoSlides({videoId:id})
					.then(function(data){
						var slideData = data[0];

						vm.form.topicTitle = slideData.topicTitle;
						vm.form.slideDesc = slideData.slideDesc;
						vm.videoSlides = slideData.slideDetail;
						vm.form.slideContent = "image";
						vm.form.blocks = [];
					})
					.catch(function(err){
						console.error(err);
						vm.videoSlides = undefined;
					})
					.finally(function(){
						pageLoader.hide();
					})
		}

		function uploadSlideImages(){
			vm.isUploadCompleted = false;
			vm.isUploadGoingOn = true;
			Upload.upload({
				// 	/admin/uploadVideoSlideImages
				url: '/videoSlide/uploadVideoSlideImages',
			  	method: 'POST',
			  	data: {videoId:vm.form.videoId,files: vm.files},
			})
			.then(function(d){
				console.log(d);
			},function(err){
				console.error(err);
			},function(evt){
				var width = parseInt(100.0 * evt.loaded / evt.total);
				vm.slidesImageProgress = width + "%";
				$('.slides-image-progress').width(width + "%");
				if(width === 100){
					vm.isUploadCompleted = true;
				}
			})
			.finally(function(){
				vm.isUploadGoingOn = false;
				// vm.files = [];
			})
		}

		//set or get
		function slideDescription(desc){

			if(desc){
				$('#slidedesc').froalaEditor('html.set',desc);
				return;
			}
			return $('#slidedesc').froalaEditor('html.get',true);
		}

	}