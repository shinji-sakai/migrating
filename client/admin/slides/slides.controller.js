// (function(){
// 	angular
// 		.module(window.moduleName)
// 		.controller('AdminSlidesController',AdminSlidesController);

// 	AdminSlidesController.$inject = ['admin','pageLoader','courselist','$filter','$scope','Upload'];
// 	function AdminSlidesController(admin,pageLoader,courselist,$filter,$scope,Upload){
// 		var vm = this;

// 		$scope.$watch(function(){
// 			return vm.files;
// 		},function(newValue,oldValue){
// 			vm.fileNames = [];
// 			angular.forEach(newValue, function(value, key){
// 				vm.fileNames[key] = value.name;
// 			});
// 		})

// 		vm.courseList = courselist;
// 		vm.videoSlides = [];
// 		vm.showForm = false;

// 		vm.changeCourse = changeCourse;
// 		vm.changeModule = changeModule;
// 		vm.changeVideo = changeVideo;
// 		vm.newSlide = newSlide;
// 		vm.addVideoSlide=addVideoSlide;

// 		vm.deleteVideoSlide  = deleteVideoSlide;
// 		vm.editVideoSlide = editVideoSlide;
// 		vm.getNumber = getNumber;
// 		vm.uploadSlideImages = uploadSlideImages;

// 		vm.submitted = false;

// 		function getNumber(num) {
// 			console.log(num);
// 		    return Array.apply(null,{length:num});   
// 		}

// 		function editVideoSlide(id){
// 			vm.showForm = true;
// 			var item = $filter('filter')(vm.videoSlides,{slideId:id})[0];
// 			vm.form.slideNumber = item.slideNumber;
// 			vm.form.slideImage = item.slideImage;
// 			vm.form.slideNotes= item.slideNotes;
// 			vm.form.slideId = item.slideId;
// 			vm.form.slideTitle = item.slideTitle;
// 		}

// 		function deleteVideoSlide(id){
// 			pageLoader.show();
// 			admin.deleteVideoSlide({slideId:id,videoId:vm.form.videoId})
// 				.then(function(){
// 					//Update Slides Data
// 					updateSlidesData(vm.form.videoId);
// 				})
// 				.catch(function(err){console.error(err);})
// 				.finally(function(){
// 					pageLoader.hide();
// 				})
// 		}


// 		function addVideoSlide() {

// 			var slideData = {
// 				videoId : vm.form.videoId,
// 				topicTitle: vm.form.topicTitle,
// 				slideDesc: vm.form.slideDesc,
// 				slideDetail: {
// 					slideNumber: vm.form.slideNumber,
// 					slideId: vm.form.videoId + "_" + vm.form.slideNumber,
// 					slideImage: vm.form.slideImage,
// 					slideTitle : vm.form.slideTitle,
// 					slideNotes: vm.form.slideNotes
// 				}
// 			}


// 			pageLoader.show();
// 			admin.addVideoSlide(slideData)
// 				.then(function(data){

// 					vm.form.error = false;
// 					vm.videoSlides = [];
// 					updateSlidesData(vm.form.videoId);
// 				})
// 				.catch(function(err){
// 					vm.form.error = true;
// 					console.error(err);
// 				})
// 				.finally(function(){
// 					vm.submitted = true;
// 					vm.showForm = false;
// 					pageLoader.hide();
// 				})

// 		}
// 		function newSlide(){
// 			vm.showForm = true;
// 			if(vm.videoSlides){
// 				vm.form.slideNumber = vm.videoSlides.length + 1;
// 			}else{
// 				vm.form.slideNumber = 1;
// 			}
// 			vm.form.slideImage = vm.form.videoId + "_" + vm.form.slideNumber + ".png";
// 			vm.form.slideTitle = "";
// 			vm.form.slideNotes = [];
// 			vm.form.noOfSlideNotes = 1;
// 		}

// 		function changeCourse(){
// 			vm.moduleList = [];
// 			vm.videoList = [];
// 			vm.videoSlides = [];
// 			vm.showForm = false;
// 			vm.showUploadImageForm = false;
// 			if(vm.form.courseId){
// 				pageLoader.show();
// 				admin.getCourseModules({courseId:vm.form.courseId})
// 					.then(function(data){
// 						vm.moduleList = data;
// 					})
// 					.catch(function(err){console.error(err);})
// 					.finally(function(){pageLoader.hide();})
// 			}
// 		}

// 		function changeModule(){
// 			vm.videoList = [];
// 			vm.videoSlides = [];
// 			vm.showForm = false;
// 			vm.showUploadImageForm = false;
// 			if(vm.form.moduleId){
// 				pageLoader.show();
// 				admin.getModuleVideos({moduleId:vm.form.moduleId})
// 					.then(function(data){
// 						vm.videoList = data;
// 					})
// 					.catch(function(err){console.error(err);})
// 					.finally(function(){pageLoader.hide();})
// 			}
// 		}

// 		function changeVideo(){
// 			vm.videoSlides = [];
// 			vm.showForm = false;
// 			if(vm.form.videoId){
// 				updateSlidesData(vm.form.videoId);
// 			}
// 		}

// 		function updateSlidesData(id){
// 			pageLoader.show();
// 			admin.getVideoSlides({videoId:id})
// 					.then(function(data){
// 						var slideData = data[0];
// 						vm.form.topicTitle = slideData.topicTitle;
// 						vm.form.slideDesc = slideData.slideDesc;
// 						vm.videoSlides = slideData.slideDetail;
// 						vm.showUploadImageForm = true;
// 					})
// 					.catch(function(err){
// 						console.error(err);
// 						vm.videoSlides = undefined;
// 					})
// 					.finally(function(){
// 						pageLoader.hide();
// 					})
// 		}

// 		function uploadSlideImages(){
// 			Upload.upload({
// 				url: '/admin/uploadVideoSlideImages',
// 			  	method: 'POST',
// 			  	data: {videoId:vm.form.videoId,files: vm.files},
// 			})
// 			.then(function(d){
// 				console.log(d);
// 			},function(err){
// 				console.error(err);
// 			},function(evt){
// 				var width = parseInt(100.0 * evt.loaded / evt.total);
// 				vm.slidesImageProgress = width + "%";
// 				$('.slides-image-progress').width(width + "%");
// 			})
// 		}

// 	}
// })();