require('service.profile');
require("./_style.scss")
angular.module(window.moduleName)
    .component('verifyPracticeQuestionPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./verifyPracticeQuestion.html'),
        controller: AdminVerifyPracticeQuestionController,
        controllerAs: 'vm'
    });

AdminVerifyPracticeQuestionController.$inject = ['admin', 'pageLoader', '$filter', '$scope', "profile"];

function AdminVerifyPracticeQuestionController(admin, pageLoader, $filter, $scope, profile) {
    var vm = this;

    vm.selectedSubjects = [];
    vm.selectedTopics = [];
    vm.selectedQuestion = [];
    vm.selectedTesters = [];

    vm.nowDate = (new Date());
    vm.yesterday = new Date(vm.nowDate);
    vm.yesterday.setDate(vm.nowDate.getDate() - 1);
    vm.yesterday = vm.yesterday.toString();


    vm.checkValidityOfID = checkValidityOfID;
    vm.updateExam = updateExam;
    vm.resetFormDetail = resetFormDetail;
    vm.removeExam = removeExam;
    vm.editExam = editExam;

    vm.$onInit = function() {
        getAllSubjects();
        getAllTopics();
        getAllUsers();
    };

    function getAllUsers() {
        profile.getAllUsersShortDetails()
            .then(function(d) {
                vm.allUsers = d.data;
            })
            .catch(function(err) {
                console.log(err);
            })
    }

    function getAllSubjects() {
        admin.getAllSubject()
            .then(function(d) {
                vm.subjects = d.data;
            });
    }

    function getAllTopics() {
        admin.getAllTopic()
            .then(function(d) {
                vm.topics = d.data;
            });
    }

    vm.onFilterChange = function onFilterChange(item, model) {
        if (vm.selectedSubjects.length > 0) {
            admin.getAllTopic({
                    subjectId: vm.selectedSubjects[0]
                })
                .then(function(d) {
                    vm.topics = d.data;
                });
        }
        if (!vm.selectedTopics[0] || !vm.selectedSubjects[0]) {
            return;
        }
        var query = {
            subjects: vm.selectedSubjects,
            topics: vm.selectedTopics
        }
        admin.findPracticeQuestionsByQuery(query)
            .then(function(d) {
                if (d.data.length > 0) {
                    vm.questions = d.data;
                    vm.getAllQuestionStatues();
                } else {
                    vm.questions = [];
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    }

    vm.makeRowEditable = function(index) {
        if(vm.currentEditableRow || vm.currentEditableRow === 0){
            vm.shouldShowAlert = true;
            vm.goingToBeEditableRow = index;
            return;
        }
        vm.currentEditableRow = index;
    }
    vm.saveEditableRow = function(index) {
        var status = vm.visibleQuestionStatues[index];
        status.test_comments = vm.test_comments || status.test_comments;
        status.q_state_end_dt = status.q_state_end_dt ? new Date(status.q_state_end_dt) : undefined;
        status.tested_dt = status.tested_dt ? new Date(status.tested_dt) : undefined;
        status.tester_id = vm.selectedTesters[0];
        vm.isStatusUpdating = true;
        delete status["verify_id"]
        admin.updateVerifyPracticeQuestion(status)
            .then(function(res) {
                vm.getAllQuestionStatues();
                vm.test_comments = "";
                vm.currentEditableRow = undefined;
                vm.isStatusUpdating = false;
            })
            .catch(function(err) {

            })
    }

    vm.getAllQuestionStatues = function() {
        if (vm.questions.length > 0) {
            var ids = vm.questions.map(function(v, i) {
                return v.questionId
            });
            admin.getLatestVerifyStatusOfGivenPracticeQuestions({
                    q_id: ids
                })
                .then(function(res) {
                    console.log("getAllQuestionStatues..", res);
                    if (res.data) {
                        vm.allQuestionStatuses = res.data;
                        vm.visibleQuestionStatues = angular.copy(vm.allQuestionStatuses)
                    }
                })
                .catch(function(err) {
                    console.error(err);
                })
        }
    }

    vm.onSelectedQuestionChange = function onSelectedQuestionChange(item, model) {
        if (vm.selectedQuestion && vm.selectedQuestion.length > 0) {
            vm.visibleQuestionStatues = vm.allQuestionStatuses.filter(function(v, i) {
                if (vm.selectedQuestion.indexOf(v.q_id) > -1) {
                    return true;
                }
                return false
            })


            // admin.getAllVerifyStatusOfPracticeQuestion({
            //         q_id: vm.selectedQuestion[0]
            //     })
            //     .then(function(res) {
            //         if (res.data) {
            //             vm.selectedQuestionStatuses = res.data;
            //         }
            //     })
            //     .catch(function(err) {
            //         console.error(err);
            //     })
        } else {
            vm.visibleQuestionStatues = vm.allQuestionStatuses
        }
    }

    vm.onFilterStatusChange = function() {
        if(!vm.form.test_status){
            vm.visibleQuestionStatues = angular.copy(vm.allQuestionStatuses);
            return;
        }
        vm.visibleQuestionStatues = vm.allQuestionStatuses.filter(function(v, i) {
            if (v.test_status === vm.form.test_status) {
                return true;
            }
            return false;
        })
    }

    vm.updateQuestionStatus = function() {
        admin.updateVerifyPracticeQuestion({
                q_id: vm.selectedQuestion[0],
                test_status: vm.form.test_status,
                q_state_start_dt: new Date()
            })
            .then(function(res) {
                vm.onSelectedQuestionChange();
                vm.getAllQuestionStatues();
                vm.form.test_status = "";
            })
            .catch(function(err) {

            })
    }


    function checkValidityOfID() {
        vm.form.examId = vm.form.examId || "";
        vm.form.examId = vm.form.examId.replace(/\s/g, "-");
        var arr = vm.exams.filter(function(v, i) {
            if (vm.form.examId === v.examId) {
                return true;
            }
            return false;
        })
        if (arr.length > 0) {
            vm.idExists = true;
        } else {
            vm.idExists = false;
        }
    }

    function updateExamsList() {
        pageLoader.show();
        admin.getAllExam()
            .then(function(data) {
                if (data.data.length > 0)
                    vm.exams = data.data;
                else
                    vm.exams = [];
            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function updateExam() {
        var data = {
            examId: vm.form.examId,
            examName: vm.form.examName,
            examDesc: vm.form.examDesc
        }
        pageLoader.show();
        admin.updateExam(data)
            .then(function(res) {
                vm.submitted = true;
                vm.form.error = false;
                vm.form.examName = "";
                vm.form.examDesc = "";
                vm.form.examId = "";
                vm.idExists = false;
                vm.formIsInEditMode = false;
                updateExamsList();
            })
            .catch(function() {
                vm.submitted = true;
                vm.form.error = true;
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function resetFormDetail() {
        vm.submitted = false;
        vm.idExists = false;
        vm.formIsInEditMode = false;
        vm.form = {};
    }

    function removeExam(id) {
        var result = confirm("Want to delete?");
        if (!result) {
            //Declined
            return;
        }
        pageLoader.show();
        admin.removeExam({
                examId: id
            })
            .then(function(d) {
                console.log(d);
                updateExamsList();
            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function editExam(id) {

        var book = $filter('filter')(vm.exams, {
            examId: id
        })[0];
        vm.form = {};
        vm.form.examId = book.examId;
        vm.form.examName = book.examName;
        vm.form.examDesc = book.examDesc;
        vm.formIsInEditMode = true;
    }
}