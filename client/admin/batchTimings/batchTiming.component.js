require('./_batchTiming.scss');
angular.module(window.moduleName)
    .component('batchTimingsPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./batchTiming.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state'];
function Controller(admin,$filter,$scope,$state){

	var vm = this;
	vm.form = vm.form || {};
	vm.form.faculty_id = vm.form.faculty_id || [];
	vm.nowDate = (new Date());
    vm.yesterday = new Date(vm.nowDate);
    vm.yesterday.setDate(vm.nowDate.getDate() - 1);
    vm.yesterday = vm.yesterday.toString();

	vm.daysOfWeek = ["Monday",'Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']


	vm.addPriceToList = addPriceToList;
	vm.removePriceFromList = removePriceFromList;
	vm.saveTiming  = saveTiming;
	vm.deleteTiming  =deleteTiming;
	vm.editTiming = editTiming;
	vm.onTrainingChange = onTrainingChange;

	vm.$onInit = function(){
		getAllEmployees();
		getAllTiming();
		getAllCurrency();
		getAllCoursesFromMaster();
	}

	function getAllCoursesFromMaster() {
		admin.coursemaster()
			.then(function(d){
				vm.coursesMaster = d;
			})
			.catch(function(err){
				console.error(err);
			});
	}

	function onTrainingChange() {
		vm.form = vm.form || {};
		vm.form.bat_id = Math.floor(Math.random()*90000);
		vm.form.bat_nm = vm.form.crs_id + "-" + vm.form.bat_id;
	}

	function getAllCurrency(){
		admin.getAllCurrency()
			.then(function(res){
				vm.allCurrency = res.data
			})
	}

	function getAllEmployees(){
		admin.getAllEmployees()
			.then(function(res){
				vm.allEmps = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function getAllTiming(){
		admin.getAllBatchTiming()
			.then(function(res){
				vm.allTimings = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function saveTiming(){
		// vm.form.cls_start_dt = vm.bat_mn + "-" + vm.bat_dt + "-" + vm.bat_yr;
		vm.form.cls_frm_tm = vm.bat_frm_tm_hr + ":" + vm.bat_frm_tm_min;
		vm.form.cls_to_tm = vm.bat_to_tm_hr + ":" + vm.bat_to_tm_min;

		admin.addBatchTiming(vm.form)
			.then(function(res){
				getAllTiming();
				vm.form={};
				vm.bat_mn = "";
				vm.bat_dt = "";
				vm.bat_yr = "";
				vm.bat_frm_tm_hr = "";
				vm.bat_frm_tm_min = "";
				vm.bat_to_tm_hr = "";
				vm.bat_to_tm_min = "";
			})
			.catch(function(err){
				console.error(err);
			})
	}


	function addPriceToList(){
		vm.form = vm.form || {};
		vm.form.bat_crs_price = vm.form.bat_crs_price || {};
		if(vm.bat_price_curr && vm.bat_price_amount){
			vm.form.bat_crs_price[vm.bat_price_curr] = vm.bat_price_amount;
			vm.bat_price_curr = "";
			vm.bat_price_amount = "";
		}
	}

	function removePriceFromList(key){
		delete vm.form.bat_crs_price[key];
	}


	function deleteTiming(id){
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		admin.deleteBatchTiming({bat_id : id})
			.then(function(res){
				getAllTiming();
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function editTiming(id) {
		var batch = $filter('filter')(vm.allTimings,{bat_id : id})[0];

		var tempDate = new Date(batch.cls_start_dt);
		vm.bat_mn = "" + (tempDate.getMonth() + 1);
		vm.bat_dt = "" + (tempDate.getDate());
		vm.bat_yr = "" + (tempDate.getFullYear());
		batch.cls_start_dt = vm.bat_mn + "-" + vm.bat_dt + "-" +vm.bat_yr;

		var tempArr = batch.cls_frm_tm.split(":");
		vm.bat_frm_tm_hr = tempArr[0] || 0;
		vm.bat_frm_tm_min = tempArr[1] || 0;

		tempArr = batch.cls_to_tm.split(":");
		vm.bat_to_tm_hr = tempArr[0] || 0;
		vm.bat_to_tm_min = tempArr[1] || 0;

		vm.form = batch;

		vm.form.bat_nm = vm.form.bat_nm || (batch.crs_id + "-" + batch.bat_id)
	}
}