require("./_styles.scss")
angular.module(window.moduleName)
    .component('boardCompetitiveCoursesPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./boardCompetitiveCourse.html'),
        controller: AdminBoardCompetitiveCoursesController,
        controllerAs: 'vm'
    })

AdminBoardCompetitiveCoursesController.$inject = ['admin', '$filter', '$scope', '$state','alertify'];

function AdminBoardCompetitiveCoursesController(admin, $filter, $scope, $state , alertify) {

    var vm = this;

    vm.selectedCourse = [];
    vm.form = {};
    vm.form.children = [];

    vm.$onInit = function() {
        $scope.$watch(function() {
            return vm.parent.coursemaster;
        }, function(v) {
        	vm.actualAllCourses = v;
            vm.allCourses = v;
        });

        vm.getCourses();
    }

    vm.getCourses = function(){
    	admin.getBoardCompetitiveCourse()
        	.then(function(res){
        		vm.boardCompetitiveCourses = res.data;
        	})
        	.catch(function(err){
        		console.log(err)
        	})
    }

    vm.addChildToArray = function(){
    	if(!vm.childId || !vm.childName || !vm.childWeight){
    		alertify.error("Child id, Child name and Child Weight required")
    		return;
    	}
        vm.childId = vm.childId.replace(/ /g,"");
    	var arr = $filter('filter')(vm.form.children,{childId : vm.childId});
    	if(arr && arr.length > 0){
			alertify.error("Child id exists")
    	}else{
    		vm.form.children.push({
    			childId : vm.childId,
    			childName : vm.childName,
                childWeight : vm.childWeight
    		})
    		vm.childId = "";
    		vm.childName = "";
            vm.childWeight = "";
    		//every time update the db
    		vm.updateCourse();
    	}
    }

    vm.removeChildFromArray = function(index){
        var result = confirm("Are you sure you want to remove child?");
        if (!result) {
            //Declined
            return;
        }
    	vm.form.children.splice(index,1)
    	vm.updateCourse();
    }

    vm.editChildInArray = function(index){
    	vm.editingChild = index;
    }

    vm.saveChildInArray = function(index){
    	var obj = vm.form.children[index];
    	if(!obj.childId || !obj.childName || !obj.childWeight){
    		alertify.error("Child id, Child name and child weight required")
    		return;
    	}
    	vm.updateCourse();
    	vm.editingChild = undefined;
    }

    vm.changeCourseType = function(){
		if(vm.form.courseType){
			vm.allCourses = vm.actualAllCourses
			if(vm.form.courseType != 'both'){
				vm.allCourses = $filter("filter")(vm.allCourses,{courseType : vm.form.courseType});
			}else{
				vm.allCourses = vm.actualAllCourses
			}
		}
    }

    vm.onCourseChange = function(){
		if(vm.selectedCourse.length > 0){
			admin.getBoardCompetitiveCourse({
				course_id : vm.selectedCourse[0]
			})
			.then(function(res){
				if(res.data && res.data[0] && res.data[0].course_id){
					vm.form = res.data[0]
				}
			})
			.catch(function(err){
				console.log(err);
			})
		}else{
			vm.form.children = [];
		}
    }

    vm.updateCourse = function(){
    	vm.form.course_id = vm.selectedCourse[0]
    	delete vm.form._id
    	vm.isFormSaving = true;
    	admin.updateBoardCompetitiveCourse(vm.form)
    		.then(function(res){
				vm.isFormSaving = false;
				alertify.success("Data updated")
				vm.getCourses();
    		})
    		.catch(function(err){
    			console.log(err);
    			vm.isFormSaving = false;
    			alertify.success("Error in updating data")
    		})
    }

    vm.editCourse = function(id){
		var arr = $filter('filter')(vm.boardCompetitiveCourses,{course_id  : id});
		if(arr && arr[0]){
			vm.form = arr[0];
			vm.selectedCourse = [vm.form.course_id]
		}
    }

    vm.deleteCourse = function(id){
    	var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
    	admin.deleteBoardCompetitiveCourse({course_id : id})
    		.then(function(res){
				alertify.success("Course removed successfully.")
				vm.getCourses();
    		})
    		.catch(function(err){
    			alertify.success("Error in removing data")
    		})
    }
	
	vm.resetForm = function(){
		var result = confirm("Are you sure you want to reset data?");
        if (!result) {
            //Declined
            return;
        }
        vm.form = {};
        vm.selectedCourse = [];
        vm.form.children = [];
	}
}