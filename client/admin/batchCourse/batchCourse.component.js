require('./_batchCourse.scss');
require('angular-froala');
angular.module(window.moduleName)
    .component('batchCoursePage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./batchCourse.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
function Controller(admin,$filter,$scope,$state,EditorConfig){

	var vm = this;

	EditorConfig.toolbarButtons = EditorConfig.toolbarButtonsMD = EditorConfig.toolbarButtonsSM = EditorConfig.toolbarButtonsXS = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertImage', 'insertVideo', 'insertTable', 'html'],
    EditorConfig.quickInsertButtons = []
    EditorConfig.pluginsEnabled = ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"]
    EditorConfig.events = {
        'froalaEditor.keyup': function() {
            $(".preview-div").each(function(v, ele) {
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, ele]);
            })
        }
    }
    vm.EditorConfig = EditorConfig;

	vm.form = vm.form || {};

	vm.form.crs_id = [];
	vm.form.includedBundles = [];

	vm.changeTrainingDropdown = changeTrainingDropdown;

	vm.saveCourse = saveCourse;
	vm.deleteCourse = deleteCourse;
	vm.editCourse = editCourse;

	vm.addQuestionToFAQList = addQuestionToFAQList;
	vm.editQuestionFromFAQList = editQuestionFromFAQList;
	vm.removeQuestionFromFAQList = removeQuestionFromFAQList;

	vm.addPointToCurriculamList = addPointToCurriculamList;
	vm.editPointFromCurriculamList = editPointFromCurriculamList;
	vm.removePointFromCurriculamList = removePointFromCurriculamList;

	vm.addFeatureToList = addFeatureToList;
	vm.editFeatureFromList = editFeatureFromList;
	vm.removeFeatureFromList = removeFeatureFromList;

	vm.addContactPhoneToList = addContactPhoneToList;
	vm.editContactPhoneFromList = editContactPhoneFromList;
	vm.removeContactPhoneFromList = removeContactPhoneFromList;

	vm.addContactEmailToList = addContactEmailToList;
	vm.editContactEmailFromList = editContactEmailFromList;
	vm.removeContactEmailFromList = removeContactEmailFromList;

	vm.$onInit = function(){
		// getAll();
		getAllCoursesFromMaster();
		getAllBundle();

		setTimeout(function(){
			// $('#about-course').froalaEditor(EditorConfig);
			// $('#curriculam').froalaEditor(EditorConfig);
			// $('#why-course').froalaEditor(EditorConfig);
		})
	}

	function getAllCoursesFromMaster() {
		admin.coursemaster()
			.then(function(d){
				vm.coursesMaster = d;
				vm.coursesMaster = vm.coursesMaster.filter(function(v,i){
					return v.isLiveClass
				})
			})
			.catch(function(err){
				console.error(err);
			});
	}

	function changeTrainingDropdown() {
		// var course = $filter('filter')(vm.allCourses,{crs_id : vm.form.crs_id[0]})[0];
		// var new_course_copy = {};
		// angular.copy(course,new_course_copy);
		if(vm.form.crs_id[0]){
			editCourse(vm.form.crs_id[0]);
		}else{
			vm.form = {};
			// $('#about-course').froalaEditor('html.set', '');
			// $('#curriculam').froalaEditor('html.set', '');
			// $('#why-course').froalaEditor('html.set', '');
			// vm.form.crs_id = course_id;
		}
	}

	function getAll(){
		admin.getAllBatchCourse()
			.then(function(res){
				vm.allCourses = res.data;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function getAllBundle() {
		admin.getAllBundle()
			.then(function(res){
				vm.allBundle = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function saveCourse(){
		vm.formSuccess = undefined;
		// vm.form.abt_crs = $('#about-course').froalaEditor('html.get', true);
		// vm.form.curriculam = $('#curriculam').froalaEditor('html.get', true);
		// vm.form.why_this_crs = $('#why-course').froalaEditor('html.get', true);
		vm.form.crs_id = vm.form.crs_id[0]
		vm.showLoader = true;
		admin.addBatchCourse(vm.form)
			.then(function(res){
				// getAll();
				vm.form = {};
				// $('#about-course').froalaEditor('html.set', '');
				// $('#curriculam').froalaEditor('html.set', '');
				// $('#why-course').froalaEditor('html.set', '');
				vm.formSuccess = true;
			})
			.catch(function(err){
				console.log(err);
				vm.formSuccess = false;
			})
			.finally(function(){
				vm.showLoader = false;
			})
	}

	function editCourse(id){
		vm.showLoader = true;
		admin.getBatchCourse({
			crs_id:id
		})
		.then(function(res) {
			// var cs = res.data;
			vm.form = {};
			vm.form.crs_id = [id];
			vm.form.includedBundles = res.data.includedBundles;
			vm.form.no_of_days = res.data.no_of_days;
			vm.form.abt_crs = res.data.abt_crs;
			vm.form.curriculam = res.data.curriculam;
			vm.form.why_this_crs = res.data.why_this_crs;
			vm.form.faqs = res.data.faqs;
			vm.form.contactPhone = res.data.contactPhone;
			vm.form.contactEmail = res.data.contactEmail;
			vm.form.features = res.data.features;
			console.log(vm.form)
		})
		.finally(function(){
			vm.showLoader = false;
		})
		// var course = $filter('filter')(vm.allCourses,{crs_id:id})[0];

		// var new_course_copy = {};
		// angular.copy(course,new_course_copy);

		// vm.form = new_course_copy;
		// $('#about-course').froalaEditor('html.set', vm.form.abt_crs);
		// // $('#curriculam').froalaEditor('html.set', vm.form.curriculam);
		// $('#why-course').froalaEditor('html.set', vm.form.why_this_crs);
	}

	function deleteCourse(id){
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		admin.deleteBatchCourse({crs_id:id})
			.then(function(res){
				getAll();
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function addQuestionToFAQList(){
		vm.form = vm.form || {};
		vm.form.faqs = vm.form.faqs || [];
		if(vm.FAQquestionTitle && vm.FAQquestionAns){
			vm.form.faqs.push({
				title : vm.FAQquestionTitle,
				ans : vm.FAQquestionAns
			})
			vm.FAQquestionTitle = "";
			vm.FAQquestionAns = "";
		}
	}

	function editQuestionFromFAQList(index) {
		var faq = vm.form.faqs[index];
		vm.FAQquestionTitle = faq.title;
		vm.FAQquestionAns = faq.ans;
		vm.form.faqs.splice(index,1);
	}

	function removeQuestionFromFAQList(index) {
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		vm.form.faqs.splice(index,1);
		admin.addBatchCourse(vm.form);
	}


	function addPointToCurriculamList(){
		vm.form = vm.form || {};
		vm.form.curriculam = vm.form.curriculam || [];
		if(vm.curriculamTitle && vm.curriculamDesc && vm.curriculamWeight){
			var obj = {
				title : vm.curriculamTitle,
				desc : vm.curriculamDesc,
				weight : vm.curriculamWeight
			};
			if(vm.currentlyEditingCurriculam !== undefined){
				vm.form.curriculam.splice(vm.currentlyEditingCurriculam,0,obj);
			}else{
				vm.form.curriculam.push(obj)
			}
			vm.curriculamTitle = "";
			vm.curriculamWeight = "";
			vm.curriculamDesc = "";
			vm.currentlyEditingCurriculam = undefined;
		}
	}

	function editPointFromCurriculamList(index) {
		var curriculam = vm.form.curriculam[index];
		vm.currentlyEditingCurriculam = index;
		vm.curriculamTitle = curriculam.title;
		vm.curriculamDesc = curriculam.desc;
		vm.curriculamWeight = curriculam.weight;
		vm.form.curriculam.splice(index,1);
	}

	function removePointFromCurriculamList(index) {
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		vm.form.curriculam.splice(index,1);
		admin.addBatchCourse(vm.form);
	}



	function addFeatureToList(){
		vm.form = vm.form || {};
		vm.form.features = vm.form.features || [];
		if(vm.FeatureTitle && vm.FeatureDesc){
			vm.form.features.push({
				title : vm.FeatureTitle,
				desc : vm.FeatureDesc
			})
			vm.FeatureTitle = "";
			vm.FeatureDesc = "";
		}
	}

	function editFeatureFromList(index) {
		var feature = vm.form.features[index];
		vm.FeatureTitle = feature.title;
		vm.FeatureDesc = feature.desc;
		vm.form.features.splice(index,1);
	}

	function removeFeatureFromList(index) {
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		vm.form.features.splice(index,1);
		admin.addBatchCourse(vm.form);
	}

	function addContactPhoneToList(){
		vm.form = vm.form || {};
		vm.form.contactPhone = vm.form.contactPhone || [];
		if(vm.contactPhone){
			vm.form.contactPhone.push(vm.contactPhone)
			vm.contactPhone = "";
		}
	}

	function editContactPhoneFromList(index) {
		var phone = vm.form.contactPhone[index];
		vm.contactPhone = phone;
		vm.form.contactPhone.splice(index,1);
	}

	function removeContactPhoneFromList(index) {
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		vm.form.contactPhone.splice(index,1);
		admin.addBatchCourse(vm.form);
	}

	function addContactEmailToList(){
		vm.form = vm.form || {};
		vm.form.contactEmail = vm.form.contactEmail || [];
		if(vm.contactEmail){
			vm.form.contactEmail.push(vm.contactEmail)
			vm.contactEmail = "";
		}
	}

	function editContactEmailFromList(index) {
		var contact = vm.form.contactEmail[index];
		vm.contactEmail = contact;
		vm.form.contactEmail.splice(index,1);
	}

	function removeContactEmailFromList(index) {
		var result = confirm("Are you sure you want to delete?");
        if (!result) {
            //Declined
            return;
        }
		vm.form.contactEmail.splice(index,1);
		admin.addBatchCourse(vm.form);
	}
}
