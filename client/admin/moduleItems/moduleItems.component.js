	require('service.aws');
	angular.module(window.moduleName)
	    .component('moduleItemsPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: require('./moduleItems.html'),
	        controller: Controller,
	        controllerAs: 'vm'
	    });

	Controller.$inject = ['admin', 'pageLoader', '$filter', '$scope','EditorConfig','awsService'];

	function Controller(admin, pageLoader, $filter, $scope,EditorConfig,awsService) {

	    var vm = this;

	    vm.videolist = Array.apply(null, {
	        length: 10
	    }).map(Number.call, Number);
	    vm.hrlist = Array.apply(null, {
	        length: 24
	    }).map(Number.call, Number);
	    vm.minlist = Array.apply(null, {
	        length: 60
	    }).map(Number.call, Number);
	    vm.seclist = Array.apply(null, {
	        length: 60
	    }).map(Number.call, Number);

	    vm.selectedSubjects = [];
	    vm.selectedAuthors = [];
	    vm.selectedPublications = [];
	    vm.selectedBooks = [];
	    vm.selectedExams = [];
	    vm.selectedTopics = [];
	    vm.selectedTags = [];

	    vm.form = {};
	    vm.videosOfModule = [];
	    vm.itemsOfModule = [];
	    vm.shouldShowVideoForm = false;

	    vm.filteredQuestionsPageNumber = 1;
	    vm.filteredQuestionsPageCount = 1;
	    vm.selectedQuestionsPageNumber = 1;
	    vm.selectedQuestionsPageCount = 1;
	    vm.questionsInOnePage = 3;




	    vm.$onInit = function() {
	        $scope.$watch(function() {
	            return vm.parent.courselistdata;
	        }, function(v) {
	        	vm.courselist = vm.courselist || [];
	        	if(v){
	        		vm.courselist = vm.courselist.concat(v);
	        		getAllTiming();
	        	}
	        });
	        
	        admin.getAllSubject()
	            .then(function(d) {
	                vm.subjects = d.data;
	            });
	        admin.getAllAuthor()
	            .then(function(d) {
	                vm.authors = d.data;
	            });
	        admin.getAllPublication()
	            .then(function(d) {
	                vm.publications = d.data;
	            });
	        admin.getAllBook()
	            .then(function(d) {
	                vm.books = d.data;
	            });
	        admin.getAllExam()
	            .then(function(d) {
	                vm.exams = d.data;
	            });
	        admin.getAllTopic()
	            .then(function(d) {
	                vm.topics = d.data;
	            });
	        admin.getAllTag()
	            .then(function(d) {
	                vm.tags = d.data;
	            });
	        admin.getAllSideLinksArticlePage()
	        	.then(function(d){
	        		vm.allSideLinksArticle = d.data;
	        	})

	         //Observer files
			$scope.$watch('vm.materialFile',function(newValue,oldValue){
				if(newValue){
					console.log(newValue);
					vm.materialFileName = newValue.name;	
					vm.isMaterialUploadInProgress = false;
					vm.isMaterialUploaded = false;
				}
				
			})
	    };

	    vm.getNumber = getNumber;
	    vm.changeCourse = changeCourse;
	    vm.onChangeIdField = onChangeIdField;
	    vm.addItems = addItems;
	    vm.getVideosOfModule = getVideosOfModule;
	    vm.getAllModuleItems = getAllModuleItems;
	    vm.changeModule = changeModule;
	    vm.deleteItemOfModule = deleteItemOfModule;
	    vm.editItemOfModule = editItemOfModule;
	    vm.resetDetails = resetDetails;

	    vm.showVideoForm = showVideoForm;
	    vm.showQuestionGroupForm = showQuestionGroupForm;
	    vm.showDownloadForm  = showDownloadForm;
	    vm.showBookTopicForm = showBookTopicForm;
	    vm.showUploadForm = showUploadForm;

	    vm.onFilterChange = onFilterChange;
	    vm.addToSelectedQuestions = addToSelectedQuestions;
	    vm.removeFromSelectedQuestions = removeFromSelectedQuestions;
	    vm.isQuestionSelected = isQuestionSelected;

	    vm.addToSelectedVideo = addToSelectedVideo;
	    vm.isVideoSelected = isVideoSelected;

	    vm.fecthFilteredQuestions = fecthFilteredQuestions;
	    vm.filteredQuestionPrev = filteredQuestionPrev;
	    vm.filteredQuestionNext = filteredQuestionNext;

	    vm.fecthSelectedQuestions = fecthSelectedQuestions;
	    vm.selectedQuestionPrev = selectedQuestionPrev;
	    vm.selectedQuestionNext = selectedQuestionNext;

	    vm.uploadMaterialFile = uploadMaterialFile;
	    vm.uploadDownloadMaterialFile = uploadDownloadMaterialFile;

	    function getNumber(num) {
	        return Array.apply(null, {
	            length: num
	        });
	    }

	    function getAllTiming(){
			admin.getAllBatchTiming()
				.then(function(res){
					vm.allTimings = res.data;
					var batches = vm.allTimings.map(function(v,i) {
						var nm = v.bat_nm || (v.crs_id + "-" + v.bat_id);

						var parentTraining = $filter('filter')(vm.courselist, {
				            courseId: v.crs_id
				        })[0];

						return {
							courseId : nm,
							courseName : v.crs_id + "-" + $filter("date")(v.cls_start_dt,"dd-MMM-yyyy"),
							isFromBatch : true,
							displayCourseName : parentTraining.courseName,
							training_id : v.crs_id
						}
					})
					vm.courselist = vm.courselist || [];
					if(batches){
						vm.courselist = vm.courselist.concat(batches) 	
					}
					
				})
				.catch(function(err){
					console.error(err);
				})
		}


	    function changeModule() {
	        //Clear All Videos
	        vm.itemsOfModule = [];
	        var formData = vm.form;

	        //Clear Form Data and Set Necessary Items
	        vm.form = {};
	        vm.form.item = {};
	        vm.form.course = formData.course;
	        vm.form.module = formData.module;
	        var moduleItem = $filter('filter')(vm.modulelist, {
	            moduleNumber: vm.form.module
	        })[0];
	        pageLoader.show();
	        vm.getAllModuleItems(moduleItem.moduleId)
	            .then(function(res) {
	                if (res.data.length > 0) {

	                    var modules = res.data[0].moduleDetail;
	                    var m = $filter('filter')(modules, {
	                        moduleId: moduleItem.moduleId
	                    })[0];
	                    vm.itemsOfModule = m.moduleItems;
	                    vm.itemsOfModule.sort(compare);

	                    vm.itemNumber = vm.itemsOfModule[vm.itemsOfModule.length - 1].itemNumber + 1;
	                    vm.form.item.itemWeight = vm.itemNumber;
	                    console.log(vm.form.item.itemWeight);
	                    console.log(modules);
	                } else {
	                    vm.itemsOfModule = [];
	                    vm.itemNumber = 1;
	                    vm.form.item.itemWeight = vm.itemNumber;
	                }
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            });
	    }

	    function changeCourse() {
	        vm.modulelist = [];
	        pageLoader.show();
	        admin.getCourseModules({
	            'courseId': vm.form.course
	        })
	            .then(function(res) {
	                console.log(res);
	                vm.modulelist = res;
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function onChangeIdField() {
	    	var arr = vm.itemsOfModule.filter(function(v,i) {
				if(v.itemId === vm.form.item.itemId){
					return true;
				}
				return false;
			})
			if(arr.length > 0){
				vm.idExistError = true;
			}else{
				vm.idExistError = false;
			}
	    }

	    function addItems() {

	        var courseItem = $filter('filter')(vm.courselist, {
	            courseId: vm.form.course
	        })[0];
	        var moduleItem = $filter('filter')(vm.modulelist, {
	            moduleNumber: vm.form.module
	        })[0];

	        vm.form.item.itemNumber = vm.itemNumber;
	        vm.form.item.itemDescription = $('#item-desc').froalaEditor('html.get',true);
	        if (vm.shouldShowQuestionGroupForm) {
	            vm.form.item.itemType = "qGroup";
	            vm.form.item.itemQuestions = vm.selectedQuestions.map(function(v) {
	                return v.questionId
	            });
	            vm.form.item.itemTotalQuestions = (vm.form.item.itemQuestions || []).length

	            var totalTimeOfQuesGrp = 0;
	            vm.selectedQuestions.map(function(v) {
	                totalTimeOfQuesGrp += v.questionTime;
	            });
	            vm.form.item.itemQuestionsTime = totalTimeOfQuesGrp;
	        } else if (vm.shouldShowVideoForm) {
	            vm.form.item.itemType = "video";
	            vm.form.item.itemVideo = vm.selectedVideo;
	        } else if (vm.shouldShowDownloadForm) {
	            vm.form.item.itemType = "download";
	        } else if (vm.shouldShowUploadForm) {
	            vm.form.item.itemType = "upload";
	        } else if (vm.shouldShowBookTopicForm) {
	            vm.form.item.itemType = "book-topic";
	            vm.form.item.itemBookTopic = vm.selectedBookTopic;
	        }

	        //Add Item Id
	        // vm.form.item.itemId = moduleItem.moduleId + "_" + vm.form.item.itemNumber;

	        vm.itemsOfModule = vm.itemsOfModule.filter(function(obj) {
	            if (obj.itemId == vm.form.item.itemId) {
	                //Dont return if it is same video 
	                //Because we are going to add that video in next few lines
	                return false;
	            }
	            return true;
	        });
	        // console.log("........",vm.form.item);
	        vm.itemsOfModule.push(vm.form.item);
	        vm.itemsOfModule.sort(compare);
	        var data = {
	            courseId: vm.form.course,
	            courseName: !courseItem.isFromBatch ? courseItem.courseName : courseItem.displayCourseName,
	            LastUpdated: courseItem.LastUpdated,
	            courseLongDesc: courseItem.courseLongDesc,
	            courseShortDesc: courseItem.courseShortDesc,
	            courseLevel: courseItem.courseLevel,
	            isBatch : courseItem.isFromBatch,
	            training_id : courseItem.isFromBatch ? courseItem.training_id : undefined,
	            moduleDetail: {
	                moduleNumber: vm.form.module,
	                moduleId: moduleItem.moduleId,
	                moduleName: moduleItem.moduleName,
	                moduleDesc: moduleItem.moduleDesc,
	                moduleItems: vm.itemsOfModule,
	                moduleWeight: parseFloat(moduleItem.moduleWeight)
	            },
	            author: courseItem.author
	        }

	        var authorizeItemObject = {
	        	courseId: vm.form.course,
	            courseName: courseItem.courseName,
	            moduleId: moduleItem.moduleId,
	            moduleName: moduleItem.moduleName,
	            itemId : vm.form.item.itemId,
	            itemType : 'paid',
	            itemVideo : vm.form.item.itemVideo,
	            itemName : vm.form.item.itemName
	        }
	        admin.addAuthorizedItem(authorizeItemObject);



	        vm.form = {};
	        vm.form.course = data.courseId;
	        vm.form.module = data.moduleDetail.moduleNumber;
	        vm.form.item = {};
	        // vm.form.item.itemNumber = vm.itemsOfModule.length+1;
	        vm.shouldShowVideoForm = false;
	        vm.shouldShowQuestionGroupForm = false;
	        vm.shouldShowBookTopicForm = false;
	        vm.shouldShowDownloadForm = false;
	        vm.shouldShowUploadForm = false;
	        vm.filteredQuestions = [];
	        vm.selectedQuestions = [];

	        pageLoader.show();
	        admin.updateModuleItems(data)
	            .then(function() {
	                vm.submitted = true;
	                vm.form.error = false;
	                changeModule();
	            })
	            .catch(function() {
	                vm.submitted = true;
	                vm.form.error = false;
	            })
	            .finally(function() {
	                pageLoader.hide()
	            });


	        console.log(data);
	    }

	    function getVideosOfModule(moduleId) {
	        //Return Admin Service
	        return admin.getModuleVideos({
	            moduleId: moduleId
	        });
	    }

	    function getAllModuleItems(moduleId) {
	        //Return Admin Service
	        return admin.getAllModuleItems({
	            moduleId: moduleId
	        });
	    }


	    function editItemOfModule(itemId) {

	        var item = $filter('filter')(vm.itemsOfModule, {
	            itemId: itemId
	        })[0];
	        // console.log(item);
	        vm.form = vm.form || {};
	        vm.form.item = vm.form.item || {};
	        vm.resetDetails();
	        vm.form.item.itemId = item.itemId;
	        vm.form.item.itemName = item.itemName;
	        vm.form.item.materialInfo = item.materialInfo;
	        vm.form.item.itemWeight = item.itemWeight;
	        vm.form.item.itemDescription = item.itemDescription;
	        vm.form.item.itemTakeTest = item.itemTakeTest;
	        vm.form.item.itemOptionType = item.itemOptionType;
	        vm.form.item.slidePageId = item.slidePageId ? [].concat(item.slidePageId) : [];
	        vm.idExistError = false;
	        vm.itemNumber = item.itemNumber;
	        if (item.itemType === "qGroup") {
	            vm.shouldShowQuestionGroupForm = true;
	            vm.form.item.itemQuestionsTime = item.itemQuestionsTime;
	            // vm.form.item.itemName = item.itemName;
	            // vm.form.item.itemWeight = item.itemWeight;
	            // vm.itemNumber =item.itemNumber;
	            admin.getPracticeQuestionsById({
	                questionsId: item.itemQuestions
	            })
	                .then(function(res) {
	                    console.log(res);
	                    vm.selectedQuestions = res.data;
	                })
	                .catch(function(err) {
	                    console.error(err);
	                });
	        } else if (item.itemType === "video") {
	            vm.shouldShowVideoForm = true;
	            vm.form.item.itemContainsSlide = item.itemContainsSlide;
	            // vm.itemNumber =item.itemNumber;
	            // // vm.form.item.itemName = item.itemName;
	            // vm.form.item.itemWeight = item.itemWeight;
	            admin.findVideoEntityById({
	                videoId: item.itemVideo
	            })
	                .then(function(res) {
	                    console.log(res);
	                    vm.filteredVideos = res.data;
	                    if(res.data && res.data.length > 0){
	                    	vm.selectedVideo = res.data[0].videoId;	
	                    }
	                    
	                })
	                .catch(function(err) {
	                    console.log(err);
	                })
	        } else if (item.itemType === "download") {
	        	vm.shouldShowDownloadForm = true;
	        	vm.materialFileName = item.downloadLink || "";
	        } else if (item.itemType === "upload") {
	        	vm.shouldShowUploadForm = true;
	        } else if (item.itemType === "book-topic") {
	        	vm.shouldShowBookTopicForm = true;
	        	admin.findBookTopicsByQuery({
	                bookTopicId: [item.itemBookTopic]
	            })
	                .then(function(res) {
	                    console.log(res);
	                    vm.filteredBookTopics = res.data;
	                    if(res.data && res.data.length > 0){
	                    	vm.selectedBookTopic = res.data[0].bookTopicId;	
	                    }
	                })
	                .catch(function(err) {
	                    console.log(err);
	                })
	        }

	        setTimeout(function(){
	        	$('#item-desc').froalaEditor(EditorConfig);

	        	$('#item-desc').froalaEditor('html.set',item.itemDescription);
	        })
	    }

	    function deleteItemOfModule(itemId) {
	        var result = confirm("Want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        var courseItem = $filter('filter')(vm.courselist, {
	            courseId: vm.form.course
	        })[0];
	        var moduleItem = $filter('filter')(vm.modulelist, {
	            moduleNumber: vm.form.module
	        })[0];

	        vm.itemsOfModule = vm.itemsOfModule.filter(function(data) {
	            if (data.itemId === itemId) {
	                return false;
	            }
	            return true;
	        });

	        var data = {
	            courseId: vm.form.course,
	            courseName: courseItem.courseName,
	            LastUpdated: courseItem.LastUpdated,
	            courseLongDesc: courseItem.courseLongDesc,
	            courseShortDesc: courseItem.courseShortDesc,
	            courseLevel: courseItem.courseLevel,
	            moduleDetail: {
	                moduleNumber: vm.form.module,
	                moduleId: moduleItem.moduleId,
	                moduleName: moduleItem.moduleName,
	                moduleItems: vm.itemsOfModule,
	                moduleWeight: parseFloat(moduleItem.moduleWeight)
	            },
	            author: courseItem.author
	        }
	        pageLoader.show();
	        admin.updateModuleItems(data)
	            .then(function(d) {
	                console.log("Removed Item")
	            })
	            .catch(function() {
	                console.error(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            });
	    }

	    function resetDetails() {
	        vm.shouldShowQuestionGroupForm = false;
	        vm.shouldShowVideoForm = false;
	        vm.shouldShowDownloadForm = false;
	        vm.shouldShowBookTopicForm = false;
	        vm.form.item = {};
	        vm.selectedSubjects = [];
	        vm.selectedAuthors = [];
	        vm.selectedPublications = [];
	        vm.selectedBooks = [];
	        vm.selectedExams = [];
	        vm.selectedTopics = [];
	        vm.selectedTags = [];
	        vm.filteredQuestions = [];
	        vm.selectedQuestions = [];
	        vm.filteredQuestionsPageCount = 1;
	    }

	    function showQuestionGroupForm() {
	        vm.resetDetails();
	        vm.shouldShowQuestionGroupForm = true;
	        changeFormType();
	    }

	    function showVideoForm() {
	        vm.resetDetails();
	        vm.shouldShowVideoForm = true;
	        changeFormType();
	    }

	    function showBookTopicForm() {
	        vm.resetDetails();
	        vm.shouldShowBookTopicForm = true;
	        changeFormType();
	    }

	    function showDownloadForm() {
	        vm.resetDetails();
	        vm.shouldShowDownloadForm = true;
	        changeFormType();
	    }

	    function showUploadForm() {
	        vm.resetDetails();
	        vm.shouldShowUploadForm = true;
	        changeFormType();
	    }

	    function changeFormType() {
	    	if (vm.itemsOfModule.length > 0) {
	            vm.itemsOfModule.sort(compare);
	            vm.itemNumber = vm.itemsOfModule[vm.itemsOfModule.length - 1].itemNumber + 1;
	            vm.form.item.itemWeight = vm.itemNumber;
	        } else {
	            vm.itemNumber = 1;
	            vm.form.item.itemWeight = vm.itemNumber;
	        }

	        setTimeout(function(){
	        	$('#item-desc').froalaEditor(EditorConfig);
	        })
	    }

	    function onFilterChange(item, model) {
	        if (vm.selectedSubjects.length > 0) {
	            admin.getAllTopic({
	                subjectId: vm.selectedSubjects[0].subjectId
	            })
	                .then(function(d) {
	                    vm.topics = d.data;
	                });
	        }
	        if (vm.selectedTopics.length > 0) {
	            vm.form = vm.form || {};
	            vm.form.item = vm.form.item || {};
	            vm.form.item.itemName = vm.selectedTopics[0].topicName;
	        } else {
	            vm.form = vm.form || {};
	            vm.form.item = vm.form.item || {};
	            vm.form.item.itemName = "";
	        }

	        var query = {
	                subjects: vm.selectedSubjects.map(function(v) {
	                    return v.subjectId
	                }),
	                authors: vm.selectedAuthors.map(function(v) {
	                    return v.authorId
	                }),
	                publications: vm.selectedPublications.map(function(v) {
	                    return v.publicationId
	                }),
	                books: vm.selectedBooks.map(function(v) {
	                    return v.bookId
	                }),
	                exams: vm.selectedExams.map(function(v) {
	                    return v.examId
	                }),
	                topics: vm.selectedTopics.map(function(v) {
	                    return v.topicId
	                }),
	                tags: vm.selectedTags.map(function(v) {
	                    return v.tagId
	                })
	            }
	            // if(vm.shouldShowQuestionGroupForm){
	        admin.findPracticeQuestionsByQuery(query)
	            .then(function(d) {
	                vm.filteredQuestions = d.data || [];
	                if (vm.filteredQuestions.length > vm.questionsInOnePage) {
	                    vm.filteredQuestionsPageCount = Math.ceil(vm.filteredQuestions.length / vm.questionsInOnePage);
	                    console.log(vm.filteredQuestionsPageCount);
	                }
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	        // }else if(vm.shouldShowVideoForm){
	        admin.findVideoEntityByQuery(query)
	            .then(function(d) {
	                vm.filteredVideos = d.data;
	                console.log(d);
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	        // }

			admin.findBookTopicsByQuery(query)
	            .then(function(d) {
	                vm.filteredBookTopics = d.data;
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	    }

	    function fecthFilteredQuestions() {
	        var start = (vm.filteredQuestionsPageNumber - 1) * vm.questionsInOnePage;
	        if (vm.filteredQuestions) {
	            return vm.filteredQuestions.slice(start, start + vm.questionsInOnePage);
	        } else {
	            return [];
	        }
	    }

	    function filteredQuestionPrev() {
	        if (vm.filteredQuestionsPageNumber > 1) {
	            vm.filteredQuestionsPageNumber--;
	        }
	    }

	    function filteredQuestionNext() {
	        if (vm.filteredQuestionsPageNumber < vm.filteredQuestionsPageCount) {
	            vm.filteredQuestionsPageNumber++;
	        }
	    }

	    function fecthSelectedQuestions() {
	        var start = (vm.selectedQuestionsPageNumber - 1) * vm.questionsInOnePage;
	        if (vm.selectedQuestions) {
	            return vm.selectedQuestions.slice(start, start + vm.questionsInOnePage);
	        } else {
	            return [];
	        }
	    }

	    function selectedQuestionPrev() {
	        if (vm.selectedQuestionsPageNumber > 1) {
	            vm.selectedQuestionsPageNumber--;
	        }
	    }

	    function selectedQuestionNext() {
	        if (vm.selectedQuestionsPageNumber < vm.selectedQuestionsPageCount) {
	            vm.selectedQuestionsPageNumber++;
	        }
	    }

	    function addToSelectedQuestions(questionId) {
	        var que = $filter('filter')(vm.filteredQuestions, {
	            questionId: questionId
	        })[0];
	        vm.selectedQuestions = vm.selectedQuestions || [];
	        vm.selectedQuestions.push(que);

	        if (vm.selectedQuestions.length > vm.questionsInOnePage) {
	            vm.selectedQuestionsPageCount = Math.ceil(vm.selectedQuestions.length / vm.questionsInOnePage);
	        }
	    }

	    function removeFromSelectedQuestions(questionId) {
	        var que = $filter('filter')(vm.selectedQuestions, {
	            questionId: questionId
	        })[0];
	        var pos = vm.selectedQuestions.indexOf(que);
	        vm.selectedQuestions.splice(pos, 1);
	    }

	    function isQuestionSelected(questionId) {
	        var que = $filter('filter')(vm.selectedQuestions, {
	            questionId: questionId
	        }) || [];
	        return (que.length > 0);
	    }

	    function addToSelectedVideo(id) {
	        vm.selectedVideo = id;
	    }

	    function isVideoSelected(id) {
	        if (vm.selectedVideo === id) return true;
	        return false;
	    }


	    function compare(a, b) {
	        return a.itemWeight - b.itemWeight;
	    }


	    function uploadMaterialFile(){
	    	var file = {
	    		type : 'download',
	    		file : vm.materialFile
	    	}
	    	vm.isMaterialUploadInProgress = true;
	    	awsService.uploadFileToS3(file)
	    		.then(function(d){
					var data = d.data;
					vm.form = vm.form || {};
					vm.form.item = vm.form.item || {};
					vm.form.item.materialInfo = data.Key;
					vm.isMaterialUploadInProgress = false;
					vm.isMaterialUploaded = true;
				},function(err){
					console.error(err);
				},function(evt){
					var width = parseInt(100.0 * evt.loaded / evt.total);
					console.log(width);
					vm.uploadProgress = width;
				})
	    }

	    function uploadDownloadMaterialFile(){
	    	var file = {
	    		type : 'download',
	    		file : vm.materialFile
	    	}
	    	vm.isMaterialUploadInProgress = true;
	    	awsService.uploadFileToS3(file)
	    		.then(function(d){
					var data = d.data;
					vm.form = vm.form || {};
					vm.form.item = vm.form.item || {};
					vm.form.item.downloadLink = data.Key;
					vm.isMaterialUploadInProgress = false;
					vm.isMaterialUploaded = true;
				},function(err){
					console.error(err);
				},function(evt){
					var width = parseInt(100.0 * evt.loaded / evt.total);
					console.log(width);
					vm.uploadProgress = width;
				})
	    }

	    

	}