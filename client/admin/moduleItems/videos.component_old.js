(function(){
	angular.module(window.moduleName)
        .component('videoPage', {
        	require: {
        		parent : '^adminPage'
        	},
            templateUrl: '/admin/videos/videos.html',
            controller: AdminVideosController,
            controllerAs: 'avc'
        });

	AdminVideosController.$inject=['admin','pageLoader','$filter','$scope'];



	function AdminVideosController(admin,pageLoader,$filter,$scope) {

    	var avc = this;

		avc.$onInit = function() {
        	$scope.$watch(function(){return avc.parent.courselistdata;},function(v){
        		avc.courselist = v;
        	});
	    };

		avc.videolist = Array.apply(null, {length: 10}).map(Number.call, Number);
		avc.hrlist = Array.apply(null, {length: 24}).map(Number.call, Number);
		avc.minlist = Array.apply(null, {length: 60}).map(Number.call, Number);
		avc.seclist = Array.apply(null, {length: 60}).map(Number.call, Number);

		avc.getNumber = getNumber;

    	avc.changeCourse = changeCourse;
    	avc.addVideos = addVideos;
    	avc.getVideosOfModule = getVideosOfModule;
    	avc.changeModule = changeModule;
    	avc.deleteVideoOfModule = deleteVideoOfModule;
    	avc.editVideoOfModule = editVideoOfModule;
    	avc.resetFormDetail = resetFormDetail;

    	avc.form = {};
		avc.videosOfModule = [];
		avc.shouldShowVideoForm = false;



    	function getNumber(num) {
		    return Array.apply(null,{length:num});   
		}

		function resetFormDetail(){
			avc.form.video = {};
			if(avc.videosOfModule){
				avc.form.video.videoNumber = avc.videosOfModule.length + 1;
			}else{
				avc.form.video.videoNumber = 1;
			}
			
			avc.shouldShowVideoForm = true;
		}

		function changeModule(){
			//Clear All Videos
			avc.videosOfModule = [];
			var formData = avc.form;

			//Clear Form Data and Set Necessary Items
			avc.form = {};
    		avc.form.video = {};
    		avc.form.course = formData.course;
    		avc.form.module = formData.module;
    		var moduleItem = $filter('filter')(avc.modulelist, {moduleNumber: avc.form.module})[0];
    		 pageLoader.show();
    		 avc.getVideosOfModule(moduleItem.moduleId)
    		 			.then(function (data){
							avc.form.video.videoNumber=data.length+1;
							avc.videosOfModule = data;
							avc.videosOfModule.sort(compare);
						})
    		 			.catch(function (err){console.error(err);})
    		 			.finally(function(){pageLoader.hide();});
		}

    	function changeCourse(){
			avc.modulelist={};
    		pageLoader.show();
			admin.getCourseModules({'courseId':avc.form.course})
				.then(function(res){
					console.log(res);
					avc.modulelist=res;
				})
				.finally(function(){
					pageLoader.hide();
				})
		}

		function addVideos(){

			var courseItem = $filter('filter')(avc.courselist, {courseId: avc.form.course})[0];
			var moduleItem = $filter('filter')(avc.modulelist, {moduleNumber: avc.form.module})[0];

			//Add Video Id
			avc.form.video.videoId = moduleItem.moduleId + "_" + avc.form.video.videoNumber;
			
			avc.videosOfModule = avc.videosOfModule.filter(function(obj){
				if(obj.videoId == avc.form.video.videoId){
					//Dont return if it is same video 
					//Because we are going to add that video in next few lines
					return false;
				}
				return true;
			});
			console.log("Video : " , avc.form.video);
			if(!avc.form.video.videoDuration.hr){
				avc.form.video.videoDuration.hr = 0;
			}
			if(!avc.form.video.videoDuration.min){
				avc.form.video.videoDuration.min = 0;
			}
			if(!avc.form.video.videoDuration.sec){
				avc.form.video.videoDuration.sec = 0;
			}
			console.log("Video : " , avc.form.video);
			avc.videosOfModule.push(avc.form.video);
			avc.videosOfModule.sort(compare);

			var data = {
				courseId : avc.form.course,
				courseName : moduleItem.courseName,
				LastUpdated : courseItem.LastUpdated,
				courseLongDesc : courseItem.courseLongDesc,
				courseShortDesc : courseItem.courseShortDesc,
				courseLevel : courseItem.courseLevel,
				moduleDetail :{
					moduleNumber : avc.form.module,
					moduleId : moduleItem.moduleId,
					moduleName : moduleItem.moduleName,
					moduleItems : avc.videosOfModule
				},
				author : courseItem.author
			}


			avc.form = {};
			avc.form.course = data.courseId;
			avc.form.module = data.moduleDetail.moduleNumber;
			avc.form.video = {};
			avc.form.video.videoNumber = avc.videosOfModule.length+1;
			avc.shouldShowVideoForm = false;

			pageLoader.show();
			admin.addVideos(data)
				.then(function(){
					avc.submitted = true;
					avc.form.error = false;
				})
				.catch(function(){
					avc.submitted = true;
					avc.form.error = false;
				})
				.finally(function(){pageLoader.hide()});


			console.log(data);
		}

		function getVideosOfModule(moduleId){
			//Return Admin Service
			return admin.getModuleVideos({moduleId:moduleId});
		}

		function editVideoOfModule(videoId){
			var video = $filter('filter')(avc.videosOfModule, {videoId: videoId})[0];
			avc.form.video = video;
            avc.shouldShowVideoForm = true;
		}
		function deleteVideoOfModule(videoId){
			//Return Admin delete video Service
			var courseItem = $filter('filter')(avc.courselist, {courseId: avc.form.course})[0];
			var moduleItem = $filter('filter')(avc.modulelist, {moduleNumber: avc.form.module})[0];

			avc.videosOfModule = avc.videosOfModule.filter(function(data){
				if(data.videoId === videoId){
					return false;
				}
				return true;
			});

			var data = {
				courseId : avc.form.course,
				courseName : courseItem.courseName,
				moduleDetail :{
					moduleNumber : avc.form.module,
					moduleId : moduleItem.moduleId,
					moduleName : moduleItem.moduleName,
					moduleItems : avc.videosOfModule
				},
				author : courseItem.author
			}
			pageLoader.show();
			admin.addVideos(data)
				.then(function(d){console.log("Removed Video")})
				.catch(function(){console.error(err);})
				.finally(function(){pageLoader.hide();});
		}

		function compare(a,b){
			return a.videoNumber - b.videoNumber;
		}
    }
})();