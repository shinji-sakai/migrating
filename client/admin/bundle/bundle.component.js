require('./_bundle.scss');
angular.module(window.moduleName)
    .component('bundlePage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./bundle.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['$log','admin','$filter','$scope','$state'];
function Controller($log,admin,$filter,$scope,$state){

	var vm = this;
	vm.form = {};
	vm.form.includedCourses = [];
	vm.no_of_item_already_fetch = 0;
	vm.saveBundle = saveBundle;
	vm.editBundle = editBundle;
	vm.deleteBundle = deleteBundle;
	vm.resetForm  = resetForm;

	vm.$onInit = function(){
		getAllCoursesFromMaster();
	//	getAllBundle();
	}

	function getAllCoursesFromMaster() {
		admin.coursemaster()
			.then(function(d){
				vm.allCourses = d;
			})
			.catch(function(err){
				console.error(err);
			});
	}
  	vm.fetchBundles = function(){
			vm.isBundlesLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'bundle'
			})
			.then(function(res){
				$log.debug(res)
				vm.allBundle= res.data;
				vm.no_of_item_already_fetch = 5;
				vm.last_update_dt = vm.allBundle[vm.allBundle.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isBundlesLoading = false;
			})
		}
			vm.fetchMoreBundleks = function(){
			vm.isBundlesLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
					collection : 'bundle',
				  update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.allBundle, {bndl_id: v.bndl_id})[0];
						if(!item){
							vm.allCourses.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					vm.last_update_dt = vm.allBundle[vm.allBundle.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isBundlesLoading = false;
			})
		}

	function getAllBundle() {
		admin.getAllBundle()
			.then(function(res){
				vm.allBundle = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function saveBundle() {
			if(!vm.formIsInEditMode){
                vm.form.create_dt = new Date();
                vm.form.update_dt = new Date();
            }else{
                vm.form.update_dt = new Date();
            }
		admin.saveBundle(vm.form)
			.then(function(res){
			//	getAllBundle();
				vm.fetchBundles();
				resetForm();
				vm.formIsInEditMode=false;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function deleteBundle(id){
		admin.deleteBundle({bndl_id : id})
			.then(function(res){
				var item = $filter('filter')(vm.allBundle || [], {bndl_id: id})[0];								
					 if (vm.allBundle.indexOf(item) > -1) {
                        var pos = vm.allBundle.indexOf(item);
                       vm.allBundle.splice(pos, 1);
                    }
			//	getAllBundle();
				resetForm();
			})
			.catch(function(err){
				console.error(err);
			})	
	}

	function editBundle(id){
		var pl = $filter('filter')(vm.allBundle,{bndl_id:id})[0];
		vm.form = pl;
		vm.formIsInEditMode=true;
	}

	function resetForm(){
		vm.form = {};
	}
}