// (function() {
//     angular
//         .module(window.moduleName)
//         .controller('AdminModulesController', AdminModulesController);

// 	AdminModulesController.$inject=['courselist','admin','$filter','pageLoader'];

//     function AdminModulesController(courselist,admin,$filter,pageLoader) {

//     	var amc = this;

// 		amc.courselist = courselist;
// 		amc.modulelist = [1,2,3,4,5,6,7,8,9,10];
// 		amc.buttonName = "Save";
// 		amc.submitted = false;
// 		amc.deleteCourseModule=deleteCourseModule;
// 		amc.updateFormWithData = updateFormWithData;


// 		//updateData();


// 		amc.addCourseModule = function(){

// 			pageLoader.show();
// 			var courseModuleData= {
// 				courseId:amc.form.course,
// 				courseName:amc.form.courseName,
// 				moduleNumber:amc.form.moduleNumber,
// 				moduleId:amc.form.course+"_"+amc.form.moduleNumber,
// 				moduleName:amc.form.moduleName,
// 				moduleDesc:amc.form.moduleDesc
// 			};
// 			amc.buttonName = "Uploading...";
// 			console.log(courseModuleData);
// 			admin.addCourseModule(courseModuleData)
// 				.then(function success(data){

// 					updateData(amc.form.course);

// 					//amc.form = {};
// 					amc.submitted = true;
// 					amc.form.error = false;
// 					amc.buttonName = "Save";
// 				})
// 				.catch(function err(err){

// 					amc.form = {};
// 					amc.submitted = true;
// 					amc.form.error = true;
// 					amc.buttonName = "Save";

// 					console.error(err);

// 				})
// 				.finally(function(){
// 					pageLoader.hide();
// 				});

// 		}

// 		amc.changeCourse= function(){
// 			// amc.courseId = amc.form.course;
// 			if(amc.form.course){
// 				var item = $filter('filter')(amc.courselist, {courseId: amc.form.course})[0];
// 				amc.form.courseName = item.courseName;
// 				console.log(amc.form.course);

// 				updateData(amc.form.course);
// 			}else{
// 				//On Select Course Option
// 				amc.form = {};
// 				amc.moduleFullData = undefined;
// 			}

// 		}


// 		function deleteCourseModule(id,moduleNumber) {

// 			pageLoader.show();
// 			var courseModuleData= {
// 				courseId:id,
// 				moduleNumber:moduleNumber
// 			};


// 			admin.deleteCourseModule(courseModuleData)
// 				.then(function(res){
// 					pageLoader.hide();
// 					updateData(id);
// 				});

// 		}

// 		function updateFormWithData(id,moduleNumber){
// 			var item = $filter('filter')(amc.moduleFullData, {courseId: id,moduleNumber:moduleNumber})[0];
// 			amc.form.course = id;
// 			amc.form.courseName = item.courseName;
// 			amc.form.moduleNumber = item.moduleNumber;
// 			amc.form.moduleName = item.moduleName;
// 			amc.form.moduleDesc = item.moduleDesc;

// 		}

// 		function updateData(id){

// 			pageLoader.show();
// 			amc.moduleFullData = undefined;
// 			amc.form.moduleNumber = "";
// 			amc.form.moduleName = "";
// 			amc.form.moduleDesc = "";
// 			admin.getCourseModules({'courseId':id})
// 				.then(function(data){
// 					console.log(data);
// 					amc.moduleFullData = data;
// 					amc.form.moduleNumber = amc.moduleFullData.length + 1;
// 				})	
// 				.catch(function(){
// 					console.error("Error");
// 					amc.form.moduleNumber = 1;
// 				})
// 				.finally(function(){
// 					pageLoader.hide();
// 				})
// 		}

//     }
// })();