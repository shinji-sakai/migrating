	angular.module(window.moduleName)
        .component('modulesPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./modules.html'),
            controller: AdminModulesController,
            controllerAs: 'amc'
        });
	
	AdminModulesController.$inject=['admin','$filter','pageLoader','$scope','EditorConfig'];

    function AdminModulesController(admin,$filter,pageLoader,$scope,EditorConfig) {

    	var amc = this;
		amc.modulelist = [1,2,3,4,5,6,7,8,9,10];
		amc.buttonName = "Save";
		amc.submitted = false;
		amc.deleteCourseModule=deleteCourseModule;
		amc.updateFormWithData = updateFormWithData;
		amc.resetCourseModule=resetCourseModule;

		var editor = $("#moduledesc").froalaEditor(EditorConfig);

		amc.$onInit = function() {
        	$scope.$watch(function(){return amc.parent.coursemaster;},function(v){
        		// amc.courselist = v;	
        		amc.courselist = amc.courselist || [];
	        	if(v){
	        		amc.courselist = amc.courselist.concat(v);
	        		getAllTiming();
	        	}
        	});
	    };

	    function getAllTiming(){
			admin.getAllBatchTiming()
				.then(function(res){
					amc.allTimings = res.data;
					var batches = amc.allTimings.map(function(v,i) {
						var nm = v.bat_nm || (v.crs_id + "-" + v.bat_id);
						return {
							courseId : nm,
							courseName : v.crs_id + "-" + $filter("date")(v.cls_start_dt,"dd-MMM-yyyy")
						}
					})
					amc.courselist = amc.courselist || [];
					if(batches){
						amc.courselist = amc.courselist.concat(batches) 	
					}
					
				})
				.catch(function(err){
					console.error(err);
				})
		}

		amc.addCourseModule = function(){

			pageLoader.show();
			var courseModuleData= {
				courseId:amc.form.course,
				courseName:amc.form.courseName,
				moduleNumber:amc.form.moduleNumber,
				moduleId:amc.form.course+"-"+amc.form.moduleNumber,
				moduleName:amc.form.moduleName,
				moduleDesc:$('#moduledesc').froalaEditor('html.get', true),
				moduleWeight:parseFloat(amc.form.moduleWeight),
			};
			amc.buttonName = "Uploading...";
			console.log(courseModuleData);
			admin.addCourseModule(courseModuleData)
				.then(function success(data){

					updateData(amc.form.course);

					//amc.form = {};
					amc.submitted = true;
					amc.form.error = false;
					amc.buttonName = "Save";
				})
				.catch(function err(err){

					amc.form = {};
					amc.submitted = true;
					amc.form.error = true;
					amc.buttonName = "Save";

					console.error(err);

				})
				.finally(function(){
					pageLoader.hide();
				});
		}

		amc.changeCourse= function(){
			// amc.courseId = amc.form.course;
			if(amc.form.course){
				var item = $filter('filter')(amc.courselist, {courseId: amc.form.course})[0];
				amc.form.courseName = item.courseName;
				updateData(amc.form.course);
			}else{
				//On Select Course Option
				amc.form = {};
				amc.moduleFullData = undefined;
			}
		}


		function deleteCourseModule(id,moduleNumber) {
			var result = confirm("Want to delete?");
			if (!result) {
			    //Declined
			   	return;
			}
			pageLoader.show();
			var courseModuleData= {
				courseId:id,
				moduleNumber:moduleNumber
			};
			admin.deleteCourseModule(courseModuleData)
				.then(function(res){
					pageLoader.hide();
					updateData(id);
				});
		}

		function updateFormWithData(id,moduleNumber){
			var item = $filter('filter')(amc.moduleFullData, {courseId: id,moduleNumber:moduleNumber})[0];
			amc.form.course = id;
			amc.form.courseName = item.courseName;
			amc.form.moduleNumber = item.moduleNumber;
			amc.form.moduleName = item.moduleName;
			amc.form.moduleDesc = item.moduleDesc;
			$('#moduledesc').froalaEditor('html.set', item.moduleDesc)
			amc.form.moduleWeight = parseFloat(item.moduleWeight);
		}

		function updateData(id){

			pageLoader.show();
			amc.moduleFullData = undefined;
			amc.form.moduleNumber = "";
			amc.form.moduleName = "";
			amc.form.moduleDesc = "";
			admin.getCourseModules({'courseId':id})
				.then(function(data){
					amc.moduleFullData = data;
					amc.moduleFullData.sort(function(a,b){
						return a.moduleNumber - b.moduleNumber;
					});
					
					amc.form.moduleNumber = amc.moduleFullData[amc.moduleFullData.length - 1].moduleNumber + 1;
					amc.form.moduleWeight =  amc.form.moduleNumber;
				})	
				.catch(function(){
					console.error("Error");
					amc.form.moduleNumber = 1;
					amc.form.moduleWeight = 1;
				})
				.finally(function(){
					pageLoader.hide();
				})
		}

		function resetCourseModule(){
			var course = amc.form.course;
			amc.form = {};
			amc.form.course = course;
			amc.form.moduleNumber = amc.moduleFullData[amc.moduleFullData.length - 1].moduleNumber + 1;
			amc.form.moduleWeight =  amc.form.moduleNumber;
		}

    }