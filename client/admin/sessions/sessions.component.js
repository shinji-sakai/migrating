angular.module(window.moduleName)
    .component('sessionsPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./sessions.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
function Controller(admin,$filter,$scope,$state,EditorConfig){

	var vm = this;
	vm.form = {};

	vm.changeCourse = changeCourse;
	vm.resetForm = resetForm;
	vm.addSession = addSession;
	vm.editSession = editSession;
	vm.deleteSession =  deleteSession;

	vm.$onInit = function(){
		getAllCoursesFromMaster();
	}

	function getAllCoursesFromMaster() {
		admin.coursemaster()
			.then(function(d){
				vm.coursesMaster = d;
			})
			.catch(function(err){
				console.error(err);
			});
	}

	function getSessionsForCourse(){
		admin.getSessionsByCourseId({crs_id : vm.form.crs_id})
			.then(function(res){
				var  sessionArr  = res.data;
				if(sessionArr.length <= 0){
					vm.form.sessionNumber = 1;
				}else{
					sessionArr.sort(function(a,b){
						return a.sessionNumber - b.sessionNumber;
					})
					vm.form.sessionNumber = sessionArr[sessionArr.length - 1].sessionNumber + 1;
				}
				vm.allSessions = sessionArr;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function addSession(){
		vm.form.sessionWeight = parseFloat(vm.form.sessionWeight);
		admin.addSession(vm.form)
			.then(function(res){
				getSessionsForCourse();
				resetForm();
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function editSession(sessionNumber){
		var session = $filter('filter')(vm.allSessions,{sessionNumber : sessionNumber})[0];
		vm.form = session;
	}

	function deleteSession(sessionNumber){
		admin.deleteSession({sessionNumber : sessionNumber,crs_id:vm.form.crs_id})
			.then(function(res){
				getSessionsForCourse();
				resetForm();
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function changeCourse(){
		getSessionsForCourse();
	}

	function resetForm(){
		var crs = vm.form.crs_id;
		vm.form = {};
		vm.form.crs_id = crs;
		vm.form.sessionNumber = vm.allSessions[vm.allSessions.length - 1].sessionNumber + 1;
	}

}