require('service.profile');
require('authToken');
angular.module(window.moduleName)
    .component('purchasedCourses', {
        require: {
            parent : '^adminPage'
        },
        template: require('./purchasedCourses.html'),
        controller: AdminPurchasedCoursesController,
        controllerAs: 'vm'
    })


AdminPurchasedCoursesController.$inject = ['$log','admin','$filter','$scope','$state','alertify','$timeout', 'profile', 'authToken'];
function AdminPurchasedCoursesController($log,admin,$filter,$scope,$state,alertify,$timeout, profile, authToken) {
    var vm = this;
    vm.selectedUsers = [];
    vm.onUserChange = onUserChange;
    vm.addPurchasedCourses = addPurchasedCourses;
    vm.addPurchasedBundle = addPurchasedBundle;
    vm.addPurchasedTraining = addPurchasedTraining;
    vm.updateFormWithValues = updateFormWithValues;
    vm.course = []
    vm.userID = "";
    vm.resetFrom = resetFrom;

    vm.$onInit = function() {
        getAllUsers();
        getAllCoursesFromMaster();
        getAllBundle();
        $scope.$watch(function(){return vm.parent.coursemaster;},function(v){
            vm.courselist = v;
        });
        vm.userID= authToken.getUserId();
    }
    function onUserChange(){
        vm.form = vm.form || {};
        vm.form.usr_id = vm.selectedUsers[0] || "";
    }

    function getAllUsers(){
        profile.getAllUsersShortDetails()
            .then(function(d){
                vm.allUsers = d.data;
            })
            .catch(function(err){
                console.log(err);
            })
    }

    function getAllCoursesFromMaster() {
        admin.coursemaster()
            .then(function(d){
                vm.coursesMaster = d;
                vm.coursesMaster = vm.coursesMaster.filter(function(v,i){
                    return v.isLiveClass
                })
            })
            .catch(function(err){
                console.error(err);
            });
    }

    function getAllBundle() {
        admin.getAllBundle()
            .then(function(res){
                vm.allBundle = res.data;
            })
            .catch(function(err){
                console.error(err);
            })
    }


    vm.changeCourse = function() {
        vm.updateFormWithValues(vm.course);
    }
    function updateFormWithValues(id) {

        vm.courseId = id;
        vm.course = id;
        var item = $filter('filter')(vm.courselistdata, {
            courseId: vm.course
        })[0];
        var coursemaster = $filter('filter')(vm.courselist, {
            courseId: vm.course
        })[0];
    }

    //Save Purchased Course Based on COurse ids
    var data = {}
    function addPurchasedCourses() {
        data = {}
        data["usr_id"] = vm.form.usr_id;
        data["crs_id"] = [vm.course]
        admin.purchasedCourse(data)
            .then(function(res) {
                if(res.statusText == "OK"){
                    console.log(res)
                    alertify.success(res.data.message)
                    vm.course = [];
                    vm.resetFrom();
                }
                else {alertify.error("Unable to Updated");vm.resetFrom();}
            });
    }
    //Save purchased courses based on bundle ids
    function addPurchasedBundle() {
        data = {}
        data["usr_id"] = vm.form.usr_id;
        data["bundles"] = vm.form.includedBundles;
        admin.purchasedBundle(data)
            .then(function(res) {
                if(res.statusText == "OK"){
                    alertify.success(res.data.message)
                    vm.form.includedBundles= [];
                    vm.resetFrom()
                }
                else {alertify.error("Unable to Updated");vm.resetFrom();}
            });
    }
    //save purchased courses based on training ids
    function addPurchasedTraining() {
        data = {}
        data["usr_id"] = vm.form.usr_id;
        data["trainings"] = vm.form.crs_id;
        admin.purchasedTraining(data)
            .then(function(res) {
                if(res.statusText == "OK"){
                    alertify.success(res.data.message);
                    vm.form.crs_id = []
                    vm.resetFrom();
                }
                else {alertify.error("Unable to Updated");vm.resetFrom();}
            });
    }

    function resetFrom() {
        vm.selectedUsers = []
    }
}