require('./_gettingStarted.scss');
require('angular-froala');
angular.module(window.moduleName)
    .component('gettingStartedPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./gettingStarted.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
function Controller(admin,$filter,$scope,$state,EditorConfig){

	var vm = this;
	vm.EditorConfig = EditorConfig;

	vm.addPointToList = addPointToList;
	vm.removePointFromList = removePointFromList;
	vm.addGettingStarted = addGettingStarted;
	vm.deleteGettingStarted = deleteGettingStarted;
	vm.editGettingStarted = editGettingStarted;
	vm.onTrainingChange = onTrainingChange;
	vm.resetForm = resetForm;

	vm.$onInit = function(){
		getAllCoursesFromMaster();
		getAllGettingStarted();

		setTimeout(function(){
			// $('#desc').froalaEditor(EditorConfig);
		})
	}

	function getAllCoursesFromMaster() {
		admin.coursemaster()
			.then(function(d){
				vm.coursesMaster = d;
			})
			.catch(function(err){
				console.error(err);
			});
	}

	function getAllGettingStarted() {
		admin.getAllGettingStarted()
			.then(function(res){
				vm.allGettingStarted = res.data;
			})
			.catch(function(err){
				console.error(err);
			});
	}

	function onTrainingChange() {
		if(vm.form.crs_id){
			var item = $filter("filter")(vm.allGettingStarted,{crs_id : vm.form.crs_id})[0];
			if(item){
				vm.form = item;
				// $('#desc').froalaEditor('html.set',item.desc);
			}
		}
	}

	function addGettingStarted() {
		admin.addGettingStarted(vm.form)
			.then(function(res){
				getAllGettingStarted();
				resetForm();
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function deleteGettingStarted(id) {
		admin.deleteGettingStarted({crs_id : id})
			.then(function(res){
				getAllGettingStarted();
				resetForm();
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function editGettingStarted(id) {
		var gs = $filter('filter')(vm.allGettingStarted,{crs_id : id})[0];
		vm.form = gs;
	}

	function resetForm(){
		vm.form = {};
		// $('#desc').froalaEditor('html.set','');
		vm.title = '';
	}


	function addPointToList(){
		vm.form = vm.form || {};
		vm.form.gettingStarted = vm.form.gettingStarted || [];
		if(vm.title){
			vm.form.gettingStarted.push({
				title : vm.title,
				desc : $('#desc').froalaEditor('html.get',true)
			})	
			vm.title = "";
			$('#desc').froalaEditor('html.set','');
		}
	}

	function removePointFromList(index){
		vm.form.gettingStarted.splice(index,1);
	}
}