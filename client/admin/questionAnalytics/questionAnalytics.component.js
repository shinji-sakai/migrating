require('service.admin');
angular.module(window.moduleName)
    .component('questionAnalyticsPage', {
        require: {
            parent : '^adminPage'
        },
        template: ['$element', '$attrs',
            function($element, $attrs) {
                return require('./questionAnalytics.html');
            }
        ],
        controller: QuestionAnalyticsController,
        controllerAs: 'vm'
    })

QuestionAnalyticsController.$inject = ['admin'];

function QuestionAnalyticsController(admin){

    var vm = this;




    vm.selectedSubjects = [];
    vm.selectedAuthors = [];
    vm.selectedPublications = [];
    vm.selectedBooks = [];
    vm.selectedExams = [];
    vm.selectedTopics = [];
    vm.selectedTags = [];

    vm.onFilterChange = onFilterChange;


    vm.$onInit = function(){
            vm.parent.hideSidebar = true;
            admin.getAllSubject()
                .then(function(d){
                    vm.subjects = d.data;
                });
            admin.getAllAuthor()
                .then(function(d){
                    vm.authors = d.data;
                });
            admin.getAllPublication()
                .then(function(d){
                    vm.publications = d.data;
                });
            admin.getAllBook()
                .then(function(d){
                    vm.books = d.data;
                });
            admin.getAllExam()
                .then(function(d){
                    vm.exams = d.data;
                });
            admin.getAllTopic()
                .then(function(d){
                    vm.topics = d.data;
                });
            admin.getAllTag()
            .then(function(d){
                vm.tags = d.data;
            });
            // updateQuestionList();
        }

        function onFilterChange(item,model) {
            if(vm.selectedSubjects.length > 0){
                admin.getAllTopic({subjectId : vm.selectedSubjects[0]})
                .then(function(d){
                    vm.topics = d.data;
                }); 
            }
            var query = {
                subjects    : vm.selectedSubjects,
                authors : vm.selectedAuthors,
                publications    : vm.selectedPublications,
                books   : vm.selectedBooks,
                exams   : vm.selectedExams,
                topics  : vm.selectedTopics,
                tags    : vm.selectedTags
            }
            vm.isLoading = true;
            admin.findPracticeQuestionsByQuery(query)
            .then(function(d){
                if(d.data.length > 0){
                    vm.questions = d.data;
                    vm.questions.sort(function(a,b){
                        return a.questionNumber - b.questionNumber;
                    });
                }
                vm.isLoading = false;
            })
            .catch(function(err){
                console.log(err);
            });
        }
}



