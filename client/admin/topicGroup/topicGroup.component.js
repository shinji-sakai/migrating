require('./_topicGroup.scss')
angular.module(window.moduleName)
    .component('topicGroupPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./topicGroup.html'),
        controller: AdminTopicGroupController,
        controllerAs: 'vm'
    });

AdminTopicGroupController.$inject = ['$log','admin', 'pageLoader', '$filter', '$scope'];

function AdminTopicGroupController($log,admin, pageLoader, $filter, $scope) {
    var vm = this;

    vm.topics = [];
    vm.groups = [];
    vm.selectedSubject = [];
    vm.selectedTopic = [];
    vm.no_of_item_already_fetch = 0;
    vm.updateGroup = updateGroup;
    vm.resetFormDetail = resetFormDetail;
    vm.removeGroup = removeGroup;
    vm.editGroup = editGroup;
    vm.changeSubject = changeSubject;
    vm.form = vm.form || {};


    vm.$onInit = function() {
        admin.getAllSubject()
            .then(function(d) {
                vm.subjects = d.data;
            });
        $scope.$watch(function(){
            return vm.form.groupNumber;
        }, function(newValue, oldValue, scope) {
            if(newValue && vm.selectedTopic[0]){
                vm.form = vm.form || {};
                vm.form.groupId = vm.selectedTopic[0] + "-" + vm.form.groupNumber
            }
        });
        // updateTopicsList();
    };

    function updateTopicsList() {
        pageLoader.show();
        admin.getAllTopic({
                subjectId: vm.selectedSubject[0]
            })
            .then(function(data) {
                if (data.data.length > 0) {
                    vm.topics = data.data;
                    vm.topics.sort(function(a, b) {
                        return a.topicNumber - b.topicNumber;
                    });
                } else {
                    vm.topics = [];
                }
            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function updateGroups() {
        if(!vm.selectedTopic[0]){
            vm.groups = [];
            return;
        }
        pageLoader.show();
        admin.getAllTopicGroups({
                topicId : vm.selectedTopic[0]
            })
            .then(function(res) {
                if (res.data.length > 0) {
                    vm.groups = res.data;
                    vm.groups.sort(function(a, b) {
                        return a.topicNumber - b.topicNumber;
                    });
                    vm.form = vm.form = {};
                    vm.form.groupNumber = vm.groups[vm.groups.length - 1]["groupNumber"] + 1;
                } else {
                    vm.groups = [];
                    vm.form = vm.form = {};
                    vm.form.groupNumber = 1;
                }
            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function updateGroup() {
        var data = {
            groupId: vm.form.groupId,
            groupNumber : vm.form.groupNumber,
            groupName: vm.form.groupName,
            groupDesc: vm.form.groupDesc,
            subjectId : vm.selectedSubject[0],
            topicId : vm.selectedTopic[0]
        }
        if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }
        pageLoader.show();
        admin.updateTopicGroup(data)
            .then(function(res) {
                // var item = $filter('filter')(vm.groups || [], {groupId: vm.form.groupId})[0];
				// 	if(item){
				// 		    item.groupId= vm.form.groupId || vm.selectedTopic[0] + "-" + vm.form.groupNumber,
                //             item.groupNumber = vm.form.groupNumber,
                //             item.groupName= vm.form.groupName,
                //             item.groupDesc= vm.form.groupDesc,
                //             item.subjectId = vm.selectedSubject[0],
                //             item.topicId = vm.selectedTopic[0]
                //     }else{
				// 		vm.groups = [data].concat(vm.groups || []);
				// 	}
                vm.fetchTopicGrops();
                vm.submitted = true;
                vm.form = {};
                 vm.formIsInEditMode = false;
                //updateGroups();
            })
            .catch(function() {
                vm.submitted = true;
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function resetFormDetail() {
        vm.submitted = false;
        vm.form = {};
        if (vm.groups.length > 0) {
            vm.form.groupNumber = vm.groups[vm.groups.length - 1]['groupNumber'] + 1;
        }
    }

    function removeGroup(id) {
        var result = confirm("Want to delete?");
        if (!result) {
            //Declined
            return;
        }
        pageLoader.show();
        admin.removeTopicGroup({
                groupId: id
            })
            .then(function(d) {
                var item = $filter('filter')(vm.groups || [], {groupId: id})[0];								
					 if (vm.groups.indexOf(item) > -1) {
                        var pos = vm.groups.indexOf(item);
                       vm.groups.splice(pos, 1);
                    }
                //updateGroups();
            })
            .catch(function(err) {
                console.log(err);
            })
            .finally(function() {
                pageLoader.hide();
            })
    }

    function editGroup(id) {
        var obj = $filter('filter')(vm.groups, {
            groupId: id
        })[0];
        vm.form = obj;
        vm.selectedSubject = [vm.form.subjectId];
        vm.selectedTopic = [vm.form.topicId];
         vm.formIsInEditMode = true;
    }

    function changeSubject() {
        updateTopicsList();
    }

    vm.changeTopic = function(){
        updateGroups();
    }
    vm.fetchTopicGrops = function(){
			vm.isTopicsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'topicGroup'
			})
			.then(function(res){
				$log.debug(res)
				vm.groups = res.data;
				vm.no_of_item_already_fetch = 5;
                vm.last_update_dt = vm.groups[vm.groups.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTopicsLoading = false;
			})
		}
        
		vm.fetchMoreTopicGroups = function(){
			vm.isTopicsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				 collection : 'topicGroup',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.groups , {groupId: v.groupId})[0];
						if(!item){
						 vm.groups .push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					vm.last_update_dt = vm.groups[vm.groups.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isTopicsLoading = false;
			})
		}
}