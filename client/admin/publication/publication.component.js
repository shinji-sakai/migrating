	angular.module(window.moduleName)
        .component('publicationPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./publication.html'),
            controller: AdminPublicationController,
            controllerAs: 'vm'
        });

	AdminPublicationController.$inject=['$log','admin','pageLoader','$filter','$scope'];

	function AdminPublicationController($log,admin,pageLoader,$filter,$scope) {
    	var vm = this;
        vm.publications = [];
		vm.no_of_item_already_fetch = 0;
        vm.checkValidityOfID = checkValidityOfID;
    	vm.updatePublication = updatePublication;
    	vm.resetFormDetail = resetFormDetail;
    	vm.removePublication = removePublication;
    	vm.editPublication = editPublication;
    	vm.$onInit = function() {
        //	updatePublicationsList();
	    };

        function checkValidityOfID() {
			vm.form.publicationId = vm.form.publicationId || "";
            vm.form.publicationId = vm.form.publicationId.replace(/\s/g , "-");
				admin.isIdExist({
				collection :'publication',
				key:"publicationId",
				id_value:vm.form.publicationId			
			}).then(function(data)
			{				
              console.log(data.data);
			  if(data.data.idExist)
			  {
				    vm.idExists = true;
			  }
			  else
			  {
				   vm.idExists = false;
			  }
			}).catch(function(err){
             console.log(err);
			})
				
           
        }

		vm.fetchPublications = function(){
			vm.isPublicationsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
				collection : 'publication'
			})
			.then(function(res){
				$log.debug(res)
					vm.publications = res.data;
				vm.no_of_item_already_fetch = 5;
				vm.last_update_dt = vm.publications[vm.publications.length - 1].update_dt;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isPublicationsLoading = false;
			})
		}

		vm.fetchMorePublications = function(){
			vm.isPublicationsLoading = true;
			admin.fetchItemUsingLimit({
				no_of_item_to_fetch : 5,
			  collection : 'publication',
                update_dt : vm.last_update_dt
			})
			.then(function(res){
				if(res.data.length > 1){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.publications, {publicationId: v.publicationId})[0];
						if(!item){
							vm.publications.push(v)		
							vm.no_of_item_already_fetch++;
						}
					})
					  vm.last_update_dt = vm.publications[vm.publications.length - 1].update_dt;
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isPublicationsLoading = false;
			})
		}
	    function updatePublicationsList(){
	    	pageLoader.show();
	    	admin.getAllPublication()
        		.then(function(data){
        			if(data.data.length > 0)
        				vm.publications = data.data;
        		})
        		.catch(function(err){
        			console.log(err);
        		})
        		.finally(function(){
        			pageLoader.hide();
        		})
	    }
    	function updatePublication(){
    		var data = {
    			publicationId : vm.form.publicationId,
    			publicationName : vm.form.publicationName,
    			publicationDesc : vm.form.publicationDesc 
    		}
			    if(!vm.formIsInEditMode){
                data.create_dt = new Date();
                data.update_dt = new Date();
            }else{
                data.update_dt = new Date();
            }

    		pageLoader.show();
    		admin.updatePublication(data)
    			.then(function(res){
					// var item = $filter('filter')(vm.publications || [], {publicationId: vm.form.publicationId})[0];
					// if(item){
					// 		item.publicationId = vm.form.publicationId,
					// 		item.publicationName = vm.form.publicationName,
					// 		item.publicationDesc = vm.form.publicationDesc 					
					// }else{
					// 	vm.publications = [data].concat(vm.publications || [])
					// }
					vm.fetchPublications();
    				vm.submitted = true;
    				vm.form.error = false;
    				vm.form.publicationName = "";
    				vm.form.publicationDesc = "";
                    vm.form.publicationId ="";
                    vm.formIsInEditMode = false;
    				//updatePublicationsList();
    			})
    			.catch(function(){
    				vm.submitted = true;
    				vm.form.error = true;	
    			})
    			.finally(function(){
    				pageLoader.hide();
    			})
    	}
    	function resetFormDetail(){
    		vm.submitted = false;
            vm.idExists = false;
            vm.formIsInEditMode = false;
    		vm.form = {};
    	}
    	function removePublication(id){
            var result = confirm("Want to delete?");
            if (!result) {
                //Declined
                return;
            }
    		pageLoader.show();
    		admin.removePublication({publicationId:id})
    			.then(function(d){
    				console.log('publicationId'+d);
						var item = $filter('filter')(vm.publications || [], {publicationId: id})[0];	
							console.log('publicationId'+d);							
					 if (vm.publications.indexOf(item) > -1) {
                        var pos = vm.publications.indexOf(item);
                       vm.publications.splice(pos, 1);
                    }
    				//updatePublicationsList();
    			})
    			.catch(function(err){
    				console.log(err);
    			})
    			.finally(function(){pageLoader.hide();})
    	}
    	function editPublication(id){
    		var publication = $filter('filter')(vm.publications,{publicationId:id})[0];
    		vm.form = {};
    		vm.form.publicationId = publication.publicationId;
    		vm.form.publicationName = publication.publicationName;
    		vm.form.publicationDesc = publication.publicationDesc;
            vm.formIsInEditMode = true;
    	}
    }