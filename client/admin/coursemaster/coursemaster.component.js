	angular.module(window.moduleName)
        .component('courseMasterPage', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./coursemaster.html'),
            controller: AdminCourseMasterController,
            controllerAs: 'vm'
        })

	AdminCourseMasterController.$inject = ['$log','admin','$filter','$scope','$state','alertify','$timeout'];
	function AdminCourseMasterController($log,admin,$filter,$scope,$state,alertify,$timeout){

		var vm  = this;
		vm.no_of_item_already_fetch = 0;

		vm.form = {};
		vm.form.noOfauthors = 1;
		vm.form.courseSubGroup = "informatica";
		vm.form.courseType = "it"
		vm.selectedAuthors = [];
		vm.form.author = [];
		vm.submitted = false;
		vm.buttonName = "Save";
		vm.selectedRelatedCourses = [];
		vm.courseTypes = [{
			id : 'it',
			name : 'IT'
		},{
			id : 'board',
			name : 'Board'
		},{
			id : 'competitive',
			name : 'Competitive'
		}]
		
		vm.$onInit = function(){
			$scope.$watch(function(){return vm.parent.coursemaster;},function(v){
        		vm.relatedCourses = v;
        	});
        	getAllSubgroup();
        	// admin.getAllAuthor()
        	// 	.then(function(d){
        	// 		vm.coursemasterdata = d.data;
        	// 	});
        	$timeout(function(){
				angular.element('#form')[0].addEventListener('keypress', function(event) {
			        if (event.keyCode == 13) {
			            event.preventDefault();
			        }
			    });	
			})
		}

		vm.getNumber = getNumber;
		vm.saveCourseMaster=saveCourseMaster;
		vm.updateFormWithValues = updateFormWithValues;
		vm.deleteCourseMaster = deleteCourseMaster;
		vm.resetForm = resetForm;
		vm.checkIdExist = checkIdExist;

		function getNumber(num) {
		    return Array.apply(null,{length:num});   
		}

		function checkIdExist() {
			if(!vm.form.courseId){
				return;
			}
			return admin.isIdExist({ key : "courseId" , id_value : vm.form.courseId , "collection" : "coursemaster" })
				.then(function(res){
					if(res.data && res.data.status === 'success' && res.data.idExist){
						vm.form.idExistError = true;
						var alreadyExistCourseData = res.data.data
						var item = $filter('filter')(vm.coursemasterdata || [], {courseId: vm.form.courseId})[0];
						if(item){
							var pos = vm.coursemasterdata.indexOf(item);
							if(pos > -1){
								vm.coursemasterdata.splice(pos,1);
							}
						}
						vm.coursemasterdata = [alreadyExistCourseData].concat(vm.coursemasterdata || [])
					}else{
						vm.form.idExistError = false;
					}
				})
				.catch(function(err){
					console.error(err)
				})
		}

		function getAllSubgroup(){
			admin.getAllSubgroup()
				.then(function(res){
					vm.allSubgroup = res.data
				})
		}

		function deleteCourseMaster(id) {
			var result = confirm("Want to delete?");
			if (!result) {
			    //Declined
			   	return;
			}
			var courseMasterData= {
				courseId:id,
			};


			admin.deleteCourseMaster(courseMasterData)
				.then(function(res){
					// updateData();
					var item = $filter('filter')(vm.coursemasterdata || [], {courseId: id})[0];

					if(item){
						var pos = vm.coursemasterdata.indexOf(item);
						if(pos > -1){
							vm.coursemasterdata.splice(pos,1);
						}
					}
					alertify.success("Successfully Removed")
				})
				.catch(function(err){
					alertify.error("Error in removing")
				})

		}


		function saveCourseMaster() {
			var courseMasterData= {
				courseId:vm.form.courseId,
				author : vm.selectedAuthors,
				courseName:vm.form.courseName,
				courseType : vm.form.courseType,
				courseSubGroup : vm.form.courseSubGroup,
				relatedCourses : vm.selectedRelatedCourses,
				isLiveClass : vm.form.isLiveClass
			};
			vm.buttonName = "Uploading....";
			admin.addCourseMaster(courseMasterData)
				.then(function success(data){
					vm.buttonName = "Save";
					
					// updateData();
					var item = $filter('filter')(vm.coursemasterdata || [], {courseId: vm.form.courseId})[0];
					if(item){
						item.courseId = vm.form.courseId;
						item.author = vm.selectedAuthors;
						item.courseName = vm.form.courseName;
						item.courseType = vm.form.courseType;
						item.courseSubGroup = vm.form.courseSubGroup;
						item.relatedCourses = vm.selectedRelatedCourses;
						item.isLiveClass = vm.form.isLiveClass;
					}else{
						vm.coursemasterdata = [courseMasterData].concat(vm.coursemasterdata || [])
					}

					vm.form = {};
					vm.form.noOfauthors = 1;
					vm.form.author = [];
					vm.submitted = true;
					vm.form.error = false;
					vm.selectedAuthors = [];

					// vm.form.courseType = courseMasterData.courseType || ;
					vm.selectedRelatedCourses = [];
					
					$log.info(vm.coursemasterdata)

					// $state.reload();
					alertify.success("Successfully Saved")
				})
				.catch(function error(err){
					vm.buttonName = "Save";
					vm.form = {};
					vm.submitted = true;
					vm.form.error = true;
					vm.form.noOfauthors = 1;
					vm.form.author = [];
					alertify.error("Error in saving")
				});
		}

		function updateFormWithValues(id){
			var item = $filter('filter')(vm.coursemasterdata, {courseId: id})[0];
			if(item){

				$("#courseid").attr('readonly','true').attr('disabled','true');
				vm.form.courseId = item.courseId;
				vm.form.courseName = item.courseName;
				vm.form.courseType = item.courseType;
				vm.form.courseSubGroup = item.courseSubGroup;
				vm.form.isLiveClass = item.isLiveClass;
				// vm.form.noOfauthors = item.author.length;
				vm.selectedAuthors = item.author;
				vm.selectedRelatedCourses = item.relatedCourses;
				vm.form.idExistError = false;
			}
		}

		function  updateData(){
			admin.coursemaster()
				.then(function(data){
					vm.coursemasterdata = data;
				})
				.catch(function err(err){
					vm.form.error = true;
				})
		}
		function resetForm(){
			$("#courseid").removeAttr('readonly').removeAttr('disabled');
			vm.form = {};
			vm.selectedAuthors = [];
		}

		vm.fetchCourses = function(){
			vm.isCoursesLoading = true;
			admin.fetchCourseMasterUsingLimit({
				no_of_item_to_fetch : 5,
				no_of_item_to_skip : 0
			})
			.then(function(res){
				$log.debug(res)
				vm.coursemasterdata = res.data;
				vm.no_of_item_already_fetch = 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isCoursesLoading = false;
			})
		}

		vm.fetchMoreCourses = function(){
			vm.isCoursesLoading = true;
			admin.fetchCourseMasterUsingLimit({
				no_of_item_to_fetch : 5,
				no_of_item_to_skip : vm.no_of_item_already_fetch
			})
			.then(function(res){
				if(res.data.length > 0){
					res.data.forEach(function(v,i){
						var item = $filter('filter')(vm.coursemasterdata, {courseId: v.courseId})[0];
						if(!item){
							vm.coursemasterdata.push(v)		
							// vm.no_of_item_already_fetch++;
						}
					})
					vm.no_of_item_already_fetch = vm.coursemasterdata.length
					// vm.coursemasterdata = vm.coursemasterdata.concat(res.data)	
				}else{
					vm.noMoreData = true;
				}
				// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
			})
			.catch(function(err){
				$log.error(err)
			})
			.finally(function(){
				vm.isCoursesLoading = false;
			})
		}

	}