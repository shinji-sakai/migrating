require('tagsinput_js');
require('tagsinput_css');
require('service.aws');
require('service.uploadFileAPI');
require('angular-froala');
require('angular-textbox-io');

angular.module(window.moduleName)
    .component('bookTopicPage', {
        require: {
            parent: '^adminPage'
        },
        template: require('./bookTopic.html'),
        controller: AdminVideoEntityController,
        controllerAs: 'vm'
    });

AdminVideoEntityController.$inject = ['$log', 'admin', 'pageLoader', '$filter', '$scope', 'EditorConfig', 'awsService', 'uploadFileAPI'];

function AdminVideoEntityController($log, admin, pageLoader, $filter, $scope, EditorConfig, awsService, uploadFileAPI) {

    var vm = this;
    vm.EditorConfig = EditorConfig;
    vm.selectedSubjects = [];
    vm.selectedAuthors = [];
    vm.selectedPublications = [];
    vm.selectedBooks = [];
    vm.selectedExams = [];
    vm.selectedTopics = [];
    vm.selectedRelatedTopics = [];
    vm.selectedRelatedVideos = [];
    // vm.selectedRelatedCourses = [];
    vm.selectedTags = [];
    vm.noOfRefLinks = 1;
    vm.form = {};
    vm.form.video = {};
    vm.form.video.thumbUrl = ""; //"https://examwarrior.com/images/no-thumb.png";
    vm.form.video.videoSource = "harddrive";
    vm.form.video.refLinks = [];
    vm.videosOfModule = [];
    vm.shouldShowVideoForm = false;
    vm.isVideoUploadButtonDisabled = true;


    
    vm.$onInit = function() {
        $scope.$watch(function() {
            return vm.parent.courselistdata;
        }, function(v) {
            vm.courselist = v;
        });
        updateBookTopics();
        findAllBookTopics();
        admin.getAllSubject()
            .then(function(d) {
                vm.subjects = d.data;
            });
        admin.getAllAuthor()
            .then(function(d) {
                vm.authors = d.data;
            });
        admin.getAllPublication()
            .then(function(d) {
                vm.publications = d.data;
            });
        admin.getAllBook()
            .then(function(d) {
                vm.books = d.data;
            });
        admin.getAllExam()
            .then(function(d) {
                vm.exams = d.data;
            });
        admin.getAllTopic()
            .then(function(d) {
                vm.topics = d.data;
            });
        admin.getAllTag()
            .then(function(d) {
                vm.tags = d.data;
            });
    };


    vm.updateBookTopic = updateBookTopic;
    vm.removeBookTopic = removeBookTopic;
    vm.editBookTopic = editBookTopic;
    vm.resetFormDetail = resetFormDetail;
    vm.updateBookTopics = updateBookTopics;

    function resetFormDetail() {
        vm.selectedAuthors = [];
        vm.selectedPublications = [];
        vm.selectedBooks = [];
        vm.selectedExams = [];
        vm.selectedTopics = [];
        vm.selectedRelatedTopics = [];
        vm.selectedRelatedVideos = [];
        vm.selectedTags = [];
        vm.form = {};
        vm.form.bookTopicContent = "<p><br/></p>";
    }

    function findAllBookTopics() {
        admin.findBookTopicsByQuery({})
            .then(function(data) {
                if (data.data.length > 0) {
                    vm.relatedbookTopics = data.data;
                    vm.relatedbookTopics.sort(compare);
                    vm.relatedbookTopicNumber = vm.relatedbookTopics[vm.relatedbookTopics.length - 1]['bookTopicNumber'] + 1;
                } else {
                    vm.relatedbookTopicNumber = 1;
                    vm.relatedbookTopics = undefined;
                }
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function updateBookTopics() {
        if (vm.selectedSubjects.length > 0) {
            admin.getAllTopic({
                    subjectId: vm.selectedSubjects[0]
                })
                .then(function(d) {
                    vm.topics = d.data;
                });
        }
        if (vm.selectedTopics.length > 0) {
            var topic = $filter('filter')(vm.topics, {
                topicId: vm.selectedTopics[0]
            })[0];
            vm.form = vm.form || {};
            vm.form.bookTopicName = topic.topicName;
        } else {
            vm.form = vm.form || {};
            vm.form.bookTopicName = "";
        }

        var query = {
            subjects: vm.selectedSubjects,
            authors: vm.selectedAuthors,
            publications: vm.selectedPublications,
            books: vm.selectedBooks,
            exams: vm.selectedExams,
            topics: vm.selectedTopics,
            tags: vm.selectedTags
        }
        admin.findBookTopicsByQuery(query)
            .then(function(data) {
                if (data.data.length > 0) {
                    vm.bookTopics = data.data;
                    vm.bookTopics.sort(compare);
                    vm.bookTopicNumber = vm.bookTopics[vm.bookTopics.length - 1]['bookTopicNumber'] + 1;
                } else {
                    vm.bookTopicNumber = 1;
                    vm.bookTopics = undefined;
                }
            })
            .catch(function(err) {
                console.error(err);
            })
    }


    function updateBookTopic() {
        //Add Video Id
        vm.form.bookTopicId = (vm.selectedTopics[0] || '') + "-" + vm.bookTopicNumber;
        
        var data = {
                bookTopicId: vm.form.bookTopicId,
                bookTopicNumber: vm.bookTopicNumber,
                bookTopicName: vm.form.bookTopicName,
                bookTopicContent: vm.form.bookTopicContent,
                subjects: vm.selectedSubjects,
                authors: vm.selectedAuthors,
                publications: vm.selectedPublications,
                books: vm.selectedBooks,
                exams: vm.selectedExams,
                topics: vm.selectedTopics,
                tags: vm.selectedTags
            }
            // console.log(data);
        pageLoader.show();
        admin.updateBookTopic(data)
            .then(function() {
                vm.resetFormDetail();
                vm.submitted = true;
                vm.form.error = false;
                updateBookTopics();
            })
            .catch(function() {
                vm.submitted = true;
                vm.form.error = false;
            })
            .finally(function() {
                vm.form = {};
                pageLoader.hide();
            });
    }


    function editBookTopic(bookTopicId) {
        var bookTopic = $filter('filter')(vm.bookTopics, {
            bookTopicId: bookTopicId
        })[0];
        // console.log(video);
        vm.form = bookTopic;

        vm.bookTopicNumber = bookTopic.bookTopicNumber;

        vm.selectedSubjects = bookTopic.subjects;
        vm.selectedAuthors = bookTopic.authors;
        vm.selectedPublications = bookTopic.publications;
        vm.selectedBooks = bookTopic.books;
        vm.selectedExams = bookTopic.exams;
        vm.selectedTopics = bookTopic.topics;
        vm.selectedTags = bookTopic.tags;
    }

    function removeBookTopic(bookTopicId) {
        var result = confirm("Want to delete?");
        if (!result) {
            //Declined
            return;
        }
        pageLoader.show();
        admin.removeBookTopic({
                bookTopicId: bookTopicId
            })
            .then(function(d) {
                updateBookTopics();
            })
            .catch(function() {
                console.error(err);
            })
            .finally(function() {
                pageLoader.hide();
            });
    }

    function compare(a, b) {
        return a.bookTopicNumber - b.bookTopicNumber;
    }
}