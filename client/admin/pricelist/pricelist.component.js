require('./_pricelist.scss');
angular.module(window.moduleName)
    .component('pricelistPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./pricelist.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state'];
function Controller(admin,$filter,$scope,$state){

	var vm = this;
	vm.form = {};
	vm.form.selectedCourses = [];

	vm.addPriceToList = addPriceToList;
	vm.removePriceFromList = removePriceFromList;
	vm.savePrice = savePrice;
	vm.editPricelist = editPricelist;
	vm.deletePricelist = deletePricelist;
	vm.resetForm  = resetForm;

	vm.$onInit = function(){
		getAllBatchCourse();
		getAllCurrency();
		getAllCoursesFromMaster();
		getAllPricelist();
	}

	function getAllBatchCourse(){
		admin.getAllBatchCourse()
			.then(function(res){
				vm.allBatchCourses = res.data;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function getAllCoursesFromMaster() {
		admin.coursemaster()
			.then(function(d){
				vm.allCourses = d;
			})
			.catch(function(err){
				console.error(err);
			});
	}

	function getAllCurrency(){
		admin.getAllCurrency()
			.then(function(res){
				vm.allCurrency = res.data
			})
	}

	function getAllPricelist(){
		admin.getAllPricelist()
			.then(function(res){
				vm.allPricelist = res.data;
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function addPriceToList(){
		vm.form = vm.form || {};
		vm.form.crs_price = vm.form.crs_price || {};
		if(vm.crs_price_curr && vm.crs_price_amount){
			vm.form.crs_price[vm.crs_price_curr] = vm.crs_price_amount;
			vm.crs_price_curr = "";
			vm.crs_price_amount = "";
		}
	}

	function removePriceFromList(key){
		delete vm.form.crs_price[key];
	}

	function savePrice() {
		admin.savePricelist(vm.form)
			.then(function(res){
				getAllPricelist();
				resetForm();
			})
			.catch(function(err){
				console.error(err);
			})
	}

	function deletePricelist(id){
		admin.deletePricelist({crs_id : id})
			.then(function(res){
				getAllPricelist();
				resetForm();
			})
			.catch(function(err){
				console.error(err);
			})	
	}

	function editPricelist(id){
		var pl = $filter('filter')(vm.allPricelist,{crs_id:id})[0];
		vm.form = pl;
	}

	function resetForm(){
		vm.form = {};
		vm.crs_price_curr = "";
		vm.crs_price_amount = "";
	}
}