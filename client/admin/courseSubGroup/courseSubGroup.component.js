angular.module(window.moduleName)
    .component('courseSubgroupPage', {
    	require: {
    		parent : '^adminPage'
    	},
        template: require('./courseSubGroup.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['admin','$filter','$scope','$state'];
function Controller(admin,$filter,$scope,$state){
	var vm = this;

	vm.saveSubgroup = saveSubgroup;
	vm.deleteSubgroup = deleteSubgroup;

	vm.$onInit = function(){
		getAllSubgroup();
	}

	function getAllSubgroup(){
		admin.getAllSubgroup()
			.then(function(res){
				vm.allSubgroup = res.data
			})
	}

	function saveSubgroup(){
		admin.addSubgroup(vm.form)
			.then(function(res){
				getAllSubgroup();
				vm.form = {};
			})
			.catch(function(err){
				console.log(err);
			})
	}

	function deleteSubgroup(id){
		admin.deleteSubgroup({sbgrp_id : id})
			.then(function(res){
				getAllSubgroup();
			})
			.catch(function(err){
				console.log(err);
			})
	}
}