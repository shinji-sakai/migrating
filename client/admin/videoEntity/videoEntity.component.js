require('tagsinput_js');
require('tagsinput_css');
require('service.aws');
require('service.uploadFileAPI');
	angular.module(window.moduleName)
        .component('videoEntity', {
        	require: {
        		parent : '^adminPage'
        	},
            template: require('./videoEntity.html'),
            controller: AdminVideoEntityController,
            controllerAs: 'vm'
        });

	AdminVideoEntityController.$inject=['$log','admin','pageLoader','$filter','$scope','EditorConfig','awsService','uploadFileAPI'];
	function AdminVideoEntityController($log,admin,pageLoader,$filter,$scope,EditorConfig,awsService,uploadFileAPI) {

    	var vm = this;

    	vm.selectedSubjects = [];
    	vm.selectedAuthors = [];
    	vm.selectedPublications = [];
    	vm.selectedBooks = [];
    	vm.selectedExams = [];
    	vm.selectedTopics = [];
    	vm.selectedRelatedTopics = [];
    	vm.selectedRelatedVideos = [];
    	// vm.selectedRelatedCourses = [];
    	vm.selectedTags = [];
    	vm.noOfRefLinks = 1;
    	vm.form = {};
    	vm.form.video = {};
    	vm.form.video.thumbUrl = "";//"https://examwarrior.com/images/no-thumb.png";
    	vm.form.video.videoSource = "harddrive";
    	vm.form.video.refLinks = [];
		vm.videosOfModule = [];
		vm.shouldShowVideoForm = false;
		vm.isVideoUploadButtonDisabled = true;


		vm.videolist = Array.apply(null, {length: 10}).map(Number.call, Number);
		vm.hrlist = Array.apply(null, {length: 24}).map(Number.call, Number);
		vm.minlist = Array.apply(null, {length: 60}).map(Number.call, Number);
		vm.seclist = Array.apply(null, {length: 60}).map(Number.call, Number);

    	$scope.tinymceOptions = {
		    plugins : 'advlist autolink link image lists charmap print preview',
		    menubar : false,
		    statusbar: false
		  };
		$('#shortdesc').froalaEditor(EditorConfig);

		vm.$onInit = function() {
        	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
        		vm.courselist = v;
        	});
        	updateVideoEntities();
        	findAllVideoEntities();
        	admin.getAllSubject()
        		.then(function(d){
        			vm.subjects = d.data;
        		});
        	admin.getAllAuthor()
        		.then(function(d){
        			vm.authors = d.data;
        		});
        	admin.getAllPublication()
        		.then(function(d){
        			vm.publications = d.data;
        		});
        	admin.getAllBook()
        		.then(function(d){
        			vm.books = d.data;
        		});
        	admin.getAllExam()
        		.then(function(d){
        			vm.exams = d.data;
        		});
        	admin.getAllTopic()
        		.then(function(d){
        			vm.topics = d.data;
        		});
        	admin.getAllTag()
	    		.then(function(d){
	    			vm.tags = d.data;
	    		});

	    	//Observer files
			$scope.$watch('vm.videoFile',function(newValue,oldValue){
				if(newValue){
					vm.videoFileName = newValue.name;	
					vm.isVideoUploadInProgress = false;
					vm.isVideoUploaded = false;
				}else{
					vm.videoFileName = "";	
					vm.isVideoUploadInProgress = false;
					vm.isVideoUploaded = false;
				}
				
			})
			//Observer files
			$scope.$watch('vm.videoMEDFile',function(newValue,oldValue){
				if(newValue){
					vm.videoMEDFileName = newValue.name;
					vm.isVideoMEDUploadInProgress = false;
					vm.isVideoMEDUploaded = false;
				}else{
					vm.videoMEDFileName = "";
					vm.isVideoMEDUploadInProgress = false;
					vm.isVideoMEDUploaded = false;
				}
			})
			//Observer files
			$scope.$watch('vm.videoLOWFile',function(newValue,oldValue){
				if(newValue){
					vm.videoLOWFileName = newValue.name;
					vm.isVideoLOWUploadInProgress = false;
					vm.isVideoLOWUploaded = false;
				}else{
					vm.videoLOWFileName = "";
					vm.isVideoLOWUploadInProgress = false;
					vm.isVideoLOWUploaded = false;
				}
			})

			//Observer files
			$scope.$watch('vm.subtitleFile',function(newValue,oldValue){
				if(newValue){
					vm.subtitleFileName = newValue.name;	
					vm.isSubtitleUploadInProgress = false;
					vm.isSubtitleUploaded = false;
				}
				
			})
	    };


		vm.getNumber = getNumber;

    	vm.updateVideoEntity = updateVideoEntity;
    	vm.removeVideoEntity = removeVideoEntity;
    	vm.editVideoEntity = editVideoEntity;
    	vm.resetFormDetail = resetFormDetail;
    	vm.updateVideoEntities = updateVideoEntities;

    	vm.removeRefLink = removeRefLink;
    	vm.uploadVideoFile = uploadVideoFile;
    	vm.uploadVideoMEDFile = uploadVideoMEDFile;
    	vm.uploadVideoLOWFile = uploadVideoLOWFile;
    	vm.uploadSubtitleFile = uploadSubtitleFile;

    	vm.checkVideoFilenameExistance = checkVideoFilenameExistance;

    	function getNumber(num) {
		    return Array.apply(null,{length:num});   
		}

		function resetFormDetail(){
			// vm.selectedSubjects = [];
	    	vm.selectedAuthors = [];
	    	vm.selectedPublications = [];
	    	vm.selectedBooks = [];
	    	vm.selectedExams = [];
	    	vm.selectedTopics = [];
	    	vm.selectedRelatedTopics = [];
	    	vm.selectedRelatedVideos = [];
	    	// vm.selectedRelatedCourses = [];
	    	vm.selectedTags = [];
	    	vm.noOfRefLinks = 1;    	
			vm.form.video = {};
			vm.videoLOWFile = "";
			vm.videoFile = "";
			vm.videoMEDFile = "";
			vm.isVideoUploadButtonDisabled = true;
			vm.isVideoInfoInEditMode = false;
			vm.isVideoFilenameExist = false;
		}

		function checkVideoFilenameExistance(){
			if(vm.form.video.videoFilename && vm.form.video.videoFilename.length > 0){
				vm.isVideoUploadButtonDisabled = true;
				admin.isVideoFilenameExist({videoFilename : vm.form.video.videoFilename})
					.then(function(res){
						vm.isVideoFilenameExist = res.data.exist;
						if(vm.isVideoFilenameExist === false){
							vm.isVideoUploadButtonDisabled = false;
						}
					})
					.catch(function(err){
						$log.error(err);
					})
			}else{
				vm.isVideoFilenameExist = false;
				vm.isVideoUploadButtonDisabled = true;
			}
		}

		function findAllVideoEntities() {
			admin.findVideoEntityByQuery({})
				.then(function(data){
					if(data.data.length > 0){
						console.log(data.data.length)
						vm.relatedVideoEntities = data.data;
						vm.relatedVideoEntities.sort(compare);
						vm.relatedVideoNumber = vm.relatedVideoEntities[vm.relatedVideoEntities.length - 1]['videoNumber'] + 1; 
					}else{
						vm.relatedVideoNumber = 1;
						vm.relatedVideoEntities = undefined;
					}
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function updateVideoEntities(){
			if(vm.selectedSubjects.length > 0){
				admin.getAllTopic({subjectId : vm.selectedSubjects[0]})
        		.then(function(d){
        			vm.topics = d.data;
        		});	
			}
			if(vm.selectedTopics.length > 0){
				var topic = $filter('filter')(vm.topics,{topicId:vm.selectedTopics[0]})[0];
				vm.form = vm.form || {};
				vm.form.video = vm.form.video || {};
				vm.form.video.videoName = topic.topicName;
			}else{
				vm.form = vm.form || {};
				vm.form.video = vm.form.video || {};
				vm.form.video.videoName = "";
			}

			var query = {
				subjects	: vm.selectedSubjects,
		    	authors	: vm.selectedAuthors,
		    	publications	: vm.selectedPublications,
		    	books	: vm.selectedBooks,
		    	exams	: vm.selectedExams,
		    	topics	: vm.selectedTopics,
		    	tags	: vm.selectedTags
			}
			admin.findVideoEntityByQuery(query)
				.then(function(data){
					if(data.data.length > 0){
						vm.videoEntities = data.data;
						vm.videoEntities.sort(compare);
						vm.videoNumber = vm.videoEntities[vm.videoEntities.length - 1]['videoNumber'] + 1; 
					}else{
						vm.videoNumber = 1;
						vm.videoEntities = undefined;
					}
				})
				.catch(function(err){
					console.error(err);
				})
		}
		

		function updateVideoEntity(){
			//Add Video Id
			vm.form.video.videoId = (vm.selectedTopics[0] || '') + "-" + vm.videoNumber;
			// if(!vm.form.video.videoDuration){
			// 	vm.form.video.videoDuration = {};
			// }
			// if(!vm.form.video.videoDuration.hr){
			// 	vm.form.video.videoDuration.hr = 0;
			// }
			// if(!vm.form.video.videoDuration.min){
			// 	vm.form.video.videoDuration.min = 0;
			// }
			// if(!vm.form.video.videoDuration.sec){
			// 	vm.form.video.videoDuration.sec = 0;
			// }
			
			var data = {
				videoId : vm.form.video.videoId,
				videoNumber : vm.videoNumber,
				videoName : vm.form.video.videoName,
				videoDuration : vm.form.video.videoDuration,
				videoDescription : videoDescription(),
				thumbUrl : vm.form.video.thumbUrl,
				videoUrl : vm.form.video.videoUrl,
				videoFilename : vm.form.video.videoFilename,
				subtitleUrl : vm.form.video.subtitleUrl,
				videoSource : vm.form.video.videoSource,
				videoRefLinks : vm.form.video.refLinks,
				subjects : vm.selectedSubjects,
				authors : vm.selectedAuthors,
				publications : vm.selectedPublications,
				books : vm.selectedBooks,
				exams : vm.selectedExams,
				topics : vm.selectedTopics,
				relatedTopics : vm.selectedRelatedTopics,
				// relatedCourses : vm.selectedRelatedCourses,
				tags : vm.selectedTags,
				relatedVideos : vm.selectedRelatedVideos,
				isHIGHDefVideoUploaded : vm.form.video.isHIGHDefVideoUploaded,
				isMEDDefVideoUploaded : vm.form.video.isMEDDefVideoUploaded,
				isLOWDefVideoUploaded : vm.form.video.isLOWDefVideoUploaded,
				isSubtitleUploaded : vm.form.video.isSubtitleUploaded,
				thumbnailPath : vm.form.video.thumbnailPath
			}
			// console.log(data);
			pageLoader.show();
			admin.updateVideoEntity(data)
				.then(function(){
					vm.resetFormDetail();
					vm.submitted = true;
					vm.form.error = false;

					updateVideoEntities();

				})
				.catch(function(){
					vm.submitted = true;
					vm.form.error = false;
				})
				.finally(function(){
					vm.form = {};
					vm.form.video = {};
					pageLoader.hide()
				});
		}


		function editVideoEntity(videoId){
			var video = $filter('filter')(vm.videoEntities, {videoId: videoId})[0];
			// console.log(video);
			vm.form.video = video;
			vm.form.videoId = videoId;

			vm.videoFileName = (vm.form.video.isHIGHDefVideoUploaded) ? vm.form.video.videoFilename : "Select Video";
			vm.videoMEDFileName = (vm.form.video.isMEDDefVideoUploaded) ? vm.form.video.videoFilename + "-480" : "Select Video";
			vm.videoLOWFileName = (vm.form.video.isLOWDefVideoUploaded) ? vm.form.video.videoFilename + "-360" : "Select Video";
			vm.subtitleFileName = (vm.form.video.isSubtitleUploaded) ? vm.form.video.subtitleUrl : "Select Subtitle";

			vm.thumbnailPath = video.thumbnailPath;
			vm.videoNumber = video.videoNumber;
			
			videoDescription(video.videoDescription);

			vm.selectedSubjects = video.subjects;
	    	vm.selectedAuthors = video.authors;
	    	vm.selectedPublications = video.publications;
	    	vm.selectedBooks = video.books;
	    	vm.selectedExams = video.exams;
	    	vm.selectedTopics = video.topics;
	    	vm.selectedRelatedTopics = video.relatedTopics;
	    	// vm.selectedRelatedCourses = video.relatedCourses;
	    	vm.selectedTags = video.tags;
	    	vm.selectedRelatedVideos = video.relatedVideos;

            vm.shouldShowVideoForm = true;
            if(vm.form.video.videoFilename.length > 0){
            	vm.isVideoUploadButtonDisabled = false;
            }else{
            	vm.isVideoUploadButtonDisabled = true;
            }
            
            vm.isVideoInfoInEditMode = true;
            vm.isVideoFilenameExist = false;

		}
		function removeVideoEntity(videoId){
			var result = confirm("Want to delete?");
			if (!result) {
			    //Declined
			   	return;
			}
			pageLoader.show();
			admin.removeVideoEntity({videoId:videoId})
				.then(function(d){
					updateVideoEntities();
					console.log("Removed Video")
				})
				.catch(function(){
					console.error(err);
				})
				.finally(function(){pageLoader.hide();});
		}

		function removeRefLink(index){
			if(vm.noOfRefLinks > 1){
				vm.form.video.refLinks.splice(index,1);
				vm.noOfRefLinks--;
			}
		}

		function compare(a,b){
			return a.videoNumber - b.videoNumber;
		}

		//set or get
		function videoDescription(desc){
			if(desc){
				$('#shortdesc').froalaEditor('html.set',desc);
				return;
			}
			return $('#shortdesc').froalaEditor('html.get',true);
		}

		function uploadVideoFile(){
			var file = {
	    		type : 'video',
	    		file : vm.videoFile,
	    		fileName : vm.form.video.videoFilename,
	    		generateThumbs : true,
	    		target : vm.form.video.videoSource
	    	}
			// if(vm.form.video.videoSource === "harddrive"){
				vm.isVideoUploadInProgress = true;
		    	uploadFileAPI.uploadVideoEntity(file)
		    		.then(function(d){
						var data = d.data;
						console.log(data);
						vm.form = vm.form || {};
						vm.form.video = vm.form.video || {};
						vm.form.video.videoUrl = data.Key;
						vm.form.video.isHIGHDefVideoUploaded = true;
						vm.form.video.thumbnailPath = data.thumbnailPath; 
						var durArr = data.duration.split(":");
						if(durArr.length == 2){
							vm.form.video.videoDuration = vm.form.video.videoDuration || {};
							vm.form.video.videoDuration["min"] = parseInt(durArr[0] || "0");
							vm.form.video.videoDuration["sec"] = parseInt(durArr[1] || "0");
						}else{
							vm.form.video.videoDuration = vm.form.video.videoDuration || {};
							vm.form.video.videoDuration["hr"] = parseInt(durArr[0] || "0");
							vm.form.video.videoDuration["min"] = parseInt(durArr[1] || "0");
							vm.form.video.videoDuration["sec"] = parseInt(durArr[2] || "0");
						}
						console.log(vm.form.video.videoDuration)
						vm.isVideoUploadInProgress = false;
						vm.isVideoUploaded = true;
						console.log(vm.form)
					},function(err){
						console.error(err);
					},function(evt){
						var width = parseInt(100.0 * evt.loaded / evt.total);
						vm.uploadProgress = width;
					})
			// }else if(vm.form.video.videoSource === 's3'){
		 //    	vm.isVideoUploadInProgress = true;
		 //    	awsService.uploadFileToS3(file)
		 //    		.then(function(d){
			// 			var data = d.data;
			// 			vm.form = vm.form || {};
			// 			vm.form.video = vm.form.video || {};
			// 			vm.form.video.videoUrl = data.Key;
			// 			vm.form.video.isHIGHDefVideoUploaded = true;
			// 			vm.isVideoUploadInProgress = false;
			// 			vm.isVideoUploaded = true;
			// 		},function(err){
			// 			console.error(err);
			// 		},function(evt){
			// 			var width = parseInt(100.0 * evt.loaded / evt.total);
			// 			vm.uploadProgress = width;
			// 		})
			// }
	    }

	    function uploadVideoMEDFile(){
	    	var file = {
	    		type : 'video',
	    		file : vm.videoMEDFile,
	    		fileName : vm.form.video.videoFilename + "-480",
	    		target : vm.form.video.videoSource
	    	}
	    	// if(vm.form.video.videoSource === "harddrive"){
	    		vm.isVideoMEDUploadInProgress = true;
	    		uploadFileAPI.uploadVideoEntity(file)
		    		.then(function(d){
						var data = d.data;
						vm.form = vm.form || {};
						vm.isVideoMEDUploadInProgress = false;
						vm.isVideoMEDUploaded = true;
						vm.form.video.isMEDDefVideoUploaded = true;
					},function(err){
						console.error(err);
					},function(evt){
						var width = parseInt(100.0 * evt.loaded / evt.total);
						vm.MEDuploadProgress = width;
					})
	    	// }else if(vm.form.video.videoSource === "s3"){
	    // 		vm.isVideoMEDUploadInProgress = true;
	    // 		awsService.uploadFileToS3(file)
		   //  		.then(function(d){
					// 	var data = d.data;
					// 	vm.form = vm.form || {};
					// 	vm.isVideoMEDUploadInProgress = false;
					// 	vm.isVideoMEDUploaded = true;
					// 	vm.form.video.isMEDDefVideoUploaded = true;
					// },function(err){
					// 	console.error(err);
					// },function(evt){
					// 	var width = parseInt(100.0 * evt.loaded / evt.total);
					// 	vm.MEDuploadProgress = width;
					// })
	    	// }
	    	
	    }
	    function uploadVideoLOWFile(){
	    	var file = {
	    		type : 'video',
	    		file : vm.videoLOWFile,
	    		fileName : vm.form.video.videoFilename + "-360",
	    		target : vm.form.video.videoSource
	    	}
	    	// if(vm.form.video.videoSource === "harddrive"){
	    		vm.isVideoLOWUploadInProgress = true;
		    	$log.debug(file);
		    	uploadFileAPI.uploadVideoEntity(file)
		    		.then(function(d){
						var data = d.data;
						vm.form = vm.form || {};
						vm.isVideoLOWUploadInProgress = false;
						vm.isVideoLOWUploaded = true;
						vm.form.video.isLOWDefVideoUploaded = true;
					},function(err){
						console.error(err);
					},function(evt){
						var width = parseInt(100.0 * evt.loaded / evt.total);
						vm.LOWuploadProgress = width;
					})
	    // 	}else if(vm.form.video.videoSource === "s3"){
	    // 		vm.isVideoLOWUploadInProgress = true;
		   //  	$log.debug(file);
		   //  	awsService.uploadFileToS3(file)
		   //  		.then(function(d){
					// 	var data = d.data;
					// 	vm.form = vm.form || {};
					// 	vm.isVideoLOWUploadInProgress = false;
					// 	vm.isVideoLOWUploaded = true;
					// 	vm.form.video.isLOWDefVideoUploaded = true;
					// },function(err){
					// 	console.error(err);
					// },function(evt){
					// 	var width = parseInt(100.0 * evt.loaded / evt.total);
					// 	vm.LOWuploadProgress = width;
					// })
	    	// }
	    	
	    }
	    function uploadSubtitleFile(){
	    	var file = {
	    		type : 'video',
	    		file : vm.subtitleFile,
	    		target : vm.form.video.videoSource
	    	}

	    	// if(vm.form.video.videoSource === "harddrive"){
	    		vm.isSubtitleUploadInProgress = true;
		    	uploadFileAPI.uploadVideoEntity(file)
		    		.then(function(d){
						var data = d.data;
						vm.form = vm.form || {};
						vm.form.video = vm.form.video || {};
						vm.form.video.subtitleUrl = data.Key;
						vm.form.video.isSubtitleUploaded = true;
						vm.isSubtitleUploadInProgress = false;
						vm.isSubtitleUploaded = true;
					},function(err){
						console.error(err);
					},function(evt){
						var width = parseInt(100.0 * evt.loaded / evt.total);
						vm.subtitleUploadProgress = width;
					})
	    // 	}else if(vm.form.video.videoSource === "s3"){
	    // 		vm.isSubtitleUploadInProgress = true;
		   //  	awsService.uploadFileToS3(file)
		   //  		.then(function(d){
					// 	var data = d.data;
					// 	vm.form = vm.form || {};
					// 	vm.form.video = vm.form.video || {};
					// 	vm.form.video.subtitleUrl = data.Key;
					// 	vm.isSubtitleUploadInProgress = false;
					// 	vm.form.video.isSubtitleUploaded = true;
					// 	vm.isSubtitleUploaded = true;
					// },function(err){
					// 	console.error(err);
					// },function(evt){
					// 	var width = parseInt(100.0 * evt.loaded / evt.total);
					// 	vm.subtitleUploadProgress = width;
					// })
	    // 	}
	    	
	    }
    }