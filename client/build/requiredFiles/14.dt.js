webpackJsonp([14],{

/***/ 357:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('publicationPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(358),
	            controller: AdminPublicationController,
	            controllerAs: 'vm'
	        });

		AdminPublicationController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminPublicationController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;
	        vm.publications = [];
			vm.no_of_item_already_fetch = 0;
	        vm.checkValidityOfID = checkValidityOfID;
	    	vm.updatePublication = updatePublication;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removePublication = removePublication;
	    	vm.editPublication = editPublication;
	    	vm.$onInit = function() {
	        //	updatePublicationsList();
		    };

	        function checkValidityOfID() {
				vm.form.publicationId = vm.form.publicationId || "";
	            vm.form.publicationId = vm.form.publicationId.replace(/\s/g , "-");
					admin.isIdExist({
					collection :'publication',
					key:"publicationId",
					id_value:vm.form.publicationId			
				}).then(function(data)
				{				
	              console.log(data.data);
				  if(data.data.idExist)
				  {
					    vm.idExists = true;
				  }
				  else
				  {
					   vm.idExists = false;
				  }
				}).catch(function(err){
	             console.log(err);
				})
					
	           
	        }

			vm.fetchPublications = function(){
				vm.isPublicationsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'publication'
				})
				.then(function(res){
					$log.debug(res)
						vm.publications = res.data;
					vm.no_of_item_already_fetch = 5;
					vm.last_update_dt = vm.publications[vm.publications.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isPublicationsLoading = false;
				})
			}

			vm.fetchMorePublications = function(){
				vm.isPublicationsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
				  collection : 'publication',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.publications, {publicationId: v.publicationId})[0];
							if(!item){
								vm.publications.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						  vm.last_update_dt = vm.publications[vm.publications.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isPublicationsLoading = false;
				})
			}
		    function updatePublicationsList(){
		    	pageLoader.show();
		    	admin.getAllPublication()
	        		.then(function(data){
	        			if(data.data.length > 0)
	        				vm.publications = data.data;
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }
	    	function updatePublication(){
	    		var data = {
	    			publicationId : vm.form.publicationId,
	    			publicationName : vm.form.publicationName,
	    			publicationDesc : vm.form.publicationDesc 
	    		}
				    if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }

	    		pageLoader.show();
	    		admin.updatePublication(data)
	    			.then(function(res){
						// var item = $filter('filter')(vm.publications || [], {publicationId: vm.form.publicationId})[0];
						// if(item){
						// 		item.publicationId = vm.form.publicationId,
						// 		item.publicationName = vm.form.publicationName,
						// 		item.publicationDesc = vm.form.publicationDesc 					
						// }else{
						// 	vm.publications = [data].concat(vm.publications || [])
						// }
						vm.fetchPublications();
	    				vm.submitted = true;
	    				vm.form.error = false;
	    				vm.form.publicationName = "";
	    				vm.form.publicationDesc = "";
	                    vm.form.publicationId ="";
	                    vm.formIsInEditMode = false;
	    				//updatePublicationsList();
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}
	    	function resetFormDetail(){
	    		vm.submitted = false;
	            vm.idExists = false;
	            vm.formIsInEditMode = false;
	    		vm.form = {};
	    	}
	    	function removePublication(id){
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	    		admin.removePublication({publicationId:id})
	    			.then(function(d){
	    				console.log('publicationId'+d);
							var item = $filter('filter')(vm.publications || [], {publicationId: id})[0];	
								console.log('publicationId'+d);							
						 if (vm.publications.indexOf(item) > -1) {
	                        var pos = vm.publications.indexOf(item);
	                       vm.publications.splice(pos, 1);
	                    }
	    				//updatePublicationsList();
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}
	    	function editPublication(id){
	    		var publication = $filter('filter')(vm.publications,{publicationId:id})[0];
	    		vm.form = {};
	    		vm.form.publicationId = publication.publicationId;
	    		vm.form.publicationName = publication.publicationName;
	    		vm.form.publicationDesc = publication.publicationDesc;
	            vm.formIsInEditMode = true;
	    	}
	    }

/***/ },

/***/ 358:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updatePublication()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Publication</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"pubId\">Publication Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"pubId\" class=\"form-control\" placeholder=\"Id\" ng-change=\"vm.checkValidityOfID()\" ng-disabled=\"vm.formIsInEditMode\" ng-model=\"vm.form.publicationId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.idExists\">\n\t\t\t\t\t\t\t\tPublication Id exists\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"pubname\">Publication Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"pubname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.publicationName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"pubdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"pubdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.publicationDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Publication</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" ng-disabled=\"vm.idExists\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchPublications()\" ng-if=\"!vm.publications || vm.publications.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isPublicationsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isPublicationsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Publications</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.publications\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.publications\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"pub in vm.publications\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removePublication(pub.publicationId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editPublication(pub.publicationId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{pub.publicationName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in pub\">\n\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMorePublications()\" ng-if=\"(vm.publications==[] || vm.publications.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isPublicationsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isPublicationsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\t\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});