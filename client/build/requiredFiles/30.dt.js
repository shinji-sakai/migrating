webpackJsonp([30],{

/***/ 394:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('skillsPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(395),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['$log','admin','$filter','$scope','$state'];
	function Controller($log,admin,$filter,$scope,$state){
		var vm = this;

		vm.saveSkill = saveSkill;
		vm.deleteSkill = deleteSkill;

		vm.$onInit = function(){
			//getAllSkills();
		}

		function getAllSkills(){
			admin.getAllEmployeeSkills()
				.then(function(res){
					vm.allSkills = res.data
				})
		}
			vm.fetchSkills = function(){
	            vm.allSkills = [];
				vm.isSkillsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : 0,
	                collection : 'skills'
				})
				.then(function(res){
					$log.debug(res)
					vm.allSkills = res.data;
					vm.no_of_item_already_fetch = 5;
	                vm.last_update_dt = vm.allSkills[vm.allSkills.length - 1].update_dt
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isSkillsLoading = false;
				})
			}


			vm.fetchMoreSkills = function(){
				vm.isSkillsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : vm.no_of_item_already_fetch,
	                collection : 'skills',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.allSkills, {skl_id: v.skl_id})[0];
							if(!item){
								vm.allSkills.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
	                    vm.last_update_dt = vm.allSkills[vm.allSkills.length - 1].update_dt;
						
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isSkillsLoading = false;
				})
			}


		function saveSkill(){
			    if(!vm.formIsInEditMode){
	                vm.form.create_dt = new Date();
	                vm.form.update_dt = new Date();
	            }else{
	                vm.form.update_dt = new Date();
	            }
			admin.addEmployeeSkill(vm.form)
				.then(function(res){
					vm.fetchSkills();
				//	getAllSkills();
					vm.form = {};
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function deleteSkill(id){
			admin.deleteEmployeeSkill({skl_id : id})
				.then(function(res){
						var item = $filter('filter')(vm.allSkills || [], {skl_id: id})[0];								
						 if (vm.allSkills.indexOf(item) > -1) {
	                        var pos = vm.allSkills.indexOf(item);
	                       vm.allSkills.splice(pos, 1);
	                    }	
					//getAllSkills();
				})
				.catch(function(err){
					console.log(err);
				})
		}
	}

/***/ },

/***/ 395:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" ng-submit=\"vm.saveSkill()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Skill</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"skill name\">Skill<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Skill\" ng-model=\"vm.form.skl_nm\" required/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.form= {}\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchSkills()\" ng-if=\"!vm.allSkills || vm.allSkills.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isSkillsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isSkillsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Skill</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.allSkills\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allSkills\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"skill in vm.allSkills\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteSkill(skill.skl_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t{{skill.skl_nm}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row skill-info-row\" ng-repeat=\"(key,value) in skill\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreSkills()\" ng-if=\"(vm.allSkills==[] || vm.allSkills.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isSkillsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isSkillsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});