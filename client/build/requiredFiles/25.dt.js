webpackJsonp([25],{

/***/ 129:
/***/ function(module, exports) {

	'use strict';

	angular.module("service")
	    .service('userCourses', UserCourses);


	UserCourses.$inject = ['$http','BASE_API'];

	function UserCourses($http,BASE_API) {

	    //deprecated
	    this.getPurchasedCourses = function(data){
	        return $http.post(BASE_API + '/uc/getPurchasedCourseOfUser',data);
	    }

	    this.updatePurchasedCourses = function(data){
	        return $http.post(BASE_API + '/uc/updatePurchasedCourses',data);
	    }

	    // addPurchasedCourse
	    this.addPurchasedCourse = function(data){
	        return $http.post(BASE_API + '/uc/addPurchasedCourse',data);
	    }

	    this.getPurchasedCourseOfUser = function(data) {
	        return $http.post(BASE_API + '/uc/getPurchasedCourseOfUser',data);
	    }

	    this.getPurchasedCourseDetails = function(data) {
	        return $http.post(BASE_API + '/uc/getPurchasedCourseDetails',data);
	    }
	};

/***/ },

/***/ 360:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(51);
	__webpack_require__(43);

	angular.module('service').service('profile',Profile);
	Profile.$inject = ['$http','Upload','BASE_API'];
	function Profile($http,Upload,BASE_API) {
		var self = this;

		self.saveProfilePic = function(data){
			return Upload.upload({
	            url: BASE_API + '/dashboard/profile/saveProfilePic',
	            mehtod: 'POST',
	            data: data
	        });
		}

		self.getUserFriends = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserFriendsAndFollowings',data);
		}

		self.getAllUsersShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAllUsersShortDetails',data);
		}

		self.getUserShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserShortDetails',data);
		}

		self.getRecommendFriendForUser = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getRecommendFriendForUser',data);
		}

		self.sendFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/sendFriendRequest',data);	
		}

		self.acceptFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/acceptFriendRequest',data);	
		}

		self.deleteFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteFriendRequest',data);	
		}

		// unfriendFriend
		self.unfriendFriend = function(data){
			return $http.post(BASE_API + '/dashboard/profile/unfriendFriend',data);	
		}

		self.cancelFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/cancelFriendRequest',data);	
		}

		self.blockPerson = function(data){
			return $http.post(BASE_API + '/dashboard/profile/blockPerson',data);	
		}

		self.getFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequest',data);	
		}

		self.getFriendStatus = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendStatus',data);		
		}

		// Follower
		self.addFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addFollower',data);		
		}	
		self.getFollowers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFollowers',data);		
		}	
		self.removeFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/removeFollower',data);		
		}

		//user personal details
		self.getUserPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPersonalDetails',data);		
		}
		self.saveUserBasicPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveUserBasicPersonalDetails',data);		
		}

		self.getUserEmails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserEmails',data);		
		}

		self.getUserPhoneNumbers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPhoneNumbers',data);		
		}

		self.addUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserEmail',data);		
		}

		self.addUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserPhoneNumber',data);		
		}

		self.verifyUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserEmail',data);		
		}

		self.verifyUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserPhoneNumber',data);		
		}

		self.deleteUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserEmail',data);		
		}

		self.deleteUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		}
		
		// self.saveUserPhoneDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserPhoneDetails',data);		
		// }
		// self.saveUserEmailDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserEmailDetails',data);		
		// }
		// startVerifingEmailAddress
		self.startVerifingEmailAddress = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingEmailAddress',data);		
		}
		// startVerifingPhoneNumber
		self.startVerifingPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingPhoneNumber',data);		
		}

		// // confirmVerificationOfEmail
		// self.confirmVerificationOfEmail = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfEmail',data);		
		// }
		// // confirmVerificationOfPhoneNumber
		// self.confirmVerificationOfPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfPhoneNumber',data);		
		// }

		// //deleteUserPhoneNumber
		// self.deleteUserPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		// }

		// // deleteUserEmailAddress
		// self.deleteUserEmailAddress = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserEmailAddress',data);		
		// }

		// getUserContactDetails
		self.getUserContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserContactDetails',data);		
		}
		// addNonVerifiedEmailInContactDetails
		self.addNonVerifiedEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addNonVerifiedEmailInContactDetails',data);		
		}

		// confirmVerificationOfContactEmail
		self.confirmVerificationOfContactEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfContactEmail',data);		
		}

		// deleteEmailInContactDetails
		self.deleteEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEmailInContactDetails',data);		
		}

		// changePrimaryEmailInContactDetails
		self.changePrimaryEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/changePrimaryEmailInContactDetails',data);		
		}	


		// Work Form Helper
		self.saveWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveWorkInfo',data);		
		}	
		self.getWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getWorkInfo',data);		
		}
		self.deleteWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteWorkInfo',data);		
		}

		// Edu Form Helper
		self.saveEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveEduInfo',data);		
		}	
		self.getEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getEduInfo',data);		
		}
		self.deleteEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEduInfo',data);		
		}

		// Exam Form Helper
		self.saveExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveExamInfo',data);		
		}	
		self.getExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getExamInfo',data);		
		}
		self.deleteExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteExamInfo',data);		
		}

		// Interested Course Form Helper
		self.saveInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveInterestedCourseInfo',data);		
		}	
		self.getInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getInterestedCourseInfo',data);		
		}
		self.deleteInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteInterestedCourseInfo',data);		
		}

		// Author Form Helper
		self.saveAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorInfo',data);		
		}	
		self.getAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAuthorInfo',data);		
		}
		self.deleteAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteAuthorInfo',data);		
		}
		self.saveAuthorDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorDescInfo',data);		
		}

		// Teacher info Form Helper
		self.saveTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherInfo',data);		
		}	
		self.getTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTeacherInfo',data);		
		}
		self.deleteTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteTeacherInfo',data);		
		}
		self.saveTeacherDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherDescInfo',data);		
		}

		// Kid info Form Helper
		self.saveKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveKidInfo',data);		
		}	
		self.getKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getKidInfo',data);		
		}
		self.deleteKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteKidInfo',data);		
		}


		// Friend request notification api
		// getFriendRequestNotifications
		self.getFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequestNotifications',data);		
		}
		// updateFriendRequestNotifications
		self.updateFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateFriendRequestNotifications',data);		
		}

		// getTaggedFriendsNotifications
		self.getTaggedFriendsNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTaggedFriendsNotifications',data);		
		}
		// updateTaggedFriendNotifications
		self.updateTaggedFriendNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateTaggedFriendNotifications',data);		
		}

		self.getFriendsListForMessage = function(data){
			return $http.post(BASE_API + '/chat/friends/find',data);		
		}
	}

/***/ },

/***/ 380:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(129);
	__webpack_require__(360)

	angular.module(window.moduleName)
	    .component('userCoursesPage', {
	        template: __webpack_require__(381),
	        controller: AdminUserCoursesController,
	        controllerAs: 'vm'
	    });

	AdminUserCoursesController.$inject=['admin','pageLoader','$filter','userCourses','profile'];

	function AdminUserCoursesController(admin,pageLoader,$filter,userCourses,profile) {
		var vm = this;

		vm.selectedUser = [];
		vm.selectedBundles = [];
		vm.form = {};
		var today = new Date();
		vm.form.startDate  = (today.getMonth() + 1) + '-' + today.getDate() + '-' +  today.getFullYear();

		vm.saveUserBundle = saveUserBundle;
		vm.getUserPuchasedCourses = getUserPuchasedCourses;


		//initialize data 
		init();
		function init(){
			//get all users short details 
			//to display in drop down
			profile.getAllUsersShortDetails()
				.then(function(d){
					console.log(d.data);
					vm.users = d.data;
				})
				.catch(function(err){
					console.log(err);
				})

			getBundleList();
			getUserPuchasedCourses();
		}

		//update bundle list locally
	    function getBundleList(){
	        admin.getAllCourseBundle()
	            .then(function(d){
	                vm.bundles = d.data;
	            })
	            .catch(function(err){
	                console.error(err);
	            });
	    }

	    function getUserPuchasedCourses(){
	    	if(vm.selectedUser.length > 0){
	    		var user = vm.selectedUser[0].usr_id;
	    		userCourses.getPurchasedCourses({userId : user})
	    			.then(function(r){
	    				vm.purchasedCourses = r.data;
	    			})	
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    	}
	    	else{
	    		vm.purchasedCourses = [];
	    	}
	    	
	    }


	    //save bundles to db
	    function saveUserBundle(){
	    	var usr_id = vm.selectedUser[0].usr_id;
	    	var user_courses = [];
	    	vm.selectedBundles.map(function(v,i){
	    		user_courses = user_courses.concat(v.bundleCourses.map(function(v,i){
	    			return {
	    				courseId : v,
	    				startDate : vm.form.startDate,
	    				endDate : vm.form.endDate
	    			}
	    		}));

	    		return v;
	    	});
	    	var bundl = {
	    		userId : usr_id,
	    		courses : user_courses
	    	}
	    	pageLoader.show();
	    	vm.isSubmitted = false;
	    	userCourses.updatePurchasedCourses(bundl)
	    		.then(function(r){
	    			getUserPuchasedCourses();
	    			resetFormDetails();
	    			vm.error = undefined;
	    			vm.success = true;
	    		})
	    		.catch(function(err){
	    			console.log(err);
	    			vm.error = err;
	    			vm.success = undefined;
	    		})
	    		.finally(function(){
	    			pageLoader.hide();
	    			vm.isSubmitted = true;

	    		})
	    }

	    function resetFormDetails() {
	    	vm.form = {};
	    	var today = new Date();
			vm.form.startDate  = (today.getMonth() + 1) + '-' + today.getDate() + '-' +  today.getFullYear();
			vm.selectedUser = [];
			vm.selectedBundles = [];
	    }
	}

/***/ },

/***/ 381:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add User Courses</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"userid\">UserId</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple limit=\"1\" ng-model=\"vm.selectedUser\" on-select=\"vm.getUserPuchasedCourses()\" on-remove=\"vm.getUserPuchasedCourses()\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select User...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.dsp_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item in (vm.users | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.dsp_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"courses\">Bundles</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBundles\"  theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Bundle...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.bundleName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item in (vm.bundles | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.bundleName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"startdate\">Start Date</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"startdate\" ng-model=\"vm.form.startDate\" placeholder=\"mm-dd-yyyy\"></input>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"enddate\">End Date</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"enddate\" ng-model=\"vm.form.endDate\" placeholder=\"mm-dd-yyyy\"></input>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-7\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"vm.selectedUser.length != 1 || vm.selectedBundles.length <= 0 || !vm.form.startDate || !vm.form.endDate\" ng-click=\"vm.saveUserBundle()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"vm.form.success\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.purchasedCourses\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.purchasedCourses\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"course in vm.purchasedCourses\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t{{course.courseId}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in course\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});