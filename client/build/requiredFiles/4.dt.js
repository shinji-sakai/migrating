webpackJsonp([4],{

/***/ 168:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {	angular.module(window.moduleName)
	        .component('modulesPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(169),
	            controller: AdminModulesController,
	            controllerAs: 'amc'
	        });
		
		AdminModulesController.$inject=['admin','$filter','pageLoader','$scope','EditorConfig'];

	    function AdminModulesController(admin,$filter,pageLoader,$scope,EditorConfig) {

	    	var amc = this;
			amc.modulelist = [1,2,3,4,5,6,7,8,9,10];
			amc.buttonName = "Save";
			amc.submitted = false;
			amc.deleteCourseModule=deleteCourseModule;
			amc.updateFormWithData = updateFormWithData;
			amc.resetCourseModule=resetCourseModule;

			var editor = $("#moduledesc").froalaEditor(EditorConfig);

			amc.$onInit = function() {
	        	$scope.$watch(function(){return amc.parent.coursemaster;},function(v){
	        		// amc.courselist = v;	
	        		amc.courselist = amc.courselist || [];
		        	if(v){
		        		amc.courselist = amc.courselist.concat(v);
		        		getAllTiming();
		        	}
	        	});
		    };

		    function getAllTiming(){
				admin.getAllBatchTiming()
					.then(function(res){
						amc.allTimings = res.data;
						var batches = amc.allTimings.map(function(v,i) {
							var nm = v.bat_nm || (v.crs_id + "-" + v.bat_id);
							return {
								courseId : nm,
								courseName : v.crs_id + "-" + $filter("date")(v.cls_start_dt,"dd-MMM-yyyy")
							}
						})
						amc.courselist = amc.courselist || [];
						if(batches){
							amc.courselist = amc.courselist.concat(batches) 	
						}
						
					})
					.catch(function(err){
						console.error(err);
					})
			}

			amc.addCourseModule = function(){

				pageLoader.show();
				var courseModuleData= {
					courseId:amc.form.course,
					courseName:amc.form.courseName,
					moduleNumber:amc.form.moduleNumber,
					moduleId:amc.form.course+"-"+amc.form.moduleNumber,
					moduleName:amc.form.moduleName,
					moduleDesc:$('#moduledesc').froalaEditor('html.get', true),
					moduleWeight:parseFloat(amc.form.moduleWeight),
				};
				amc.buttonName = "Uploading...";
				console.log(courseModuleData);
				admin.addCourseModule(courseModuleData)
					.then(function success(data){

						updateData(amc.form.course);

						//amc.form = {};
						amc.submitted = true;
						amc.form.error = false;
						amc.buttonName = "Save";
					})
					.catch(function err(err){

						amc.form = {};
						amc.submitted = true;
						amc.form.error = true;
						amc.buttonName = "Save";

						console.error(err);

					})
					.finally(function(){
						pageLoader.hide();
					});
			}

			amc.changeCourse= function(){
				// amc.courseId = amc.form.course;
				if(amc.form.course){
					var item = $filter('filter')(amc.courselist, {courseId: amc.form.course})[0];
					amc.form.courseName = item.courseName;
					updateData(amc.form.course);
				}else{
					//On Select Course Option
					amc.form = {};
					amc.moduleFullData = undefined;
				}
			}


			function deleteCourseModule(id,moduleNumber) {
				var result = confirm("Want to delete?");
				if (!result) {
				    //Declined
				   	return;
				}
				pageLoader.show();
				var courseModuleData= {
					courseId:id,
					moduleNumber:moduleNumber
				};
				admin.deleteCourseModule(courseModuleData)
					.then(function(res){
						pageLoader.hide();
						updateData(id);
					});
			}

			function updateFormWithData(id,moduleNumber){
				var item = $filter('filter')(amc.moduleFullData, {courseId: id,moduleNumber:moduleNumber})[0];
				amc.form.course = id;
				amc.form.courseName = item.courseName;
				amc.form.moduleNumber = item.moduleNumber;
				amc.form.moduleName = item.moduleName;
				amc.form.moduleDesc = item.moduleDesc;
				$('#moduledesc').froalaEditor('html.set', item.moduleDesc)
				amc.form.moduleWeight = parseFloat(item.moduleWeight);
			}

			function updateData(id){

				pageLoader.show();
				amc.moduleFullData = undefined;
				amc.form.moduleNumber = "";
				amc.form.moduleName = "";
				amc.form.moduleDesc = "";
				admin.getCourseModules({'courseId':id})
					.then(function(data){
						amc.moduleFullData = data;
						amc.moduleFullData.sort(function(a,b){
							return a.moduleNumber - b.moduleNumber;
						});
						
						amc.form.moduleNumber = amc.moduleFullData[amc.moduleFullData.length - 1].moduleNumber + 1;
						amc.form.moduleWeight =  amc.form.moduleNumber;
					})	
					.catch(function(){
						console.error("Error");
						amc.form.moduleNumber = 1;
						amc.form.moduleWeight = 1;
					})
					.finally(function(){
						pageLoader.hide();
					})
			}

			function resetCourseModule(){
				var course = amc.form.course;
				amc.form = {};
				amc.form.course = course;
				amc.form.moduleNumber = amc.moduleFullData[amc.moduleFullData.length - 1].moduleNumber + 1;
				amc.form.moduleWeight =  amc.form.moduleNumber;
			}

	    }
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 169:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<fieldset>\n\t\t\t\t\t<legend>Add Module</legend>\n\t\t\t\t\t<form class=\"form form-horizontal\" name=\"form\">\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"course\">Course</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"course\" ng-model=\"amc.form.course\" ng-change=\"amc.changeCourse()\"  class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='course.courseId as course.courseName for course in amc.courselist'>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Course</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"module\">Module Number</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"module\" class=\"form-control\" placeholder=\"Module Number\" readonly ng-model=\"amc.form.moduleNumber\"/>\n\t\t\t\t\t\t\t\t<!--<select id=\"module\" ng-model=\"amc.form.moduleNumber\" ng-change=\"amc.changeModule()\" class=\"form-control\"-->\n\t\t\t\t\t\t\t\t\t<!--ng-options='module for module in amc.modulelist'>-->\n\t\t\t\t\t\t\t\t\t<!--<option value=\"\">Module Number</option>-->\n\t\t\t\t\t\t\t\t<!--</select>-->\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"modulesname\">Module Name</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" ng-disabled=\"!amc.form.course\" id=\"modulesname\" class=\"form-control\" placeholder=\"Module Name\" ng-model=\"amc.form.moduleName\" required/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"moduledesc\">Module Desc</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"moduledesc\" ng-disabled=\"!amc.form.course\" class=\"form-control\" spellcheck=\"true\" placeholder=\"Module Description\" ng-model=\"amc.form.moduleDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"moduleweight\">Module Weight</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"moduleweight\" ng-disabled=\"!amc.form.course\" class=\"form-control\" spellcheck=\"true\" placeholder=\"Module Weight to order modules\" ng-model=\"amc.form.moduleWeight\" required></input>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\"  ng-click=\"amc.resetCourseModule()\">New Module</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!amc.form.course\" ng-click=\"amc.addCourseModule()\">{{amc.buttonName}}</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</form>\n\t\t\t\t</fieldset>\n\t\t\t\t<div ng-if=\"amc.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"amc.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!amc.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!amc.moduleFullData\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"amc.moduleFullData\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"module in amc.moduleFullData | orderBy:'moduleWeight'\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"amc.deleteCourseModule(module.courseId,module.moduleNumber)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"amc.updateFormWithData(module.courseId,module.moduleNumber)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{module.moduleNumber}} - {{module.moduleName}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in module\">\n\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});