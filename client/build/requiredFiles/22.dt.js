webpackJsonp([22],{

/***/ 120:
/***/ function(module, exports) {

	  angular
	    .module('service')
	    .service('admin', admin);

	admin.$inject = ['$http','BASE_API'];

	function admin($http,BASE_API) {


	    this.isIdExist = function(data) {
	        return $http.post(BASE_API + '/admin/general/isIdExist',data);
	    }


	    this.fetchItemUsingLimit = function(data) {
	        return $http.post(BASE_API + '/admin/general/fetchItemUsingLimit',data);
	    }

	    /* Course Master API */

	    this.coursemaster = function() {
	        //  /admin/coursemaster
	        return $http.post(BASE_API + '/courseMaster/getAll').then(function(res) {
	            return res.data;
	        });
	    }

	    

	    this.addCourseMaster = function(data) {
	        //      /admin/addCourseMaster
	        return $http.post(BASE_API + '/courseMaster/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseMaster = function(data) {
	        //      /admin/deleteCourseMaster
	        return $http.post(BASE_API + '/courseMaster/delete', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.fetchCourseMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/courseMaster/fetchCourseMasterUsingLimit', data);
	    }

	    this.getRelatedCoursesByCourseId = function(data) {
	        return $http.post(BASE_API + '/courseMaster/getRelatedCoursesByCourseId',data);
	    }

	    /* Course API */
	    this.courselistfull = function(data) {
	        return $http.post(BASE_API + '/course/getAll',data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourse = function(data) {
	        // /admin/addCourse
	        return $http.post(BASE_API + '/course/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.removeCourse = function(data) {
	        //  /admin/removeCourse
	        return $http.post(BASE_API + '/course/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Videos API */
	    this.addVideos = function(data) {
	        //  /admin/addVideos
	        return $http.post(BASE_API + '/courseVideos/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getModuleVideos = function(data) {
	        //  /admin/getModuleVideos
	        return $http.post(BASE_API + '/courseVideos/getModuleVideos', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getCourseVideos = function(data) {
	        //      /admin/getCourseVideos
	        return $http.post(BASE_API + '/courseVideos/getCourseVideos', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Modules API */
	    this.getCourseModules = function(data) {
	        //      /admin/getCourseModules
	        return $http.post(BASE_API + '/courseModule/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourseModule = function(data) {
	        //  /admin/addCourseModule
	        return $http.post(BASE_API + '/courseModule/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseModule = function(data) {
	        //  /admin/deleteCourseModule
	        return $http.post(BASE_API + '/courseModule/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Video Slides API */
	    this.addVideoSlide = function(data) {
	        //  /admin/addVideoSlide
	        return $http.post(BASE_API + '/videoSlide/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getVideoSlides = function(data) {
	        //  /admin/getVideoSlides
	        return $http.post(BASE_API + '/videoSlide/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteVideoSlide = function(data) {
	        //  /admin/deleteVideoSlide
	        return $http.post(BASE_API + '/videoSlide/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Authorized Items API */

	    this.getAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/getItems',data);
	    }

	    this.updateAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/updateItems',data);
	    }

	    this.addAuthorizedItem = function(data){
	        return $http.post(BASE_API + '/ai/addItem',data);
	    }

	    this.getAuthorizedVideoItemFullInfo = function(data){
	        return $http.post(BASE_API + '/ai/getVideoItemFullInfo',data);
	    }
	    this.getVideoPermission = function(data){
	        return $http.post(BASE_API + '/ai/getVideoPermission',data);
	    }


	    /* practice Question API */

	    this.savePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestion',data);
	    }
	    this.savePreviewPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/savePreviewQuestion',data);
	    }
	    this.saveQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestionStats',data);
	    }
	    this.saveTestQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveTestQuestionStats',data);
	    }
	    this.saveTestReviews  = function(data){
	        return $http.post(BASE_API + '/practice/saveTestReviews',data);
	    }
	    this.removePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/removeQuestion',data);
	    }
	    this.getAllPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getAllQuestions',data);
	    }
	    this.findPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsByQuery',data);
	    }
	    this.findPracticeQuestionsIdByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsIdByQuery',data);
	    }
	    this.findPracticeQuestionsTextByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsTextByQuery',data);
	    }
	    this.findPreviewPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findPreviewQuestionsByQuery',data);
	    }
	    this.getPracticeQuestionsById = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsById',data);
	    }

	    this.getPracticeQuestionsByLimit = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsByLimit',data);
	    }

	    this.updateMultiplePracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/updateMultiplePracticeQuestions',data);
	    }
	    this.getAllAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersQuestionPerfStats',data);
	    }
	    this.getAllMonthAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllMonthAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersMonthQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersMonthQuestionPerfStats',data);
	    }
	    this.updateVerifyPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/updateVerifyPracticeQuestion',data);
	    }
	    this.getAllVerifyStatusOfPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getAllVerifyStatusOfPracticeQuestion',data);
	    }
	    this.getLatestVerifyStatusOfGivenPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getLatestVerifyStatusOfGivenPracticeQuestions',data);
	    }
	    this.getUserMonthQuestionAttemptStats = function(data){
	        return $http.post(BASE_API + '/practice/getUserMonthQuestionAttemptStats',data);
	    }
	    this.startUserExam = function(data){
	        return $http.post(BASE_API + '/practice/startUserExam',data);
	    }
	    this.finishUserExam = function(data){
	        return $http.post(BASE_API + '/practice/finishUserExam',data);
	    }
	    this.checkForPreviousExam = function(data){
	        return $http.post(BASE_API + '/practice/checkForPreviousExam',data);
	    }
	    this.getUserExamQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getUserExamQuestion',data);
	    }
	    this.saveUserExamQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserExamQuestionData',data);
	    }
	    this.startUserPractice = function(data){
	        return $http.post(BASE_API + '/practice/startUserPractice',data);
	    }
	    this.saveUserPracticeQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserPracticeQuestionData',data);
	    }
	    this.fetchNextUserPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/fetchNextUserPracticeQuestion',data);
	    }
	    this.getPracticeNodeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeNodeQuestions',data);
	    }






	    /* Highlight APi */

	    this.saveHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/save',data);
	    }
	    this.getQuestionHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getQuestionHighlights',data);
	    }
	    this.getUserHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getUserHighlights',data);
	    }
	    this.deleteHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/delete',data);
	    }

	    this.updateHighlightNote = function(data){
	        return $http.post(BASE_API + '/highlight/updateHighlightNote',data);
	    }

	    /*
	    Author API
	     */
	    this.getAllAuthor  = function(){
	        return $http.post(BASE_API + '/author/getAll');
	    }
	    this.updateAuthor  = function(data){
	        return $http.post(BASE_API + '/author/update',data);
	    }
	    this.removeAuthor  = function(data){
	        return $http.post(BASE_API + '/author/remove',data);
	    }
	    this.fetchAuthorUsingLimit=function(data){
	        return $http.post(BASE_API+'/author/fetchAuthorUsingLimit',data);
	    }
	    this.AuthorsIdIsExits=function(data){
	        return $http.post(BASE_API+'/author/AuthorsIdIsExits',data);
	    }

	    /*
	    Publication API
	     */
	    this.getAllPublication  = function(){
	        return $http.post(BASE_API + '/publication/getAll');
	    }
	    this.updatePublication  = function(data){
	        return $http.post(BASE_API + '/publication/update',data);
	    }
	    this.removePublication  = function(data){
	        return $http.post(BASE_API + '/publication/remove',data);
	    }
	   this.fetchPublicationsUsingLimit=function(data){
	       return $http.post(BASE_API+'/publication/fetchPublicationsUsingLimit',data);
	   }
	    /*
	    Book API
	     */
	    this.getAllBook  = function(){
	        return $http.post(BASE_API + '/book/getAll');
	    }
	    this.updateBook  = function(data){
	        return $http.post(BASE_API + '/book/update',data);
	    }
	    this.removeBook  = function(data){
	        return $http.post(BASE_API + '/book/remove',data);
	    }
	    this.fetchBooksUsingLimit=function(data){
	        return $http.post(BASE_API+'/book/fetchBooksUsingLimit',data);
	    }

	    /*
	    JobOpenings API
	     */
	    this.getAllJobOpenings  = function(){
	        return $http.post(BASE_API + '/jobOpenings/getAllJobOpenings');
	    }
	    this.updateJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/updateJobOpening',data);
	    }
	    this.removeJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/removeJobOpening',data);
	    }

	    /*
	    Subject API
	     */
	    this.getAllSubject  = function(){
	        return $http.post(BASE_API + '/subject/getAll');
	    }
	    this.updateSubject  = function(data){
	        return $http.post(BASE_API + '/subject/update',data);
	    }
	    this.removeSubject  = function(data){
	        return $http.post(BASE_API + '/subject/remove',data);
	    }
	   this.fetchSubjectMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/subject/fetchSubjectMasterUsingLimit',data);
	    }
	    this.SubjectIdIsExits=function(data){
	        return $http.post(BASE_API+'/subject/SubjectIdIsExits',data);
	    }
	    /*
	    Exam API
	     */
	    this.getAllExam  = function(){
	        return $http.post(BASE_API + '/exam/getAll');
	    }
	    this.updateExam  = function(data){
	        return $http.post(BASE_API + '/exam/update',data);
	    }
	    this.removeExam  = function(data){
	        return $http.post(BASE_API + '/exam/remove',data);
	    }
	    this.fetchExamUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/exam/fetchExamUsingLimit',data);
	    }
	    /*
	    Topic API
	     */
	    this.getAllTopic  = function(data){
	        return $http.post(BASE_API + '/topic/getAll',data);
	    }
	    this.updateTopic  = function(data){
	        return $http.post(BASE_API + '/topic/update',data);
	    }
	    this.removeTopic  = function(data){
	        return $http.post(BASE_API + '/topic/remove',data);
	    }
	   this.fetchTopicsUsingLimit=function(data)
	   {
	       return $http.post(BASE_API+'/topic/fetchTopicsUsingLimit',data);
	   }
	    /*
	    Topic Group API
	     */
	    this.getAllTopicGroups  = function(data){
	        return $http.post(BASE_API + '/topicGroup/getAllTopicGroups',data);
	    }
	    this.updateTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/updateTopicGroup',data);
	    }
	    this.removeTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/removeTopicGroup',data);
	    }
	    this.fetchTopicGroupUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/topicGroup/fetchTopicGroupUsingLimit',data);
	    }
	    /*
	    Tag API
	     */
	    this.getAllTag  = function(){
	        return $http.post(BASE_API + '/tag/getAll');
	    }
	    this.updateTag  = function(data){
	        return $http.post(BASE_API + '/tag/update',data);
	    }
	    this.removeTag  = function(data){
	        return $http.post(BASE_API + '/tag/remove',data);
	    }
	    this.fetchTagsUsingLimit=function(data){
	        return $http.post(BASE_API+'/tag/fetchTagsUsingLimit',data);
	    }
	    this.TagsIdIsExits=function(data)
	    {
	        return $http.post(BASE_API+'/tag/TagsIdIsExits',data);
	    }

	    /*
	    SubTopic API
	     */
	    this.getAllSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/getAll',data);
	    }
	    this.updateSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/update',data);
	    }
	    this.removeSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/remove',data);
	    }

	    /* videoentity api */
	    this.updateVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/update',data);
	    }
	    this.removeVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/remove',data);
	    }
	    this.getAllVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/getAll',data);
	    }
	    this.isVideoFilenameExist = function(data){
	        return $http.post(BASE_API + '/videoentity/isVideoFilenameExist',data);
	    }
	    this.findVideoEntityByQuery = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityByQuery',data);
	    }
	            //One Id
	    this.findVideoEntityById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityById',data);
	    }
	            //Multiple Ids
	    this.findVideoEntitiesById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntitiesById',data);
	    }
	    this.getRelatedTopicsVideoByVideoId = function(data){
	        return $http.post(BASE_API + '/videoentity/getRelatedTopicsVideoByVideoId',data);
	    }
	    this.getRelatedVideosByVideoId = function(data) {
	        return $http.post(BASE_API + '/videoentity/getRelatedVideosByVideoId',data);
	    }
	    this.updateVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/updateVideoLastViewTime',data);
	    }
	    this.getVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/getVideoLastViewTime',data);
	    }


	    /* moduleItems api */
	    this.updateModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/update',data);
	    }
	    this.getAllModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/getAll',data);
	    }
	    this.getCourseItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getCourseItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getFullCourseDetails = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getFullCourseDetails', data);
	    }
	    
	    this.getModuleItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getModuleItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getUserBatchEnrollUploads = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getUserBatchEnrollUploads',data);
	    }

	    /* createtest api */
	    this.createTest = function(data){
	        return $http.post(BASE_API + '/createtest/save',data);
	    }
	    this.getAllTest = function(data){
	        return $http.post(BASE_API + '/createtest/getAll',data);
	    }
	    this.getTestById = function(data){
	        return $http.post(BASE_API + '/createtest/getById',data);
	    }

	    /*
	        CourseBundle API
	     */
	    this.getAllCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/getAll',data);
	    }
	    this.updateCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/update',data);
	    }
	    this.removeCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/remove',data);
	    }



	    // Employee types api
	    this.addEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/add',data);
	    }
	    this.getAllEmployeeTypes  = function(data){
	        return $http.post(BASE_API + '/employee/types/getAll',data);
	    }
	    this.deleteEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/delete',data);
	    }

	    // Employee Details api
	    this.addEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/add',data);
	    }
	    this.getAllEmployees  = function(data){
	        return $http.post(BASE_API + '/employee/getAll',data);
	    }
	    this.deleteEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/delete',data);
	    }

	    // Employee Skills
	    this.addEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/add',data);
	    }
	    this.getAllEmployeeSkills  = function(data){
	        return $http.post(BASE_API + '/employee/skills/getAll',data);
	    }
	    this.deleteEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/delete',data);
	    }

	    // All links category
	    this.addAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/add',data);
	    }
	    this.getAllLinksCategories  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/getAll',data);
	    }
	    this.deleteAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/delete',data);
	    }


	    // Sidelinks article
	    this.addSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/update',data);
	    }
	    this.getAllSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getAll',data);
	    }
	    this.getSideLinkArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/get',data);
	    }
	    this.getMultipleSideLinkArticles = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getMultipleArticles',data);
	    }
	    this.deleteSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/remove',data);
	    }


	    // All Links
	    this.addLinkToAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/update',data);
	    }
	    this.getAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/getAll',data);
	    }
	    this.deleteLinkFromAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/remove',data);
	    }


	    // BoardCompetitiveCourse
	    this.updateBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/update',data);
	    }
	    this.getBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/get',data);
	    }
	    this.getBoardCompetitiveCoursesByCourseType  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/getCoursesByCourseType',data);
	    }
	    this.deleteBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/remove',data);
	    }


	    // Course Subgroup
	    this.addSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/add',data);
	    }
	    this.getAllSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/getAll',data);
	    }
	    this.deleteSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/delete',data);
	    }

	    // Save Batch Course
	    this.addBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/add',data);
	    }
	    this.getAllBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getAll',data);
	    }
	    this.deleteBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/delete',data);
	    }
	    this.getBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getBatchCourse',data);
	    }

	    // Save Batch Timing
	    this.addBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/add',data);
	    }
	    this.getAllBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getAll',data);
	    }
	    this.deleteBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/delete',data);
	    }
	    this.getTimingByCourseId = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getTimingByCourseId',data);
	    }


	    // Save Batch Timing Dashboard
	    this.addBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/addBatchTimingDashboard',data);
	    }
	    this.getAllBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/getAllBatchTimingDashboard',data);
	    }
	    this.deleteBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/deleteBatchTimingDashboard',data);
	    }
	    this.setBatchTimingDashboardAsCompleted  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/setBatchTimingDashboardAsCompleted',data);
	    }

	    //

	    // Currency
	    this.addCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/add',data);
	    }
	    this.getAllCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/getAll',data);
	    }
	    this.deleteCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/delete',data);
	    }

	    // Getting STarted
	    this.addGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/add',data);
	    }
	    this.getAllGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getAll',data);
	    }
	    this.deleteGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/delete',data);
	    }
	    this.getGettingStartedByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getByCourseId',data);
	    }

	    // Sessions
	    this.addSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/add',data);
	    }
	    this.getAllSessions  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getAll',data);
	    }
	    this.deleteSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/delete',data);
	    }
	    this.getSessionsByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getByCourseId',data);
	    }

	    // Sessions Items
	    this.addSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/add',data);
	    }
	    this.getAllSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getAll',data);
	    }
	    this.deleteSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/delete',data);
	    }
	    this.getSessionDetailsForBatchAndSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getSessionDetailsForBatchAndSession',data);
	    }


	    // Pricelist Course
	    this.savePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/add',data);
	    }
	    this.getAllPricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/getAll',data);
	    }
	    this.deletePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/delete',data);
	    }
	    this.getDetailedPricelist = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelist',data);
	    }
	    this.getPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getPricelistByCourseId',data);
	    }
	    this.getDetailedPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelistByCourseId',data);
	    }


	    // Bundle
	    this.saveBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/add',data);
	    }
	    this.getAllBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/getAll',data);
	    }
	    this.deleteBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/delete',data);
	    }

	    // Email Templates
	    this.addEmailTemplates  = function(data){
	        return $http.post(BASE_API + '/emailTemplates/addEmailTemplates',data);
	    }

	    this.getAllEmailTemplates = function(data){
	        return $http.post(BASE_API + '/emailTemplates/getAllEmailTemplates',data);
	    }

	    this.deleteEmailTemplate = function(data){
	        return $http.post(BASE_API + '/emailTemplates/deleteEmailTemplate',data);
	    }

	    // UserEmail Group
	    this.addUserEmailGroup  = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/addUserEmailGroup',data);
	    }

	    this.getAllUserEmailGroups = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/getAllUserEmailGroups',data);
	    }

	    this.deleteUserEmailGroup = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/deleteUserEmailGroup',data);
	    }

	    // Send Email Templates
	    this.addSendEmailTemplate  = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/addSendEmailTemplate',data);
	    }

	    this.getAllSendEmailTemplates = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/getAllSendEmailTemplates',data);
	    }

	    this.deleteSendEmailTemplate = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/deleteSendEmailTemplate',data);
	    }

	    this.sendEmailTemplatesToUsers = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendEmailTemplatesToUsers',data);
	    }

	    this.sendNotificationOfEmail = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendNotificationOfEmail',data);
	    }

	    // Send Notification
	    this.addSendNotification  = function(data){
	        return $http.post(BASE_API + '/sendNotification/addSendNotification',data);
	    }

	    this.getAllSendNotifications = function(data){
	        return $http.post(BASE_API + '/sendNotification/getAllSendNotifications',data);
	    }

	    this.deleteSendNotification = function(data){
	        return $http.post(BASE_API + '/sendNotification/deleteSendNotification',data);
	    }

	    this.getSendNotificationByUserId = function(data){
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationByUserId',data);
	    }

	    this.markSendNotificationAsRead = function(data){
	        return $http.post(BASE_API + '/sendNotification/markSendNotificationAsRead',data);
	    }

	    this.getSendNotificationCount = function(data) {
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationCount',data);
	    }


	    // Roles
	    this.addRole  = function(data){
	        return $http.post(BASE_API + '/adminRoles/addRole',data);
	    }

	    this.getAllRoles = function(data){
	        return $http.post(BASE_API + '/adminRoles/getAllRoles',data);
	    }

	    this.deleteRole = function(data){
	        return $http.post(BASE_API + '/adminRoles/deleteRole',data);
	    }


	    // forms
	    this.addForm  = function(data){
	        return $http.post(BASE_API + '/adminForms/addForm',data);
	    }

	    this.getAllForms = function(data){
	        return $http.post(BASE_API + '/adminForms/getAllForms',data);
	    }

	    this.deleteForm = function(data){
	        return $http.post(BASE_API + '/adminForms/deleteForm',data);
	    }

	    // forms  help
	    this.addFormHelp  = function(data){
	        return $http.post(BASE_API + '/formsHelp/updateFormHelp',data);
	    }

	    this.getAllFormsHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getAllFormsHelp',data);
	    }

	    this.getFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getFormHelp',data);
	    }

	    this.deleteFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/deleteFormHelp',data);
	    }

	    // user roles
	    this.addUserRole  = function(data){
	        return $http.post(BASE_API + '/userRoles/addUserRole',data);
	    }

	    this.getAllUserRoles = function(data){
	        return $http.post(BASE_API + '/userRoles/getAllUserRoles',data);
	    }

	    this.deleteUserRole = function(data){
	        return $http.post(BASE_API + '/userRoles/deleteUserRole',data);
	    }

	    this.getUserAdminRoleFormAccessList = function(data) {
	        return $http.post(BASE_API + '/userRoles/getUserAdminRoleFormAccessList',data);
	    }

	    // user batch enroll
	    this.getUserEnrollAllBatch = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/getUserEnrollAllBatch',data);
	    }
	    this.addUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/addUserBatchEnroll',data);
	    }
	    this.removeUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/removeUserBatchEnroll',data);
	    }



	    this.updateBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/updateBookTopic',data);
	    }
	    this.removeBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/removeBookTopic',data);
	    }
	    this.getAllBookTopics = function(data) {
	        return $http.post(BASE_API + '/booktopic/getAllBookTopics',data);
	    }
	    this.findBookTopicsByQuery = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsByQuery',data);
	    }
	    this.findBookTopicById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicById',data);
	    }
	    this.findBookTopicsById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsById',data);
	    }


	    this.updatePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/update',data);
	    }
	    this.removePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/remove',data);
	    }
	    this.getAllPreviousPapers = function(data) {
	        return $http.post(BASE_API + '/previousPapers/getAll',data);
	    }
	    this.getPreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/get',data);
	    }


	    //Save Purchased Course Based on COurse ids
	    this.purchasedCourse = function(data) {
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnCourseId',data);
	    }
	    //Save purchased courses based on bundle ids
	    this.purchasedBundle = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnBundleId',data);
	    }
	    //save purchased courses based on training ids
	    this.purchasedTraining = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnTrainingId',data);
	    }
	}


/***/ },

/***/ 344:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAA/hJREFUSA2tVltsVEUY/s/Zswvbi4HiA6QmVgshUnwwxBdLLD4YSYgCkvatLjGYrNEgL/pgUrKixkRfjPZB4+XZ1N4jGgQtxLQYMYVqWo21JUAWufRCurtnuzvnzPh/s3OW0wZrtum/mbNzZub/vrn933+I/sNau1ojqRTZ4e6mLorRR7QORddDnRgLn1BTuWqVa6FKaxdFvm4jH00HumhXgWjfw1X0VG20pbEqWhezyKJscU5k/eHpK64YchR9299Gv2D8nhQ5Z1PkoR7YUhLF3m9xSZF8ZXBH04I/ceLRjYn9zdsORR64r5E2ra+nqB3TvkIKmltMUzozRcNT/XJs9ouT61X98c8Opi8RcGAW15YYOkxnoptefvP7J/Ojt35QbnFBsQkuPhe5rKBNuMWMGrt9VnWcfrrwQje9anDLeHd5zP4n+ujtzgtJdSt7jf01uJTSV/woFebRP/2OdvBocjGTS6tPRl9TiX76QAMHK8LLniFy8N/eSy91/ppUd/K34ej5BhwvKxlIMZbNzxTm5acXj6lELx0FJs6XH6Ubkex9ZCe2yKxAE6wEfK++gGgmd10dP7PXO/LNll0gopTZpvY+6rnIZ8AmzBbcC+d/2wyR+H3mJ3V4gL7TJHg8/xU9/v65hG8Ombcf57s6w9axyUXPlR8OJ1VbDzXrYCtGaN/ubYfseLTW436LrTyBSiuIISay1kXifnPjQRKKDmiSB+PUUl/TCLwlEV4pQTAeRMDaXNVADdUPPWNDHmqclq2b4vVmzOpXEZCYf3tjbDPVOE3b7fEJsmtidVETyWvDYFCcSJSqoxtiZntC2KHqspmt+tXeWkcqJ+YEtEjbMrVZNTI7etKjnLcg7L+PUiErzl+G2LGtDUUJRd4p3qCc+GNSb9dltzCUzkyDRK4FjyrNVd50r9DV3OQpTRIlOjky1SfzIutYlq3MIJBWbPC1GaPoL0ZGpgfIkjRwV1b6aRByzSaMNKBesQWyMj47ol4coNOYpT2+o1XfJySc7kvvFmfd6w7PBEJe8SrgA9/5/E2ne+w9ZVvxjjKIlmN+a++hY8gHmcV5CJCPWRktWnFF0DqzAuif/+Vvb6jDvfQ6CALsElmQFTnhIB9ArtmQEXViKicuTcvUJmmFtlbMuTcUCDhpdWrQEmYo8kJZDAkH+QByDTU1ZDorgTRkaBMFLy8n+AxO/PisDFYAEr5BmiDEgtZSIz8VEo7n//POY/cn9z7BarqluoE2sBY55kPC4+BFHOCanp8epNGZj8/YKt7x+XP5n4GDUGEx1xGzlAT0bOFPIq7v9mzaDzWtZbGrYi3CGJcjOSv+nLya++sUril/Qp0L+eLWlAP7X+i6v7i1t2fmAAAAAElFTkSuQmCC"

/***/ },

/***/ 345:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAAkxJREFUSA21Vj1PG0EQfWdsAwKkRKSkDUJOB+mi5C/QJFF+QByRJj3d0UT5ByALJHqjpKeElGnjjlRpE0EEgtxhX96b2cWmwHKwb0+zn7Pv7czuzl6CmLawjgJv2XxMqQRJWCb8PBVWU6sIPYNFjw1Jl3JC2UOKzyzD9BQfMYdNgz7HX/Zfa5BQDpZYzalU9zGRS8MXYp1Wr2IeddO6wCcSbSbMXrKzjXP8pt571HCMnMo1quWm6tOH5Rn156hQcJkJrcnxjK1tLOARzvCmysZGgGpG84bhjTh2QKxr0n0hbVMky7jAH5ZHBpBiBg2upWOmj4gZ1Bpcboc+SHFFpx+z94woKyKZpmQUbTaM4LVtnjX/O3sVZiwQLyNVgWkB1yjxZOBeFgyu5EnwQGaboENSF8lUIHFVmTxOivP9fGnxUyJxN40DfPdcLTiJJGIsI9nd6lvh5k2ayFwfSRKeBL/Rk6W5ZUkRAsFkKQJa2ZYYTSQpy5Jbe6I7X8ae3FjSI3xpBGKRu3q842WRCLdQ/NdLFvdm/NgVo7e7XyTdCq3IA4kTfR8zdsX5dfOOMHOFeoV5PZiyCHgO9QExZFtjxKxNPc1vEyszPEX4KwH+wCxe4BJrrB/ig73xoOL9kgg8rRL3IXG/Vrnt+7RCJNvY4t/KDL6x3g//s3TfryF8ixy7vDk4FQJ3+dI+Ze8OccH6vp+qFC08QJPKoB0/mfvfCitMo7wvg6ezyrd2iWTAKXaRotkfTPGO3ZIGxVMcHXbE+79IcZbKDqVFgpYa/wBYoZjk+M6h+gAAAABJRU5ErkJggg=="

/***/ },

/***/ 374:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(120);
	angular.module(window.moduleName)
	    .component('questionAnalyticsPage', {
	        require: {
	            parent : '^adminPage'
	        },
	        template: ['$element', '$attrs',
	            function($element, $attrs) {
	                return __webpack_require__(375);
	            }
	        ],
	        controller: QuestionAnalyticsController,
	        controllerAs: 'vm'
	    })

	QuestionAnalyticsController.$inject = ['admin'];

	function QuestionAnalyticsController(admin){

	    var vm = this;




	    vm.selectedSubjects = [];
	    vm.selectedAuthors = [];
	    vm.selectedPublications = [];
	    vm.selectedBooks = [];
	    vm.selectedExams = [];
	    vm.selectedTopics = [];
	    vm.selectedTags = [];

	    vm.onFilterChange = onFilterChange;


	    vm.$onInit = function(){
	            vm.parent.hideSidebar = true;
	            admin.getAllSubject()
	                .then(function(d){
	                    vm.subjects = d.data;
	                });
	            admin.getAllAuthor()
	                .then(function(d){
	                    vm.authors = d.data;
	                });
	            admin.getAllPublication()
	                .then(function(d){
	                    vm.publications = d.data;
	                });
	            admin.getAllBook()
	                .then(function(d){
	                    vm.books = d.data;
	                });
	            admin.getAllExam()
	                .then(function(d){
	                    vm.exams = d.data;
	                });
	            admin.getAllTopic()
	                .then(function(d){
	                    vm.topics = d.data;
	                });
	            admin.getAllTag()
	            .then(function(d){
	                vm.tags = d.data;
	            });
	            // updateQuestionList();
	        }

	        function onFilterChange(item,model) {
	            if(vm.selectedSubjects.length > 0){
	                admin.getAllTopic({subjectId : vm.selectedSubjects[0]})
	                .then(function(d){
	                    vm.topics = d.data;
	                }); 
	            }
	            var query = {
	                subjects    : vm.selectedSubjects,
	                authors : vm.selectedAuthors,
	                publications    : vm.selectedPublications,
	                books   : vm.selectedBooks,
	                exams   : vm.selectedExams,
	                topics  : vm.selectedTopics,
	                tags    : vm.selectedTags
	            }
	            vm.isLoading = true;
	            admin.findPracticeQuestionsByQuery(query)
	            .then(function(d){
	                if(d.data.length > 0){
	                    vm.questions = d.data;
	                    vm.questions.sort(function(a,b){
	                        return a.questionNumber - b.questionNumber;
	                    });
	                }
	                vm.isLoading = false;
	            })
	            .catch(function(err){
	                console.log(err);
	            });
	        }
	}





/***/ },

/***/ 375:
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div class=\"question-analytics-wrapper\">\n\t<div class=\"sidebar\">\n\t\t<div class=\"form-group\">\n\t\t\t<label class=\"\" for=\"subject\">Subject</label>\n\t\t\t<div>\n\t\t\t\t<ui-select multiple ng-model=\"vm.selectedSubjects\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t<ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t<span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t</ui-select-match>\n\t\t\t\t<ui-select-choices repeat=\"item.subjectId as item in (vm.subjects | filter: $select.search) track by $index\">\n\t\t\t\t<span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t</ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label  for=\"author\">Author</label>\n\t\t\t<div>\n\t\t\t\t<ui-select multiple ng-model=\"vm.selectedAuthors\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t<ui-select-match placeholder=\"Select Author...\">\n\t\t\t\t<span ng-bind=\"$item.authorName\"></span>\n\t\t\t\t</ui-select-match>\n\t\t\t\t<ui-select-choices repeat=\"item.authorId as item in (vm.authors | filter: $select.search) track by $index\">\n\t\t\t\t<span ng-bind=\"item.authorName\"></span>\n\t\t\t\t</ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"publication\">Publication</label>\n\t\t\t<div>\n\t\t\t\t<ui-select multiple ng-model=\"vm.selectedPublications\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t<ui-select-match placeholder=\"Select Publication...\">\n\t\t\t\t<span ng-bind=\"$item.publicationName\"></span>\n\t\t\t\t</ui-select-match>\n\t\t\t\t<ui-select-choices repeat=\"item.publicationId as item in (vm.publications | filter: $select.search) track by $index\">\n\t\t\t\t<span ng-bind=\"item.publicationName\"></span>\n\t\t\t\t</ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"book\">Book</label>\n\t\t\t<div>\n\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBooks\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t<ui-select-match placeholder=\"Select Book...\">\n\t\t\t\t<span ng-bind=\"$item.bookName\"></span>\n\t\t\t\t</ui-select-match>\n\t\t\t\t<ui-select-choices repeat=\"item.bookId as item in (vm.books | filter: $select.search) track by $index\">\n\t\t\t\t<span ng-bind=\"item.bookName\"></span>\n\t\t\t\t</ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"exam\">Exam</label>\n\t\t\t<div>\n\t\t\t\t<ui-select multiple ng-model=\"vm.selectedExams\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t<ui-select-match placeholder=\"Select Exam...\">\n\t\t\t\t<span ng-bind=\"$item.examName\"></span>\n\t\t\t\t</ui-select-match>\n\t\t\t\t<ui-select-choices repeat=\"item.examId as item in (vm.exams | filter: $select.search) track by $index\">\n\t\t\t\t<span ng-bind=\"item.examName\"></span>\n\t\t\t\t</ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"topic\">Topic</label>\n\t\t\t<div>\n\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTopics\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t<ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t<span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t</ui-select-match>\n\t\t\t\t<ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t<span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t</ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"tag\">Tag</label>\n\t\t\t<div>\n\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTags\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t<ui-select-match placeholder=\"Select Tag...\">\n\t\t\t\t<span ng-bind=\"$item.tagName\"></span>\n\t\t\t\t</ui-select-match>\n\t\t\t\t<ui-select-choices repeat=\"item.tagId as item in (vm.tags | filter: $select.search) track by $index\">\n\t\t\t\t<span ng-bind=\"item.tagName\"></span>\n\t\t\t\t</ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"tag\">Question</label>\n\t\t\t<div>\n\t\t\t\t<input type=\"text\" ng-model=\"vm.questionSearch\"></input>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"main-container\">\n\t\t<div class=\"question-stats\" ng-if=\"vm.questions.length > 0\">\n\t\t\tTotal Questions : {{vm.questions.length}}\n\t\t</div>\n\t\t<div class=\"loader\" ng-if=\"vm.isLoading\">\n\t\t\t<i class=\"fa fa-spin fa-circl-o-notch\"></i>\n\t\t</div>\n\t\t<div class=\"qa-wrapper vertical has-big-sidebar\" ng-if=\"vm.questions.length > 0\">\n\t\t\t<div class=\"question-wrapper\" ng-repeat=\"q in vm.questions | filter:vm.questionSearch track by $index\">\n\t\t\t\t<div class=\"q\">\n\t\t\t\t\t<div class=\"index\">\n\t\t\t\t\t\t<span>{{$index + 1}} | {{q.questionId}} </span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"question\" ng-bind-html=\"q.question\"></div>\n\t\t\t\t\t<div class=\"questionOneLine\" ng-bind-html=\"q.questionOneLine\"></div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"options-wrapper\">\n\t\t\t\t\t<div class=\"o\">\n\t\t\t\t\t\t<div class=\"ng-option\" ng-repeat=\"option in q['options'] track by $index\" >\n\t\t\t\t\t\t\t<div id=\"{{option.id}}\" \n\t\t\t\t\t\t\t\tng-if=\"q['ans'].length <= 1\" \n\t\t\t\t\t\t\t\tclass=\"option\">\n\t\t\t\t\t\t\t\t<div class=\"useroption\" style=\"display: inline-block;\">\n\t\t\t\t\t\t\t\t\t<img style=\"width: 29px;height: 29px\" src=\"" + __webpack_require__(344) + "\" alt=\"\">\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<pre style=\"display: inline-block;\" ng-if=\"option.text\" ><code class=\"ruleslanguage\" ng-bind-html=\"option.text\"></code></pre>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div id=\"{{option.id}}\" \n\t\t\t\t\t\t\t\tng-if=\"q['ans'].length > 1\" \n\t\t\t\t\t\t\t\tclass=\"option\">\n\t\t\t\t\t\t\t\t<span class=\"useroption\" style=\"cursor:pointer\">\n\t\t\t\t\t\t\t\t\t<img src=\"" + __webpack_require__(345) + "\" alt=\"\">\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<pre ng-if=\"option.text\" ><code class=\"ruleslanguage\" ng-if=\"option.text\" ng-bind-html=\"option.text\"></code></pre>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>";

/***/ }

});