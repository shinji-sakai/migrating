webpackJsonp([33],{

/***/ 402:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('sessionsPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(403),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
	function Controller(admin,$filter,$scope,$state,EditorConfig){

		var vm = this;
		vm.form = {};

		vm.changeCourse = changeCourse;
		vm.resetForm = resetForm;
		vm.addSession = addSession;
		vm.editSession = editSession;
		vm.deleteSession =  deleteSession;

		vm.$onInit = function(){
			getAllCoursesFromMaster();
		}

		function getAllCoursesFromMaster() {
			admin.coursemaster()
				.then(function(d){
					vm.coursesMaster = d;
				})
				.catch(function(err){
					console.error(err);
				});
		}

		function getSessionsForCourse(){
			admin.getSessionsByCourseId({crs_id : vm.form.crs_id})
				.then(function(res){
					var  sessionArr  = res.data;
					if(sessionArr.length <= 0){
						vm.form.sessionNumber = 1;
					}else{
						sessionArr.sort(function(a,b){
							return a.sessionNumber - b.sessionNumber;
						})
						vm.form.sessionNumber = sessionArr[sessionArr.length - 1].sessionNumber + 1;
					}
					vm.allSessions = sessionArr;
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function addSession(){
			vm.form.sessionWeight = parseFloat(vm.form.sessionWeight);
			admin.addSession(vm.form)
				.then(function(res){
					getSessionsForCourse();
					resetForm();
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function editSession(sessionNumber){
			var session = $filter('filter')(vm.allSessions,{sessionNumber : sessionNumber})[0];
			vm.form = session;
		}

		function deleteSession(sessionNumber){
			admin.deleteSession({sessionNumber : sessionNumber,crs_id:vm.form.crs_id})
				.then(function(res){
					getSessionsForCourse();
					resetForm();
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function changeCourse(){
			getSessionsForCourse();
		}

		function resetForm(){
			var crs = vm.form.crs_id;
			vm.form = {};
			vm.form.crs_id = crs;
			vm.form.sessionNumber = vm.allSessions[vm.allSessions.length - 1].sessionNumber + 1;
		}

	}

/***/ },

/***/ 403:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<fieldset>\n\t\t\t\t\t<legend>Add Session</legend>\n\t\t\t\t\t<form class=\"form form-horizontal\" name=\"form\">\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Training Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.crs_id\" id=\"course\" ng-change=\"vm.changeCourse()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"course.courseId as course.courseName for course in vm.coursesMaster \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Training</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"session-number\">Session Number</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"session-number\" class=\"form-control\" placeholder=\"Session Number\" readonly ng-model=\"vm.form.sessionNumber\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"sessionname\">Session Name</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" ng-disabled=\"!vm.form.crs_id\" id=\"sessionname\" class=\"form-control\" placeholder=\"Session Name\" ng-model=\"vm.form.sessionName\" required/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"sessiondesc\">Session Desc</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"sessiondesc\" ng-disabled=\"!vm.form.crs_id\" class=\"form-control\" spellcheck=\"true\" placeholder=\"Session Description\" ng-model=\"vm.form.sessionDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"sessionweight\">Session Weight</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"sessionweight\" ng-disabled=\"!vm.form.crs_id\" class=\"form-control\" spellcheck=\"true\" placeholder=\"Session Weight to order sessions\" ng-model=\"vm.form.sessionWeight\" required></input>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\"  ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.crs_id || !vm.form.sessionName\" ng-click=\"vm.addSession()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</form>\n\t\t\t\t</fieldset>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allSessions\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allSessions\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"session in vm.allSessions | orderBy:'sessionWeight'\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteSession(session.sessionNumber)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editSession(session.sessionNumber)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{session.sessionNumber}} - {{session.sessionName}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in session\">\n\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});