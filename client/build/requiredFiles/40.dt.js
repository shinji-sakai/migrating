webpackJsonp([40],{

/***/ 424:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('allLinksCategoryPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(425),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['$log','admin','$filter','$scope','$state'];
	function Controller($log,admin,$filter,$scope,$state){
		var vm = this;
	    vm.no_of_item_already_fetch = 0;
		vm.saveCategory = saveCategory;
		vm.deleteCategory = deleteCategory;
		vm.editCategory = editCategory;

		vm.$onInit = function(){
			//getAllCategories();
		}
	    	vm.fetchCategories = function(){
				vm.isCategoriesLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'allLinksCategory'
				})
				.then(function(res){
					$log.debug(res)
					vm.allCategories= res.data;
					vm.no_of_item_already_fetch = 5;
					vm.last_update_dt = vm.allCategories[vm.allCategories.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isCategoriesLoading = false;
				})
			}

			vm.fetchMoreCategories = function(){
				vm.isCategoriesLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'allLinksCategory',
					  update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							$log.log('v.cat_id'+v.cat_id);
							var item = $filter('filter')(vm.allCategories, {cat_id: v.cat_id})[0];
							if(!item){
								vm.allCategories.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						vm.last_update_dt = vm.allCategories[vm.allCategories.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isCategoriesLoading = false;
				})
			}

		function getAllCategories(){
			admin.getAllLinksCategories()
				.then(function(res){
					vm.allCategories = res.data
				})
		}

		function saveCategory(){
				if(!vm.formIsInEditMode){
	                vm.form.create_dt = new Date();
	                vm.form.update_dt = new Date();
	            }else{
	                vm.form.update_dt = new Date();
	            }
			admin.addAllLinksCategory(vm.form)
				.then(function(res){
					debugger;
					//getAllCategories();
				  vm.fetchCategories();
						
					vm.form = {};
					vm.formIsInEditMode=false;
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function deleteCategory(id){
			admin.deleteAllLinksCategory({cat_id : id})
				.then(function(res){
					//getAllCategories();
					var item = $filter('filter')(vm.allCategories || [], {cat_id: id})[0];								
						 if (vm.allCategories.indexOf(item) > -1) {
	                        var pos = vm.allCategories.indexOf(item);
	                       vm.allCategories.splice(pos, 1);
	                    }
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function editCategory(id){
			var cat = $filter('filter')(vm.allCategories,{cat_id : id})[0];
			vm.form = cat;
			vm.formIsInEditMode=true;
		}
	}

/***/ },

/***/ 425:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" ng-submit=\"vm.saveCategory()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>All Links Category</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"cateogry name\">Category<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Category\" ng-model=\"vm.form.cat_nm\" required=\"\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.form= {}\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" >Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchCategories()\" ng-if=\"!vm.allCategories || vm.allCategories.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isCategoriesLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isCategoriesLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Category</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.allCategories\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allCategories\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"cat in vm.allCategories\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteCategory(cat.cat_id)\"><i class=\"fa fa-times\"></i></span>\n\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editCategory(cat.cat_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t{{cat.cat_nm}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row skill-info-row\" ng-repeat=\"(key,value) in cat\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreCategories()\" ng-if=\"(vm.allCategories==[] || vm.allCategories.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isCategoriesLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isCategoriesLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});