webpackJsonp([57],{

/***/ 471:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(472)
	angular.module(window.moduleName)
	    .component('boardCompetitiveCoursesPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(474),
	        controller: AdminBoardCompetitiveCoursesController,
	        controllerAs: 'vm'
	    })

	AdminBoardCompetitiveCoursesController.$inject = ['admin', '$filter', '$scope', '$state','alertify'];

	function AdminBoardCompetitiveCoursesController(admin, $filter, $scope, $state , alertify) {

	    var vm = this;

	    vm.selectedCourse = [];
	    vm.form = {};
	    vm.form.children = [];

	    vm.$onInit = function() {
	        $scope.$watch(function() {
	            return vm.parent.coursemaster;
	        }, function(v) {
	        	vm.actualAllCourses = v;
	            vm.allCourses = v;
	        });

	        vm.getCourses();
	    }

	    vm.getCourses = function(){
	    	admin.getBoardCompetitiveCourse()
	        	.then(function(res){
	        		vm.boardCompetitiveCourses = res.data;
	        	})
	        	.catch(function(err){
	        		console.log(err)
	        	})
	    }

	    vm.addChildToArray = function(){
	    	if(!vm.childId || !vm.childName || !vm.childWeight){
	    		alertify.error("Child id, Child name and Child Weight required")
	    		return;
	    	}
	        vm.childId = vm.childId.replace(/ /g,"");
	    	var arr = $filter('filter')(vm.form.children,{childId : vm.childId});
	    	if(arr && arr.length > 0){
				alertify.error("Child id exists")
	    	}else{
	    		vm.form.children.push({
	    			childId : vm.childId,
	    			childName : vm.childName,
	                childWeight : vm.childWeight
	    		})
	    		vm.childId = "";
	    		vm.childName = "";
	            vm.childWeight = "";
	    		//every time update the db
	    		vm.updateCourse();
	    	}
	    }

	    vm.removeChildFromArray = function(index){
	        var result = confirm("Are you sure you want to remove child?");
	        if (!result) {
	            //Declined
	            return;
	        }
	    	vm.form.children.splice(index,1)
	    	vm.updateCourse();
	    }

	    vm.editChildInArray = function(index){
	    	vm.editingChild = index;
	    }

	    vm.saveChildInArray = function(index){
	    	var obj = vm.form.children[index];
	    	if(!obj.childId || !obj.childName || !obj.childWeight){
	    		alertify.error("Child id, Child name and child weight required")
	    		return;
	    	}
	    	vm.updateCourse();
	    	vm.editingChild = undefined;
	    }

	    vm.changeCourseType = function(){
			if(vm.form.courseType){
				vm.allCourses = vm.actualAllCourses
				if(vm.form.courseType != 'both'){
					vm.allCourses = $filter("filter")(vm.allCourses,{courseType : vm.form.courseType});
				}else{
					vm.allCourses = vm.actualAllCourses
				}
			}
	    }

	    vm.onCourseChange = function(){
			if(vm.selectedCourse.length > 0){
				admin.getBoardCompetitiveCourse({
					course_id : vm.selectedCourse[0]
				})
				.then(function(res){
					if(res.data && res.data[0] && res.data[0].course_id){
						vm.form = res.data[0]
					}
				})
				.catch(function(err){
					console.log(err);
				})
			}else{
				vm.form.children = [];
			}
	    }

	    vm.updateCourse = function(){
	    	vm.form.course_id = vm.selectedCourse[0]
	    	delete vm.form._id
	    	vm.isFormSaving = true;
	    	admin.updateBoardCompetitiveCourse(vm.form)
	    		.then(function(res){
					vm.isFormSaving = false;
					alertify.success("Data updated")
					vm.getCourses();
	    		})
	    		.catch(function(err){
	    			console.log(err);
	    			vm.isFormSaving = false;
	    			alertify.success("Error in updating data")
	    		})
	    }

	    vm.editCourse = function(id){
			var arr = $filter('filter')(vm.boardCompetitiveCourses,{course_id  : id});
			if(arr && arr[0]){
				vm.form = arr[0];
				vm.selectedCourse = [vm.form.course_id]
			}
	    }

	    vm.deleteCourse = function(id){
	    	var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	    	admin.deleteBoardCompetitiveCourse({course_id : id})
	    		.then(function(res){
					alertify.success("Course removed successfully.")
					vm.getCourses();
	    		})
	    		.catch(function(err){
	    			alertify.success("Error in removing data")
	    		})
	    }
		
		vm.resetForm = function(){
			var result = confirm("Are you sure you want to reset data?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        vm.form = {};
	        vm.selectedCourse = [];
	        vm.form.children = [];
		}
	}

/***/ },

/***/ 472:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(473);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_styles.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_styles.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 473:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.course-child-wrapper {\n  background: #f4f4f4;\n  padding: 20px;\n  margin-bottom: 40px; }\n  .course-child-wrapper .title {\n    font-size: 18px;\n    font-family: 'nunito',sans-serif;\n    margin-bottom: 10px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: justify;\n    -moz-box-pack: justify;\n    box-pack: justify;\n    -webkit-justify-content: space-between;\n    -moz-justify-content: space-between;\n    -ms-justify-content: space-between;\n    -o-justify-content: space-between;\n    justify-content: space-between;\n    -ms-flex-pack: justify;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n  .course-child-wrapper .tf-wrapper {\n    margin-bottom: 10px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .course-child-wrapper .tf-wrapper input {\n      background: white;\n      margin-right: 10px;\n      padding: 0px 10px;\n      border-radius: 5px; }\n    .course-child-wrapper .tf-wrapper label {\n      margin-bottom: 0px;\n      -webkit-box-flex: 1;\n      -moz-box-flex: 1;\n      box-flex: 1;\n      -webkit-flex: 1;\n      -moz-flex: 1;\n      -ms-flex: 1;\n      flex: 1; }\n  .course-child-wrapper table input {\n    padding: 0px 10px !important;\n    border-radius: 5px !important;\n    background: white !important; }\n  .course-child-wrapper table input[readonly] {\n    background: #f4f4f4 !important; }\n  .course-child-wrapper .child {\n    font-size: 15px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n  .course-child-wrapper .inline-remove-btn {\n    text-decoration: underline;\n    color: red;\n    font-size: 16px;\n    cursor: pointer; }\n  .course-child-wrapper .inline-edit-btn {\n    text-decoration: underline;\n    color: green;\n    font-size: 16px;\n    cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 474:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add New Board/Competitive Course</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"\">Course Type</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.courseType\" class=\"form-control\" ng-change=\"vm.changeCourseType()\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Course Type</option>\n\t\t\t\t\t\t\t\t\t<option value=\"board\">Board</option>\n\t\t\t\t\t\t\t\t\t<option value=\"competitive\">Competitive</option>\n\t\t\t\t\t\t\t\t\t<option value=\"both\">Both</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"\">Select Course</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple limit=\"1\" ng-model=\"vm.selectedCourse\" on-select=\"vm.onCourseChange($item, $model)\" on-remove=\"vm.onCourseChange($item, $model)\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Course...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.courseId as item in (vm.allCourses | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"course-child-wrapper\" ng-if=\"vm.form.courseType && vm.selectedCourse.length > 0\">\n\t\t\t\t\t\t\t<div class=\"title\">\n\t\t\t\t\t\t\t\t<span>Add Child in Course</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"tf-wrapper\" style=\"margin-bottom: 5px;\">\n\t\t\t\t\t\t\t\t<label>Child ID</label>\n\t\t\t\t\t\t\t\t<label>Child Name</label>\n\t\t\t\t\t\t\t\t<label>Child Weight</label>\n\t\t\t\t\t\t\t\t<span style=\"width: 100px;\">\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"tf-wrapper\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" ng-model=\"vm.childId\" name=\"childid\" placeholder=\"Enter Child Id\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" ng-model=\"vm.childName\" name=\"childname\" placeholder=\"Enter Child Name\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" ng-model=\"vm.childWeight\" name=\"childweight\" placeholder=\"Enter Child Weight\">\n\t\t\t\t\t\t\t\t<span class=\"ew-rounded-btn\" ng-click=\"vm.addChildToArray()\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-plus right\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"text\">Add</span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"children\">\n\t\t\t\t\t\t\t\t<table class=\"table\" ng-if=\"vm.form.children && vm.form.children.length > 0\">\n\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t<th>Number</th>\n\t\t\t\t\t\t\t\t\t\t<th>Child Id</th>\n\t\t\t\t\t\t\t\t\t\t<th>Child Name</th>\n\t\t\t\t\t\t\t\t\t\t<th>Child Weight</th>\n\t\t\t\t\t\t\t\t\t\t<th>Edit</th>\n\t\t\t\t\t\t\t\t\t\t<th>Remove</th>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t<tr ng-repeat=\"child in vm.form.children track by $index\">\n\t\t\t\t\t\t\t\t\t\t<td>{{$index + 1}}</td>\n\t\t\t\t\t\t\t\t\t\t<td><input type=\"text\" class=\"form-control\" name=\"\" ng-readonly=\"vm.editingChild != $index\" ng-model=\"child.childId\" placeholder=\"Enter child id\" /></td>\n\t\t\t\t\t\t\t\t\t\t<td><input type=\"text\" class=\"form-control\" name=\"\" ng-readonly=\"vm.editingChild != $index\" ng-model=\"child.childName\" placeholder=\"Enter child id\" /></td>\n\t\t\t\t\t\t\t\t\t\t<td><input type=\"text\" class=\"form-control\" name=\"\" ng-readonly=\"vm.editingChild != $index\" ng-model=\"child.childWeight\" placeholder=\"Enter child weight\" /></td>\n\t\t\t\t\t\t\t\t\t\t<td>\n\n\t\t\t\t\t\t\t\t\t\t\t<!-- <span style=\"display: inline-flex;\" class=\"ew-rounded-btn\" ng-if=\"vm.editingChild != $index\" ng-click=\"vm.editChildInArray($index)\">\n\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil right\"></i>\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text\">Edit</span>\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<span style=\"display: inline-flex;\" class=\"ew-rounded-btn green\" ng-if=\"vm.editingChild === $index\" ng-click=\"vm.saveChildInArray($index)\">\n\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-save right\"></i>\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text\">Save</span>\n\t\t\t\t\t\t\t\t\t\t\t</span> -->\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"inline-edit-btn\" ng-if=\"vm.editingChild != $index\" ng-click=\"vm.editChildInArray($index)\">Edit</span>\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"inline-edit-btn\" ng-if=\"vm.editingChild === $index\" ng-click=\"vm.saveChildInArray($index)\">Save</span>\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"inline-remove-btn\" ng-click=\"vm.removeChildFromArray($index)\">Remove</span>\n\t\t\t\t\t\t\t\t\t\t\t<!-- <span style=\"display: inline-flex;\" class=\"ew-rounded-btn red\" ng-click=\"vm.removeChildFromArray($index)\">\n\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times right\"></i>\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text\">Remove</span>\n\t\t\t\t\t\t\t\t\t\t\t</span> -->\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.form.courseType && vm.selectedCourse.length > 0\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 col-xs-offset-8\">\n\t\t\t\t\t\t\t\t<div class=\"ew-rounded-btn red\" ng-click=\"vm.resetForm()\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-trash right\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"text\">Reset</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\" >\n\t\t\t\t\t\t\t\t<div class=\"ew-rounded-btn green\" \n\t\t\t\t\t\t\t\tng-click=\"vm.updateCourse()\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-save right\" ng-if=\"!vm.isFormSaving\"></i>\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isFormSaving\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"text\">Save</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.boardCompetitiveCourses\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"course in vm.boardCompetitiveCourses\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteCourse(course.course_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editCourse(course.course_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{course.course_id}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in course\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});