webpackJsonp([56],{

/***/ 467:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(468)
	angular.module(window.moduleName)
	    .component('topicGroupPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(470),
	        controller: AdminTopicGroupController,
	        controllerAs: 'vm'
	    });

	AdminTopicGroupController.$inject = ['$log','admin', 'pageLoader', '$filter', '$scope'];

	function AdminTopicGroupController($log,admin, pageLoader, $filter, $scope) {
	    var vm = this;

	    vm.topics = [];
	    vm.groups = [];
	    vm.selectedSubject = [];
	    vm.selectedTopic = [];
	    vm.no_of_item_already_fetch = 0;
	    vm.updateGroup = updateGroup;
	    vm.resetFormDetail = resetFormDetail;
	    vm.removeGroup = removeGroup;
	    vm.editGroup = editGroup;
	    vm.changeSubject = changeSubject;
	    vm.form = vm.form || {};


	    vm.$onInit = function() {
	        admin.getAllSubject()
	            .then(function(d) {
	                vm.subjects = d.data;
	            });
	        $scope.$watch(function(){
	            return vm.form.groupNumber;
	        }, function(newValue, oldValue, scope) {
	            if(newValue && vm.selectedTopic[0]){
	                vm.form = vm.form || {};
	                vm.form.groupId = vm.selectedTopic[0] + "-" + vm.form.groupNumber
	            }
	        });
	        // updateTopicsList();
	    };

	    function updateTopicsList() {
	        pageLoader.show();
	        admin.getAllTopic({
	                subjectId: vm.selectedSubject[0]
	            })
	            .then(function(data) {
	                if (data.data.length > 0) {
	                    vm.topics = data.data;
	                    vm.topics.sort(function(a, b) {
	                        return a.topicNumber - b.topicNumber;
	                    });
	                } else {
	                    vm.topics = [];
	                }
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function updateGroups() {
	        if(!vm.selectedTopic[0]){
	            vm.groups = [];
	            return;
	        }
	        pageLoader.show();
	        admin.getAllTopicGroups({
	                topicId : vm.selectedTopic[0]
	            })
	            .then(function(res) {
	                if (res.data.length > 0) {
	                    vm.groups = res.data;
	                    vm.groups.sort(function(a, b) {
	                        return a.topicNumber - b.topicNumber;
	                    });
	                    vm.form = vm.form = {};
	                    vm.form.groupNumber = vm.groups[vm.groups.length - 1]["groupNumber"] + 1;
	                } else {
	                    vm.groups = [];
	                    vm.form = vm.form = {};
	                    vm.form.groupNumber = 1;
	                }
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function updateGroup() {
	        var data = {
	            groupId: vm.form.groupId,
	            groupNumber : vm.form.groupNumber,
	            groupName: vm.form.groupName,
	            groupDesc: vm.form.groupDesc,
	            subjectId : vm.selectedSubject[0],
	            topicId : vm.selectedTopic[0]
	        }
	        if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }
	        pageLoader.show();
	        admin.updateTopicGroup(data)
	            .then(function(res) {
	                // var item = $filter('filter')(vm.groups || [], {groupId: vm.form.groupId})[0];
					// 	if(item){
					// 		    item.groupId= vm.form.groupId || vm.selectedTopic[0] + "-" + vm.form.groupNumber,
	                //             item.groupNumber = vm.form.groupNumber,
	                //             item.groupName= vm.form.groupName,
	                //             item.groupDesc= vm.form.groupDesc,
	                //             item.subjectId = vm.selectedSubject[0],
	                //             item.topicId = vm.selectedTopic[0]
	                //     }else{
					// 		vm.groups = [data].concat(vm.groups || []);
					// 	}
	                vm.fetchTopicGrops();
	                vm.submitted = true;
	                vm.form = {};
	                 vm.formIsInEditMode = false;
	                //updateGroups();
	            })
	            .catch(function() {
	                vm.submitted = true;
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function resetFormDetail() {
	        vm.submitted = false;
	        vm.form = {};
	        if (vm.groups.length > 0) {
	            vm.form.groupNumber = vm.groups[vm.groups.length - 1]['groupNumber'] + 1;
	        }
	    }

	    function removeGroup(id) {
	        var result = confirm("Want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        pageLoader.show();
	        admin.removeTopicGroup({
	                groupId: id
	            })
	            .then(function(d) {
	                var item = $filter('filter')(vm.groups || [], {groupId: id})[0];								
						 if (vm.groups.indexOf(item) > -1) {
	                        var pos = vm.groups.indexOf(item);
	                       vm.groups.splice(pos, 1);
	                    }
	                //updateGroups();
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function editGroup(id) {
	        var obj = $filter('filter')(vm.groups, {
	            groupId: id
	        })[0];
	        vm.form = obj;
	        vm.selectedSubject = [vm.form.subjectId];
	        vm.selectedTopic = [vm.form.topicId];
	         vm.formIsInEditMode = true;
	    }

	    function changeSubject() {
	        updateTopicsList();
	    }

	    vm.changeTopic = function(){
	        updateGroups();
	    }
	    vm.fetchTopicGrops = function(){
				vm.isTopicsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'topicGroup'
				})
				.then(function(res){
					$log.debug(res)
					vm.groups = res.data;
					vm.no_of_item_already_fetch = 5;
	                vm.last_update_dt = vm.groups[vm.groups.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTopicsLoading = false;
				})
			}
	        
			vm.fetchMoreTopicGroups = function(){
				vm.isTopicsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					 collection : 'topicGroup',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.groups , {groupId: v.groupId})[0];
							if(!item){
							 vm.groups .push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						vm.last_update_dt = vm.groups[vm.groups.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTopicsLoading = false;
				})
			}
	}

/***/ },

/***/ 468:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(469);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_topicGroup.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_topicGroup.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 469:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "", ""]);

	// exports


/***/ },

/***/ 470:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateGroup()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Topic</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"subject\">Subject<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple limit=\"1\" ng-model=\"vm.selectedSubject\" theme=\"bootstrap\" on-select=\"vm.changeSubject($item, $model)\" on-remove=\"vm.changeSubject($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.subjectId as item in (vm.subjects | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"Topic\">Topic<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple limit=\"1\" ng-model=\"vm.selectedTopic\" theme=\"bootstrap\" on-select=\"vm.changeTopic($item, $model)\" on-remove=\"vm.changeTopic($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"groupName\">Group Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"groupName\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.groupName\" required=\"\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"groupDesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"groupDesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.groupDesc\" ></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- <div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"groupId\">Group Id</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"groupId\" class=\"form-control\" placeholder=\"Id\" ng-model=\"vm.form.groupId\" readonly/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"groupNumber\">Group Number</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"groupNumber\" class=\"form-control\" placeholder=\"Group Number\" ng-model=\"vm.form.groupNumber\" readonly />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Group</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" ng-disabled=\"!vm.selectedTopic[0]\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchTopicGrops()\" ng-if=\"!vm.groups || vm.groups.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTopicsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTopicsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Topicgroup</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.groups || vm.groups.length <= 0\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.groups\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"group in vm.groups\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeGroup(group.groupId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editGroup(group.groupId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{group.groupName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in group\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreTopicGroups()\" ng-if=\"(vm.groups==[] || vm.groups.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTopicsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTopicsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\t\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});