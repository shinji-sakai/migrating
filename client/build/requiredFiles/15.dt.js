webpackJsonp([15],{

/***/ 359:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(360);
	__webpack_require__(57);
	angular.module(window.moduleName)
	    .component('purchasedCourses', {
	        require: {
	            parent : '^adminPage'
	        },
	        template: __webpack_require__(361),
	        controller: AdminPurchasedCoursesController,
	        controllerAs: 'vm'
	    })


	AdminPurchasedCoursesController.$inject = ['$log','admin','$filter','$scope','$state','alertify','$timeout', 'profile', 'authToken'];
	function AdminPurchasedCoursesController($log,admin,$filter,$scope,$state,alertify,$timeout, profile, authToken) {
	    var vm = this;
	    vm.selectedUsers = [];
	    vm.onUserChange = onUserChange;
	    vm.addPurchasedCourses = addPurchasedCourses;
	    vm.addPurchasedBundle = addPurchasedBundle;
	    vm.addPurchasedTraining = addPurchasedTraining;
	    vm.updateFormWithValues = updateFormWithValues;
	    vm.course = []
	    vm.userID = "";
	    vm.resetFrom = resetFrom;

	    vm.$onInit = function() {
	        getAllUsers();
	        getAllCoursesFromMaster();
	        getAllBundle();
	        $scope.$watch(function(){return vm.parent.coursemaster;},function(v){
	            vm.courselist = v;
	        });
	        vm.userID= authToken.getUserId();
	    }
	    function onUserChange(){
	        vm.form = vm.form || {};
	        vm.form.usr_id = vm.selectedUsers[0] || "";
	    }

	    function getAllUsers(){
	        profile.getAllUsersShortDetails()
	            .then(function(d){
	                vm.allUsers = d.data;
	            })
	            .catch(function(err){
	                console.log(err);
	            })
	    }

	    function getAllCoursesFromMaster() {
	        admin.coursemaster()
	            .then(function(d){
	                vm.coursesMaster = d;
	                vm.coursesMaster = vm.coursesMaster.filter(function(v,i){
	                    return v.isLiveClass
	                })
	            })
	            .catch(function(err){
	                console.error(err);
	            });
	    }

	    function getAllBundle() {
	        admin.getAllBundle()
	            .then(function(res){
	                vm.allBundle = res.data;
	            })
	            .catch(function(err){
	                console.error(err);
	            })
	    }


	    vm.changeCourse = function() {
	        vm.updateFormWithValues(vm.course);
	    }
	    function updateFormWithValues(id) {

	        vm.courseId = id;
	        vm.course = id;
	        var item = $filter('filter')(vm.courselistdata, {
	            courseId: vm.course
	        })[0];
	        var coursemaster = $filter('filter')(vm.courselist, {
	            courseId: vm.course
	        })[0];
	    }

	    //Save Purchased Course Based on COurse ids
	    var data = {}
	    function addPurchasedCourses() {
	        data = {}
	        data["usr_id"] = vm.form.usr_id;
	        data["crs_id"] = [vm.course]
	        admin.purchasedCourse(data)
	            .then(function(res) {
	                if(res.statusText == "OK"){
	                    console.log(res)
	                    alertify.success(res.data.message)
	                    vm.course = [];
	                    vm.resetFrom();
	                }
	                else {alertify.error("Unable to Updated");vm.resetFrom();}
	            });
	    }
	    //Save purchased courses based on bundle ids
	    function addPurchasedBundle() {
	        data = {}
	        data["usr_id"] = vm.form.usr_id;
	        data["bundles"] = vm.form.includedBundles;
	        admin.purchasedBundle(data)
	            .then(function(res) {
	                if(res.statusText == "OK"){
	                    alertify.success(res.data.message)
	                    vm.form.includedBundles= [];
	                    vm.resetFrom()
	                }
	                else {alertify.error("Unable to Updated");vm.resetFrom();}
	            });
	    }
	    //save purchased courses based on training ids
	    function addPurchasedTraining() {
	        data = {}
	        data["usr_id"] = vm.form.usr_id;
	        data["trainings"] = vm.form.crs_id;
	        admin.purchasedTraining(data)
	            .then(function(res) {
	                if(res.statusText == "OK"){
	                    alertify.success(res.data.message);
	                    vm.form.crs_id = []
	                    vm.resetFrom();
	                }
	                else {alertify.error("Unable to Updated");vm.resetFrom();}
	            });
	    }

	    function resetFrom() {
	        vm.selectedUsers = []
	    }
	}

/***/ },

/***/ 360:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(51);
	__webpack_require__(43);

	angular.module('service').service('profile',Profile);
	Profile.$inject = ['$http','Upload','BASE_API'];
	function Profile($http,Upload,BASE_API) {
		var self = this;

		self.saveProfilePic = function(data){
			return Upload.upload({
	            url: BASE_API + '/dashboard/profile/saveProfilePic',
	            mehtod: 'POST',
	            data: data
	        });
		}

		self.getUserFriends = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserFriendsAndFollowings',data);
		}

		self.getAllUsersShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAllUsersShortDetails',data);
		}

		self.getUserShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserShortDetails',data);
		}

		self.getRecommendFriendForUser = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getRecommendFriendForUser',data);
		}

		self.sendFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/sendFriendRequest',data);	
		}

		self.acceptFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/acceptFriendRequest',data);	
		}

		self.deleteFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteFriendRequest',data);	
		}

		// unfriendFriend
		self.unfriendFriend = function(data){
			return $http.post(BASE_API + '/dashboard/profile/unfriendFriend',data);	
		}

		self.cancelFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/cancelFriendRequest',data);	
		}

		self.blockPerson = function(data){
			return $http.post(BASE_API + '/dashboard/profile/blockPerson',data);	
		}

		self.getFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequest',data);	
		}

		self.getFriendStatus = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendStatus',data);		
		}

		// Follower
		self.addFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addFollower',data);		
		}	
		self.getFollowers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFollowers',data);		
		}	
		self.removeFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/removeFollower',data);		
		}

		//user personal details
		self.getUserPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPersonalDetails',data);		
		}
		self.saveUserBasicPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveUserBasicPersonalDetails',data);		
		}

		self.getUserEmails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserEmails',data);		
		}

		self.getUserPhoneNumbers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPhoneNumbers',data);		
		}

		self.addUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserEmail',data);		
		}

		self.addUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserPhoneNumber',data);		
		}

		self.verifyUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserEmail',data);		
		}

		self.verifyUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserPhoneNumber',data);		
		}

		self.deleteUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserEmail',data);		
		}

		self.deleteUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		}
		
		// self.saveUserPhoneDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserPhoneDetails',data);		
		// }
		// self.saveUserEmailDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserEmailDetails',data);		
		// }
		// startVerifingEmailAddress
		self.startVerifingEmailAddress = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingEmailAddress',data);		
		}
		// startVerifingPhoneNumber
		self.startVerifingPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingPhoneNumber',data);		
		}

		// // confirmVerificationOfEmail
		// self.confirmVerificationOfEmail = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfEmail',data);		
		// }
		// // confirmVerificationOfPhoneNumber
		// self.confirmVerificationOfPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfPhoneNumber',data);		
		// }

		// //deleteUserPhoneNumber
		// self.deleteUserPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		// }

		// // deleteUserEmailAddress
		// self.deleteUserEmailAddress = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserEmailAddress',data);		
		// }

		// getUserContactDetails
		self.getUserContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserContactDetails',data);		
		}
		// addNonVerifiedEmailInContactDetails
		self.addNonVerifiedEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addNonVerifiedEmailInContactDetails',data);		
		}

		// confirmVerificationOfContactEmail
		self.confirmVerificationOfContactEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfContactEmail',data);		
		}

		// deleteEmailInContactDetails
		self.deleteEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEmailInContactDetails',data);		
		}

		// changePrimaryEmailInContactDetails
		self.changePrimaryEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/changePrimaryEmailInContactDetails',data);		
		}	


		// Work Form Helper
		self.saveWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveWorkInfo',data);		
		}	
		self.getWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getWorkInfo',data);		
		}
		self.deleteWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteWorkInfo',data);		
		}

		// Edu Form Helper
		self.saveEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveEduInfo',data);		
		}	
		self.getEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getEduInfo',data);		
		}
		self.deleteEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEduInfo',data);		
		}

		// Exam Form Helper
		self.saveExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveExamInfo',data);		
		}	
		self.getExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getExamInfo',data);		
		}
		self.deleteExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteExamInfo',data);		
		}

		// Interested Course Form Helper
		self.saveInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveInterestedCourseInfo',data);		
		}	
		self.getInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getInterestedCourseInfo',data);		
		}
		self.deleteInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteInterestedCourseInfo',data);		
		}

		// Author Form Helper
		self.saveAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorInfo',data);		
		}	
		self.getAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAuthorInfo',data);		
		}
		self.deleteAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteAuthorInfo',data);		
		}
		self.saveAuthorDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorDescInfo',data);		
		}

		// Teacher info Form Helper
		self.saveTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherInfo',data);		
		}	
		self.getTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTeacherInfo',data);		
		}
		self.deleteTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteTeacherInfo',data);		
		}
		self.saveTeacherDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherDescInfo',data);		
		}

		// Kid info Form Helper
		self.saveKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveKidInfo',data);		
		}	
		self.getKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getKidInfo',data);		
		}
		self.deleteKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteKidInfo',data);		
		}


		// Friend request notification api
		// getFriendRequestNotifications
		self.getFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequestNotifications',data);		
		}
		// updateFriendRequestNotifications
		self.updateFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateFriendRequestNotifications',data);		
		}

		// getTaggedFriendsNotifications
		self.getTaggedFriendsNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTaggedFriendsNotifications',data);		
		}
		// updateTaggedFriendNotifications
		self.updateTaggedFriendNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateTaggedFriendNotifications',data);		
		}

		self.getFriendsListForMessage = function(data){
			return $http.post(BASE_API + '/chat/friends/find',data);		
		}
	}

/***/ },

/***/ 361:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n    <div class=\"row\">\n        <div class=\"col-md-12\">\n            <div class=\"form-wrapper\">\n                <form class=\"form-horizontal\" id=\"form\">\n                    <fieldset>\n                        <legend>Purchased Courses</legend>\n\n                        <div class=\"form-group\">\n                            <label class=\"col-xs-2 control-label\" for=\"usrs\">Select User</label>\n                            <div class=\"col-xs-10\">\n                                <ui-select multiple ng-model=\"vm.selectedUsers\" on-select=\"vm.onUserChange()\" on-remove=\"vm.onUserChange()\" theme=\"bootstrap\" limit=\"1\" close-on-select=\"true\">\n                                    <ui-select-match placeholder=\"Select Users...\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{$item.usr_id}}\n\t\t\t\t\t\t\t\t        </span>\n                                    </ui-select-match>\n                                    <ui-select-choices repeat=\"item.usr_id as item in (vm.allUsers | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{item.usr_id}}\n\t\t\t\t\t\t\t\t        </span>\n                                    </ui-select-choices>\n                                </ui-select>\n                            </div>\n                        </div>\n\n\n                        <div #showButtonGroups ng-show=\"!showButtonGroups.hidden\" class=\"form-group\">\n                                <div class=\"col-xs-3 col-xs-offset-2\">\n                                    <button  type=\"submit\" class=\"form-control btn btn-primary submit\"\n                                             ng-disabled=\"vm.selectedUsers.length == 0\" ng-click=\"addCourse.hidden = !addCourse.hidden;showButtonGroups.hidden = true;addBundle.hidden = false;addTraining.hidden = false \">Add Course</button>\n                                </div>\n\n                            <div class=\"col-xs-3\">\n                                <button  type=\"submit\" class=\"form-control btn btn-primary submit\"\n                                         ng-disabled=\"vm.selectedUsers.length == 0\" ng-click=\"addBundle.hidden = !addBundle.hidden; addCourse.hidden = false;addTraining.hidden = false;showButtonGroups.hidden = true;\">Add Bundle</button>\n                            </div>\n\n                            <div class=\"col-xs-3\">\n                                <button  type=\"submit\" class=\"form-control btn btn-primary submit\"\n                                         ng-disabled=\"vm.selectedUsers.length == 0\" ng-click=\"addTraining.hidden = !addTraining.hidden;addBundle.hidden=false;addCourse.hidden=false; showButtonGroups.hidden = true;\">Add Training</button>\n                            </div>\n                        </div>\n\n\n                        <div ng-if=\"addCourse.hidden\" #addCourse class=\"form-group\">\n                            <label class=\"col-xs-2 control-label\" for=\"course\">Course</label>\n                            <div class=\"col-xs-10\">\n                                <select ng-model=\"vm.course\" id=\"course\" class=\"form-control\"\n                                        ng-options=\"course.courseId as course.courseName for course in vm.courselist \">\n                                    <option value=\"\"> <b>Select Course</b> </option>\n                                </select>\n                            </div>\n                        </div>\n\n                        <div ng-if=\"addBundle.hidden\" #addBundle class=\"form-group\">\n                            <label class=\"col-xs-2 control-label\" >Include Bundles</label>\n                            <div class=\"col-xs-10\">\n                                <ui-select multiple ng-model=\"vm.form.includedBundles\" theme=\"bootstrap\" close-on-select=\"true\">\n                                    <ui-select-match placeholder=\"Select Bundle...\">\n                                        <span ng-bind=\"$item.bndl_nm\"></span>\n                                    </ui-select-match>\n                                    <ui-select-choices repeat=\"item.bndl_id as item in (vm.allBundle | filter: $select.search) track by $index\">\n                                        <span ng-bind=\"item.bndl_nm\"></span>\n                                    </ui-select-choices>\n                                </ui-select>\n                            </div>\n                        </div>\n\n                        <div ng-if=\"addTraining.hidden\" #addTraining class=\"form-group\">\n                            <label class=\"col-xs-2 control-label\">Training Name</label>\n                            <div class=\"col-xs-10\">\n                                <ui-select multiple ng-model=\"vm.form.crs_id\" theme=\"bootstrap\" close-on-select=\"true\" on-select=\"vm.changeTrainingDropdown($item, $model)\" on-remove=\"vm.changeTrainingDropdown($item, $model)\" >\n                                    <ui-select-match placeholder=\"Select Training...\">\n                                        <span ng-bind=\"$item.courseName\"></span>\n                                    </ui-select-match>\n                                    <ui-select-choices  repeat=\"course.courseId as course in (vm.coursesMaster | filter: $select.search) track by $index\">\n                                        <span ng-bind=\"course.courseName\"></span>\n                                    </ui-select-choices>\n                                </ui-select>\n                            </div>\n                        </div>\n\n\n                        <div ng-show=\"showButtonGroups.hidden\" class=\"form-group\">\n                            <div class=\"col-xs-5 col-xs-offset-2\">\n                                <button class=\"form-control btn btn-primary submit\" ng-click=\"showButtonGroups.hidden = !showButtonGroups.hidden; addCourse.hidden=false;addTraining.hidden=false;addBundle.hidden=false;\">Reset</button>\n                            </div>\n                            <div class=\"col-xs-5\" >\n                                <!-- !vm.form.courseId || !vm.form.courseName || !vm.form.courseType ||  -->\n                                <button  type=\"submit\" class=\"form-control btn btn-primary submit\"\n                                         ng-disabled=\"vm.form.idExistError\" ng-click=\" addCourse.hidden ? vm.addPurchasedCourses() : addBundle.hidden ? vm.addPurchasedBundle() : vm.addPurchasedTraining() \">Submit</button>\n                            </div>\n                        </div>\n                    </fieldset>\n                </form>\n\n            </div>\n        </div>\n\n\n\n\n    </div>\n</div>\n<style type=\"text/css\">\n    body{\n        background-color: #ecf0f1;\n    }\n</style>";

/***/ }

});