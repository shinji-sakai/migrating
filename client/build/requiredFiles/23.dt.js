webpackJsonp([23],{

/***/ 234:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(51);
	angular.module('service').service('forums',Forums);
	Forums.$inject = ['$http','BASE_API'];
	function Forums($http,BASE_API) {
	    this.saveQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/saveQuestion',data);
	    }

	    this.updateQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/updateQuestion',data);
	    }

	    this.deleteQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/deleteQuestion',data);
	    }

	    this.getFirstFewQuestions = function (data) {
	        return $http.post(BASE_API + '/forums/getFirstFewQuestions',data);
	    }

	    this.getNextFewQuestions = function (data) {
	        return $http.post(BASE_API + '/forums/getNextFewQuestions',data);
	    }

		this.updateTrendingViews = function (data) {
	        return $http.post(BASE_API + '/forums/updateTrendingViews',data);
	    }

	    this.getTrendingQrys = function (data) {
	        return $http.post(BASE_API + '/forums/getTrendingQrys',data);
	    }

	    this.updateTrendingCounts = function (data) {
	        return $http.post(BASE_API + '/forums/updateTrendingCounts',data);
	    }

	    this.getQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/getQuestion',data);
	    }

	    this.getAllQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/getAllQuestion',data);
	    }

	    ////////////////////
	    // Categories API //
	    ////////////////////
	    this.getAllCategories = function (data) {
	        return $http.post(BASE_API + '/forums/category/getAllCategories',data);
	    }
	    this.saveCategory = function (data) {
	        //if coming with id then update or else add new
	        return $http.post(BASE_API + '/forums/category/saveCategory',data);
	    }
	    this.deleteCategory = function (data) {
	        return $http.post(BASE_API + '/forums/category/deleteCategory',data);
	    }

	    ///////////////////////
	    // qry stats counter //
	    ///////////////////////
	    this.updateQuestionStatsCounter = function (data) {
	        return $http.post(BASE_API + '/forums/updateQuestionStatsCounter',data);
	    }
	    this.getQuestionStatsCounter = function (data) {
	        return $http.post(BASE_API + '/forums/getQuestionStatsCounter',data);
	    }

	    ///////////////////////
	    // user qry stats counter //
	    ///////////////////////

	    this.updateUserQuestionStats = function (data) {
	        return $http.post(BASE_API + '/forums/updateUserQuestionStats',data);
	    }
	    this.getUserQuestionStats = function (data) {
	        return $http.post(BASE_API + '/forums/getUserQuestionStats',data);
	    }

	    //update question stats + update user question stats
	    this.updateQuestionStats = function (data) {
	        return $http.post(BASE_API + '/forums/updateQuestionStats',data);
	    }

	    ///////////////
	    //Follow API //
	    ///////////////

	    this.updateFollower = function (data) {
	        // data = {usr_id , qry_id}
	        return $http.post(BASE_API + '/forums/follow/updateFollower',data);
	    }

	    this.deleteFollower = function (data) {
	        // data = {usr_id , qry_id}
	        return $http.post(BASE_API + '/forums/follow/deleteFollower',data);
	    }

	    this.updateNotifications = function(data){
	        // data = {ans_usr_id , qry_id}
	        return $http.post(BASE_API + '/forums/follow/updateNotifications',data);
	    }

	    // answer later
	    this.updateAnswerLater = function(data){
	        // data = {usr_id , qry_id , ans_flg}
	        return $http.post(BASE_API + '/forums/updateAnswerLater',data);
	    }

	    this.getAnswerLater = function(data){
	        // data = {usr_id}
	        return $http.post(BASE_API + '/forums/getAnswerLater',data);
	    }

	    //Follow Notifications
	    this.getAllNotifications = function(data){
	        //
	        // data = {usr_id}
	        return $http.post(BASE_API + '/forums/getAllNotifications',data);
	    }

	    this.getDetailedFollowNotifications = function(data){
	        return $http.post(BASE_API + '/forums/getDetailedFollowNotifications',data);
	    }

	    this.markFollowNotificationRead = function(data){
	        return $http.post(BASE_API + '/forums/follow/markFollowNotificationRead',data);      
	    }





	    //report question
	    this.reportQuestion = function(data){
	        return $http.post(BASE_API + '/forums/saveComplain',data);
	    }

	    this.saveQAAnswer = function(data){
	        return $http.post(BASE_API + '/forums/ans/saveAnswer',data);
	    }

	    this.deleteQAAnswer = function(data){
	        return $http.post(BASE_API + '/forums/ans/deleteAnswer',data);
	    }

	    this.editQAAnswer = function(data){
	        return $http.post(BASE_API + '/forums/ans/editAnswer',data);
	    }

	    this.getAllAnswersOfQuestion = function(data){
	        return $http.post(BASE_API + '/forums/ans/getAllAnswersOfQuestion',data);
	    }

	    this.getAnswerOfQuestionUsingId = function(data){
	        return $http.post(BASE_API + '/forums/ans/getAnswerOfQuestionUsingId',data);
	    }

	    this.likeDislikeComment = function(data) {
	        return $http.post(BASE_API + '/forums/ans/likeDislike',data);
	    }
	    this.updateCommentLikeDislikeCount = function(data) {
	        return $http.post(BASE_API + '/forums/ans/updateLikeDislikeCount',data);
	    }
	}


/***/ },

/***/ 376:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(234);
	angular.module(window.moduleName)
	    .component('forumsCategoryPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(377),
	        controller: ForumsCategoryController,
	        controllerAs: 'vm'
	    });

	ForumsCategoryController.$inject = ['forums', 'pageLoader', '$filter', '$scope'];

	function ForumsCategoryController(forums, pageLoader, $filter, $scope) {
	    var vm = this;

	    vm.form = {};
	    vm.buttonName = "Save";
	    
	    vm.$onInit = function() {
	        updateData();
	    }

	    vm.saveCategory = saveCategory;
	    vm.updateCategory = updateCategory;
	    vm.deleteCategory = deleteCategory;
	    vm.resetForm = resetForm;

	    function saveCategory() {
	        var catData = {
	            cat_id : vm.form.cat_id,
	            cat_name: vm.form.cat_name,
	            cat_desc: vm.form.cat_desc
	        };
	        vm.buttonName = "Uploading....";
	        forums.saveCategory(catData)
	            .then(function success(data) {
	                vm.buttonName = "Save";
	                vm.submitted = true;
	                vm.form.error = false;

	                vm.form = {};
	                updateData();
	            })
	            .catch(function error(err) {
	                vm.buttonName = "Save";
	                vm.submitted = true;
	                vm.form.error = true;
	                vm.form = {};
	            });
	    }
	    
	    function deleteCategory(id) {
	        var result = confirm("Want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        forums.deleteCategory({cat_id : id})
	            .then(function(res) {
	                updateData();
	            });
	    }


	    function updateCategory(id) {
	        var item = $filter('filter')(vm.categories, {
	            cat_id: id
	        })[0];

	        if (item) {
	            vm.form = item;
	        }
	    }

	    function updateData() {
	        forums.getAllCategories()
	            .then(function(d) {
	                vm.categories = d.data;
	            })
	            .catch(function err(err) {
	                vm.form.error = true;
	            })
	    }
	    function resetForm(){
	        vm.form = {};
	    }
	}

/***/ },

/***/ 377:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add New Category</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"cat_name\">Category Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"cat_name\" class=\"form-control\" placeholder=\"Category Name\" ng-model=\"vm.form.cat_name\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"cat_desc\">Category Description</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" placeholder=\"Category Description\" id=\"cat_desc\" ng-model=\"vm.form.cat_desc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">New Category</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.cat_name\" ng-click=\"vm.saveCategory()\">{{vm.buttonName}}</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.categories\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.categories\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"cat in vm.categories\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteCategory(cat.cat_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.updateCategory(cat.cat_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t{{cat.cat_name}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in cat\">\n\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});