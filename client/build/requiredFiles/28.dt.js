webpackJsonp([28],{

/***/ 386:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(387);
	__webpack_require__(112);
	angular.module(window.moduleName)
	    .component('batchCoursePage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(389),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
	function Controller(admin,$filter,$scope,$state,EditorConfig){

		var vm = this;

		EditorConfig.toolbarButtons = EditorConfig.toolbarButtonsMD = EditorConfig.toolbarButtonsSM = EditorConfig.toolbarButtonsXS = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertImage', 'insertVideo', 'insertTable', 'html'],
	    EditorConfig.quickInsertButtons = []
	    EditorConfig.pluginsEnabled = ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"]
	    EditorConfig.events = {
	        'froalaEditor.keyup': function() {
	            $(".preview-div").each(function(v, ele) {
	                MathJax.Hub.Queue(["Typeset", MathJax.Hub, ele]);
	            })
	        }
	    }
	    vm.EditorConfig = EditorConfig;

		vm.form = vm.form || {};

		vm.form.crs_id = [];
		vm.form.includedBundles = [];

		vm.changeTrainingDropdown = changeTrainingDropdown;

		vm.saveCourse = saveCourse;
		vm.deleteCourse = deleteCourse;
		vm.editCourse = editCourse;

		vm.addQuestionToFAQList = addQuestionToFAQList;
		vm.editQuestionFromFAQList = editQuestionFromFAQList;
		vm.removeQuestionFromFAQList = removeQuestionFromFAQList;

		vm.addPointToCurriculamList = addPointToCurriculamList;
		vm.editPointFromCurriculamList = editPointFromCurriculamList;
		vm.removePointFromCurriculamList = removePointFromCurriculamList;

		vm.addFeatureToList = addFeatureToList;
		vm.editFeatureFromList = editFeatureFromList;
		vm.removeFeatureFromList = removeFeatureFromList;

		vm.addContactPhoneToList = addContactPhoneToList;
		vm.editContactPhoneFromList = editContactPhoneFromList;
		vm.removeContactPhoneFromList = removeContactPhoneFromList;

		vm.addContactEmailToList = addContactEmailToList;
		vm.editContactEmailFromList = editContactEmailFromList;
		vm.removeContactEmailFromList = removeContactEmailFromList;

		vm.$onInit = function(){
			// getAll();
			getAllCoursesFromMaster();
			getAllBundle();

			setTimeout(function(){
				// $('#about-course').froalaEditor(EditorConfig);
				// $('#curriculam').froalaEditor(EditorConfig);
				// $('#why-course').froalaEditor(EditorConfig);
			})
		}

		function getAllCoursesFromMaster() {
			admin.coursemaster()
				.then(function(d){
					vm.coursesMaster = d;
					vm.coursesMaster = vm.coursesMaster.filter(function(v,i){
						return v.isLiveClass
					})
				})
				.catch(function(err){
					console.error(err);
				});
		}

		function changeTrainingDropdown() {
			// var course = $filter('filter')(vm.allCourses,{crs_id : vm.form.crs_id[0]})[0];
			// var new_course_copy = {};
			// angular.copy(course,new_course_copy);
			if(vm.form.crs_id[0]){
				editCourse(vm.form.crs_id[0]);
			}else{
				vm.form = {};
				// $('#about-course').froalaEditor('html.set', '');
				// $('#curriculam').froalaEditor('html.set', '');
				// $('#why-course').froalaEditor('html.set', '');
				// vm.form.crs_id = course_id;
			}
		}

		function getAll(){
			admin.getAllBatchCourse()
				.then(function(res){
					vm.allCourses = res.data;
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function getAllBundle() {
			admin.getAllBundle()
				.then(function(res){
					vm.allBundle = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function saveCourse(){
			vm.formSuccess = undefined;
			// vm.form.abt_crs = $('#about-course').froalaEditor('html.get', true);
			// vm.form.curriculam = $('#curriculam').froalaEditor('html.get', true);
			// vm.form.why_this_crs = $('#why-course').froalaEditor('html.get', true);
			vm.form.crs_id = vm.form.crs_id[0]
			vm.showLoader = true;
			admin.addBatchCourse(vm.form)
				.then(function(res){
					// getAll();
					vm.form = {};
					// $('#about-course').froalaEditor('html.set', '');
					// $('#curriculam').froalaEditor('html.set', '');
					// $('#why-course').froalaEditor('html.set', '');
					vm.formSuccess = true;
				})
				.catch(function(err){
					console.log(err);
					vm.formSuccess = false;
				})
				.finally(function(){
					vm.showLoader = false;
				})
		}

		function editCourse(id){
			vm.showLoader = true;
			admin.getBatchCourse({
				crs_id:id
			})
			.then(function(res) {
				// var cs = res.data;
				vm.form = {};
				vm.form.crs_id = [id];
				vm.form.includedBundles = res.data.includedBundles;
				vm.form.no_of_days = res.data.no_of_days;
				vm.form.abt_crs = res.data.abt_crs;
				vm.form.curriculam = res.data.curriculam;
				vm.form.why_this_crs = res.data.why_this_crs;
				vm.form.faqs = res.data.faqs;
				vm.form.contactPhone = res.data.contactPhone;
				vm.form.contactEmail = res.data.contactEmail;
				vm.form.features = res.data.features;
				console.log(vm.form)
			})
			.finally(function(){
				vm.showLoader = false;
			})
			// var course = $filter('filter')(vm.allCourses,{crs_id:id})[0];

			// var new_course_copy = {};
			// angular.copy(course,new_course_copy);

			// vm.form = new_course_copy;
			// $('#about-course').froalaEditor('html.set', vm.form.abt_crs);
			// // $('#curriculam').froalaEditor('html.set', vm.form.curriculam);
			// $('#why-course').froalaEditor('html.set', vm.form.why_this_crs);
		}

		function deleteCourse(id){
			var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
			admin.deleteBatchCourse({crs_id:id})
				.then(function(res){
					getAll();
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function addQuestionToFAQList(){
			vm.form = vm.form || {};
			vm.form.faqs = vm.form.faqs || [];
			if(vm.FAQquestionTitle && vm.FAQquestionAns){
				vm.form.faqs.push({
					title : vm.FAQquestionTitle,
					ans : vm.FAQquestionAns
				})
				vm.FAQquestionTitle = "";
				vm.FAQquestionAns = "";
			}
		}

		function editQuestionFromFAQList(index) {
			var faq = vm.form.faqs[index];
			vm.FAQquestionTitle = faq.title;
			vm.FAQquestionAns = faq.ans;
			vm.form.faqs.splice(index,1);
		}

		function removeQuestionFromFAQList(index) {
			var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
			vm.form.faqs.splice(index,1);
			admin.addBatchCourse(vm.form);
		}


		function addPointToCurriculamList(){
			vm.form = vm.form || {};
			vm.form.curriculam = vm.form.curriculam || [];
			if(vm.curriculamTitle && vm.curriculamDesc && vm.curriculamWeight){
				var obj = {
					title : vm.curriculamTitle,
					desc : vm.curriculamDesc,
					weight : vm.curriculamWeight
				};
				if(vm.currentlyEditingCurriculam !== undefined){
					vm.form.curriculam.splice(vm.currentlyEditingCurriculam,0,obj);
				}else{
					vm.form.curriculam.push(obj)
				}
				vm.curriculamTitle = "";
				vm.curriculamWeight = "";
				vm.curriculamDesc = "";
				vm.currentlyEditingCurriculam = undefined;
			}
		}

		function editPointFromCurriculamList(index) {
			var curriculam = vm.form.curriculam[index];
			vm.currentlyEditingCurriculam = index;
			vm.curriculamTitle = curriculam.title;
			vm.curriculamDesc = curriculam.desc;
			vm.curriculamWeight = curriculam.weight;
			vm.form.curriculam.splice(index,1);
		}

		function removePointFromCurriculamList(index) {
			var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
			vm.form.curriculam.splice(index,1);
			admin.addBatchCourse(vm.form);
		}



		function addFeatureToList(){
			vm.form = vm.form || {};
			vm.form.features = vm.form.features || [];
			if(vm.FeatureTitle && vm.FeatureDesc){
				vm.form.features.push({
					title : vm.FeatureTitle,
					desc : vm.FeatureDesc
				})
				vm.FeatureTitle = "";
				vm.FeatureDesc = "";
			}
		}

		function editFeatureFromList(index) {
			var feature = vm.form.features[index];
			vm.FeatureTitle = feature.title;
			vm.FeatureDesc = feature.desc;
			vm.form.features.splice(index,1);
		}

		function removeFeatureFromList(index) {
			var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
			vm.form.features.splice(index,1);
			admin.addBatchCourse(vm.form);
		}

		function addContactPhoneToList(){
			vm.form = vm.form || {};
			vm.form.contactPhone = vm.form.contactPhone || [];
			if(vm.contactPhone){
				vm.form.contactPhone.push(vm.contactPhone)
				vm.contactPhone = "";
			}
		}

		function editContactPhoneFromList(index) {
			var phone = vm.form.contactPhone[index];
			vm.contactPhone = phone;
			vm.form.contactPhone.splice(index,1);
		}

		function removeContactPhoneFromList(index) {
			var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
			vm.form.contactPhone.splice(index,1);
			admin.addBatchCourse(vm.form);
		}

		function addContactEmailToList(){
			vm.form = vm.form || {};
			vm.form.contactEmail = vm.form.contactEmail || [];
			if(vm.contactEmail){
				vm.form.contactEmail.push(vm.contactEmail)
				vm.contactEmail = "";
			}
		}

		function editContactEmailFromList(index) {
			var contact = vm.form.contactEmail[index];
			vm.contactEmail = contact;
			vm.form.contactEmail.splice(index,1);
		}

		function removeContactEmailFromList(index) {
			var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
			vm.form.contactEmail.splice(index,1);
			admin.addBatchCourse(vm.form);
		}
	}

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 387:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(388);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_batchCourse.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_batchCourse.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 388:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.question-title {\n  font-family: aller;\n  font-size: 17px;\n  font-weight: bold; }\n\n.question-ans {\n  font-family: aller;\n  font-size: 15px; }\n\n.gray-back {\n  background: #e5e5e5;\n  padding: 0px 10px; }\n\n.ew-action-btns {\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: end;\n  -moz-box-pack: end;\n  box-pack: end;\n  -webkit-justify-content: flex-end;\n  -moz-justify-content: flex-end;\n  -ms-justify-content: flex-end;\n  -o-justify-content: flex-end;\n  justify-content: flex-end;\n  -ms-flex-pack: end;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n  .ew-action-btns .ew-btn {\n    margin: 0px 10px;\n    font-size: 15px;\n    cursor: pointer; }\n\n.remove-btn {\n  font-size: 15px;\n  cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 389:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<div class=\"dot-loader-wrapper\" ng-if=\"vm.showLoader\">\n\t\t\t\t\t<div class=\"dot-loader\">\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Batch Course</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Training Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<!-- <select ng-model=\"vm.form.crs_id\" id=\"course\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-change=\"vm.changeTrainingDropdown()\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Training</b> </option>\n\t\t\t\t\t\t\t\t\t<option ng-if=\"course.isLiveClass\" ng-repeat=\"course in vm.coursesMaster\" value=\"{{course.courseId}}\">{{course.courseName}}</option>\n\t\t\t\t\t\t\t\t</select> -->\n\n\n\t\t\t\t\t\t\t\t<ui-select multiple limit=\"1\" ng-model=\"vm.form.crs_id\" theme=\"bootstrap\" close-on-select=\"true\" on-select=\"vm.changeTrainingDropdown($item, $model)\" on-remove=\"vm.changeTrainingDropdown($item, $model)\" >\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Training...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices  repeat=\"course.courseId as course in (vm.coursesMaster | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"course.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t<!-- <input type=\"text\" class=\"form-control\" placeholder=\"Course Name\" ng-model=\"vm.form.crs_nm\"/> -->\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" >Include Bundles</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.form.includedBundles\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Bundle...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.bndl_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.bndl_id as item in (vm.allBundle | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.bndl_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"no of days\">No Of days</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"no of days\" ng-model=\"vm.form.no_of_days\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"about\">Why this training</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" froala=\"vm.EditorConfig\" id=\"why-course\" placeholder=\"Why this training\" ng-model=\"vm.form.why_this_crs\" ></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"about\">About training</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" froala=\"vm.EditorConfig\" id=\"about-course\" placeholder=\"About training\" ng-model=\"vm.form.abt_crs\" ></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Curriculam</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin: 10px 0px;margin-top: 0px;\" class=\"form-control\" placeholder=\"Title\" ng-model=\"vm.curriculamTitle\"/>\n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin: 10px 0px;margin-top: 0px;\" class=\"form-control\" placeholder=\"Weight\" ng-model=\"vm.curriculamWeight\"/>\n\t\t\t\t\t\t\t\t<textarea style=\"margin: 10px 0px;\" froala=\"vm.EditorConfig\" class=\"form-control\" placeholder=\"Desc\" ng-model=\"vm.curriculamDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div ng-click=\"vm.addPointToCurriculamList()\" class=\"btn btn-primary pull-right\" ng-disabled=\"!vm.curriculamTitle || !vm.curriculamWeight || !vm.curriculamDesc\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<!-- <label class=\"col-xs-2 control-label\" for=\"curriculam\">Curriculam</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"curriculam\" placeholder=\"Curriculam\" ng-model=\"vm.form.curriculam\" ></textarea>\n\t\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-repeat=\"point in vm.form.curriculam track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Curriculam {{$index+1}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<div class=\"ew-action-btns\">\n\t\t\t\t\t\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.editPointFromCurriculamList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\" style=\"color: green\"></i>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.removePointFromCurriculamList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<div class=\"question-title\">{{point[\"title\"]}}</div>\n\t\t\t\t\t\t\t\t<div class=\"question-ans\">{{point[\"desc\"]}}</div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">FAQ</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin: 10px 0px;margin-top: 0px;\" class=\"form-control\" placeholder=\"Question Title\" ng-model=\"vm.FAQquestionTitle\"/>\n\t\t\t\t\t\t\t\t<textarea style=\"margin: 10px 0px;\" class=\"form-control\" placeholder=\"Question Ans\" ng-model=\"vm.FAQquestionAns\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div ng-click=\"vm.addQuestionToFAQList()\" class=\"btn btn-primary pull-right\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-repeat=\"que in vm.form.faqs track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">FAQ {{$index+1}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<div class=\"ew-action-btns\">\n\t\t\t\t\t\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.editQuestionFromFAQList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\" style=\"color: green\"></i>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.removeQuestionFromFAQList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<div class=\"question-title\">{{que[\"title\"]}}</div>\n\t\t\t\t\t\t\t\t<div class=\"question-ans\">{{que[\"ans\"]}}</div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Feature</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin: 10px 0px;margin-top: 0px;\" class=\"form-control\" placeholder=\"Feature Title\" ng-model=\"vm.FeatureTitle\"/>\n\t\t\t\t\t\t\t\t<textarea style=\"margin: 10px 0px;\" class=\"form-control\" placeholder=\"Feature Description\" ng-model=\"vm.FeatureDesc\"></textarea>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div ng-click=\"vm.addFeatureToList()\" class=\"btn btn-primary pull-right\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-repeat=\"ft in vm.form.features track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Ft {{$index+1}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<div class=\"ew-action-btns\">\n\t\t\t\t\t\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.editFeatureFromList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\" style=\"color: green\"></i>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"remove-btn\" ng-click=\"vm.removeFeatureFromList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"question-title\">{{ft[\"title\"]}}</div>\n\t\t\t\t\t\t\t\t<div class=\"question-ans\">{{ft[\"desc\"]}}</div>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Contact Phone</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-9\"> \n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin: 10px 0px;margin-top: 0px;\" class=\"form-control\" placeholder=\"Phone Number\" ng-model=\"vm.contactPhone\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\"> \n\t\t\t\t\t\t\t\t<div ng-click=\"vm.addContactPhoneToList()\" class=\"btn btn-primary pull-right\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-repeat=\"ft in vm.form.contactPhone track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Contact Phone {{$index+1}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<div class=\"ew-action-btns\">\n\t\t\t\t\t\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.editContactPhoneFromList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\" style=\"color: green\"></i>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"remove-btn\" ng-click=\"vm.removeContactPhoneFromList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"question-title\">{{ft}}</div>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Contact Email</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin: 10px 0px;margin-top: 0px;\" class=\"form-control\" placeholder=\"Email\" ng-model=\"vm.contactEmail\"/>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div ng-click=\"vm.addContactEmailToList()\" class=\"btn btn-primary pull-right\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-repeat=\"ft in vm.form.contactEmail track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Contact Email {{$index+1}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<div class=\"ew-action-btns\">\n\t\t\t\t\t\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.editContactEmailFromList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\" style=\"color: green\"></i>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"remove-btn\" ng-click=\"vm.removeContactEmailFromList($index)\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"question-title\">{{ft}}</div>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.crs_id\" ng-click=\"vm.saveCourse()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<!-- <div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allCourses\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allCourses\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"course in vm.allCourses\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteCourse(course.crs_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editCourse(course.crs_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{course.crs_id}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in course\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div> -->\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t.dot-loader-wrapper{\n\t\tposition: absolute;\n\t    top: 0px;\n\t    left: 0px;\n\t    bottom: 0px;\n\t    right: 0px;\n\t    z-index: 10;\n\t    background: rgba(0,0,0,0.3);\n\t    border-radius: 5px;\n\t    display: flex;\n\t    align-items: center;\n\t    justify-content: center;\n\t}\n</style>";

/***/ }

});