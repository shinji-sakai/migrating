webpackJsonp([17],{

/***/ 364:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('subjectPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(365),
	            controller: AdminSubjectController,
	            controllerAs: 'vm'
	        });

		AdminSubjectController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminSubjectController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;
	        vm.no_of_item_already_fetch=0;
	        vm.checkValidityOfID = checkValidityOfID;
	    	vm.updateSubject = updateSubject;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removeSubject = removeSubject;
	    	vm.editSubject = editSubject;

	    	vm.$onInit = function() {
	        	//updateSubjectsList();
		    };

	        function checkValidityOfID() {
	            vm.form.subjectId = vm.form.subjectId || "";
	            vm.form.subjectId = vm.form.subjectId.replace(/\s/g , "-");
					admin.isIdExist({
					collection :'subject',
					key:"subjectId",
					id_value:vm.form.subjectId			
				}).then(function(data)
				{				
	              console.log(data.data);
				  if(data.data.idExist)
				  {
					    vm.idExists = true;
				  }
				  else
				  {
					   vm.idExists = false;
				  }
				}).catch(function(err){
	             console.log(err);
				})
				
				
	         
	        }

		    function updateSubjectsList(){
		    	pageLoader.show();
		    	admin.getAllSubject()
	        		.then(function(data){
	        			if(data.data.length > 0)
	        				vm.subjects = data.data;
	                    else
	                        vm.subjects = [];
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }

	    	function updateSubject(){
	    		var data = {
	    			subjectId : vm.form.subjectId,
	    			subjectName : vm.form.subjectName,
	    			subjectDesc : vm.form.subjectDesc 
	    		}
				if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }

	    		pageLoader.show();
	    		admin.updateSubject(data)
	    			.then(function(res){
						vm.fetchSubject();
	    				vm.form.error = false;
	    				vm.form.subjectName = "";
	    				vm.form.subjectDesc = "";
	                    vm.form.subjectId = "";
	                    vm.idExists = false;
	                    vm.formIsInEditMode = false;
	    			//	updateSubjectsList();
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}

	    	function resetFormDetail(){
	    		vm.submitted = false;
	            vm.idExists = false;
	            vm.formIsInEditMode = false;
	    		vm.form = {};
	    	}
			vm.fetchMoreSubject = function(){
				vm.isSubjectLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					   collection : 'subject',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					$log.log('res.data'+res.data);
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.subjects , {subjectId: v.subjectId})[0];
							if(!item){
									vm.subjects.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						vm.last_update_dt = vm.subjects[vm.subjects.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isSubjectLoading = false;
				})
			}
	vm.fetchSubject=function(){
		vm.isSubjectLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
				 collection : 'subject'
				})
				.then(function(res){
					$log.debug(res)
						vm.subjects = res.data;
				    	vm.no_of_item_already_fetch = 5;
						vm.last_update_dt = vm.subjects[vm.subjects.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isSubjectLoading = false;
				})
	}
	    	function removeSubject(id){
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	    		admin.removeSubject({subjectId:id})
	    			.then(function(d){
	    				console.log(d);
							var item = $filter('filter')(vm.subjects || [], {subjectId: id})[0];								
						 if (vm.subjects.indexOf(item) > -1) {
	                        var pos = vm.subjects.indexOf(item);
	                       vm.subjects.splice(pos, 1);
	                    }
	    				//updateSubjectsList();
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}

	    	function editSubject(id){
	            console.log(id);
	    		var subject = $filter('filter')(vm.subjects,{subjectId:id})[0];
	    		vm.form = {};
	    		vm.form.subjectId = subject.subjectId;
	    		vm.form.subjectName = subject.subjectName;
	    		vm.form.subjectDesc = subject.subjectDesc;
	            vm.formIsInEditMode = true;
	    	}
	    }

/***/ },

/***/ 365:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateSubject()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Subject</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subjectId\">Subject Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"subjectId\" class=\"form-control\" placeholder=\"Id\" ng-change=\"vm.checkValidityOfID()\" ng-disabled=\"vm.formIsInEditMode\" ng-model=\"vm.form.subjectId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.idExists\">\n\t\t\t\t\t\t\t\tSubject Id exists\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subjectname\">Subject Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"subjectname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.subjectName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subjectdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"subjectdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.subjectDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Subject</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" ng-disabled=\"vm.idExists\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchSubject()\" ng-if=\"!vm.subjects || vm.subjects.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isSubjectLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isSubjectLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Subject</span>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.subjects\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.subjects\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"subject in vm.subjects\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeSubject(subject.subjectId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editSubject(subject.subjectId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{subject.subjectName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in subject\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreSubject()\" ng-if=\"(vm.subjects || vm.subjects.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isSubjectLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isSubjectLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});