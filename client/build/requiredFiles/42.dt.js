webpackJsonp([42],{

/***/ 158:
/***/ function(module, exports) {

	angular
		.module('service')
		.service('Mail',Mail);

	Mail.$inject = ['$http','BASE_API'];

	function Mail($http,BASE_API) {
	    this.sendMail = function (data) {
	        return $http.post(BASE_API + '/mail/send', data).then(function (res) {
	            return res.data;
	        });
	    }

	    this.sendBulkEmails = function (data) {
	        return $http.post(BASE_API + '/mail/sendBulkEmails', data);
	    }

	    this.subscribeUser = function (data) {
	        return $http.post(BASE_API + '/mail/subscribeUser', data);
	    }

	    this.sendMailToSupportForEnrollBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForEnrollBtn', data);   
	    }

	    this.sendMailToSupportForPaySecurelyBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForPaySecurelyBtn', data);   
	    }
	}


/***/ },

/***/ 180:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(181);
	__webpack_require__(182);
	__webpack_require__(183);


/***/ },

/***/ 181:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {(function () {
		var tbioDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Directive');
			var link = function (scope, element, attrs, controllers) {
				//var idOfElement = '[' + element.prop('id') + '] ';
				//$log.log('In TBIO directive for ID ' + idOfElement);
				var tbioCtrl = controllers[0];
				var ngModelCtrl = controllers[1];
				if (ngModelCtrl) {
					tbioCtrl.init(element, attrs, ngModelCtrl);
				}
			};
			return {
				restrict: 'A',
				priority: 100,
				require: ['tbio', 'ngModel'],
				controller: 'TextboxioController',
				scope: {
					configuration: '=' //tbioConfiguration JavaScript object
				},
				link: link
			};
		}];

		var tbioRequiredDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Required Directive');
			var link = function (scope, element, attrs, ngModelCtrl) {
				if (!ngModelCtrl) return;
				attrs.required = true; // force truthy in case we are on non input element
				attrs.$observe('tbioRequired', function () {
					ngModelCtrl.$validate();
				});
				ngModelCtrl.$validators.tbioRequired = function (modelValue, viewValue) {
					var jStrippedString = jQuery(modelValue).text().trim();
					//$log.log('REQUIRED: ' + (!attrs.required || !ngModelCtrl.$isEmpty(jStrippedString)));
					return !attrs.required || !ngModelCtrl.$isEmpty(jStrippedString);
				};
			};
			return {
				restrict: 'A',
				require: '?ngModel',
				link: link
			};
		}];

		var tbioMinLengthDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Min Length Directive');
			var link = function (scope, element, attrs, ngModelCtrl) {
				if (!ngModelCtrl) return;
				var minlength = 0;
				attrs.$observe('tbioMinlength', function (value) {
					minlength = parseInt(value) || 0;
					ngModelCtrl.$validate();
				});
				ngModelCtrl.$validators.tbioMinlength = function (modelValue, viewValue) {
					var jStrippedString = jQuery(modelValue).text().trim();
					//$log.log('Min Length? ' + (ngModelCtrl.$isEmpty(jStrippedString) || jStrippedString.length >= minlength));
					return ngModelCtrl.$isEmpty(jStrippedString) || jStrippedString.length >= minlength;
				};
			};
			return {
				restrict: 'A',
				require: 'ngModel',
				link: link
			};
		}];

		var tbioMaxLengthDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Max Length Directive');
			var link = function (scope, element, attrs, ngModelCtrl) {
				if (!ngModelCtrl) return;
				var maxlength = -1;
				attrs.$observe('tbioMaxlength', function (value) {
					maxlength = isNaN(parseInt(value)) ? -1 : parseInt(value);
					ngModelCtrl.$validate();
				});
				ngModelCtrl.$validators.tbioMaxlength = function (modelValue, viewValue) {
					var jStrippedString = jQuery(modelValue).text().trim();
					//$log.log('Max Length? ' + ((maxlength < 0) || ngModelCtrl.$isEmpty(jStrippedString) || (jStrippedString.length <= maxlength)));
					return (maxlength < 0) || ngModelCtrl.$isEmpty(jStrippedString) || (jStrippedString.length <= maxlength);
				};
			};
			return {
				restrict: 'A',
				require: 'ngModel',
				link: link
			};
		}];

		var tbioController = ['$scope', '$interval', '$log', 'tbioConfigFactory', 'tbioValidationsFactory',
							  function ($scope, $interval, $log, tbioConfigFactory, tbioValidationsFactory) {
				//$log.log('Loading Textbox.io Controller');
				this.init = function (element, attrs, ngModelController) {
					//var idOfElement = '[' + element.prop('id') + '] ';
					//$log.log('In this.init for ' + idOfElement);
					var theEditor;
					var config = attrs['configuration'];

					//Populate the editor once the modelValue exists
					//would reload the editor if the model is changed in the background.
					ngModelController.$render = function () {
						if (!theEditor) { //only load the editor the first time
							if (tbioConfigFactory.hasOwnProperty(config)) {
								theEditor = textboxio.replace(element[0], tbioConfigFactory[config]);
							} else {
								theEditor = textboxio.replace(element[0]);
							}
						}
						if (ngModelController.$modelValue) {
							theEditor.content.set(ngModelController.$modelValue);
							ngModelController.$setPristine()
						}
					}; //$render end

					// In lieu of events I just update the model every X seconds.
					// Once the editor has event(s) this gets replaced by event code.
					var interval = $interval(function () {
						//Workaround to keep $pristine accurate until you type into the editor
						if (ngModelController.$viewValue && (ngModelController.$viewValue != theEditor.content.get())) {
							//$log.log('Content Changed!');
							ngModelController.$setViewValue(theEditor.content.get());
							return;
						}
						//Check for the default "empty" string and don't put anything in the view when "empty" is there
						//to start with.  The prior if will catch having editor content that is completely deleted to revert
						//to "empty".
						if (!('<p><br /></p>' == theEditor.content.get())) {
							ngModelController.$setViewValue(theEditor.content.get());
						}
					}, 500); // interval end

					// When the DOM element is removed from the page AngularJS will trigger the $destroy event on
					// the scope. This gives us a chance to cancel the $interval.
					$scope.$on("$destroy", function (event) {
						$interval.cancel(interval);
					}); //$on end

					//Allow developer to inject custom validation functions via tbioValidationsFactory
					for (var validationFn in tbioValidationsFactory) {
						$log.log('Adding custom validation!');
						ngModelController.$validators[validationFn] = tbioValidationsFactory[validationFn];
					};
				}; //init end
		}];

		//Create the actual Controller and Directive
		angular.module('ephox.textboxio', [])
			.controller('TextboxioController', tbioController)
			.directive('tbio', tbioDirective)
			.directive('tbioMinlength', tbioMinLengthDirective)
			.directive('tbioMaxlength', tbioMaxLengthDirective)
			.directive('tbioRequired', tbioRequiredDirective);
	}());

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 182:
/***/ function(module, exports) {

	angular.module('ephox.textboxio').factory('tbioConfigFactory', ['$log', function ($log) {
		//    $log.log('Loading tbioConfigFactory');
		var configurations = {};

		configurations.default = {
			// basePath: '../textboxio/resources',
			css: {
				stylesheets: [''],
				styles: [
					{
						rule: 'p',
						text: 'Paragraph'
						},
					{
						rule: 'h1',
						text: 'H1'
						},
					{
						rule: 'h2',
						text: 'H2'
						},
					{
						rule: 'h3',
						text: 'H3'
						},
					{
						rule: 'h4',
						text: 'H4'
						},
					{
						rule: 'div',
						text: 'div'
						},
					{
						rule: 'pre',
						text: 'pre'
						}
				]
			},
			codeview: {
				enabled: true,
				showButton: true
			},
			images: {
				upload : {
					url : '/ckeditor/uploadTextboxIOImage'
				},
				allowLocal: true,
				editing: {
	                enabled: true,
	                preferredWidth : 1024,
	                preferredHeight : 1024
	            }
			},
			languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
			// locale : '', // Default locale is inferred from client browser
			paste: {
				style: 'prompt'
			},
			// spelling : {},
			ui: {
				toolbar: {
					items: ['undo', 'insert', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
				}
			}
		};

		configurations.simple = {
			//        basePath: '/sites/all/libraries/textboxio/resources',
			css: {
				stylesheets: [''],
				styles: [
					{
						rule: 'p',
						text: 'block.p'
					},
					{
						rule: 'div',
						text: 'block.div'
					},
					{
						rule: 'h1',
						text: 'block.h1'
					},
					{
						rule: 'h2',
						text: 'block.h2'
					},
					{
						rule: 'h3',
						text: 'block.h3'
					},
					{
						rule: 'h4',
						text: 'block.h4'
					}
				]
			},
			codeview: {
				enabled: false,
				showButton: false
			},
			images: {
				// upload : {},
				allowLocal: true
			},
			languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
			// locale : '', // Default locale is inferred from client browser
			paste: {
				style: 'prompt'
			},
			// spelling : {},
			ui: {
				toolbar: {
					items: ['emphasis']
				}
			}
		};

		configurations.videonotes = {
			// basePath: '../textboxio/resources',
			css: {
				stylesheets: [''],
				styles: [
					{
						rule: 'p',
						text: 'Paragraph'
						},
					{
						rule: 'h1',
						text: 'H1'
						},
					{
						rule: 'h2',
						text: 'H2'
						},
					{
						rule: 'h3',
						text: 'H3'
						},
					{
						rule: 'h4',
						text: 'H4'
						},
					{
						rule: 'div',
						text: 'div'
						},
					{
						rule: 'pre',
						text: 'pre'
						}
				]
			},
			codeview: {
				enabled: false,
				showButton: false
			},
			images: {
				upload : {
					url : '/ckeditor/uploadTextboxIOImage'
				},
				allowLocal: true,
				editing: {
	                enabled: true,
	                preferredWidth : 1024,
	                preferredHeight : 1024
	            }
			},
			languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
			// locale : '', // Default locale is inferred from client browser
			paste: {
				style: 'prompt'
			},
			// spelling : {},
			ui: {
				toolbar: {
					items: ['undo', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
				}
			}
		};

		return configurations;
	}]);


/***/ },

/***/ 183:
/***/ function(module, exports) {

	// You can add your own custom validation functions that will be added to the $validators pipeline for all textboxio
	// instances on the page.

	angular.module('ephox.textboxio').factory('tbioValidationsFactory', ['$log', function ($log) {
		//    $log.log('Loading tbioValidationsFactory');
		var validations = [];
		/*
			validations['sampleValidation1'] = function (modelValue, viewValue) {
				//        $log.log('Model value: [' + modelValue + '  View value:[' + viewValue + ']');
				return true;
			};

			validations['sampleValidation2'] = function (modelValue, viewValue) {
				//        $log.log('Model value: [' + modelValue + '  View value:[' + viewValue + ']');
				return true;
			}
		*/
		return validations;
	}]);


/***/ },

/***/ 428:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(158);
	__webpack_require__(180);
	angular.module(window.moduleName)
	    .component('sendBulkMails', {
	        template: __webpack_require__(429),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','Mail'];

	function Controller($log,$filter, $scope, $state, EditorConfig, $timeout,Mail) {
	    var vm = this;

	    vm.sendMails = sendMails;
	    
	    vm.$onInit = function() {
	     
	    }
	    function sendMails() {
	    	Mail.sendBulkEmails(vm.form)
	            .then(function(res){
	                $log.debug(res);
	                vm.form = {};
	                $timeout(function() {
	                    vm.form.mail_body = "<p>&nbsp;</p>";
	                })
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }
	}

/***/ },

/***/ 429:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" onsubmit=\"return false;\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Send Bulk Mails</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-1 control-label\" for=\"mailSubject\">Subject</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-11\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"mailSubject\" placeholder=\"Mail Subject\" ng-model=\"vm.form.mail_subject\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-1 control-label\" for=\"mailBody\">Content</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-11\">\n\t\t\t\t\t\t\t\t<TEXTAREA tbio class=\"form-control\" configuration=\"default\" rows=\"10\" id=\"mailBody\" placeholder=\"Page Content\" ng-model=\"vm.form.mail_body\"></TEXTAREA>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.form= {}\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" \n\t\t\t\t\t\t\t\t\tng-disabled=\"!vm.form.mail_subject || !vm.form.mail_body\"\n\t\t\t\t\t\t\t\t\tng-click=\"vm.sendMails()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});