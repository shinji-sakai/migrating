webpackJsonp([18],{

/***/ 366:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('examPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(367),
	            controller: AdminExamController,
	            controllerAs: 'vm'
	        });

		AdminExamController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminExamController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;
	       	vm.no_of_item_already_fetch = 0;
	        vm.checkValidityOfID = checkValidityOfID;
	    	vm.updateExam = updateExam;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removeExam = removeExam;
	    	vm.editExam = editExam;

	    	vm.$onInit = function() {
	        	//updateExamsList();
		    };


	        function checkValidityOfID() {
	            vm.form.examId = vm.form.examId || "";
	            vm.form.examId = vm.form.examId.replace(/\s/g , "-");
				admin.isIdExist({
					collection :'exam',
					key:"examId",
					id_value:vm.form.examId			
				}).then(function(data)
				{				
	              console.log(data.data);
				  if(data.data.idExist)
				  {
					    vm.idExists = true;
				  }
				  else
				  {
					   vm.idExists = false;
				  }
				}).catch(function(err){
	             console.log(err);
				})
			
				
	          
	        }

		    function updateExamsList(){
		    	pageLoader.show();
		    	admin.getAllExam()
	        		.then(function(data){
	        			if(data.data.length > 0)
	        				vm.exams = data.data;
	                    else
	                        vm.exams = [];
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }
	      vm.fetchExams = function(){
				vm.isExamsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'exam'
				})
				.then(function(res){
					$log.debug(res)
					vm.exams = res.data;
					vm.no_of_item_already_fetch = 5;
					 vm.last_update_dt = vm.exams[vm.exams.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isExamsLoading = false;
				})
			}

			vm.fetchMoreExams = function(){
				debugger;
				vm.isExamsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'exam',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.exams, {examId: v.examId})[0];
							if(!item){
								vm.exams.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						 vm.last_update_dt = vm.exams[vm.exams.length - 1].update_dt;
						// vm.coursemasterdata = vm.coursemasterdata.concat(res.data)	
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isExamsLoading = false;
				})
			}
	    	function updateExam(){
	    		var data = {
	    			examId : vm.form.examId,
	    			examName : vm.form.examName,
	    			examDesc : vm.form.examDesc 
	    		}
				if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }
	    		pageLoader.show();
	    		admin.updateExam(data)
	    			.then(function(res){
						// var item = $filter('filter')(vm.exams || [], {examId: vm.form.examId})[0];
						// if(item){
						// 	item.examId = vm.form.examId,
						// 	item.examName = vm.form.examName,
						// 	item.examDesc = vm.form.examDesc 
						// }else{
						// 	vm.exams = [data].concat(vm.exams || [])
						// }
						vm.fetchExams();
	    				vm.submitted = true;
	    				vm.form.error = false;
	    				vm.form.examName = "";
	    				vm.form.examDesc = "";
	                    vm.form.examId = "";
	                    vm.idExists = false;
	                    vm.formIsInEditMode = false;
	    				//updateExamsList();
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}

	    	function resetFormDetail(){
	    		vm.submitted = false;
	            vm.idExists = false;
	            vm.formIsInEditMode = false;
	    		vm.form = {};
	    	}

	    	function removeExam(id){
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	    		admin.removeExam({examId:id})
	    			.then(function(d){
	    				console.log(d);
						var item = $filter('filter')(vm.exams || [], {examId: id})[0];								
						 if (vm.exams.indexOf(item) > -1) {
	                        var pos = vm.exams.indexOf(item);
	                       vm.exams.splice(pos, 1);
	                    }
	    				//updateExamsList();
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}

	    	function editExam(id){

	    		var book = $filter('filter')(vm.exams,{examId:id})[0];
	    		vm.form = {};
	    		vm.form.examId = book.examId;
	    		vm.form.examName = book.examName;
	    		vm.form.examDesc = book.examDesc;
	            vm.formIsInEditMode = true;
	    	}
	    }

/***/ },

/***/ 367:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateExam()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Exam</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"examId\">Exam Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"examId\" class=\"form-control\" placeholder=\"Id\" ng-change=\"vm.checkValidityOfID()\" ng-disabled=\"vm.formIsInEditMode\" ng-model=\"vm.form.examId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\"  ng-if=\"vm.idExists\">\n\t\t\t\t\t\t\t\tExam Id exists\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"examname\">Exam Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"examname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.examName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"examdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"examdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.examDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Exam</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" ng-disabled=\"vm.idExists\" >Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchExams()\" ng-if=\"!vm.exams || vm.exams.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isExamsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isExamsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Exam</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.exams\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.exams\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"exam in vm.exams\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeExam(exam.examId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editExam(exam.examId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{exam.examName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in exam\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreExams()\" ng-if=\"(vm.exams || vm.exams.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isExamsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isExamsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});