webpackJsonp([55],{

/***/ 465:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(112);
	angular.module(window.moduleName)
	    .component('jobOpeningsPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(466),
	        controller: AdminJobOpeningsController,
	        controllerAs: 'vm'
	    });

	AdminJobOpeningsController.$inject = ['$log','admin', 'pageLoader', '$filter', '$scope','EditorConfig'];

	function AdminJobOpeningsController($log,admin, pageLoader, $filter, $scope,EditorConfig) {
	    var vm = this;
	    
	    vm.form = {};
	    vm.form.jobStatus = "open"

	    vm.EditorConfig = EditorConfig;

	    vm.jobOpenings = [];
	  vm.no_of_item_already_fetch=0;
	    vm.updateJobOpening = updateJobOpening;
	    vm.resetFormDetail = resetFormDetail;
	    vm.removeJobOpening = removeJobOpening;
	    vm.editJobOpening = editJobOpening;

	    vm.$onInit = function() {
	      //  updateJobOpeningsList();
	    };

	    //get jobOpenings from db
	    //update jobOpenings array to display at local
	    function updateJobOpeningsList() {
	        pageLoader.show();
	        //get all jobOpenings from db
	        admin.getAllJobOpenings()
	            .then(function(data) {
	                if (data.data.length > 0)
	                    vm.jobOpenings = data.data;
	                else
	                    vm.jobOpenings = [];
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    	vm.fetchjobOpenings = function(){
	            vm.jobOpenings = [];
				vm.isjobOpeningsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : 0,
	                collection : 'jobOpenings'
				})
				.then(function(res){
					$log.debug(res)
					vm.jobOpenings = res.data;
					vm.no_of_item_already_fetch = 5;
	                vm.last_update_dt = vm.jobOpenings[vm.jobOpenings.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isjobOpeningsLoading = false;
				})
			}
	   
			vm.fetchMorejobOpenings = function(){
				vm.isjobOpeningsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : vm.no_of_item_already_fetch,
	                collection : 'jobOpenings',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.jobOpenings, {jobOpeningId: v.jobOpeningId})[0];
							if(!item){
								vm.jobOpenings.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
	                    vm.last_update_dt = vm.jobOpenings[vm.jobOpenings.length - 1].update_dt
						
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isjobOpeningsLoading = false;
				})
			}

	    //update/insert book to db
	    function updateJobOpening() {
	        
	            if(!vm.formIsInEditMode){
	                vm.form.create_dt = new Date();
	                vm.form.update_dt = new Date();
	            }else{
	                vm.form.update_dt = new Date();
	            }
	        pageLoader.show();
	        vm.submitted = false;
	        admin.updateJobOpening(vm.form)
	            .then(function(res) {
	                vm.fetchjobOpenings();
	                resetFormDetail();
	               // updateJobOpeningsList();
	            })
	            .catch(function() {
	                vm.submitted = true;
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function resetFormDetail() {
	        vm.submitted = false;
	        vm.formIsInEditMode = false;
	        vm.form = {};
	        vm.form.jobStatus = "open";
	    }


	    //this function will call api and remove book from db
	    function removeJobOpening(id) {
	        //ask for confirmation
	        var result = confirm("Want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        pageLoader.show();
	        //service to remove book from server
	        admin.removeJobOpening({
	                jobOpeningId: id
	            })
	            .then(function(d) {
	                var item = $filter('filter')(vm.jobOpenings || [], {jobOpeningId: id})[0];								
						 if (vm.jobOpenings.indexOf(item) > -1) {
	                        var pos = vm.jobOpenings.indexOf(item);
	                       vm.jobOpenings.splice(pos, 1);
	                    }
	                //update array locally
	                //updateJobOpeningsList();
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    //this function will be called when user will click on press button of book 
	    function editJobOpening(id) {
	        var jobOpening = $filter('filter')(vm.jobOpenings, {
	            jobOpeningId: id
	        })[0];
	        delete jobOpening["_id"];
	        vm.form = jobOpening;
	        vm.formIsInEditMode = true;
	    }
	}

/***/ },

/***/ 466:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateJobOpening()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Job Opening</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"jobname\">Job Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"jobname\" name=\"jobname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.jobName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"jobdesc\">Description<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea froala=\"vm.EditorConfig\" id=\"jobdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.jobDesc\" required=\"\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"jobdesc\">Status</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.jobStatus\" class=\"form-control\">\n\t\t\t\t\t\t\t\t\t<option value=\"open\">Open</option>\n\t\t\t\t\t\t\t\t\t<option value=\"closed\">Closed</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Opening</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" >Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchjobOpenings()\" ng-if=\"!vm.jobOpenings || vm.jobOpenings.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isjobOpeningsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isjobOpeningsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show jobOpenings</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.jobOpenings\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.jobOpenings\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"jobOpening in vm.jobOpenings\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeJobOpening(jobOpening.jobOpeningId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editJobOpening(jobOpening.jobOpeningId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{jobOpening.jobName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in jobOpening\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMorejobOpenings()\" ng-if=\"(vm.jobOpenings==[] || vm.jobOpenings.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isjobOpeningsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isjobOpeningsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});