webpackJsonp([19],{

/***/ 368:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('topicPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(369),
	            controller: AdminTopicController,
	            controllerAs: 'vm'
	        });

		AdminTopicController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminTopicController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;

	        vm.topics = [];
	       vm.no_of_item_already_fetch = 0;
	    	vm.updateTopic = updateTopic;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removeTopic = removeTopic;
	    	vm.editTopic = editTopic;
	        vm.changeSubject = changeSubject;
	        vm.form = vm.form || {};
	        vm.form.topicDifficulty = vm.form.topicDifficulty || []; 

	        vm.difficultyLevel = [{
	            id : 'easy',
	            title : "Easy"
	        },{
	            id : 'medium',
	            title : "Medium"
	        },{
	            id : 'hard',
	            title : "Hard"
	        }]

	    	vm.$onInit = function() {
	            admin.getAllSubject()
	                .then(function(d){
	                    vm.subjectList = d.data;
	                });
	        	// updateTopicsList();
		    };
	          function checkValidityOfID() {
	            vm.form.topicId = vm.form.topicId || "";
	            vm.form.topicId = vm.form.topicId.replace(/\s/g , "-");
				admin.isIdExist({
					collection :'topic',
					key:"topicId",
					id_value:vm.form.topicId			
				}).then(function(data)
				{				
	              console.log(data.data);
				  if(data.data.idExist)
				  {
					    vm.idExists = true;
				  }
				  else
				  {
					   vm.idExists = false;
				  }
				}).catch(function(err){
	             console.log(err);
				})
	        }

		    function updateTopicsList(){
		    	pageLoader.show();
	            console.log(vm.subjectId);
		    	admin.getAllTopic({subjectId : vm.subjectId})
	        		.then(function(data){
	        			if(data.data.length > 0){
	        				vm.topics = data.data;
	                        vm.topics.sort(function(a,b){
	                            return a.topicNumber - b.topicNumber;
	                        });
	                        vm.form = vm.form || {};
	                        vm.form.topicNumber = vm.topics[vm.topics.length - 1]['topicNumber'] + 1;
	                    }
	                    else{
	                        vm.topics = [];
	                        vm.form = vm.form || {};
	                        vm.form.topicNumber = 1;
	                    }
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }

	    	function updateTopic(){
	    		var data = {
	    			topicId : vm.form.topicId,
	                topicNumber : vm.form.topicNumber,
	    			topicName : vm.form.topicName,
	    			topicDesc : vm.form.topicDesc,
	                subjectId : vm.subjectId,
	                topicDifficulty : vm.form.topicDifficulty
	    		}
					if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }
	    		pageLoader.show();
	    		admin.updateTopic(data)
	    			.then(function(res){
						var item = $filter('filter')(vm.topics || [], {topicId: vm.form.topicId})[0];
						// if(item){
						// 		item.topicId = vm.form.topicId,
						// 		item.topicNumber = vm.form.topicNumber,
						// 		item.topicName = vm.form.topicName,
						// 		item.topicDesc = vm.form.topicDesc,
						// 		item.subjectId = vm.subjectId,
						// 		item.topicDifficulty = vm.form.topicDifficulty
						// }else{
						// 	vm.topics = [data].concat(vm.topics || [])
						// }
						vm.fetchTopics();
	    				vm.submitted = true;
	    				vm.form.error = false;
	    				vm.form.topicName = "";
	    				vm.form.topicDesc = "";
	                    vm.form.topicId = "";
	                    vm.form.topicNumber += 1;
	                    vm.form.topicDifficulty = [];
						vm.formIsInEditMode=false;
	    				//updateTopicsList();
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}

	    	function resetFormDetail(){
	    		vm.submitted = false;
	    		vm.form = {};
	            vm.form.topicDifficulty = [];
	            if(vm.topics.length > 0){
	                vm.form.topicNumber = vm.topics[vm.topics.length - 1]['topicNumber'] + 1;
	            }
	    	}

	    	function removeTopic(id){
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	    		admin.removeTopic({topicId:id})
	    			.then(function(d){
	    				console.log(d);
						var item = $filter('filter')(vm.topics || [], {topicId: id})[0];								
						 if (vm.topics.indexOf(item) > -1) {
	                        var pos = vm.topics.indexOf(item);
	                       vm.topics.splice(pos, 1);
	                    }
	    				//updateTopicsList();
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}

	    	function editTopic(id){

	    		var obj = $filter('filter')(vm.topics,{topicId:id})[0];
	    		vm.form = {};
	    		vm.form.topicId = obj.topicId;
	    		vm.form.topicName = obj.topicName;
	    		vm.form.topicDesc = obj.topicDesc;
	            vm.form.topicNumber = obj.topicNumber;
	            vm.form.topicDifficulty = obj.topicDifficulty;
				vm.formIsInEditMode=true;
	    	}
			vm.fetchTopics = function(){
				vm.isTopicsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
				collection : 'topic'
				})
				.then(function(res){
					$log.debug(res)
					  vm.topics = res.data;
					vm.no_of_item_already_fetch = 5;
					vm.last_update_dt = vm.topics[vm.topics.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTopicsLoading = false;
				})
			}
			vm.fetchMoreTopics = function(){
				vm.isTopicsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					   collection : 'topic',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 0){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.topics, {topicId: v.topicId})[0];
							if(!item){
								  vm.topics.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						vm.last_update_dt = vm.topics[vm.topics.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTopicsLoading = false;
				})
			}
	        function changeSubject(){
				//   if(vm.topics.length > 0){
				//  vm.form.topicNumber = vm.topics[vm.topics.length - 1]['topicNumber'] + 1;
				//   }
	            //updateTopicsList();
	        }
	    }

/***/ },

/***/ 369:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateTopic()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Topic</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"subject\">Subject<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.subjectId\" ng-change=\"vm.changeSubject()\" id=\"subject\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"subject.subjectId as subject.subjectName for subject in vm.subjectList \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Subject</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topicId\">Topic Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"topicId\" class=\"form-control\" placeholder=\"Id\" ng-model=\"vm.form.topicId\" ng-change=\"vm.checkValidityOfID()\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.idExists\">\n\t\t\t\t\t\t\t\tTopic Id exists\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topicNumber\">Topic Number<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"topicNumber\" class=\"form-control\" placeholder=\"Topic Number\" ng-model=\"vm.form.topicNumber\" readonly=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topicname\">Topic Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"topicname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.topicName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topicDifficulty\">Difficulty level</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.form.topicDifficulty\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Difficulty...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.title\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item as item in (vm.difficultyLevel | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.title\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topicdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"topicdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.topicDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Topic</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" ng-disabled=\"vm.idExists\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchTopics()\" ng-if=\"!vm.topics || vm.topics.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTopicsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTopicsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Topics</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.topics\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.topics\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"topic in vm.topics\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeTopic(topic.topicId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editTopic(topic.topicId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{topic.topicNumber}} - {{topic.topicName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in topic\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreTopics()\" ng-if=\"(vm.topics==[] || vm.topics.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTopicsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTopicsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});