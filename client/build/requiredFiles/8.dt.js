webpackJsonp([8],{

/***/ 177:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {	__webpack_require__(112);
		angular.module(window.moduleName)
	        .component('slidesPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(178),
	            controller: AdminSlidesController,
	            controllerAs: 'vm'
	        });

		AdminSlidesController.$inject = ['admin','pageLoader','$filter','$scope','Upload','EditorConfig','$sce'];
		function AdminSlidesController(admin,pageLoader,$filter,$scope,Upload,EditorConfig,$sce){
			var vm = this;

			vm.EditorConfig = EditorConfig;
			vm.trustAsHTML = function(html){
				return $sce.trustAsHtml(html);
			}

			//Observer parent variable
			vm.$onInit = function() {
	        	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
	        		vm.courseList = v;
	        	});
		    };



		    //Observer files
			$scope.$watch('vm.files',function(newValue,oldValue){
				vm.isUploadCompleted = false;
				vm.fileNames = [];
				vm.slidesImageProgress = undefined;
				$('.slides-image-progress').width('0' + "%");
				angular.forEach(newValue, function(value, key){
					vm.fileNames[key] = value.name;
				});
			})

			//Observer showUploadImageForm to change file array
			$scope.$watch('vm.showUploadImageForm', function(newValue, oldValue, scope) {
				vm.files = undefined;
			});

			//Observe videoSlides array to sort it when it change
			$scope.$watch('vm.videoSlides', function(newValue, oldValue, scope) {
				vm.videoSlides.sort(function(a,b){
					return a.slideWeight - b.slideWeight;
				});
			});

			$scope.$watch('vm.form.blocks', function(newValue, oldValue, scope) {
				setTimeout(function(){
					Prism.highlightAll(true);
				},10);
			},true);

			$scope.$watch('vm.form.noOfBlocks', function(newValue, oldValue, scope) {
				if(vm.form){
					vm.form.blocks = [];
				}
			});

			$scope.$watch('vm.form.slideContent', function(newValue, oldValue, scope) {
				if(newValue === 'image'){
					vm.form.slideImage = vm.form.videoId + "_" + vm.form.slideNumber + ".png";
				}
			});
			//Render Code Preview based on Language
			$scope.$watch('vm.form.codeLanguage', function(newValue, oldValue, scope) {
				if(vm.form){
					vm.form.slideCode += " ";
				}
			});

			vm.videoSlides = [];
			vm.showForm = false;
			vm.codeLanguages = [
				{name : 'Java',value : 'java'},
				{name : 'JavaScript',value : 'javascript'},
				{name : 'Python',value : 'python'},
				{name : 'Plain Text',value : 'plain'}
			]


			vm.changeCourse = changeCourse;
			vm.changeModule = changeModule;
			vm.changeVideo = changeVideo;
			vm.newSlide = newSlide;
			vm.addVideoSlide=addVideoSlide;


			vm.deleteVideoSlide  = deleteVideoSlide;
			vm.editVideoSlide = editVideoSlide;
			vm.getNumber = getNumber;
			vm.uploadSlideImages = uploadSlideImages;

			vm.submitted = false;

			function getNumber(num) {
			    return Array.apply(null,{length:num});   
			}

			function editVideoSlide(id){
				vm.showForm = true;
				var item = $filter('filter')(vm.videoSlides,{slideId:id})[0];
				vm.form.slideNumber = item.slideNumber;
				vm.form.slideWeight = item.slideWeight;
				
				if (item.slideImage) {
					vm.form.slideContent = "image";
					vm.form.slideImage = item.slideImage;
					vm.form.codeLanguage = 'javascript';	
				}else if(item.slideBlocks){
					vm.form.slideContent = "code";
					vm.form.noOfBlocks = item.slideBlocks.length;
					
					setTimeout(function(){
						vm.form.blocks = item.slideBlocks;	
					},10);
				}
				vm.form.slideNotes= item.slideNotes;
				vm.form.slideId = item.slideId;
				vm.form.slideTitle = item.slideTitle;
				setTimeout(function(){
					console.log("edit");
					$('#slidedesc').froalaEditor(EditorConfig);

					slideDescription('');
					slideDescription(item.slideDesc);
				})
				
			}

			function deleteVideoSlide(id){
				var result = confirm("Want to delete?");
				if (!result) {
				    //Declined
				   	return;
				}
				pageLoader.show();
				admin.deleteVideoSlide({slideId:id,videoId:vm.form.videoId})
					.then(function(){
						//Update Slides Data
						updateSlidesData(vm.form.videoId);
					})
					.catch(function(err){console.error(err);})
					.finally(function(){
						pageLoader.hide();
					})
			}


			function addVideoSlide() {
				if(vm.form.slideContent === 'image'){
					//If Slide Content is Image
					vm.form.blocks = undefined;
				}else{
					//If Slide Content is Code
					vm.form.slideImage = undefined;
				}
				var slideData = {
					videoId : vm.form.videoId,
					topicTitle: vm.form.topicTitle,
					slideDesc: slideDescription(),
					slideDetail: {
						slideNumber: vm.form.slideNumber,
						slideWeight : vm.form.slideWeight,
						slideId: vm.form.videoId + "_" + vm.form.slideNumber,
						slideImage: vm.form.slideImage,
						slideBlocks : vm.form.blocks,
						slideTitle : vm.form.slideTitle,
						slideNotes: vm.form.slideNotes
					}
				}

				console.log("slide data.............",slideData);
				pageLoader.show();
				admin.addVideoSlide(slideData)
					.then(function(data){

						vm.form.error = false;
						vm.videoSlides = [];
						updateSlidesData(vm.form.videoId);
					})
					.catch(function(err){
						vm.form.error = true;
						console.error(err);
					})
					.finally(function(){
						vm.submitted = true;
						vm.showForm = false;
						pageLoader.hide();
					})

			}
			function newSlide(){
				vm.showForm = true;
				if(vm.videoSlides){
					vm.form.slideNumber = vm.videoSlides.length + 1;
					vm.form.slideWeight = vm.form.slideNumber;
				}else{
					vm.form.slideNumber = 1;
					vm.form.slideWeight = vm.form.slideNumber;
				}
				vm.form.slideContent = "image";
				vm.form.slideImage = vm.form.videoId + "_" + vm.form.slideNumber + ".png";
				vm.form.slideCode = undefined;
				vm.form.slideTitle = "";
				vm.form.slideNotes = [];
				vm.form.noOfSlideNotes = 1;

				setTimeout(function(){
					$('#slidedesc').froalaEditor(EditorConfig);
					slideDescription(' ');
				})
			}

			function changeCourse(){
				vm.moduleList = [];
				vm.videoList = [];
				vm.videoSlides = [];
				vm.showForm = false;
				vm.showUploadImageForm = false;
				if(vm.form.courseId){
					pageLoader.show();
					admin.getCourseModules({courseId:vm.form.courseId})
						.then(function(data){
							vm.moduleList = data;
						})
						.catch(function(err){console.error(err);})
						.finally(function(){pageLoader.hide();})
				}
			}

			function changeModule(){
				vm.videoList = [];
				vm.videoSlides = [];
				vm.showForm = false;
				vm.showUploadImageForm = false;
				if(vm.form.moduleId){
					pageLoader.show();
					admin.getModuleItems({moduleId:vm.form.moduleId})
						.then(function(data){
							vm.videoList = data;
						})
						.catch(function(err){console.error(err);})
						.finally(function(){pageLoader.hide();})
				}
			}

			function changeVideo(){
				vm.videoSlides = [];
				vm.showForm = false;
				if(vm.form.videoId){
					updateSlidesData(vm.form.videoId);
				}
			}

			function updateSlidesData(id){
				pageLoader.show();
				admin.getVideoSlides({videoId:id})
						.then(function(data){
							var slideData = data[0];

							vm.form.topicTitle = slideData.topicTitle;
							vm.form.slideDesc = slideData.slideDesc;
							vm.videoSlides = slideData.slideDetail;
							vm.form.slideContent = "image";
							vm.form.blocks = [];
						})
						.catch(function(err){
							console.error(err);
							vm.videoSlides = undefined;
						})
						.finally(function(){
							pageLoader.hide();
						})
			}

			function uploadSlideImages(){
				vm.isUploadCompleted = false;
				vm.isUploadGoingOn = true;
				Upload.upload({
					// 	/admin/uploadVideoSlideImages
					url: '/videoSlide/uploadVideoSlideImages',
				  	method: 'POST',
				  	data: {videoId:vm.form.videoId,files: vm.files},
				})
				.then(function(d){
					console.log(d);
				},function(err){
					console.error(err);
				},function(evt){
					var width = parseInt(100.0 * evt.loaded / evt.total);
					vm.slidesImageProgress = width + "%";
					$('.slides-image-progress').width(width + "%");
					if(width === 100){
						vm.isUploadCompleted = true;
					}
				})
				.finally(function(){
					vm.isUploadGoingOn = false;
					// vm.files = [];
				})
			}

			//set or get
			function slideDescription(desc){

				if(desc){
					$('#slidedesc').froalaEditor('html.set',desc);
					return;
				}
				return $('#slidedesc').froalaEditor('html.get',true);
			}

		}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 178:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page video-slide-page\">\n\t<div class=\"row\">\n\t\t<div ng-class=\"{'col-md-6':vm.showUploadImageForm,'col-md-12':!vm.showUploadImageForm}\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" name=\"form\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Slides</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"course\">Course</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"course\"  ng-model=\"vm.form.courseId\" ng-change=\"vm.changeCourse()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='course.courseId as course.courseName for course in vm.courseList'>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Course</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"module\">Module</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"module\" ng-model=\"vm.form.moduleId\" ng-change=\"vm.changeModule()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='module.moduleId as (module.moduleNumber + \" - \" + module.moduleName) for module in vm.moduleList'>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Module</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"video\">Video</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"video\" \n\t\t\t\t\t\t\t\t\tng-model=\"vm.form.videoId\" \n\t\t\t\t\t\t\t\t\tng-change=\"vm.changeVideo()\" \n\t\t\t\t\t\t\t\t\tclass=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='video.itemId as (video.itemNumber + \" - \" + video.itemName) for video in vm.videoList' required>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Video</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div ng-if=\"vm.showForm\">\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topictitle\">Topic Title</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\"  id=\"topictitle\" placeholder=\"Topic Title\" ng-model=\"vm.form.topicTitle\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slidedesc\">Slide Description</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<textarea id=\"slidedesc\" class=\"form-control\" placeholder=\"Slide Description\" ng-model=\"vm.form.slideDesc\"></textarea>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slidenumber\">Slide Number</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"slidenumber\" placeholder=\"Slide Number\" ng-model=\"vm.form.slideNumber\" readonly></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slideTitle\">Slide Title</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"slideTitle\" placeholder=\"Slide Title\" ng-model=\"vm.form.slideTitle\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slideweight\">Slide Weight</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"slideweight\" placeholder=\"Slide Weight to order slides\" ng-model=\"vm.form.slideWeight\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slidecontent\">Slide Content</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"slidecontent\" ng-model=\"vm.form.slideContent\" value=\"image\" class=\"\" id=\"slidecontent\" checked>Image</input>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"slidecontent\" ng-model=\"vm.form.slideContent\" value=\"code\" class=\"\" id=\"slidecontent\">Code</input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.form.slideContent === 'image'\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slideimage\">Slide Image</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"slideimage\" placeholder=\"Slide Image\" ng-model=\"vm.form.slideImage\" readonly></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.form.slideContent === 'code'\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"noofblocks\">No Of Blocks</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"noofblocks\" placeholder=\"No Of Blocks\" ng-model=\"vm.form.noOfBlocks\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-repeat=\"blockN in vm.getNumber(vm.form.noOfBlocks) track by $index\" ng-if=\"vm.form.slideContent === 'code'\">\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"language\">Language #{{$index + 1}}</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<select id=\"language\" ng-model=\"vm.form.blocks[$index]['codeLanguage']\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\t\tng-options='language.value as (language.name) for language in vm.codeLanguages' required>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"heading\">Heading #{{$index + 1}}</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"heading\" placeholder=\"Code Heading\" ng-model=\"vm.form.blocks[$index]['heading']\"></input>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.form.blocks[$index]['codeLanguage'] != 'plain'\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slidecode\">Code #{{$index + 1}}</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"slidecode\" placeholder=\"Your Code Here\" ng-model=\"vm.form.blocks[$index]['slideCode']\" rows=\"10\"></textarea>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.form.blocks[$index]['codeLanguage'] === 'plain'\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slidecode\">Code #{{$index + 1}}</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" froala=\"vm.EditorConfig\" id=\"slidecode\" placeholder=\"Your Code Here\" ng-model=\"vm.form.blocks[$index]['slideCode']\" rows=\"10\"></textarea>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"codepreview\">Preview #{{$index + 1}}</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t\t\t\t\t<pre><code ng-bind-html=\"vm.trustAsHTML(vm.form.blocks[$index]['slideCode'])\" class=\"line-numbers language-{{vm.form.blocks[$index]['codeLanguage']}}\" ></code></pre>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"noofslidenotes\">No of Slide Notes</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"noofslidenotes\" placeholder=\"No of SlideNotes\" ng-model=\"vm.form.noOfSlideNotes\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-repeat=\"slideN in vm.getNumber(vm.form.noOfSlideNotes) track by $index\">\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slideNotes\">Slide Notes - {{$index + 1}}</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<textarea id=\"slideNotes\" class=\"form-control\" placeholder=\"Slide Notes(Seperate Notes with new line)\" ng-model=\"vm.form.slideNotes[$index]\"></textarea>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.videoId\" ng-click=\"vm.showUploadImageForm = !vm.showUploadImageForm\" style=\"margin-top: 25px;\"><i class=\"fa fa-upload\"></i> Images</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.videoId\" ng-click=\"vm.newSlide()\" style=\"margin-top: 25px;\">New Slide</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.courseId || !vm.form.moduleId || !vm.form.videoId\" ng-click=\"vm.addVideoSlide()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- <div class=\"clearfix\" ng-class=\"{'col-md-6':vm.showUploadImageForm,'col-md-12':!vm.showUploadImageForm}\"> -->\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.videoSlides\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.videoSlides\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"slide in vm.videoSlides\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t<div class=\"\">\n\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteVideoSlide(slide.slideId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editVideoSlide(slide.slideId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t{{slide.slideNumber}} - {{slide.slideTitle}}\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in slide\">\n\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<!-- </div> -->\n\t\t</div>\n\t\t<div class=\"col-md-6\" ng-if=\"vm.showUploadImageForm\">\n\t\t\t<div class=\"upload-image-form\">\n\t\t\t\t<div class=\"title\">Uplaod Slide Images</div>\n\t\t\t\t<div class=\"file-select-wrapper\" ngf-drop ng-model=\"vm.files\"\n\t\t\t\t\tngf-pattern=\"'image/*'\" ngf-accept=\"'image/*'\"\n\t\t\t\t\tngf-max-size=\"15MB\" ngf-min-height=\"100\"\n\t\t\t\t\tngf-multiple=\"true\">\n\t\t\t\t\t<span class=\"btn-select\"\n\t\t\t\t\t\tngf-select ng-model=\"vm.files\" name=\"file\"\n\t\t\t\t\t\tngf-pattern=\"'image/*'\" ngf-accept=\"'image/*'\"\n\t\t\t\t\t\tngf-max-size=\"15MB\" ngf-min-height=\"100\"\n\t\t\t\t\tngf-multiple=\"true\">Select files</span>\n\t\t\t\t\t<div>Or</div>\n\t\t\t\t\t<div>Drag Files</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div style=\"margin-top: 20px;\" ng-if=\"vm.files.length > 0\">\n\t\t\t\t<span ng-class=\"{'disabled':vm.isUploadGoingOn}\" class=\"upload-btn btn btn-primary pull-right\" ng-click=\"vm.uploadSlideImages()\">Upload All</span>\n\t\t\t</div>\n\t\t\t<div class=\"clearfix\"></div>\n\t\t\t<div>{{vm.slidesImageProgress}}</div>\n\t\t\t<div class=\"slides-image-progress\"></div>\n\t\t\t<div class=\"upload-image-wrapper\" ng-if=\"!vm.isUploadCompleted\">\n\t\t\t\t<div class=\"slide-image\" ng-repeat=\"file in vm.files track by $index\">\n\t\t\t\t\t<img ngf-src=\"file\" ngf-resize=\"{width: 70 , height:70 }\">\n\t\t\t\t\t<span class=\"image-name\">{{vm.fileNames[$index]}}</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"upload-complete-msg\" ng-if=\"vm.isUploadCompleted\">\n\t\t\t\tSlides Uploaded\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});