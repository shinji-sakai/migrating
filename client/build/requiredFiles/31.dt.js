webpackJsonp([31],{

/***/ 396:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('currencyPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(397),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['$log','admin','$filter','$scope','$state'];
	function Controller($log,admin,$filter,$scope,$state){
		var vm = this;
	    vm.no_of_item_already_fetch=0;
		vm.saveCurrency = saveCurrency;
		vm.deleteCurrency = deleteCurrency;

		vm.$onInit = function(){
			//getAllCurrency();
		}

		function getAllCurrency(){
			admin.getAllCurrency()
				.then(function(res){
					vm.allCurrency = res.data
				})
		}
			vm.fetchCurrency = function(){
	            vm.allCurrency = [];
				vm.isCurrencyLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : 0,
	                collection : 'currency'
				})
				.then(function(res){
					$log.debug(res)
					vm.allCurrency = res.data;
					vm.no_of_item_already_fetch = 5;
	                vm.last_update_dt = vm.allCurrency[vm.allCurrency.length - 1].update_dt
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isCurrencyLoading = false;
				})
			}

			
			vm.fetchMoreAuthors = function(){
				vm.isCurrencyLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : vm.no_of_item_already_fetch,
	                collection : 'currency',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.allCurrency, {currency_id: v.currency_id})[0];
							if(!item){
								vm.allCurrency.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
	                    vm.last_update_dt = vm.allCurrency[vm.allCurrency.length - 1].update_dt
						
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isAuthorsLoading = false;
				})
			}

		function saveCurrency(){
			
	            if(!vm.formIsInEditMode){
	                vm.form.create_dt = new Date();
	                vm.form.update_dt = new Date();
	            }else{
	                vm.form.update_dt = new Date();
	            }
			admin.addCurrency(vm.form)
				.then(function(res){
					// getAllCurrency();
					 vm.fetchCurrency();
					vm.form = {};
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function deleteCurrency(id){
			admin.deleteCurrency({currency_id : id})
				.then(function(res){
					var item = $filter('filter')(vm.allCurrency || [], {currency_id: id})[0];								
						 if (vm.allCurrency.indexOf(item) > -1) {
	                        var pos = vm.allCurrency.indexOf(item);
	                       vm.allCurrency.splice(pos, 1);
	                    }
					//getAllCurrency();
				})
				.catch(function(err){
					console.log(err);
				})
		}
	}

/***/ },

/***/ 397:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" ng-submit=\"vm.saveCurrency()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Currency</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"currency name\">Currency<i style=\"color: red\">*</i> </label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"USD,INR,GBP....\" ng-model=\"vm.form.currency_nm\" required/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.form= {}\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchCurrency()\" ng-if=\"!vm.allCurrency || vm.allCurrency.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isCurrencyLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isCurrencyLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Currency</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.allCurrency\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allCurrency\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"curr in vm.allCurrency\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteCurrency(curr.currency_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t{{curr.currency_nm}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row skill-info-row\" ng-repeat=\"(key,value) in curr\">\n\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreCurrency()\" ng-if=\"(vm.allCurrency==[] || vm.allCurrency.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isCurrencyLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isCurrencyLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});