webpackJsonp([32],{

/***/ 398:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(399);
	__webpack_require__(112);
	angular.module(window.moduleName)
	    .component('gettingStartedPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(401),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
	function Controller(admin,$filter,$scope,$state,EditorConfig){

		var vm = this;
		vm.EditorConfig = EditorConfig;

		vm.addPointToList = addPointToList;
		vm.removePointFromList = removePointFromList;
		vm.addGettingStarted = addGettingStarted;
		vm.deleteGettingStarted = deleteGettingStarted;
		vm.editGettingStarted = editGettingStarted;
		vm.onTrainingChange = onTrainingChange;
		vm.resetForm = resetForm;

		vm.$onInit = function(){
			getAllCoursesFromMaster();
			getAllGettingStarted();

			setTimeout(function(){
				// $('#desc').froalaEditor(EditorConfig);
			})
		}

		function getAllCoursesFromMaster() {
			admin.coursemaster()
				.then(function(d){
					vm.coursesMaster = d;
				})
				.catch(function(err){
					console.error(err);
				});
		}

		function getAllGettingStarted() {
			admin.getAllGettingStarted()
				.then(function(res){
					vm.allGettingStarted = res.data;
				})
				.catch(function(err){
					console.error(err);
				});
		}

		function onTrainingChange() {
			if(vm.form.crs_id){
				var item = $filter("filter")(vm.allGettingStarted,{crs_id : vm.form.crs_id})[0];
				if(item){
					vm.form = item;
					// $('#desc').froalaEditor('html.set',item.desc);
				}
			}
		}

		function addGettingStarted() {
			admin.addGettingStarted(vm.form)
				.then(function(res){
					getAllGettingStarted();
					resetForm();
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function deleteGettingStarted(id) {
			admin.deleteGettingStarted({crs_id : id})
				.then(function(res){
					getAllGettingStarted();
					resetForm();
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function editGettingStarted(id) {
			var gs = $filter('filter')(vm.allGettingStarted,{crs_id : id})[0];
			vm.form = gs;
		}

		function resetForm(){
			vm.form = {};
			// $('#desc').froalaEditor('html.set','');
			vm.title = '';
		}


		function addPointToList(){
			vm.form = vm.form || {};
			vm.form.gettingStarted = vm.form.gettingStarted || [];
			if(vm.title){
				vm.form.gettingStarted.push({
					title : vm.title,
					desc : $('#desc').froalaEditor('html.get',true)
				})	
				vm.title = "";
				$('#desc').froalaEditor('html.set','');
			}
		}

		function removePointFromList(index){
			vm.form.gettingStarted.splice(index,1);
		}
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 399:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(400);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_gettingStarted.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_gettingStarted.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 400:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, ".question-title {\n  font-family: aller;\n  font-size: 17px;\n  font-weight: bold; }\n\n.question-ans {\n  font-family: aller;\n  font-size: 15px; }\n\n.gray-back {\n  background: #e5e5e5;\n  padding: 0px 10px; }\n\n.remove-btn {\n  position: absolute;\n  right: 5px;\n  top: 5px;\n  font-size: 15px;\n  cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 401:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Getting Started Points</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Training Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.crs_id\" id=\"course\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"course.courseId as course.courseName for course in vm.coursesMaster\" ng-change=\"vm.onTrainingChange()\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Training</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t<!-- <input type=\"text\" class=\"form-control\" placeholder=\"Course Name\" ng-model=\"vm.form.crs_nm\"/> -->\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"title\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<!-- <input type=\"text\" class=\"form-control\" placeholder=\"Enter Title\" ng-model=\"vm.title\"/> -->\n\t\t\t\t\t\t\t\t<textarea froala=\"vm.EditorConfig\" class=\"form-control\" style=\"margin-top: 5px;\" id=\"desc\" placeholder=\"Description\" ng-model=\"vm.form.desc\" ></textarea>\n\t\t\t\t\t\t\t\t<!-- <div style=\"margin: 10px 0px;\" ng-click=\"vm.addPointToList()\" class=\"btn btn-primary pull-right\">Add</div> -->\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<!-- <div class=\"form-group\" ng-repeat=\"point in vm.form.gettingStarted track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">{{$index+1}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<div class=\"question-title\">{{point[\"title\"]}}</div>\n\t\t\t\t\t\t\t\t<div class=\"question-ans\" ng-bind-html=\"point['desc']\"></div>\n\t\t\t\t\t\t\t\t<div class=\"remove-btn\" ng-click=\"vm.removePointFromList($index)\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.crs_id\" ng-click=\"vm.addGettingStarted()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allGettingStarted\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allGettingStarted\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"gs in vm.allGettingStarted\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteGettingStarted(gs.crs_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editGettingStarted(gs.crs_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{gs.crs_id}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in gs\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});