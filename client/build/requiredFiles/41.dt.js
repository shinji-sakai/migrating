webpackJsonp([41],{

/***/ 426:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('allLinksPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(427),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['$log','admin','$filter','$scope','$state'];
	function Controller($log,admin,$filter,$scope,$state){
		var vm = this;
		vm.form = vm.form || {};
	    vm.no_of_item_already_fetch = 0;
		vm.form.link_show_modal = "no";
		vm.form.link_login_required="no";
		vm.form.link_modal_type = "all-links"

		vm.saveLink = saveLink;
		vm.editLink = editLink;
		vm.deleteLink = deleteLink;

		vm.$onInit = function(){
		//	getAllLinks();
			getAllLinksCategory();
		}

		function getAllLinksCategory(){
				admin.getAllLinksCategories()
					.then(function(res){
						vm.allLinksCategory = res.data
					})
			}

		function getAllLinks(){
			admin.getAllLinks()
				.then(function(res){
					vm.allLinks = res.data
				})
		}
		vm.fetchAllLinks = function(){
				vm.isLinksLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
				collection : 'allLinks'
				})
				.then(function(res){
					$log.debug(res)
					vm.allLinks= res.data;
					vm.no_of_item_already_fetch = 5;
					vm.last_update_dt = vm.allLinks[vm.allLinks.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isLinksLoading = false;
				})
			}
			
			vm.fetchMoreLinks = function(){
				vm.isLinksLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
				collection : 'allLinks',
					  update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.allLinks, {link_id: v.link_id})[0];
							if(!item){
								vm.allLinks.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						vm.last_update_dt = vm.allLinks[vm.allLinks.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isLinksLoading = false;
				})
			}

		function saveLink(){
	       	if(!vm.formIsInEditMode){
	                vm.form.create_dt = new Date();
	                vm.form.update_dt = new Date();
	            }else{
	                vm.form.update_dt = new Date();
	            }
			admin.addLinkToAllLinks(vm.form)
				.then(function(res){
		           	vm.fetchAllLinks();
					//getAllLinks();
					vm.form = {};
					vm.formIsInEditMode=false;
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function editLink(id){
			var link = $filter('filter')(vm.allLinks,{link_id : id})[0];
			delete link._id;
			vm.form = link;
			vm.formIsInEditMode=true;
		}

		function deleteLink(id){
			admin.deleteLinkFromAllLinks({link_id : id})
				.then(function(res){
						var item = $filter('filter')(vm.allLinks || [], {link_id: id})[0];								
						 if (vm.allLinks.indexOf(item) > -1) {
	                        var pos = vm.allLinks.indexOf(item);
	                       vm.allLinks.splice(pos, 1);
	                    }
					//getAllLinks();
				})
				.catch(function(err){
					console.log(err);
				})
		}
	}

/***/ },

/***/ 427:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" ng-submit=\"vm.saveLink()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>All Links</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course-sub-group\">All Links Category<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.link_cat\" id=\"course-sub-group\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"cat.cat_id as cat.cat_nm for cat in vm.allLinksCategory \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Category</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"LinkTitle\">Link Title<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"LinkTitle\" placeholder=\"Link Title\" ng-model=\"vm.form.link_title\" required/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"LinkUrl\">Link Url<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"LinkUrl\" placeholder=\"Link Url\" ng-model=\"vm.form.link_url\" required/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"afterLogin\">Show Only After Login</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\" style=\"margin-top: 6px;\">\n\t\t\t\t\t\t\t\t<input type=\"checkbox\" ng-disabled=\"vm.form.link_before_login\" class=\"\" id=\"afterLogin\" name=\"afterLogin\" value=\"yes\" ng-model=\"vm.form.link_after_login\"/>Yes\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"beforeLogin\">Show Only Before Login</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\" style=\"margin-top: 6px;\">\n\t\t\t\t\t\t\t\t<input type=\"checkbox\" ng-disabled=\"vm.form.link_after_login\" class=\"\" id=\"beforeLogin\" name=\"beforeLogin\" value=\"yes\" ng-model=\"vm.form.link_before_login\"/>Yes\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"shoModal\">Show Modal</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\" style=\"margin-top: 6px;\">\n\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"\" id=\"shoModal\" name=\"showmodal\" value=\"no\" placeholder=\"Link Url\" ng-model=\"vm.form.link_show_modal\" checked=\"checked\" />No\n\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"\" id=\"shoModal\" name=\"showmodal\" value=\"yes\" placeholder=\"Link Url\" ng-model=\"vm.form.link_show_modal\"/>Yes\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.form.link_show_modal === 'yes'\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"link_modal_type\">Modal Type</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"link_modal_type\" placeholder=\"Link Modal Type\" ng-model=\"vm.form.link_modal_type\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.form= {}\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" \n\t\t\t\t\t\t\t\t type=\"submit\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchAllLinks()\" ng-if=\"!vm.allLinks || vm.allLinks.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isLinksLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isLinksLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Links</span>\n\t\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.allLinks\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allLinks\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"link in vm.allLinks\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteLink(link.link_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editLink(link.link_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t{{link.link_title}} - {{link.link_url}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"skill-info-row\">\n\t\t\t\t\t<div class=\"col-xs-2 title\">Preview Link</div>\n\t\t\t\t\t<div class=\"col-xs-10 desc\">\n\t\t\t\t\t\t<a ng-href=\"{{link.link_url}}\" target=\"_blank\">Preview Link</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row skill-info-row\" ng-repeat=\"(key,value) in link\">\n\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t\n\t\t</div>\n\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreLinks()\" ng-if=\"(vm.allLinks==[] || vm.allLinks.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isLinksLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isLinksLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});