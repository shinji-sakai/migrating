webpackJsonp([1],Array(119).concat([
/* 119 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(120);
	__webpack_require__(57);
	__webpack_require__(121);
	__webpack_require__(122);
	__webpack_require__(123);
	__webpack_require__(124);
	__webpack_require__(147);
	__webpack_require__(148);
	__webpack_require__(149);

	angular.module(window.moduleName)
	    .component('adminPage', {
	        template: __webpack_require__(163),
	        controller: AdminController,
	        controllerAs: 'vm'
	    })

	AdminController.$inject = ['$scope','admin', 'authToken'];

	function AdminController($scope,admin, authToken) {
	    var vm = this;
	    vm.isUserAuthorized = false;

	    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
	        vm.hideSidebar = false;
	        vm.isUserAuthorized = false;
	        checkForAuthorization(toState.url.substring(1));
	        vm.form_url = toState.url.substring(1);
	    });

	    $scope.$watch(function(){
	        return vm.hideSidebar;
	    },function(v){
	    });


	    vm.$onInit = function() {
	        vm.testing = "testing";
	        vm.hideSidebar = true;


	        var pos = window.location.pathname.indexOf("/",2);
	        var string_path = window.location.pathname.substring(pos+1);
	        vm.form_url = string_path;
	        checkForAuthorization(string_path);

	        //get coursemaster from db
	        admin.coursemaster()
	            .then(function(d) {
	                vm.coursemaster = d;
	            })
	            .catch(function(err) {
	                console.error(err);
	            });

	        //get courselist from db
	        admin.courselistfull()
	            .then(function(d) {
	                vm.courselistdata = d;
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }

	    function  checkForAuthorization(path) {
	        if(!authToken.isAuthenticated()){
	            vm.isUserAuthorized = false;
	            return;
	        }
	        var userRoles = authToken.getUserRoles();
	        if(userRoles.indexOf("super-admin") > -1){
	            vm.isUserAuthorized = true;
	        }else if(userRoles.indexOf("admin") > -1){
	            vm.isUserAuthorized = false;
	            admin.getUserAdminRoleFormAccessList({usr_id : authToken.getUserId()})
	                .then(function(res) {
	                    var authorizedLinks = res.data;
	                    if(authorizedLinks.indexOf(path) > -1){
	                        vm.isUserAuthorized = true;            
	                    }else{
	                        vm.isUserAuthorized = false;
	                    }
	                })
	                .catch(function(err) {
	                    console.error(err);
	                })
	        }else if(userRoles.indexOf("developer") > -1){
	            vm.isUserAuthorized = false;
	        }
	    }
	}

/***/ },
/* 120 */
/***/ function(module, exports) {

	  angular
	    .module('service')
	    .service('admin', admin);

	admin.$inject = ['$http','BASE_API'];

	function admin($http,BASE_API) {


	    this.isIdExist = function(data) {
	        return $http.post(BASE_API + '/admin/general/isIdExist',data);
	    }


	    this.fetchItemUsingLimit = function(data) {
	        return $http.post(BASE_API + '/admin/general/fetchItemUsingLimit',data);
	    }

	    /* Course Master API */

	    this.coursemaster = function() {
	        //  /admin/coursemaster
	        return $http.post(BASE_API + '/courseMaster/getAll').then(function(res) {
	            return res.data;
	        });
	    }

	    

	    this.addCourseMaster = function(data) {
	        //      /admin/addCourseMaster
	        return $http.post(BASE_API + '/courseMaster/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseMaster = function(data) {
	        //      /admin/deleteCourseMaster
	        return $http.post(BASE_API + '/courseMaster/delete', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.fetchCourseMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/courseMaster/fetchCourseMasterUsingLimit', data);
	    }

	    this.getRelatedCoursesByCourseId = function(data) {
	        return $http.post(BASE_API + '/courseMaster/getRelatedCoursesByCourseId',data);
	    }

	    /* Course API */
	    this.courselistfull = function(data) {
	        return $http.post(BASE_API + '/course/getAll',data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourse = function(data) {
	        // /admin/addCourse
	        return $http.post(BASE_API + '/course/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.removeCourse = function(data) {
	        //  /admin/removeCourse
	        return $http.post(BASE_API + '/course/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Videos API */
	    this.addVideos = function(data) {
	        //  /admin/addVideos
	        return $http.post(BASE_API + '/courseVideos/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getModuleVideos = function(data) {
	        //  /admin/getModuleVideos
	        return $http.post(BASE_API + '/courseVideos/getModuleVideos', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getCourseVideos = function(data) {
	        //      /admin/getCourseVideos
	        return $http.post(BASE_API + '/courseVideos/getCourseVideos', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Modules API */
	    this.getCourseModules = function(data) {
	        //      /admin/getCourseModules
	        return $http.post(BASE_API + '/courseModule/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourseModule = function(data) {
	        //  /admin/addCourseModule
	        return $http.post(BASE_API + '/courseModule/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseModule = function(data) {
	        //  /admin/deleteCourseModule
	        return $http.post(BASE_API + '/courseModule/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Video Slides API */
	    this.addVideoSlide = function(data) {
	        //  /admin/addVideoSlide
	        return $http.post(BASE_API + '/videoSlide/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getVideoSlides = function(data) {
	        //  /admin/getVideoSlides
	        return $http.post(BASE_API + '/videoSlide/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteVideoSlide = function(data) {
	        //  /admin/deleteVideoSlide
	        return $http.post(BASE_API + '/videoSlide/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Authorized Items API */

	    this.getAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/getItems',data);
	    }

	    this.updateAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/updateItems',data);
	    }

	    this.addAuthorizedItem = function(data){
	        return $http.post(BASE_API + '/ai/addItem',data);
	    }

	    this.getAuthorizedVideoItemFullInfo = function(data){
	        return $http.post(BASE_API + '/ai/getVideoItemFullInfo',data);
	    }
	    this.getVideoPermission = function(data){
	        return $http.post(BASE_API + '/ai/getVideoPermission',data);
	    }


	    /* practice Question API */

	    this.savePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestion',data);
	    }
	    this.savePreviewPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/savePreviewQuestion',data);
	    }
	    this.saveQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestionStats',data);
	    }
	    this.saveTestQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveTestQuestionStats',data);
	    }
	    this.saveTestReviews  = function(data){
	        return $http.post(BASE_API + '/practice/saveTestReviews',data);
	    }
	    this.removePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/removeQuestion',data);
	    }
	    this.getAllPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getAllQuestions',data);
	    }
	    this.findPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsByQuery',data);
	    }
	    this.findPracticeQuestionsIdByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsIdByQuery',data);
	    }
	    this.findPracticeQuestionsTextByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsTextByQuery',data);
	    }
	    this.findPreviewPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findPreviewQuestionsByQuery',data);
	    }
	    this.getPracticeQuestionsById = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsById',data);
	    }

	    this.getPracticeQuestionsByLimit = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsByLimit',data);
	    }

	    this.updateMultiplePracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/updateMultiplePracticeQuestions',data);
	    }
	    this.getAllAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersQuestionPerfStats',data);
	    }
	    this.getAllMonthAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllMonthAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersMonthQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersMonthQuestionPerfStats',data);
	    }
	    this.updateVerifyPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/updateVerifyPracticeQuestion',data);
	    }
	    this.getAllVerifyStatusOfPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getAllVerifyStatusOfPracticeQuestion',data);
	    }
	    this.getLatestVerifyStatusOfGivenPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getLatestVerifyStatusOfGivenPracticeQuestions',data);
	    }
	    this.getUserMonthQuestionAttemptStats = function(data){
	        return $http.post(BASE_API + '/practice/getUserMonthQuestionAttemptStats',data);
	    }
	    this.startUserExam = function(data){
	        return $http.post(BASE_API + '/practice/startUserExam',data);
	    }
	    this.finishUserExam = function(data){
	        return $http.post(BASE_API + '/practice/finishUserExam',data);
	    }
	    this.checkForPreviousExam = function(data){
	        return $http.post(BASE_API + '/practice/checkForPreviousExam',data);
	    }
	    this.getUserExamQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getUserExamQuestion',data);
	    }
	    this.saveUserExamQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserExamQuestionData',data);
	    }
	    this.startUserPractice = function(data){
	        return $http.post(BASE_API + '/practice/startUserPractice',data);
	    }
	    this.saveUserPracticeQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserPracticeQuestionData',data);
	    }
	    this.fetchNextUserPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/fetchNextUserPracticeQuestion',data);
	    }
	    this.getPracticeNodeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeNodeQuestions',data);
	    }






	    /* Highlight APi */

	    this.saveHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/save',data);
	    }
	    this.getQuestionHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getQuestionHighlights',data);
	    }
	    this.getUserHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getUserHighlights',data);
	    }
	    this.deleteHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/delete',data);
	    }

	    this.updateHighlightNote = function(data){
	        return $http.post(BASE_API + '/highlight/updateHighlightNote',data);
	    }

	    /*
	    Author API
	     */
	    this.getAllAuthor  = function(){
	        return $http.post(BASE_API + '/author/getAll');
	    }
	    this.updateAuthor  = function(data){
	        return $http.post(BASE_API + '/author/update',data);
	    }
	    this.removeAuthor  = function(data){
	        return $http.post(BASE_API + '/author/remove',data);
	    }
	    this.fetchAuthorUsingLimit=function(data){
	        return $http.post(BASE_API+'/author/fetchAuthorUsingLimit',data);
	    }
	    this.AuthorsIdIsExits=function(data){
	        return $http.post(BASE_API+'/author/AuthorsIdIsExits',data);
	    }

	    /*
	    Publication API
	     */
	    this.getAllPublication  = function(){
	        return $http.post(BASE_API + '/publication/getAll');
	    }
	    this.updatePublication  = function(data){
	        return $http.post(BASE_API + '/publication/update',data);
	    }
	    this.removePublication  = function(data){
	        return $http.post(BASE_API + '/publication/remove',data);
	    }
	   this.fetchPublicationsUsingLimit=function(data){
	       return $http.post(BASE_API+'/publication/fetchPublicationsUsingLimit',data);
	   }
	    /*
	    Book API
	     */
	    this.getAllBook  = function(){
	        return $http.post(BASE_API + '/book/getAll');
	    }
	    this.updateBook  = function(data){
	        return $http.post(BASE_API + '/book/update',data);
	    }
	    this.removeBook  = function(data){
	        return $http.post(BASE_API + '/book/remove',data);
	    }
	    this.fetchBooksUsingLimit=function(data){
	        return $http.post(BASE_API+'/book/fetchBooksUsingLimit',data);
	    }

	    /*
	    JobOpenings API
	     */
	    this.getAllJobOpenings  = function(){
	        return $http.post(BASE_API + '/jobOpenings/getAllJobOpenings');
	    }
	    this.updateJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/updateJobOpening',data);
	    }
	    this.removeJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/removeJobOpening',data);
	    }

	    /*
	    Subject API
	     */
	    this.getAllSubject  = function(){
	        return $http.post(BASE_API + '/subject/getAll');
	    }
	    this.updateSubject  = function(data){
	        return $http.post(BASE_API + '/subject/update',data);
	    }
	    this.removeSubject  = function(data){
	        return $http.post(BASE_API + '/subject/remove',data);
	    }
	   this.fetchSubjectMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/subject/fetchSubjectMasterUsingLimit',data);
	    }
	    this.SubjectIdIsExits=function(data){
	        return $http.post(BASE_API+'/subject/SubjectIdIsExits',data);
	    }
	    /*
	    Exam API
	     */
	    this.getAllExam  = function(){
	        return $http.post(BASE_API + '/exam/getAll');
	    }
	    this.updateExam  = function(data){
	        return $http.post(BASE_API + '/exam/update',data);
	    }
	    this.removeExam  = function(data){
	        return $http.post(BASE_API + '/exam/remove',data);
	    }
	    this.fetchExamUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/exam/fetchExamUsingLimit',data);
	    }
	    /*
	    Topic API
	     */
	    this.getAllTopic  = function(data){
	        return $http.post(BASE_API + '/topic/getAll',data);
	    }
	    this.updateTopic  = function(data){
	        return $http.post(BASE_API + '/topic/update',data);
	    }
	    this.removeTopic  = function(data){
	        return $http.post(BASE_API + '/topic/remove',data);
	    }
	   this.fetchTopicsUsingLimit=function(data)
	   {
	       return $http.post(BASE_API+'/topic/fetchTopicsUsingLimit',data);
	   }
	    /*
	    Topic Group API
	     */
	    this.getAllTopicGroups  = function(data){
	        return $http.post(BASE_API + '/topicGroup/getAllTopicGroups',data);
	    }
	    this.updateTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/updateTopicGroup',data);
	    }
	    this.removeTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/removeTopicGroup',data);
	    }
	    this.fetchTopicGroupUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/topicGroup/fetchTopicGroupUsingLimit',data);
	    }
	    /*
	    Tag API
	     */
	    this.getAllTag  = function(){
	        return $http.post(BASE_API + '/tag/getAll');
	    }
	    this.updateTag  = function(data){
	        return $http.post(BASE_API + '/tag/update',data);
	    }
	    this.removeTag  = function(data){
	        return $http.post(BASE_API + '/tag/remove',data);
	    }
	    this.fetchTagsUsingLimit=function(data){
	        return $http.post(BASE_API+'/tag/fetchTagsUsingLimit',data);
	    }
	    this.TagsIdIsExits=function(data)
	    {
	        return $http.post(BASE_API+'/tag/TagsIdIsExits',data);
	    }

	    /*
	    SubTopic API
	     */
	    this.getAllSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/getAll',data);
	    }
	    this.updateSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/update',data);
	    }
	    this.removeSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/remove',data);
	    }

	    /* videoentity api */
	    this.updateVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/update',data);
	    }
	    this.removeVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/remove',data);
	    }
	    this.getAllVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/getAll',data);
	    }
	    this.isVideoFilenameExist = function(data){
	        return $http.post(BASE_API + '/videoentity/isVideoFilenameExist',data);
	    }
	    this.findVideoEntityByQuery = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityByQuery',data);
	    }
	            //One Id
	    this.findVideoEntityById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityById',data);
	    }
	            //Multiple Ids
	    this.findVideoEntitiesById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntitiesById',data);
	    }
	    this.getRelatedTopicsVideoByVideoId = function(data){
	        return $http.post(BASE_API + '/videoentity/getRelatedTopicsVideoByVideoId',data);
	    }
	    this.getRelatedVideosByVideoId = function(data) {
	        return $http.post(BASE_API + '/videoentity/getRelatedVideosByVideoId',data);
	    }
	    this.updateVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/updateVideoLastViewTime',data);
	    }
	    this.getVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/getVideoLastViewTime',data);
	    }


	    /* moduleItems api */
	    this.updateModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/update',data);
	    }
	    this.getAllModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/getAll',data);
	    }
	    this.getCourseItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getCourseItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getFullCourseDetails = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getFullCourseDetails', data);
	    }
	    
	    this.getModuleItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getModuleItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getUserBatchEnrollUploads = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getUserBatchEnrollUploads',data);
	    }

	    /* createtest api */
	    this.createTest = function(data){
	        return $http.post(BASE_API + '/createtest/save',data);
	    }
	    this.getAllTest = function(data){
	        return $http.post(BASE_API + '/createtest/getAll',data);
	    }
	    this.getTestById = function(data){
	        return $http.post(BASE_API + '/createtest/getById',data);
	    }

	    /*
	        CourseBundle API
	     */
	    this.getAllCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/getAll',data);
	    }
	    this.updateCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/update',data);
	    }
	    this.removeCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/remove',data);
	    }



	    // Employee types api
	    this.addEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/add',data);
	    }
	    this.getAllEmployeeTypes  = function(data){
	        return $http.post(BASE_API + '/employee/types/getAll',data);
	    }
	    this.deleteEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/delete',data);
	    }

	    // Employee Details api
	    this.addEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/add',data);
	    }
	    this.getAllEmployees  = function(data){
	        return $http.post(BASE_API + '/employee/getAll',data);
	    }
	    this.deleteEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/delete',data);
	    }

	    // Employee Skills
	    this.addEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/add',data);
	    }
	    this.getAllEmployeeSkills  = function(data){
	        return $http.post(BASE_API + '/employee/skills/getAll',data);
	    }
	    this.deleteEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/delete',data);
	    }

	    // All links category
	    this.addAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/add',data);
	    }
	    this.getAllLinksCategories  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/getAll',data);
	    }
	    this.deleteAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/delete',data);
	    }


	    // Sidelinks article
	    this.addSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/update',data);
	    }
	    this.getAllSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getAll',data);
	    }
	    this.getSideLinkArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/get',data);
	    }
	    this.getMultipleSideLinkArticles = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getMultipleArticles',data);
	    }
	    this.deleteSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/remove',data);
	    }


	    // All Links
	    this.addLinkToAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/update',data);
	    }
	    this.getAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/getAll',data);
	    }
	    this.deleteLinkFromAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/remove',data);
	    }


	    // BoardCompetitiveCourse
	    this.updateBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/update',data);
	    }
	    this.getBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/get',data);
	    }
	    this.getBoardCompetitiveCoursesByCourseType  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/getCoursesByCourseType',data);
	    }
	    this.deleteBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/remove',data);
	    }


	    // Course Subgroup
	    this.addSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/add',data);
	    }
	    this.getAllSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/getAll',data);
	    }
	    this.deleteSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/delete',data);
	    }

	    // Save Batch Course
	    this.addBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/add',data);
	    }
	    this.getAllBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getAll',data);
	    }
	    this.deleteBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/delete',data);
	    }
	    this.getBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getBatchCourse',data);
	    }

	    // Save Batch Timing
	    this.addBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/add',data);
	    }
	    this.getAllBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getAll',data);
	    }
	    this.deleteBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/delete',data);
	    }
	    this.getTimingByCourseId = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getTimingByCourseId',data);
	    }


	    // Save Batch Timing Dashboard
	    this.addBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/addBatchTimingDashboard',data);
	    }
	    this.getAllBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/getAllBatchTimingDashboard',data);
	    }
	    this.deleteBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/deleteBatchTimingDashboard',data);
	    }
	    this.setBatchTimingDashboardAsCompleted  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/setBatchTimingDashboardAsCompleted',data);
	    }

	    //

	    // Currency
	    this.addCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/add',data);
	    }
	    this.getAllCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/getAll',data);
	    }
	    this.deleteCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/delete',data);
	    }

	    // Getting STarted
	    this.addGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/add',data);
	    }
	    this.getAllGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getAll',data);
	    }
	    this.deleteGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/delete',data);
	    }
	    this.getGettingStartedByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getByCourseId',data);
	    }

	    // Sessions
	    this.addSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/add',data);
	    }
	    this.getAllSessions  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getAll',data);
	    }
	    this.deleteSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/delete',data);
	    }
	    this.getSessionsByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getByCourseId',data);
	    }

	    // Sessions Items
	    this.addSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/add',data);
	    }
	    this.getAllSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getAll',data);
	    }
	    this.deleteSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/delete',data);
	    }
	    this.getSessionDetailsForBatchAndSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getSessionDetailsForBatchAndSession',data);
	    }


	    // Pricelist Course
	    this.savePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/add',data);
	    }
	    this.getAllPricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/getAll',data);
	    }
	    this.deletePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/delete',data);
	    }
	    this.getDetailedPricelist = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelist',data);
	    }
	    this.getPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getPricelistByCourseId',data);
	    }
	    this.getDetailedPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelistByCourseId',data);
	    }


	    // Bundle
	    this.saveBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/add',data);
	    }
	    this.getAllBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/getAll',data);
	    }
	    this.deleteBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/delete',data);
	    }

	    // Email Templates
	    this.addEmailTemplates  = function(data){
	        return $http.post(BASE_API + '/emailTemplates/addEmailTemplates',data);
	    }

	    this.getAllEmailTemplates = function(data){
	        return $http.post(BASE_API + '/emailTemplates/getAllEmailTemplates',data);
	    }

	    this.deleteEmailTemplate = function(data){
	        return $http.post(BASE_API + '/emailTemplates/deleteEmailTemplate',data);
	    }

	    // UserEmail Group
	    this.addUserEmailGroup  = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/addUserEmailGroup',data);
	    }

	    this.getAllUserEmailGroups = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/getAllUserEmailGroups',data);
	    }

	    this.deleteUserEmailGroup = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/deleteUserEmailGroup',data);
	    }

	    // Send Email Templates
	    this.addSendEmailTemplate  = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/addSendEmailTemplate',data);
	    }

	    this.getAllSendEmailTemplates = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/getAllSendEmailTemplates',data);
	    }

	    this.deleteSendEmailTemplate = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/deleteSendEmailTemplate',data);
	    }

	    this.sendEmailTemplatesToUsers = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendEmailTemplatesToUsers',data);
	    }

	    this.sendNotificationOfEmail = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendNotificationOfEmail',data);
	    }

	    // Send Notification
	    this.addSendNotification  = function(data){
	        return $http.post(BASE_API + '/sendNotification/addSendNotification',data);
	    }

	    this.getAllSendNotifications = function(data){
	        return $http.post(BASE_API + '/sendNotification/getAllSendNotifications',data);
	    }

	    this.deleteSendNotification = function(data){
	        return $http.post(BASE_API + '/sendNotification/deleteSendNotification',data);
	    }

	    this.getSendNotificationByUserId = function(data){
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationByUserId',data);
	    }

	    this.markSendNotificationAsRead = function(data){
	        return $http.post(BASE_API + '/sendNotification/markSendNotificationAsRead',data);
	    }

	    this.getSendNotificationCount = function(data) {
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationCount',data);
	    }


	    // Roles
	    this.addRole  = function(data){
	        return $http.post(BASE_API + '/adminRoles/addRole',data);
	    }

	    this.getAllRoles = function(data){
	        return $http.post(BASE_API + '/adminRoles/getAllRoles',data);
	    }

	    this.deleteRole = function(data){
	        return $http.post(BASE_API + '/adminRoles/deleteRole',data);
	    }


	    // forms
	    this.addForm  = function(data){
	        return $http.post(BASE_API + '/adminForms/addForm',data);
	    }

	    this.getAllForms = function(data){
	        return $http.post(BASE_API + '/adminForms/getAllForms',data);
	    }

	    this.deleteForm = function(data){
	        return $http.post(BASE_API + '/adminForms/deleteForm',data);
	    }

	    // forms  help
	    this.addFormHelp  = function(data){
	        return $http.post(BASE_API + '/formsHelp/updateFormHelp',data);
	    }

	    this.getAllFormsHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getAllFormsHelp',data);
	    }

	    this.getFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getFormHelp',data);
	    }

	    this.deleteFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/deleteFormHelp',data);
	    }

	    // user roles
	    this.addUserRole  = function(data){
	        return $http.post(BASE_API + '/userRoles/addUserRole',data);
	    }

	    this.getAllUserRoles = function(data){
	        return $http.post(BASE_API + '/userRoles/getAllUserRoles',data);
	    }

	    this.deleteUserRole = function(data){
	        return $http.post(BASE_API + '/userRoles/deleteUserRole',data);
	    }

	    this.getUserAdminRoleFormAccessList = function(data) {
	        return $http.post(BASE_API + '/userRoles/getUserAdminRoleFormAccessList',data);
	    }

	    // user batch enroll
	    this.getUserEnrollAllBatch = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/getUserEnrollAllBatch',data);
	    }
	    this.addUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/addUserBatchEnroll',data);
	    }
	    this.removeUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/removeUserBatchEnroll',data);
	    }



	    this.updateBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/updateBookTopic',data);
	    }
	    this.removeBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/removeBookTopic',data);
	    }
	    this.getAllBookTopics = function(data) {
	        return $http.post(BASE_API + '/booktopic/getAllBookTopics',data);
	    }
	    this.findBookTopicsByQuery = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsByQuery',data);
	    }
	    this.findBookTopicById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicById',data);
	    }
	    this.findBookTopicsById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsById',data);
	    }


	    this.updatePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/update',data);
	    }
	    this.removePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/remove',data);
	    }
	    this.getAllPreviousPapers = function(data) {
	        return $http.post(BASE_API + '/previousPapers/getAll',data);
	    }
	    this.getPreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/get',data);
	    }


	    //Save Purchased Course Based on COurse ids
	    this.purchasedCourse = function(data) {
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnCourseId',data);
	    }
	    //Save purchased courses based on bundle ids
	    this.purchasedBundle = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnBundleId',data);
	    }
	    //save purchased courses based on training ids
	    this.purchasedTraining = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnTrainingId',data);
	    }
	}


/***/ },
/* 121 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {angular
		.module('service')
		.service('pageLoader',PageLoader);

	PageLoader.$inject = ['$compile','$rootScope'];	
	function PageLoader($compile,$rootScope){	


		this.show = function(){
			var el = $compile("<page-loading></page-loading>")($rootScope);
			// $("body").css("overflow","hidden");
			$("body>div").append(el);
		}

		this.hide = function(){
			// $("body").css("overflow","auto");
			$(".page-spinner").remove();
		}

	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 122 */
/***/ function(module, exports) {

	    angular
	        .module("directive")
	        .directive('pageLoading', PageLoading)

	    PageLoading.$inject = [];

	    function PageLoading() {

	        return {
	            restrict: 'E',
	            replace: true,
	            // template: '<div class="page-spinner">' +
	            //     '<div class="loader-wrapper">' +
	            //     '<div class="sk-fading-circle">' +
	            //     '<div class="sk-circle1 sk-circle"></div>' +
	            //     '<div class="sk-circle2 sk-circle"></div>' +
	            //     '<div class="sk-circle3 sk-circle"></div>' +
	            //     '<div class="sk-circle4 sk-circle"></div>' +
	            //     '<div class="sk-circle5 sk-circle"></div>' +
	            //     '<div class="sk-circle6 sk-circle"></div>' +
	            //     '<div class="sk-circle7 sk-circle"></div>' +
	            //     '<div class="sk-circle8 sk-circle"></div>' +
	            //     '<div class="sk-circle9 sk-circle"></div>' +
	            //     '<div class="sk-circle10 sk-circle"></div>' +
	            //     '<div class="sk-circle11 sk-circle"></div>' +
	            //     '<div class="sk-circle12 sk-circle"></div>' +
	            //     '</div>' +
	            //     '</div>' +
	            //     '</div>'
	            template: '<div class="page-spinner">' +
	                '<div class="loader-wrapper">' +
	                '<div class="dot-loader">' +
	                '</div>' +
	                '</div>' +
	                '</div>'
	        }
	    }

/***/ },
/* 123 */
/***/ function(module, exports) {

	/**
	 * Binds a TinyMCE widget to <textarea> elements.
	 */
	angular.module('ui.tinymce', [])
	  .value('uiTinymceConfig', {})
	  .directive('uiTinymce', ['$rootScope', '$compile', '$timeout', '$window', '$sce', 'uiTinymceConfig', function($rootScope, $compile, $timeout, $window, $sce, uiTinymceConfig) {
	    uiTinymceConfig = uiTinymceConfig || {};
	    var generatedIds = 0;
	    var ID_ATTR = 'ui-tinymce';
	    if (uiTinymceConfig.baseUrl) {
	      tinymce.baseURL = uiTinymceConfig.baseUrl;
	    }

	    return {
	      require: ['ngModel', '^?form'],
	      priority: 599,
	      link: function(scope, element, attrs, ctrls) {
	        if (!$window.tinymce) {
	          return;
	        }

	        var ngModel = ctrls[0],
	          form = ctrls[1] || null;

	        var expression, options = {
	          debounce: true
	        }, tinyInstance,
	          updateView = function(editor) {
	            var content = editor.getContent({format: options.format}).trim();
	            content = $sce.trustAsHtml(content);

	            ngModel.$setViewValue(content);
	            if (!$rootScope.$$phase) {
	              scope.$digest();
	            }
	          };

	        function toggleDisable(disabled) {
	          if (disabled) {
	            ensureInstance();

	            if (tinyInstance) {
	              tinyInstance.getBody().setAttribute('contenteditable', false);
	            }
	          } else {
	            ensureInstance();

	            if (tinyInstance && !tinyInstance.settings.readonly) {
	              tinyInstance.getBody().setAttribute('contenteditable', true);
	            }
	          }
	        }

	        // generate an ID
	        attrs.$set('id', ID_ATTR + '-' + generatedIds++);

	        expression = {};

	        angular.extend(expression, scope.$eval(attrs.uiTinymce));

	        //Debounce update and save action
	        var debouncedUpdate = (function(debouncedUpdateDelay) {
	          var debouncedUpdateTimer;
	          return function(ed) {
		        $timeout.cancel(debouncedUpdateTimer);
		         debouncedUpdateTimer = $timeout(function() {
	              return (function(ed) {
	                if (ed.isDirty()) {
	                  ed.save();
	                  updateView(ed);
	                }
	              })(ed);
	            }, debouncedUpdateDelay);
	          };
	        })(400);

	        var setupOptions = {
	          // Update model when calling setContent
	          // (such as from the source editor popup)
	          setup: function(ed) {
	            ed.on('init', function() {
	              ngModel.$render();
	              ngModel.$setPristine();
	                ngModel.$setUntouched();
	              if (form) {
	                form.$setPristine();
	              }
	            });

	            // Update model when:
	            // - a button has been clicked [ExecCommand]
	            // - the editor content has been modified [change]
	            // - the node has changed [NodeChange]
	            // - an object has been resized (table, image) [ObjectResized]
	            ed.on('ExecCommand change NodeChange ObjectResized', function() {
	              if (!options.debounce) {
	                ed.save();
	                updateView(ed);
	              	return;
	              }
	              debouncedUpdate(ed);
	            });

	            ed.on('blur', function() {
	              element[0].blur();
	              ngModel.$setTouched();
	              if (!$rootScope.$$phase) {
	                scope.$digest();
	              }
	            });

	            ed.on('remove', function() {
	              element.remove();
	            });

	            if (uiTinymceConfig.setup) {
	              uiTinymceConfig.setup(ed, {
	                updateView: updateView
	              });
	            }

	            if (expression.setup) {
	              expression.setup(ed, {
	                updateView: updateView
	              });
	            }
	          },
	          format: expression.format || 'html',
	          selector: '#' + attrs.id
	        };
	        // extend options with initial uiTinymceConfig and
	        // options from directive attribute value
	        angular.extend(options, uiTinymceConfig, expression, setupOptions);
	        // Wrapped in $timeout due to $tinymce:refresh implementation, requires
	        // element to be present in DOM before instantiating editor when
	        // re-rendering directive
	        $timeout(function() {
	          if (options.baseURL){
	            tinymce.baseURL = options.baseURL;
	          }
	          tinymce.init(options);
	          toggleDisable(scope.$eval(attrs.ngDisabled));
	        });

	        ngModel.$formatters.unshift(function(modelValue) {
	          return modelValue ? $sce.trustAsHtml(modelValue) : '';
	        });

	        ngModel.$parsers.unshift(function(viewValue) {
	          return viewValue ? $sce.getTrustedHtml(viewValue) : '';
	        });

	        ngModel.$render = function() {
	          ensureInstance();

	          var viewValue = ngModel.$viewValue ?
	            $sce.getTrustedHtml(ngModel.$viewValue) : '';

	          // instance.getDoc() check is a guard against null value
	          // when destruction & recreation of instances happen
	          if (tinyInstance &&
	            tinyInstance.getDoc()
	          ) {
	            tinyInstance.setContent(viewValue);
	            // Triggering change event due to TinyMCE not firing event &
	            // becoming out of sync for change callbacks
	            tinyInstance.fire('change');
	          }
	        };

	        attrs.$observe('disabled', toggleDisable);

	        // This block is because of TinyMCE not playing well with removal and
	        // recreation of instances, requiring instances to have different
	        // selectors in order to render new instances properly
	        scope.$on('$tinymce:refresh', function(e, id) {
	          var eid = attrs.id;
	          if (angular.isUndefined(id) || id === eid) {
	            var parentElement = element.parent();
	            var clonedElement = element.clone();
	            clonedElement.removeAttr('id');
	            clonedElement.removeAttr('style');
	            clonedElement.removeAttr('aria-hidden');
	            tinymce.execCommand('mceRemoveEditor', false, eid);
	            parentElement.append($compile(clonedElement)(scope));
	          }
	        });

	        scope.$on('$destroy', function() {
	          ensureInstance();

	          if (tinyInstance) {
	            tinyInstance.remove();
	            tinyInstance = null;
	          }
	        });

	        function ensureInstance() {
	          if (!tinyInstance) {
	            tinyInstance = tinymce.get(attrs.id);
	          }
	        }
	      }
	    };
	  }]);


/***/ },
/* 124 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(59);
	__webpack_require__(51);
	__webpack_require__(57);
	__webpack_require__(120);
	__webpack_require__(125);
	__webpack_require__(129);
	__webpack_require__(130);
	__webpack_require__(136);
	__webpack_require__(138);
	__webpack_require__(121);
	__webpack_require__(139);
	__webpack_require__(140);
	__webpack_require__(122);
	__webpack_require__(158);
	__webpack_require__(151);


	angular.module("directive")
	    .component('menuComponent', {
	        template: __webpack_require__(159),
	        controller: MenuController,
	        controllerAs: 'vm',
	        bindings: {
	            menuDelegate: '=',
	            showLoginButtons: '@',
	            showHeaderButtons: '@'
	        }
	    });

	MenuController.$inject = ['$log','$compile', '$ocLazyLoad', 'authToken', 'admin', 'utility', '$timeout', '$scope', 'userCourses', 'paypal', 'pageLoader', '$filter','Mail','contactus','chatUtility'];

	function MenuController($log,$compile, $ocLazyLoad, authToken, admin, utility, $timeout, $scope, userCourses, paypal, pageLoader, $filter,Mail,contactus,chatUtility) {

	    ///////////////////////////////////////////////
	    //Load Chat Module Seperatly for code splitting
	    ///////////////////////////////////////////////
	    // $ocLazyLoad.toggleWatch(true);
	    // require(['directive.chat'], function() {
	    //     //inject using oclazyload
	    //     $ocLazyLoad.inject()
	    //         .then(function(res) {
	    //             //chat html
	    //             var html = '<chat show-chat-component="vm.shouldShowChatComponent"></chat>';
	    //             //compile html
	    //             var ele = $compile(html)($scope);
	    //             //append it to parent
	    //             $(".menu-component-wrapper").append(ele);
	    //         })
	    //     $ocLazyLoad.toggleWatch(false);
	    // });
	    /////////////////////////////////////////////


	    var vm = this;
	    

	    if(window.innerWidth < 1280){
	        vm.shouldShowChatComponent = false;
	    }else{
	        vm.shouldShowChatComponent = false;
	    }

	    vm.activeNotificationTab = "web";

	    
	    

	    vm.isUserLoggedIn = authToken.isAuthenticated;
	    vm.goToLogin = utility.goToLogin;
	    vm.goToLogout = utility.goToLogout;
	    vm.goToRegister = utility.goToRegister;
	    vm.goToAdminApp = utility.goToAdminApp;
	    vm.goToDashboardApp = utility.goToDashboardApp;
	    vm.getDashboardUrl = utility.getDashboardUrl;
	    vm.goToChangePassword = utility.goToChangePassword;
	    vm.goToForgotPassword = utility.goToForgotPassword;
	    vm.getUserProfileUrl = utility.getUserProfileUrl;
	    vm.getNotificationsPageUrl = utility.getNotificationsPageUrl;
	    vm.getSpecificNotificationPageUrl = utility.getSpecificNotificationPageUrl;
	    vm.getUserId = authToken.getUserId;
	    vm.getUser = authToken.getUser;
	    vm.getUserPic = authToken.getUserPic;
	    vm.goToURL = goToURL;
	    vm.getCareerBookUrl = utility.getCareerBookUrl;
	    vm.goToCareerBook = utility.goToCareerBook;
	    vm.trainingURL = trainingURL;
	    vm.trainingCourseURL = trainingCourseURL;
	    vm.getQAUrl = utility.getQAUrl;

	    vm.toggleHiddenMenu = toggleHiddenMenu;
	    //menu delegate to access in parent page
	   
	    vm.toggleSmallScreenTab = toggleSmallScreenTab;
	    vm.closeSmallScreenTab = closeSmallScreenTab;

	    vm.openAllLinks = openAllLinks;
	    vm.closeAllLinks = closeAllLinks;

	    vm.getSendNotificationCount = getSendNotificationCount;
	    vm.getNotificationByUserId = getNotificationByUserId;
	    vm.markSendNotificationAsRead = markSendNotificationAsRead;

	    vm.toggleContactUsModal = toggleContactUsModal;

	    vm.toggleSmallMenuModal = toggleSmallMenuModal;
	    vm.toggleUserMenuModal = toggleUserMenuModal;

	    vm.openSubscribeModal = openSubscribeModal;
	    vm.closeSubscribeModal = closeSubscribeModal;
	    vm.validateInputField = validateInputField;
	    vm.subscribeUser = subscribeUser;


	    vm.$onInit = function() {
	        vm.showLoginButtons = (vm.showLoginButtons === "false") ? false : true;
	        vm.showHeaderButtons = (vm.showHeaderButtons === "false") ? false : true;
	        vm.menuDelegate = vm.menuDelegate || {};
	        vm.toggleChatModal = vm.menuDelegate.toggleChatModal = toggleChatModal;
	        vm.menuDelegate.toggleMenuModal = vm.toggleMenuModal = toggleMenuModal;
	        vm.menuDelegate.openMenuModal = vm.openMenuModal = openMenuModal;
	        vm.menuDelegate.closeMenuModal = vm.closeMenuModal = closeMenuModal;
	        vm.menuDelegate.openNotificationModal = vm.openNotificationModal = openNotificationModal;
	        vm.menuDelegate.toggleNotificationModal = vm.toggleNotificationModal = toggleNotificationModal;

	        init();
	        getContactUsDetails();
	        // getAllLinks(); 
	        if(vm.isUserLoggedIn()){
	            getSendNotificationCount();    
	        }
	    }

	    function getContactUsDetails(argument) {
	        contactus.getContactUsDetails()
	            .then(function(res) {
	                vm.contactusDetails = res.data[0] || {};
	            })
	            .catch(function(err) {
	                $log.error(err);
	            })
	    }

	    function init() {
	        //bind document click
	        //when user click outside of menu then hide it
	        $(document).on('click', function(e) {

	            // console.log("document click menu")

	            var captureClick = $(e.target).attr("data-capture-document-click");

	            if (!captureClick) {
	                element = $(e.target).closest("[data-capture-document-click]");
	                captureClick = $(element).attr("data-capture-document-click");
	            }

	            if (captureClick === "false") {
	                return;
	            }

	            resetMenuModals();

	            $scope.$apply();

	            if (!vm.isMenuModalOpen) {
	                //if it is not opened then return
	                return;
	            }
	            toggleMenuModal();

	            $scope.$apply();
	        })

	        $(".modal-close-btn").on('click', function(e) {
	            e.stopPropagation();
	        })

	        //if user click on menu modal
	        //then don't propagate that click to document
	        $("#menu-modal").on('click', function(e) {
	            e.stopPropagation();
	        })

	        $(".menu-component-wrapper").on('click', function(e) {
	            // e.stopPropagation();
	        })

	        //fetch all courses
	        admin.courselistfull()
	            .then(function(d) {
	                vm.courses = d;
	            })
	            .catch(function(d) {
	                console.error(d);
	            })

	        // if (vm.isUserLoggedIn) {
	        //     userCourses.getPurchasedCourses({
	        //             userId: authToken.getUserId()
	        //         })
	        //         .then(function(d) {
	        //             vm.myCourses = d.data;
	        //         })
	        //         .catch(function(err) {
	        //             console.error(err);
	        //         })
	        // }
	    }

	    //toggle hidden menu
	    function toggleHiddenMenu($event) {
	        var target = $event.target;
	        //find from clicked
	        var menu = $(target).find('.hidden-menu');

	        //if not found any hidden element
	        if (menu.length === 0) {
	            //try from finding from parent
	            menu = $(target).parent().siblings('.hidden-menu');
	        }

	        if (target.tagName === 'A') {
	            // console.log("toggling", target);
	            $event.stopPropagation();
	            // don't toggle
	            return;
	        }

	        //hide all menu with 'hidden-menu' class
	        $('.menu-component-wrapper').find('.hidden-menu').each(function(i, v) {
	            if ($(v)[0] != $(menu)[0]) {
	                $(v).hide();
	            }
	        });

	        $(menu).toggle();

	    }

	    //toggle entire menu modal
	    function toggleMenuModal(evt) {
	        console.log("toggleMenuModal")
	        if (evt) {
	            evt.stopPropagation();
	        }
	        if (!vm.isMenuModalOpen) {
	            //It is not opened
	            //Now it is going to open
	            openMenuModal();
	        } else {
	            //if it is opened
	            //then close it
	            closeMenuModal();
	        }
	    }

	    function openMenuModal() {
	        resetMenuModals();
	        vm.isMenuModalOpen = true;
	        // $("#menu-modal").css('width', '40%').css('min-width', '1000px');
	        // setTimeout(function() {
	        //     // $("#menu-modal").css('min-width','500px');
	        // });
	        // $timeout(function() {
	        //     vm.isMenuModalOpen = true;
	        // }, 300);
	    }

	    function closeMenuModal() {
	        // $("#menu-modal").width(0).css('min-width', '0px');;
	        vm.isMenuModalOpen = false;
	    }

	    function goToURL(url) {
	        window.location = url;
	    }

	    function trainingURL(type) {
	        var url = '/' + type + '-trainings';
	        return url;
	    }

	    function trainingCourseURL(type, courseId, courseGroup) {
	        var url = '/' + type + '/' + courseGroup + '-trainings/' + courseId;
	        return url;
	    }

	    function toggleSmallScreenTab(tab) {
	        if (vm.smallScreenActiveTab && vm.smallScreenActiveTab != tab) {
	            vm.smallScreenActiveTab = tab;
	            return;
	        }
	        if (vm.smallScreenActiveTab) {
	            closeSmallScreenTab();
	        } else {
	            openSmallScreenTab(tab);
	        }
	    }

	    function openSmallScreenTab(tab) {
	        vm.smallScreenActiveTab = tab;
	        $('.ew-tab-content-wrapper').addClass('ew-open');
	    }

	    function closeSmallScreenTab() {
	        $('.ew-tab-content-wrapper').removeClass('ew-open');
	        vm.smallScreenActiveTab = '';
	    }

	    function openAllLinks() {
	        vm.shouldShowAllLinksModal = true;
	    }

	    function closeAllLinks() {
	        vm.shouldShowAllLinksModal = false;
	    }

	    function getAllLinks() {
	        admin.getAllLinksCategories()
	            .then(function(res) {
	                vm.allLinksCategories = res.data;
	                //get all links
	                admin.getAllLinks()
	                    .then(function(res) {
	                        var links = res.data;
	                        var allLinksMap = {};
	                        links.forEach(function(v, i) {
	                            var cat_obj = $filter('filter')(vm.allLinksCategories, {
	                                cat_id: v.link_cat
	                            })[0] || {};
	                            allLinksMap[cat_obj.cat_nm] = allLinksMap[cat_obj.cat_nm] || [];
	                            allLinksMap[cat_obj.cat_nm].push(v);
	                        })
	                        vm.allLinks = allLinksMap;
	                    })
	                    .catch(function(err) {
	                        console.log(err);
	                    })
	            })

	    }

	    function openNotificationModal() {
	        resetMenuModals();
	        if (!vm.allNotifications) {
	            getNotificationByUserId();
	        }
	        vm.showNotificationModal = true;
	    }

	    function closeNotificationModal() {
	        vm.showNotificationModal = false;
	    }

	    function toggleNotificationModal() {
	        if (!vm.showNotificationModal) {
	            openNotificationModal();
	        } else {
	            closeNotificationModal();
	        }
	    }

	    

	    function getSendNotificationCount() {
	        admin.getSendNotificationCount({
	                usr_id: authToken.getUserId()
	            })
	            .then(function(res) {
	                var rows = res.data;
	                if(rows[0]){
	                    vm.notificaionCount = rows[0].noti_cnt;    
	                }
	                
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }

	    function getNotificationByUserId() {
	        admin.getSendNotificationByUserId({
	                usr_id: authToken.getUserId()
	            })
	            .then(function(res) {
	                vm.allNotifications = res.data;
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }

	    function markSendNotificationAsRead($event,usr_noti_pk) {
	        $event.stopPropagation();
	        $event.preventDefault();

	        admin.markSendNotificationAsRead({
	                usr_id: authToken.getUserId(),
	                usr_noti_pk: usr_noti_pk
	            })
	            .then(function(res) {
	                getNotificationByUserId();
	            })
	            .catch(function(err) {})
	    }

	    function resetMenuModals(){
	        closeNotificationModal();
	        closeContactUsModal();
	        closeSmallMenuModal();
	        closeMenuModal();
	        closeUserMenuModal();
	    }

	    function openContactUsModal() {
	        resetMenuModals()
	        vm.showContactUsModal = true;
	    }

	    function closeContactUsModal() {
	        vm.showContactUsModal = false;
	    }

	    function toggleContactUsModal() {
	        if (!vm.showContactUsModal) {
	            openContactUsModal();
	        } else {
	            closeContactUsModal();
	        }
	    }

	    function openSmallMenuModal() {
	        resetMenuModals();
	        vm.showSmallMenuModal = true;
	    }

	    function closeSmallMenuModal() {
	        vm.showSmallMenuModal = false;
	    }

	    function toggleSmallMenuModal() {
	        if (!vm.showSmallMenuModal) {
	            openSmallMenuModal();
	        } else {
	            closeSmallMenuModal();
	        }
	    }


	    function openUserMenuModal() {
	        resetMenuModals();
	        vm.showUserMenuModal = true;
	    }

	    function closeUserMenuModal() {
	        vm.showUserMenuModal = false;
	    }

	    function toggleUserMenuModal() {
	        if (!vm.showUserMenuModal) {
	            openUserMenuModal();
	        } else {
	            closeUserMenuModal();
	        }
	    }


	    function toggleChatModal() {
	        vm.shouldShowChatComponent = !vm.shouldShowChatComponent;
	    }


	    function validateInputField() {

	        var input = vm.subscribeInput;
	        console.log(input);
	        if(input.length <= 0 || !validateEmail(input)){
	            vm.showInputErrorMsg = true;
	        }else{
	            vm.showInputErrorMsg = false;
	        }
	    }

	    function validateEmail(email) {
	        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	        return re.test(email);
	    }

	    function subscribeUser() {
	        if(vm.showInputErrorMsg){
	            return;
	        }
	        Mail.subscribeUser({
	            subs_email : vm.subscribeInput,
	            subs_typ : vm.subs_typ
	        })
	        .then(function(res) {
	            vm.showSuccessMsg = true;
	            vm.showErrorMsg = false;
	            vm.subscribeInput= "";
	            vm.showInputErrorMsg = false;
	            vm.subs_typ = "";
	        })
	        .catch(function(err) {
	            vm.showErrorMsg = true;
	            vm.showSuccessMsg = false;
	        })
	    }

	    function openSubscribeModal(title,type) {
	        closeMenuModal();
	        vm.subs_typ = type;
	        vm.showSuccessMsg = false;
	        vm.showInputErrorMsg = false;
	        vm.showErrorMsg = false;
	        vm.subscribeInput= "";
	        vm.shouldShowSubscribeModal = true;
	        vm.subscribeTitle = title + " will be live shortly. Please Subscribe for Updates";
	    }
	    function closeSubscribeModal() {
	        vm.shouldShowSubscribeModal = false;
	        vm.showInputErrorMsg = false;
	        vm.subscribeInput= "";
	        vm.subs_typ = "";
	    }
	    $('body').on('click','.chat_fixed', function(){
	        $('.fa-angle-up').click();
	    });
	    var chatHistory = chatUtility.getInfo();
	    if (chatHistory!=null) {
	        if (chatHistory.isOpenDialog==true) {
	            vm.shouldShowChatComponent = true;
	        }   
	    };
	    $('body').on('click','.chat_fixed', function(){
	        $('.fa-angle-up').click();
	        chatUtility.isOpenDialog(vm.shouldShowChatComponent);
	    });
	    $('body').on('click','.logout', function(){
	        chatUtility.clearChatInfo();
	    });
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 125 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($, hljs) {__webpack_require__(57);
	__webpack_require__(127);

	angular.module("service")
	    .service('utility', Utility);


	Utility.$inject = ['authToken','$sce','$http','BASE_API'];

	function Utility(authToken,$sce,$http,BASE_API) {

	    var self = this;

	    self.BASE_URL = BASE_API;
	    self.QA_QUESTION_ID_URL_LENGTH = 300;

	    self.goToLogin = goToLogin;
	    self.getLoginUrl = getLoginUrl;
	    self.goToLogout = goToLogout;
	    self.getLogoutUrl = getLogoutUrl;
	    self.goToRegister = goToRegister;
	    self.getRegisterUrl = getRegisterUrl;
	    self.goToChangePassword = goToChangePassword;
	    self.goToForgotPassword = goToForgotPassword;
	    self.getForgotPasswordUrl = getForgotPasswordUrl;
	    self.goToAdminApp = goToAdminApp;
	    self.goToDashboardApp = goToDashboardApp;
	    self.goToPracticePage = goToPracticePage;

	    self.goToCareerBook = goToCareerBook;
	    self.getCareerBookUrl = getCareerBookUrl;

	    self.goToQA = goToQA;
	    self.getQAUrl = getQAUrl;
	    self.getQAQuestionPageUrl = getQAQuestionPageUrl;
	    self.getQAAnswerPageUrl = getQAAnswerPageUrl;

	    self.goToWhiteboard = goToWhiteboard;
	    self.getWhiteboardUrl = getWhiteboardUrl;

	    self.goToDashboard = goToDashboard;
	    self.getDashboardUrl = getDashboardUrl;

	    self.goToBooks = goToBooks;
	    self.getBooksUrl = getBooksUrl;

	    self.getUserProfileUrl = getUserProfileUrl;
	    self.goToUserProfile = goToUserProfile;

	    self.getSitemapUrl = getSitemapUrl;

	    self.getITTrainingsUrl = getITTrainingsUrl;

	    self.getNotificationsPageUrl = getNotificationsPageUrl;
	    self.getSpecificNotificationPageUrl = getSpecificNotificationPageUrl;

	    self.doSyntaxHighlighting = doSyntaxHighlighting;
	    self.getLanguageForSyntaxHighlighting = getLanguageForSyntaxHighlighting;
	    self.changeTimeFormat = changeTimeFormat;
	    self.trustAsHTML = trustAsHTML;
	    self.trustAsHTMLReturnPlainText = trustAsHTMLReturnPlainText;
	    self.shareToEWProfile = shareToEWProfile;trustAsHTMLReturnPlainText;
	    self.shareToQA = shareToQA;
	    self.openPopUp = openPopUp;

	    self.buyPage = buyPage;
	    self.buyCoursePage = buyCoursePage;
	    self.getBuyCoursePageURL = getBuyCoursePageURL;
	    self.buyPaymentPage = buyPaymentPage;
	    self.buyCoursePricePage = buyCoursePricePage;
	    self.getCoursePricePageURL = getCoursePricePageURL;

	    self.getVideoPageURL = getVideoPageURL;
	    self.getQuestionPageURL = getQuestionPageURL;



	    self.getTrainingPageUrl = getTrainingPageUrl;

	    self.getNextTrainingDay = getNextTrainingDay;
	    self.getCourseDetailsPageUrl = getCourseDetailsPageUrl;

	    self.getAdminPracticePageUrl = getAdminPracticePageUrl;

	    self.convertTimeTo_AM_PM = convertTimeTo_AM_PM;



	    self.submitQuestionStats = function(data){
	        return $http.post(BASE_API + '/userstats', data);
	    }

	    function goToLogin(){
	        authToken.cachedUrl(window.location.href);
	        window.location = '/login';
	    }

	    function getLoginUrl(){
	        return '/login';
	    }

	    function goToRegister(){
	        authToken.cachedUrl(window.location.href);
	        window.location = '/register';
	    }

	    function getRegisterUrl(){
	        return '/register';
	    }

	    function goToLogout(){
	        window.location = '/logout';
	    }

	    function  getLogoutUrl() {
	        return '/logout';
	    }

	    function goToChangePassword(code) {
	        if(code){
	            window.location = '/changepassword/' + code;
	        }else{
	            window.location = '/changepassword/';
	        }
	    }

	    function goToForgotPassword() {
	        window.location = '/forgotpassword';
	    }

	    function getForgotPasswordUrl(){
	        return '/forgotpassword';
	    }


	    function goToAdminApp() {
	        window.location = '/adminApp/';
	    }

	    function goToDashboardApp() {
	        window.location = '/dashboard/';
	    }


	    function buyPage() {
	        return self.BASE_URL + '/buy/'
	    }
	    function buyCoursePricePage(crs_id) {
	        return self.BASE_URL + '/buy/price/' + crs_id;
	    }
	    function getCoursePricePageURL(shouldRedirect,crs_id) {
	        var url = self.BASE_URL + '/buy/price/' + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }
	    function buyCoursePage(crs_id) {
	        return self.BASE_URL + '/buy/course/' + crs_id;
	    }
	    function getBuyCoursePageURL(shouldRedirect,crs_id) {
	        var url = self.BASE_URL + '/buy/course/' + crs_id;;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }
	    function buyPaymentPage(crs_id,bat_id) {
	        return self.BASE_URL + '/buy/payment?course=' + crs_id + '&bat_id=' + bat_id ;
	    }

	    function goToPracticePage(courseId,moduleId,itemId){
	        var url = "/" + courseId + "/video/" + itemId;
	        // var url = "/app/practice?module="+ moduleId + "&course=" + courseId + "&item=" + itemId;
	        // window.location.href = url;
	        var win = window.open(url, '_blank');
	        win.focus();
	    }

	    /////////////////////

	    function goToQA(){
	        window.location = '/question-answers';
	    }
	    function getQAUrl() {
	        return '/question-answers';
	    }

	    function getQAQuestionPageUrl(shouldRedirect,que_id,que_text){
	        var url = '/question-answers/question/' + que_id + '/' + (que_text ? que_text : '');
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    function getQAAnswerPageUrl(shouldRedirect,que_id,ans_id,que_text){
	        var url = '/question-answers/question/' + que_id + "/" + (que_text || "text") +  "/answer/" + ans_id;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    /////////////////////

	    function goToCareerBook(){
	        window.location = '/career-book/';
	    }
	    function getCareerBookUrl(){
	        return '/career-book/';
	    }

	    self.getCareerBookProfileUrl = function(usr_id){
	        return '/career-book/' + usr_id;
	    }

	    /////////////////////

	    function goToWhiteboard(){
	        window.location = '/whiteboard/';
	    }
	    function getWhiteboardUrl(){
	        return '/whiteboard/';
	    }

	    /////////////////////

	    function goToBooks(){
	        window.location = '/books/';
	    }
	    function getBooksUrl(){
	        return '/books/';
	    }

	    /////////////////////


	    function goToDashboard(){
	        window.location = '/dashboard/';
	    }
	    function getDashboardUrl(){
	        return '/dashboard/';
	    }

	    /////////////////////


	    function getUserProfileUrl(){
	        return '/user-profile';
	    }
	    function goToUserProfile(){
	        window.location = '/user-profile';
	    }

	    /////////////////////


	    function getSitemapUrl(shouldRedirect){
	        var url = '/sitemap';
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    /////////////////////

	    function getVideoPageURL(courseId,itemId,time){
	        if(time){
	            return self.BASE_URL + '/' + courseId + '/video/' + itemId + '?time=' + time;
	        }else{
	            return self.BASE_URL + '/' + courseId + '/video/' + itemId;
	        }
	    }

	    function getQuestionPageURL(courseId,itemId,question){
	        if(question){
	            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/' + question;
	        }else{
	            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/';
	        }
	    }

	    self.getBookPageURL = getBookPageURL;
	    function getBookPageURL(courseId,itemId){
	        return self.BASE_URL + '/' + courseId + '/book-topic/' + itemId;
	    }

	    function getITTrainingsUrl(shouldRedirect){
	        var url = "/it-trainings";
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getCourseDetailsPageUrl(shouldRedirect,type,subgroup,crs_id){
	        // var url = `${type}/${subgroup}-trainings/${crs_id}`;
	        var url = "/" + type + "/" + subgroup + "-trainings/" + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getNotificationsPageUrl(shouldRedirect,usr_id){
	        var url = "/notifications";
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getSpecificNotificationPageUrl(shouldRedirect,usr_id,notification_id){
	        var url = "/notifications/" + notification_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getTrainingPageUrl(shouldRedirect,crs_id){
	        var url = "/training/" + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getAdminPracticePageUrl(shouldRedirect,q_id){
	        var url = "/adminApp/practice/" + q_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    //do highlighting
	    function doSyntaxHighlighting() {
	        var x = 0;
	        var intervalID = setInterval(function() {
	            // Prism.highlightAll();
	            $('pre code').each(function(i, block) {
	                hljs.highlightBlock(block);
	            });
	            $('.syntax').each(function(i, block) {
	                hljs.highlightBlock(block);
	            });
	            if (++x === 10) {
	                window.clearInterval(intervalID);
	            }
	        }, 1);
	    }

	    //Get language for syntax highlighting
	    function getLanguageForSyntaxHighlighting(coursename){
	        if(coursename.toLowerCase().indexOf("javascript") > -1){
	            return "javascript";
	        }else if(coursename.toLowerCase().indexOf("java") > -1){
	            return "java";
	        }else if(coursename.toLowerCase().indexOf("sql") > -1){
	            return "sql";
	        }
	    }

	    var vm = this;
	    function getNextTrainingDay(frm_wk_dy,to_wk_dy,cls_start_dt,bat_frm_tm_hr,bat_frm_tm_min) {
	        vm.daysOfWeek = ["Monday", 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

	        var training_days = [];
	        var start_counting = false;
	        for (var i = 0; i < vm.daysOfWeek.length; i++) {
	            var d = vm.daysOfWeek[i];
	            if (start_counting) {
	                training_days.push(d);
	                if (d === to_wk_dy) {
	                    start_counting = false;
	                    break;
	                }
	            }
	            if (d === frm_wk_dy) {
	                start_counting = true;
	                training_days.push(d);
	            }
	        }

	        var today_name = getDayName();
	        var today_date = new Date();
	        var next_training_date = "";
	        bat_frm_tm_hr = parseInt(bat_frm_tm_hr);
	        bat_frm_tm_min = parseInt(bat_frm_tm_min);
	        if(today_date < new Date(cls_start_dt)){
	            next_training_date = new Date(cls_start_dt);
	            // console.log("next date is start date.")
	        }else if (training_days.indexOf(today_name) > -1) {
	            //check if today is in training_days
	            var current_hour = today_date.getHours();
	            var current_min = today_date.getMinutes();
	            // console.log(current_hour,bat_frm_tm_hr,current_min,bat_frm_tm_min)
	            if (current_hour > bat_frm_tm_hr) {
	                // console.log("time is exceeded , find next day");
	                var daysToGo = nextTrainingDay(training_days)
	                // console.log("daysToGo", daysToGo);
	                var today = new Date();
	                next_training_date = new Date(today);
	                next_training_date.setDate(today.getDate() + daysToGo);
	            }else if(current_hour === bat_frm_tm_hr && current_min > bat_frm_tm_min) {
	                // console.log("time is exceeded , find next day");
	                var daysToGo = nextTrainingDay(training_days)
	                // console.log("daysToGo", daysToGo);
	                var today = new Date();
	                next_training_date = new Date(today);
	                next_training_date.setDate(today.getDate() + daysToGo);
	            }else {
	                // console.log("today is the next batch day");
	                next_training_date = new Date();
	            }
	        } else {
	            //if today is not in training_days
	            // console.log("today is not training_days , find next day");
	            var daysToGo = nextTrainingDay(training_days);
	            // console.log("daysToGo", daysToGo);
	            var today = new Date();
	            next_training_date = new Date(today);
	            next_training_date.setDate(today.getDate() + daysToGo);
	        }
	        return next_training_date;
	    }

	    function getDayName(dateString) {
	        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	        if (!dateString) {
	            dateString = (new Date()).toString();
	        }
	        var d = new Date(dateString);
	        var dayName = days[d.getDay()];
	        return dayName;
	    }

	    function nextTrainingDay(training_days) {
	        var today_name = getDayName();
	        var today_day_index = vm.daysOfWeek.indexOf(today_name);
	        var day_index = today_day_index + 1;
	        var daysToGo = 1;
	        var nextDayFound = false;
	        while (!nextDayFound) {
	            var day = vm.daysOfWeek[day_index];
	            if (training_days.indexOf(day) > -1) {
	                nextDayFound = true;
	            } else {
	                day_index = (day_index + 1) % 7;
	                daysToGo++;
	                nextDayFound = false;
	            }
	        }
	        return daysToGo;
	    }

	    function changeTimeFormat(sec) {
	        var time = sec || 0;
	        var hours = parseInt(time / 3600) % 24;
	        var minutes = parseInt(time / 60) % 60;
	        var seconds = parseInt(time % 60);

	        hours = ("0" + hours).slice(-2);
	        minutes = ("0" + minutes).slice(-2);
	        seconds = ("0" + seconds).slice(-2);

	        var ret_time;
	        if (hours == "00") {
	            ret_time = minutes + ":" + seconds;
	        } else {
	            ret_time = hours + ":" + minutes + ":" + seconds;
	        }

	        return ret_time;
	    }

	    function convertTimeTo_AM_PM(time) {
	        // time = 10:10
	        time = time || "00:00";
	        var split_time = time.split(":");
	        var hour = parseInt(split_time[0]);
	        if(hour < 12){
	            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " AM";
	        }else if(hour === 12){
	            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
	        }else if(hour > 12){
	            return ('00' + (split_time[0] - 12)).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
	        }
	    }

	    function trustAsHTML(html){
	        return $sce.trustAsHtml(html);
	    }

	    function trustAsHTMLReturnPlainText(html){
	        var html2 = html.replace(/(<([^>]+)>)/g, "");
	        return $sce.trustAsHtml(html2);
	    }

	    function shareToEWProfile(title,desc,url){
	        if(!url){
	            url = encodeURIComponent(window.location.href);
	        }else{
	            url = encodeURIComponent(url);
	        }
	        var w = 500;
	        var h = 560;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = window.location.origin + '/app/share?type=cb&title=' + title + "&url=" + url + "&desc=" + (desc || '');
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    function shareToQA(title,desc,url){
	        if(!url){
	            url = encodeURIComponent(window.location.href);
	        }else{
	            url = encodeURIComponent(url);
	        }
	        var w = 500;
	        var h = 560;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = window.location.origin + '/app/share?type=qa&title=' + title + "&url=" + url + "&desc=" + (desc || '');
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    function openPopUp(url){
	        var w = 500;
	        var h = 500;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = url;
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    self.getSelectionText = getSelectionText;
	    function getSelectionText() {
	        var text = "";
	        if (window.getSelection) {
	            text = window.getSelection().toString();
	        } else if (document.selection && document.selection.type != "Control") {
	            text = document.selection.createRange().text;
	        }
	        return text;
	    }

	    self.getPlainQuestionIdText = function(text){
	        var text = $("<div></div>").html(text).text().split(" ").join("-").toLowerCase();
	        text = text.substring(0,self.QA_QUESTION_ID_URL_LENGTH);
	        text = text.replace(/[^a-zA-Z0-9-]/g, "")
	        return text
	    }
	};

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1), __webpack_require__(126)))

/***/ },
/* 126 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(hljs) {/*! highlight.js v9.5.0 | BSD3 License | git.io/hljslicense */ ! function(e) {
	    var n = "object" == typeof window && window || "object" == typeof self && self;
	     true ? e(exports) : n && (n.hljs = e({}), "function" == typeof define && define.amd && define([], function() {
	        return n.hljs
	    }))
	}(function(e) {
	    function n(e) {
	        return e.replace(/[&<>]/gm, function(e) {
	            return I[e]
	        })
	    }

	    function t(e) {
	        return e.nodeName.toLowerCase()
	    }

	    function r(e, n) {
	        var t = e && e.exec(n);
	        return t && 0 === t.index
	    }

	    function a(e) {
	        return k.test(e)
	    }

	    function i(e) {
	        var n, t, r, i, o = e.className + " ";
	        if (o += e.parentNode ? e.parentNode.className : "", t = B.exec(o)) return R(t[1]) ? t[1] : "no-highlight";
	        for (o = o.split(/\s+/), n = 0, r = o.length; r > n; n++)
	            if (i = o[n], a(i) || R(i)) return i
	    }

	    function o(e, n) {
	        var t, r = {};
	        for (t in e) r[t] = e[t];
	        if (n)
	            for (t in n) r[t] = n[t];
	        return r
	    }

	    function u(e) {
	        var n = [];
	        return function r(e, a) {
	            for (var i = e.firstChild; i; i = i.nextSibling) 3 === i.nodeType ? a += i.nodeValue.length : 1 === i.nodeType && (n.push({
	                event: "start",
	                offset: a,
	                node: i
	            }), a = r(i, a), t(i).match(/br|hr|img|input/) || n.push({
	                event: "stop",
	                offset: a,
	                node: i
	            }));
	            return a
	        }(e, 0), n
	    }

	    function c(e, r, a) {
	        function i() {
	            return e.length && r.length ? e[0].offset !== r[0].offset ? e[0].offset < r[0].offset ? e : r : "start" === r[0].event ? e : r : e.length ? e : r
	        }

	        function o(e) {
	            function r(e) {
	                return " " + e.nodeName + '="' + n(e.value) + '"'
	            }
	            l += "<" + t(e) + w.map.call(e.attributes, r).join("") + ">"
	        }

	        function u(e) {
	            l += "</" + t(e) + ">"
	        }

	        function c(e) {
	            ("start" === e.event ? o : u)(e.node)
	        }
	        for (var s = 0, l = "", f = []; e.length || r.length;) {
	            var g = i();
	            if (l += n(a.substr(s, g[0].offset - s)), s = g[0].offset, g === e) {
	                f.reverse().forEach(u);
	                do c(g.splice(0, 1)[0]), g = i(); while (g === e && g.length && g[0].offset === s);
	                f.reverse().forEach(o)
	            } else "start" === g[0].event ? f.push(g[0].node) : f.pop(), c(g.splice(0, 1)[0])
	        }
	        return l + n(a.substr(s))
	    }

	    function s(e) {
	        function n(e) {
	            return e && e.source || e
	        }

	        function t(t, r) {
	            return new RegExp(n(t), "m" + (e.cI ? "i" : "") + (r ? "g" : ""))
	        }

	        function r(a, i) {
	            if (!a.compiled) {
	                if (a.compiled = !0, a.k = a.k || a.bK, a.k) {
	                    var u = {},
	                        c = function(n, t) {
	                            e.cI && (t = t.toLowerCase()), t.split(" ").forEach(function(e) {
	                                var t = e.split("|");
	                                u[t[0]] = [n, t[1] ? Number(t[1]) : 1]
	                            })
	                        };
	                    "string" == typeof a.k ? c("keyword", a.k) : E(a.k).forEach(function(e) {
	                        c(e, a.k[e])
	                    }), a.k = u
	                }
	                a.lR = t(a.l || /\w+/, !0), i && (a.bK && (a.b = "\\b(" + a.bK.split(" ").join("|") + ")\\b"), a.b || (a.b = /\B|\b/), a.bR = t(a.b), a.e || a.eW || (a.e = /\B|\b/), a.e && (a.eR = t(a.e)), a.tE = n(a.e) || "", a.eW && i.tE && (a.tE += (a.e ? "|" : "") + i.tE)), a.i && (a.iR = t(a.i)), null == a.r && (a.r = 1), a.c || (a.c = []);
	                var s = [];
	                a.c.forEach(function(e) {
	                    e.v ? e.v.forEach(function(n) {
	                        s.push(o(e, n))
	                    }) : s.push("self" === e ? a : e)
	                }), a.c = s, a.c.forEach(function(e) {
	                    r(e, a)
	                }), a.starts && r(a.starts, i);
	                var l = a.c.map(function(e) {
	                    return e.bK ? "\\.?(" + e.b + ")\\.?" : e.b
	                }).concat([a.tE, a.i]).map(n).filter(Boolean);
	                a.t = l.length ? t(l.join("|"), !0) : {
	                    exec: function() {
	                        return null
	                    }
	                }
	            }
	        }
	        r(e)
	    }

	    function l(e, t, a, i) {
	        function o(e, n) {
	            for (var t = 0; t < n.c.length; t++)
	                if (r(n.c[t].bR, e)) return n.c[t]
	        }

	        function u(e, n) {
	            if (r(e.eR, n)) {
	                for (; e.endsParent && e.parent;) e = e.parent;
	                return e
	            }
	            return e.eW ? u(e.parent, n) : void 0
	        }

	        function c(e, n) {
	            return !a && r(n.iR, e)
	        }

	        function g(e, n) {
	            var t = N.cI ? n[0].toLowerCase() : n[0];
	            return e.k.hasOwnProperty(t) && e.k[t]
	        }

	        function h(e, n, t, r) {
	            var a = r ? "" : y.classPrefix,
	                i = '<span class="' + a,
	                o = t ? "" : C;
	            return i += e + '">', i + n + o
	        }

	        function p() {
	            var e, t, r, a;
	            if (!E.k) return n(B);
	            for (a = "", t = 0, E.lR.lastIndex = 0, r = E.lR.exec(B); r;) a += n(B.substr(t, r.index - t)), e = g(E, r), e ? (M += e[1], a += h(e[0], n(r[0]))) : a += n(r[0]), t = E.lR.lastIndex, r = E.lR.exec(B);
	            return a + n(B.substr(t))
	        }

	        function d() {
	            var e = "string" == typeof E.sL;
	            if (e && !x[E.sL]) return n(B);
	            var t = e ? l(E.sL, B, !0, L[E.sL]) : f(B, E.sL.length ? E.sL : void 0);
	            return E.r > 0 && (M += t.r), e && (L[E.sL] = t.top), h(t.language, t.value, !1, !0)
	        }

	        function b() {
	            k += null != E.sL ? d() : p(), B = ""
	        }

	        function v(e) {
	            k += e.cN ? h(e.cN, "", !0) : "", E = Object.create(e, {
	                parent: {
	                    value: E
	                }
	            })
	        }

	        function m(e, n) {
	            if (B += e, null == n) return b(), 0;
	            var t = o(n, E);
	            if (t) return t.skip ? B += n : (t.eB && (B += n), b(), t.rB || t.eB || (B = n)), v(t, n), t.rB ? 0 : n.length;
	            var r = u(E, n);
	            if (r) {
	                var a = E;
	                a.skip ? B += n : (a.rE || a.eE || (B += n), b(), a.eE && (B = n));
	                do E.cN && (k += C), E.skip || (M += E.r), E = E.parent; while (E !== r.parent);
	                return r.starts && v(r.starts, ""), a.rE ? 0 : n.length
	            }
	            if (c(n, E)) throw new Error('Illegal lexeme "' + n + '" for mode "' + (E.cN || "<unnamed>") + '"');
	            return B += n, n.length || 1
	        }
	        var N = R(e);
	        if (!N) throw new Error('Unknown language: "' + e + '"');
	        s(N);
	        var w, E = i || N,
	            L = {},
	            k = "";
	        for (w = E; w !== N; w = w.parent) w.cN && (k = h(w.cN, "", !0) + k);
	        var B = "",
	            M = 0;
	        try {
	            for (var I, j, O = 0;;) {
	                if (E.t.lastIndex = O, I = E.t.exec(t), !I) break;
	                j = m(t.substr(O, I.index - O), I[0]), O = I.index + j
	            }
	            for (m(t.substr(O)), w = E; w.parent; w = w.parent) w.cN && (k += C);
	            return {
	                r: M,
	                value: k,
	                language: e,
	                top: E
	            }
	        } catch (T) {
	            if (T.message && -1 !== T.message.indexOf("Illegal")) return {
	                r: 0,
	                value: n(t)
	            };
	            throw T
	        }
	    }

	    function f(e, t) {
	        t = t || y.languages || E(x);
	        var r = {
	                r: 0,
	                value: n(e)
	            },
	            a = r;
	        return t.filter(R).forEach(function(n) {
	            var t = l(n, e, !1);
	            t.language = n, t.r > a.r && (a = t), t.r > r.r && (a = r, r = t)
	        }), a.language && (r.second_best = a), r
	    }

	    function g(e) {
	        return y.tabReplace || y.useBR ? e.replace(M, function(e, n) {
	            return y.useBR && "\n" === e ? "<br>" : y.tabReplace ? n.replace(/\t/g, y.tabReplace) : void 0
	        }) : e
	    }

	    function h(e, n, t) {
	        var r = n ? L[n] : t,
	            a = [e.trim()];
	        return e.match(/\bhljs\b/) || a.push("hljs"), -1 === e.indexOf(r) && a.push(r), a.join(" ").trim()
	    }

	    function p(e) {
	        var n, t, r, o, s, p = i(e);
	        a(p) || (y.useBR ? (n = document.createElementNS("http://www.w3.org/1999/xhtml", "div"), n.innerHTML = e.innerHTML.replace(/\n/g, "").replace(/<br[ \/]*>/g, "\n")) : n = e, s = n.textContent, r = p ? l(p, s, !0) : f(s), t = u(n), t.length && (o = document.createElementNS("http://www.w3.org/1999/xhtml", "div"), o.innerHTML = r.value, r.value = c(t, u(o), s)), r.value = g(r.value), e.innerHTML = r.value, e.className = h(e.className, p, r.language), e.result = {
	            language: r.language,
	            re: r.r
	        }, r.second_best && (e.second_best = {
	            language: r.second_best.language,
	            re: r.second_best.r
	        }))
	    }

	    function d(e) {
	        y = o(y, e)
	    }

	    function b() {
	        if (!b.called) {
	            b.called = !0;
	            var e = document.querySelectorAll("pre code");
	            w.forEach.call(e, p)
	        }
	    }

	    function v() {
	        addEventListener("DOMContentLoaded", b, !1), addEventListener("load", b, !1)
	    }

	    function m(n, t) {
	        var r = x[n] = t(e);
	        r.aliases && r.aliases.forEach(function(e) {
	            L[e] = n
	        })
	    }

	    function N() {
	        return E(x)
	    }

	    function R(e) {
	        return e = (e || "").toLowerCase(), x[e] || x[L[e]]
	    }
	    var w = [],
	        E = Object.keys,
	        x = {},
	        L = {},
	        k = /^(no-?highlight|plain|text)$/i,
	        B = /\blang(?:uage)?-([\w-]+)\b/i,
	        M = /((^(<[^>]+>|\t|)+|(?:\n)))/gm,
	        C = "</span>",
	        y = {
	            classPrefix: "hljs-",
	            tabReplace: null,
	            useBR: !1,
	            languages: void 0
	        },
	        I = {
	            "&": "&amp;",
	            "<": "&lt;",
	            ">": "&gt;"
	        };
	    return e.highlight = l, e.highlightAuto = f, e.fixMarkup = g, e.highlightBlock = p, e.configure = d, e.initHighlighting = b, e.initHighlightingOnLoad = v, e.registerLanguage = m, e.listLanguages = N, e.getLanguage = R, e.inherit = o, e.IR = "[a-zA-Z]\\w*", e.UIR = "[a-zA-Z_]\\w*", e.NR = "\\b\\d+(\\.\\d+)?", e.CNR = "(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", e.BNR = "\\b(0b[01]+)", e.RSR = "!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~", e.BE = {
	        b: "\\\\[\\s\\S]",
	        r: 0
	    }, e.ASM = {
	        cN: "string",
	        b: "'",
	        e: "'",
	        i: "\\n",
	        c: [e.BE]
	    }, e.QSM = {
	        cN: "string",
	        b: '"',
	        e: '"',
	        i: "\\n",
	        c: [e.BE]
	    }, e.PWM = {
	        b: /\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|like)\b/
	    }, e.C = function(n, t, r) {
	        var a = e.inherit({
	            cN: "comment",
	            b: n,
	            e: t,
	            c: []
	        }, r || {});
	        return a.c.push(e.PWM), a.c.push({
	            cN: "doctag",
	            b: "(?:TODO|FIXME|NOTE|BUG|XXX):",
	            r: 0
	        }), a
	    }, e.CLCM = e.C("//", "$"), e.CBCM = e.C("/\\*", "\\*/"), e.HCM = e.C("#", "$"), e.NM = {
	        cN: "number",
	        b: e.NR,
	        r: 0
	    }, e.CNM = {
	        cN: "number",
	        b: e.CNR,
	        r: 0
	    }, e.BNM = {
	        cN: "number",
	        b: e.BNR,
	        r: 0
	    }, e.CSSNM = {
	        cN: "number",
	        b: e.NR + "(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?",
	        r: 0
	    }, e.RM = {
	        cN: "regexp",
	        b: /\//,
	        e: /\/[gimuy]*/,
	        i: /\n/,
	        c: [e.BE, {
	            b: /\[/,
	            e: /\]/,
	            r: 0,
	            c: [e.BE]
	        }]
	    }, e.TM = {
	        cN: "title",
	        b: e.IR,
	        r: 0
	    }, e.UTM = {
	        cN: "title",
	        b: e.UIR,
	        r: 0
	    }, e.METHOD_GUARD = {
	        b: "\\.\\s*" + e.UIR,
	        r: 0
	    }, e
	});
	hljs.registerLanguage("ruleslanguage", function(T) {
	    return {
	        k: {
	            keyword: 'ABORT ACCEPT ACCESS ADD ADMIN AFTER ALLOCATE ALTER ANALYZE ARCHIVE ARCHIVELOG ' +
	                'ARRAY ARRAYLEN AS ASC ASSERT ASSIGN AT AUDIT AUTHORIZATION BACKUP BASE_TABLE ' +
	                'BECOME BEFORE BEGIN BINARY_INTEGER BLOCK BODY BOOLEAN BY CACHE CANCEL CASCADE ' +
	                'CASE CHANGE CHAR CHARACTER CHAR_BASE CHECK CHECKPOINT CLOSE CLUSTER CLUSTERS ' +
	                'COBOL COLAUTH COLUMN COLUMNS COMMENT COMMIT COMPILE COMPRESS CONNECT CONSTANT ' +
	                'CONSTRAINT CONSTRAINTS CONTENTS CONTINUE CONTROLFILE CRASH CREATE CURRENT ' +
	                'CURRVAL CURSOR CYCLE DATABASE DATAFILE DATA_BASE DATE DATE DBA DEBUGOFF ' +
	                'DEBUGON DEC DECIMAL DECLARE DEFAULT DEFINITION DELAY DELETE DELTA DESC DIGITS ' +
	                'DISABLE DISMOUNT DISPOSE DISTINCT DO DOUBLE DROP EACH ELSE ELSIF ENABLE END ' +
	                'ENTRY ESCAPE EVENTS EXCEPT EXCEPTION EXCEPTIONS EXCEPTION_INIT EXCLUSIVE EXEC ' +
	                'EXECUTE EXISTS EXIT EXPLAIN EXTENT EXTERNALLY FALSE FETCH FILE FLOAT FLUSH FOR ' +
	                'FORCE FOREIGN FORM FORTRAN FOUND FREELIST FREELISTS FROM FUNCTION GENERIC GO ' +
	                'GOTO GRANT GROUP GROUPS HAVING IDENTIFIED IF IMMEDIATE INCLUDING INCREMENT ' +
	                'INDEX INDEXES INDICATOR INITIAL INITRANS INSERT INSTANCE INT INTEGER INTERSECT ' +
	                'INTO IS KEY LANGUAGE LAYER LEVEL LIMITED LINK LISTS LOCK LOGFILE LONG LOOP ' +
	                'MANAGE MANUAL MAXDATAFILES MAXEXTENTS MAXINSTANCES MAXLOGFILES MAXLOGHISTORY ' +
	                'MAXLOGMEMBERS MAXTRANS MAXVALUE MINEXTENTS MINUS MINVALUE MLSLABEL MODE MODIFY ' +
	                'MODULE MOUNT NATURAL NEW NEXT NEXTVAL NOARCHIVELOG NOAUDIT NOCACHE NOCOMPRESS ' +
	                'NOCYCLE NOMAXVALUE NOMINVALUE NONE NOORDER NORESETLOGS NORMAL NOSORT NOTFOUND ' +
	                'NOWAIT NUMBER number NUMBER_BASE NUMERIC OF OFF OFFLINE OLD ON ONLINE ONLY OPEN OPTIMAL ' +
	                'OPTION ORDER OTHERS OUT OWN PACKAGE PARALLEL PARTITION PCTFREE PCTINCREASE ' +
	                'PCTUSED PLAN PLI POSITIVE PRAGMA PRECISION PRIMARY PRIOR PRIVATE PRIVILEGES ' +
	                'PROCEDURE PROFILE PUBLIC QUOTA RAISE RANGE RAW READ REAL RECORD RECOVER ' +
	                'REFERENCES REFERENCING RELEASE REMR RENAME RESETLOGS RESOURCE RESTRICTED ' +
	                'RETURN REUSE REVOKE ROLE ROLES ROLLBACK ROW ROWID ROWLABEL ROWNUM ROWS ROWTYPE ' +
	                'RUN SAVEPOINT SCHEMA SCN SECTION SEGMENT SELECT SEPARATE SEQUENCE SESSION ' +
	                'SHARE SHARED SIZE SMALLINT SNAPSHOT SORT SPACE SQL SQLBUF SQLERROR SQLSTATE ' +
	                'START STATEMENT STATEMENT_ID STATISTICS STDDEV STOP STORAGE SUBTYPE ' +
	                'SUCCESSFUL SWITCH SYNONYM SYSDATE SYSTEM TABAUTH TABLESPACE TASK TEMPORARY ' +
	                'TERMINATE THEN THREAD TIME TO TRACING TRANSACTION TRIGGER TRIGGERS TRUE ' +
	                'TRUNCATE TYPE UNDER UNION UNIQUE UNLIMITED UNTIL UPDATE USE USING VALIDATE ' +
	                'VALUES VARCHAR VARCHAR2 varchar2 VIEW VIEWS WHEN WHENEVER WHERE WHILE WITH WORK WRITE ' +
	                'TABLE SQLCODE SQLERRM USER',
	            built_in: 'ABS ACOS ADD_MONTHS ADJ_DATE APPENDCHILDXML ASCII ASCIISTR ASIN ATAN ATAN2 AVG ' +
	        'BFILENAME BIN_TO_NUM BINARY2VARCHAR BIT_COMPLEMENT BIT_OR BIT_XOR BITAND ' +
	        'BOOL_TO_INT CARDINALITY CASE CAST CAST_FROM_BINARY_DOUBLE CAST_FROM_BINARY_FLOAT ' +
	        'CAST_FROM_BINARY_INTEGER CAST_FROM_NUMBER CAST_TO_BINARY_DOUBLE CAST_TO_BINARY_FLOAT ' +
	        'CAST_TO_BINARY_INTEGER CAST_TO_NUMBER CAST_TO_NVARCHAR2 CAST_TO_RAW CAST_TO_VARCHAR ' +
	        'CEIL CHARTOROWID CHR CLUSTER_ID CLUSTER_PROBABILITY CLUSTER_SET COALESCE COLLECT ' +
	        'COMPOSE CONCAT CONVERT CORR CORR_K CORR_S COS COSH COUNT COVAR_POP COVAR_SAMP ' +
	        'CUME_DIST CURRENT_DATE CURRENT_TIMESTAMP CV DBTIMEZONE DENSE_RANK DECODE DECOMPOSE ' +
	        'DELETEXML DEPTH DEREF DUMP EMPTY_BLOB EMPTY_CLOB ESTIMATE_CPU_UNITS EXISTSNODE EXP ' +
	        'EXTRACT EXTRACTVALUE FEATURE_ID FEATURE_SET FEATURE_VALUE FIRST FIRST_VALUE FLOOR ' +
	        'FROM_TZ GET_CLOCK_TIME GET_DDL GET_DEPENDENT_DDL GET_DEPENDENT_XML GET_GRANTED_DDL ' +
	        'GET_GRANTED_XDL GET_HASH GET_REBUILD_COMMAND GET_SCN GET_XML GREATEST GROUP_ID ' +
	        'GROUPING GROUPING_ID HEXTORAW INITCAP INSERTCHILDXML INSERTXMLBEFORE INSTR INSTRB ' +
	        'INSTRC INSTR2 INSTR4 INT_TO_BOOL INTERVAL ITERATE ITERATION_NUMBER LAG LAST LAST_DAY ' +
	        'LAST_VALUE LEAD LEAST LENGTH LENGTHB LENGTHC LENGTH2 LENGTH4 LN LNNVL LOCALTIMESTAMP ' +
	        'LOG LOWER LPAD LTRIM MAKEREF MAX MEDIAN MIN MONTHS_BETWEEN MOD NANVL NEW_TIME NEXT_DAY ' +
	        'NHEXTORAW NLS_CHARSET_DECL_LEN NLS_CHARSET_ID NLS_CHARSET_NAME NLS_INITCAP NLS_LOWER ' +
	        'NLSSORT NLS_UPPER NTILE NULLFN NULLIF NUMTODSINTERVAL NUMTOHEX NUMTOHEX2 NUMTOYMINTERVAL ' +
	        'NVL NVL2 ORA_HASH PATH PERCENT_RANK PERCENTILE_CONT PERCENTILE_DISC POWER POWERMULTISET ' +
	        'POWERMULTISET_BY_CARDINALITY PREDICTION PREDICTION_BOUNDS PREDICTION_COST PREDICTION_DETAILS ' +
	        'PREDICTION_PROBABILITY PREDICTION_SET PRESENTNNV PRESENTV PREVIOUS QUOTE DELIMITERS ' +
	        'RANDOMBYTES RANDOMINTEGER RANDOMNUMBER RANK RATIO_TO_REPORT RAW_TO_CHAR RAW_TO_NCHAR ' +
	        'RAW_TO_VARCHAR2 RAWTOHEX RAWTONHEX RAWTONUM RAWTONUM2 REF REFTOHEX REGEXP_COUNT REGEXP_INSTR ' +
	        'REGEXP_REPLACE REGEXP_SUBSTR REGR_AVGX REGR_AVGY REGR_COUNT REGR_INTERCEPT REGR_R2 REGR_SLOPE ' +
	        'REGR_SXX REGR_SXY REGR_SYY REMAINDER REPLACE REVERSE ROUND ROW_NUMBER ROWIDTOCHAR ' +
	        'ROWIDTONCHAR RPAD RTRIM SCN_TO_TIMESTAMP SESSIONTIMEZONE SET SIGN SIN SINH SOUNDEX ' +
	        'SQRT STATS_BINOMIAL_TEST STATS_CROSSTAB STATS_F_TEST STATS_KS_TEST STATS_MODE ' +
	        'STATS_MW_TEST STATS_ONE_WAY_ANOVA STATS_T_TEST STATS_WSR_TEST STDDEV STDDEV_POP ' +
	        'STDDEV_SAMP STRING_TO_RAW SUBSTR SUBSTRB SUBSTRC SUBSTR2 SUBSTR4 SUM SYS_CONNECT_BY_PATH ' +
	        'SYS_CONTEXT SYS_DBURIGEN SYS_EXTRACT_UTC SYS_GUID SYS_OP_COMBINED_HASH SYS_OP_DESCEND ' +
	        'SYS_OP_DISTINCT SYS_OP_GUID SYS_OP_LBID SYS_OP_MAP_NONNULL SYS_OP_RAWTONUM SYS_OP_RPB ' +
	        'SYS_OP_TOSETID SYS_TYPEID SYS_XMLAGG SYS_XMLGEN SYSDATE SYSTIMESTAMP TAN TANH ' +
	        'TIMESTAMP_TO_SCN TO_BINARYDOUBLE TO_BINARYFLOAT TO_CHAR TO_CLOB TO_DATE TO_DSINTERVAL ' +
	        'TO_LOB TO_MULTI_BYTE TO_NCHAR TO_NCLOB TO_NUMBER TO_SINGLE_BYTE TO_TIMESTAMP ' +
	        'TO_TIMESTAMP_TZ TO_YMINTERVAL TRANSLATE TRANSLITERATE TREAT TRIM TRUNC TZ_OFFSET ' +
	        'UID UNISTR UPDATEXML UPPER USER USERENV VALUE VAR_POP VAR_SAMP VARIANCE VERIFY_OWNER ' +
	        'VERIFY_TABLE VERTICAL BARS VSIZE WIDTH_BUCKET XMLAGG XMLCAST XMLCDATA XMLCOLLATVAL ' +
	        'XMLCOMMENT XMLCONCAT XMLDIFF XMLELEMENT XMLEXISTS XMLFOREST XMLISVALID XMLPARSE ' +
	        'XMLPATCH XMLPI XMLQUERY XMLROOT XMLSEQUENCE XMLSERIALIZE XMLTABLE XMLTRANSFORM XOR'
	        },
	        c: [T.CLCM, T.CBCM, T.ASM, T.QSM, T.CNM, {
	            cN: "literal",
	            v: [{
	                b: "#\\s+[a-zA-Z\\ \\.]*",
	                r: 0
	            }, {
	                b: "#[a-zA-Z\\ \\.]+"
	            }]
	        }]
	    }
	});
	hljs.registerLanguage("php", function(e) {
	    var c = {
	            b: "\\$+[a-zA-Z_-ÿ][a-zA-Z0-9_-ÿ]*"
	        },
	        i = {
	            cN: "meta",
	            b: /<\?(php)?|\?>/
	        },
	        t = {
	            cN: "string",
	            c: [e.BE, i],
	            v: [{
	                    b: 'b"',
	                    e: '"'
	                }, {
	                    b: "b'",
	                    e: "'"
	                },
	                e.inherit(e.ASM, {
	                    i: null
	                }), e.inherit(e.QSM, {
	                    i: null
	                })
	            ]
	        },
	        a = {
	            v: [e.BNM, e.CNM]
	        };
	    return {
	        aliases: ["php3", "php4", "php5", "php6"],
	        cI: !0,
	        k: "and include_once list abstract global private echo interface as static endswitch array null if endwhile or const for endforeach self var while isset public protected exit foreach throw elseif include __FILE__ empty require_once do xor return parent clone use __CLASS__ __LINE__ else break print eval new catch __METHOD__ case exception default die require __FUNCTION__ enddeclare final try switch continue endfor endif declare unset true false trait goto instanceof insteadof __DIR__ __NAMESPACE__ yield finally",
	        c: [e.HCM, e.C("//", "$", {
	                c: [i]
	            }), e.C("/\\*", "\\*/", {
	                c: [{
	                    cN: "doctag",
	                    b: "@[A-Za-z]+"
	                }]
	            }), e.C("__halt_compiler.+?;", !1, {
	                eW: !0,
	                k: "__halt_compiler",
	                l: e.UIR
	            }), {
	                cN: "string",
	                b: /<<<['"]?\w+['"]?$/,
	                e: /^\w+;?$/,
	                c: [e.BE, {
	                    cN: "subst",
	                    v: [{
	                        b: /\$\w+/
	                    }, {
	                        b: /\{\$/,
	                        e: /\}/
	                    }]
	                }]
	            },
	            i, {
	                cN: "keyword",
	                b: /\$this\b/
	            },
	            c, {
	                b: /(::|->)+[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/
	            }, {
	                cN: "function",
	                bK: "function",
	                e: /[;{]/,
	                eE: !0,
	                i: "\\$|\\[|%",
	                c: [e.UTM, {
	                    cN: "params",
	                    b: "\\(",
	                    e: "\\)",
	                    c: ["self", c, e.CBCM, t, a]
	                }]
	            }, {
	                cN: "class",
	                bK: "class interface",
	                e: "{",
	                eE: !0,
	                i: /[:\(\$"]/,
	                c: [{
	                        bK: "extends implements"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "namespace",
	                e: ";",
	                i: /[\.']/,
	                c: [e.UTM]
	            }, {
	                bK: "use",
	                e: ";",
	                c: [e.UTM]
	            }, {
	                b: "=>"
	            },
	            t, a
	        ]
	    }
	});
	hljs.registerLanguage("javascript", function(e) {
	    return {
	        aliases: ["js", "jsx"],
	        k: {
	            keyword: "in of if for while finally var new function do return void else break catch instanceof with throw case default try this switch continue typeof delete let yield const export super debugger as async await static import from as",
	            literal: "true false null undefined NaN Infinity",
	            built_in: "eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape Object Function Boolean Error EvalError InternalError RangeError ReferenceError StopIteration SyntaxError TypeError URIError Number Math Date String RegExp Array Float32Array Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Uint8Array Uint8ClampedArray ArrayBuffer DataView JSON Intl arguments require module console window document Symbol Set Map WeakSet WeakMap Proxy Reflect Promise"
	        },
	        c: [{
	                cN: "meta",
	                r: 10,
	                b: /^\s*['"]use (strict|asm)['"]/
	            }, {
	                cN: "meta",
	                b: /^#!/,
	                e: /$/
	            },
	            e.ASM, e.QSM, {
	                cN: "string",
	                b: "`",
	                e: "`",
	                c: [e.BE, {
	                    cN: "subst",
	                    b: "\\$\\{",
	                    e: "\\}"
	                }]
	            },
	            e.CLCM, e.CBCM, {
	                cN: "number",
	                v: [{
	                    b: "\\b(0[bB][01]+)"
	                }, {
	                    b: "\\b(0[oO][0-7]+)"
	                }, {
	                    b: e.CNR
	                }],
	                r: 0
	            }, {
	                b: "(" + e.RSR + "|\\b(case|return|throw)\\b)\\s*",
	                k: "return throw case",
	                c: [e.CLCM, e.CBCM, e.RM, {
	                    b: /</,
	                    e: /(\/\w+|\w+\/)>/,
	                    sL: "xml",
	                    c: [{
	                        b: /<\w+\s*\/>/,
	                        skip: !0
	                    }, {
	                        b: /<\w+/,
	                        e: /(\/\w+|\w+\/)>/,
	                        skip: !0,
	                        c: ["self"]
	                    }]
	                }],
	                r: 0
	            }, {
	                cN: "function",
	                bK: "function",
	                e: /\{/,
	                eE: !0,
	                c: [e.inherit(e.TM, {
	                    b: /[A-Za-z$_][0-9A-Za-z$_]*/
	                }), {
	                    cN: "params",
	                    b: /\(/,
	                    e: /\)/,
	                    eB: !0,
	                    eE: !0,
	                    c: [e.CLCM, e.CBCM]
	                }],
	                i: /\[|%/
	            }, {
	                b: /\$[(.]/
	            },
	            e.METHOD_GUARD, {
	                cN: "class",
	                bK: "class",
	                e: /[{;=]/,
	                eE: !0,
	                i: /[:"\[\]]/,
	                c: [{
	                        bK: "extends"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "constructor",
	                e: /\{/,
	                eE: !0
	            }
	        ],
	        i: /#(?!!)/
	    }
	});
	hljs.registerLanguage("java", function(e) {
	    var t = e.UIR + "(<" + e.UIR + "(\\s*,\\s*" + e.UIR + ")*>)?",
	        a = "false synchronized int abstract float private char boolean static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports",
	        r = "\\b(0[bB]([01]+[01_]+[01]+|[01]+)|0[xX]([a-fA-F0-9]+[a-fA-F0-9_]+[a-fA-F0-9]+|[a-fA-F0-9]+)|(([\\d]+[\\d_]+[\\d]+|[\\d]+)(\\.([\\d]+[\\d_]+[\\d]+|[\\d]+))?|\\.([\\d]+[\\d_]+[\\d]+|[\\d]+))([eE][-+]?\\d+)?)[lLfF]?",
	        s = {
	            cN: "number",
	            b: r,
	            r: 0
	        };
	    return {
	        aliases: ["jsp"],
	        k: a,
	        i: /<\/|#/,
	        c: [e.C("/\\*\\*", "\\*/", {
	                r: 0,
	                c: [{
	                    b: /\w+@/,
	                    r: 0
	                }, {
	                    cN: "doctag",
	                    b: "@[A-Za-z]+"
	                }]
	            }), e.CLCM, e.CBCM, e.ASM, e.QSM, {
	                cN: "class",
	                bK: "class interface",
	                e: /[{;=]/,
	                eE: !0,
	                k: "class interface",
	                i: /[:"\[\]]/,
	                c: [{
	                        bK: "extends implements"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "new throw return else",
	                r: 0
	            }, {
	                cN: "function",
	                b: "(" + t + "\\s+)+" + e.UIR + "\\s*\\(",
	                rB: !0,
	                e: /[{;=]/,
	                eE: !0,
	                k: a,
	                c: [{
	                        b: e.UIR + "\\s*\\(",
	                        rB: !0,
	                        r: 0,
	                        c: [e.UTM]
	                    }, {
	                        cN: "params",
	                        b: /\(/,
	                        e: /\)/,
	                        k: a,
	                        r: 0,
	                        c: [e.ASM, e.QSM, e.CNM, e.CBCM]
	                    },
	                    e.CLCM, e.CBCM
	                ]
	            },
	            s, {
	                cN: "meta",
	                b: "@[A-Za-z]+"
	            }
	        ]
	    }
	});
	hljs.registerLanguage("css", function(e) {
	    var c = "[a-zA-Z-][a-zA-Z0-9_-]*",
	        t = {
	            b: /[A-Z\_\.\-]+\s*:/,
	            rB: !0,
	            e: ";",
	            eW: !0,
	            c: [{
	                cN: "attribute",
	                b: /\S/,
	                e: ":",
	                eE: !0,
	                starts: {
	                    eW: !0,
	                    eE: !0,
	                    c: [{
	                            b: /[\w-]+\(/,
	                            rB: !0,
	                            c: [{
	                                cN: "built_in",
	                                b: /[\w-]+/
	                            }, {
	                                b: /\(/,
	                                e: /\)/,
	                                c: [e.ASM, e.QSM]
	                            }]
	                        },
	                        e.CSSNM, e.QSM, e.ASM, e.CBCM, {
	                            cN: "number",
	                            b: "#[0-9A-Fa-f]+"
	                        }, {
	                            cN: "meta",
	                            b: "!important"
	                        }
	                    ]
	                }
	            }]
	        };
	    return {
	        cI: !0,
	        i: /[=\/|'\$]/,
	        c: [e.CBCM, {
	            cN: "selector-id",
	            b: /#[A-Za-z0-9_-]+/
	        }, {
	            cN: "selector-class",
	            b: /\.[A-Za-z0-9_-]+/
	        }, {
	            cN: "selector-attr",
	            b: /\[/,
	            e: /\]/,
	            i: "$"
	        }, {
	            cN: "selector-pseudo",
	            b: /:(:)?[a-zA-Z0-9\_\-\+\(\)"'.]+/
	        }, {
	            b: "@(font-face|page)",
	            l: "[a-z-]+",
	            k: "font-face page"
	        }, {
	            b: "@",
	            e: "[{;]",
	            i: /:/,
	            c: [{
	                cN: "keyword",
	                b: /\w+/
	            }, {
	                b: /\s/,
	                eW: !0,
	                eE: !0,
	                r: 0,
	                c: [e.ASM, e.QSM, e.CSSNM]
	            }]
	        }, {
	            cN: "selector-tag",
	            b: c,
	            r: 0
	        }, {
	            b: "{",
	            e: "}",
	            i: /\S/,
	            c: [e.CBCM, t]
	        }]
	    }
	});
	hljs.registerLanguage("xml", function(s) {
	    var e = "[A-Za-z0-9\\._:-]+",
	        t = {
	            eW: !0,
	            i: /</,
	            r: 0,
	            c: [{
	                cN: "attr",
	                b: e,
	                r: 0
	            }, {
	                b: /=\s*/,
	                r: 0,
	                c: [{
	                    cN: "string",
	                    endsParent: !0,
	                    v: [{
	                        b: /"/,
	                        e: /"/
	                    }, {
	                        b: /'/,
	                        e: /'/
	                    }, {
	                        b: /[^\s"'=<>`]+/
	                    }]
	                }]
	            }]
	        };
	    return {
	        aliases: ["html", "xhtml", "rss", "atom", "xjb", "xsd", "xsl", "plist"],
	        cI: !0,
	        c: [{
	                cN: "meta",
	                b: "<!DOCTYPE",
	                e: ">",
	                r: 10,
	                c: [{
	                    b: "\\[",
	                    e: "\\]"
	                }]
	            },
	            s.C("<!--", "-->", {
	                r: 10
	            }), {
	                b: "<\\!\\[CDATA\\[",
	                e: "\\]\\]>",
	                r: 10
	            }, {
	                b: /<\?(php)?/,
	                e: /\?>/,
	                sL: "php",
	                c: [{
	                    b: "/\\*",
	                    e: "\\*/",
	                    skip: !0
	                }]
	            }, {
	                cN: "tag",
	                b: "<style(?=\\s|>|$)",
	                e: ">",
	                k: {
	                    name: "style"
	                },
	                c: [t],
	                starts: {
	                    e: "</style>",
	                    rE: !0,
	                    sL: ["css", "xml"]
	                }
	            }, {
	                cN: "tag",
	                b: "<script(?=\\s|>|$)",
	                e: ">",
	                k: {
	                    name: "script"
	                },
	                c: [t],
	                starts: {
	                    e: "</script>",
	                    rE: !0,
	                    sL: ["actionscript", "javascript", "handlebars", "xml"]
	                }
	            }, {
	                cN: "meta",
	                v: [{
	                    b: /<\?xml/,
	                    e: /\?>/,
	                    r: 10
	                }, {
	                    b: /<\?\w+/,
	                    e: /\?>/
	                }]
	            }, {
	                cN: "tag",
	                b: "</?",
	                e: "/?>",
	                c: [{
	                        cN: "name",
	                        b: /[^\/><\s]+/,
	                        r: 0
	                    },
	                    t
	                ]
	            }
	        ]
	    }
	});
	hljs.registerLanguage("sql", function(e) {
	    var t = e.C("--", "$");
	    return {
	        cI: !0,
	        i: /[<>{}*#]/,
	        c: [{
	                bK: "begin end start commit rollback savepoint lock alter create drop rename call delete do handler insert load replace select truncate update set show pragma grant merge describe use explain help declare prepare execute deallocate release unlock purge reset change stop analyze cache flush optimize repair kill install uninstall checksum restore check backup revoke comment",
	                e: /;/,
	                eW: !0,
	                l: /[\w\.]+/,
	                k: {
	                    keyword: "abort abs absolute acc acce accep accept access accessed accessible account acos action activate add addtime admin administer advanced advise aes_decrypt aes_encrypt after agent aggregate ali alia alias allocate allow alter always analyze ancillary and any anydata anydataset anyschema anytype apply archive archived archivelog are as asc ascii asin assembly assertion associate asynchronous at atan atn2 attr attri attrib attribu attribut attribute attributes audit authenticated authentication authid authors auto autoallocate autodblink autoextend automatic availability avg backup badfile basicfile before begin beginning benchmark between bfile bfile_base big bigfile bin binary_double binary_float binlog bit_and bit_count bit_length bit_or bit_xor bitmap blob_base block blocksize body both bound buffer_cache buffer_pool build bulk by byte byteordermark bytes cache caching call calling cancel capacity cascade cascaded case cast catalog category ceil ceiling chain change changed char_base char_length character_length characters characterset charindex charset charsetform charsetid check checksum checksum_agg child choose chr chunk class cleanup clear client clob clob_base clone close cluster_id cluster_probability cluster_set clustering coalesce coercibility col collate collation collect colu colum column column_value columns columns_updated comment commit compact compatibility compiled complete composite_limit compound compress compute concat concat_ws concurrent confirm conn connec connect connect_by_iscycle connect_by_isleaf connect_by_root connect_time connection consider consistent constant constraint constraints constructor container content contents context contributors controlfile conv convert convert_tz corr corr_k corr_s corresponding corruption cos cost count count_big counted covar_pop covar_samp cpu_per_call cpu_per_session crc32 create creation critical cross cube cume_dist curdate current current_date current_time current_timestamp current_user cursor curtime customdatum cycle data database databases datafile datafiles datalength date_add date_cache date_format date_sub dateadd datediff datefromparts datename datepart datetime2fromparts day day_to_second dayname dayofmonth dayofweek dayofyear days db_role_change dbtimezone ddl deallocate declare decode decompose decrement decrypt deduplicate def defa defau defaul default defaults deferred defi defin define degrees delayed delegate delete delete_all delimited demand dense_rank depth dequeue des_decrypt des_encrypt des_key_file desc descr descri describ describe descriptor deterministic diagnostics difference dimension direct_load directory disable disable_all disallow disassociate discardfile disconnect diskgroup distinct distinctrow distribute distributed div do document domain dotnet double downgrade drop dumpfile duplicate duration each edition editionable editions element ellipsis else elsif elt empty enable enable_all enclosed encode encoding encrypt end end-exec endian enforced engine engines enqueue enterprise entityescaping eomonth error errors escaped evalname evaluate event eventdata events except exception exceptions exchange exclude excluding execu execut execute exempt exists exit exp expire explain export export_set extended extent external external_1 external_2 externally extract failed failed_login_attempts failover failure far fast feature_set feature_value fetch field fields file file_name_convert filesystem_like_logging final finish first first_value fixed flash_cache flashback floor flush following follows for forall force form forma format found found_rows freelist freelists freepools fresh from from_base64 from_days ftp full function general generated get get_format get_lock getdate getutcdate global global_name globally go goto grant grants greatest group group_concat group_id grouping grouping_id groups gtid_subtract guarantee guard handler hash hashkeys having hea head headi headin heading heap help hex hierarchy high high_priority hosts hour http id ident_current ident_incr ident_seed identified identity idle_time if ifnull ignore iif ilike ilm immediate import in include including increment index indexes indexing indextype indicator indices inet6_aton inet6_ntoa inet_aton inet_ntoa infile initial initialized initially initrans inmemory inner innodb input insert install instance instantiable instr interface interleaved intersect into invalidate invisible is is_free_lock is_ipv4 is_ipv4_compat is_not is_not_null is_used_lock isdate isnull isolation iterate java join json json_exists keep keep_duplicates key keys kill language large last last_day last_insert_id last_value lax lcase lead leading least leaves left len lenght length less level levels library like like2 like4 likec limit lines link list listagg little ln load load_file lob lobs local localtime localtimestamp locate locator lock locked log log10 log2 logfile logfiles logging logical logical_reads_per_call logoff logon logs long loop low low_priority lower lpad lrtrim ltrim main make_set makedate maketime managed management manual map mapping mask master master_pos_wait match matched materialized max maxextents maximize maxinstances maxlen maxlogfiles maxloghistory maxlogmembers maxsize maxtrans md5 measures median medium member memcompress memory merge microsecond mid migration min minextents minimum mining minus minute minvalue missing mod mode model modification modify module monitoring month months mount move movement multiset mutex name name_const names nan national native natural nav nchar nclob nested never new newline next nextval no no_write_to_binlog noarchivelog noaudit nobadfile nocheck nocompress nocopy nocycle nodelay nodiscardfile noentityescaping noguarantee nokeep nologfile nomapping nomaxvalue nominimize nominvalue nomonitoring none noneditionable nonschema noorder nopr nopro noprom nopromp noprompt norely noresetlogs noreverse normal norowdependencies noschemacheck noswitch not nothing notice notrim novalidate now nowait nth_value nullif nulls num numb numbe nvarchar nvarchar2 object ocicoll ocidate ocidatetime ociduration ociinterval ociloblocator ocinumber ociref ocirefcursor ocirowid ocistring ocitype oct octet_length of off offline offset oid oidindex old on online only opaque open operations operator optimal optimize option optionally or oracle oracle_date oradata ord ordaudio orddicom orddoc order ordimage ordinality ordvideo organization orlany orlvary out outer outfile outline output over overflow overriding package pad parallel parallel_enable parameters parent parse partial partition partitions pascal passing password password_grace_time password_lock_time password_reuse_max password_reuse_time password_verify_function patch path patindex pctincrease pctthreshold pctused pctversion percent percent_rank percentile_cont percentile_disc performance period period_add period_diff permanent physical pi pipe pipelined pivot pluggable plugin policy position post_transaction pow power pragma prebuilt precedes preceding precision prediction prediction_cost prediction_details prediction_probability prediction_set prepare present preserve prior priority private private_sga privileges procedural procedure procedure_analyze processlist profiles project prompt protection public publishingservername purge quarter query quick quiesce quota quotename radians raise rand range rank raw read reads readsize rebuild record records recover recovery recursive recycle redo reduced ref reference referenced references referencing refresh regexp_like register regr_avgx regr_avgy regr_count regr_intercept regr_r2 regr_slope regr_sxx regr_sxy reject rekey relational relative relaylog release release_lock relies_on relocate rely rem remainder rename repair repeat replace replicate replication required reset resetlogs resize resource respect restore restricted result result_cache resumable resume retention return returning returns reuse reverse revoke right rlike role roles rollback rolling rollup round row row_count rowdependencies rowid rownum rows rtrim rules safe salt sample save savepoint sb1 sb2 sb4 scan schema schemacheck scn scope scroll sdo_georaster sdo_topo_geometry search sec_to_time second section securefile security seed segment select self sequence sequential serializable server servererror session session_user sessions_per_user set sets settings sha sha1 sha2 share shared shared_pool short show shrink shutdown si_averagecolor si_colorhistogram si_featurelist si_positionalcolor si_stillimage si_texture siblings sid sign sin size size_t sizes skip slave sleep smalldatetimefromparts smallfile snapshot some soname sort soundex source space sparse spfile split sql sql_big_result sql_buffer_result sql_cache sql_calc_found_rows sql_small_result sql_variant_property sqlcode sqldata sqlerror sqlname sqlstate sqrt square standalone standby start starting startup statement static statistics stats_binomial_test stats_crosstab stats_ks_test stats_mode stats_mw_test stats_one_way_anova stats_t_test_ stats_t_test_indep stats_t_test_one stats_t_test_paired stats_wsr_test status std stddev stddev_pop stddev_samp stdev stop storage store stored str str_to_date straight_join strcmp strict string struct stuff style subdate subpartition subpartitions substitutable substr substring subtime subtring_index subtype success sum suspend switch switchoffset switchover sync synchronous synonym sys sys_xmlagg sysasm sysaux sysdate sysdatetimeoffset sysdba sysoper system system_user sysutcdatetime table tables tablespace tan tdo template temporary terminated tertiary_weights test than then thread through tier ties time time_format time_zone timediff timefromparts timeout timestamp timestampadd timestampdiff timezone_abbr timezone_minute timezone_region to to_base64 to_date to_days to_seconds todatetimeoffset trace tracking transaction transactional translate translation treat trigger trigger_nestlevel triggers trim truncate try_cast try_convert try_parse type ub1 ub2 ub4 ucase unarchived unbounded uncompress under undo unhex unicode uniform uninstall union unique unix_timestamp unknown unlimited unlock unpivot unrecoverable unsafe unsigned until untrusted unusable unused update updated upgrade upped upper upsert url urowid usable usage use use_stored_outlines user user_data user_resources users using utc_date utc_timestamp uuid uuid_short validate validate_password_strength validation valist value values var var_samp varcharc vari varia variab variabl variable variables variance varp varraw varrawc varray verify version versions view virtual visible void wait wallet warning warnings week weekday weekofyear wellformed when whene whenev wheneve whenever where while whitespace with within without work wrapped xdb xml xmlagg xmlattributes xmlcast xmlcolattval xmlelement xmlexists xmlforest xmlindex xmlnamespaces xmlpi xmlquery xmlroot xmlschema xmlserialize xmltable xmltype xor year year_to_month years yearweek",
	                    literal: "true false null",
	                    built_in: "array bigint binary bit blob boolean char character date dec decimal float int int8 integer interval number numeric real record serial serial8 smallint text varchar varying void"
	                },
	                c: [{
	                        cN: "string",
	                        b: "'",
	                        e: "'",
	                        c: [e.BE, {
	                            b: "''"
	                        }]
	                    }, {
	                        cN: "string",
	                        b: '"',
	                        e: '"',
	                        c: [e.BE, {
	                            b: '""'
	                        }]
	                    }, {
	                        cN: "string",
	                        b: "`",
	                        e: "`",
	                        c: [e.BE]
	                    },
	                    e.CNM, e.CBCM, t
	                ]
	            },
	            e.CBCM, t
	        ]
	    }
	});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(126)))

/***/ },
/* 127 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(128);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js!./github.css", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js!./github.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 128 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "/*\n\ngithub.com style (c) Vasily Polovnyov <vast@whiteants.net>\n\n*/\n\n.hljs {\n  display: block;\n  overflow-x: auto;\n  padding: 0.5em;\n  color: #333;\n  background: #f8f8f8;\n}\n\n.hljs-comment,\n.hljs-quote {\n  color: #998;\n  font-style: italic;\n}\n\n.hljs-keyword,\n.hljs-selector-tag,\n.hljs-subst {\n  color: #333;\n  font-weight: bold;\n}\n\n.hljs-number,\n.hljs-literal,\n.hljs-variable,\n.hljs-template-variable,\n.hljs-tag .hljs-attr {\n  color: #008080;\n}\n\n.hljs-string,\n.hljs-doctag {\n  color: #d14;\n}\n\n.hljs-title,\n.hljs-section,\n.hljs-selector-id {\n  color: #900;\n  font-weight: bold;\n}\n\n.hljs-subst {\n  font-weight: normal;\n}\n\n.hljs-type,\n.hljs-class .hljs-title {\n  color: #458;\n  font-weight: bold;\n}\n\n.hljs-tag,\n.hljs-name,\n.hljs-attribute {\n  color: #000080;\n  font-weight: normal;\n}\n\n.hljs-regexp,\n.hljs-link {\n  color: #009926;\n}\n\n.hljs-symbol,\n.hljs-bullet {\n  color: #990073;\n}\n\n.hljs-built_in,\n.hljs-builtin-name {\n  color: #0086b3;\n}\n\n.hljs-meta {\n  color: #999;\n  font-weight: bold;\n}\n\n.hljs-deletion {\n  background: #fdd;\n}\n\n.hljs-addition {\n  background: #dfd;\n}\n\n.hljs-emphasis {\n  font-style: italic;\n}\n\n.hljs-strong {\n  font-weight: bold;\n}\n", ""]);

	// exports


/***/ },
/* 129 */
/***/ function(module, exports) {

	'use strict';

	angular.module("service")
	    .service('userCourses', UserCourses);


	UserCourses.$inject = ['$http','BASE_API'];

	function UserCourses($http,BASE_API) {

	    //deprecated
	    this.getPurchasedCourses = function(data){
	        return $http.post(BASE_API + '/uc/getPurchasedCourseOfUser',data);
	    }

	    this.updatePurchasedCourses = function(data){
	        return $http.post(BASE_API + '/uc/updatePurchasedCourses',data);
	    }

	    // addPurchasedCourse
	    this.addPurchasedCourse = function(data){
	        return $http.post(BASE_API + '/uc/addPurchasedCourse',data);
	    }

	    this.getPurchasedCourseOfUser = function(data) {
	        return $http.post(BASE_API + '/uc/getPurchasedCourseOfUser',data);
	    }

	    this.getPurchasedCourseDetails = function(data) {
	        return $http.post(BASE_API + '/uc/getPurchasedCourseDetails',data);
	    }
	};

/***/ },
/* 130 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(59);
	__webpack_require__(51);
	__webpack_require__(57);
	__webpack_require__(125);
	__webpack_require__(131);
	__webpack_require__(132);

	__webpack_require__(133);
	angular.module("directive")
	    .component('loginComponent', {
	        template: __webpack_require__(135),
	        controller: LoginController,
	        controllerAs: 'vm',
	        bindings: {
	            loginDelegate : '='
	        }
	    });

	LoginController.$inject = ['authToken', 'utility','$timeout','$scope','$window','auth','$auth','$state'];

	function LoginController(authToken, utility,$timeout,$scope,$window,auth,$auth,$state) {
	    var vm = this;

	    vm.$onInit = function(){
	        vm.loginDelegate  = vm.loginDelegate || {};

	        vm.loginDelegate.toggleLoginModal =vm.toggleLoginModal = toggleLoginModal;

	        vm.checkAlreadyLogin = checkAlreadyLogin;
	        vm.doLogin = doLogin;
	        vm.authenticateWith = authenticateWith;    
	    }
	    
	    goToDashboardApp = utility.goToDashboardApp;

	    //toggle login modal
	    function toggleLoginModal(d) {
	    	if (!vm.isLoginModalOpen) {
	            //It is not opened
	            //Now it is going to open
	            $("#login-modal").css("height", "100vh");
	            $timeout(function() {
	                vm.isLoginModalOpen = true;
	            }, 300);
	        } else {
	            //if it is opened
	            //then close it
	            $("#login-modal").css("height", "0px");
	            vm.isLoginModalOpen = false;
	        }
	    }

	    //check if already login
	    function checkAlreadyLogin() {
	        // vm.toggleLoginModal();
	        $state.reload();
	    }


	    //do login
	    function doLogin() {
	        if (!vm.userId || !vm.password) {
	            vm.formError = "Please fill all required fields.";
	            return;
	        }
	        var userloginData = {
	            usr_id: vm.userId,
	            pwd: vm.password
	        };
	        vm.isFormSubmitting = true;
	        vm.formError = undefined;
	        vm.userId = "";
	        vm.password = "";
	        auth.login(userloginData).then(function(resp) {
	            var res = resp.data;
	            if (res.err) {
	                vm.formError = res.err;
	                return;
	            }
	            checkAlreadyLogin();
	        }).catch(function(err) {
	            console.error(err);
	            if(err.status === 500){
	                vm.formError = err.data.err;
	            }else{
	                vm.formError = err;
	            }
	        })
	        .finally(function() {
	            vm.isFormSubmitting = false;
	        })
	    }

	    //authenticate with facebook,twitter,linkedin,google etc...
	    function authenticateWith(provider) {
	        vm.isFormSubmitting = true;
	        $auth.authenticate(provider)
	            .then(function(response) {
	                authToken.setToken(response.data.token);
	                authToken.setUser(response.data.user);
	                checkAlreadyLogin();
	            })
	            .catch(function(response) {
	                console.error(response);
	                vm.formError = response.error;
	            })
	            .finally(function() {
	                vm.isFormSubmitting = false;
	            })
	    }
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 131 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(57);
	angular.module('service').service('auth', auth);
	auth.$inject = ['$http', 'authToken','BASE_API'];

	function auth($http, authToken,BASE_API) {

	    function authSuccessful(res) {
	        if(res.data && res.data.token || res.data.user){
	            authToken.setToken(res.data.token);
	            authToken.setUser(res.data.user);
	            authToken.setUserCourses(res.data.uc);
	        }
	        return res
	    }

	    this.login = function(data) {
	        return $http.post(BASE_API + '/auth/login',data).then(authSuccessful);
	    }
	    this.logout = function(data) {
	        return $http.post(BASE_API + '/auth/logout',data);
	    }
	    this.register = function(data){
	        return $http.post(BASE_API + '/auth/register',data);   
	    }
	    this.verifyOTP = function(data){
	        return $http.post(BASE_API + '/auth/verifyOTP',data).then(authSuccessful);   
	    }
	    this.resendOTP = function(data){
	        return $http.post(BASE_API + '/auth/resendOTP',data);   
	    }
	    this.removeUser = function(data){
	        return $http.post(BASE_API + '/auth/removeUser',data);   
	    }
	    this.checkUserExistance = function(data){
	        return $http.post(BASE_API + '/auth/checkUserExistance',data)
	                .then(function(res){
	                    return res.data;
	                });
	    }
	    this.checkPassword = function checkPassword(data){
	        return $http.post(BASE_API + '/auth/checkPassword',data);
	    }
	    this.changePassword = function changePassword(data){
	        console.log("data.........",data);
	        return $http.post(BASE_API + '/auth/changePassword',data);   
	    }

	    this.forgotPassword = function (data){
	        return $http.post(BASE_API + '/auth/forgotPassword',data);   
	    }
	    this.resendForgotPasswordOTP = function (data){
	        return $http.post(BASE_API + '/auth/resendForgotPasswordOTP',data);   
	    }
	    this.verifyForgotPasswordOTP = function(data){
	        return $http.post(BASE_API + '/auth/verifyForgotPasswordOTP',data);   
	    }
	}

/***/ },
/* 132 */
/***/ function(module, exports) {

	/**
	 * Satellizer 0.14.1
	 * (c) 2016 Sahat Yalkabov
	 * License: MIT
	 */

	// CommonJS package manager support.
	if (typeof module !== 'undefined' && typeof exports !== 'undefined' && module.exports === exports) {
	  module.exports = 'satellizer';
	}

	(function(window, angular, undefined) {
	  'use strict';

	  if (!window.location.origin) {
	    window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? (':' + window.location.port) : '');
	  }

	  angular.module('satellizer', [])
	    .constant('SatellizerConfig', {
	      httpInterceptor: function() { return true; },
	      withCredentials: false,
	      tokenRoot: null,
	      baseUrl: '/',
	      loginUrl: '/auth/login',
	      signupUrl: '/auth/signup',
	      unlinkUrl: '/auth/unlink',
	      tokenName: 'token',
	      tokenPrefix: 'satellizer',
	      authHeader: 'Authorization',
	      authToken: 'Bearer',
	      storageType: 'localStorage',
	      providers: {
	        facebook: {
	          name: 'facebook',
	          url: '/auth/facebook',
	          authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
	          redirectUri: window.location.origin + '/',
	          requiredUrlParams: ['display', 'scope'],
	          scope: ['email'],
	          scopeDelimiter: ',',
	          display: 'popup',
	          oauthType: '2.0',
	          popupOptions: { width: 580, height: 400 }
	        },
	        google: {
	          name: 'google',
	          url: '/auth/google',
	          authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
	          redirectUri: window.location.origin,
	          requiredUrlParams: ['scope'],
	          optionalUrlParams: ['display', 'state'],
	          scope: ['profile', 'email'],
	          scopePrefix: 'openid',
	          scopeDelimiter: ' ',
	          display: 'popup',
	          oauthType: '2.0',
	          popupOptions: { width: 452, height: 633 },
	          state: function() {
	            var rand = Math.random().toString(36).substr(2);
	            return encodeURIComponent(rand);
	          }
	        },
	        github: {
	          name: 'github',
	          url: '/auth/github',
	          authorizationEndpoint: 'https://github.com/login/oauth/authorize',
	          redirectUri: window.location.origin,
	          optionalUrlParams: ['scope'],
	          scope: ['user:email'],
	          scopeDelimiter: ' ',
	          oauthType: '2.0',
	          popupOptions: { width: 1020, height: 618 }
	        },
	        instagram: {
	          name: 'instagram',
	          url: '/auth/instagram',
	          authorizationEndpoint: 'https://api.instagram.com/oauth/authorize',
	          redirectUri: window.location.origin,
	          requiredUrlParams: ['scope'],
	          scope: ['basic'],
	          scopeDelimiter: '+',
	          oauthType: '2.0'
	        },
	        linkedin: {
	          name: 'linkedin',
	          url: '/auth/linkedin',
	          authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
	          redirectUri: window.location.origin,
	          requiredUrlParams: ['state'],
	          scope: ['r_emailaddress'],
	          scopeDelimiter: ' ',
	          state: 'STATE',
	          oauthType: '2.0',
	          popupOptions: { width: 527, height: 582 }
	        },
	        twitter: {
	          name: 'twitter',
	          url: '/auth/twitter',
	          authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
	          redirectUri: window.location.origin,
	          oauthType: '1.0',
	          popupOptions: { width: 495, height: 645 }
	        },
	        twitch: {
	          name: 'twitch',
	          url: '/auth/twitch',
	          authorizationEndpoint: 'https://api.twitch.tv/kraken/oauth2/authorize',
	          redirectUri: window.location.origin,
	          requiredUrlParams: ['scope'],
	          scope: ['user_read'],
	          scopeDelimiter: ' ',
	          display: 'popup',
	          oauthType: '2.0',
	          popupOptions: { width: 500, height: 560 }
	        },
	        live: {
	          name: 'live',
	          url: '/auth/live',
	          authorizationEndpoint: 'https://login.live.com/oauth20_authorize.srf',
	          redirectUri: window.location.origin,
	          requiredUrlParams: ['display', 'scope'],
	          scope: ['wl.emails'],
	          scopeDelimiter: ' ',
	          display: 'popup',
	          oauthType: '2.0',
	          popupOptions: { width: 500, height: 560 }
	        },
	        yahoo: {
	          name: 'yahoo',
	          url: '/auth/yahoo',
	          authorizationEndpoint: 'https://api.login.yahoo.com/oauth2/request_auth',
	          redirectUri: window.location.origin,
	          scope: [],
	          scopeDelimiter: ',',
	          oauthType: '2.0',
	          popupOptions: { width: 559, height: 519 }
	        },
	        bitbucket: {
	          name: 'bitbucket',
	          url: '/auth/bitbucket',
	          authorizationEndpoint: 'https://bitbucket.org/site/oauth2/authorize',
	          redirectUri: window.location.origin + '/',
	          requiredUrlParams: ['scope'],
	          scope: ['email'],
	          scopeDelimiter: ' ',
	          oauthType: '2.0',
	          popupOptions: { width: 1028, height: 529 }
	        }
	      }
	    })
	    .provider('$auth', ['SatellizerConfig', function(config) {
	      Object.defineProperties(this, {
	        httpInterceptor: {
	          get: function() { return config.httpInterceptor; },
	          set: function(value) {
	            if (typeof value === 'function') {
	              config.httpInterceptor = value;
	            } else {
	              config.httpInterceptor = function() {
	                return value;
	              };
	            }
	          }
	        },
	        baseUrl: {
	          get: function() { return config.baseUrl; },
	          set: function(value) { config.baseUrl = value; }
	        },
	        loginUrl: {
	          get: function() { return config.loginUrl; },
	          set: function(value) { config.loginUrl = value; }
	        },
	        signupUrl: {
	          get: function() { return config.signupUrl; },
	          set: function(value) { config.signupUrl = value; }
	        },
	        tokenRoot: {
	          get: function() { return config.tokenRoot; },
	          set: function(value) { config.tokenRoot = value; }
	        },
	        tokenName: {
	          get: function() { return config.tokenName; },
	          set: function(value) { config.tokenName = value; }
	        },
	        tokenPrefix: {
	          get: function() { return config.tokenPrefix; },
	          set: function(value) { config.tokenPrefix = value; }
	        },
	        unlinkUrl: {
	          get: function() { return config.unlinkUrl; },
	          set: function(value) { config.unlinkUrl = value; }
	        },
	        authHeader: {
	          get: function() { return config.authHeader; },
	          set: function(value) { config.authHeader = value; }
	        },
	        authToken: {
	          get: function() { return config.authToken; },
	          set: function(value) { config.authToken = value; }
	        },
	        withCredentials: {
	          get: function() { return config.withCredentials; },
	          set: function(value) { config.withCredentials = value; }
	        },
	        storageType: {
	          get: function() { return config.storageType; },
	          set: function(value) { config.storageType = value; }
	        }
	      });

	      angular.forEach(Object.keys(config.providers), function(provider) {
	        this[provider] = function(params) {
	          return angular.extend(config.providers[provider], params);
	        };
	      }, this);

	      var oauth = function(params) {
	        config.providers[params.name] = config.providers[params.name] || {};
	        angular.extend(config.providers[params.name], params);
	      };

	      this.oauth1 = function(params) {
	        oauth(params);
	        config.providers[params.name].oauthType = '1.0';
	      };

	      this.oauth2 = function(params) {
	        oauth(params);
	        config.providers[params.name].oauthType = '2.0';
	      };

	      this.$get = [
	        '$q',
	        'SatellizerShared',
	        'SatellizerLocal',
	        'SatellizerOauth',
	        function($q, shared, local, oauth) {
	          var $auth = {};

	          $auth.login = function(user, opts) {
	            return local.login(user, opts);
	          };

	          $auth.signup = function(user, options) {
	            return local.signup(user, options);
	          };

	          $auth.logout = function() {
	            return shared.logout();
	          };

	          $auth.authenticate = function(name, userData) {
	            return oauth.authenticate(name, userData);
	          };

	          $auth.link = function(name, userData) {
	            return oauth.authenticate(name, userData);
	          };

	          $auth.unlink = function(provider, opts) {
	            return oauth.unlink(provider, opts);
	          };

	          $auth.isAuthenticated = function() {
	            return shared.isAuthenticated();
	          };

	          $auth.getToken = function() {
	            return shared.getToken();
	          };

	          $auth.setToken = function(token) {
	            shared.setToken({ access_token: token });
	          };

	          $auth.removeToken = function() {
	            return shared.removeToken();
	          };

	          $auth.getPayload = function() {
	            return shared.getPayload();
	          };

	          $auth.setStorageType = function(type) {
	            return shared.setStorageType(type);
	          };

	          return $auth;
	        }];
	    }])
	    .factory('SatellizerShared', [
	      '$q',
	      '$window',
	      '$log',
	      'SatellizerConfig',
	      'SatellizerStorage',
	      function($q, $window, $log, config, storage) {
	        var Shared = {};

	        var tokenName = config.tokenPrefix ? [config.tokenPrefix, config.tokenName].join('_') : config.tokenName;

	        Shared.getToken = function() {
	          return storage.get(tokenName);
	        };

	        Shared.getPayload = function() {
	          var token = storage.get(tokenName);

	          if (token && token.split('.').length === 3) {
	            try {
	              var base64Url = token.split('.')[1];
	              var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
	              return JSON.parse(decodeURIComponent(escape(window.atob(base64))));
	            } catch(e) {
	              return undefined;
	            }
	          }
	        };

	        Shared.setToken = function(response) {
	          if (!response) {
	            return $log.warn('Can\'t set token without passing a value');
	          }

	          var accessToken = response && response.access_token;
	          var token;

	          if (accessToken) {
	            if (angular.isObject(accessToken) && angular.isObject(accessToken.data)) {
	              response = accessToken;
	            } else if (angular.isString(accessToken)) {
	              token = accessToken;
	            }
	          }

	          if (!token && response) {
	            var tokenRootData = config.tokenRoot && config.tokenRoot.split('.').reduce(function(o, x) { return o[x]; }, response.data);
	            token = tokenRootData ? tokenRootData[config.tokenName] : response.data && response.data[config.tokenName];
	          }

	          if (!token) {
	            var tokenPath = config.tokenRoot ? config.tokenRoot + '.' + config.tokenName : config.tokenName;
	            return $log.warn('Expecting a token named "' + tokenPath);
	          }

	          storage.set(tokenName, token);
	        };

	        Shared.removeToken = function() {
	          storage.remove(tokenName);
	        };

	        /**
	         * @returns {boolean}
	         */
	        Shared.isAuthenticated = function() {
	          var token = storage.get(tokenName);
	          // A token is present
	          if (token) {
	            // Token with a valid JWT format XXX.YYY.ZZZ
	            if (token.split('.').length === 3) {
	              // Could be a valid JWT or an access token with the same format
	              try {
	                var base64Url = token.split('.')[1];
	                var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
	                var exp = JSON.parse($window.atob(base64)).exp;
	                // JWT with an optonal expiration claims
	                if (exp) {
	                  var isExpired = Math.round(new Date().getTime() / 1000) >= exp;
	                  if (isExpired) {
	                    // FAIL: Expired token
	                    return false;
	                  } else {
	                    // PASS: Non-expired token
	                    return true;
	                  }
	                }
	              } catch(e) {
	                // PASS: Non-JWT token that looks like JWT
	                return true;
	              }
	            }
	            // PASS: All other tokens
	            return true;
	          }
	          // FAIL: No token at all
	          return false;
	        };

	        Shared.logout = function() {
	          storage.remove(tokenName);
	          return $q.when();
	        };

	        Shared.setStorageType = function(type) {
	          config.storageType = type;
	        };

	        return Shared;
	      }])
	    .factory('SatellizerOauth', [
	      '$q',
	      '$http',
	      'SatellizerConfig',
	      'SatellizerUtils',
	      'SatellizerShared',
	      'SatellizerOauth1',
	      'SatellizerOauth2',
	      function($q, $http, config, utils, shared, Oauth1, Oauth2) {
	        var Oauth = {};

	        Oauth.authenticate = function(name, userData) {
	          var provider = config.providers[name].oauthType === '1.0' ? new Oauth1() : new Oauth2();
	          var deferred = $q.defer();

	          provider.open(config.providers[name], userData || {})
	            .then(function(response) {
	              // This is for a scenario when someone wishes to opt out from
	              // Satellizer's magic by doing authorization code exchange and
	              // saving a token manually.
	              if (config.providers[name].url) {
	                shared.setToken(response, false);
	              }
	              deferred.resolve(response);
	            })
	            .catch(function(error) {
	              deferred.reject(error);
	            });

	          return deferred.promise;
	        };

	        Oauth.unlink = function(provider, opts) {
	          opts = opts || {};
	          opts.url = opts.url ? opts.url : utils.joinUrl(config.baseUrl, config.unlinkUrl);
	          opts.data = { provider: provider } || opts.data;
	          opts.method = opts.method || 'POST';
	          opts.withCredentials = opts.withCredentials || config.withCredentials;

	          return $http(opts);
	        };

	        return Oauth;
	      }])
	    .factory('SatellizerLocal', [
	      '$http',
	      'SatellizerUtils',
	      'SatellizerShared',
	      'SatellizerConfig',
	      function($http, utils, shared, config) {
	        var Local = {};

	        Local.login = function(user, opts) {
	          opts = opts || {};
	          opts.url = opts.url ? opts.url : utils.joinUrl(config.baseUrl, config.loginUrl);
	          opts.data = user || opts.data;
	          opts.method = opts.method || 'POST';
	          opts.withCredentials = opts.withCredentials || config.withCredentials;

	          return $http(opts).then(function(response) {
	            shared.setToken(response);
	            return response;
	          });
	        };

	        Local.signup = function(user, opts) {
	          opts = opts || {};
	          opts.url = opts.url ? opts.url : utils.joinUrl(config.baseUrl, config.signupUrl);
	          opts.data = user || opts.data;
	          opts.method = opts.method || 'POST';
	          opts.withCredentials = opts.withCredentials || config.withCredentials;

	          return $http(opts);
	        };

	        return Local;
	      }])
	    .factory('SatellizerOauth2', [
	      '$q',
	      '$http',
	      '$window',
	      '$timeout',
	      'SatellizerPopup',
	      'SatellizerUtils',
	      'SatellizerConfig',
	      'SatellizerStorage',
	      function($q, $http, $window, $timeout, popup, utils, config, storage) {
	        return function() {
	          var Oauth2 = {};

	          var defaults = {
	            defaultUrlParams: ['response_type', 'client_id', 'redirect_uri'],
	            responseType: 'code',
	            responseParams: {
	              code: 'code',
	              clientId: 'clientId',
	              redirectUri: 'redirectUri'
	            }
	          };

	          Oauth2.open = function(options, userData) {
	            defaults = utils.merge(options, defaults);
	            var defer = $q.defer();

	            $timeout(function () {
	              var url;
	              var openPopup;
	              var stateName = defaults.name + '_state';

	              if (angular.isFunction(defaults.state)) {
	                storage.set(stateName, defaults.state());
	              } else if (angular.isString(defaults.state)) {
	                storage.set(stateName, defaults.state);
	              }

	              url = [defaults.authorizationEndpoint, Oauth2.buildQueryString()].join('?');

	              if (window.cordova) {
	                openPopup = popup.open(url, defaults.name, defaults.popupOptions, defaults.redirectUri).eventListener(defaults.redirectUri);
	              } else {
	                openPopup = popup.open(url, defaults.name, defaults.popupOptions, defaults.redirectUri).pollPopup(defaults.redirectUri);
	              }

	              openPopup
	                .then(function(oauthData) {
	                  // When no server URL provided, return popup params as-is.
	                  // This is for a scenario when someone wishes to opt out from
	                  // Satellizer's magic by doing authorization code exchange and
	                  // saving a token manually.
	                  if (defaults.responseType === 'token' || !defaults.url) {
	                    return defer.resolve(oauthData);
	                  }

	                  if (oauthData.state && oauthData.state !== storage.get(stateName)) {
	                    return defer.reject(
	                      'The value returned in the state parameter does not match the state value from your original ' +
	                      'authorization code request.'
	                    );
	                  }

	                  defer.resolve(Oauth2.exchangeForToken(oauthData, userData));
	                }, function (err) {
	                  defer.reject(err);
	                });
	            });

	            return defer.promise;
	          };

	          Oauth2.exchangeForToken = function(oauthData, userData) {
	            var data = angular.extend({}, userData);

	            angular.forEach(defaults.responseParams, function(value, key) {
	              switch (key) {
	                case 'code':
	                  data[value] = oauthData.code;
	                  break;
	                case 'clientId':
	                  data[value] = defaults.clientId;
	                  break;
	                case 'redirectUri':
	                  data[value] = defaults.redirectUri;
	                  break;
	                default:
	                  data[value] = oauthData[key];
	              }
	            });

	            if (oauthData.state) {
	              data.state = oauthData.state;
	            }

	            var exchangeForTokenUrl = config.baseUrl ? utils.joinUrl(config.baseUrl, defaults.url) : defaults.url;

	            return $http.post(exchangeForTokenUrl, data, { withCredentials: config.withCredentials });
	          };

	          Oauth2.buildQueryString = function() {
	            var keyValuePairs = [];
	            var urlParamsCategories = ['defaultUrlParams', 'requiredUrlParams', 'optionalUrlParams'];

	            angular.forEach(urlParamsCategories, function(paramsCategory) {
	              angular.forEach(defaults[paramsCategory], function(paramName) {
	                var camelizedName = utils.camelCase(paramName);
	                var paramValue = angular.isFunction(defaults[paramName]) ? defaults[paramName]() : defaults[camelizedName];

	                if (paramName === 'redirect_uri' && !paramValue) {
	                    return;
	                }

	                if (paramName === 'state') {
	                  var stateName = defaults.name + '_state';
	                  paramValue = encodeURIComponent(storage.get(stateName));
	                }

	                if (paramName === 'scope' && Array.isArray(paramValue)) {
	                  paramValue = paramValue.join(defaults.scopeDelimiter);

	                  if (defaults.scopePrefix) {
	                    paramValue = [defaults.scopePrefix, paramValue].join(defaults.scopeDelimiter);
	                  }
	                }

	                keyValuePairs.push([paramName, paramValue]);
	              });
	            });

	            return keyValuePairs.map(function(pair) {
	              return pair.join('=');
	            }).join('&');
	          };

	          return Oauth2;
	        };
	      }])
	    .factory('SatellizerOauth1', [
	      '$q',
	      '$http',
	      'SatellizerPopup',
	      'SatellizerConfig',
	      'SatellizerUtils',
	      function($q, $http, popup, config, utils) {
	        return function() {
	          var Oauth1 = {};

	          var defaults = {
	            url: null,
	            name: null,
	            popupOptions: null,
	            redirectUri: null,
	            authorizationEndpoint: null
	          };

	          Oauth1.open = function(options, userData) {
	            angular.extend(defaults, options);
	            var popupWindow;
	            var serverUrl = config.baseUrl ? utils.joinUrl(config.baseUrl, defaults.url) : defaults.url;

	            if (!window.cordova) {
	                popupWindow = popup.open('', defaults.name, defaults.popupOptions, defaults.redirectUri);
	            }

	            return $http.post(serverUrl, defaults)
	              .then(function(response) {
	                var url = [defaults.authorizationEndpoint, Oauth1.buildQueryString(response.data)].join('?');

	                if (window.cordova) {
	                  popupWindow = popup.open(url, defaults.name, defaults.popupOptions, defaults.redirectUri);
	                } else {
	                  popupWindow.popupWindow.location = url;
	                }

	                var popupListener;

	                if (window.cordova) {
	                  popupListener = popupWindow.eventListener(defaults.redirectUri);
	                } else {
	                  popupListener = popupWindow.pollPopup(defaults.redirectUri);
	                }

	                return popupListener
	                  .then(function(response) {
	                    return Oauth1.exchangeForToken(response, userData);
	                  });
	              });

	          };

	          Oauth1.exchangeForToken = function(oauthData, userData) {
	            var data = angular.extend({}, userData, oauthData);
	            var exchangeForTokenUrl = config.baseUrl ? utils.joinUrl(config.baseUrl, defaults.url) : defaults.url;
	            return $http.post(exchangeForTokenUrl, data, { withCredentials: config.withCredentials });
	          };

	          Oauth1.buildQueryString = function(obj) {
	            var str = [];

	            angular.forEach(obj, function(value, key) {
	              str.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
	            });

	            return str.join('&');
	          };

	          return Oauth1;
	        };
	      }])
	    .factory('SatellizerPopup', [
	      '$q',
	      '$interval',
	      '$window',
	      'SatellizerConfig',
	      'SatellizerUtils',
	      function($q, $interval, $window, config, utils) {
	        var Popup = {};

	        Popup.url = '';
	        Popup.popupWindow = null;

	        Popup.open = function(url, name, options) {
	          Popup.url = url;

	          var stringifiedOptions = Popup.stringifyOptions(Popup.prepareOptions(options));
	          var UA = $window.navigator.userAgent;
	          var windowName = (window.cordova || UA.indexOf('CriOS') > -1) ? '_blank' : name;

	          Popup.popupWindow = $window.open(url, windowName, stringifiedOptions);

	          $window.popup = Popup.popupWindow;

	          if (Popup.popupWindow && Popup.popupWindow.focus) {
	            Popup.popupWindow.focus();
	          }

	          return Popup;
	        };

	        Popup.eventListener = function(redirectUri) {
	          var deferred = $q.defer();

	          Popup.popupWindow.addEventListener('loadstart', function(event) {
	            if (event.url.indexOf(redirectUri) !== 0) {
	              return;
	            }

	            var parser = document.createElement('a');
	            parser.href = event.url;

	            if (parser.search || parser.hash) {
	              var queryParams = parser.search.substring(1).replace(/\/$/, '');
	              var hashParams = parser.hash.substring(1).replace(/\/$/, '');
	              var hash = utils.parseQueryString(hashParams);
	              var qs = utils.parseQueryString(queryParams);

	              angular.extend(qs, hash);

	              if (!qs.error) {
	                deferred.resolve(qs);
	              }

	              Popup.popupWindow.close();
	            }
	          });

	          Popup.popupWindow.addEventListener('loaderror', function() {
	            deferred.reject('Authorization Failed');
	          });

	          return deferred.promise;
	        };

	        Popup.pollPopup = function(redirectUri) {
	          var deferred = $q.defer();

	          var redirectUriParser = document.createElement('a');
	          redirectUriParser.href = redirectUri;

	          var redirectUriPath = utils.getFullUrlPath(redirectUriParser);

	          var polling = $interval(function() {
	            if (!Popup.popupWindow || Popup.popupWindow.closed || Popup.popupWindow.closed === undefined) {
	              deferred.reject('The popup window was closed.');
	              $interval.cancel(polling);
	            }

	            try {
	              var popupWindowPath = utils.getFullUrlPath(Popup.popupWindow.location);

	              // Redirect has occurred.
	              if (popupWindowPath === redirectUriPath) {
	                // Contains query/hash parameters as expected.
	                if (Popup.popupWindow.location.search || Popup.popupWindow.location.hash) {
	                  var queryParams = Popup.popupWindow.location.search.substring(1).replace(/\/$/, '');
	                  var hashParams = Popup.popupWindow.location.hash.substring(1).replace(/[\/$]/, '');
	                  var hash = utils.parseQueryString(hashParams);
	                  var qs = utils.parseQueryString(queryParams);

	                  angular.extend(qs, hash);

	                  if (qs.error) {
	                    deferred.reject(qs);
	                  } else {
	                    deferred.resolve(qs);
	                  }
	                } else {
	                  // Does not contain query/hash parameters, can't do anything at this point.
	                  deferred.reject(
	                    'Redirect has occurred but no query or hash parameters were found. ' +
	                    'They were either not set during the redirect, or were removed before Satellizer ' +
	                    'could read them, e.g. AngularJS routing mechanism.'
	                  );
	                }

	                $interval.cancel(polling);
	                Popup.popupWindow.close();
	              }
	            } catch (error) {
	              // Ignore DOMException: Blocked a frame with origin from accessing a cross-origin frame.
	              // A hack to get around same-origin security policy errors in IE.
	            }
	          }, 20);

	          return deferred.promise;
	        };

	        Popup.prepareOptions = function(options) {
	          options = options || {};
	          var width = options.width || 500;
	          var height = options.height || 500;

	          return angular.extend({
	            width: width,
	            height: height,
	            left: $window.screenX + (($window.outerWidth - width) / 2),
	            top: $window.screenY + (($window.outerHeight - height) / 2.5)
	          }, options);
	        };

	        Popup.stringifyOptions = function(options) {
	          var parts = [];
	          angular.forEach(options, function(value, key) {
	            parts.push(key + '=' + value);
	          });
	          return parts.join(',');
	        };

	        return Popup;
	      }])
	    .service('SatellizerUtils', function() {
	      this.getFullUrlPath = function(location) {
	        var isHttps = location.protocol === 'https:';
	        return location.protocol + '//' + location.hostname +
	          ':' + (location.port || (isHttps ? '443' : '80')) +
	          (/^\//.test(location.pathname) ? location.pathname : '/' + location.pathname);
	      };

	      this.camelCase = function(name) {
	        return name.replace(/([\:\-\_]+(.))/g, function(_, separator, letter, offset) {
	          return offset ? letter.toUpperCase() : letter;
	        });
	      };

	      this.parseQueryString = function(keyValue) {
	        var obj = {}, key, value;
	        angular.forEach((keyValue || '').split('&'), function(keyValue) {
	          if (keyValue) {
	            value = keyValue.split('=');
	            key = decodeURIComponent(value[0]);
	            obj[key] = angular.isDefined(value[1]) ? decodeURIComponent(value[1]) : true;
	          }
	        });
	        return obj;
	      };

	      this.joinUrl = function(baseUrl, url) {
	        if (/^(?:[a-z]+:)?\/\//i.test(url)) {
	          return url;
	        }

	        var joined = [baseUrl, url].join('/');

	        var normalize = function(str) {
	          return str
	            .replace(/[\/]+/g, '/')
	            .replace(/\/\?/g, '?')
	            .replace(/\/\#/g, '#')
	            .replace(/\:\//g, '://');
	        };

	        return normalize(joined);
	      };

	      this.merge = function(obj1, obj2) {
	        var result = {};
	        for (var i in obj1) {
	          if (obj1.hasOwnProperty(i)) {
	            if ((i in obj2) && (typeof obj1[i] === 'object') && (i !== null)) {
	              result[i] = this.merge(obj1[i], obj2[i]);
	            } else {
	              result[i] = obj1[i];
	            }
	          }
	        }
	        for (i in obj2) {
	          if (obj2.hasOwnProperty(i)) {
	            if (i in result) {
	              continue;
	            }
	            result[i] = obj2[i];
	          }

	        }
	        return result;
	      };
	    })
	    .factory('SatellizerStorage', ['$window', '$log', 'SatellizerConfig', function($window, $log, config) {

	      var store = {};

	      // Check if localStorage or sessionStorage is available or enabled
	      var isStorageAvailable = (function() {
	        try {
	          var supported = config.storageType in $window && $window[config.storageType] !== null;

	          if (supported) {
	            var key = Math.random().toString(36).substring(7);
	            $window[config.storageType].setItem(key, '');
	            $window[config.storageType].removeItem(key);
	          }

	          return supported;
	        } catch (e) {
	          return false;
	        }
	      })();

	      if (!isStorageAvailable) {
	        $log.warn(config.storageType + ' is not available.');
	      }

	      return {
	        get: function(key) {
	          return isStorageAvailable ? $window[config.storageType].getItem(key) : store[key];
	        },
	        set: function(key, value) {
	          return isStorageAvailable ? $window[config.storageType].setItem(key, value) : store[key] = value;
	        },
	        remove: function(key) {
	          return isStorageAvailable ? $window[config.storageType].removeItem(key): delete store[key];
	        }
	      };

	    }])
	    .factory('SatellizerInterceptor', [
	      '$q',
	      'SatellizerConfig',
	      'SatellizerStorage',
	      'SatellizerShared',
	      function($q, config, storage, shared) {
	        return {
	          request: function(request) {
	            if (request.skipAuthorization) {
	              return request;
	            }

	            if (shared.isAuthenticated() && config.httpInterceptor(request)) {
	              var tokenName = config.tokenPrefix ? config.tokenPrefix + '_' + config.tokenName : config.tokenName;
	              var token = storage.get(tokenName);

	              if (config.authHeader && config.authToken) {
	                token = config.authToken + ' ' + token;
	              }

	              request.headers[config.authHeader] = token;
	            }

	            return request;
	          },
	          responseError: function(response) {
	            return $q.reject(response);
	          }
	        };
	      }])
	    .config(['$httpProvider', function($httpProvider) {
	      $httpProvider.interceptors.push('SatellizerInterceptor');
	    }]);

	})(window, window.angular);


/***/ },
/* 133 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(134);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_login.scss", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_login.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 134 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.login-modal-component {\n  position: fixed;\n  top: 0px;\n  left: 0px;\n  height: 100vh;\n  width: 100vw;\n  background-color: rgba(0, 0, 0, 0.8);\n  z-index: 10000;\n  font-family: aller;\n  font-size: 20px;\n  height: 0px;\n  overflow: auto;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: center;\n  -moz-box-pack: center;\n  box-pack: center;\n  -webkit-justify-content: center;\n  -moz-justify-content: center;\n  -ms-justify-content: center;\n  -o-justify-content: center;\n  justify-content: center;\n  -ms-flex-pack: center;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row;\n  -webkit-transition: all 0.3s ease-in-out;\n  -moz-transition: all 0.3s ease-in-out;\n  transition: all 0.3s ease-in-out; }\n  .login-modal-component .close-btn-login {\n    font-size: 20px;\n    position: absolute;\n    left: 10px;\n    top: 10px;\n    background: white;\n    padding: 5px 15px;\n    color: #222;\n    border-radius: 38px;\n    cursor: pointer; }\n  .login-modal-component .login-form-wrapper {\n    width: 100%;\n    background: white;\n    padding: 20px;\n    border-radius: 3px;\n    max-width: 800px;\n    border: 1px solid #e5e5e5;\n    box-shadow: 1px 1px 1px #ccc;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: vertical;\n    -moz-box-orient: vertical;\n    box-orient: vertical;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: column;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    -ms-flex-direction: column; }\n    @media (min-width: 1000px) {\n      .login-modal-component .login-form-wrapper {\n        min-width: 650px;\n        width: auto;\n        padding-right: 0px;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; } }\n  .login-modal-component .form {\n    -webkit-box-flex: 1;\n    -moz-box-flex: 1;\n    box-flex: 1;\n    -webkit-flex: 1;\n    -moz-flex: 1;\n    -ms-flex: 1;\n    flex: 1;\n    -webkit-align-self: stretch;\n    -moz-align-self: stretch;\n    align-self: stretch;\n    -ms-flex-item-align: stretch; }\n  .login-modal-component #error {\n    max-height: 100px;\n    overflow: auto; }\n  .login-modal-component label {\n    color: #222;\n    font-family: 'nunito',sans-serif; }\n  .login-modal-component input {\n    height: 50px;\n    padding: 10px !important;\n    border-radius: 5px !important;\n    color: #222 !important;\n    font-weight: bold;\n    font-family: 'nunito',sans-serif;\n    box-shadow: 0px 0px 0px !important;\n    border: 2px solid #e5e5e5 !important; }\n    .login-modal-component input:focus {\n      border: 2px solid #1d99d1 !important; }\n  .login-modal-component .login-btn {\n    width: 100%;\n    color: #fff;\n    text-align: center;\n    display: inline-block;\n    background: #1d99d1;\n    padding: 10px;\n    border-radius: 3px;\n    cursor: pointer;\n    border: 0px; }\n  .login-modal-component .social-media {\n    padding: 20px 0px;\n    color: #fff;\n    margin-left: 20px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    @media (min-width: 1000px) {\n      .login-modal-component .social-media {\n        border-left: 1px solid rgba(0, 0, 0, 0.2);\n        -webkit-box-orient: vertical;\n        -moz-box-orient: vertical;\n        box-orient: vertical;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: column;\n        -moz-flex-direction: column;\n        flex-direction: column;\n        -ms-flex-direction: column; } }\n  .login-modal-component .social-icon-wrapper {\n    margin: 5px 10px;\n    cursor: pointer;\n    border-radius: 3px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-flex: 1;\n    -moz-box-flex: 1;\n    box-flex: 1;\n    -webkit-flex: 1;\n    -moz-flex: 1;\n    -ms-flex: 1;\n    flex: 1; }\n    @media (min-width: 1000px) {\n      .login-modal-component .social-icon-wrapper {\n        margin: 5px 30px; } }\n    .login-modal-component .social-icon-wrapper i {\n      padding: 20px;\n      width: 53px;\n      text-align: center;\n      border-radius: 3px; }\n    .login-modal-component .social-icon-wrapper > span {\n      padding: 8px;\n      display: inline-block; }\n    .login-modal-component .social-icon-wrapper.facebook {\n      background: #44619D; }\n    .login-modal-component .social-icon-wrapper.google {\n      background: #EA4335; }\n    .login-modal-component .social-icon-wrapper.twitter {\n      background: #28A9E0; }\n    .login-modal-component .social-icon-wrapper.linkedin {\n      background: #006DA6; }\n", ""]);

	// exports


/***/ },
/* 135 */
/***/ function(module, exports) {

	module.exports = "<div class=\"login-modal-component\" id=\"login-modal\">\n\t<div class=\"login-form-wrapper\">\n\t\t<div class=\"form\">\n\t\t\t<div id=\"error\" ng-if=\"vm.formError\" class=\"alert alert-danger\">{{vm.formError}}</div>\n\t\t\t<form name=\"form\" ng-submit=\"vm.doLogin()\">\n\t\t\t\t<div class=\"form-group input\">\n\t\t\t\t\t<label for=\"email\"><i class=\"fa fa-user\"></i> Username</label>\n\t\t\t\t\t<input type=\"text\" ng-model=\"vm.userId\" id=\"email\" class=\"form-control\" autofocus=\"\" name=\"email\" required=\"\" />\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group input\">\n\t\t\t\t\t<label for=\"password\"><i class=\"fa fa-key\"></i> Password</label>\n\t\t\t\t\t<input type=\"password\" ng-model=\"vm.password\" id=\"password\" class=\"form-control\" name=\"password\"required=\"\" />\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<button class=\"login-btn\" type=\"submit\">\n\t\t\t\t\t\t<span ng-if=\"!vm.isFormSubmitting\">\n\t\t\t\t\t\t\t<i class=\"fa fa-lock\"></i>\n\t\t\t\t\t\t\tLogin\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span ng-if=\"vm.isFormSubmitting\">\n\t\t\t\t\t\t\t<i class=\"fa fa-circle-o-notch fa-spin\"></i>\n\t\t\t\t\t\t\tLogging in...\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\t\t<div class=\"social-media\">\n\t\t\t<div class=\"social-icon-wrapper facebook\" ng-click=\"vm.authenticateWith('facebook')\">\n\t\t\t\t<i class=\"fa fa-facebook\"></i>\n\t\t\t</div>\n\t\t\t<div class=\"social-icon-wrapper google\" ng-click=\"vm.authenticateWith('google')\">\n\t\t\t\t<i class=\"fa fa-google\"></i>\n\t\t\t</div>\n\t\t\t<div class=\"social-icon-wrapper twitter\" ng-click=\"vm.authenticateWith('twitter')\">\n\t\t\t\t<i class=\"fa fa-twitter\"></i>\n\t\t\t</div>\n\t\t\t<div class=\"social-icon-wrapper linkedin\" ng-click=\"vm.authenticateWith('linkedin')\">\n\t\t\t\t<i class=\"fa fa-linkedin\"></i>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"close-btn-login\"  ng-click=\"vm.toggleLoginModal()\"><i class=\"fa fa-arrow-left\"></i></div>\n</div>";

/***/ },
/* 136 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(137);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_menu.scss", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_menu.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 137 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.menu-component-wrapper .big-screen {\n  display: none; }\n  @media (min-width: 1280px) {\n    .menu-component-wrapper .big-screen {\n      display: block; } }\n\n@media (min-width: 1280px) {\n  .menu-component-wrapper .small-screen {\n    display: none; } }\n\n.menu-component-wrapper .left-side-btns {\n  right: 943px !important;\n  border-bottom-right-radius: 5px; }\n  @media (min-width: 105em) {\n    .menu-component-wrapper .left-side-btns {\n      right: 1169px !important; } }\n\n.menu-component-wrapper .header-btns {\n  right: 5px;\n  top: 5px;\n  position: fixed;\n  padding: 6px;\n  z-index: 9999;\n  display: none;\n  border-radius: 5px; }\n  @media (min-width: 1280px) {\n    .menu-component-wrapper .header-btns {\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: end;\n      -moz-box-pack: end;\n      box-pack: end;\n      -webkit-justify-content: flex-end;\n      -moz-justify-content: flex-end;\n      -ms-justify-content: flex-end;\n      -o-justify-content: flex-end;\n      justify-content: flex-end;\n      -ms-flex-pack: end;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; } }\n  .menu-component-wrapper .header-btns .usr-small-profile-pic {\n    width: 30px;\n    border-radius: 50%;\n    margin-right: 10px; }\n  .menu-component-wrapper .header-btns .menu-head-btn:after {\n    content: '|';\n    margin: 0px 8px;\n    color: #fff; }\n  .menu-component-wrapper .header-btns .btn-wrap {\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: end;\n    -moz-box-pack: end;\n    box-pack: end;\n    -webkit-justify-content: flex-end;\n    -moz-justify-content: flex-end;\n    -ms-justify-content: flex-end;\n    -o-justify-content: flex-end;\n    justify-content: flex-end;\n    -ms-flex-pack: end;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n  .menu-component-wrapper .header-btns .usr_nm {\n    color: white;\n    border-radius: 5px;\n    font-size: 12px;\n    font-weight: bold;\n    cursor: pointer; }\n  .menu-component-wrapper .header-btns .login-btn, .menu-component-wrapper .header-btns .register-btn {\n    color: #fff;\n    font-size: 12px;\n    cursor: pointer;\n    text-decoration: none;\n    font-weight: bold;\n    font-family: \"Helvetica Neue\",Helvetica,Arial,sans-serif; }\n    .menu-component-wrapper .header-btns .login-btn:last-child, .menu-component-wrapper .header-btns .register-btn:last-child {\n      border: 0px; }\n    .menu-component-wrapper .header-btns .login-btn i, .menu-component-wrapper .header-btns .register-btn i {\n      color: #fff;\n      margin: 0px 3px; }\n\n.menu-component-wrapper .mobile-header-btns {\n  right: 0px;\n  top: 0px;\n  position: fixed;\n  padding: 6px;\n  z-index: 9999;\n  height: 50px;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: end;\n  -moz-box-pack: end;\n  box-pack: end;\n  -webkit-justify-content: flex-end;\n  -moz-justify-content: flex-end;\n  -ms-justify-content: flex-end;\n  -o-justify-content: flex-end;\n  justify-content: flex-end;\n  -ms-flex-pack: end;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n  @media (min-width: 1280px) {\n    .menu-component-wrapper .mobile-header-btns {\n      display: none; } }\n  .menu-component-wrapper .mobile-header-btns .btn-wrap {\n    background: #3e6289;\n    position: fixed;\n    top: 60px;\n    right: 0px;\n    bottom: 0px;\n    width: 0px;\n    border-top-left-radius: 10px;\n    border-bottom-left-radius: 10px;\n    box-shadow: 0px 0px 10px 0px #000;\n    overflow-y: auto;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: start;\n    -moz-box-pack: start;\n    box-pack: start;\n    -webkit-justify-content: flex-start;\n    -moz-justify-content: flex-start;\n    -ms-justify-content: flex-start;\n    -o-justify-content: flex-start;\n    justify-content: flex-start;\n    -ms-flex-pack: start;\n    -webkit-box-orient: vertical;\n    -moz-box-orient: vertical;\n    box-orient: vertical;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: column;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    -ms-flex-direction: column;\n    -webkit-transition: all 0.2s ease-in;\n    -moz-transition: all 0.2s ease-in;\n    transition: all 0.2s ease-in; }\n    .menu-component-wrapper .mobile-header-btns .btn-wrap.ew-open {\n      width: 320px; }\n  .menu-component-wrapper .mobile-header-btns .login-btn, .menu-component-wrapper .mobile-header-btns .register-btn {\n    color: #fff;\n    font-size: 20px;\n    cursor: pointer;\n    text-decoration: none;\n    font-weight: bold;\n    font-family: \"Roboto\",\"Nunito\",Helvetica,Arial,sans-serif;\n    border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n    padding: 25px;\n    font: 400 20px/1.5em \"Roboto\",sans-serif;\n    font-weight: bold;\n    text-transform: uppercase;\n    white-space: nowrap; }\n    .menu-component-wrapper .mobile-header-btns .login-btn:last-child, .menu-component-wrapper .mobile-header-btns .register-btn:last-child {\n      border: 0px; }\n    .menu-component-wrapper .mobile-header-btns .login-btn i, .menu-component-wrapper .mobile-header-btns .register-btn i {\n      color: #fff;\n      margin-right: 5px; }\n  .menu-component-wrapper .mobile-header-btns .modal-close-btn {\n    color: #222;\n    padding: 10px;\n    font-size: 15px;\n    cursor: pointer;\n    text-decoration: none;\n    height: 50px;\n    font-family: \"Roboto\",\"Nunito\",Helvetica,Arial,sans-serif;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .menu-component-wrapper .mobile-header-btns .modal-close-btn i {\n      margin-right: 5px; }\n\n.menu-component-wrapper .notification-btn {\n  position: relative; }\n\n.menu-component-wrapper .notification-cnt {\n  border-radius: 50%;\n  padding: 0px 5px;\n  background: white;\n  color: #444;\n  position: absolute;\n  top: -6px;\n  right: 14px;\n  font-size: 10px; }\n\n.menu-component-wrapper .notification-modal {\n  position: absolute;\n  font-family: 'nunito',sans-serif;\n  background: white;\n  border-radius: 5px;\n  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);\n  width: 400px;\n  left: -150px;\n  top: 35px; }\n  .menu-component-wrapper .notification-modal:before {\n    content: '';\n    border-bottom: 10px solid #fff;\n    border-left: 10px solid transparent;\n    border-right: 10px solid transparent;\n    position: absolute;\n    top: -10px;\n    left: 200px; }\n  .menu-component-wrapper .notification-modal.dark-blue {\n    background: #3e6289;\n    color: white; }\n    .menu-component-wrapper .notification-modal.dark-blue:before {\n      content: '';\n      border-bottom: 10px solid #3e6289; }\n    .menu-component-wrapper .notification-modal.dark-blue .ew-modal-footer .see-all {\n      color: #fff; }\n  .menu-component-wrapper .notification-modal a {\n    text-decoration: none; }\n  .menu-component-wrapper .notification-modal .ew-modal-title {\n    font-size: 15px;\n    padding: 10px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: justify;\n    -moz-box-pack: justify;\n    box-pack: justify;\n    -webkit-justify-content: space-between;\n    -moz-justify-content: space-between;\n    -ms-justify-content: space-between;\n    -o-justify-content: space-between;\n    justify-content: space-between;\n    -ms-flex-pack: justify;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .menu-component-wrapper .notification-modal .ew-modal-title .text img {\n      width: 30px;\n      margin-right: 10px; }\n    .menu-component-wrapper .notification-modal .ew-modal-title i {\n      padding: 0px 5px;\n      cursor: pointer; }\n  .menu-component-wrapper .notification-modal .ew-modal-subheader .ew-tabs {\n    border-top: 1px solid rgba(0, 0, 0, 0.3);\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .menu-component-wrapper .notification-modal .ew-modal-subheader .ew-tabs .ew-tab {\n      font-family: 'nunito',sans-serif;\n      text-align: center;\n      padding: 5px;\n      cursor: pointer;\n      -webkit-box-flex: 1;\n      -moz-box-flex: 1;\n      box-flex: 1;\n      -webkit-flex: 1;\n      -moz-flex: 1;\n      -ms-flex: 1;\n      flex: 1; }\n      .menu-component-wrapper .notification-modal .ew-modal-subheader .ew-tabs .ew-tab.active {\n        background-color: #f4f4f4;\n        color: #222; }\n  .menu-component-wrapper .notification-modal .ew-modal-content {\n    max-height: 300px;\n    overflow: auto;\n    overflow-x: hidden;\n    background: #f4f4f4;\n    min-height: 300px; }\n    .menu-component-wrapper .notification-modal .ew-modal-content.white {\n      background: white;\n      color: #222; }\n  .menu-component-wrapper .notification-modal .no-notification {\n    text-align: center;\n    font-size: 19px;\n    padding: 10px; }\n  .menu-component-wrapper .notification-modal .notification {\n    padding: 10px;\n    border-bottom: 1px solid #e5e5e5;\n    position: relative;\n    text-decoration: none;\n    display: block; }\n    .menu-component-wrapper .notification-modal .notification.unread {\n      background: #e8f4fa; }\n    .menu-component-wrapper .notification-modal .notification:hover .mark-as-read-icon {\n      display: block; }\n    .menu-component-wrapper .notification-modal .notification.smenu-links {\n      color: #fff;\n      cursor: initial;\n      text-decoration: none;\n      border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n      padding: 25px;\n      font: 400 20px/1.5em \"Roboto\",sans-serif;\n      text-transform: uppercase;\n      white-space: nowrap;\n      background: #3e6289; }\n      .menu-component-wrapper .notification-modal .notification.smenu-links a {\n        color: white;\n        cursor: pointer; }\n        .menu-component-wrapper .notification-modal .notification.smenu-links a:hover {\n          color: white; }\n      .menu-component-wrapper .notification-modal .notification.smenu-links .notification-sub {\n        font-size: inherit;\n        color: white;\n        cursor: initial; }\n      .menu-component-wrapper .notification-modal .notification.smenu-links .notification-msg {\n        color: #fff; }\n    .menu-component-wrapper .notification-modal .notification .notification-sub {\n      font-size: 18px;\n      color: #3db0e4;\n      display: block;\n      cursor: pointer; }\n      .menu-component-wrapper .notification-modal .notification .notification-sub i {\n        margin-right: 10px;\n        min-width: 20px; }\n    .menu-component-wrapper .notification-modal .notification .user-pic img {\n      width: 50px;\n      border-radius: 5px;\n      margin-right: 10px; }\n    .menu-component-wrapper .notification-modal .notification .notification-msg {\n      font-size: 14px;\n      color: #222; }\n    .menu-component-wrapper .notification-modal .notification .notification-time {\n      font-size: 10px;\n      color: #777;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row;\n      -webkit-box-pack: justify;\n      -moz-box-pack: justify;\n      box-pack: justify;\n      -webkit-justify-content: space-between;\n      -moz-justify-content: space-between;\n      -ms-justify-content: space-between;\n      -o-justify-content: space-between;\n      justify-content: space-between;\n      -ms-flex-pack: justify; }\n    .menu-component-wrapper .notification-modal .notification .mark-as-read-icon {\n      margin-left: 10px;\n      cursor: pointer; }\n      .menu-component-wrapper .notification-modal .notification .mark-as-read-icon:hover {\n        color: #1d99d1; }\n  .menu-component-wrapper .notification-modal .ew-modal-footer {\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .menu-component-wrapper .notification-modal .ew-modal-footer .see-all {\n      color: #1d99d1;\n      text-align: center;\n      display: block;\n      padding: 10px;\n      font-size: 16px;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: center;\n      -moz-box-pack: center;\n      box-pack: center;\n      -webkit-justify-content: center;\n      -moz-justify-content: center;\n      -ms-justify-content: center;\n      -o-justify-content: center;\n      justify-content: center;\n      -ms-flex-pack: center;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; }\n      .menu-component-wrapper .notification-modal .ew-modal-footer .see-all img {\n        width: 20px;\n        margin-right: 10px; }\n\n.menu-component-wrapper .notification-modal.contactus-modal {\n  left: -200px;\n  width: 260px; }\n  .menu-component-wrapper .notification-modal.contactus-modal:before {\n    left: 200px; }\n  .menu-component-wrapper .notification-modal.contactus-modal .ew-modal-content {\n    max-height: 600px;\n    min-height: auto; }\n  .menu-component-wrapper .notification-modal.contactus-modal .smenu-links {\n    padding: 10px 20px;\n    font-size: 18px; }\n\n.menu-component-wrapper .menu-modal-component {\n  margin: 0px auto;\n  position: fixed;\n  z-index: 100000000000;\n  top: 80px;\n  right: 0px;\n  width: 0px;\n  bottom: 0px;\n  background: white;\n  color: #fff;\n  overflow-y: auto;\n  overflow-x: hidden;\n  white-space: nowrap;\n  border-top-left-radius: 5px;\n  border-bottom-left-radius: 5px;\n  border-right: 0px;\n  background: #81a2c6;\n  overflow-y: scroll; }\n  .menu-component-wrapper .menu-modal-component.ew-open {\n    width: 90%; }\n  @media (min-width: 1280px) {\n    .menu-component-wrapper .menu-modal-component {\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: start;\n      -moz-box-align: start;\n      box-align: start;\n      -webkit-align-items: flex-start;\n      -moz-align-items: flex-start;\n      -ms-align-items: flex-start;\n      -o-align-items: flex-start;\n      align-items: flex-start;\n      -ms-flex-align: start;\n      -webkit-box-pack: distribute;\n      -moz-box-pack: distribute;\n      box-pack: distribute;\n      -webkit-justify-content: space-around;\n      -moz-justify-content: space-around;\n      -ms-justify-content: space-around;\n      -o-justify-content: space-around;\n      justify-content: space-around;\n      -ms-flex-pack: distribute;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row;\n      -webkit-transition: 0.3s all ease-in;\n      -moz-transition: 0.3s all ease-in;\n      transition: 0.3s all ease-in; } }\n  .menu-component-wrapper .menu-modal-component .modal-close-btn {\n    position: absolute;\n    top: 10px;\n    right: 20px;\n    font-size: 20px;\n    cursor: pointer; }\n  .menu-component-wrapper .menu-modal-component .menu-head {\n    padding: 17px;\n    background: #3e6289;\n    font-size: 20px;\n    text-align: center;\n    border-bottom: 1px solid #ccc;\n    color: white;\n    position: relative; }\n  .menu-component-wrapper .menu-modal-component .close-icon {\n    position: absolute;\n    right: 10px;\n    top: 17px;\n    cursor: pointer; }\n  .menu-component-wrapper .menu-modal-component .menu-foot {\n    background: #1e3043;\n    cursor: pointer; }\n    .menu-component-wrapper .menu-modal-component .menu-foot i {\n      margin-right: 5px; }\n  .menu-component-wrapper .menu-modal-component .menu-part {\n    width: 50%;\n    height: 100%;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -moz-box-orient: vertical;\n    box-orient: vertical;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: column;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    -ms-flex-direction: column; }\n    .menu-component-wrapper .menu-modal-component .menu-part.left {\n      border-right: 1px solid #ccc; }\n  .menu-component-wrapper .menu-modal-component ul {\n    list-style: none;\n    font-size: 15px;\n    text-align: left;\n    padding: 0px;\n    overflow: auto;\n    height: 380px;\n    -webkit-box-flex: 1;\n    -moz-box-flex: 1;\n    box-flex: 1;\n    -webkit-flex: 1;\n    -moz-flex: 1;\n    -ms-flex: 1;\n    flex: 1; }\n  .menu-component-wrapper .menu-modal-component .left li {\n    padding: 10px;\n    margin: 0px; }\n  .menu-component-wrapper .menu-modal-component li {\n    margin: 10px 0px;\n    cursor: pointer;\n    text-transform: uppercase; }\n    .menu-component-wrapper .menu-modal-component li a {\n      text-decoration: none;\n      color: #fff;\n      display: block; }\n      .menu-component-wrapper .menu-modal-component li a.white {\n        color: #fff; }\n    .menu-component-wrapper .menu-modal-component li i {\n      margin-right: 15px; }\n    .menu-component-wrapper .menu-modal-component li .li-title {\n      margin: 10px; }\n    .menu-component-wrapper .menu-modal-component li .hidden-menu {\n      padding: 10px;\n      margin: 10px 0px;\n      background: #3e6289;\n      color: white; }\n      .menu-component-wrapper .menu-modal-component li .hidden-menu .item {\n        text-overflow: ellipsis;\n        overflow: hidden;\n        max-width: 240px;\n        font-size: 12px;\n        margin: 5px 0px;\n        color: white; }\n        @media (min-width: 105em) {\n          .menu-component-wrapper .menu-modal-component li .hidden-menu .item {\n            max-width: 300px; } }\n\n.menu-component-wrapper .menu-small-screen {\n  position: fixed;\n  bottom: 0px;\n  left: 0px;\n  right: 0px;\n  z-index: 9999; }\n  .menu-component-wrapper .menu-small-screen .ew-tabs {\n    width: 100%;\n    box-shadow: -1px -1px 2px #444;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .menu-component-wrapper .menu-small-screen .ew-tabs .ew-tab {\n      padding: 10px 10px;\n      background-color: white;\n      color: #222;\n      border-right: 1px solid #ccc;\n      text-align: center;\n      cursor: pointer;\n      white-space: nowrap;\n      font-size: 13px;\n      -webkit-box-flex: 1;\n      -moz-box-flex: 1;\n      box-flex: 1;\n      -webkit-flex: 1;\n      -moz-flex: 1;\n      -ms-flex: 1;\n      flex: 1; }\n      .menu-component-wrapper .menu-small-screen .ew-tabs .ew-tab.active {\n        box-shadow: inset 0px 0px 10px #ccc; }\n  .menu-component-wrapper .menu-small-screen .ew-tab-content-wrapper {\n    overflow: hidden;\n    position: fixed;\n    left: 0px;\n    right: 0px;\n    bottom: 43px;\n    height: 0px;\n    background-color: #e5e5e5;\n    -webkit-transition: all 0.2s ease-in;\n    -moz-transition: all 0.2s ease-in;\n    transition: all 0.2s ease-in; }\n    .menu-component-wrapper .menu-small-screen .ew-tab-content-wrapper.ew-open {\n      height: calc(100vh - 43px); }\n    .menu-component-wrapper .menu-small-screen .ew-tab-content-wrapper .close-btn {\n      position: absolute;\n      right: 20px;\n      top: 10px;\n      font-size: 20px;\n      padding: 10px;\n      color: #222;\n      z-index: 1; }\n  .menu-component-wrapper .menu-small-screen .ew-tab-content {\n    position: absolute;\n    top: 0px;\n    right: 0px;\n    left: 0px;\n    bottom: 0px;\n    overflow: auto;\n    padding: 5px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: vertical;\n    -moz-box-orient: vertical;\n    box-orient: vertical;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: column;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    -ms-flex-direction: column; }\n    .menu-component-wrapper .menu-small-screen .ew-tab-content ul {\n      list-style: none;\n      padding: 0px;\n      margin: 0px;\n      font-size: 20px;\n      width: 100%;\n      font-family: 'Roboto Condensed','Aller'; }\n      .menu-component-wrapper .menu-small-screen .ew-tab-content ul li {\n        padding: 0px 20px;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center; }\n        .menu-component-wrapper .menu-small-screen .ew-tab-content ul li a {\n          white-space: nowrap;\n          text-overflow: ellipsis;\n          overflow: hidden;\n          width: 100%;\n          display: inline-block;\n          color: #222; }\n      .menu-component-wrapper .menu-small-screen .ew-tab-content ul i {\n        margin-right: 10px;\n        width: 30px;\n        display: inline-block; }\n\n.menu-component-wrapper .all-links-modal-wrapper {\n  position: fixed;\n  overflow: auto;\n  left: 0px;\n  right: 0px;\n  top: 0px;\n  bottom: 0px;\n  z-index: 10000;\n  height: 0px;\n  background: rgba(0, 0, 0, 0.2);\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: center;\n  -moz-box-pack: center;\n  box-pack: center;\n  -webkit-justify-content: center;\n  -moz-justify-content: center;\n  -ms-justify-content: center;\n  -o-justify-content: center;\n  justify-content: center;\n  -ms-flex-pack: center;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row;\n  -webkit-transition: all 0.2s ease-in;\n  -moz-transition: all 0.2s ease-in;\n  transition: all 0.2s ease-in; }\n  .menu-component-wrapper .all-links-modal-wrapper.ds-open {\n    height: 100vh; }\n  .menu-component-wrapper .all-links-modal-wrapper .all-links-modal-content {\n    background: white;\n    max-width: 960px;\n    padding: 10px;\n    border-radius: 5px;\n    position: relative;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row;\n    -webkit-box-lines: multiple;\n    -moz-box-lines: multiple;\n    box-lines: multiple;\n    -webkit-flex-wrap: wrap;\n    -moz-flex-wrap: wrap;\n    -ms-flex-wrap: wrap;\n    flex-wrap: wrap; }\n    .menu-component-wrapper .all-links-modal-wrapper .all-links-modal-content .close-btn {\n      position: absolute;\n      right: 5px;\n      top: 5px;\n      font-size: 16px;\n      border: 1px solid #ccc;\n      border-radius: 50%;\n      cursor: pointer;\n      padding: 0px 8px; }\n  .menu-component-wrapper .all-links-modal-wrapper .cat {\n    margin: 10px; }\n    .menu-component-wrapper .all-links-modal-wrapper .cat .cat-name {\n      font-family: 'roboto',sans-serif;\n      font-size: 17px;\n      color: #222;\n      border-bottom: 1px solid #e5e5e5;\n      padding: 10px; }\n    .menu-component-wrapper .all-links-modal-wrapper .cat .links {\n      padding: 10px; }\n      .menu-component-wrapper .all-links-modal-wrapper .cat .links a {\n        text-decoration: none;\n        padding: 5px 0px;\n        font-size: 15px;\n        font-family: 'roboto',sans-serif;\n        display: inline-block; }\n        .menu-component-wrapper .all-links-modal-wrapper .cat .links a:hover {\n          text-decoration: none; }\n\n.menu-component-wrapper .subscribe-modal {\n  width: 500px;\n  position: fixed;\n  left: 50%;\n  margin-left: -250px;\n  top: 50%;\n  margin-top: -100px;\n  border-radius: 5px;\n  z-index: 10000;\n  background-color: rgba(0, 0, 0, 0.8);\n  display: none;\n  padding: 15px;\n  z-index: 100000000000; }\n\n.menu-component-wrapper .subscribe-modal.open {\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -moz-box-orient: vertical;\n  box-orient: vertical;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: column;\n  -moz-flex-direction: column;\n  flex-direction: column;\n  -ms-flex-direction: column; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-close-btn {\n  position: absolute;\n  top: 5px;\n  right: 15px;\n  color: white;\n  font-size: 18px;\n  cursor: pointer; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-title {\n  text-align: center;\n  margin: 5px 0px;\n  padding: 5px;\n  font-family: 'nunito',sans-serif;\n  font-size: 16px;\n  color: white; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-input input {\n  height: 40px;\n  border: 1px solid #f4f4f4;\n  border-radius: 5px;\n  margin: 5px 0px;\n  padding: 5px;\n  font-family: 'nunito',sans-serif;\n  font-size: 16px;\n  width: 100%;\n  outline: none;\n  box-shadow: 0px 0px 0px; }\n\n.menu-component-wrapper .subscribe-modal .input-error-msg {\n  color: red;\n  font-family: 'nunito',sans-serif;\n  font-size: 12px; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-input input.error {\n  border: 1px solid #f00; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-btn i {\n  padding: 5px 10px;\n  border-right: 1px solid #177aa7;\n  line-height: 25px;\n  padding-left: 0px;\n  margin-right: 5px; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-btn {\n  background-color: #1d99d1;\n  color: white;\n  font-family: 'nunito',sans-serif;\n  font-size: 16px;\n  text-align: center;\n  margin: 5px 0px;\n  cursor: pointer;\n  display: inline-block;\n  padding: 0px 10px;\n  align-self: flex-end;\n  float: right; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-btn.ew-close {\n  background-color: #EC644B;\n  float: left; }\n\n.menu-component-wrapper .subscribe-modal .subscribe-btn.ew-close i {\n  border-right: 1px solid #8d3c2d; }\n\n.menu-component-wrapper .subscribe-modal .success-msg {\n  background: green; }\n\n.menu-component-wrapper .subscribe-modal .error-msg {\n  background: #EC644B; }\n\n.menu-component-wrapper .subscribe-modal .success-msg, .menu-component-wrapper .subscribe-modal .error-msg {\n  color: white;\n  padding: 10px;\n  border-radius: 5px;\n  text-align: center;\n  font-family: 'nunito';\n  font-size: 17px;\n  margin: 10px 0px; }\n\n.menu-component-wrapper .chat_fixed_toggle {\n  opacity: 0 !important; }\n\n.menu-component-wrapper .chat_fixed {\n  position: fixed;\n  bottom: 0;\n  right: 2%;\n  background-color: rgba(23, 120, 164, 0.7);\n  padding: 8px;\n  cursor: pointer;\n  border-radius: 5px;\n  border-bottom-right-radius: 0px;\n  border-bottom-left-radius: 0px;\n  z-index: 9900; }\n  .menu-component-wrapper .chat_fixed i {\n    color: white;\n    font-size: 25px; }\n", ""]);

	// exports


/***/ },
/* 138 */
/***/ function(module, exports) {

	angular
	.module('service')
	.service('paypal', paypal);

	paypal.$inject = ['$http','BASE_API'];

	function paypal($http,BASE_API){

		this.createPayment = function(data){
			return $http.post(BASE_API + '/payment/paypal/create',data);
		}

		this.executePayment = function(data){
			return $http.post(BASE_API + '/payment/paypal/'+ data.paymentId + '/execute',data);
		}

		// save payment details
		this.savePaymentDetails = function(data){
			return $http.post(BASE_API + '/payment/savePaymentDetails',data);
		}


		this.paymentUsingCCAvenue = function(data){
			return $http.post(BASE_API + '/payment/ccavenue/ccavRequestHandler',data);	
		}

		this.getPurchasedTrainingDetails = function(data){
			return $http.post(BASE_API + '/payment/getPurchasedTrainingDetails',data);	
		}

		this.saveUserEnrolledBatchDetails = function(data){
			return $http.post(BASE_API + '/payment/saveUserEnrolledBatchDetails',data);	
		}

		this.getUserEnrolledBatchDetails = function(data){
			return $http.post(BASE_API + '/payment/getUserEnrolledBatchDetails',data);	
		}
	}

/***/ },
/* 139 */
/***/ function(module, exports) {

	angular.module('service').service('contactus',Contactus);

	Contactus.$inject = ['$http','BASE_API'];

	function Contactus($http,BASE_API) {
	    this.getContactUsDetails = function (data) {
	        return $http.post(BASE_API + '/contact/getContactUsDetails', data);
	    }
	}

/***/ },
/* 140 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {var io = __webpack_require__(141);
	__webpack_require__(51);
	__webpack_require__(59);
	__webpack_require__(57);
	__webpack_require__(142);
	__webpack_require__(28);
	__webpack_require__(48);
	__webpack_require__(143);
	__webpack_require__(43);
	__webpack_require__(145);
	__webpack_require__(146);

	__webpack_require__(147);
	__webpack_require__(148);
	__webpack_require__(149);
	// require('service.websocket');
	__webpack_require__(151);

	var notification_sound_path = __webpack_require__(152);
	__webpack_require__(153);

	var uuid = __webpack_require__(155);

	angular.module('directive')
	    .component('chat', {
	        template: ['$element', '$attrs',
	            function($element, $attrs) {
	                return __webpack_require__(157);
	            }
	        ],
	        controller: ChatController,
	        controllerAs: 'vm',
	        bindings: {
	            showChatComponent: '=' // whether to show chat component or not
	        }
	    });

	ChatController.$inject = ['authToken','$timeout', '$scope', '$filter', 'chat', 'Upload', 'alertify','BASE_API','chatUtility'];

	function ChatController(authToken,$timeout, $scope, $filter, chat, Upload, alertify,BASE_API,chatUtility) {
	    var vm = this;
	    var userKey = "usr_id";

	    var RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription;
	    var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
	    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	    var configuration = {
	        'iceServers': [{
	            'url': 'stun:stun.l.google.com:19302'
	        }]
	    };
	    vm.rtcPeerConn;
	    vm.videoStream;

	    vm.friendListOpened = true;
	    vm.openedChats = [];
	    vm.socketId = "/#";
	    vm.msgs = {};
	    vm.isModalVisible = false;
	    vm.chatMembers = [];
	    vm.frndList = [];
	    vm.connectedMembers = {}; // Online Members
	    vm.newFriendsToGrp = [];
	    vm.allFriendsInfo = {}; // id -> detail map

	    vm.isCreateGrpModalVisible = false;

	    vm.isUserLoggedIn = isUserLoggedIn;
	    vm.openGroupModal = openGroupModal;
	    vm.openChatOf = openChatOf;
	    vm.closeChatOf = closeChatOf;
	    vm.toggleChatOf = toggleChatOf;
	    vm.sendMsg = sendMsg;
	    vm.sendMsgFromModalOnKeyPress = sendMsgFromModalOnKeyPress;
	    vm.openAndFetchMsgs = openAndFetchMsgs;
	    vm.isUserGrpOnline = isUserGrpOnline;
	    vm.shouldShowPlaceHolder = shouldShowPlaceHolder;
	    vm.removePlaceHolder = removePlaceHolder;
	    vm.setActiveMember = setActiveMember;
	    vm.sendMsgFromModal = sendMsgFromModal;
	    vm.clearAllMsgOf = clearAllMsgOf;
	    vm.getOpenMemberIndex = getOpenMemberIndex;
	    // vm.toggleHeightWidthOfFriendList = toggleHeightWidthOfFriendList;
	    // vm.toggleHeightWidthOfMsgs = toggleHeightWidthOfMsgs;
	    vm.visibleModalWithMember = visibleModalWithMember;

	    vm.getUserInfo = getUserInfo;

	    vm.findFriends = findFriends;
	    vm.isUserOnline = isUserOnline;

	    vm.createGroup = createGroup;
	    vm.addMembersToGrp = addMembersToGrp;

	    vm.startAudioCall = startAudioCall;
	    vm.stopAudioCall = stopAudioCall;
	    vm.startVideoCall = startVideoCall;
	    vm.stopVideoCall = stopVideoCall;
	    vm.acceptAudioCall = acceptAudioCall;
	    vm.declineAudioCall = declineAudioCall;

	    vm.acceptVideoCall = acceptVideoCall;
	    vm.declineVideoCall = declineVideoCall;

	    var mySound = new Audio(notification_sound_path);

	    $scope.$watch('vm.grpPic', function(newValue, oldValue, scope) {
	        if (newValue) {
	            console.log(newValue);
	            var reader = new FileReader();
	            reader.onload = function(evt) {
	                $scope.$apply(function($scope) {
	                    vm.showGrpPicModal = true;
	                    vm.image = evt.target.result;
	                });
	            };
	            reader.readAsDataURL(newValue);
	        }
	    });

	    vm.cropImage = function cropImage() {
	        vm.initCrop = true;
	        setTimeout(function() {
	            vm.showGrpPicModal = false;
	            $scope.$apply();
	        }, 100);
	    }

	    vm.$onInit = function() {

	        $(window).on('focus', function() {
	            vm.isTabbedFocus = true;
	        });
	        $(window).on('blur', function() {
	            vm.isTabbedFocus = false;
	        });

	        if (authToken.isAuthenticated()) {
	            vm.getUserInfo();
	            vm.findFriends()
	                .then(function() {
	                    // socketInit();
	                })
	                .catch(function() {
	                    console.error(err);
	                });
	        }
	    }

	    function socketInit() {
	        var host = window.location.hostname;
	        vm.socket = {};//websocket.socket;//io(BASE_API + '/');
	        console.log('Socket Trying to connect ' + BASE_API)
	        // if (host.indexOf("localhost") <= -1) {
	        //     //If not localhost
	        //     vm.socket = io('https://' + host + '/');
	        //     console.log('Socket Trying to connect https://' + host + '/')
	        // } else {
	        //     //If localhost
	        //     vm.socket = io('http://' + host + '/');
	        //     console.log('Socket Trying to connect http://' + host + '/')
	        // }
	        // vm.socket = websocket.socket;

	        vm.socket.on('connect', function() {
	            vm.socketId = vm.socket.id;
	            console.info("Connect", vm.socket.id);
	            authToken.setSocketId(vm.socket.id);
	            var user = authToken.getUser();
	            vm.userInfo = {
	                user_id: user[userKey],
	                display_name: user.dsp_nm
	            };
	            console.log("sending userInfo to server socket....")
	            vm.socket.emit('userInfo', {
	                socketId: vm.socketId,
	                user_id: vm.userInfo.user_id,
	                display_name: vm.userInfo.display_name
	            });
	        });

	        vm.socket.on('connectedSockets', function(data) {
	            var clients = data.clients;
	            var idStr = vm.socketId;
	            vm.connectedMembers = {};
	            var members = clients.filter(function(v) {
	                vm.connectedMembers[v.user_id] = v.user_id;
	                if (v.user_id === vm.userInfo.user_id) return false;
	                return true;
	            });
	            vm.activeMember = members[0];
	            vm.activeGroup = '';

	            console.log("Connected mems:", data);
	            $scope.$apply();
	        });

	        vm.socket.on('disconnect', function() {
	            console.info("Socket DisConnected");
	            authToken.removeSocketId();
	        });

	        vm.socket.on('logoutMsg', function(data) {
	            console.info("logoutMsg in chat component", data);
	            alertify.alert(data.msg, function() {
	                window.location.href = "/login/session-expired"
	            });
	        });

	        vm.socket.on('disconnectedSockets', function(data) {
	            var id = data.socket;
	            console.log("Connected mems:", vm.connectedMembers);
	            if (vm.connectedMembers[id]) {
	                delete vm.connectedMembers[id];
	            }
	        });

	        vm.socket.on('newMsg', function(d) {
	            console.log('newMsg......', d);
	            vm.msgs[d.frm_usr] = vm.msgs[d.frm_usr] || [];
	            d.msg_ts = new Date(d.msg_ts);
	            vm.msgs[d.frm_usr].push(d);
	            openIncomingChat(d.frm_usr);
	            scrollToEndOnNewMsg(d.frm_usr);
	            if (!vm.isTabbedFocus) {
	                mySound.play();
	            }
	            $scope.$apply();
	        });

	        vm.socket.on('newTypingMsg', function(d) {
	            console.log('newTypingMsg......', d);
	            vm.newMesageUserId = d.user_id;
	            vm.msgs[d.frm_usr] = vm.msgs[d.frm_usr] || [];
	            clearTimeout(vm.activeMember["timerId"]);

	            if (!vm.isTabbedFocus) {
	                vm.activeMember["isTyping"] = true;
	                vm.activeMember["timerId"] = setTimeout(function(){
	                    vm.activeMember.isTyping = false
	                    $scope.$apply();
	                },2000)
	            }
	            $scope.$apply();
	        });

	        vm.socket.on('selfNewMsg', function(d) {
	            console.log('selfNewMsg......', d);
	            vm.newMesageUserId = d.user_id;
	            var isThereAnySameMsg = vm.msgs[d.fr_id].filter(function(v, i) {
	                if (v.msg_id === d.msg_id) {
	                    return true;
	                }
	                return false;
	            })
	            if (isThereAnySameMsg.length <= 0) {
	                vm.msgs[d.fr_id] = vm.msgs[d.fr_id] || [];
	                d.msg_ts = new Date(d.msg_ts);
	                vm.msgs[d.fr_id].push(d);
	                scrollToEndOnNewMsg(d.fr_id);
	                $scope.$apply();
	            }
	        });

	        vm.socket.on('newMsgInGroup', function(d) {
	            //fr_id means grp_id
	            // console.log(d);
	            if (d.frm_usr !== vm.userInfo.user_id) {
	                vm.msgs[d.fr_id] = vm.msgs[d.fr_id] || [];
	                d.msg_ts = new Date(d.msg_ts);
	                vm.msgs[d.fr_id].push(d);
	                openIncomingChat(d.fr_id);
	                scrollToEndOnNewMsg(d.fr_id);
	                if (!vm.isTabbedFocus) {
	                    mySound.play();
	                }
	                $scope.$apply();
	            }
	        });

	         vm.socket.on('newTypingMsgInGroup', function(d) {
	            //fr_id means grp_id
	            // console.log(d);
	            if (d.frm_usr !== vm.userInfo.user_id) {
	                vm.msgs[d.fr_id] = vm.msgs[d.fr_id] || [];
	                d.msg_ts = new Date(d.msg_ts);
	                vm.msgs[d.fr_id].push(d);
	                openIncomingChat(d.fr_id);
	                scrollToEndOnNewMsg(d.fr_id);
	                if (!vm.isTabbedFocus) {
	                    //mySound.play();
	                }
	                $scope.$apply();
	            }
	        });

	        vm.socket.on('startAudioCallRequest', function(d) {
	            console.log("startAudioCallRequest", vm.allFriendsInfo[d.from]);
	            vm.activeMediaPerson = vm.allFriendsInfo[d.from].display_name;
	            vm.isCallAccepted = false;
	            vm.isActiveAudioCall = true;
	            vm.currentCallRequest = d;
	            vm.isCallEnded = false;
	            vm.isCallConnecting = true;
	            vm.isCallConnected = false;
	            $scope.$apply();
	        });

	        vm.socket.on('startVideoCallRequest', function(d) {
	            console.log("startVideoCallRequest", vm.allFriendsInfo[d.from]);
	            vm.activeMediaPerson = vm.allFriendsInfo[d.from].display_name;
	            vm.isActiveVideoCall = true;
	            vm.isCallAccepted = false;
	            vm.currentCallRequest = d;
	            vm.isCallEnded = false;
	            vm.isCallConnecting = true;
	            vm.isCallConnected = false;
	            $scope.$apply();
	        });

	        vm.socket.on('acceptedAudioCall', function(d) {
	            console.log("acceptedAudioCall", vm.allFriendsInfo[d.from]);
	            if (!vm.rtcPeerConn) {
	                startSignaling(true, false);
	            }
	            vm.isCallEnded = false;
	            vm.isCallConnecting = false;
	            vm.isCallConnected = true;
	            $scope.$apply();
	        });

	        vm.socket.on('acceptedVideoCall', function(d) {
	            console.log("acceptedAudioCall", vm.allFriendsInfo[d.from]);
	            if (!vm.rtcPeerConn) {
	                startSignaling(false, false);
	            }
	            vm.isCallEnded = false;
	            vm.isCallConnecting = false;
	            vm.isCallConnected = true;
	            $scope.$apply();
	        });

	        vm.socket.on('endCall', function(d) {
	            endCallOnSocketMsg();
	            $scope.$apply();
	        });

	        vm.socket.on('signaling_message', function(data) {
	            var message = JSON.parse(data.message);
	            console.log(data);
	            if (message.sdp) {
	                vm.shouldAddICE = true;
	                vm.rtcPeerConn.setRemoteDescription(new RTCSessionDescription(message.sdp), function() {
	                    // if we received an offer, we need to answer
	                    if (vm.rtcPeerConn.remoteDescription.type == 'offer') {
	                        console.log("offer came,sent answer");
	                        vm.rtcPeerConn.createAnswer(sendLocalDesc, function(err) {
	                            console.error(err);
	                        });
	                    }
	                }, function(err) {
	                    console.error(err);
	                });
	            } else {
	                if (vm.shouldAddICE) {
	                    vm.rtcPeerConn.addIceCandidate(new RTCIceCandidate(message.candidate));
	                }
	            }
	            $scope.$apply();
	        });
	    }

	    function isUserLoggedIn() {
	        var loggedIn = authToken.isAuthenticated();
	        return loggedIn;
	    }

	    function getUserInfo() {
	        var user = authToken.getUser();
	        vm.userInfo = {
	            display_name: user.displayName,
	            user_id: user[userKey]
	        }
	        return vm.userInfo;
	    }

	    function openGroupModal() {
	        vm.isCreateGrpModalVisible = true;
	    }

	    function openChatOf(member) {
	        var item = $filter('filter')(vm.openedChats, {
	            member: member
	        })[0] || [];
	        item.closed = false;
	        item.minimized = false;
	        item.color = false;
	        scrollToEndOnNewMsg(member.user_id);
	    }

	    function closeChatOf(member) {
	        var item = $filter('filter')(vm.openedChats, {
	            member: member
	        })[0];
	        if (item) {
	            var pos = vm.openedChats.indexOf(item);
	            vm.openedChats[pos].closed = true;
	            vm.openedChats[pos].minimized = true;
	        }
	        chatUtility.deleteMember(member.user_id);
	    }

	    function toggleChatOf(member) {
	        var item = $filter('filter')(vm.openedChats, {
	            member: member
	        })[0] || [];

	        if (!item.minimized) {
	            item.minimized = true;
	        } else {
	            openChatOf(member);
	        }
	    }

	    function openIncomingChat(user_id) {
	        var item = $filter('filter')(vm.openedChats, {
	            'member': {
	                'user_id': user_id
	            }
	        })[0];
	        item.closed = false;
	        if (item.minimized) {
	            item.color = true;
	            item.minimized = true;
	        }
	    }

	    function getOpenMemberIndex(user_id) {
	        var item = $filter('filter')(vm.openedChats, {
	            'member': {
	                'user_id': user_id
	            }
	        })[0];
	        var pos = vm.openedChats.indexOf(item);
	        return pos;
	    }

	    function scrollToEndOnNewMsg(user_id) {
	        if (vm.isModalVisible) {
	            setTimeout(function() {
	                var id = '#msgs';
	                var ele = $(id)[0];
	                ele.scrollTop = ele.scrollHeight;
	            });
	        } else {
	            setTimeout(function() {
	                var id = '#msgs-wrapper_' + getOpenMemberIndex(user_id);
	                var ele = $(id)[0];
	                ele.scrollTop = ele.scrollHeight;
	            });
	        }

	    }

	    function clearAllMsgOf(member) {
	        var user_id = member.user_id;
	        var confirmation = confirm("Do you really want to delete messages?");
	        if (!confirmation) {
	            return;
	        }
	        vm.msgs[user_id] = [];
	        var o = {
	            user_id: vm.userInfo.user_id,
	            fr_id: member.user_id
	        }
	        chat.clearMsgs(o)
	            .then(function(res) {
	                console.log(res);
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }


	    function sendMsgFromSocket(msg, data) {
	        vm.socket.emit(msg, data);
	    }

	    function send(member, fromModal) {
	        var msgFrom = vm.getUserInfo();
	        var msgTo = member;
	        var id = "";

	        if (fromModal) {
	            id = '#msg';
	        } else {
	            id = '#msg_' + getOpenMemberIndex(member.user_id);
	        }

	        var msg = $(id).html();
	        if (msg.length > 0 && $(id).text().trim().length > 0) {
	            var t = Date.now();
	            var format = new Date(t);

	            var data = {
	                    frm_usr: msgFrom.user_id,
	                    user_id: msgFrom.user_id,
	                    fr_id: msgTo.user_id,
	                    msg: msg,
	                    msg_id: '' + Math.floor(100000 + Math.random() * 900000),
	                    msg_ts: new Date()
	                }
	                // vm.msgs[data.fr_id] = vm.msgs[data.fr_id] || [];
	                // vm.msgs[data.fr_id].push(data);
	                //
	            vm.msgs[data.fr_id] = vm.msgs[data.fr_id] || [];
	            vm.msgs[data.fr_id].push(data);
	            scrollToEndOnNewMsg(data.fr_id);

	            if (member.typ === 'grp') {
	                sendMsgFromSocket('sendMsgToGroup', data);
	            } else if (member.typ === 'usr') {
	                sendMsgFromSocket('sendMsg', data);
	            }

	            setTimeout(function() {
	                $(id).html('');
	                $(id).trigger("click");
	            }, 10);
	            var ele;
	            if (fromModal) {
	                ele = $(id).parent().siblings('.msgs')[0];
	            } else {
	                ele = $(id).parent().siblings('.msgs-wrapper')[0];
	            }
	            setTimeout(function() {
	                ele.scrollTop = ele.scrollHeight;
	            });

	        }
	    }

	    function sendTypingMsg(member) {
	        debugger;
	        var msgFrom = vm.getUserInfo();
	        var msgTo = member;
	        var id = "";

	        var data = {
	            frm_usr: msgFrom.user_id,
	            user_id: msgFrom.user_id,
	            fr_id: msgTo.user_id
	        }
	        console.log("sendTypingMsg "+member.typ)
	        if (member.typ === 'grp') {
	            sendMsgFromSocket('sendTypingMsgToGroup', data);
	        } else if (member.typ === 'usr') {
	            sendMsgFromSocket('sendTypingMsg', data);
	        }

	    }

	    function sendMsg($event, member) {
	        sendTypingMsg(member);

	        if ($event.keyCode === 13 && !$event.shiftKey) {
	            send(member, false);
	        }
	    }

	    function sendMsgFromModalOnKeyPress($event, member) {
	        sendTypingMsg(member);

	        if ($event.keyCode === 13 && !$event.shiftKey) {
	            send(member, true);
	        }
	    }

	    function sendMsgFromModal(member) {
	        sendTypingMsg(member);
	        send(member, true);
	    }


	    function openAndFetchMsgs(member,flag) {
	        console.log(member);
	        vm.openChatOf(member);
	        var d = {
	            user_id: vm.userInfo.user_id,
	            fr_id: member.user_id
	        }
	        if (vm.msgs[d.fr_id]) {
	            return;
	        }
	        if (!flag) {
	            chatUtility.openChatList(member.user_id);
	        }
	        chat.fetchMsgs(d)
	            .then(function(d) {
	                var msgs = d.data;

	                msgs = msgs.map(function(v) {
	                    v.msg_ts = new Date(v.msg_ts)
	                    return v;
	                });
	                vm.msgs[member.user_id] = msgs;
	                if (vm.isModalVisible) {
	                    //If Modal is open then only one wrapper
	                    setTimeout(function() {
	                        var id = '#msgs';
	                        var ele = $(id)[0];
	                        ele.scrollTop = ele.scrollHeight;;
	                    })
	                } else {
	                    //If Modal is open then multiple wrapper
	                    setTimeout(function() {
	                        var id = '#msgs-wrapper_' + getOpenMemberIndex(member.user_id);
	                        var ele = $(id)[0];
	                        ele.scrollTop = ele.scrollHeight;;
	                    })
	                }

	            })
	            .catch(function(err) {
	                console.error(err);
	            })

	        if (member.typ == 'grp') {
	            //Basically create room on server side
	            sendMsgFromSocket('createGroup', {
	                room_id: member.user_id
	            });
	        }
	    }

	    function setActiveMember(member) {
	        vm.activeMember = member;
	        vm.openAndFetchMsgs(member);
	    }

	    function shouldShowPlaceHolder($event, user_id) {
	        // var id = '#msg_' + user_id.substr(2);
	        if ($event.keyCode === 13)
	            return;
	        var id = $event.target;
	        var element = $(id);
	        if (element.text().length <= 0) {
	            // element.html('Type a Message').addClass('gray-msg');
	        }
	    }

	    function removePlaceHolder($event, user_id) {
	        // var id = '#msg_' + user_id.substr(2);
	        var id = $event.target;
	        var element = $(id);
	        if (element.text() === 'Type a Message') {
	            element.html('').removeClass('gray-msg');
	        }
	    }

	    function visibleModalWithMember(user_id) {
	        var item = $filter('filter')(vm.chatMembers, {
	            'user_id': user_id
	        })[0];
	        vm.activeMember = item;
	        // console.log('visibleModalWithMember', vm.activeMember);
	        vm.isModalVisible = true;
	        // console.log(vm.chatMembers);
	    }

	    function isUserOnline(user_id) {
	        // console.log("Userid......",user_id , !!vm.connectedMembers[user_id]);
	        return !!vm.connectedMembers[user_id];
	    }

	    function isUserGrpOnline(grp_mem) {
	        // console.log("Userid......",user_id , !!vm.connectedMembers[user_id]);

	        var isOnline = false;
	        for(var x in grp_mem) {
	            if(x != vm.userInfo.user_id && vm.connectedMembers[x]) {
	                isOnline = true;
	                break;
	            }
	        }

	        return isOnline;
	    }

	    function findFriends() {
	        var user = authToken.getUser();
	        var user_id = user[userKey];
	        var keyword = vm.searchFriend || '';
	        vm.openedChats = [];
	        var d = {
	            user_id: user_id,
	            keyword: keyword
	        }
	        return chat.findFriends(d)
	            .then(function(d) {
	                var frnds = d.data;
	                vm.chatMembers = vm.chatMembers || [];
	                frnds.forEach(function(v, i) {
	                    if (v && v.user_id != user_id) {
	                        vm.chatMembers.push(v);
	                        if(v.typ == "usr") {
	                            vm.frndList.push(v);
	                        }
	                    }
	                });
	                if (chatHistory!=null) {
	                    openChantWindow(vm.chatMembers);
	                }
	                arr = frnds.filter(function(v) {
	                    //If it is grp , add it to connectedMembers list
	                    if (v.typ === 'grp') {
	                        vm.connectedMembers[v.user_id] = v.user_id;
	                    }
	                    var obj = {
	                        member: v,
	                        minimized: true,
	                        closed: true
	                    }

	                    //Store all members as a closed and in minimize state
	                    vm.openedChats.push(obj);
	                    if (v.typ === 'usr') {
	                        //Store details of members
	                        vm.allFriendsInfo[v.user_id] = v;
	                    }
	                    //if its a grp , fetch members
	                    if (v.typ === 'grp') {
	                        // chat.findMembers({
	                        //         grp_id: v.user_id
	                        //     })
	                        //     .then(function(res) {
	                        //         var mems = res.data;

	                        //         var item = $filter('filter')(vm.chatMembers, {
	                        //             'user_id': v.user_id
	                        //         })[0];
	                        //         item.members = mems;
	                        //         //Iterate for all members
	                        //         for (v in mems) {
	                        //             //If member detail is not there
	                        //             if (!vm.allFriendsInfo[v]) {
	                        //                 //find member detail
	                        //                 // chat.findFriendWithId({
	                        //                 //         user_id: v
	                        //                 //     })
	                        //                 //     .then(function(resUser) {
	                        //                 //         var user = resUser.data;
	                        //                 //         // store member details
	                        //                 //         vm.allFriendsInfo[user.user_id] = user;
	                        //                 //     })
	                        //                 //     .catch(function(err) {
	                        //                 //         console.error(err);
	                        //                 //     })
	                        //             }
	                        //         }
	                        //     })
	                        //     .catch(function(err) {
	                        //         console.error(err);
	                        //     })
	                    }
	                    return true;
	                });
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }

	    function createGroup() {
	        console.log("createGroup");
	        var grp_id = uuid.v4().replace(/-/g, '');
	        var grp_nm = vm.grp_nm;
	        var grp_owm = vm.userInfo.user_id;
	        var members = [];
	        if (vm.selectedFriends) {
	            members = vm.selectedFriends.map(function(v) {
	                return v;
	            });
	        }
	        members.push(grp_owm);
	        var obj = {
	            grp_id: grp_id,
	            grp_nm: grp_nm,
	            grp_own: grp_owm,
	            members: members
	        }

	        var blob = Upload.dataUrltoBlob(vm.myGrpPic, grp_id + "_pic.jpg");

	        Upload.upload({
	                url: '/chat/grp/create',
	                data: {
	                    file: blob,
	                    grp_id: grp_id,
	                    grp_nm: grp_nm,
	                    grp_own: grp_owm,
	                    members: members
	                }
	            })
	            .then(function(d) {
	                console.log("Group Created.");
	                var o = {
	                    user_id: obj.grp_id,
	                    display_name: obj.grp_nm,
	                    user_email: obj.grp_id,
	                    typ: 'grp',
	                    pic50: vm.myGrpPic
	                }

	                //Add Group to list
	                vm.chatMembers.push(o);

	                //Open Group Chat
	                vm.openedChats.push({
	                    member: o,
	                    minimized: false,
	                    closed: false
	                });

	                //As it is grp , it is always connected
	                vm.connectedMembers[o.user_id] = o.user_id;

	                vm.isCreateGrpModalVisible = false;
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            // chat.createGroup(obj)
	            //     .then(function(d){

	        //     })

	    }

	    function addMembersToGrp(obj) {
	        // console.log(grp_id,vm.newFriendsToGrp);
	        var o = {
	            members: vm.newFriendsToGrp,
	            grp_id: obj.member.user_id
	        }
	        chat.addMembersToGroup(o).then(function(res) {
	                vm.newFriendsToGrp = [];
	                obj.addMemberFieldVisible = false;
	                vm.successInAddMember = vm.successInAddMember || {};
	                vm.successInAddMember[obj.member.user_id] = true;
	                setTimeout(function() {
	                    vm.successInAddMember[obj.member.user_id] = false;
	                    $scope.$apply();
	                }, 2000);
	                sendMsgFromSocket('createGroup', {
	                    room_id: obj.member.user_id
	                });
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	    }

	    function startAudioCall(calleeId) {
	        var obj = {
	            from: vm.userInfo.user_id,
	            to: calleeId
	        };
	        vm.isCallAccepted = true;
	        sendMsgFromSocket("startAudioCallRequest", obj);
	        vm.activeMediaPerson = vm.allFriendsInfo[calleeId].display_name;
	        vm.currentCallRequest = obj;
	        vm.isActiveAudioCall = true;
	        vm.isCallEnded = false;
	        vm.isCallConnecting = true;
	        vm.isCallConnected = false;

	    }

	    function stopAudioCall(calleeId) {
	        vm.isActiveAudioCall = false;
	        console.log(vm.rtcPeerConn);
	        closeRTCPeerConnetion();
	    }

	    function startVideoCall(calleeId) {
	        var obj = {
	            from: vm.userInfo.user_id,
	            to: calleeId
	        };
	        vm.isCallAccepted = true;
	        sendMsgFromSocket("startVideoCallRequest", obj);
	        vm.activeMediaPerson = vm.allFriendsInfo[calleeId].display_name;
	        vm.currentCallRequest = obj;
	        vm.isActiveVideoCall = true;
	        vm.isCallEnded = false;
	        vm.isCallConnecting = true;
	        vm.isCallConnected = false;
	    }

	    function stopVideoCall(calleeId) {
	        vm.isActiveVideoCall = false;
	        console.log(vm.rtcPeerConn);
	        closeRTCPeerConnetion();
	    }

	    function endCallOnSocketMsg() {
	        if (vm.rtcPeerConn.removeStream) {
	            try {
	                vm.rtcPeerConn.removeStream(vm.stream);
	            } catch (e) {
	                console.log("Optional Warning....", e);
	            }
	        }
	        vm.rtcPeerConn.close();
	        vm.rtcPeerConn.onicecandidate = null;
	        vm.rtcPeerConn.onaddstream = null;
	        vm.rtcPeerConn = undefined;
	        vm.shouldAddICE = false;

	        if (vm.stream.stop) {
	            vm.stream.stop();
	        } else {
	            var arrAudioTracks = vm.stream.getAudioTracks();
	            arrAudioTracks.forEach(function(streamTrack) {
	                streamTrack.stop();
	            });
	            var arrVideoTracks = vm.stream.getVideoTracks();
	            arrVideoTracks.forEach(function(streamTrack) {
	                streamTrack.stop();
	            });
	        }
	        vm.isCallEnded = true;
	        vm.isCallConnecting = false;
	        vm.isCallConnected = false;
	        vm.endCallMsg = "Call is ended.";
	    }

	    function closeRTCPeerConnetion() {
	        if (vm.isCallEnded) {
	            //Call is already ended.
	            return;
	        }
	        vm.isActiveAudioCall = false;
	        vm.isActiveVideoCall = false;

	        if (vm.rtcPeerConn && vm.rtcPeerConn.removeStream) {
	            try {
	                vm.rtcPeerConn.removeStream(vm.stream);
	            } catch (e) {
	                console.log("Optional Warning....", e);
	            }
	        }
	        if (vm.rtcPeerConn) {
	            vm.rtcPeerConn.close();
	            vm.rtcPeerConn.onicecandidate = null;
	            vm.rtcPeerConn.onaddstream = null;
	            vm.rtcPeerConn = undefined;
	        }

	        vm.shouldAddICE = false;


	        if (vm.stream && vm.stream.stop) {
	            vm.stream.stop();
	        } else {
	            var arrAudioTracks = vm.stream.getAudioTracks();
	            arrAudioTracks.forEach(function(streamTrack) {
	                streamTrack.stop();
	            });
	            var arrVideoTracks = vm.stream.getVideoTracks();
	            arrVideoTracks.forEach(function(streamTrack) {
	                streamTrack.stop();
	            });
	        }

	        var room_id = vm.currentCallRequest.to + "_" + vm.currentCallRequest.from;
	        vm.socket.emit('endCall', {
	            room_id: room_id
	        });
	    }

	    function acceptAudioCall() {
	        vm.isCallAccepted = true;
	        var obj = vm.currentCallRequest;
	        sendMsgFromSocket("acceptedAudioCall", obj);
	        startSignaling(true, true);
	        vm.isCallEnded = false;
	        vm.isCallConnecting = false;
	        vm.isCallConnected = true;
	    }

	    function acceptVideoCall() {
	        vm.isCallAccepted = true;
	        var obj = vm.currentCallRequest;
	        sendMsgFromSocket("acceptedVideoCall", obj);
	        startSignaling(false, true);
	        vm.isCallEnded = false;
	        vm.isCallConnecting = false;
	        vm.isCallConnected = true;
	    }

	    function declineAudioCall() {
	        vm.isCallAccepted = true;
	        vm.isActiveAudioCall = false;
	    }

	    function declineVideoCall() {
	        vm.isCallAccepted = true;
	        vm.isActiveVideoCall = false;
	    }


	    function startSignaling(isAudio, shouldCreateOffer) {
	        var myVideoArea = document.querySelector("#myVideoTag");
	        var theirVideoArea = document.querySelector("#theirVideoTag");
	        vm.rtcPeerConn = new RTCPeerConnection(configuration);
	        var room_id = vm.currentCallRequest.to + "_" + vm.currentCallRequest.from;
	        console.log(room_id);
	        // send any ice candidates to the other peer
	        vm.rtcPeerConn.onicecandidate = function(evt) {
	            if (evt && evt.candidate)
	                vm.socket.emit('signal', {
	                    "type": "ice candidate",
	                    "message": JSON.stringify({
	                        'candidate': evt.candidate
	                    }),
	                    "room": room_id
	                });
	        };

	        // let the 'negotiationneeded' event trigger offer generation
	        vm.rtcPeerConn.onnegotiationneeded = function() {
	            if (shouldCreateOffer) {
	                console.log("onnegotiationneeded");
	                vm.rtcPeerConn.createOffer(sendLocalDesc, function(err) {
	                    console.error(err);
	                });
	            }
	        }

	        // once remote stream arrives, show it in the remote video element
	        vm.rtcPeerConn.onaddstream = function(evt) {
	            // if(!isAudio){
	            theirVideoArea.src = URL.createObjectURL(evt.stream);
	            // }
	        };

	        // get a local stream, show it in our video tag and add it to be sent
	        navigator.getUserMedia({
	            'audio': true,
	            'video': !isAudio
	        }, function(stream) {
	            vm.stream = stream;
	            console.log(stream);

	            // if(!isAudio){
	            myVideoArea.src = URL.createObjectURL(stream);
	            // }
	            vm.rtcPeerConn.addStream(stream);
	        }, function(err) {
	            console.error(err);
	        });
	    }

	    function sendLocalDesc(desc) {
	        var room_id = vm.currentCallRequest.to + "_" + vm.currentCallRequest.from;
	        vm.rtcPeerConn.setLocalDescription(desc, function() {
	            vm.shouldAddICE = true;
	            vm.socket.emit('signal', {
	                "type": "SDP",
	                "message": JSON.stringify({
	                    'sdp': vm.rtcPeerConn.localDescription
	                }),
	                "room": room_id
	            });
	        }, function(err) {
	            console.error(err);
	        });
	    }

	    function logError(error) {
	        console.error(error);
	    }
	    var chatHistory = chatUtility.getInfo();
	    if (chatHistory!=null) {
	        console.log(chatHistory.userList);
	        if (chatHistory.isOpenDialog==true) {
	            vm.friendListOpened = true;
	        }
	    }

	    function openChantWindow(data){
	        $timeout(function() {
	            if(chatHistory.userList){
	                for (var i = 0; i < chatHistory.userList.length; i++) {
	                    for (var j = 0; j < data.length; j++) {
	                        if (chatHistory.userList[i]==data[j].user_id) {
	                            console.log('yes');
	                            vm.openAndFetchMsgs(data[j],true);
	                            break;
	                        };
	                    };
	                };
	            }
	        });
	    }
	    $('body').on('click','.chat-action-bar', function(){
	        $('body').css('position','relative');
	    });
	    $('body').on('click','.fa-square-o', function(){
	        $('body').css('position','fixed');
	    });
	    $('body').on('click','.title', function(){
	        chatUtility.isOpenDialog(vm.friendListOpened);
	    });
	    $('body').on('click','.mainclose', function(){
	        console.log('ajay');
	        chatUtility.isOpenDialog(String(false));
	    });
	}

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 141 */
/***/ function(module, exports, __webpack_require__) {

	var require;var require;/* WEBPACK VAR INJECTION */(function(global) {(function(f){if(true){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.io = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return require(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){

	module.exports =  _dereq_('./lib/');

	},{"./lib/":2}],2:[function(_dereq_,module,exports){

	module.exports = _dereq_('./socket');

	/**
	 * Exports parser
	 *
	 * @api public
	 *
	 */
	module.exports.parser = _dereq_('engine.io-parser');

	},{"./socket":3,"engine.io-parser":19}],3:[function(_dereq_,module,exports){
	(function (global){
	/**
	 * Module dependencies.
	 */

	var transports = _dereq_('./transports');
	var Emitter = _dereq_('component-emitter');
	var debug = _dereq_('debug')('engine.io-client:socket');
	var index = _dereq_('indexof');
	var parser = _dereq_('engine.io-parser');
	var parseuri = _dereq_('parseuri');
	var parsejson = _dereq_('parsejson');
	var parseqs = _dereq_('parseqs');

	/**
	 * Module exports.
	 */

	module.exports = Socket;

	/**
	 * Noop function.
	 *
	 * @api private
	 */

	function noop(){}

	/**
	 * Socket constructor.
	 *
	 * @param {String|Object} uri or options
	 * @param {Object} options
	 * @api public
	 */

	function Socket(uri, opts){
	  if (!(this instanceof Socket)) return new Socket(uri, opts);

	  opts = opts || {};

	  if (uri && 'object' == typeof uri) {
	    opts = uri;
	    uri = null;
	  }

	  if (uri) {
	    uri = parseuri(uri);
	    opts.hostname = uri.host;
	    opts.secure = uri.protocol == 'https' || uri.protocol == 'wss';
	    opts.port = uri.port;
	    if (uri.query) opts.query = uri.query;
	  } else if (opts.host) {
	    opts.hostname = parseuri(opts.host).host;
	  }

	  this.secure = null != opts.secure ? opts.secure :
	    (global.location && 'https:' == location.protocol);

	  if (opts.hostname && !opts.port) {
	    // if no port is specified manually, use the protocol default
	    opts.port = this.secure ? '443' : '80';
	  }

	  this.agent = opts.agent || false;
	  this.hostname = opts.hostname ||
	    (global.location ? location.hostname : 'localhost');
	  this.port = opts.port || (global.location && location.port ?
	       location.port :
	       (this.secure ? 443 : 80));
	  this.query = opts.query || {};
	  if ('string' == typeof this.query) this.query = parseqs.decode(this.query);
	  this.upgrade = false !== opts.upgrade;
	  this.path = (opts.path || '/engine.io').replace(/\/$/, '') + '/';
	  this.forceJSONP = !!opts.forceJSONP;
	  this.jsonp = false !== opts.jsonp;
	  this.forceBase64 = !!opts.forceBase64;
	  this.enablesXDR = !!opts.enablesXDR;
	  this.timestampParam = opts.timestampParam || 't';
	  this.timestampRequests = opts.timestampRequests;
	  this.transports = opts.transports || ['polling', 'websocket'];
	  this.readyState = '';
	  this.writeBuffer = [];
	  this.policyPort = opts.policyPort || 843;
	  this.rememberUpgrade = opts.rememberUpgrade || false;
	  this.binaryType = null;
	  this.onlyBinaryUpgrades = opts.onlyBinaryUpgrades;
	  this.perMessageDeflate = false !== opts.perMessageDeflate ? (opts.perMessageDeflate || {}) : false;

	  if (true === this.perMessageDeflate) this.perMessageDeflate = {};
	  if (this.perMessageDeflate && null == this.perMessageDeflate.threshold) {
	    this.perMessageDeflate.threshold = 1024;
	  }

	  // SSL options for Node.js client
	  this.pfx = opts.pfx || null;
	  this.key = opts.key || null;
	  this.passphrase = opts.passphrase || null;
	  this.cert = opts.cert || null;
	  this.ca = opts.ca || null;
	  this.ciphers = opts.ciphers || null;
	  this.rejectUnauthorized = opts.rejectUnauthorized === undefined ? true : opts.rejectUnauthorized;

	  // other options for Node.js client
	  var freeGlobal = typeof global == 'object' && global;
	  if (freeGlobal.global === freeGlobal) {
	    if (opts.extraHeaders && Object.keys(opts.extraHeaders).length > 0) {
	      this.extraHeaders = opts.extraHeaders;
	    }
	  }

	  this.open();
	}

	Socket.priorWebsocketSuccess = false;

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Socket.prototype);

	/**
	 * Protocol version.
	 *
	 * @api public
	 */

	Socket.protocol = parser.protocol; // this is an int

	/**
	 * Expose deps for legacy compatibility
	 * and standalone browser access.
	 */

	Socket.Socket = Socket;
	Socket.Transport = _dereq_('./transport');
	Socket.transports = _dereq_('./transports');
	Socket.parser = _dereq_('engine.io-parser');

	/**
	 * Creates transport of the given type.
	 *
	 * @param {String} transport name
	 * @return {Transport}
	 * @api private
	 */

	Socket.prototype.createTransport = function (name) {
	  debug('creating transport "%s"', name);
	  var query = clone(this.query);

	  // append engine.io protocol identifier
	  query.EIO = parser.protocol;

	  // transport name
	  query.transport = name;

	  // session id if we already have one
	  if (this.id) query.sid = this.id;

	  var transport = new transports[name]({
	    agent: this.agent,
	    hostname: this.hostname,
	    port: this.port,
	    secure: this.secure,
	    path: this.path,
	    query: query,
	    forceJSONP: this.forceJSONP,
	    jsonp: this.jsonp,
	    forceBase64: this.forceBase64,
	    enablesXDR: this.enablesXDR,
	    timestampRequests: this.timestampRequests,
	    timestampParam: this.timestampParam,
	    policyPort: this.policyPort,
	    socket: this,
	    pfx: this.pfx,
	    key: this.key,
	    passphrase: this.passphrase,
	    cert: this.cert,
	    ca: this.ca,
	    ciphers: this.ciphers,
	    rejectUnauthorized: this.rejectUnauthorized,
	    perMessageDeflate: this.perMessageDeflate,
	    extraHeaders: this.extraHeaders
	  });

	  return transport;
	};

	function clone (obj) {
	  var o = {};
	  for (var i in obj) {
	    if (obj.hasOwnProperty(i)) {
	      o[i] = obj[i];
	    }
	  }
	  return o;
	}

	/**
	 * Initializes transport to use and starts probe.
	 *
	 * @api private
	 */
	Socket.prototype.open = function () {
	  var transport;
	  if (this.rememberUpgrade && Socket.priorWebsocketSuccess && this.transports.indexOf('websocket') != -1) {
	    transport = 'websocket';
	  } else if (0 === this.transports.length) {
	    // Emit error on next tick so it can be listened to
	    var self = this;
	    setTimeout(function() {
	      self.emit('error', 'No transports available');
	    }, 0);
	    return;
	  } else {
	    transport = this.transports[0];
	  }
	  this.readyState = 'opening';

	  // Retry with the next transport if the transport is disabled (jsonp: false)
	  try {
	    transport = this.createTransport(transport);
	  } catch (e) {
	    this.transports.shift();
	    this.open();
	    return;
	  }

	  transport.open();
	  this.setTransport(transport);
	};

	/**
	 * Sets the current transport. Disables the existing one (if any).
	 *
	 * @api private
	 */

	Socket.prototype.setTransport = function(transport){
	  debug('setting transport %s', transport.name);
	  var self = this;

	  if (this.transport) {
	    debug('clearing existing transport %s', this.transport.name);
	    this.transport.removeAllListeners();
	  }

	  // set up transport
	  this.transport = transport;

	  // set up transport listeners
	  transport
	  .on('drain', function(){
	    self.onDrain();
	  })
	  .on('packet', function(packet){
	    self.onPacket(packet);
	  })
	  .on('error', function(e){
	    self.onError(e);
	  })
	  .on('close', function(){
	    self.onClose('transport close');
	  });
	};

	/**
	 * Probes a transport.
	 *
	 * @param {String} transport name
	 * @api private
	 */

	Socket.prototype.probe = function (name) {
	  debug('probing transport "%s"', name);
	  var transport = this.createTransport(name, { probe: 1 })
	    , failed = false
	    , self = this;

	  Socket.priorWebsocketSuccess = false;

	  function onTransportOpen(){
	    if (self.onlyBinaryUpgrades) {
	      var upgradeLosesBinary = !this.supportsBinary && self.transport.supportsBinary;
	      failed = failed || upgradeLosesBinary;
	    }
	    if (failed) return;

	    debug('probe transport "%s" opened', name);
	    transport.send([{ type: 'ping', data: 'probe' }]);
	    transport.once('packet', function (msg) {
	      if (failed) return;
	      if ('pong' == msg.type && 'probe' == msg.data) {
	        debug('probe transport "%s" pong', name);
	        self.upgrading = true;
	        self.emit('upgrading', transport);
	        if (!transport) return;
	        Socket.priorWebsocketSuccess = 'websocket' == transport.name;

	        debug('pausing current transport "%s"', self.transport.name);
	        self.transport.pause(function () {
	          if (failed) return;
	          if ('closed' == self.readyState) return;
	          debug('changing transport and sending upgrade packet');

	          cleanup();

	          self.setTransport(transport);
	          transport.send([{ type: 'upgrade' }]);
	          self.emit('upgrade', transport);
	          transport = null;
	          self.upgrading = false;
	          self.flush();
	        });
	      } else {
	        debug('probe transport "%s" failed', name);
	        var err = new Error('probe error');
	        err.transport = transport.name;
	        self.emit('upgradeError', err);
	      }
	    });
	  }

	  function freezeTransport() {
	    if (failed) return;

	    // Any callback called by transport should be ignored since now
	    failed = true;

	    cleanup();

	    transport.close();
	    transport = null;
	  }

	  //Handle any error that happens while probing
	  function onerror(err) {
	    var error = new Error('probe error: ' + err);
	    error.transport = transport.name;

	    freezeTransport();

	    debug('probe transport "%s" failed because of error: %s', name, err);

	    self.emit('upgradeError', error);
	  }

	  function onTransportClose(){
	    onerror("transport closed");
	  }

	  //When the socket is closed while we're probing
	  function onclose(){
	    onerror("socket closed");
	  }

	  //When the socket is upgraded while we're probing
	  function onupgrade(to){
	    if (transport && to.name != transport.name) {
	      debug('"%s" works - aborting "%s"', to.name, transport.name);
	      freezeTransport();
	    }
	  }

	  //Remove all listeners on the transport and on self
	  function cleanup(){
	    transport.removeListener('open', onTransportOpen);
	    transport.removeListener('error', onerror);
	    transport.removeListener('close', onTransportClose);
	    self.removeListener('close', onclose);
	    self.removeListener('upgrading', onupgrade);
	  }

	  transport.once('open', onTransportOpen);
	  transport.once('error', onerror);
	  transport.once('close', onTransportClose);

	  this.once('close', onclose);
	  this.once('upgrading', onupgrade);

	  transport.open();

	};

	/**
	 * Called when connection is deemed open.
	 *
	 * @api public
	 */

	Socket.prototype.onOpen = function () {
	  debug('socket open');
	  this.readyState = 'open';
	  Socket.priorWebsocketSuccess = 'websocket' == this.transport.name;
	  this.emit('open');
	  this.flush();

	  // we check for `readyState` in case an `open`
	  // listener already closed the socket
	  if ('open' == this.readyState && this.upgrade && this.transport.pause) {
	    debug('starting upgrade probes');
	    for (var i = 0, l = this.upgrades.length; i < l; i++) {
	      this.probe(this.upgrades[i]);
	    }
	  }
	};

	/**
	 * Handles a packet.
	 *
	 * @api private
	 */

	Socket.prototype.onPacket = function (packet) {
	  if ('opening' == this.readyState || 'open' == this.readyState) {
	    debug('socket receive: type "%s", data "%s"', packet.type, packet.data);

	    this.emit('packet', packet);

	    // Socket is live - any packet counts
	    this.emit('heartbeat');

	    switch (packet.type) {
	      case 'open':
	        this.onHandshake(parsejson(packet.data));
	        break;

	      case 'pong':
	        this.setPing();
	        this.emit('pong');
	        break;

	      case 'error':
	        var err = new Error('server error');
	        err.code = packet.data;
	        this.onError(err);
	        break;

	      case 'message':
	        this.emit('data', packet.data);
	        this.emit('message', packet.data);
	        break;
	    }
	  } else {
	    debug('packet received with socket readyState "%s"', this.readyState);
	  }
	};

	/**
	 * Called upon handshake completion.
	 *
	 * @param {Object} handshake obj
	 * @api private
	 */

	Socket.prototype.onHandshake = function (data) {
	  this.emit('handshake', data);
	  this.id = data.sid;
	  this.transport.query.sid = data.sid;
	  this.upgrades = this.filterUpgrades(data.upgrades);
	  this.pingInterval = data.pingInterval;
	  this.pingTimeout = data.pingTimeout;
	  this.onOpen();
	  // In case open handler closes socket
	  if  ('closed' == this.readyState) return;
	  this.setPing();

	  // Prolong liveness of socket on heartbeat
	  this.removeListener('heartbeat', this.onHeartbeat);
	  this.on('heartbeat', this.onHeartbeat);
	};

	/**
	 * Resets ping timeout.
	 *
	 * @api private
	 */

	Socket.prototype.onHeartbeat = function (timeout) {
	  clearTimeout(this.pingTimeoutTimer);
	  var self = this;
	  self.pingTimeoutTimer = setTimeout(function () {
	    if ('closed' == self.readyState) return;
	    self.onClose('ping timeout');
	  }, timeout || (self.pingInterval + self.pingTimeout));
	};

	/**
	 * Pings server every `this.pingInterval` and expects response
	 * within `this.pingTimeout` or closes connection.
	 *
	 * @api private
	 */

	Socket.prototype.setPing = function () {
	  var self = this;
	  clearTimeout(self.pingIntervalTimer);
	  self.pingIntervalTimer = setTimeout(function () {
	    debug('writing ping packet - expecting pong within %sms', self.pingTimeout);
	    self.ping();
	    self.onHeartbeat(self.pingTimeout);
	  }, self.pingInterval);
	};

	/**
	* Sends a ping packet.
	*
	* @api private
	*/

	Socket.prototype.ping = function () {
	  var self = this;
	  this.sendPacket('ping', function(){
	    self.emit('ping');
	  });
	};

	/**
	 * Called on `drain` event
	 *
	 * @api private
	 */

	Socket.prototype.onDrain = function() {
	  this.writeBuffer.splice(0, this.prevBufferLen);

	  // setting prevBufferLen = 0 is very important
	  // for example, when upgrading, upgrade packet is sent over,
	  // and a nonzero prevBufferLen could cause problems on `drain`
	  this.prevBufferLen = 0;

	  if (0 === this.writeBuffer.length) {
	    this.emit('drain');
	  } else {
	    this.flush();
	  }
	};

	/**
	 * Flush write buffers.
	 *
	 * @api private
	 */

	Socket.prototype.flush = function () {
	  if ('closed' != this.readyState && this.transport.writable &&
	    !this.upgrading && this.writeBuffer.length) {
	    debug('flushing %d packets in socket', this.writeBuffer.length);
	    this.transport.send(this.writeBuffer);
	    // keep track of current length of writeBuffer
	    // splice writeBuffer and callbackBuffer on `drain`
	    this.prevBufferLen = this.writeBuffer.length;
	    this.emit('flush');
	  }
	};

	/**
	 * Sends a message.
	 *
	 * @param {String} message.
	 * @param {Function} callback function.
	 * @param {Object} options.
	 * @return {Socket} for chaining.
	 * @api public
	 */

	Socket.prototype.write =
	Socket.prototype.send = function (msg, options, fn) {
	  this.sendPacket('message', msg, options, fn);
	  return this;
	};

	/**
	 * Sends a packet.
	 *
	 * @param {String} packet type.
	 * @param {String} data.
	 * @param {Object} options.
	 * @param {Function} callback function.
	 * @api private
	 */

	Socket.prototype.sendPacket = function (type, data, options, fn) {
	  if('function' == typeof data) {
	    fn = data;
	    data = undefined;
	  }

	  if ('function' == typeof options) {
	    fn = options;
	    options = null;
	  }

	  if ('closing' == this.readyState || 'closed' == this.readyState) {
	    return;
	  }

	  options = options || {};
	  options.compress = false !== options.compress;

	  var packet = {
	    type: type,
	    data: data,
	    options: options
	  };
	  this.emit('packetCreate', packet);
	  this.writeBuffer.push(packet);
	  if (fn) this.once('flush', fn);
	  this.flush();
	};

	/**
	 * Closes the connection.
	 *
	 * @api private
	 */

	Socket.prototype.close = function () {
	  if ('opening' == this.readyState || 'open' == this.readyState) {
	    this.readyState = 'closing';

	    var self = this;

	    if (this.writeBuffer.length) {
	      this.once('drain', function() {
	        if (this.upgrading) {
	          waitForUpgrade();
	        } else {
	          close();
	        }
	      });
	    } else if (this.upgrading) {
	      waitForUpgrade();
	    } else {
	      close();
	    }
	  }

	  function close() {
	    self.onClose('forced close');
	    debug('socket closing - telling transport to close');
	    self.transport.close();
	  }

	  function cleanupAndClose() {
	    self.removeListener('upgrade', cleanupAndClose);
	    self.removeListener('upgradeError', cleanupAndClose);
	    close();
	  }

	  function waitForUpgrade() {
	    // wait for upgrade to finish since we can't send packets while pausing a transport
	    self.once('upgrade', cleanupAndClose);
	    self.once('upgradeError', cleanupAndClose);
	  }

	  return this;
	};

	/**
	 * Called upon transport error
	 *
	 * @api private
	 */

	Socket.prototype.onError = function (err) {
	  debug('socket error %j', err);
	  Socket.priorWebsocketSuccess = false;
	  this.emit('error', err);
	  this.onClose('transport error', err);
	};

	/**
	 * Called upon transport close.
	 *
	 * @api private
	 */

	Socket.prototype.onClose = function (reason, desc) {
	  if ('opening' == this.readyState || 'open' == this.readyState || 'closing' == this.readyState) {
	    debug('socket close with reason: "%s"', reason);
	    var self = this;

	    // clear timers
	    clearTimeout(this.pingIntervalTimer);
	    clearTimeout(this.pingTimeoutTimer);

	    // stop event from firing again for transport
	    this.transport.removeAllListeners('close');

	    // ensure transport won't stay open
	    this.transport.close();

	    // ignore further transport communication
	    this.transport.removeAllListeners();

	    // set ready state
	    this.readyState = 'closed';

	    // clear session id
	    this.id = null;

	    // emit close event
	    this.emit('close', reason, desc);

	    // clean buffers after, so users can still
	    // grab the buffers on `close` event
	    self.writeBuffer = [];
	    self.prevBufferLen = 0;
	  }
	};

	/**
	 * Filters upgrades, returning only those matching client transports.
	 *
	 * @param {Array} server upgrades
	 * @api private
	 *
	 */

	Socket.prototype.filterUpgrades = function (upgrades) {
	  var filteredUpgrades = [];
	  for (var i = 0, j = upgrades.length; i<j; i++) {
	    if (~index(this.transports, upgrades[i])) filteredUpgrades.push(upgrades[i]);
	  }
	  return filteredUpgrades;
	};

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"./transport":4,"./transports":5,"component-emitter":15,"debug":17,"engine.io-parser":19,"indexof":23,"parsejson":26,"parseqs":27,"parseuri":28}],4:[function(_dereq_,module,exports){
	/**
	 * Module dependencies.
	 */

	var parser = _dereq_('engine.io-parser');
	var Emitter = _dereq_('component-emitter');

	/**
	 * Module exports.
	 */

	module.exports = Transport;

	/**
	 * Transport abstract constructor.
	 *
	 * @param {Object} options.
	 * @api private
	 */

	function Transport (opts) {
	  this.path = opts.path;
	  this.hostname = opts.hostname;
	  this.port = opts.port;
	  this.secure = opts.secure;
	  this.query = opts.query;
	  this.timestampParam = opts.timestampParam;
	  this.timestampRequests = opts.timestampRequests;
	  this.readyState = '';
	  this.agent = opts.agent || false;
	  this.socket = opts.socket;
	  this.enablesXDR = opts.enablesXDR;

	  // SSL options for Node.js client
	  this.pfx = opts.pfx;
	  this.key = opts.key;
	  this.passphrase = opts.passphrase;
	  this.cert = opts.cert;
	  this.ca = opts.ca;
	  this.ciphers = opts.ciphers;
	  this.rejectUnauthorized = opts.rejectUnauthorized;

	  // other options for Node.js client
	  this.extraHeaders = opts.extraHeaders;
	}

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Transport.prototype);

	/**
	 * Emits an error.
	 *
	 * @param {String} str
	 * @return {Transport} for chaining
	 * @api public
	 */

	Transport.prototype.onError = function (msg, desc) {
	  var err = new Error(msg);
	  err.type = 'TransportError';
	  err.description = desc;
	  this.emit('error', err);
	  return this;
	};

	/**
	 * Opens the transport.
	 *
	 * @api public
	 */

	Transport.prototype.open = function () {
	  if ('closed' == this.readyState || '' == this.readyState) {
	    this.readyState = 'opening';
	    this.doOpen();
	  }

	  return this;
	};

	/**
	 * Closes the transport.
	 *
	 * @api private
	 */

	Transport.prototype.close = function () {
	  if ('opening' == this.readyState || 'open' == this.readyState) {
	    this.doClose();
	    this.onClose();
	  }

	  return this;
	};

	/**
	 * Sends multiple packets.
	 *
	 * @param {Array} packets
	 * @api private
	 */

	Transport.prototype.send = function(packets){
	  if ('open' == this.readyState) {
	    this.write(packets);
	  } else {
	    throw new Error('Transport not open');
	  }
	};

	/**
	 * Called upon open
	 *
	 * @api private
	 */

	Transport.prototype.onOpen = function () {
	  this.readyState = 'open';
	  this.writable = true;
	  this.emit('open');
	};

	/**
	 * Called with data.
	 *
	 * @param {String} data
	 * @api private
	 */

	Transport.prototype.onData = function(data){
	  var packet = parser.decodePacket(data, this.socket.binaryType);
	  this.onPacket(packet);
	};

	/**
	 * Called with a decoded packet.
	 */

	Transport.prototype.onPacket = function (packet) {
	  this.emit('packet', packet);
	};

	/**
	 * Called upon close.
	 *
	 * @api private
	 */

	Transport.prototype.onClose = function () {
	  this.readyState = 'closed';
	  this.emit('close');
	};

	},{"component-emitter":15,"engine.io-parser":19}],5:[function(_dereq_,module,exports){
	(function (global){
	/**
	 * Module dependencies
	 */

	var XMLHttpRequest = _dereq_('xmlhttprequest-ssl');
	var XHR = _dereq_('./polling-xhr');
	var JSONP = _dereq_('./polling-jsonp');
	var websocket = _dereq_('./websocket');

	/**
	 * Export transports.
	 */

	exports.polling = polling;
	exports.websocket = websocket;

	/**
	 * Polling transport polymorphic constructor.
	 * Decides on xhr vs jsonp based on feature detection.
	 *
	 * @api private
	 */

	function polling(opts){
	  var xhr;
	  var xd = false;
	  var xs = false;
	  var jsonp = false !== opts.jsonp;

	  if (global.location) {
	    var isSSL = 'https:' == location.protocol;
	    var port = location.port;

	    // some user agents have empty `location.port`
	    if (!port) {
	      port = isSSL ? 443 : 80;
	    }

	    xd = opts.hostname != location.hostname || port != opts.port;
	    xs = opts.secure != isSSL;
	  }

	  opts.xdomain = xd;
	  opts.xscheme = xs;
	  xhr = new XMLHttpRequest(opts);

	  if ('open' in xhr && !opts.forceJSONP) {
	    return new XHR(opts);
	  } else {
	    if (!jsonp) throw new Error('JSONP disabled');
	    return new JSONP(opts);
	  }
	}

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"./polling-jsonp":6,"./polling-xhr":7,"./websocket":9,"xmlhttprequest-ssl":10}],6:[function(_dereq_,module,exports){
	(function (global){

	/**
	 * Module requirements.
	 */

	var Polling = _dereq_('./polling');
	var inherit = _dereq_('component-inherit');

	/**
	 * Module exports.
	 */

	module.exports = JSONPPolling;

	/**
	 * Cached regular expressions.
	 */

	var rNewline = /\n/g;
	var rEscapedNewline = /\\n/g;

	/**
	 * Global JSONP callbacks.
	 */

	var callbacks;

	/**
	 * Callbacks count.
	 */

	var index = 0;

	/**
	 * Noop.
	 */

	function empty () { }

	/**
	 * JSONP Polling constructor.
	 *
	 * @param {Object} opts.
	 * @api public
	 */

	function JSONPPolling (opts) {
	  Polling.call(this, opts);

	  this.query = this.query || {};

	  // define global callbacks array if not present
	  // we do this here (lazily) to avoid unneeded global pollution
	  if (!callbacks) {
	    // we need to consider multiple engines in the same page
	    if (!global.___eio) global.___eio = [];
	    callbacks = global.___eio;
	  }

	  // callback identifier
	  this.index = callbacks.length;

	  // add callback to jsonp global
	  var self = this;
	  callbacks.push(function (msg) {
	    self.onData(msg);
	  });

	  // append to query string
	  this.query.j = this.index;

	  // prevent spurious errors from being emitted when the window is unloaded
	  if (global.document && global.addEventListener) {
	    global.addEventListener('beforeunload', function () {
	      if (self.script) self.script.onerror = empty;
	    }, false);
	  }
	}

	/**
	 * Inherits from Polling.
	 */

	inherit(JSONPPolling, Polling);

	/*
	 * JSONP only supports binary as base64 encoded strings
	 */

	JSONPPolling.prototype.supportsBinary = false;

	/**
	 * Closes the socket.
	 *
	 * @api private
	 */

	JSONPPolling.prototype.doClose = function () {
	  if (this.script) {
	    this.script.parentNode.removeChild(this.script);
	    this.script = null;
	  }

	  if (this.form) {
	    this.form.parentNode.removeChild(this.form);
	    this.form = null;
	    this.iframe = null;
	  }

	  Polling.prototype.doClose.call(this);
	};

	/**
	 * Starts a poll cycle.
	 *
	 * @api private
	 */

	JSONPPolling.prototype.doPoll = function () {
	  var self = this;
	  var script = document.createElement('script');

	  if (this.script) {
	    this.script.parentNode.removeChild(this.script);
	    this.script = null;
	  }

	  script.async = true;
	  script.src = this.uri();
	  script.onerror = function(e){
	    self.onError('jsonp poll error',e);
	  };

	  var insertAt = document.getElementsByTagName('script')[0];
	  if (insertAt) {
	    insertAt.parentNode.insertBefore(script, insertAt);
	  }
	  else {
	    (document.head || document.body).appendChild(script);
	  }
	  this.script = script;

	  var isUAgecko = 'undefined' != typeof navigator && /gecko/i.test(navigator.userAgent);
	  
	  if (isUAgecko) {
	    setTimeout(function () {
	      var iframe = document.createElement('iframe');
	      document.body.appendChild(iframe);
	      document.body.removeChild(iframe);
	    }, 100);
	  }
	};

	/**
	 * Writes with a hidden iframe.
	 *
	 * @param {String} data to send
	 * @param {Function} called upon flush.
	 * @api private
	 */

	JSONPPolling.prototype.doWrite = function (data, fn) {
	  var self = this;

	  if (!this.form) {
	    var form = document.createElement('form');
	    var area = document.createElement('textarea');
	    var id = this.iframeId = 'eio_iframe_' + this.index;
	    var iframe;

	    form.className = 'socketio';
	    form.style.position = 'absolute';
	    form.style.top = '-1000px';
	    form.style.left = '-1000px';
	    form.target = id;
	    form.method = 'POST';
	    form.setAttribute('accept-charset', 'utf-8');
	    area.name = 'd';
	    form.appendChild(area);
	    document.body.appendChild(form);

	    this.form = form;
	    this.area = area;
	  }

	  this.form.action = this.uri();

	  function complete () {
	    initIframe();
	    fn();
	  }

	  function initIframe () {
	    if (self.iframe) {
	      try {
	        self.form.removeChild(self.iframe);
	      } catch (e) {
	        self.onError('jsonp polling iframe removal error', e);
	      }
	    }

	    try {
	      // ie6 dynamic iframes with target="" support (thanks Chris Lambacher)
	      var html = '<iframe src="javascript:0" name="'+ self.iframeId +'">';
	      iframe = document.createElement(html);
	    } catch (e) {
	      iframe = document.createElement('iframe');
	      iframe.name = self.iframeId;
	      iframe.src = 'javascript:0';
	    }

	    iframe.id = self.iframeId;

	    self.form.appendChild(iframe);
	    self.iframe = iframe;
	  }

	  initIframe();

	  // escape \n to prevent it from being converted into \r\n by some UAs
	  // double escaping is required for escaped new lines because unescaping of new lines can be done safely on server-side
	  data = data.replace(rEscapedNewline, '\\\n');
	  this.area.value = data.replace(rNewline, '\\n');

	  try {
	    this.form.submit();
	  } catch(e) {}

	  if (this.iframe.attachEvent) {
	    this.iframe.onreadystatechange = function(){
	      if (self.iframe.readyState == 'complete') {
	        complete();
	      }
	    };
	  } else {
	    this.iframe.onload = complete;
	  }
	};

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"./polling":8,"component-inherit":16}],7:[function(_dereq_,module,exports){
	(function (global){
	/**
	 * Module requirements.
	 */

	var XMLHttpRequest = _dereq_('xmlhttprequest-ssl');
	var Polling = _dereq_('./polling');
	var Emitter = _dereq_('component-emitter');
	var inherit = _dereq_('component-inherit');
	var debug = _dereq_('debug')('engine.io-client:polling-xhr');

	/**
	 * Module exports.
	 */

	module.exports = XHR;
	module.exports.Request = Request;

	/**
	 * Empty function
	 */

	function empty(){}

	/**
	 * XHR Polling constructor.
	 *
	 * @param {Object} opts
	 * @api public
	 */

	function XHR(opts){
	  Polling.call(this, opts);

	  if (global.location) {
	    var isSSL = 'https:' == location.protocol;
	    var port = location.port;

	    // some user agents have empty `location.port`
	    if (!port) {
	      port = isSSL ? 443 : 80;
	    }

	    this.xd = opts.hostname != global.location.hostname ||
	      port != opts.port;
	    this.xs = opts.secure != isSSL;
	  } else {
	    this.extraHeaders = opts.extraHeaders;
	  }
	}

	/**
	 * Inherits from Polling.
	 */

	inherit(XHR, Polling);

	/**
	 * XHR supports binary
	 */

	XHR.prototype.supportsBinary = true;

	/**
	 * Creates a request.
	 *
	 * @param {String} method
	 * @api private
	 */

	XHR.prototype.request = function(opts){
	  opts = opts || {};
	  opts.uri = this.uri();
	  opts.xd = this.xd;
	  opts.xs = this.xs;
	  opts.agent = this.agent || false;
	  opts.supportsBinary = this.supportsBinary;
	  opts.enablesXDR = this.enablesXDR;

	  // SSL options for Node.js client
	  opts.pfx = this.pfx;
	  opts.key = this.key;
	  opts.passphrase = this.passphrase;
	  opts.cert = this.cert;
	  opts.ca = this.ca;
	  opts.ciphers = this.ciphers;
	  opts.rejectUnauthorized = this.rejectUnauthorized;

	  // other options for Node.js client
	  opts.extraHeaders = this.extraHeaders;

	  return new Request(opts);
	};

	/**
	 * Sends data.
	 *
	 * @param {String} data to send.
	 * @param {Function} called upon flush.
	 * @api private
	 */

	XHR.prototype.doWrite = function(data, fn){
	  var isBinary = typeof data !== 'string' && data !== undefined;
	  var req = this.request({ method: 'POST', data: data, isBinary: isBinary });
	  var self = this;
	  req.on('success', fn);
	  req.on('error', function(err){
	    self.onError('xhr post error', err);
	  });
	  this.sendXhr = req;
	};

	/**
	 * Starts a poll cycle.
	 *
	 * @api private
	 */

	XHR.prototype.doPoll = function(){
	  debug('xhr poll');
	  var req = this.request();
	  var self = this;
	  req.on('data', function(data){
	    self.onData(data);
	  });
	  req.on('error', function(err){
	    self.onError('xhr poll error', err);
	  });
	  this.pollXhr = req;
	};

	/**
	 * Request constructor
	 *
	 * @param {Object} options
	 * @api public
	 */

	function Request(opts){
	  this.method = opts.method || 'GET';
	  this.uri = opts.uri;
	  this.xd = !!opts.xd;
	  this.xs = !!opts.xs;
	  this.async = false !== opts.async;
	  this.data = undefined != opts.data ? opts.data : null;
	  this.agent = opts.agent;
	  this.isBinary = opts.isBinary;
	  this.supportsBinary = opts.supportsBinary;
	  this.enablesXDR = opts.enablesXDR;

	  // SSL options for Node.js client
	  this.pfx = opts.pfx;
	  this.key = opts.key;
	  this.passphrase = opts.passphrase;
	  this.cert = opts.cert;
	  this.ca = opts.ca;
	  this.ciphers = opts.ciphers;
	  this.rejectUnauthorized = opts.rejectUnauthorized;

	  // other options for Node.js client
	  this.extraHeaders = opts.extraHeaders;

	  this.create();
	}

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Request.prototype);

	/**
	 * Creates the XHR object and sends the request.
	 *
	 * @api private
	 */

	Request.prototype.create = function(){
	  var opts = { agent: this.agent, xdomain: this.xd, xscheme: this.xs, enablesXDR: this.enablesXDR };

	  // SSL options for Node.js client
	  opts.pfx = this.pfx;
	  opts.key = this.key;
	  opts.passphrase = this.passphrase;
	  opts.cert = this.cert;
	  opts.ca = this.ca;
	  opts.ciphers = this.ciphers;
	  opts.rejectUnauthorized = this.rejectUnauthorized;

	  var xhr = this.xhr = new XMLHttpRequest(opts);
	  var self = this;

	  try {
	    debug('xhr open %s: %s', this.method, this.uri);
	    xhr.open(this.method, this.uri, this.async);
	    try {
	      if (this.extraHeaders) {
	        xhr.setDisableHeaderCheck(true);
	        for (var i in this.extraHeaders) {
	          if (this.extraHeaders.hasOwnProperty(i)) {
	            xhr.setRequestHeader(i, this.extraHeaders[i]);
	          }
	        }
	      }
	    } catch (e) {}
	    if (this.supportsBinary) {
	      // This has to be done after open because Firefox is stupid
	      // http://stackoverflow.com/questions/13216903/get-binary-data-with-xmlhttprequest-in-a-firefox-extension
	      xhr.responseType = 'arraybuffer';
	    }

	    if ('POST' == this.method) {
	      try {
	        if (this.isBinary) {
	          xhr.setRequestHeader('Content-type', 'application/octet-stream');
	        } else {
	          xhr.setRequestHeader('Content-type', 'text/plain;charset=UTF-8');
	        }
	      } catch (e) {}
	    }

	    // ie6 check
	    if ('withCredentials' in xhr) {
	      xhr.withCredentials = true;
	    }

	    if (this.hasXDR()) {
	      xhr.onload = function(){
	        self.onLoad();
	      };
	      xhr.onerror = function(){
	        self.onError(xhr.responseText);
	      };
	    } else {
	      xhr.onreadystatechange = function(){
	        if (4 != xhr.readyState) return;
	        if (200 == xhr.status || 1223 == xhr.status) {
	          self.onLoad();
	        } else {
	          // make sure the `error` event handler that's user-set
	          // does not throw in the same tick and gets caught here
	          setTimeout(function(){
	            self.onError(xhr.status);
	          }, 0);
	        }
	      };
	    }

	    debug('xhr data %s', this.data);
	    xhr.send(this.data);
	  } catch (e) {
	    // Need to defer since .create() is called directly fhrom the constructor
	    // and thus the 'error' event can only be only bound *after* this exception
	    // occurs.  Therefore, also, we cannot throw here at all.
	    setTimeout(function() {
	      self.onError(e);
	    }, 0);
	    return;
	  }

	  if (global.document) {
	    this.index = Request.requestsCount++;
	    Request.requests[this.index] = this;
	  }
	};

	/**
	 * Called upon successful response.
	 *
	 * @api private
	 */

	Request.prototype.onSuccess = function(){
	  this.emit('success');
	  this.cleanup();
	};

	/**
	 * Called if we have data.
	 *
	 * @api private
	 */

	Request.prototype.onData = function(data){
	  this.emit('data', data);
	  this.onSuccess();
	};

	/**
	 * Called upon error.
	 *
	 * @api private
	 */

	Request.prototype.onError = function(err){
	  this.emit('error', err);
	  this.cleanup(true);
	};

	/**
	 * Cleans up house.
	 *
	 * @api private
	 */

	Request.prototype.cleanup = function(fromError){
	  if ('undefined' == typeof this.xhr || null === this.xhr) {
	    return;
	  }
	  // xmlhttprequest
	  if (this.hasXDR()) {
	    this.xhr.onload = this.xhr.onerror = empty;
	  } else {
	    this.xhr.onreadystatechange = empty;
	  }

	  if (fromError) {
	    try {
	      this.xhr.abort();
	    } catch(e) {}
	  }

	  if (global.document) {
	    delete Request.requests[this.index];
	  }

	  this.xhr = null;
	};

	/**
	 * Called upon load.
	 *
	 * @api private
	 */

	Request.prototype.onLoad = function(){
	  var data;
	  try {
	    var contentType;
	    try {
	      contentType = this.xhr.getResponseHeader('Content-Type').split(';')[0];
	    } catch (e) {}
	    if (contentType === 'application/octet-stream') {
	      data = this.xhr.response;
	    } else {
	      if (!this.supportsBinary) {
	        data = this.xhr.responseText;
	      } else {
	        try {
	          data = String.fromCharCode.apply(null, new Uint8Array(this.xhr.response));
	        } catch (e) {
	          var ui8Arr = new Uint8Array(this.xhr.response);
	          var dataArray = [];
	          for (var idx = 0, length = ui8Arr.length; idx < length; idx++) {
	            dataArray.push(ui8Arr[idx]);
	          }

	          data = String.fromCharCode.apply(null, dataArray);
	        }
	      }
	    }
	  } catch (e) {
	    this.onError(e);
	  }
	  if (null != data) {
	    this.onData(data);
	  }
	};

	/**
	 * Check if it has XDomainRequest.
	 *
	 * @api private
	 */

	Request.prototype.hasXDR = function(){
	  return 'undefined' !== typeof global.XDomainRequest && !this.xs && this.enablesXDR;
	};

	/**
	 * Aborts the request.
	 *
	 * @api public
	 */

	Request.prototype.abort = function(){
	  this.cleanup();
	};

	/**
	 * Aborts pending requests when unloading the window. This is needed to prevent
	 * memory leaks (e.g. when using IE) and to ensure that no spurious error is
	 * emitted.
	 */

	if (global.document) {
	  Request.requestsCount = 0;
	  Request.requests = {};
	  if (global.attachEvent) {
	    global.attachEvent('onunload', unloadHandler);
	  } else if (global.addEventListener) {
	    global.addEventListener('beforeunload', unloadHandler, false);
	  }
	}

	function unloadHandler() {
	  for (var i in Request.requests) {
	    if (Request.requests.hasOwnProperty(i)) {
	      Request.requests[i].abort();
	    }
	  }
	}

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"./polling":8,"component-emitter":15,"component-inherit":16,"debug":17,"xmlhttprequest-ssl":10}],8:[function(_dereq_,module,exports){
	/**
	 * Module dependencies.
	 */

	var Transport = _dereq_('../transport');
	var parseqs = _dereq_('parseqs');
	var parser = _dereq_('engine.io-parser');
	var inherit = _dereq_('component-inherit');
	var yeast = _dereq_('yeast');
	var debug = _dereq_('debug')('engine.io-client:polling');

	/**
	 * Module exports.
	 */

	module.exports = Polling;

	/**
	 * Is XHR2 supported?
	 */

	var hasXHR2 = (function() {
	  var XMLHttpRequest = _dereq_('xmlhttprequest-ssl');
	  var xhr = new XMLHttpRequest({ xdomain: false });
	  return null != xhr.responseType;
	})();

	/**
	 * Polling interface.
	 *
	 * @param {Object} opts
	 * @api private
	 */

	function Polling(opts){
	  var forceBase64 = (opts && opts.forceBase64);
	  if (!hasXHR2 || forceBase64) {
	    this.supportsBinary = false;
	  }
	  Transport.call(this, opts);
	}

	/**
	 * Inherits from Transport.
	 */

	inherit(Polling, Transport);

	/**
	 * Transport name.
	 */

	Polling.prototype.name = 'polling';

	/**
	 * Opens the socket (triggers polling). We write a PING message to determine
	 * when the transport is open.
	 *
	 * @api private
	 */

	Polling.prototype.doOpen = function(){
	  this.poll();
	};

	/**
	 * Pauses polling.
	 *
	 * @param {Function} callback upon buffers are flushed and transport is paused
	 * @api private
	 */

	Polling.prototype.pause = function(onPause){
	  var pending = 0;
	  var self = this;

	  this.readyState = 'pausing';

	  function pause(){
	    debug('paused');
	    self.readyState = 'paused';
	    onPause();
	  }

	  if (this.polling || !this.writable) {
	    var total = 0;

	    if (this.polling) {
	      debug('we are currently polling - waiting to pause');
	      total++;
	      this.once('pollComplete', function(){
	        debug('pre-pause polling complete');
	        --total || pause();
	      });
	    }

	    if (!this.writable) {
	      debug('we are currently writing - waiting to pause');
	      total++;
	      this.once('drain', function(){
	        debug('pre-pause writing complete');
	        --total || pause();
	      });
	    }
	  } else {
	    pause();
	  }
	};

	/**
	 * Starts polling cycle.
	 *
	 * @api public
	 */

	Polling.prototype.poll = function(){
	  debug('polling');
	  this.polling = true;
	  this.doPoll();
	  this.emit('poll');
	};

	/**
	 * Overloads onData to detect payloads.
	 *
	 * @api private
	 */

	Polling.prototype.onData = function(data){
	  var self = this;
	  debug('polling got data %s', data);
	  var callback = function(packet, index, total) {
	    // if its the first message we consider the transport open
	    if ('opening' == self.readyState) {
	      self.onOpen();
	    }

	    // if its a close packet, we close the ongoing requests
	    if ('close' == packet.type) {
	      self.onClose();
	      return false;
	    }

	    // otherwise bypass onData and handle the message
	    self.onPacket(packet);
	  };

	  // decode payload
	  parser.decodePayload(data, this.socket.binaryType, callback);

	  // if an event did not trigger closing
	  if ('closed' != this.readyState) {
	    // if we got data we're not polling
	    this.polling = false;
	    this.emit('pollComplete');

	    if ('open' == this.readyState) {
	      this.poll();
	    } else {
	      debug('ignoring poll - transport state "%s"', this.readyState);
	    }
	  }
	};

	/**
	 * For polling, send a close packet.
	 *
	 * @api private
	 */

	Polling.prototype.doClose = function(){
	  var self = this;

	  function close(){
	    debug('writing close packet');
	    self.write([{ type: 'close' }]);
	  }

	  if ('open' == this.readyState) {
	    debug('transport open - closing');
	    close();
	  } else {
	    // in case we're trying to close while
	    // handshaking is in progress (GH-164)
	    debug('transport not open - deferring close');
	    this.once('open', close);
	  }
	};

	/**
	 * Writes a packets payload.
	 *
	 * @param {Array} data packets
	 * @param {Function} drain callback
	 * @api private
	 */

	Polling.prototype.write = function(packets){
	  var self = this;
	  this.writable = false;
	  var callbackfn = function() {
	    self.writable = true;
	    self.emit('drain');
	  };

	  var self = this;
	  parser.encodePayload(packets, this.supportsBinary, function(data) {
	    self.doWrite(data, callbackfn);
	  });
	};

	/**
	 * Generates uri for connection.
	 *
	 * @api private
	 */

	Polling.prototype.uri = function(){
	  var query = this.query || {};
	  var schema = this.secure ? 'https' : 'http';
	  var port = '';

	  // cache busting is forced
	  if (false !== this.timestampRequests) {
	    query[this.timestampParam] = yeast();
	  }

	  if (!this.supportsBinary && !query.sid) {
	    query.b64 = 1;
	  }

	  query = parseqs.encode(query);

	  // avoid port if default for schema
	  if (this.port && (('https' == schema && this.port != 443) ||
	     ('http' == schema && this.port != 80))) {
	    port = ':' + this.port;
	  }

	  // prepend ? to query
	  if (query.length) {
	    query = '?' + query;
	  }

	  var ipv6 = this.hostname.indexOf(':') !== -1;
	  return schema + '://' + (ipv6 ? '[' + this.hostname + ']' : this.hostname) + port + this.path + query;
	};

	},{"../transport":4,"component-inherit":16,"debug":17,"engine.io-parser":19,"parseqs":27,"xmlhttprequest-ssl":10,"yeast":30}],9:[function(_dereq_,module,exports){
	(function (global){
	/**
	 * Module dependencies.
	 */

	var Transport = _dereq_('../transport');
	var parser = _dereq_('engine.io-parser');
	var parseqs = _dereq_('parseqs');
	var inherit = _dereq_('component-inherit');
	var yeast = _dereq_('yeast');
	var debug = _dereq_('debug')('engine.io-client:websocket');
	var BrowserWebSocket = global.WebSocket || global.MozWebSocket;

	/**
	 * Get either the `WebSocket` or `MozWebSocket` globals
	 * in the browser or try to resolve WebSocket-compatible
	 * interface exposed by `ws` for Node-like environment.
	 */

	var WebSocket = BrowserWebSocket;
	if (!WebSocket && typeof window === 'undefined') {
	  try {
	    WebSocket = _dereq_('ws');
	  } catch (e) { }
	}

	/**
	 * Module exports.
	 */

	module.exports = WS;

	/**
	 * WebSocket transport constructor.
	 *
	 * @api {Object} connection options
	 * @api public
	 */

	function WS(opts){
	  var forceBase64 = (opts && opts.forceBase64);
	  if (forceBase64) {
	    this.supportsBinary = false;
	  }
	  this.perMessageDeflate = opts.perMessageDeflate;
	  Transport.call(this, opts);
	}

	/**
	 * Inherits from Transport.
	 */

	inherit(WS, Transport);

	/**
	 * Transport name.
	 *
	 * @api public
	 */

	WS.prototype.name = 'websocket';

	/*
	 * WebSockets support binary
	 */

	WS.prototype.supportsBinary = true;

	/**
	 * Opens socket.
	 *
	 * @api private
	 */

	WS.prototype.doOpen = function(){
	  if (!this.check()) {
	    // let probe timeout
	    return;
	  }

	  var self = this;
	  var uri = this.uri();
	  var protocols = void(0);
	  var opts = {
	    agent: this.agent,
	    perMessageDeflate: this.perMessageDeflate
	  };

	  // SSL options for Node.js client
	  opts.pfx = this.pfx;
	  opts.key = this.key;
	  opts.passphrase = this.passphrase;
	  opts.cert = this.cert;
	  opts.ca = this.ca;
	  opts.ciphers = this.ciphers;
	  opts.rejectUnauthorized = this.rejectUnauthorized;
	  if (this.extraHeaders) {
	    opts.headers = this.extraHeaders;
	  }

	  this.ws = BrowserWebSocket ? new WebSocket(uri) : new WebSocket(uri, protocols, opts);

	  if (this.ws.binaryType === undefined) {
	    this.supportsBinary = false;
	  }

	  if (this.ws.supports && this.ws.supports.binary) {
	    this.supportsBinary = true;
	    this.ws.binaryType = 'buffer';
	  } else {
	    this.ws.binaryType = 'arraybuffer';
	  }

	  this.addEventListeners();
	};

	/**
	 * Adds event listeners to the socket
	 *
	 * @api private
	 */

	WS.prototype.addEventListeners = function(){
	  var self = this;

	  this.ws.onopen = function(){
	    self.onOpen();
	  };
	  this.ws.onclose = function(){
	    self.onClose();
	  };
	  this.ws.onmessage = function(ev){
	    self.onData(ev.data);
	  };
	  this.ws.onerror = function(e){
	    self.onError('websocket error', e);
	  };
	};

	/**
	 * Override `onData` to use a timer on iOS.
	 * See: https://gist.github.com/mloughran/2052006
	 *
	 * @api private
	 */

	if ('undefined' != typeof navigator
	  && /iPad|iPhone|iPod/i.test(navigator.userAgent)) {
	  WS.prototype.onData = function(data){
	    var self = this;
	    setTimeout(function(){
	      Transport.prototype.onData.call(self, data);
	    }, 0);
	  };
	}

	/**
	 * Writes data to socket.
	 *
	 * @param {Array} array of packets.
	 * @api private
	 */

	WS.prototype.write = function(packets){
	  var self = this;
	  this.writable = false;

	  // encodePacket efficient as it uses WS framing
	  // no need for encodePayload
	  var total = packets.length;
	  for (var i = 0, l = total; i < l; i++) {
	    (function(packet) {
	      parser.encodePacket(packet, self.supportsBinary, function(data) {
	        if (!BrowserWebSocket) {
	          // always create a new object (GH-437)
	          var opts = {};
	          if (packet.options) {
	            opts.compress = packet.options.compress;
	          }

	          if (self.perMessageDeflate) {
	            var len = 'string' == typeof data ? global.Buffer.byteLength(data) : data.length;
	            if (len < self.perMessageDeflate.threshold) {
	              opts.compress = false;
	            }
	          }
	        }

	        //Sometimes the websocket has already been closed but the browser didn't
	        //have a chance of informing us about it yet, in that case send will
	        //throw an error
	        try {
	          if (BrowserWebSocket) {
	            // TypeError is thrown when passing the second argument on Safari
	            self.ws.send(data);
	          } else {
	            self.ws.send(data, opts);
	          }
	        } catch (e){
	          debug('websocket closed before onclose event');
	        }

	        --total || done();
	      });
	    })(packets[i]);
	  }

	  function done(){
	    self.emit('flush');

	    // fake drain
	    // defer to next tick to allow Socket to clear writeBuffer
	    setTimeout(function(){
	      self.writable = true;
	      self.emit('drain');
	    }, 0);
	  }
	};

	/**
	 * Called upon close
	 *
	 * @api private
	 */

	WS.prototype.onClose = function(){
	  Transport.prototype.onClose.call(this);
	};

	/**
	 * Closes socket.
	 *
	 * @api private
	 */

	WS.prototype.doClose = function(){
	  if (typeof this.ws !== 'undefined') {
	    this.ws.close();
	  }
	};

	/**
	 * Generates uri for connection.
	 *
	 * @api private
	 */

	WS.prototype.uri = function(){
	  var query = this.query || {};
	  var schema = this.secure ? 'wss' : 'ws';
	  var port = '';

	  // avoid port if default for schema
	  if (this.port && (('wss' == schema && this.port != 443)
	    || ('ws' == schema && this.port != 80))) {
	    port = ':' + this.port;
	  }

	  // append timestamp to URI
	  if (this.timestampRequests) {
	    query[this.timestampParam] = yeast();
	  }

	  // communicate binary support capabilities
	  if (!this.supportsBinary) {
	    query.b64 = 1;
	  }

	  query = parseqs.encode(query);

	  // prepend ? to query
	  if (query.length) {
	    query = '?' + query;
	  }

	  var ipv6 = this.hostname.indexOf(':') !== -1;
	  return schema + '://' + (ipv6 ? '[' + this.hostname + ']' : this.hostname) + port + this.path + query;
	};

	/**
	 * Feature detection for WebSocket.
	 *
	 * @return {Boolean} whether this transport is available.
	 * @api public
	 */

	WS.prototype.check = function(){
	  return !!WebSocket && !('__initialize' in WebSocket && this.name === WS.prototype.name);
	};

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"../transport":4,"component-inherit":16,"debug":17,"engine.io-parser":19,"parseqs":27,"ws":undefined,"yeast":30}],10:[function(_dereq_,module,exports){
	// browser shim for xmlhttprequest module
	var hasCORS = _dereq_('has-cors');

	module.exports = function(opts) {
	  var xdomain = opts.xdomain;

	  // scheme must be same when usign XDomainRequest
	  // http://blogs.msdn.com/b/ieinternals/archive/2010/05/13/xdomainrequest-restrictions-limitations-and-workarounds.aspx
	  var xscheme = opts.xscheme;

	  // XDomainRequest has a flow of not sending cookie, therefore it should be disabled as a default.
	  // https://github.com/Automattic/engine.io-client/pull/217
	  var enablesXDR = opts.enablesXDR;

	  // XMLHttpRequest can be disabled on IE
	  try {
	    if ('undefined' != typeof XMLHttpRequest && (!xdomain || hasCORS)) {
	      return new XMLHttpRequest();
	    }
	  } catch (e) { }

	  // Use XDomainRequest for IE8 if enablesXDR is true
	  // because loading bar keeps flashing when using jsonp-polling
	  // https://github.com/yujiosaka/socke.io-ie8-loading-example
	  try {
	    if ('undefined' != typeof XDomainRequest && !xscheme && enablesXDR) {
	      return new XDomainRequest();
	    }
	  } catch (e) { }

	  if (!xdomain) {
	    try {
	      return new ActiveXObject('Microsoft.XMLHTTP');
	    } catch(e) { }
	  }
	}

	},{"has-cors":22}],11:[function(_dereq_,module,exports){
	module.exports = after

	function after(count, callback, err_cb) {
	    var bail = false
	    err_cb = err_cb || noop
	    proxy.count = count

	    return (count === 0) ? callback() : proxy

	    function proxy(err, result) {
	        if (proxy.count <= 0) {
	            throw new Error('after called too many times')
	        }
	        --proxy.count

	        // after first error, rest are passed to err_cb
	        if (err) {
	            bail = true
	            callback(err)
	            // future error callbacks will go to error handler
	            callback = err_cb
	        } else if (proxy.count === 0 && !bail) {
	            callback(null, result)
	        }
	    }
	}

	function noop() {}

	},{}],12:[function(_dereq_,module,exports){
	/**
	 * An abstraction for slicing an arraybuffer even when
	 * ArrayBuffer.prototype.slice is not supported
	 *
	 * @api public
	 */

	module.exports = function(arraybuffer, start, end) {
	  var bytes = arraybuffer.byteLength;
	  start = start || 0;
	  end = end || bytes;

	  if (arraybuffer.slice) { return arraybuffer.slice(start, end); }

	  if (start < 0) { start += bytes; }
	  if (end < 0) { end += bytes; }
	  if (end > bytes) { end = bytes; }

	  if (start >= bytes || start >= end || bytes === 0) {
	    return new ArrayBuffer(0);
	  }

	  var abv = new Uint8Array(arraybuffer);
	  var result = new Uint8Array(end - start);
	  for (var i = start, ii = 0; i < end; i++, ii++) {
	    result[ii] = abv[i];
	  }
	  return result.buffer;
	};

	},{}],13:[function(_dereq_,module,exports){
	/*
	 * base64-arraybuffer
	 * https://github.com/niklasvh/base64-arraybuffer
	 *
	 * Copyright (c) 2012 Niklas von Hertzen
	 * Licensed under the MIT license.
	 */
	(function(chars){
	  "use strict";

	  exports.encode = function(arraybuffer) {
	    var bytes = new Uint8Array(arraybuffer),
	    i, len = bytes.length, base64 = "";

	    for (i = 0; i < len; i+=3) {
	      base64 += chars[bytes[i] >> 2];
	      base64 += chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)];
	      base64 += chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)];
	      base64 += chars[bytes[i + 2] & 63];
	    }

	    if ((len % 3) === 2) {
	      base64 = base64.substring(0, base64.length - 1) + "=";
	    } else if (len % 3 === 1) {
	      base64 = base64.substring(0, base64.length - 2) + "==";
	    }

	    return base64;
	  };

	  exports.decode =  function(base64) {
	    var bufferLength = base64.length * 0.75,
	    len = base64.length, i, p = 0,
	    encoded1, encoded2, encoded3, encoded4;

	    if (base64[base64.length - 1] === "=") {
	      bufferLength--;
	      if (base64[base64.length - 2] === "=") {
	        bufferLength--;
	      }
	    }

	    var arraybuffer = new ArrayBuffer(bufferLength),
	    bytes = new Uint8Array(arraybuffer);

	    for (i = 0; i < len; i+=4) {
	      encoded1 = chars.indexOf(base64[i]);
	      encoded2 = chars.indexOf(base64[i+1]);
	      encoded3 = chars.indexOf(base64[i+2]);
	      encoded4 = chars.indexOf(base64[i+3]);

	      bytes[p++] = (encoded1 << 2) | (encoded2 >> 4);
	      bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2);
	      bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63);
	    }

	    return arraybuffer;
	  };
	})("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");

	},{}],14:[function(_dereq_,module,exports){
	(function (global){
	/**
	 * Create a blob builder even when vendor prefixes exist
	 */

	var BlobBuilder = global.BlobBuilder
	  || global.WebKitBlobBuilder
	  || global.MSBlobBuilder
	  || global.MozBlobBuilder;

	/**
	 * Check if Blob constructor is supported
	 */

	var blobSupported = (function() {
	  try {
	    var a = new Blob(['hi']);
	    return a.size === 2;
	  } catch(e) {
	    return false;
	  }
	})();

	/**
	 * Check if Blob constructor supports ArrayBufferViews
	 * Fails in Safari 6, so we need to map to ArrayBuffers there.
	 */

	var blobSupportsArrayBufferView = blobSupported && (function() {
	  try {
	    var b = new Blob([new Uint8Array([1,2])]);
	    return b.size === 2;
	  } catch(e) {
	    return false;
	  }
	})();

	/**
	 * Check if BlobBuilder is supported
	 */

	var blobBuilderSupported = BlobBuilder
	  && BlobBuilder.prototype.append
	  && BlobBuilder.prototype.getBlob;

	/**
	 * Helper function that maps ArrayBufferViews to ArrayBuffers
	 * Used by BlobBuilder constructor and old browsers that didn't
	 * support it in the Blob constructor.
	 */

	function mapArrayBufferViews(ary) {
	  for (var i = 0; i < ary.length; i++) {
	    var chunk = ary[i];
	    if (chunk.buffer instanceof ArrayBuffer) {
	      var buf = chunk.buffer;

	      // if this is a subarray, make a copy so we only
	      // include the subarray region from the underlying buffer
	      if (chunk.byteLength !== buf.byteLength) {
	        var copy = new Uint8Array(chunk.byteLength);
	        copy.set(new Uint8Array(buf, chunk.byteOffset, chunk.byteLength));
	        buf = copy.buffer;
	      }

	      ary[i] = buf;
	    }
	  }
	}

	function BlobBuilderConstructor(ary, options) {
	  options = options || {};

	  var bb = new BlobBuilder();
	  mapArrayBufferViews(ary);

	  for (var i = 0; i < ary.length; i++) {
	    bb.append(ary[i]);
	  }

	  return (options.type) ? bb.getBlob(options.type) : bb.getBlob();
	};

	function BlobConstructor(ary, options) {
	  mapArrayBufferViews(ary);
	  return new Blob(ary, options || {});
	};

	module.exports = (function() {
	  if (blobSupported) {
	    return blobSupportsArrayBufferView ? global.Blob : BlobConstructor;
	  } else if (blobBuilderSupported) {
	    return BlobBuilderConstructor;
	  } else {
	    return undefined;
	  }
	})();

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{}],15:[function(_dereq_,module,exports){

	/**
	 * Expose `Emitter`.
	 */

	module.exports = Emitter;

	/**
	 * Initialize a new `Emitter`.
	 *
	 * @api public
	 */

	function Emitter(obj) {
	  if (obj) return mixin(obj);
	};

	/**
	 * Mixin the emitter properties.
	 *
	 * @param {Object} obj
	 * @return {Object}
	 * @api private
	 */

	function mixin(obj) {
	  for (var key in Emitter.prototype) {
	    obj[key] = Emitter.prototype[key];
	  }
	  return obj;
	}

	/**
	 * Listen on the given `event` with `fn`.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.on =
	Emitter.prototype.addEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};
	  (this._callbacks[event] = this._callbacks[event] || [])
	    .push(fn);
	  return this;
	};

	/**
	 * Adds an `event` listener that will be invoked a single
	 * time then automatically removed.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.once = function(event, fn){
	  var self = this;
	  this._callbacks = this._callbacks || {};

	  function on() {
	    self.off(event, on);
	    fn.apply(this, arguments);
	  }

	  on.fn = fn;
	  this.on(event, on);
	  return this;
	};

	/**
	 * Remove the given callback for `event` or all
	 * registered callbacks.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.off =
	Emitter.prototype.removeListener =
	Emitter.prototype.removeAllListeners =
	Emitter.prototype.removeEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};

	  // all
	  if (0 == arguments.length) {
	    this._callbacks = {};
	    return this;
	  }

	  // specific event
	  var callbacks = this._callbacks[event];
	  if (!callbacks) return this;

	  // remove all handlers
	  if (1 == arguments.length) {
	    delete this._callbacks[event];
	    return this;
	  }

	  // remove specific handler
	  var cb;
	  for (var i = 0; i < callbacks.length; i++) {
	    cb = callbacks[i];
	    if (cb === fn || cb.fn === fn) {
	      callbacks.splice(i, 1);
	      break;
	    }
	  }
	  return this;
	};

	/**
	 * Emit `event` with the given args.
	 *
	 * @param {String} event
	 * @param {Mixed} ...
	 * @return {Emitter}
	 */

	Emitter.prototype.emit = function(event){
	  this._callbacks = this._callbacks || {};
	  var args = [].slice.call(arguments, 1)
	    , callbacks = this._callbacks[event];

	  if (callbacks) {
	    callbacks = callbacks.slice(0);
	    for (var i = 0, len = callbacks.length; i < len; ++i) {
	      callbacks[i].apply(this, args);
	    }
	  }

	  return this;
	};

	/**
	 * Return array of callbacks for `event`.
	 *
	 * @param {String} event
	 * @return {Array}
	 * @api public
	 */

	Emitter.prototype.listeners = function(event){
	  this._callbacks = this._callbacks || {};
	  return this._callbacks[event] || [];
	};

	/**
	 * Check if this emitter has `event` handlers.
	 *
	 * @param {String} event
	 * @return {Boolean}
	 * @api public
	 */

	Emitter.prototype.hasListeners = function(event){
	  return !! this.listeners(event).length;
	};

	},{}],16:[function(_dereq_,module,exports){

	module.exports = function(a, b){
	  var fn = function(){};
	  fn.prototype = b.prototype;
	  a.prototype = new fn;
	  a.prototype.constructor = a;
	};
	},{}],17:[function(_dereq_,module,exports){

	/**
	 * This is the web browser implementation of `debug()`.
	 *
	 * Expose `debug()` as the module.
	 */

	exports = module.exports = _dereq_('./debug');
	exports.log = log;
	exports.formatArgs = formatArgs;
	exports.save = save;
	exports.load = load;
	exports.useColors = useColors;
	exports.storage = 'undefined' != typeof chrome
	               && 'undefined' != typeof chrome.storage
	                  ? chrome.storage.local
	                  : localstorage();

	/**
	 * Colors.
	 */

	exports.colors = [
	  'lightseagreen',
	  'forestgreen',
	  'goldenrod',
	  'dodgerblue',
	  'darkorchid',
	  'crimson'
	];

	/**
	 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
	 * and the Firebug extension (any Firefox version) are known
	 * to support "%c" CSS customizations.
	 *
	 * TODO: add a `localStorage` variable to explicitly enable/disable colors
	 */

	function useColors() {
	  // is webkit? http://stackoverflow.com/a/16459606/376773
	  return ('WebkitAppearance' in document.documentElement.style) ||
	    // is firebug? http://stackoverflow.com/a/398120/376773
	    (window.console && (console.firebug || (console.exception && console.table))) ||
	    // is firefox >= v31?
	    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
	    (navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31);
	}

	/**
	 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
	 */

	exports.formatters.j = function(v) {
	  return JSON.stringify(v);
	};


	/**
	 * Colorize log arguments if enabled.
	 *
	 * @api public
	 */

	function formatArgs() {
	  var args = arguments;
	  var useColors = this.useColors;

	  args[0] = (useColors ? '%c' : '')
	    + this.namespace
	    + (useColors ? ' %c' : ' ')
	    + args[0]
	    + (useColors ? '%c ' : ' ')
	    + '+' + exports.humanize(this.diff);

	  if (!useColors) return args;

	  var c = 'color: ' + this.color;
	  args = [args[0], c, 'color: inherit'].concat(Array.prototype.slice.call(args, 1));

	  // the final "%c" is somewhat tricky, because there could be other
	  // arguments passed either before or after the %c, so we need to
	  // figure out the correct index to insert the CSS into
	  var index = 0;
	  var lastC = 0;
	  args[0].replace(/%[a-z%]/g, function(match) {
	    if ('%%' === match) return;
	    index++;
	    if ('%c' === match) {
	      // we only are interested in the *last* %c
	      // (the user may have provided their own)
	      lastC = index;
	    }
	  });

	  args.splice(lastC, 0, c);
	  return args;
	}

	/**
	 * Invokes `console.log()` when available.
	 * No-op when `console.log` is not a "function".
	 *
	 * @api public
	 */

	function log() {
	  // this hackery is required for IE8/9, where
	  // the `console.log` function doesn't have 'apply'
	  return 'object' === typeof console
	    && console.log
	    && Function.prototype.apply.call(console.log, console, arguments);
	}

	/**
	 * Save `namespaces`.
	 *
	 * @param {String} namespaces
	 * @api private
	 */

	function save(namespaces) {
	  try {
	    if (null == namespaces) {
	      exports.storage.removeItem('debug');
	    } else {
	      exports.storage.debug = namespaces;
	    }
	  } catch(e) {}
	}

	/**
	 * Load `namespaces`.
	 *
	 * @return {String} returns the previously persisted debug modes
	 * @api private
	 */

	function load() {
	  var r;
	  try {
	    r = exports.storage.debug;
	  } catch(e) {}
	  return r;
	}

	/**
	 * Enable namespaces listed in `localStorage.debug` initially.
	 */

	exports.enable(load());

	/**
	 * Localstorage attempts to return the localstorage.
	 *
	 * This is necessary because safari throws
	 * when a user disables cookies/localstorage
	 * and you attempt to access it.
	 *
	 * @return {LocalStorage}
	 * @api private
	 */

	function localstorage(){
	  try {
	    return window.localStorage;
	  } catch (e) {}
	}

	},{"./debug":18}],18:[function(_dereq_,module,exports){

	/**
	 * This is the common logic for both the Node.js and web browser
	 * implementations of `debug()`.
	 *
	 * Expose `debug()` as the module.
	 */

	exports = module.exports = debug;
	exports.coerce = coerce;
	exports.disable = disable;
	exports.enable = enable;
	exports.enabled = enabled;
	exports.humanize = _dereq_('ms');

	/**
	 * The currently active debug mode names, and names to skip.
	 */

	exports.names = [];
	exports.skips = [];

	/**
	 * Map of special "%n" handling functions, for the debug "format" argument.
	 *
	 * Valid key names are a single, lowercased letter, i.e. "n".
	 */

	exports.formatters = {};

	/**
	 * Previously assigned color.
	 */

	var prevColor = 0;

	/**
	 * Previous log timestamp.
	 */

	var prevTime;

	/**
	 * Select a color.
	 *
	 * @return {Number}
	 * @api private
	 */

	function selectColor() {
	  return exports.colors[prevColor++ % exports.colors.length];
	}

	/**
	 * Create a debugger with the given `namespace`.
	 *
	 * @param {String} namespace
	 * @return {Function}
	 * @api public
	 */

	function debug(namespace) {

	  // define the `disabled` version
	  function disabled() {
	  }
	  disabled.enabled = false;

	  // define the `enabled` version
	  function enabled() {

	    var self = enabled;

	    // set `diff` timestamp
	    var curr = +new Date();
	    var ms = curr - (prevTime || curr);
	    self.diff = ms;
	    self.prev = prevTime;
	    self.curr = curr;
	    prevTime = curr;

	    // add the `color` if not set
	    if (null == self.useColors) self.useColors = exports.useColors();
	    if (null == self.color && self.useColors) self.color = selectColor();

	    var args = Array.prototype.slice.call(arguments);

	    args[0] = exports.coerce(args[0]);

	    if ('string' !== typeof args[0]) {
	      // anything else let's inspect with %o
	      args = ['%o'].concat(args);
	    }

	    // apply any `formatters` transformations
	    var index = 0;
	    args[0] = args[0].replace(/%([a-z%])/g, function(match, format) {
	      // if we encounter an escaped % then don't increase the array index
	      if (match === '%%') return match;
	      index++;
	      var formatter = exports.formatters[format];
	      if ('function' === typeof formatter) {
	        var val = args[index];
	        match = formatter.call(self, val);

	        // now we need to remove `args[index]` since it's inlined in the `format`
	        args.splice(index, 1);
	        index--;
	      }
	      return match;
	    });

	    if ('function' === typeof exports.formatArgs) {
	      args = exports.formatArgs.apply(self, args);
	    }
	    var logFn = enabled.log || exports.log || console.log.bind(console);
	    logFn.apply(self, args);
	  }
	  enabled.enabled = true;

	  var fn = exports.enabled(namespace) ? enabled : disabled;

	  fn.namespace = namespace;

	  return fn;
	}

	/**
	 * Enables a debug mode by namespaces. This can include modes
	 * separated by a colon and wildcards.
	 *
	 * @param {String} namespaces
	 * @api public
	 */

	function enable(namespaces) {
	  exports.save(namespaces);

	  var split = (namespaces || '').split(/[\s,]+/);
	  var len = split.length;

	  for (var i = 0; i < len; i++) {
	    if (!split[i]) continue; // ignore empty strings
	    namespaces = split[i].replace(/\*/g, '.*?');
	    if (namespaces[0] === '-') {
	      exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
	    } else {
	      exports.names.push(new RegExp('^' + namespaces + '$'));
	    }
	  }
	}

	/**
	 * Disable debug output.
	 *
	 * @api public
	 */

	function disable() {
	  exports.enable('');
	}

	/**
	 * Returns true if the given mode name is enabled, false otherwise.
	 *
	 * @param {String} name
	 * @return {Boolean}
	 * @api public
	 */

	function enabled(name) {
	  var i, len;
	  for (i = 0, len = exports.skips.length; i < len; i++) {
	    if (exports.skips[i].test(name)) {
	      return false;
	    }
	  }
	  for (i = 0, len = exports.names.length; i < len; i++) {
	    if (exports.names[i].test(name)) {
	      return true;
	    }
	  }
	  return false;
	}

	/**
	 * Coerce `val`.
	 *
	 * @param {Mixed} val
	 * @return {Mixed}
	 * @api private
	 */

	function coerce(val) {
	  if (val instanceof Error) return val.stack || val.message;
	  return val;
	}

	},{"ms":25}],19:[function(_dereq_,module,exports){
	(function (global){
	/**
	 * Module dependencies.
	 */

	var keys = _dereq_('./keys');
	var hasBinary = _dereq_('has-binary');
	var sliceBuffer = _dereq_('arraybuffer.slice');
	var base64encoder = _dereq_('base64-arraybuffer');
	var after = _dereq_('after');
	var utf8 = _dereq_('utf8');

	/**
	 * Check if we are running an android browser. That requires us to use
	 * ArrayBuffer with polling transports...
	 *
	 * http://ghinda.net/jpeg-blob-ajax-android/
	 */

	var isAndroid = navigator.userAgent.match(/Android/i);

	/**
	 * Check if we are running in PhantomJS.
	 * Uploading a Blob with PhantomJS does not work correctly, as reported here:
	 * https://github.com/ariya/phantomjs/issues/11395
	 * @type boolean
	 */
	var isPhantomJS = /PhantomJS/i.test(navigator.userAgent);

	/**
	 * When true, avoids using Blobs to encode payloads.
	 * @type boolean
	 */
	var dontSendBlobs = isAndroid || isPhantomJS;

	/**
	 * Current protocol version.
	 */

	exports.protocol = 3;

	/**
	 * Packet types.
	 */

	var packets = exports.packets = {
	    open:     0    // non-ws
	  , close:    1    // non-ws
	  , ping:     2
	  , pong:     3
	  , message:  4
	  , upgrade:  5
	  , noop:     6
	};

	var packetslist = keys(packets);

	/**
	 * Premade error packet.
	 */

	var err = { type: 'error', data: 'parser error' };

	/**
	 * Create a blob api even for blob builder when vendor prefixes exist
	 */

	var Blob = _dereq_('blob');

	/**
	 * Encodes a packet.
	 *
	 *     <packet type id> [ <data> ]
	 *
	 * Example:
	 *
	 *     5hello world
	 *     3
	 *     4
	 *
	 * Binary is encoded in an identical principle
	 *
	 * @api private
	 */

	exports.encodePacket = function (packet, supportsBinary, utf8encode, callback) {
	  if ('function' == typeof supportsBinary) {
	    callback = supportsBinary;
	    supportsBinary = false;
	  }

	  if ('function' == typeof utf8encode) {
	    callback = utf8encode;
	    utf8encode = null;
	  }

	  var data = (packet.data === undefined)
	    ? undefined
	    : packet.data.buffer || packet.data;

	  if (global.ArrayBuffer && data instanceof ArrayBuffer) {
	    return encodeArrayBuffer(packet, supportsBinary, callback);
	  } else if (Blob && data instanceof global.Blob) {
	    return encodeBlob(packet, supportsBinary, callback);
	  }

	  // might be an object with { base64: true, data: dataAsBase64String }
	  if (data && data.base64) {
	    return encodeBase64Object(packet, callback);
	  }

	  // Sending data as a utf-8 string
	  var encoded = packets[packet.type];

	  // data fragment is optional
	  if (undefined !== packet.data) {
	    encoded += utf8encode ? utf8.encode(String(packet.data)) : String(packet.data);
	  }

	  return callback('' + encoded);

	};

	function encodeBase64Object(packet, callback) {
	  // packet data is an object { base64: true, data: dataAsBase64String }
	  var message = 'b' + exports.packets[packet.type] + packet.data.data;
	  return callback(message);
	}

	/**
	 * Encode packet helpers for binary types
	 */

	function encodeArrayBuffer(packet, supportsBinary, callback) {
	  if (!supportsBinary) {
	    return exports.encodeBase64Packet(packet, callback);
	  }

	  var data = packet.data;
	  var contentArray = new Uint8Array(data);
	  var resultBuffer = new Uint8Array(1 + data.byteLength);

	  resultBuffer[0] = packets[packet.type];
	  for (var i = 0; i < contentArray.length; i++) {
	    resultBuffer[i+1] = contentArray[i];
	  }

	  return callback(resultBuffer.buffer);
	}

	function encodeBlobAsArrayBuffer(packet, supportsBinary, callback) {
	  if (!supportsBinary) {
	    return exports.encodeBase64Packet(packet, callback);
	  }

	  var fr = new FileReader();
	  fr.onload = function() {
	    packet.data = fr.result;
	    exports.encodePacket(packet, supportsBinary, true, callback);
	  };
	  return fr.readAsArrayBuffer(packet.data);
	}

	function encodeBlob(packet, supportsBinary, callback) {
	  if (!supportsBinary) {
	    return exports.encodeBase64Packet(packet, callback);
	  }

	  if (dontSendBlobs) {
	    return encodeBlobAsArrayBuffer(packet, supportsBinary, callback);
	  }

	  var length = new Uint8Array(1);
	  length[0] = packets[packet.type];
	  var blob = new Blob([length.buffer, packet.data]);

	  return callback(blob);
	}

	/**
	 * Encodes a packet with binary data in a base64 string
	 *
	 * @param {Object} packet, has `type` and `data`
	 * @return {String} base64 encoded message
	 */

	exports.encodeBase64Packet = function(packet, callback) {
	  var message = 'b' + exports.packets[packet.type];
	  if (Blob && packet.data instanceof global.Blob) {
	    var fr = new FileReader();
	    fr.onload = function() {
	      var b64 = fr.result.split(',')[1];
	      callback(message + b64);
	    };
	    return fr.readAsDataURL(packet.data);
	  }

	  var b64data;
	  try {
	    b64data = String.fromCharCode.apply(null, new Uint8Array(packet.data));
	  } catch (e) {
	    // iPhone Safari doesn't let you apply with typed arrays
	    var typed = new Uint8Array(packet.data);
	    var basic = new Array(typed.length);
	    for (var i = 0; i < typed.length; i++) {
	      basic[i] = typed[i];
	    }
	    b64data = String.fromCharCode.apply(null, basic);
	  }
	  message += global.btoa(b64data);
	  return callback(message);
	};

	/**
	 * Decodes a packet. Changes format to Blob if requested.
	 *
	 * @return {Object} with `type` and `data` (if any)
	 * @api private
	 */

	exports.decodePacket = function (data, binaryType, utf8decode) {
	  // String data
	  if (typeof data == 'string' || data === undefined) {
	    if (data.charAt(0) == 'b') {
	      return exports.decodeBase64Packet(data.substr(1), binaryType);
	    }

	    if (utf8decode) {
	      try {
	        data = utf8.decode(data);
	      } catch (e) {
	        return err;
	      }
	    }
	    var type = data.charAt(0);

	    if (Number(type) != type || !packetslist[type]) {
	      return err;
	    }

	    if (data.length > 1) {
	      return { type: packetslist[type], data: data.substring(1) };
	    } else {
	      return { type: packetslist[type] };
	    }
	  }

	  var asArray = new Uint8Array(data);
	  var type = asArray[0];
	  var rest = sliceBuffer(data, 1);
	  if (Blob && binaryType === 'blob') {
	    rest = new Blob([rest]);
	  }
	  return { type: packetslist[type], data: rest };
	};

	/**
	 * Decodes a packet encoded in a base64 string
	 *
	 * @param {String} base64 encoded message
	 * @return {Object} with `type` and `data` (if any)
	 */

	exports.decodeBase64Packet = function(msg, binaryType) {
	  var type = packetslist[msg.charAt(0)];
	  if (!global.ArrayBuffer) {
	    return { type: type, data: { base64: true, data: msg.substr(1) } };
	  }

	  var data = base64encoder.decode(msg.substr(1));

	  if (binaryType === 'blob' && Blob) {
	    data = new Blob([data]);
	  }

	  return { type: type, data: data };
	};

	/**
	 * Encodes multiple messages (payload).
	 *
	 *     <length>:data
	 *
	 * Example:
	 *
	 *     11:hello world2:hi
	 *
	 * If any contents are binary, they will be encoded as base64 strings. Base64
	 * encoded strings are marked with a b before the length specifier
	 *
	 * @param {Array} packets
	 * @api private
	 */

	exports.encodePayload = function (packets, supportsBinary, callback) {
	  if (typeof supportsBinary == 'function') {
	    callback = supportsBinary;
	    supportsBinary = null;
	  }

	  var isBinary = hasBinary(packets);

	  if (supportsBinary && isBinary) {
	    if (Blob && !dontSendBlobs) {
	      return exports.encodePayloadAsBlob(packets, callback);
	    }

	    return exports.encodePayloadAsArrayBuffer(packets, callback);
	  }

	  if (!packets.length) {
	    return callback('0:');
	  }

	  function setLengthHeader(message) {
	    return message.length + ':' + message;
	  }

	  function encodeOne(packet, doneCallback) {
	    exports.encodePacket(packet, !isBinary ? false : supportsBinary, true, function(message) {
	      doneCallback(null, setLengthHeader(message));
	    });
	  }

	  map(packets, encodeOne, function(err, results) {
	    return callback(results.join(''));
	  });
	};

	/**
	 * Async array map using after
	 */

	function map(ary, each, done) {
	  var result = new Array(ary.length);
	  var next = after(ary.length, done);

	  var eachWithIndex = function(i, el, cb) {
	    each(el, function(error, msg) {
	      result[i] = msg;
	      cb(error, result);
	    });
	  };

	  for (var i = 0; i < ary.length; i++) {
	    eachWithIndex(i, ary[i], next);
	  }
	}

	/*
	 * Decodes data when a payload is maybe expected. Possible binary contents are
	 * decoded from their base64 representation
	 *
	 * @param {String} data, callback method
	 * @api public
	 */

	exports.decodePayload = function (data, binaryType, callback) {
	  if (typeof data != 'string') {
	    return exports.decodePayloadAsBinary(data, binaryType, callback);
	  }

	  if (typeof binaryType === 'function') {
	    callback = binaryType;
	    binaryType = null;
	  }

	  var packet;
	  if (data == '') {
	    // parser error - ignoring payload
	    return callback(err, 0, 1);
	  }

	  var length = ''
	    , n, msg;

	  for (var i = 0, l = data.length; i < l; i++) {
	    var chr = data.charAt(i);

	    if (':' != chr) {
	      length += chr;
	    } else {
	      if ('' == length || (length != (n = Number(length)))) {
	        // parser error - ignoring payload
	        return callback(err, 0, 1);
	      }

	      msg = data.substr(i + 1, n);

	      if (length != msg.length) {
	        // parser error - ignoring payload
	        return callback(err, 0, 1);
	      }

	      if (msg.length) {
	        packet = exports.decodePacket(msg, binaryType, true);

	        if (err.type == packet.type && err.data == packet.data) {
	          // parser error in individual packet - ignoring payload
	          return callback(err, 0, 1);
	        }

	        var ret = callback(packet, i + n, l);
	        if (false === ret) return;
	      }

	      // advance cursor
	      i += n;
	      length = '';
	    }
	  }

	  if (length != '') {
	    // parser error - ignoring payload
	    return callback(err, 0, 1);
	  }

	};

	/**
	 * Encodes multiple messages (payload) as binary.
	 *
	 * <1 = binary, 0 = string><number from 0-9><number from 0-9>[...]<number
	 * 255><data>
	 *
	 * Example:
	 * 1 3 255 1 2 3, if the binary contents are interpreted as 8 bit integers
	 *
	 * @param {Array} packets
	 * @return {ArrayBuffer} encoded payload
	 * @api private
	 */

	exports.encodePayloadAsArrayBuffer = function(packets, callback) {
	  if (!packets.length) {
	    return callback(new ArrayBuffer(0));
	  }

	  function encodeOne(packet, doneCallback) {
	    exports.encodePacket(packet, true, true, function(data) {
	      return doneCallback(null, data);
	    });
	  }

	  map(packets, encodeOne, function(err, encodedPackets) {
	    var totalLength = encodedPackets.reduce(function(acc, p) {
	      var len;
	      if (typeof p === 'string'){
	        len = p.length;
	      } else {
	        len = p.byteLength;
	      }
	      return acc + len.toString().length + len + 2; // string/binary identifier + separator = 2
	    }, 0);

	    var resultArray = new Uint8Array(totalLength);

	    var bufferIndex = 0;
	    encodedPackets.forEach(function(p) {
	      var isString = typeof p === 'string';
	      var ab = p;
	      if (isString) {
	        var view = new Uint8Array(p.length);
	        for (var i = 0; i < p.length; i++) {
	          view[i] = p.charCodeAt(i);
	        }
	        ab = view.buffer;
	      }

	      if (isString) { // not true binary
	        resultArray[bufferIndex++] = 0;
	      } else { // true binary
	        resultArray[bufferIndex++] = 1;
	      }

	      var lenStr = ab.byteLength.toString();
	      for (var i = 0; i < lenStr.length; i++) {
	        resultArray[bufferIndex++] = parseInt(lenStr[i]);
	      }
	      resultArray[bufferIndex++] = 255;

	      var view = new Uint8Array(ab);
	      for (var i = 0; i < view.length; i++) {
	        resultArray[bufferIndex++] = view[i];
	      }
	    });

	    return callback(resultArray.buffer);
	  });
	};

	/**
	 * Encode as Blob
	 */

	exports.encodePayloadAsBlob = function(packets, callback) {
	  function encodeOne(packet, doneCallback) {
	    exports.encodePacket(packet, true, true, function(encoded) {
	      var binaryIdentifier = new Uint8Array(1);
	      binaryIdentifier[0] = 1;
	      if (typeof encoded === 'string') {
	        var view = new Uint8Array(encoded.length);
	        for (var i = 0; i < encoded.length; i++) {
	          view[i] = encoded.charCodeAt(i);
	        }
	        encoded = view.buffer;
	        binaryIdentifier[0] = 0;
	      }

	      var len = (encoded instanceof ArrayBuffer)
	        ? encoded.byteLength
	        : encoded.size;

	      var lenStr = len.toString();
	      var lengthAry = new Uint8Array(lenStr.length + 1);
	      for (var i = 0; i < lenStr.length; i++) {
	        lengthAry[i] = parseInt(lenStr[i]);
	      }
	      lengthAry[lenStr.length] = 255;

	      if (Blob) {
	        var blob = new Blob([binaryIdentifier.buffer, lengthAry.buffer, encoded]);
	        doneCallback(null, blob);
	      }
	    });
	  }

	  map(packets, encodeOne, function(err, results) {
	    return callback(new Blob(results));
	  });
	};

	/*
	 * Decodes data when a payload is maybe expected. Strings are decoded by
	 * interpreting each byte as a key code for entries marked to start with 0. See
	 * description of encodePayloadAsBinary
	 *
	 * @param {ArrayBuffer} data, callback method
	 * @api public
	 */

	exports.decodePayloadAsBinary = function (data, binaryType, callback) {
	  if (typeof binaryType === 'function') {
	    callback = binaryType;
	    binaryType = null;
	  }

	  var bufferTail = data;
	  var buffers = [];

	  var numberTooLong = false;
	  while (bufferTail.byteLength > 0) {
	    var tailArray = new Uint8Array(bufferTail);
	    var isString = tailArray[0] === 0;
	    var msgLength = '';

	    for (var i = 1; ; i++) {
	      if (tailArray[i] == 255) break;

	      if (msgLength.length > 310) {
	        numberTooLong = true;
	        break;
	      }

	      msgLength += tailArray[i];
	    }

	    if(numberTooLong) return callback(err, 0, 1);

	    bufferTail = sliceBuffer(bufferTail, 2 + msgLength.length);
	    msgLength = parseInt(msgLength);

	    var msg = sliceBuffer(bufferTail, 0, msgLength);
	    if (isString) {
	      try {
	        msg = String.fromCharCode.apply(null, new Uint8Array(msg));
	      } catch (e) {
	        // iPhone Safari doesn't let you apply to typed arrays
	        var typed = new Uint8Array(msg);
	        msg = '';
	        for (var i = 0; i < typed.length; i++) {
	          msg += String.fromCharCode(typed[i]);
	        }
	      }
	    }

	    buffers.push(msg);
	    bufferTail = sliceBuffer(bufferTail, msgLength);
	  }

	  var total = buffers.length;
	  buffers.forEach(function(buffer, i) {
	    callback(exports.decodePacket(buffer, binaryType, true), i, total);
	  });
	};

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"./keys":20,"after":11,"arraybuffer.slice":12,"base64-arraybuffer":13,"blob":14,"has-binary":21,"utf8":29}],20:[function(_dereq_,module,exports){

	/**
	 * Gets the keys for an object.
	 *
	 * @return {Array} keys
	 * @api private
	 */

	module.exports = Object.keys || function keys (obj){
	  var arr = [];
	  var has = Object.prototype.hasOwnProperty;

	  for (var i in obj) {
	    if (has.call(obj, i)) {
	      arr.push(i);
	    }
	  }
	  return arr;
	};

	},{}],21:[function(_dereq_,module,exports){
	(function (global){

	/*
	 * Module requirements.
	 */

	var isArray = _dereq_('isarray');

	/**
	 * Module exports.
	 */

	module.exports = hasBinary;

	/**
	 * Checks for binary data.
	 *
	 * Right now only Buffer and ArrayBuffer are supported..
	 *
	 * @param {Object} anything
	 * @api public
	 */

	function hasBinary(data) {

	  function _hasBinary(obj) {
	    if (!obj) return false;

	    if ( (global.Buffer && global.Buffer.isBuffer(obj)) ||
	         (global.ArrayBuffer && obj instanceof ArrayBuffer) ||
	         (global.Blob && obj instanceof Blob) ||
	         (global.File && obj instanceof File)
	        ) {
	      return true;
	    }

	    if (isArray(obj)) {
	      for (var i = 0; i < obj.length; i++) {
	          if (_hasBinary(obj[i])) {
	              return true;
	          }
	      }
	    } else if (obj && 'object' == typeof obj) {
	      if (obj.toJSON) {
	        obj = obj.toJSON();
	      }

	      for (var key in obj) {
	        if (Object.prototype.hasOwnProperty.call(obj, key) && _hasBinary(obj[key])) {
	          return true;
	        }
	      }
	    }

	    return false;
	  }

	  return _hasBinary(data);
	}

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"isarray":24}],22:[function(_dereq_,module,exports){

	/**
	 * Module exports.
	 *
	 * Logic borrowed from Modernizr:
	 *
	 *   - https://github.com/Modernizr/Modernizr/blob/master/feature-detects/cors.js
	 */

	try {
	  module.exports = typeof XMLHttpRequest !== 'undefined' &&
	    'withCredentials' in new XMLHttpRequest();
	} catch (err) {
	  // if XMLHttp support is disabled in IE then it will throw
	  // when trying to create
	  module.exports = false;
	}

	},{}],23:[function(_dereq_,module,exports){

	var indexOf = [].indexOf;

	module.exports = function(arr, obj){
	  if (indexOf) return arr.indexOf(obj);
	  for (var i = 0; i < arr.length; ++i) {
	    if (arr[i] === obj) return i;
	  }
	  return -1;
	};
	},{}],24:[function(_dereq_,module,exports){
	module.exports = Array.isArray || function (arr) {
	  return Object.prototype.toString.call(arr) == '[object Array]';
	};

	},{}],25:[function(_dereq_,module,exports){
	/**
	 * Helpers.
	 */

	var s = 1000;
	var m = s * 60;
	var h = m * 60;
	var d = h * 24;
	var y = d * 365.25;

	/**
	 * Parse or format the given `val`.
	 *
	 * Options:
	 *
	 *  - `long` verbose formatting [false]
	 *
	 * @param {String|Number} val
	 * @param {Object} options
	 * @return {String|Number}
	 * @api public
	 */

	module.exports = function(val, options){
	  options = options || {};
	  if ('string' == typeof val) return parse(val);
	  return options.long
	    ? long(val)
	    : short(val);
	};

	/**
	 * Parse the given `str` and return milliseconds.
	 *
	 * @param {String} str
	 * @return {Number}
	 * @api private
	 */

	function parse(str) {
	  str = '' + str;
	  if (str.length > 10000) return;
	  var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(str);
	  if (!match) return;
	  var n = parseFloat(match[1]);
	  var type = (match[2] || 'ms').toLowerCase();
	  switch (type) {
	    case 'years':
	    case 'year':
	    case 'yrs':
	    case 'yr':
	    case 'y':
	      return n * y;
	    case 'days':
	    case 'day':
	    case 'd':
	      return n * d;
	    case 'hours':
	    case 'hour':
	    case 'hrs':
	    case 'hr':
	    case 'h':
	      return n * h;
	    case 'minutes':
	    case 'minute':
	    case 'mins':
	    case 'min':
	    case 'm':
	      return n * m;
	    case 'seconds':
	    case 'second':
	    case 'secs':
	    case 'sec':
	    case 's':
	      return n * s;
	    case 'milliseconds':
	    case 'millisecond':
	    case 'msecs':
	    case 'msec':
	    case 'ms':
	      return n;
	  }
	}

	/**
	 * Short format for `ms`.
	 *
	 * @param {Number} ms
	 * @return {String}
	 * @api private
	 */

	function short(ms) {
	  if (ms >= d) return Math.round(ms / d) + 'd';
	  if (ms >= h) return Math.round(ms / h) + 'h';
	  if (ms >= m) return Math.round(ms / m) + 'm';
	  if (ms >= s) return Math.round(ms / s) + 's';
	  return ms + 'ms';
	}

	/**
	 * Long format for `ms`.
	 *
	 * @param {Number} ms
	 * @return {String}
	 * @api private
	 */

	function long(ms) {
	  return plural(ms, d, 'day')
	    || plural(ms, h, 'hour')
	    || plural(ms, m, 'minute')
	    || plural(ms, s, 'second')
	    || ms + ' ms';
	}

	/**
	 * Pluralization helper.
	 */

	function plural(ms, n, name) {
	  if (ms < n) return;
	  if (ms < n * 1.5) return Math.floor(ms / n) + ' ' + name;
	  return Math.ceil(ms / n) + ' ' + name + 's';
	}

	},{}],26:[function(_dereq_,module,exports){
	(function (global){
	/**
	 * JSON parse.
	 *
	 * @see Based on jQuery#parseJSON (MIT) and JSON2
	 * @api private
	 */

	var rvalidchars = /^[\],:{}\s]*$/;
	var rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
	var rvalidtokens = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
	var rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g;
	var rtrimLeft = /^\s+/;
	var rtrimRight = /\s+$/;

	module.exports = function parsejson(data) {
	  if ('string' != typeof data || !data) {
	    return null;
	  }

	  data = data.replace(rtrimLeft, '').replace(rtrimRight, '');

	  // Attempt to parse using the native JSON parser first
	  if (global.JSON && JSON.parse) {
	    return JSON.parse(data);
	  }

	  if (rvalidchars.test(data.replace(rvalidescape, '@')
	      .replace(rvalidtokens, ']')
	      .replace(rvalidbraces, ''))) {
	    return (new Function('return ' + data))();
	  }
	};
	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{}],27:[function(_dereq_,module,exports){
	/**
	 * Compiles a querystring
	 * Returns string representation of the object
	 *
	 * @param {Object}
	 * @api private
	 */

	exports.encode = function (obj) {
	  var str = '';

	  for (var i in obj) {
	    if (obj.hasOwnProperty(i)) {
	      if (str.length) str += '&';
	      str += encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]);
	    }
	  }

	  return str;
	};

	/**
	 * Parses a simple querystring into an object
	 *
	 * @param {String} qs
	 * @api private
	 */

	exports.decode = function(qs){
	  var qry = {};
	  var pairs = qs.split('&');
	  for (var i = 0, l = pairs.length; i < l; i++) {
	    var pair = pairs[i].split('=');
	    qry[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
	  }
	  return qry;
	};

	},{}],28:[function(_dereq_,module,exports){
	/**
	 * Parses an URI
	 *
	 * @author Steven Levithan <stevenlevithan.com> (MIT license)
	 * @api private
	 */

	var re = /^(?:(?![^:@]+:[^:@\/]*@)(http|https|ws|wss):\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?((?:[a-f0-9]{0,4}:){2,7}[a-f0-9]{0,4}|[^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/;

	var parts = [
	    'source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'anchor'
	];

	module.exports = function parseuri(str) {
	    var src = str,
	        b = str.indexOf('['),
	        e = str.indexOf(']');

	    if (b != -1 && e != -1) {
	        str = str.substring(0, b) + str.substring(b, e).replace(/:/g, ';') + str.substring(e, str.length);
	    }

	    var m = re.exec(str || ''),
	        uri = {},
	        i = 14;

	    while (i--) {
	        uri[parts[i]] = m[i] || '';
	    }

	    if (b != -1 && e != -1) {
	        uri.source = src;
	        uri.host = uri.host.substring(1, uri.host.length - 1).replace(/;/g, ':');
	        uri.authority = uri.authority.replace('[', '').replace(']', '').replace(/;/g, ':');
	        uri.ipv6uri = true;
	    }

	    return uri;
	};

	},{}],29:[function(_dereq_,module,exports){
	(function (global){
	/*! https://mths.be/utf8js v2.0.0 by @mathias */
	;(function(root) {

		// Detect free variables `exports`
		var freeExports = typeof exports == 'object' && exports;

		// Detect free variable `module`
		var freeModule = typeof module == 'object' && module &&
			module.exports == freeExports && module;

		// Detect free variable `global`, from Node.js or Browserified code,
		// and use it as `root`
		var freeGlobal = typeof global == 'object' && global;
		if (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal) {
			root = freeGlobal;
		}

		/*--------------------------------------------------------------------------*/

		var stringFromCharCode = String.fromCharCode;

		// Taken from https://mths.be/punycode
		function ucs2decode(string) {
			var output = [];
			var counter = 0;
			var length = string.length;
			var value;
			var extra;
			while (counter < length) {
				value = string.charCodeAt(counter++);
				if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
					// high surrogate, and there is a next character
					extra = string.charCodeAt(counter++);
					if ((extra & 0xFC00) == 0xDC00) { // low surrogate
						output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
					} else {
						// unmatched surrogate; only append this code unit, in case the next
						// code unit is the high surrogate of a surrogate pair
						output.push(value);
						counter--;
					}
				} else {
					output.push(value);
				}
			}
			return output;
		}

		// Taken from https://mths.be/punycode
		function ucs2encode(array) {
			var length = array.length;
			var index = -1;
			var value;
			var output = '';
			while (++index < length) {
				value = array[index];
				if (value > 0xFFFF) {
					value -= 0x10000;
					output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
					value = 0xDC00 | value & 0x3FF;
				}
				output += stringFromCharCode(value);
			}
			return output;
		}

		function checkScalarValue(codePoint) {
			if (codePoint >= 0xD800 && codePoint <= 0xDFFF) {
				throw Error(
					'Lone surrogate U+' + codePoint.toString(16).toUpperCase() +
					' is not a scalar value'
				);
			}
		}
		/*--------------------------------------------------------------------------*/

		function createByte(codePoint, shift) {
			return stringFromCharCode(((codePoint >> shift) & 0x3F) | 0x80);
		}

		function encodeCodePoint(codePoint) {
			if ((codePoint & 0xFFFFFF80) == 0) { // 1-byte sequence
				return stringFromCharCode(codePoint);
			}
			var symbol = '';
			if ((codePoint & 0xFFFFF800) == 0) { // 2-byte sequence
				symbol = stringFromCharCode(((codePoint >> 6) & 0x1F) | 0xC0);
			}
			else if ((codePoint & 0xFFFF0000) == 0) { // 3-byte sequence
				checkScalarValue(codePoint);
				symbol = stringFromCharCode(((codePoint >> 12) & 0x0F) | 0xE0);
				symbol += createByte(codePoint, 6);
			}
			else if ((codePoint & 0xFFE00000) == 0) { // 4-byte sequence
				symbol = stringFromCharCode(((codePoint >> 18) & 0x07) | 0xF0);
				symbol += createByte(codePoint, 12);
				symbol += createByte(codePoint, 6);
			}
			symbol += stringFromCharCode((codePoint & 0x3F) | 0x80);
			return symbol;
		}

		function utf8encode(string) {
			var codePoints = ucs2decode(string);
			var length = codePoints.length;
			var index = -1;
			var codePoint;
			var byteString = '';
			while (++index < length) {
				codePoint = codePoints[index];
				byteString += encodeCodePoint(codePoint);
			}
			return byteString;
		}

		/*--------------------------------------------------------------------------*/

		function readContinuationByte() {
			if (byteIndex >= byteCount) {
				throw Error('Invalid byte index');
			}

			var continuationByte = byteArray[byteIndex] & 0xFF;
			byteIndex++;

			if ((continuationByte & 0xC0) == 0x80) {
				return continuationByte & 0x3F;
			}

			// If we end up here, it’s not a continuation byte
			throw Error('Invalid continuation byte');
		}

		function decodeSymbol() {
			var byte1;
			var byte2;
			var byte3;
			var byte4;
			var codePoint;

			if (byteIndex > byteCount) {
				throw Error('Invalid byte index');
			}

			if (byteIndex == byteCount) {
				return false;
			}

			// Read first byte
			byte1 = byteArray[byteIndex] & 0xFF;
			byteIndex++;

			// 1-byte sequence (no continuation bytes)
			if ((byte1 & 0x80) == 0) {
				return byte1;
			}

			// 2-byte sequence
			if ((byte1 & 0xE0) == 0xC0) {
				var byte2 = readContinuationByte();
				codePoint = ((byte1 & 0x1F) << 6) | byte2;
				if (codePoint >= 0x80) {
					return codePoint;
				} else {
					throw Error('Invalid continuation byte');
				}
			}

			// 3-byte sequence (may include unpaired surrogates)
			if ((byte1 & 0xF0) == 0xE0) {
				byte2 = readContinuationByte();
				byte3 = readContinuationByte();
				codePoint = ((byte1 & 0x0F) << 12) | (byte2 << 6) | byte3;
				if (codePoint >= 0x0800) {
					checkScalarValue(codePoint);
					return codePoint;
				} else {
					throw Error('Invalid continuation byte');
				}
			}

			// 4-byte sequence
			if ((byte1 & 0xF8) == 0xF0) {
				byte2 = readContinuationByte();
				byte3 = readContinuationByte();
				byte4 = readContinuationByte();
				codePoint = ((byte1 & 0x0F) << 0x12) | (byte2 << 0x0C) |
					(byte3 << 0x06) | byte4;
				if (codePoint >= 0x010000 && codePoint <= 0x10FFFF) {
					return codePoint;
				}
			}

			throw Error('Invalid UTF-8 detected');
		}

		var byteArray;
		var byteCount;
		var byteIndex;
		function utf8decode(byteString) {
			byteArray = ucs2decode(byteString);
			byteCount = byteArray.length;
			byteIndex = 0;
			var codePoints = [];
			var tmp;
			while ((tmp = decodeSymbol()) !== false) {
				codePoints.push(tmp);
			}
			return ucs2encode(codePoints);
		}

		/*--------------------------------------------------------------------------*/

		var utf8 = {
			'version': '2.0.0',
			'encode': utf8encode,
			'decode': utf8decode
		};

		// Some AMD build optimizers, like r.js, check for specific condition patterns
		// like the following:
		if (
			typeof define == 'function' &&
			typeof define.amd == 'object' &&
			define.amd
		) {
			define(function() {
				return utf8;
			});
		}	else if (freeExports && !freeExports.nodeType) {
			if (freeModule) { // in Node.js or RingoJS v0.8.0+
				freeModule.exports = utf8;
			} else { // in Narwhal or RingoJS v0.7.0-
				var object = {};
				var hasOwnProperty = object.hasOwnProperty;
				for (var key in utf8) {
					hasOwnProperty.call(utf8, key) && (freeExports[key] = utf8[key]);
				}
			}
		} else { // in Rhino or a web browser
			root.utf8 = utf8;
		}

	}(this));

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{}],30:[function(_dereq_,module,exports){
	'use strict';

	var alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_'.split('')
	  , length = 64
	  , map = {}
	  , seed = 0
	  , i = 0
	  , prev;

	/**
	 * Return a string representing the specified number.
	 *
	 * @param {Number} num The number to convert.
	 * @returns {String} The string representation of the number.
	 * @api public
	 */
	function encode(num) {
	  var encoded = '';

	  do {
	    encoded = alphabet[num % length] + encoded;
	    num = Math.floor(num / length);
	  } while (num > 0);

	  return encoded;
	}

	/**
	 * Return the integer value specified by the given string.
	 *
	 * @param {String} str The string to convert.
	 * @returns {Number} The integer value represented by the string.
	 * @api public
	 */
	function decode(str) {
	  var decoded = 0;

	  for (i = 0; i < str.length; i++) {
	    decoded = decoded * length + map[str.charAt(i)];
	  }

	  return decoded;
	}

	/**
	 * Yeast: A tiny growing id generator.
	 *
	 * @returns {String} A unique id.
	 * @api public
	 */
	function yeast() {
	  var now = encode(+new Date());

	  if (now !== prev) return seed = 0, prev = now;
	  return now +'.'+ encode(seed++);
	}

	//
	// Map each character to its index.
	//
	for (; i < length; i++) map[alphabet[i]] = i;

	//
	// Expose the `yeast`, `encode` and `decode` functions.
	//
	yeast.encode = encode;
	yeast.decode = decode;
	module.exports = yeast;

	},{}],31:[function(_dereq_,module,exports){

	/**
	 * Module dependencies.
	 */

	var url = _dereq_('./url');
	var parser = _dereq_('socket.io-parser');
	var Manager = _dereq_('./manager');
	var debug = _dereq_('debug')('socket.io-client');

	/**
	 * Module exports.
	 */

	module.exports = exports = lookup;

	/**
	 * Managers cache.
	 */

	var cache = exports.managers = {};

	/**
	 * Looks up an existing `Manager` for multiplexing.
	 * If the user summons:
	 *
	 *   `io('http://localhost/a');`
	 *   `io('http://localhost/b');`
	 *
	 * We reuse the existing instance based on same scheme/port/host,
	 * and we initialize sockets for each namespace.
	 *
	 * @api public
	 */

	function lookup(uri, opts) {
	  if (typeof uri == 'object') {
	    opts = uri;
	    uri = undefined;
	  }

	  opts = opts || {};

	  var parsed = url(uri);
	  var source = parsed.source;
	  var id = parsed.id;
	  var path = parsed.path;
	  var sameNamespace = cache[id] && path in cache[id].nsps;
	  var newConnection = opts.forceNew || opts['force new connection'] ||
	                      false === opts.multiplex || sameNamespace;

	  var io;

	  if (newConnection) {
	    debug('ignoring socket cache for %s', source);
	    io = Manager(source, opts);
	  } else {
	    if (!cache[id]) {
	      debug('new io instance for %s', source);
	      cache[id] = Manager(source, opts);
	    }
	    io = cache[id];
	  }

	  return io.socket(parsed.path);
	}

	/**
	 * Protocol version.
	 *
	 * @api public
	 */

	exports.protocol = parser.protocol;

	/**
	 * `connect`.
	 *
	 * @param {String} uri
	 * @api public
	 */

	exports.connect = lookup;

	/**
	 * Expose constructors for standalone build.
	 *
	 * @api public
	 */

	exports.Manager = _dereq_('./manager');
	exports.Socket = _dereq_('./socket');

	},{"./manager":32,"./socket":34,"./url":35,"debug":39,"socket.io-parser":47}],32:[function(_dereq_,module,exports){

	/**
	 * Module dependencies.
	 */

	var eio = _dereq_('engine.io-client');
	var Socket = _dereq_('./socket');
	var Emitter = _dereq_('component-emitter');
	var parser = _dereq_('socket.io-parser');
	var on = _dereq_('./on');
	var bind = _dereq_('component-bind');
	var debug = _dereq_('debug')('socket.io-client:manager');
	var indexOf = _dereq_('indexof');
	var Backoff = _dereq_('backo2');

	/**
	 * IE6+ hasOwnProperty
	 */

	var has = Object.prototype.hasOwnProperty;

	/**
	 * Module exports
	 */

	module.exports = Manager;

	/**
	 * `Manager` constructor.
	 *
	 * @param {String} engine instance or engine uri/opts
	 * @param {Object} options
	 * @api public
	 */

	function Manager(uri, opts){
	  if (!(this instanceof Manager)) return new Manager(uri, opts);
	  if (uri && ('object' == typeof uri)) {
	    opts = uri;
	    uri = undefined;
	  }
	  opts = opts || {};

	  opts.path = opts.path || '/socket.io';
	  this.nsps = {};
	  this.subs = [];
	  this.opts = opts;
	  this.reconnection(opts.reconnection !== false);
	  this.reconnectionAttempts(opts.reconnectionAttempts || Infinity);
	  this.reconnectionDelay(opts.reconnectionDelay || 1000);
	  this.reconnectionDelayMax(opts.reconnectionDelayMax || 5000);
	  this.randomizationFactor(opts.randomizationFactor || 0.5);
	  this.backoff = new Backoff({
	    min: this.reconnectionDelay(),
	    max: this.reconnectionDelayMax(),
	    jitter: this.randomizationFactor()
	  });
	  this.timeout(null == opts.timeout ? 20000 : opts.timeout);
	  this.readyState = 'closed';
	  this.uri = uri;
	  this.connecting = [];
	  this.lastPing = null;
	  this.encoding = false;
	  this.packetBuffer = [];
	  this.encoder = new parser.Encoder();
	  this.decoder = new parser.Decoder();
	  this.autoConnect = opts.autoConnect !== false;
	  if (this.autoConnect) this.open();
	}

	/**
	 * Propagate given event to sockets and emit on `this`
	 *
	 * @api private
	 */

	Manager.prototype.emitAll = function() {
	  this.emit.apply(this, arguments);
	  for (var nsp in this.nsps) {
	    if (has.call(this.nsps, nsp)) {
	      this.nsps[nsp].emit.apply(this.nsps[nsp], arguments);
	    }
	  }
	};

	/**
	 * Update `socket.id` of all sockets
	 *
	 * @api private
	 */

	Manager.prototype.updateSocketIds = function(){
	  for (var nsp in this.nsps) {
	    if (has.call(this.nsps, nsp)) {
	      this.nsps[nsp].id = this.engine.id;
	    }
	  }
	};

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Manager.prototype);

	/**
	 * Sets the `reconnection` config.
	 *
	 * @param {Boolean} true/false if it should automatically reconnect
	 * @return {Manager} self or value
	 * @api public
	 */

	Manager.prototype.reconnection = function(v){
	  if (!arguments.length) return this._reconnection;
	  this._reconnection = !!v;
	  return this;
	};

	/**
	 * Sets the reconnection attempts config.
	 *
	 * @param {Number} max reconnection attempts before giving up
	 * @return {Manager} self or value
	 * @api public
	 */

	Manager.prototype.reconnectionAttempts = function(v){
	  if (!arguments.length) return this._reconnectionAttempts;
	  this._reconnectionAttempts = v;
	  return this;
	};

	/**
	 * Sets the delay between reconnections.
	 *
	 * @param {Number} delay
	 * @return {Manager} self or value
	 * @api public
	 */

	Manager.prototype.reconnectionDelay = function(v){
	  if (!arguments.length) return this._reconnectionDelay;
	  this._reconnectionDelay = v;
	  this.backoff && this.backoff.setMin(v);
	  return this;
	};

	Manager.prototype.randomizationFactor = function(v){
	  if (!arguments.length) return this._randomizationFactor;
	  this._randomizationFactor = v;
	  this.backoff && this.backoff.setJitter(v);
	  return this;
	};

	/**
	 * Sets the maximum delay between reconnections.
	 *
	 * @param {Number} delay
	 * @return {Manager} self or value
	 * @api public
	 */

	Manager.prototype.reconnectionDelayMax = function(v){
	  if (!arguments.length) return this._reconnectionDelayMax;
	  this._reconnectionDelayMax = v;
	  this.backoff && this.backoff.setMax(v);
	  return this;
	};

	/**
	 * Sets the connection timeout. `false` to disable
	 *
	 * @return {Manager} self or value
	 * @api public
	 */

	Manager.prototype.timeout = function(v){
	  if (!arguments.length) return this._timeout;
	  this._timeout = v;
	  return this;
	};

	/**
	 * Starts trying to reconnect if reconnection is enabled and we have not
	 * started reconnecting yet
	 *
	 * @api private
	 */

	Manager.prototype.maybeReconnectOnOpen = function() {
	  // Only try to reconnect if it's the first time we're connecting
	  if (!this.reconnecting && this._reconnection && this.backoff.attempts === 0) {
	    // keeps reconnection from firing twice for the same reconnection loop
	    this.reconnect();
	  }
	};


	/**
	 * Sets the current transport `socket`.
	 *
	 * @param {Function} optional, callback
	 * @return {Manager} self
	 * @api public
	 */

	Manager.prototype.open =
	Manager.prototype.connect = function(fn){
	  debug('readyState %s', this.readyState);
	  if (~this.readyState.indexOf('open')) return this;

	  debug('opening %s', this.uri);
	  this.engine = eio(this.uri, this.opts);
	  var socket = this.engine;
	  var self = this;
	  this.readyState = 'opening';
	  this.skipReconnect = false;

	  // emit `open`
	  var openSub = on(socket, 'open', function() {
	    self.onopen();
	    fn && fn();
	  });

	  // emit `connect_error`
	  var errorSub = on(socket, 'error', function(data){
	    debug('connect_error');
	    self.cleanup();
	    self.readyState = 'closed';
	    self.emitAll('connect_error', data);
	    if (fn) {
	      var err = new Error('Connection error');
	      err.data = data;
	      fn(err);
	    } else {
	      // Only do this if there is no fn to handle the error
	      self.maybeReconnectOnOpen();
	    }
	  });

	  // emit `connect_timeout`
	  if (false !== this._timeout) {
	    var timeout = this._timeout;
	    debug('connect attempt will timeout after %d', timeout);

	    // set timer
	    var timer = setTimeout(function(){
	      debug('connect attempt timed out after %d', timeout);
	      openSub.destroy();
	      socket.close();
	      socket.emit('error', 'timeout');
	      self.emitAll('connect_timeout', timeout);
	    }, timeout);

	    this.subs.push({
	      destroy: function(){
	        clearTimeout(timer);
	      }
	    });
	  }

	  this.subs.push(openSub);
	  this.subs.push(errorSub);

	  return this;
	};

	/**
	 * Called upon transport open.
	 *
	 * @api private
	 */

	Manager.prototype.onopen = function(){
	  debug('open');

	  // clear old subs
	  this.cleanup();

	  // mark as open
	  this.readyState = 'open';
	  this.emit('open');

	  // add new subs
	  var socket = this.engine;
	  this.subs.push(on(socket, 'data', bind(this, 'ondata')));
	  this.subs.push(on(socket, 'ping', bind(this, 'onping')));
	  this.subs.push(on(socket, 'pong', bind(this, 'onpong')));
	  this.subs.push(on(socket, 'error', bind(this, 'onerror')));
	  this.subs.push(on(socket, 'close', bind(this, 'onclose')));
	  this.subs.push(on(this.decoder, 'decoded', bind(this, 'ondecoded')));
	};

	/**
	 * Called upon a ping.
	 *
	 * @api private
	 */

	Manager.prototype.onping = function(){
	  this.lastPing = new Date;
	  this.emitAll('ping');
	};

	/**
	 * Called upon a packet.
	 *
	 * @api private
	 */

	Manager.prototype.onpong = function(){
	  this.emitAll('pong', new Date - this.lastPing);
	};

	/**
	 * Called with data.
	 *
	 * @api private
	 */

	Manager.prototype.ondata = function(data){
	  this.decoder.add(data);
	};

	/**
	 * Called when parser fully decodes a packet.
	 *
	 * @api private
	 */

	Manager.prototype.ondecoded = function(packet) {
	  this.emit('packet', packet);
	};

	/**
	 * Called upon socket error.
	 *
	 * @api private
	 */

	Manager.prototype.onerror = function(err){
	  debug('error', err);
	  this.emitAll('error', err);
	};

	/**
	 * Creates a new socket for the given `nsp`.
	 *
	 * @return {Socket}
	 * @api public
	 */

	Manager.prototype.socket = function(nsp){
	  var socket = this.nsps[nsp];
	  if (!socket) {
	    socket = new Socket(this, nsp);
	    this.nsps[nsp] = socket;
	    var self = this;
	    socket.on('connecting', onConnecting);
	    socket.on('connect', function(){
	      socket.id = self.engine.id;
	    });

	    if (this.autoConnect) {
	      // manually call here since connecting evnet is fired before listening
	      onConnecting();
	    }
	  }

	  function onConnecting() {
	    if (!~indexOf(self.connecting, socket)) {
	      self.connecting.push(socket);
	    }
	  }

	  return socket;
	};

	/**
	 * Called upon a socket close.
	 *
	 * @param {Socket} socket
	 */

	Manager.prototype.destroy = function(socket){
	  var index = indexOf(this.connecting, socket);
	  if (~index) this.connecting.splice(index, 1);
	  if (this.connecting.length) return;

	  this.close();
	};

	/**
	 * Writes a packet.
	 *
	 * @param {Object} packet
	 * @api private
	 */

	Manager.prototype.packet = function(packet){
	  debug('writing packet %j', packet);
	  var self = this;

	  if (!self.encoding) {
	    // encode, then write to engine with result
	    self.encoding = true;
	    this.encoder.encode(packet, function(encodedPackets) {
	      for (var i = 0; i < encodedPackets.length; i++) {
	        self.engine.write(encodedPackets[i], packet.options);
	      }
	      self.encoding = false;
	      self.processPacketQueue();
	    });
	  } else { // add packet to the queue
	    self.packetBuffer.push(packet);
	  }
	};

	/**
	 * If packet buffer is non-empty, begins encoding the
	 * next packet in line.
	 *
	 * @api private
	 */

	Manager.prototype.processPacketQueue = function() {
	  if (this.packetBuffer.length > 0 && !this.encoding) {
	    var pack = this.packetBuffer.shift();
	    this.packet(pack);
	  }
	};

	/**
	 * Clean up transport subscriptions and packet buffer.
	 *
	 * @api private
	 */

	Manager.prototype.cleanup = function(){
	  debug('cleanup');

	  var sub;
	  while (sub = this.subs.shift()) sub.destroy();

	  this.packetBuffer = [];
	  this.encoding = false;
	  this.lastPing = null;

	  this.decoder.destroy();
	};

	/**
	 * Close the current socket.
	 *
	 * @api private
	 */

	Manager.prototype.close =
	Manager.prototype.disconnect = function(){
	  debug('disconnect');
	  this.skipReconnect = true;
	  this.reconnecting = false;
	  if ('opening' == this.readyState) {
	    // `onclose` will not fire because
	    // an open event never happened
	    this.cleanup();
	  }
	  this.backoff.reset();
	  this.readyState = 'closed';
	  if (this.engine) this.engine.close();
	};

	/**
	 * Called upon engine close.
	 *
	 * @api private
	 */

	Manager.prototype.onclose = function(reason){
	  debug('onclose');

	  this.cleanup();
	  this.backoff.reset();
	  this.readyState = 'closed';
	  this.emit('close', reason);

	  if (this._reconnection && !this.skipReconnect) {
	    this.reconnect();
	  }
	};

	/**
	 * Attempt a reconnection.
	 *
	 * @api private
	 */

	Manager.prototype.reconnect = function(){
	  if (this.reconnecting || this.skipReconnect) return this;

	  var self = this;

	  if (this.backoff.attempts >= this._reconnectionAttempts) {
	    debug('reconnect failed');
	    this.backoff.reset();
	    this.emitAll('reconnect_failed');
	    this.reconnecting = false;
	  } else {
	    var delay = this.backoff.duration();
	    debug('will wait %dms before reconnect attempt', delay);

	    this.reconnecting = true;
	    var timer = setTimeout(function(){
	      if (self.skipReconnect) return;

	      debug('attempting reconnect');
	      self.emitAll('reconnect_attempt', self.backoff.attempts);
	      self.emitAll('reconnecting', self.backoff.attempts);

	      // check again for the case socket closed in above events
	      if (self.skipReconnect) return;

	      self.open(function(err){
	        if (err) {
	          debug('reconnect attempt error');
	          self.reconnecting = false;
	          self.reconnect();
	          self.emitAll('reconnect_error', err.data);
	        } else {
	          debug('reconnect success');
	          self.onreconnect();
	        }
	      });
	    }, delay);

	    this.subs.push({
	      destroy: function(){
	        clearTimeout(timer);
	      }
	    });
	  }
	};

	/**
	 * Called upon successful reconnect.
	 *
	 * @api private
	 */

	Manager.prototype.onreconnect = function(){
	  var attempt = this.backoff.attempts;
	  this.reconnecting = false;
	  this.backoff.reset();
	  this.updateSocketIds();
	  this.emitAll('reconnect', attempt);
	};

	},{"./on":33,"./socket":34,"backo2":36,"component-bind":37,"component-emitter":38,"debug":39,"engine.io-client":1,"indexof":42,"socket.io-parser":47}],33:[function(_dereq_,module,exports){

	/**
	 * Module exports.
	 */

	module.exports = on;

	/**
	 * Helper for subscriptions.
	 *
	 * @param {Object|EventEmitter} obj with `Emitter` mixin or `EventEmitter`
	 * @param {String} event name
	 * @param {Function} callback
	 * @api public
	 */

	function on(obj, ev, fn) {
	  obj.on(ev, fn);
	  return {
	    destroy: function(){
	      obj.removeListener(ev, fn);
	    }
	  };
	}

	},{}],34:[function(_dereq_,module,exports){

	/**
	 * Module dependencies.
	 */

	var parser = _dereq_('socket.io-parser');
	var Emitter = _dereq_('component-emitter');
	var toArray = _dereq_('to-array');
	var on = _dereq_('./on');
	var bind = _dereq_('component-bind');
	var debug = _dereq_('debug')('socket.io-client:socket');
	var hasBin = _dereq_('has-binary');

	/**
	 * Module exports.
	 */

	module.exports = exports = Socket;

	/**
	 * Internal events (blacklisted).
	 * These events can't be emitted by the user.
	 *
	 * @api private
	 */

	var events = {
	  connect: 1,
	  connect_error: 1,
	  connect_timeout: 1,
	  connecting: 1,
	  disconnect: 1,
	  error: 1,
	  reconnect: 1,
	  reconnect_attempt: 1,
	  reconnect_failed: 1,
	  reconnect_error: 1,
	  reconnecting: 1,
	  ping: 1,
	  pong: 1
	};

	/**
	 * Shortcut to `Emitter#emit`.
	 */

	var emit = Emitter.prototype.emit;

	/**
	 * `Socket` constructor.
	 *
	 * @api public
	 */

	function Socket(io, nsp){
	  this.io = io;
	  this.nsp = nsp;
	  this.json = this; // compat
	  this.ids = 0;
	  this.acks = {};
	  this.receiveBuffer = [];
	  this.sendBuffer = [];
	  this.connected = false;
	  this.disconnected = true;
	  if (this.io.autoConnect) this.open();
	}

	/**
	 * Mix in `Emitter`.
	 */

	Emitter(Socket.prototype);

	/**
	 * Subscribe to open, close and packet events
	 *
	 * @api private
	 */

	Socket.prototype.subEvents = function() {
	  if (this.subs) return;

	  var io = this.io;
	  this.subs = [
	    on(io, 'open', bind(this, 'onopen')),
	    on(io, 'packet', bind(this, 'onpacket')),
	    on(io, 'close', bind(this, 'onclose'))
	  ];
	};

	/**
	 * "Opens" the socket.
	 *
	 * @api public
	 */

	Socket.prototype.open =
	Socket.prototype.connect = function(){
	  if (this.connected) return this;

	  this.subEvents();
	  this.io.open(); // ensure open
	  if ('open' == this.io.readyState) this.onopen();
	  this.emit('connecting');
	  return this;
	};

	/**
	 * Sends a `message` event.
	 *
	 * @return {Socket} self
	 * @api public
	 */

	Socket.prototype.send = function(){
	  var args = toArray(arguments);
	  args.unshift('message');
	  this.emit.apply(this, args);
	  return this;
	};

	/**
	 * Override `emit`.
	 * If the event is in `events`, it's emitted normally.
	 *
	 * @param {String} event name
	 * @return {Socket} self
	 * @api public
	 */

	Socket.prototype.emit = function(ev){
	  if (events.hasOwnProperty(ev)) {
	    emit.apply(this, arguments);
	    return this;
	  }

	  var args = toArray(arguments);
	  var parserType = parser.EVENT; // default
	  if (hasBin(args)) { parserType = parser.BINARY_EVENT; } // binary
	  var packet = { type: parserType, data: args };

	  packet.options = {};
	  packet.options.compress = !this.flags || false !== this.flags.compress;

	  // event ack callback
	  if ('function' == typeof args[args.length - 1]) {
	    debug('emitting packet with ack id %d', this.ids);
	    this.acks[this.ids] = args.pop();
	    packet.id = this.ids++;
	  }

	  if (this.connected) {
	    this.packet(packet);
	  } else {
	    this.sendBuffer.push(packet);
	  }

	  delete this.flags;

	  return this;
	};

	/**
	 * Sends a packet.
	 *
	 * @param {Object} packet
	 * @api private
	 */

	Socket.prototype.packet = function(packet){
	  packet.nsp = this.nsp;
	  this.io.packet(packet);
	};

	/**
	 * Called upon engine `open`.
	 *
	 * @api private
	 */

	Socket.prototype.onopen = function(){
	  debug('transport is open - connecting');

	  // write connect packet if necessary
	  if ('/' != this.nsp) {
	    this.packet({ type: parser.CONNECT });
	  }
	};

	/**
	 * Called upon engine `close`.
	 *
	 * @param {String} reason
	 * @api private
	 */

	Socket.prototype.onclose = function(reason){
	  debug('close (%s)', reason);
	  this.connected = false;
	  this.disconnected = true;
	  delete this.id;
	  this.emit('disconnect', reason);
	};

	/**
	 * Called with socket packet.
	 *
	 * @param {Object} packet
	 * @api private
	 */

	Socket.prototype.onpacket = function(packet){
	  if (packet.nsp != this.nsp) return;

	  switch (packet.type) {
	    case parser.CONNECT:
	      this.onconnect();
	      break;

	    case parser.EVENT:
	      this.onevent(packet);
	      break;

	    case parser.BINARY_EVENT:
	      this.onevent(packet);
	      break;

	    case parser.ACK:
	      this.onack(packet);
	      break;

	    case parser.BINARY_ACK:
	      this.onack(packet);
	      break;

	    case parser.DISCONNECT:
	      this.ondisconnect();
	      break;

	    case parser.ERROR:
	      this.emit('error', packet.data);
	      break;
	  }
	};

	/**
	 * Called upon a server event.
	 *
	 * @param {Object} packet
	 * @api private
	 */

	Socket.prototype.onevent = function(packet){
	  var args = packet.data || [];
	  debug('emitting event %j', args);

	  if (null != packet.id) {
	    debug('attaching ack callback to event');
	    args.push(this.ack(packet.id));
	  }

	  if (this.connected) {
	    emit.apply(this, args);
	  } else {
	    this.receiveBuffer.push(args);
	  }
	};

	/**
	 * Produces an ack callback to emit with an event.
	 *
	 * @api private
	 */

	Socket.prototype.ack = function(id){
	  var self = this;
	  var sent = false;
	  return function(){
	    // prevent double callbacks
	    if (sent) return;
	    sent = true;
	    var args = toArray(arguments);
	    debug('sending ack %j', args);

	    var type = hasBin(args) ? parser.BINARY_ACK : parser.ACK;
	    self.packet({
	      type: type,
	      id: id,
	      data: args
	    });
	  };
	};

	/**
	 * Called upon a server acknowlegement.
	 *
	 * @param {Object} packet
	 * @api private
	 */

	Socket.prototype.onack = function(packet){
	  var ack = this.acks[packet.id];
	  if ('function' == typeof ack) {
	    debug('calling ack %s with %j', packet.id, packet.data);
	    ack.apply(this, packet.data);
	    delete this.acks[packet.id];
	  } else {
	    debug('bad ack %s', packet.id);
	  }
	};

	/**
	 * Called upon server connect.
	 *
	 * @api private
	 */

	Socket.prototype.onconnect = function(){
	  this.connected = true;
	  this.disconnected = false;
	  this.emit('connect');
	  this.emitBuffered();
	};

	/**
	 * Emit buffered events (received and emitted).
	 *
	 * @api private
	 */

	Socket.prototype.emitBuffered = function(){
	  var i;
	  for (i = 0; i < this.receiveBuffer.length; i++) {
	    emit.apply(this, this.receiveBuffer[i]);
	  }
	  this.receiveBuffer = [];

	  for (i = 0; i < this.sendBuffer.length; i++) {
	    this.packet(this.sendBuffer[i]);
	  }
	  this.sendBuffer = [];
	};

	/**
	 * Called upon server disconnect.
	 *
	 * @api private
	 */

	Socket.prototype.ondisconnect = function(){
	  debug('server disconnect (%s)', this.nsp);
	  this.destroy();
	  this.onclose('io server disconnect');
	};

	/**
	 * Called upon forced client/server side disconnections,
	 * this method ensures the manager stops tracking us and
	 * that reconnections don't get triggered for this.
	 *
	 * @api private.
	 */

	Socket.prototype.destroy = function(){
	  if (this.subs) {
	    // clean subscriptions to avoid reconnections
	    for (var i = 0; i < this.subs.length; i++) {
	      this.subs[i].destroy();
	    }
	    this.subs = null;
	  }

	  this.io.destroy(this);
	};

	/**
	 * Disconnects the socket manually.
	 *
	 * @return {Socket} self
	 * @api public
	 */

	Socket.prototype.close =
	Socket.prototype.disconnect = function(){
	  if (this.connected) {
	    debug('performing disconnect (%s)', this.nsp);
	    this.packet({ type: parser.DISCONNECT });
	  }

	  // remove socket from pool
	  this.destroy();

	  if (this.connected) {
	    // fire events
	    this.onclose('io client disconnect');
	  }
	  return this;
	};

	/**
	 * Sets the compress flag.
	 *
	 * @param {Boolean} if `true`, compresses the sending data
	 * @return {Socket} self
	 * @api public
	 */

	Socket.prototype.compress = function(compress){
	  this.flags = this.flags || {};
	  this.flags.compress = compress;
	  return this;
	};

	},{"./on":33,"component-bind":37,"component-emitter":38,"debug":39,"has-binary":41,"socket.io-parser":47,"to-array":51}],35:[function(_dereq_,module,exports){
	(function (global){

	/**
	 * Module dependencies.
	 */

	var parseuri = _dereq_('parseuri');
	var debug = _dereq_('debug')('socket.io-client:url');

	/**
	 * Module exports.
	 */

	module.exports = url;

	/**
	 * URL parser.
	 *
	 * @param {String} url
	 * @param {Object} An object meant to mimic window.location.
	 *                 Defaults to window.location.
	 * @api public
	 */

	function url(uri, loc){
	  var obj = uri;

	  // default to window.location
	  var loc = loc || global.location;
	  if (null == uri) uri = loc.protocol + '//' + loc.host;

	  // relative path support
	  if ('string' == typeof uri) {
	    if ('/' == uri.charAt(0)) {
	      if ('/' == uri.charAt(1)) {
	        uri = loc.protocol + uri;
	      } else {
	        uri = loc.host + uri;
	      }
	    }

	    if (!/^(https?|wss?):\/\//.test(uri)) {
	      debug('protocol-less url %s', uri);
	      if ('undefined' != typeof loc) {
	        uri = loc.protocol + '//' + uri;
	      } else {
	        uri = 'https://' + uri;
	      }
	    }

	    // parse
	    debug('parse %s', uri);
	    obj = parseuri(uri);
	  }

	  // make sure we treat `localhost:80` and `localhost` equally
	  if (!obj.port) {
	    if (/^(http|ws)$/.test(obj.protocol)) {
	      obj.port = '80';
	    }
	    else if (/^(http|ws)s$/.test(obj.protocol)) {
	      obj.port = '443';
	    }
	  }

	  obj.path = obj.path || '/';

	  var ipv6 = obj.host.indexOf(':') !== -1;
	  var host = ipv6 ? '[' + obj.host + ']' : obj.host;

	  // define unique id
	  obj.id = obj.protocol + '://' + host + ':' + obj.port;
	  // define href
	  obj.href = obj.protocol + '://' + host + (loc && loc.port == obj.port ? '' : (':' + obj.port));

	  return obj;
	}

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"debug":39,"parseuri":45}],36:[function(_dereq_,module,exports){

	/**
	 * Expose `Backoff`.
	 */

	module.exports = Backoff;

	/**
	 * Initialize backoff timer with `opts`.
	 *
	 * - `min` initial timeout in milliseconds [100]
	 * - `max` max timeout [10000]
	 * - `jitter` [0]
	 * - `factor` [2]
	 *
	 * @param {Object} opts
	 * @api public
	 */

	function Backoff(opts) {
	  opts = opts || {};
	  this.ms = opts.min || 100;
	  this.max = opts.max || 10000;
	  this.factor = opts.factor || 2;
	  this.jitter = opts.jitter > 0 && opts.jitter <= 1 ? opts.jitter : 0;
	  this.attempts = 0;
	}

	/**
	 * Return the backoff duration.
	 *
	 * @return {Number}
	 * @api public
	 */

	Backoff.prototype.duration = function(){
	  var ms = this.ms * Math.pow(this.factor, this.attempts++);
	  if (this.jitter) {
	    var rand =  Math.random();
	    var deviation = Math.floor(rand * this.jitter * ms);
	    ms = (Math.floor(rand * 10) & 1) == 0  ? ms - deviation : ms + deviation;
	  }
	  return Math.min(ms, this.max) | 0;
	};

	/**
	 * Reset the number of attempts.
	 *
	 * @api public
	 */

	Backoff.prototype.reset = function(){
	  this.attempts = 0;
	};

	/**
	 * Set the minimum duration
	 *
	 * @api public
	 */

	Backoff.prototype.setMin = function(min){
	  this.ms = min;
	};

	/**
	 * Set the maximum duration
	 *
	 * @api public
	 */

	Backoff.prototype.setMax = function(max){
	  this.max = max;
	};

	/**
	 * Set the jitter
	 *
	 * @api public
	 */

	Backoff.prototype.setJitter = function(jitter){
	  this.jitter = jitter;
	};


	},{}],37:[function(_dereq_,module,exports){
	/**
	 * Slice reference.
	 */

	var slice = [].slice;

	/**
	 * Bind `obj` to `fn`.
	 *
	 * @param {Object} obj
	 * @param {Function|String} fn or string
	 * @return {Function}
	 * @api public
	 */

	module.exports = function(obj, fn){
	  if ('string' == typeof fn) fn = obj[fn];
	  if ('function' != typeof fn) throw new Error('bind() requires a function');
	  var args = slice.call(arguments, 2);
	  return function(){
	    return fn.apply(obj, args.concat(slice.call(arguments)));
	  }
	};

	},{}],38:[function(_dereq_,module,exports){

	/**
	 * Expose `Emitter`.
	 */

	module.exports = Emitter;

	/**
	 * Initialize a new `Emitter`.
	 *
	 * @api public
	 */

	function Emitter(obj) {
	  if (obj) return mixin(obj);
	};

	/**
	 * Mixin the emitter properties.
	 *
	 * @param {Object} obj
	 * @return {Object}
	 * @api private
	 */

	function mixin(obj) {
	  for (var key in Emitter.prototype) {
	    obj[key] = Emitter.prototype[key];
	  }
	  return obj;
	}

	/**
	 * Listen on the given `event` with `fn`.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.on =
	Emitter.prototype.addEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};
	  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
	    .push(fn);
	  return this;
	};

	/**
	 * Adds an `event` listener that will be invoked a single
	 * time then automatically removed.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.once = function(event, fn){
	  function on() {
	    this.off(event, on);
	    fn.apply(this, arguments);
	  }

	  on.fn = fn;
	  this.on(event, on);
	  return this;
	};

	/**
	 * Remove the given callback for `event` or all
	 * registered callbacks.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.off =
	Emitter.prototype.removeListener =
	Emitter.prototype.removeAllListeners =
	Emitter.prototype.removeEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};

	  // all
	  if (0 == arguments.length) {
	    this._callbacks = {};
	    return this;
	  }

	  // specific event
	  var callbacks = this._callbacks['$' + event];
	  if (!callbacks) return this;

	  // remove all handlers
	  if (1 == arguments.length) {
	    delete this._callbacks['$' + event];
	    return this;
	  }

	  // remove specific handler
	  var cb;
	  for (var i = 0; i < callbacks.length; i++) {
	    cb = callbacks[i];
	    if (cb === fn || cb.fn === fn) {
	      callbacks.splice(i, 1);
	      break;
	    }
	  }
	  return this;
	};

	/**
	 * Emit `event` with the given args.
	 *
	 * @param {String} event
	 * @param {Mixed} ...
	 * @return {Emitter}
	 */

	Emitter.prototype.emit = function(event){
	  this._callbacks = this._callbacks || {};
	  var args = [].slice.call(arguments, 1)
	    , callbacks = this._callbacks['$' + event];

	  if (callbacks) {
	    callbacks = callbacks.slice(0);
	    for (var i = 0, len = callbacks.length; i < len; ++i) {
	      callbacks[i].apply(this, args);
	    }
	  }

	  return this;
	};

	/**
	 * Return array of callbacks for `event`.
	 *
	 * @param {String} event
	 * @return {Array}
	 * @api public
	 */

	Emitter.prototype.listeners = function(event){
	  this._callbacks = this._callbacks || {};
	  return this._callbacks['$' + event] || [];
	};

	/**
	 * Check if this emitter has `event` handlers.
	 *
	 * @param {String} event
	 * @return {Boolean}
	 * @api public
	 */

	Emitter.prototype.hasListeners = function(event){
	  return !! this.listeners(event).length;
	};

	},{}],39:[function(_dereq_,module,exports){
	arguments[4][17][0].apply(exports,arguments)
	},{"./debug":40,"dup":17}],40:[function(_dereq_,module,exports){
	arguments[4][18][0].apply(exports,arguments)
	},{"dup":18,"ms":44}],41:[function(_dereq_,module,exports){
	(function (global){

	/*
	 * Module requirements.
	 */

	var isArray = _dereq_('isarray');

	/**
	 * Module exports.
	 */

	module.exports = hasBinary;

	/**
	 * Checks for binary data.
	 *
	 * Right now only Buffer and ArrayBuffer are supported..
	 *
	 * @param {Object} anything
	 * @api public
	 */

	function hasBinary(data) {

	  function _hasBinary(obj) {
	    if (!obj) return false;

	    if ( (global.Buffer && global.Buffer.isBuffer && global.Buffer.isBuffer(obj)) ||
	         (global.ArrayBuffer && obj instanceof ArrayBuffer) ||
	         (global.Blob && obj instanceof Blob) ||
	         (global.File && obj instanceof File)
	        ) {
	      return true;
	    }

	    if (isArray(obj)) {
	      for (var i = 0; i < obj.length; i++) {
	          if (_hasBinary(obj[i])) {
	              return true;
	          }
	      }
	    } else if (obj && 'object' == typeof obj) {
	      // see: https://github.com/Automattic/has-binary/pull/4
	      if (obj.toJSON && 'function' == typeof obj.toJSON) {
	        obj = obj.toJSON();
	      }

	      for (var key in obj) {
	        if (Object.prototype.hasOwnProperty.call(obj, key) && _hasBinary(obj[key])) {
	          return true;
	        }
	      }
	    }

	    return false;
	  }

	  return _hasBinary(data);
	}

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"isarray":43}],42:[function(_dereq_,module,exports){
	arguments[4][23][0].apply(exports,arguments)
	},{"dup":23}],43:[function(_dereq_,module,exports){
	arguments[4][24][0].apply(exports,arguments)
	},{"dup":24}],44:[function(_dereq_,module,exports){
	arguments[4][25][0].apply(exports,arguments)
	},{"dup":25}],45:[function(_dereq_,module,exports){
	arguments[4][28][0].apply(exports,arguments)
	},{"dup":28}],46:[function(_dereq_,module,exports){
	(function (global){
	/*global Blob,File*/

	/**
	 * Module requirements
	 */

	var isArray = _dereq_('isarray');
	var isBuf = _dereq_('./is-buffer');

	/**
	 * Replaces every Buffer | ArrayBuffer in packet with a numbered placeholder.
	 * Anything with blobs or files should be fed through removeBlobs before coming
	 * here.
	 *
	 * @param {Object} packet - socket.io event packet
	 * @return {Object} with deconstructed packet and list of buffers
	 * @api public
	 */

	exports.deconstructPacket = function(packet){
	  var buffers = [];
	  var packetData = packet.data;

	  function _deconstructPacket(data) {
	    if (!data) return data;

	    if (isBuf(data)) {
	      var placeholder = { _placeholder: true, num: buffers.length };
	      buffers.push(data);
	      return placeholder;
	    } else if (isArray(data)) {
	      var newData = new Array(data.length);
	      for (var i = 0; i < data.length; i++) {
	        newData[i] = _deconstructPacket(data[i]);
	      }
	      return newData;
	    } else if ('object' == typeof data && !(data instanceof Date)) {
	      var newData = {};
	      for (var key in data) {
	        newData[key] = _deconstructPacket(data[key]);
	      }
	      return newData;
	    }
	    return data;
	  }

	  var pack = packet;
	  pack.data = _deconstructPacket(packetData);
	  pack.attachments = buffers.length; // number of binary 'attachments'
	  return {packet: pack, buffers: buffers};
	};

	/**
	 * Reconstructs a binary packet from its placeholder packet and buffers
	 *
	 * @param {Object} packet - event packet with placeholders
	 * @param {Array} buffers - binary buffers to put in placeholder positions
	 * @return {Object} reconstructed packet
	 * @api public
	 */

	exports.reconstructPacket = function(packet, buffers) {
	  var curPlaceHolder = 0;

	  function _reconstructPacket(data) {
	    if (data && data._placeholder) {
	      var buf = buffers[data.num]; // appropriate buffer (should be natural order anyway)
	      return buf;
	    } else if (isArray(data)) {
	      for (var i = 0; i < data.length; i++) {
	        data[i] = _reconstructPacket(data[i]);
	      }
	      return data;
	    } else if (data && 'object' == typeof data) {
	      for (var key in data) {
	        data[key] = _reconstructPacket(data[key]);
	      }
	      return data;
	    }
	    return data;
	  }

	  packet.data = _reconstructPacket(packet.data);
	  packet.attachments = undefined; // no longer useful
	  return packet;
	};

	/**
	 * Asynchronously removes Blobs or Files from data via
	 * FileReader's readAsArrayBuffer method. Used before encoding
	 * data as msgpack. Calls callback with the blobless data.
	 *
	 * @param {Object} data
	 * @param {Function} callback
	 * @api private
	 */

	exports.removeBlobs = function(data, callback) {
	  function _removeBlobs(obj, curKey, containingObject) {
	    if (!obj) return obj;

	    // convert any blob
	    if ((global.Blob && obj instanceof Blob) ||
	        (global.File && obj instanceof File)) {
	      pendingBlobs++;

	      // async filereader
	      var fileReader = new FileReader();
	      fileReader.onload = function() { // this.result == arraybuffer
	        if (containingObject) {
	          containingObject[curKey] = this.result;
	        }
	        else {
	          bloblessData = this.result;
	        }

	        // if nothing pending its callback time
	        if(! --pendingBlobs) {
	          callback(bloblessData);
	        }
	      };

	      fileReader.readAsArrayBuffer(obj); // blob -> arraybuffer
	    } else if (isArray(obj)) { // handle array
	      for (var i = 0; i < obj.length; i++) {
	        _removeBlobs(obj[i], i, obj);
	      }
	    } else if (obj && 'object' == typeof obj && !isBuf(obj)) { // and object
	      for (var key in obj) {
	        _removeBlobs(obj[key], key, obj);
	      }
	    }
	  }

	  var pendingBlobs = 0;
	  var bloblessData = data;
	  _removeBlobs(bloblessData);
	  if (!pendingBlobs) {
	    callback(bloblessData);
	  }
	};

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{"./is-buffer":48,"isarray":43}],47:[function(_dereq_,module,exports){

	/**
	 * Module dependencies.
	 */

	var debug = _dereq_('debug')('socket.io-parser');
	var json = _dereq_('json3');
	var isArray = _dereq_('isarray');
	var Emitter = _dereq_('component-emitter');
	var binary = _dereq_('./binary');
	var isBuf = _dereq_('./is-buffer');

	/**
	 * Protocol version.
	 *
	 * @api public
	 */

	exports.protocol = 4;

	/**
	 * Packet types.
	 *
	 * @api public
	 */

	exports.types = [
	  'CONNECT',
	  'DISCONNECT',
	  'EVENT',
	  'BINARY_EVENT',
	  'ACK',
	  'BINARY_ACK',
	  'ERROR'
	];

	/**
	 * Packet type `connect`.
	 *
	 * @api public
	 */

	exports.CONNECT = 0;

	/**
	 * Packet type `disconnect`.
	 *
	 * @api public
	 */

	exports.DISCONNECT = 1;

	/**
	 * Packet type `event`.
	 *
	 * @api public
	 */

	exports.EVENT = 2;

	/**
	 * Packet type `ack`.
	 *
	 * @api public
	 */

	exports.ACK = 3;

	/**
	 * Packet type `error`.
	 *
	 * @api public
	 */

	exports.ERROR = 4;

	/**
	 * Packet type 'binary event'
	 *
	 * @api public
	 */

	exports.BINARY_EVENT = 5;

	/**
	 * Packet type `binary ack`. For acks with binary arguments.
	 *
	 * @api public
	 */

	exports.BINARY_ACK = 6;

	/**
	 * Encoder constructor.
	 *
	 * @api public
	 */

	exports.Encoder = Encoder;

	/**
	 * Decoder constructor.
	 *
	 * @api public
	 */

	exports.Decoder = Decoder;

	/**
	 * A socket.io Encoder instance
	 *
	 * @api public
	 */

	function Encoder() {}

	/**
	 * Encode a packet as a single string if non-binary, or as a
	 * buffer sequence, depending on packet type.
	 *
	 * @param {Object} obj - packet object
	 * @param {Function} callback - function to handle encodings (likely engine.write)
	 * @return Calls callback with Array of encodings
	 * @api public
	 */

	Encoder.prototype.encode = function(obj, callback){
	  debug('encoding packet %j', obj);

	  if (exports.BINARY_EVENT == obj.type || exports.BINARY_ACK == obj.type) {
	    encodeAsBinary(obj, callback);
	  }
	  else {
	    var encoding = encodeAsString(obj);
	    callback([encoding]);
	  }
	};

	/**
	 * Encode packet as string.
	 *
	 * @param {Object} packet
	 * @return {String} encoded
	 * @api private
	 */

	function encodeAsString(obj) {
	  var str = '';
	  var nsp = false;

	  // first is type
	  str += obj.type;

	  // attachments if we have them
	  if (exports.BINARY_EVENT == obj.type || exports.BINARY_ACK == obj.type) {
	    str += obj.attachments;
	    str += '-';
	  }

	  // if we have a namespace other than `/`
	  // we append it followed by a comma `,`
	  if (obj.nsp && '/' != obj.nsp) {
	    nsp = true;
	    str += obj.nsp;
	  }

	  // immediately followed by the id
	  if (null != obj.id) {
	    if (nsp) {
	      str += ',';
	      nsp = false;
	    }
	    str += obj.id;
	  }

	  // json data
	  if (null != obj.data) {
	    if (nsp) str += ',';
	    str += json.stringify(obj.data);
	  }

	  debug('encoded %j as %s', obj, str);
	  return str;
	}

	/**
	 * Encode packet as 'buffer sequence' by removing blobs, and
	 * deconstructing packet into object with placeholders and
	 * a list of buffers.
	 *
	 * @param {Object} packet
	 * @return {Buffer} encoded
	 * @api private
	 */

	function encodeAsBinary(obj, callback) {

	  function writeEncoding(bloblessData) {
	    var deconstruction = binary.deconstructPacket(bloblessData);
	    var pack = encodeAsString(deconstruction.packet);
	    var buffers = deconstruction.buffers;

	    buffers.unshift(pack); // add packet info to beginning of data list
	    callback(buffers); // write all the buffers
	  }

	  binary.removeBlobs(obj, writeEncoding);
	}

	/**
	 * A socket.io Decoder instance
	 *
	 * @return {Object} decoder
	 * @api public
	 */

	function Decoder() {
	  this.reconstructor = null;
	}

	/**
	 * Mix in `Emitter` with Decoder.
	 */

	Emitter(Decoder.prototype);

	/**
	 * Decodes an ecoded packet string into packet JSON.
	 *
	 * @param {String} obj - encoded packet
	 * @return {Object} packet
	 * @api public
	 */

	Decoder.prototype.add = function(obj) {
	  var packet;
	  if ('string' == typeof obj) {
	    packet = decodeString(obj);
	    if (exports.BINARY_EVENT == packet.type || exports.BINARY_ACK == packet.type) { // binary packet's json
	      this.reconstructor = new BinaryReconstructor(packet);

	      // no attachments, labeled binary but no binary data to follow
	      if (this.reconstructor.reconPack.attachments === 0) {
	        this.emit('decoded', packet);
	      }
	    } else { // non-binary full packet
	      this.emit('decoded', packet);
	    }
	  }
	  else if (isBuf(obj) || obj.base64) { // raw binary data
	    if (!this.reconstructor) {
	      throw new Error('got binary data when not reconstructing a packet');
	    } else {
	      packet = this.reconstructor.takeBinaryData(obj);
	      if (packet) { // received final buffer
	        this.reconstructor = null;
	        this.emit('decoded', packet);
	      }
	    }
	  }
	  else {
	    throw new Error('Unknown type: ' + obj);
	  }
	};

	/**
	 * Decode a packet String (JSON data)
	 *
	 * @param {String} str
	 * @return {Object} packet
	 * @api private
	 */

	function decodeString(str) {
	  var p = {};
	  var i = 0;

	  // look up type
	  p.type = Number(str.charAt(0));
	  if (null == exports.types[p.type]) return error();

	  // look up attachments if type binary
	  if (exports.BINARY_EVENT == p.type || exports.BINARY_ACK == p.type) {
	    var buf = '';
	    while (str.charAt(++i) != '-') {
	      buf += str.charAt(i);
	      if (i == str.length) break;
	    }
	    if (buf != Number(buf) || str.charAt(i) != '-') {
	      throw new Error('Illegal attachments');
	    }
	    p.attachments = Number(buf);
	  }

	  // look up namespace (if any)
	  if ('/' == str.charAt(i + 1)) {
	    p.nsp = '';
	    while (++i) {
	      var c = str.charAt(i);
	      if (',' == c) break;
	      p.nsp += c;
	      if (i == str.length) break;
	    }
	  } else {
	    p.nsp = '/';
	  }

	  // look up id
	  var next = str.charAt(i + 1);
	  if ('' !== next && Number(next) == next) {
	    p.id = '';
	    while (++i) {
	      var c = str.charAt(i);
	      if (null == c || Number(c) != c) {
	        --i;
	        break;
	      }
	      p.id += str.charAt(i);
	      if (i == str.length) break;
	    }
	    p.id = Number(p.id);
	  }

	  // look up json data
	  if (str.charAt(++i)) {
	    try {
	      p.data = json.parse(str.substr(i));
	    } catch(e){
	      return error();
	    }
	  }

	  debug('decoded %s as %j', str, p);
	  return p;
	}

	/**
	 * Deallocates a parser's resources
	 *
	 * @api public
	 */

	Decoder.prototype.destroy = function() {
	  if (this.reconstructor) {
	    this.reconstructor.finishedReconstruction();
	  }
	};

	/**
	 * A manager of a binary event's 'buffer sequence'. Should
	 * be constructed whenever a packet of type BINARY_EVENT is
	 * decoded.
	 *
	 * @param {Object} packet
	 * @return {BinaryReconstructor} initialized reconstructor
	 * @api private
	 */

	function BinaryReconstructor(packet) {
	  this.reconPack = packet;
	  this.buffers = [];
	}

	/**
	 * Method to be called when binary data received from connection
	 * after a BINARY_EVENT packet.
	 *
	 * @param {Buffer | ArrayBuffer} binData - the raw binary data received
	 * @return {null | Object} returns null if more binary data is expected or
	 *   a reconstructed packet object if all buffers have been received.
	 * @api private
	 */

	BinaryReconstructor.prototype.takeBinaryData = function(binData) {
	  this.buffers.push(binData);
	  if (this.buffers.length == this.reconPack.attachments) { // done with buffer list
	    var packet = binary.reconstructPacket(this.reconPack, this.buffers);
	    this.finishedReconstruction();
	    return packet;
	  }
	  return null;
	};

	/**
	 * Cleans up binary packet reconstruction variables.
	 *
	 * @api private
	 */

	BinaryReconstructor.prototype.finishedReconstruction = function() {
	  this.reconPack = null;
	  this.buffers = [];
	};

	function error(data){
	  return {
	    type: exports.ERROR,
	    data: 'parser error'
	  };
	}

	},{"./binary":46,"./is-buffer":48,"component-emitter":49,"debug":39,"isarray":43,"json3":50}],48:[function(_dereq_,module,exports){
	(function (global){

	module.exports = isBuf;

	/**
	 * Returns true if obj is a buffer or an arraybuffer.
	 *
	 * @api private
	 */

	function isBuf(obj) {
	  return (global.Buffer && global.Buffer.isBuffer(obj)) ||
	         (global.ArrayBuffer && obj instanceof ArrayBuffer);
	}

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{}],49:[function(_dereq_,module,exports){
	arguments[4][15][0].apply(exports,arguments)
	},{"dup":15}],50:[function(_dereq_,module,exports){
	(function (global){
	/*! JSON v3.3.2 | http://bestiejs.github.io/json3 | Copyright 2012-2014, Kit Cambridge | http://kit.mit-license.org */
	;(function () {
	  // Detect the `define` function exposed by asynchronous module loaders. The
	  // strict `define` check is necessary for compatibility with `r.js`.
	  var isLoader = typeof define === "function" && define.amd;

	  // A set of types used to distinguish objects from primitives.
	  var objectTypes = {
	    "function": true,
	    "object": true
	  };

	  // Detect the `exports` object exposed by CommonJS implementations.
	  var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;

	  // Use the `global` object exposed by Node (including Browserify via
	  // `insert-module-globals`), Narwhal, and Ringo as the default context,
	  // and the `window` object in browsers. Rhino exports a `global` function
	  // instead.
	  var root = objectTypes[typeof window] && window || this,
	      freeGlobal = freeExports && objectTypes[typeof module] && module && !module.nodeType && typeof global == "object" && global;

	  if (freeGlobal && (freeGlobal["global"] === freeGlobal || freeGlobal["window"] === freeGlobal || freeGlobal["self"] === freeGlobal)) {
	    root = freeGlobal;
	  }

	  // Public: Initializes JSON 3 using the given `context` object, attaching the
	  // `stringify` and `parse` functions to the specified `exports` object.
	  function runInContext(context, exports) {
	    context || (context = root["Object"]());
	    exports || (exports = root["Object"]());

	    // Native constructor aliases.
	    var Number = context["Number"] || root["Number"],
	        String = context["String"] || root["String"],
	        Object = context["Object"] || root["Object"],
	        Date = context["Date"] || root["Date"],
	        SyntaxError = context["SyntaxError"] || root["SyntaxError"],
	        TypeError = context["TypeError"] || root["TypeError"],
	        Math = context["Math"] || root["Math"],
	        nativeJSON = context["JSON"] || root["JSON"];

	    // Delegate to the native `stringify` and `parse` implementations.
	    if (typeof nativeJSON == "object" && nativeJSON) {
	      exports.stringify = nativeJSON.stringify;
	      exports.parse = nativeJSON.parse;
	    }

	    // Convenience aliases.
	    var objectProto = Object.prototype,
	        getClass = objectProto.toString,
	        isProperty, forEach, undef;

	    // Test the `Date#getUTC*` methods. Based on work by @Yaffle.
	    var isExtended = new Date(-3509827334573292);
	    try {
	      // The `getUTCFullYear`, `Month`, and `Date` methods return nonsensical
	      // results for certain dates in Opera >= 10.53.
	      isExtended = isExtended.getUTCFullYear() == -109252 && isExtended.getUTCMonth() === 0 && isExtended.getUTCDate() === 1 &&
	        // Safari < 2.0.2 stores the internal millisecond time value correctly,
	        // but clips the values returned by the date methods to the range of
	        // signed 32-bit integers ([-2 ** 31, 2 ** 31 - 1]).
	        isExtended.getUTCHours() == 10 && isExtended.getUTCMinutes() == 37 && isExtended.getUTCSeconds() == 6 && isExtended.getUTCMilliseconds() == 708;
	    } catch (exception) {}

	    // Internal: Determines whether the native `JSON.stringify` and `parse`
	    // implementations are spec-compliant. Based on work by Ken Snyder.
	    function has(name) {
	      if (has[name] !== undef) {
	        // Return cached feature test result.
	        return has[name];
	      }
	      var isSupported;
	      if (name == "bug-string-char-index") {
	        // IE <= 7 doesn't support accessing string characters using square
	        // bracket notation. IE 8 only supports this for primitives.
	        isSupported = "a"[0] != "a";
	      } else if (name == "json") {
	        // Indicates whether both `JSON.stringify` and `JSON.parse` are
	        // supported.
	        isSupported = has("json-stringify") && has("json-parse");
	      } else {
	        var value, serialized = '{"a":[1,true,false,null,"\\u0000\\b\\n\\f\\r\\t"]}';
	        // Test `JSON.stringify`.
	        if (name == "json-stringify") {
	          var stringify = exports.stringify, stringifySupported = typeof stringify == "function" && isExtended;
	          if (stringifySupported) {
	            // A test function object with a custom `toJSON` method.
	            (value = function () {
	              return 1;
	            }).toJSON = value;
	            try {
	              stringifySupported =
	                // Firefox 3.1b1 and b2 serialize string, number, and boolean
	                // primitives as object literals.
	                stringify(0) === "0" &&
	                // FF 3.1b1, b2, and JSON 2 serialize wrapped primitives as object
	                // literals.
	                stringify(new Number()) === "0" &&
	                stringify(new String()) == '""' &&
	                // FF 3.1b1, 2 throw an error if the value is `null`, `undefined`, or
	                // does not define a canonical JSON representation (this applies to
	                // objects with `toJSON` properties as well, *unless* they are nested
	                // within an object or array).
	                stringify(getClass) === undef &&
	                // IE 8 serializes `undefined` as `"undefined"`. Safari <= 5.1.7 and
	                // FF 3.1b3 pass this test.
	                stringify(undef) === undef &&
	                // Safari <= 5.1.7 and FF 3.1b3 throw `Error`s and `TypeError`s,
	                // respectively, if the value is omitted entirely.
	                stringify() === undef &&
	                // FF 3.1b1, 2 throw an error if the given value is not a number,
	                // string, array, object, Boolean, or `null` literal. This applies to
	                // objects with custom `toJSON` methods as well, unless they are nested
	                // inside object or array literals. YUI 3.0.0b1 ignores custom `toJSON`
	                // methods entirely.
	                stringify(value) === "1" &&
	                stringify([value]) == "[1]" &&
	                // Prototype <= 1.6.1 serializes `[undefined]` as `"[]"` instead of
	                // `"[null]"`.
	                stringify([undef]) == "[null]" &&
	                // YUI 3.0.0b1 fails to serialize `null` literals.
	                stringify(null) == "null" &&
	                // FF 3.1b1, 2 halts serialization if an array contains a function:
	                // `[1, true, getClass, 1]` serializes as "[1,true,],". FF 3.1b3
	                // elides non-JSON values from objects and arrays, unless they
	                // define custom `toJSON` methods.
	                stringify([undef, getClass, null]) == "[null,null,null]" &&
	                // Simple serialization test. FF 3.1b1 uses Unicode escape sequences
	                // where character escape codes are expected (e.g., `\b` => `\u0008`).
	                stringify({ "a": [value, true, false, null, "\x00\b\n\f\r\t"] }) == serialized &&
	                // FF 3.1b1 and b2 ignore the `filter` and `width` arguments.
	                stringify(null, value) === "1" &&
	                stringify([1, 2], null, 1) == "[\n 1,\n 2\n]" &&
	                // JSON 2, Prototype <= 1.7, and older WebKit builds incorrectly
	                // serialize extended years.
	                stringify(new Date(-8.64e15)) == '"-271821-04-20T00:00:00.000Z"' &&
	                // The milliseconds are optional in ES 5, but required in 5.1.
	                stringify(new Date(8.64e15)) == '"+275760-09-13T00:00:00.000Z"' &&
	                // Firefox <= 11.0 incorrectly serializes years prior to 0 as negative
	                // four-digit years instead of six-digit years. Credits: @Yaffle.
	                stringify(new Date(-621987552e5)) == '"-000001-01-01T00:00:00.000Z"' &&
	                // Safari <= 5.1.5 and Opera >= 10.53 incorrectly serialize millisecond
	                // values less than 1000. Credits: @Yaffle.
	                stringify(new Date(-1)) == '"1969-12-31T23:59:59.999Z"';
	            } catch (exception) {
	              stringifySupported = false;
	            }
	          }
	          isSupported = stringifySupported;
	        }
	        // Test `JSON.parse`.
	        if (name == "json-parse") {
	          var parse = exports.parse;
	          if (typeof parse == "function") {
	            try {
	              // FF 3.1b1, b2 will throw an exception if a bare literal is provided.
	              // Conforming implementations should also coerce the initial argument to
	              // a string prior to parsing.
	              if (parse("0") === 0 && !parse(false)) {
	                // Simple parsing test.
	                value = parse(serialized);
	                var parseSupported = value["a"].length == 5 && value["a"][0] === 1;
	                if (parseSupported) {
	                  try {
	                    // Safari <= 5.1.2 and FF 3.1b1 allow unescaped tabs in strings.
	                    parseSupported = !parse('"\t"');
	                  } catch (exception) {}
	                  if (parseSupported) {
	                    try {
	                      // FF 4.0 and 4.0.1 allow leading `+` signs and leading
	                      // decimal points. FF 4.0, 4.0.1, and IE 9-10 also allow
	                      // certain octal literals.
	                      parseSupported = parse("01") !== 1;
	                    } catch (exception) {}
	                  }
	                  if (parseSupported) {
	                    try {
	                      // FF 4.0, 4.0.1, and Rhino 1.7R3-R4 allow trailing decimal
	                      // points. These environments, along with FF 3.1b1 and 2,
	                      // also allow trailing commas in JSON objects and arrays.
	                      parseSupported = parse("1.") !== 1;
	                    } catch (exception) {}
	                  }
	                }
	              }
	            } catch (exception) {
	              parseSupported = false;
	            }
	          }
	          isSupported = parseSupported;
	        }
	      }
	      return has[name] = !!isSupported;
	    }

	    if (!has("json")) {
	      // Common `[[Class]]` name aliases.
	      var functionClass = "[object Function]",
	          dateClass = "[object Date]",
	          numberClass = "[object Number]",
	          stringClass = "[object String]",
	          arrayClass = "[object Array]",
	          booleanClass = "[object Boolean]";

	      // Detect incomplete support for accessing string characters by index.
	      var charIndexBuggy = has("bug-string-char-index");

	      // Define additional utility methods if the `Date` methods are buggy.
	      if (!isExtended) {
	        var floor = Math.floor;
	        // A mapping between the months of the year and the number of days between
	        // January 1st and the first of the respective month.
	        var Months = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
	        // Internal: Calculates the number of days between the Unix epoch and the
	        // first day of the given month.
	        var getDay = function (year, month) {
	          return Months[month] + 365 * (year - 1970) + floor((year - 1969 + (month = +(month > 1))) / 4) - floor((year - 1901 + month) / 100) + floor((year - 1601 + month) / 400);
	        };
	      }

	      // Internal: Determines if a property is a direct property of the given
	      // object. Delegates to the native `Object#hasOwnProperty` method.
	      if (!(isProperty = objectProto.hasOwnProperty)) {
	        isProperty = function (property) {
	          var members = {}, constructor;
	          if ((members.__proto__ = null, members.__proto__ = {
	            // The *proto* property cannot be set multiple times in recent
	            // versions of Firefox and SeaMonkey.
	            "toString": 1
	          }, members).toString != getClass) {
	            // Safari <= 2.0.3 doesn't implement `Object#hasOwnProperty`, but
	            // supports the mutable *proto* property.
	            isProperty = function (property) {
	              // Capture and break the object's prototype chain (see section 8.6.2
	              // of the ES 5.1 spec). The parenthesized expression prevents an
	              // unsafe transformation by the Closure Compiler.
	              var original = this.__proto__, result = property in (this.__proto__ = null, this);
	              // Restore the original prototype chain.
	              this.__proto__ = original;
	              return result;
	            };
	          } else {
	            // Capture a reference to the top-level `Object` constructor.
	            constructor = members.constructor;
	            // Use the `constructor` property to simulate `Object#hasOwnProperty` in
	            // other environments.
	            isProperty = function (property) {
	              var parent = (this.constructor || constructor).prototype;
	              return property in this && !(property in parent && this[property] === parent[property]);
	            };
	          }
	          members = null;
	          return isProperty.call(this, property);
	        };
	      }

	      // Internal: Normalizes the `for...in` iteration algorithm across
	      // environments. Each enumerated key is yielded to a `callback` function.
	      forEach = function (object, callback) {
	        var size = 0, Properties, members, property;

	        // Tests for bugs in the current environment's `for...in` algorithm. The
	        // `valueOf` property inherits the non-enumerable flag from
	        // `Object.prototype` in older versions of IE, Netscape, and Mozilla.
	        (Properties = function () {
	          this.valueOf = 0;
	        }).prototype.valueOf = 0;

	        // Iterate over a new instance of the `Properties` class.
	        members = new Properties();
	        for (property in members) {
	          // Ignore all properties inherited from `Object.prototype`.
	          if (isProperty.call(members, property)) {
	            size++;
	          }
	        }
	        Properties = members = null;

	        // Normalize the iteration algorithm.
	        if (!size) {
	          // A list of non-enumerable properties inherited from `Object.prototype`.
	          members = ["valueOf", "toString", "toLocaleString", "propertyIsEnumerable", "isPrototypeOf", "hasOwnProperty", "constructor"];
	          // IE <= 8, Mozilla 1.0, and Netscape 6.2 ignore shadowed non-enumerable
	          // properties.
	          forEach = function (object, callback) {
	            var isFunction = getClass.call(object) == functionClass, property, length;
	            var hasProperty = !isFunction && typeof object.constructor != "function" && objectTypes[typeof object.hasOwnProperty] && object.hasOwnProperty || isProperty;
	            for (property in object) {
	              // Gecko <= 1.0 enumerates the `prototype` property of functions under
	              // certain conditions; IE does not.
	              if (!(isFunction && property == "prototype") && hasProperty.call(object, property)) {
	                callback(property);
	              }
	            }
	            // Manually invoke the callback for each non-enumerable property.
	            for (length = members.length; property = members[--length]; hasProperty.call(object, property) && callback(property));
	          };
	        } else if (size == 2) {
	          // Safari <= 2.0.4 enumerates shadowed properties twice.
	          forEach = function (object, callback) {
	            // Create a set of iterated properties.
	            var members = {}, isFunction = getClass.call(object) == functionClass, property;
	            for (property in object) {
	              // Store each property name to prevent double enumeration. The
	              // `prototype` property of functions is not enumerated due to cross-
	              // environment inconsistencies.
	              if (!(isFunction && property == "prototype") && !isProperty.call(members, property) && (members[property] = 1) && isProperty.call(object, property)) {
	                callback(property);
	              }
	            }
	          };
	        } else {
	          // No bugs detected; use the standard `for...in` algorithm.
	          forEach = function (object, callback) {
	            var isFunction = getClass.call(object) == functionClass, property, isConstructor;
	            for (property in object) {
	              if (!(isFunction && property == "prototype") && isProperty.call(object, property) && !(isConstructor = property === "constructor")) {
	                callback(property);
	              }
	            }
	            // Manually invoke the callback for the `constructor` property due to
	            // cross-environment inconsistencies.
	            if (isConstructor || isProperty.call(object, (property = "constructor"))) {
	              callback(property);
	            }
	          };
	        }
	        return forEach(object, callback);
	      };

	      // Public: Serializes a JavaScript `value` as a JSON string. The optional
	      // `filter` argument may specify either a function that alters how object and
	      // array members are serialized, or an array of strings and numbers that
	      // indicates which properties should be serialized. The optional `width`
	      // argument may be either a string or number that specifies the indentation
	      // level of the output.
	      if (!has("json-stringify")) {
	        // Internal: A map of control characters and their escaped equivalents.
	        var Escapes = {
	          92: "\\\\",
	          34: '\\"',
	          8: "\\b",
	          12: "\\f",
	          10: "\\n",
	          13: "\\r",
	          9: "\\t"
	        };

	        // Internal: Converts `value` into a zero-padded string such that its
	        // length is at least equal to `width`. The `width` must be <= 6.
	        var leadingZeroes = "000000";
	        var toPaddedString = function (width, value) {
	          // The `|| 0` expression is necessary to work around a bug in
	          // Opera <= 7.54u2 where `0 == -0`, but `String(-0) !== "0"`.
	          return (leadingZeroes + (value || 0)).slice(-width);
	        };

	        // Internal: Double-quotes a string `value`, replacing all ASCII control
	        // characters (characters with code unit values between 0 and 31) with
	        // their escaped equivalents. This is an implementation of the
	        // `Quote(value)` operation defined in ES 5.1 section 15.12.3.
	        var unicodePrefix = "\\u00";
	        var quote = function (value) {
	          var result = '"', index = 0, length = value.length, useCharIndex = !charIndexBuggy || length > 10;
	          var symbols = useCharIndex && (charIndexBuggy ? value.split("") : value);
	          for (; index < length; index++) {
	            var charCode = value.charCodeAt(index);
	            // If the character is a control character, append its Unicode or
	            // shorthand escape sequence; otherwise, append the character as-is.
	            switch (charCode) {
	              case 8: case 9: case 10: case 12: case 13: case 34: case 92:
	                result += Escapes[charCode];
	                break;
	              default:
	                if (charCode < 32) {
	                  result += unicodePrefix + toPaddedString(2, charCode.toString(16));
	                  break;
	                }
	                result += useCharIndex ? symbols[index] : value.charAt(index);
	            }
	          }
	          return result + '"';
	        };

	        // Internal: Recursively serializes an object. Implements the
	        // `Str(key, holder)`, `JO(value)`, and `JA(value)` operations.
	        var serialize = function (property, object, callback, properties, whitespace, indentation, stack) {
	          var value, className, year, month, date, time, hours, minutes, seconds, milliseconds, results, element, index, length, prefix, result;
	          try {
	            // Necessary for host object support.
	            value = object[property];
	          } catch (exception) {}
	          if (typeof value == "object" && value) {
	            className = getClass.call(value);
	            if (className == dateClass && !isProperty.call(value, "toJSON")) {
	              if (value > -1 / 0 && value < 1 / 0) {
	                // Dates are serialized according to the `Date#toJSON` method
	                // specified in ES 5.1 section 15.9.5.44. See section 15.9.1.15
	                // for the ISO 8601 date time string format.
	                if (getDay) {
	                  // Manually compute the year, month, date, hours, minutes,
	                  // seconds, and milliseconds if the `getUTC*` methods are
	                  // buggy. Adapted from @Yaffle's `date-shim` project.
	                  date = floor(value / 864e5);
	                  for (year = floor(date / 365.2425) + 1970 - 1; getDay(year + 1, 0) <= date; year++);
	                  for (month = floor((date - getDay(year, 0)) / 30.42); getDay(year, month + 1) <= date; month++);
	                  date = 1 + date - getDay(year, month);
	                  // The `time` value specifies the time within the day (see ES
	                  // 5.1 section 15.9.1.2). The formula `(A % B + B) % B` is used
	                  // to compute `A modulo B`, as the `%` operator does not
	                  // correspond to the `modulo` operation for negative numbers.
	                  time = (value % 864e5 + 864e5) % 864e5;
	                  // The hours, minutes, seconds, and milliseconds are obtained by
	                  // decomposing the time within the day. See section 15.9.1.10.
	                  hours = floor(time / 36e5) % 24;
	                  minutes = floor(time / 6e4) % 60;
	                  seconds = floor(time / 1e3) % 60;
	                  milliseconds = time % 1e3;
	                } else {
	                  year = value.getUTCFullYear();
	                  month = value.getUTCMonth();
	                  date = value.getUTCDate();
	                  hours = value.getUTCHours();
	                  minutes = value.getUTCMinutes();
	                  seconds = value.getUTCSeconds();
	                  milliseconds = value.getUTCMilliseconds();
	                }
	                // Serialize extended years correctly.
	                value = (year <= 0 || year >= 1e4 ? (year < 0 ? "-" : "+") + toPaddedString(6, year < 0 ? -year : year) : toPaddedString(4, year)) +
	                  "-" + toPaddedString(2, month + 1) + "-" + toPaddedString(2, date) +
	                  // Months, dates, hours, minutes, and seconds should have two
	                  // digits; milliseconds should have three.
	                  "T" + toPaddedString(2, hours) + ":" + toPaddedString(2, minutes) + ":" + toPaddedString(2, seconds) +
	                  // Milliseconds are optional in ES 5.0, but required in 5.1.
	                  "." + toPaddedString(3, milliseconds) + "Z";
	              } else {
	                value = null;
	              }
	            } else if (typeof value.toJSON == "function" && ((className != numberClass && className != stringClass && className != arrayClass) || isProperty.call(value, "toJSON"))) {
	              // Prototype <= 1.6.1 adds non-standard `toJSON` methods to the
	              // `Number`, `String`, `Date`, and `Array` prototypes. JSON 3
	              // ignores all `toJSON` methods on these objects unless they are
	              // defined directly on an instance.
	              value = value.toJSON(property);
	            }
	          }
	          if (callback) {
	            // If a replacement function was provided, call it to obtain the value
	            // for serialization.
	            value = callback.call(object, property, value);
	          }
	          if (value === null) {
	            return "null";
	          }
	          className = getClass.call(value);
	          if (className == booleanClass) {
	            // Booleans are represented literally.
	            return "" + value;
	          } else if (className == numberClass) {
	            // JSON numbers must be finite. `Infinity` and `NaN` are serialized as
	            // `"null"`.
	            return value > -1 / 0 && value < 1 / 0 ? "" + value : "null";
	          } else if (className == stringClass) {
	            // Strings are double-quoted and escaped.
	            return quote("" + value);
	          }
	          // Recursively serialize objects and arrays.
	          if (typeof value == "object") {
	            // Check for cyclic structures. This is a linear search; performance
	            // is inversely proportional to the number of unique nested objects.
	            for (length = stack.length; length--;) {
	              if (stack[length] === value) {
	                // Cyclic structures cannot be serialized by `JSON.stringify`.
	                throw TypeError();
	              }
	            }
	            // Add the object to the stack of traversed objects.
	            stack.push(value);
	            results = [];
	            // Save the current indentation level and indent one additional level.
	            prefix = indentation;
	            indentation += whitespace;
	            if (className == arrayClass) {
	              // Recursively serialize array elements.
	              for (index = 0, length = value.length; index < length; index++) {
	                element = serialize(index, value, callback, properties, whitespace, indentation, stack);
	                results.push(element === undef ? "null" : element);
	              }
	              result = results.length ? (whitespace ? "[\n" + indentation + results.join(",\n" + indentation) + "\n" + prefix + "]" : ("[" + results.join(",") + "]")) : "[]";
	            } else {
	              // Recursively serialize object members. Members are selected from
	              // either a user-specified list of property names, or the object
	              // itself.
	              forEach(properties || value, function (property) {
	                var element = serialize(property, value, callback, properties, whitespace, indentation, stack);
	                if (element !== undef) {
	                  // According to ES 5.1 section 15.12.3: "If `gap` {whitespace}
	                  // is not the empty string, let `member` {quote(property) + ":"}
	                  // be the concatenation of `member` and the `space` character."
	                  // The "`space` character" refers to the literal space
	                  // character, not the `space` {width} argument provided to
	                  // `JSON.stringify`.
	                  results.push(quote(property) + ":" + (whitespace ? " " : "") + element);
	                }
	              });
	              result = results.length ? (whitespace ? "{\n" + indentation + results.join(",\n" + indentation) + "\n" + prefix + "}" : ("{" + results.join(",") + "}")) : "{}";
	            }
	            // Remove the object from the traversed object stack.
	            stack.pop();
	            return result;
	          }
	        };

	        // Public: `JSON.stringify`. See ES 5.1 section 15.12.3.
	        exports.stringify = function (source, filter, width) {
	          var whitespace, callback, properties, className;
	          if (objectTypes[typeof filter] && filter) {
	            if ((className = getClass.call(filter)) == functionClass) {
	              callback = filter;
	            } else if (className == arrayClass) {
	              // Convert the property names array into a makeshift set.
	              properties = {};
	              for (var index = 0, length = filter.length, value; index < length; value = filter[index++], ((className = getClass.call(value)), className == stringClass || className == numberClass) && (properties[value] = 1));
	            }
	          }
	          if (width) {
	            if ((className = getClass.call(width)) == numberClass) {
	              // Convert the `width` to an integer and create a string containing
	              // `width` number of space characters.
	              if ((width -= width % 1) > 0) {
	                for (whitespace = "", width > 10 && (width = 10); whitespace.length < width; whitespace += " ");
	              }
	            } else if (className == stringClass) {
	              whitespace = width.length <= 10 ? width : width.slice(0, 10);
	            }
	          }
	          // Opera <= 7.54u2 discards the values associated with empty string keys
	          // (`""`) only if they are used directly within an object member list
	          // (e.g., `!("" in { "": 1})`).
	          return serialize("", (value = {}, value[""] = source, value), callback, properties, whitespace, "", []);
	        };
	      }

	      // Public: Parses a JSON source string.
	      if (!has("json-parse")) {
	        var fromCharCode = String.fromCharCode;

	        // Internal: A map of escaped control characters and their unescaped
	        // equivalents.
	        var Unescapes = {
	          92: "\\",
	          34: '"',
	          47: "/",
	          98: "\b",
	          116: "\t",
	          110: "\n",
	          102: "\f",
	          114: "\r"
	        };

	        // Internal: Stores the parser state.
	        var Index, Source;

	        // Internal: Resets the parser state and throws a `SyntaxError`.
	        var abort = function () {
	          Index = Source = null;
	          throw SyntaxError();
	        };

	        // Internal: Returns the next token, or `"$"` if the parser has reached
	        // the end of the source string. A token may be a string, number, `null`
	        // literal, or Boolean literal.
	        var lex = function () {
	          var source = Source, length = source.length, value, begin, position, isSigned, charCode;
	          while (Index < length) {
	            charCode = source.charCodeAt(Index);
	            switch (charCode) {
	              case 9: case 10: case 13: case 32:
	                // Skip whitespace tokens, including tabs, carriage returns, line
	                // feeds, and space characters.
	                Index++;
	                break;
	              case 123: case 125: case 91: case 93: case 58: case 44:
	                // Parse a punctuator token (`{`, `}`, `[`, `]`, `:`, or `,`) at
	                // the current position.
	                value = charIndexBuggy ? source.charAt(Index) : source[Index];
	                Index++;
	                return value;
	              case 34:
	                // `"` delimits a JSON string; advance to the next character and
	                // begin parsing the string. String tokens are prefixed with the
	                // sentinel `@` character to distinguish them from punctuators and
	                // end-of-string tokens.
	                for (value = "@", Index++; Index < length;) {
	                  charCode = source.charCodeAt(Index);
	                  if (charCode < 32) {
	                    // Unescaped ASCII control characters (those with a code unit
	                    // less than the space character) are not permitted.
	                    abort();
	                  } else if (charCode == 92) {
	                    // A reverse solidus (`\`) marks the beginning of an escaped
	                    // control character (including `"`, `\`, and `/`) or Unicode
	                    // escape sequence.
	                    charCode = source.charCodeAt(++Index);
	                    switch (charCode) {
	                      case 92: case 34: case 47: case 98: case 116: case 110: case 102: case 114:
	                        // Revive escaped control characters.
	                        value += Unescapes[charCode];
	                        Index++;
	                        break;
	                      case 117:
	                        // `\u` marks the beginning of a Unicode escape sequence.
	                        // Advance to the first character and validate the
	                        // four-digit code point.
	                        begin = ++Index;
	                        for (position = Index + 4; Index < position; Index++) {
	                          charCode = source.charCodeAt(Index);
	                          // A valid sequence comprises four hexdigits (case-
	                          // insensitive) that form a single hexadecimal value.
	                          if (!(charCode >= 48 && charCode <= 57 || charCode >= 97 && charCode <= 102 || charCode >= 65 && charCode <= 70)) {
	                            // Invalid Unicode escape sequence.
	                            abort();
	                          }
	                        }
	                        // Revive the escaped character.
	                        value += fromCharCode("0x" + source.slice(begin, Index));
	                        break;
	                      default:
	                        // Invalid escape sequence.
	                        abort();
	                    }
	                  } else {
	                    if (charCode == 34) {
	                      // An unescaped double-quote character marks the end of the
	                      // string.
	                      break;
	                    }
	                    charCode = source.charCodeAt(Index);
	                    begin = Index;
	                    // Optimize for the common case where a string is valid.
	                    while (charCode >= 32 && charCode != 92 && charCode != 34) {
	                      charCode = source.charCodeAt(++Index);
	                    }
	                    // Append the string as-is.
	                    value += source.slice(begin, Index);
	                  }
	                }
	                if (source.charCodeAt(Index) == 34) {
	                  // Advance to the next character and return the revived string.
	                  Index++;
	                  return value;
	                }
	                // Unterminated string.
	                abort();
	              default:
	                // Parse numbers and literals.
	                begin = Index;
	                // Advance past the negative sign, if one is specified.
	                if (charCode == 45) {
	                  isSigned = true;
	                  charCode = source.charCodeAt(++Index);
	                }
	                // Parse an integer or floating-point value.
	                if (charCode >= 48 && charCode <= 57) {
	                  // Leading zeroes are interpreted as octal literals.
	                  if (charCode == 48 && ((charCode = source.charCodeAt(Index + 1)), charCode >= 48 && charCode <= 57)) {
	                    // Illegal octal literal.
	                    abort();
	                  }
	                  isSigned = false;
	                  // Parse the integer component.
	                  for (; Index < length && ((charCode = source.charCodeAt(Index)), charCode >= 48 && charCode <= 57); Index++);
	                  // Floats cannot contain a leading decimal point; however, this
	                  // case is already accounted for by the parser.
	                  if (source.charCodeAt(Index) == 46) {
	                    position = ++Index;
	                    // Parse the decimal component.
	                    for (; position < length && ((charCode = source.charCodeAt(position)), charCode >= 48 && charCode <= 57); position++);
	                    if (position == Index) {
	                      // Illegal trailing decimal.
	                      abort();
	                    }
	                    Index = position;
	                  }
	                  // Parse exponents. The `e` denoting the exponent is
	                  // case-insensitive.
	                  charCode = source.charCodeAt(Index);
	                  if (charCode == 101 || charCode == 69) {
	                    charCode = source.charCodeAt(++Index);
	                    // Skip past the sign following the exponent, if one is
	                    // specified.
	                    if (charCode == 43 || charCode == 45) {
	                      Index++;
	                    }
	                    // Parse the exponential component.
	                    for (position = Index; position < length && ((charCode = source.charCodeAt(position)), charCode >= 48 && charCode <= 57); position++);
	                    if (position == Index) {
	                      // Illegal empty exponent.
	                      abort();
	                    }
	                    Index = position;
	                  }
	                  // Coerce the parsed value to a JavaScript number.
	                  return +source.slice(begin, Index);
	                }
	                // A negative sign may only precede numbers.
	                if (isSigned) {
	                  abort();
	                }
	                // `true`, `false`, and `null` literals.
	                if (source.slice(Index, Index + 4) == "true") {
	                  Index += 4;
	                  return true;
	                } else if (source.slice(Index, Index + 5) == "false") {
	                  Index += 5;
	                  return false;
	                } else if (source.slice(Index, Index + 4) == "null") {
	                  Index += 4;
	                  return null;
	                }
	                // Unrecognized token.
	                abort();
	            }
	          }
	          // Return the sentinel `$` character if the parser has reached the end
	          // of the source string.
	          return "$";
	        };

	        // Internal: Parses a JSON `value` token.
	        var get = function (value) {
	          var results, hasMembers;
	          if (value == "$") {
	            // Unexpected end of input.
	            abort();
	          }
	          if (typeof value == "string") {
	            if ((charIndexBuggy ? value.charAt(0) : value[0]) == "@") {
	              // Remove the sentinel `@` character.
	              return value.slice(1);
	            }
	            // Parse object and array literals.
	            if (value == "[") {
	              // Parses a JSON array, returning a new JavaScript array.
	              results = [];
	              for (;; hasMembers || (hasMembers = true)) {
	                value = lex();
	                // A closing square bracket marks the end of the array literal.
	                if (value == "]") {
	                  break;
	                }
	                // If the array literal contains elements, the current token
	                // should be a comma separating the previous element from the
	                // next.
	                if (hasMembers) {
	                  if (value == ",") {
	                    value = lex();
	                    if (value == "]") {
	                      // Unexpected trailing `,` in array literal.
	                      abort();
	                    }
	                  } else {
	                    // A `,` must separate each array element.
	                    abort();
	                  }
	                }
	                // Elisions and leading commas are not permitted.
	                if (value == ",") {
	                  abort();
	                }
	                results.push(get(value));
	              }
	              return results;
	            } else if (value == "{") {
	              // Parses a JSON object, returning a new JavaScript object.
	              results = {};
	              for (;; hasMembers || (hasMembers = true)) {
	                value = lex();
	                // A closing curly brace marks the end of the object literal.
	                if (value == "}") {
	                  break;
	                }
	                // If the object literal contains members, the current token
	                // should be a comma separator.
	                if (hasMembers) {
	                  if (value == ",") {
	                    value = lex();
	                    if (value == "}") {
	                      // Unexpected trailing `,` in object literal.
	                      abort();
	                    }
	                  } else {
	                    // A `,` must separate each object member.
	                    abort();
	                  }
	                }
	                // Leading commas are not permitted, object property names must be
	                // double-quoted strings, and a `:` must separate each property
	                // name and value.
	                if (value == "," || typeof value != "string" || (charIndexBuggy ? value.charAt(0) : value[0]) != "@" || lex() != ":") {
	                  abort();
	                }
	                results[value.slice(1)] = get(lex());
	              }
	              return results;
	            }
	            // Unexpected token encountered.
	            abort();
	          }
	          return value;
	        };

	        // Internal: Updates a traversed object member.
	        var update = function (source, property, callback) {
	          var element = walk(source, property, callback);
	          if (element === undef) {
	            delete source[property];
	          } else {
	            source[property] = element;
	          }
	        };

	        // Internal: Recursively traverses a parsed JSON object, invoking the
	        // `callback` function for each value. This is an implementation of the
	        // `Walk(holder, name)` operation defined in ES 5.1 section 15.12.2.
	        var walk = function (source, property, callback) {
	          var value = source[property], length;
	          if (typeof value == "object" && value) {
	            // `forEach` can't be used to traverse an array in Opera <= 8.54
	            // because its `Object#hasOwnProperty` implementation returns `false`
	            // for array indices (e.g., `![1, 2, 3].hasOwnProperty("0")`).
	            if (getClass.call(value) == arrayClass) {
	              for (length = value.length; length--;) {
	                update(value, length, callback);
	              }
	            } else {
	              forEach(value, function (property) {
	                update(value, property, callback);
	              });
	            }
	          }
	          return callback.call(source, property, value);
	        };

	        // Public: `JSON.parse`. See ES 5.1 section 15.12.2.
	        exports.parse = function (source, callback) {
	          var result, value;
	          Index = 0;
	          Source = "" + source;
	          result = get(lex());
	          // If a JSON string contains multiple tokens, it is invalid.
	          if (lex() != "$") {
	            abort();
	          }
	          // Reset the parser state.
	          Index = Source = null;
	          return callback && getClass.call(callback) == functionClass ? walk((value = {}, value[""] = result, value), "", callback) : result;
	        };
	      }
	    }

	    exports["runInContext"] = runInContext;
	    return exports;
	  }

	  if (freeExports && !isLoader) {
	    // Export for CommonJS environments.
	    runInContext(root, freeExports);
	  } else {
	    // Export for web browsers and JavaScript engines.
	    var nativeJSON = root.JSON,
	        previousJSON = root["JSON3"],
	        isRestored = false;

	    var JSON3 = runInContext(root, (root["JSON3"] = {
	      // Public: Restores the original value of the global `JSON` object and
	      // returns a reference to the `JSON3` object.
	      "noConflict": function () {
	        if (!isRestored) {
	          isRestored = true;
	          root.JSON = nativeJSON;
	          root["JSON3"] = previousJSON;
	          nativeJSON = previousJSON = null;
	        }
	        return JSON3;
	      }
	    }));

	    root.JSON = {
	      "parse": JSON3.parse,
	      "stringify": JSON3.stringify
	    };
	  }

	  // Export for asynchronous module loaders.
	  if (isLoader) {
	    define(function () {
	      return JSON3;
	    });
	  }
	}).call(this);

	}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : {})
	},{}],51:[function(_dereq_,module,exports){
	module.exports = toArray

	function toArray(list, index) {
	    var array = []

	    index = index || 0

	    for (var i = index || 0; i < list.length; i++) {
	        array[i - index] = list[i]
	    }

	    return array
	}

	},{}]},{},[31])(31)
	});

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 142 */
/***/ function(module, exports) {

	angular.module('service').service('chat',Chat);

	Chat.$inject = ['$http','BASE_API'];

	function Chat($http,BASE_API) {
	    this.findFriends = function (data) {
	        //Userid and keyword to find
	    	return $http.post(BASE_API + '/chat/friends/find',data);
	    }


	    this.findFriendWithId = function (data) {
	        //Userid as a friend id to search
	        return $http.post(BASE_API + '/chat/friends/findFriendWithId',data);
	    }

	    this.fetchMsgs = function (data) {
	        //Userid and fr_id to find
	    	return $http.post(BASE_API + '/chat/msgs/find',data);
	    }
	    this.clearMsgs = function (data) {
	        //Userid and fr_id to clear
	        return $http.post(BASE_API + '/chat/msgs/clear',data);
	    }

	    this.deleteMsgs = function (data) {
	        return $http.post(BASE_API + '/chat/msgs/deleteMsgs',data);
	    }

	    this.editMsgs = function (data) {
	        return $http.post(BASE_API + '/chat/msgs/editMsgs',data);
	    }

	    this.createGroup = function (data) {
	        //grp_nm , grp_own,members
	    	return $http.post(BASE_API + '/chat/grp/create',data);
	    }

	    this.addMembersToGroup = function (data) {
	        //grp_nm , grp_own,members
	        return $http.post(BASE_API + '/chat/grp/addMembers',data);
	    }

	    this.findMembers = function (data) {
	        //grp_id
	        return $http.post(BASE_API + '/chat/grp/findMembers',data);
	    }

	}

/***/ },
/* 143 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(144);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!./image-crop-styles.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!./image-crop-styles.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 144 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "/* Some of these styles you can override, things like colors,\n * however some styles are required for the structure, and are critical to this module behaving properly!\n */\n\n/* Core component styles */\n.ng-image-crop {\n  text-align: center;\n  margin: 0 auto;\n  position: relative;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  -o-user-select: none;\n  user-select: none;\n}\n/* Each of the 3 steps in the process are contained within sections */\n.ng-image-crop > section {\n  background: #ccc;\n}\n/* The cropping button */\n.ng-image-crop button {\n  margin-top: 10px;\n}\n/* The dashed cropping guideline */\n.ng-image-crop .cropping-guide {\n  display: block;\n  background: rgba(255, 255, 255, .3);\n  border: 2px dashed white;\n  position: absolute;\n  pointer-events: none;\n}\n/* The circular themed cropping guideline */\n.ng-image-crop--circle .cropping-guide {\n  border-radius: 50%;\n  -webkit-border-radius: 50%;\n  -moz-border-radius: 50%;\n  -ms-border-radius: 50%;\n  -o-border-radius: 50%;\n}\n/* The canvas where the user positions the image via dragging and zooming */\n.ng-image-crop .cropping-canvas {\n  background: rgba(255, 255, 255, .3);\n  margin: 0 auto;\n  cursor: move;\n}\n/* The overlayed draggable zoom handle in the corner of the module */\n.ng-image-crop .zoom-handle {\n  display: block;\n  position: absolute;\n  bottom: 1px;\n  left: 1px;\n  background: rgba(255,255,255,0.7);\n  width: 80px;\n  height: 80px;\n  cursor: move;\n  border-radius: 200px 50px;\n}\n/* The text within the zoom handle */\n.ng-image-crop .zoom-handle > span {\n  color: rgba(0, 0, 0, 0.5);\n  -webkit-transform: rotate(-45deg);\n  -moz-transform: rotate(-45deg);\n  -ms-transform: rotate(-45deg);\n  -o-transform: rotate(-45deg);\n  transform: rotate(-45deg);\n  display: block;\n  position: relative;\n  top: 32px;\n}", ""]);

	// exports


/***/ },
/* 145 */
/***/ function(module, exports) {

	/**
	 * AngularJS Directive - Image Crop v1.1.0
	 * Copyright (c) 2014 Andy Shora, andyshora@gmail.com, andyshora.com
	 * Licensed under the MPL License [http://www.nihilogic.dk/licenses/mpl-license.txt]
	 */
	(function() {

	  /*
	   * DEPENDENCY
	   * Javascript BinaryFile
	   * Copyright (c) 2008 Jacob Seidelin, jseidelin@nihilogic.dk, http://blog.nihilogic.dk/
	   * Licensed under the MPL License [http://www.nihilogic.dk/licenses/mpl-license.txt]
	   */

	  var BinaryFile = function(strData, iDataOffset, iDataLength) {
	      var data = strData;
	      var dataOffset = iDataOffset || 0;
	      var dataLength = 0;

	      this.getRawData = function() {
	          return data;
	      }

	      if (typeof strData == "string") {
	          dataLength = iDataLength || data.length;

	          this.getByteAt = function(iOffset) {
	              return data.charCodeAt(iOffset + dataOffset) & 0xFF;
	          }

	          this.getBytesAt = function(iOffset, iLength) {
	              var aBytes = [];

	              for (var i = 0; i < iLength; i++) {
	                  aBytes[i] = data.charCodeAt((iOffset + i) + dataOffset) & 0xFF
	              }
	              ;

	              return aBytes;
	          }
	      } else if (typeof strData == "unknown") {
	          dataLength = iDataLength || IEBinary_getLength(data);

	          this.getByteAt = function(iOffset) {
	              return IEBinary_getByteAt(data, iOffset + dataOffset);
	          }

	          this.getBytesAt = function(iOffset, iLength) {
	              return new VBArray(IEBinary_getBytesAt(data, iOffset + dataOffset, iLength)).toArray();
	          }
	      }

	      this.getLength = function() {
	          return dataLength;
	      }

	      this.getSByteAt = function(iOffset) {
	          var iByte = this.getByteAt(iOffset);
	          if (iByte > 127)
	              return iByte - 256;
	          else
	              return iByte;
	      }

	      this.getShortAt = function(iOffset, bBigEndian) {
	          var iShort = bBigEndian ?
	                  (this.getByteAt(iOffset) << 8) + this.getByteAt(iOffset + 1)
	                  : (this.getByteAt(iOffset + 1) << 8) + this.getByteAt(iOffset)
	          if (iShort < 0)
	              iShort += 65536;
	          return iShort;
	      }
	      this.getSShortAt = function(iOffset, bBigEndian) {
	          var iUShort = this.getShortAt(iOffset, bBigEndian);
	          if (iUShort > 32767)
	              return iUShort - 65536;
	          else
	              return iUShort;
	      }
	      this.getLongAt = function(iOffset, bBigEndian) {
	          var iByte1 = this.getByteAt(iOffset),
	                  iByte2 = this.getByteAt(iOffset + 1),
	                  iByte3 = this.getByteAt(iOffset + 2),
	                  iByte4 = this.getByteAt(iOffset + 3);

	          var iLong = bBigEndian ?
	                  (((((iByte1 << 8) + iByte2) << 8) + iByte3) << 8) + iByte4
	                  : (((((iByte4 << 8) + iByte3) << 8) + iByte2) << 8) + iByte1;
	          if (iLong < 0)
	              iLong += 4294967296;
	          return iLong;
	      }
	      this.getSLongAt = function(iOffset, bBigEndian) {
	          var iULong = this.getLongAt(iOffset, bBigEndian);
	          if (iULong > 2147483647)
	              return iULong - 4294967296;
	          else
	              return iULong;
	      }

	      this.getStringAt = function(iOffset, iLength) {
	          var aStr = [];

	          var aBytes = this.getBytesAt(iOffset, iLength);
	          for (var j = 0; j < iLength; j++) {
	              aStr[j] = String.fromCharCode(aBytes[j]);
	          }
	          return aStr.join("");
	      }

	      this.getCharAt = function(iOffset) {
	          return String.fromCharCode(this.getByteAt(iOffset));
	      }
	      this.toBase64 = function() {
	          return window.btoa(data);
	      }
	      this.fromBase64 = function(strBase64) {
	          data = window.atob(strBase64);
	      }
	  };
	  /*
	   * DEPENDENCY
	   * Javascript EXIF Reader 0.1.6
	   * Copyright (c) 2008 Jacob Seidelin, jseidelin@nihilogic.dk, http://blog.nihilogic.dk/
	   * Licensed under the MPL License [http://www.nihilogic.dk/licenses/mpl-license.txt]
	   */
	  var EXIF = (function() {

	      var debug = false;

	      var ExifTags = {

	          // version tags
	          0x9000: "ExifVersion", // EXIF version
	          0xA000: "FlashpixVersion", // Flashpix format version

	          // colorspace tags
	          0xA001: "ColorSpace", // Color space information tag

	          // image configuration
	          0xA002: "PixelXDimension", // Valid width of meaningful image
	          0xA003: "PixelYDimension", // Valid height of meaningful image
	          0x9101: "ComponentsConfiguration", // Information about channels
	          0x9102: "CompressedBitsPerPixel", // Compressed bits per pixel

	          // user information
	          0x927C: "MakerNote", // Any desired information written by the manufacturer
	          0x9286: "UserComment", // Comments by user

	          // related file
	          0xA004: "RelatedSoundFile", // Name of related sound file

	          // date and time
	          0x9003: "DateTimeOriginal", // Date and time when the original image was generated
	          0x9004: "DateTimeDigitized", // Date and time when the image was stored digitally
	          0x9290: "SubsecTime", // Fractions of seconds for DateTime
	          0x9291: "SubsecTimeOriginal", // Fractions of seconds for DateTimeOriginal
	          0x9292: "SubsecTimeDigitized", // Fractions of seconds for DateTimeDigitized

	          // picture-taking conditions
	          0x829A: "ExposureTime", // Exposure time (in seconds)
	          0x829D: "FNumber", // F number
	          0x8822: "ExposureProgram", // Exposure program
	          0x8824: "SpectralSensitivity", // Spectral sensitivity
	          0x8827: "ISOSpeedRatings", // ISO speed rating
	          0x8828: "OECF", // Optoelectric conversion factor
	          0x9201: "ShutterSpeedValue", // Shutter speed
	          0x9202: "ApertureValue", // Lens aperture
	          0x9203: "BrightnessValue", // Value of brightness
	          0x9204: "ExposureBias", // Exposure bias
	          0x9205: "MaxApertureValue", // Smallest F number of lens
	          0x9206: "SubjectDistance", // Distance to subject in meters
	          0x9207: "MeteringMode", // Metering mode
	          0x9208: "LightSource", // Kind of light source
	          0x9209: "Flash", // Flash status
	          0x9214: "SubjectArea", // Location and area of main subject
	          0x920A: "FocalLength", // Focal length of the lens in mm
	          0xA20B: "FlashEnergy", // Strobe energy in BCPS
	          0xA20C: "SpatialFrequencyResponse", //
	          0xA20E: "FocalPlaneXResolution", // Number of pixels in width direction per FocalPlaneResolutionUnit
	          0xA20F: "FocalPlaneYResolution", // Number of pixels in height direction per FocalPlaneResolutionUnit
	          0xA210: "FocalPlaneResolutionUnit", // Unit for measuring FocalPlaneXResolution and FocalPlaneYResolution
	          0xA214: "SubjectLocation", // Location of subject in image
	          0xA215: "ExposureIndex", // Exposure index selected on camera
	          0xA217: "SensingMethod", // Image sensor type
	          0xA300: "FileSource", // Image source (3 == DSC)
	          0xA301: "SceneType", // Scene type (1 == directly photographed)
	          0xA302: "CFAPattern", // Color filter array geometric pattern
	          0xA401: "CustomRendered", // Special processing
	          0xA402: "ExposureMode", // Exposure mode
	          0xA403: "WhiteBalance", // 1 = auto white balance, 2 = manual
	          0xA404: "DigitalZoomRation", // Digital zoom ratio
	          0xA405: "FocalLengthIn35mmFilm", // Equivalent foacl length assuming 35mm film camera (in mm)
	          0xA406: "SceneCaptureType", // Type of scene
	          0xA407: "GainControl", // Degree of overall image gain adjustment
	          0xA408: "Contrast", // Direction of contrast processing applied by camera
	          0xA409: "Saturation", // Direction of saturation processing applied by camera
	          0xA40A: "Sharpness", // Direction of sharpness processing applied by camera
	          0xA40B: "DeviceSettingDescription", //
	          0xA40C: "SubjectDistanceRange", // Distance to subject

	          // other tags
	          0xA005: "InteroperabilityIFDPointer",
	          0xA420: "ImageUniqueID"   // Identifier assigned uniquely to each image
	      };

	      var TiffTags = {
	          0x0100: "ImageWidth",
	          0x0101: "ImageHeight",
	          0x8769: "ExifIFDPointer",
	          0x8825: "GPSInfoIFDPointer",
	          0xA005: "InteroperabilityIFDPointer",
	          0x0102: "BitsPerSample",
	          0x0103: "Compression",
	          0x0106: "PhotometricInterpretation",
	          0x0112: "Orientation",
	          0x0115: "SamplesPerPixel",
	          0x011C: "PlanarConfiguration",
	          0x0212: "YCbCrSubSampling",
	          0x0213: "YCbCrPositioning",
	          0x011A: "XResolution",
	          0x011B: "YResolution",
	          0x0128: "ResolutionUnit",
	          0x0111: "StripOffsets",
	          0x0116: "RowsPerStrip",
	          0x0117: "StripByteCounts",
	          0x0201: "JPEGInterchangeFormat",
	          0x0202: "JPEGInterchangeFormatLength",
	          0x012D: "TransferFunction",
	          0x013E: "WhitePoint",
	          0x013F: "PrimaryChromaticities",
	          0x0211: "YCbCrCoefficients",
	          0x0214: "ReferenceBlackWhite",
	          0x0132: "DateTime",
	          0x010E: "ImageDescription",
	          0x010F: "Make",
	          0x0110: "Model",
	          0x0131: "Software",
	          0x013B: "Artist",
	          0x8298: "Copyright"
	      };

	      var GPSTags = {
	          0x0000: "GPSVersionID",
	          0x0001: "GPSLatitudeRef",
	          0x0002: "GPSLatitude",
	          0x0003: "GPSLongitudeRef",
	          0x0004: "GPSLongitude",
	          0x0005: "GPSAltitudeRef",
	          0x0006: "GPSAltitude",
	          0x0007: "GPSTimeStamp",
	          0x0008: "GPSSatellites",
	          0x0009: "GPSStatus",
	          0x000A: "GPSMeasureMode",
	          0x000B: "GPSDOP",
	          0x000C: "GPSSpeedRef",
	          0x000D: "GPSSpeed",
	          0x000E: "GPSTrackRef",
	          0x000F: "GPSTrack",
	          0x0010: "GPSImgDirectionRef",
	          0x0011: "GPSImgDirection",
	          0x0012: "GPSMapDatum",
	          0x0013: "GPSDestLatitudeRef",
	          0x0014: "GPSDestLatitude",
	          0x0015: "GPSDestLongitudeRef",
	          0x0016: "GPSDestLongitude",
	          0x0017: "GPSDestBearingRef",
	          0x0018: "GPSDestBearing",
	          0x0019: "GPSDestDistanceRef",
	          0x001A: "GPSDestDistance",
	          0x001B: "GPSProcessingMethod",
	          0x001C: "GPSAreaInformation",
	          0x001D: "GPSDateStamp",
	          0x001E: "GPSDifferential"
	      };

	      var StringValues = {
	          ExposureProgram: {
	              0: "Not defined",
	              1: "Manual",
	              2: "Normal program",
	              3: "Aperture priority",
	              4: "Shutter priority",
	              5: "Creative program",
	              6: "Action program",
	              7: "Portrait mode",
	              8: "Landscape mode"
	          },
	          MeteringMode: {
	              0: "Unknown",
	              1: "Average",
	              2: "CenterWeightedAverage",
	              3: "Spot",
	              4: "MultiSpot",
	              5: "Pattern",
	              6: "Partial",
	              255: "Other"
	          },
	          LightSource: {
	              0: "Unknown",
	              1: "Daylight",
	              2: "Fluorescent",
	              3: "Tungsten (incandescent light)",
	              4: "Flash",
	              9: "Fine weather",
	              10: "Cloudy weather",
	              11: "Shade",
	              12: "Daylight fluorescent (D 5700 - 7100K)",
	              13: "Day white fluorescent (N 4600 - 5400K)",
	              14: "Cool white fluorescent (W 3900 - 4500K)",
	              15: "White fluorescent (WW 3200 - 3700K)",
	              17: "Standard light A",
	              18: "Standard light B",
	              19: "Standard light C",
	              20: "D55",
	              21: "D65",
	              22: "D75",
	              23: "D50",
	              24: "ISO studio tungsten",
	              255: "Other"
	          },
	          Flash: {
	              0x0000: "Flash did not fire",
	              0x0001: "Flash fired",
	              0x0005: "Strobe return light not detected",
	              0x0007: "Strobe return light detected",
	              0x0009: "Flash fired, compulsory flash mode",
	              0x000D: "Flash fired, compulsory flash mode, return light not detected",
	              0x000F: "Flash fired, compulsory flash mode, return light detected",
	              0x0010: "Flash did not fire, compulsory flash mode",
	              0x0018: "Flash did not fire, auto mode",
	              0x0019: "Flash fired, auto mode",
	              0x001D: "Flash fired, auto mode, return light not detected",
	              0x001F: "Flash fired, auto mode, return light detected",
	              0x0020: "No flash function",
	              0x0041: "Flash fired, red-eye reduction mode",
	              0x0045: "Flash fired, red-eye reduction mode, return light not detected",
	              0x0047: "Flash fired, red-eye reduction mode, return light detected",
	              0x0049: "Flash fired, compulsory flash mode, red-eye reduction mode",
	              0x004D: "Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected",
	              0x004F: "Flash fired, compulsory flash mode, red-eye reduction mode, return light detected",
	              0x0059: "Flash fired, auto mode, red-eye reduction mode",
	              0x005D: "Flash fired, auto mode, return light not detected, red-eye reduction mode",
	              0x005F: "Flash fired, auto mode, return light detected, red-eye reduction mode"
	          },
	          SensingMethod: {
	              1: "Not defined",
	              2: "One-chip color area sensor",
	              3: "Two-chip color area sensor",
	              4: "Three-chip color area sensor",
	              5: "Color sequential area sensor",
	              7: "Trilinear sensor",
	              8: "Color sequential linear sensor"
	          },
	          SceneCaptureType: {
	              0: "Standard",
	              1: "Landscape",
	              2: "Portrait",
	              3: "Night scene"
	          },
	          SceneType: {
	              1: "Directly photographed"
	          },
	          CustomRendered: {
	              0: "Normal process",
	              1: "Custom process"
	          },
	          WhiteBalance: {
	              0: "Auto white balance",
	              1: "Manual white balance"
	          },
	          GainControl: {
	              0: "None",
	              1: "Low gain up",
	              2: "High gain up",
	              3: "Low gain down",
	              4: "High gain down"
	          },
	          Contrast: {
	              0: "Normal",
	              1: "Soft",
	              2: "Hard"
	          },
	          Saturation: {
	              0: "Normal",
	              1: "Low saturation",
	              2: "High saturation"
	          },
	          Sharpness: {
	              0: "Normal",
	              1: "Soft",
	              2: "Hard"
	          },
	          SubjectDistanceRange: {
	              0: "Unknown",
	              1: "Macro",
	              2: "Close view",
	              3: "Distant view"
	          },
	          FileSource: {
	              3: "DSC"
	          },
	          Components: {
	              0: "",
	              1: "Y",
	              2: "Cb",
	              3: "Cr",
	              4: "R",
	              5: "G",
	              6: "B"
	          }
	      };

	      function addEvent(element, event, handler) {
	          if (element.addEventListener) {
	              element.addEventListener(event, handler, false);
	          } else if (element.attachEvent) {
	              element.attachEvent("on" + event, handler);
	          }
	      }

	      function imageHasData(img) {
	          return !!(img.exifdata);
	      }

	      function getImageData(img, callback) {
	          BinaryAjax(img.src, function(http) {
	              var data = findEXIFinJPEG(http.binaryResponse);
	              img.exifdata = data || {};
	              if (callback) {
	                  callback.call(img)
	              }
	          });
	      }

	      function findEXIFinJPEG(file) {
	          if (file.getByteAt(0) != 0xFF || file.getByteAt(1) != 0xD8) {
	              return false; // not a valid jpeg
	          }

	          var offset = 2,
	                  length = file.getLength(),
	                  marker;

	          while (offset < length) {
	              if (file.getByteAt(offset) != 0xFF) {
	                  if (debug)
	                      console.log("Not a valid marker at offset " + offset + ", found: " + file.getByteAt(offset));
	                  return false; // not a valid marker, something is wrong
	              }

	              marker = file.getByteAt(offset + 1);

	              // we could implement handling for other markers here,
	              // but we're only looking for 0xFFE1 for EXIF data

	              if (marker == 22400) {
	                  if (debug)
	                      console.log("Found 0xFFE1 marker");

	                  return readEXIFData(file, offset + 4, file.getShortAt(offset + 2, true) - 2);

	                  // offset += 2 + file.getShortAt(offset+2, true);

	              } else if (marker == 225) {
	                  // 0xE1 = Application-specific 1 (for EXIF)
	                  if (debug)
	                      console.log("Found 0xFFE1 marker");

	                  return readEXIFData(file, offset + 4, file.getShortAt(offset + 2, true) - 2);

	              } else {
	                  offset += 2 + file.getShortAt(offset + 2, true);
	              }

	          }

	      }


	      function readTags(file, tiffStart, dirStart, strings, bigEnd) {
	          var entries = file.getShortAt(dirStart, bigEnd),
	                  tags = {},
	                  entryOffset, tag,
	                  i;

	          for (i = 0; i < entries; i++) {
	              entryOffset = dirStart + i * 12 + 2;
	              tag = strings[file.getShortAt(entryOffset, bigEnd)];
	              if (!tag && debug)
	                  console.log("Unknown tag: " + file.getShortAt(entryOffset, bigEnd));
	              tags[tag] = readTagValue(file, entryOffset, tiffStart, dirStart, bigEnd);
	          }
	          return tags;
	      }


	      function readTagValue(file, entryOffset, tiffStart, dirStart, bigEnd) {
	          var type = file.getShortAt(entryOffset + 2, bigEnd),
	                  numValues = file.getLongAt(entryOffset + 4, bigEnd),
	                  valueOffset = file.getLongAt(entryOffset + 8, bigEnd) + tiffStart,
	                  offset,
	                  vals, val, n,
	                  numerator, denominator;

	          switch (type) {
	              case 1: // byte, 8-bit unsigned int
	              case 7: // undefined, 8-bit byte, value depending on field
	                  if (numValues == 1) {
	                      return file.getByteAt(entryOffset + 8, bigEnd);
	                  } else {
	                      offset = numValues > 4 ? valueOffset : (entryOffset + 8);
	                      vals = [];
	                      for (n = 0; n < numValues; n++) {
	                          vals[n] = file.getByteAt(offset + n);
	                      }
	                      return vals;
	                  }

	              case 2: // ascii, 8-bit byte
	                  offset = numValues > 4 ? valueOffset : (entryOffset + 8);
	                  return file.getStringAt(offset, numValues - 1);

	              case 3: // short, 16 bit int
	                  if (numValues == 1) {
	                      return file.getShortAt(entryOffset + 8, bigEnd);
	                  } else {
	                      offset = numValues > 2 ? valueOffset : (entryOffset + 8);
	                      vals = [];
	                      for (n = 0; n < numValues; n++) {
	                          vals[n] = file.getShortAt(offset + 2 * n, bigEnd);
	                      }
	                      return vals;
	                  }

	              case 4: // long, 32 bit int
	                  if (numValues == 1) {
	                      return file.getLongAt(entryOffset + 8, bigEnd);
	                  } else {
	                      vals = [];
	                      for (var n = 0; n < numValues; n++) {
	                          vals[n] = file.getLongAt(valueOffset + 4 * n, bigEnd);
	                      }
	                      return vals;
	                  }

	              case 5: // rational = two long values, first is numerator, second is denominator
	                  if (numValues == 1) {
	                      numerator = file.getLongAt(valueOffset, bigEnd);
	                      denominator = file.getLongAt(valueOffset + 4, bigEnd);
	                      val = new Number(numerator / denominator);
	                      val.numerator = numerator;
	                      val.denominator = denominator;
	                      return val;
	                  } else {
	                      vals = [];
	                      for (n = 0; n < numValues; n++) {
	                          numerator = file.getLongAt(valueOffset + 8 * n, bigEnd);
	                          denominator = file.getLongAt(valueOffset + 4 + 8 * n, bigEnd);
	                          vals[n] = new Number(numerator / denominator);
	                          vals[n].numerator = numerator;
	                          vals[n].denominator = denominator;
	                      }
	                      return vals;
	                  }

	              case 9: // slong, 32 bit signed int
	                  if (numValues == 1) {
	                      return file.getSLongAt(entryOffset + 8, bigEnd);
	                  } else {
	                      vals = [];
	                      for (n = 0; n < numValues; n++) {
	                          vals[n] = file.getSLongAt(valueOffset + 4 * n, bigEnd);
	                      }
	                      return vals;
	                  }

	              case 10: // signed rational, two slongs, first is numerator, second is denominator
	                  if (numValues == 1) {
	                      return file.getSLongAt(valueOffset, bigEnd) / file.getSLongAt(valueOffset + 4, bigEnd);
	                  } else {
	                      vals = [];
	                      for (n = 0; n < numValues; n++) {
	                          vals[n] = file.getSLongAt(valueOffset + 8 * n, bigEnd) / file.getSLongAt(valueOffset + 4 + 8 * n, bigEnd);
	                      }
	                      return vals;
	                  }
	          }
	      }


	      function readEXIFData(file, start) {
	          if (file.getStringAt(start, 4) != "Exif") {
	              if (debug)
	                  console.log("Not valid EXIF data! " + file.getStringAt(start, 4));
	              return false;
	          }

	          var bigEnd,
	                  tags, tag,
	                  exifData, gpsData,
	                  tiffOffset = start + 6;

	          // test for TIFF validity and endianness
	          if (file.getShortAt(tiffOffset) == 0x4949) {
	              bigEnd = false;
	          } else if (file.getShortAt(tiffOffset) == 0x4D4D) {
	              bigEnd = true;
	          } else {
	              if (debug)
	                  console.log("Not valid TIFF data! (no 0x4949 or 0x4D4D)");
	              return false;
	          }

	          if (file.getShortAt(tiffOffset + 2, bigEnd) != 0x002A) {
	              if (debug)
	                  console.log("Not valid TIFF data! (no 0x002A)");
	              return false;
	          }

	          if (file.getLongAt(tiffOffset + 4, bigEnd) != 0x00000008) {
	              if (debug)
	                  console.log("Not valid TIFF data! (First offset not 8)", file.getShortAt(tiffOffset + 4, bigEnd));
	              return false;
	          }

	          tags = readTags(file, tiffOffset, tiffOffset + 8, TiffTags, bigEnd);

	          if (tags.ExifIFDPointer) {
	              exifData = readTags(file, tiffOffset, tiffOffset + tags.ExifIFDPointer, ExifTags, bigEnd);
	              for (tag in exifData) {
	                  switch (tag) {
	                      case "LightSource" :
	                      case "Flash" :
	                      case "MeteringMode" :
	                      case "ExposureProgram" :
	                      case "SensingMethod" :
	                      case "SceneCaptureType" :
	                      case "SceneType" :
	                      case "CustomRendered" :
	                      case "WhiteBalance" :
	                      case "GainControl" :
	                      case "Contrast" :
	                      case "Saturation" :
	                      case "Sharpness" :
	                      case "SubjectDistanceRange" :
	                      case "FileSource" :
	                          exifData[tag] = StringValues[tag][exifData[tag]];
	                          break;

	                      case "ExifVersion" :
	                      case "FlashpixVersion" :
	                          exifData[tag] = String.fromCharCode(exifData[tag][0], exifData[tag][1], exifData[tag][2], exifData[tag][3]);
	                          break;

	                      case "ComponentsConfiguration" :
	                          exifData[tag] =
	                                  StringValues.Components[exifData[tag][0]]
	                                  + StringValues.Components[exifData[tag][1]]
	                                  + StringValues.Components[exifData[tag][2]]
	                                  + StringValues.Components[exifData[tag][3]];
	                          break;
	                  }
	                  tags[tag] = exifData[tag];
	              }
	          }

	          if (tags.GPSInfoIFDPointer) {
	              gpsData = readTags(file, tiffOffset, tiffOffset + tags.GPSInfoIFDPointer, GPSTags, bigEnd);
	              for (tag in gpsData) {
	                  switch (tag) {
	                      case "GPSVersionID" :
	                          gpsData[tag] = gpsData[tag][0]
	                                  + "." + gpsData[tag][1]
	                                  + "." + gpsData[tag][2]
	                                  + "." + gpsData[tag][3];
	                          break;
	                  }
	                  tags[tag] = gpsData[tag];
	              }
	          }

	          return tags;
	      }


	      function getData(img, callback) {
	          if (!img.complete)
	              return false;
	          if (!imageHasData(img)) {
	              getImageData(img, callback);
	          } else {
	              if (callback) {
	                  callback.call(img);
	              }
	          }
	          return true;
	      }

	      function getTag(img, tag) {
	          if (!imageHasData(img))
	              return;
	          return img.exifdata[tag];
	      }

	      function getAllTags(img) {
	          if (!imageHasData(img))
	              return {};
	          var a,
	                  data = img.exifdata,
	                  tags = {};
	          for (a in data) {
	              if (data.hasOwnProperty(a)) {
	                  tags[a] = data[a];
	              }
	          }
	          return tags;
	      }

	      function pretty(img) {
	          if (!imageHasData(img))
	              return "";
	          var a,
	                  data = img.exifdata,
	                  strPretty = "";
	          for (a in data) {
	              if (data.hasOwnProperty(a)) {
	                  if (typeof data[a] == "object") {
	                      if (data[a] instanceof Number) {
	                          strPretty += a + " : " + data[a] + " [" + data[a].numerator + "/" + data[a].denominator + "]\r\n";
	                      } else {
	                          strPretty += a + " : [" + data[a].length + " values]\r\n";
	                      }
	                  } else {
	                      strPretty += a + " : " + data[a] + "\r\n";
	                  }
	              }
	          }
	          return strPretty;
	      }

	      function readFromBinaryFile(file) {
	          return findEXIFinJPEG(file);
	      }


	      return {
	          readFromBinaryFile: readFromBinaryFile,
	          pretty: pretty,
	          getTag: getTag,
	          getAllTags: getAllTags,
	          getData: getData,
	          Tags: ExifTags,
	          TiffTags: TiffTags,
	          GPSTags: GPSTags,
	          StringValues: StringValues
	      };

	  })();

	  angular.module('ImageCropper',[])
	    .directive('imageCrop', function() {

	      return {
	        template: '<div id="image-crop-{{ rand }}" class="ng-image-crop ng-image-crop--{{ shape }}" ng-style="moduleStyles"><section ng-style="sectionStyles" ng-show="step==1"></section><section ng-style="sectionStyles" ng-show="step==2"><canvas class="cropping-canvas" width="{{ canvasWidth }}" height="{{ canvasHeight }}" ng-mousemove="onCanvasMouseMove($event)" ng-mousedown="onCanvasMouseDown($event)"></canvas><div ng-style="croppingGuideStyles" class="cropping-guide"></div><div class="zoom-handle" ng-mousemove="onHandleMouseMove($event)" ng-mousedown="onHandleMouseDown($event)" ng-mouseup="onHandleMouseUp($event)"><span>&larr; zoom &rarr;</span></div></section><section ng-style="sectionStyles" class="image-crop-section-final" ng-show="step==3"><img class="image-crop-final" ng-src="{{ croppedDataUri }}" /></section></div>',
	        replace: true,
	        restrict: 'AE',
	        scope: {
			  crop: '=',
	          width: '@',
	          height: '@',
	          shape: '@',
			  src: '=',
	          resultBlob: '=',
			  result: '=',
	          step: '=',
	          padding: '@',
			  maxSize: '@'
	        },
	        link: function (scope, element, attributes) {
			  
			  var padding = scope.padding ? Number(scope.padding) : 200;
			  
	          scope.rand = Math.round(Math.random() * 99999);
	          scope.step = scope.step || 1;
	          scope.shape = scope.shape || 'circle';
	          scope.width = parseInt(scope.width, 10) || 300;
	          scope.height = parseInt(scope.height, 10) || 300;

	          scope.canvasWidth = scope.width + padding;
	          scope.canvasHeight = scope.height + padding;

	          var $elm = element[0];

	          var $canvas = $elm.getElementsByClassName('cropping-canvas')[0];
	          var $handle = $elm.getElementsByClassName('zoom-handle')[0];
	          var $finalImg = $elm.getElementsByClassName('image-crop-final')[0];
	          var $img = new Image();
	          var fileReader = new FileReader();

	          var maxLeft = 0, minLeft = 0, maxTop = 0, minTop = 0, imgLoaded = false, imgWidth = 0, imgHeight = 0;
	          var currentX = 0, currentY = 0, dragging = false, startX = 0, startY = 0, zooming = false;
	          var newWidth = imgWidth, newHeight = imgHeight;
	          var targetX = 0, targetY = 0;
	          var zoom = 1;
	          var maxZoomGestureLength = 0;
	          var maxZoomedInLevel = 0, maxZoomedOutLevel = 2;
	          var minXPos = 0, maxXPos = (padding/2), minYPos = 0, maxYPos = (padding/2); // for dragging bounds		  
			  var maxSize = scope.maxSize ? Number(scope.maxSize) : null; //max size of the image in px
			  
	          var zoomWeight = .6;
	          var ctx = $canvas.getContext('2d');
	          var exif = null;
	          var files = [];

	          // ---------- INLINE STYLES ----------- //
	          scope.moduleStyles = {
	            width: (scope.width + padding) + 'px',
	            height: (scope.height + padding) + 'px'
	          };

	          scope.sectionStyles = {
	            width: (scope.width + padding) + 'px',
	            height: (scope.height + padding) + 'px'
	          };

	          scope.croppingGuideStyles = {
	            width: scope.width + 'px',
	            height: scope.height + 'px',
	            top: (padding/2)+'px',
	            left: (padding/2)+'px'
	          };
	  		  
			  function handleSize(base64ImageSrc) {
			  
				return new Promise(function(resolve, reject) {
					
					if(!maxSize) {
						return resolve(base64ImageSrc);
					}
					
					var img = new Image();
					img.src = base64ImageSrc;
					
					img.onload = function() {
					
						var height = img.height;
						var width = img.width;
																
						//if the size is already ok, just return the image
						if(height <= maxSize && width <= maxSize) {						
							return resolve(base64ImageSrc);
						}			 	
						
						var ratio = width/height;
						
						if(ratio > 1) {
							width = maxSize;
							height = maxSize/ratio;
						}
						else {
							width = maxSize*ratio;
							height = maxSize;
						}							
						
						width = Math.round(width);
						height = Math.round(height);			 	
						
						var canvas = document.createElement("canvas");
						canvas.width = width;
						canvas.height = height;			 	
						
						var context = canvas.getContext("2d");
										
						context.drawImage(img, 0, 0, img.width,    img.height,      // source
											   0, 0, canvas.width, canvas.height);  // destination	 
						
						context.save();
									
						var dataUrl = canvas.toDataURL();
						
						resolve(dataUrl);

					};
						
				});		  
					
			  }
						
			  function handleEXIF(base64ImageSrc, exif) {
			  		
				return new Promise(function(resolve, reject) {
									
					var img = new Image();
					img.src = base64ImageSrc;
					
					img.onload = function() {
					
						var canvas = document.createElement("canvas");
						
						if(exif.Orientation >= 5) {
							canvas.width = img.height;
							canvas.height = img.width;
						} else {
							canvas.width = img.width;
							canvas.height = img.height;
						}
						
						var context = canvas.getContext("2d");
			
						// change mobile orientation, if required
						switch(exif.Orientation){
							case 1:
								// nothing
								break;
							case 2:
								// horizontal flip
								context.translate(img.width, 0);
								context.scale(-1, 1);
								break;
							case 3:
								// 180 rotate left
								context.translate(img.width, img.height);
								context.rotate(Math.PI);
								break;
							case 4:
								// vertical flip
								context.translate(0, img.height);
								context.scale(1, -1);
								break;
							case 5:
								// vertical flip + 90 rotate right
								context.rotate(0.5 * Math.PI);
								context.scale(1, -1);
								break;
							case 6:
								// 90 rotate right
								context.rotate(0.5 * Math.PI);
								context.translate(0, -img.height);
								break;
							case 7:
								// horizontal flip + 90 rotate right
								context.rotate(0.5 * Math.PI);
								context.translate(img.width, -img.height);
								context.scale(-1, 1);
								break;
							case 8:
								// 90 rotate left					 		                   
								context.rotate(-0.5 * Math.PI);
								context.translate(-img.width, 0);
								break;
							default:
								break;
						}
						
						context.drawImage(img, 0, 0);	
						context.save();
						
						var dataUrl = canvas.toDataURL();
						
						resolve(dataUrl);										
					
					};
					
				});				
					
			  }
			  
			  function loadImage(base64ImageSrc) {
			  
				//get the EXIF information from the image
	            var byteString = atob(base64ImageSrc.split(',')[1]);
	            var binary = new BinaryFile(byteString, 0, byteString.length);
	            exif = EXIF.readFromBinaryFile(binary);		  
	           
			    //handle image size
	            handleSize(base64ImageSrc).then(function(base64ImageSrc) {
				
					//if the image has EXIF orientation..
					if (exif && exif.Orientation && exif.Orientation > 1) {			
						return handleEXIF(base64ImageSrc, exif);
					} 
					//otherwise, just return the image without any treatment
					else {
						return base64ImageSrc;
					}
					
				}).then(function(base64ImageSrc) {
				
					$img.src = base64ImageSrc;
					
				}).catch(function(error) {							
					console.log(error);				
				});    
				
			  };
			  
	          // ---------- EVENT HANDLERS ---------- //
	          fileReader.onload = function(e) {
	          	
	          	loadImage(this.resultBlob);	

	          };	  

	          $img.onload = function() {
			  
				scope.step = 2;
				scope.$apply();		  
				
	            ctx.drawImage($img, 0, 0);

	            imgWidth = $img.width;
	            imgHeight = $img.height;

	            minLeft = (scope.width + padding) - this.width;
	            minTop = (scope.height + padding) - this.height;
	            newWidth = imgWidth;
	            newHeight = imgHeight;
	            
				if(imgWidth >= imgHeight) {
					maxZoomedInLevel = ($canvas.height - padding) / imgHeight;
				} else {
					maxZoomedInLevel = ($canvas.width - padding) / imgWidth;
				}		

	            maxZoomGestureLength = to2Dp(Math.sqrt(Math.pow($canvas.width, 2) + Math.pow($canvas.height, 2)));

	            updateDragBounds();
				
				var initialX = Math.round((minXPos + maxXPos)/2);
				var initialY = Math.round((minYPos + maxYPos)/2);
							
				moveImage(initialX, initialY);
				
	          };
			  
	          function reset() {
	            files = [];
	            zoom = 1;
				currentX = 0; 
				currentY = 0; 
				dragging = false; 
				startX = 0; 
				startY = 0; 
				zooming = false;
	            ctx.clearRect(0, 0, $canvas.width, $canvas.height);            
	            $img.src = '';
	          }		  

	          // ---------- PRIVATE FUNCTIONS ---------- //
	          function moveImage(x, y) {
				
				x = x < minXPos ? minXPos : x;
				x = x > maxXPos ? maxXPos : x;
				y = y < minYPos ? minYPos : y;
				y = y > maxYPos ? maxYPos : y;			

	            targetX = x;
	            targetY = y;
				
	            ctx.clearRect(0, 0, $canvas.width, $canvas.height);
	            ctx.drawImage($img, x, y, newWidth, newHeight);
				
				return x == minXPos || x == maxXPos || y == minYPos || y == maxYPos;
	          }

	          function to2Dp(val) {
	            return Math.round(val * 1000) / 1000;
	          }

	          function updateDragBounds() {
	            // $img.width, $canvas.width, zoom

	            minXPos = $canvas.width - ($img.width * zoom) - (padding/2);
	            minYPos = $canvas.height - ($img.height * zoom) - (padding/2);

	          }

	          function zoomImage(val) {

	            if (!val) {
	              return;
	            }
				
	            var proposedZoomLevel = to2Dp(zoom + val);
				
	            if ((proposedZoomLevel < maxZoomedInLevel) || (proposedZoomLevel > maxZoomedOutLevel)) {
	              // image wont fill whole canvas
	              // or image is too far zoomed in, it's gonna get pretty pixelated!
	              return;
	            }

	            zoom = proposedZoomLevel;
	            // console.log('zoom', zoom);

	            updateDragBounds();

	            newWidth = $img.width * zoom;
	            newHeight = $img.height * zoom;

	            var newXPos = currentX * zoom;
	            var newYPos = currentY * zoom;

	            // check if we've exposed the gutter
	            if (newXPos < minXPos) {
	              newXPos = minXPos;
	            } else if (newXPos > maxXPos) {
	              newXPos = maxXPos;
	            }

	            if (newYPos < minYPos) {
	              newYPos = minYPos;
	            } else if (newYPos > maxYPos) {
	              newYPos = maxYPos;
	            }

	            // check if image is still going to fit the bounds of the box
	            ctx.clearRect(0, 0, $canvas.width, $canvas.height);
	            ctx.drawImage($img, newXPos, newYPos, newWidth, newHeight);
	          }

	          function calcZoomLevel(diffX, diffY) {

	            var hyp = Math.sqrt( Math.pow(diffX, 2) + Math.pow(diffY, 2) );
	            var zoomGestureRatio = to2Dp(hyp / maxZoomGestureLength);
	            var newZoomDiff = to2Dp((maxZoomedOutLevel - maxZoomedInLevel) * zoomGestureRatio * zoomWeight);
	            return diffX > 0 ? -newZoomDiff : newZoomDiff;
				
	          }
	          
			  function dataURItoBlob(dataURI) {
				    var byteString, 
				        mimestring;
				
				    if(dataURI.split(',')[0].indexOf('base64') !== -1 ) {
				        byteString = atob(dataURI.split(',')[1]);
				    } else {
				        byteString = decodeURI(dataURI.split(',')[1]);
				    }
				
				    mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0];
				
				    var content = new Array();
				    for (var i = 0; i < byteString.length; i++) {
				        content[i] = byteString.charCodeAt(i);
				    }
				
				    return new Blob([new Uint8Array(content)], {type: mimestring});
			  }       

	          // ---------- SCOPE FUNCTIONS ---------- //

			  scope.$watch('src', function(){
				if(scope.src) {
					if(scope.step != 3) {
						if(typeof(scope.src) == 'Blob') {
							fileReader.readAsDataURL(scope.src);	
						} else {
							loadImage(scope.src);
						}
					}		
				} else {
					scope.step = 1;
					reset();
				}
			  });	

			  scope.$watch('crop',function(){
				if(scope.crop) {
					scope.doCrop();
					scope.crop = false;
				}
			  });	
			  
	          $finalImg.onload = function() {			
	            var tempCanvas = document.createElement('canvas');
	            tempCanvas.width = this.width - padding;
	            tempCanvas.height = this.height - padding;
	            tempCanvas.style.display = 'none';

	            var tempCanvasContext = tempCanvas.getContext('2d');
	            tempCanvasContext.drawImage($finalImg, -(padding/2), -(padding/2));

	            $elm.getElementsByClassName('image-crop-section-final')[0].appendChild(tempCanvas);
				
				var dataUrl = tempCanvas.toDataURL();
				
				scope.result = dataUrl;
	            scope.resultBlob = dataURItoBlob(dataUrl);
	            
	            scope.$apply();
	          };

	          scope.doCrop = function() {
	            scope.croppedDataUri = $canvas.toDataURL();
	            scope.step = 3;
	          };

	          scope.onCanvasMouseUp = function(e) {

	            if (!dragging) {
	              return;
	            }

	            e.preventDefault();
	            e.stopPropagation(); // if event was on canvas, stop it propagating up

	            startX = 0;
	            startY = 0;
	            dragging = false;
	            currentX = targetX;
	            currentY = targetY;

	            removeBodyEventListener('mouseup', scope.onCanvasMouseUp);
	            removeBodyEventListener('touchend', scope.onCanvasMouseUp);
	            removeBodyEventListener('mousemove', scope.onCanvasMouseMove);
	            removeBodyEventListener('touchmove', scope.onCanvasMouseMove);
	          };

	          $canvas.addEventListener('touchend', scope.onCanvasMouseUp, false);

	          scope.onCanvasMouseDown = function(e) {
	            startX = e.type === 'touchstart' ? e.changedTouches[0].clientX : e.clientX;
	            startY = e.type === 'touchstart' ? e.changedTouches[0].clientY : e.clientY;
	            zooming = false;
	            dragging = true;

	            addBodyEventListener('mouseup', scope.onCanvasMouseUp);
	            addBodyEventListener('mousemove', scope.onCanvasMouseMove);
	          };

	          $canvas.addEventListener('touchstart', scope.onCanvasMouseDown, false);

	          function addBodyEventListener(eventName, func) {
	            document.documentElement.addEventListener(eventName, func, false);
	          }

	          function removeBodyEventListener(eventName, func) {
	            document.documentElement.removeEventListener(eventName, func);
	          }

	          scope.onHandleMouseDown = function(e) {

	            e.preventDefault();
	            e.stopPropagation(); // if event was on handle, stop it propagating up

	            startX = lastHandleX = (e.type === 'touchstart') ? e.changedTouches[0].clientX : e.clientX;
	            startY = lastHandleY = (e.type === 'touchstart') ? e.changedTouches[0].clientY : e.clientY;
	            dragging = false;
	            zooming = true;

	            addBodyEventListener('mouseup', scope.onHandleMouseUp);
	            addBodyEventListener('touchend', scope.onHandleMouseUp);
	            addBodyEventListener('mousemove', scope.onHandleMouseMove);
	            addBodyEventListener('touchmove', scope.onHandleMouseMove);
				
	          };

	          $handle.addEventListener('touchstart', scope.onHandleMouseDown, false);

	          scope.onHandleMouseUp = function(e) {

	            // this is applied on the whole section so check we're zooming
	            if (!zooming) {
	              return;
	            }

	            e.preventDefault();
	            e.stopPropagation(); // if event was on canvas, stop it propagating up

	            startX = 0;
	            startY = 0;
	            zooming = false;
	            currentX = targetX;
	            currentY = targetY;

	            removeBodyEventListener('mouseup', scope.onHandleMouseUp);
	            removeBodyEventListener('touchend', scope.onHandleMouseUp);
	            removeBodyEventListener('mousemove', scope.onHandleMouseMove);
	            removeBodyEventListener('touchmove', scope.onHandleMouseMove);
	          };

	          $handle.addEventListener('touchend', scope.onHandleMouseUp, false);

	          scope.onCanvasMouseMove = function(e) {

	            e.preventDefault();
	            e.stopPropagation();

	            if (!dragging) {
	              return;
	            }

	            var diffX = startX - ((e.type === 'touchmove') ? e.changedTouches[0].clientX : e.clientX); // how far mouse has moved in current drag
	            var diffY = startY - ((e.type === 'touchmove') ? e.changedTouches[0].clientY : e.clientY); // how far mouse has moved in current drag
	            /*targetX = currentX - diffX; // desired new X position
	            targetY = currentY - diffY; // desired new X position*/

	            moveImage(currentX - diffX, currentY - diffY);

	          };

	          $canvas.addEventListener('touchmove', scope.onCanvasMouseMove, false);

	          var lastHandleX = null, lastHandleY = null;

	          scope.onHandleMouseMove = function(e) {

	            e.stopPropagation();
	            e.preventDefault();

	            // this is applied on the whole section so check we're zooming
	            if (!zooming) {
	              return false;
	            }

	            var diffX = lastHandleX - ((e.type === 'touchmove') ? e.changedTouches[0].clientX : e.clientX); // how far mouse has moved in current drag
	            var diffY = lastHandleY - ((e.type === 'touchmove') ? e.changedTouches[0].clientY : e.clientY); // how far mouse has moved in current drag

	            lastHandleX = (e.type === 'touchmove') ? e.changedTouches[0].clientX : e.clientX;
	            lastHandleY = (e.type === 'touchmove') ? e.changedTouches[0].clientY : e.clientY;

	            var zoomVal = calcZoomLevel(diffX, diffY);			
	            zoomImage(zoomVal);

	          };

	          $handle.addEventListener('touchmove', scope.onHandleMouseMove, false);	  		 
			  	  		  
			  scope.onHandleMouseWheel = function(e){
				  e.preventDefault();		  
				  
				  zoomImage(e.deltaY > 0 ? -0.05 : 0.05);			  
			  };

			  $canvas.addEventListener('mousewheel', scope.onHandleMouseWheel);
			  $handle.addEventListener('mousewheel', scope.onHandleMouseWheel);

	        }
	      };
	    });


	})();

/***/ },
/* 146 */
/***/ function(module, exports) {

	var app = angular.module("directive");

	app.directive('errImage', function() {
	    return {
	        link: function(scope, element, attrs) {
	            // console.log(".............", attrs.errImage, attrs.src);
	            if (!attrs.src) {
	                attrs.$set('src', attrs.errImage);
	            }
	            
	            element.bind('error', function(e) {
	                if (attrs.src != attrs.errImage) {
	                    attrs.errImage = attrs.errImage || "/assets/images/dummy-user-pic.png"
	                    attrs.$set('src', attrs.errImage);
	                }
	            });
	        }
	    }
	});

/***/ },
/* 147 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module) {!function(){"use strict";function t(){var t={parent:document.body,version:"1.0.12",defaultOkLabel:"Ok",okLabel:"Ok",defaultCancelLabel:"Cancel",cancelLabel:"Cancel",defaultMaxLogItems:2,maxLogItems:2,promptValue:"",promptPlaceholder:"",closeLogOnClick:!1,closeLogOnClickDefault:!1,delay:5e3,defaultDelay:5e3,logContainerClass:"alertify-logs",logContainerDefaultClass:"alertify-logs",dialogs:{buttons:{holder:"<nav>{{buttons}}</nav>",ok:"<button class='ok' tabindex='1'>{{ok}}</button>",cancel:"<button class='cancel' tabindex='2'>{{cancel}}</button>"},input:"<input type='text'>",message:"<p class='msg'>{{message}}</p>",log:"<div class='{{class}}'>{{message}}</div>"},defaultDialogs:{buttons:{holder:"<nav>{{buttons}}</nav>",ok:"<button class='ok' tabindex='1'>{{ok}}</button>",cancel:"<button class='cancel' tabindex='2'>{{cancel}}</button>"},input:"<input type='text'>",message:"<p class='msg'>{{message}}</p>",log:"<div class='{{class}}'>{{message}}</div>"},build:function(t){var e=this.dialogs.buttons.ok,o="<div class='dialog'><div>"+this.dialogs.message.replace("{{message}}",t.message);return"confirm"!==t.type&&"prompt"!==t.type||(e=this.dialogs.buttons.cancel+this.dialogs.buttons.ok),"prompt"===t.type&&(o+=this.dialogs.input),o=(o+this.dialogs.buttons.holder+"</div></div>").replace("{{buttons}}",e).replace("{{ok}}",this.okLabel).replace("{{cancel}}",this.cancelLabel)},setCloseLogOnClick:function(t){this.closeLogOnClick=!!t},close:function(t,e){this.closeLogOnClick&&t.addEventListener("click",function(){o(t)}),e=e&&!isNaN(+e)?+e:this.delay,0>e?o(t):e>0&&setTimeout(function(){o(t)},e)},dialog:function(t,e,o,n){return this.setup({type:e,message:t,onOkay:o,onCancel:n})},log:function(t,e,o){var n=document.querySelectorAll(".alertify-logs > div");if(n){var i=n.length-this.maxLogItems;if(i>=0)for(var a=0,l=i+1;l>a;a++)this.close(n[a],-1)}this.notify(t,e,o)},setLogPosition:function(t){this.logContainerClass="alertify-logs "+t},setupLogContainer:function(){var t=document.querySelector(".alertify-logs"),e=this.logContainerClass;return t||(t=document.createElement("div"),t.className=e,this.parent.appendChild(t)),t.className!==e&&(t.className=e),t},notify:function(e,o,n){var i=this.setupLogContainer(),a=document.createElement("div");a.className=o||"default",t.logTemplateMethod?a.innerHTML=t.logTemplateMethod(e):a.innerHTML=e,"function"==typeof n&&a.addEventListener("click",n),i.appendChild(a),setTimeout(function(){a.className+=" show"},10),this.close(a,this.delay)},setup:function(t){function e(e){"function"!=typeof e&&(e=function(){}),i&&i.addEventListener("click",function(i){t.onOkay&&"function"==typeof t.onOkay&&(l?t.onOkay(l.value,i):t.onOkay(i)),e(l?{buttonClicked:"ok",inputValue:l.value,event:i}:{buttonClicked:"ok",event:i}),o(n)}),a&&a.addEventListener("click",function(i){t.onCancel&&"function"==typeof t.onCancel&&t.onCancel(i),e({buttonClicked:"cancel",event:i}),o(n)}),l&&l.addEventListener("keyup",function(t){13===t.which&&i.click()})}var n=document.createElement("div");n.className="alertify hide",n.innerHTML=this.build(t);var i=n.querySelector(".ok"),a=n.querySelector(".cancel"),l=n.querySelector("input"),s=n.querySelector("label");l&&("string"==typeof this.promptPlaceholder&&(s?s.textContent=this.promptPlaceholder:l.placeholder=this.promptPlaceholder),"string"==typeof this.promptValue&&(l.value=this.promptValue));var r;return"function"==typeof Promise?r=new Promise(e):e(),this.parent.appendChild(n),setTimeout(function(){n.classList.remove("hide"),l&&t.type&&"prompt"===t.type?(l.select(),l.focus()):i&&i.focus()},100),r},okBtn:function(t){return this.okLabel=t,this},setDelay:function(t){return t=t||0,this.delay=isNaN(t)?this.defaultDelay:parseInt(t,10),this},cancelBtn:function(t){return this.cancelLabel=t,this},setMaxLogItems:function(t){this.maxLogItems=parseInt(t||this.defaultMaxLogItems)},theme:function(t){switch(t.toLowerCase()){case"bootstrap":this.dialogs.buttons.ok="<button class='ok btn btn-primary' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel btn btn-default' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<input type='text' class='form-control'>";break;case"purecss":this.dialogs.buttons.ok="<button class='ok pure-button' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel pure-button' tabindex='2'>{{cancel}}</button>";break;case"mdl":case"material-design-light":this.dialogs.buttons.ok="<button class='ok mdl-button mdl-js-button mdl-js-ripple-effect'  tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel mdl-button mdl-js-button mdl-js-ripple-effect' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<div class='mdl-textfield mdl-js-textfield'><input class='mdl-textfield__input'><label class='md-textfield__label'></label></div>";break;case"angular-material":this.dialogs.buttons.ok="<button class='ok md-primary md-button' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel md-button' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<div layout='column'><md-input-container md-no-float><input type='text'></md-input-container></div>";break;case"default":default:this.dialogs.buttons.ok=this.defaultDialogs.buttons.ok,this.dialogs.buttons.cancel=this.defaultDialogs.buttons.cancel,this.dialogs.input=this.defaultDialogs.input}},reset:function(){this.parent=document.body,this.theme("default"),this.okBtn(this.defaultOkLabel),this.cancelBtn(this.defaultCancelLabel),this.setMaxLogItems(),this.promptValue="",this.promptPlaceholder="",this.delay=this.defaultDelay,this.setCloseLogOnClick(this.closeLogOnClickDefault),this.setLogPosition("bottom left"),this.logTemplateMethod=null},injectCSS:function(){if(!document.querySelector("#alertifyCSS")){var t=document.getElementsByTagName("head")[0],e=document.createElement("style");e.type="text/css",e.id="alertifyCSS",e.innerHTML=".alertify-logs>*{padding:12px 24px;color:#fff;box-shadow:0 2px 5px 0 rgba(0,0,0,.2);border-radius:1px}.alertify-logs>*,.alertify-logs>.default{background:rgba(0,0,0,.8)}.alertify-logs>.error{background:rgba(244,67,54,.8)}.alertify-logs>.success{background:rgba(76,175,80,.9)}.alertify{position:fixed;background-color:rgba(0,0,0,.3);left:0;right:0;top:0;bottom:0;width:100%;height:100%;z-index:1}.alertify.hide{opacity:0;pointer-events:none}.alertify,.alertify.show{box-sizing:border-box;transition:all .33s cubic-bezier(.25,.8,.25,1)}.alertify,.alertify *{box-sizing:border-box}.alertify .dialog{padding:12px}.alertify .alert,.alertify .dialog{width:100%;margin:0 auto;position:relative;top:50%;transform:translateY(-50%)}.alertify .alert>*,.alertify .dialog>*{width:400px;max-width:95%;margin:0 auto;text-align:center;padding:12px;background:#fff;box-shadow:0 2px 4px -1px rgba(0,0,0,.14),0 4px 5px 0 rgba(0,0,0,.098),0 1px 10px 0 rgba(0,0,0,.084)}.alertify .alert .msg,.alertify .dialog .msg{padding:12px;margin-bottom:12px;margin:0;text-align:left}.alertify .alert input:not(.form-control),.alertify .dialog input:not(.form-control){margin-bottom:15px;width:100%;font-size:100%;padding:12px}.alertify .alert input:not(.form-control):focus,.alertify .dialog input:not(.form-control):focus{outline-offset:-2px}.alertify .alert nav,.alertify .dialog nav{text-align:right}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button),.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button){background:transparent;box-sizing:border-box;color:rgba(0,0,0,.87);position:relative;outline:0;border:0;display:inline-block;-ms-flex-align:center;-ms-grid-row-align:center;align-items:center;padding:0 6px;margin:6px 8px;line-height:36px;min-height:36px;white-space:nowrap;min-width:88px;text-align:center;text-transform:uppercase;font-size:14px;text-decoration:none;cursor:pointer;border:1px solid transparent;border-radius:2px}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover{background-color:rgba(0,0,0,.05)}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus{border:1px solid rgba(0,0,0,.1)}.alertify .alert nav button.btn,.alertify .dialog nav button.btn{margin:6px 4px}.alertify-logs{position:fixed;z-index:1}.alertify-logs.bottom,.alertify-logs:not(.top){bottom:16px}.alertify-logs.left,.alertify-logs:not(.right){left:16px}.alertify-logs.left>*,.alertify-logs:not(.right)>*{float:left;transform:translateZ(0);height:auto}.alertify-logs.left>.show,.alertify-logs:not(.right)>.show{left:0}.alertify-logs.left>*,.alertify-logs.left>.hide,.alertify-logs:not(.right)>*,.alertify-logs:not(.right)>.hide{left:-110%}.alertify-logs.right{right:16px}.alertify-logs.right>*{float:right;transform:translateZ(0)}.alertify-logs.right>.show{right:0;opacity:1}.alertify-logs.right>*,.alertify-logs.right>.hide{right:-110%;opacity:0}.alertify-logs.top{top:0}.alertify-logs>*{box-sizing:border-box;transition:all .4s cubic-bezier(.25,.8,.25,1);position:relative;clear:both;backface-visibility:hidden;perspective:1000;max-height:0;margin:0;padding:0;overflow:hidden;opacity:0;pointer-events:none}.alertify-logs>.show{margin-top:12px;opacity:1;max-height:1000px;padding:12px;pointer-events:auto}",t.insertBefore(e,t.firstChild)}},removeCSS:function(){var t=document.querySelector("#alertifyCSS");t&&t.parentNode&&t.parentNode.removeChild(t)}};return t.injectCSS(),{_$$alertify:t,parent:function(e){t.parent=e},reset:function(){return t.reset(),this},alert:function(e,o,n){return t.dialog(e,"alert",o,n)||this},confirm:function(e,o,n){return t.dialog(e,"confirm",o,n)||this},prompt:function(e,o,n){return t.dialog(e,"prompt",o,n)||this},log:function(e,o){return t.log(e,"default",o),this},theme:function(e){return t.theme(e),this},success:function(e,o){return t.log(e,"success",o),this},error:function(e,o){return t.log(e,"error",o),this},cancelBtn:function(e){return t.cancelBtn(e),this},okBtn:function(e){return t.okBtn(e),this},delay:function(e){return t.setDelay(e),this},placeholder:function(e){return t.promptPlaceholder=e,this},defaultValue:function(e){return t.promptValue=e,this},maxLogItems:function(e){return t.setMaxLogItems(e),this},closeLogOnClick:function(e){return t.setCloseLogOnClick(!!e),this},logPosition:function(e){return t.setLogPosition(e||""),this},setLogTemplate:function(e){return t.logTemplateMethod=e,this},clearLogs:function(){return t.setupLogContainer().innerHTML="",this},version:t.version}}var e=500,o=function(t){if(t){var o=function(){t&&t.parentNode&&t.parentNode.removeChild(t)};t.classList.remove("show"),t.classList.add("hide"),t.addEventListener("transitionend",o),setTimeout(o,e)}};if("undefined"!=typeof module&&module&&module.exports){module.exports=function(){return new t};var n=new t;for(var i in n)module.exports[i]=n[i]}else true?!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){return new t}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):window.alertify=new t}();
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(117)(module)))

/***/ },
/* 148 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;angular.module("ngAlertify",[]).factory("alertify",function(){"use strict";var t={exports:!0};!function(){function e(){var t={parent:document.body,version:"1.0.12",defaultOkLabel:"Ok",okLabel:"Ok",defaultCancelLabel:"Cancel",cancelLabel:"Cancel",defaultMaxLogItems:2,maxLogItems:2,promptValue:"",promptPlaceholder:"",closeLogOnClick:!1,closeLogOnClickDefault:!1,delay:5e3,defaultDelay:5e3,logContainerClass:"alertify-logs",logContainerDefaultClass:"alertify-logs",dialogs:{buttons:{holder:"<nav>{{buttons}}</nav>",ok:"<button class='ok' tabindex='1'>{{ok}}</button>",cancel:"<button class='cancel' tabindex='2'>{{cancel}}</button>"},input:"<input type='text'>",message:"<p class='msg'>{{message}}</p>",log:"<div class='{{class}}'>{{message}}</div>"},defaultDialogs:{buttons:{holder:"<nav>{{buttons}}</nav>",ok:"<button class='ok' tabindex='1'>{{ok}}</button>",cancel:"<button class='cancel' tabindex='2'>{{cancel}}</button>"},input:"<input type='text'>",message:"<p class='msg'>{{message}}</p>",log:"<div class='{{class}}'>{{message}}</div>"},build:function(t){var e=this.dialogs.buttons.ok,o="<div class='dialog'><div>"+this.dialogs.message.replace("{{message}}",t.message);return"confirm"!==t.type&&"prompt"!==t.type||(e=this.dialogs.buttons.cancel+this.dialogs.buttons.ok),"prompt"===t.type&&(o+=this.dialogs.input),o=(o+this.dialogs.buttons.holder+"</div></div>").replace("{{buttons}}",e).replace("{{ok}}",this.okLabel).replace("{{cancel}}",this.cancelLabel)},setCloseLogOnClick:function(t){this.closeLogOnClick=!!t},close:function(t,e){this.closeLogOnClick&&t.addEventListener("click",function(){n(t)}),e=e&&!isNaN(+e)?+e:this.delay,0>e?n(t):e>0&&setTimeout(function(){n(t)},e)},dialog:function(t,e,o,n){return this.setup({type:e,message:t,onOkay:o,onCancel:n})},log:function(t,e,o){var n=document.querySelectorAll(".alertify-logs > div");if(n){var i=n.length-this.maxLogItems;if(i>=0)for(var a=0,l=i+1;l>a;a++)this.close(n[a],-1)}this.notify(t,e,o)},setLogPosition:function(t){this.logContainerClass="alertify-logs "+t},setupLogContainer:function(){var t=document.querySelector(".alertify-logs"),e=this.logContainerClass;return t||(t=document.createElement("div"),t.className=e,this.parent.appendChild(t)),t.className!==e&&(t.className=e),t},notify:function(e,o,n){var i=this.setupLogContainer(),a=document.createElement("div");a.className=o||"default",t.logTemplateMethod?a.innerHTML=t.logTemplateMethod(e):a.innerHTML=e,"function"==typeof n&&a.addEventListener("click",n),i.appendChild(a),setTimeout(function(){a.className+=" show"},10),this.close(a,this.delay)},setup:function(t){function e(e){"function"!=typeof e&&(e=function(){}),i&&i.addEventListener("click",function(i){t.onOkay&&"function"==typeof t.onOkay&&(l?t.onOkay(l.value,i):t.onOkay(i)),e(l?{buttonClicked:"ok",inputValue:l.value,event:i}:{buttonClicked:"ok",event:i}),n(o)}),a&&a.addEventListener("click",function(i){t.onCancel&&"function"==typeof t.onCancel&&t.onCancel(i),e({buttonClicked:"cancel",event:i}),n(o)}),l&&l.addEventListener("keyup",function(t){13===t.which&&i.click()})}var o=document.createElement("div");o.className="alertify hide",o.innerHTML=this.build(t);var i=o.querySelector(".ok"),a=o.querySelector(".cancel"),l=o.querySelector("input"),r=o.querySelector("label");l&&("string"==typeof this.promptPlaceholder&&(r?r.textContent=this.promptPlaceholder:l.placeholder=this.promptPlaceholder),"string"==typeof this.promptValue&&(l.value=this.promptValue));var s;return"function"==typeof Promise?s=new Promise(e):e(),this.parent.appendChild(o),setTimeout(function(){o.classList.remove("hide"),l&&t.type&&"prompt"===t.type?(l.select(),l.focus()):i&&i.focus()},100),s},okBtn:function(t){return this.okLabel=t,this},setDelay:function(t){return t=t||0,this.delay=isNaN(t)?this.defaultDelay:parseInt(t,10),this},cancelBtn:function(t){return this.cancelLabel=t,this},setMaxLogItems:function(t){this.maxLogItems=parseInt(t||this.defaultMaxLogItems)},theme:function(t){switch(t.toLowerCase()){case"bootstrap":this.dialogs.buttons.ok="<button class='ok btn btn-primary' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel btn btn-default' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<input type='text' class='form-control'>";break;case"purecss":this.dialogs.buttons.ok="<button class='ok pure-button' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel pure-button' tabindex='2'>{{cancel}}</button>";break;case"mdl":case"material-design-light":this.dialogs.buttons.ok="<button class='ok mdl-button mdl-js-button mdl-js-ripple-effect'  tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel mdl-button mdl-js-button mdl-js-ripple-effect' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<div class='mdl-textfield mdl-js-textfield'><input class='mdl-textfield__input'><label class='md-textfield__label'></label></div>";break;case"angular-material":this.dialogs.buttons.ok="<button class='ok md-primary md-button' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel md-button' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<div layout='column'><md-input-container md-no-float><input type='text'></md-input-container></div>";break;case"default":default:this.dialogs.buttons.ok=this.defaultDialogs.buttons.ok,this.dialogs.buttons.cancel=this.defaultDialogs.buttons.cancel,this.dialogs.input=this.defaultDialogs.input}},reset:function(){this.parent=document.body,this.theme("default"),this.okBtn(this.defaultOkLabel),this.cancelBtn(this.defaultCancelLabel),this.setMaxLogItems(),this.promptValue="",this.promptPlaceholder="",this.delay=this.defaultDelay,this.setCloseLogOnClick(this.closeLogOnClickDefault),this.setLogPosition("bottom left"),this.logTemplateMethod=null},injectCSS:function(){if(!document.querySelector("#alertifyCSS")){var t=document.getElementsByTagName("head")[0],e=document.createElement("style");e.type="text/css",e.id="alertifyCSS",e.innerHTML=".alertify-logs>*{padding:12px 24px;color:#fff;box-shadow:0 2px 5px 0 rgba(0,0,0,.2);border-radius:1px}.alertify-logs>*,.alertify-logs>.default{background:rgba(0,0,0,.8)}.alertify-logs>.error{background:rgba(244,67,54,.8)}.alertify-logs>.success{background:rgba(76,175,80,.9)}.alertify{position:fixed;background-color:rgba(0,0,0,.3);left:0;right:0;top:0;bottom:0;width:100%;height:100%;z-index:1}.alertify.hide{opacity:0;pointer-events:none}.alertify,.alertify.show{box-sizing:border-box;transition:all .33s cubic-bezier(.25,.8,.25,1)}.alertify,.alertify *{box-sizing:border-box}.alertify .dialog{padding:12px}.alertify .alert,.alertify .dialog{width:100%;margin:0 auto;position:relative;top:50%;transform:translateY(-50%)}.alertify .alert>*,.alertify .dialog>*{width:400px;max-width:95%;margin:0 auto;text-align:center;padding:12px;background:#fff;box-shadow:0 2px 4px -1px rgba(0,0,0,.14),0 4px 5px 0 rgba(0,0,0,.098),0 1px 10px 0 rgba(0,0,0,.084)}.alertify .alert .msg,.alertify .dialog .msg{padding:12px;margin-bottom:12px;margin:0;text-align:left}.alertify .alert input:not(.form-control),.alertify .dialog input:not(.form-control){margin-bottom:15px;width:100%;font-size:100%;padding:12px}.alertify .alert input:not(.form-control):focus,.alertify .dialog input:not(.form-control):focus{outline-offset:-2px}.alertify .alert nav,.alertify .dialog nav{text-align:right}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button),.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button){background:transparent;box-sizing:border-box;color:rgba(0,0,0,.87);position:relative;outline:0;border:0;display:inline-block;-ms-flex-align:center;-ms-grid-row-align:center;align-items:center;padding:0 6px;margin:6px 8px;line-height:36px;min-height:36px;white-space:nowrap;min-width:88px;text-align:center;text-transform:uppercase;font-size:14px;text-decoration:none;cursor:pointer;border:1px solid transparent;border-radius:2px}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover{background-color:rgba(0,0,0,.05)}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus{border:1px solid rgba(0,0,0,.1)}.alertify .alert nav button.btn,.alertify .dialog nav button.btn{margin:6px 4px}.alertify-logs{position:fixed;z-index:1}.alertify-logs.bottom,.alertify-logs:not(.top){bottom:16px}.alertify-logs.left,.alertify-logs:not(.right){left:16px}.alertify-logs.left>*,.alertify-logs:not(.right)>*{float:left;transform:translateZ(0);height:auto}.alertify-logs.left>.show,.alertify-logs:not(.right)>.show{left:0}.alertify-logs.left>*,.alertify-logs.left>.hide,.alertify-logs:not(.right)>*,.alertify-logs:not(.right)>.hide{left:-110%}.alertify-logs.right{right:16px}.alertify-logs.right>*{float:right;transform:translateZ(0)}.alertify-logs.right>.show{right:0;opacity:1}.alertify-logs.right>*,.alertify-logs.right>.hide{right:-110%;opacity:0}.alertify-logs.top{top:0}.alertify-logs>*{box-sizing:border-box;transition:all .4s cubic-bezier(.25,.8,.25,1);position:relative;clear:both;backface-visibility:hidden;perspective:1000;max-height:0;margin:0;padding:0;overflow:hidden;opacity:0;pointer-events:none}.alertify-logs>.show{margin-top:12px;opacity:1;max-height:1000px;padding:12px;pointer-events:auto}",t.insertBefore(e,t.firstChild)}},removeCSS:function(){var t=document.querySelector("#alertifyCSS");t&&t.parentNode&&t.parentNode.removeChild(t)}};return t.injectCSS(),{_$alertify:t,parent:function(e){t.parent=e},reset:function(){return t.reset(),this},alert:function(e,o,n){return t.dialog(e,"alert",o,n)||this},confirm:function(e,o,n){return t.dialog(e,"confirm",o,n)||this},prompt:function(e,o,n){return t.dialog(e,"prompt",o,n)||this},log:function(e,o){return t.log(e,"default",o),this},theme:function(e){return t.theme(e),this},success:function(e,o){return t.log(e,"success",o),this},error:function(e,o){return t.log(e,"error",o),this},cancelBtn:function(e){return t.cancelBtn(e),this},okBtn:function(e){return t.okBtn(e),this},delay:function(e){return t.setDelay(e),this},placeholder:function(e){return t.promptPlaceholder=e,this},defaultValue:function(e){return t.promptValue=e,this},maxLogItems:function(e){return t.setMaxLogItems(e),this},closeLogOnClick:function(e){return t.setCloseLogOnClick(!!e),this},logPosition:function(e){return t.setLogPosition(e||""),this},setLogTemplate:function(e){return t.logTemplateMethod=e,this},clearLogs:function(){return t.setupLogContainer().innerHTML="",this},version:t.version}}var o=500,n=function(t){if(t){var e=function(){t&&t.parentNode&&t.parentNode.removeChild(t)};t.classList.remove("show"),t.classList.add("hide"),t.addEventListener("transitionend",e),setTimeout(e,o)}};if("undefined"!=typeof t&&t&&t.exports){t.exports=function(){return new e};var i=new e;for(var a in i)t.exports[a]=i[a]}else true?!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){return new e}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):window.alertify=new e}();var e=t.exports;return new e});

/***/ },
/* 149 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(150);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../css-loader/index.js!./alertify.css", function() {
				var newContent = require("!!../../../css-loader/index.js!./alertify.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 150 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, ".alertify-logs>*{padding:12px 24px;color:#fff;box-shadow:0 2px 5px 0 rgba(0,0,0,.2);border-radius:1px}.alertify-logs>*,.alertify-logs>.default{background:rgba(0,0,0,.8)}.alertify-logs>.error{background:rgba(244,67,54,.8)}.alertify-logs>.success{background:rgba(76,175,80,.9)}.alertify{position:fixed;background-color:rgba(0,0,0,.3);left:0;right:0;top:0;bottom:0;width:100%;height:100%;z-index:1}.alertify.hide{opacity:0;pointer-events:none}.alertify,.alertify.show{box-sizing:border-box;transition:all .33s cubic-bezier(.25,.8,.25,1)}.alertify,.alertify *{box-sizing:border-box}.alertify .dialog{padding:12px}.alertify .alert,.alertify .dialog{width:100%;margin:0 auto;position:relative;top:50%;transform:translateY(-50%)}.alertify .alert>*,.alertify .dialog>*{width:400px;max-width:95%;margin:0 auto;text-align:center;padding:12px;background:#fff;box-shadow:0 2px 4px -1px rgba(0,0,0,.14),0 4px 5px 0 rgba(0,0,0,.098),0 1px 10px 0 rgba(0,0,0,.084)}.alertify .alert .msg,.alertify .dialog .msg{padding:12px;margin-bottom:12px;margin:0;text-align:left}.alertify .alert input:not(.form-control),.alertify .dialog input:not(.form-control){margin-bottom:15px;width:100%;font-size:100%;padding:12px}.alertify .alert input:not(.form-control):focus,.alertify .dialog input:not(.form-control):focus{outline-offset:-2px}.alertify .alert nav,.alertify .dialog nav{text-align:right}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button),.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button){background:transparent;box-sizing:border-box;color:rgba(0,0,0,.87);position:relative;outline:0;border:0;display:inline-block;-ms-flex-align:center;-ms-grid-row-align:center;align-items:center;padding:0 6px;margin:6px 8px;line-height:36px;min-height:36px;white-space:nowrap;min-width:88px;text-align:center;text-transform:uppercase;font-size:14px;text-decoration:none;cursor:pointer;border:1px solid transparent;border-radius:2px}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover{background-color:rgba(0,0,0,.05)}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus{border:1px solid rgba(0,0,0,.1)}.alertify .alert nav button.btn,.alertify .dialog nav button.btn{margin:6px 4px}.alertify-logs{position:fixed;z-index:1}.alertify-logs.bottom,.alertify-logs:not(.top){bottom:16px}.alertify-logs.left,.alertify-logs:not(.right){left:16px}.alertify-logs.left>*,.alertify-logs:not(.right)>*{float:left;transform:translateZ(0);height:auto}.alertify-logs.left>.show,.alertify-logs:not(.right)>.show{left:0}.alertify-logs.left>*,.alertify-logs.left>.hide,.alertify-logs:not(.right)>*,.alertify-logs:not(.right)>.hide{left:-110%}.alertify-logs.right{right:16px}.alertify-logs.right>*{float:right;transform:translateZ(0)}.alertify-logs.right>.show{right:0;opacity:1}.alertify-logs.right>*,.alertify-logs.right>.hide{right:-110%;opacity:0}.alertify-logs.top{top:0}.alertify-logs>*{box-sizing:border-box;transition:all .4s cubic-bezier(.25,.8,.25,1);position:relative;clear:both;backface-visibility:hidden;perspective:1000;max-height:0;margin:0;padding:0;overflow:hidden;opacity:0;pointer-events:none}.alertify-logs>.show{margin-top:12px;opacity:1;max-height:1000px;padding:12px;pointer-events:auto}", ""]);

	// exports


/***/ },
/* 151 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	__webpack_require__(38);
	var Base64 = __webpack_require__(58).Base64;
	angular.module("service")
	    .factory('chatUtility', chatUtility);
	chatUtility.$inject = ['localStorageService'];
	function chatUtility(localStorageService) {
	    var chatUtility = {
	        isOpenDialog: function(val) {
	        	localStorageService.set('chatUtility', {'isOpenDialog':val});
	        },
	        openChatList: function(user_id){
	        	var obj = localStorageService.get('chatUtility');
	    		if (obj.userList!=undefined) {
	    			obj.userList.push(String(user_id));
	        		localStorageService.set('chatUtility', obj);
	        	}
	        	else{
	        		obj.userList = [];
	        		obj.userList[0] = user_id;
		        	localStorageService.set('chatUtility', obj);
	        	}
	        },
	        getInfo: function(){
	            return localStorageService.get('chatUtility');
	        },
	        deleteMember: function(user_id){
	            console.log(user_id);
	            var obj = localStorageService.get('chatUtility');
	            var a = obj.userList.indexOf(String(user_id));
	            obj.userList.splice(a,1);
	            console.log(obj.userList);
	            localStorageService.set('chatUtility', obj);
	        },
	        clearChatInfo: function(){
	            localStorageService.remove('chatUtility');
	        }
	    }
	    return chatUtility;
	};

/***/ },
/* 152 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "audios/notification.mp3";

/***/ },
/* 153 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(154);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_chat.scss", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_chat.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 154 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.chat-component {\n  font-family: 'aller';\n  position: fixed;\n  bottom: 0px;\n  right: 10px;\n  z-index: 9999;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: end;\n  -moz-box-align: end;\n  box-align: end;\n  -webkit-align-items: flex-end;\n  -moz-align-items: flex-end;\n  -ms-align-items: flex-end;\n  -o-align-items: flex-end;\n  align-items: flex-end;\n  -ms-flex-align: end;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n  .chat-component .chat-friends {\n    min-width: 300px;\n    border: 1px solid #1a89bb;\n    border-bottom: 0px;\n    border-top-left-radius: 5px;\n    border-top-right-radius: 5px;\n    margin: 0px 5px;\n    -webkit-box-ordinal-group: 1;\n    -moz-box-ordinal-group: 1;\n    box-ordinal-group: 1;\n    -webkit-order: 1;\n    -moz-order: 1;\n    order: 1;\n    -ms-flex-order: 1; }\n    .chat-component .chat-friends .head-wrapper {\n      background-color: #1a89bb;\n      height: 40px;\n      color: white;\n      position: relative;\n      border-top-left-radius: 3px;\n      border-top-right-radius: 3px;\n      padding: 5px 10px; }\n      .chat-component .chat-friends .head-wrapper .title {\n        font-size: 17px; }\n      .chat-component .chat-friends .head-wrapper .icons-wrapper {\n        position: absolute;\n        right: 0px;\n        top: 0px;\n        line-height: 40px;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .chat-friends .head-wrapper .icons-wrapper .icon {\n          padding: 0px 10px;\n          cursor: pointer; }\n    .chat-component .chat-friends .search-wrapper {\n      border: 0px;\n      font-family: 'nunito',sans-serif; }\n      .chat-component .chat-friends .search-wrapper input {\n        height: 40px;\n        box-shadow: 0px 0px 0px;\n        padding: 0px 10px;\n        outline: 0px;\n        width: 100%;\n        font-family: 'nunito',sans-serif; }\n    .chat-component .chat-friends .search-modal-wrapper {\n      border-bottom: 1px solid #ccc; }\n      .chat-component .chat-friends .search-modal-wrapper input {\n        height: 40px;\n        box-shadow: 0px 0px 0px;\n        padding: 0px 10px;\n        outline: 0px;\n        width: 100%; }\n    .chat-component .chat-friends .friend-list-wrapper {\n      min-height: 260px;\n      max-height: 260px;\n      overflow: auto;\n      background: white; }\n      .chat-component .chat-friends .friend-list-wrapper .friend {\n        padding: 5px 10px;\n        border-bottom: 1px solid #ddd;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .chat-friends .friend-list-wrapper .friend:last-child {\n          border-bottom: 0px; }\n      .chat-component .chat-friends .friend-list-wrapper .audio-icon, .chat-component .chat-friends .friend-list-wrapper .video-icon {\n        cursor: pointer;\n        margin: 5px;\n        color: #1a89bb; }\n        .chat-component .chat-friends .friend-list-wrapper .audio-icon.disabled, .chat-component .chat-friends .friend-list-wrapper .video-icon.disabled {\n          color: #e5e5e5;\n          pointer-events: none; }\n      .chat-component .chat-friends .friend-list-wrapper .pic {\n        height: 30px;\n        width: 30px;\n        margin: 5px;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex; }\n        .chat-component .chat-friends .friend-list-wrapper .pic img {\n          width: 100%;\n          height: 100%;\n          border-radius: 50%; }\n      .chat-component .chat-friends .friend-list-wrapper .name {\n        font-size: 15px;\n        text-overflow: ellipsis;\n        overflow: hidden;\n        width: 0px;\n        white-space: nowrap;\n        cursor: pointer;\n        font-family: 'nunito',sans-serif;\n        -webkit-box-flex: 1;\n        -moz-box-flex: 1;\n        box-flex: 1;\n        -webkit-flex: 1;\n        -moz-flex: 1;\n        -ms-flex: 1;\n        flex: 1; }\n      .chat-component .chat-friends .friend-list-wrapper .unread {\n        display: inline-block;\n        margin: 0;\n        text-align: center;\n        font-size: 12px;\n        padding: 1px 5px;\n        border-radius: 5px;\n        background-color: #1d99d1;\n        color: white;\n        margin-right: 5px; }\n  .chat-component .groupFrndLine {\n    line-height: 22px;\n    background: #1d99d1;\n    color: white;\n    padding: 0px 10px;\n    font-family: 'nunito',sans-serif;\n    font-weight: bold; }\n  .chat-component .width-500 {\n    width: 500px !important; }\n  .chat-component .chat-modal-search {\n    width: 100%;\n    padding: 12px; }\n  .chat-component .height-500 {\n    height: 500px !important; }\n  .chat-component .status {\n    width: 10px;\n    height: 10px;\n    margin: 10px;\n    border-radius: 50%;\n    display: inline-block; }\n    .chat-component .status.online {\n      background-color: #2ea677; }\n    .chat-component .status.offline {\n      background-color: #D24D57; }\n  .chat-component .chat-msgs-individual {\n    min-width: 300px;\n    max-width: 300px;\n    max-height: 350px;\n    border: 1px solid #1a89bb;\n    border-bottom: 0px;\n    border-top-left-radius: 5px;\n    border-top-right-radius: 5px;\n    margin: 0px 5px;\n    position: relative;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -moz-box-orient: vertical;\n    box-orient: vertical;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: column;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    -ms-flex-direction: column; }\n    .chat-component .chat-msgs-individual .head-wrapper {\n      background-color: #1a89bb;\n      min-height: 40px;\n      color: white;\n      position: relative;\n      border-top-left-radius: 3px;\n      border-top-right-radius: 3px;\n      padding: 5px 10px;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; }\n      .chat-component .chat-msgs-individual .head-wrapper.unread-color {\n        background-color: #87D37C !important; }\n      .chat-component .chat-msgs-individual .head-wrapper .pic {\n        width: 30px;\n        height: 30px;\n        display: inline-block; }\n        .chat-component .chat-msgs-individual .head-wrapper .pic img {\n          width: 100%;\n          height: 100%;\n          border-radius: 50%; }\n      .chat-component .chat-msgs-individual .head-wrapper .title {\n        font-size: 17px;\n        margin-left: 10px;\n        width: 130px;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: nowrap; }\n        .chat-component .chat-msgs-individual .head-wrapper .title .status {\n          margin: 0px;\n          margin-left: 10px; }\n      .chat-component .chat-msgs-individual .head-wrapper .icons-wrapper {\n        position: absolute;\n        right: 0px;\n        top: 0px;\n        line-height: 40px;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .chat-msgs-individual .head-wrapper .icons-wrapper .icon {\n          padding: 0px 10px;\n          cursor: pointer; }\n    .chat-component .chat-msgs-individual .msgs-wrapper {\n      min-height: 200px;\n      overflow: auto;\n      background: #eee;\n      position: relative;\n      margin-bottom: 0px;\n      -webkit-box-flex: 1;\n      -moz-box-flex: 1;\n      box-flex: 1;\n      -webkit-flex: 1;\n      -moz-flex: 1;\n      -ms-flex: 1;\n      flex: 1; }\n      .chat-component .chat-msgs-individual .msgs-wrapper .msg {\n        margin: 5px 0px;\n        max-width: 90%;\n        background-color: #fff;\n        clear: both;\n        padding: 0px 10px;\n        word-wrap: break-word;\n        min-width: 40%;\n        position: relative;\n        border-radius: 3px;\n        border: 1px solid #ccc; }\n        .chat-component .chat-msgs-individual .msgs-wrapper .msg .time {\n          text-align: right;\n          font-size: 12px; }\n        .chat-component .chat-msgs-individual .msgs-wrapper .msg .text {\n          font-size: 15px; }\n        .chat-component .chat-msgs-individual .msgs-wrapper .msg .frm-usr {\n          font-weight: bold; }\n        .chat-component .chat-msgs-individual .msgs-wrapper .msg.left {\n          float: left;\n          border-top-right-radius: 3px;\n          border-bottom-right-radius: 3px;\n          box-shadow: 1px 1px 1px #ccc;\n          margin-left: 10px; }\n          .chat-component .chat-msgs-individual .msgs-wrapper .msg.left:before {\n            content: '';\n            position: absolute;\n            border-right: 10px solid #fff;\n            border-top: 10px solid transparent;\n            left: -10px;\n            bottom: 10px;\n            box-shadow: 0px 1px 0px #ccc; }\n        .chat-component .chat-msgs-individual .msgs-wrapper .msg.right {\n          float: right;\n          border-top-left-radius: 3px;\n          border-bottom-left-radius: 3px;\n          background-color: #DCF8C6;\n          color: #222;\n          box-shadow: 1px 1px 1px #ccc;\n          margin-right: 10px; }\n          .chat-component .chat-msgs-individual .msgs-wrapper .msg.right:before {\n            content: '';\n            position: absolute;\n            border-left: 10px solid #DCF8C6;\n            border-top: 10px solid transparent;\n            right: -10px;\n            bottom: 10px;\n            box-shadow: 0px 1px 0px #ccc; }\n    .chat-component .chat-msgs-individual .grp-member-wrapper {\n      background: #eee;\n      max-height: 50px;\n      overflow: auto;\n      border-bottom: 1px solid #eee;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex; }\n      .chat-component .chat-msgs-individual .grp-member-wrapper .grp-member {\n        padding: 10px;\n        background: #eee; }\n        .chat-component .chat-msgs-individual .grp-member-wrapper .grp-member .status {\n          margin: 0px;\n          margin-left: 10px; }\n    .chat-component .chat-msgs-individual .offline-msg {\n      position: relative;\n      background-color: #bbb;\n      left: 0px;\n      top: 0px;\n      right: 0px;\n      padding: 10px;\n      color: white;\n      font-size: 14px; }\n    .chat-component .chat-msgs-individual .success-add-msg {\n      background: #2ea677;\n      color: white;\n      padding: 10px; }\n    .chat-component .chat-msgs-individual .grp-field {\n      position: relative;\n      background-color: #ccc;\n      left: 0px;\n      top: 0px;\n      right: 0px;\n      padding: 10px;\n      color: #555;\n      font-size: 14px;\n      max-width: 300px;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: center;\n      -moz-box-pack: center;\n      box-pack: center;\n      -webkit-justify-content: center;\n      -moz-justify-content: center;\n      -ms-justify-content: center;\n      -o-justify-content: center;\n      justify-content: center;\n      -ms-flex-pack: center;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; }\n      .chat-component .chat-msgs-individual .grp-field .ui-select-container {\n        background: white;\n        background: white !important; }\n      .chat-component .chat-msgs-individual .grp-field .ui-select-search {\n        padding: 0px 10px;\n        box-shadow: 0px 0px 0px;\n        border-radius: 2px;\n        margin-right: 5px;\n        padding: 0px 5px;\n        width: 150px !important;\n        -webkit-box-flex: 1;\n        -moz-box-flex: 1;\n        box-flex: 1;\n        -webkit-flex: 1;\n        -moz-flex: 1;\n        -ms-flex: 1;\n        flex: 1; }\n      .chat-component .chat-msgs-individual .grp-field .add-btn {\n        background: #1d99d1;\n        height: 30px;\n        padding: 0px 10px;\n        display: inline-block;\n        color: white;\n        margin-left: 10px;\n        border-radius: 3px;\n        cursor: pointer;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center; }\n    .chat-component .chat-msgs-individual .msg-field {\n      border-top: 1px solid #ccc;\n      color: #222;\n      min-height: 60px;\n      padding: 0px 10px;\n      left: 0px;\n      right: 0px;\n      bottom: 0px;\n      background: white;\n      max-height: 150px;\n      overflow: auto; }\n      .chat-component .chat-msgs-individual .msg-field .textfield {\n        word-spacing: 2px;\n        left: 10px;\n        right: 10px;\n        top: 0px;\n        bottom: 0px;\n        outline: none;\n        word-break: break-word;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row;\n        -webkit-box-flex: 1;\n        -moz-box-flex: 1;\n        box-flex: 1;\n        -webkit-flex: 1;\n        -moz-flex: 1;\n        -ms-flex: 1;\n        flex: 1;\n        min-height: inherit;\n        align-items: flex-start;\n        padding: 10px 0px; }\n      .chat-component .chat-msgs-individual .msg-field .gray-msg {\n        color: #ccc; }\n      .chat-component .chat-msgs-individual .msg-field .textfield[placeholder]:empty:before {\n        content: attr(placeholder);\n        color: #ccc; }\n      .chat-component .chat-msgs-individual .msg-field .textfield[placeholder]:empty:focus:before {\n        content: \"\"; }\n  .chat-component .image-modal {\n    position: fixed;\n    top: 0px;\n    left: 0px;\n    right: 0px;\n    bottom: 0px;\n    background: rgba(0, 0, 0, 0.7);\n    z-index: 9999;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: vertical;\n    -moz-box-orient: vertical;\n    box-orient: vertical;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: column;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    -ms-flex-direction: column; }\n  .chat-component .create-grp-modal {\n    position: fixed;\n    top: 0px;\n    left: 0px;\n    right: 0px;\n    bottom: 0px;\n    background: rgba(0, 0, 0, 0.4);\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .chat-component .create-grp-modal .grp-form-wrapper {\n      width: 50%;\n      background-color: #f4f4f4;\n      padding: 10px;\n      border-radius: 3px;\n      position: relative;\n      max-width: 500px; }\n    .chat-component .create-grp-modal .close-btn {\n      position: absolute;\n      right: 10px;\n      top: 10px;\n      font-size: 20px;\n      cursor: pointer; }\n    .chat-component .create-grp-modal .title {\n      font-size: 20px;\n      font-family: 'nunito',sans-serif;\n      font-weight: bold;\n      margin-bottom: 20px;\n      border-bottom: 1px solid #ccc;\n      padding-bottom: 10px; }\n    .chat-component .create-grp-modal input[type=text] {\n      background: white;\n      padding: 0px 10px;\n      border-radius: 3px;\n      border: 1px solid #e5e5e5; }\n    .chat-component .create-grp-modal .ui-select-container {\n      border: 0px;\n      box-shadow: 0px 0px 0px;\n      height: 100%;\n      background: white;\n      padding: 10px;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center; }\n    .chat-component .create-grp-modal .upload-btn {\n      border: 1px solid #ccc;\n      cursor: pointer; }\n    .chat-component .create-grp-modal .grp-pic {\n      display: inline-block;\n      height: 50px;\n      width: 50px;\n      margin-top: 10px;\n      border-radius: 3px; }\n    .chat-component .create-grp-modal .create-btn {\n      display: inline-flex;\n      float: right; }\n  .chat-component .audio-modal-wrapper {\n    position: fixed;\n    left: 0px;\n    right: 0px;\n    top: 0px;\n    bottom: 0px;\n    background: rgba(0, 0, 0, 0.2);\n    z-index: 10000;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .chat-component .audio-modal-wrapper .audio-call {\n      width: 80%;\n      position: relative; }\n      .chat-component .audio-modal-wrapper .audio-call .toolbar {\n        height: 35px;\n        width: 100%;\n        border-top-right-radius: 5px;\n        border-top-left-radius: 5px;\n        background: #e2e2e2;\n        padding: 0px 10px;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: justify;\n        -moz-box-pack: justify;\n        box-pack: justify;\n        -webkit-justify-content: space-between;\n        -moz-justify-content: space-between;\n        -ms-justify-content: space-between;\n        -o-justify-content: space-between;\n        justify-content: space-between;\n        -ms-flex-pack: justify;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .audio-modal-wrapper .audio-call .toolbar .close-icon {\n          background: #D24D57;\n          padding: 2px;\n          float: right;\n          margin-right: 10px;\n          height: 20px;\n          width: 20px;\n          border-radius: 3px;\n          cursor: pointer;\n          display: -webkit-box;\n          display: -moz-box;\n          display: box;\n          display: -webkit-flex;\n          display: -moz-flex;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n          -moz-box-align: center;\n          box-align: center;\n          -webkit-align-items: center;\n          -moz-align-items: center;\n          -ms-align-items: center;\n          -o-align-items: center;\n          align-items: center;\n          -ms-flex-align: center;\n          -webkit-box-pack: center;\n          -moz-box-pack: center;\n          box-pack: center;\n          -webkit-justify-content: center;\n          -moz-justify-content: center;\n          -ms-justify-content: center;\n          -o-justify-content: center;\n          justify-content: center;\n          -ms-flex-pack: center;\n          -webkit-box-orient: horizontal;\n          -moz-box-orient: horizontal;\n          box-orient: horizontal;\n          -webkit-box-direction: normal;\n          -moz-box-direction: normal;\n          box-direction: normal;\n          -webkit-flex-direction: row;\n          -moz-flex-direction: row;\n          flex-direction: row;\n          -ms-flex-direction: row; }\n          .chat-component .audio-modal-wrapper .audio-call .toolbar .close-icon i {\n            color: white;\n            font-size: 15px; }\n      .chat-component .audio-modal-wrapper .audio-call .content {\n        background: #222;\n        min-height: 200px;\n        position: relative;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .audio-modal-wrapper .audio-call .content video {\n          width: 1px;\n          height: 1px;\n          position: absolute; }\n        .chat-component .audio-modal-wrapper .audio-call .content .middle-msg-wrapper {\n          font-size: 20px;\n          font-family: waverly;\n          color: white; }\n        .chat-component .audio-modal-wrapper .audio-call .content .right-side-pic {\n          position: absolute;\n          right: 0px; }\n        .chat-component .audio-modal-wrapper .audio-call .content .right-side-pic, .chat-component .audio-modal-wrapper .audio-call .content .left-side-pic {\n          width: 160px;\n          height: 160px;\n          margin: 20px;\n          background: #ddd;\n          border-radius: 5px;\n          top: 0px; }\n        .chat-component .audio-modal-wrapper .audio-call .content .left-side-pic {\n          position: absolute;\n          left: 0px; }\n        .chat-component .audio-modal-wrapper .audio-call .content .signal {\n          font-size: 70px;\n          margin-top: -50px; }\n        .chat-component .audio-modal-wrapper .audio-call .content .one {\n          opacity: 0;\n          -webkit-animation: dot 1.6s infinite;\n          -webkit-animation-delay: 0.0s;\n          animation: dot 1.6s infinite;\n          animation-delay: 0.0s;\n          color: #1a89bb; }\n        .chat-component .audio-modal-wrapper .audio-call .content .two {\n          opacity: 0;\n          -webkit-animation: dot 1.6s infinite;\n          -webkit-animation-delay: 0.2s;\n          animation: dot 1.6s infinite;\n          animation-delay: 0.2s;\n          color: #1a89bb; }\n        .chat-component .audio-modal-wrapper .audio-call .content .three {\n          opacity: 0;\n          -webkit-animation: dot 1.6s infinite;\n          -webkit-animation-delay: 0.3s;\n          animation: dot 1.6s infinite;\n          animation-delay: 0.3s;\n          color: #2ea677; }\n        .chat-component .audio-modal-wrapper .audio-call .content .four {\n          opacity: 0;\n          -webkit-animation: dot 163s infinite;\n          -webkit-animation-delay: 0.4s;\n          animation: dot 1.6s infinite;\n          animation-delay: 0.4s;\n          color: #2ea677; }\n        .chat-component .audio-modal-wrapper .audio-call .content .five {\n          opacity: 0;\n          -webkit-animation: dot 1.6s infinite;\n          -webkit-animation-delay: 0.5s;\n          animation: dot 1.6s infinite;\n          animation-delay: 0.5s;\n          color: #D24D57; }\n        .chat-component .audio-modal-wrapper .audio-call .content .six {\n          opacity: 0;\n          -webkit-animation: dot 1.6s infinite;\n          -webkit-animation-delay: 0.6s;\n          animation: dot 1.6s infinite;\n          animation-delay: 0.6s;\n          color: #D24D57; }\n\n@-webkit-keyframes dot {\n  0% {\n    opacity: 0; }\n  50% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n\n@keyframes dot {\n  0% {\n    opacity: 0; }\n  50% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n      .chat-component .audio-modal-wrapper .audio-call .action-btns {\n        position: absolute;\n        left: 0px;\n        right: 0px;\n        bottom: 10px;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .audio-modal-wrapper .audio-call .action-btns .act-btn {\n          padding: 5px 15px;\n          background: #1d99d1;\n          color: white;\n          font-family: waverly;\n          display: inline-block;\n          border-radius: 3px;\n          cursor: pointer;\n          margin: 0px 5px; }\n  .chat-component .video-modal-wrapper {\n    position: fixed;\n    left: 0px;\n    right: 0px;\n    top: 0px;\n    bottom: 0px;\n    background: rgba(0, 0, 0, 0.2);\n    z-index: 10000;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .chat-component .video-modal-wrapper .video-call {\n      width: 90%;\n      position: relative; }\n      .chat-component .video-modal-wrapper .video-call .toolbar {\n        height: 35px;\n        width: 100%;\n        border-top-right-radius: 5px;\n        border-top-left-radius: 5px;\n        background: #e2e2e2;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: justify;\n        -moz-box-pack: justify;\n        box-pack: justify;\n        -webkit-justify-content: space-between;\n        -moz-justify-content: space-between;\n        -ms-justify-content: space-between;\n        -o-justify-content: space-between;\n        justify-content: space-between;\n        -ms-flex-pack: justify;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .video-modal-wrapper .video-call .toolbar .close-icon {\n          background: #D24D57;\n          padding: 2px;\n          float: right;\n          margin-right: 10px;\n          height: 20px;\n          width: 20px;\n          border-radius: 3px;\n          cursor: pointer;\n          display: -webkit-box;\n          display: -moz-box;\n          display: box;\n          display: -webkit-flex;\n          display: -moz-flex;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n          -moz-box-align: center;\n          box-align: center;\n          -webkit-align-items: center;\n          -moz-align-items: center;\n          -ms-align-items: center;\n          -o-align-items: center;\n          align-items: center;\n          -ms-flex-align: center;\n          -webkit-box-pack: center;\n          -moz-box-pack: center;\n          box-pack: center;\n          -webkit-justify-content: center;\n          -moz-justify-content: center;\n          -ms-justify-content: center;\n          -o-justify-content: center;\n          justify-content: center;\n          -ms-flex-pack: center;\n          -webkit-box-orient: horizontal;\n          -moz-box-orient: horizontal;\n          box-orient: horizontal;\n          -webkit-box-direction: normal;\n          -moz-box-direction: normal;\n          box-direction: normal;\n          -webkit-flex-direction: row;\n          -moz-flex-direction: row;\n          flex-direction: row;\n          -ms-flex-direction: row; }\n          .chat-component .video-modal-wrapper .video-call .toolbar .close-icon i {\n            color: white;\n            font-size: 15px; }\n      .chat-component .video-modal-wrapper .video-call .content {\n        background: #222;\n        height: 90vh;\n        position: relative;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .video-modal-wrapper .video-call .content .big-video {\n          margin: 0px;\n          width: 100%;\n          height: 100%; }\n        .chat-component .video-modal-wrapper .video-call .content video {\n          width: 100%;\n          height: 100%; }\n        .chat-component .video-modal-wrapper .video-call .content .small-video {\n          position: absolute;\n          bottom: 15px;\n          right: 15px;\n          width: 250px;\n          height: 250px;\n          background: #e5e5e5;\n          border-radius: 5px; }\n        .chat-component .video-modal-wrapper .video-call .content .middle-msg-wrapper {\n          position: absolute;\n          left: 0px;\n          right: 0px;\n          top: 0px;\n          bottom: 0px;\n          z-index: 1;\n          color: white;\n          font-family: waverly;\n          font-size: 20px;\n          display: -webkit-box;\n          display: -moz-box;\n          display: box;\n          display: -webkit-flex;\n          display: -moz-flex;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n          -moz-box-align: center;\n          box-align: center;\n          -webkit-align-items: center;\n          -moz-align-items: center;\n          -ms-align-items: center;\n          -o-align-items: center;\n          align-items: center;\n          -ms-flex-align: center;\n          -webkit-box-pack: center;\n          -moz-box-pack: center;\n          box-pack: center;\n          -webkit-justify-content: center;\n          -moz-justify-content: center;\n          -ms-justify-content: center;\n          -o-justify-content: center;\n          justify-content: center;\n          -ms-flex-pack: center;\n          -webkit-box-orient: horizontal;\n          -moz-box-orient: horizontal;\n          box-orient: horizontal;\n          -webkit-box-direction: normal;\n          -moz-box-direction: normal;\n          box-direction: normal;\n          -webkit-flex-direction: row;\n          -moz-flex-direction: row;\n          flex-direction: row;\n          -ms-flex-direction: row; }\n      .chat-component .video-modal-wrapper .video-call .action-btns {\n        position: absolute;\n        left: 0px;\n        right: 0px;\n        bottom: 10px;\n        z-index: 2;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .chat-component .video-modal-wrapper .video-call .action-btns .act-btn {\n          padding: 5px 15px;\n          background: #1d99d1;\n          color: white;\n          font-family: waverly;\n          display: inline-block;\n          border-radius: 3px;\n          cursor: pointer; }\n  .chat-component .chat-modal {\n    position: fixed;\n    top: 0px;\n    left: 0px;\n    right: 0px;\n    bottom: 0px;\n    background: rgba(0, 0, 0, 0.7);\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: vertical;\n    -moz-box-orient: vertical;\n    box-orient: vertical;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: column;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    -ms-flex-direction: column; }\n  .chat-component .chat-action-bar {\n    width: 100%;\n    background: #eee;\n    height: 40px;\n    border-bottom: 1px solid #ddd;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: end;\n    -moz-box-pack: end;\n    box-pack: end;\n    -webkit-justify-content: flex-end;\n    -moz-justify-content: flex-end;\n    -ms-justify-content: flex-end;\n    -o-justify-content: flex-end;\n    justify-content: flex-end;\n    -ms-flex-pack: end;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .chat-component .chat-action-bar .icon {\n      cursor: pointer;\n      color: #444;\n      padding: 0px 10px; }\n  .chat-component .chat-modal-content {\n    background: #eee;\n    min-height: calc(100vh - 40px);\n    width: 100%;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex; }\n  .chat-component .chat-members {\n    min-width: 250px;\n    width: 250px;\n    max-height: 95vh;\n    overflow: auto; }\n    .chat-component .chat-members .create-grp {\n      display: inline-block;\n      cursor: pointer;\n      width: 100%;\n      border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n      padding: 10px;\n      background: #eee;\n      color: #444;\n      font-size: 14px;\n      text-align: center;\n      height: 50px; }\n      .chat-component .chat-members .create-grp i {\n        margin-right: 5px; }\n    .chat-component .chat-members .member {\n      padding: 3px;\n      border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n      display: inline-block;\n      cursor: pointer;\n      width: 100%;\n      position: relative; }\n      .chat-component .chat-members .member i {\n        margin-right: 5px; }\n      .chat-component .chat-members .member .unread {\n        display: inline-block;\n        margin: 0;\n        text-align: center;\n        font-size: 12px;\n        padding: 1px 5px;\n        border-radius: 5px;\n        background-color: #1d99d1;\n        color: white;\n        margin-right: 5px; }\n      .chat-component .chat-members .member.active {\n        background: white; }\n      .chat-component .chat-members .member .modal-pic {\n        height: 30px;\n        width: 100%;\n        margin: 5px 5px;\n        position: relative;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex; }\n        .chat-component .chat-members .member .modal-pic img {\n          width: 12%;\n          height: 100%; }\n      .chat-component .chat-members .member .title {\n        font-size: 14px;\n        margin-left: 10px;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: nowrap; }\n        .chat-component .chat-members .member .title .status {\n          margin: 0px;\n          position: absolute;\n          top: 10px;\n          right: 10px; }\n  .chat-component .chat-msgs {\n    width: calc(100% - 250px);\n    height: 95vh;\n    border-left: 1px solid rgba(0, 0, 0, 0.1);\n    position: relative; }\n    .chat-component .chat-msgs .title {\n      height: 50px;\n      border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n      font-size: 16px;\n      padding: 10px;\n      position: absolute;\n      left: 0px;\n      right: 0px;\n      top: 0px;\n      background: #eee; }\n      .chat-component .chat-msgs .title .icon {\n        padding: 0px 8px;\n        background: #1d99d1;\n        border-radius: 1px;\n        display: inline-block;\n        color: white;\n        float: right; }\n      .chat-component .chat-msgs .title i {\n        cursor: pointer; }\n      .chat-component .chat-msgs .title .textfield {\n        background: white;\n        height: 25px;\n        line-height: 25px;\n        border-radius: 2px;\n        margin-right: 5px;\n        padding: 0px 5px;\n        width: 200px;\n        display: inline-block;\n        color: #222;\n        white-space: nowrap;\n        overflow: hidden;\n        top: 4px;\n        position: relative; }\n    .chat-component .chat-msgs .msgs {\n      height: calc(100% - 78px - 50px);\n      overflow: auto;\n      margin-top: 50px; }\n    .chat-component .chat-msgs .msg {\n      padding: 5px 15px;\n      background: white;\n      clear: both;\n      display: block;\n      margin: 5px 0px;\n      word-wrap: break-word;\n      word-break: break-word; }\n      .chat-component .chat-msgs .msg.left {\n        float: left;\n        border-top-right-radius: 5px;\n        border-bottom-right-radius: 5px;\n        margin-right: 20px; }\n      .chat-component .chat-msgs .msg.right {\n        float: right;\n        border-top-left-radius: 5px;\n        border-bottom-left-radius: 5px;\n        margin-left: 20px; }\n      .chat-component .chat-msgs .msg .time {\n        font-size: 10px;\n        float: right;\n        color: #1a89bb; }\n      .chat-component .chat-msgs .msg .text {\n        font-size: 15px; }\n      .chat-component .chat-msgs .msg .from {\n        font-size: 10px; }\n    .chat-component .chat-msgs .msg-field-wrapper {\n      background: white;\n      width: 100%;\n      position: absolute;\n      bottom: 0px;\n      border-top: 1px solid rgba(0, 0, 0, 0.2); }\n      .chat-component .chat-msgs .msg-field-wrapper .msg-field {\n        background: white;\n        width: 100%;\n        min-height: 40px;\n        max-height: 100px;\n        line-height: 40px;\n        font-size: 16px;\n        padding: 0px 10px;\n        font-family: aller;\n        overflow: auto; }\n      .chat-component .chat-msgs .msg-field-wrapper .btns-wrapper {\n        padding: 5px;\n        color: white; }\n      .chat-component .chat-msgs .msg-field-wrapper .send-btn {\n        padding: 2px 8px;\n        background: #1d99d1;\n        cursor: pointer;\n        border-radius: 3px;\n        display: inline-block;\n        float: right; }\n  .chat-component .ui-select-multiple-div .ui-select-match-item, .chat-component .ui-select-multiple-div span[type=button] {\n    position: relative;\n    padding: 5px;\n    background-color: #1d99d1;\n    color: white;\n    font-weight: bold;\n    border-radius: 5px;\n    margin-right: 10px;\n    -webkit-appearance: initial; }\n", ""]);

	// exports


/***/ },
/* 155 */
/***/ function(module, exports, __webpack_require__) {

	//     uuid.js
	//
	//     Copyright (c) 2010-2012 Robert Kieffer
	//     MIT License - http://opensource.org/licenses/mit-license.php

	// Unique ID creation requires a high quality random # generator.  We feature
	// detect to determine the best RNG source, normalizing to a function that
	// returns 128-bits of randomness, since that's what's usually required
	var _rng = __webpack_require__(156);

	// Maps for number <-> hex string conversion
	var _byteToHex = [];
	var _hexToByte = {};
	for (var i = 0; i < 256; i++) {
	  _byteToHex[i] = (i + 0x100).toString(16).substr(1);
	  _hexToByte[_byteToHex[i]] = i;
	}

	// **`parse()` - Parse a UUID into it's component bytes**
	function parse(s, buf, offset) {
	  var i = (buf && offset) || 0, ii = 0;

	  buf = buf || [];
	  s.toLowerCase().replace(/[0-9a-f]{2}/g, function(oct) {
	    if (ii < 16) { // Don't overflow!
	      buf[i + ii++] = _hexToByte[oct];
	    }
	  });

	  // Zero out remaining bytes if string was short
	  while (ii < 16) {
	    buf[i + ii++] = 0;
	  }

	  return buf;
	}

	// **`unparse()` - Convert UUID byte array (ala parse()) into a string**
	function unparse(buf, offset) {
	  var i = offset || 0, bth = _byteToHex;
	  return  bth[buf[i++]] + bth[buf[i++]] +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] +
	          bth[buf[i++]] + bth[buf[i++]] +
	          bth[buf[i++]] + bth[buf[i++]];
	}

	// **`v1()` - Generate time-based UUID**
	//
	// Inspired by https://github.com/LiosK/UUID.js
	// and http://docs.python.org/library/uuid.html

	// random #'s we need to init node and clockseq
	var _seedBytes = _rng();

	// Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
	var _nodeId = [
	  _seedBytes[0] | 0x01,
	  _seedBytes[1], _seedBytes[2], _seedBytes[3], _seedBytes[4], _seedBytes[5]
	];

	// Per 4.2.2, randomize (14 bit) clockseq
	var _clockseq = (_seedBytes[6] << 8 | _seedBytes[7]) & 0x3fff;

	// Previous uuid creation time
	var _lastMSecs = 0, _lastNSecs = 0;

	// See https://github.com/broofa/node-uuid for API details
	function v1(options, buf, offset) {
	  var i = buf && offset || 0;
	  var b = buf || [];

	  options = options || {};

	  var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq;

	  // UUID timestamps are 100 nano-second units since the Gregorian epoch,
	  // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
	  // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
	  // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
	  var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime();

	  // Per 4.2.1.2, use count of uuid's generated during the current clock
	  // cycle to simulate higher resolution clock
	  var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1;

	  // Time since last uuid creation (in msecs)
	  var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

	  // Per 4.2.1.2, Bump clockseq on clock regression
	  if (dt < 0 && options.clockseq === undefined) {
	    clockseq = clockseq + 1 & 0x3fff;
	  }

	  // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
	  // time interval
	  if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
	    nsecs = 0;
	  }

	  // Per 4.2.1.2 Throw error if too many uuids are requested
	  if (nsecs >= 10000) {
	    throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
	  }

	  _lastMSecs = msecs;
	  _lastNSecs = nsecs;
	  _clockseq = clockseq;

	  // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
	  msecs += 12219292800000;

	  // `time_low`
	  var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
	  b[i++] = tl >>> 24 & 0xff;
	  b[i++] = tl >>> 16 & 0xff;
	  b[i++] = tl >>> 8 & 0xff;
	  b[i++] = tl & 0xff;

	  // `time_mid`
	  var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
	  b[i++] = tmh >>> 8 & 0xff;
	  b[i++] = tmh & 0xff;

	  // `time_high_and_version`
	  b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
	  b[i++] = tmh >>> 16 & 0xff;

	  // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
	  b[i++] = clockseq >>> 8 | 0x80;

	  // `clock_seq_low`
	  b[i++] = clockseq & 0xff;

	  // `node`
	  var node = options.node || _nodeId;
	  for (var n = 0; n < 6; n++) {
	    b[i + n] = node[n];
	  }

	  return buf ? buf : unparse(b);
	}

	// **`v4()` - Generate random UUID**

	// See https://github.com/broofa/node-uuid for API details
	function v4(options, buf, offset) {
	  // Deprecated - 'format' argument, as supported in v1.2
	  var i = buf && offset || 0;

	  if (typeof(options) == 'string') {
	    buf = options == 'binary' ? new Array(16) : null;
	    options = null;
	  }
	  options = options || {};

	  var rnds = options.random || (options.rng || _rng)();

	  // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
	  rnds[6] = (rnds[6] & 0x0f) | 0x40;
	  rnds[8] = (rnds[8] & 0x3f) | 0x80;

	  // Copy bytes to buffer, if provided
	  if (buf) {
	    for (var ii = 0; ii < 16; ii++) {
	      buf[i + ii] = rnds[ii];
	    }
	  }

	  return buf || unparse(rnds);
	}

	// Export public API
	var uuid = v4;
	uuid.v1 = v1;
	uuid.v4 = v4;
	uuid.parse = parse;
	uuid.unparse = unparse;

	module.exports = uuid;


/***/ },
/* 156 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(global) {
	var rng;

	if (global.crypto && crypto.getRandomValues) {
	  // WHATWG crypto-based RNG - http://wiki.whatwg.org/wiki/Crypto
	  // Moderately fast, high quality
	  var _rnds8 = new Uint8Array(16);
	  rng = function whatwgRNG() {
	    crypto.getRandomValues(_rnds8);
	    return _rnds8;
	  };
	}

	if (!rng) {
	  // Math.random()-based (RNG)
	  //
	  // If all else fails, use Math.random().  It's fast, but is of unspecified
	  // quality.
	  var  _rnds = new Array(16);
	  rng = function() {
	    for (var i = 0, r; i < 16; i++) {
	      if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
	      _rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
	    }

	    return _rnds;
	  };
	}

	module.exports = rng;


	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 157 */
/***/ function(module, exports) {

	module.exports = "<div class=\"chat-component\" ng-if=\"vm.isUserLoggedIn()\" ng-show=\"vm.showChatComponent\">\n\t<div class=\"chat-friends\">\n\t\t<div class=\"head-wrapper\">\n\t\t\t<div class=\"title\" ng-click=\"vm.friendListOpened = !vm.friendListOpened\">Friends</div>\n\t\t\t<div class=\"icons-wrapper\">\n\t\t\t\t<!-- <span class=\"icon\" ng-click=\"vm.openGroupModal()\"><i class=\"fa fa-user-plus\"></i></span> -->\n\t\t\t\t<!-- <span class=\"icon\" ng-click=\"vm.friendListOpened = !vm.friendListOpened\"><i class=\"fa\" ng-class=\"{'fa-angle-down':vm.friendListOpened,'fa-angle-up':!vm.friendListOpened}\"></i></span> -->\n\t\t\t\t<!-- <span class=\"icon\" ng-click=\"vm.isModalVisible = true;\"><i class=\"fa fa-square-o\"></i></span> -->\n\t\t\t\t<span class=\"icon\" ng-click=\"vm.showChatComponent = false;\"><i class=\"fa fa-close mainclose\"></i></span>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"search-wrapper\" ng-show=\"vm.friendListOpened\">\n\t\t\t<input type=\"text\" ng-model=\"vm.searchFriend\" placeholder=\"Search Friends\"></input>\n\t\t</div>\n\t\t<div class=\"friend-list-wrapper\" ng-show=\"vm.friendListOpened\">\n\t\t\t<div class=\"groupFrndLine\">Contacts</div>\n\t\t\t<div class=\"friend\"\n\t\t\t\tng-repeat=\"frnd in vm.frndList | filter:vm.searchFriend\"\n\t\t\t\tng-if=\"frnd.user_id != vm.userInfo.user_id\">\n\t\t\t\t<span class=\"pic\"><img ng-src=\"{{frnd.pic50}}\" err-image=\"/assets/images/dummy-user-pic.png\" alt=\"\"></span>\n\t\t\t\t<span class=\"name\" ng-click=\"vm.openAndFetchMsgs(frnd)\">{{frnd.display_name}}</span>\n\t\t\t\t<p class=\"unread\">10</p>\n\t\t\t\t<span class=\"audio-icon\" ng-class=\"{'disabled':!vm.isUserOnline(frnd.user_id)}\" ng-click=\"vm.startAudioCall(frnd.user_id)\">\n\t\t\t\t\t<i class=\"fa fa-headphones\"></i>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"video-icon\" ng-class=\"{'disabled':!vm.isUserOnline(frnd.user_id)}\" ng-click=\"vm.startVideoCall(frnd.user_id)\">\n\t\t\t\t\t<i class=\"fa fa-video-camera\"></i>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"status\" ng-class=\"{'online':vm.isUserOnline(frnd.user_id),'offline':!vm.isUserOnline(frnd.user_id)}\"></span>\n\t\t\t</div>\n\t\t\t<div class=\"groupFrndLine\">Group Conversations</div>\n\t\t\t<div class=\"friend\"\n\t\t\t\tng-repeat=\"frnd in vm.chatMembers | filter:vm.searchFriend\"\n\t\t\t\tng-if=\"frnd.typ === 'grp'\">\n\t\t\t\t<span class=\"pic\"><img ng-src=\"{{frnd.pic50}}\" err-image=\"/assets/images/dummy-user-pic.png\" alt=\"\"></span>\n\t\t\t\t<span class=\"name\" ng-click=\"vm.openAndFetchMsgs(frnd)\">{{frnd.display_name}}</span>\n\t\t\t\t<span class=\"audio-icon\" ng-class=\"{'disabled':!vm.isUserOnline(frnd.user_id)}\" ng-click=\"vm.startAudioCall(frnd.user_id)\">\n\t\t\t\t\t<i class=\"fa fa-headphones\"></i>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"video-icon\" ng-class=\"{'disabled':!vm.isUserOnline(frnd.user_id)}\" ng-click=\"vm.startVideoCall(frnd.user_id)\">\n\t\t\t\t\t<i class=\"fa fa-video-camera\"></i>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"status\" ng-class=\"{'online':vm.isUserGrpOnline(frnd.grp_mem),'offline':!vm.isUserGrpOnline(frnd.grp_mem)}\"></span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"chat-msgs-individual\" id=\"chat-msgs_{{vm.getOpenMemberIndex(obj.member.user_id)}}\" ng-repeat=\"obj in vm.openedChats track by $index\" ng-if=\"!obj.closed && !vm.isModalVisible\">\n\t\t<div class=\"head-wrapper\" ng-class=\"{'unread-color':obj.color}\">\n\t\t\t<span class=\"pic\"><img ng-src=\"{{obj.member.pic50}}\" err-image=\"/assets/images/dummy-user-pic.png\" alt=\"\"></span>\n\t\t\t<div class=\"title\" ng-click=\"vm.openChatOf(obj.member)\">\n\t\t\t\t{{obj.member.display_name}}\n\t\t\t\t<span class=\"status\" ng-if=\"obj.member.typ === 'usr'\" ng-class=\"{'online':vm.isUserOnline(obj.member.user_id),'offline':!vm.isUserOnline(obj.member.user_id)}\"></span>\n\t\t\t</div>\n\t\t\t<div class=\"icons-wrapper\">\n\t\t\t\t<span class=\"icon\" ng-if=\"obj.member.typ === 'grp'\" ng-click=\"obj.addMemberFieldVisible = !obj.addMemberFieldVisible;\">\n\t\t\t\t\t<i class=\"fa fa-user-plus\"></i>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"icon\" ng-click=\"vm.toggleChatOf(obj.member)\">\n\t\t\t\t\t<i class=\"fa\" ng-class=\"{'fa-angle-down':!obj.minimized,'fa-angle-up':obj.minimized}\"></i>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"icon\" ng-if=\"obj.member.typ === 'usr'\" ng-click=\"vm.clearAllMsgOf(obj.member)\">\n\t\t\t\t\t<i class=\"fa fa-trash\"></i>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"icon\" ng-click=\"vm.visibleModalWithMember(obj.member.user_id)\"><i class=\"fa fa-square-o\"></i></span>\n\t\t\t\t<span class=\"icon\" ng-click=\"vm.closeChatOf(obj.member)\"><i class=\"fa fa-close\"></i></span>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"grp-field ui-select-multiple-div\" ng-show=\"obj.addMemberFieldVisible && !obj.minimized\">\n\t\t\t\t<ui-select multiple ng-model=\"vm.newFriendsToGrp\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t    <ui-select-match placeholder=\"Select Friends...\">\n\t\t\t\t        <span ng-bind=\"$item.display_name\"></span>\n\t\t\t\t    </ui-select-match>\n\t\t\t\t    <ui-select-choices repeat=\"item.user_id as item in (vm.frndList | filter: $select.search) track by $index\">\n\t\t\t\t        <span ng-bind=\"item.display_name\"></span>\n\t\t\t\t    </ui-select-choices>\n\t\t\t\t</ui-select>\n\t\t\t\t<span ng-click=\"vm.addMembersToGrp(obj)\" class=\"add-btn\">Add</span>\n\t\t</div>\n\t\t<div class=\"success-add-msg\"\n\t\t\tng-if=\"vm.successInAddMember[obj.member.user_id]\">\n\t\t\tMembers Added.\n\t\t</div>\n\t\t<div class=\"grp-member-wrapper\" ng-show=\"!obj.minimized\">\n\t\t\t<div class=\"grp-member\" ng-repeat=\"(mem,joinig) in obj.member.members\">\n\t\t\t\t{{(mem === vm.userInfo.user_id) ? 'You' : vm.allFriendsInfo[mem].display_name }}\n\t\t\t\t<span class=\"status\" ng-class=\"{'online':vm.isUserOnline(mem),'offline':!vm.isUserOnline(mem)}\"></span>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"offline-msg\"\n\t\t\tng-show=\"!obj.minimized\"\n\t\t\tng-if=\"obj.member.typ === 'usr' && !vm.isUserOnline(obj.member.user_id)\">\n\t\t\tUser is offline.\n\t\t</div>\n\t\t<div class=\"msgs-wrapper\" id=\"msgs-wrapper_{{vm.getOpenMemberIndex(obj.member.user_id)}}\" ng-show=\"!obj.minimized\">\n\t\t\t<div class=\"msg\"\n\t\t\t\tng-class=\"{'right':(data.frm_usr === vm.userInfo.user_id),'left':(data.frm_usr === obj.member.user_id)}\"\n\t\t\t\tng-repeat=\"data in vm.msgs[obj.member.user_id] | orderBy:'msg_ts':false\">\n\t\t\t\t<div class=\"frm-usr\" ng-if=\"obj.member.typ === 'grp'\">\n\t\t\t\t\t{{(data.frm_usr === vm.userInfo.user_id) ? 'You' : vm.allFriendsInfo[data.frm_usr].display_name }}\n\t\t\t\t</div>\n\t\t\t\t<div class=\"text\" ng-bind-html=\"data.msg\"></div>\n\t\t\t\t<div class=\"time\">{{data.msg_ts | date : 'HH:mm'}}</div>\n\t\t\t</div>\n\t\t\t<div class=\"clearfix\"></div>\n\t\t</div>\n\t\t<div style=\"background: #eeeeee;\" ng-show=\"vm.activeMember.isTyping\">typing...</div>\n\t\t<div class=\"msg-field\" ng-show=\"!obj.minimized\">\n\t\t\t<div class=\"textfield\" id=\"msg_{{vm.getOpenMemberIndex(obj.member.user_id)}}\"\n\t\t\t\tng-keydown=\"vm.sendMsg($event,obj.member)\"\n\t\t\t\tcontenteditable=\"\"\n\t\t\t\tplaceholder=\"Write a Message\"\n\t\t\t\tng-keyup=\"vm.shouldShowPlaceHolder($event,obj.member.user_id)\"\n\t\t\t\tng-blur=\"vm.shouldShowPlaceHolder($event,obj.member.user_id)\"\n\t\t\t\tng-keypress=\"vm.removePlaceHolder($event,obj.member.user_id)\"\n\t\t\t\tng-click=\"vm.removePlaceHolder($event,obj.member.user_id)\"\n\t\t\t></div>\n\t\t</div>\n\t</div>\n\t<div class=\"clearfix\"></div>\n\n\t<div class=\"chat-modal\" ng-show=\"vm.isModalVisible\">\n\t\t<div class=\"chat-action-bar\">\n\t\t\t<span class=\"icon\" ng-click=\"vm.isModalVisible = false;\"><i class=\"fa fa-close\"></i></span>\n\t\t</div>\n\t\t<div class=\"chat-modal-content\">\n\t\t\t<div class=\"chat-members\">\n\t\t\t\t<div class=\"search-modal-wrapper\" ng-show=\"vm.isModalVisible\">\n\t\t\t\t\t<input type=\"text\" class=\"chat-modal-search\" ng-model=\"vm.searchFriendModal\" placeholder=\"Search Friends\"></input>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"groupFrndLine\">Contacts</div>\n\n\t\t\t\t<span class=\"member\"\n\t\t\t\t\tng-if=\"vm.userInfo.user_id != member.user_id\"\n\t\t\t\t\tng-class=\"{'active':(member.user_id === vm.activeMember.user_id)}\"\n\t\t\t\t\tng-click=\"vm.setActiveMember(member)\"\n\t\t\t\t\tng-repeat=\"member in (vm.frndList | filter:vm.searchFriendModal) track by $index\" ng-style=\"member.user_id==vm.newMesageUserId && {'background-color':'#85d6fb'}\">\n\t\t\t\t\t<span class=\"modal-pic\">\n\t\t\t\t\t\t<img ng-src=\"{{member.pic50}}\" err-image=\"/assets/images/dummy-user-pic.png\" alt=\"\" >\n\t\t\t\t\t\t<div class=\"title\">\n\t\t\t\t\t\t\t{{member.display_name}}\n\t\t\t\t\t\t<p class=\"unread\">10</p>\n\t\t\t\t\t\t<span class=\"status\" ng-class=\"{'online':vm.isUserOnline(member.user_id),'offline':!vm.isUserOnline(member.user_id)}\"></span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</span>\n\t\t\t\t</span>\n\t\t\t\t<div class=\"groupFrndLine\">Group Conversations</div>\n\t\t\t\t<span class=\"member\"\n\t\t\t\t\tng-if=\"vm.userInfo.user_id != member.user_id\"\n\t\t\t\t\tng-class=\"{'active':(member.user_id === vm.activeMember.user_id)}\"\n\t\t\t\t\tng-click=\"vm.setActiveMember(member)\"\n\t\t\t\t\tng-repeat=\"member in (vm.chatMembers | filter:vm.searchFriendModal) track by $index\" ng-show=\"member.typ === 'grp'\">\n\t\t\t\t\t<span class=\"modal-pic\">\n\t\t\t\t\t\t<img ng-src=\"{{member.pic50}}\" err-image=\"/assets/images/dummy-user-pic.png\" alt=\"\">\n\t\t\t\t\t\t<div class=\"title\">\n\t\t\t\t\t\t\t{{member.display_name}}\n\t\t\t\t\t\t<p class=\"unread\">10</p>\n\t\t\t\t\t\t<span class=\"status\" ng-class=\"{'online':vm.isUserGrpOnline(frnd.grp_mem),'offline':!vm.isUserGrpOnline(frnd.grp_mem)}\"></span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</span>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<div class=\"chat-msgs\" ng-if=\"vm.activeMember\">\n\t\t\t\t<div class=\"msgs\" id=\"msgs\">\n\t\t\t\t\t<div class=\"title\">\n\t\t\t\t\t\t<span>{{vm.activeMember.display_name}}</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<span class=\"msg\" ng-class=\"{'right':(data.frm_usr === vm.userInfo.user_id),'left':(data.frm_usr === vm.activeMember.user_id)}\" ng-repeat=\"data in vm.msgs[vm.activeMember.user_id] | orderBy:'msg_ts':false\">\n\t\t\t\t\t\t<div class=\"text\" ng-bind-html=\"data.msg\"></div>\n\t\t\t\t\t\t<div class=\"time\">{{data.msg_ts| date : 'HH:mm'}}</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t</span>\n\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"msg-field-wrapper\">\n\t\t\t\t\t<div class=\"msg-field\" id=\"msg\"\n\t\t\t\t\t\tng-keydown=\"vm.sendMsgFromModalOnKeyPress($event,vm.activeMember)\"\n\t\t\t\t\t\tcontenteditable=\"\"\n\t\t\t\t\t\tng-keyup=\"vm.shouldShowPlaceHolder($event,vm.activeMember.user_id)\"\n\t\t\t\t\t\tng-keypress=\"vm.removePlaceHolder($event,vm.activeMember.user_id)\"\n\t\t\t\t\t\tng-click=\"vm.removePlaceHolder($event,vm.activeMember.user_id)\"></div>\n\t\t\t\t\t<div class=\"btns-wrapper\">\n\t\t\t\t\t\t<div class=\"send-btn\" ng-click=\"vm.sendMsgFromModal(vm.activeMember)\">Send</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class=\"create-grp-modal\" ng-if=\"vm.isCreateGrpModalVisible\">\n\t\t<div class=\"grp-form-wrapper\">\n\t\t\t<div class=\"close-btn\" ng-click=\"vm.isCreateGrpModalVisible = false;\">\n\t\t\t\t<i class=\"fa fa-times\"></i>\n\t\t\t</div>\n\t\t\t<div class=\"title\">Create Group</div>\n\t\t\t<form class=\"form form-horizontal\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"grpname\">Name</label>\n\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t<input type=\"text\" ng-model=\"vm.grp_nm\" class=\"form-control\" placeholder=\"Enter Group Name\"></input>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"frnd\">Friends</label>\n\t\t\t\t\t<div class=\"col-sm-10 ui-select-multiple-div\">\n\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedFriends\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Friends...\">\n\t\t\t\t\t\t        <span ng-bind=\"$item.display_name\"></span>\n\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t    <ui-select-choices repeat=\"item.user_id as item in (vm.frndList | filter: $select.search) track by $index\">\n\t\t\t\t\t\t        <span ng-bind=\"item.display_name\" ></span>\n\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"pic\">Pic</label>\n\t\t\t\t\t<div class=\"col-sm-4\" ngf-select ng-model=\"vm.grpPic\" ngf-multiple='false'>\n\t\t\t\t\t\t<div class=\"form-control upload-btn\">Select Image</div>\n\t\t\t\t\t\t<span class=\"grp-pic\" ng-if=\"vm.myGrpPic\" style=\"background: url({{vm.myGrpPic}});background-size: cover;background-position: 50% 50%;\"></span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"create-btn ew-rounded-btn\" ng-click=\"vm.createGroup()\">\n\t\t\t\t\t<i class=\"fa fa-plus right\"></i>\n\t\t\t\t\t<span class=\"text\">Create Group</span>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\t</div>\n\n\t<div class=\"audio-modal-wrapper\" ng-if=\"vm.isActiveAudioCall\">\n\t\t<div class=\"audio-call\">\n\t\t\t<div class=\"toolbar\">\n\t\t\t\t<span>{{vm.activeMediaPerson}}</span>\n\t\t\t\t<span class=\"close-icon\" ng-click=\"vm.stopAudioCall()\">\n\t\t\t\t\t<i class=\"fa fa-times\"></i>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<div class=\"content\">\n\t\t\t\t<div class=\"left-side-pic\"></div>\n\t\t\t\t<div class=\"middle-msg-wrapper\">\n\t\t\t\t\t<span ng-if=\"vm.isCallEnded\">{{vm.endCallMsg}}</span>\n\t\t\t\t\t<span ng-if=\"vm.isCallConnecting\">Connecting ....</span>\n\t\t\t\t\t<div  ng-if=\"vm.isCallConnected\" class=\"signal\">\n\t\t\t\t\t\t<span class=\"one\">.</span>\n\t\t\t\t\t\t<span class=\"two\">.</span>\n\t\t\t\t\t\t<span class=\"three\">.</span>\n\t\t\t\t\t\t<span class=\"four\">.</span>\n\t\t\t\t\t\t<span class=\"five\">.</span>\n\t\t\t\t\t\t<span class=\"six\">.</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"right-side-pic\"></div>\n\t\t\t\t<div class=\"big-video\">\n\t\t\t\t\t<video id=\"theirVideoTag\" autoplay></video>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"small-video\">\n\t\t\t\t\t<video id=\"myVideoTag\" muted=\"muted\" autoplay></video>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"action-btns\">\n\t\t\t\t<span class=\"act-btn\" ng-if=\"!vm.isCallAccepted\" ng-click=\"vm.acceptAudioCall()\">Accept</span>\n\t\t\t\t<span class=\"act-btn\" ng-if=\"!vm.isCallAccepted\" ng-click=\"vm.declineAudioCall()\">Decline</span>\n\t\t\t\t<span class=\"act-btn\" ng-if=\"vm.isCallAccepted\" ng-click=\"vm.stopAudioCall()\">End Call</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class=\"video-modal-wrapper\" ng-if=\"vm.isActiveVideoCall\">\n\t\t<div class=\"video-call\">\n\t\t\t<div class=\"toolbar\">\n\t\t\t<span>{{vm.activeMediaPerson}}</span>\n\t\t\t\t<span class=\"close-icon\" ng-click=\"vm.stopVideoCall()\">\n\t\t\t\t\t<i class=\"fa fa-times\"></i>\n\t\t\t\t</span>\n\t\t\t</div>\n\n\t\t\t<div class=\"content\">\n\t\t\t\t<div class=\"big-video\">\n\t\t\t\t\t<video id=\"theirVideoTag\" autoplay></video>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"small-video\">\n\t\t\t\t\t<video id=\"myVideoTag\" muted=\"muted\" autoplay></video>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"middle-msg-wrapper\">\n\t\t\t\t\t<span ng-if=\"vm.isCallEnded\">{{vm.endCallMsg}}</span>\n\t\t\t\t\t<span ng-if=\"vm.isCallConnecting\">Connecting ....</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"action-btns\" ng-if=\"!vm.isCallAccepted\">\n\t\t\t\t<span class=\"act-btn\" ng-click=\"vm.acceptVideoCall()\">Accept</span>\n\t\t\t\t<span class=\"act-btn\" ng-click=\"vm.declineVideoCall()\">Decline</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<!-- Grp Image Modal -->\n\t<div class=\"image-modal profile\" ng-if=\"vm.showGrpPicModal\">\n\t\t<div style=\"width: 100%;\">\n\t\t\t<image-crop\n\t\t\tdata-height=\"200\"\n\t\t\tdata-width=\"200\"\n\t\t\tdata-shape=\"square\"\n\t\t\tdata-step=\"1\"\n\t\t\tsrc=\"vm.image\"\n\t\t\tdata-result=\"vm.myGrpPic\"\n\t\t\tdata-result-blob= \"vm.myGrpPicBlob\"\n\t\t\tcrop=\"vm.initCrop\"\n\t\t\t></image-crop>\n\t\t</div>\n\t\t<div style=\"margin-top: 20px;z-index: 10\">\n\t\t\t<button class=\"btn btn-primary\" ng-click=\"vm.cropImage()\">Crop</button>\n\t\t\t<button class=\"btn btn-primary\" ng-click=\"vm.showGrpPicModal=false;\">Close</button>\n\t\t</div>\n\t</div>\n</div>\n";

/***/ },
/* 158 */
/***/ function(module, exports) {

	angular
		.module('service')
		.service('Mail',Mail);

	Mail.$inject = ['$http','BASE_API'];

	function Mail($http,BASE_API) {
	    this.sendMail = function (data) {
	        return $http.post(BASE_API + '/mail/send', data).then(function (res) {
	            return res.data;
	        });
	    }

	    this.sendBulkEmails = function (data) {
	        return $http.post(BASE_API + '/mail/sendBulkEmails', data);
	    }

	    this.subscribeUser = function (data) {
	        return $http.post(BASE_API + '/mail/subscribeUser', data);
	    }

	    this.sendMailToSupportForEnrollBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForEnrollBtn', data);   
	    }

	    this.sendMailToSupportForPaySecurelyBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForPaySecurelyBtn', data);   
	    }
	}


/***/ },
/* 159 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div class=\"menu-component-wrapper\">\n\t<div class=\"header-btns\" ng-if=\"vm.showHeaderButtons === true\">\n\t\t<div class=\"btn-wrap\">\n\t\t\t<div class=\"notification-btn menu-head-btn\" data-capture-document-click=\"false\" ng-if=\"vm.isUserLoggedIn()\">\n\t\t\t\t<a class=\"login-btn\" style=\"text-decoration: none;\" ng-click=\"vm.toggleUserMenuModal()\">\n\t\t\t\t\t<img class=\"usr-small-profile-pic\" ng-src=\"{{vm.getUserPic()}}\" ng-if=\"vm.getUserPic()\" alt=\"\"/>\n\t\t\t\t\t<img class=\"usr-small-profile-pic\" src=\"" + __webpack_require__(160) + "\" ng-if=\"!vm.getUserPic()\" alt=\"\"/>{{vm.getUser()['dsp_nm']}} \n\t\t\t\t</a>\n\t\t\t\t<div class=\"notification-modal\" style=\"width: 250px;\" ng-show=\"vm.showUserMenuModal\">\n\t\t\t\t\t<div class=\"ew-modal-content\" style=\"max-height: 600px;min-height: auto;border-top-right-radius: 5px;border-top-left-radius: 5px;\">\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub user-pic\">\n\t\t\t\t\t\t\t\t<a ng-href=\"{{vm.getDashboardUrl()}}\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-dashcube\"></i>\n\t\t\t\t\t\t\t\t\tDashboard\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">\n\t\t\t\t\t\t\t\t<a ng-click=\"vm.toggleChatModal()\"><i class=\"fa fa-comment-o\"></i> Chat</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub logout\">\n\t\t\t\t\t\t\t\t<a ng-click=\"vm.goToLogout()\"><i class=\"fa fa-sign-out\"></i> Logout</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\t\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-modal-footer\">\n\t\t\t\t\t\t<a class=\"see-all\" style=\"cursor: pointer;\" ng-click=\"vm.toggleUserMenuModal()\"><img src=\"" + __webpack_require__(161) + "\" alt=\"\"/>Close</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"notification-btn menu-head-btn\"  data-capture-document-click=\"false\">\n\t\t\t\t<a class=\"login-btn\" style=\"text-decoration: none;\" ng-click=\"vm.toggleSmallMenuModal()\"><i class=\"fa fa-list\"></i> SMENU </a>\n\t\t\t\t<div class=\"notification-modal\"  style=\"width: 250px;\" ng-show=\"vm.showSmallMenuModal\">\n\t\t\t\t\t<div class=\"ew-modal-content\" style=\"max-height: 600px;min-height: auto;border-top-right-radius: 5px;border-top-left-radius: 5px;\">\n\t\t\t\t\t\t<div class=\"notification smenu-links\" ng-if=\"!vm.isUserLoggedIn()\" ng-show=\"vm.showLoginButtons === true\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">\n\t\t\t\t\t\t\t\t<a ng-click=\"vm.menuDelegate.loginDelegate.toggleLoginModal()\"><i class=\"fa fa-lock\"></i> Login</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"notification smenu-links\" ng-if=\"!vm.isUserLoggedIn()\" ng-show=\"vm.showLoginButtons === true\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">\n\t\t\t\t\t\t\t\t<a ng-click=\"vm.goToRegister()\"><i class=\"fa fa-user\"></i> Sign Up</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">\n\t\t\t\t\t\t\t\t<a href=\"/buy/\"><i class=\"fa fa-credit-card\"></i> Pricing</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"notification smenu-links\" ng-if=\"vm.isUserLoggedIn()\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">\n\t\t\t\t\t\t\t\t<a ng-click=\"vm.toggleChatModal()\"><i class=\"fa fa-comment-o\"></i> Chat</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">\n\t\t\t\t\t\t\t\t<a target=\"_blank\" href=\"/sitemap\"><i class=\"fa fa-list\"></i>All Links</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-modal-footer\">\n\t\t\t\t\t\t<a class=\"see-all\" style=\"cursor: pointer;\" ng-click=\"vm.toggleSmallMenuModal()\"><img src=\"" + __webpack_require__(161) + "\" alt=\"\"/>Close</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"notification-btn menu-head-btn\" ng-if=\"vm.isUserLoggedIn()\" data-capture-document-click=\"false\">\n\t\t\t\t<a class=\"login-btn\" style=\"text-decoration: none;\"  ng-click=\"vm.toggleNotificationModal()\"><i class=\"fa fa-bell\"></i> Notifications \n\t\t\t\t\t<span class=\"notification-cnt\" ng-if=\"vm.notificaionCount > 0\">{{vm.notificaionCount}}</span></a>\n\t\t\t\t<div class=\"notification-modal dark-blue\" ng-show=\"vm.showNotificationModal\">\n\t\t\t\t\t<div class=\"ew-modal-title\">\n\t\t\t\t\t\t<span class=\"text\">\n\t\t\t\t\t\t\t<img src=\"" + __webpack_require__(162) + "\" alt=\"\">\n\t\t\t\t\t\t\tNotifications\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<i class=\"fa fa-refresh\" ng-click=\"vm.getNotificationByUserId();vm.getSendNotificationCount()\"></i>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-modal-subheader\">\n\t\t\t\t\t\t<div class=\"ew-tabs\">\n\t\t\t\t\t\t\t<div class=\"ew-tab\" ng-class=\"{'active':vm.activeNotificationTab === 'web'}\">Web</div>\n\t\t\t\t\t\t\t<div class=\"ew-tab\"  ng-class=\"{'active':vm.activeNotificationTab === 'qa'}\">QA</div>\n\t\t\t\t\t\t\t<div class=\"ew-tab\" ng-class=\"{'active':vm.activeNotificationTab === 'cb'}\">CB</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-modal-content white\">\n\t\t\t\t\t\t<div class=\"no-notification\" ng-if=\"!vm.allNotifications || vm.allNotifications.length <= 0\">\n\t\t\t\t\t\t\tNo Notifications to show.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div ng-repeat=\"noti in vm.allNotifications | orderBy:'snd_dt':true\">\n\t\t\t\t\t\t\t<div class=\"notification\" ng-class=\"{'unread':!noti.noti_read}\">\n\t\t\t\t\t\t\t\t<a class=\"notification-sub\" ng-href=\"{{vm.getSpecificNotificationPageUrl(false,vm.getUserId(),noti.usr_noti_pk)}}\">{{noti.sub}}</a>\n\t\t\t\t\t\t\t\t<div class=\"notification-msg\">{{noti.noti_msg}}</div>\n\t\t\t\t\t\t\t\t<div class=\"notification-time\">\n\t\t\t\t\t\t\t\t\t{{noti.snd_dt | date:'dd-MMM-yy hh:mm'}}\n\t\t\t\t\t\t\t\t\t<div class=\"mark-as-read-icon\" ng-if=\"!noti.noti_read\" ng-click=\"vm.markSendNotificationAsRead($event,noti.usr_noti_pk)\" tooltips tooltip-template=\"Mark as read\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-check-square\"></i>\n\t\t\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-modal-footer\">\n\t\t\t\t\t\t<a class=\"see-all\" ng-if=\"vm.allNotifications && vm.allNotifications.length > 0\" ng-href=\"{{vm.getNotificationsPageUrl(false,vm.getUserId())}}\"><img src=\"" + __webpack_require__(162) + "\" alt=\"\">See All</a>\n\t\t\t\t\t\t<a class=\"see-all\" style=\"cursor: pointer;\" ng-click=\"vm.toggleNotificationModal()\"><img src=\"" + __webpack_require__(161) + "\" alt=\"\">Close</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"notification-btn menu-head-btn\" data-capture-document-click=\"false\">\n\t\t\t\t<a class=\"login-btn\" style=\"text-decoration: none;\"  ng-click=\"vm.toggleContactUsModal()\"><i class=\"fa fa-phone\"></i> Contact US </a>\n\t\t\t\t<div class=\"notification-modal contactus-modal\"  ng-show=\"vm.showContactUsModal\">\n\t\t\t\t\t<div class=\"ew-modal-content\" style=\"min-height: auto;border-top-right-radius: 5px;border-top-left-radius: 5px;\">\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">Phone</div>\n\t\t\t\t\t\t\t<div class=\"notification-msg\">{{vm.contactusDetails.ph}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">Support</div>\n\t\t\t\t\t\t\t<div class=\"notification-msg\">{{vm.contactusDetails.support}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"notification smenu-links\">\n\t\t\t\t\t\t\t<div class=\"notification-sub\">Sales</div>\n\t\t\t\t\t\t\t<div class=\"notification-msg\">{{vm.contactusDetails.sales}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-modal-footer\">\n\t\t\t\t\t\t<a class=\"see-all\" target=\"_blank\" href=\"/contactus\">\n\t\t\t\t\t\t\t<img src=\"" + __webpack_require__(162) + "\" alt=\"\"/>\n\t\t\t\t\t\t\tMessage\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a class=\"see-all\" style=\"cursor: pointer;\" target=\"_blank\" ng-click=\"vm.toggleContactUsModal()\">\n\t\t\t\t\t\t\t<img src=\"" + __webpack_require__(161) + "\" alt=\"\"/>\n\t\t\t\t\t\t\tClose\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- <a class=\"register-btn menu-head-btn\" href=\"/contactus\"><i class=\"fa fa-phone\"></i> Contact Us</a> -->\n\t\t</div>\n\t\t<a class=\"login-btn modal-close-btn\" ng-click=\"vm.menuDelegate.toggleMenuModal($event)\">\n\t\t\t<i class=\"fa\" ng-class=\"{'fa-bars':!vm.isMenuModalOpen,'fa-times':vm.isMenuModalOpen}\"></i> Menu\n\t\t</a>\n\t</div>\n\n\t<div class=\"mobile-header-btns\">\n\t\t<div class=\"btn-wrap\" ng-class=\"{'ew-open':vm.isMenuModalOpen}\">\n\t\t\t<a class=\"login-btn\" ng-href=\"{{vm.getDashboardUrl()}}\" ng-if=\"vm.isUserLoggedIn()\">Welcome, {{vm.getUser()['dsp_nm']}}</a>\n\t\t\t<a class=\"register-btn\" ng-if=\"!vm.isUserLoggedIn()\" ng-show=\"vm.showLoginButtons === true\" ng-click=\"vm.menuDelegate.loginDelegate.toggleLoginModal()\"><i class=\"fa fa-lock\"></i> Login</a>\n\t\t\t<a class=\"login-btn\"  ng-if=\"!vm.isUserLoggedIn()\" ng-show=\"vm.showLoginButtons === true\" ng-click=\"vm.goToRegister()\"><i class=\"fa fa-user\"></i> Sign Up</a>\n\t\t\t<a class=\"login-btn logout\" ng-if=\"vm.isUserLoggedIn()\" ng-click=\"vm.goToLogout()\"><i class=\"fa fa-sign-out\"></i> Logout</a>\n\t\t\t<a class=\"register-btn\" href=\"/sitemap\"><i class=\"fa fa-rocket\"></i> All Links</a>\n\t\t\t<a class=\"register-btn\" href=\"/buy/\"><i class=\"fa fa-lock\"></i> Pricing</a>\n\t\t\t<a class=\"register-btn\" href=\"/contactus\"><i class=\"fa fa-phone\"></i> Contact Us</a>\n\t\t\t<a class=\"register-btn\" ng-href=\"{{vm.trainingURL('it')}}\"><i class=\"fa fa-list\"></i> IT COURSES</a>\n\t\t\t<a class=\"register-btn\" ng-href=\"{{vm.getDashboardUrl()}}\"><i class=\"fa fa-dashcube\"></i> Dashboard</a>\n\t\t\t<a class=\"register-btn\" ng-href=\"{{vm.getQAUrl()}}\"><i class=\"fa fa-question-circle\"></i> Q&A</a>\n\t\t\t<a class=\"register-btn\" ng-href=\"{{vm.getNotificationsPageUrl(false,vm.getUserId())}}\"><i class=\"fa fa-bell\"></i> Notifications</a>\n\t\t</div>\n\t\t<a class=\"modal-close-btn\" ng-click=\"vm.toggleMenuModal($event)\" ng-if=\"vm.showHeaderButtons === true\">\n\t\t\t<i class=\"fa\" ng-class=\"{'fa-bars':!vm.isMenuModalOpen,'fa-times':vm.isMenuModalOpen}\"></i>Menu\n\t\t</a>\n\t</div>\n\t<login-component login-delegate=\"vm.menuDelegate.loginDelegate\"></login-component>\n\n\t<div class=\"big-screen\">\n\t\t<div class=\"menu-modal-component\" id=\"menu-modal\" ng-class=\"{'ew-open':vm.isMenuModalOpen}\">\n\t\t\t<div class=\"menu-part left\">\n\t\t\t\t<div class=\"menu-head\">\n\t\t\t\t\t<i class=\"fa fa-sitemap\"></i>\n\t\t\t\t\tMenu\n\t\t\t\t</div>\n\t\t\t\t<ul>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"/\">\n\t\t\t\t\t\t\t<i class=\"fa fa-home\"></i>\n\t\t\t\t\t\t\t<span>Home</span>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li><a href=\"/sitemap\"><i class=\"fa fa-home\"></i> All Links</a></li>\n\t\t\t\t\t<li ng-click=\"vm.goToLogin()\" ng-if=\"!vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-lock\"></i>\n\t\t\t\t\t\t<span>Login</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToLogout()\" class=\"logout\" ng-if=\"vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-sign-out\"></i>\n\t\t\t\t\t\t<span>Logout</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToChangePassword()\" ng-if=\"vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\n\t\t\t\t\t\t<span>Change Password</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToForgotPassword()\" ng-if=\"!vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\n\t\t\t\t\t\t<span>Forgot Password</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToAdminApp()\">\n\t\t\t\t\t\t<i class=\"fa fa-user-secret\"></i>\n\t\t\t\t\t\t<span>Admin</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToDashboardApp()\">\n\t\t\t\t\t\t<i class=\"fa fa-dashcube\"></i>\n\t\t\t\t\t\t<span>Dashboard</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"/all-links\"><i class=\"fa fa-dashcube\"></i><span>All Links</span></a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<div class=\"menu-head menu-foot\" ng-click=\"vm.goToURL('/question-answers')\"><i class=\"fa fa-sitemap\"></i>Q & A</div>\n\t\t\t</div>\n\t\t\t<div class=\"menu-part left\">\n\t\t\t\t<div class=\"menu-head\">\n\t\t\t\t\t<i class=\"fa fa-tasks\"></i>\n\t\t\t\t\tMenu\n\t\t\t\t</div>\n\t\t\t\t<ul>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/dashboard/performance')\">\n\t\t\t\t\t\t<i class=\"fa fa-user-secret\"></i>\n\t\t\t\t\t\t<span>Performace</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/dashboard/user-courses')\">\n\t\t\t\t\t\t<span>\n\t\t\t\t\t\t\t<i class=\"fa fa-list\"></i>\n\t\t\t\t\t\t\t<span>My Courses</span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/books')\">\n\t\t\t\t\t\t<i class=\"fa fa-book\"></i>\n\t\t\t\t\t\t<span>Books</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/question-answers')\">\n\t\t\t\t\t\t<i class=\"fa fa-users\"></i>\n\t\t\t\t\t\t<span>Q & A</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a ng-href=\"{{vm.getUserProfileUrl()}}\">\n\t\t\t\t\t\t\t<i class=\"fa fa-gear\"></i>\n\t\t\t\t\t\t\t<span>User Profile</span>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/dashboardApp/tests')\">\n\t\t\t\t\t\t<i class=\"fa fa-dashcube\"></i>\n\t\t\t\t\t\t<span>Tests</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToCareerBook()\">\n\t\t\t\t\t\t<i class=\"fa fa-user\"></i>\n\t\t\t\t\t\t<span>Career Book</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/dashboard/user-highlights')\">\n\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\n\t\t\t\t\t\t<span>Highlights</span>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<div class=\"menu-head menu-foot\" ng-click=\"vm.goToCareerBook()\"><i class=\"fa fa-tasks\"></i>CareerBook</div>\n\t\t\t</div>\n\t\t\t<div class=\"menu-part left\">\n\t\t\t\t<div class=\"menu-head\">\n\t\t\t\t\t<i class=\"fa fa-tasks\"></i>\n\t\t\t\t\tIT Courses\n\t\t\t\t</div>\n\t\t\t\t<ul>\n\t\t\t\t\t<li ng-repeat=\"course in vm.courses track by $index\" ng-if=\"course.courseType == 'it' && $index < 5\">\n\t\t\t\t\t\t<a class=\"item white\" ng-href=\"{{vm.trainingCourseURL('it',course.courseId,course.courseSubGroup)}}\" >{{course.courseName}}</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a class=\"item white\" style=\"text-align: right;\" ng-href=\"{{vm.trainingURL('it')}}\" >All IT Courses <i class=\"fa fa-arrow-right\"></i></a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<div class=\"menu-head menu-foot\" ng-click=\"vm.goToURL('/books')\"><i class=\"fa fa-tasks\"></i>Books</div>\n\t\t\t</div>\n\t\t\t<div class=\"menu-part right\">\n\t\t\t\t<div class=\"menu-head\">\n\t\t\t\t\t<i class=\"fa fa-tags\"></i>\n\t\t\t\t\tCourses\n\t\t\t\t\t<div class=\"close-icon\">\n\t\t\t\t\t\t<i class=\"fa fa-times\" ng-click=\"vm.menuDelegate.toggleMenuModal()\"></i>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<ul>\n\t\t\t\t\t<li ng-click=\"vm.openSubscribeModal('Board','Board')\">\n\t\t\t\t\t\t<span class=\"li-title\" >\n\t\t\t\t\t\t\t<i class=\"fa fa-list-alt\"></i>\n\t\t\t\t\t\t\t<span>Board Courses</span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<!-- <div class=\"hidden-menu\">\n\t\t\t\t\t\t\t<a class=\"item\" ng-href=\"{{vm.trainingCourseURL('board',course.courseId,course.courseSubGroup)}}\"  ng-if=\"course.courseType == 'board'\" ng-repeat=\"course in vm.courses track by $index\">{{course.courseName}}</a>\n\t\t\t\t\t\t\t<a class=\"item white\" style=\"text-align: right;\" ng-href=\"{{vm.trainingURL('board')}}\" >All Board Courses <i class=\"fa fa-arrow-right\"></i></a>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.openSubscribeModal('Competitive','Competitive')\">\n\t\t\t\t\t\t<span class=\"li-title\" >\n\t\t\t\t\t\t\t<i class=\"fa fa-th-large\"></i>\n\t\t\t\t\t\t\t<span>Competitive Courses</span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<!-- <div class=\"hidden-menu\">\n\t\t\t\t\t\t\t<a class=\"item\" ng-href=\"{{vm.trainingCourseURL('competitive',course.courseId,course.courseSubGroup)}}\"  ng-if=\"course.courseType == 'competitive'\" ng-repeat=\"course in vm.courses track by $index\">{{course.courseName}}</a>\n\t\t\t\t\t\t\t<a class=\"item white\" style=\"text-align: right;\" ng-href=\"{{vm.trainingURL('competitive')}}\" >All Competitive Courses <i class=\"fa fa-arrow-right\"></i></a>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<div class=\"menu-head menu-foot\" ng-click=\"vm.goToURL('/dashboard/performance')\"><i class=\"fa fa-tags\"></i>Performace</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div class=\"menu-small-screen small-screen\" ng-if=\"false\">\n\t\t<div class=\"ew-tabs\">\n\t\t\t<div class=\"ew-tab\" ng-class=\"{'active':vm.smallScreenActiveTab==='tab1'}\" ng-click=\"vm.toggleSmallScreenTab('tab1');\">Login Links</div>\n\t\t\t<div class=\"ew-tab\" ng-class=\"{'active':vm.smallScreenActiveTab==='tab2'}\" ng-click=\"vm.toggleSmallScreenTab('tab2');\">Dashboard</div>\n\t\t\t<div class=\"ew-tab\" ng-class=\"{'active':vm.smallScreenActiveTab==='tab3'}\" ng-click=\"vm.toggleSmallScreenTab('tab3');\">IT Course</div>\n\t\t\t<div class=\"ew-tab\" ng-class=\"{'active':vm.smallScreenActiveTab==='tab4'}\" ng-click=\"vm.toggleSmallScreenTab('tab4');\">All Course</div>\n\t\t</div>\n\t\t<div class=\"ew-tab-content-wrapper\">\n\t\t\t<div class=\"close-btn\" ng-click=\"vm.closeSmallScreenTab()\">\n\t\t\t\t<i class=\"fa fa-times\"></i>\n\t\t\t</div>\n\t\t\t<!-- Tab 1 -->\n\t\t\t<div class=\"ew-tab-content\" ng-if=\"vm.smallScreenActiveTab === 'tab1'\">\n\t\t\t\t<ul>\n\t\t\t\t\t<li ng-click=\"vm.goToLogin()\" ng-if=\"!vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-lock\"></i>\n\t\t\t\t\t\t<span>Login</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToLogout()\" class=\"logout\" ng-if=\"vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-sign-out\"></i>\n\t\t\t\t\t\t<span>Logout</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToChangePassword()\" ng-if=\"vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\n\t\t\t\t\t\t<span>Change Password</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToForgotPassword()\" ng-if=\"!vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\n\t\t\t\t\t\t<span>Forgot Password</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToAdminApp()\" ng-if=\"vm.isUserLoggedIn()\">\n\t\t\t\t\t\t<i class=\"fa fa-user-secret\"></i>\n\t\t\t\t\t\t<span>Admin</span>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<!-- Tab 2 -->\n\t\t\t<div class=\"ew-tab-content\" ng-if=\"vm.smallScreenActiveTab === 'tab2'\">\n\t\t\t\t<ul>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/dashboard/performance')\">\n\t\t\t\t\t\t<i class=\"fa fa-user-secret\"></i>\n\t\t\t\t\t\t<span>Performace</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.toggleHiddenMenu($event)\">\n\t\t\t\t\t\t<span>\n\t\t\t\t\t\t\t<i class=\"fa fa-list\"></i>\n\t\t\t\t\t\t\t<span>My Courses</span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<div class=\"hidden-menu\">\n\t\t\t\t\t\t\t<a class=\"item\" ng-href=\"/courses/{{course.courseType}}/{{course.courseId}}\" ng-repeat=\"course in vm.myCourses\">{{course.courseName}}</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/books')\">\n\t\t\t\t\t\t<i class=\"fa fa-book\"></i>\n\t\t\t\t\t\t<span>Books</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/question-answers')\">\n\t\t\t\t\t\t<i class=\"fa fa-users\"></i>\n\t\t\t\t\t\t<span>Q & A</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a ng-href=\"{{vm.getUserProfileUrl()}}\">\n\t\t\t\t\t\t\t<i class=\"fa fa-gear\"></i>\n\t\t\t\t\t\t\t<span>User Profile</span>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/dashboardApp/tests')\">\n\t\t\t\t\t\t<i class=\"fa fa-dashcube\"></i>\n\t\t\t\t\t\t<span>Tests</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToCareerBook()\">\n\t\t\t\t\t\t<i class=\"fa fa-user\"></i>\n\t\t\t\t\t\t<span>Career Book</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.goToURL('/dashboard/user-highlights')\">\n\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\n\t\t\t\t\t\t<span>Highlights</span>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<!-- Tab 3 -->\n\t\t\t<div class=\"ew-tab-content\" ng-if=\"vm.smallScreenActiveTab === 'tab3'\">\n\t\t\t\t<ul >\n\t\t\t\t\t<li ng-repeat=\"course in vm.courses track by $index\">\n\t\t\t\t\t\t<a class=\"item white\" ng-href=\"/courses/it/{{course.courseId}}\" ng-if=\"course.courseType == 'it' && $index < 5\">\n\t\t\t\t\t\t\t<i class=\"fa fa-book\"></i>{{course.courseName}}\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a class=\"item white\" style=\"text-align: right;\" ng-href=\"/courses/it\" >All IT Courses <i class=\"fa fa-arrow-right\"></i></a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<!-- Tab 4 -->\n\t\t\t<div class=\"ew-tab-content\" ng-if=\"vm.smallScreenActiveTab === 'tab4'\">\n\t\t\t\t<ul>\n\t\t\t\t\t<li ng-click=\"vm.toggleHiddenMenu($event)\">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t<span class=\"li-title\" style=\"border-bottom: 1px solid #ccc;padding-bottom: 10px;margin-bottom: 10px;display: inline-block;\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-list-alt\"></i>\n\t\t\t\t\t\t\t\t<span>Board Courses</span>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t<div class=\"hidden-menu\" style=\"font-size: 15px;\">\n\t\t\t\t\t\t\t\t<!-- <a class=\"item white\" ng-href=\"/courses/it/{{course.courseId}}\" ng-if=\"course.courseType == 'it' && $index < 5\" ng-repeat=\"course in vm.courses\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-book\"></i>{{course.courseName}}\n\t\t\t\t\t\t\t\t</a> -->\n\t\t\t\t\t\t\t\t<a class=\"item\" ng-href=\"/courses/board/{{course.courseId}}\"  ng-if=\"course.courseType == 'board'\" ng-repeat=\"course in vm.courses\">{{course.courseName}}</a>\n\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.toggleHiddenMenu($event)\">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t<span class=\"li-title\" style=\"border-bottom: 1px solid #ccc;padding-bottom: 10px;margin-bottom: 10px;display: inline-block;\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-th-large\"></i>\n\t\t\t\t\t\t\t\t<span>Competitive Courses</span>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t<div class=\"hidden-menu\" style=\"font-size: 15px;\">\n\t\t\t\t\t\t\t\t<a class=\"item\" ng-href=\"/courses/competitive/{{course.courseId}}\"  ng-if=\"course.courseType == 'competitive'\" ng-repeat=\"course in vm.courses\">{{course.courseName}}</a>\n\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"all-links-modal-wrapper\" ng-class=\"{'ds-open':vm.shouldShowAllLinksModal}\">\n\t\t<div class=\"all-links-modal-content\">\n\t\t\t<div class=\"close-btn\" ng-click=\"vm.closeAllLinks()\">\n\t\t\t\t<i class=\"fa fa-times\"></i>\n\t\t\t</div>\n\t\t\t<div class=\"cat\" ng-repeat=\"(cat,links) in vm.allLinks\">\n\t\t\t\t<div class=\"cat-name\">\n\t\t\t\t\t{{cat}}\t\n\t\t\t\t</div>\n\t\t\t\t<div class=\"links\">\n\t\t\t\t\t<div ng-repeat=\"link in links\">\n\t\t\t\t\t\t<a ng-href=\"{{link.link_url}}\">{{link.link_title}}</a>\n\t\t\t\t\t</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"subscribe-modal\" ng-class=\"{'open':vm.shouldShowSubscribeModal}\">\n\t\t<div class=\"subscribe-close-btn\" ng-click=\"vm.closeSubscribeModal()\">\n\t\t\t<i class=\"fa fa-times\"></i>\n\t\t</div>\n\t\t<div class=\"subscribe-title\">\n\t\t\t{{vm.subscribeTitle}}\n\t\t</div>\n\t\t<div class=\"subscribe-input\" ng-if=\"!vm.showSuccessMsg\">\n\t\t\t<input type=\"text\" id=\"subscribe-tf\" ng-class=\"{'error':vm.showInputErrorMsg}\" ng-model=\"vm.subscribeInput\" ng-change=\"vm.validateInputField()\" placeholder=\"Email Address Here\">\n\t\t\t<div class=\"input-error-msg\" ng-if=\"vm.showInputErrorMsg\">*Please Enter valid Email Address</div>\n\t\t</div>\n\t\t<div class=\"subscribe-btn-wrapper\">\n\t\t\t<div class=\"ew-rounded-btn subscribe-btn ew-close\" ng-if=\"!vm.showSuccessMsg\" ng-click=\"vm.closeSubscribeModal()\">\n\t\t\t\t<i class=\"fa fa-times\"></i>\n\t\t\t\t<span>Cancel</span>\n\t\t\t</div>\t\n\t\t\t<div class=\"ew-rounded-btn subscribe-btn\" ng-click=\"vm.subscribeUser()\" ng-if=\"!vm.showSuccessMsg\">\n\t\t\t\t<i class=\"fa fa-envelope\"></i>\n\t\t\t\t<span>Subscribe</span>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<div class=\"alert alert-success success-msg\" ng-if=\"vm.showSuccessMsg\">\n\t\t\tThank you for subscribption.\n\t\t</div>\n\n\t\t<div class=\"alert alert-danger error-msg\" ng-if=\"vm.showErrorMsg\">\n\t\t\tSome Error occured.\n\t\t</div>\n\t</div>\n\t<chat show-chat-component=\"vm.shouldShowChatComponent\"></chat>\n\t<!-- Chat Module will be added dynamicall here -->\n\t<div class=\"chat_fixed\" ng-if=\"vm.isUserLoggedIn()\" ng-click=\"vm.shouldShowChatComponent = !vm.shouldShowChatComponent;\">\n\t\t<i class=\"fa fa-comment-o\"></i>\n\t</div>\n</div>\n\n";

/***/ },
/* 160 */
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPwAAAD8CAYAAABTq8lnAAAcuklEQVR4nO2d2XNdxZ3HXVSx+IWiikn+gaQqU0PCFkKqhjxMHmCopCYQD1SqHF6JE5gKKV6Yh4xIDIwN3vAiGxvbAm9439eAbK6177sla993XUxCSEIq09Pfq9PXfVtX0pV0z/l13/49fApKlq66f7/fR+ecPr0sE0IsYxjGD8gbwDBMdJA3gGGY6CBvAMMw0UHeAIZhooO8AQzDRAd5AxiGiQ7yBjAMEx3kDWAYJjrIG8AwTHSQN4BhmOggbwDDMNFB3gCGYaKDvAEMw0QHeQMYhokO8gYwDBMd5A1gGCY6yBvAMEx0kDeAYZjoIG8AwzDRkbUPilXUMtFyj+SfJU9LfiVZLcmXHJYUSuolXZJJSVwi0hAP/r0r+P7C4OfxOb+XvCz5D8lDknst6HPO4Z3w8VufM3MQiPavklWS7ZLrkuFZBA6bseD3bw/ag3bdSx0jl2HhPUbG4w7JtyW/kOyTdBKJvVA6g/b+Imj/HdSxdAUW3jNkDB6WvCYpWYhk1yvrRHldk6htbhPNNztFW1ev6OjtFz0DQ6J/eFQMj02I0YlJMTEVl3wm5O+aAb6Of8f34fvxc/h5fA4+r/lml6hruSkq6ptFUVX9Qv8IlAT9epg6xjbDwuc4ss/LJT+TFEhG5hMHolU13hBNUuqO3gExMDImxifjaQUOm3H5xwG/H+1Ae9CuDP8QjAT9Rb+XU+fAJlj4HET2807JC5Ky+eTA1RQydfcPJq68pnQ2gnaivWg32p/BH4DSIB53UueGGhY+h5D9WxEU96zFXyZvy5vbu0Tf4Mist96ugX6gP+gX+peB/Cuoc0UFC+84sk/fkmyS3EpX4Hj2rmluFR09/WJ03I0r+FJBP9Ff9Pv67OLfCuL2LeocRgkL7yiyLz+V1KaVXFLd1Co65bMvnoOzKZNroP+IA+Ixh/yI40+pcxoFLLxDBM/mv5YMzvY83t7TRzbIZjuIC+Izx3P/YBDfnH3WZ+EdQLb7fslvJV+aRYpR68a2TjE0Ok4ulEsgXojbLKP+Xwbxvp8699mGhbeY2PSsN7xb/sosyrLapsTVaiKeGwNvVCB+iCPimUb8r4L458zsPhbeQmQ775b8RvKFWYSVDS2JySpTFsiSSyCeiCvim0b8Pwf5uJu6NpYKC28Zso3PxdJMkKlquCH6hkbIxfABxBnxTiM+8vIcdY0sBRbeEmTbnpI0zbii17ck3jFTS+AjiPssV3zk6SnqmlkMLDy96A/G0sxrxwSSnoFh8qJnPk/kYZYJPcjbg5T1s1BYeFrZVwbPh8kiKq6qF+3dfWLqs1vkhc7cBvlAXopnjupjRH8lRf0sBhaeRvTHJFXmFaOprdP7iTK2g/xgDn+aqz3y+VgU9bMUWPjoZf+lWSyYCDLI79GdAu/xZ5nA88sw6iZbsPDRiY516Cmr1zDPHevA+fbdTZA35A95NKRHnq1cl8/CRyP7s+azenXjDTHiyWKWXAd5RD4N6ZHvZ5dSN2HAwocv+7q0V3ULCpXJHsjnLFf79Yupm7Bg4cMT/buSSj35eLXDz+q5DfKb5hWeNQN6LHw4sv+75E960hvbOsQkz3v3AuQZ+Takx7z8H89VN1HAwmdf9pfMW3jsyUZdhEz0dPUNprvFfykecg3OBQufXdnX6MktrWkUgyNj5IXH0DE0Ni5KaxtN6dfGWXi3hZeffURPanVjK0+iYRKgDlAPhvRH4iy8e8LLz3wkZsyaa2ht53frTAqoh4bWGc/1qJtH4iy8G8LHpqfIdutJbO3sIS8uxl5QH4b0PbEIR/BZ+MXL/n3JqD44h0Ea6oJi7CfNYB7q6PtxFt5O4eXnPCH5XJe9d5CXsjKZg7X2hvSopyfiLLxdwgey/00lCpsg4ow06gJi3AN1Y2yi+dewpWfhFyb74/qVHWuj+bUbsxRQP8Yae9TX43EWnlb4YIBuTJedt4ZmsgHqyJB+LKyBPBY+M9kfDUZTg9v4Or6yM1kF9YS6iqWO3j8aZ+FJhK/TB+j4mZ0JA9SVMZBXH2fhoxVe/swBXXbeQZYJkzSj9wfiLHw0wseMufE4k5y6IJjcp7t/yJycsybOwocrvPzel/Wg3+joJi8Exh9Qb4b0L8dZ+HCEl9/3tB5szI2nLgDGP1B3hvRPx1n47AofjMj/UQUZ54rzQhiGAtQd6k8T/o9LHbln4WcKX6ECjPXsvMSVoQT1hzrUpK+Is/DZET6mbTiJkVJ+187YAOrQGLlfF2fhlyZ8bHor6WRQO3lbKsYiUI/G8/yitsBm4W8lN7FInsWODQipE8wwJsbGmNj3fsGbZ7Dw08KXq0Bii2HeXZaxEdSlsQV2eZyFX5jw8murUp7bR/m5nbEX7HtvPM8v6Cw7r4WPTa+ASwYPJ4dQJ9QHcKWqlFeqA8fPiLc27RBHz14ib5NLoE6N5/mMV9b5Lnxy88mqxht8/FPINMtn0FMXPxbr8neLvLc3J7lUeJ28bS6BOjXOsquKs/BzCy//f6V+K88HO4ZDT/+guHKtSGzdvT9FcgWu8BM8ZrJgUK/Grf3KOAufXnj534ckX/KtfDhgskhFbYPYd/RUWsl1Dhw/S95eVzFu7VHPD8VZ+LTCl6pAVdQ389TZLIFb9hPnr4i3t+6aV3TF5at8O79YULeoX0360jgLP4Mnk7fyEt6mamkMjY6JWGml2PHBoYwl1zl54Q/kfXAZ1O/11AG8J+MsfAoNKjhNbZ3kCXOVhpY28dGp8+LNjdsXJboCdwPUfXEd1LEmfEOchU/ynAoMtgfmhTELY3BkVFwrLhf5ew8uSXITPOtT981lUMfGdtfPx1n42uUx7aSY9u4+8kS5wo32TnHkzEWxekN+VkXXwSh+dUMzeV9dBfWsCY86Xx73XPhXVUDK65p4oG4eEJ/iyhqxa/+R0CRPB+4e8BqvrZN3GFpovspTp92+GvdY+K9JvlLB4GOhZmdgeFRc+bRYbHyvIFLR0/Hurg/E0TOXREllbeJ9PnVsbAd1rQmPev9a3FPh81QgqhpukCfGRrr7BsTpS58seRAuTHDbj1F93PqP8kSptKC+Nenz4h4K/0+S/1NB6B/iPeV1Onv6xbGzl8Tr72whF3oh/O/m98TBE+dEZV0jeQxtAvUdS31Nl3KV90H4/0le3Rv56q7ArTuultTiZgM8fuAxZHxyijyuNmDMs0+5yvsgfHJji/4hPkQC4NXam5vsvXVfLO9sez8xCYg6vtQYV/kv4h4J/6LqeGV9C3kiqBmTV8CCj06Qixk26OPI+AR5vClBvWvSvxj3RPg+1ekuz0d5eweGrBh5jwoswe3o8XeuBepdE77PB+GfUR0urW30+r17e3dvYhkqtYRRs3rDNnGjvYs8/hSg3lH3mvTP5LrwNaqzPs+qw7hFmDPkbAdvH7p6+8nzQIEx+64ml4X/huooztz2dYMFbCW1fvsecumoWbtlp5cj+Kh748z5b+Sq8MlTX33ecnr/sTPkstlCweGT5PmgwNjaek2uCj+lOjng6ekx1fXN5JLZBjbOpM5L1KD+NeGnclH4FaqD5XX+rr5ayK4zvuDr2nt4oEm/IteET25f1e7paxmscqOWy1awEIc6P1EDDzThS3NJ+DtVx7Dtz/iknxtcbNtzgFwsW9m29wB5fqIGHhjbYN2VK8L/XHUK52pTB5qCrr4BcqlsBysDqfMUNTWp58y/kCvCl6lO+Xr667WSCnKhbAdrCajzFDXG6bNluSD8Pcnb+co6b/erO3TyHLlQtoPNN6nzFDXwwTi44h7XhX9edabG09t5sNjton0CMaLOEwXGbf3zrgu/W3Wmo8fPqZSAZ9bND2JEnScK4IUm/G7XhR9WnfH5nDjzoEZmJogRdZ4ogBea8MMuC/+g6khZrX+zqXT4Cj8/vl7hAfzQpH/QVeFfU51ovunnckgWnoXPBPihCf+aq8KXqE70Dfq9jRULz8LPBfzQhC9xUfg7tA6IiSk/l8Ky8Cx8JsAP3Rf445rwD6jG4+hc6oBSw8Kz8PNhHDH9gGvCr1KNb7rJp8Gy8Cz8fMATTfhVrgn/oWq87xtVsvAsfCZ0p25wuc814TtU4/n4IRaehZ+f0dT38R0uCX+fanhxVT15IG2AhWfhM6E49Tz5+1wR/t9Uo31dDmvCwrPwmVCdOq/+h64I/4pqdIun+4+bsPAsfCbAF034V1wRfqdqtK/r301YeBY+E4z18TtdEb5INdrX3WlNWHgWPhOM3WyLXBF+TDXa1w0vTFh4Fj4T4Ism/JgLwt+rGlzEI/RJWHgWPlOKUkfq77Vd+IdvT6nlo6AVLDwLnykVqUdKP2y78D9Rja1ruUkePFtg4Vn4TIE3mvA/sV34l3kN/ExYeBY+U4y18S/ZLvxq1di2rl7y4NkCC8/CZwq80YRfbbvwyU0ruzw8WGA2WHgWPlPgjSZ8KJtaZlP406qxvYPD5MGzBRaehc8UeKMJf9p24WO3J92MkgfPFlh4Fj5T4I0mfMx24RtUY4fHJsiDZwssPAufKfBGE77BduH7VGPHJqbIg2cLLDwLnynwRhO+z3bheVptGja+V0AulO0gRtR5sgHXhL+lGjsR93unWp01m3eSC2U7a7fsJM+TDcAbTfhbtgv/d9XYqc9ukQfPBmoaW8hlcoWGljbyfFHjmvDJxlIHzgYKi8rEW5t2kIvkCogVYkadN0pwodQ8+jsL7wgXC2PkArnKpcLr5PmjRPeIhXeEtVt2kYvjKj4/z7t2hedn+AAemV88G3bsJc8fFa49w/MofcDeQ8fJxXGVgo9OkOePhc9M+HF+Dz/N+Y+vkYvjKogddf6ocO09PM+0CyivqScXx1UQO+r8sfCZCc9z6QM6e/vJxXGVzp5+8vxR4dpcel4tF4BBS55ht3AQs0mPx39cWy3H6+E13j9wlFwg13h//xHyvFHi2np43vFG49TFj8kFcg3EjDpvlLi24w3vaadRVFFNLpBrIGbUeaPEtT3teNdaPXkd3eQCuUZrh99149qutbwvvcb4ZFy8sXE7uUSugFiNT/r9Ote1fen55BmD7QUHyUVyhfy9B8nzRY1rJ8/w2XIGR89cIhfJFY6cuUieL2pcO1uOt7kyuFZcTi6SKyBW1PmixMXTY0GxavQgnw8vGm/cJBfJFRAr6nxR4ur58DtVozt7+V382OSUeGNDPrlMtoMYjXk+YAdfNOF3uiL8K6rRLe1+v2JRHDh+llwo20GMqPNEDXzRhH/FFeF/qBpd3dRKHkQbqGu6QS6U7SBG1HmiBr5owv/QFeHvU40u5pH6JG9v5S2vZgOxoc6PDRSnjtDf54rwoEM1fHR8kjyQNnDmciG5WLaC2FDnhxp4osneEZbsYQm/TzW+u3+QPJg20HKzk1wsW0FsqPNDDTzRhN/nmvCrVOObOJkJsMab96ifCWLi8/p3BTzRhF/lmvAP3J5i20weTFvAWm9qwWxjl+fr3xXwRBP+AdeEv0NrvJiY4r/g4MT5K+SC2cbxc1fI80IN/NB9gT+uCQ9KVAf6BkfIg2oDn5ZWkgtmGzEZE+q8UAM/NNlLwpQ9TOFf47XxqXR095ILZhvt3bxRirEG/jVXhX9IdaKstok8qLawLn83uWS28M623eT5sAH4oQn/oKvCg2HVkRF+H5+An+Nvg1hQ54OakdT378Nhyx628MlNLTs83mtch9/H34bfv3+e8EITfo/rwj+vOlPD8+qT8EGTmxMxoM6DDdSkzp9/3nXh71GduV5ZxxtiBJRW1ZELRw1iQJ0HauADvNCEv8d14UGZ6hCvj7+Nz4N36Dt1/G3AWP9eFoXsUQj/c76tn0mtx0tma3kpbALjdv6FXBH+ruRtPfa5m+TbesW+Y6fJ5YuafUdPkcfdBuDB9dTZdXflivCgVHWsvaePPNi2gKSv3eLPgZPoq+/7zivaU0fnS6OSPSrhV6jOldfxYhqdtk5/TqdBX6njbQvwQBP+P3NNeDClOsi72aZy9Gzu712PPlLH2RYGU3ennYpS9iiFX6s62djWQR50m6isbSQXMmzQR+o42wLqXxN+ba4K/03VyaKqOjHBmx4kKa6sIRcybNBH6jjbAOoe9a8J/41cFR7UJAfvunnwTnGp8Dq5kGGDPlLH2QZQ95rsNfJrkcoetfDPqM6Wylu8qc9ukSfABnxYUMMLZT5P1DvqXhP+mXiOCw/6VIe7eIPLBPuPnSEXMmzQR+o4U9OVulFln/zasrgHwr+oOl3JR0on2LnvMLmQYYM+UseZmsrUo6BfjHsiPPhCdbx/aJQ8EdS8u/MDciHDBn2kjjMl/cOjuuxfxAPZ454In6c6X93o97xqrJhaszn3Z9uhjz6vlkSda8K/7pvwX9M6n/jrR50QKvqHRsTr72whFzJs0Ef0lTreJDlOvbqDr/smPHhdBaCqwd+rfLtHG1v6umEl6nu2q7tPwn9d8pUKRO/gMHliKKhvaSUXMSrQV+p4Rw3qWpP9K/Pq7pPw4FUVjPK6Ji/fy5dV15OLGBXoK3W8owT1jLrWhH/VlN034ZdLRlVAfJx9d62kglzEqEBfqeMdJcasujHUu+/Cg+RGlzgf27eR3G179pOLGBXoK3W8owJ1bJz3/nw62X0UHjSqwDS1+bF1cVNru9h94Bi5hFGDPqPv1PEPPb+pp8E2zSa7r8I/qYKDbX+GRsfJExYmPt3Gz0Yu396jfo3tq55k4TXQ6Zi2DRaOzs3VAbyOnj5y2WyhIwe3O0PdGkc/l84lu8/C4yy6L1Wg2rpy853th0dOkYtmC4gFdT6yzU1Zt5rsf0Fds/BphA+kX5m8ta+sy7nz6EZlf3yYVZcpiMVoDuUY9WocLPHz+WT3WvhA+ioVsKrGG2LKgkRmC5/euWdKrrybR50a8+WrMpGdha+ofUwLWk7d2h84fpZcMNtATKjzkg3aUm/lwWMsfAbCB9Kv0m/tB3Ng1H5iKi7e2JBPLphtICYTjs+9SIzKp97K/ypT2Vn429KXqwCW1TWJScc3vaxpbCGXy1YQG+r8LBbUZVnq9NnyhcjOwt8W/hHJn1UgXd/a+vDpC+Ri2QpiQ52fxWJsOY23TI+w8IsQPpD+Wf25yOXTZ33Y5GKxIDbU+VkMxumv4NmFys7Cz5R+XerzvFun1lTUNogtu/eRS2U7iJFLh1WgDo3n9nWLkZ2FTy99hQpsaU2jEwtscCSyD5tTZhvEzPbjpFF/qENN9orFys7Cpxf+0Zi28WV1U6u1U2+b5TNdwUcnyMVxHcSw2cJxG9Rddeq57qjLR1n4LAofSP+0/rzUYNmKq47uXnHg+BlyUXINxLTDoq2xUHfGc/vTS5GdhZ9b+v/Sg93a2UNeAD39g+LYucvkYuQ6iHHvwBBprlFvhuwvL1V2Fn5+6dfoQe/upymCwZFRceZyIc+NjxDEGjFH7KPON+rMkH1NNmRn4TOTvlgfue+LcPvj0YnJxMGIb23aQS6AryD2yAFyEUXOUV/GiHxxtmRn4TMT/juSepWAIpmMsPe2x2BNYVGZeHvb++QFz0yDXCAnYQ7goq6KUmVH3X2HhY9Q+EB6jNz3JqWvqpO3euG8oy8qrxab3isgL3AmPcgNcpTtvKOejLPcUW9LGpFn4RcpfCD992LTu4EmN8HM5vZYmDSzdbc/m0y6DjbJrKzLzuQd1JGxCSXq7HvZlp2FX7j0j0s+16Vf6uq6Op404zTIXd0SJu8MzpQd9fV4GLKz8IuT/gnJ33TpBxZxe5+YNHP4JHnBMtkBuVzo5B3UjSH7X1FfYcnOwi9e+h/EtNV1ePbKdPR+etIMb06RqyC3mUzeQb0Yz+yopx+EKTsLv/Qr/Wcx7ZVdV9/grAnGRA6eNOMPc03eQZ0Yr94+C/vKzsJnR3o80/dpiZsxI29odEycufSJ+N26reRFyEQLco7cD2mrLtPMoOsL85mdhc9yAGPTr+yq9CQ2tHaI0fEJcfnqdfHmpu3khcfQghpALdS33DRlR91k/dUbCx+i8IH0mJxTrCfz3CdS9o0sO7NZvCHr4PSVa6bsqJesTqph4SMSXhN/rZ7UP1wvE+t37CUvOIaOdfl7xJVYmSn720Xl1ZGKzsJnkYrahmXb9hxYJhOM3/GSntxrZdVi5/6j5IXHRM+ODw8n8m/I/jLqBKBmKmsbWXhXhK9rbl22c9/hZSqBitj0evov9EQfPnNJ/H79NvIiZMIHeUa+DdG/kvzYrBWAGkItsfCWCt9ys3NZweGTMxKXlyr9dyWVetIvf1rCt/g5zvrtexN5NmSvlnxvrnoBqCnUFgtvifC9A0PLDp04N2fSTOTvXK8n/9PyGrH38Ele255jIJ/IK/JryL5+IfUCUGOoNRaeUPjuvoFlb27cvqDE5d2WHltg/1kvhDN/+FSs3bqLvFCZpYM8Ip+G6Ng3/tnF1AtAraHmWHgi4T88cmpRicu7Lf3DkjJzQI+v9u6iruppBuZwktEjS6kXgJpj4YmEf3fnB0tKnkK24ZdGcYgLhUXy2W8PeQEzmYN8IW9mLpHfbNQJQM2x8ETCb929PytJzJuWHqfWpszO+7S8Vhw8dSExSYO6mJnZQX4OnryQ7lkd+XwsWzUCUHMsPJHw2/ZkT3iFbM9KyV/0wiksqRS7D50Qv1vHt/k2gXwgL8iPITrytzLbtQFQcyx8DgmfNy39g5IS89YQr3a27T1IXujM5kQe0rxqA6WSh8KoizwWPjeFV8i2PSVpMovq/CfXxRbe+ooExB3xTyM68vRUmPWQx8LntvAK2cbnJCNmkZ37OCY2v88HRUYB4nxWxjuN6MjLc1HUQR4L74fwedPS3y35TWz6XW7qFb+wSGz/4CN+lZdlEE/E9Xz6kfcvg3zcHVUN5LHw/givkO29V/JabHoedkoRXrlelhhEWr0hn1wWl0H8EEfEM43ofw/if2/Uuc9j4f0TXiHbfb/kt5I/mUV5tbRaYKHGBt7LfkEgXogb4pdG9D8F8b6fKud5LLy/wuvIPqyKaYdi6Fy8Wix2Hzwu3uSjqdKCuCA+iFO6+AVxXUWdYwULz8Lr4j8jqUlXuJgUcvry1cQ6fN8n8qD/iMPpK1fTTZZRII7PUOfUhIVn4dOJ/03J25Kp2eQ/dblQ7DpwTKzZ4sdiHfQT/UW/55B8KojbN6lzOBssPAs/n/wrYtOTQWYr8sQEkoMnzyfeMefKgB/6gf6gX7NMkDEny6ygzlUmsPAsfKbi3yV5IWaszpt59a8VF64WiUOnLiZeSa3ZspNc3kxAO9FetBvtRz/mkbwsiMed1Llh4Vn4sOVfLvmZpCCWZkKPSWFJVWJt96FTFxLPvhvfKyAbBMTvxe9HO9AetAvtm68PQT8Lgn4vp84BC++g8NlaHktNbHpd/n/H0szfnwus+cbo9smLnyRWixUcPiWfk4+K/IJDYtOuD8U7+bsTV9633t2RuMUGr78zLS7+q76Gf8f34fvxc/h5fA4+D5+Lz8fvSbPGfD5Kgn49TB3jbPHuLl4eSyZ8/t6D5AUQgvx3SL4dm37Vt1/SuUDJqOgM2rsqaP8d1LEMA9QcC79E4Zl5wew+nJuHjTp2SIpiGTwKhMRI8Pt3BO15ImgfdYychYVnMgVjAf8i+VFseu/91ZJ8yWFJoaRe0iUZksQD/hGbFvcf2teGgu+rD37ucPA5q4PP/VHwe5Zb0Oecw1nhGYaxH/IGMAwTHeQNYBgmOsgbwDBMdJA3gGGY6CBvAMMw0UHeAIZhooO8AQzDRAd5AxiGiQ7yBjAMEx3kDWAYJjrIG8AwTHSQN4BhmOggbwDDMNFB3gCGYaKDvAEMw0QHeQMYhokO8gYwDBMd5A1gGCY6/h9CiPl2EsGh+QAAAABJRU5ErkJggg=="

/***/ },
/* 161 */
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPEAAADxCAYAAAAay1EJAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAFUhJREFUeNrsnU9sXNUVxh8jS0QCeaYCyaBgPFUkkGqLTANEwkjxFFiQFaaVCLtMFgmrNq5Kdw0Y0l2DmrQrkkXGO4JUEjZ1Fkk6QcJI4U/HKEZKK1QbJ0otEdUTJVIqIaX3G99JJ86M5715f8+53ycN4xDHnvfe/b3vnHPPve++27dve1R29cnQowXzVjKv1rvX9meoaF4jIX7Fknkt2q9Xzatuv663/rxj5eoqr0R2dR8hzgysJQtkqQ3SiQx9xPNtkNct3Iu8coTYVWBbsJbt+4Tgwzlvoa4RbEKsHdpy22tE8eEuWaCbL0JNiCWDO2neJh2A1g/UpyzQpzgyCHGWoS1YaPF6hWekqz62UJ9iwYwQE1wCTRHi0KHybp6NSNSwMFcNzDWeDkIcF7hF81axrxGekVhz6KoFepGngxBHAW/ZgkvXTV4zdGdCHAZegDtlXlt5NlLXeQtzlaeCEPcCt1WommbInNlQe5owE+Ju8E7ZV57DgjATYnlh82HCS5gJsUx4GTbrgXnK1Y4w5yC21WbAO8Gxr07nrTPXCLFOeIsWXk4V6deMdWYnusByjgAMeOsE2BnhOi+a6z5FJ5YPL9bqVj3O9bqsefOqGFeuE2JZ8BZs6LyfY5iyeseAPE2IZQBctu7LqjPlhCvnlAGMO+3fCDDVRUir/m7HCZ04Y/AWvbWlbMx9qSCuPKlhpVROAcAVb63yTICpoK5ct2vD6cQpwYviFdolOW1EhdUR48hThJjhMyU/vC5LbBDJCQS4zPCZiim8XrS9BYQ45vwX1WeuOKLiEMbV3+04YzgdA8BV5r9UgpoxoXWFEEcDb8Hmv1x1RCUtbKlbyXqenGmILcA15r9Uisp8wSuXYYBRYGABi0pbGH81OyNCJw4IcM1jAYvKjhrWkTPXd50jwFQ/GsgPepuGh1065Lx15MxNQWXKiQlwNlUYH/ceGBv1Nj0+7D04Otb8emBw0Pv61V94q3NzdGRCTICz5rB5A23h+fE1eEdHu37v3BNPej80rjO0JsQEOG0hLH5o58veI6/v2hDadt1cWPC+fOEl5sgZAHmAABNcv+C268bFBddPYStHTh3kAQLsXn67+Y293kMvvxzq5ziYC28EcinNdcmphdNs5Ege3pHfvmny3eci+XkO58OdlGpDSCoQawYYhaHN+/Y1obm1vGzCzoveTRN6puVcD46NelsOHowMXubD2QM5rXBa7VpgADO06zUbaz135+vW4P/3Bye8a7Onm4DHLTjvyJu/ifznMpTuKIxnbFJRUe/E2lcjbT35kS/Xu3b6tLf0h0OxFIhQtBqdOd5XwcqPvnrxJRa2uivxXUJyCQNc8ZQvJ/TrsCgsbTt7xtvy+3ebIXiU4fO2c2diA/jW5csEeGPtT3o9ci5BgMvm7bj2K/jtgQPNsNmvNu/d623/4vNmDh1WQ6/vat4Y0E0Vl67NzhLT3jqeZHtmLiGAizYPVi9UbFH0uXLsmP/ChIHuqZN/aeawYRz4ySOHYz++FZPTU75UswVc+RC3Lep3ai7429+95S1U9ng/XPc/DYMi1JN/OhI4vEYO/JTJxWNPFRhKB1FzDlmLE8MenJwLRhX6K+PKQcJrVLNRHAsCMopYcYbQLV15/yjRDKatxsQOi4bYhUJWT/daXg4cXqMo5Rfkzfv2xlbEuitNMBHFygmG0n1of9wb1OdiBLhoXZjqI7z2AzL+LkweHTSqYIdW36rGuTNInE7sXB7sB4SvX/257/C6F8hDu3YlEkZDmNOmQuXHVVEQ26fOsSe6g1AYmjcgo9kjCMidhBVISWjlxIeJdJgp10RcT2PMxQBw2by9zWu2QX5pwtKF3Xt858kAGVXrdqEinUQuTBeOVG/HMX+cixjgQpxhg8Y8+dJ+fx16qFqju+tOfPb8eDIAH3qPLhxxfpx1J0a4wAd8BwlVPzjR3KvKT8EL3V1DNoROYpM6zAtfOcpppYi1NeqwOjKIbZiwn9couLAqCAUvPyBvOfhuszsrijbN3pHCAVakBYTVUToxw+gQQsHLT+Ua1eifzFQjXTTRScjXUU2nYlNk06+RLEU0dxUkdn/kdQkvwIlqdFJFq07CjQQVdLpw7Pr1jpWrh1OH2E5iY6MwzgkrABkh/YVnniXAyQg7ZhbD7gYSRTg9TYAjBskANB+gKSRKgL+mAyepfBRhdSgntnPCf+O1kO/ILYC5SikV/TTMtrdhnXia51++I+PnI4QmwKkplBv3DbFdocQHfycEMuZs4xCaObDKiiF0qpoIs9IpjBPThRME+ZvdlUAbDPQS+qEvPLOdLZUK3LivnNi68HGe92SFJg/s4NHvyqXG3Gfe97OziW2ZSwXWHpMbV2OH2PZHL3qsSKcidGphP65u+vbAW833gfza5Wl8OmccvMF8V4aWDMTFJCBGGM1VSuscEo8DbT2/F/Kz9zScEULbJZwRwPlxSPRPd9sUL4mnM7SeV/zg2Fizh/t+c9ybHnts45TApAI37Y3kxsJF79Z35njNcfPmEt6N+4F4lS7sNZ8o+PDOnc33KBfmo4CFbWGvvH9sQ6CxogkLIjrezg+9F3muG9fxAm6E960wnwruxoEgdj0Xbj1nCU7Yy3miEDYOwHLFbjBjg7xOTzcEGNigL4q8F9Bu+f3BRI4XNzCs6sLKKcer5YHcOCjEyIVHXIQXe1l1c77Yb81dnHWjZhDcALDxQJhjxkYEYR+B2q87Xzl6zGWYA7mxb4hddeEmvPv2JraX1Ub580Klcs+g3qhijXXK/Tz8LAuLMNphdnQazLcbB4EYbWHO7JuF4s0Tfz6SSBjpV92KVgh5R6vHO4anF55+ViTA648baYVjT2M8byAu+/nGnE+Ay64AjEGMohGmcbIEMASw2rfouRM6z57uuF8XPj+iiCBCCJ0lgFvHjevR6dgVa8LvxgF+O7YqLpw1hKZwobRyXz/CZ+u0qwecqlOPNdIBvxsIoGCXRg4c5NifPncmka2JMqKpSCC264XVP8UBIelTGQsjuwlhficwUcha35qJXBkVdV8RyMHsOx2uDx7diuvlgHb7eSibHydW78IIOZFTpl288iuEyVsOHrwHZEwptTq27nLjN3+zoXu1KtFSjh+fE9drKKF9t1NWT/56Fra0Tyth8GI7WMlq74aCOnWLobrdKgwB2lZnGb6WEH10ExZyXPqV6v0Ze043bQixXR51kgBTBDlV/cyAXOs3nJ4kwFTWheu4/gkZLoXUXZ3YJtT/IcAUHTl1NYwTF/pxYpUujCkXAqzXkYPOiwtR3nZMEmJUM1GppfQK02RKq9aTgcJpjaE0Gjm2nT3DUe6AFO/c+aNOe1TnXHBhTKPg0SeUG0rqUTdZcWMnIEYhK2t90FS8wvVWWLEuB4H4FS1HjUJHlvuBqfiE664sP/bnxGH2v83c3Xh4uFmNptwVCl2KFkzk7YrCnk6sBmJJ/cBUfPmxsiWMk34gLms4UoRRfnacpNwIqxWterqHz7ummOyyw3+Jv/vmB71t586ymEXdUT+7nGRYP96xcnWxmxOrcGGsnyXAVLswHhTVR8obhdPiIV7bVnYvRy3V4ea+V8vcsW6I4cIsZlEdb/A+dzmRBvGdnFhDPoy77PYvPifEVFehJRPPYlawn/WdFsx2Jy5JPyq6MOXHjYd2qWgAKXcKp8WH0o7suUSFvdm/oSKkLnWCuCQdYFakKT/COFEwb9zRiSckH9HDO9kfTfnXI/Kjtrud2O9O85m9sw4Pc5EDFUgYL8Knm/K2GH3HiYuiLwhdmOpr3OyUfgh3QSzaiR9hQYtyMwUrq4AYobTkzc8phtRROXFB6lHknx/naKRcDanvglhsZZpVaSqMOj1hUpCaEXTOz1PXMu3E43RiytlILt9yYrH5MLahZZslFUZo/JC8fQ+268lJzocfGBvjKKQiGEeyC6PinZiiwo8j0WZQyok++aN0Yiq8hBe3CqKdmBvhUQyn1yAWmRMrfEQHldZYkl0clRtOM5SmGFKvCRAXJX7w+x8f5sijKAvxiMQPrujRHFQGJLjpQ3Z1mqIoLy83J+YcMRWhJBdKxUI8MJjnyKOiMwXBhVKG0xQlXISYoggxRVGEmKIoQkxRhJiiKEJMURQhpiiKEFNUcP1wvUGIk9aNhYsceVR04+niAiFO/M4p/0nvFBWFGoB4ieeBcj6cbogNp+uAeFHk7efTOY48KjLdZDhNUcKdWHhha1XiB2dhi4p0PMl14mY4XZeZw7CwRUWjW5cvS/74q6LD6cbcZxyBVGj997tl8RDXpX76W8vLHIGU66lZXWxOvJbHMC+mnM6HmxLtxDeFn3yK4XRY7Vi5WsuZ/4h14tU5zhVTTo+jRsuJofNi3XiBbkyFoEB2cbTeDrHgvJgQUyHGj+yi1mI7xGLzYobUVKjxI7t9VwfE7KGmwoXTosdPTQXEmCsW3nFDpSTUU4R3/v0/J96xcnVR8pFcm53liKRcS8UarZml9rZLsRXqVYbUVD83/7+eFu/C6yGWmxezuEUF1A/Xr0t34loniGtiL4jJa66dPs2RSbl049flxND3s4SYcmq83OvEtrgldr8tFrcoh8bLfHu7dK4b3QypKbUAm3GiYWpJHcQMqSkXQ2l1EDOkpnpGbNevaxgnp7pCLD0vZkhN9b7Riw+l59cvH871olya/v3BCY5UaoNQWrwL3xMt5/x8k7Q7LXupqU7CuLgmPx8+1RNiY9WnpB8lc2Oqk1bkR2nol/blxNDHko/0yvvHOGIpjRB3NNhckG8WEzYtL7PARd0N8IkPNWxx7A7EdGNKoQtDNd8Q2xK26JAaK1RY4KIgLP5XsI3Tx912ps0FtW5JWvrDIY5gyrt8VEVUVu32F/fdvn274198MvRowVvbiCsv+cjH/3nJGxgc5Eh2VIjGLjz9rPTDQFW60O0vuzqxtW75ufFR5sbMhcVrQw57PRWxKh/io81+Wco94brj+ivQ4b4hthPLS6IvZOM63dhR4boreI71kuGwHsaJ6cYUXTjDLuwMxHRjurBgVUNDbJcnztCNKbpw4prx89TSXFR3A7oxRRdOPpSGus4Tr9cnQ48iud4q+YwM5Ae97V98znlj5S584ZlnNUB83rhw2c835qK+K9CNqTT17YG3nHLhQE5s3Rj58Yj0s7P9y8+9TY89xhGvTEq6syBMKxX9fnMu4A+fVnGG2FOt04V/d0DLoQTiLJATa3LjrSc/8vLjz3HkK1Fj7jNv/tWfO+fC/Tgx3ZjKaC7spgv3BbG5S1Q94a2YENaXYrcHSr6uHDvm3bi4oMWFq7FDrMmNcfdmA4hs4fopiqr64qoviLW4MaYiGFZLvxGrmVLqy4XDOLEaN8a8MbZvoeQJxawVPQ8LqPT7DwNXp9v1ydCjNfM2If3sPTg26m07e4ZUCNNXL76kJRf23Z0VtROrcWMMBBRHKEGx56H3tAAMTYX5x6EgtpsGzKgYFCY35u6YMoTrpGSVEnSk16L/uJ24dRdpSD+TKI7845f7SYgA4TopKWY1oohmQ0Ns1zuqCKsxd8wnR2RbSHsU7CF9xwD9rBfupVCFrXZpWKoIcblitsPor154UYsLhypmRR1Ot1TRcGYxQC79imE1w+j4XTiqHxQZxDY5f0fD2cUzbNmSyTA6Rr0TtpgVSzitMazedu4s1x0zjI5a8wbgUpQ/MBfDh1QTVrNanQ19s7uiKYyOnI/IIdYUViN8Q1MBlZ6UNXX8OsowOrZwWltYDT197oz3wOgoiUpY6Gn/8oWXtBxOZNXoJMLpliY9BU0g0MLuPVyymHQ6Y843zrsSNSwPniiI7abzKvLjW8vLzSVvVHLCNB/Ou5Y8OIqmjjScGCDjkYxHNFwFLHnjtFNC59qcZ0zzKdERy0Fsii0n1pgfY9oJG+wxP443D8aGd+zKyogTa8uPW91czI/jy4Ob51fP4obJJH5RIhDb/Lis4cpguoP5cXx5sKLppHKceXAaTtyaP1ZRbmR+HL3QVqkoD94Tx3xwqjnxuvy4at52a7hSnD+OLg9WNB+MQtZUkr8wcYgtyKjWvSL9am0aHva2GZC5bDFcHqzkKYYQnidcSfqX5lI6WBzovPQrhnnMb/Q0JKSir/VUoufTADg1J7ZuXDBvNU/B1NPmfXu9LQffzVR4+v26/LIwPt6cIstS+H9p/5SWLWdhSIkVsjIDsQW5aN5QAMhLv4pP/umIN7TrtdR+P/Zg/n52tlkc2qjTCSA/tHOn9/DOl708wE4pFUBhUMnmC3iIQiktgFOH2IJcso4sHuQkC13IJQFsc18wA2+/ISkcOv/8eBPqpD67okJWwzpwPc0PkTrEmkCOc38uQNswwK5+Otd8j2M+FZ8f7lwwUAPuOKBWtMA/EwBnBmJNIONpEk+d/Cg0yBjsjU8B68XYoA3i1DiuB8bGQu10gmPCAn8FDR2ZAThTEGtzZL95J0JLuNKNhYvere+WvZtmgOPrLDsVwL7/8eHmFBvgHhhcu1zrH9qO6AHHgxy9+ShZHUWsTAGcOYi15ciUOmUOYCiXtbNkT1DZU7KhAKVGS1kEOJMQt4EMR57n2KEyIIzDUhYBzmQ4vS60VtMQQokGuJzmPLBoiNtAPuwpWTRBidJMWq2UqiBugxkgcyNoKiklvhpJPcQWZNwVj3N8UTEL64GrUj6sKIgtyJyCouJSJqeQeikn7SzbE1z0WLmmotV5jCtpAIt0YubJlMv5rzqILcjYUbDK8JrqM3yuxL0vNMPp3uE1LkDJhkMUFSR8LkkHWIUTr3PlafP2Nscn1UN4OuFhLQejCmILcsmG1+zyotZr3obPdU0HpQ5iujLVRe8YeKc1HphaiNtcGWHTBMew07nvlDb3dQbiNpgxfYC7MCvY7qhh4a1qP9CcC1fTFjGK5jXDse2E8DjdogsAO+PEDLEZOhNinTBPWphHOPbFC7tuoOpcc/HgnYW4DeaKzZcJs0x4p10JmwkxYSa8hJgwU4SXEMuDGS8WwNIXClaHNfQ5E+J0YEY1G/PM3OMrec1YeOs8FYQ4CpgL1pmnGGrHHjJj1qCa5R0mCbF8oMsWaExTsQssvNBddYquS4jTzJ0B8ys8G4H1sXVc5rqEODPhNmAu06F7Oi5eNYbLhDjrULeAxsvltc1Yw1sDuK52VBFiHUAX24DGa0Q5tPU2cOm2hFht6A2YS23vEsPvRhuwdYbIhJhuvbZcsmzfixmCuwXron0B2kUD7CKvHCGm/AFetl8C6oJ9ldq+ZSIknC3h61X7av5/5rHZ1/8EGADcbjGmkt0/AgAAAABJRU5ErkJggg=="

/***/ },
/* 162 */
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPEAAADxCAYAAAAay1EJAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAFEVJREFUeNrsnU+MVFUWxp9PWcgsqie0C02wKw4JuLELcTE6GbtGxsSVtKyYjTSwdYbGBbMbGpcstHHYAt1udIUNszHxX7Uz/lkMUO1igISYLkhkYZPpWgiJhDDve9yHZU9V1/v/7jn3+5I3hWPT1nvv/t53zrnn3vfQvXv3PMpeHTjz8Ejw0QiO6NPr+WeoHhxjGf4TneBYNn9eDY62+XM7+ueTu++u8k7Yq4cIsTWwNgyQjR5IJyz6ios9kLcN3Mu8c4TYVWAjWJvmc0Lw6SwaqFsEmxBrh7bZc4wpPt2OATo8CDUhlgzuZPAx6QC0caBeMEAvcGQQYpuhHTHQ4tjFKzJQZw3UCyyYEWKCS6ApQpw5VN7Lq5GLugbmuQDmFi8HIS4K3HrwMWWOMV6RQnPoOQP0Mi8HIc4D3qYBl65bvubpzoQ4C7wAdzo4xnk1KteigXmOl4IQDwM3KlTNMGS2NtSeIcyEeBC80+aocVgQZkIsL2yeJbyEmRDLhJdhsx6Yp13tCHMOYlNtBrwTHPvqtGicuUWIdcJbN/Byqki/5o0zO9EF5jsCMOBtE2BnhPu8HNz3aTqxfHixVnfO41yvy1oKjqnAlduEWBa8IyZ0PsgxTBkdDUCeIcQyAG4a92XVmXLClX1lAONJ+zkBpgYIadVFM07oxJbBW/fuL2Vj7kslceVJDSulfAUAT3n3K88EmErqym2zNpxOXBG8KF6hXZLTRlRWHQ8ceZoQM3ym5IfXTYkNIr5AgJsMn6mCwutl01tAiAvOf1F95oojqghhXF0044zhdAEAzzH/pUrUfBBaTxHifOAdMfkvVx1RZQtb6k7ZnidbDbEBuMX8l6pQ1he8fIsBRoGBBSyqamH8tcyMCJ04IcAtjwUsyh51jSNb13ftE2AqjTZuGPFGN9ZdOuWacWTrpqCscmICbKe2PjbhPVlrhNBuHhkP//zohpp37J8veVd+WKQjE2ICbJvDAtpto83wc3NtcEniz//Y5N264+R70KwC2QqICXC1grMC2N+N7V0X2l5d7y55M58+yxzZApAfIcBuCqHx9id2JQL3FxCvLrl+CaMcuXKQHyHA7uW3L2+Z9rY//mqm33N5pcWL2VPsqnJdcmXhNBs5yod319NHvK2j+TS+OZwP91OlDSGVQKwZYBSGXt5yMITm5o8d71q3HR5VVXGR7+4Zfzs3eJkP2wdyVeG02rXAf3rmHe+FsddN4ul5L3iv/2Lwf9mZ9y5+f9ZbuVV89AXnffXpv+X+ex2cVoojjGdsUjGl3om1r0aa2XkhVqHo4o1z3rn/HA1dOm+haPXG82dSFazi6OinOwr53kpU+i4hfskAT3nKlxPGrdqisHRk53nvwI7TYQieZ/gc90GSRjdvdQjw+jpY9npkv0SAm8HHae138P1vD4Vhc1wh9D72yndhDp1VmC7CgwHdVEUJqQA1VKfLbM/0SwK4bvJg9ULFFkWfT66+G/vvALrDv/8szGGzOPD+HacKPz/k9FQstUwBVz7EPYv6nZoLhiOf+Ga3d/tON/bfQREqTXiNHPjwi58Vfk4MpRMpnEPW4sSo2Dk5F4zQE66cNLwGkElARhGryBA60sdXjxPNZBoPTGxWNMQuFLKGCVNJScNrFKXigow56aKKWL1CRMFQOpUOFr1BvV8gwHXjwlSK8DoOyPh3WfLopFEFO7RSa67InUGKdGLn8uA4IBz74qXY4fUwkFGNLiOMhs5eOsobmC0/nhMFsXnrHHui+wiFIYCMZo8kIA+CuAx91XmvlA4z5Zoo6m2MfgEAN4OPI7xng4Ww9MTXr8XOkwEyqta9CnfZqJXznKQL56YjRcwf+zkDPFJk2KAxTz51fn+sn0XVGn3ZkfJoDomjc5feogvnnB/b7sQIF/iC7wRCxRd7VcUpeP1xy18ehNBlbFKHeWFOK+Wu8bzD6twgNmHCQd6j5MKqIOTJcUCGG0fb6ZQRKbAibX9YnacTM4zOoKjgNaxyjWo0mjvyXDTRT8jX2SddqHKbfs1lKWLwVMHSq3d4X7ILcKIaXVbRqp/wIMEDhS5cuA6d3H13tnKIzSQ2Gmo5J6wAZIT0hz96igCXI+RP9ay7geQRTs8Q4HwFgJI0heQJMB24VNXyCKszObGZE/6c90K+I0cAc5VSJdqeZdvbrE48w+sv35Hx+xFCE+DKlMmNU0NsVijxxd8lgYw52yKEZg6ssmIIXakmsqx0yuLEdOESQT7xdbINBoYJ/dB//eg3bKlU4MapcmLjwqd53csVmjyQI6dduXRlZTGc+0WXGJ3XSu0LcuO5wiE2/dHLHivSlQidWtiPa5A++PbN8DNqBsHrVm7/1GW+K0OdAOJ6GRAjjOYqpTUOCbii9/eGsMV44wIKSnBEtF1ikUH0OUzonx60KV4Zb2eI3leMY9OvxsLz3rRxeMs8IoHwO64uPThfPlyyu3EaiFfpwl74RsFnH58MP/NcmI8CFkJeLDxYD2j0UGNBRD+hWJV3rlvU+SLPx/leuLHANs+UbpwIYtdz4eg9S3DCOM6TVdg44IOlQwNhfuP5D/u+3RBgwI3zWEIIaPHAKON88QBDvo4HmOM5eyI3TgoxRsWYi/BiL6tBzle0Bjnres0geABg44Es57z/udOZX4Ga1p0BssMwJ3Lj2BC76sKAF+5b1l5W6+WTmGZaO6jXq1hjnXKal5/ZsAijF2ZHp8Fiu3GSeeJpl64gijd4vQo2dK8a4PD7jE703WsLhaGT5/f1f/DuOC0WYAjXHdcf75YqaycTizQV9wdjQWx6pJ3Y+A6DGDkgpnHKyAOTCGD1btHzIHT+/mzf/brw/RFFJBFCaBsAXnveuB/9zl2xJuJuHODn/VSQrCg0rSr3jSN8t36uNOhFbkgH4m4ggIJdFTlwknOHK5exNZElms4FYrNeWP1bHFCFtSWMHHpPBryvqV9rJkLSOG4cRSC2C/cHION+OaC9cV7KFseJ1bswBvkbvz1jRe4bRwiTAdxaR8KUEhx5rZBXrudeUSVayvmHWxQF96usfbdtz42HVqe1TyvB1bAdrGTBfXs7n/p1i6G6HVWqAW3UWRb+uSa33IGFHIMKe0o0dLppXYjN8qgPCTBFkCvVHwKQW2nD6UkCTNku3Mc002laQmp/HRdGQr2XAFMEuXJNpoJYqwujIESA9YKcdF5ciGqmY5IQo5pp8xwwlV17nnlba9V6II99C1smlP6vpiuARo4jO89zlDsgxTt3/rrfHtW+Cy6MaRS8+oRyQ2W96sYWN3YCYjQy2NYHTRUr3G/cd2VqJoFYTU8bCh029wNTxQn3XVl+HM+Js+x/a5vQaogFAJS76teeKlg1s6JwqBOrgXj/c6fE9ANTxeXHe8ZVLWGcjANxU8OZIoyKs+Mk5UZYrWjV0/pObJYdiq8ARXtiUVRvWK1E44bTgU6swoVRzGI1muoVxoOiB3tTNcTRtrIU1e/hrmTuWDfENuxMSdmpuLuciIVYQz5MF6YcceOx3m17ep24oeEG0YWpYW6spAGk2Q9i8aG0I3suUTk87BWooc6Jy3o/EiVfGCcK5o37OrHozojtT0xydFIJHvpTupw47k7ztgq9sVzkQCV66AfjRXiBqxY1fUROXJftwrs4KikXx80vIBafD1OUgylYUwXECKUlb35OMaTOy4nFnomDr7ykGFL3hXhC7k1gVZpKr22jTclfP4yg/ThvXbP7JtCJKWcjuVrkxGLzYWxDyzZLKovQ+CF5+x5s1+NLzoejN/tRlMvjSLwTU5Tj46jhS/72dGKKebE3ItqJuREeRSe+D7HInFjhKzqoiiS8OCo3nGYoTTGkvi9AXJf4xRXt6k9RmSEeI8SU6xLcuSW7Ok1RlFcTnBNzjpjKT5ILpb7ci852SypPUxgnxBRFEWKKoggxRRFiiqIIMUVRhJiiKEJMUYSYoihCTFFSdOtOlxCXreurSxx5VI7jqU2Iy39yrnLkUZTndQFxh9eBYjgt1hTagHhZ4je/vNLiyKNy07Uuw2mKEq3bP8kubImMI1jYoujEP4fTIr89C1tUXrp5S3RZaFV0OH1lZZEjkMqslVvL4iEWG0fc/JGFdcr51KwtNicWnsdQHEe5SbQTE2LK9XD65O67LT/4H7FOfOUH5sSU0+OoGzkxJPYsrnc51URlAFh2cbTdC7FYN+Z8MeXw+FnuhVhscsn2S8rh8aMDYubFlMPjp6UCYlQWhXfcUFWF0t0l6Z1/P+fEJ3ffXZZ8Jhe/P8sRSbnmwt1oZqm37VLsGTEvptLowo0F8S68FmLmxZQzun2nqyIfXguxWDtDXnPxxjmOTCpB9Cb+wa/Lie/nxQscmZRL4+X/ndgUtzpybwqLW5Qz42Wpt13aH0Q3Q2pKLcDBONEwtaQOYobUlIuhtEKIGVJT6wtVaQXjZGEgxNLzYobUVJwHvfBQemnt8mF/GOXS9GVnjiOVGijhDR59o2U/zg9Je9Kyl5rqJ4wLbaF0X4gDqxb/qGJuTPWP0ualnwL6pWM5MSSago+vHueIpTRC3Ndg/SQ/LEVYnsgCF9WrrzrvSd9f2i2I77vxLEcu9UD/ujan4TRasSE2JWzRITVWqLDARUFY/K9gpdvZQTvT+kmtW9RZXzrKEUxpqZEMDCWGQdyVfNYoZKBDh3JXiMaUVKUXEkNsrFtBbsxKtctSAPDQqHjYWxHnNEBMN3ZTuO9KHuKzqSE2E8uiq0Pok6Ubu5sLK3iPdSfgsJ3FienGFF3YYhd2BmK6MV1YsOYyQ2yWJ85ruKl0Y7qwMM3HeWupn9fTgG5M0YXLD6VjQ2wKXIsabi7dmC4sRIvDClpJnZhuTInQ+98ecsqFoYfu3bsX+7ceOPMw8uMx6Vfn2CvfeZs2jnHEKxO6sw5/9JSGU8G0Uj3uD/sJf/mMhivEnmqdOnl+n5ZTScRZIoiDpwNCavFLg9CKd2WF72/SJNxPJe/k6hjOioGYbkzZqg+W3nTShVNBrMWN8dTGbg+UfH1y9V3vWret4VQSu3BaJ1bjxqhkcspJtnD/FEVVqbhKBbEWN8ZUBMNq+Q9iJVNKqVw4ixND0xquHOaNsX0LJTAlWlnUsl4Ymkr7F1NDbHYaUFEOPPXv/SRCoBQVsxb77SddhhOryY1RFEFxhJKjc5fe0lLMyhzVZoLYPD1UxDPIjbk7pgzhPilqnz0et0e6KCeOniLiS7wojijq+FEt3CclxaxuHtFsZojNekcVYTXmjvnmCLuFtEdJZ1ZogHHWCw9TogUQ6+nAmYcREoxLv6obN4yECyQe3VAjMRaG0TOfPqvFhVHMaubxi/wcv9SUhivLsJphdEnKjZfcIDbJuYrOCbwalS2ZDKML1FGz7VUuyi2c1hhWz+y8wHXHDKPz1lIAcCPPX+gX8CUZVlO56sTXuxlGlwmxprAa4RuaCqjqpKyp41DWOeFSwmltYTWEsHpzbZxElf0QXVn0jn3xkpbTya0aXUY4HWnSU9AEEoVzXLJYrnC9FfW0dw0PniiITfVNRX68cms5XPJGlSfUI3DdteTBeTR1VOHE0UonFU2uWPLGaadyhOuMaT4lOr7eu4Wtzok15seYdjr84mfMjwsU1nYjD2ZXliVO3KOmp2SRBPI05sfF5sGKFjdMlvEfKgVikw80NdwZTHcwPy4uD1Y0ndQsMg+uwomj+WMV3RPMj/MX2ioV5cH7ipgPrhxiA/Kcp2QTAbgG9+bKLw9WFN0cT7vhnQiIDchTwYeKRy7nj/PJgxU1dOB9wqVvIOlXdLIAWbyNYR7z79+8RhIzSFEleskYVOkqZYqpnw6ceXgk+Gh5CqaeXt5y0NvzzNtWhadr88utj02EU2Q2TY+dOr9fy5azMKTSClnWQGxArgcfKACI30bjwI7T3gtjr1f230efMcDFsV6nE0De/sSu4Jj0to1OVLaDCQqDSlaJYXfFRlUAVw6xAblhHFk8yGUulEAuCWAvr7TCz7QhKRx622gzBLus745IAeuDFahrHLjSebHKIdYEcpH7cwHay+HrO1vhEski5lPx/SOo8VkE1IoW+FsBsDUQawL5yVojbM3MCjIGewRrUdAmcerNI43g3MYz7XSCc0JFX0FDhzUAWwWxNkeOm3citIQrXV9dCnNZDHD82WanAtijG+vhAbg3mvPbGpzr2ugB53Pzx04Y9ispYlkFsHUQa8uRKXWyDmDIt+0qmQvU9JRsKECpUcdGgK2EuAdkODL7GikbhHHYsBFgK8PpNaG1moYQSjTAzSrngUVD3APybHDs5XiiStZ8Va2UqiDugRkgH+S4okrS8SoWM6iG2ICMp+Jpji+qYO0rezmhMxAbkDkFRRUlK6eQhsmXdpXNBa57rFxT+Qpva6tLA1ikEzNPplzOf9VBbEDGjoJzDK+plOHzVNH7QhPieCDXDcgTHJdUgvB50ub5X6cg7oF5Jvg4wvFJDRHeTjir5WRUQWxAbhhXZpcXtVZLJnxuazopdRDTlakBOhrAO6PxxNRC3OPKs8yVnc99p8xbOj1CLBdmTB/gKcwKtjtC5XlaUudVWvku3E1TxKh7St4+QQ0VXqdbdwFgZ5yYIbZTofO0tsIVIR4M86SBeYxjX7w6Ju9tuXjyzkLcA/OUyZcJs0x4Z1wJmwkxYSa8hJgwU4SXEMuDGQcLYNULBatZ6QsVCHF1MKOajXlm7vFVvuYNvG1eCkKcB8wjxpmnGWoXHjJj1mBOwwojQmwv0E0DNKap2AWWXeiuWqDrEuIqc2fAvItXI7HOGsdlrkuIrQm3AXOTDj3UcXG0GC4TYgkhdwS1y2ubsYa3BXBd7agixDqArhuYo2NMObTtHnDptoRYbegNmBs9nxLD724PsG2GyISYbn1/uWTTfNYtgjuCddkcIbQElhBTyfJrz0A9Yo5Gz4+khT2CMxL+vGqO8P9nHmu//ifAAF/ECzxU2C/nAAAAAElFTkSuQmCC"

/***/ },
/* 163 */
/***/ function(module, exports) {

	module.exports = "<div class=\"fluid-container admin-master-page-wrapper\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"menu-collapse-btn\" \n\t\t\t\tng-click=\"vm.hideSidebar = !vm.hideSidebar\"\n\t\t\t\tstyle=\"cursor: pointer;color: white;position: fixed;top: 3px;left: 15px;z-index: 10002;font-size: 25px;\">\n\t\t\t\t<i class=\"fa\" ng-class=\"{'fa-times':!vm.hideSidebar,'fa-bars':vm.hideSidebar}\"></i>\n\t\t\t</div>\n\t\t\t<div class=\"col-md-2 admin-sidebar-menu\" ng-if=\"!vm.hideSidebar\">\n\t\t\t\t<ul class=\"list-unstyled list-inline\">\n\t\t\t\t\t<!-- <li><a ui-sref-active=\"active\" ui-sref=\"admin.courseBundle\">Course Bundle</a></li> -->\n\t\t\t\t\t<!-- <li><a ui-sref-active=\"active\" ui-sref=\"admin.userCourses\">User Courses</a></li> -->\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.allLinks\">All links</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.allLinksCategory\">All links Category</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.author\">Author</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.batchCourse\">Batch Course</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.batchTimings\">Batch Timings</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.batchTimingsDashboard\">Batch Timings Dashboard</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.blogSideLinksArticle\">Blog Side Link Article</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.blogSideLinksArticleTextboxIo\">Blog Article with textbox.io</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.boardCompetitiveCourses\">Board/Competitive Courses</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.book\">Book</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.bookTopic\">Book Topic</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.bundle\">Bundle</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.courselist\">Courselist</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.coursemaster\">CourseMaster</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.courseSubGroup\">Course SubGroup</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.currency\">Currency</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.demovideos\">Demo Videos</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.emailTemplates\">Emails Templates</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.empDetails\">Employee Details</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.empTypes\">Employee Types</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.exam\">Exam</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.forms\">Forms</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.forumsCategory\">Forums Category</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.gettingStarted\">Getting Started</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.helpForm\">Help Form</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.itemtransfer\">Item Transfer</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.jobOpenings\">Job Openings</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.moduleItems\">Module Items</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.modules\">Modules</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.practice\">Practice</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.pricelist\">Pricelist</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.publication\">Publication</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.purchasedCourses\">Purchased Courses</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.previousPapers\">Previous Paper</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.questionAnalytics\">Que Analytics</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.roles\">Roles</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.bulkSMS\">Send Bulk SMS</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.sendBulkMails\">Send Bulk Mails</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.sendEmailTemplate\">Send Email Template</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.sendNotification\">Send Notification</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.sessionDetails\">Sessions Details</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.sessions\">Sessions</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.skills\">Skills</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.slides\">Slides</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.subject\">Subject</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.subtopic\">SubTopic</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.tags\">Tags</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.topic\">Topic</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.topicGroup\">Topic Group</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.userRoles\">User Roles</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.userEmailGroup\">User Email Group</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.userBatchEnroll\">User Batch Enroll</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.videoEntity\">Video</a></li>\n\t\t\t\t\t<li><a ui-sref-active=\"active\" ui-sref=\"admin.verifyPracticeQuestion\">Verify Practice Question</a></li>\n\t\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"col-md-10 main-admin-child-content\">\n\t\t\t\t<div class=\"no-authorization-msg\" ng-if=\"!vm.isUserAuthorized\">\n\t\t\t\t\tAccess denied.\n\t\t\t\t</div>\n\t\t\t\t<div ng-if=\"vm.isUserAuthorized\">\n\t\t\t\t\t<a class=\"ew-rounded-btn\"  target=\"_blank\" ui-sref=\"admin.helpForm({form:vm.form_url})\" style=\"display: inline-block;position: fixed;top: 7px;left:250px;z-index: 10002;\">\n\t\t\t\t\t\t<i class=\"fa  fa-question-circle right\"></i>\n\t\t\t\t\t\t<span class=\"text\">Help</span>\n\t\t\t\t\t</a>\n\t\t\t\t\t<div ui-view></div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<menu-component menu-delegate=\"vm.menuDelegate\"></menu-component>\n</div>\n";

/***/ }
]));