webpackJsonp([2],{

/***/ 164:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {	angular.module(window.moduleName)
	        .component('courseMasterPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(165),
	            controller: AdminCourseMasterController,
	            controllerAs: 'vm'
	        })

		AdminCourseMasterController.$inject = ['$log','admin','$filter','$scope','$state','alertify','$timeout'];
		function AdminCourseMasterController($log,admin,$filter,$scope,$state,alertify,$timeout){

			var vm  = this;
			vm.no_of_item_already_fetch = 0;

			vm.form = {};
			vm.form.noOfauthors = 1;
			vm.form.courseSubGroup = "informatica";
			vm.form.courseType = "it"
			vm.selectedAuthors = [];
			vm.form.author = [];
			vm.submitted = false;
			vm.buttonName = "Save";
			vm.selectedRelatedCourses = [];
			vm.courseTypes = [{
				id : 'it',
				name : 'IT'
			},{
				id : 'board',
				name : 'Board'
			},{
				id : 'competitive',
				name : 'Competitive'
			}]
			
			vm.$onInit = function(){
				$scope.$watch(function(){return vm.parent.coursemaster;},function(v){
	        		vm.relatedCourses = v;
	        	});
	        	getAllSubgroup();
	        	// admin.getAllAuthor()
	        	// 	.then(function(d){
	        	// 		vm.coursemasterdata = d.data;
	        	// 	});
	        	$timeout(function(){
					angular.element('#form')[0].addEventListener('keypress', function(event) {
				        if (event.keyCode == 13) {
				            event.preventDefault();
				        }
				    });	
				})
			}

			vm.getNumber = getNumber;
			vm.saveCourseMaster=saveCourseMaster;
			vm.updateFormWithValues = updateFormWithValues;
			vm.deleteCourseMaster = deleteCourseMaster;
			vm.resetForm = resetForm;
			vm.checkIdExist = checkIdExist;

			function getNumber(num) {
			    return Array.apply(null,{length:num});   
			}

			function checkIdExist() {
				if(!vm.form.courseId){
					return;
				}
				return admin.isIdExist({ key : "courseId" , id_value : vm.form.courseId , "collection" : "coursemaster" })
					.then(function(res){
						if(res.data && res.data.status === 'success' && res.data.idExist){
							vm.form.idExistError = true;
							var alreadyExistCourseData = res.data.data
							var item = $filter('filter')(vm.coursemasterdata || [], {courseId: vm.form.courseId})[0];
							if(item){
								var pos = vm.coursemasterdata.indexOf(item);
								if(pos > -1){
									vm.coursemasterdata.splice(pos,1);
								}
							}
							vm.coursemasterdata = [alreadyExistCourseData].concat(vm.coursemasterdata || [])
						}else{
							vm.form.idExistError = false;
						}
					})
					.catch(function(err){
						console.error(err)
					})
			}

			function getAllSubgroup(){
				admin.getAllSubgroup()
					.then(function(res){
						vm.allSubgroup = res.data
					})
			}

			function deleteCourseMaster(id) {
				var result = confirm("Want to delete?");
				if (!result) {
				    //Declined
				   	return;
				}
				var courseMasterData= {
					courseId:id,
				};


				admin.deleteCourseMaster(courseMasterData)
					.then(function(res){
						// updateData();
						var item = $filter('filter')(vm.coursemasterdata || [], {courseId: id})[0];

						if(item){
							var pos = vm.coursemasterdata.indexOf(item);
							if(pos > -1){
								vm.coursemasterdata.splice(pos,1);
							}
						}
						alertify.success("Successfully Removed")
					})
					.catch(function(err){
						alertify.error("Error in removing")
					})

			}


			function saveCourseMaster() {
				var courseMasterData= {
					courseId:vm.form.courseId,
					author : vm.selectedAuthors,
					courseName:vm.form.courseName,
					courseType : vm.form.courseType,
					courseSubGroup : vm.form.courseSubGroup,
					relatedCourses : vm.selectedRelatedCourses,
					isLiveClass : vm.form.isLiveClass
				};
				vm.buttonName = "Uploading....";
				admin.addCourseMaster(courseMasterData)
					.then(function success(data){
						vm.buttonName = "Save";
						
						// updateData();
						var item = $filter('filter')(vm.coursemasterdata || [], {courseId: vm.form.courseId})[0];
						if(item){
							item.courseId = vm.form.courseId;
							item.author = vm.selectedAuthors;
							item.courseName = vm.form.courseName;
							item.courseType = vm.form.courseType;
							item.courseSubGroup = vm.form.courseSubGroup;
							item.relatedCourses = vm.selectedRelatedCourses;
							item.isLiveClass = vm.form.isLiveClass;
						}else{
							vm.coursemasterdata = [courseMasterData].concat(vm.coursemasterdata || [])
						}

						vm.form = {};
						vm.form.noOfauthors = 1;
						vm.form.author = [];
						vm.submitted = true;
						vm.form.error = false;
						vm.selectedAuthors = [];

						// vm.form.courseType = courseMasterData.courseType || ;
						vm.selectedRelatedCourses = [];
						
						$log.info(vm.coursemasterdata)

						// $state.reload();
						alertify.success("Successfully Saved")
					})
					.catch(function error(err){
						vm.buttonName = "Save";
						vm.form = {};
						vm.submitted = true;
						vm.form.error = true;
						vm.form.noOfauthors = 1;
						vm.form.author = [];
						alertify.error("Error in saving")
					});
			}

			function updateFormWithValues(id){
				var item = $filter('filter')(vm.coursemasterdata, {courseId: id})[0];
				if(item){

					$("#courseid").attr('readonly','true').attr('disabled','true');
					vm.form.courseId = item.courseId;
					vm.form.courseName = item.courseName;
					vm.form.courseType = item.courseType;
					vm.form.courseSubGroup = item.courseSubGroup;
					vm.form.isLiveClass = item.isLiveClass;
					// vm.form.noOfauthors = item.author.length;
					vm.selectedAuthors = item.author;
					vm.selectedRelatedCourses = item.relatedCourses;
					vm.form.idExistError = false;
				}
			}

			function  updateData(){
				admin.coursemaster()
					.then(function(data){
						vm.coursemasterdata = data;
					})
					.catch(function err(err){
						vm.form.error = true;
					})
			}
			function resetForm(){
				$("#courseid").removeAttr('readonly').removeAttr('disabled');
				vm.form = {};
				vm.selectedAuthors = [];
			}

			vm.fetchCourses = function(){
				vm.isCoursesLoading = true;
				admin.fetchCourseMasterUsingLimit({
					no_of_item_to_fetch : 5,
					no_of_item_to_skip : 0
				})
				.then(function(res){
					$log.debug(res)
					vm.coursemasterdata = res.data;
					vm.no_of_item_already_fetch = 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isCoursesLoading = false;
				})
			}

			vm.fetchMoreCourses = function(){
				vm.isCoursesLoading = true;
				admin.fetchCourseMasterUsingLimit({
					no_of_item_to_fetch : 5,
					no_of_item_to_skip : vm.no_of_item_already_fetch
				})
				.then(function(res){
					if(res.data.length > 0){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.coursemasterdata, {courseId: v.courseId})[0];
							if(!item){
								vm.coursemasterdata.push(v)		
								// vm.no_of_item_already_fetch++;
							}
						})
						vm.no_of_item_already_fetch = vm.coursemasterdata.length
						// vm.coursemasterdata = vm.coursemasterdata.concat(res.data)	
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isCoursesLoading = false;
				})
			}

		}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 165:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" id=\"form\" ng-submit=\"vm.saveCourseMaster()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add New Course</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"courseid\">Course Id*</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"courseid\" class=\"form-control\" ng-change=\"vm.form.idExistError = false;\" ng-blur=\"vm.checkIdExist()\" placeholder=\"Course Id\" ng-model=\"vm.form.courseId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.form.idExistError\">\n\t\t\t\t\t\t\tId is already exist\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"coursename\">Course Name*</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"coursename\" class=\"form-control\" placeholder=\"Course Name\" ng-model=\"vm.form.courseName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"coursetype\">Course Type*</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.courseType\" id=\"coursetype\" class=\"form-control\"\n\t\t\t\t\t\t\t\tng-change=\"vm.typeChange()\"\n\t\t\t\t\t\t\t\t\tng-options=\"coursetype.id as coursetype.name for coursetype in vm.courseTypes \" required=\"\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Type</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course-sub-group\">Course subgroup</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.courseSubGroup\" id=\"course-sub-group\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"subgroup.subgroupInUrl as subgroup.subgroupName for subgroup in vm.allSubgroup \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select SubGroup</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"liveclass\">Live Class</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"checkbox\" style=\"margin-top: 10px;\" id=\"liveclass\" class=\"\" ng-model=\"vm.form.isLiveClass\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"author\">Author</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedAuthors\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Author...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.authorId as item in (vm.authors | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"\">Related Courses</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedRelatedCourses\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Related Courses...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.courseId as item in (vm.relatedCourses | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\" >\n\t\t\t\t\t\t\t<!-- !vm.form.courseId || !vm.form.courseName || !vm.form.courseType ||  -->\n\t\t\t\t\t\t\t\t<button  type=\"submit\" class=\"form-control btn btn-primary submit\" \n\t\t\t\t\t\t\t\tng-disabled=\"vm.form.idExistError\">{{vm.buttonName}}</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<!-- <div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div> -->\n\t\t\t</div>\n\t\t</div>\n<!-- \n\t\t<div class=\"col-xs-12\">\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.alreadyExistCourseData\">\n\t\t\t\t<uib-accordion close-others=\"false\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteCourseMaster(vm.alreadyExistCourseData.courseId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.updateFormWithValues(vm.alreadyExistCourseData.courseId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{vm.alreadyExistCourseData.courseName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in vm.alreadyExistCourseData\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div> -->\n\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchCourses()\" ng-if=\"!vm.coursemasterdata || vm.coursemasterdata.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isCoursesLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isCoursesLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Courses</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t\n\t\t<div class=\"col-md-12\">\n\t\t\t<!-- <div class=\"ds-alert\" ng-if=\"!vm.coursemasterdata\">No Data to display</div> -->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.coursemasterdata\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"course in vm.coursemasterdata\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteCourseMaster(course.courseId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.updateFormWithValues(course.courseId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{course.courseName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in course\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreCourses()\" ng-if=\"(vm.coursemasterdata || vm.coursemasterdata.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isCoursesLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isCoursesLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\t\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});