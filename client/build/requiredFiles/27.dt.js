webpackJsonp([27],{

/***/ 384:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('empDetailsPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(385),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state'];
	function Controller(admin,$filter,$scope,$state){

		var vm  = this;

		vm.form = {};
		vm.form.experties = vm.form.experties || [];

		vm.saveEmployee = saveEmployee;
		vm.deleteEmployee = deleteEmployee;

		vm.$onInit = function(){
			getAllTypes();
			getAllEmployees();
			getAllSkills();
		}

		function getAllSkills(){
			admin.getAllEmployeeSkills()
				.then(function(res){
					vm.allSkills = res.data
				})
		}

		function getAllTypes(){
			admin.getAllEmployeeTypes()
				.then(function(res){
					vm.allEmpTypes = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function getAllEmployees(){
			admin.getAllEmployees()
				.then(function(res){
					vm.allEmps = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function saveEmployee(){
			admin.addEmployee(vm.form)
				.then(function(res){
					getAllEmployees();
					vm.form = {};
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function deleteEmployee(id){
			admin.deleteEmployee({emp_id : parseInt(id)})
				.then(function(res){
					getAllEmployees();
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function resetForm(){
			vm.form = {};
		}
	}

/***/ },

/***/ 385:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Employee Details</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"fst_nm\">Firstname</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"fst_nm\" class=\"form-control\" placeholder=\"Firstname\" ng-model=\"vm.form.fst_nm\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"lst_nm\">Lastname</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"lst_nm\" class=\"form-control\" placeholder=\"Lastname\" ng-model=\"vm.form.lst_nm\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"mdl_nm\">Middle name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"mdl_nm\" class=\"form-control\" placeholder=\"Middle name\" ng-model=\"vm.form.mdl_nm\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"mdl_nm\">Type</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.typ\" id=\"typ\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"typ.typ_id as typ.typ_desc for typ in vm.allEmpTypes\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Type</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"address\">Address</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea ng-model=\"vm.form.address\" class=\"form-control\" placeholder=\"Address\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"address\">Phone No</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Phone Number\" ng-model=\"vm.form.ph_no\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"address\">Experties</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.form.experties\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Skill...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.skl_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.skl_id as item in (vm.allSkills | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.skl_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.saveEmployee()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allEmps\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allEmps\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"emp in vm.allEmps\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteEmployee(emp.emp_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t{{emp.fst_nm}} {{emp.lst_nm}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in emp\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});