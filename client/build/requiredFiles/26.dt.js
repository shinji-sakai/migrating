webpackJsonp([26],{

/***/ 382:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('empTypesPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(383),
	        controller: EmployeeTypesController,
	        controllerAs: 'vm'
	    })

	EmployeeTypesController.$inject = ['admin','$filter','$scope','$state'];
	function EmployeeTypesController(admin,$filter,$scope,$state){

		var vm  = this;

		vm.form = {};

		vm.saveType = saveType;
		vm.deleteType = deleteType;

		vm.$onInit = function(){
			getAllTypes();
		}

		function getAllTypes(){
			admin.getAllEmployeeTypes()
				.then(function(res){
					resetForm();
					vm.allEmpTypes = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function saveType() {
			var obj = {
				typ_id : vm.form.typ_id,
				typ_desc : vm.form.typ_desc
			}
			admin.addEmployeeType(obj)
				.then(function(){
					getAllTypes();
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function resetForm(){
			vm.form = {};
		}

		function deleteType(typ_id){
			var result = confirm("Want to delete?");
			if (!result) {
			    //Declined
			   	return;
			}
			var obj = {
				typ_id : typ_id,
			}
			admin.deleteEmployeeType(obj)
				.then(function(){
					getAllTypes();
				})
				.catch(function(err){
					console.error(err);
				})
		}
	}

/***/ },

/***/ 383:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add New Employee Type</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"type\">Type</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"type\" class=\"form-control\" placeholder=\"Type\" ng-model=\"vm.form.typ_desc\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.saveType()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allEmpTypes\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allEmpTypes\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"type in vm.allEmpTypes\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteType(type.typ_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t{{type.typ_desc}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in type\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});