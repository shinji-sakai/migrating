webpackJsonp([20],{

/***/ 370:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('tagsPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(371),
	            controller: AdminTagController,
	            controllerAs: 'vm'
	        });

		AdminTagController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminTagController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;

	        vm.checkValidityOfID = checkValidityOfID;
	    	vm.updateTag = updateTag;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removeTag = removeTag;
	    	vm.editTag = editTag;
	        vm.no_of_item_already_fetch = 0;

	    	vm.$onInit = function() {
	        	//updateTagsList();
		    };

	        function checkValidityOfID() {
				debugger;
	            vm.form.tagId = vm.form.tagId || "";
	            vm.form.tagId = vm.form.tagId.replace(/\s/g , "-");
				admin.isIdExist({
					collection :'tag',
					key:"tagId",
					id_value:vm.form.tagId 			
				}).then(function(data)
				{				
	              console.log(data.data);
				  if(data.data.idExist)
				  {
					    vm.idExists = true;
				  }
				  else
				  {
					   vm.idExists = false;
				  }
				}).catch(function(err){
	             console.log(err);
				})
				
			
	             }

		    function updateTagsList(){
		    	pageLoader.show();
		    	admin.getAllTag()
	        		.then(function(data){
	        			if(data.data.length > 0)
	        				vm.tags = data.data;
	                    else
	                        vm.tags = [];
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }

	    	function updateTag(){
	    		var data = {
	    			tagId : vm.form.tagId,
	    			tagName : vm.form.tagName,
	    			tagDesc : vm.form.tagDesc 
	    		}
				if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }
	    		pageLoader.show();
	    		admin.updateTag(data)
	    			.then(function(res){
						// 	var item = $filter('filter')(vm.tags || [], {tagId: vm.form.tagId})[0];
						// if(item){
						// 		item.tagId = vm.form.tagId,
						// 		item.tagName = vm.form.tagName,
						// 		item.tagDesc = vm.form.tagDesc 
						// }else{
						// 	vm.tags = [data].concat(vm.tags || [])
						// }
						 vm.fetchTags();
	    				vm.submitted = true;
	    				vm.form.error = false;
	    				vm.form.tagName = "";
	    				vm.form.tagDesc = "";
	                    vm.form.tagId = "";
	                    vm.idExists = false;
	                    vm.formIsInEditMode = false;
	    				//updateTagsList();
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}

	    	function resetFormDetail(){
	    		vm.submitted = false;
	            vm.idExists = false;
	            vm.formIsInEditMode = false;
	    		vm.form = {};
	    	}

	    	function removeTag(id){
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	    		admin.removeTag({tagId:id})
	    			.then(function(d){
	    				console.log(d);
						var item = $filter('filter')(vm.tags || [], {tagId: id})[0];								
						 if (vm.tags.indexOf(item) > -1) {
	                        var pos = vm.tags.indexOf(item);
	                       vm.tags.splice(pos, 1);
	                    }
	    				//updateTagsList();
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}

	    	function editTag(id){
	    		var obj = $filter('filter')(vm.tags,{tagId:id})[0];
	    		vm.form = {};
	    		vm.form.tagId = obj.tagId;
	    		vm.form.tagName = obj.tagName;
	    		vm.form.tagDesc = obj.tagDesc;
	            vm.formIsInEditMode = true;
	    	}

			vm.fetchTags = function(){
				vm.isTagsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					//no_of_item_to_skip : 0
					collection : 'tag'
				})
				.then(function(res){
					$log.debug(res)
					vm.tags = res.data;
					vm.no_of_item_already_fetch = 5;
					   vm.last_update_dt = vm.tags[vm.tags.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTagsLoading = false;
				})
			}

			vm.fetchMoreTags = function(){
				vm.isTagsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
				//	no_of_item_to_skip : vm.no_of_item_already_fetch
					collection : 'tag',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.tags,{tagId: v.tagId})[0];
							if(!item){
								vm.tags.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						  vm.last_update_dt = vm.tags[vm.tags.length - 1].update_dt;
						// vm.coursemasterdata = vm.coursemasterdata.concat(res.data)	
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTagsLoading = false;
				})
			}
	    }

/***/ },

/***/ 371:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateTag()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Tag</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tagId\">Tag Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"tagId\" class=\"form-control\" placeholder=\"Id\" ng-change=\"vm.checkValidityOfID()\" ng-disabled=\"vm.formIsInEditMode\" ng-model=\"vm.form.tagId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.idExists\">\n\t\t\t\t\t\t\t\tTag Id exists\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tagname\">Tag Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"tagname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.tagName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tagdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"tagdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.tagDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Tag</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" ng-disabled=\"vm.idExists\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchTags()\" ng-if=\"!vm.tags || vm.tags.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTagsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTagsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Tags</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.tags\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.tags\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"tag in vm.tags\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeTag(tag.tagId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editTag(tag.tagId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{tag.tagName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in tag\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreTags()\" ng-if=\"(vm.tags==[] || vm.tags.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTagsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTagsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\t\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});