webpackJsonp([3],{

/***/ 166:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {angular.module(window.moduleName)
	    .component('courselistPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(167),
	        controller: AdminCourseListController,
	        controllerAs: 'vm'
	    })
	AdminCourseListController.$inject = ['admin', '$filter','$scope','EditorConfig'];
	function AdminCourseListController(admin, $filter,$scope,EditorConfig) {

	    var vm = this;

	    vm.$onInit = function() {
	        //observe parent(admin master page) coursemaster
	    	$scope.$watch(function(){return vm.parent.coursemaster;},function(v){
	    		vm.courselist = v;	
	    		// console.log(v);
	    	});
	        //observe parent(admin master page) courselist data
	    	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
	    		vm.courselistdata = v;
	    	});
	    };

	    vm.courseLevel = "_";
	    vm.buttonName = "Save";
	    vm.submitted = false;


	    vm.updateFormWithValues = updateFormWithValues;
	    vm.removeCourse = removeCourse;
	    //initialize froala editor in short description
	    $('#shortdesc').froalaEditor(EditorConfig);
	    //initialize froala editor in long description
	    $('#longdesc').froalaEditor(EditorConfig);

	    //this method will be called when user click add/update button
	    vm.addCourse = function() {

	        vm.buttonName = "Uploading ...";
	        var courseListData = {
	            courseId: vm.courseId,
	            author: vm.author,
	            courseName: vm.courseName,
	            courseLongDesc: $('#longdesc').froalaEditor('html.get',true),
	            courseShortDesc: $('#shortdesc').froalaEditor('html.get',true),
	            courseLevel: vm.courseLevel,
	            courseLevelDesc: vm.courseLevelDesc,
	            courseType: vm.courseType,
	            imageUrl: vm.imageUrl,
	            courseSubGroup : vm.courseSubGroup
	        };

	        //add course to database
	        admin.addCourse(courseListData)
	            .then(function success() {
	                vm.submitted = true;
	                vm.error = false;
	                vm.buttonName = "Save";

	                vm.couse = "";
	                vm.courseName = "";
	                vm.author = "";
	                vm.courseLevel = "";
	                vm.courseLevelDesc = "";
	                vm.courseLongDesc = "";
	                vm.courseShortDesc = "";
	                vm.courseType = '';
	                vm.imageUrl = "";
	                vm.comments = "";
	                // vm.lastUpdated = "";
	                $('#shortdesc').froalaEditor('html.set','');
	                $('#longdesc').froalaEditor('html.set','');
	                updateData();
	            })
	            .catch(function err(err) {
	                vm.submitted = true;
	                vm.error = true;
	                vm.buttonName = "Save";
	            });
	    }

	    vm.changeCourse = function() {
	        vm.updateFormWithValues(vm.course);
	    }

	    function updateFormWithValues(id) {

	        vm.courseId = id;
	        vm.course = id;
	        var item = $filter('filter')(vm.courselistdata, {
	            courseId: vm.course
	        })[0];
	        var coursemaster = $filter('filter')(vm.courselist, {
	            courseId: vm.course
	        })[0];



	        if (item) {
	            vm.courseType = coursemaster.courseType;
	            vm.courseSubGroup = coursemaster.courseSubGroup;
	            vm.courseName = coursemaster.courseName;
	            vm.author = coursemaster.author;
	            vm.courseLevel = item.courseLevel;
	            vm.courseLevelDesc = item.courseLevelDesc;
	            $('#shortdesc').froalaEditor('html.set', item.courseShortDesc);
	            $('#longdesc').froalaEditor('html.set', item.courseLongDesc);
	            vm.imageUrl = item.imageUrl;
	            // vm.comments = item["CommentsReviews"];
	            // vm.lastUpdated = new Date(item["LastUpdated"]);
	            vm.buttonName = "Update";
	        } else {
	            var item = $filter('filter')(vm.courselist, {
	                courseId: vm.course
	            })[0];
	            if (item) {
	                vm.courseName = coursemaster.courseName;
	                vm.courseType = coursemaster.courseType;
	                vm.author = item.author;
	            } else {
	                vm.courseName = "";
	                vm.author = "";
	            }
	            vm.courseSubGroup = coursemaster.courseSubGroup;
	            vm.courseLevel = "";
	            vm.courseLevelDesc = "";
	            vm.courseLongDesc = "";
	            vm.courseShortDesc = "";
	            vm.imageUrl = "";
	            vm.comments = "";
	            // vm.lastUpdated = new Date();
	            vm.buttonName = "Save";
	        }

	    }

	    function removeCourse(id) {
	        var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	        var deleteCourseData = {
	            courseId: id,
	        };


	        admin.removeCourse(deleteCourseData)
	            .then(function(res) {
	                updateData();
	            });

	    }

	    function updateData() {
	        admin.courselistfull()
	            .then(function(data) {
	                vm.courselistdata = data;
	            })
	            .catch(function err(err) {

	            });
	    }


	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 167:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<fieldset>\n\t\t\t\t\t<legend>Add Course</legend>\n\t\t\t\t\t<form class=\"form form-horizontal\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course\">Course</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.course\" ng-change=\"vm.changeCourse()\" id=\"course\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"course.courseId as course.courseName for course in vm.courselist \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Course</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"author\">Author</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"author\" class=\"form-control\" readonly placeholder=\"Author\" ng-model=\"vm.author\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"courseid\">Course Id</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"courseid\" class=\"form-control\" readonly placeholder=\"Course Id\" ng-model=\"vm.courseId\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"coursename\">Course Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"coursename\" class=\"form-control\" readonly placeholder=\"Course Name\" ng-model=\"vm.courseName\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"shortdesc\">Short Desc</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"shortdesc\" class=\"form-control large\" spellcheck=\"true\" placeholder=\"Short Description\" ng-model=\"vm.courseShortDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"longdesc\">Long Desc</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"longdesc\" class=\"form-control large\" spellcheck=\"true\" placeholder=\"Long Description\" ng-model=\"vm.courseLongDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"level\">Level</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select id=\"level\" class=\"form-control\" ng-model=\"vm.courseLevel\">\n\t\t\t\t\t\t\t\t\t<option value=\"_\">Select Level</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Beginner\">Beginner</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Intermediate\">Intermediate</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Advanced\">Advanced</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"leveldesc\">Level Desc</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"leveldesc\" class=\"form-control\" spellcheck=\"true\" placeholder=\"Level Description\" ng-model=\"vm.courseLevelDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"thumbnail\">Thumbnail URL</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"thumbnail\" class=\"form-control\" placeholder=\"Thumbnail URL\" ng-model=\"vm.imageUrl\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- <div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"comments\">Comments / Reviews</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"comments\" class=\"form-control\" spellcheck=\"true\" placeholder=\"Comments / Reviews\" ng-model=\"vm.comments\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"duration col-xs-2 control-label\" for=\"lastupdated\">Last Updated</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"date\" class=\"form-control\" placeholder=\"Select Last Updated Date\" ng-model=\"vm.lastUpdated\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-10 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.addCourse()\">{{vm.buttonName}}</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t</form>\n\t\t\t\t</fieldset>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.courselistdata\">No course to Disaplay.</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.courselistdata\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"course in vm.courselistdata\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeCourse(course.courseId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.updateFormWithValues(course.courseId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\n\t\t\t\t\t{{course.courseName}}\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in course\">\n\t\t\t\t\t<div class=\"col-sm-2 title\">{{key}}</div>\n\t\t\t\t\t<div class=\"col-sm-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});