webpackJsonp([29],{

/***/ 390:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(391);
	angular.module(window.moduleName)
	    .component('batchTimingsPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(393),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state'];
	function Controller(admin,$filter,$scope,$state){

		var vm = this;
		vm.form = vm.form || {};
		vm.form.faculty_id = vm.form.faculty_id || [];
		vm.nowDate = (new Date());
	    vm.yesterday = new Date(vm.nowDate);
	    vm.yesterday.setDate(vm.nowDate.getDate() - 1);
	    vm.yesterday = vm.yesterday.toString();

		vm.daysOfWeek = ["Monday",'Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']


		vm.addPriceToList = addPriceToList;
		vm.removePriceFromList = removePriceFromList;
		vm.saveTiming  = saveTiming;
		vm.deleteTiming  =deleteTiming;
		vm.editTiming = editTiming;
		vm.onTrainingChange = onTrainingChange;

		vm.$onInit = function(){
			getAllEmployees();
			getAllTiming();
			getAllCurrency();
			getAllCoursesFromMaster();
		}

		function getAllCoursesFromMaster() {
			admin.coursemaster()
				.then(function(d){
					vm.coursesMaster = d;
				})
				.catch(function(err){
					console.error(err);
				});
		}

		function onTrainingChange() {
			vm.form = vm.form || {};
			vm.form.bat_id = Math.floor(Math.random()*90000);
			vm.form.bat_nm = vm.form.crs_id + "-" + vm.form.bat_id;
		}

		function getAllCurrency(){
			admin.getAllCurrency()
				.then(function(res){
					vm.allCurrency = res.data
				})
		}

		function getAllEmployees(){
			admin.getAllEmployees()
				.then(function(res){
					vm.allEmps = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function getAllTiming(){
			admin.getAllBatchTiming()
				.then(function(res){
					vm.allTimings = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function saveTiming(){
			// vm.form.cls_start_dt = vm.bat_mn + "-" + vm.bat_dt + "-" + vm.bat_yr;
			vm.form.cls_frm_tm = vm.bat_frm_tm_hr + ":" + vm.bat_frm_tm_min;
			vm.form.cls_to_tm = vm.bat_to_tm_hr + ":" + vm.bat_to_tm_min;

			admin.addBatchTiming(vm.form)
				.then(function(res){
					getAllTiming();
					vm.form={};
					vm.bat_mn = "";
					vm.bat_dt = "";
					vm.bat_yr = "";
					vm.bat_frm_tm_hr = "";
					vm.bat_frm_tm_min = "";
					vm.bat_to_tm_hr = "";
					vm.bat_to_tm_min = "";
				})
				.catch(function(err){
					console.error(err);
				})
		}


		function addPriceToList(){
			vm.form = vm.form || {};
			vm.form.bat_crs_price = vm.form.bat_crs_price || {};
			if(vm.bat_price_curr && vm.bat_price_amount){
				vm.form.bat_crs_price[vm.bat_price_curr] = vm.bat_price_amount;
				vm.bat_price_curr = "";
				vm.bat_price_amount = "";
			}
		}

		function removePriceFromList(key){
			delete vm.form.bat_crs_price[key];
		}


		function deleteTiming(id){
			var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
			admin.deleteBatchTiming({bat_id : id})
				.then(function(res){
					getAllTiming();
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function editTiming(id) {
			var batch = $filter('filter')(vm.allTimings,{bat_id : id})[0];

			var tempDate = new Date(batch.cls_start_dt);
			vm.bat_mn = "" + (tempDate.getMonth() + 1);
			vm.bat_dt = "" + (tempDate.getDate());
			vm.bat_yr = "" + (tempDate.getFullYear());
			batch.cls_start_dt = vm.bat_mn + "-" + vm.bat_dt + "-" +vm.bat_yr;

			var tempArr = batch.cls_frm_tm.split(":");
			vm.bat_frm_tm_hr = tempArr[0] || 0;
			vm.bat_frm_tm_min = tempArr[1] || 0;

			tempArr = batch.cls_to_tm.split(":");
			vm.bat_to_tm_hr = tempArr[0] || 0;
			vm.bat_to_tm_min = tempArr[1] || 0;

			vm.form = batch;

			vm.form.bat_nm = vm.form.bat_nm || (batch.crs_id + "-" + batch.bat_id)
		}
	}

/***/ },

/***/ 391:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(392);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_batchTiming.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_batchTiming.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 392:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, ".question-title {\n  font-family: aller;\n  font-size: 17px;\n  font-weight: bold; }\n\n.question-ans {\n  font-family: aller;\n  font-size: 15px; }\n\n.gray-back {\n  background: #e5e5e5;\n  padding: 0px 10px; }\n\n.remove-btn {\n  position: absolute;\n  right: 5px;\n  top: 5px;\n  font-size: 15px;\n  cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 393:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Batch Timing</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Training Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.crs_id\" id=\"course\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"course.courseId as course.courseName for course in vm.coursesMaster \" ng-change=\"vm.onTrainingChange()\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Training</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"bat_nm\">Batch Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Batch name\" ng-model=\"vm.form.bat_nm\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Class Start Date</label>\n\t\t\t\t\t\t\t<datepicker date-format=\"MM-dd-yyyy\" class=\"col-xs-10\" date-set=\"{{vm.nowDate.toString()}}\" date-min-limit=\"{{vm.yesterday}}\">\n\t\t\t\t\t\t\t\t<input class=\"form-control\" ng-model=\"vm.form.cls_start_dt\" placeholder=\"Class Start Date\">\n\t\t\t\t\t\t\t</datepicker>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"Class End Date\">Class End Date</label>\n\t\t\t\t\t\t\t<datepicker date-format=\"MM-dd-yyyy\" class=\"col-xs-10\" date-min-limit=\"{{vm.nowDate.toString()}}\">\n\t\t\t\t\t\t\t\t<input class=\"form-control\" ng-model=\"vm.form.cls_end_dt\" placeholder=\"Class End Date\">\n\t\t\t\t\t\t\t</datepicker>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"about\">Class Time</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.bat_frm_tm_hr\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">From Time Hour</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=24) && _) track by $index\" value=\"{{$index}}\">{{$index}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.bat_frm_tm_min\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">From Time Minutes</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=60) && _) track by $index\" value=\"{{$index}}\">{{$index}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.bat_to_tm_hr\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">To Time Hours</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=24) && _) track by $index\" value=\"{{$index}}\">{{$index}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.bat_to_tm_min\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">To Time Minutes</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=60) && _) track by $index\" value=\"{{$index}}\">{{$index}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Class Days</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.frm_wk_dy\" id=\"course\" class=\"form-control\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Start Week Day</b> </option>\n\t\t\t\t\t\t\t\t\t<option value=\"{{day}}\" ng-repeat=\"day in vm.daysOfWeek\"> {{day}} </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.to_wk_dy\" id=\"course\" class=\"form-control\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select End Week Day</b> </option>\n\t\t\t\t\t\t\t\t\t<option value=\"{{day}}\" ng-repeat=\"day in vm.daysOfWeek\"> {{day}} </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Class Duration</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.no_wk_dy\" class=\"form-control\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Duration</b> </option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=50) && _) track by $index\" value=\"{{$index + 1}}\"> {{$index + 1}} </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.wk_dy\" class=\"form-control\">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Unit</b> </option>\n\t\t\t\t\t\t\t\t\t<option value=\"weeks\"> weeks </option>\n\t\t\t\t\t\t\t\t\t<option value=\"days\"> days </option>\n\t\t\t\t\t\t\t\t\t<option value=\"months\"> months </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"Discount\">Discount Reason</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Discount name\" ng-model=\"vm.form.discount_rsn\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"Discount\">Discount Percentage</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Discount Percentage\" ng-model=\"vm.form.discount\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\">Discount Duration</label>\n\t\t\t\t\t\t\t<datepicker date-format=\"MM-dd-yyyy\" class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<input class=\"form-control\" ng-model=\"vm.form.discount_frm_dt\" placeholder=\"Discount Start Date\">\n\t\t\t\t\t\t\t</datepicker>\n\t\t\t\t\t\t\t<datepicker date-format=\"MM-dd-yyyy\" class=\"col-xs-5\" date-min-limit=\"{{vm.form.discount_frm_dt}}\">\n\t\t\t\t\t\t\t\t<input class=\"form-control\" ng-model=\"vm.form.discount_to_dt\" placeholder=\"Discount End Date\">\n\t\t\t\t\t\t\t</datepicker>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<!-- <div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" >Last class Video</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Last Class Video\" ng-model=\"vm.form.last_class_video\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" >Faculty</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.form.faculty_id\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Faculty...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.fst_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.emp_id as item in (vm.allEmps | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.fst_nm\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- <div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"about\">Price</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.bat_price_curr\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Currency</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"curr in vm.allCurrency track by $index\" value=\"{{curr.currency_nm}}\">{{curr.currency_nm}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Amount\" name=\"amount\" ng-model=\"vm.bat_price_amount\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t\t<span class=\"btn btn-primary\" ng-click=\"vm.addPriceToList()\">Add Price</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t<!-- <div class=\"form-group\" ng-repeat=\"(price,value) in vm.form.bat_crs_price track by $index\">\n\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"about\">Price {{price}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<span class=\"question-title\">{{value}}</span>\n\t\t\t\t\t\t\t\t<div class=\"remove-btn\" ng-click=\"vm.removePriceFromList(price)\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" >Msg</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea type=\"text\" class=\"form-control\" placeholder=\"Msg\" ng-model=\"vm.form.msg\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.saveTiming()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allTimings\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allTimings\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"timing in vm.allTimings\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editTiming(timing.bat_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteTiming(timing.bat_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t{{timing.crs_id}} - {{timing.cls_start_dt | date : 'dd-MMM-yyyy'}} - {{(timing.discount || 0) + '% discount'}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in timing\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});