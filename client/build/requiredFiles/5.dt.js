webpackJsonp([5],{

/***/ 170:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {	__webpack_require__(171);
		angular.module(window.moduleName)
		    .component('moduleItemsPage', {
		        require: {
		            parent: '^adminPage'
		        },
		        template: __webpack_require__(172),
		        controller: Controller,
		        controllerAs: 'vm'
		    });

		Controller.$inject = ['admin', 'pageLoader', '$filter', '$scope','EditorConfig','awsService'];

		function Controller(admin, pageLoader, $filter, $scope,EditorConfig,awsService) {

		    var vm = this;

		    vm.videolist = Array.apply(null, {
		        length: 10
		    }).map(Number.call, Number);
		    vm.hrlist = Array.apply(null, {
		        length: 24
		    }).map(Number.call, Number);
		    vm.minlist = Array.apply(null, {
		        length: 60
		    }).map(Number.call, Number);
		    vm.seclist = Array.apply(null, {
		        length: 60
		    }).map(Number.call, Number);

		    vm.selectedSubjects = [];
		    vm.selectedAuthors = [];
		    vm.selectedPublications = [];
		    vm.selectedBooks = [];
		    vm.selectedExams = [];
		    vm.selectedTopics = [];
		    vm.selectedTags = [];

		    vm.form = {};
		    vm.videosOfModule = [];
		    vm.itemsOfModule = [];
		    vm.shouldShowVideoForm = false;

		    vm.filteredQuestionsPageNumber = 1;
		    vm.filteredQuestionsPageCount = 1;
		    vm.selectedQuestionsPageNumber = 1;
		    vm.selectedQuestionsPageCount = 1;
		    vm.questionsInOnePage = 3;




		    vm.$onInit = function() {
		        $scope.$watch(function() {
		            return vm.parent.courselistdata;
		        }, function(v) {
		        	vm.courselist = vm.courselist || [];
		        	if(v){
		        		vm.courselist = vm.courselist.concat(v);
		        		getAllTiming();
		        	}
		        });
		        
		        admin.getAllSubject()
		            .then(function(d) {
		                vm.subjects = d.data;
		            });
		        admin.getAllAuthor()
		            .then(function(d) {
		                vm.authors = d.data;
		            });
		        admin.getAllPublication()
		            .then(function(d) {
		                vm.publications = d.data;
		            });
		        admin.getAllBook()
		            .then(function(d) {
		                vm.books = d.data;
		            });
		        admin.getAllExam()
		            .then(function(d) {
		                vm.exams = d.data;
		            });
		        admin.getAllTopic()
		            .then(function(d) {
		                vm.topics = d.data;
		            });
		        admin.getAllTag()
		            .then(function(d) {
		                vm.tags = d.data;
		            });
		        admin.getAllSideLinksArticlePage()
		        	.then(function(d){
		        		vm.allSideLinksArticle = d.data;
		        	})

		         //Observer files
				$scope.$watch('vm.materialFile',function(newValue,oldValue){
					if(newValue){
						console.log(newValue);
						vm.materialFileName = newValue.name;	
						vm.isMaterialUploadInProgress = false;
						vm.isMaterialUploaded = false;
					}
					
				})
		    };

		    vm.getNumber = getNumber;
		    vm.changeCourse = changeCourse;
		    vm.onChangeIdField = onChangeIdField;
		    vm.addItems = addItems;
		    vm.getVideosOfModule = getVideosOfModule;
		    vm.getAllModuleItems = getAllModuleItems;
		    vm.changeModule = changeModule;
		    vm.deleteItemOfModule = deleteItemOfModule;
		    vm.editItemOfModule = editItemOfModule;
		    vm.resetDetails = resetDetails;

		    vm.showVideoForm = showVideoForm;
		    vm.showQuestionGroupForm = showQuestionGroupForm;
		    vm.showDownloadForm  = showDownloadForm;
		    vm.showBookTopicForm = showBookTopicForm;
		    vm.showUploadForm = showUploadForm;

		    vm.onFilterChange = onFilterChange;
		    vm.addToSelectedQuestions = addToSelectedQuestions;
		    vm.removeFromSelectedQuestions = removeFromSelectedQuestions;
		    vm.isQuestionSelected = isQuestionSelected;

		    vm.addToSelectedVideo = addToSelectedVideo;
		    vm.isVideoSelected = isVideoSelected;

		    vm.fecthFilteredQuestions = fecthFilteredQuestions;
		    vm.filteredQuestionPrev = filteredQuestionPrev;
		    vm.filteredQuestionNext = filteredQuestionNext;

		    vm.fecthSelectedQuestions = fecthSelectedQuestions;
		    vm.selectedQuestionPrev = selectedQuestionPrev;
		    vm.selectedQuestionNext = selectedQuestionNext;

		    vm.uploadMaterialFile = uploadMaterialFile;
		    vm.uploadDownloadMaterialFile = uploadDownloadMaterialFile;

		    function getNumber(num) {
		        return Array.apply(null, {
		            length: num
		        });
		    }

		    function getAllTiming(){
				admin.getAllBatchTiming()
					.then(function(res){
						vm.allTimings = res.data;
						var batches = vm.allTimings.map(function(v,i) {
							var nm = v.bat_nm || (v.crs_id + "-" + v.bat_id);

							var parentTraining = $filter('filter')(vm.courselist, {
					            courseId: v.crs_id
					        })[0];

							return {
								courseId : nm,
								courseName : v.crs_id + "-" + $filter("date")(v.cls_start_dt,"dd-MMM-yyyy"),
								isFromBatch : true,
								displayCourseName : parentTraining.courseName,
								training_id : v.crs_id
							}
						})
						vm.courselist = vm.courselist || [];
						if(batches){
							vm.courselist = vm.courselist.concat(batches) 	
						}
						
					})
					.catch(function(err){
						console.error(err);
					})
			}


		    function changeModule() {
		        //Clear All Videos
		        vm.itemsOfModule = [];
		        var formData = vm.form;

		        //Clear Form Data and Set Necessary Items
		        vm.form = {};
		        vm.form.item = {};
		        vm.form.course = formData.course;
		        vm.form.module = formData.module;
		        var moduleItem = $filter('filter')(vm.modulelist, {
		            moduleNumber: vm.form.module
		        })[0];
		        pageLoader.show();
		        vm.getAllModuleItems(moduleItem.moduleId)
		            .then(function(res) {
		                if (res.data.length > 0) {

		                    var modules = res.data[0].moduleDetail;
		                    var m = $filter('filter')(modules, {
		                        moduleId: moduleItem.moduleId
		                    })[0];
		                    vm.itemsOfModule = m.moduleItems;
		                    vm.itemsOfModule.sort(compare);

		                    vm.itemNumber = vm.itemsOfModule[vm.itemsOfModule.length - 1].itemNumber + 1;
		                    vm.form.item.itemWeight = vm.itemNumber;
		                    console.log(vm.form.item.itemWeight);
		                    console.log(modules);
		                } else {
		                    vm.itemsOfModule = [];
		                    vm.itemNumber = 1;
		                    vm.form.item.itemWeight = vm.itemNumber;
		                }
		            })
		            .catch(function(err) {
		                console.error(err);
		            })
		            .finally(function() {
		                pageLoader.hide();
		            });
		    }

		    function changeCourse() {
		        vm.modulelist = [];
		        pageLoader.show();
		        admin.getCourseModules({
		            'courseId': vm.form.course
		        })
		            .then(function(res) {
		                console.log(res);
		                vm.modulelist = res;
		            })
		            .finally(function() {
		                pageLoader.hide();
		            })
		    }

		    function onChangeIdField() {
		    	var arr = vm.itemsOfModule.filter(function(v,i) {
					if(v.itemId === vm.form.item.itemId){
						return true;
					}
					return false;
				})
				if(arr.length > 0){
					vm.idExistError = true;
				}else{
					vm.idExistError = false;
				}
		    }

		    function addItems() {

		        var courseItem = $filter('filter')(vm.courselist, {
		            courseId: vm.form.course
		        })[0];
		        var moduleItem = $filter('filter')(vm.modulelist, {
		            moduleNumber: vm.form.module
		        })[0];

		        vm.form.item.itemNumber = vm.itemNumber;
		        vm.form.item.itemDescription = $('#item-desc').froalaEditor('html.get',true);
		        if (vm.shouldShowQuestionGroupForm) {
		            vm.form.item.itemType = "qGroup";
		            vm.form.item.itemQuestions = vm.selectedQuestions.map(function(v) {
		                return v.questionId
		            });
		            vm.form.item.itemTotalQuestions = (vm.form.item.itemQuestions || []).length

		            var totalTimeOfQuesGrp = 0;
		            vm.selectedQuestions.map(function(v) {
		                totalTimeOfQuesGrp += v.questionTime;
		            });
		            vm.form.item.itemQuestionsTime = totalTimeOfQuesGrp;
		        } else if (vm.shouldShowVideoForm) {
		            vm.form.item.itemType = "video";
		            vm.form.item.itemVideo = vm.selectedVideo;
		        } else if (vm.shouldShowDownloadForm) {
		            vm.form.item.itemType = "download";
		        } else if (vm.shouldShowUploadForm) {
		            vm.form.item.itemType = "upload";
		        } else if (vm.shouldShowBookTopicForm) {
		            vm.form.item.itemType = "book-topic";
		            vm.form.item.itemBookTopic = vm.selectedBookTopic;
		        }

		        //Add Item Id
		        // vm.form.item.itemId = moduleItem.moduleId + "_" + vm.form.item.itemNumber;

		        vm.itemsOfModule = vm.itemsOfModule.filter(function(obj) {
		            if (obj.itemId == vm.form.item.itemId) {
		                //Dont return if it is same video 
		                //Because we are going to add that video in next few lines
		                return false;
		            }
		            return true;
		        });
		        // console.log("........",vm.form.item);
		        vm.itemsOfModule.push(vm.form.item);
		        vm.itemsOfModule.sort(compare);
		        var data = {
		            courseId: vm.form.course,
		            courseName: !courseItem.isFromBatch ? courseItem.courseName : courseItem.displayCourseName,
		            LastUpdated: courseItem.LastUpdated,
		            courseLongDesc: courseItem.courseLongDesc,
		            courseShortDesc: courseItem.courseShortDesc,
		            courseLevel: courseItem.courseLevel,
		            isBatch : courseItem.isFromBatch,
		            training_id : courseItem.isFromBatch ? courseItem.training_id : undefined,
		            moduleDetail: {
		                moduleNumber: vm.form.module,
		                moduleId: moduleItem.moduleId,
		                moduleName: moduleItem.moduleName,
		                moduleDesc: moduleItem.moduleDesc,
		                moduleItems: vm.itemsOfModule,
		                moduleWeight: parseFloat(moduleItem.moduleWeight)
		            },
		            author: courseItem.author
		        }

		        var authorizeItemObject = {
		        	courseId: vm.form.course,
		            courseName: courseItem.courseName,
		            moduleId: moduleItem.moduleId,
		            moduleName: moduleItem.moduleName,
		            itemId : vm.form.item.itemId,
		            itemType : 'paid',
		            itemVideo : vm.form.item.itemVideo,
		            itemName : vm.form.item.itemName
		        }
		        admin.addAuthorizedItem(authorizeItemObject);



		        vm.form = {};
		        vm.form.course = data.courseId;
		        vm.form.module = data.moduleDetail.moduleNumber;
		        vm.form.item = {};
		        // vm.form.item.itemNumber = vm.itemsOfModule.length+1;
		        vm.shouldShowVideoForm = false;
		        vm.shouldShowQuestionGroupForm = false;
		        vm.shouldShowBookTopicForm = false;
		        vm.shouldShowDownloadForm = false;
		        vm.shouldShowUploadForm = false;
		        vm.filteredQuestions = [];
		        vm.selectedQuestions = [];

		        pageLoader.show();
		        admin.updateModuleItems(data)
		            .then(function() {
		                vm.submitted = true;
		                vm.form.error = false;
		                changeModule();
		            })
		            .catch(function() {
		                vm.submitted = true;
		                vm.form.error = false;
		            })
		            .finally(function() {
		                pageLoader.hide()
		            });


		        console.log(data);
		    }

		    function getVideosOfModule(moduleId) {
		        //Return Admin Service
		        return admin.getModuleVideos({
		            moduleId: moduleId
		        });
		    }

		    function getAllModuleItems(moduleId) {
		        //Return Admin Service
		        return admin.getAllModuleItems({
		            moduleId: moduleId
		        });
		    }


		    function editItemOfModule(itemId) {

		        var item = $filter('filter')(vm.itemsOfModule, {
		            itemId: itemId
		        })[0];
		        // console.log(item);
		        vm.form = vm.form || {};
		        vm.form.item = vm.form.item || {};
		        vm.resetDetails();
		        vm.form.item.itemId = item.itemId;
		        vm.form.item.itemName = item.itemName;
		        vm.form.item.materialInfo = item.materialInfo;
		        vm.form.item.itemWeight = item.itemWeight;
		        vm.form.item.itemDescription = item.itemDescription;
		        vm.form.item.itemTakeTest = item.itemTakeTest;
		        vm.form.item.itemOptionType = item.itemOptionType;
		        vm.form.item.slidePageId = item.slidePageId ? [].concat(item.slidePageId) : [];
		        vm.idExistError = false;
		        vm.itemNumber = item.itemNumber;
		        if (item.itemType === "qGroup") {
		            vm.shouldShowQuestionGroupForm = true;
		            vm.form.item.itemQuestionsTime = item.itemQuestionsTime;
		            // vm.form.item.itemName = item.itemName;
		            // vm.form.item.itemWeight = item.itemWeight;
		            // vm.itemNumber =item.itemNumber;
		            admin.getPracticeQuestionsById({
		                questionsId: item.itemQuestions
		            })
		                .then(function(res) {
		                    console.log(res);
		                    vm.selectedQuestions = res.data;
		                })
		                .catch(function(err) {
		                    console.error(err);
		                });
		        } else if (item.itemType === "video") {
		            vm.shouldShowVideoForm = true;
		            vm.form.item.itemContainsSlide = item.itemContainsSlide;
		            // vm.itemNumber =item.itemNumber;
		            // // vm.form.item.itemName = item.itemName;
		            // vm.form.item.itemWeight = item.itemWeight;
		            admin.findVideoEntityById({
		                videoId: item.itemVideo
		            })
		                .then(function(res) {
		                    console.log(res);
		                    vm.filteredVideos = res.data;
		                    if(res.data && res.data.length > 0){
		                    	vm.selectedVideo = res.data[0].videoId;	
		                    }
		                    
		                })
		                .catch(function(err) {
		                    console.log(err);
		                })
		        } else if (item.itemType === "download") {
		        	vm.shouldShowDownloadForm = true;
		        	vm.materialFileName = item.downloadLink || "";
		        } else if (item.itemType === "upload") {
		        	vm.shouldShowUploadForm = true;
		        } else if (item.itemType === "book-topic") {
		        	vm.shouldShowBookTopicForm = true;
		        	admin.findBookTopicsByQuery({
		                bookTopicId: [item.itemBookTopic]
		            })
		                .then(function(res) {
		                    console.log(res);
		                    vm.filteredBookTopics = res.data;
		                    if(res.data && res.data.length > 0){
		                    	vm.selectedBookTopic = res.data[0].bookTopicId;	
		                    }
		                })
		                .catch(function(err) {
		                    console.log(err);
		                })
		        }

		        setTimeout(function(){
		        	$('#item-desc').froalaEditor(EditorConfig);

		        	$('#item-desc').froalaEditor('html.set',item.itemDescription);
		        })
		    }

		    function deleteItemOfModule(itemId) {
		        var result = confirm("Want to delete?");
		        if (!result) {
		            //Declined
		            return;
		        }
		        var courseItem = $filter('filter')(vm.courselist, {
		            courseId: vm.form.course
		        })[0];
		        var moduleItem = $filter('filter')(vm.modulelist, {
		            moduleNumber: vm.form.module
		        })[0];

		        vm.itemsOfModule = vm.itemsOfModule.filter(function(data) {
		            if (data.itemId === itemId) {
		                return false;
		            }
		            return true;
		        });

		        var data = {
		            courseId: vm.form.course,
		            courseName: courseItem.courseName,
		            LastUpdated: courseItem.LastUpdated,
		            courseLongDesc: courseItem.courseLongDesc,
		            courseShortDesc: courseItem.courseShortDesc,
		            courseLevel: courseItem.courseLevel,
		            moduleDetail: {
		                moduleNumber: vm.form.module,
		                moduleId: moduleItem.moduleId,
		                moduleName: moduleItem.moduleName,
		                moduleItems: vm.itemsOfModule,
		                moduleWeight: parseFloat(moduleItem.moduleWeight)
		            },
		            author: courseItem.author
		        }
		        pageLoader.show();
		        admin.updateModuleItems(data)
		            .then(function(d) {
		                console.log("Removed Item")
		            })
		            .catch(function() {
		                console.error(err);
		            })
		            .finally(function() {
		                pageLoader.hide();
		            });
		    }

		    function resetDetails() {
		        vm.shouldShowQuestionGroupForm = false;
		        vm.shouldShowVideoForm = false;
		        vm.shouldShowDownloadForm = false;
		        vm.shouldShowBookTopicForm = false;
		        vm.form.item = {};
		        vm.selectedSubjects = [];
		        vm.selectedAuthors = [];
		        vm.selectedPublications = [];
		        vm.selectedBooks = [];
		        vm.selectedExams = [];
		        vm.selectedTopics = [];
		        vm.selectedTags = [];
		        vm.filteredQuestions = [];
		        vm.selectedQuestions = [];
		        vm.filteredQuestionsPageCount = 1;
		    }

		    function showQuestionGroupForm() {
		        vm.resetDetails();
		        vm.shouldShowQuestionGroupForm = true;
		        changeFormType();
		    }

		    function showVideoForm() {
		        vm.resetDetails();
		        vm.shouldShowVideoForm = true;
		        changeFormType();
		    }

		    function showBookTopicForm() {
		        vm.resetDetails();
		        vm.shouldShowBookTopicForm = true;
		        changeFormType();
		    }

		    function showDownloadForm() {
		        vm.resetDetails();
		        vm.shouldShowDownloadForm = true;
		        changeFormType();
		    }

		    function showUploadForm() {
		        vm.resetDetails();
		        vm.shouldShowUploadForm = true;
		        changeFormType();
		    }

		    function changeFormType() {
		    	if (vm.itemsOfModule.length > 0) {
		            vm.itemsOfModule.sort(compare);
		            vm.itemNumber = vm.itemsOfModule[vm.itemsOfModule.length - 1].itemNumber + 1;
		            vm.form.item.itemWeight = vm.itemNumber;
		        } else {
		            vm.itemNumber = 1;
		            vm.form.item.itemWeight = vm.itemNumber;
		        }

		        setTimeout(function(){
		        	$('#item-desc').froalaEditor(EditorConfig);
		        })
		    }

		    function onFilterChange(item, model) {
		        if (vm.selectedSubjects.length > 0) {
		            admin.getAllTopic({
		                subjectId: vm.selectedSubjects[0].subjectId
		            })
		                .then(function(d) {
		                    vm.topics = d.data;
		                });
		        }
		        if (vm.selectedTopics.length > 0) {
		            vm.form = vm.form || {};
		            vm.form.item = vm.form.item || {};
		            vm.form.item.itemName = vm.selectedTopics[0].topicName;
		        } else {
		            vm.form = vm.form || {};
		            vm.form.item = vm.form.item || {};
		            vm.form.item.itemName = "";
		        }

		        var query = {
		                subjects: vm.selectedSubjects.map(function(v) {
		                    return v.subjectId
		                }),
		                authors: vm.selectedAuthors.map(function(v) {
		                    return v.authorId
		                }),
		                publications: vm.selectedPublications.map(function(v) {
		                    return v.publicationId
		                }),
		                books: vm.selectedBooks.map(function(v) {
		                    return v.bookId
		                }),
		                exams: vm.selectedExams.map(function(v) {
		                    return v.examId
		                }),
		                topics: vm.selectedTopics.map(function(v) {
		                    return v.topicId
		                }),
		                tags: vm.selectedTags.map(function(v) {
		                    return v.tagId
		                })
		            }
		            // if(vm.shouldShowQuestionGroupForm){
		        admin.findPracticeQuestionsByQuery(query)
		            .then(function(d) {
		                vm.filteredQuestions = d.data || [];
		                if (vm.filteredQuestions.length > vm.questionsInOnePage) {
		                    vm.filteredQuestionsPageCount = Math.ceil(vm.filteredQuestions.length / vm.questionsInOnePage);
		                    console.log(vm.filteredQuestionsPageCount);
		                }
		            })
		            .catch(function(err) {
		                console.log(err);
		            })
		        // }else if(vm.shouldShowVideoForm){
		        admin.findVideoEntityByQuery(query)
		            .then(function(d) {
		                vm.filteredVideos = d.data;
		                console.log(d);
		            })
		            .catch(function(err) {
		                console.log(err);
		            })
		        // }

				admin.findBookTopicsByQuery(query)
		            .then(function(d) {
		                vm.filteredBookTopics = d.data;
		            })
		            .catch(function(err) {
		                console.log(err);
		            })
		    }

		    function fecthFilteredQuestions() {
		        var start = (vm.filteredQuestionsPageNumber - 1) * vm.questionsInOnePage;
		        if (vm.filteredQuestions) {
		            return vm.filteredQuestions.slice(start, start + vm.questionsInOnePage);
		        } else {
		            return [];
		        }
		    }

		    function filteredQuestionPrev() {
		        if (vm.filteredQuestionsPageNumber > 1) {
		            vm.filteredQuestionsPageNumber--;
		        }
		    }

		    function filteredQuestionNext() {
		        if (vm.filteredQuestionsPageNumber < vm.filteredQuestionsPageCount) {
		            vm.filteredQuestionsPageNumber++;
		        }
		    }

		    function fecthSelectedQuestions() {
		        var start = (vm.selectedQuestionsPageNumber - 1) * vm.questionsInOnePage;
		        if (vm.selectedQuestions) {
		            return vm.selectedQuestions.slice(start, start + vm.questionsInOnePage);
		        } else {
		            return [];
		        }
		    }

		    function selectedQuestionPrev() {
		        if (vm.selectedQuestionsPageNumber > 1) {
		            vm.selectedQuestionsPageNumber--;
		        }
		    }

		    function selectedQuestionNext() {
		        if (vm.selectedQuestionsPageNumber < vm.selectedQuestionsPageCount) {
		            vm.selectedQuestionsPageNumber++;
		        }
		    }

		    function addToSelectedQuestions(questionId) {
		        var que = $filter('filter')(vm.filteredQuestions, {
		            questionId: questionId
		        })[0];
		        vm.selectedQuestions = vm.selectedQuestions || [];
		        vm.selectedQuestions.push(que);

		        if (vm.selectedQuestions.length > vm.questionsInOnePage) {
		            vm.selectedQuestionsPageCount = Math.ceil(vm.selectedQuestions.length / vm.questionsInOnePage);
		        }
		    }

		    function removeFromSelectedQuestions(questionId) {
		        var que = $filter('filter')(vm.selectedQuestions, {
		            questionId: questionId
		        })[0];
		        var pos = vm.selectedQuestions.indexOf(que);
		        vm.selectedQuestions.splice(pos, 1);
		    }

		    function isQuestionSelected(questionId) {
		        var que = $filter('filter')(vm.selectedQuestions, {
		            questionId: questionId
		        }) || [];
		        return (que.length > 0);
		    }

		    function addToSelectedVideo(id) {
		        vm.selectedVideo = id;
		    }

		    function isVideoSelected(id) {
		        if (vm.selectedVideo === id) return true;
		        return false;
		    }


		    function compare(a, b) {
		        return a.itemWeight - b.itemWeight;
		    }


		    function uploadMaterialFile(){
		    	var file = {
		    		type : 'download',
		    		file : vm.materialFile
		    	}
		    	vm.isMaterialUploadInProgress = true;
		    	awsService.uploadFileToS3(file)
		    		.then(function(d){
						var data = d.data;
						vm.form = vm.form || {};
						vm.form.item = vm.form.item || {};
						vm.form.item.materialInfo = data.Key;
						vm.isMaterialUploadInProgress = false;
						vm.isMaterialUploaded = true;
					},function(err){
						console.error(err);
					},function(evt){
						var width = parseInt(100.0 * evt.loaded / evt.total);
						console.log(width);
						vm.uploadProgress = width;
					})
		    }

		    function uploadDownloadMaterialFile(){
		    	var file = {
		    		type : 'download',
		    		file : vm.materialFile
		    	}
		    	vm.isMaterialUploadInProgress = true;
		    	awsService.uploadFileToS3(file)
		    		.then(function(d){
						var data = d.data;
						vm.form = vm.form || {};
						vm.form.item = vm.form.item || {};
						vm.form.item.downloadLink = data.Key;
						vm.isMaterialUploadInProgress = false;
						vm.isMaterialUploaded = true;
					},function(err){
						console.error(err);
					},function(evt){
						var width = parseInt(100.0 * evt.loaded / evt.total);
						console.log(width);
						vm.uploadProgress = width;
					})
		    }

		    

		}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 171:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(43);
	angular
	.module('service')
	.service('awsService', awsService);

	awsService.$inject = ['$http','Upload','BASE_API'];

	function awsService($http,Upload,BASE_API){

		this.uploadFileToS3 = function(data){
			return Upload.upload({
	            url: BASE_API + '/aws/s3/uploadFile',
	            data: data,
	            method: 'POST'
	        });
		}

		this.getS3SignedUrl = function(data){
			return $http.post(BASE_API + '/aws/s3/getSignedUrl',data);
		}
	}

/***/ },

/***/ 172:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page videos-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Module Item</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"course\">Course</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"course\"  ng-model=\"vm.form.course\" ng-change=\"vm.changeCourse()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='course.courseId as course.courseName for course in vm.courselist'>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Course</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"module\">Module</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"module\" ng-model=\"vm.form.module\" ng-change=\"vm.changeModule()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='module.moduleNumber as (module.moduleNumber+ \" - \" +module.moduleName) for module in vm.modulelist'>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Module</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div ng-if=\"vm.shouldShowQuestionGroupForm || vm.shouldShowVideoForm || vm.shouldShowDownloadForm || vm.shouldShowUploadForm || vm.shouldShowBookTopicForm\">\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"item-id\">Item Id</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"item-id\" ng-change=\"vm.onChangeIdField()\" ng-model=\"vm.form.item.itemId\" placeholder=\"Item Id\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.idExistError\">\n\t\t\t\t\t\t\t\tId is already exist\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"item-weight\">Item Weight</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"item-weight\" ng-model=\"vm.form.item.itemWeight\" placeholder=\"Item Weight to Order Items\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"item-desc\">Item Description</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"item-desc\" ng-model=\"vm.form.item.itemDescription\" placeholder=\"Item Description\"></textarea>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowQuestionGroupForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"grp-test\">Is it a Test?</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" class=\"\" id=\"grp-test\" ng-model=\"vm.form.item.itemTakeTest\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowQuestionGroupForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"grp-test\">Question Option Type</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"capital-alpha\" ng-model=\"vm.form.item.itemOptionType\">A,B,C..</input>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"small-alpha\" ng-model=\"vm.form.item.itemOptionType\">a,b,c..</input>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"digits\" ng-model=\"vm.form.item.itemOptionType\">1,2,3..</input>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"roman\" ng-model=\"vm.form.item.itemOptionType\">Roman</input>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"checkboxes\" ng-model=\"vm.form.item.itemOptionType\">Checkboxes</input>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"radio\" ng-model=\"vm.form.item.itemOptionType\">Radio</input>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"radio-checkbox\" ng-model=\"vm.form.item.itemOptionType\">Radio/Checkboxes</input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowQuestionGroupForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"grp-name\">Group Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"grp-name\" ng-model=\"vm.form.item.itemName\" placeholder=\"Question Group Name\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowVideoForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"video-name\">Video Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"video-name\" ng-model=\"vm.form.item.itemName\" placeholder=\"Video Name\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowVideoForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"Slide Page\">Slide Pages</label>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.form.item.slidePageId\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Slide Page...\">\n\t\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t\t        \t{{$item.page_nm}}\n\t\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.page_id as item in (vm.allSideLinksArticle | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t\t        \t{{item.page_nm}}\n\t\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowVideoForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"video-link\">Material</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\"\n\t\t\t\t\t\t\t\t\t\tstyle=\"margin-right: 10px;\" \n\t\t\t\t\t\t\t\t\t\tngf-select\n\t\t\t\t\t\t\t\t\t\tng-model=\"vm.materialFile\"\n\t\t\t\t\t\t\t\t\t\tngf-multiple=\"false\"\n\t\t\t\t\t\t\t\t\t\t>{{vm.materialFileName || 'Select File'}}</div>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.materialFileName && !vm.isMaterialUploadInProgress && !vm.isMaterialUploaded\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\" ng-click=\"vm.uploadMaterialFile()\">Upload</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"btn btn-danger\" \n\t\t\t\t\t\t\t\t\t\t\tng-click=\"vm.materialFileName = undefined\">Reset</div>\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.isMaterialUploadInProgress\">\n\t\t\t\t\t\t\t\t\t\tMaterial Upload in progress {{vm.uploadProgress}}%\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.isMaterialUploaded\">\n\t\t\t\t\t\t\t\t\t\tMaterial Uploaded\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowVideoForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"slidecheck\" style=\"padding-top: 0px;\">Contain Slides?</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" class=\"\" id=\"slidecheck\" ng-model=\"vm.form.item.itemContainsSlide\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowDownloadForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"download-name\">Dowload Item Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"download-name\" ng-model=\"vm.form.item.itemName\" placeholder=\"Download item Name\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowDownloadForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"download-link\">Material</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\"\n\t\t\t\t\t\t\t\t\t\tstyle=\"margin-right: 10px;\" \n\t\t\t\t\t\t\t\t\t\tngf-select\n\t\t\t\t\t\t\t\t\t\tng-model=\"vm.materialFile\"\n\t\t\t\t\t\t\t\t\t\tngf-multiple=\"false\"\n\t\t\t\t\t\t\t\t\t\t>{{vm.materialFileName || 'Select File'}}</div>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.materialFile && !vm.isMaterialUploadInProgress && !vm.isMaterialUploaded\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\" ng-click=\"vm.uploadDownloadMaterialFile()\">Upload</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"btn btn-danger\" \n\t\t\t\t\t\t\t\t\t\t\tng-click=\"vm.materialFile = undefined;vm.materialFileName = undefined\">Reset</div>\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.isMaterialUploadInProgress\">\n\t\t\t\t\t\t\t\t\t\tMaterial Upload in progress {{vm.uploadProgress}}%\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.isMaterialUploaded\">\n\t\t\t\t\t\t\t\t\t\tMaterial Uploaded\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowUploadForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"upload-name\">Upload Item Display Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"upload-name\" ng-model=\"vm.form.item.itemName\" placeholder=\"Upload item Name\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.shouldShowBookTopicForm\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"download-name\">Book Topic Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"download-name\" ng-model=\"vm.form.item.itemName\" placeholder=\"Book Topic Name\"></input>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.shouldShowQuestionGroupForm || vm.shouldShowVideoForm || vm.shouldShowBookTopicForm\">\n\t\t\t\t\t\t\t\t<h5 style=\"text-align: center;\">\n\t\t\t\t\t\t\t\t\tFilter video/questions/book-topic by selecting following items\n\t\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Subject</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<ui-select multiple on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" ng-model=\"vm.selectedSubjects\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t\t<ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-match>\n\t\t\t\t\t\t\t\t\t\t<ui-select-choices repeat=\"item in (vm.subjects | filter: $select.search) track by item.subjectId\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-choices>\n\t\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"author\">Author</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<ui-select multiple on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" ng-model=\"vm.selectedAuthors\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t\t<ui-select-match placeholder=\"Select Author...\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"$item.authorName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-match>\n\t\t\t\t\t\t\t\t\t\t<ui-select-choices repeat=\"item in (vm.authors | filter: $select.search) track by item.authorId\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"item.authorName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-choices>\n\t\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"publication\">Publication</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<ui-select multiple on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" ng-model=\"vm.selectedPublications\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t\t<ui-select-match placeholder=\"Select Publication...\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"$item.publicationName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-match>\n\t\t\t\t\t\t\t\t\t\t<ui-select-choices repeat=\"item in (vm.publications | filter: $select.search) track by item.publicationId\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"item.publicationName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-choices>\n\t\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"book\">Book</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<ui-select multiple on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" ng-model=\"vm.selectedBooks\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t\t<ui-select-match placeholder=\"Select Book...\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"$item.bookName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-match>\n\t\t\t\t\t\t\t\t\t\t<ui-select-choices repeat=\"item in (vm.books | filter: $select.search) track by item.bookId\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"item.bookName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-choices>\n\t\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"exam\">Exam</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<ui-select multiple on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" ng-model=\"vm.selectedExams\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t\t<ui-select-match placeholder=\"Select Exam...\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"$item.examName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-match>\n\t\t\t\t\t\t\t\t\t\t<ui-select-choices repeat=\"item in (vm.exams | filter: $select.search) track by item.examId\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"item.examName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-choices>\n\t\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Topic</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<ui-select multiple on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" ng-model=\"vm.selectedTopics\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t\t<ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-match>\n\t\t\t\t\t\t\t\t\t\t<ui-select-choices repeat=\"item in (vm.topics | filter: $select.search) track by item.topicId\">\n\t\t\t\t\t\t\t\t\t\t<span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-choices>\n\t\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tag\">Tag</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t\t<ui-select multiple on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" ng-model=\"vm.selectedTags\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t\t<ui-select-match placeholder=\"Select Tag...\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"$item.tagName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-match>\n\t\t\t\t\t\t\t\t\t\t<ui-select-choices repeat=\"item in (vm.tags | filter: $select.search) track by item.tagId\">\n\t\t\t\t\t\t\t\t\t\t<span ng-bind=\"item.tagName\"></span>\n\t\t\t\t\t\t\t\t\t\t</ui-select-choices>\n\t\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-md-12\" ng-if=\"vm.shouldShowQuestionGroupForm\">\n\t\t\t\t\t\t\t<div class=\"filtered-question-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"total-questions\">\n\t\t\t\t\t\t\t\t\t<div class=\"head\">All Questions</div>\n\t\t\t\t\t\t\t\t\t<div class=\"page-changer\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"count\">{{vm.filteredQuestionsPageNumber}}/{{ vm.filteredQuestionsPageCount}}</div>\n\t\t\t\t\t\t\t\t\t\t<span class=\"prev\" ng-class=\"{'disabled':!(vm.filteredQuestionsPageNumber > 1)}\" ng-click=\"vm.filteredQuestionPrev()\"><i class=\"fa fa-angle-left\"></i></span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"next\" ng-class=\"{'disabled':!(vm.filteredQuestionsPageNumber < vm.filteredQuestionsPageCount)}\" ng-click=\"vm.filteredQuestionNext()\"><i class=\"fa fa-angle-right\"></i></span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" ng-class=\"{'selected':vm.isQuestionSelected(question.questionId)}\" ng-repeat=\"question in vm.fecthFilteredQuestions()\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\" ng-bind-html=\"question.question\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"oneLine\" ng-bind-html=\"question.questionOneLine\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"action-btn\" ng-if=\"!vm.isQuestionSelected(question.questionId)\" ng-click=\"vm.addToSelectedQuestions(question.questionId)\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" style=\"padding: 10px;\" ng-if=\"vm.fecthFilteredQuestions().length <= 0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\">No Question Found - Change Filter</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"selected-questions\">\n\t\t\t\t\t\t\t\t\t<div class=\"head\">Selected Questions</div>\n\t\t\t\t\t\t\t\t\t<div class=\"page-changer\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"count\">{{vm.selectedQuestionsPageNumber}}/{{vm.selectedQuestionsPageCount}}</div>\n\t\t\t\t\t\t\t\t\t\t<span class=\"prev\" ng-class=\"{'disabled':!(vm.selectedQuestionsPageNumber > 1)}\" ng-click=\"vm.selectedQuestionPrev()\"><i class=\"fa fa-angle-left\"></i></span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"next\" ng-class=\"{'disabled':!(vm.selectedQuestionsPageNumber < vm.selectedQuestionsPageCount)}\" ng-click=\"vm.selectedQuestionNext()\"><i class=\"fa fa-angle-right\"></i></span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" ng-repeat=\"question in vm.fecthSelectedQuestions()\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\" ng-bind-html=\"question.question\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"oneLine\" ng-bind-html=\"question.questionOneLine\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"action-btn\" ng-click=\"vm.removeFromSelectedQuestions(question.questionId)\"><i class=\"fa fa-times\"></i></div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" style=\"padding: 10px;\" ng-if=\"vm.fecthSelectedQuestions().length <= 0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\">No Question Selected</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-md-12\" ng-if=\"vm.shouldShowVideoForm\">\n\t\t\t\t\t\t\t<div class=\"filtered-video-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"total-videos\">\n\t\t\t\t\t\t\t\t\t<div class=\"head\">Filtered Videos - Select atleast One Video</div>\n\t\t\t\t\t\t\t\t\t<div class=\"video-wrapper\" ng-class=\"{'selected':vm.isVideoSelected(video.videoId)}\" ng-repeat=\"video in vm.filteredVideos\" ng-click=\"vm.addToSelectedVideo(video.videoId)\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"name\">{{video.videoName}}</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"video-wrapper\" ng-if=\"!vm.filteredVideos || vm.filteredVideos.length <= 0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"name\">No Video Found - Change Filter</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-md-12\" ng-if=\"vm.shouldShowBookTopicForm\">\n\t\t\t\t\t\t\t<div class=\"filtered-video-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"total-videos\">\n\t\t\t\t\t\t\t\t\t<div class=\"head\">Filtered BookTopics - Select atleast One Topic</div>\n\t\t\t\t\t\t\t\t\t<div class=\"video-wrapper\" ng-class=\"{'selected':vm.selectedBookTopic === bookTopic.bookTopicId}\" ng-repeat=\"bookTopic in vm.filteredBookTopics\" ng-click=\"vm.selectedBookTopic = bookTopic.bookTopicId\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"name\">{{bookTopic.bookTopicName}}</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"video-wrapper\" ng-if=\"!vm.filteredBookTopics || vm.filteredBookTopics.length <= 0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"name\">No BookTopic Found - Change Filter</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info submit\" ng-disabled=\"!vm.form.course || !vm.form.module\" ng-click=\"vm.showQuestionGroupForm()\" style=\"margin-top: 25px;\">New Practice</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info submit\" ng-click=\"vm.showVideoForm()\" ng-disabled=\"!vm.form.course || !vm.form.module\" style=\"margin-top: 25px;\">New Video</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info submit\" ng-click=\"vm.showBookTopicForm()\" ng-disabled=\"!vm.form.course || !vm.form.module\" style=\"margin-top: 25px;\">New Book Topic</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info submit\" ng-click=\"vm.showDownloadForm()\" ng-disabled=\"!vm.form.course || !vm.form.module\" style=\"margin-top: 25px;\">New Download</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info submit\" ng-click=\"vm.showUploadForm()\" ng-disabled=\"!vm.form.course || !vm.form.module\" style=\"margin-top: 25px;\">New Upload</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.course || !vm.form.module || !vm.form.item.itemName || vm.isMaterialUploadInProgress || vm.idExistError || !vm.form.item.itemId\" ng-click=\"vm.addItems()\">{{vm.isMaterialUploadInProgress ? 'Material uploading....' : 'Save'}}</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.itemsOfModule\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.itemsOfModule\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"item in vm.itemsOfModule\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t<div class=\"\">\n\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteItemOfModule(item.itemId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editItemOfModule(item.itemId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t{{item.itemName}} - {{item.itemType}}\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in item\">\n\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t<div class=\"col-xs-10 desc\" style=\"word-break: break-all;\">{{value}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});