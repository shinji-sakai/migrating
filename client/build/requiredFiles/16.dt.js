webpackJsonp([16],{

/***/ 362:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('bookPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(363),
	            controller: AdminBookController,
	            controllerAs: 'vm'
	        });

		AdminBookController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminBookController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;

	        vm.books = [];
	       	vm.no_of_item_already_fetch = 0;
	        vm.checkValidityOfID = checkValidityOfID;
	    	vm.updateBook = updateBook;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removeBook = removeBook;
	    	vm.editBook = editBook;

	    	vm.$onInit = function() {
	            //update books list
	        	//updateBooksList();
		    };

	        function checkValidityOfID() {
	            vm.form.bookId = vm.form.bookId || "";
	            vm.form.bookId = vm.form.bookId.replace(/\s/g , "-");
					admin.isIdExist({
					collection :'book',
					key:"bookId",
					id_value:vm.form.bookId			
				}).then(function(data)
				{				
	              console.log(data.data);
				  if(data.data.idExist)
				  {
					    vm.idExists = true;
				  }
				  else
				  {
					   vm.idExists = false;
				  }
				}).catch(function(err){
	             console.log(err);
				})
				
	            
	        }

	        //get books from db
	        //update books array to display at local
		    function updateBooksList(){
		    	pageLoader.show();
	            //get all books from db
		    	admin.getAllBook()
	        		.then(function(data){
	        			if(data.data.length > 0)
	        				vm.books = data.data;
	                    else
	                        vm.books = [];
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }
		vm.fetchBooks = function(){
				vm.isBooksLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					  collection : 'book'
				})
				.then(function(res){
					$log.debug(res)
					vm.books= res.data;
					vm.no_of_item_already_fetch = 5;
					vm.last_update_dt = vm.books[vm.books.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isBooksLoading = false;
				})
			}

			vm.fetchMoreBooks = function(){
				vm.isBooksLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					  collection : 'book',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.books, {bookId: v.bookId})[0];
							if(!item){
								vm.books.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						   vm.last_update_dt = vm.books[vm.books.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isBooksLoading = false;
				})
			}
	        //update/insert book to db
	    	function updateBook(){
	    		var data = {
	    			bookId : vm.form.bookId,
	    			bookName : vm.form.bookName,
	    			bookDesc : vm.form.bookDesc 
	    		}
				 if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }

	    		pageLoader.show();
	            //update book object to server
	            //if books is not there the it will insert new 
	            //else it will update
	    		admin.updateBook(data)
	    			.then(function(res){
						// 	var item = $filter('filter')(vm.books || [], {bookId: vm.form.bookId})[0];
						// if(item){
						// 	item.bookId = vm.form.bookId,
						// 	item.bookName = vm.form.bookName,
						// 	item.bookDesc = vm.form.bookDesc 
						// }else{
						// 	vm.books = [data].concat(vm.books || [])
						// }
						vm.fetchBooks();
	    				vm.submitted = true;
	    				vm.form.error = false;
	                    //reset from fields
	    				vm.form.bookName = "";
	    				vm.form.bookDesc = "";
	                    vm.form.bookId = "";
	                    vm.idExists = false;
	                    vm.formIsInEditMode = false;
	                    //updates books array locally
	    			//	updateBooksList();
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}

	    	function resetFormDetail(){
	    		vm.submitted = false;
	            vm.idExists = false;
	            vm.formIsInEditMode = false;
	    		vm.form = {};
	    	}


	        //this function will call api and remove book from db
	    	function removeBook(id){
	            //ask for confirmation
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	            //service to remove book from server
	    		admin.removeBook({bookId:id})
	    			.then(function(d){
	    				//update array locally
						var item = $filter('filter')(vm.books || [], {bookId: id})[0];								
						 if (vm.books.indexOf(item) > -1) {
	                        var pos = vm.books.indexOf(item);
	                       vm.books.splice(pos, 1);
	                    }
	    				//updateBooksList();
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}

	        //this function will be called when user will click on press button of book 
	    	function editBook(id){
	            //fetch book from book array
	    		var book = $filter('filter')(vm.books,{bookId:id})[0];
	    		vm.form = {};
	            //update form field from table
	    		vm.form.bookId = book.bookId;
	    		vm.form.bookName = book.bookName;
	    		vm.form.bookDesc = book.bookDesc;
	            vm.formIsInEditMode = true;
	    	}
	    }

/***/ },

/***/ 363:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateBook()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Book</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"bookId\">Book Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"bookId\" class=\"form-control\" placeholder=\"Id\" ng-change=\"vm.checkValidityOfID()\" ng-disabled=\"vm.formIsInEditMode\"  ng-model=\"vm.form.bookId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.idExists\">\n\t\t\t\t\t\t\t\tBook Id exists\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"bookname\">Book Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"bookname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.bookName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"bookdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"bookdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.bookDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Book</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\" ng-disabled=\"vm.idExists\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchBooks()\" ng-if=\"!vm.books || vm.books.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isBooksLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isBooksLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Book</span>\n\t\t\t</div>\t\n\t\t</div>\n\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.books\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.books\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"book in vm.books\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeBook(book.bookId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editBook(book.bookId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{book.bookName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in book\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreBooks()\" ng-if=\"(vm.books==[] || vm.books.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isBooksLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isBooksLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\t\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});