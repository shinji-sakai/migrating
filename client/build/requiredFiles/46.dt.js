webpackJsonp([46],{

/***/ 120:
/***/ function(module, exports) {

	  angular
	    .module('service')
	    .service('admin', admin);

	admin.$inject = ['$http','BASE_API'];

	function admin($http,BASE_API) {


	    this.isIdExist = function(data) {
	        return $http.post(BASE_API + '/admin/general/isIdExist',data);
	    }


	    this.fetchItemUsingLimit = function(data) {
	        return $http.post(BASE_API + '/admin/general/fetchItemUsingLimit',data);
	    }

	    /* Course Master API */

	    this.coursemaster = function() {
	        //  /admin/coursemaster
	        return $http.post(BASE_API + '/courseMaster/getAll').then(function(res) {
	            return res.data;
	        });
	    }

	    

	    this.addCourseMaster = function(data) {
	        //      /admin/addCourseMaster
	        return $http.post(BASE_API + '/courseMaster/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseMaster = function(data) {
	        //      /admin/deleteCourseMaster
	        return $http.post(BASE_API + '/courseMaster/delete', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.fetchCourseMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/courseMaster/fetchCourseMasterUsingLimit', data);
	    }

	    this.getRelatedCoursesByCourseId = function(data) {
	        return $http.post(BASE_API + '/courseMaster/getRelatedCoursesByCourseId',data);
	    }

	    /* Course API */
	    this.courselistfull = function(data) {
	        return $http.post(BASE_API + '/course/getAll',data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourse = function(data) {
	        // /admin/addCourse
	        return $http.post(BASE_API + '/course/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.removeCourse = function(data) {
	        //  /admin/removeCourse
	        return $http.post(BASE_API + '/course/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Videos API */
	    this.addVideos = function(data) {
	        //  /admin/addVideos
	        return $http.post(BASE_API + '/courseVideos/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getModuleVideos = function(data) {
	        //  /admin/getModuleVideos
	        return $http.post(BASE_API + '/courseVideos/getModuleVideos', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getCourseVideos = function(data) {
	        //      /admin/getCourseVideos
	        return $http.post(BASE_API + '/courseVideos/getCourseVideos', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Modules API */
	    this.getCourseModules = function(data) {
	        //      /admin/getCourseModules
	        return $http.post(BASE_API + '/courseModule/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourseModule = function(data) {
	        //  /admin/addCourseModule
	        return $http.post(BASE_API + '/courseModule/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseModule = function(data) {
	        //  /admin/deleteCourseModule
	        return $http.post(BASE_API + '/courseModule/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Video Slides API */
	    this.addVideoSlide = function(data) {
	        //  /admin/addVideoSlide
	        return $http.post(BASE_API + '/videoSlide/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getVideoSlides = function(data) {
	        //  /admin/getVideoSlides
	        return $http.post(BASE_API + '/videoSlide/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteVideoSlide = function(data) {
	        //  /admin/deleteVideoSlide
	        return $http.post(BASE_API + '/videoSlide/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Authorized Items API */

	    this.getAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/getItems',data);
	    }

	    this.updateAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/updateItems',data);
	    }

	    this.addAuthorizedItem = function(data){
	        return $http.post(BASE_API + '/ai/addItem',data);
	    }

	    this.getAuthorizedVideoItemFullInfo = function(data){
	        return $http.post(BASE_API + '/ai/getVideoItemFullInfo',data);
	    }
	    this.getVideoPermission = function(data){
	        return $http.post(BASE_API + '/ai/getVideoPermission',data);
	    }


	    /* practice Question API */

	    this.savePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestion',data);
	    }
	    this.savePreviewPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/savePreviewQuestion',data);
	    }
	    this.saveQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestionStats',data);
	    }
	    this.saveTestQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveTestQuestionStats',data);
	    }
	    this.saveTestReviews  = function(data){
	        return $http.post(BASE_API + '/practice/saveTestReviews',data);
	    }
	    this.removePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/removeQuestion',data);
	    }
	    this.getAllPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getAllQuestions',data);
	    }
	    this.findPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsByQuery',data);
	    }
	    this.findPracticeQuestionsIdByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsIdByQuery',data);
	    }
	    this.findPracticeQuestionsTextByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsTextByQuery',data);
	    }
	    this.findPreviewPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findPreviewQuestionsByQuery',data);
	    }
	    this.getPracticeQuestionsById = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsById',data);
	    }

	    this.getPracticeQuestionsByLimit = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsByLimit',data);
	    }

	    this.updateMultiplePracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/updateMultiplePracticeQuestions',data);
	    }
	    this.getAllAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersQuestionPerfStats',data);
	    }
	    this.getAllMonthAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllMonthAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersMonthQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersMonthQuestionPerfStats',data);
	    }
	    this.updateVerifyPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/updateVerifyPracticeQuestion',data);
	    }
	    this.getAllVerifyStatusOfPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getAllVerifyStatusOfPracticeQuestion',data);
	    }
	    this.getLatestVerifyStatusOfGivenPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getLatestVerifyStatusOfGivenPracticeQuestions',data);
	    }
	    this.getUserMonthQuestionAttemptStats = function(data){
	        return $http.post(BASE_API + '/practice/getUserMonthQuestionAttemptStats',data);
	    }
	    this.startUserExam = function(data){
	        return $http.post(BASE_API + '/practice/startUserExam',data);
	    }
	    this.finishUserExam = function(data){
	        return $http.post(BASE_API + '/practice/finishUserExam',data);
	    }
	    this.checkForPreviousExam = function(data){
	        return $http.post(BASE_API + '/practice/checkForPreviousExam',data);
	    }
	    this.getUserExamQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getUserExamQuestion',data);
	    }
	    this.saveUserExamQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserExamQuestionData',data);
	    }
	    this.startUserPractice = function(data){
	        return $http.post(BASE_API + '/practice/startUserPractice',data);
	    }
	    this.saveUserPracticeQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserPracticeQuestionData',data);
	    }
	    this.fetchNextUserPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/fetchNextUserPracticeQuestion',data);
	    }
	    this.getPracticeNodeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeNodeQuestions',data);
	    }






	    /* Highlight APi */

	    this.saveHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/save',data);
	    }
	    this.getQuestionHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getQuestionHighlights',data);
	    }
	    this.getUserHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getUserHighlights',data);
	    }
	    this.deleteHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/delete',data);
	    }

	    this.updateHighlightNote = function(data){
	        return $http.post(BASE_API + '/highlight/updateHighlightNote',data);
	    }

	    /*
	    Author API
	     */
	    this.getAllAuthor  = function(){
	        return $http.post(BASE_API + '/author/getAll');
	    }
	    this.updateAuthor  = function(data){
	        return $http.post(BASE_API + '/author/update',data);
	    }
	    this.removeAuthor  = function(data){
	        return $http.post(BASE_API + '/author/remove',data);
	    }
	    this.fetchAuthorUsingLimit=function(data){
	        return $http.post(BASE_API+'/author/fetchAuthorUsingLimit',data);
	    }
	    this.AuthorsIdIsExits=function(data){
	        return $http.post(BASE_API+'/author/AuthorsIdIsExits',data);
	    }

	    /*
	    Publication API
	     */
	    this.getAllPublication  = function(){
	        return $http.post(BASE_API + '/publication/getAll');
	    }
	    this.updatePublication  = function(data){
	        return $http.post(BASE_API + '/publication/update',data);
	    }
	    this.removePublication  = function(data){
	        return $http.post(BASE_API + '/publication/remove',data);
	    }
	   this.fetchPublicationsUsingLimit=function(data){
	       return $http.post(BASE_API+'/publication/fetchPublicationsUsingLimit',data);
	   }
	    /*
	    Book API
	     */
	    this.getAllBook  = function(){
	        return $http.post(BASE_API + '/book/getAll');
	    }
	    this.updateBook  = function(data){
	        return $http.post(BASE_API + '/book/update',data);
	    }
	    this.removeBook  = function(data){
	        return $http.post(BASE_API + '/book/remove',data);
	    }
	    this.fetchBooksUsingLimit=function(data){
	        return $http.post(BASE_API+'/book/fetchBooksUsingLimit',data);
	    }

	    /*
	    JobOpenings API
	     */
	    this.getAllJobOpenings  = function(){
	        return $http.post(BASE_API + '/jobOpenings/getAllJobOpenings');
	    }
	    this.updateJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/updateJobOpening',data);
	    }
	    this.removeJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/removeJobOpening',data);
	    }

	    /*
	    Subject API
	     */
	    this.getAllSubject  = function(){
	        return $http.post(BASE_API + '/subject/getAll');
	    }
	    this.updateSubject  = function(data){
	        return $http.post(BASE_API + '/subject/update',data);
	    }
	    this.removeSubject  = function(data){
	        return $http.post(BASE_API + '/subject/remove',data);
	    }
	   this.fetchSubjectMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/subject/fetchSubjectMasterUsingLimit',data);
	    }
	    this.SubjectIdIsExits=function(data){
	        return $http.post(BASE_API+'/subject/SubjectIdIsExits',data);
	    }
	    /*
	    Exam API
	     */
	    this.getAllExam  = function(){
	        return $http.post(BASE_API + '/exam/getAll');
	    }
	    this.updateExam  = function(data){
	        return $http.post(BASE_API + '/exam/update',data);
	    }
	    this.removeExam  = function(data){
	        return $http.post(BASE_API + '/exam/remove',data);
	    }
	    this.fetchExamUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/exam/fetchExamUsingLimit',data);
	    }
	    /*
	    Topic API
	     */
	    this.getAllTopic  = function(data){
	        return $http.post(BASE_API + '/topic/getAll',data);
	    }
	    this.updateTopic  = function(data){
	        return $http.post(BASE_API + '/topic/update',data);
	    }
	    this.removeTopic  = function(data){
	        return $http.post(BASE_API + '/topic/remove',data);
	    }
	   this.fetchTopicsUsingLimit=function(data)
	   {
	       return $http.post(BASE_API+'/topic/fetchTopicsUsingLimit',data);
	   }
	    /*
	    Topic Group API
	     */
	    this.getAllTopicGroups  = function(data){
	        return $http.post(BASE_API + '/topicGroup/getAllTopicGroups',data);
	    }
	    this.updateTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/updateTopicGroup',data);
	    }
	    this.removeTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/removeTopicGroup',data);
	    }
	    this.fetchTopicGroupUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/topicGroup/fetchTopicGroupUsingLimit',data);
	    }
	    /*
	    Tag API
	     */
	    this.getAllTag  = function(){
	        return $http.post(BASE_API + '/tag/getAll');
	    }
	    this.updateTag  = function(data){
	        return $http.post(BASE_API + '/tag/update',data);
	    }
	    this.removeTag  = function(data){
	        return $http.post(BASE_API + '/tag/remove',data);
	    }
	    this.fetchTagsUsingLimit=function(data){
	        return $http.post(BASE_API+'/tag/fetchTagsUsingLimit',data);
	    }
	    this.TagsIdIsExits=function(data)
	    {
	        return $http.post(BASE_API+'/tag/TagsIdIsExits',data);
	    }

	    /*
	    SubTopic API
	     */
	    this.getAllSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/getAll',data);
	    }
	    this.updateSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/update',data);
	    }
	    this.removeSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/remove',data);
	    }

	    /* videoentity api */
	    this.updateVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/update',data);
	    }
	    this.removeVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/remove',data);
	    }
	    this.getAllVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/getAll',data);
	    }
	    this.isVideoFilenameExist = function(data){
	        return $http.post(BASE_API + '/videoentity/isVideoFilenameExist',data);
	    }
	    this.findVideoEntityByQuery = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityByQuery',data);
	    }
	            //One Id
	    this.findVideoEntityById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityById',data);
	    }
	            //Multiple Ids
	    this.findVideoEntitiesById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntitiesById',data);
	    }
	    this.getRelatedTopicsVideoByVideoId = function(data){
	        return $http.post(BASE_API + '/videoentity/getRelatedTopicsVideoByVideoId',data);
	    }
	    this.getRelatedVideosByVideoId = function(data) {
	        return $http.post(BASE_API + '/videoentity/getRelatedVideosByVideoId',data);
	    }
	    this.updateVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/updateVideoLastViewTime',data);
	    }
	    this.getVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/getVideoLastViewTime',data);
	    }


	    /* moduleItems api */
	    this.updateModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/update',data);
	    }
	    this.getAllModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/getAll',data);
	    }
	    this.getCourseItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getCourseItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getFullCourseDetails = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getFullCourseDetails', data);
	    }
	    
	    this.getModuleItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getModuleItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getUserBatchEnrollUploads = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getUserBatchEnrollUploads',data);
	    }

	    /* createtest api */
	    this.createTest = function(data){
	        return $http.post(BASE_API + '/createtest/save',data);
	    }
	    this.getAllTest = function(data){
	        return $http.post(BASE_API + '/createtest/getAll',data);
	    }
	    this.getTestById = function(data){
	        return $http.post(BASE_API + '/createtest/getById',data);
	    }

	    /*
	        CourseBundle API
	     */
	    this.getAllCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/getAll',data);
	    }
	    this.updateCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/update',data);
	    }
	    this.removeCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/remove',data);
	    }



	    // Employee types api
	    this.addEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/add',data);
	    }
	    this.getAllEmployeeTypes  = function(data){
	        return $http.post(BASE_API + '/employee/types/getAll',data);
	    }
	    this.deleteEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/delete',data);
	    }

	    // Employee Details api
	    this.addEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/add',data);
	    }
	    this.getAllEmployees  = function(data){
	        return $http.post(BASE_API + '/employee/getAll',data);
	    }
	    this.deleteEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/delete',data);
	    }

	    // Employee Skills
	    this.addEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/add',data);
	    }
	    this.getAllEmployeeSkills  = function(data){
	        return $http.post(BASE_API + '/employee/skills/getAll',data);
	    }
	    this.deleteEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/delete',data);
	    }

	    // All links category
	    this.addAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/add',data);
	    }
	    this.getAllLinksCategories  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/getAll',data);
	    }
	    this.deleteAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/delete',data);
	    }


	    // Sidelinks article
	    this.addSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/update',data);
	    }
	    this.getAllSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getAll',data);
	    }
	    this.getSideLinkArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/get',data);
	    }
	    this.getMultipleSideLinkArticles = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getMultipleArticles',data);
	    }
	    this.deleteSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/remove',data);
	    }


	    // All Links
	    this.addLinkToAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/update',data);
	    }
	    this.getAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/getAll',data);
	    }
	    this.deleteLinkFromAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/remove',data);
	    }


	    // BoardCompetitiveCourse
	    this.updateBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/update',data);
	    }
	    this.getBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/get',data);
	    }
	    this.getBoardCompetitiveCoursesByCourseType  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/getCoursesByCourseType',data);
	    }
	    this.deleteBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/remove',data);
	    }


	    // Course Subgroup
	    this.addSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/add',data);
	    }
	    this.getAllSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/getAll',data);
	    }
	    this.deleteSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/delete',data);
	    }

	    // Save Batch Course
	    this.addBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/add',data);
	    }
	    this.getAllBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getAll',data);
	    }
	    this.deleteBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/delete',data);
	    }
	    this.getBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getBatchCourse',data);
	    }

	    // Save Batch Timing
	    this.addBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/add',data);
	    }
	    this.getAllBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getAll',data);
	    }
	    this.deleteBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/delete',data);
	    }
	    this.getTimingByCourseId = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getTimingByCourseId',data);
	    }


	    // Save Batch Timing Dashboard
	    this.addBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/addBatchTimingDashboard',data);
	    }
	    this.getAllBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/getAllBatchTimingDashboard',data);
	    }
	    this.deleteBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/deleteBatchTimingDashboard',data);
	    }
	    this.setBatchTimingDashboardAsCompleted  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/setBatchTimingDashboardAsCompleted',data);
	    }

	    //

	    // Currency
	    this.addCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/add',data);
	    }
	    this.getAllCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/getAll',data);
	    }
	    this.deleteCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/delete',data);
	    }

	    // Getting STarted
	    this.addGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/add',data);
	    }
	    this.getAllGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getAll',data);
	    }
	    this.deleteGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/delete',data);
	    }
	    this.getGettingStartedByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getByCourseId',data);
	    }

	    // Sessions
	    this.addSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/add',data);
	    }
	    this.getAllSessions  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getAll',data);
	    }
	    this.deleteSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/delete',data);
	    }
	    this.getSessionsByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getByCourseId',data);
	    }

	    // Sessions Items
	    this.addSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/add',data);
	    }
	    this.getAllSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getAll',data);
	    }
	    this.deleteSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/delete',data);
	    }
	    this.getSessionDetailsForBatchAndSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getSessionDetailsForBatchAndSession',data);
	    }


	    // Pricelist Course
	    this.savePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/add',data);
	    }
	    this.getAllPricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/getAll',data);
	    }
	    this.deletePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/delete',data);
	    }
	    this.getDetailedPricelist = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelist',data);
	    }
	    this.getPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getPricelistByCourseId',data);
	    }
	    this.getDetailedPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelistByCourseId',data);
	    }


	    // Bundle
	    this.saveBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/add',data);
	    }
	    this.getAllBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/getAll',data);
	    }
	    this.deleteBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/delete',data);
	    }

	    // Email Templates
	    this.addEmailTemplates  = function(data){
	        return $http.post(BASE_API + '/emailTemplates/addEmailTemplates',data);
	    }

	    this.getAllEmailTemplates = function(data){
	        return $http.post(BASE_API + '/emailTemplates/getAllEmailTemplates',data);
	    }

	    this.deleteEmailTemplate = function(data){
	        return $http.post(BASE_API + '/emailTemplates/deleteEmailTemplate',data);
	    }

	    // UserEmail Group
	    this.addUserEmailGroup  = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/addUserEmailGroup',data);
	    }

	    this.getAllUserEmailGroups = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/getAllUserEmailGroups',data);
	    }

	    this.deleteUserEmailGroup = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/deleteUserEmailGroup',data);
	    }

	    // Send Email Templates
	    this.addSendEmailTemplate  = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/addSendEmailTemplate',data);
	    }

	    this.getAllSendEmailTemplates = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/getAllSendEmailTemplates',data);
	    }

	    this.deleteSendEmailTemplate = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/deleteSendEmailTemplate',data);
	    }

	    this.sendEmailTemplatesToUsers = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendEmailTemplatesToUsers',data);
	    }

	    this.sendNotificationOfEmail = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendNotificationOfEmail',data);
	    }

	    // Send Notification
	    this.addSendNotification  = function(data){
	        return $http.post(BASE_API + '/sendNotification/addSendNotification',data);
	    }

	    this.getAllSendNotifications = function(data){
	        return $http.post(BASE_API + '/sendNotification/getAllSendNotifications',data);
	    }

	    this.deleteSendNotification = function(data){
	        return $http.post(BASE_API + '/sendNotification/deleteSendNotification',data);
	    }

	    this.getSendNotificationByUserId = function(data){
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationByUserId',data);
	    }

	    this.markSendNotificationAsRead = function(data){
	        return $http.post(BASE_API + '/sendNotification/markSendNotificationAsRead',data);
	    }

	    this.getSendNotificationCount = function(data) {
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationCount',data);
	    }


	    // Roles
	    this.addRole  = function(data){
	        return $http.post(BASE_API + '/adminRoles/addRole',data);
	    }

	    this.getAllRoles = function(data){
	        return $http.post(BASE_API + '/adminRoles/getAllRoles',data);
	    }

	    this.deleteRole = function(data){
	        return $http.post(BASE_API + '/adminRoles/deleteRole',data);
	    }


	    // forms
	    this.addForm  = function(data){
	        return $http.post(BASE_API + '/adminForms/addForm',data);
	    }

	    this.getAllForms = function(data){
	        return $http.post(BASE_API + '/adminForms/getAllForms',data);
	    }

	    this.deleteForm = function(data){
	        return $http.post(BASE_API + '/adminForms/deleteForm',data);
	    }

	    // forms  help
	    this.addFormHelp  = function(data){
	        return $http.post(BASE_API + '/formsHelp/updateFormHelp',data);
	    }

	    this.getAllFormsHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getAllFormsHelp',data);
	    }

	    this.getFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getFormHelp',data);
	    }

	    this.deleteFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/deleteFormHelp',data);
	    }

	    // user roles
	    this.addUserRole  = function(data){
	        return $http.post(BASE_API + '/userRoles/addUserRole',data);
	    }

	    this.getAllUserRoles = function(data){
	        return $http.post(BASE_API + '/userRoles/getAllUserRoles',data);
	    }

	    this.deleteUserRole = function(data){
	        return $http.post(BASE_API + '/userRoles/deleteUserRole',data);
	    }

	    this.getUserAdminRoleFormAccessList = function(data) {
	        return $http.post(BASE_API + '/userRoles/getUserAdminRoleFormAccessList',data);
	    }

	    // user batch enroll
	    this.getUserEnrollAllBatch = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/getUserEnrollAllBatch',data);
	    }
	    this.addUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/addUserBatchEnroll',data);
	    }
	    this.removeUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/removeUserBatchEnroll',data);
	    }



	    this.updateBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/updateBookTopic',data);
	    }
	    this.removeBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/removeBookTopic',data);
	    }
	    this.getAllBookTopics = function(data) {
	        return $http.post(BASE_API + '/booktopic/getAllBookTopics',data);
	    }
	    this.findBookTopicsByQuery = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsByQuery',data);
	    }
	    this.findBookTopicById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicById',data);
	    }
	    this.findBookTopicsById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsById',data);
	    }


	    this.updatePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/update',data);
	    }
	    this.removePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/remove',data);
	    }
	    this.getAllPreviousPapers = function(data) {
	        return $http.post(BASE_API + '/previousPapers/getAll',data);
	    }
	    this.getPreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/get',data);
	    }


	    //Save Purchased Course Based on COurse ids
	    this.purchasedCourse = function(data) {
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnCourseId',data);
	    }
	    //Save purchased courses based on bundle ids
	    this.purchasedBundle = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnBundleId',data);
	    }
	    //save purchased courses based on training ids
	    this.purchasedTraining = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnTrainingId',data);
	    }
	}


/***/ },

/***/ 158:
/***/ function(module, exports) {

	angular
		.module('service')
		.service('Mail',Mail);

	Mail.$inject = ['$http','BASE_API'];

	function Mail($http,BASE_API) {
	    this.sendMail = function (data) {
	        return $http.post(BASE_API + '/mail/send', data).then(function (res) {
	            return res.data;
	        });
	    }

	    this.sendBulkEmails = function (data) {
	        return $http.post(BASE_API + '/mail/sendBulkEmails', data);
	    }

	    this.subscribeUser = function (data) {
	        return $http.post(BASE_API + '/mail/subscribeUser', data);
	    }

	    this.sendMailToSupportForEnrollBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForEnrollBtn', data);   
	    }

	    this.sendMailToSupportForPaySecurelyBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForPaySecurelyBtn', data);   
	    }
	}


/***/ },

/***/ 437:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(120);
	__webpack_require__(158);
	__webpack_require__(438);
	angular.module(window.moduleName)
	    .component('sendEmailTemplatePage', {
	        template: __webpack_require__(440),
	        controller: SendEmailTemplatesController,
	        controllerAs: 'vm'
	    })

	SendEmailTemplatesController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin'];

	function SendEmailTemplatesController($log,$filter, $scope, $state, EditorConfig, $timeout,admin) {
	    var vm = this;

	    vm.selectedEmailTemplate = [];
	    vm.selectedUserEmailGroup = [];
	    vm.selectedTraining = [];
	    vm.selectedBatch = [];

	    vm.onTrainingChange = onTrainingChange;
	    vm.onBatchChange = onBatchChange;
	    vm.onUserEmailGroupChange = onUserEmailGroupChange;
	    vm.onEmailTemplateChange = onEmailTemplateChange;

	    vm.addSendEmailTemplate = addSendEmailTemplate;
	    vm.editSendEmailTemplate = editSendEmailTemplate;
	    vm.deleteSendEmailTemplate = deleteSendEmailTemplate;
	    vm.resetFormDetails = resetFormDetails;
	    vm.sendEmailTemplatesToUsers = sendEmailTemplatesToUsers;
	    vm.sendNotificationOfEmail = sendNotificationOfEmail;


	    vm.$onInit = function() {
	        getAllSendEmailTemplates();
	        getAllEmailTemplates();
	        getAllUserEmailGroups();
	        getAllBatchTraining();
	    }

	    function getAllSendEmailTemplates(){
	        admin.getAllSendEmailTemplates()
	            .then(function(res){
	                vm.allSendEmailTemplates = res.data;
	                $log.debug(res);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function getAllEmailTemplates(){
	        admin.getAllEmailTemplates()
	            .then(function(res){
	                vm.allTemplates = res.data;
	                $log.debug(res);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function getAllUserEmailGroups(){
	        admin.getAllUserEmailGroups()
	            .then(function(res){
	                vm.allUserEmailGroup = res.data;
	                $log.debug(res);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function getAllBatchTraining() {
	        admin.getAllBatchCourse()
	            .then(function(res){
	                vm.allTrainings = res.data;
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function onTrainingChange(){
	        vm.form = vm.form || {};
	        vm.form.training_id = vm.selectedTraining[0] || "";
	        getAllTimingByTrainingId();
	    }

	    function onBatchChange(){
	        vm.form = vm.form || {};
	        vm.form.bat_id = vm.selectedBatch[0] || "";
	    }

	    function  getAllTimingByTrainingId(id) {
	        admin.getTimingByCourseId({crs_id : id || vm.selectedTraining[0]})
	            .then(function(res){
	                vm.allBatches = res.data;
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function onEmailTemplateChange(){
	        vm.form = vm.form || {};
	        vm.form.email_template = vm.selectedEmailTemplate[0] || undefined;
	    }

	    function onUserEmailGroupChange(){
	        vm.form = vm.form || {};
	        vm.form.users_template = vm.selectedUserEmailGroup[0] || undefined;
	    }

	    function addSendEmailTemplate(){
	        vm.formSuccess = undefined;
	        admin.addSendEmailTemplate(vm.form)
	            .then(function(res){

	                var pk_id = res.data.pk_id;
	                var group = $filter("filter")(vm.allSendEmailTemplates,{pk_id : pk_id})[0];
	                if(group){
	                    var pos = vm.allSendEmailTemplates.indexOf(group);
	                    vm.allSendEmailTemplates.splice(pos,1);    
	                }

	                vm.formSuccess = true;
	                vm.allSendEmailTemplates.push(res.data);
	                vm.form = {};
	                vm.selectedTraining = [];
	                vm.selectedBatch = [];
	                vm.selectedEmailTemplate = [];
	                vm.selectedUserEmailGroup = [];
	            })
	            .catch(function(err){
	                $log.error(err);
	                vm.formSuccess = false;
	            })
	    }

	    function sendEmailTemplatesToUsers(data) {
	        vm.emailSent = vm.emailSent || {};
	        vm.emailSentInProgress = vm.emailSentInProgress || {};
	        vm.emailSent[data.pk_id] = undefined;
	        vm.emailSentInProgress[data.pk_id] = true;
	        admin.sendEmailTemplatesToUsers(data)
	            .then(function(res){
	                vm.emailSent[data.pk_id] = true;
	                vm.emailSentInProgress[data.pk_id] = false;

	            })
	            .catch(function(err){
	                vm.emailSent[data.pk_id] = false;
	                $log.error(err);
	            })
	            .finally(function(){
	                $timeout(function(){
	                    vm.emailSent[data.pk_id] = undefined;
	                    vm.emailSentInProgress[data.pk_id] = false;
	                },1000*5);
	            })
	    }

	    function sendNotificationOfEmail(data){
	        vm.emailNotificationSent = vm.emailNotificationSent || {};
	        vm.emailNotificationSentInProgress = vm.emailNotificationSentInProgress || {};
	        vm.emailNotificationSent[data.pk_id] = undefined;
	        vm.emailNotificationSentInProgress[data.pk_id] = true;
	        admin.sendNotificationOfEmail(data)
	            .then(function(res){
	                vm.emailNotificationSent[data.pk_id] = true;
	                vm.emailNotificationSentInProgress[data.pk_id] = false;
	            })
	            .catch(function(err){
	                vm.emailNotificationSent[data.pk_id] = false;
	                vm.emailNotificationSentInProgress[data.pk_id] = false;
	                $log.error(err);
	            })
	            .finally(function(){
	                $timeout(function(){
	                    vm.emailNotificationSent[data.pk_id] = undefined;
	                    vm.emailNotificationSentInProgress[data.pk_id] = false;
	                },1000*5);
	            })
	    }

	    function editSendEmailTemplate(pk_id){
	        vm.formSuccess = undefined;
	        var template = $filter("filter")(vm.allSendEmailTemplates,{pk_id : pk_id});
	        if(template && template[0]){
	            vm.form = template[0];

	            vm.selectedTraining = [vm.form.training_id]
	            vm.selectedBatch = [vm.form.bat_id]
	            vm.selectedUserEmailGroup = [vm.form.users_template];
	            vm.selectedEmailTemplate = [vm.form.email_template];
	            getAllTimingByTrainingId(vm.form.training_id);
	            if(!vm.allTemplates || vm.allTemplates.length <=0){
	                getAllEmailTemplates();    
	            }

	            if(!vm.allUserEmailGroup || vm.allUserEmailGroup.length <=0){
	                getAllUserEmailGroups();
	            }
	        }
	    }



	    function deleteSendEmailTemplate(pk_id){
	        var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        vm.formSuccess = undefined;
	        admin.deleteSendEmailTemplate({pk_id : pk_id})
	            .then(function(res){
	                var pk_id = res.data.pk_id;
	                var template = $filter("filter")(vm.allSendEmailTemplates,{pk_id : pk_id})[0];
	                var pos = vm.allSendEmailTemplates.indexOf(template);
	                vm.allSendEmailTemplates.splice(pos,1);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function resetFormDetails(){
	        vm.form = {};
	        vm.selectedEmailTemplate = [];
	        vm.selectedUserEmailGroup = [];
	        vm.selectedTraining = [];
	        vm.selectedBatch = [];       
	    }
	}

/***/ },

/***/ 438:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(439);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_sendEmailTemplate.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_sendEmailTemplate.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 439:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.ew-btns-wrapper-flex-end {\n  margin: 10px 0px;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: end;\n  -moz-box-pack: end;\n  box-pack: end;\n  -webkit-justify-content: flex-end;\n  -moz-justify-content: flex-end;\n  -ms-justify-content: flex-end;\n  -o-justify-content: flex-end;\n  justify-content: flex-end;\n  -ms-flex-pack: end;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n\n.ew-btn {\n  background: #1d99d1;\n  margin: 0px 10px;\n  padding: 5px 20px;\n  color: white;\n  border-radius: 20px;\n  font-size: 15px;\n  font-family: 'nunito',sans-serif;\n  font-weight: bold;\n  cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 440:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" onsubmit=\"return false;\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Send Email Template</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"Formname\">Form Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"Formname\" placeholder=\"Form Name\" ng-model=\"vm.form.frm_nm\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Training</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTraining\" theme=\"bootstrap\" on-select=\"vm.onTrainingChange()\" on-remove=\"vm.onTrainingChange()\" limit=\"1\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Training...\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{$item.crs_id}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.crs_id as item in (vm.allTrainings | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{item.crs_id}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Batch</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBatch\" theme=\"bootstrap\" on-select=\"vm.onBatchChange()\" on-remove=\"vm.onBatchChange()\" limit=\"1\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Batch...\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{$item.cls_start_dt | date : 'dd-MMM-yyyy'}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.bat_id as item in (vm.allBatches | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{item.cls_start_dt | date : 'dd-MMM-yyyy'}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Email Temaplates</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedEmailTemplate\" theme=\"bootstrap\" on-select=\"vm.onEmailTemplateChange()\" on-remove=\"vm.onEmailTemplateChange()\" limit=\"1\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Email Template...\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{$item.frm_nm}} -- {{$item.training_id}} - <strong>{{$item.batch_details.cls_start_dt | date : 'dd-MMM-yyyy'}}</strong>\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.pk_id as item in (vm.allTemplates | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{item.frm_nm}} -- {{item.training_id}} - <strong>{{item.batch_details.cls_start_dt | date : 'dd-MMM-yyyy'}}</strong>\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"user email group\">User Email Group</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedUserEmailGroup\" theme=\"bootstrap\" on-select=\"vm.onUserEmailGroupChange()\" on-remove=\"vm.onUserEmailGroupChange()\" limit=\"1\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select User Email Group...\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{$item.frm_nm}}-- {{$item.training_id}} - <strong>{{$item.batch_details.cls_start_dt | date : 'dd-MMM-yyyy'}}</strong>\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.pk_id as item in (vm.allUserEmailGroup | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{item.frm_nm}} -- {{item.training_id}} - <strong>{{item.batch_details.cls_start_dt | date : 'dd-MMM-yyyy'}}</strong>\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetFormDetails()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" \n\t\t\t\t\t\t\t\t\tng-disabled=\"!vm.form.users_template || !vm.form.email_template || !vm.form.frm_nm || !vm.form.bat_id || !vm.form.training_id\"\n\t\t\t\t\t\t\t\t\tng-click=\"vm.addSendEmailTemplate()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allSendEmailTemplates || vm.allSendEmailTemplates.length <= 0\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allSendEmailTemplates\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"template in vm.allSendEmailTemplates\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteSendEmailTemplate(template.pk_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editSendEmailTemplate(template.pk_id)\"><i class=\"fa fa-pencil\"></i></span>\t\t\n\t\t\t\t\t\t{{template.frm_nm}} -- {{template.training_id}} - {{template.batch_details.cls_start_dt | date : 'dd-MMM-yyyy'}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"ew-btns-wrapper-flex-end\">\n\t\t\t\t\t<div class=\"ew-rounded-btn\" ng-class=\"{'disable': vm.emailSentInProgress[template.pk_id]}\" ng-click=\"vm.sendEmailTemplatesToUsers(template)\">\n\t\t\t\t\t\t<i class=\"fa fa-envelope right\"></i>\n\t\t\t\t\t\t<span class=\"text\">Send Email</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-rounded-btn\" ng-class=\"{'disable': vm.emailNotificationSentInProgress[template.pk_id]}\" ng-click=\"vm.sendNotificationOfEmail(template)\">\n\t\t\t\t\t\t<i class=\"fa fa-bell right\"></i>\n\t\t\t\t\t\t<span class=\"text\">Notifications</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ew-rounded-btn\">\n\t\t\t\t\t\t<i class=\"fa fa-clock-o right\"></i>\n\t\t\t\t\t\t<span class=\"text\">Schedule Email</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div>\n\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.emailSent[template.pk_id] === false\">Error in sending mails.</div>\n\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.emailSent[template.pk_id] === true\">Emails sent successfully.</div>\n\t\t\t\t</div>\n\t\t\t\t<div>\n\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.emailNotificationSent[template.pk_id] === false\">Error in sending notifications.</div>\n\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.emailNotificationSent[template.pk_id] === true\">Notifications sent successfully.</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in template\">\n\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t<div class=\"col-xs-10 desc\" style=\"word-break: break-word;\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});