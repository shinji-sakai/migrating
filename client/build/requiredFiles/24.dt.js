webpackJsonp([24],{

/***/ 378:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('courseBundlePage', {
	        template: __webpack_require__(379),
	        controller: AdminCourseBundleController,
	        controllerAs: 'vm'
	    });

	AdminCourseBundleController.$inject=['admin','pageLoader','$filter'];

	function AdminCourseBundleController(admin,pageLoader,$filter) {

	    var vm = this;
	    vm.selectedCourses = [];

	    //functions
	    vm.init = init;
	    vm.saveBundle = saveBundle;
	    vm.updateFormDetails = updateFormDetails;
	    vm.removeCourseBundle = removeCourseBundle;
	    vm.resetFormDetails = resetFormDetails;

	    //init data
	    init();
	    function init(){
	        //get coursemaster from db
	        admin.coursemaster()
	        .then(function(d){
	            vm.courses = d;
	        })
	        .catch(function(err){
	            console.error(err);
	        });
	        getBundleList();
	        
	    }

	    //update bundle list locally
	    function getBundleList(){
	        admin.getAllCourseBundle()
	            .then(function(d){
	                vm.bundles = d.data;
	            })
	            .catch(function(err){
	                console.error(err);
	            });
	    }

	    //Save bundle to database
	    function saveBundle(){
	        var data = {
	            bundleId : vm.form.bundleId || undefined,
	            bundleName : vm.form.bundleName,
	            bundleCourses : vm.selectedCourses
	        }

	        pageLoader.show();
	        vm.isSubmitted = true;
	        admin.updateCourseBundle(data)
	            .then(function(r){
	                vm.form.success = true;
	                vm.form.error = null;

	                getBundleList();
	                resetFormDetails();
	            })
	            .catch(function(e){
	                vm.form.success = undefined;                
	                vm.form.error = e;
	            })
	            .finally(function() {
	                pageLoader.hide();
	                vm.isSubmitted = true;
	            })
	    }

	    //reset form details
	    function resetFormDetails(){
	        vm.form = {};
	        vm.selectedCourses = [];
	    }

	    //update form details
	    //this will be called when user click on edit button of bundle
	    function updateFormDetails(id){
	        var bundle = $filter('filter')(vm.bundles,{bundleId : id})[0];
	        vm.form = {};
	        vm.form = bundle;
	        vm.selectedCourses = bundle.bundleCourses;
	    }  

	    //remove bundle from database
	    function removeCourseBundle(id){
	        pageLoader.show();
	        vm.isSubmitted = false;
	        admin.removeCourseBundle({bundleId : id})
	            .then(function(r){
	                getBundleList();
	            })
	            .catch(function(e){
	                vm.form.success = undefined;
	                vm.form.error = e;
	            })
	            .finally(function() {
	                pageLoader.hide();
	                vm.isSubmitted = true;
	            })
	    }
	}

/***/ },

/***/ 379:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Course Bundle</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"bundlename\">Bundle Name</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"bundlename\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.bundleName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"courses\">Bundle Courses</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedCourses\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Course...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.courseId as item in (vm.courses | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetails()\" style=\"margin-top: 25px;\">New Bundle</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.bundleName || vm.selectedCourses.length <= 0\" ng-click=\"vm.saveBundle()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"vm.form.success\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.bundles\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.bundles\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"bundle in vm.bundles\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeCourseBundle(bundle.bundleId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.updateFormDetails(bundle.bundleId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{bundle.bundleName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in bundle\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});