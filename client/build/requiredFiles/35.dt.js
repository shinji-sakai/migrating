webpackJsonp([35],{

/***/ 408:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(409);
	angular.module(window.moduleName)
	    .component('pricelistPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(411),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state'];
	function Controller(admin,$filter,$scope,$state){

		var vm = this;
		vm.form = {};
		vm.form.selectedCourses = [];

		vm.addPriceToList = addPriceToList;
		vm.removePriceFromList = removePriceFromList;
		vm.savePrice = savePrice;
		vm.editPricelist = editPricelist;
		vm.deletePricelist = deletePricelist;
		vm.resetForm  = resetForm;

		vm.$onInit = function(){
			getAllBatchCourse();
			getAllCurrency();
			getAllCoursesFromMaster();
			getAllPricelist();
		}

		function getAllBatchCourse(){
			admin.getAllBatchCourse()
				.then(function(res){
					vm.allBatchCourses = res.data;
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function getAllCoursesFromMaster() {
			admin.coursemaster()
				.then(function(d){
					vm.allCourses = d;
				})
				.catch(function(err){
					console.error(err);
				});
		}

		function getAllCurrency(){
			admin.getAllCurrency()
				.then(function(res){
					vm.allCurrency = res.data
				})
		}

		function getAllPricelist(){
			admin.getAllPricelist()
				.then(function(res){
					vm.allPricelist = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function addPriceToList(){
			vm.form = vm.form || {};
			vm.form.crs_price = vm.form.crs_price || {};
			if(vm.crs_price_curr && vm.crs_price_amount){
				vm.form.crs_price[vm.crs_price_curr] = vm.crs_price_amount;
				vm.crs_price_curr = "";
				vm.crs_price_amount = "";
			}
		}

		function removePriceFromList(key){
			delete vm.form.crs_price[key];
		}

		function savePrice() {
			admin.savePricelist(vm.form)
				.then(function(res){
					getAllPricelist();
					resetForm();
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function deletePricelist(id){
			admin.deletePricelist({crs_id : id})
				.then(function(res){
					getAllPricelist();
					resetForm();
				})
				.catch(function(err){
					console.error(err);
				})	
		}

		function editPricelist(id){
			var pl = $filter('filter')(vm.allPricelist,{crs_id:id})[0];
			vm.form = pl;
		}

		function resetForm(){
			vm.form = {};
			vm.crs_price_curr = "";
			vm.crs_price_amount = "";
		}
	}

/***/ },

/***/ 409:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(410);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_pricelist.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_pricelist.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 410:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, ".question-title {\n  font-family: aller;\n  font-size: 17px;\n  font-weight: bold; }\n\n.question-ans {\n  font-family: aller;\n  font-size: 15px; }\n\n.gray-back {\n  background: #e5e5e5;\n  padding: 0px 10px; }\n\n.remove-btn {\n  position: absolute;\n  right: 5px;\n  top: 5px;\n  font-size: 15px;\n  cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 411:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Pricing</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Batch Training</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.crs_id\" id=\"course\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"bcourse.crs_id as bcourse.crs_id for bcourse in vm.allBatchCourses \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Batch Training</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Training Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Bundle Name\" ng-model=\"vm.form.bndl_nm\" name=\"\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<!-- <div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" >Include Courses</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.form.selectedCourses\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Course...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.courseId as item in (vm.allCourses | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"about\">Price</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.crs_price_curr\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Currency</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"curr in vm.allCurrency track by $index\" value=\"{{curr.currency_nm}}\">{{curr.currency_nm}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Amount\" name=\"amount\" ng-model=\"vm.crs_price_amount\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t\t<span class=\"btn btn-primary\" ng-click=\"vm.addPriceToList()\">Add Price</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-repeat=\"(price,value) in vm.form.crs_price track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"about\">Price {{price}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<span class=\"question-title\">{{value}}</span>\n\t\t\t\t\t\t\t\t<div class=\"remove-btn\" ng-click=\"vm.removePriceFromList(price)\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" >Msg</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<textarea type=\"text\" class=\"form-control\" placeholder=\"Msg\" ng-model=\"vm.form.msg\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.savePrice()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allPricelist\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allPricelist\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"pricelist in vm.allPricelist\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editPricelist(pricelist.crs_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deletePricelist(pricelist.crs_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t{{pricelist.bndl_nm}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in pricelist\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});