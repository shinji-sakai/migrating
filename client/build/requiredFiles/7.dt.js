webpackJsonp([7],{

/***/ 175:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('demoVideosPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(176),
	            controller: AdminDemoVideosController,
	            controllerAs: 'vm'
	        });
	    AdminDemoVideosController.$inject=['admin','pageLoader','$filter','$scope'];
		function AdminDemoVideosController(admin,pageLoader,$filter,$scope) {

			var vm = this;

			vm.$onInit = function() {
	        	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
	        		vm.courselist = v;
	        	});
		    };

			//////////////
			//Variables //
			//////////////
			vm.formError = false;
			vm.formSuccess = false;

			//////////////
			//Functions //
			//////////////
			vm.changeCourse = changeCourse;
			vm.changeVideoType = changeVideoType;
			vm.makeAllPaid = makeAllPaid;
			vm.isItemDemo = isItemDemo;
			vm.isItemDemoLogin = isItemDemoLogin;
			vm.isItemPaid = isItemPaid;


			//////////////////
			// Declarations //
			//////////////////
			function changeCourse(){
				pageLoader.show();

				admin.getCourseItems({'courseId':vm.form.course})
					.then(function(res){
						var tempData = res[0];
						getAuthorizedItems()
							.then(function(){
								vm.courseVideos = tempData;
								//Sort Module Based On ModuleNumber
								vm.courseVideos.moduleDetail.sort(function(a,b){return a.moduleNumber - b.moduleNumber});
								//Sort Videos Based On VideoNumber
								vm.courseVideos.moduleDetail.map(function(v){v.moduleItems.sort(function(a,b){return a.itemWeight - b.itemWeight;});return v;});
							});
						
					})
					.finally(function(){
						pageLoader.hide();
					})
			}

			function getAuthorizedItems(){
				return admin.getAuthorizedItems({courseId:vm.form.course})
					.then(function(res){
						vm.authorizedItems = res.data;
						vm.authorizedIdTypeMap = {};
						vm.authorizedItems.map(function(v){
							vm.authorizedIdTypeMap[v.itemId] = v.itemType;	
						});
					})
					.catch(function(err){
						console.error(err);
					})
			}

			function isItemDemo(id){
				return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'demo');
			}
			function isItemDemoLogin(id){
				return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'demologin');
			}
			function isItemPaid(id){
				return (vm.authorizedIdTypeMap[id] && vm.authorizedIdTypeMap[id] === 'paid');
			}

			function makeAllPaid(){
				var allVideosArr = [];
				var courseObj = $filter('filter')(vm.courselist,{courseId : vm.form.course})[0];

				vm.courseVideos.moduleDetail.map(function(v){
					v.moduleItems.map(function(item){
						var obj = {
							courseId : vm.form.course,
							courseName : courseObj["courseName"],
							moduleId : v.moduleId,
							moduleName : v.moduleName,
							itemId : item.itemId,
							itemVideo : item.itemVideo,
							itemName : item.itemName,
							itemType : 'paid',
							lastUpdated : new Date()
						}
						allVideosArr.push(obj);
					});
				});
				updateAuthorizedItems(allVideosArr);
			}

			function changeVideoType(moduleId,moduleName,itemId,itemName,itemType,itemVideo){
				var allVideosArr = [];

				var courseObj = $filter('filter')(vm.courselist,{courseId : vm.form.course})[0];

				var obj = {
					courseId : vm.form.course,
					courseName : courseObj["courseName"],
					moduleId : moduleId,
					moduleName : moduleName,
					itemId : itemId,
					itemVideo : itemVideo,
					itemName : itemName,
					itemType : itemType,
					lastUpdated : new Date()
				}
				allVideosArr.push(obj);
				updateAuthorizedItems(allVideosArr);
			}
	    	
	    	function updateAuthorizedItems(videosArr){
	    		admin.updateAuthorizedItems(videosArr)
					.then(function(){
						vm.formSuccess = true;
						vm.formError = false;
						getAuthorizedItems();
					})
					.catch(function(err){
						vm.formError = true;
						vm.formSuccess = false;
					})
	    	}
	    }

/***/ },

/***/ 176:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Full Course Detail</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"course\">Course</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"course\"  ng-model=\"vm.form.course\" ng-change=\"vm.changeCourse()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='course.courseId as course.courseName for course in vm.courselist' required>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Course</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.courseVideos\">No Data to display</div>\n\t\t\t\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.courseVideos.moduleDetail\">\n\t\t\t\t<div>\n\t\t\t\t\t<span class=\"alert alert-success\" ng-if=\"vm.formSuccess\">Item(s) Changed Successfully.</span>\n\t\t\t\t\t<span class=\"alert alert-success\" ng-if=\"vm.formError\">Error in updating item(s).</span>\n\t\t\t\t\t<button class=\"btn btn-primary pull-right\" ng-click=\"vm.makeAllPaid()\">Make All Paid</button>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"clearfix\" style=\"margin-bottom: 20px;\"></div>\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"module in vm.courseVideos.moduleDetail\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t{{module.moduleNumber}} - {{module.moduleName}}\n\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"demovideo-list-row\" ng-repeat=\"item in module.moduleItems\">\n\t\t\t\t\t\t<span>{{item.itemName}}</span>\n\t\t\t\t\t\t<span uib-dropdown class=\"pull-right\">\n\t\t\t\t\t\t\t<button uib-dropdown-toggle class=\"btn btn-primary btn-sm\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t<ul uib-dropdown-menu aria-labelledby=\"simple-dropdown\">\n\t\t\t\t\t\t\t\t<li ng-click=\"vm.changeVideoType(module.moduleId,module.moduleName,item.itemId,item.itemName,'demo',item.itemVideo)\">\n\t\t\t\t\t\t\t\t\t<span class=\"fa-stack fa-sm\" style=\"color:green\">\n\t\t\t\t\t\t\t\t\t  <i class=\"fa fa-square-o fa-stack-2x\"></i>\n\t\t\t\t\t\t\t\t\t  <i ng-if=\"vm.isItemDemo(item.itemId)\" class=\"fa fa-check fa-stack-1x\" ></i>\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\tDemo\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li ng-click=\"vm.changeVideoType(module.moduleId,module.moduleName,item.itemId,item.itemName,'demologin',item.itemVideo)\">\n\t\t\t\t\t\t\t\t\t<span class=\"fa-stack fa-sm\" style=\"color:green\">\n\t\t\t\t\t\t\t\t\t  <i class=\"fa fa-square-o fa-stack-2x\"></i>\n\t\t\t\t\t\t\t\t\t  <i ng-if=\"vm.isItemDemoLogin(item.itemId)\" class=\"fa fa-check fa-stack-1x\" ></i>\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\tDemo Login\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li ng-click=\"vm.changeVideoType(module.moduleId,module.moduleName,item.itemId,item.itemName,'paid',item.itemVideo)\">\n\t\t\t\t\t\t\t\t\t<span class=\"fa-stack fa-sm\" style=\"color:green\">\n\t\t\t\t\t\t\t\t\t  <i class=\"fa fa-square-o fa-stack-2x\"></i>\n\t\t\t\t\t\t\t\t\t  <i ng-if=\"vm.isItemPaid(item.itemId)\" class=\"fa fa-check fa-stack-1x\" ></i>\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\tPaid\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</span>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});