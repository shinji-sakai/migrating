webpackJsonp([58],{

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($, hljs) {__webpack_require__(57);
	__webpack_require__(127);

	angular.module("service")
	    .service('utility', Utility);


	Utility.$inject = ['authToken','$sce','$http','BASE_API'];

	function Utility(authToken,$sce,$http,BASE_API) {

	    var self = this;

	    self.BASE_URL = BASE_API;
	    self.QA_QUESTION_ID_URL_LENGTH = 300;

	    self.goToLogin = goToLogin;
	    self.getLoginUrl = getLoginUrl;
	    self.goToLogout = goToLogout;
	    self.getLogoutUrl = getLogoutUrl;
	    self.goToRegister = goToRegister;
	    self.getRegisterUrl = getRegisterUrl;
	    self.goToChangePassword = goToChangePassword;
	    self.goToForgotPassword = goToForgotPassword;
	    self.getForgotPasswordUrl = getForgotPasswordUrl;
	    self.goToAdminApp = goToAdminApp;
	    self.goToDashboardApp = goToDashboardApp;
	    self.goToPracticePage = goToPracticePage;

	    self.goToCareerBook = goToCareerBook;
	    self.getCareerBookUrl = getCareerBookUrl;

	    self.goToQA = goToQA;
	    self.getQAUrl = getQAUrl;
	    self.getQAQuestionPageUrl = getQAQuestionPageUrl;
	    self.getQAAnswerPageUrl = getQAAnswerPageUrl;

	    self.goToWhiteboard = goToWhiteboard;
	    self.getWhiteboardUrl = getWhiteboardUrl;

	    self.goToDashboard = goToDashboard;
	    self.getDashboardUrl = getDashboardUrl;

	    self.goToBooks = goToBooks;
	    self.getBooksUrl = getBooksUrl;

	    self.getUserProfileUrl = getUserProfileUrl;
	    self.goToUserProfile = goToUserProfile;

	    self.getSitemapUrl = getSitemapUrl;

	    self.getITTrainingsUrl = getITTrainingsUrl;

	    self.getNotificationsPageUrl = getNotificationsPageUrl;
	    self.getSpecificNotificationPageUrl = getSpecificNotificationPageUrl;

	    self.doSyntaxHighlighting = doSyntaxHighlighting;
	    self.getLanguageForSyntaxHighlighting = getLanguageForSyntaxHighlighting;
	    self.changeTimeFormat = changeTimeFormat;
	    self.trustAsHTML = trustAsHTML;
	    self.trustAsHTMLReturnPlainText = trustAsHTMLReturnPlainText;
	    self.shareToEWProfile = shareToEWProfile;trustAsHTMLReturnPlainText;
	    self.shareToQA = shareToQA;
	    self.openPopUp = openPopUp;

	    self.buyPage = buyPage;
	    self.buyCoursePage = buyCoursePage;
	    self.getBuyCoursePageURL = getBuyCoursePageURL;
	    self.buyPaymentPage = buyPaymentPage;
	    self.buyCoursePricePage = buyCoursePricePage;
	    self.getCoursePricePageURL = getCoursePricePageURL;

	    self.getVideoPageURL = getVideoPageURL;
	    self.getQuestionPageURL = getQuestionPageURL;



	    self.getTrainingPageUrl = getTrainingPageUrl;

	    self.getNextTrainingDay = getNextTrainingDay;
	    self.getCourseDetailsPageUrl = getCourseDetailsPageUrl;

	    self.getAdminPracticePageUrl = getAdminPracticePageUrl;

	    self.convertTimeTo_AM_PM = convertTimeTo_AM_PM;



	    self.submitQuestionStats = function(data){
	        return $http.post(BASE_API + '/userstats', data);
	    }

	    function goToLogin(){
	        authToken.cachedUrl(window.location.href);
	        window.location = '/login';
	    }

	    function getLoginUrl(){
	        return '/login';
	    }

	    function goToRegister(){
	        authToken.cachedUrl(window.location.href);
	        window.location = '/register';
	    }

	    function getRegisterUrl(){
	        return '/register';
	    }

	    function goToLogout(){
	        window.location = '/logout';
	    }

	    function  getLogoutUrl() {
	        return '/logout';
	    }

	    function goToChangePassword(code) {
	        if(code){
	            window.location = '/changepassword/' + code;
	        }else{
	            window.location = '/changepassword/';
	        }
	    }

	    function goToForgotPassword() {
	        window.location = '/forgotpassword';
	    }

	    function getForgotPasswordUrl(){
	        return '/forgotpassword';
	    }


	    function goToAdminApp() {
	        window.location = '/adminApp/';
	    }

	    function goToDashboardApp() {
	        window.location = '/dashboard/';
	    }


	    function buyPage() {
	        return self.BASE_URL + '/buy/'
	    }
	    function buyCoursePricePage(crs_id) {
	        return self.BASE_URL + '/buy/price/' + crs_id;
	    }
	    function getCoursePricePageURL(shouldRedirect,crs_id) {
	        var url = self.BASE_URL + '/buy/price/' + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }
	    function buyCoursePage(crs_id) {
	        return self.BASE_URL + '/buy/course/' + crs_id;
	    }
	    function getBuyCoursePageURL(shouldRedirect,crs_id) {
	        var url = self.BASE_URL + '/buy/course/' + crs_id;;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }
	    function buyPaymentPage(crs_id,bat_id) {
	        return self.BASE_URL + '/buy/payment?course=' + crs_id + '&bat_id=' + bat_id ;
	    }

	    function goToPracticePage(courseId,moduleId,itemId){
	        var url = "/" + courseId + "/video/" + itemId;
	        // var url = "/app/practice?module="+ moduleId + "&course=" + courseId + "&item=" + itemId;
	        // window.location.href = url;
	        var win = window.open(url, '_blank');
	        win.focus();
	    }

	    /////////////////////

	    function goToQA(){
	        window.location = '/question-answers';
	    }
	    function getQAUrl() {
	        return '/question-answers';
	    }

	    function getQAQuestionPageUrl(shouldRedirect,que_id,que_text){
	        var url = '/question-answers/question/' + que_id + '/' + (que_text ? que_text : '');
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    function getQAAnswerPageUrl(shouldRedirect,que_id,ans_id,que_text){
	        var url = '/question-answers/question/' + que_id + "/" + (que_text || "text") +  "/answer/" + ans_id;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    /////////////////////

	    function goToCareerBook(){
	        window.location = '/career-book/';
	    }
	    function getCareerBookUrl(){
	        return '/career-book/';
	    }

	    self.getCareerBookProfileUrl = function(usr_id){
	        return '/career-book/' + usr_id;
	    }

	    /////////////////////

	    function goToWhiteboard(){
	        window.location = '/whiteboard/';
	    }
	    function getWhiteboardUrl(){
	        return '/whiteboard/';
	    }

	    /////////////////////

	    function goToBooks(){
	        window.location = '/books/';
	    }
	    function getBooksUrl(){
	        return '/books/';
	    }

	    /////////////////////


	    function goToDashboard(){
	        window.location = '/dashboard/';
	    }
	    function getDashboardUrl(){
	        return '/dashboard/';
	    }

	    /////////////////////


	    function getUserProfileUrl(){
	        return '/user-profile';
	    }
	    function goToUserProfile(){
	        window.location = '/user-profile';
	    }

	    /////////////////////


	    function getSitemapUrl(shouldRedirect){
	        var url = '/sitemap';
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    /////////////////////

	    function getVideoPageURL(courseId,itemId,time){
	        if(time){
	            return self.BASE_URL + '/' + courseId + '/video/' + itemId + '?time=' + time;
	        }else{
	            return self.BASE_URL + '/' + courseId + '/video/' + itemId;
	        }
	    }

	    function getQuestionPageURL(courseId,itemId,question){
	        if(question){
	            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/' + question;
	        }else{
	            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/';
	        }
	    }

	    self.getBookPageURL = getBookPageURL;
	    function getBookPageURL(courseId,itemId){
	        return self.BASE_URL + '/' + courseId + '/book-topic/' + itemId;
	    }

	    function getITTrainingsUrl(shouldRedirect){
	        var url = "/it-trainings";
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getCourseDetailsPageUrl(shouldRedirect,type,subgroup,crs_id){
	        // var url = `${type}/${subgroup}-trainings/${crs_id}`;
	        var url = "/" + type + "/" + subgroup + "-trainings/" + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getNotificationsPageUrl(shouldRedirect,usr_id){
	        var url = "/notifications";
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getSpecificNotificationPageUrl(shouldRedirect,usr_id,notification_id){
	        var url = "/notifications/" + notification_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getTrainingPageUrl(shouldRedirect,crs_id){
	        var url = "/training/" + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getAdminPracticePageUrl(shouldRedirect,q_id){
	        var url = "/adminApp/practice/" + q_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    //do highlighting
	    function doSyntaxHighlighting() {
	        var x = 0;
	        var intervalID = setInterval(function() {
	            // Prism.highlightAll();
	            $('pre code').each(function(i, block) {
	                hljs.highlightBlock(block);
	            });
	            $('.syntax').each(function(i, block) {
	                hljs.highlightBlock(block);
	            });
	            if (++x === 10) {
	                window.clearInterval(intervalID);
	            }
	        }, 1);
	    }

	    //Get language for syntax highlighting
	    function getLanguageForSyntaxHighlighting(coursename){
	        if(coursename.toLowerCase().indexOf("javascript") > -1){
	            return "javascript";
	        }else if(coursename.toLowerCase().indexOf("java") > -1){
	            return "java";
	        }else if(coursename.toLowerCase().indexOf("sql") > -1){
	            return "sql";
	        }
	    }

	    var vm = this;
	    function getNextTrainingDay(frm_wk_dy,to_wk_dy,cls_start_dt,bat_frm_tm_hr,bat_frm_tm_min) {
	        vm.daysOfWeek = ["Monday", 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

	        var training_days = [];
	        var start_counting = false;
	        for (var i = 0; i < vm.daysOfWeek.length; i++) {
	            var d = vm.daysOfWeek[i];
	            if (start_counting) {
	                training_days.push(d);
	                if (d === to_wk_dy) {
	                    start_counting = false;
	                    break;
	                }
	            }
	            if (d === frm_wk_dy) {
	                start_counting = true;
	                training_days.push(d);
	            }
	        }

	        var today_name = getDayName();
	        var today_date = new Date();
	        var next_training_date = "";
	        bat_frm_tm_hr = parseInt(bat_frm_tm_hr);
	        bat_frm_tm_min = parseInt(bat_frm_tm_min);
	        if(today_date < new Date(cls_start_dt)){
	            next_training_date = new Date(cls_start_dt);
	            // console.log("next date is start date.")
	        }else if (training_days.indexOf(today_name) > -1) {
	            //check if today is in training_days
	            var current_hour = today_date.getHours();
	            var current_min = today_date.getMinutes();
	            // console.log(current_hour,bat_frm_tm_hr,current_min,bat_frm_tm_min)
	            if (current_hour > bat_frm_tm_hr) {
	                // console.log("time is exceeded , find next day");
	                var daysToGo = nextTrainingDay(training_days)
	                // console.log("daysToGo", daysToGo);
	                var today = new Date();
	                next_training_date = new Date(today);
	                next_training_date.setDate(today.getDate() + daysToGo);
	            }else if(current_hour === bat_frm_tm_hr && current_min > bat_frm_tm_min) {
	                // console.log("time is exceeded , find next day");
	                var daysToGo = nextTrainingDay(training_days)
	                // console.log("daysToGo", daysToGo);
	                var today = new Date();
	                next_training_date = new Date(today);
	                next_training_date.setDate(today.getDate() + daysToGo);
	            }else {
	                // console.log("today is the next batch day");
	                next_training_date = new Date();
	            }
	        } else {
	            //if today is not in training_days
	            // console.log("today is not training_days , find next day");
	            var daysToGo = nextTrainingDay(training_days);
	            // console.log("daysToGo", daysToGo);
	            var today = new Date();
	            next_training_date = new Date(today);
	            next_training_date.setDate(today.getDate() + daysToGo);
	        }
	        return next_training_date;
	    }

	    function getDayName(dateString) {
	        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	        if (!dateString) {
	            dateString = (new Date()).toString();
	        }
	        var d = new Date(dateString);
	        var dayName = days[d.getDay()];
	        return dayName;
	    }

	    function nextTrainingDay(training_days) {
	        var today_name = getDayName();
	        var today_day_index = vm.daysOfWeek.indexOf(today_name);
	        var day_index = today_day_index + 1;
	        var daysToGo = 1;
	        var nextDayFound = false;
	        while (!nextDayFound) {
	            var day = vm.daysOfWeek[day_index];
	            if (training_days.indexOf(day) > -1) {
	                nextDayFound = true;
	            } else {
	                day_index = (day_index + 1) % 7;
	                daysToGo++;
	                nextDayFound = false;
	            }
	        }
	        return daysToGo;
	    }

	    function changeTimeFormat(sec) {
	        var time = sec || 0;
	        var hours = parseInt(time / 3600) % 24;
	        var minutes = parseInt(time / 60) % 60;
	        var seconds = parseInt(time % 60);

	        hours = ("0" + hours).slice(-2);
	        minutes = ("0" + minutes).slice(-2);
	        seconds = ("0" + seconds).slice(-2);

	        var ret_time;
	        if (hours == "00") {
	            ret_time = minutes + ":" + seconds;
	        } else {
	            ret_time = hours + ":" + minutes + ":" + seconds;
	        }

	        return ret_time;
	    }

	    function convertTimeTo_AM_PM(time) {
	        // time = 10:10
	        time = time || "00:00";
	        var split_time = time.split(":");
	        var hour = parseInt(split_time[0]);
	        if(hour < 12){
	            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " AM";
	        }else if(hour === 12){
	            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
	        }else if(hour > 12){
	            return ('00' + (split_time[0] - 12)).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
	        }
	    }

	    function trustAsHTML(html){
	        return $sce.trustAsHtml(html);
	    }

	    function trustAsHTMLReturnPlainText(html){
	        var html2 = html.replace(/(<([^>]+)>)/g, "");
	        return $sce.trustAsHtml(html2);
	    }

	    function shareToEWProfile(title,desc,url){
	        if(!url){
	            url = encodeURIComponent(window.location.href);
	        }else{
	            url = encodeURIComponent(url);
	        }
	        var w = 500;
	        var h = 560;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = window.location.origin + '/app/share?type=cb&title=' + title + "&url=" + url + "&desc=" + (desc || '');
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    function shareToQA(title,desc,url){
	        if(!url){
	            url = encodeURIComponent(window.location.href);
	        }else{
	            url = encodeURIComponent(url);
	        }
	        var w = 500;
	        var h = 560;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = window.location.origin + '/app/share?type=qa&title=' + title + "&url=" + url + "&desc=" + (desc || '');
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    function openPopUp(url){
	        var w = 500;
	        var h = 500;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = url;
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    self.getSelectionText = getSelectionText;
	    function getSelectionText() {
	        var text = "";
	        if (window.getSelection) {
	            text = window.getSelection().toString();
	        } else if (document.selection && document.selection.type != "Control") {
	            text = document.selection.createRange().text;
	        }
	        return text;
	    }

	    self.getPlainQuestionIdText = function(text){
	        var text = $("<div></div>").html(text).text().split(" ").join("-").toLowerCase();
	        text = text.substring(0,self.QA_QUESTION_ID_URL_LENGTH);
	        text = text.replace(/[^a-zA-Z0-9-]/g, "")
	        return text
	    }
	};

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1), __webpack_require__(126)))

/***/ },

/***/ 126:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(hljs) {/*! highlight.js v9.5.0 | BSD3 License | git.io/hljslicense */ ! function(e) {
	    var n = "object" == typeof window && window || "object" == typeof self && self;
	     true ? e(exports) : n && (n.hljs = e({}), "function" == typeof define && define.amd && define([], function() {
	        return n.hljs
	    }))
	}(function(e) {
	    function n(e) {
	        return e.replace(/[&<>]/gm, function(e) {
	            return I[e]
	        })
	    }

	    function t(e) {
	        return e.nodeName.toLowerCase()
	    }

	    function r(e, n) {
	        var t = e && e.exec(n);
	        return t && 0 === t.index
	    }

	    function a(e) {
	        return k.test(e)
	    }

	    function i(e) {
	        var n, t, r, i, o = e.className + " ";
	        if (o += e.parentNode ? e.parentNode.className : "", t = B.exec(o)) return R(t[1]) ? t[1] : "no-highlight";
	        for (o = o.split(/\s+/), n = 0, r = o.length; r > n; n++)
	            if (i = o[n], a(i) || R(i)) return i
	    }

	    function o(e, n) {
	        var t, r = {};
	        for (t in e) r[t] = e[t];
	        if (n)
	            for (t in n) r[t] = n[t];
	        return r
	    }

	    function u(e) {
	        var n = [];
	        return function r(e, a) {
	            for (var i = e.firstChild; i; i = i.nextSibling) 3 === i.nodeType ? a += i.nodeValue.length : 1 === i.nodeType && (n.push({
	                event: "start",
	                offset: a,
	                node: i
	            }), a = r(i, a), t(i).match(/br|hr|img|input/) || n.push({
	                event: "stop",
	                offset: a,
	                node: i
	            }));
	            return a
	        }(e, 0), n
	    }

	    function c(e, r, a) {
	        function i() {
	            return e.length && r.length ? e[0].offset !== r[0].offset ? e[0].offset < r[0].offset ? e : r : "start" === r[0].event ? e : r : e.length ? e : r
	        }

	        function o(e) {
	            function r(e) {
	                return " " + e.nodeName + '="' + n(e.value) + '"'
	            }
	            l += "<" + t(e) + w.map.call(e.attributes, r).join("") + ">"
	        }

	        function u(e) {
	            l += "</" + t(e) + ">"
	        }

	        function c(e) {
	            ("start" === e.event ? o : u)(e.node)
	        }
	        for (var s = 0, l = "", f = []; e.length || r.length;) {
	            var g = i();
	            if (l += n(a.substr(s, g[0].offset - s)), s = g[0].offset, g === e) {
	                f.reverse().forEach(u);
	                do c(g.splice(0, 1)[0]), g = i(); while (g === e && g.length && g[0].offset === s);
	                f.reverse().forEach(o)
	            } else "start" === g[0].event ? f.push(g[0].node) : f.pop(), c(g.splice(0, 1)[0])
	        }
	        return l + n(a.substr(s))
	    }

	    function s(e) {
	        function n(e) {
	            return e && e.source || e
	        }

	        function t(t, r) {
	            return new RegExp(n(t), "m" + (e.cI ? "i" : "") + (r ? "g" : ""))
	        }

	        function r(a, i) {
	            if (!a.compiled) {
	                if (a.compiled = !0, a.k = a.k || a.bK, a.k) {
	                    var u = {},
	                        c = function(n, t) {
	                            e.cI && (t = t.toLowerCase()), t.split(" ").forEach(function(e) {
	                                var t = e.split("|");
	                                u[t[0]] = [n, t[1] ? Number(t[1]) : 1]
	                            })
	                        };
	                    "string" == typeof a.k ? c("keyword", a.k) : E(a.k).forEach(function(e) {
	                        c(e, a.k[e])
	                    }), a.k = u
	                }
	                a.lR = t(a.l || /\w+/, !0), i && (a.bK && (a.b = "\\b(" + a.bK.split(" ").join("|") + ")\\b"), a.b || (a.b = /\B|\b/), a.bR = t(a.b), a.e || a.eW || (a.e = /\B|\b/), a.e && (a.eR = t(a.e)), a.tE = n(a.e) || "", a.eW && i.tE && (a.tE += (a.e ? "|" : "") + i.tE)), a.i && (a.iR = t(a.i)), null == a.r && (a.r = 1), a.c || (a.c = []);
	                var s = [];
	                a.c.forEach(function(e) {
	                    e.v ? e.v.forEach(function(n) {
	                        s.push(o(e, n))
	                    }) : s.push("self" === e ? a : e)
	                }), a.c = s, a.c.forEach(function(e) {
	                    r(e, a)
	                }), a.starts && r(a.starts, i);
	                var l = a.c.map(function(e) {
	                    return e.bK ? "\\.?(" + e.b + ")\\.?" : e.b
	                }).concat([a.tE, a.i]).map(n).filter(Boolean);
	                a.t = l.length ? t(l.join("|"), !0) : {
	                    exec: function() {
	                        return null
	                    }
	                }
	            }
	        }
	        r(e)
	    }

	    function l(e, t, a, i) {
	        function o(e, n) {
	            for (var t = 0; t < n.c.length; t++)
	                if (r(n.c[t].bR, e)) return n.c[t]
	        }

	        function u(e, n) {
	            if (r(e.eR, n)) {
	                for (; e.endsParent && e.parent;) e = e.parent;
	                return e
	            }
	            return e.eW ? u(e.parent, n) : void 0
	        }

	        function c(e, n) {
	            return !a && r(n.iR, e)
	        }

	        function g(e, n) {
	            var t = N.cI ? n[0].toLowerCase() : n[0];
	            return e.k.hasOwnProperty(t) && e.k[t]
	        }

	        function h(e, n, t, r) {
	            var a = r ? "" : y.classPrefix,
	                i = '<span class="' + a,
	                o = t ? "" : C;
	            return i += e + '">', i + n + o
	        }

	        function p() {
	            var e, t, r, a;
	            if (!E.k) return n(B);
	            for (a = "", t = 0, E.lR.lastIndex = 0, r = E.lR.exec(B); r;) a += n(B.substr(t, r.index - t)), e = g(E, r), e ? (M += e[1], a += h(e[0], n(r[0]))) : a += n(r[0]), t = E.lR.lastIndex, r = E.lR.exec(B);
	            return a + n(B.substr(t))
	        }

	        function d() {
	            var e = "string" == typeof E.sL;
	            if (e && !x[E.sL]) return n(B);
	            var t = e ? l(E.sL, B, !0, L[E.sL]) : f(B, E.sL.length ? E.sL : void 0);
	            return E.r > 0 && (M += t.r), e && (L[E.sL] = t.top), h(t.language, t.value, !1, !0)
	        }

	        function b() {
	            k += null != E.sL ? d() : p(), B = ""
	        }

	        function v(e) {
	            k += e.cN ? h(e.cN, "", !0) : "", E = Object.create(e, {
	                parent: {
	                    value: E
	                }
	            })
	        }

	        function m(e, n) {
	            if (B += e, null == n) return b(), 0;
	            var t = o(n, E);
	            if (t) return t.skip ? B += n : (t.eB && (B += n), b(), t.rB || t.eB || (B = n)), v(t, n), t.rB ? 0 : n.length;
	            var r = u(E, n);
	            if (r) {
	                var a = E;
	                a.skip ? B += n : (a.rE || a.eE || (B += n), b(), a.eE && (B = n));
	                do E.cN && (k += C), E.skip || (M += E.r), E = E.parent; while (E !== r.parent);
	                return r.starts && v(r.starts, ""), a.rE ? 0 : n.length
	            }
	            if (c(n, E)) throw new Error('Illegal lexeme "' + n + '" for mode "' + (E.cN || "<unnamed>") + '"');
	            return B += n, n.length || 1
	        }
	        var N = R(e);
	        if (!N) throw new Error('Unknown language: "' + e + '"');
	        s(N);
	        var w, E = i || N,
	            L = {},
	            k = "";
	        for (w = E; w !== N; w = w.parent) w.cN && (k = h(w.cN, "", !0) + k);
	        var B = "",
	            M = 0;
	        try {
	            for (var I, j, O = 0;;) {
	                if (E.t.lastIndex = O, I = E.t.exec(t), !I) break;
	                j = m(t.substr(O, I.index - O), I[0]), O = I.index + j
	            }
	            for (m(t.substr(O)), w = E; w.parent; w = w.parent) w.cN && (k += C);
	            return {
	                r: M,
	                value: k,
	                language: e,
	                top: E
	            }
	        } catch (T) {
	            if (T.message && -1 !== T.message.indexOf("Illegal")) return {
	                r: 0,
	                value: n(t)
	            };
	            throw T
	        }
	    }

	    function f(e, t) {
	        t = t || y.languages || E(x);
	        var r = {
	                r: 0,
	                value: n(e)
	            },
	            a = r;
	        return t.filter(R).forEach(function(n) {
	            var t = l(n, e, !1);
	            t.language = n, t.r > a.r && (a = t), t.r > r.r && (a = r, r = t)
	        }), a.language && (r.second_best = a), r
	    }

	    function g(e) {
	        return y.tabReplace || y.useBR ? e.replace(M, function(e, n) {
	            return y.useBR && "\n" === e ? "<br>" : y.tabReplace ? n.replace(/\t/g, y.tabReplace) : void 0
	        }) : e
	    }

	    function h(e, n, t) {
	        var r = n ? L[n] : t,
	            a = [e.trim()];
	        return e.match(/\bhljs\b/) || a.push("hljs"), -1 === e.indexOf(r) && a.push(r), a.join(" ").trim()
	    }

	    function p(e) {
	        var n, t, r, o, s, p = i(e);
	        a(p) || (y.useBR ? (n = document.createElementNS("http://www.w3.org/1999/xhtml", "div"), n.innerHTML = e.innerHTML.replace(/\n/g, "").replace(/<br[ \/]*>/g, "\n")) : n = e, s = n.textContent, r = p ? l(p, s, !0) : f(s), t = u(n), t.length && (o = document.createElementNS("http://www.w3.org/1999/xhtml", "div"), o.innerHTML = r.value, r.value = c(t, u(o), s)), r.value = g(r.value), e.innerHTML = r.value, e.className = h(e.className, p, r.language), e.result = {
	            language: r.language,
	            re: r.r
	        }, r.second_best && (e.second_best = {
	            language: r.second_best.language,
	            re: r.second_best.r
	        }))
	    }

	    function d(e) {
	        y = o(y, e)
	    }

	    function b() {
	        if (!b.called) {
	            b.called = !0;
	            var e = document.querySelectorAll("pre code");
	            w.forEach.call(e, p)
	        }
	    }

	    function v() {
	        addEventListener("DOMContentLoaded", b, !1), addEventListener("load", b, !1)
	    }

	    function m(n, t) {
	        var r = x[n] = t(e);
	        r.aliases && r.aliases.forEach(function(e) {
	            L[e] = n
	        })
	    }

	    function N() {
	        return E(x)
	    }

	    function R(e) {
	        return e = (e || "").toLowerCase(), x[e] || x[L[e]]
	    }
	    var w = [],
	        E = Object.keys,
	        x = {},
	        L = {},
	        k = /^(no-?highlight|plain|text)$/i,
	        B = /\blang(?:uage)?-([\w-]+)\b/i,
	        M = /((^(<[^>]+>|\t|)+|(?:\n)))/gm,
	        C = "</span>",
	        y = {
	            classPrefix: "hljs-",
	            tabReplace: null,
	            useBR: !1,
	            languages: void 0
	        },
	        I = {
	            "&": "&amp;",
	            "<": "&lt;",
	            ">": "&gt;"
	        };
	    return e.highlight = l, e.highlightAuto = f, e.fixMarkup = g, e.highlightBlock = p, e.configure = d, e.initHighlighting = b, e.initHighlightingOnLoad = v, e.registerLanguage = m, e.listLanguages = N, e.getLanguage = R, e.inherit = o, e.IR = "[a-zA-Z]\\w*", e.UIR = "[a-zA-Z_]\\w*", e.NR = "\\b\\d+(\\.\\d+)?", e.CNR = "(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", e.BNR = "\\b(0b[01]+)", e.RSR = "!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~", e.BE = {
	        b: "\\\\[\\s\\S]",
	        r: 0
	    }, e.ASM = {
	        cN: "string",
	        b: "'",
	        e: "'",
	        i: "\\n",
	        c: [e.BE]
	    }, e.QSM = {
	        cN: "string",
	        b: '"',
	        e: '"',
	        i: "\\n",
	        c: [e.BE]
	    }, e.PWM = {
	        b: /\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|like)\b/
	    }, e.C = function(n, t, r) {
	        var a = e.inherit({
	            cN: "comment",
	            b: n,
	            e: t,
	            c: []
	        }, r || {});
	        return a.c.push(e.PWM), a.c.push({
	            cN: "doctag",
	            b: "(?:TODO|FIXME|NOTE|BUG|XXX):",
	            r: 0
	        }), a
	    }, e.CLCM = e.C("//", "$"), e.CBCM = e.C("/\\*", "\\*/"), e.HCM = e.C("#", "$"), e.NM = {
	        cN: "number",
	        b: e.NR,
	        r: 0
	    }, e.CNM = {
	        cN: "number",
	        b: e.CNR,
	        r: 0
	    }, e.BNM = {
	        cN: "number",
	        b: e.BNR,
	        r: 0
	    }, e.CSSNM = {
	        cN: "number",
	        b: e.NR + "(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?",
	        r: 0
	    }, e.RM = {
	        cN: "regexp",
	        b: /\//,
	        e: /\/[gimuy]*/,
	        i: /\n/,
	        c: [e.BE, {
	            b: /\[/,
	            e: /\]/,
	            r: 0,
	            c: [e.BE]
	        }]
	    }, e.TM = {
	        cN: "title",
	        b: e.IR,
	        r: 0
	    }, e.UTM = {
	        cN: "title",
	        b: e.UIR,
	        r: 0
	    }, e.METHOD_GUARD = {
	        b: "\\.\\s*" + e.UIR,
	        r: 0
	    }, e
	});
	hljs.registerLanguage("ruleslanguage", function(T) {
	    return {
	        k: {
	            keyword: 'ABORT ACCEPT ACCESS ADD ADMIN AFTER ALLOCATE ALTER ANALYZE ARCHIVE ARCHIVELOG ' +
	                'ARRAY ARRAYLEN AS ASC ASSERT ASSIGN AT AUDIT AUTHORIZATION BACKUP BASE_TABLE ' +
	                'BECOME BEFORE BEGIN BINARY_INTEGER BLOCK BODY BOOLEAN BY CACHE CANCEL CASCADE ' +
	                'CASE CHANGE CHAR CHARACTER CHAR_BASE CHECK CHECKPOINT CLOSE CLUSTER CLUSTERS ' +
	                'COBOL COLAUTH COLUMN COLUMNS COMMENT COMMIT COMPILE COMPRESS CONNECT CONSTANT ' +
	                'CONSTRAINT CONSTRAINTS CONTENTS CONTINUE CONTROLFILE CRASH CREATE CURRENT ' +
	                'CURRVAL CURSOR CYCLE DATABASE DATAFILE DATA_BASE DATE DATE DBA DEBUGOFF ' +
	                'DEBUGON DEC DECIMAL DECLARE DEFAULT DEFINITION DELAY DELETE DELTA DESC DIGITS ' +
	                'DISABLE DISMOUNT DISPOSE DISTINCT DO DOUBLE DROP EACH ELSE ELSIF ENABLE END ' +
	                'ENTRY ESCAPE EVENTS EXCEPT EXCEPTION EXCEPTIONS EXCEPTION_INIT EXCLUSIVE EXEC ' +
	                'EXECUTE EXISTS EXIT EXPLAIN EXTENT EXTERNALLY FALSE FETCH FILE FLOAT FLUSH FOR ' +
	                'FORCE FOREIGN FORM FORTRAN FOUND FREELIST FREELISTS FROM FUNCTION GENERIC GO ' +
	                'GOTO GRANT GROUP GROUPS HAVING IDENTIFIED IF IMMEDIATE INCLUDING INCREMENT ' +
	                'INDEX INDEXES INDICATOR INITIAL INITRANS INSERT INSTANCE INT INTEGER INTERSECT ' +
	                'INTO IS KEY LANGUAGE LAYER LEVEL LIMITED LINK LISTS LOCK LOGFILE LONG LOOP ' +
	                'MANAGE MANUAL MAXDATAFILES MAXEXTENTS MAXINSTANCES MAXLOGFILES MAXLOGHISTORY ' +
	                'MAXLOGMEMBERS MAXTRANS MAXVALUE MINEXTENTS MINUS MINVALUE MLSLABEL MODE MODIFY ' +
	                'MODULE MOUNT NATURAL NEW NEXT NEXTVAL NOARCHIVELOG NOAUDIT NOCACHE NOCOMPRESS ' +
	                'NOCYCLE NOMAXVALUE NOMINVALUE NONE NOORDER NORESETLOGS NORMAL NOSORT NOTFOUND ' +
	                'NOWAIT NUMBER number NUMBER_BASE NUMERIC OF OFF OFFLINE OLD ON ONLINE ONLY OPEN OPTIMAL ' +
	                'OPTION ORDER OTHERS OUT OWN PACKAGE PARALLEL PARTITION PCTFREE PCTINCREASE ' +
	                'PCTUSED PLAN PLI POSITIVE PRAGMA PRECISION PRIMARY PRIOR PRIVATE PRIVILEGES ' +
	                'PROCEDURE PROFILE PUBLIC QUOTA RAISE RANGE RAW READ REAL RECORD RECOVER ' +
	                'REFERENCES REFERENCING RELEASE REMR RENAME RESETLOGS RESOURCE RESTRICTED ' +
	                'RETURN REUSE REVOKE ROLE ROLES ROLLBACK ROW ROWID ROWLABEL ROWNUM ROWS ROWTYPE ' +
	                'RUN SAVEPOINT SCHEMA SCN SECTION SEGMENT SELECT SEPARATE SEQUENCE SESSION ' +
	                'SHARE SHARED SIZE SMALLINT SNAPSHOT SORT SPACE SQL SQLBUF SQLERROR SQLSTATE ' +
	                'START STATEMENT STATEMENT_ID STATISTICS STDDEV STOP STORAGE SUBTYPE ' +
	                'SUCCESSFUL SWITCH SYNONYM SYSDATE SYSTEM TABAUTH TABLESPACE TASK TEMPORARY ' +
	                'TERMINATE THEN THREAD TIME TO TRACING TRANSACTION TRIGGER TRIGGERS TRUE ' +
	                'TRUNCATE TYPE UNDER UNION UNIQUE UNLIMITED UNTIL UPDATE USE USING VALIDATE ' +
	                'VALUES VARCHAR VARCHAR2 varchar2 VIEW VIEWS WHEN WHENEVER WHERE WHILE WITH WORK WRITE ' +
	                'TABLE SQLCODE SQLERRM USER',
	            built_in: 'ABS ACOS ADD_MONTHS ADJ_DATE APPENDCHILDXML ASCII ASCIISTR ASIN ATAN ATAN2 AVG ' +
	        'BFILENAME BIN_TO_NUM BINARY2VARCHAR BIT_COMPLEMENT BIT_OR BIT_XOR BITAND ' +
	        'BOOL_TO_INT CARDINALITY CASE CAST CAST_FROM_BINARY_DOUBLE CAST_FROM_BINARY_FLOAT ' +
	        'CAST_FROM_BINARY_INTEGER CAST_FROM_NUMBER CAST_TO_BINARY_DOUBLE CAST_TO_BINARY_FLOAT ' +
	        'CAST_TO_BINARY_INTEGER CAST_TO_NUMBER CAST_TO_NVARCHAR2 CAST_TO_RAW CAST_TO_VARCHAR ' +
	        'CEIL CHARTOROWID CHR CLUSTER_ID CLUSTER_PROBABILITY CLUSTER_SET COALESCE COLLECT ' +
	        'COMPOSE CONCAT CONVERT CORR CORR_K CORR_S COS COSH COUNT COVAR_POP COVAR_SAMP ' +
	        'CUME_DIST CURRENT_DATE CURRENT_TIMESTAMP CV DBTIMEZONE DENSE_RANK DECODE DECOMPOSE ' +
	        'DELETEXML DEPTH DEREF DUMP EMPTY_BLOB EMPTY_CLOB ESTIMATE_CPU_UNITS EXISTSNODE EXP ' +
	        'EXTRACT EXTRACTVALUE FEATURE_ID FEATURE_SET FEATURE_VALUE FIRST FIRST_VALUE FLOOR ' +
	        'FROM_TZ GET_CLOCK_TIME GET_DDL GET_DEPENDENT_DDL GET_DEPENDENT_XML GET_GRANTED_DDL ' +
	        'GET_GRANTED_XDL GET_HASH GET_REBUILD_COMMAND GET_SCN GET_XML GREATEST GROUP_ID ' +
	        'GROUPING GROUPING_ID HEXTORAW INITCAP INSERTCHILDXML INSERTXMLBEFORE INSTR INSTRB ' +
	        'INSTRC INSTR2 INSTR4 INT_TO_BOOL INTERVAL ITERATE ITERATION_NUMBER LAG LAST LAST_DAY ' +
	        'LAST_VALUE LEAD LEAST LENGTH LENGTHB LENGTHC LENGTH2 LENGTH4 LN LNNVL LOCALTIMESTAMP ' +
	        'LOG LOWER LPAD LTRIM MAKEREF MAX MEDIAN MIN MONTHS_BETWEEN MOD NANVL NEW_TIME NEXT_DAY ' +
	        'NHEXTORAW NLS_CHARSET_DECL_LEN NLS_CHARSET_ID NLS_CHARSET_NAME NLS_INITCAP NLS_LOWER ' +
	        'NLSSORT NLS_UPPER NTILE NULLFN NULLIF NUMTODSINTERVAL NUMTOHEX NUMTOHEX2 NUMTOYMINTERVAL ' +
	        'NVL NVL2 ORA_HASH PATH PERCENT_RANK PERCENTILE_CONT PERCENTILE_DISC POWER POWERMULTISET ' +
	        'POWERMULTISET_BY_CARDINALITY PREDICTION PREDICTION_BOUNDS PREDICTION_COST PREDICTION_DETAILS ' +
	        'PREDICTION_PROBABILITY PREDICTION_SET PRESENTNNV PRESENTV PREVIOUS QUOTE DELIMITERS ' +
	        'RANDOMBYTES RANDOMINTEGER RANDOMNUMBER RANK RATIO_TO_REPORT RAW_TO_CHAR RAW_TO_NCHAR ' +
	        'RAW_TO_VARCHAR2 RAWTOHEX RAWTONHEX RAWTONUM RAWTONUM2 REF REFTOHEX REGEXP_COUNT REGEXP_INSTR ' +
	        'REGEXP_REPLACE REGEXP_SUBSTR REGR_AVGX REGR_AVGY REGR_COUNT REGR_INTERCEPT REGR_R2 REGR_SLOPE ' +
	        'REGR_SXX REGR_SXY REGR_SYY REMAINDER REPLACE REVERSE ROUND ROW_NUMBER ROWIDTOCHAR ' +
	        'ROWIDTONCHAR RPAD RTRIM SCN_TO_TIMESTAMP SESSIONTIMEZONE SET SIGN SIN SINH SOUNDEX ' +
	        'SQRT STATS_BINOMIAL_TEST STATS_CROSSTAB STATS_F_TEST STATS_KS_TEST STATS_MODE ' +
	        'STATS_MW_TEST STATS_ONE_WAY_ANOVA STATS_T_TEST STATS_WSR_TEST STDDEV STDDEV_POP ' +
	        'STDDEV_SAMP STRING_TO_RAW SUBSTR SUBSTRB SUBSTRC SUBSTR2 SUBSTR4 SUM SYS_CONNECT_BY_PATH ' +
	        'SYS_CONTEXT SYS_DBURIGEN SYS_EXTRACT_UTC SYS_GUID SYS_OP_COMBINED_HASH SYS_OP_DESCEND ' +
	        'SYS_OP_DISTINCT SYS_OP_GUID SYS_OP_LBID SYS_OP_MAP_NONNULL SYS_OP_RAWTONUM SYS_OP_RPB ' +
	        'SYS_OP_TOSETID SYS_TYPEID SYS_XMLAGG SYS_XMLGEN SYSDATE SYSTIMESTAMP TAN TANH ' +
	        'TIMESTAMP_TO_SCN TO_BINARYDOUBLE TO_BINARYFLOAT TO_CHAR TO_CLOB TO_DATE TO_DSINTERVAL ' +
	        'TO_LOB TO_MULTI_BYTE TO_NCHAR TO_NCLOB TO_NUMBER TO_SINGLE_BYTE TO_TIMESTAMP ' +
	        'TO_TIMESTAMP_TZ TO_YMINTERVAL TRANSLATE TRANSLITERATE TREAT TRIM TRUNC TZ_OFFSET ' +
	        'UID UNISTR UPDATEXML UPPER USER USERENV VALUE VAR_POP VAR_SAMP VARIANCE VERIFY_OWNER ' +
	        'VERIFY_TABLE VERTICAL BARS VSIZE WIDTH_BUCKET XMLAGG XMLCAST XMLCDATA XMLCOLLATVAL ' +
	        'XMLCOMMENT XMLCONCAT XMLDIFF XMLELEMENT XMLEXISTS XMLFOREST XMLISVALID XMLPARSE ' +
	        'XMLPATCH XMLPI XMLQUERY XMLROOT XMLSEQUENCE XMLSERIALIZE XMLTABLE XMLTRANSFORM XOR'
	        },
	        c: [T.CLCM, T.CBCM, T.ASM, T.QSM, T.CNM, {
	            cN: "literal",
	            v: [{
	                b: "#\\s+[a-zA-Z\\ \\.]*",
	                r: 0
	            }, {
	                b: "#[a-zA-Z\\ \\.]+"
	            }]
	        }]
	    }
	});
	hljs.registerLanguage("php", function(e) {
	    var c = {
	            b: "\\$+[a-zA-Z_-ÿ][a-zA-Z0-9_-ÿ]*"
	        },
	        i = {
	            cN: "meta",
	            b: /<\?(php)?|\?>/
	        },
	        t = {
	            cN: "string",
	            c: [e.BE, i],
	            v: [{
	                    b: 'b"',
	                    e: '"'
	                }, {
	                    b: "b'",
	                    e: "'"
	                },
	                e.inherit(e.ASM, {
	                    i: null
	                }), e.inherit(e.QSM, {
	                    i: null
	                })
	            ]
	        },
	        a = {
	            v: [e.BNM, e.CNM]
	        };
	    return {
	        aliases: ["php3", "php4", "php5", "php6"],
	        cI: !0,
	        k: "and include_once list abstract global private echo interface as static endswitch array null if endwhile or const for endforeach self var while isset public protected exit foreach throw elseif include __FILE__ empty require_once do xor return parent clone use __CLASS__ __LINE__ else break print eval new catch __METHOD__ case exception default die require __FUNCTION__ enddeclare final try switch continue endfor endif declare unset true false trait goto instanceof insteadof __DIR__ __NAMESPACE__ yield finally",
	        c: [e.HCM, e.C("//", "$", {
	                c: [i]
	            }), e.C("/\\*", "\\*/", {
	                c: [{
	                    cN: "doctag",
	                    b: "@[A-Za-z]+"
	                }]
	            }), e.C("__halt_compiler.+?;", !1, {
	                eW: !0,
	                k: "__halt_compiler",
	                l: e.UIR
	            }), {
	                cN: "string",
	                b: /<<<['"]?\w+['"]?$/,
	                e: /^\w+;?$/,
	                c: [e.BE, {
	                    cN: "subst",
	                    v: [{
	                        b: /\$\w+/
	                    }, {
	                        b: /\{\$/,
	                        e: /\}/
	                    }]
	                }]
	            },
	            i, {
	                cN: "keyword",
	                b: /\$this\b/
	            },
	            c, {
	                b: /(::|->)+[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/
	            }, {
	                cN: "function",
	                bK: "function",
	                e: /[;{]/,
	                eE: !0,
	                i: "\\$|\\[|%",
	                c: [e.UTM, {
	                    cN: "params",
	                    b: "\\(",
	                    e: "\\)",
	                    c: ["self", c, e.CBCM, t, a]
	                }]
	            }, {
	                cN: "class",
	                bK: "class interface",
	                e: "{",
	                eE: !0,
	                i: /[:\(\$"]/,
	                c: [{
	                        bK: "extends implements"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "namespace",
	                e: ";",
	                i: /[\.']/,
	                c: [e.UTM]
	            }, {
	                bK: "use",
	                e: ";",
	                c: [e.UTM]
	            }, {
	                b: "=>"
	            },
	            t, a
	        ]
	    }
	});
	hljs.registerLanguage("javascript", function(e) {
	    return {
	        aliases: ["js", "jsx"],
	        k: {
	            keyword: "in of if for while finally var new function do return void else break catch instanceof with throw case default try this switch continue typeof delete let yield const export super debugger as async await static import from as",
	            literal: "true false null undefined NaN Infinity",
	            built_in: "eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape Object Function Boolean Error EvalError InternalError RangeError ReferenceError StopIteration SyntaxError TypeError URIError Number Math Date String RegExp Array Float32Array Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Uint8Array Uint8ClampedArray ArrayBuffer DataView JSON Intl arguments require module console window document Symbol Set Map WeakSet WeakMap Proxy Reflect Promise"
	        },
	        c: [{
	                cN: "meta",
	                r: 10,
	                b: /^\s*['"]use (strict|asm)['"]/
	            }, {
	                cN: "meta",
	                b: /^#!/,
	                e: /$/
	            },
	            e.ASM, e.QSM, {
	                cN: "string",
	                b: "`",
	                e: "`",
	                c: [e.BE, {
	                    cN: "subst",
	                    b: "\\$\\{",
	                    e: "\\}"
	                }]
	            },
	            e.CLCM, e.CBCM, {
	                cN: "number",
	                v: [{
	                    b: "\\b(0[bB][01]+)"
	                }, {
	                    b: "\\b(0[oO][0-7]+)"
	                }, {
	                    b: e.CNR
	                }],
	                r: 0
	            }, {
	                b: "(" + e.RSR + "|\\b(case|return|throw)\\b)\\s*",
	                k: "return throw case",
	                c: [e.CLCM, e.CBCM, e.RM, {
	                    b: /</,
	                    e: /(\/\w+|\w+\/)>/,
	                    sL: "xml",
	                    c: [{
	                        b: /<\w+\s*\/>/,
	                        skip: !0
	                    }, {
	                        b: /<\w+/,
	                        e: /(\/\w+|\w+\/)>/,
	                        skip: !0,
	                        c: ["self"]
	                    }]
	                }],
	                r: 0
	            }, {
	                cN: "function",
	                bK: "function",
	                e: /\{/,
	                eE: !0,
	                c: [e.inherit(e.TM, {
	                    b: /[A-Za-z$_][0-9A-Za-z$_]*/
	                }), {
	                    cN: "params",
	                    b: /\(/,
	                    e: /\)/,
	                    eB: !0,
	                    eE: !0,
	                    c: [e.CLCM, e.CBCM]
	                }],
	                i: /\[|%/
	            }, {
	                b: /\$[(.]/
	            },
	            e.METHOD_GUARD, {
	                cN: "class",
	                bK: "class",
	                e: /[{;=]/,
	                eE: !0,
	                i: /[:"\[\]]/,
	                c: [{
	                        bK: "extends"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "constructor",
	                e: /\{/,
	                eE: !0
	            }
	        ],
	        i: /#(?!!)/
	    }
	});
	hljs.registerLanguage("java", function(e) {
	    var t = e.UIR + "(<" + e.UIR + "(\\s*,\\s*" + e.UIR + ")*>)?",
	        a = "false synchronized int abstract float private char boolean static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports",
	        r = "\\b(0[bB]([01]+[01_]+[01]+|[01]+)|0[xX]([a-fA-F0-9]+[a-fA-F0-9_]+[a-fA-F0-9]+|[a-fA-F0-9]+)|(([\\d]+[\\d_]+[\\d]+|[\\d]+)(\\.([\\d]+[\\d_]+[\\d]+|[\\d]+))?|\\.([\\d]+[\\d_]+[\\d]+|[\\d]+))([eE][-+]?\\d+)?)[lLfF]?",
	        s = {
	            cN: "number",
	            b: r,
	            r: 0
	        };
	    return {
	        aliases: ["jsp"],
	        k: a,
	        i: /<\/|#/,
	        c: [e.C("/\\*\\*", "\\*/", {
	                r: 0,
	                c: [{
	                    b: /\w+@/,
	                    r: 0
	                }, {
	                    cN: "doctag",
	                    b: "@[A-Za-z]+"
	                }]
	            }), e.CLCM, e.CBCM, e.ASM, e.QSM, {
	                cN: "class",
	                bK: "class interface",
	                e: /[{;=]/,
	                eE: !0,
	                k: "class interface",
	                i: /[:"\[\]]/,
	                c: [{
	                        bK: "extends implements"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "new throw return else",
	                r: 0
	            }, {
	                cN: "function",
	                b: "(" + t + "\\s+)+" + e.UIR + "\\s*\\(",
	                rB: !0,
	                e: /[{;=]/,
	                eE: !0,
	                k: a,
	                c: [{
	                        b: e.UIR + "\\s*\\(",
	                        rB: !0,
	                        r: 0,
	                        c: [e.UTM]
	                    }, {
	                        cN: "params",
	                        b: /\(/,
	                        e: /\)/,
	                        k: a,
	                        r: 0,
	                        c: [e.ASM, e.QSM, e.CNM, e.CBCM]
	                    },
	                    e.CLCM, e.CBCM
	                ]
	            },
	            s, {
	                cN: "meta",
	                b: "@[A-Za-z]+"
	            }
	        ]
	    }
	});
	hljs.registerLanguage("css", function(e) {
	    var c = "[a-zA-Z-][a-zA-Z0-9_-]*",
	        t = {
	            b: /[A-Z\_\.\-]+\s*:/,
	            rB: !0,
	            e: ";",
	            eW: !0,
	            c: [{
	                cN: "attribute",
	                b: /\S/,
	                e: ":",
	                eE: !0,
	                starts: {
	                    eW: !0,
	                    eE: !0,
	                    c: [{
	                            b: /[\w-]+\(/,
	                            rB: !0,
	                            c: [{
	                                cN: "built_in",
	                                b: /[\w-]+/
	                            }, {
	                                b: /\(/,
	                                e: /\)/,
	                                c: [e.ASM, e.QSM]
	                            }]
	                        },
	                        e.CSSNM, e.QSM, e.ASM, e.CBCM, {
	                            cN: "number",
	                            b: "#[0-9A-Fa-f]+"
	                        }, {
	                            cN: "meta",
	                            b: "!important"
	                        }
	                    ]
	                }
	            }]
	        };
	    return {
	        cI: !0,
	        i: /[=\/|'\$]/,
	        c: [e.CBCM, {
	            cN: "selector-id",
	            b: /#[A-Za-z0-9_-]+/
	        }, {
	            cN: "selector-class",
	            b: /\.[A-Za-z0-9_-]+/
	        }, {
	            cN: "selector-attr",
	            b: /\[/,
	            e: /\]/,
	            i: "$"
	        }, {
	            cN: "selector-pseudo",
	            b: /:(:)?[a-zA-Z0-9\_\-\+\(\)"'.]+/
	        }, {
	            b: "@(font-face|page)",
	            l: "[a-z-]+",
	            k: "font-face page"
	        }, {
	            b: "@",
	            e: "[{;]",
	            i: /:/,
	            c: [{
	                cN: "keyword",
	                b: /\w+/
	            }, {
	                b: /\s/,
	                eW: !0,
	                eE: !0,
	                r: 0,
	                c: [e.ASM, e.QSM, e.CSSNM]
	            }]
	        }, {
	            cN: "selector-tag",
	            b: c,
	            r: 0
	        }, {
	            b: "{",
	            e: "}",
	            i: /\S/,
	            c: [e.CBCM, t]
	        }]
	    }
	});
	hljs.registerLanguage("xml", function(s) {
	    var e = "[A-Za-z0-9\\._:-]+",
	        t = {
	            eW: !0,
	            i: /</,
	            r: 0,
	            c: [{
	                cN: "attr",
	                b: e,
	                r: 0
	            }, {
	                b: /=\s*/,
	                r: 0,
	                c: [{
	                    cN: "string",
	                    endsParent: !0,
	                    v: [{
	                        b: /"/,
	                        e: /"/
	                    }, {
	                        b: /'/,
	                        e: /'/
	                    }, {
	                        b: /[^\s"'=<>`]+/
	                    }]
	                }]
	            }]
	        };
	    return {
	        aliases: ["html", "xhtml", "rss", "atom", "xjb", "xsd", "xsl", "plist"],
	        cI: !0,
	        c: [{
	                cN: "meta",
	                b: "<!DOCTYPE",
	                e: ">",
	                r: 10,
	                c: [{
	                    b: "\\[",
	                    e: "\\]"
	                }]
	            },
	            s.C("<!--", "-->", {
	                r: 10
	            }), {
	                b: "<\\!\\[CDATA\\[",
	                e: "\\]\\]>",
	                r: 10
	            }, {
	                b: /<\?(php)?/,
	                e: /\?>/,
	                sL: "php",
	                c: [{
	                    b: "/\\*",
	                    e: "\\*/",
	                    skip: !0
	                }]
	            }, {
	                cN: "tag",
	                b: "<style(?=\\s|>|$)",
	                e: ">",
	                k: {
	                    name: "style"
	                },
	                c: [t],
	                starts: {
	                    e: "</style>",
	                    rE: !0,
	                    sL: ["css", "xml"]
	                }
	            }, {
	                cN: "tag",
	                b: "<script(?=\\s|>|$)",
	                e: ">",
	                k: {
	                    name: "script"
	                },
	                c: [t],
	                starts: {
	                    e: "</script>",
	                    rE: !0,
	                    sL: ["actionscript", "javascript", "handlebars", "xml"]
	                }
	            }, {
	                cN: "meta",
	                v: [{
	                    b: /<\?xml/,
	                    e: /\?>/,
	                    r: 10
	                }, {
	                    b: /<\?\w+/,
	                    e: /\?>/
	                }]
	            }, {
	                cN: "tag",
	                b: "</?",
	                e: "/?>",
	                c: [{
	                        cN: "name",
	                        b: /[^\/><\s]+/,
	                        r: 0
	                    },
	                    t
	                ]
	            }
	        ]
	    }
	});
	hljs.registerLanguage("sql", function(e) {
	    var t = e.C("--", "$");
	    return {
	        cI: !0,
	        i: /[<>{}*#]/,
	        c: [{
	                bK: "begin end start commit rollback savepoint lock alter create drop rename call delete do handler insert load replace select truncate update set show pragma grant merge describe use explain help declare prepare execute deallocate release unlock purge reset change stop analyze cache flush optimize repair kill install uninstall checksum restore check backup revoke comment",
	                e: /;/,
	                eW: !0,
	                l: /[\w\.]+/,
	                k: {
	                    keyword: "abort abs absolute acc acce accep accept access accessed accessible account acos action activate add addtime admin administer advanced advise aes_decrypt aes_encrypt after agent aggregate ali alia alias allocate allow alter always analyze ancillary and any anydata anydataset anyschema anytype apply archive archived archivelog are as asc ascii asin assembly assertion associate asynchronous at atan atn2 attr attri attrib attribu attribut attribute attributes audit authenticated authentication authid authors auto autoallocate autodblink autoextend automatic availability avg backup badfile basicfile before begin beginning benchmark between bfile bfile_base big bigfile bin binary_double binary_float binlog bit_and bit_count bit_length bit_or bit_xor bitmap blob_base block blocksize body both bound buffer_cache buffer_pool build bulk by byte byteordermark bytes cache caching call calling cancel capacity cascade cascaded case cast catalog category ceil ceiling chain change changed char_base char_length character_length characters characterset charindex charset charsetform charsetid check checksum checksum_agg child choose chr chunk class cleanup clear client clob clob_base clone close cluster_id cluster_probability cluster_set clustering coalesce coercibility col collate collation collect colu colum column column_value columns columns_updated comment commit compact compatibility compiled complete composite_limit compound compress compute concat concat_ws concurrent confirm conn connec connect connect_by_iscycle connect_by_isleaf connect_by_root connect_time connection consider consistent constant constraint constraints constructor container content contents context contributors controlfile conv convert convert_tz corr corr_k corr_s corresponding corruption cos cost count count_big counted covar_pop covar_samp cpu_per_call cpu_per_session crc32 create creation critical cross cube cume_dist curdate current current_date current_time current_timestamp current_user cursor curtime customdatum cycle data database databases datafile datafiles datalength date_add date_cache date_format date_sub dateadd datediff datefromparts datename datepart datetime2fromparts day day_to_second dayname dayofmonth dayofweek dayofyear days db_role_change dbtimezone ddl deallocate declare decode decompose decrement decrypt deduplicate def defa defau defaul default defaults deferred defi defin define degrees delayed delegate delete delete_all delimited demand dense_rank depth dequeue des_decrypt des_encrypt des_key_file desc descr descri describ describe descriptor deterministic diagnostics difference dimension direct_load directory disable disable_all disallow disassociate discardfile disconnect diskgroup distinct distinctrow distribute distributed div do document domain dotnet double downgrade drop dumpfile duplicate duration each edition editionable editions element ellipsis else elsif elt empty enable enable_all enclosed encode encoding encrypt end end-exec endian enforced engine engines enqueue enterprise entityescaping eomonth error errors escaped evalname evaluate event eventdata events except exception exceptions exchange exclude excluding execu execut execute exempt exists exit exp expire explain export export_set extended extent external external_1 external_2 externally extract failed failed_login_attempts failover failure far fast feature_set feature_value fetch field fields file file_name_convert filesystem_like_logging final finish first first_value fixed flash_cache flashback floor flush following follows for forall force form forma format found found_rows freelist freelists freepools fresh from from_base64 from_days ftp full function general generated get get_format get_lock getdate getutcdate global global_name globally go goto grant grants greatest group group_concat group_id grouping grouping_id groups gtid_subtract guarantee guard handler hash hashkeys having hea head headi headin heading heap help hex hierarchy high high_priority hosts hour http id ident_current ident_incr ident_seed identified identity idle_time if ifnull ignore iif ilike ilm immediate import in include including increment index indexes indexing indextype indicator indices inet6_aton inet6_ntoa inet_aton inet_ntoa infile initial initialized initially initrans inmemory inner innodb input insert install instance instantiable instr interface interleaved intersect into invalidate invisible is is_free_lock is_ipv4 is_ipv4_compat is_not is_not_null is_used_lock isdate isnull isolation iterate java join json json_exists keep keep_duplicates key keys kill language large last last_day last_insert_id last_value lax lcase lead leading least leaves left len lenght length less level levels library like like2 like4 likec limit lines link list listagg little ln load load_file lob lobs local localtime localtimestamp locate locator lock locked log log10 log2 logfile logfiles logging logical logical_reads_per_call logoff logon logs long loop low low_priority lower lpad lrtrim ltrim main make_set makedate maketime managed management manual map mapping mask master master_pos_wait match matched materialized max maxextents maximize maxinstances maxlen maxlogfiles maxloghistory maxlogmembers maxsize maxtrans md5 measures median medium member memcompress memory merge microsecond mid migration min minextents minimum mining minus minute minvalue missing mod mode model modification modify module monitoring month months mount move movement multiset mutex name name_const names nan national native natural nav nchar nclob nested never new newline next nextval no no_write_to_binlog noarchivelog noaudit nobadfile nocheck nocompress nocopy nocycle nodelay nodiscardfile noentityescaping noguarantee nokeep nologfile nomapping nomaxvalue nominimize nominvalue nomonitoring none noneditionable nonschema noorder nopr nopro noprom nopromp noprompt norely noresetlogs noreverse normal norowdependencies noschemacheck noswitch not nothing notice notrim novalidate now nowait nth_value nullif nulls num numb numbe nvarchar nvarchar2 object ocicoll ocidate ocidatetime ociduration ociinterval ociloblocator ocinumber ociref ocirefcursor ocirowid ocistring ocitype oct octet_length of off offline offset oid oidindex old on online only opaque open operations operator optimal optimize option optionally or oracle oracle_date oradata ord ordaudio orddicom orddoc order ordimage ordinality ordvideo organization orlany orlvary out outer outfile outline output over overflow overriding package pad parallel parallel_enable parameters parent parse partial partition partitions pascal passing password password_grace_time password_lock_time password_reuse_max password_reuse_time password_verify_function patch path patindex pctincrease pctthreshold pctused pctversion percent percent_rank percentile_cont percentile_disc performance period period_add period_diff permanent physical pi pipe pipelined pivot pluggable plugin policy position post_transaction pow power pragma prebuilt precedes preceding precision prediction prediction_cost prediction_details prediction_probability prediction_set prepare present preserve prior priority private private_sga privileges procedural procedure procedure_analyze processlist profiles project prompt protection public publishingservername purge quarter query quick quiesce quota quotename radians raise rand range rank raw read reads readsize rebuild record records recover recovery recursive recycle redo reduced ref reference referenced references referencing refresh regexp_like register regr_avgx regr_avgy regr_count regr_intercept regr_r2 regr_slope regr_sxx regr_sxy reject rekey relational relative relaylog release release_lock relies_on relocate rely rem remainder rename repair repeat replace replicate replication required reset resetlogs resize resource respect restore restricted result result_cache resumable resume retention return returning returns reuse reverse revoke right rlike role roles rollback rolling rollup round row row_count rowdependencies rowid rownum rows rtrim rules safe salt sample save savepoint sb1 sb2 sb4 scan schema schemacheck scn scope scroll sdo_georaster sdo_topo_geometry search sec_to_time second section securefile security seed segment select self sequence sequential serializable server servererror session session_user sessions_per_user set sets settings sha sha1 sha2 share shared shared_pool short show shrink shutdown si_averagecolor si_colorhistogram si_featurelist si_positionalcolor si_stillimage si_texture siblings sid sign sin size size_t sizes skip slave sleep smalldatetimefromparts smallfile snapshot some soname sort soundex source space sparse spfile split sql sql_big_result sql_buffer_result sql_cache sql_calc_found_rows sql_small_result sql_variant_property sqlcode sqldata sqlerror sqlname sqlstate sqrt square standalone standby start starting startup statement static statistics stats_binomial_test stats_crosstab stats_ks_test stats_mode stats_mw_test stats_one_way_anova stats_t_test_ stats_t_test_indep stats_t_test_one stats_t_test_paired stats_wsr_test status std stddev stddev_pop stddev_samp stdev stop storage store stored str str_to_date straight_join strcmp strict string struct stuff style subdate subpartition subpartitions substitutable substr substring subtime subtring_index subtype success sum suspend switch switchoffset switchover sync synchronous synonym sys sys_xmlagg sysasm sysaux sysdate sysdatetimeoffset sysdba sysoper system system_user sysutcdatetime table tables tablespace tan tdo template temporary terminated tertiary_weights test than then thread through tier ties time time_format time_zone timediff timefromparts timeout timestamp timestampadd timestampdiff timezone_abbr timezone_minute timezone_region to to_base64 to_date to_days to_seconds todatetimeoffset trace tracking transaction transactional translate translation treat trigger trigger_nestlevel triggers trim truncate try_cast try_convert try_parse type ub1 ub2 ub4 ucase unarchived unbounded uncompress under undo unhex unicode uniform uninstall union unique unix_timestamp unknown unlimited unlock unpivot unrecoverable unsafe unsigned until untrusted unusable unused update updated upgrade upped upper upsert url urowid usable usage use use_stored_outlines user user_data user_resources users using utc_date utc_timestamp uuid uuid_short validate validate_password_strength validation valist value values var var_samp varcharc vari varia variab variabl variable variables variance varp varraw varrawc varray verify version versions view virtual visible void wait wallet warning warnings week weekday weekofyear wellformed when whene whenev wheneve whenever where while whitespace with within without work wrapped xdb xml xmlagg xmlattributes xmlcast xmlcolattval xmlelement xmlexists xmlforest xmlindex xmlnamespaces xmlpi xmlquery xmlroot xmlschema xmlserialize xmltable xmltype xor year year_to_month years yearweek",
	                    literal: "true false null",
	                    built_in: "array bigint binary bit blob boolean char character date dec decimal float int int8 integer interval number numeric real record serial serial8 smallint text varchar varying void"
	                },
	                c: [{
	                        cN: "string",
	                        b: "'",
	                        e: "'",
	                        c: [e.BE, {
	                            b: "''"
	                        }]
	                    }, {
	                        cN: "string",
	                        b: '"',
	                        e: '"',
	                        c: [e.BE, {
	                            b: '""'
	                        }]
	                    }, {
	                        cN: "string",
	                        b: "`",
	                        e: "`",
	                        c: [e.BE]
	                    },
	                    e.CNM, e.CBCM, t
	                ]
	            },
	            e.CBCM, t
	        ]
	    }
	});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(126)))

/***/ },

/***/ 127:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(128);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js!./github.css", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js!./github.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 128:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "/*\n\ngithub.com style (c) Vasily Polovnyov <vast@whiteants.net>\n\n*/\n\n.hljs {\n  display: block;\n  overflow-x: auto;\n  padding: 0.5em;\n  color: #333;\n  background: #f8f8f8;\n}\n\n.hljs-comment,\n.hljs-quote {\n  color: #998;\n  font-style: italic;\n}\n\n.hljs-keyword,\n.hljs-selector-tag,\n.hljs-subst {\n  color: #333;\n  font-weight: bold;\n}\n\n.hljs-number,\n.hljs-literal,\n.hljs-variable,\n.hljs-template-variable,\n.hljs-tag .hljs-attr {\n  color: #008080;\n}\n\n.hljs-string,\n.hljs-doctag {\n  color: #d14;\n}\n\n.hljs-title,\n.hljs-section,\n.hljs-selector-id {\n  color: #900;\n  font-weight: bold;\n}\n\n.hljs-subst {\n  font-weight: normal;\n}\n\n.hljs-type,\n.hljs-class .hljs-title {\n  color: #458;\n  font-weight: bold;\n}\n\n.hljs-tag,\n.hljs-name,\n.hljs-attribute {\n  color: #000080;\n  font-weight: normal;\n}\n\n.hljs-regexp,\n.hljs-link {\n  color: #009926;\n}\n\n.hljs-symbol,\n.hljs-bullet {\n  color: #990073;\n}\n\n.hljs-built_in,\n.hljs-builtin-name {\n  color: #0086b3;\n}\n\n.hljs-meta {\n  color: #999;\n  font-weight: bold;\n}\n\n.hljs-deletion {\n  background: #fdd;\n}\n\n.hljs-addition {\n  background: #dfd;\n}\n\n.hljs-emphasis {\n  font-style: italic;\n}\n\n.hljs-strong {\n  font-weight: bold;\n}\n", ""]);

	// exports


/***/ },

/***/ 475:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(476)
	__webpack_require__(125)
	__webpack_require__(112);
	angular.module(window.moduleName)
	    .component('previousPapersPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(478),
	        controller: AdminPreviousPapersController,
	        controllerAs: 'vm'
	    })

	AdminPreviousPapersController.$inject = ['admin', '$filter', '$scope', '$state','alertify','utility','EditorConfig'];

	function AdminPreviousPapersController(admin, $filter, $scope, $state , alertify, utility,EditorConfig) {
	    var vm = this;

		EditorConfig.toolbarButtons = EditorConfig.toolbarButtonsMD = EditorConfig.toolbarButtonsSM = EditorConfig.toolbarButtonsXS = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertImage', 'insertVideo', 'insertTable', 'html'],
	    EditorConfig.width = screen.availWidth / 2;
	    EditorConfig.height = screen.availHeight;
	    EditorConfig.placeholderText = 'Type Previous Paper Format Here',
	    EditorConfig.quickInsertButtons = []
	    EditorConfig.pluginsEnabled = ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"]

	    vm.EditorConfig = EditorConfig;

	    vm.selectedCourse = [];
	    vm.selectedChild = [];
	    vm.isCourseExpanded = true;
	    vm.isFilterExpanded = true;

	    vm.selectedSubjects = [];
	    vm.selectedAuthors = [];
	    vm.selectedPublications = [];
	    vm.selectedBooks = [];
	    vm.selectedExams = [];
	    vm.selectedTopics = [];
	    vm.selectedTopicGroup = [];
	    vm.selectedTags = [];

	    vm.trustAsHTML = utility.trustAsHTML;
		
		vm.$onInit = function(){
			$scope.$watch(function() {
	            return vm.parent.coursemaster;
	        }, function(v) {
	        	vm.allCourses = v;
	        });
	        getAllFilterValues();
	        getAllPreviousPapers();
		}

	    function getAllPreviousPapers(){
	        admin.getAllPreviousPapers()
	            .then(function(res){
	                vm.allPreviousPapers = res.data
	            })
	            .catch(function(err){
	                console.error(err)
	            })
	    }

		function getAllFilterValues(){
			admin.getAllSubject()
	            .then(function(d) {
	                vm.subjects = d.data;
	            });
	        admin.getAllAuthor()
	            .then(function(d) {
	                vm.authors = d.data;
	            });
	        admin.getAllPublication()
	            .then(function(d) {
	                vm.publications = d.data;
	            });
	        admin.getAllBook()
	            .then(function(d) {
	                vm.books = d.data;
	            });
	        admin.getAllExam()
	            .then(function(d) {
	                vm.exams = d.data;
	            });
	        admin.getAllTopic()
	            .then(function(d) {
	                vm.topics = d.data;
	            });
	        admin.getAllTopicGroups()
	            .then(function(d) {
	                vm.topicGroups = d.data;
	            });
	        admin.getAllTag()
	            .then(function(d) {
	                vm.tags = d.data;
	            });
		}

		vm.collapseAllQuestions = function(){
			if(vm.questions && vm.questions.length > 0){
				vm.questions = vm.questions.map(function(v,i){
					v.isOpen = false;
					return v;
				})
			}
		}

		vm.openAllQuestions = function(){
			if(vm.questions && vm.questions.length > 0){
				vm.questions = vm.questions.map(function(v,i){
					v.isOpen = true;
					return v;
				})
			}
		}

		vm.onCourseChange = function(){
			if(vm.selectedCourse.length > 0){
				admin.getBoardCompetitiveCourse({
					course_id : vm.selectedCourse[0]
				})
				.then(function(res){
					if(res.data && res.data[0] && res.data[0].course_id){
						vm.fullCourse = res.data[0]
					}
				})
				.catch(function(err){
					console.log(err);
				})
			}else{
				vm.fullCourse = {};
			}
	    }

	    vm.onChildChange = function(){
	    	if(vm.selectedChild.length > 0){
				admin.getPreviousPaper({
					paper_id : vm.selectedChild[0]
				})
				.then(function(res){
					if(res.data){
						vm.previousPaper = res.data.paper
					}
				})
				.catch(function(err){
					console.error(err);

				})
	    	}else{
	    		vm.previousPaper = "";
	    	}
	    }

	    vm.onFilterChange = function(item, model) {
	        if(vm.selectedSubjects.length === 0 &&
				vm.selectedAuthors.length === 0 &&
				vm.selectedPublications.length === 0 &&
				vm.selectedBooks.length === 0 &&
				vm.selectedExams.length === 0 &&
				vm.selectedTopics.length === 0 &&
				vm.selectedTopicGroup.length === 0 &&
				vm.selectedTags.length === 0)
	        {
	        	vm.questions = [];
				return;
	        }

	        var query = {
	                subjects: vm.selectedSubjects,
	                authors: vm.selectedAuthors,
	                publications: vm.selectedPublications,
	                books: vm.selectedBooks,
	                exams: vm.selectedExams,
	                topics: vm.selectedTopics,
	                topicGroup: vm.selectedTopicGroup,
	                tags: vm.selectedTags
	            }
	        vm.showLoader = true;
	        admin.findPracticeQuestionsTextByQuery(query)
	            .then(function(d) {
	            	vm.showLoader = false;
	                // console.log(d.data);
	                if (d.data.length > 0) {
	                    vm.questions = d.data;
	                    vm.questions.sort(function(a, b) {
	                        return a.questionNumber - b.questionNumber;
	                    });
	                } else {
	                    vm.questions = [];
	                }
	            })
	            .catch(function(err) {
	            	vm.showLoader = false;
	                console.log(err);
	            });
	    }

	    vm.updatePreviousPaper = function(){
	    	if(vm.isPaperSaving){
	    		return;
	    	}
	    	var obj = {
	    		paper_id : vm.selectedChild[0],
	    		course_id : vm.selectedCourse[0],
	    		paper : vm.previousPaper
	    	}
			
			vm.isPaperSaving = true;
	    	admin.updatePreviousPaper(obj)
	    		.then(function(res){
	    			console.log(res)
	    			alertify.success("Successfully Saved")
	    			vm.isPaperSaving = false;
	                getAllPreviousPapers();
	    		})
	    		.catch(function(err){
	    			console.log(err)
	    			alertify.error("Error in updating paper")
	    			vm.isPaperSaving = false;
	    		})
	    }

	    vm.editPreviousPaper = function(paper_id){
	        if(vm.allPreviousPapers){
	            var paper = $filter('filter')(vm.allPreviousPapers,{paper_id : paper_id});

	            if(paper && paper[0]){
	                vm.previousPaper = paper[0].paper;
	                vm.selectedCourse = [paper[0].course_id];
	                vm.selectedChild = [paper[0].paper_id];
	                vm.onCourseChange();
	            }
	        }
	    }

	    vm.removePreviousPaper = function(paper_id){
	        var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        admin.removePreviousPaper({paper_id : paper_id})
	            .then(function(res){
	                getAllPreviousPapers();
	            })
	            .catch(function(err){
	                console.log(err)
	                alertify.error("Error in deleting paper")
	            })
	    }

	    vm.resetForm = function(){
	    	var result = confirm("Are you sure you want to reset form");
	        if (!result) {
	            //Declined
	            return;
	        }
	        vm.isPaperSaving = false;
	        vm.previousPaper = "";
	    }
	}

/***/ },

/***/ 476:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(477);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_styles.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_styles.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 477:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.admin-previous-page-wrapper .ew-parts-wrapper {\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n  .admin-previous-page-wrapper .ew-parts-wrapper .ew-part {\n    min-height: 100vh;\n    padding: 0px 10px;\n    -webkit-box-flex: 1;\n    -moz-box-flex: 1;\n    box-flex: 1;\n    -webkit-flex: 1;\n    -moz-flex: 1;\n    -ms-flex: 1;\n    flex: 1; }\n    .admin-previous-page-wrapper .ew-parts-wrapper .ew-part.left {\n      border-right: 2px solid #e5e5e5; }\n  .admin-previous-page-wrapper .ew-parts-wrapper .title {\n    font-size: 15px;\n    margin-bottom: 10px;\n    font-weight: bold;\n    border: 1px solid #e5e5e5;\n    border-radius: 5px;\n    padding: 0px 10px;\n    cursor: pointer; }\n    .admin-previous-page-wrapper .ew-parts-wrapper .title i {\n      margin-right: 10px; }\n  .admin-previous-page-wrapper .ew-parts-wrapper .btns {\n    margin: 10px 0px;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: end;\n    -moz-box-pack: end;\n    box-pack: end;\n    -webkit-justify-content: flex-end;\n    -moz-justify-content: flex-end;\n    -ms-justify-content: flex-end;\n    -o-justify-content: flex-end;\n    justify-content: flex-end;\n    -ms-flex-pack: end;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n\n.admin-previous-page-wrapper .filters-wrapper .dot-loader-wrapper {\n  position: absolute;\n  z-index: 1;\n  background: rgba(0, 0, 0, 0.1);\n  left: 0px;\n  right: 0px;\n  top: -10px;\n  bottom: -10px;\n  border-radius: 5px; }\n\n.admin-previous-page-wrapper .filters-wrapper .dot-loader {\n  width: 100%;\n  height: 100%;\n  background-repeat: no-repeat; }\n\n.admin-previous-page-wrapper .questions-wrapper .MJXc-display {\n  display: inline-block !important; }\n\n.admin-previous-page-wrapper .questions-wrapper .collapse-all-btn {\n  margin-left: 20px;\n  color: #1778a4;\n  font-weight: bold;\n  text-decoration: underline;\n  display: inline-block;\n  font-size: 12px;\n  cursor: pointer; }\n\n.admin-previous-page-wrapper .questions-wrapper .question {\n  border: 1px solid #e5e5e5;\n  border-radius: 5px;\n  margin-bottom: 10px;\n  padding: 10px; }\n\n.admin-previous-page-wrapper .questions-wrapper .que-id {\n  font-weight: bold; }\n\n.admin-previous-page-wrapper .questions-wrapper .que-text {\n  font-size: 13px; }\n\n.admin-previous-page-wrapper .big-warning {\n  font-size: 20px;\n  font-weight: 'nunito',sans-serif;\n  color: #1d99d1; }\n\n.admin-previous-page-wrapper .form-group {\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n\n.admin-previous-page-wrapper #previous-papers-list {\n  border: 1px solid #e5e5e5;\n  border-top: 0px;\n  border-radius: 5px; }\n\n.admin-previous-page-wrapper .prev-ppr-info {\n  padding: 5px 10px; }\n  .admin-previous-page-wrapper .prev-ppr-info .paper-id {\n    font-weight: bold;\n    font-family: 'nunito',sans-serif;\n    margin: 0px 10px; }\n  .admin-previous-page-wrapper .prev-ppr-info .edit-btn {\n    text-decoration: underline;\n    color: green;\n    margin: 0px 10px;\n    cursor: pointer; }\n  .admin-previous-page-wrapper .prev-ppr-info .remove-btn {\n    text-decoration: underline;\n    color: red;\n    cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 478:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-previous-page-wrapper\">\n\t<div class=\"ew-parts-wrapper\">\n\t\t<div class=\"ew-part left\">\n\t\t\t<div class=\"big-warning\">\n\t\t\t\tPrevious Papers form\n\t\t\t</div>\n\t\t\t<div class=\"title\" data-toggle=\"collapse\" data-target=\"#course\" ng-click=\"vm.isCourseExpanded = !vm.isCourseExpanded\">\n\t\t\t\t<i class=\"fa\" ng-class=\"{'fa-plus':!vm.isCourseExpanded,'fa-minus':vm.isCourseExpanded}\"></i>Select Course and Child to enter previous year paper\n\t\t\t</div>\n\t\t\t<div class=\"ew-panel-wrapper collapse in\" id=\"course\" >\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Course</label>\n\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t<ui-select multiple limit=\"1\" ng-model=\"vm.selectedCourse\" on-select=\"vm.onCourseChange($item, $model)\" on-remove=\"vm.onCourseChange($item, $model)\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Course...\">\n\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t    <ui-select-choices repeat=\"item.courseId as item in (vm.allCourses | filter: $select.search) track by $index\">\n\t\t\t\t\t\t        <span ng-bind=\"item.courseName\"></span>\n\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Child</label>\n\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t<ui-select multiple limit=\"1\" ng-model=\"vm.selectedChild\" on-select=\"vm.onChildChange($item, $model)\" on-remove=\"vm.onChildChange($item, $model)\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Child...\">\n\t\t\t\t\t\t        <span ng-bind=\"$item.childName\"></span>\n\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t    <ui-select-choices repeat=\"item.childId as item in (vm.fullCourse.children | filter: $select.search) track by $index\">\n\t\t\t\t\t\t        <span ng-bind=\"item.childName\"></span>\n\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\t\n\t\t\t</div>\n\t\t\t\n\t\t\t<div class=\"filters-wrapper\" ng-if=\"vm.selectedCourse.length > 0 && vm.selectedChild.length > 0\">\n\t\t\t\t<div class=\"title\" data-toggle=\"collapse\" data-target=\"#filters\" ng-click=\"vm.isFilterExpanded = !vm.isFilterExpanded\">\n\t\t\t\t\t<i class=\"fa\" ng-class=\"{'fa-plus':!vm.isFilterExpanded,'fa-minus':vm.isFilterExpanded}\"></i>Select filters to find your questions\n\t\t\t\t</div>\n\t\t\t\t<div class=\"ew-panel-wrapper collapse in\" id=\"filters\" style=\"position: relative;\">\n\t\t\t\t\t<div class=\"dot-loader-wrapper\" ng-show=\"vm.showLoader\">\n\t\t\t\t\t\t<div class=\"dot-loader\"></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Subject</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedSubjects\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.subjectId as item in (vm.subjects | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"author\">Author</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedAuthors\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Author...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.authorName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.authorId as item in (vm.authors | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span ng-bind=\"item.authorName\"></span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"publication\">Publication</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedPublications\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Publication...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.publicationName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.publicationId as item in (vm.publications | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span ng-bind=\"item.publicationName\"></span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"book\">Book</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBooks\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Book...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.bookName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.bookId as item in (vm.books | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span ng-bind=\"item.bookName\"></span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"exam\">Exam</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedExams\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Exam...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.examName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.examId as item in (vm.exams | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span ng-bind=\"item.examName\"></span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Topic</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTopics\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Topic Group</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTopicGroup\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" limit=\"1\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic Group...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.groupName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.groupId as item in (vm.topicGroups | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span>{{item.groupNumber}} - {{item.groupName}}</span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tag\">Tag</label>\n\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTags\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Tag...\">\n\t\t\t\t\t\t\t        <span ng-bind=\"$item.tagName\"></span>\n\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.tagId as item in (vm.tags | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t        <span ng-bind=\"item.tagName\"></span>\n\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div ng-if=\"vm.questions.length === 0\">\n\t\t\t\t<div class=\"big-warning\">\n\t\t\t\t\tNo questions found.Please select appropriate filters to add question.\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"questions-wrapper\" ng-if=\"vm.questions.length > 0\">\n\t\t\t\t<div class=\"big-warning que-title\">\n\t\t\t\t\tQuestions ({{vm.questions.length}})\n\t\t\t\t\t<div class=\"collapse-all-btn\" ng-click=\"vm.collapseAllQuestions()\">Collapse All</div>\n\t\t\t\t\t<div class=\"collapse-all-btn\" ng-click=\"vm.openAllQuestions()\">Open All</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"question\" ng-init=\"que.isOpen = true\" ng-repeat=\"que in vm.questions\">\n\t\t\t\t\t<div class=\"que-id\" ng-click=\"que.isOpen = !que.isOpen\">{{que.questionId}}</div>\n\t\t\t\t\t<div class=\"que-text collapse\" ng-class=\"{'in':que.isOpen}\" id=\"{{que.questionId}}\" ng-bind-html=\"vm.trustAsHTML(que.question)\"></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"ew-part\">\n\t\t\t<div class=\"title\" style=\"margin-bottom: 0px;\" data-toggle=\"collapse\" data-target=\"#previous-papers-list\" ng-click=\"vm.isAlreadyEnteredPapersExpanded = !vm.isAlreadyEnteredPapersExpanded\" ng-if=\"vm.allPreviousPapers && vm.allPreviousPapers.length > 0\">\n\t\t\t\t<i class=\"fa\" ng-class=\"{'fa-plus':!vm.isAlreadyEnteredPapersExpanded,'fa-minus':vm.isAlreadyEnteredPapersExpanded}\"></i>Already Entered All Previous Papers\n\t\t\t</div>\n\t\t\t<div id=\"previous-papers-list\" class=\"collapse\" ng-if=\"vm.allPreviousPapers.length > 0\">\n\t\t\t\t<div class=\"prev-ppr-info\" ng-repeat=\"previous_paper in vm.allPreviousPapers\">\n\t\t\t\t\t<span ng-click=\"vm.editPreviousPaper(previous_paper.paper_id)\" class=\"edit-btn\">Edit</span>\n\t\t\t\t\t<span ng-click=\"vm.removePreviousPaper(previous_paper.paper_id)\" class=\"remove-btn\">Remove</span>\n\t\t\t\t\t<span class=\"paper-id\">{{previous_paper.paper_id}}</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"big-warning\" ng-if=\"vm.selectedCourse.length <= 0 || vm.selectedChild.length <= 0\">\n\t\t\t\tPlease select Course and Child in left panel to start entering previous year paper.\n\t\t\t</div>\n\t\t\t<div class=\"btns\" ng-if=\"vm.selectedCourse.length > 0 && vm.selectedChild.length > 0\">\n\t\t\t\t<div class=\"ew-rounded-btn green\" ng-click=\"vm.updatePreviousPaper()\">\n\t\t\t\t\t<i class=\"fa fa-save right\" ng-if=\"!vm.isPaperSaving\"></i>\n\t\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isPaperSaving\"></i>\n\t\t\t\t\t<span class=\"text\">Save</span>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn red\" ng-click=\"vm.resetForm()\">\n\t\t\t\t\t<i class=\"fa fa-trash right\"></i>\n\t\t\t\t\t<span class=\"text\">Reset</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div ng-if=\"vm.selectedCourse.length > 0 && vm.selectedChild.length > 0\">\n\t\t\t\t<textarea froala=\"vm.EditorConfig\" id=\"questionDesc\" rows=\"7\" class=\"form-control\" placeholder=\"Question Description\" ng-model=\"vm.previousPaper\"></textarea>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>";

/***/ }

});