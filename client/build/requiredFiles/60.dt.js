webpackJsonp([60],{

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(44);
	module.exports = 'ngFileUpload';

/***/ },

/***/ 44:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery, jQuery) {/**!
	 * AngularJS file upload directives and services. Supoorts: file upload/drop/paste, resume, cancel/abort,
	 * progress, resize, thumbnail, preview, validation and CORS
	 * FileAPI Flash shim for old browsers not supporting FormData
	 * @author  Danial  <danial.farid@gmail.com>
	 * @version 12.0.4
	 */

	(function () {
	  /** @namespace FileAPI.noContentTimeout */

	  function patchXHR(fnName, newFn) {
	    window.XMLHttpRequest.prototype[fnName] = newFn(window.XMLHttpRequest.prototype[fnName]);
	  }

	  function redefineProp(xhr, prop, fn) {
	    try {
	      Object.defineProperty(xhr, prop, {get: fn});
	    } catch (e) {/*ignore*/
	    }
	  }

	  if (!window.FileAPI) {
	    window.FileAPI = {};
	  }

	  if (!window.XMLHttpRequest) {
	    throw 'AJAX is not supported. XMLHttpRequest is not defined.';
	  }

	  FileAPI.shouldLoad = !window.FormData || FileAPI.forceLoad;
	  if (FileAPI.shouldLoad) {
	    var initializeUploadListener = function (xhr) {
	      if (!xhr.__listeners) {
	        if (!xhr.upload) xhr.upload = {};
	        xhr.__listeners = [];
	        var origAddEventListener = xhr.upload.addEventListener;
	        xhr.upload.addEventListener = function (t, fn) {
	          xhr.__listeners[t] = fn;
	          if (origAddEventListener) origAddEventListener.apply(this, arguments);
	        };
	      }
	    };

	    patchXHR('open', function (orig) {
	      return function (m, url, b) {
	        initializeUploadListener(this);
	        this.__url = url;
	        try {
	          orig.apply(this, [m, url, b]);
	        } catch (e) {
	          if (e.message.indexOf('Access is denied') > -1) {
	            this.__origError = e;
	            orig.apply(this, [m, '_fix_for_ie_crossdomain__', b]);
	          }
	        }
	      };
	    });

	    patchXHR('getResponseHeader', function (orig) {
	      return function (h) {
	        return this.__fileApiXHR && this.__fileApiXHR.getResponseHeader ? this.__fileApiXHR.getResponseHeader(h) : (orig == null ? null : orig.apply(this, [h]));
	      };
	    });

	    patchXHR('getAllResponseHeaders', function (orig) {
	      return function () {
	        return this.__fileApiXHR && this.__fileApiXHR.getAllResponseHeaders ? this.__fileApiXHR.getAllResponseHeaders() : (orig == null ? null : orig.apply(this));
	      };
	    });

	    patchXHR('abort', function (orig) {
	      return function () {
	        return this.__fileApiXHR && this.__fileApiXHR.abort ? this.__fileApiXHR.abort() : (orig == null ? null : orig.apply(this));
	      };
	    });

	    patchXHR('setRequestHeader', function (orig) {
	      return function (header, value) {
	        if (header === '__setXHR_') {
	          initializeUploadListener(this);
	          var val = value(this);
	          // fix for angular < 1.2.0
	          if (val instanceof Function) {
	            val(this);
	          }
	        } else {
	          this.__requestHeaders = this.__requestHeaders || {};
	          this.__requestHeaders[header] = value;
	          orig.apply(this, arguments);
	        }
	      };
	    });

	    patchXHR('send', function (orig) {
	      return function () {
	        var xhr = this;
	        if (arguments[0] && arguments[0].__isFileAPIShim) {
	          var formData = arguments[0];
	          var config = {
	            url: xhr.__url,
	            jsonp: false, //removes the callback form param
	            cache: true, //removes the ?fileapiXXX in the url
	            complete: function (err, fileApiXHR) {
	              if (err && angular.isString(err) && err.indexOf('#2174') !== -1) {
	                // this error seems to be fine the file is being uploaded properly.
	                err = null;
	              }
	              xhr.__completed = true;
	              if (!err && xhr.__listeners.load)
	                xhr.__listeners.load({
	                  type: 'load',
	                  loaded: xhr.__loaded,
	                  total: xhr.__total,
	                  target: xhr,
	                  lengthComputable: true
	                });
	              if (!err && xhr.__listeners.loadend)
	                xhr.__listeners.loadend({
	                  type: 'loadend',
	                  loaded: xhr.__loaded,
	                  total: xhr.__total,
	                  target: xhr,
	                  lengthComputable: true
	                });
	              if (err === 'abort' && xhr.__listeners.abort)
	                xhr.__listeners.abort({
	                  type: 'abort',
	                  loaded: xhr.__loaded,
	                  total: xhr.__total,
	                  target: xhr,
	                  lengthComputable: true
	                });
	              if (fileApiXHR.status !== undefined) redefineProp(xhr, 'status', function () {
	                return (fileApiXHR.status === 0 && err && err !== 'abort') ? 500 : fileApiXHR.status;
	              });
	              if (fileApiXHR.statusText !== undefined) redefineProp(xhr, 'statusText', function () {
	                return fileApiXHR.statusText;
	              });
	              redefineProp(xhr, 'readyState', function () {
	                return 4;
	              });
	              if (fileApiXHR.response !== undefined) redefineProp(xhr, 'response', function () {
	                return fileApiXHR.response;
	              });
	              var resp = fileApiXHR.responseText || (err && fileApiXHR.status === 0 && err !== 'abort' ? err : undefined);
	              redefineProp(xhr, 'responseText', function () {
	                return resp;
	              });
	              redefineProp(xhr, 'response', function () {
	                return resp;
	              });
	              if (err) redefineProp(xhr, 'err', function () {
	                return err;
	              });
	              xhr.__fileApiXHR = fileApiXHR;
	              if (xhr.onreadystatechange) xhr.onreadystatechange();
	              if (xhr.onload) xhr.onload();
	            },
	            progress: function (e) {
	              e.target = xhr;
	              if (xhr.__listeners.progress) xhr.__listeners.progress(e);
	              xhr.__total = e.total;
	              xhr.__loaded = e.loaded;
	              if (e.total === e.loaded) {
	                // fix flash issue that doesn't call complete if there is no response text from the server
	                var _this = this;
	                setTimeout(function () {
	                  if (!xhr.__completed) {
	                    xhr.getAllResponseHeaders = function () {
	                    };
	                    _this.complete(null, {status: 204, statusText: 'No Content'});
	                  }
	                }, FileAPI.noContentTimeout || 10000);
	              }
	            },
	            headers: xhr.__requestHeaders
	          };
	          config.data = {};
	          config.files = {};
	          for (var i = 0; i < formData.data.length; i++) {
	            var item = formData.data[i];
	            if (item.val != null && item.val.name != null && item.val.size != null && item.val.type != null) {
	              config.files[item.key] = item.val;
	            } else {
	              config.data[item.key] = item.val;
	            }
	          }

	          setTimeout(function () {
	            if (!FileAPI.hasFlash) {
	              throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
	            }
	            xhr.__fileApiXHR = FileAPI.upload(config);
	          }, 1);
	        } else {
	          if (this.__origError) {
	            throw this.__origError;
	          }
	          orig.apply(xhr, arguments);
	        }
	      };
	    });
	    window.XMLHttpRequest.__isFileAPIShim = true;
	    window.FormData = FormData = function () {
	      return {
	        append: function (key, val, name) {
	          if (val.__isFileAPIBlobShim) {
	            val = val.data[0];
	          }
	          this.data.push({
	            key: key,
	            val: val,
	            name: name
	          });
	        },
	        data: [],
	        __isFileAPIShim: true
	      };
	    };

	    window.Blob = Blob = function (b) {
	      return {
	        data: b,
	        __isFileAPIBlobShim: true
	      };
	    };
	  }

	})();

	(function () {
	  /** @namespace FileAPI.forceLoad */
	  /** @namespace window.FileAPI.jsUrl */
	  /** @namespace window.FileAPI.jsPath */

	  function isInputTypeFile(elem) {
	    return elem[0].tagName.toLowerCase() === 'input' && elem.attr('type') && elem.attr('type').toLowerCase() === 'file';
	  }

	  function hasFlash() {
	    try {
	      var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
	      if (fo) return true;
	    } catch (e) {
	      if (navigator.mimeTypes['application/x-shockwave-flash'] !== undefined) return true;
	    }
	    return false;
	  }

	  function getOffset(obj) {
	    var left = 0, top = 0;

	    if (__webpack_provided_window_dot_jQuery) {
	      return jQuery(obj).offset();
	    }

	    if (obj.offsetParent) {
	      do {
	        left += (obj.offsetLeft - obj.scrollLeft);
	        top += (obj.offsetTop - obj.scrollTop);
	        obj = obj.offsetParent;
	      } while (obj);
	    }
	    return {
	      left: left,
	      top: top
	    };
	  }

	  if (FileAPI.shouldLoad) {
	    FileAPI.hasFlash = hasFlash();

	    //load FileAPI
	    if (FileAPI.forceLoad) {
	      FileAPI.html5 = false;
	    }

	    if (!FileAPI.upload) {
	      var jsUrl, basePath, script = document.createElement('script'), allScripts = document.getElementsByTagName('script'), i, index, src;
	      if (window.FileAPI.jsUrl) {
	        jsUrl = window.FileAPI.jsUrl;
	      } else if (window.FileAPI.jsPath) {
	        basePath = window.FileAPI.jsPath;
	      } else {
	        for (i = 0; i < allScripts.length; i++) {
	          src = allScripts[i].src;
	          index = src.search(/\/ng\-file\-upload[\-a-zA-z0-9\.]*\.js/);
	          if (index > -1) {
	            basePath = src.substring(0, index + 1);
	            break;
	          }
	        }
	      }

	      if (FileAPI.staticPath == null) FileAPI.staticPath = basePath;
	      script.setAttribute('src', jsUrl || basePath + 'FileAPI.min.js');
	      document.getElementsByTagName('head')[0].appendChild(script);
	    }

	    FileAPI.ngfFixIE = function (elem, fileElem, changeFn) {
	      if (!hasFlash()) {
	        throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
	      }
	      var fixInputStyle = function () {
	        var label = fileElem.parent();
	        if (elem.attr('disabled')) {
	          if (label) label.removeClass('js-fileapi-wrapper');
	        } else {
	          if (!fileElem.attr('__ngf_flash_')) {
	            fileElem.unbind('change');
	            fileElem.unbind('click');
	            fileElem.bind('change', function (evt) {
	              fileApiChangeFn.apply(this, [evt]);
	              changeFn.apply(this, [evt]);
	            });
	            fileElem.attr('__ngf_flash_', 'true');
	          }
	          label.addClass('js-fileapi-wrapper');
	          if (!isInputTypeFile(elem)) {
	            label.css('position', 'absolute')
	              .css('top', getOffset(elem[0]).top + 'px').css('left', getOffset(elem[0]).left + 'px')
	              .css('width', elem[0].offsetWidth + 'px').css('height', elem[0].offsetHeight + 'px')
	              .css('filter', 'alpha(opacity=0)').css('display', elem.css('display'))
	              .css('overflow', 'hidden').css('z-index', '900000')
	              .css('visibility', 'visible');
	            fileElem.css('width', elem[0].offsetWidth + 'px').css('height', elem[0].offsetHeight + 'px')
	              .css('position', 'absolute').css('top', '0px').css('left', '0px');
	          }
	        }
	      };

	      elem.bind('mouseenter', fixInputStyle);

	      var fileApiChangeFn = function (evt) {
	        var files = FileAPI.getFiles(evt);
	        //just a double check for #233
	        for (var i = 0; i < files.length; i++) {
	          if (files[i].size === undefined) files[i].size = 0;
	          if (files[i].name === undefined) files[i].name = 'file';
	          if (files[i].type === undefined) files[i].type = 'undefined';
	        }
	        if (!evt.target) {
	          evt.target = {};
	        }
	        evt.target.files = files;
	        // if evt.target.files is not writable use helper field
	        if (evt.target.files !== files) {
	          evt.__files_ = files;
	        }
	        (evt.__files_ || evt.target.files).item = function (i) {
	          return (evt.__files_ || evt.target.files)[i] || null;
	        };
	      };
	    };

	    FileAPI.disableFileInput = function (elem, disable) {
	      if (disable) {
	        elem.removeClass('js-fileapi-wrapper');
	      } else {
	        elem.addClass('js-fileapi-wrapper');
	      }
	    };
	  }
	})();

	if (!window.FileReader) {
	  window.FileReader = function () {
	    var _this = this, loadStarted = false;
	    this.listeners = {};
	    this.addEventListener = function (type, fn) {
	      _this.listeners[type] = _this.listeners[type] || [];
	      _this.listeners[type].push(fn);
	    };
	    this.removeEventListener = function (type, fn) {
	      if (_this.listeners[type]) _this.listeners[type].splice(_this.listeners[type].indexOf(fn), 1);
	    };
	    this.dispatchEvent = function (evt) {
	      var list = _this.listeners[evt.type];
	      if (list) {
	        for (var i = 0; i < list.length; i++) {
	          list[i].call(_this, evt);
	        }
	      }
	    };
	    this.onabort = this.onerror = this.onload = this.onloadstart = this.onloadend = this.onprogress = null;

	    var constructEvent = function (type, evt) {
	      var e = {type: type, target: _this, loaded: evt.loaded, total: evt.total, error: evt.error};
	      if (evt.result != null) e.target.result = evt.result;
	      return e;
	    };
	    var listener = function (evt) {
	      if (!loadStarted) {
	        loadStarted = true;
	        if (_this.onloadstart) _this.onloadstart(constructEvent('loadstart', evt));
	      }
	      var e;
	      if (evt.type === 'load') {
	        if (_this.onloadend) _this.onloadend(constructEvent('loadend', evt));
	        e = constructEvent('load', evt);
	        if (_this.onload) _this.onload(e);
	        _this.dispatchEvent(e);
	      } else if (evt.type === 'progress') {
	        e = constructEvent('progress', evt);
	        if (_this.onprogress) _this.onprogress(e);
	        _this.dispatchEvent(e);
	      } else {
	        e = constructEvent('error', evt);
	        if (_this.onerror) _this.onerror(e);
	        _this.dispatchEvent(e);
	      }
	    };
	    this.readAsDataURL = function (file) {
	      FileAPI.readAsDataURL(file, listener);
	    };
	    this.readAsText = function (file) {
	      FileAPI.readAsText(file, listener);
	    };
	  };
	}

	/**!
	 * AngularJS file upload directives and services. Supoorts: file upload/drop/paste, resume, cancel/abort,
	 * progress, resize, thumbnail, preview, validation and CORS
	 * @author  Danial  <danial.farid@gmail.com>
	 * @version 12.0.4
	 */

	if (window.XMLHttpRequest && !(window.FileAPI && FileAPI.shouldLoad)) {
	  window.XMLHttpRequest.prototype.setRequestHeader = (function (orig) {
	    return function (header, value) {
	      if (header === '__setXHR_') {
	        var val = value(this);
	        // fix for angular < 1.2.0
	        if (val instanceof Function) {
	          val(this);
	        }
	      } else {
	        orig.apply(this, arguments);
	      }
	    };
	  })(window.XMLHttpRequest.prototype.setRequestHeader);
	}

	var ngFileUpload = angular.module('ngFileUpload', []);

	ngFileUpload.version = '12.0.4';

	ngFileUpload.service('UploadBase', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {
	  var upload = this;
	  upload.promisesCount = 0;

	  this.isResumeSupported = function () {
	    return window.Blob && window.Blob.prototype.slice;
	  };

	  var resumeSupported = this.isResumeSupported();

	  function sendHttp(config) {
	    config.method = config.method || 'POST';
	    config.headers = config.headers || {};

	    var deferred = config._deferred = config._deferred || $q.defer();
	    var promise = deferred.promise;

	    function notifyProgress(e) {
	      if (deferred.notify) {
	        deferred.notify(e);
	      }
	      if (promise.progressFunc) {
	        $timeout(function () {
	          promise.progressFunc(e);
	        });
	      }
	    }

	    function getNotifyEvent(n) {
	      if (config._start != null && resumeSupported) {
	        return {
	          loaded: n.loaded + config._start,
	          total: (config._file && config._file.size) || n.total,
	          type: n.type, config: config,
	          lengthComputable: true, target: n.target
	        };
	      } else {
	        return n;
	      }
	    }

	    if (!config.disableProgress) {
	      config.headers.__setXHR_ = function () {
	        return function (xhr) {
	          if (!xhr || !xhr.upload || !xhr.upload.addEventListener) return;
	          config.__XHR = xhr;
	          if (config.xhrFn) config.xhrFn(xhr);
	          xhr.upload.addEventListener('progress', function (e) {
	            e.config = config;
	            notifyProgress(getNotifyEvent(e));
	          }, false);
	          //fix for firefox not firing upload progress end, also IE8-9
	          xhr.upload.addEventListener('load', function (e) {
	            if (e.lengthComputable) {
	              e.config = config;
	              notifyProgress(getNotifyEvent(e));
	            }
	          }, false);
	        };
	      };
	    }

	    function uploadWithAngular() {
	      $http(config).then(function (r) {
	          if (resumeSupported && config._chunkSize && !config._finished && config._file) {
	            notifyProgress({
	                loaded: config._end,
	                total: config._file && config._file.size,
	                config: config, type: 'progress'
	              }
	            );
	            upload.upload(config, true);
	          } else {
	            if (config._finished) delete config._finished;
	            deferred.resolve(r);
	          }
	        }, function (e) {
	          deferred.reject(e);
	        }, function (n) {
	          deferred.notify(n);
	        }
	      );
	    }

	    if (!resumeSupported) {
	      uploadWithAngular();
	    } else if (config._chunkSize && config._end && !config._finished) {
	      config._start = config._end;
	      config._end += config._chunkSize;
	      uploadWithAngular();
	    } else if (config.resumeSizeUrl) {
	      $http.get(config.resumeSizeUrl).then(function (resp) {
	        if (config.resumeSizeResponseReader) {
	          config._start = config.resumeSizeResponseReader(resp.data);
	        } else {
	          config._start = parseInt((resp.data.size == null ? resp.data : resp.data.size).toString());
	        }
	        if (config._chunkSize) {
	          config._end = config._start + config._chunkSize;
	        }
	        uploadWithAngular();
	      }, function (e) {
	        throw e;
	      });
	    } else if (config.resumeSize) {
	      config.resumeSize().then(function (size) {
	        config._start = size;
	        uploadWithAngular();
	      }, function (e) {
	        throw e;
	      });
	    } else {
	      if (config._chunkSize) {
	        config._start = 0;
	        config._end = config._start + config._chunkSize;
	      }
	      uploadWithAngular();
	    }


	    promise.success = function (fn) {
	      promise.then(function (response) {
	        fn(response.data, response.status, response.headers, config);
	      });
	      return promise;
	    };

	    promise.error = function (fn) {
	      promise.then(null, function (response) {
	        fn(response.data, response.status, response.headers, config);
	      });
	      return promise;
	    };

	    promise.progress = function (fn) {
	      promise.progressFunc = fn;
	      promise.then(null, null, function (n) {
	        fn(n);
	      });
	      return promise;
	    };
	    promise.abort = promise.pause = function () {
	      if (config.__XHR) {
	        $timeout(function () {
	          config.__XHR.abort();
	        });
	      }
	      return promise;
	    };
	    promise.xhr = function (fn) {
	      config.xhrFn = (function (origXhrFn) {
	        return function () {
	          if (origXhrFn) origXhrFn.apply(promise, arguments);
	          fn.apply(promise, arguments);
	        };
	      })(config.xhrFn);
	      return promise;
	    };

	    upload.promisesCount++;
	    promise['finally'](function () {
	      upload.promisesCount--;
	    });
	    return promise;
	  }

	  this.isUploadInProgress = function () {
	    return upload.promisesCount > 0;
	  };

	  this.rename = function (file, name) {
	    file.ngfName = name;
	    return file;
	  };

	  this.jsonBlob = function (val) {
	    if (val != null && !angular.isString(val)) {
	      val = JSON.stringify(val);
	    }
	    var blob = new window.Blob([val], {type: 'application/json'});
	    blob._ngfBlob = true;
	    return blob;
	  };

	  this.json = function (val) {
	    return angular.toJson(val);
	  };

	  function copy(obj) {
	    var clone = {};
	    for (var key in obj) {
	      if (obj.hasOwnProperty(key)) {
	        clone[key] = obj[key];
	      }
	    }
	    return clone;
	  }

	  this.isFile = function (file) {
	    return file != null && (file instanceof window.Blob || (file.flashId && file.name && file.size));
	  };

	  this.upload = function (config, internal) {
	    function toResumeFile(file, formData) {
	      if (file._ngfBlob) return file;
	      config._file = config._file || file;
	      if (config._start != null && resumeSupported) {
	        if (config._end && config._end >= file.size) {
	          config._finished = true;
	          config._end = file.size;
	        }
	        var slice = file.slice(config._start, config._end || file.size);
	        slice.name = file.name;
	        slice.ngfName = file.ngfName;
	        if (config._chunkSize) {
	          formData.append('_chunkSize', config._chunkSize);
	          formData.append('_currentChunkSize', config._end - config._start);
	          formData.append('_chunkNumber', Math.floor(config._start / config._chunkSize));
	          formData.append('_totalSize', config._file.size);
	        }
	        return slice;
	      }
	      return file;
	    }

	    function addFieldToFormData(formData, val, key) {
	      if (val !== undefined) {
	        if (angular.isDate(val)) {
	          val = val.toISOString();
	        }
	        if (angular.isString(val)) {
	          formData.append(key, val);
	        } else if (upload.isFile(val)) {
	          var file = toResumeFile(val, formData);
	          var split = key.split(',');
	          if (split[1]) {
	            file.ngfName = split[1].replace(/^\s+|\s+$/g, '');
	            key = split[0];
	          }
	          config._fileKey = config._fileKey || key;
	          formData.append(key, file, file.ngfName || file.name);
	        } else {
	          if (angular.isObject(val)) {
	            if (val.$$ngfCircularDetection) throw 'ngFileUpload: Circular reference in config.data. Make sure specified data for Upload.upload() has no circular reference: ' + key;

	            val.$$ngfCircularDetection = true;
	            try {
	              for (var k in val) {
	                if (val.hasOwnProperty(k) && k !== '$$ngfCircularDetection') {
	                  var objectKey = config.objectKey == null ? '[i]' : config.objectKey;
	                  if (val.length && parseInt(k) > -1) {
	                    objectKey = config.arrayKey == null ? objectKey : config.arrayKey;
	                  }
	                  addFieldToFormData(formData, val[k], key + objectKey.replace(/[ik]/g, k));
	                }
	              }
	            } finally {
	              delete val.$$ngfCircularDetection;
	            }
	          } else {
	            formData.append(key, val);
	          }
	        }
	      }
	    }

	    function digestConfig() {
	      config._chunkSize = upload.translateScalars(config.resumeChunkSize);
	      config._chunkSize = config._chunkSize ? parseInt(config._chunkSize.toString()) : null;

	      config.headers = config.headers || {};
	      config.headers['Content-Type'] = undefined;
	      config.transformRequest = config.transformRequest ?
	        (angular.isArray(config.transformRequest) ?
	          config.transformRequest : [config.transformRequest]) : [];
	      config.transformRequest.push(function (data) {
	        var formData = new window.FormData(), key;
	        data = data || config.fields || {};
	        if (config.file) {
	          data.file = config.file;
	        }
	        for (key in data) {
	          if (data.hasOwnProperty(key)) {
	            var val = data[key];
	            if (config.formDataAppender) {
	              config.formDataAppender(formData, key, val);
	            } else {
	              addFieldToFormData(formData, val, key);
	            }
	          }
	        }

	        return formData;
	      });
	    }

	    if (!internal) config = copy(config);
	    if (!config._isDigested) {
	      config._isDigested = true;
	      digestConfig();
	    }

	    return sendHttp(config);
	  };

	  this.http = function (config) {
	    config = copy(config);
	    config.transformRequest = config.transformRequest || function (data) {
	        if ((window.ArrayBuffer && data instanceof window.ArrayBuffer) || data instanceof window.Blob) {
	          return data;
	        }
	        return $http.defaults.transformRequest[0].apply(this, arguments);
	      };
	    config._chunkSize = upload.translateScalars(config.resumeChunkSize);
	    config._chunkSize = config._chunkSize ? parseInt(config._chunkSize.toString()) : null;

	    return sendHttp(config);
	  };

	  this.translateScalars = function (str) {
	    if (angular.isString(str)) {
	      if (str.search(/kb/i) === str.length - 2) {
	        return parseFloat(str.substring(0, str.length - 2) * 1024);
	      } else if (str.search(/mb/i) === str.length - 2) {
	        return parseFloat(str.substring(0, str.length - 2) * 1048576);
	      } else if (str.search(/gb/i) === str.length - 2) {
	        return parseFloat(str.substring(0, str.length - 2) * 1073741824);
	      } else if (str.search(/b/i) === str.length - 1) {
	        return parseFloat(str.substring(0, str.length - 1));
	      } else if (str.search(/s/i) === str.length - 1) {
	        return parseFloat(str.substring(0, str.length - 1));
	      } else if (str.search(/m/i) === str.length - 1) {
	        return parseFloat(str.substring(0, str.length - 1) * 60);
	      } else if (str.search(/h/i) === str.length - 1) {
	        return parseFloat(str.substring(0, str.length - 1) * 3600);
	      }
	    }
	    return str;
	  };

	  this.urlToBlob = function(url) {
	    var defer = $q.defer();
	    $http({url: url, method: 'get', responseType: 'arraybuffer'}).then(function (resp) {
	      var arrayBufferView = new Uint8Array(resp.data);
	      var type = resp.headers('content-type') || 'image/WebP';
	      var blob = new window.Blob([arrayBufferView], {type: type});
	      defer.resolve(blob);
	      //var split = type.split('[/;]');
	      //blob.name = url.substring(0, 150).replace(/\W+/g, '') + '.' + (split.length > 1 ? split[1] : 'jpg');
	    }, function (e) {
	      defer.reject(e);
	    });
	    return defer.promise;
	  };

	  this.setDefaults = function (defaults) {
	    this.defaults = defaults || {};
	  };

	  this.defaults = {};
	  this.version = ngFileUpload.version;
	}

	]);

	ngFileUpload.service('Upload', ['$parse', '$timeout', '$compile', '$q', 'UploadExif', function ($parse, $timeout, $compile, $q, UploadExif) {
	  var upload = UploadExif;
	  upload.getAttrWithDefaults = function (attr, name) {
	    if (attr[name] != null) return attr[name];
	    var def = upload.defaults[name];
	    return (def == null ? def : (angular.isString(def) ? def : JSON.stringify(def)));
	  };

	  upload.attrGetter = function (name, attr, scope, params) {
	    var attrVal = this.getAttrWithDefaults(attr, name);
	    if (scope) {
	      try {
	        if (params) {
	          return $parse(attrVal)(scope, params);
	        } else {
	          return $parse(attrVal)(scope);
	        }
	      } catch (e) {
	        // hangle string value without single qoute
	        if (name.search(/min|max|pattern/i)) {
	          return attrVal;
	        } else {
	          throw e;
	        }
	      }
	    } else {
	      return attrVal;
	    }
	  };

	  upload.shouldUpdateOn = function (type, attr, scope) {
	    var modelOptions = upload.attrGetter('ngModelOptions', attr, scope);
	    if (modelOptions && modelOptions.updateOn) {
	      return modelOptions.updateOn.split(' ').indexOf(type) > -1;
	    }
	    return true;
	  };

	  upload.emptyPromise = function () {
	    var d = $q.defer();
	    var args = arguments;
	    $timeout(function () {
	      d.resolve.apply(d, args);
	    });
	    return d.promise;
	  };

	  upload.rejectPromise = function () {
	    var d = $q.defer();
	    var args = arguments;
	    $timeout(function () {
	      d.reject.apply(d, args);
	    });
	    return d.promise;
	  };

	  upload.happyPromise = function (promise, data) {
	    var d = $q.defer();
	    promise.then(function (result) {
	      d.resolve(result);
	    }, function (error) {
	      $timeout(function () {
	        throw error;
	      });
	      d.resolve(data);
	    });
	    return d.promise;
	  };

	  function applyExifRotations(files, attr, scope) {
	    var promises = [upload.emptyPromise()];
	    angular.forEach(files, function (f, i) {
	      if (f.type.indexOf('image/jpeg') === 0 && upload.attrGetter('ngfFixOrientation', attr, scope, {$file: f})) {
	        promises.push(upload.happyPromise(upload.applyExifRotation(f), f).then(function (fixedFile) {
	          files.splice(i, 1, fixedFile);
	        }));
	      }
	    });
	    return $q.all(promises);
	  }

	  function resize(files, attr, scope) {
	    var resizeVal = upload.attrGetter('ngfResize', attr, scope);
	    if (!resizeVal || !upload.isResizeSupported() || !files.length) return upload.emptyPromise();
	    if (resizeVal instanceof Function) {
	      var defer = $q.defer();
	      resizeVal(files).then(function (p) {
	        resizeWithParams(p, files, attr, scope).then(function (r) {
	          defer.resolve(r);
	        }, function (e) {
	          defer.reject(e);
	        });
	      }, function (e) {
	        defer.reject(e);
	      });
	    } else {
	      return resizeWithParams(resizeVal, files, attr, scope);
	    }
	  }

	  function resizeWithParams(param, files, attr, scope) {
	    var promises = [upload.emptyPromise()];

	    function handleFile(f, i) {
	      if (f.type.indexOf('image') === 0) {
	        if (param.pattern && !upload.validatePattern(f, param.pattern)) return;
	        var promise = upload.resize(f, param.width, param.height, param.quality,
	          param.type, param.ratio, param.centerCrop, function (width, height) {
	            return upload.attrGetter('ngfResizeIf', attr, scope,
	              {$width: width, $height: height, $file: f});
	          }, param.restoreExif !== false);
	        promises.push(promise);
	        promise.then(function (resizedFile) {
	          files.splice(i, 1, resizedFile);
	        }, function (e) {
	          f.$error = 'resize';
	          f.$errorParam = (e ? (e.message ? e.message : e) + ': ' : '') + (f && f.name);
	        });
	      }
	    }

	    for (var i = 0; i < files.length; i++) {
	      handleFile(files[i], i);
	    }
	    return $q.all(promises);
	  }

	  upload.updateModel = function (ngModel, attr, scope, fileChange, files, evt, noDelay) {
	    function update(files, invalidFiles, newFiles, dupFiles, isSingleModel) {
	      attr.$$ngfPrevValidFiles = files;
	      attr.$$ngfPrevInvalidFiles = invalidFiles;
	      var file = files && files.length ? files[0] : null;
	      var invalidFile = invalidFiles && invalidFiles.length ? invalidFiles[0] : null;

	      if (ngModel) {
	        upload.applyModelValidation(ngModel, files);
	        ngModel.$setViewValue(isSingleModel ? file : files);
	      }

	      if (fileChange) {
	        $parse(fileChange)(scope, {
	          $files: files,
	          $file: file,
	          $newFiles: newFiles,
	          $duplicateFiles: dupFiles,
	          $invalidFiles: invalidFiles,
	          $invalidFile: invalidFile,
	          $event: evt
	        });
	      }

	      var invalidModel = upload.attrGetter('ngfModelInvalid', attr);
	      if (invalidModel) {
	        $timeout(function () {
	          $parse(invalidModel).assign(scope, isSingleModel ? invalidFile : invalidFiles);
	        });
	      }
	      $timeout(function () {
	        // scope apply changes
	      });
	    }

	    var allNewFiles, dupFiles = [], prevValidFiles, prevInvalidFiles,
	      invalids = [], valids = [];

	    function removeDuplicates() {
	      function equals(f1, f2) {
	        return f1.name === f2.name && (f1.$ngfOrigSize || f1.size) === (f2.$ngfOrigSize || f2.size) &&
	          f1.type === f2.type;
	      }

	      function isInPrevFiles(f) {
	        var j;
	        for (j = 0; j < prevValidFiles.length; j++) {
	          if (equals(f, prevValidFiles[j])) {
	            return true;
	          }
	        }
	        for (j = 0; j < prevInvalidFiles.length; j++) {
	          if (equals(f, prevInvalidFiles[j])) {
	            return true;
	          }
	        }
	        return false;
	      }

	      if (files) {
	        allNewFiles = [];
	        dupFiles = [];
	        for (var i = 0; i < files.length; i++) {
	          if (isInPrevFiles(files[i])) {
	            dupFiles.push(files[i]);
	          } else {
	            allNewFiles.push(files[i]);
	          }
	        }
	      }
	    }

	    function toArray(v) {
	      return angular.isArray(v) ? v : [v];
	    }

	    function separateInvalids() {
	      valids = [];
	      invalids = [];
	      angular.forEach(allNewFiles, function (file) {
	        if (file.$error) {
	          invalids.push(file);
	        } else {
	          valids.push(file);
	        }
	      });
	    }

	    function resizeAndUpdate() {
	      function updateModel() {
	        $timeout(function () {
	          update(keep ? prevValidFiles.concat(valids) : valids,
	            keep ? prevInvalidFiles.concat(invalids) : invalids,
	            files, dupFiles, isSingleModel);
	        }, options && options.debounce ? options.debounce.change || options.debounce : 0);
	      }

	      resize(validateAfterResize ? allNewFiles : valids, attr, scope).then(function () {
	        if (validateAfterResize) {
	          upload.validate(allNewFiles, prevValidFiles.length, ngModel, attr, scope).then(function () {
	            separateInvalids();
	            updateModel();
	          });
	        } else {
	          updateModel();
	        }
	      }, function (e) {
	        throw 'Could not resize files ' + e;
	      });
	    }

	    prevValidFiles = attr.$$ngfPrevValidFiles || [];
	    prevInvalidFiles = attr.$$ngfPrevInvalidFiles || [];
	    if (ngModel && ngModel.$modelValue) {
	      prevValidFiles = toArray(ngModel.$modelValue);
	    }

	    var keep = upload.attrGetter('ngfKeep', attr, scope);
	    allNewFiles = (files || []).slice(0);
	    if (keep === 'distinct' || upload.attrGetter('ngfKeepDistinct', attr, scope) === true) {
	      removeDuplicates(attr, scope);
	    }

	    var isSingleModel = !keep && !upload.attrGetter('ngfMultiple', attr, scope) && !upload.attrGetter('multiple', attr);

	    if (keep && !allNewFiles.length) return;

	    upload.attrGetter('ngfBeforeModelChange', attr, scope, {
	      $files: files,
	      $file: files && files.length ? files[0] : null,
	      $newFiles: allNewFiles,
	      $duplicateFiles: dupFiles,
	      $event: evt
	    });

	    var validateAfterResize = upload.attrGetter('ngfValidateAfterResize', attr, scope);

	    var options = upload.attrGetter('ngModelOptions', attr, scope);
	    upload.validate(allNewFiles, prevValidFiles.length, ngModel, attr, scope).then(function () {
	      if (noDelay) {
	        update(allNewFiles, [], files, dupFiles, isSingleModel);
	      } else {
	        if ((!options || !options.allowInvalid) && !validateAfterResize) {
	          separateInvalids();
	        } else {
	          valids = allNewFiles;
	        }
	        if (upload.attrGetter('ngfFixOrientation', attr, scope) && upload.isExifSupported()) {
	          applyExifRotations(valids, attr, scope).then(function () {
	            resizeAndUpdate();
	          });
	        } else {
	          resizeAndUpdate();
	        }
	      }
	    });
	  };

	  return upload;
	}]);

	ngFileUpload.directive('ngfSelect', ['$parse', '$timeout', '$compile', 'Upload', function ($parse, $timeout, $compile, Upload) {
	  var generatedElems = [];

	  function isDelayedClickSupported(ua) {
	    // fix for android native browser < 4.4 and safari windows
	    var m = ua.match(/Android[^\d]*(\d+)\.(\d+)/);
	    if (m && m.length > 2) {
	      var v = Upload.defaults.androidFixMinorVersion || 4;
	      return parseInt(m[1]) < 4 || (parseInt(m[1]) === v && parseInt(m[2]) < v);
	    }

	    // safari on windows
	    return ua.indexOf('Chrome') === -1 && /.*Windows.*Safari.*/.test(ua);
	  }

	  function linkFileSelect(scope, elem, attr, ngModel, $parse, $timeout, $compile, upload) {
	    /** @namespace attr.ngfSelect */
	    /** @namespace attr.ngfChange */
	    /** @namespace attr.ngModel */
	    /** @namespace attr.ngModelOptions */
	    /** @namespace attr.ngfMultiple */
	    /** @namespace attr.ngfCapture */
	    /** @namespace attr.ngfValidate */
	    /** @namespace attr.ngfKeep */
	    var attrGetter = function (name, scope) {
	      return upload.attrGetter(name, attr, scope);
	    };

	    function isInputTypeFile() {
	      return elem[0].tagName.toLowerCase() === 'input' && attr.type && attr.type.toLowerCase() === 'file';
	    }

	    function fileChangeAttr() {
	      return attrGetter('ngfChange') || attrGetter('ngfSelect');
	    }

	    function changeFn(evt) {
	      if (upload.shouldUpdateOn('change', attr, scope)) {
	        var fileList = evt.__files_ || (evt.target && evt.target.files), files = [];
	        for (var i = 0; i < fileList.length; i++) {
	          files.push(fileList[i]);
	        }
	        upload.updateModel(ngModel, attr, scope, fileChangeAttr(),
	          files.length ? files : null, evt);
	      }
	    }

	    upload.registerModelChangeValidator(ngModel, attr, scope);

	    var unwatches = [];
	    unwatches.push(scope.$watch(attrGetter('ngfMultiple'), function () {
	      fileElem.attr('multiple', attrGetter('ngfMultiple', scope));
	    }));
	    unwatches.push(scope.$watch(attrGetter('ngfCapture'), function () {
	      fileElem.attr('capture', attrGetter('ngfCapture', scope));
	    }));
	    unwatches.push(scope.$watch(attrGetter('ngfAccept'), function () {
	      fileElem.attr('accept', attrGetter('ngfAccept', scope));
	    }));
	    attr.$observe('accept', function () {
	      fileElem.attr('accept', attrGetter('accept'));
	    });
	    unwatches.push(function () {
	      if (attr.$$observers) delete attr.$$observers.accept;
	    });
	    function bindAttrToFileInput(fileElem) {
	      if (elem !== fileElem) {
	        for (var i = 0; i < elem[0].attributes.length; i++) {
	          var attribute = elem[0].attributes[i];
	          if (attribute.name !== 'type' && attribute.name !== 'class' && attribute.name !== 'style') {
	            if (attribute.value == null || attribute.value === '') {
	              if (attribute.name === 'required') attribute.value = 'required';
	              if (attribute.name === 'multiple') attribute.value = 'multiple';
	            }
	            fileElem.attr(attribute.name, attribute.name === 'id' ? 'ngf-' + attribute.value : attribute.value);
	          }
	        }
	      }
	    }

	    function createFileInput() {
	      if (isInputTypeFile()) {
	        return elem;
	      }

	      var fileElem = angular.element('<input type="file">');

	      bindAttrToFileInput(fileElem);

	      var label = angular.element('<label>upload</label>');
	      label.css('visibility', 'hidden').css('position', 'absolute').css('overflow', 'hidden')
	        .css('width', '0px').css('height', '0px').css('border', 'none')
	        .css('margin', '0px').css('padding', '0px').attr('tabindex', '-1');
	      generatedElems.push({el: elem, ref: label});

	      document.body.appendChild(label.append(fileElem)[0]);

	      return fileElem;
	    }

	    var initialTouchStartY = 0;

	    function clickHandler(evt) {
	      if (elem.attr('disabled')) return false;
	      if (attrGetter('ngfSelectDisabled', scope)) return;

	      var r = handleTouch(evt);
	      if (r != null) return r;

	      resetModel(evt);

	      // fix for md when the element is removed from the DOM and added back #460
	      try {
	        if (!isInputTypeFile() && !document.body.contains(fileElem[0])) {
	          generatedElems.push({el: elem, ref: fileElem.parent()});
	          document.body.appendChild(fileElem.parent()[0]);
	          fileElem.bind('change', changeFn);
	        }
	      } catch(e){/*ignore*/}

	      if (isDelayedClickSupported(navigator.userAgent)) {
	        setTimeout(function () {
	          fileElem[0].click();
	        }, 0);
	      } else {
	        fileElem[0].click();
	      }

	      return false;
	    }

	    function handleTouch(evt) {
	      var touches = evt.changedTouches || (evt.originalEvent && evt.originalEvent.changedTouches);
	      if (evt.type === 'touchstart') {
	        initialTouchStartY = touches ? touches[0].clientY : 0;
	        return true; // don't block event default
	      } else {
	        evt.stopPropagation();
	        evt.preventDefault();

	        // prevent scroll from triggering event
	        if (evt.type === 'touchend') {
	          var currentLocation = touches ? touches[0].clientY : 0;
	          if (Math.abs(currentLocation - initialTouchStartY) > 20) return false;
	        }
	      }
	    }

	    var fileElem = elem;

	    function resetModel(evt) {
	      if (upload.shouldUpdateOn('click', attr, scope) && fileElem.val()) {
	        fileElem.val(null);
	        upload.updateModel(ngModel, attr, scope, fileChangeAttr(), null, evt, true);
	      }
	    }

	    if (!isInputTypeFile()) {
	      fileElem = createFileInput();
	    }
	    fileElem.bind('change', changeFn);

	    if (!isInputTypeFile()) {
	      elem.bind('click touchstart touchend', clickHandler);
	    } else {
	      elem.bind('click', resetModel);
	    }

	    function ie10SameFileSelectFix(evt) {
	      if (fileElem && !fileElem.attr('__ngf_ie10_Fix_')) {
	        if (!fileElem[0].parentNode) {
	          fileElem = null;
	          return;
	        }
	        evt.preventDefault();
	        evt.stopPropagation();
	        fileElem.unbind('click');
	        var clone = fileElem.clone();
	        fileElem.replaceWith(clone);
	        fileElem = clone;
	        fileElem.attr('__ngf_ie10_Fix_', 'true');
	        fileElem.bind('change', changeFn);
	        fileElem.bind('click', ie10SameFileSelectFix);
	        fileElem[0].click();
	        return false;
	      } else {
	        fileElem.removeAttr('__ngf_ie10_Fix_');
	      }
	    }

	    if (navigator.appVersion.indexOf('MSIE 10') !== -1) {
	      fileElem.bind('click', ie10SameFileSelectFix);
	    }

	    if (ngModel) ngModel.$formatters.push(function (val) {
	      if (val == null || val.length === 0) {
	        if (fileElem.val()) {
	          fileElem.val(null);
	        }
	      }
	      return val;
	    });

	    scope.$on('$destroy', function () {
	      if (!isInputTypeFile()) fileElem.parent().remove();
	      angular.forEach(unwatches, function (unwatch) {
	        unwatch();
	      });
	    });

	    $timeout(function () {
	      for (var i = 0; i < generatedElems.length; i++) {
	        var g = generatedElems[i];
	        if (!document.body.contains(g.el[0])) {
	          generatedElems.splice(i, 1);
	          g.ref.remove();
	        }
	      }
	    });

	    if (window.FileAPI && window.FileAPI.ngfFixIE) {
	      window.FileAPI.ngfFixIE(elem, fileElem, changeFn);
	    }
	  }

	  return {
	    restrict: 'AEC',
	    require: '?ngModel',
	    link: function (scope, elem, attr, ngModel) {
	      linkFileSelect(scope, elem, attr, ngModel, $parse, $timeout, $compile, Upload);
	    }
	  };
	}]);

	(function () {

	  ngFileUpload.service('UploadDataUrl', ['UploadBase', '$timeout', '$q', function (UploadBase, $timeout, $q) {
	    var upload = UploadBase;
	    upload.base64DataUrl = function (file) {
	      if (angular.isArray(file)) {
	        var d = $q.defer(), count = 0;
	        angular.forEach(file, function (f) {
	          upload.dataUrl(f, true)['finally'](function () {
	            count++;
	            if (count === file.length) {
	              var urls = [];
	              angular.forEach(file, function (ff) {
	                urls.push(ff.$ngfDataUrl);
	              });
	              d.resolve(urls, file);
	            }
	          });
	        });
	        return d.promise;
	      } else {
	        return upload.dataUrl(file, true);
	      }
	    };
	    upload.dataUrl = function (file, disallowObjectUrl) {
	      if (!file) return upload.emptyPromise(file, file);
	      if ((disallowObjectUrl && file.$ngfDataUrl != null) || (!disallowObjectUrl && file.$ngfBlobUrl != null)) {
	        return upload.emptyPromise(disallowObjectUrl ? file.$ngfDataUrl : file.$ngfBlobUrl, file);
	      }
	      var p = disallowObjectUrl ? file.$$ngfDataUrlPromise : file.$$ngfBlobUrlPromise;
	      if (p) return p;

	      var deferred = $q.defer();
	      $timeout(function () {
	        if (window.FileReader && file &&
	          (!window.FileAPI || navigator.userAgent.indexOf('MSIE 8') === -1 || file.size < 20000) &&
	          (!window.FileAPI || navigator.userAgent.indexOf('MSIE 9') === -1 || file.size < 4000000)) {
	          //prefer URL.createObjectURL for handling refrences to files of all sizes
	          //since it doesn´t build a large string in memory
	          var URL = window.URL || window.webkitURL;
	          if (URL && URL.createObjectURL && !disallowObjectUrl) {
	            var url;
	            try {
	              url = URL.createObjectURL(file);
	            } catch (e) {
	              $timeout(function () {
	                file.$ngfBlobUrl = '';
	                deferred.reject();
	              });
	              return;
	            }
	            $timeout(function () {
	              file.$ngfBlobUrl = url;
	              if (url) {
	                deferred.resolve(url, file);
	                upload.blobUrls = upload.blobUrls || [];
	                upload.blobUrlsTotalSize = upload.blobUrlsTotalSize || 0;
	                upload.blobUrls.push({url: url, size: file.size});
	                upload.blobUrlsTotalSize += file.size || 0;
	                var maxMemory = upload.defaults.blobUrlsMaxMemory || 268435456;
	                var maxLength = upload.defaults.blobUrlsMaxQueueSize || 200;
	                while ((upload.blobUrlsTotalSize > maxMemory || upload.blobUrls.length > maxLength) && upload.blobUrls.length > 1) {
	                  var obj = upload.blobUrls.splice(0, 1)[0];
	                  URL.revokeObjectURL(obj.url);
	                  upload.blobUrlsTotalSize -= obj.size;
	                }
	              }
	            });
	          } else {
	            var fileReader = new FileReader();
	            fileReader.onload = function (e) {
	              $timeout(function () {
	                file.$ngfDataUrl = e.target.result;
	                deferred.resolve(e.target.result, file);
	                $timeout(function () {
	                  delete file.$ngfDataUrl;
	                }, 1000);
	              });
	            };
	            fileReader.onerror = function () {
	              $timeout(function () {
	                file.$ngfDataUrl = '';
	                deferred.reject();
	              });
	            };
	            fileReader.readAsDataURL(file);
	          }
	        } else {
	          $timeout(function () {
	            file[disallowObjectUrl ? '$ngfDataUrl' : '$ngfBlobUrl'] = '';
	            deferred.reject();
	          });
	        }
	      });

	      if (disallowObjectUrl) {
	        p = file.$$ngfDataUrlPromise = deferred.promise;
	      } else {
	        p = file.$$ngfBlobUrlPromise = deferred.promise;
	      }
	      p['finally'](function () {
	        delete file[disallowObjectUrl ? '$$ngfDataUrlPromise' : '$$ngfBlobUrlPromise'];
	      });
	      return p;
	    };
	    return upload;
	  }]);

	  function getTagType(el) {
	    if (el.tagName.toLowerCase() === 'img') return 'image';
	    if (el.tagName.toLowerCase() === 'audio') return 'audio';
	    if (el.tagName.toLowerCase() === 'video') return 'video';
	    return /./;
	  }

	  function linkFileDirective(Upload, $timeout, scope, elem, attr, directiveName, resizeParams, isBackground) {
	    function constructDataUrl(file) {
	      var disallowObjectUrl = Upload.attrGetter('ngfNoObjectUrl', attr, scope);
	      Upload.dataUrl(file, disallowObjectUrl)['finally'](function () {
	        $timeout(function () {
	          var src = (disallowObjectUrl ? file.$ngfDataUrl : file.$ngfBlobUrl) || file.$ngfDataUrl;
	          if (isBackground) {
	            elem.css('background-image', 'url(\'' + (src || '') + '\')');
	          } else {
	            elem.attr('src', src);
	          }
	          if (src) {
	            elem.removeClass('ng-hide');
	          } else {
	            elem.addClass('ng-hide');
	          }
	        });
	      });
	    }

	    $timeout(function () {
	      var unwatch = scope.$watch(attr[directiveName], function (file) {
	        var size = resizeParams;
	        if (directiveName === 'ngfThumbnail') {
	          if (!size) {
	            size = {width: elem[0].clientWidth, height: elem[0].clientHeight};
	          }
	          if (size.width === 0 && window.getComputedStyle) {
	            var style = getComputedStyle(elem[0]);
	            size = {
	              width: parseInt(style.width.slice(0, -2)),
	              height: parseInt(style.height.slice(0, -2))
	            };
	          }
	        }

	        if (angular.isString(file)) {
	          elem.removeClass('ng-hide');
	          if (isBackground) {
	            return elem.css('background-image', 'url(\'' + file + '\')');
	          } else {
	            return elem.attr('src', file);
	          }
	        }
	        if (file && file.type && file.type.search(getTagType(elem[0])) === 0 &&
	          (!isBackground || file.type.indexOf('image') === 0)) {
	          if (size && Upload.isResizeSupported()) {
	            Upload.resize(file, size.width, size.height, size.quality).then(
	              function (f) {
	                constructDataUrl(f);
	              }, function (e) {
	                throw e;
	              }
	            );
	          } else {
	            constructDataUrl(file);
	          }
	        } else {
	          elem.addClass('ng-hide');
	        }
	      });

	      scope.$on('$destroy', function () {
	        unwatch();
	      });
	    });
	  }


	  /** @namespace attr.ngfSrc */
	  /** @namespace attr.ngfNoObjectUrl */
	  ngFileUpload.directive('ngfSrc', ['Upload', '$timeout', function (Upload, $timeout) {
	    return {
	      restrict: 'AE',
	      link: function (scope, elem, attr) {
	        linkFileDirective(Upload, $timeout, scope, elem, attr, 'ngfSrc',
	          Upload.attrGetter('ngfResize', attr, scope), false);
	      }
	    };
	  }]);

	  /** @namespace attr.ngfBackground */
	  /** @namespace attr.ngfNoObjectUrl */
	  ngFileUpload.directive('ngfBackground', ['Upload', '$timeout', function (Upload, $timeout) {
	    return {
	      restrict: 'AE',
	      link: function (scope, elem, attr) {
	        linkFileDirective(Upload, $timeout, scope, elem, attr, 'ngfBackground',
	          Upload.attrGetter('ngfResize', attr, scope), true);
	      }
	    };
	  }]);

	  /** @namespace attr.ngfThumbnail */
	  /** @namespace attr.ngfAsBackground */
	  /** @namespace attr.ngfSize */
	  /** @namespace attr.ngfNoObjectUrl */
	  ngFileUpload.directive('ngfThumbnail', ['Upload', '$timeout', function (Upload, $timeout) {
	    return {
	      restrict: 'AE',
	      link: function (scope, elem, attr) {
	        var size = Upload.attrGetter('ngfSize', attr, scope);
	        linkFileDirective(Upload, $timeout, scope, elem, attr, 'ngfThumbnail', size,
	          Upload.attrGetter('ngfAsBackground', attr, scope));
	      }
	    };
	  }]);

	  ngFileUpload.config(['$compileProvider', function ($compileProvider) {
	    if ($compileProvider.imgSrcSanitizationWhitelist) $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|local|file|data|blob):/);
	    if ($compileProvider.aHrefSanitizationWhitelist) $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|local|file|data|blob):/);
	  }]);

	  ngFileUpload.filter('ngfDataUrl', ['UploadDataUrl', '$sce', function (UploadDataUrl, $sce) {
	    return function (file, disallowObjectUrl, trustedUrl) {
	      if (angular.isString(file)) {
	        return $sce.trustAsResourceUrl(file);
	      }
	      var src = file && ((disallowObjectUrl ? file.$ngfDataUrl : file.$ngfBlobUrl) || file.$ngfDataUrl);
	      if (file && !src) {
	        if (!file.$ngfDataUrlFilterInProgress && angular.isObject(file)) {
	          file.$ngfDataUrlFilterInProgress = true;
	          UploadDataUrl.dataUrl(file, disallowObjectUrl);
	        }
	        return '';
	      }
	      if (file) delete file.$ngfDataUrlFilterInProgress;
	      return (file && src ? (trustedUrl ? $sce.trustAsResourceUrl(src) : src) : file) || '';
	    };
	  }]);

	})();

	ngFileUpload.service('UploadValidate', ['UploadDataUrl', '$q', '$timeout', function (UploadDataUrl, $q, $timeout) {
	  var upload = UploadDataUrl;

	  function globStringToRegex(str) {
	    var regexp = '', excludes = [];
	    if (str.length > 2 && str[0] === '/' && str[str.length - 1] === '/') {
	      regexp = str.substring(1, str.length - 1);
	    } else {
	      var split = str.split(',');
	      if (split.length > 1) {
	        for (var i = 0; i < split.length; i++) {
	          var r = globStringToRegex(split[i]);
	          if (r.regexp) {
	            regexp += '(' + r.regexp + ')';
	            if (i < split.length - 1) {
	              regexp += '|';
	            }
	          } else {
	            excludes = excludes.concat(r.excludes);
	          }
	        }
	      } else {
	        if (str.indexOf('!') === 0) {
	          excludes.push('^((?!' + globStringToRegex(str.substring(1)).regexp + ').)*$');
	        } else {
	          if (str.indexOf('.') === 0) {
	            str = '*' + str;
	          }
	          regexp = '^' + str.replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]', 'g'), '\\$&') + '$';
	          regexp = regexp.replace(/\\\*/g, '.*').replace(/\\\?/g, '.');
	        }
	      }
	    }
	    return {regexp: regexp, excludes: excludes};
	  }

	  upload.validatePattern = function (file, val) {
	    if (!val) {
	      return true;
	    }
	    var pattern = globStringToRegex(val), valid = true;
	    if (pattern.regexp && pattern.regexp.length) {
	      var regexp = new RegExp(pattern.regexp, 'i');
	      valid = (file.type != null && regexp.test(file.type)) ||
	        (file.name != null && regexp.test(file.name));
	    }
	    var len = pattern.excludes.length;
	    while (len--) {
	      var exclude = new RegExp(pattern.excludes[len], 'i');
	      valid = valid && (file.type == null || exclude.test(file.type)) &&
	        (file.name == null || exclude.test(file.name));
	    }
	    return valid;
	  };

	  upload.ratioToFloat = function (val) {
	    var r = val.toString(), xIndex = r.search(/[x:]/i);
	    if (xIndex > -1) {
	      r = parseFloat(r.substring(0, xIndex)) / parseFloat(r.substring(xIndex + 1));
	    } else {
	      r = parseFloat(r);
	    }
	    return r;
	  };

	  upload.registerModelChangeValidator = function (ngModel, attr, scope) {
	    if (ngModel) {
	      ngModel.$formatters.push(function (files) {
	        if (ngModel.$dirty) {
	          if (files && !angular.isArray(files)) {
	            files = [files];
	          }
	          upload.validate(files, 0, ngModel, attr, scope).then(function () {
	            upload.applyModelValidation(ngModel, files);
	          });
	        }
	      });
	    }
	  };

	  function markModelAsDirty(ngModel, files) {
	    if (files != null && !ngModel.$dirty) {
	      if (ngModel.$setDirty) {
	        ngModel.$setDirty();
	      } else {
	        ngModel.$dirty = true;
	      }
	    }
	  }

	  upload.applyModelValidation = function (ngModel, files) {
	    markModelAsDirty(ngModel, files);
	    angular.forEach(ngModel.$ngfValidations, function (validation) {
	      ngModel.$setValidity(validation.name, validation.valid);
	    });
	  };

	  upload.getValidationAttr = function (attr, scope, name, validationName, file) {
	    var dName = 'ngf' + name[0].toUpperCase() + name.substr(1);
	    var val = upload.attrGetter(dName, attr, scope, {$file: file});
	    if (val == null) {
	      val = upload.attrGetter('ngfValidate', attr, scope, {$file: file});
	      if (val) {
	        var split = (validationName || name).split('.');
	        val = val[split[0]];
	        if (split.length > 1) {
	          val = val && val[split[1]];
	        }
	      }
	    }
	    return val;
	  };

	  upload.validate = function (files, prevLength, ngModel, attr, scope) {
	    ngModel = ngModel || {};
	    ngModel.$ngfValidations = ngModel.$ngfValidations || [];

	    angular.forEach(ngModel.$ngfValidations, function (v) {
	      v.valid = true;
	    });

	    var attrGetter = function (name, params) {
	      return upload.attrGetter(name, attr, scope, params);
	    };

	    if (files == null || files.length === 0) {
	      return upload.emptyPromise(ngModel);
	    }

	    files = files.length === undefined ? [files] : files.slice(0);

	    function validateSync(name, validationName, fn) {
	      if (files) {
	        var i = files.length, valid = null;
	        while (i--) {
	          var file = files[i];
	          if (file) {
	            var val = upload.getValidationAttr(attr, scope, name, validationName, file);
	            if (val != null) {
	              if (!fn(file, val, i)) {
	                file.$error = name;
	                (file.$errorMessages = (file.$errorMessages || {}))[name] = true;
	                file.$errorParam = val;
	                files.splice(i, 1);
	                valid = false;
	              }
	            }
	          }
	        }
	        if (valid !== null) {
	          ngModel.$ngfValidations.push({name: name, valid: valid});
	        }
	      }
	    }

	    validateSync('maxFiles', null, function (file, val, i) {
	      return prevLength + i < val;
	    });
	    validateSync('pattern', null, upload.validatePattern);
	    validateSync('minSize', 'size.min', function (file, val) {
	      return file.size + 0.1 >= upload.translateScalars(val);
	    });
	    validateSync('maxSize', 'size.max', function (file, val) {
	      return file.size - 0.1 <= upload.translateScalars(val);
	    });
	    var totalSize = 0;
	    validateSync('maxTotalSize', null, function (file, val) {
	      totalSize += file.size;
	      if (totalSize > upload.translateScalars(val)) {
	        files.splice(0, files.length);
	        return false;
	      }
	      return true;
	    });

	    validateSync('validateFn', null, function (file, r) {
	      return r === true || r === null || r === '';
	    });

	    if (!files.length) {
	      return upload.emptyPromise(ngModel, ngModel.$ngfValidations);
	    }

	    function validateAsync(name, validationName, type, asyncFn, fn) {
	      function resolveResult(defer, file, val) {
	        if (val != null) {
	          asyncFn(file, val).then(function (d) {
	            if (!fn(d, val)) {
	              file.$error = name;
	              (file.$errorMessages = (file.$errorMessages || {}))[name] = true;
	              file.$errorParam = val;
	              defer.reject();
	            } else {
	              defer.resolve();
	            }
	          }, function () {
	            if (attrGetter('ngfValidateForce', {$file: file})) {
	              file.$error = name;
	              (file.$errorMessages = (file.$errorMessages || {}))[name] = true;
	              file.$errorParam = val;
	              defer.reject();
	            } else {
	              defer.resolve();
	            }
	          });
	        } else {
	          defer.resolve();
	        }
	      }

	      var promises = [upload.emptyPromise()];
	      if (files) {
	        files = files.length === undefined ? [files] : files;
	        angular.forEach(files, function (file) {
	          var defer = $q.defer();
	          promises.push(defer.promise);
	          if (type && (file.type == null || file.type.search(type) !== 0)) {
	            defer.resolve();
	            return;
	          }
	          if (name === 'dimensions' && upload.attrGetter('ngfDimensions', attr) != null) {
	            upload.imageDimensions(file).then(function (d) {
	              resolveResult(defer, file,
	                attrGetter('ngfDimensions', {$file: file, $width: d.width, $height: d.height}));
	            }, function () {
	              defer.reject();
	            });
	          } else if (name === 'duration' && upload.attrGetter('ngfDuration', attr) != null) {
	            upload.mediaDuration(file).then(function (d) {
	              resolveResult(defer, file,
	                attrGetter('ngfDuration', {$file: file, $duration: d}));
	            }, function () {
	              defer.reject();
	            });
	          } else {
	            resolveResult(defer, file,
	              upload.getValidationAttr(attr, scope, name, validationName, file));
	          }
	        });
	        return $q.all(promises).then(function () {
	          ngModel.$ngfValidations.push({name: name, valid: true});
	        }, function () {
	          ngModel.$ngfValidations.push({name: name, valid: false});
	        });
	      }
	    }

	    var deffer = $q.defer();
	    var promises = [];

	    promises.push(upload.happyPromise(validateAsync('maxHeight', 'height.max', /image/,
	      this.imageDimensions, function (d, val) {
	        return d.height <= val;
	      })));
	    promises.push(upload.happyPromise(validateAsync('minHeight', 'height.min', /image/,
	      this.imageDimensions, function (d, val) {
	        return d.height >= val;
	      })));
	    promises.push(upload.happyPromise(validateAsync('maxWidth', 'width.max', /image/,
	      this.imageDimensions, function (d, val) {
	        return d.width <= val;
	      })));
	    promises.push(upload.happyPromise(validateAsync('minWidth', 'width.min', /image/,
	      this.imageDimensions, function (d, val) {
	        return d.width >= val;
	      })));
	    promises.push(upload.happyPromise(validateAsync('dimensions', null, /image/,
	      function (file, val) {
	        return upload.emptyPromise(val);
	      }, function (r) {
	        return r;
	      })));
	    promises.push(upload.happyPromise(validateAsync('ratio', null, /image/,
	      this.imageDimensions, function (d, val) {
	        var split = val.toString().split(','), valid = false;
	        for (var i = 0; i < split.length; i++) {
	          if (Math.abs((d.width / d.height) - upload.ratioToFloat(split[i])) < 0.0001) {
	            valid = true;
	          }
	        }
	        return valid;
	      })));
	    promises.push(upload.happyPromise(validateAsync('maxRatio', 'ratio.max', /image/,
	      this.imageDimensions, function (d, val) {
	        return (d.width / d.height) - upload.ratioToFloat(val) < 0.0001;
	      })));
	    promises.push(upload.happyPromise(validateAsync('minRatio', 'ratio.min', /image/,
	      this.imageDimensions, function (d, val) {
	        return (d.width / d.height) - upload.ratioToFloat(val) > -0.0001;
	      })));
	    promises.push(upload.happyPromise(validateAsync('maxDuration', 'duration.max', /audio|video/,
	      this.mediaDuration, function (d, val) {
	        return d <= upload.translateScalars(val);
	      })));
	    promises.push(upload.happyPromise(validateAsync('minDuration', 'duration.min', /audio|video/,
	      this.mediaDuration, function (d, val) {
	        return d >= upload.translateScalars(val);
	      })));
	    promises.push(upload.happyPromise(validateAsync('duration', null, /audio|video/,
	      function (file, val) {
	        return upload.emptyPromise(val);
	      }, function (r) {
	        return r;
	      })));

	    promises.push(upload.happyPromise(validateAsync('validateAsyncFn', null, null,
	      function (file, val) {
	        return val;
	      }, function (r) {
	        return r === true || r === null || r === '';
	      })));

	    return $q.all(promises).then(function () {
	      deffer.resolve(ngModel, ngModel.$ngfValidations);
	    });
	  };

	  upload.imageDimensions = function (file) {
	    if (file.$ngfWidth && file.$ngfHeight) {
	      var d = $q.defer();
	      $timeout(function () {
	        d.resolve({width: file.$ngfWidth, height: file.$ngfHeight});
	      });
	      return d.promise;
	    }
	    if (file.$ngfDimensionPromise) return file.$ngfDimensionPromise;

	    var deferred = $q.defer();
	    $timeout(function () {
	      if (file.type.indexOf('image') !== 0) {
	        deferred.reject('not image');
	        return;
	      }
	      upload.dataUrl(file).then(function (dataUrl) {
	        var img = angular.element('<img>').attr('src', dataUrl)
	          .css('visibility', 'hidden').css('position', 'fixed')
	          .css('max-width', 'none !important').css('max-height', 'none !important');

	        function success() {
	          var width = img[0].clientWidth;
	          var height = img[0].clientHeight;
	          img.remove();
	          file.$ngfWidth = width;
	          file.$ngfHeight = height;
	          deferred.resolve({width: width, height: height});
	        }

	        function error() {
	          img.remove();
	          deferred.reject('load error');
	        }

	        img.on('load', success);
	        img.on('error', error);
	        var count = 0;

	        function checkLoadError() {
	          $timeout(function () {
	            if (img[0].parentNode) {
	              if (img[0].clientWidth) {
	                success();
	              } else if (count > 10) {
	                error();
	              } else {
	                checkLoadError();
	              }
	            }
	          }, 1000);
	        }

	        checkLoadError();

	        angular.element(document.getElementsByTagName('body')[0]).append(img);
	      }, function () {
	        deferred.reject('load error');
	      });
	    });

	    file.$ngfDimensionPromise = deferred.promise;
	    file.$ngfDimensionPromise['finally'](function () {
	      delete file.$ngfDimensionPromise;
	    });
	    return file.$ngfDimensionPromise;
	  };

	  upload.mediaDuration = function (file) {
	    if (file.$ngfDuration) {
	      var d = $q.defer();
	      $timeout(function () {
	        d.resolve(file.$ngfDuration);
	      });
	      return d.promise;
	    }
	    if (file.$ngfDurationPromise) return file.$ngfDurationPromise;

	    var deferred = $q.defer();
	    $timeout(function () {
	      if (file.type.indexOf('audio') !== 0 && file.type.indexOf('video') !== 0) {
	        deferred.reject('not media');
	        return;
	      }
	      upload.dataUrl(file).then(function (dataUrl) {
	        var el = angular.element(file.type.indexOf('audio') === 0 ? '<audio>' : '<video>')
	          .attr('src', dataUrl).css('visibility', 'none').css('position', 'fixed');

	        function success() {
	          var duration = el[0].duration;
	          file.$ngfDuration = duration;
	          el.remove();
	          deferred.resolve(duration);
	        }

	        function error() {
	          el.remove();
	          deferred.reject('load error');
	        }

	        el.on('loadedmetadata', success);
	        el.on('error', error);
	        var count = 0;

	        function checkLoadError() {
	          $timeout(function () {
	            if (el[0].parentNode) {
	              if (el[0].duration) {
	                success();
	              } else if (count > 10) {
	                error();
	              } else {
	                checkLoadError();
	              }
	            }
	          }, 1000);
	        }

	        checkLoadError();

	        angular.element(document.body).append(el);
	      }, function () {
	        deferred.reject('load error');
	      });
	    });

	    file.$ngfDurationPromise = deferred.promise;
	    file.$ngfDurationPromise['finally'](function () {
	      delete file.$ngfDurationPromise;
	    });
	    return file.$ngfDurationPromise;
	  };
	  return upload;
	}
	]);

	ngFileUpload.service('UploadResize', ['UploadValidate', '$q', function (UploadValidate, $q) {
	  var upload = UploadValidate;

	  /**
	   * Conserve aspect ratio of the original region. Useful when shrinking/enlarging
	   * images to fit into a certain area.
	   * Source:  http://stackoverflow.com/a/14731922
	   *
	   * @param {Number} srcWidth Source area width
	   * @param {Number} srcHeight Source area height
	   * @param {Number} maxWidth Nestable area maximum available width
	   * @param {Number} maxHeight Nestable area maximum available height
	   * @return {Object} { width, height }
	   */
	  var calculateAspectRatioFit = function (srcWidth, srcHeight, maxWidth, maxHeight, centerCrop) {
	    var ratio = centerCrop ? Math.max(maxWidth / srcWidth, maxHeight / srcHeight) :
	      Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
	    return {
	      width: srcWidth * ratio, height: srcHeight * ratio,
	      marginX: srcWidth * ratio - maxWidth, marginY: srcHeight * ratio - maxHeight
	    };
	  };

	  // Extracted from https://github.com/romelgomez/angular-firebase-image-upload/blob/master/app/scripts/fileUpload.js#L89
	  var resize = function (imagen, width, height, quality, type, ratio, centerCrop, resizeIf) {
	    var deferred = $q.defer();
	    var canvasElement = document.createElement('canvas');
	    var imageElement = document.createElement('img');

	    imageElement.onload = function () {
	      if (resizeIf != null && resizeIf(imageElement.width, imageElement.height) === false) {
	        deferred.reject('resizeIf');
	        return;
	      }
	      try {
	        if (ratio) {
	          var ratioFloat = upload.ratioToFloat(ratio);
	          var imgRatio = imageElement.width / imageElement.height;
	          if (imgRatio < ratioFloat) {
	            width = imageElement.width;
	            height = width / ratioFloat;
	          } else {
	            height = imageElement.height;
	            width = height * ratioFloat;
	          }
	        }
	        if (!width) {
	          width = imageElement.width;
	        }
	        if (!height) {
	          height = imageElement.height;
	        }
	        var dimensions = calculateAspectRatioFit(imageElement.width, imageElement.height, width, height, centerCrop);
	        canvasElement.width = Math.min(dimensions.width, width);
	        canvasElement.height = Math.min(dimensions.height, height);
	        var context = canvasElement.getContext('2d');
	        context.drawImage(imageElement,
	          Math.min(0, -dimensions.marginX / 2), Math.min(0, -dimensions.marginY / 2),
	          dimensions.width, dimensions.height);
	        deferred.resolve(canvasElement.toDataURL(type || 'image/WebP', quality || 0.934));
	      } catch (e) {
	        deferred.reject(e);
	      }
	    };
	    imageElement.onerror = function () {
	      deferred.reject();
	    };
	    imageElement.src = imagen;
	    return deferred.promise;
	  };

	  upload.dataUrltoBlob = function (dataurl, name, origSize) {
	    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
	      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
	    while (n--) {
	      u8arr[n] = bstr.charCodeAt(n);
	    }
	    var blob = new window.Blob([u8arr], {type: mime});
	    blob.name = name;
	    blob.$ngfOrigSize = origSize;
	    return blob;
	  };

	  upload.isResizeSupported = function () {
	    var elem = document.createElement('canvas');
	    return window.atob && elem.getContext && elem.getContext('2d') && window.Blob;
	  };

	  if (upload.isResizeSupported()) {
	    // add name getter to the blob constructor prototype
	    Object.defineProperty(window.Blob.prototype, 'name', {
	      get: function () {
	        return this.$ngfName;
	      },
	      set: function (v) {
	        this.$ngfName = v;
	      },
	      configurable: true
	    });
	  }

	  upload.resize = function (file, width, height, quality, type, ratio, centerCrop, resizeIf, restoreExif) {
	    if (file.type.indexOf('image') !== 0) return upload.emptyPromise(file);

	    var deferred = $q.defer();
	    upload.dataUrl(file, true).then(function (url) {
	      resize(url, width, height, quality, type || file.type, ratio, centerCrop, resizeIf)
	        .then(function (dataUrl) {
	          if (file.type === 'image/jpeg' && restoreExif) {
	            try {
	              dataUrl = upload.restoreExif(url, dataUrl);
	            } catch (e) {
	              setTimeout(function () {throw e;}, 1);
	            }
	          }
	          try {
	            var blob = upload.dataUrltoBlob(dataUrl, file.name, file.size);
	            deferred.resolve(blob);
	          } catch (e) {
	            deferred.reject(e);
	          }
	        }, function (r) {
	          if (r === 'resizeIf') {
	            deferred.resolve(file);
	          }
	          deferred.reject(r);
	        });
	    }, function (e) {
	      deferred.reject(e);
	    });
	    return deferred.promise;
	  };

	  return upload;
	}]);

	(function () {
	  ngFileUpload.directive('ngfDrop', ['$parse', '$timeout', '$location', 'Upload', '$http', '$q',
	    function ($parse, $timeout, $location, Upload, $http, $q) {
	      return {
	        restrict: 'AEC',
	        require: '?ngModel',
	        link: function (scope, elem, attr, ngModel) {
	          linkDrop(scope, elem, attr, ngModel, $parse, $timeout, $location, Upload, $http, $q);
	        }
	      };
	    }]);

	  ngFileUpload.directive('ngfNoFileDrop', function () {
	    return function (scope, elem) {
	      if (dropAvailable()) elem.css('display', 'none');
	    };
	  });

	  ngFileUpload.directive('ngfDropAvailable', ['$parse', '$timeout', 'Upload', function ($parse, $timeout, Upload) {
	    return function (scope, elem, attr) {
	      if (dropAvailable()) {
	        var model = $parse(Upload.attrGetter('ngfDropAvailable', attr));
	        $timeout(function () {
	          model(scope);
	          if (model.assign) {
	            model.assign(scope, true);
	          }
	        });
	      }
	    };
	  }]);

	  function linkDrop(scope, elem, attr, ngModel, $parse, $timeout, $location, upload, $http, $q) {
	    var available = dropAvailable();

	    var attrGetter = function (name, scope, params) {
	      return upload.attrGetter(name, attr, scope, params);
	    };

	    if (attrGetter('dropAvailable')) {
	      $timeout(function () {
	        if (scope[attrGetter('dropAvailable')]) {
	          scope[attrGetter('dropAvailable')].value = available;
	        } else {
	          scope[attrGetter('dropAvailable')] = available;
	        }
	      });
	    }
	    if (!available) {
	      if (attrGetter('ngfHideOnDropNotAvailable', scope) === true) {
	        elem.css('display', 'none');
	      }
	      return;
	    }

	    function isDisabled() {
	      return elem.attr('disabled') || attrGetter('ngfDropDisabled', scope);
	    }

	    if (attrGetter('ngfSelect') == null) {
	      upload.registerModelChangeValidator(ngModel, attr, scope);
	    }

	    var leaveTimeout = null;
	    var stopPropagation = $parse(attrGetter('ngfStopPropagation'));
	    var dragOverDelay = 1;
	    var actualDragOverClass;

	    elem[0].addEventListener('dragover', function (evt) {
	      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
	      evt.preventDefault();
	      if (stopPropagation(scope)) evt.stopPropagation();
	      // handling dragover events from the Chrome download bar
	      if (navigator.userAgent.indexOf('Chrome') > -1) {
	        var b = evt.dataTransfer.effectAllowed;
	        evt.dataTransfer.dropEffect = ('move' === b || 'linkMove' === b) ? 'move' : 'copy';
	      }
	      $timeout.cancel(leaveTimeout);
	      if (!actualDragOverClass) {
	        actualDragOverClass = 'C';
	        calculateDragOverClass(scope, attr, evt, function (clazz) {
	          actualDragOverClass = clazz;
	          elem.addClass(actualDragOverClass);
	          attrGetter('ngfDrag', scope, {$isDragging: true, $class: actualDragOverClass, $event: evt});
	        });
	      }
	    }, false);
	    elem[0].addEventListener('dragenter', function (evt) {
	      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
	      evt.preventDefault();
	      if (stopPropagation(scope)) evt.stopPropagation();
	    }, false);
	    elem[0].addEventListener('dragleave', function (evt) {
	      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
	      evt.preventDefault();
	      if (stopPropagation(scope)) evt.stopPropagation();
	      leaveTimeout = $timeout(function () {
	        if (actualDragOverClass) elem.removeClass(actualDragOverClass);
	        actualDragOverClass = null;
	        attrGetter('ngfDrag', scope, {$isDragging: false, $event: evt});
	      }, dragOverDelay || 100);
	    }, false);
	    elem[0].addEventListener('drop', function (evt) {
	      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
	      evt.preventDefault();
	      if (stopPropagation(scope)) evt.stopPropagation();
	      if (actualDragOverClass) elem.removeClass(actualDragOverClass);
	      actualDragOverClass = null;
	      var items = evt.dataTransfer.items;
	      var html;
	      try {
	        html = evt.dataTransfer && evt.dataTransfer.getData && evt.dataTransfer.getData('text/html');
	      } catch (e) {/* Fix IE11 that throw error calling getData */
	      }

	      extractFiles(items, evt.dataTransfer.files, attrGetter('ngfAllowDir', scope) !== false,
	        attrGetter('multiple') || attrGetter('ngfMultiple', scope)).then(function (files) {
	        if (files.length) {
	          updateModel(files, evt);
	        } else {
	          extractFilesFromHtml('dropUrl', html).then(function (files) {
	            updateModel(files, evt);
	          });
	        }
	      });
	    }, false);
	    elem[0].addEventListener('paste', function (evt) {
	      if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1 &&
	        attrGetter('ngfEnableFirefoxPaste', scope)) {
	        evt.preventDefault();
	      }
	      if (isDisabled() || !upload.shouldUpdateOn('paste', attr, scope)) return;
	      var files = [];
	      var clipboard = evt.clipboardData || evt.originalEvent.clipboardData;
	      if (clipboard && clipboard.items) {
	        for (var k = 0; k < clipboard.items.length; k++) {
	          if (clipboard.items[k].type.indexOf('image') !== -1) {
	            files.push(clipboard.items[k].getAsFile());
	          }
	        }
	      }
	      if (files.length) {
	        updateModel(files, evt);
	      } else {
	        extractFilesFromHtml('pasteUrl', clipboard).then(function (files) {
	          updateModel(files, evt);
	        });
	      }
	    }, false);

	    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1 &&
	      attrGetter('ngfEnableFirefoxPaste', scope)) {
	      elem.attr('contenteditable', true);
	      elem.on('keypress', function (e) {
	        if (!e.metaKey && !e.ctrlKey) {
	          e.preventDefault();
	        }
	      });
	    }

	    function updateModel(files, evt) {
	      upload.updateModel(ngModel, attr, scope, attrGetter('ngfChange') || attrGetter('ngfDrop'), files, evt);
	    }

	    function extractFilesFromHtml(updateOn, html) {
	      if (!upload.shouldUpdateOn(updateOn, attr, scope) || !html) return upload.rejectPromise([]);
	      var urls = [];
	      html.replace(/<(img src|img [^>]* src) *=\"([^\"]*)\"/gi, function (m, n, src) {
	        urls.push(src);
	      });
	      var promises = [], files = [];
	      if (urls.length) {
	        angular.forEach(urls, function (url) {
	          promises.push(upload.urlToBlob(url).then(function (blob) {
	            files.push(blob);
	          }));
	        });
	        var defer = $q.defer();
	        $q.all(promises).then(function () {
	          defer.resolve(files);
	        }, function (e) {
	          defer.reject(e);
	        });
	        return defer.promise;
	      }
	      return upload.emptyPromise();
	    }

	    function calculateDragOverClass(scope, attr, evt, callback) {
	      var obj = attrGetter('ngfDragOverClass', scope, {$event: evt}), dClass = 'dragover';
	      if (angular.isString(obj)) {
	        dClass = obj;
	      } else if (obj) {
	        if (obj.delay) dragOverDelay = obj.delay;
	        if (obj.accept || obj.reject) {
	          var items = evt.dataTransfer.items;
	          if (items == null || !items.length) {
	            dClass = obj.accept;
	          } else {
	            var pattern = obj.pattern || attrGetter('ngfPattern', scope, {$event: evt});
	            var len = items.length;
	            while (len--) {
	              if (!upload.validatePattern(items[len], pattern)) {
	                dClass = obj.reject;
	                break;
	              } else {
	                dClass = obj.accept;
	              }
	            }
	          }
	        }
	      }
	      callback(dClass);
	    }

	    function extractFiles(items, fileList, allowDir, multiple) {
	      var maxFiles = upload.getValidationAttr(attr, scope, 'maxFiles') || Number.MAX_VALUE;
	      var maxTotalSize = upload.getValidationAttr(attr, scope, 'maxTotalSize') || Number.MAX_VALUE;
	      var includeDir = attrGetter('ngfIncludeDir', scope);
	      var files = [], totalSize = 0;

	      function traverseFileTree(entry, path) {
	        var defer = $q.defer();
	        if (entry != null) {
	          if (entry.isDirectory) {
	            var promises = [upload.emptyPromise()];
	            if (includeDir) {
	              var file = {type: 'directory'};
	              file.name = file.path = (path || '') + entry.name + entry.name;
	              files.push(file);
	            }
	            var dirReader = entry.createReader();
	            var entries = [];
	            var readEntries = function () {
	              dirReader.readEntries(function (results) {
	                try {
	                  if (!results.length) {
	                    angular.forEach(entries.slice(0), function (e) {
	                      if (files.length <= maxFiles && totalSize <= maxTotalSize) {
	                        promises.push(traverseFileTree(e, (path ? path : '') + entry.name + '/'));
	                      }
	                    });
	                    $q.all(promises).then(function () {
	                      defer.resolve();
	                    }, function (e) {
	                      defer.reject(e);
	                    });
	                  } else {
	                    entries = entries.concat(Array.prototype.slice.call(results || [], 0));
	                    readEntries();
	                  }
	                } catch (e) {
	                  defer.reject(e);
	                }
	              }, function (e) {
	                defer.reject(e);
	              });
	            };
	            readEntries();
	          } else {
	            entry.file(function (file) {
	              try {
	                file.path = (path ? path : '') + file.name;
	                if (includeDir) {
	                  file = upload.rename(file, file.path);
	                }
	                files.push(file);
	                totalSize += file.size;
	                defer.resolve();
	              } catch (e) {
	                defer.reject(e);
	              }
	            }, function (e) {
	              defer.reject(e);
	            });
	          }
	        }
	        return defer.promise;
	      }

	      var promises = [upload.emptyPromise()];

	      if (items && items.length > 0 && $location.protocol() !== 'file') {
	        for (var i = 0; i < items.length; i++) {
	          if (items[i].webkitGetAsEntry && items[i].webkitGetAsEntry() && items[i].webkitGetAsEntry().isDirectory) {
	            var entry = items[i].webkitGetAsEntry();
	            if (entry.isDirectory && !allowDir) {
	              continue;
	            }
	            if (entry != null) {
	              promises.push(traverseFileTree(entry));
	            }
	          } else {
	            var f = items[i].getAsFile();
	            if (f != null) {
	              files.push(f);
	              totalSize += f.size;
	            }
	          }
	          if (files.length > maxFiles || totalSize > maxTotalSize ||
	            (!multiple && files.length > 0)) break;
	        }
	      } else {
	        if (fileList != null) {
	          for (var j = 0; j < fileList.length; j++) {
	            var file = fileList.item(j);
	            if (file.type || file.size > 0) {
	              files.push(file);
	              totalSize += file.size;
	            }
	            if (files.length > maxFiles || totalSize > maxTotalSize ||
	              (!multiple && files.length > 0)) break;
	          }
	        }
	      }

	      var defer = $q.defer();
	      $q.all(promises).then(function () {
	        if (!multiple && !includeDir && files.length) {
	          var i = 0;
	          while (files[i] && files[i].type === 'directory') i++;
	          defer.resolve([files[i]]);
	        } else {
	          defer.resolve(files);
	        }
	      }, function (e) {
	        defer.reject(e);
	      });

	      return defer.promise;
	    }
	  }

	  function dropAvailable() {
	    var div = document.createElement('div');
	    return ('draggable' in div) && ('ondrop' in div) && !/Edge\/12./i.test(navigator.userAgent);
	  }

	})();

	// customized version of https://github.com/exif-js/exif-js
	ngFileUpload.service('UploadExif', ['UploadResize', '$q', function (UploadResize, $q) {
	  var upload = UploadResize;

	  upload.isExifSupported = function () {
	    return window.FileReader && new FileReader().readAsArrayBuffer && upload.isResizeSupported();
	  };

	  function applyTransform(ctx, orientation, width, height) {
	    switch (orientation) {
	      case 2:
	        return ctx.transform(-1, 0, 0, 1, width, 0);
	      case 3:
	        return ctx.transform(-1, 0, 0, -1, width, height);
	      case 4:
	        return ctx.transform(1, 0, 0, -1, 0, height);
	      case 5:
	        return ctx.transform(0, 1, 1, 0, 0, 0);
	      case 6:
	        return ctx.transform(0, 1, -1, 0, height, 0);
	      case 7:
	        return ctx.transform(0, -1, -1, 0, height, width);
	      case 8:
	        return ctx.transform(0, -1, 1, 0, 0, width);
	    }
	  }

	  upload.readOrientation = function (file) {
	    var defer = $q.defer();
	    var reader = new FileReader();
	    var slicedFile = file.slice ? file.slice(0, 64 * 1024) : file;
	    reader.readAsArrayBuffer(slicedFile);
	    reader.onerror = function (e) {
	      return defer.reject(e);
	    };
	    reader.onload = function (e) {
	      var result = {orientation: 1};
	      var view = new DataView(this.result);
	      if (view.getUint16(0, false) !== 0xFFD8) return defer.resolve(result);

	      var length = view.byteLength,
	        offset = 2;
	      while (offset < length) {
	        var marker = view.getUint16(offset, false);
	        offset += 2;
	        if (marker === 0xFFE1) {
	          if (view.getUint32(offset += 2, false) !== 0x45786966) return defer.resolve(result);

	          var little = view.getUint16(offset += 6, false) === 0x4949;
	          offset += view.getUint32(offset + 4, little);
	          var tags = view.getUint16(offset, little);
	          offset += 2;
	          for (var i = 0; i < tags; i++)
	            if (view.getUint16(offset + (i * 12), little) === 0x0112) {
	              var orientation = view.getUint16(offset + (i * 12) + 8, little);
	              if (orientation >= 2 && orientation <= 8) {
	                view.setUint16(offset + (i * 12) + 8, 1, little);
	                result.fixedArrayBuffer = e.target.result;
	              }
	              result.orientation = orientation;
	              return defer.resolve(result);
	            }
	        } else if ((marker & 0xFF00) !== 0xFF00) break;
	        else offset += view.getUint16(offset, false);
	      }
	      return defer.resolve(result);
	    };
	    return defer.promise;
	  };

	  function arrayBufferToBase64(buffer) {
	    var binary = '';
	    var bytes = new Uint8Array(buffer);
	    var len = bytes.byteLength;
	    for (var i = 0; i < len; i++) {
	      binary += String.fromCharCode(bytes[i]);
	    }
	    return window.btoa(binary);
	  }

	  upload.applyExifRotation = function (file) {
	    if (file.type.indexOf('image/jpeg') !== 0) {
	      return upload.emptyPromise(file);
	    }

	    var deferred = $q.defer();
	    upload.readOrientation(file).then(function (result) {
	      if (result.orientation < 2 || result.orientation > 8) {
	        return deferred.resolve(file);
	      }
	      upload.dataUrl(file, true).then(function (url) {
	        var canvas = document.createElement('canvas');
	        var img = document.createElement('img');

	        img.onload = function () {
	          try {
	            canvas.width = result.orientation > 4 ? img.height : img.width;
	            canvas.height = result.orientation > 4 ? img.width : img.height;
	            var ctx = canvas.getContext('2d');
	            applyTransform(ctx, result.orientation, img.width, img.height);
	            ctx.drawImage(img, 0, 0);
	            var dataUrl = canvas.toDataURL(file.type || 'image/WebP', 0.934);
	            dataUrl = upload.restoreExif(arrayBufferToBase64(result.fixedArrayBuffer), dataUrl);
	            var blob = upload.dataUrltoBlob(dataUrl, file.name);
	            deferred.resolve(blob);
	          } catch (e) {
	            return deferred.reject(e);
	          }
	        };
	        img.onerror = function () {
	          deferred.reject();
	        };
	        img.src = url;
	      }, function (e) {
	        deferred.reject(e);
	      });
	    }, function (e) {
	      deferred.reject(e);
	    });
	    return deferred.promise;
	  };

	  upload.restoreExif = function (orig, resized) {
	    var ExifRestorer = {};

	    ExifRestorer.KEY_STR = 'ABCDEFGHIJKLMNOP' +
	      'QRSTUVWXYZabcdef' +
	      'ghijklmnopqrstuv' +
	      'wxyz0123456789+/' +
	      '=';

	    ExifRestorer.encode64 = function (input) {
	      var output = '',
	        chr1, chr2, chr3 = '',
	        enc1, enc2, enc3, enc4 = '',
	        i = 0;

	      do {
	        chr1 = input[i++];
	        chr2 = input[i++];
	        chr3 = input[i++];

	        enc1 = chr1 >> 2;
	        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
	        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
	        enc4 = chr3 & 63;

	        if (isNaN(chr2)) {
	          enc3 = enc4 = 64;
	        } else if (isNaN(chr3)) {
	          enc4 = 64;
	        }

	        output = output +
	          this.KEY_STR.charAt(enc1) +
	          this.KEY_STR.charAt(enc2) +
	          this.KEY_STR.charAt(enc3) +
	          this.KEY_STR.charAt(enc4);
	        chr1 = chr2 = chr3 = '';
	        enc1 = enc2 = enc3 = enc4 = '';
	      } while (i < input.length);

	      return output;
	    };

	    ExifRestorer.restore = function (origFileBase64, resizedFileBase64) {
	      if (origFileBase64.match('data:image/jpeg;base64,')) {
	        origFileBase64 = origFileBase64.replace('data:image/jpeg;base64,', '');
	      }

	      var rawImage = this.decode64(origFileBase64);
	      var segments = this.slice2Segments(rawImage);

	      var image = this.exifManipulation(resizedFileBase64, segments);

	      return 'data:image/jpeg;base64,' + this.encode64(image);
	    };


	    ExifRestorer.exifManipulation = function (resizedFileBase64, segments) {
	      var exifArray = this.getExifArray(segments),
	        newImageArray = this.insertExif(resizedFileBase64, exifArray);
	      return new Uint8Array(newImageArray);
	    };


	    ExifRestorer.getExifArray = function (segments) {
	      var seg;
	      for (var x = 0; x < segments.length; x++) {
	        seg = segments[x];
	        if (seg[0] === 255 & seg[1] === 225) //(ff e1)
	        {
	          return seg;
	        }
	      }
	      return [];
	    };


	    ExifRestorer.insertExif = function (resizedFileBase64, exifArray) {
	      var imageData = resizedFileBase64.replace('data:image/jpeg;base64,', ''),
	        buf = this.decode64(imageData),
	        separatePoint = buf.indexOf(255, 3),
	        mae = buf.slice(0, separatePoint),
	        ato = buf.slice(separatePoint),
	        array = mae;

	      array = array.concat(exifArray);
	      array = array.concat(ato);
	      return array;
	    };


	    ExifRestorer.slice2Segments = function (rawImageArray) {
	      var head = 0,
	        segments = [];

	      while (1) {
	        if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 218) {
	          break;
	        }
	        if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 216) {
	          head += 2;
	        }
	        else {
	          var length = rawImageArray[head + 2] * 256 + rawImageArray[head + 3],
	            endPoint = head + length + 2,
	            seg = rawImageArray.slice(head, endPoint);
	          segments.push(seg);
	          head = endPoint;
	        }
	        if (head > rawImageArray.length) {
	          break;
	        }
	      }

	      return segments;
	    };


	    ExifRestorer.decode64 = function (input) {
	      var chr1, chr2, chr3 = '',
	        enc1, enc2, enc3, enc4 = '',
	        i = 0,
	        buf = [];

	      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
	      var base64test = /[^A-Za-z0-9\+\/\=]/g;
	      if (base64test.exec(input)) {
	        console.log('There were invalid base64 characters in the input text.\n' +
	          'Valid base64 characters are A-Z, a-z, 0-9, ' + ', ' / ',and "="\n' +
	          'Expect errors in decoding.');
	      }
	      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');

	      do {
	        enc1 = this.KEY_STR.indexOf(input.charAt(i++));
	        enc2 = this.KEY_STR.indexOf(input.charAt(i++));
	        enc3 = this.KEY_STR.indexOf(input.charAt(i++));
	        enc4 = this.KEY_STR.indexOf(input.charAt(i++));

	        chr1 = (enc1 << 2) | (enc2 >> 4);
	        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
	        chr3 = ((enc3 & 3) << 6) | enc4;

	        buf.push(chr1);

	        if (enc3 !== 64) {
	          buf.push(chr2);
	        }
	        if (enc4 !== 64) {
	          buf.push(chr3);
	        }

	        chr1 = chr2 = chr3 = '';
	        enc1 = enc2 = enc3 = enc4 = '';

	      } while (i < input.length);

	      return buf;
	    };

	    return ExifRestorer.restore(orig, resized);  //<= EXIF
	  };

	  return upload;
	}]);


	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1), __webpack_require__(1)))

/***/ },

/***/ 112:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {angular.module('froala', []).
	value('froalaConfig', {})
	.directive('froala', ['froalaConfig', function (froalaConfig) {
	    "use strict"; //Scope strict mode to only this directive
	    var generatedIds = 0;
	    var defaultConfig = {
	        immediateAngularModelUpdate: false,
	        angularIgnoreAttrs: null
	    };

	    var innerHtmlAttr = 'innerHTML';

	    var scope = {
	        froalaOptions: '=froala',
	        initFunction: '&froalaInit'
	    };

	    froalaConfig = froalaConfig || {};

	    // Constants
	    var MANUAL = "manual";
	    var AUTOMATIC = "automatic";
	    var SPECIAL_TAGS = ['img', 'button', 'input', 'a'];

	    return {
	        restrict: 'A',
	        require: 'ngModel',
	        scope: scope,
	        link: function (scope, element, attrs, ngModel) {

	            var specialTag = false;
	            if (SPECIAL_TAGS.indexOf(element.prop("tagName").toLowerCase()) != -1) {
	                specialTag = true;
	            }

	            var ctrl = {
	                editorInitialized: false
	            };

	            scope.initMode = attrs.froalaInit ? MANUAL : AUTOMATIC;

	            ctrl.init = function () {
	                if (!attrs.id) {
	                    // generate an ID if not present
	                    attrs.$set('id', 'froala-' + generatedIds++);
	                }

	                //init the editor
	                if (scope.initMode === AUTOMATIC) {
	                    ctrl.createEditor();
	                }

	                //Instruct ngModel how to update the froala editor
	                ngModel.$render = function () {
	                    if (ctrl.editorInitialized) {
	                        if (specialTag) {
	                            var tags = ngModel.$modelValue;

	                            // add tags on element
	                            if (tags) {
	                                for (var attr in tags) {
	                                    if (tags.hasOwnProperty(attr) && attr != innerHtmlAttr) {
	                                        element.attr(attr, tags[attr]);
	                                    }
	                                }
	                                if (tags.hasOwnProperty(innerHtmlAttr)) {
	                                    element[0].innerHTML = tags[innerHtmlAttr];
	                                }
	                            }
	                        } else {
	                            element.froalaEditor('html.set', ngModel.$viewValue || '', true);
	                            //This will reset the undo stack everytime the model changes externally. Can we fix this?
	                            element.froalaEditor('undo.reset');
	                            element.froalaEditor('undo.saveStep');
	                        }
	                    }
	                };

	                ngModel.$isEmpty = function (value) {
	                    if (!value) {
	                        return true;
	                    }

	                    var isEmpty = element.froalaEditor('node.isEmpty', jQuery('<div>' + value + '</div>').get(0));
	                    return isEmpty;
	                };
	            };

	            ctrl.createEditor = function (froalaInitOptions) {
	                ctrl.listeningEvents = ['froalaEditor'];
	                if (!ctrl.editorInitialized) {
	                    froalaInitOptions = (froalaInitOptions || {});
	                    ctrl.options = angular.extend({}, defaultConfig, froalaConfig, scope.froalaOptions,froalaInitOptions);

	                    if (ctrl.options.immediateAngularModelUpdate) {
	                        ctrl.listeningEvents.push('keyup');
	                    }

	                    // flush means to load ng-model into editor
	                    var flushNgModel = function() {
	                        ctrl.editorInitialized = true;
	                        ngModel.$render();
	                    }

	                    if (specialTag) {
	                        // flush before editor is initialized
	                        flushNgModel();
	                    } else {
	                        ctrl.registerEventsWithCallbacks('froalaEditor.initialized', function() {
	                            flushNgModel();
	                        });
	                    }

	                    // Register events provided in the options
	                    // Registering events before initializing the editor will bind the initialized event correctly.
	                    for (var eventName in ctrl.options.events) {
	                        if (ctrl.options.events.hasOwnProperty(eventName)) {
	                            ctrl.registerEventsWithCallbacks(eventName, ctrl.options.events[eventName]);
	                        }
	                    }

	                    ctrl.froalaElement = element.froalaEditor(ctrl.options).data('froala.editor').$el;
	                    ctrl.froalaEditor = angular.bind(element, element.froalaEditor);
	                    ctrl.initListeners();

	                    //assign the froala instance to the options object to make methods available in parent scope
	                    if (scope.froalaOptions) {
	                        scope.froalaOptions.froalaEditor = ctrl.froalaEditor;
	                    }
	                }
	            };

	            ctrl.initListeners = function () {
	                if (ctrl.options.immediateAngularModelUpdate) {
	                    ctrl.froalaElement.on('keyup', function () {
	                        scope.$evalAsync(ctrl.updateModelView);
	                    });
	                }

	                element.on('froalaEditor.contentChanged', function () {
	                    scope.$evalAsync(ctrl.updateModelView);
	                });

	                element.bind('$destroy', function () {
	                    element.off(ctrl.listeningEvents.join(" "));
	                    element.froalaEditor('destroy');
	                    element = null;
	                });
	            };

	            ctrl.updateModelView = function () {

	                var modelContent = null;

	                if (specialTag) {
	                    var attributeNodes = element[0].attributes;
	                    var attrs = {};

	                    for (var i = 0; i < attributeNodes.length; i++ ) {
	                        var attrName = attributeNodes[i].name;
	                        if (ctrl.options.angularIgnoreAttrs && ctrl.options.angularIgnoreAttrs.indexOf(attrName) != -1) {
	                            continue;
	                        }
	                        attrs[attrName] = attributeNodes[i].value;
	                    }
	                    if (element[0].innerHTML) {
	                        attrs[innerHtmlAttr] = element[0].innerHTML;
	                    }
	                    modelContent = attrs;
	                } else {
	                    var returnedHtml = element.froalaEditor('html.get');
	                    if (angular.isString(returnedHtml)) {
	                        modelContent = returnedHtml;
	                    }
	                }

	                ngModel.$setViewValue(modelContent);
	                if (!scope.$root.$$phase) {
	                    scope.$apply();
	                }
	            };

	            ctrl.registerEventsWithCallbacks = function (eventName, callback) {
	                if (eventName && callback) {
	                    ctrl.listeningEvents.push(eventName);
	                    element.on(eventName, callback);
	                }
	            };

	            if (scope.initMode === MANUAL) {
	                var _ctrl = ctrl;
	                var controls = {
	                    initialize: ctrl.createEditor,
	                    destroy: function () {
	                        if (_ctrl.froalaEditor) {
	                            _ctrl.froalaEditor('destroy');
	                            _ctrl.editorInitialized = false;
	                        }
	                    },
	                    getEditor: function () {
	                        return _ctrl.froalaEditor ? _ctrl.froalaEditor : null;
	                    }
	                };
	                scope.initFunction({initControls: controls});
	            }
	            ctrl.init();
	        }
	    };
	}])
	.directive('froalaView', ['$sce', function ($sce) {
		return {
			restrict: 'ACM',
			scope: false,
			link: function (scope, element, attrs) {
				element.addClass('fr-view');
				scope.$watch(attrs.froalaView, function (nv) {
					if (nv || nv === ''){
						var explicitlyTrustedValue = $sce.trustAsHtml(nv);
						element.html(explicitlyTrustedValue.toString());
					}
				});
			}
		};
	}]);
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 120:
/***/ function(module, exports) {

	  angular
	    .module('service')
	    .service('admin', admin);

	admin.$inject = ['$http','BASE_API'];

	function admin($http,BASE_API) {


	    this.isIdExist = function(data) {
	        return $http.post(BASE_API + '/admin/general/isIdExist',data);
	    }


	    this.fetchItemUsingLimit = function(data) {
	        return $http.post(BASE_API + '/admin/general/fetchItemUsingLimit',data);
	    }

	    /* Course Master API */

	    this.coursemaster = function() {
	        //  /admin/coursemaster
	        return $http.post(BASE_API + '/courseMaster/getAll').then(function(res) {
	            return res.data;
	        });
	    }

	    

	    this.addCourseMaster = function(data) {
	        //      /admin/addCourseMaster
	        return $http.post(BASE_API + '/courseMaster/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseMaster = function(data) {
	        //      /admin/deleteCourseMaster
	        return $http.post(BASE_API + '/courseMaster/delete', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.fetchCourseMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/courseMaster/fetchCourseMasterUsingLimit', data);
	    }

	    this.getRelatedCoursesByCourseId = function(data) {
	        return $http.post(BASE_API + '/courseMaster/getRelatedCoursesByCourseId',data);
	    }

	    /* Course API */
	    this.courselistfull = function(data) {
	        return $http.post(BASE_API + '/course/getAll',data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourse = function(data) {
	        // /admin/addCourse
	        return $http.post(BASE_API + '/course/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.removeCourse = function(data) {
	        //  /admin/removeCourse
	        return $http.post(BASE_API + '/course/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Videos API */
	    this.addVideos = function(data) {
	        //  /admin/addVideos
	        return $http.post(BASE_API + '/courseVideos/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getModuleVideos = function(data) {
	        //  /admin/getModuleVideos
	        return $http.post(BASE_API + '/courseVideos/getModuleVideos', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getCourseVideos = function(data) {
	        //      /admin/getCourseVideos
	        return $http.post(BASE_API + '/courseVideos/getCourseVideos', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Modules API */
	    this.getCourseModules = function(data) {
	        //      /admin/getCourseModules
	        return $http.post(BASE_API + '/courseModule/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourseModule = function(data) {
	        //  /admin/addCourseModule
	        return $http.post(BASE_API + '/courseModule/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseModule = function(data) {
	        //  /admin/deleteCourseModule
	        return $http.post(BASE_API + '/courseModule/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Video Slides API */
	    this.addVideoSlide = function(data) {
	        //  /admin/addVideoSlide
	        return $http.post(BASE_API + '/videoSlide/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getVideoSlides = function(data) {
	        //  /admin/getVideoSlides
	        return $http.post(BASE_API + '/videoSlide/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteVideoSlide = function(data) {
	        //  /admin/deleteVideoSlide
	        return $http.post(BASE_API + '/videoSlide/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Authorized Items API */

	    this.getAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/getItems',data);
	    }

	    this.updateAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/updateItems',data);
	    }

	    this.addAuthorizedItem = function(data){
	        return $http.post(BASE_API + '/ai/addItem',data);
	    }

	    this.getAuthorizedVideoItemFullInfo = function(data){
	        return $http.post(BASE_API + '/ai/getVideoItemFullInfo',data);
	    }
	    this.getVideoPermission = function(data){
	        return $http.post(BASE_API + '/ai/getVideoPermission',data);
	    }


	    /* practice Question API */

	    this.savePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestion',data);
	    }
	    this.savePreviewPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/savePreviewQuestion',data);
	    }
	    this.saveQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestionStats',data);
	    }
	    this.saveTestQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveTestQuestionStats',data);
	    }
	    this.saveTestReviews  = function(data){
	        return $http.post(BASE_API + '/practice/saveTestReviews',data);
	    }
	    this.removePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/removeQuestion',data);
	    }
	    this.getAllPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getAllQuestions',data);
	    }
	    this.findPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsByQuery',data);
	    }
	    this.findPracticeQuestionsIdByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsIdByQuery',data);
	    }
	    this.findPracticeQuestionsTextByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsTextByQuery',data);
	    }
	    this.findPreviewPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findPreviewQuestionsByQuery',data);
	    }
	    this.getPracticeQuestionsById = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsById',data);
	    }

	    this.getPracticeQuestionsByLimit = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsByLimit',data);
	    }

	    this.updateMultiplePracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/updateMultiplePracticeQuestions',data);
	    }
	    this.getAllAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersQuestionPerfStats',data);
	    }
	    this.getAllMonthAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllMonthAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersMonthQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersMonthQuestionPerfStats',data);
	    }
	    this.updateVerifyPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/updateVerifyPracticeQuestion',data);
	    }
	    this.getAllVerifyStatusOfPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getAllVerifyStatusOfPracticeQuestion',data);
	    }
	    this.getLatestVerifyStatusOfGivenPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getLatestVerifyStatusOfGivenPracticeQuestions',data);
	    }
	    this.getUserMonthQuestionAttemptStats = function(data){
	        return $http.post(BASE_API + '/practice/getUserMonthQuestionAttemptStats',data);
	    }
	    this.startUserExam = function(data){
	        return $http.post(BASE_API + '/practice/startUserExam',data);
	    }
	    this.finishUserExam = function(data){
	        return $http.post(BASE_API + '/practice/finishUserExam',data);
	    }
	    this.checkForPreviousExam = function(data){
	        return $http.post(BASE_API + '/practice/checkForPreviousExam',data);
	    }
	    this.getUserExamQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getUserExamQuestion',data);
	    }
	    this.saveUserExamQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserExamQuestionData',data);
	    }
	    this.startUserPractice = function(data){
	        return $http.post(BASE_API + '/practice/startUserPractice',data);
	    }
	    this.saveUserPracticeQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserPracticeQuestionData',data);
	    }
	    this.fetchNextUserPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/fetchNextUserPracticeQuestion',data);
	    }
	    this.getPracticeNodeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeNodeQuestions',data);
	    }






	    /* Highlight APi */

	    this.saveHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/save',data);
	    }
	    this.getQuestionHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getQuestionHighlights',data);
	    }
	    this.getUserHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getUserHighlights',data);
	    }
	    this.deleteHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/delete',data);
	    }

	    this.updateHighlightNote = function(data){
	        return $http.post(BASE_API + '/highlight/updateHighlightNote',data);
	    }

	    /*
	    Author API
	     */
	    this.getAllAuthor  = function(){
	        return $http.post(BASE_API + '/author/getAll');
	    }
	    this.updateAuthor  = function(data){
	        return $http.post(BASE_API + '/author/update',data);
	    }
	    this.removeAuthor  = function(data){
	        return $http.post(BASE_API + '/author/remove',data);
	    }
	    this.fetchAuthorUsingLimit=function(data){
	        return $http.post(BASE_API+'/author/fetchAuthorUsingLimit',data);
	    }
	    this.AuthorsIdIsExits=function(data){
	        return $http.post(BASE_API+'/author/AuthorsIdIsExits',data);
	    }

	    /*
	    Publication API
	     */
	    this.getAllPublication  = function(){
	        return $http.post(BASE_API + '/publication/getAll');
	    }
	    this.updatePublication  = function(data){
	        return $http.post(BASE_API + '/publication/update',data);
	    }
	    this.removePublication  = function(data){
	        return $http.post(BASE_API + '/publication/remove',data);
	    }
	   this.fetchPublicationsUsingLimit=function(data){
	       return $http.post(BASE_API+'/publication/fetchPublicationsUsingLimit',data);
	   }
	    /*
	    Book API
	     */
	    this.getAllBook  = function(){
	        return $http.post(BASE_API + '/book/getAll');
	    }
	    this.updateBook  = function(data){
	        return $http.post(BASE_API + '/book/update',data);
	    }
	    this.removeBook  = function(data){
	        return $http.post(BASE_API + '/book/remove',data);
	    }
	    this.fetchBooksUsingLimit=function(data){
	        return $http.post(BASE_API+'/book/fetchBooksUsingLimit',data);
	    }

	    /*
	    JobOpenings API
	     */
	    this.getAllJobOpenings  = function(){
	        return $http.post(BASE_API + '/jobOpenings/getAllJobOpenings');
	    }
	    this.updateJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/updateJobOpening',data);
	    }
	    this.removeJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/removeJobOpening',data);
	    }

	    /*
	    Subject API
	     */
	    this.getAllSubject  = function(){
	        return $http.post(BASE_API + '/subject/getAll');
	    }
	    this.updateSubject  = function(data){
	        return $http.post(BASE_API + '/subject/update',data);
	    }
	    this.removeSubject  = function(data){
	        return $http.post(BASE_API + '/subject/remove',data);
	    }
	   this.fetchSubjectMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/subject/fetchSubjectMasterUsingLimit',data);
	    }
	    this.SubjectIdIsExits=function(data){
	        return $http.post(BASE_API+'/subject/SubjectIdIsExits',data);
	    }
	    /*
	    Exam API
	     */
	    this.getAllExam  = function(){
	        return $http.post(BASE_API + '/exam/getAll');
	    }
	    this.updateExam  = function(data){
	        return $http.post(BASE_API + '/exam/update',data);
	    }
	    this.removeExam  = function(data){
	        return $http.post(BASE_API + '/exam/remove',data);
	    }
	    this.fetchExamUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/exam/fetchExamUsingLimit',data);
	    }
	    /*
	    Topic API
	     */
	    this.getAllTopic  = function(data){
	        return $http.post(BASE_API + '/topic/getAll',data);
	    }
	    this.updateTopic  = function(data){
	        return $http.post(BASE_API + '/topic/update',data);
	    }
	    this.removeTopic  = function(data){
	        return $http.post(BASE_API + '/topic/remove',data);
	    }
	   this.fetchTopicsUsingLimit=function(data)
	   {
	       return $http.post(BASE_API+'/topic/fetchTopicsUsingLimit',data);
	   }
	    /*
	    Topic Group API
	     */
	    this.getAllTopicGroups  = function(data){
	        return $http.post(BASE_API + '/topicGroup/getAllTopicGroups',data);
	    }
	    this.updateTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/updateTopicGroup',data);
	    }
	    this.removeTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/removeTopicGroup',data);
	    }
	    this.fetchTopicGroupUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/topicGroup/fetchTopicGroupUsingLimit',data);
	    }
	    /*
	    Tag API
	     */
	    this.getAllTag  = function(){
	        return $http.post(BASE_API + '/tag/getAll');
	    }
	    this.updateTag  = function(data){
	        return $http.post(BASE_API + '/tag/update',data);
	    }
	    this.removeTag  = function(data){
	        return $http.post(BASE_API + '/tag/remove',data);
	    }
	    this.fetchTagsUsingLimit=function(data){
	        return $http.post(BASE_API+'/tag/fetchTagsUsingLimit',data);
	    }
	    this.TagsIdIsExits=function(data)
	    {
	        return $http.post(BASE_API+'/tag/TagsIdIsExits',data);
	    }

	    /*
	    SubTopic API
	     */
	    this.getAllSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/getAll',data);
	    }
	    this.updateSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/update',data);
	    }
	    this.removeSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/remove',data);
	    }

	    /* videoentity api */
	    this.updateVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/update',data);
	    }
	    this.removeVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/remove',data);
	    }
	    this.getAllVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/getAll',data);
	    }
	    this.isVideoFilenameExist = function(data){
	        return $http.post(BASE_API + '/videoentity/isVideoFilenameExist',data);
	    }
	    this.findVideoEntityByQuery = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityByQuery',data);
	    }
	            //One Id
	    this.findVideoEntityById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityById',data);
	    }
	            //Multiple Ids
	    this.findVideoEntitiesById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntitiesById',data);
	    }
	    this.getRelatedTopicsVideoByVideoId = function(data){
	        return $http.post(BASE_API + '/videoentity/getRelatedTopicsVideoByVideoId',data);
	    }
	    this.getRelatedVideosByVideoId = function(data) {
	        return $http.post(BASE_API + '/videoentity/getRelatedVideosByVideoId',data);
	    }
	    this.updateVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/updateVideoLastViewTime',data);
	    }
	    this.getVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/getVideoLastViewTime',data);
	    }


	    /* moduleItems api */
	    this.updateModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/update',data);
	    }
	    this.getAllModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/getAll',data);
	    }
	    this.getCourseItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getCourseItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getFullCourseDetails = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getFullCourseDetails', data);
	    }
	    
	    this.getModuleItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getModuleItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getUserBatchEnrollUploads = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getUserBatchEnrollUploads',data);
	    }

	    /* createtest api */
	    this.createTest = function(data){
	        return $http.post(BASE_API + '/createtest/save',data);
	    }
	    this.getAllTest = function(data){
	        return $http.post(BASE_API + '/createtest/getAll',data);
	    }
	    this.getTestById = function(data){
	        return $http.post(BASE_API + '/createtest/getById',data);
	    }

	    /*
	        CourseBundle API
	     */
	    this.getAllCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/getAll',data);
	    }
	    this.updateCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/update',data);
	    }
	    this.removeCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/remove',data);
	    }



	    // Employee types api
	    this.addEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/add',data);
	    }
	    this.getAllEmployeeTypes  = function(data){
	        return $http.post(BASE_API + '/employee/types/getAll',data);
	    }
	    this.deleteEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/delete',data);
	    }

	    // Employee Details api
	    this.addEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/add',data);
	    }
	    this.getAllEmployees  = function(data){
	        return $http.post(BASE_API + '/employee/getAll',data);
	    }
	    this.deleteEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/delete',data);
	    }

	    // Employee Skills
	    this.addEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/add',data);
	    }
	    this.getAllEmployeeSkills  = function(data){
	        return $http.post(BASE_API + '/employee/skills/getAll',data);
	    }
	    this.deleteEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/delete',data);
	    }

	    // All links category
	    this.addAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/add',data);
	    }
	    this.getAllLinksCategories  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/getAll',data);
	    }
	    this.deleteAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/delete',data);
	    }


	    // Sidelinks article
	    this.addSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/update',data);
	    }
	    this.getAllSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getAll',data);
	    }
	    this.getSideLinkArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/get',data);
	    }
	    this.getMultipleSideLinkArticles = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getMultipleArticles',data);
	    }
	    this.deleteSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/remove',data);
	    }


	    // All Links
	    this.addLinkToAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/update',data);
	    }
	    this.getAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/getAll',data);
	    }
	    this.deleteLinkFromAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/remove',data);
	    }


	    // BoardCompetitiveCourse
	    this.updateBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/update',data);
	    }
	    this.getBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/get',data);
	    }
	    this.getBoardCompetitiveCoursesByCourseType  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/getCoursesByCourseType',data);
	    }
	    this.deleteBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/remove',data);
	    }


	    // Course Subgroup
	    this.addSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/add',data);
	    }
	    this.getAllSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/getAll',data);
	    }
	    this.deleteSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/delete',data);
	    }

	    // Save Batch Course
	    this.addBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/add',data);
	    }
	    this.getAllBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getAll',data);
	    }
	    this.deleteBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/delete',data);
	    }
	    this.getBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getBatchCourse',data);
	    }

	    // Save Batch Timing
	    this.addBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/add',data);
	    }
	    this.getAllBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getAll',data);
	    }
	    this.deleteBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/delete',data);
	    }
	    this.getTimingByCourseId = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getTimingByCourseId',data);
	    }


	    // Save Batch Timing Dashboard
	    this.addBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/addBatchTimingDashboard',data);
	    }
	    this.getAllBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/getAllBatchTimingDashboard',data);
	    }
	    this.deleteBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/deleteBatchTimingDashboard',data);
	    }
	    this.setBatchTimingDashboardAsCompleted  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/setBatchTimingDashboardAsCompleted',data);
	    }

	    //

	    // Currency
	    this.addCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/add',data);
	    }
	    this.getAllCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/getAll',data);
	    }
	    this.deleteCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/delete',data);
	    }

	    // Getting STarted
	    this.addGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/add',data);
	    }
	    this.getAllGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getAll',data);
	    }
	    this.deleteGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/delete',data);
	    }
	    this.getGettingStartedByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getByCourseId',data);
	    }

	    // Sessions
	    this.addSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/add',data);
	    }
	    this.getAllSessions  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getAll',data);
	    }
	    this.deleteSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/delete',data);
	    }
	    this.getSessionsByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getByCourseId',data);
	    }

	    // Sessions Items
	    this.addSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/add',data);
	    }
	    this.getAllSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getAll',data);
	    }
	    this.deleteSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/delete',data);
	    }
	    this.getSessionDetailsForBatchAndSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getSessionDetailsForBatchAndSession',data);
	    }


	    // Pricelist Course
	    this.savePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/add',data);
	    }
	    this.getAllPricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/getAll',data);
	    }
	    this.deletePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/delete',data);
	    }
	    this.getDetailedPricelist = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelist',data);
	    }
	    this.getPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getPricelistByCourseId',data);
	    }
	    this.getDetailedPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelistByCourseId',data);
	    }


	    // Bundle
	    this.saveBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/add',data);
	    }
	    this.getAllBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/getAll',data);
	    }
	    this.deleteBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/delete',data);
	    }

	    // Email Templates
	    this.addEmailTemplates  = function(data){
	        return $http.post(BASE_API + '/emailTemplates/addEmailTemplates',data);
	    }

	    this.getAllEmailTemplates = function(data){
	        return $http.post(BASE_API + '/emailTemplates/getAllEmailTemplates',data);
	    }

	    this.deleteEmailTemplate = function(data){
	        return $http.post(BASE_API + '/emailTemplates/deleteEmailTemplate',data);
	    }

	    // UserEmail Group
	    this.addUserEmailGroup  = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/addUserEmailGroup',data);
	    }

	    this.getAllUserEmailGroups = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/getAllUserEmailGroups',data);
	    }

	    this.deleteUserEmailGroup = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/deleteUserEmailGroup',data);
	    }

	    // Send Email Templates
	    this.addSendEmailTemplate  = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/addSendEmailTemplate',data);
	    }

	    this.getAllSendEmailTemplates = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/getAllSendEmailTemplates',data);
	    }

	    this.deleteSendEmailTemplate = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/deleteSendEmailTemplate',data);
	    }

	    this.sendEmailTemplatesToUsers = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendEmailTemplatesToUsers',data);
	    }

	    this.sendNotificationOfEmail = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendNotificationOfEmail',data);
	    }

	    // Send Notification
	    this.addSendNotification  = function(data){
	        return $http.post(BASE_API + '/sendNotification/addSendNotification',data);
	    }

	    this.getAllSendNotifications = function(data){
	        return $http.post(BASE_API + '/sendNotification/getAllSendNotifications',data);
	    }

	    this.deleteSendNotification = function(data){
	        return $http.post(BASE_API + '/sendNotification/deleteSendNotification',data);
	    }

	    this.getSendNotificationByUserId = function(data){
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationByUserId',data);
	    }

	    this.markSendNotificationAsRead = function(data){
	        return $http.post(BASE_API + '/sendNotification/markSendNotificationAsRead',data);
	    }

	    this.getSendNotificationCount = function(data) {
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationCount',data);
	    }


	    // Roles
	    this.addRole  = function(data){
	        return $http.post(BASE_API + '/adminRoles/addRole',data);
	    }

	    this.getAllRoles = function(data){
	        return $http.post(BASE_API + '/adminRoles/getAllRoles',data);
	    }

	    this.deleteRole = function(data){
	        return $http.post(BASE_API + '/adminRoles/deleteRole',data);
	    }


	    // forms
	    this.addForm  = function(data){
	        return $http.post(BASE_API + '/adminForms/addForm',data);
	    }

	    this.getAllForms = function(data){
	        return $http.post(BASE_API + '/adminForms/getAllForms',data);
	    }

	    this.deleteForm = function(data){
	        return $http.post(BASE_API + '/adminForms/deleteForm',data);
	    }

	    // forms  help
	    this.addFormHelp  = function(data){
	        return $http.post(BASE_API + '/formsHelp/updateFormHelp',data);
	    }

	    this.getAllFormsHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getAllFormsHelp',data);
	    }

	    this.getFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getFormHelp',data);
	    }

	    this.deleteFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/deleteFormHelp',data);
	    }

	    // user roles
	    this.addUserRole  = function(data){
	        return $http.post(BASE_API + '/userRoles/addUserRole',data);
	    }

	    this.getAllUserRoles = function(data){
	        return $http.post(BASE_API + '/userRoles/getAllUserRoles',data);
	    }

	    this.deleteUserRole = function(data){
	        return $http.post(BASE_API + '/userRoles/deleteUserRole',data);
	    }

	    this.getUserAdminRoleFormAccessList = function(data) {
	        return $http.post(BASE_API + '/userRoles/getUserAdminRoleFormAccessList',data);
	    }

	    // user batch enroll
	    this.getUserEnrollAllBatch = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/getUserEnrollAllBatch',data);
	    }
	    this.addUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/addUserBatchEnroll',data);
	    }
	    this.removeUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/removeUserBatchEnroll',data);
	    }



	    this.updateBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/updateBookTopic',data);
	    }
	    this.removeBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/removeBookTopic',data);
	    }
	    this.getAllBookTopics = function(data) {
	        return $http.post(BASE_API + '/booktopic/getAllBookTopics',data);
	    }
	    this.findBookTopicsByQuery = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsByQuery',data);
	    }
	    this.findBookTopicById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicById',data);
	    }
	    this.findBookTopicsById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsById',data);
	    }


	    this.updatePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/update',data);
	    }
	    this.removePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/remove',data);
	    }
	    this.getAllPreviousPapers = function(data) {
	        return $http.post(BASE_API + '/previousPapers/getAll',data);
	    }
	    this.getPreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/get',data);
	    }


	    //Save Purchased Course Based on COurse ids
	    this.purchasedCourse = function(data) {
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnCourseId',data);
	    }
	    //Save purchased courses based on bundle ids
	    this.purchasedBundle = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnBundleId',data);
	    }
	    //save purchased courses based on training ids
	    this.purchasedTraining = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnTrainingId',data);
	    }
	}


/***/ },

/***/ 121:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {angular
		.module('service')
		.service('pageLoader',PageLoader);

	PageLoader.$inject = ['$compile','$rootScope'];	
	function PageLoader($compile,$rootScope){	


		this.show = function(){
			var el = $compile("<page-loading></page-loading>")($rootScope);
			// $("body").css("overflow","hidden");
			$("body>div").append(el);
		}

		this.hide = function(){
			// $("body").css("overflow","auto");
			$(".page-spinner").remove();
		}

	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 122:
/***/ function(module, exports) {

	    angular
	        .module("directive")
	        .directive('pageLoading', PageLoading)

	    PageLoading.$inject = [];

	    function PageLoading() {

	        return {
	            restrict: 'E',
	            replace: true,
	            // template: '<div class="page-spinner">' +
	            //     '<div class="loader-wrapper">' +
	            //     '<div class="sk-fading-circle">' +
	            //     '<div class="sk-circle1 sk-circle"></div>' +
	            //     '<div class="sk-circle2 sk-circle"></div>' +
	            //     '<div class="sk-circle3 sk-circle"></div>' +
	            //     '<div class="sk-circle4 sk-circle"></div>' +
	            //     '<div class="sk-circle5 sk-circle"></div>' +
	            //     '<div class="sk-circle6 sk-circle"></div>' +
	            //     '<div class="sk-circle7 sk-circle"></div>' +
	            //     '<div class="sk-circle8 sk-circle"></div>' +
	            //     '<div class="sk-circle9 sk-circle"></div>' +
	            //     '<div class="sk-circle10 sk-circle"></div>' +
	            //     '<div class="sk-circle11 sk-circle"></div>' +
	            //     '<div class="sk-circle12 sk-circle"></div>' +
	            //     '</div>' +
	            //     '</div>' +
	            //     '</div>'
	            template: '<div class="page-spinner">' +
	                '<div class="loader-wrapper">' +
	                '<div class="dot-loader">' +
	                '</div>' +
	                '</div>' +
	                '</div>'
	        }
	    }

/***/ },

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($, hljs) {__webpack_require__(57);
	__webpack_require__(127);

	angular.module("service")
	    .service('utility', Utility);


	Utility.$inject = ['authToken','$sce','$http','BASE_API'];

	function Utility(authToken,$sce,$http,BASE_API) {

	    var self = this;

	    self.BASE_URL = BASE_API;
	    self.QA_QUESTION_ID_URL_LENGTH = 300;

	    self.goToLogin = goToLogin;
	    self.getLoginUrl = getLoginUrl;
	    self.goToLogout = goToLogout;
	    self.getLogoutUrl = getLogoutUrl;
	    self.goToRegister = goToRegister;
	    self.getRegisterUrl = getRegisterUrl;
	    self.goToChangePassword = goToChangePassword;
	    self.goToForgotPassword = goToForgotPassword;
	    self.getForgotPasswordUrl = getForgotPasswordUrl;
	    self.goToAdminApp = goToAdminApp;
	    self.goToDashboardApp = goToDashboardApp;
	    self.goToPracticePage = goToPracticePage;

	    self.goToCareerBook = goToCareerBook;
	    self.getCareerBookUrl = getCareerBookUrl;

	    self.goToQA = goToQA;
	    self.getQAUrl = getQAUrl;
	    self.getQAQuestionPageUrl = getQAQuestionPageUrl;
	    self.getQAAnswerPageUrl = getQAAnswerPageUrl;

	    self.goToWhiteboard = goToWhiteboard;
	    self.getWhiteboardUrl = getWhiteboardUrl;

	    self.goToDashboard = goToDashboard;
	    self.getDashboardUrl = getDashboardUrl;

	    self.goToBooks = goToBooks;
	    self.getBooksUrl = getBooksUrl;

	    self.getUserProfileUrl = getUserProfileUrl;
	    self.goToUserProfile = goToUserProfile;

	    self.getSitemapUrl = getSitemapUrl;

	    self.getITTrainingsUrl = getITTrainingsUrl;

	    self.getNotificationsPageUrl = getNotificationsPageUrl;
	    self.getSpecificNotificationPageUrl = getSpecificNotificationPageUrl;

	    self.doSyntaxHighlighting = doSyntaxHighlighting;
	    self.getLanguageForSyntaxHighlighting = getLanguageForSyntaxHighlighting;
	    self.changeTimeFormat = changeTimeFormat;
	    self.trustAsHTML = trustAsHTML;
	    self.trustAsHTMLReturnPlainText = trustAsHTMLReturnPlainText;
	    self.shareToEWProfile = shareToEWProfile;trustAsHTMLReturnPlainText;
	    self.shareToQA = shareToQA;
	    self.openPopUp = openPopUp;

	    self.buyPage = buyPage;
	    self.buyCoursePage = buyCoursePage;
	    self.getBuyCoursePageURL = getBuyCoursePageURL;
	    self.buyPaymentPage = buyPaymentPage;
	    self.buyCoursePricePage = buyCoursePricePage;
	    self.getCoursePricePageURL = getCoursePricePageURL;

	    self.getVideoPageURL = getVideoPageURL;
	    self.getQuestionPageURL = getQuestionPageURL;



	    self.getTrainingPageUrl = getTrainingPageUrl;

	    self.getNextTrainingDay = getNextTrainingDay;
	    self.getCourseDetailsPageUrl = getCourseDetailsPageUrl;

	    self.getAdminPracticePageUrl = getAdminPracticePageUrl;

	    self.convertTimeTo_AM_PM = convertTimeTo_AM_PM;



	    self.submitQuestionStats = function(data){
	        return $http.post(BASE_API + '/userstats', data);
	    }

	    function goToLogin(){
	        authToken.cachedUrl(window.location.href);
	        window.location = '/login';
	    }

	    function getLoginUrl(){
	        return '/login';
	    }

	    function goToRegister(){
	        authToken.cachedUrl(window.location.href);
	        window.location = '/register';
	    }

	    function getRegisterUrl(){
	        return '/register';
	    }

	    function goToLogout(){
	        window.location = '/logout';
	    }

	    function  getLogoutUrl() {
	        return '/logout';
	    }

	    function goToChangePassword(code) {
	        if(code){
	            window.location = '/changepassword/' + code;
	        }else{
	            window.location = '/changepassword/';
	        }
	    }

	    function goToForgotPassword() {
	        window.location = '/forgotpassword';
	    }

	    function getForgotPasswordUrl(){
	        return '/forgotpassword';
	    }


	    function goToAdminApp() {
	        window.location = '/adminApp/';
	    }

	    function goToDashboardApp() {
	        window.location = '/dashboard/';
	    }


	    function buyPage() {
	        return self.BASE_URL + '/buy/'
	    }
	    function buyCoursePricePage(crs_id) {
	        return self.BASE_URL + '/buy/price/' + crs_id;
	    }
	    function getCoursePricePageURL(shouldRedirect,crs_id) {
	        var url = self.BASE_URL + '/buy/price/' + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }
	    function buyCoursePage(crs_id) {
	        return self.BASE_URL + '/buy/course/' + crs_id;
	    }
	    function getBuyCoursePageURL(shouldRedirect,crs_id) {
	        var url = self.BASE_URL + '/buy/course/' + crs_id;;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }
	    function buyPaymentPage(crs_id,bat_id) {
	        return self.BASE_URL + '/buy/payment?course=' + crs_id + '&bat_id=' + bat_id ;
	    }

	    function goToPracticePage(courseId,moduleId,itemId){
	        var url = "/" + courseId + "/video/" + itemId;
	        // var url = "/app/practice?module="+ moduleId + "&course=" + courseId + "&item=" + itemId;
	        // window.location.href = url;
	        var win = window.open(url, '_blank');
	        win.focus();
	    }

	    /////////////////////

	    function goToQA(){
	        window.location = '/question-answers';
	    }
	    function getQAUrl() {
	        return '/question-answers';
	    }

	    function getQAQuestionPageUrl(shouldRedirect,que_id,que_text){
	        var url = '/question-answers/question/' + que_id + '/' + (que_text ? que_text : '');
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    function getQAAnswerPageUrl(shouldRedirect,que_id,ans_id,que_text){
	        var url = '/question-answers/question/' + que_id + "/" + (que_text || "text") +  "/answer/" + ans_id;
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    /////////////////////

	    function goToCareerBook(){
	        window.location = '/career-book/';
	    }
	    function getCareerBookUrl(){
	        return '/career-book/';
	    }

	    self.getCareerBookProfileUrl = function(usr_id){
	        return '/career-book/' + usr_id;
	    }

	    /////////////////////

	    function goToWhiteboard(){
	        window.location = '/whiteboard/';
	    }
	    function getWhiteboardUrl(){
	        return '/whiteboard/';
	    }

	    /////////////////////

	    function goToBooks(){
	        window.location = '/books/';
	    }
	    function getBooksUrl(){
	        return '/books/';
	    }

	    /////////////////////


	    function goToDashboard(){
	        window.location = '/dashboard/';
	    }
	    function getDashboardUrl(){
	        return '/dashboard/';
	    }

	    /////////////////////


	    function getUserProfileUrl(){
	        return '/user-profile';
	    }
	    function goToUserProfile(){
	        window.location = '/user-profile';
	    }

	    /////////////////////


	    function getSitemapUrl(shouldRedirect){
	        var url = '/sitemap';
	        if(shouldRedirect){
	            window.location = url;
	        }
	        return url;
	    }

	    /////////////////////

	    function getVideoPageURL(courseId,itemId,time){
	        if(time){
	            return self.BASE_URL + '/' + courseId + '/video/' + itemId + '?time=' + time;
	        }else{
	            return self.BASE_URL + '/' + courseId + '/video/' + itemId;
	        }
	    }

	    function getQuestionPageURL(courseId,itemId,question){
	        if(question){
	            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/' + question;
	        }else{
	            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/';
	        }
	    }

	    self.getBookPageURL = getBookPageURL;
	    function getBookPageURL(courseId,itemId){
	        return self.BASE_URL + '/' + courseId + '/book-topic/' + itemId;
	    }

	    function getITTrainingsUrl(shouldRedirect){
	        var url = "/it-trainings";
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getCourseDetailsPageUrl(shouldRedirect,type,subgroup,crs_id){
	        // var url = `${type}/${subgroup}-trainings/${crs_id}`;
	        var url = "/" + type + "/" + subgroup + "-trainings/" + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getNotificationsPageUrl(shouldRedirect,usr_id){
	        var url = "/notifications";
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getSpecificNotificationPageUrl(shouldRedirect,usr_id,notification_id){
	        var url = "/notifications/" + notification_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getTrainingPageUrl(shouldRedirect,crs_id){
	        var url = "/training/" + crs_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    function getAdminPracticePageUrl(shouldRedirect,q_id){
	        var url = "/adminApp/practice/" + q_id;
	        if(shouldRedirect){
	            window.location = url;
	        }else{
	            return url;
	        }
	    }

	    //do highlighting
	    function doSyntaxHighlighting() {
	        var x = 0;
	        var intervalID = setInterval(function() {
	            // Prism.highlightAll();
	            $('pre code').each(function(i, block) {
	                hljs.highlightBlock(block);
	            });
	            $('.syntax').each(function(i, block) {
	                hljs.highlightBlock(block);
	            });
	            if (++x === 10) {
	                window.clearInterval(intervalID);
	            }
	        }, 1);
	    }

	    //Get language for syntax highlighting
	    function getLanguageForSyntaxHighlighting(coursename){
	        if(coursename.toLowerCase().indexOf("javascript") > -1){
	            return "javascript";
	        }else if(coursename.toLowerCase().indexOf("java") > -1){
	            return "java";
	        }else if(coursename.toLowerCase().indexOf("sql") > -1){
	            return "sql";
	        }
	    }

	    var vm = this;
	    function getNextTrainingDay(frm_wk_dy,to_wk_dy,cls_start_dt,bat_frm_tm_hr,bat_frm_tm_min) {
	        vm.daysOfWeek = ["Monday", 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

	        var training_days = [];
	        var start_counting = false;
	        for (var i = 0; i < vm.daysOfWeek.length; i++) {
	            var d = vm.daysOfWeek[i];
	            if (start_counting) {
	                training_days.push(d);
	                if (d === to_wk_dy) {
	                    start_counting = false;
	                    break;
	                }
	            }
	            if (d === frm_wk_dy) {
	                start_counting = true;
	                training_days.push(d);
	            }
	        }

	        var today_name = getDayName();
	        var today_date = new Date();
	        var next_training_date = "";
	        bat_frm_tm_hr = parseInt(bat_frm_tm_hr);
	        bat_frm_tm_min = parseInt(bat_frm_tm_min);
	        if(today_date < new Date(cls_start_dt)){
	            next_training_date = new Date(cls_start_dt);
	            // console.log("next date is start date.")
	        }else if (training_days.indexOf(today_name) > -1) {
	            //check if today is in training_days
	            var current_hour = today_date.getHours();
	            var current_min = today_date.getMinutes();
	            // console.log(current_hour,bat_frm_tm_hr,current_min,bat_frm_tm_min)
	            if (current_hour > bat_frm_tm_hr) {
	                // console.log("time is exceeded , find next day");
	                var daysToGo = nextTrainingDay(training_days)
	                // console.log("daysToGo", daysToGo);
	                var today = new Date();
	                next_training_date = new Date(today);
	                next_training_date.setDate(today.getDate() + daysToGo);
	            }else if(current_hour === bat_frm_tm_hr && current_min > bat_frm_tm_min) {
	                // console.log("time is exceeded , find next day");
	                var daysToGo = nextTrainingDay(training_days)
	                // console.log("daysToGo", daysToGo);
	                var today = new Date();
	                next_training_date = new Date(today);
	                next_training_date.setDate(today.getDate() + daysToGo);
	            }else {
	                // console.log("today is the next batch day");
	                next_training_date = new Date();
	            }
	        } else {
	            //if today is not in training_days
	            // console.log("today is not training_days , find next day");
	            var daysToGo = nextTrainingDay(training_days);
	            // console.log("daysToGo", daysToGo);
	            var today = new Date();
	            next_training_date = new Date(today);
	            next_training_date.setDate(today.getDate() + daysToGo);
	        }
	        return next_training_date;
	    }

	    function getDayName(dateString) {
	        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	        if (!dateString) {
	            dateString = (new Date()).toString();
	        }
	        var d = new Date(dateString);
	        var dayName = days[d.getDay()];
	        return dayName;
	    }

	    function nextTrainingDay(training_days) {
	        var today_name = getDayName();
	        var today_day_index = vm.daysOfWeek.indexOf(today_name);
	        var day_index = today_day_index + 1;
	        var daysToGo = 1;
	        var nextDayFound = false;
	        while (!nextDayFound) {
	            var day = vm.daysOfWeek[day_index];
	            if (training_days.indexOf(day) > -1) {
	                nextDayFound = true;
	            } else {
	                day_index = (day_index + 1) % 7;
	                daysToGo++;
	                nextDayFound = false;
	            }
	        }
	        return daysToGo;
	    }

	    function changeTimeFormat(sec) {
	        var time = sec || 0;
	        var hours = parseInt(time / 3600) % 24;
	        var minutes = parseInt(time / 60) % 60;
	        var seconds = parseInt(time % 60);

	        hours = ("0" + hours).slice(-2);
	        minutes = ("0" + minutes).slice(-2);
	        seconds = ("0" + seconds).slice(-2);

	        var ret_time;
	        if (hours == "00") {
	            ret_time = minutes + ":" + seconds;
	        } else {
	            ret_time = hours + ":" + minutes + ":" + seconds;
	        }

	        return ret_time;
	    }

	    function convertTimeTo_AM_PM(time) {
	        // time = 10:10
	        time = time || "00:00";
	        var split_time = time.split(":");
	        var hour = parseInt(split_time[0]);
	        if(hour < 12){
	            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " AM";
	        }else if(hour === 12){
	            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
	        }else if(hour > 12){
	            return ('00' + (split_time[0] - 12)).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
	        }
	    }

	    function trustAsHTML(html){
	        return $sce.trustAsHtml(html);
	    }

	    function trustAsHTMLReturnPlainText(html){
	        var html2 = html.replace(/(<([^>]+)>)/g, "");
	        return $sce.trustAsHtml(html2);
	    }

	    function shareToEWProfile(title,desc,url){
	        if(!url){
	            url = encodeURIComponent(window.location.href);
	        }else{
	            url = encodeURIComponent(url);
	        }
	        var w = 500;
	        var h = 560;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = window.location.origin + '/app/share?type=cb&title=' + title + "&url=" + url + "&desc=" + (desc || '');
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    function shareToQA(title,desc,url){
	        if(!url){
	            url = encodeURIComponent(window.location.href);
	        }else{
	            url = encodeURIComponent(url);
	        }
	        var w = 500;
	        var h = 560;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = window.location.origin + '/app/share?type=qa&title=' + title + "&url=" + url + "&desc=" + (desc || '');
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    function openPopUp(url){
	        var w = 500;
	        var h = 500;
	        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	        var top = ((height / 2) - (h / 2)) + dualScreenTop;

	        var popup_url = url;
	        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
	    }

	    self.getSelectionText = getSelectionText;
	    function getSelectionText() {
	        var text = "";
	        if (window.getSelection) {
	            text = window.getSelection().toString();
	        } else if (document.selection && document.selection.type != "Control") {
	            text = document.selection.createRange().text;
	        }
	        return text;
	    }

	    self.getPlainQuestionIdText = function(text){
	        var text = $("<div></div>").html(text).text().split(" ").join("-").toLowerCase();
	        text = text.substring(0,self.QA_QUESTION_ID_URL_LENGTH);
	        text = text.replace(/[^a-zA-Z0-9-]/g, "")
	        return text
	    }
	};

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1), __webpack_require__(126)))

/***/ },

/***/ 126:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(hljs) {/*! highlight.js v9.5.0 | BSD3 License | git.io/hljslicense */ ! function(e) {
	    var n = "object" == typeof window && window || "object" == typeof self && self;
	     true ? e(exports) : n && (n.hljs = e({}), "function" == typeof define && define.amd && define([], function() {
	        return n.hljs
	    }))
	}(function(e) {
	    function n(e) {
	        return e.replace(/[&<>]/gm, function(e) {
	            return I[e]
	        })
	    }

	    function t(e) {
	        return e.nodeName.toLowerCase()
	    }

	    function r(e, n) {
	        var t = e && e.exec(n);
	        return t && 0 === t.index
	    }

	    function a(e) {
	        return k.test(e)
	    }

	    function i(e) {
	        var n, t, r, i, o = e.className + " ";
	        if (o += e.parentNode ? e.parentNode.className : "", t = B.exec(o)) return R(t[1]) ? t[1] : "no-highlight";
	        for (o = o.split(/\s+/), n = 0, r = o.length; r > n; n++)
	            if (i = o[n], a(i) || R(i)) return i
	    }

	    function o(e, n) {
	        var t, r = {};
	        for (t in e) r[t] = e[t];
	        if (n)
	            for (t in n) r[t] = n[t];
	        return r
	    }

	    function u(e) {
	        var n = [];
	        return function r(e, a) {
	            for (var i = e.firstChild; i; i = i.nextSibling) 3 === i.nodeType ? a += i.nodeValue.length : 1 === i.nodeType && (n.push({
	                event: "start",
	                offset: a,
	                node: i
	            }), a = r(i, a), t(i).match(/br|hr|img|input/) || n.push({
	                event: "stop",
	                offset: a,
	                node: i
	            }));
	            return a
	        }(e, 0), n
	    }

	    function c(e, r, a) {
	        function i() {
	            return e.length && r.length ? e[0].offset !== r[0].offset ? e[0].offset < r[0].offset ? e : r : "start" === r[0].event ? e : r : e.length ? e : r
	        }

	        function o(e) {
	            function r(e) {
	                return " " + e.nodeName + '="' + n(e.value) + '"'
	            }
	            l += "<" + t(e) + w.map.call(e.attributes, r).join("") + ">"
	        }

	        function u(e) {
	            l += "</" + t(e) + ">"
	        }

	        function c(e) {
	            ("start" === e.event ? o : u)(e.node)
	        }
	        for (var s = 0, l = "", f = []; e.length || r.length;) {
	            var g = i();
	            if (l += n(a.substr(s, g[0].offset - s)), s = g[0].offset, g === e) {
	                f.reverse().forEach(u);
	                do c(g.splice(0, 1)[0]), g = i(); while (g === e && g.length && g[0].offset === s);
	                f.reverse().forEach(o)
	            } else "start" === g[0].event ? f.push(g[0].node) : f.pop(), c(g.splice(0, 1)[0])
	        }
	        return l + n(a.substr(s))
	    }

	    function s(e) {
	        function n(e) {
	            return e && e.source || e
	        }

	        function t(t, r) {
	            return new RegExp(n(t), "m" + (e.cI ? "i" : "") + (r ? "g" : ""))
	        }

	        function r(a, i) {
	            if (!a.compiled) {
	                if (a.compiled = !0, a.k = a.k || a.bK, a.k) {
	                    var u = {},
	                        c = function(n, t) {
	                            e.cI && (t = t.toLowerCase()), t.split(" ").forEach(function(e) {
	                                var t = e.split("|");
	                                u[t[0]] = [n, t[1] ? Number(t[1]) : 1]
	                            })
	                        };
	                    "string" == typeof a.k ? c("keyword", a.k) : E(a.k).forEach(function(e) {
	                        c(e, a.k[e])
	                    }), a.k = u
	                }
	                a.lR = t(a.l || /\w+/, !0), i && (a.bK && (a.b = "\\b(" + a.bK.split(" ").join("|") + ")\\b"), a.b || (a.b = /\B|\b/), a.bR = t(a.b), a.e || a.eW || (a.e = /\B|\b/), a.e && (a.eR = t(a.e)), a.tE = n(a.e) || "", a.eW && i.tE && (a.tE += (a.e ? "|" : "") + i.tE)), a.i && (a.iR = t(a.i)), null == a.r && (a.r = 1), a.c || (a.c = []);
	                var s = [];
	                a.c.forEach(function(e) {
	                    e.v ? e.v.forEach(function(n) {
	                        s.push(o(e, n))
	                    }) : s.push("self" === e ? a : e)
	                }), a.c = s, a.c.forEach(function(e) {
	                    r(e, a)
	                }), a.starts && r(a.starts, i);
	                var l = a.c.map(function(e) {
	                    return e.bK ? "\\.?(" + e.b + ")\\.?" : e.b
	                }).concat([a.tE, a.i]).map(n).filter(Boolean);
	                a.t = l.length ? t(l.join("|"), !0) : {
	                    exec: function() {
	                        return null
	                    }
	                }
	            }
	        }
	        r(e)
	    }

	    function l(e, t, a, i) {
	        function o(e, n) {
	            for (var t = 0; t < n.c.length; t++)
	                if (r(n.c[t].bR, e)) return n.c[t]
	        }

	        function u(e, n) {
	            if (r(e.eR, n)) {
	                for (; e.endsParent && e.parent;) e = e.parent;
	                return e
	            }
	            return e.eW ? u(e.parent, n) : void 0
	        }

	        function c(e, n) {
	            return !a && r(n.iR, e)
	        }

	        function g(e, n) {
	            var t = N.cI ? n[0].toLowerCase() : n[0];
	            return e.k.hasOwnProperty(t) && e.k[t]
	        }

	        function h(e, n, t, r) {
	            var a = r ? "" : y.classPrefix,
	                i = '<span class="' + a,
	                o = t ? "" : C;
	            return i += e + '">', i + n + o
	        }

	        function p() {
	            var e, t, r, a;
	            if (!E.k) return n(B);
	            for (a = "", t = 0, E.lR.lastIndex = 0, r = E.lR.exec(B); r;) a += n(B.substr(t, r.index - t)), e = g(E, r), e ? (M += e[1], a += h(e[0], n(r[0]))) : a += n(r[0]), t = E.lR.lastIndex, r = E.lR.exec(B);
	            return a + n(B.substr(t))
	        }

	        function d() {
	            var e = "string" == typeof E.sL;
	            if (e && !x[E.sL]) return n(B);
	            var t = e ? l(E.sL, B, !0, L[E.sL]) : f(B, E.sL.length ? E.sL : void 0);
	            return E.r > 0 && (M += t.r), e && (L[E.sL] = t.top), h(t.language, t.value, !1, !0)
	        }

	        function b() {
	            k += null != E.sL ? d() : p(), B = ""
	        }

	        function v(e) {
	            k += e.cN ? h(e.cN, "", !0) : "", E = Object.create(e, {
	                parent: {
	                    value: E
	                }
	            })
	        }

	        function m(e, n) {
	            if (B += e, null == n) return b(), 0;
	            var t = o(n, E);
	            if (t) return t.skip ? B += n : (t.eB && (B += n), b(), t.rB || t.eB || (B = n)), v(t, n), t.rB ? 0 : n.length;
	            var r = u(E, n);
	            if (r) {
	                var a = E;
	                a.skip ? B += n : (a.rE || a.eE || (B += n), b(), a.eE && (B = n));
	                do E.cN && (k += C), E.skip || (M += E.r), E = E.parent; while (E !== r.parent);
	                return r.starts && v(r.starts, ""), a.rE ? 0 : n.length
	            }
	            if (c(n, E)) throw new Error('Illegal lexeme "' + n + '" for mode "' + (E.cN || "<unnamed>") + '"');
	            return B += n, n.length || 1
	        }
	        var N = R(e);
	        if (!N) throw new Error('Unknown language: "' + e + '"');
	        s(N);
	        var w, E = i || N,
	            L = {},
	            k = "";
	        for (w = E; w !== N; w = w.parent) w.cN && (k = h(w.cN, "", !0) + k);
	        var B = "",
	            M = 0;
	        try {
	            for (var I, j, O = 0;;) {
	                if (E.t.lastIndex = O, I = E.t.exec(t), !I) break;
	                j = m(t.substr(O, I.index - O), I[0]), O = I.index + j
	            }
	            for (m(t.substr(O)), w = E; w.parent; w = w.parent) w.cN && (k += C);
	            return {
	                r: M,
	                value: k,
	                language: e,
	                top: E
	            }
	        } catch (T) {
	            if (T.message && -1 !== T.message.indexOf("Illegal")) return {
	                r: 0,
	                value: n(t)
	            };
	            throw T
	        }
	    }

	    function f(e, t) {
	        t = t || y.languages || E(x);
	        var r = {
	                r: 0,
	                value: n(e)
	            },
	            a = r;
	        return t.filter(R).forEach(function(n) {
	            var t = l(n, e, !1);
	            t.language = n, t.r > a.r && (a = t), t.r > r.r && (a = r, r = t)
	        }), a.language && (r.second_best = a), r
	    }

	    function g(e) {
	        return y.tabReplace || y.useBR ? e.replace(M, function(e, n) {
	            return y.useBR && "\n" === e ? "<br>" : y.tabReplace ? n.replace(/\t/g, y.tabReplace) : void 0
	        }) : e
	    }

	    function h(e, n, t) {
	        var r = n ? L[n] : t,
	            a = [e.trim()];
	        return e.match(/\bhljs\b/) || a.push("hljs"), -1 === e.indexOf(r) && a.push(r), a.join(" ").trim()
	    }

	    function p(e) {
	        var n, t, r, o, s, p = i(e);
	        a(p) || (y.useBR ? (n = document.createElementNS("http://www.w3.org/1999/xhtml", "div"), n.innerHTML = e.innerHTML.replace(/\n/g, "").replace(/<br[ \/]*>/g, "\n")) : n = e, s = n.textContent, r = p ? l(p, s, !0) : f(s), t = u(n), t.length && (o = document.createElementNS("http://www.w3.org/1999/xhtml", "div"), o.innerHTML = r.value, r.value = c(t, u(o), s)), r.value = g(r.value), e.innerHTML = r.value, e.className = h(e.className, p, r.language), e.result = {
	            language: r.language,
	            re: r.r
	        }, r.second_best && (e.second_best = {
	            language: r.second_best.language,
	            re: r.second_best.r
	        }))
	    }

	    function d(e) {
	        y = o(y, e)
	    }

	    function b() {
	        if (!b.called) {
	            b.called = !0;
	            var e = document.querySelectorAll("pre code");
	            w.forEach.call(e, p)
	        }
	    }

	    function v() {
	        addEventListener("DOMContentLoaded", b, !1), addEventListener("load", b, !1)
	    }

	    function m(n, t) {
	        var r = x[n] = t(e);
	        r.aliases && r.aliases.forEach(function(e) {
	            L[e] = n
	        })
	    }

	    function N() {
	        return E(x)
	    }

	    function R(e) {
	        return e = (e || "").toLowerCase(), x[e] || x[L[e]]
	    }
	    var w = [],
	        E = Object.keys,
	        x = {},
	        L = {},
	        k = /^(no-?highlight|plain|text)$/i,
	        B = /\blang(?:uage)?-([\w-]+)\b/i,
	        M = /((^(<[^>]+>|\t|)+|(?:\n)))/gm,
	        C = "</span>",
	        y = {
	            classPrefix: "hljs-",
	            tabReplace: null,
	            useBR: !1,
	            languages: void 0
	        },
	        I = {
	            "&": "&amp;",
	            "<": "&lt;",
	            ">": "&gt;"
	        };
	    return e.highlight = l, e.highlightAuto = f, e.fixMarkup = g, e.highlightBlock = p, e.configure = d, e.initHighlighting = b, e.initHighlightingOnLoad = v, e.registerLanguage = m, e.listLanguages = N, e.getLanguage = R, e.inherit = o, e.IR = "[a-zA-Z]\\w*", e.UIR = "[a-zA-Z_]\\w*", e.NR = "\\b\\d+(\\.\\d+)?", e.CNR = "(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", e.BNR = "\\b(0b[01]+)", e.RSR = "!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~", e.BE = {
	        b: "\\\\[\\s\\S]",
	        r: 0
	    }, e.ASM = {
	        cN: "string",
	        b: "'",
	        e: "'",
	        i: "\\n",
	        c: [e.BE]
	    }, e.QSM = {
	        cN: "string",
	        b: '"',
	        e: '"',
	        i: "\\n",
	        c: [e.BE]
	    }, e.PWM = {
	        b: /\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|like)\b/
	    }, e.C = function(n, t, r) {
	        var a = e.inherit({
	            cN: "comment",
	            b: n,
	            e: t,
	            c: []
	        }, r || {});
	        return a.c.push(e.PWM), a.c.push({
	            cN: "doctag",
	            b: "(?:TODO|FIXME|NOTE|BUG|XXX):",
	            r: 0
	        }), a
	    }, e.CLCM = e.C("//", "$"), e.CBCM = e.C("/\\*", "\\*/"), e.HCM = e.C("#", "$"), e.NM = {
	        cN: "number",
	        b: e.NR,
	        r: 0
	    }, e.CNM = {
	        cN: "number",
	        b: e.CNR,
	        r: 0
	    }, e.BNM = {
	        cN: "number",
	        b: e.BNR,
	        r: 0
	    }, e.CSSNM = {
	        cN: "number",
	        b: e.NR + "(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?",
	        r: 0
	    }, e.RM = {
	        cN: "regexp",
	        b: /\//,
	        e: /\/[gimuy]*/,
	        i: /\n/,
	        c: [e.BE, {
	            b: /\[/,
	            e: /\]/,
	            r: 0,
	            c: [e.BE]
	        }]
	    }, e.TM = {
	        cN: "title",
	        b: e.IR,
	        r: 0
	    }, e.UTM = {
	        cN: "title",
	        b: e.UIR,
	        r: 0
	    }, e.METHOD_GUARD = {
	        b: "\\.\\s*" + e.UIR,
	        r: 0
	    }, e
	});
	hljs.registerLanguage("ruleslanguage", function(T) {
	    return {
	        k: {
	            keyword: 'ABORT ACCEPT ACCESS ADD ADMIN AFTER ALLOCATE ALTER ANALYZE ARCHIVE ARCHIVELOG ' +
	                'ARRAY ARRAYLEN AS ASC ASSERT ASSIGN AT AUDIT AUTHORIZATION BACKUP BASE_TABLE ' +
	                'BECOME BEFORE BEGIN BINARY_INTEGER BLOCK BODY BOOLEAN BY CACHE CANCEL CASCADE ' +
	                'CASE CHANGE CHAR CHARACTER CHAR_BASE CHECK CHECKPOINT CLOSE CLUSTER CLUSTERS ' +
	                'COBOL COLAUTH COLUMN COLUMNS COMMENT COMMIT COMPILE COMPRESS CONNECT CONSTANT ' +
	                'CONSTRAINT CONSTRAINTS CONTENTS CONTINUE CONTROLFILE CRASH CREATE CURRENT ' +
	                'CURRVAL CURSOR CYCLE DATABASE DATAFILE DATA_BASE DATE DATE DBA DEBUGOFF ' +
	                'DEBUGON DEC DECIMAL DECLARE DEFAULT DEFINITION DELAY DELETE DELTA DESC DIGITS ' +
	                'DISABLE DISMOUNT DISPOSE DISTINCT DO DOUBLE DROP EACH ELSE ELSIF ENABLE END ' +
	                'ENTRY ESCAPE EVENTS EXCEPT EXCEPTION EXCEPTIONS EXCEPTION_INIT EXCLUSIVE EXEC ' +
	                'EXECUTE EXISTS EXIT EXPLAIN EXTENT EXTERNALLY FALSE FETCH FILE FLOAT FLUSH FOR ' +
	                'FORCE FOREIGN FORM FORTRAN FOUND FREELIST FREELISTS FROM FUNCTION GENERIC GO ' +
	                'GOTO GRANT GROUP GROUPS HAVING IDENTIFIED IF IMMEDIATE INCLUDING INCREMENT ' +
	                'INDEX INDEXES INDICATOR INITIAL INITRANS INSERT INSTANCE INT INTEGER INTERSECT ' +
	                'INTO IS KEY LANGUAGE LAYER LEVEL LIMITED LINK LISTS LOCK LOGFILE LONG LOOP ' +
	                'MANAGE MANUAL MAXDATAFILES MAXEXTENTS MAXINSTANCES MAXLOGFILES MAXLOGHISTORY ' +
	                'MAXLOGMEMBERS MAXTRANS MAXVALUE MINEXTENTS MINUS MINVALUE MLSLABEL MODE MODIFY ' +
	                'MODULE MOUNT NATURAL NEW NEXT NEXTVAL NOARCHIVELOG NOAUDIT NOCACHE NOCOMPRESS ' +
	                'NOCYCLE NOMAXVALUE NOMINVALUE NONE NOORDER NORESETLOGS NORMAL NOSORT NOTFOUND ' +
	                'NOWAIT NUMBER number NUMBER_BASE NUMERIC OF OFF OFFLINE OLD ON ONLINE ONLY OPEN OPTIMAL ' +
	                'OPTION ORDER OTHERS OUT OWN PACKAGE PARALLEL PARTITION PCTFREE PCTINCREASE ' +
	                'PCTUSED PLAN PLI POSITIVE PRAGMA PRECISION PRIMARY PRIOR PRIVATE PRIVILEGES ' +
	                'PROCEDURE PROFILE PUBLIC QUOTA RAISE RANGE RAW READ REAL RECORD RECOVER ' +
	                'REFERENCES REFERENCING RELEASE REMR RENAME RESETLOGS RESOURCE RESTRICTED ' +
	                'RETURN REUSE REVOKE ROLE ROLES ROLLBACK ROW ROWID ROWLABEL ROWNUM ROWS ROWTYPE ' +
	                'RUN SAVEPOINT SCHEMA SCN SECTION SEGMENT SELECT SEPARATE SEQUENCE SESSION ' +
	                'SHARE SHARED SIZE SMALLINT SNAPSHOT SORT SPACE SQL SQLBUF SQLERROR SQLSTATE ' +
	                'START STATEMENT STATEMENT_ID STATISTICS STDDEV STOP STORAGE SUBTYPE ' +
	                'SUCCESSFUL SWITCH SYNONYM SYSDATE SYSTEM TABAUTH TABLESPACE TASK TEMPORARY ' +
	                'TERMINATE THEN THREAD TIME TO TRACING TRANSACTION TRIGGER TRIGGERS TRUE ' +
	                'TRUNCATE TYPE UNDER UNION UNIQUE UNLIMITED UNTIL UPDATE USE USING VALIDATE ' +
	                'VALUES VARCHAR VARCHAR2 varchar2 VIEW VIEWS WHEN WHENEVER WHERE WHILE WITH WORK WRITE ' +
	                'TABLE SQLCODE SQLERRM USER',
	            built_in: 'ABS ACOS ADD_MONTHS ADJ_DATE APPENDCHILDXML ASCII ASCIISTR ASIN ATAN ATAN2 AVG ' +
	        'BFILENAME BIN_TO_NUM BINARY2VARCHAR BIT_COMPLEMENT BIT_OR BIT_XOR BITAND ' +
	        'BOOL_TO_INT CARDINALITY CASE CAST CAST_FROM_BINARY_DOUBLE CAST_FROM_BINARY_FLOAT ' +
	        'CAST_FROM_BINARY_INTEGER CAST_FROM_NUMBER CAST_TO_BINARY_DOUBLE CAST_TO_BINARY_FLOAT ' +
	        'CAST_TO_BINARY_INTEGER CAST_TO_NUMBER CAST_TO_NVARCHAR2 CAST_TO_RAW CAST_TO_VARCHAR ' +
	        'CEIL CHARTOROWID CHR CLUSTER_ID CLUSTER_PROBABILITY CLUSTER_SET COALESCE COLLECT ' +
	        'COMPOSE CONCAT CONVERT CORR CORR_K CORR_S COS COSH COUNT COVAR_POP COVAR_SAMP ' +
	        'CUME_DIST CURRENT_DATE CURRENT_TIMESTAMP CV DBTIMEZONE DENSE_RANK DECODE DECOMPOSE ' +
	        'DELETEXML DEPTH DEREF DUMP EMPTY_BLOB EMPTY_CLOB ESTIMATE_CPU_UNITS EXISTSNODE EXP ' +
	        'EXTRACT EXTRACTVALUE FEATURE_ID FEATURE_SET FEATURE_VALUE FIRST FIRST_VALUE FLOOR ' +
	        'FROM_TZ GET_CLOCK_TIME GET_DDL GET_DEPENDENT_DDL GET_DEPENDENT_XML GET_GRANTED_DDL ' +
	        'GET_GRANTED_XDL GET_HASH GET_REBUILD_COMMAND GET_SCN GET_XML GREATEST GROUP_ID ' +
	        'GROUPING GROUPING_ID HEXTORAW INITCAP INSERTCHILDXML INSERTXMLBEFORE INSTR INSTRB ' +
	        'INSTRC INSTR2 INSTR4 INT_TO_BOOL INTERVAL ITERATE ITERATION_NUMBER LAG LAST LAST_DAY ' +
	        'LAST_VALUE LEAD LEAST LENGTH LENGTHB LENGTHC LENGTH2 LENGTH4 LN LNNVL LOCALTIMESTAMP ' +
	        'LOG LOWER LPAD LTRIM MAKEREF MAX MEDIAN MIN MONTHS_BETWEEN MOD NANVL NEW_TIME NEXT_DAY ' +
	        'NHEXTORAW NLS_CHARSET_DECL_LEN NLS_CHARSET_ID NLS_CHARSET_NAME NLS_INITCAP NLS_LOWER ' +
	        'NLSSORT NLS_UPPER NTILE NULLFN NULLIF NUMTODSINTERVAL NUMTOHEX NUMTOHEX2 NUMTOYMINTERVAL ' +
	        'NVL NVL2 ORA_HASH PATH PERCENT_RANK PERCENTILE_CONT PERCENTILE_DISC POWER POWERMULTISET ' +
	        'POWERMULTISET_BY_CARDINALITY PREDICTION PREDICTION_BOUNDS PREDICTION_COST PREDICTION_DETAILS ' +
	        'PREDICTION_PROBABILITY PREDICTION_SET PRESENTNNV PRESENTV PREVIOUS QUOTE DELIMITERS ' +
	        'RANDOMBYTES RANDOMINTEGER RANDOMNUMBER RANK RATIO_TO_REPORT RAW_TO_CHAR RAW_TO_NCHAR ' +
	        'RAW_TO_VARCHAR2 RAWTOHEX RAWTONHEX RAWTONUM RAWTONUM2 REF REFTOHEX REGEXP_COUNT REGEXP_INSTR ' +
	        'REGEXP_REPLACE REGEXP_SUBSTR REGR_AVGX REGR_AVGY REGR_COUNT REGR_INTERCEPT REGR_R2 REGR_SLOPE ' +
	        'REGR_SXX REGR_SXY REGR_SYY REMAINDER REPLACE REVERSE ROUND ROW_NUMBER ROWIDTOCHAR ' +
	        'ROWIDTONCHAR RPAD RTRIM SCN_TO_TIMESTAMP SESSIONTIMEZONE SET SIGN SIN SINH SOUNDEX ' +
	        'SQRT STATS_BINOMIAL_TEST STATS_CROSSTAB STATS_F_TEST STATS_KS_TEST STATS_MODE ' +
	        'STATS_MW_TEST STATS_ONE_WAY_ANOVA STATS_T_TEST STATS_WSR_TEST STDDEV STDDEV_POP ' +
	        'STDDEV_SAMP STRING_TO_RAW SUBSTR SUBSTRB SUBSTRC SUBSTR2 SUBSTR4 SUM SYS_CONNECT_BY_PATH ' +
	        'SYS_CONTEXT SYS_DBURIGEN SYS_EXTRACT_UTC SYS_GUID SYS_OP_COMBINED_HASH SYS_OP_DESCEND ' +
	        'SYS_OP_DISTINCT SYS_OP_GUID SYS_OP_LBID SYS_OP_MAP_NONNULL SYS_OP_RAWTONUM SYS_OP_RPB ' +
	        'SYS_OP_TOSETID SYS_TYPEID SYS_XMLAGG SYS_XMLGEN SYSDATE SYSTIMESTAMP TAN TANH ' +
	        'TIMESTAMP_TO_SCN TO_BINARYDOUBLE TO_BINARYFLOAT TO_CHAR TO_CLOB TO_DATE TO_DSINTERVAL ' +
	        'TO_LOB TO_MULTI_BYTE TO_NCHAR TO_NCLOB TO_NUMBER TO_SINGLE_BYTE TO_TIMESTAMP ' +
	        'TO_TIMESTAMP_TZ TO_YMINTERVAL TRANSLATE TRANSLITERATE TREAT TRIM TRUNC TZ_OFFSET ' +
	        'UID UNISTR UPDATEXML UPPER USER USERENV VALUE VAR_POP VAR_SAMP VARIANCE VERIFY_OWNER ' +
	        'VERIFY_TABLE VERTICAL BARS VSIZE WIDTH_BUCKET XMLAGG XMLCAST XMLCDATA XMLCOLLATVAL ' +
	        'XMLCOMMENT XMLCONCAT XMLDIFF XMLELEMENT XMLEXISTS XMLFOREST XMLISVALID XMLPARSE ' +
	        'XMLPATCH XMLPI XMLQUERY XMLROOT XMLSEQUENCE XMLSERIALIZE XMLTABLE XMLTRANSFORM XOR'
	        },
	        c: [T.CLCM, T.CBCM, T.ASM, T.QSM, T.CNM, {
	            cN: "literal",
	            v: [{
	                b: "#\\s+[a-zA-Z\\ \\.]*",
	                r: 0
	            }, {
	                b: "#[a-zA-Z\\ \\.]+"
	            }]
	        }]
	    }
	});
	hljs.registerLanguage("php", function(e) {
	    var c = {
	            b: "\\$+[a-zA-Z_-ÿ][a-zA-Z0-9_-ÿ]*"
	        },
	        i = {
	            cN: "meta",
	            b: /<\?(php)?|\?>/
	        },
	        t = {
	            cN: "string",
	            c: [e.BE, i],
	            v: [{
	                    b: 'b"',
	                    e: '"'
	                }, {
	                    b: "b'",
	                    e: "'"
	                },
	                e.inherit(e.ASM, {
	                    i: null
	                }), e.inherit(e.QSM, {
	                    i: null
	                })
	            ]
	        },
	        a = {
	            v: [e.BNM, e.CNM]
	        };
	    return {
	        aliases: ["php3", "php4", "php5", "php6"],
	        cI: !0,
	        k: "and include_once list abstract global private echo interface as static endswitch array null if endwhile or const for endforeach self var while isset public protected exit foreach throw elseif include __FILE__ empty require_once do xor return parent clone use __CLASS__ __LINE__ else break print eval new catch __METHOD__ case exception default die require __FUNCTION__ enddeclare final try switch continue endfor endif declare unset true false trait goto instanceof insteadof __DIR__ __NAMESPACE__ yield finally",
	        c: [e.HCM, e.C("//", "$", {
	                c: [i]
	            }), e.C("/\\*", "\\*/", {
	                c: [{
	                    cN: "doctag",
	                    b: "@[A-Za-z]+"
	                }]
	            }), e.C("__halt_compiler.+?;", !1, {
	                eW: !0,
	                k: "__halt_compiler",
	                l: e.UIR
	            }), {
	                cN: "string",
	                b: /<<<['"]?\w+['"]?$/,
	                e: /^\w+;?$/,
	                c: [e.BE, {
	                    cN: "subst",
	                    v: [{
	                        b: /\$\w+/
	                    }, {
	                        b: /\{\$/,
	                        e: /\}/
	                    }]
	                }]
	            },
	            i, {
	                cN: "keyword",
	                b: /\$this\b/
	            },
	            c, {
	                b: /(::|->)+[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/
	            }, {
	                cN: "function",
	                bK: "function",
	                e: /[;{]/,
	                eE: !0,
	                i: "\\$|\\[|%",
	                c: [e.UTM, {
	                    cN: "params",
	                    b: "\\(",
	                    e: "\\)",
	                    c: ["self", c, e.CBCM, t, a]
	                }]
	            }, {
	                cN: "class",
	                bK: "class interface",
	                e: "{",
	                eE: !0,
	                i: /[:\(\$"]/,
	                c: [{
	                        bK: "extends implements"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "namespace",
	                e: ";",
	                i: /[\.']/,
	                c: [e.UTM]
	            }, {
	                bK: "use",
	                e: ";",
	                c: [e.UTM]
	            }, {
	                b: "=>"
	            },
	            t, a
	        ]
	    }
	});
	hljs.registerLanguage("javascript", function(e) {
	    return {
	        aliases: ["js", "jsx"],
	        k: {
	            keyword: "in of if for while finally var new function do return void else break catch instanceof with throw case default try this switch continue typeof delete let yield const export super debugger as async await static import from as",
	            literal: "true false null undefined NaN Infinity",
	            built_in: "eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape Object Function Boolean Error EvalError InternalError RangeError ReferenceError StopIteration SyntaxError TypeError URIError Number Math Date String RegExp Array Float32Array Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Uint8Array Uint8ClampedArray ArrayBuffer DataView JSON Intl arguments require module console window document Symbol Set Map WeakSet WeakMap Proxy Reflect Promise"
	        },
	        c: [{
	                cN: "meta",
	                r: 10,
	                b: /^\s*['"]use (strict|asm)['"]/
	            }, {
	                cN: "meta",
	                b: /^#!/,
	                e: /$/
	            },
	            e.ASM, e.QSM, {
	                cN: "string",
	                b: "`",
	                e: "`",
	                c: [e.BE, {
	                    cN: "subst",
	                    b: "\\$\\{",
	                    e: "\\}"
	                }]
	            },
	            e.CLCM, e.CBCM, {
	                cN: "number",
	                v: [{
	                    b: "\\b(0[bB][01]+)"
	                }, {
	                    b: "\\b(0[oO][0-7]+)"
	                }, {
	                    b: e.CNR
	                }],
	                r: 0
	            }, {
	                b: "(" + e.RSR + "|\\b(case|return|throw)\\b)\\s*",
	                k: "return throw case",
	                c: [e.CLCM, e.CBCM, e.RM, {
	                    b: /</,
	                    e: /(\/\w+|\w+\/)>/,
	                    sL: "xml",
	                    c: [{
	                        b: /<\w+\s*\/>/,
	                        skip: !0
	                    }, {
	                        b: /<\w+/,
	                        e: /(\/\w+|\w+\/)>/,
	                        skip: !0,
	                        c: ["self"]
	                    }]
	                }],
	                r: 0
	            }, {
	                cN: "function",
	                bK: "function",
	                e: /\{/,
	                eE: !0,
	                c: [e.inherit(e.TM, {
	                    b: /[A-Za-z$_][0-9A-Za-z$_]*/
	                }), {
	                    cN: "params",
	                    b: /\(/,
	                    e: /\)/,
	                    eB: !0,
	                    eE: !0,
	                    c: [e.CLCM, e.CBCM]
	                }],
	                i: /\[|%/
	            }, {
	                b: /\$[(.]/
	            },
	            e.METHOD_GUARD, {
	                cN: "class",
	                bK: "class",
	                e: /[{;=]/,
	                eE: !0,
	                i: /[:"\[\]]/,
	                c: [{
	                        bK: "extends"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "constructor",
	                e: /\{/,
	                eE: !0
	            }
	        ],
	        i: /#(?!!)/
	    }
	});
	hljs.registerLanguage("java", function(e) {
	    var t = e.UIR + "(<" + e.UIR + "(\\s*,\\s*" + e.UIR + ")*>)?",
	        a = "false synchronized int abstract float private char boolean static null if const for true while long strictfp finally protected import native final void enum else break transient catch instanceof byte super volatile case assert short package default double public try this switch continue throws protected public private module requires exports",
	        r = "\\b(0[bB]([01]+[01_]+[01]+|[01]+)|0[xX]([a-fA-F0-9]+[a-fA-F0-9_]+[a-fA-F0-9]+|[a-fA-F0-9]+)|(([\\d]+[\\d_]+[\\d]+|[\\d]+)(\\.([\\d]+[\\d_]+[\\d]+|[\\d]+))?|\\.([\\d]+[\\d_]+[\\d]+|[\\d]+))([eE][-+]?\\d+)?)[lLfF]?",
	        s = {
	            cN: "number",
	            b: r,
	            r: 0
	        };
	    return {
	        aliases: ["jsp"],
	        k: a,
	        i: /<\/|#/,
	        c: [e.C("/\\*\\*", "\\*/", {
	                r: 0,
	                c: [{
	                    b: /\w+@/,
	                    r: 0
	                }, {
	                    cN: "doctag",
	                    b: "@[A-Za-z]+"
	                }]
	            }), e.CLCM, e.CBCM, e.ASM, e.QSM, {
	                cN: "class",
	                bK: "class interface",
	                e: /[{;=]/,
	                eE: !0,
	                k: "class interface",
	                i: /[:"\[\]]/,
	                c: [{
	                        bK: "extends implements"
	                    },
	                    e.UTM
	                ]
	            }, {
	                bK: "new throw return else",
	                r: 0
	            }, {
	                cN: "function",
	                b: "(" + t + "\\s+)+" + e.UIR + "\\s*\\(",
	                rB: !0,
	                e: /[{;=]/,
	                eE: !0,
	                k: a,
	                c: [{
	                        b: e.UIR + "\\s*\\(",
	                        rB: !0,
	                        r: 0,
	                        c: [e.UTM]
	                    }, {
	                        cN: "params",
	                        b: /\(/,
	                        e: /\)/,
	                        k: a,
	                        r: 0,
	                        c: [e.ASM, e.QSM, e.CNM, e.CBCM]
	                    },
	                    e.CLCM, e.CBCM
	                ]
	            },
	            s, {
	                cN: "meta",
	                b: "@[A-Za-z]+"
	            }
	        ]
	    }
	});
	hljs.registerLanguage("css", function(e) {
	    var c = "[a-zA-Z-][a-zA-Z0-9_-]*",
	        t = {
	            b: /[A-Z\_\.\-]+\s*:/,
	            rB: !0,
	            e: ";",
	            eW: !0,
	            c: [{
	                cN: "attribute",
	                b: /\S/,
	                e: ":",
	                eE: !0,
	                starts: {
	                    eW: !0,
	                    eE: !0,
	                    c: [{
	                            b: /[\w-]+\(/,
	                            rB: !0,
	                            c: [{
	                                cN: "built_in",
	                                b: /[\w-]+/
	                            }, {
	                                b: /\(/,
	                                e: /\)/,
	                                c: [e.ASM, e.QSM]
	                            }]
	                        },
	                        e.CSSNM, e.QSM, e.ASM, e.CBCM, {
	                            cN: "number",
	                            b: "#[0-9A-Fa-f]+"
	                        }, {
	                            cN: "meta",
	                            b: "!important"
	                        }
	                    ]
	                }
	            }]
	        };
	    return {
	        cI: !0,
	        i: /[=\/|'\$]/,
	        c: [e.CBCM, {
	            cN: "selector-id",
	            b: /#[A-Za-z0-9_-]+/
	        }, {
	            cN: "selector-class",
	            b: /\.[A-Za-z0-9_-]+/
	        }, {
	            cN: "selector-attr",
	            b: /\[/,
	            e: /\]/,
	            i: "$"
	        }, {
	            cN: "selector-pseudo",
	            b: /:(:)?[a-zA-Z0-9\_\-\+\(\)"'.]+/
	        }, {
	            b: "@(font-face|page)",
	            l: "[a-z-]+",
	            k: "font-face page"
	        }, {
	            b: "@",
	            e: "[{;]",
	            i: /:/,
	            c: [{
	                cN: "keyword",
	                b: /\w+/
	            }, {
	                b: /\s/,
	                eW: !0,
	                eE: !0,
	                r: 0,
	                c: [e.ASM, e.QSM, e.CSSNM]
	            }]
	        }, {
	            cN: "selector-tag",
	            b: c,
	            r: 0
	        }, {
	            b: "{",
	            e: "}",
	            i: /\S/,
	            c: [e.CBCM, t]
	        }]
	    }
	});
	hljs.registerLanguage("xml", function(s) {
	    var e = "[A-Za-z0-9\\._:-]+",
	        t = {
	            eW: !0,
	            i: /</,
	            r: 0,
	            c: [{
	                cN: "attr",
	                b: e,
	                r: 0
	            }, {
	                b: /=\s*/,
	                r: 0,
	                c: [{
	                    cN: "string",
	                    endsParent: !0,
	                    v: [{
	                        b: /"/,
	                        e: /"/
	                    }, {
	                        b: /'/,
	                        e: /'/
	                    }, {
	                        b: /[^\s"'=<>`]+/
	                    }]
	                }]
	            }]
	        };
	    return {
	        aliases: ["html", "xhtml", "rss", "atom", "xjb", "xsd", "xsl", "plist"],
	        cI: !0,
	        c: [{
	                cN: "meta",
	                b: "<!DOCTYPE",
	                e: ">",
	                r: 10,
	                c: [{
	                    b: "\\[",
	                    e: "\\]"
	                }]
	            },
	            s.C("<!--", "-->", {
	                r: 10
	            }), {
	                b: "<\\!\\[CDATA\\[",
	                e: "\\]\\]>",
	                r: 10
	            }, {
	                b: /<\?(php)?/,
	                e: /\?>/,
	                sL: "php",
	                c: [{
	                    b: "/\\*",
	                    e: "\\*/",
	                    skip: !0
	                }]
	            }, {
	                cN: "tag",
	                b: "<style(?=\\s|>|$)",
	                e: ">",
	                k: {
	                    name: "style"
	                },
	                c: [t],
	                starts: {
	                    e: "</style>",
	                    rE: !0,
	                    sL: ["css", "xml"]
	                }
	            }, {
	                cN: "tag",
	                b: "<script(?=\\s|>|$)",
	                e: ">",
	                k: {
	                    name: "script"
	                },
	                c: [t],
	                starts: {
	                    e: "</script>",
	                    rE: !0,
	                    sL: ["actionscript", "javascript", "handlebars", "xml"]
	                }
	            }, {
	                cN: "meta",
	                v: [{
	                    b: /<\?xml/,
	                    e: /\?>/,
	                    r: 10
	                }, {
	                    b: /<\?\w+/,
	                    e: /\?>/
	                }]
	            }, {
	                cN: "tag",
	                b: "</?",
	                e: "/?>",
	                c: [{
	                        cN: "name",
	                        b: /[^\/><\s]+/,
	                        r: 0
	                    },
	                    t
	                ]
	            }
	        ]
	    }
	});
	hljs.registerLanguage("sql", function(e) {
	    var t = e.C("--", "$");
	    return {
	        cI: !0,
	        i: /[<>{}*#]/,
	        c: [{
	                bK: "begin end start commit rollback savepoint lock alter create drop rename call delete do handler insert load replace select truncate update set show pragma grant merge describe use explain help declare prepare execute deallocate release unlock purge reset change stop analyze cache flush optimize repair kill install uninstall checksum restore check backup revoke comment",
	                e: /;/,
	                eW: !0,
	                l: /[\w\.]+/,
	                k: {
	                    keyword: "abort abs absolute acc acce accep accept access accessed accessible account acos action activate add addtime admin administer advanced advise aes_decrypt aes_encrypt after agent aggregate ali alia alias allocate allow alter always analyze ancillary and any anydata anydataset anyschema anytype apply archive archived archivelog are as asc ascii asin assembly assertion associate asynchronous at atan atn2 attr attri attrib attribu attribut attribute attributes audit authenticated authentication authid authors auto autoallocate autodblink autoextend automatic availability avg backup badfile basicfile before begin beginning benchmark between bfile bfile_base big bigfile bin binary_double binary_float binlog bit_and bit_count bit_length bit_or bit_xor bitmap blob_base block blocksize body both bound buffer_cache buffer_pool build bulk by byte byteordermark bytes cache caching call calling cancel capacity cascade cascaded case cast catalog category ceil ceiling chain change changed char_base char_length character_length characters characterset charindex charset charsetform charsetid check checksum checksum_agg child choose chr chunk class cleanup clear client clob clob_base clone close cluster_id cluster_probability cluster_set clustering coalesce coercibility col collate collation collect colu colum column column_value columns columns_updated comment commit compact compatibility compiled complete composite_limit compound compress compute concat concat_ws concurrent confirm conn connec connect connect_by_iscycle connect_by_isleaf connect_by_root connect_time connection consider consistent constant constraint constraints constructor container content contents context contributors controlfile conv convert convert_tz corr corr_k corr_s corresponding corruption cos cost count count_big counted covar_pop covar_samp cpu_per_call cpu_per_session crc32 create creation critical cross cube cume_dist curdate current current_date current_time current_timestamp current_user cursor curtime customdatum cycle data database databases datafile datafiles datalength date_add date_cache date_format date_sub dateadd datediff datefromparts datename datepart datetime2fromparts day day_to_second dayname dayofmonth dayofweek dayofyear days db_role_change dbtimezone ddl deallocate declare decode decompose decrement decrypt deduplicate def defa defau defaul default defaults deferred defi defin define degrees delayed delegate delete delete_all delimited demand dense_rank depth dequeue des_decrypt des_encrypt des_key_file desc descr descri describ describe descriptor deterministic diagnostics difference dimension direct_load directory disable disable_all disallow disassociate discardfile disconnect diskgroup distinct distinctrow distribute distributed div do document domain dotnet double downgrade drop dumpfile duplicate duration each edition editionable editions element ellipsis else elsif elt empty enable enable_all enclosed encode encoding encrypt end end-exec endian enforced engine engines enqueue enterprise entityescaping eomonth error errors escaped evalname evaluate event eventdata events except exception exceptions exchange exclude excluding execu execut execute exempt exists exit exp expire explain export export_set extended extent external external_1 external_2 externally extract failed failed_login_attempts failover failure far fast feature_set feature_value fetch field fields file file_name_convert filesystem_like_logging final finish first first_value fixed flash_cache flashback floor flush following follows for forall force form forma format found found_rows freelist freelists freepools fresh from from_base64 from_days ftp full function general generated get get_format get_lock getdate getutcdate global global_name globally go goto grant grants greatest group group_concat group_id grouping grouping_id groups gtid_subtract guarantee guard handler hash hashkeys having hea head headi headin heading heap help hex hierarchy high high_priority hosts hour http id ident_current ident_incr ident_seed identified identity idle_time if ifnull ignore iif ilike ilm immediate import in include including increment index indexes indexing indextype indicator indices inet6_aton inet6_ntoa inet_aton inet_ntoa infile initial initialized initially initrans inmemory inner innodb input insert install instance instantiable instr interface interleaved intersect into invalidate invisible is is_free_lock is_ipv4 is_ipv4_compat is_not is_not_null is_used_lock isdate isnull isolation iterate java join json json_exists keep keep_duplicates key keys kill language large last last_day last_insert_id last_value lax lcase lead leading least leaves left len lenght length less level levels library like like2 like4 likec limit lines link list listagg little ln load load_file lob lobs local localtime localtimestamp locate locator lock locked log log10 log2 logfile logfiles logging logical logical_reads_per_call logoff logon logs long loop low low_priority lower lpad lrtrim ltrim main make_set makedate maketime managed management manual map mapping mask master master_pos_wait match matched materialized max maxextents maximize maxinstances maxlen maxlogfiles maxloghistory maxlogmembers maxsize maxtrans md5 measures median medium member memcompress memory merge microsecond mid migration min minextents minimum mining minus minute minvalue missing mod mode model modification modify module monitoring month months mount move movement multiset mutex name name_const names nan national native natural nav nchar nclob nested never new newline next nextval no no_write_to_binlog noarchivelog noaudit nobadfile nocheck nocompress nocopy nocycle nodelay nodiscardfile noentityescaping noguarantee nokeep nologfile nomapping nomaxvalue nominimize nominvalue nomonitoring none noneditionable nonschema noorder nopr nopro noprom nopromp noprompt norely noresetlogs noreverse normal norowdependencies noschemacheck noswitch not nothing notice notrim novalidate now nowait nth_value nullif nulls num numb numbe nvarchar nvarchar2 object ocicoll ocidate ocidatetime ociduration ociinterval ociloblocator ocinumber ociref ocirefcursor ocirowid ocistring ocitype oct octet_length of off offline offset oid oidindex old on online only opaque open operations operator optimal optimize option optionally or oracle oracle_date oradata ord ordaudio orddicom orddoc order ordimage ordinality ordvideo organization orlany orlvary out outer outfile outline output over overflow overriding package pad parallel parallel_enable parameters parent parse partial partition partitions pascal passing password password_grace_time password_lock_time password_reuse_max password_reuse_time password_verify_function patch path patindex pctincrease pctthreshold pctused pctversion percent percent_rank percentile_cont percentile_disc performance period period_add period_diff permanent physical pi pipe pipelined pivot pluggable plugin policy position post_transaction pow power pragma prebuilt precedes preceding precision prediction prediction_cost prediction_details prediction_probability prediction_set prepare present preserve prior priority private private_sga privileges procedural procedure procedure_analyze processlist profiles project prompt protection public publishingservername purge quarter query quick quiesce quota quotename radians raise rand range rank raw read reads readsize rebuild record records recover recovery recursive recycle redo reduced ref reference referenced references referencing refresh regexp_like register regr_avgx regr_avgy regr_count regr_intercept regr_r2 regr_slope regr_sxx regr_sxy reject rekey relational relative relaylog release release_lock relies_on relocate rely rem remainder rename repair repeat replace replicate replication required reset resetlogs resize resource respect restore restricted result result_cache resumable resume retention return returning returns reuse reverse revoke right rlike role roles rollback rolling rollup round row row_count rowdependencies rowid rownum rows rtrim rules safe salt sample save savepoint sb1 sb2 sb4 scan schema schemacheck scn scope scroll sdo_georaster sdo_topo_geometry search sec_to_time second section securefile security seed segment select self sequence sequential serializable server servererror session session_user sessions_per_user set sets settings sha sha1 sha2 share shared shared_pool short show shrink shutdown si_averagecolor si_colorhistogram si_featurelist si_positionalcolor si_stillimage si_texture siblings sid sign sin size size_t sizes skip slave sleep smalldatetimefromparts smallfile snapshot some soname sort soundex source space sparse spfile split sql sql_big_result sql_buffer_result sql_cache sql_calc_found_rows sql_small_result sql_variant_property sqlcode sqldata sqlerror sqlname sqlstate sqrt square standalone standby start starting startup statement static statistics stats_binomial_test stats_crosstab stats_ks_test stats_mode stats_mw_test stats_one_way_anova stats_t_test_ stats_t_test_indep stats_t_test_one stats_t_test_paired stats_wsr_test status std stddev stddev_pop stddev_samp stdev stop storage store stored str str_to_date straight_join strcmp strict string struct stuff style subdate subpartition subpartitions substitutable substr substring subtime subtring_index subtype success sum suspend switch switchoffset switchover sync synchronous synonym sys sys_xmlagg sysasm sysaux sysdate sysdatetimeoffset sysdba sysoper system system_user sysutcdatetime table tables tablespace tan tdo template temporary terminated tertiary_weights test than then thread through tier ties time time_format time_zone timediff timefromparts timeout timestamp timestampadd timestampdiff timezone_abbr timezone_minute timezone_region to to_base64 to_date to_days to_seconds todatetimeoffset trace tracking transaction transactional translate translation treat trigger trigger_nestlevel triggers trim truncate try_cast try_convert try_parse type ub1 ub2 ub4 ucase unarchived unbounded uncompress under undo unhex unicode uniform uninstall union unique unix_timestamp unknown unlimited unlock unpivot unrecoverable unsafe unsigned until untrusted unusable unused update updated upgrade upped upper upsert url urowid usable usage use use_stored_outlines user user_data user_resources users using utc_date utc_timestamp uuid uuid_short validate validate_password_strength validation valist value values var var_samp varcharc vari varia variab variabl variable variables variance varp varraw varrawc varray verify version versions view virtual visible void wait wallet warning warnings week weekday weekofyear wellformed when whene whenev wheneve whenever where while whitespace with within without work wrapped xdb xml xmlagg xmlattributes xmlcast xmlcolattval xmlelement xmlexists xmlforest xmlindex xmlnamespaces xmlpi xmlquery xmlroot xmlschema xmlserialize xmltable xmltype xor year year_to_month years yearweek",
	                    literal: "true false null",
	                    built_in: "array bigint binary bit blob boolean char character date dec decimal float int int8 integer interval number numeric real record serial serial8 smallint text varchar varying void"
	                },
	                c: [{
	                        cN: "string",
	                        b: "'",
	                        e: "'",
	                        c: [e.BE, {
	                            b: "''"
	                        }]
	                    }, {
	                        cN: "string",
	                        b: '"',
	                        e: '"',
	                        c: [e.BE, {
	                            b: '""'
	                        }]
	                    }, {
	                        cN: "string",
	                        b: "`",
	                        e: "`",
	                        c: [e.BE]
	                    },
	                    e.CNM, e.CBCM, t
	                ]
	            },
	            e.CBCM, t
	        ]
	    }
	});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(126)))

/***/ },

/***/ 127:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(128);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js!./github.css", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js!./github.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 128:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "/*\n\ngithub.com style (c) Vasily Polovnyov <vast@whiteants.net>\n\n*/\n\n.hljs {\n  display: block;\n  overflow-x: auto;\n  padding: 0.5em;\n  color: #333;\n  background: #f8f8f8;\n}\n\n.hljs-comment,\n.hljs-quote {\n  color: #998;\n  font-style: italic;\n}\n\n.hljs-keyword,\n.hljs-selector-tag,\n.hljs-subst {\n  color: #333;\n  font-weight: bold;\n}\n\n.hljs-number,\n.hljs-literal,\n.hljs-variable,\n.hljs-template-variable,\n.hljs-tag .hljs-attr {\n  color: #008080;\n}\n\n.hljs-string,\n.hljs-doctag {\n  color: #d14;\n}\n\n.hljs-title,\n.hljs-section,\n.hljs-selector-id {\n  color: #900;\n  font-weight: bold;\n}\n\n.hljs-subst {\n  font-weight: normal;\n}\n\n.hljs-type,\n.hljs-class .hljs-title {\n  color: #458;\n  font-weight: bold;\n}\n\n.hljs-tag,\n.hljs-name,\n.hljs-attribute {\n  color: #000080;\n  font-weight: normal;\n}\n\n.hljs-regexp,\n.hljs-link {\n  color: #009926;\n}\n\n.hljs-symbol,\n.hljs-bullet {\n  color: #990073;\n}\n\n.hljs-built_in,\n.hljs-builtin-name {\n  color: #0086b3;\n}\n\n.hljs-meta {\n  color: #999;\n  font-weight: bold;\n}\n\n.hljs-deletion {\n  background: #fdd;\n}\n\n.hljs-addition {\n  background: #dfd;\n}\n\n.hljs-emphasis {\n  font-style: italic;\n}\n\n.hljs-strong {\n  font-weight: bold;\n}\n", ""]);

	// exports


/***/ },

/***/ 158:
/***/ function(module, exports) {

	angular
		.module('service')
		.service('Mail',Mail);

	Mail.$inject = ['$http','BASE_API'];

	function Mail($http,BASE_API) {
	    this.sendMail = function (data) {
	        return $http.post(BASE_API + '/mail/send', data).then(function (res) {
	            return res.data;
	        });
	    }

	    this.sendBulkEmails = function (data) {
	        return $http.post(BASE_API + '/mail/sendBulkEmails', data);
	    }

	    this.subscribeUser = function (data) {
	        return $http.post(BASE_API + '/mail/subscribeUser', data);
	    }

	    this.sendMailToSupportForEnrollBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForEnrollBtn', data);   
	    }

	    this.sendMailToSupportForPaySecurelyBtn = function(data){
	        return $http.post(BASE_API + '/mail/sendMailToSupportForPaySecurelyBtn', data);   
	    }
	}


/***/ },

/***/ 188:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {window.jwplayer = __webpack_require__(189);
	window.jwplayer.key = "fA95pIQiK4bMKdS9Arqw70sKJ3GIvpO7elB7XQ==";
	__webpack_require__(190);
	window.noUiSlider = __webpack_require__(192);
	__webpack_require__(193);
	__webpack_require__(194);
	__webpack_require__(195);
	__webpack_require__(57);
	__webpack_require__(125);
	__webpack_require__(120);
	__webpack_require__(196);
	__webpack_require__(197);
	var WebVTTParser = __webpack_require__(198);

	angular
	    .module('directive')
	    .directive('videoControlsJw', VideoControls)
	    .controller('VideoControlsCtrl', VideoControlsCtrl);

	VideoControlsCtrl.$inject = ['$log', '$timeout', 'infoDetection', 'fullScreen', 'videoPlayer', '$scope', 'authToken', 'utility', 'admin', 'videoUrl', 'videoViews'];

	function VideoControlsCtrl($log, $timeout, infoDetection, fullScreen, videoPlayer, $scope, authToken, utility, admin, videoUrl, videoViews) {
	    var vm = this;

	    if(window.innerWidth < 1024){
	        vm.isMobileTabletDevice = true;
	    }else{
	        vm.isMobileTabletDevice = false;
	    }

	    //Variables
	    vm.isPlaying = false;
	    vm.isFullScreen = false;
	    vm.isMute = false;
	    vm.isSubtitleOn = false;
	    vm.isContinuousPlay = false;

	    vm.currentPlayBackRate = '1.0';
	    vm.videoQuality = "HIGH";
	    vm.offset_time = 8;
	    vm.isDimLights = false;
	    vm.video_current = "00:00";
	    vm.video_duration = "00:00";
	    vm.isLoading = true;
	    vm.isPlayerReady = false;

	    vm.shouldShowLoader = true;
	    vm.isLoading = true;
	    // vm.isPlaying = true;
	    vm.videoPlayer = {};

	    vm.playerVolume = authToken.playerVolume();
	    vm.playerSpeed = authToken.playerSpeed();
	    vm.isPlayerMute = authToken.playerMute();
	    vm.playerVolume = authToken.playerVolume();
	    vm.subtitleStatus = authToken.subtitleStatus() || false;
	    vm.isAutoplay = authToken.playerAutoplay() || false;
	    vm.isInTheaterMode = authToken.playerTheaterMode() || false;
	    vm.videoQuality = authToken.playerQuality()

	    //Funtions	
	    //
	    vm.isUserLoggedIn = authToken.isAuthenticated;
	    vm.goToLogin = utility.goToLogin;


	    vm.playToggle = playToggle;
	    vm.rePlay = rePlay;
	    vm.fullscreen = fullscreen;
	    vm.toggleVolume = toggleVolume;
	    vm.subtitleToggle = subtitleToggle;
	    vm.changeSpeed = changeSpeed;
	    vm.resetSpeed = resetSpeed;
	    vm.changeVideoQuality = changeVideoQuality;
	    vm.fastForward = fastForward;
	    vm.rewind = rewind;
	    vm.setFallBack = setFallBack;
	    vm.changeVideoPlayer = changeVideoPlayer;

	    vm.nextVideo = nextVideo;
	    vm.prevVideo = prevVideo;

	    vm.setPlayBackRate = setPlayBackRate;
	    vm.getPlayBackRate = getPlayBackRate;

	    vm.initVideoPlayer = initVideoPlayer;
	    vm.toggleOverlay = toggleOverlay;
	    vm.toggleContinuousPlay = toggleContinuousPlay;
	    vm.toggleMute = toggleMute;
	    vm.popoutVideo = popoutVideo;

	    vm.inBrowserFullScreen = inBrowserFullScreen;
	    vm.setPlayerMute = setPlayerMute;
	    vm.setPlayerVolume = setPlayerVolume;
	    vm.setSubtitle = setSubtitle;
	    vm.setContinuousPlay = setContinuousPlay;
	    vm.setTheaterMode = setTheaterMode;

	    videoQualities();
	    videoSpeeds();
	    initVideoPlayer();
	    // trackStatsOfPlayer();
	    // 

	    // set variables based on localstorage settings
	    vm.subtitleStatus = authToken.subtitleStatus() || false;
	    vm.isAutoplay = authToken.playerAutoplay() || false;
	    vm.setSubtitle(vm.subtitleStatus);
	    vm.setContinuousPlay(vm.isAutoplay);
	    // vm.isInTheaterMode = authToken.playerTheaterMode() || false;
	    // if(vm.isInTheaterMode){
	    //     vm.theaterModeCallback();
	    // }

	    function initVideoPlayer() {

	        if (vm.playerId && vm.playerUrl) {
	            vm.videoId = vm.playerId;
	        } else {
	            vm.videoId = "video_jw";
	        }
	    }

	    $scope.$on('takePrevVideoName', function(event, statsPrevVideoObj) {
	        // vm.prevVideoName = prevVideoName;
	        vm.statsPrevVideoObj = statsPrevVideoObj;
	    });
	    $scope.$on('takeNextVideoName', function(event, statsNextVideoObj) {
	        vm.statsNextVideoObj = statsNextVideoObj;
	    });

	    function sendRequestForPrevVideoName() {
	        $scope.$emit('sendPrevVideoName');
	    }

	    function sendRequestForNextVideoName() {
	        $scope.$emit('sendNextVideoName');
	    }

	    //observe video playing object
	    //if change detect , play new video
	    $scope.$watch(function() {
	        return vm.playingVideoObject;
	    }, function(newValue, oldValue) {
	        if (newValue != oldValue && newValue) {
	            loadNewVideo();
	        }
	    });

	    function loadNewVideo() {
	        var videoId = vm.playingVideoObject.videoId;
	        var videoTime = vm.playingVideoObject.videoTime;
	        //fetch video object from server
	        return findVideoEntity(videoId)
	            .then(fetchVideoUrl) //fetch video url from server
	            .then(function(url) {
	                if (url) {
	                    //got and url and subtitleurl
	                    var videoUrl = url.videoUrl;
	                    var subtitleUrl = url.subtitleUrl;
	                    //load video with new video url
	                    loadVideo(videoUrl, subtitleUrl);

	                    if (videoTime && vm.videoPlayer.seek) {
	                        // if there is videotime then seek video to videotime
	                        $log.debug("Setting video time to..", videoTime);
	                        vm.videoPlayer.seek(videoTime);
	                    }
	                } else {
	                    vm.playerError = true;
	                }
	                return;
	            })
	            .catch(function(err) {
	                $log.error(err);
	            })
	    }

	    function findVideoEntity(videoId) {

	        vm.findVideoTimer = $timeout(function() {
	            vm.playerError = true;
	        }, 1000 * 20);


	        return admin.findVideoEntityById({
	                videoId: videoId
	            })
	            .then(function(res) {

	                $timeout.cancel(vm.findVideoTimer);

	                var v = res.data[0] || {};
	                //Change video name based on video quality
	                var videoFileNameWithExt = v.videoUrl;
	                if (!videoFileNameWithExt) return;
	                var extPos = videoFileNameWithExt.lastIndexOf(".");

	                //get only video file name
	                var videoFileName = videoFileNameWithExt.substring(0, extPos);
	                //get only extension
	                var ext = videoFileNameWithExt.substring(extPos)
	                    //final video file name
	                var finalVideoFileName = videoFileNameWithExt;

	                vm.videoQuality = authToken.playerQuality() || "HIGH";

	                if (vm.videoQuality === "HIGH") {
	                    finalVideoFileName = videoFileName + ext;
	                } else if (vm.videoQuality === "MED") {
	                    finalVideoFileName = videoFileName + "-480" + ext;
	                } else if (vm.videoQuality === "LOW") {
	                    finalVideoFileName = videoFileName + "-360" + ext;
	                }
	                v.videoUrl = finalVideoFileName;
	                if(v.thumbnailPath){
	                    vm.downloadVTTFile(v.thumbnailPath);    
	                }
	                return v;
	            })
	            .catch(function(err) {
	                $log.error(err);
	            });
	    }

	    function fetchVideoUrl(video) {
	        return videoUrl.fetchVideoUrl(video)
	            .then(function(video) {
	                var url = video.url;
	                var subtitleUrl = video.subtitleUrl;
	                return {
	                    videoUrl: url,
	                    subtitleUrl: subtitleUrl
	                };
	            })
	            .catch(function(err) {
	                $log.error(err);
	            });
	    }


	    var videos = {
	        'mpeg-dash': {
	            type: 'application/dash+xml',
	            src: 'http://rdmedia.bbc.co.uk/dash/ondemand/bbb/2/client_manifest-common_init.mpd'
	        },
	        'hls': {
	            type: 'application/x-mpegURL',
	            src: 'http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8'
	        },
	        'flash': {
	            type: 'rtmp/mp4',
	            src: 'rtmp://192.168.1.2:1935/vod/mp4:sanam.mp4'
	        },
	        'html5': {
	            type: 'video/mp4',
	            src: 'assets/video/1.mp4'
	        }
	    }

	    function fallbackPossibilities() {

	        return {
	            'windows+modern': [
	                //videos['mpeg-dash'], //Mpeg-dash
	                // videos['hls'], //HLS
	                videos['html5'], //HTML5
	                //videos['flash'] //Flash
	            ],
	            'windows+old': [
	                videos['flash'], //Flash
	                videos['html5'] //HTML5
	            ],
	            'windows+ie+modern': [
	                // videos['mpeg-dash'], //Mpeg-dash
	                // videos['flash'], //flash
	                videos['html5'] //html5
	            ],
	            'windows+ie+old': [
	                // videos['flash'], //flash
	                videos['html5'] //html5
	            ],
	            'mac+modern': [
	                // videos['hls'], //hls
	                // videos['mpeg-dash'], //mpeg-dash
	                videos['html5'], //html5
	                // videos['flash'] //flash
	            ],
	            'mac+old': [
	                videos['html5'], //html5
	                videos['flash'] //flash    
	            ]
	        }
	    }

	    function setFallBack() {
	        var comp = infoDetection.getCompatibility();

	        var poss = fallbackPossibilities();
	        //vm.videoPlayer.load([{
	        //    file: poss[comp][0].src,
	        //    image: "assets/video/1.jpg",
	        //    tracks: [{
	        //        file: "assets/video/1.vtt"
	        //    }]
	        //}]);

	    }

	    $scope.$on('loadVideo', function(event, video, subtitleUrl) {
	        loadVideo(video, subtitleUrl);
	    });
	    $scope.$on('loadVideoFromNote', function(event, video, time, subtitleUrl) {
	        loadVideo(video, subtitleUrl);
	        vm.videoPlayer.seek(time);
	    });
	    $scope.$on('setupVideo', function(event, video) {
	        if (!vm.isPlayerReady && vm.videoPlayer) {
	            vm.videoPlayer.setup({
	                playlist: [{
	                    sources: [{
	                        file: video
	                    }]
	                }],
	                width: '100%',
	                height: '100%',
	                // primary: 'html5',
	                controls: false,
	                // repeat : false,
	                // autostart : false,
	            });
	            vm.setupVideoPlayer();
	        }

	        // vm.videoPlayer.play(true);
	        // vm.isPlaying = true;
	    });
	    $scope.$on('pauseVideo', function() {
	        if (vm.isPlaying) {
	            vm.videoPlayer.play(false);
	            vm.isPlaying = false;
	        }
	    });
	    $scope.$on('replayVideo', function() {
	        if (vm.videoPlayer.play) {
	            vm.videoPlayer.play();
	        }
	    });

	    //If Multiple videos on one page
	    //Then this event will be fired
	    //Pause all with except the video being played
	    $scope.$on('pauseAllVideoExceptWithId', function(event, playerId) {
	        if (vm.playerId !== playerId) {
	            vm.videoPlayer.play(false);
	            vm.isPlaying = false;
	        }
	    });
	    $scope.$on('playVideo', function() {
	        if (!vm.isPlaying) {
	            vm.videoPlayer.play(true);
	            vm.isPlaying = true;
	        }
	    });

	    $scope.$on('videoName', function(eve, videoName) {
	        vm.videoTitle = videoName;
	    })
	    
	    vm.loadVideo = loadVideo;
	    function loadVideo(video, subtitleUrl) {
	        if (vm.isRestricted) {
	            return;
	        }
	        if (vm.videoPlayer && vm.videoPlayer.remove) {
	            // console.log("removing...");
	            vm.videoPlayer.remove();
	        }
	        vm.videoPlayer = {};
	        vm.isPlayerReady = false;

	        $log.debug("Loading Video.......", video);
	        $log.debug("Loading Subtitle.......", subtitleUrl);

	        sendRequestForNextVideoName();
	        sendRequestForPrevVideoName();

	        vm.shouldShowLoader = true;
	        vm.isLoading = true;
	        vm.playerError = false;
	        vm.isVideoComplete = false;
	        vm.isVideoStarted = false;
	        vm.isPlaying = false;
	        if (!vm.videoPlayer.setup) {
	            vm.videoPlayer = jwplayer(vm.videoId);
	        }

	        $timeout(function() {
	            vm.isPlayerMute = authToken.playerMute();
	            vm.playerVolume = authToken.playerVolume();
	            vm.subtitleStatus = authToken.subtitleStatus() || false;
	            vm.isAutoplay = authToken.playerAutoplay() || false;

	            vm.setPlayerVolume(vm.playerVolume);
	            vm.setSubtitle(vm.subtitleStatus);
	            vm.setContinuousPlay(vm.isAutoplay);

	            vm.isInTheaterMode = authToken.playerTheaterMode() || false;
	            if (vm.isInTheaterMode) {
	                vm.theaterModeCallback({
	                    isVideoInTheaterMode: vm.isInTheaterMode
	                });
	            }
	        })
	        if (!vm.isPlayerReady) {
	            vm.videoPlayer.setup({
	                playlist: [{
	                    sources: [{
	                        file: video
	                    }],
	                    tracks: [{
	                        file: subtitleUrl,
	                        label: "English",
	                        kind: "captions",
	                        default: true
	                    }]
	                }],
	                width: '100%',
	                height: '100%',
	                primary: 'html5',
	                controls: false,
	                mute: true,
	                autostart: (vm.autoStart == 'true')
	            });

	            vm.setupVideoPlayer();

	            return;
	        }

	        vm.video_current = "00:00";
	        vm.video_duration = "00:00";

	        vm.currentVideoUrl = video;
	        vm.videoPlayer.load([{
	            file: video
	        }]);
	        $timeout(function() {
	            vm.playerSpeed = authToken.playerSpeed();
	            // console.log(vm.playerSpeed);
	            vm.changeSpeed(vm.playerSpeed || 1);
	        });

	        vm.videoPlayer.play(true);

	    }

	    function playToggle() {
	        // console.log("playToggle...",vm.videoId,$("#"+vm.videoId),$("#"+vm.videoId).find("video"));
	        // var videoElement = $("#"+vm.videoId).find("video");
	        // if(videoElement.length <= 0){
	        //     loadNewVideo();
	        // }
	        if (vm.isPlaying) {
	            vm.videoPlayer.play(false);
	            vm.isPlaying = false;
	            vm.showAnimatedPauseBtn = true;
	            vm.showAnimatedPlayBtn = false;
	        } else {
	            vm.videoPlayer.play(true);
	            vm.isVideoStarted = true;
	            vm.isPlaying = true;
	            vm.showAnimatedPlayBtn = true;
	            vm.showAnimatedPauseBtn = false;
	            //If Multiple videos on one page 
	            //Except The one which is being played
	            $scope.$emit('notificationToPauseAllVideo', vm.playerId);
	        }
	    }

	    function rePlay() {
	        setFallBack();
	        vm.videoPlayer.play(true);
	    }



	    $timeout(function() {
	        $($("#" + vm.videoId).parent()[0]).on('fullscreenchange', function(e, fullScreenEnabled) {
	            console.log("in controller fullscreen", vm.isFullScreen)
	            if (fullScreenEnabled) {
	                e.stopPropagation();
	                vm.isFullScreen = true;
	            } else {
	                console.log("doing exitFullScreen controlbar")
	                vm.isFullScreen = false;
	            }
	            $scope.$apply();
	        });
	    })

	    function fullscreen() {
	        if (vm.isFullScreen) {
	            fullScreen.exitFullScreen();
	        } else {
	            if (vm.isInTheaterMode) {
	                vm.isInTheaterMode = false;
	                authToken.playerTheaterMode(false);
	                vm.theaterModeCallback({
	                    isVideoInTheaterMode: vm.isInTheaterMode
	                });
	            }
	            fullScreen.launchFullScreen($("#" + vm.videoId).parent()[0]);
	        }
	        vm.isFullScreen = !vm.isFullScreen;
	    }


	    function toggleVolume() {
	        if (vm.isMute) {
	            vm.videoPlayer.volume(1);
	            vm.volumeSlider.noUiSlider.set(100);
	        } else {
	            vm.videoPlayer.volume(0);
	            vm.volumeSlider.noUiSlider.set(0);
	        }
	        vm.isMute = !vm.isMute;
	    }


	    function subtitleToggle(str) { //Str = ON/OFF/null
	        // if (!str) {
	        //     vm.isSubtitleOn = !vm.isSubtitleOn;
	        // } else if (str && str.toLowerCase() === "on") {
	        //     vm.isSubtitleOn = true;
	        // } else if (str && str.toLowerCase() === "off") {
	        //     vm.isSubtitleOn = false;
	        // }

	        vm.isSubtitleOn = !vm.isSubtitleOn;

	        if (vm.isSubtitleOn) {
	            setSubtitle(vm.isSubtitleOn);
	            // var wrap = $('#jw_video_wrapper')[0];
	            // $(wrap).css('height', 'calc(100% - 85px - 40px'); //58px - Control Bar , 40px - Subtitle    
	        } else {
	            setSubtitle(false);
	            // var wrap = $('#jw_video_wrapper')[0];
	            // $(wrap).css('height', 'calc(100% - 85px'); //58px - Control Bar
	        }

	    }

	    function setSubtitle(isSubtitleOn) { //Str = ON/OFF/null
	        if (isSubtitleOn) {
	            var wrap = $('#jw_video_wrapper')[0];
	            $(wrap).css('height', 'calc(100% - 85px - 40px'); //58px - Control Bar , 40px - Subtitle    
	            vm.isSubtitleOn = authToken.subtitleStatus(true);
	        } else {
	            var wrap = $('#jw_video_wrapper')[0];
	            $(wrap).css('height', 'calc(100% - 85px'); //58px - Control Bar
	            vm.isSubtitleOn = authToken.subtitleStatus(false);
	        }
	    }


	    function changeSpeed(val) {
	        vm.currentPlayBackRate = val.toFixed(1);
	        vm.setPlayBackRate(val);
	        authToken.playerSpeed(val);
	    }

	    function setPlayBackRate(rate) {
	        var video = document.getElementsByTagName('video')[0];
	        if (video) {
	            video.playbackRate = rate;
	        }
	    }

	    function getPlayBackRate() {
	        var video = document.getElementsByTagName('video')[0];
	        if (video) {
	            return video.playbackRate;
	        }
	        return 1;
	    }

	    function resetSpeed() {
	        vm.changeSpeed(1);
	    }

	    function rewind() {
	        var currentTime = vm.videoPlayer.getPosition();
	        var nextTime = currentTime - vm.offset_time;
	        vm.videoPlayer.seek(nextTime);
	        vm.isPlaying = true;
	    }

	    function fastForward() {
	        var currentTime = vm.videoPlayer.getPosition();
	        if (currentTime < vm.videoPlayer.getDuration()) {
	            var nextTime = currentTime + vm.offset_time;
	            vm.videoPlayer.seek(nextTime);
	            vm.isPlaying = true;
	        }
	    }

	    function prevVideo(isUserClicked) {
	        if (isUserClicked) {
	            $scope.$emit('prevVideo', true);
	        } else {
	            $scope.$emit('prevVideo', vm.isContinuousPlay);
	        }

	    }

	    function nextVideo(isUserClicked) {
	        if (isUserClicked) {
	            $scope.$emit('nextVideo', true);
	        } else {
	            $scope.$emit('nextVideo', vm.isContinuousPlay);
	        }
	    }

	    function changeVideoQuality(quality) { //quality = HIGH / MED / LOW
	        vm.videoQuality = quality;
	        //set settings to local storage
	        authToken.playerQuality(quality);
	        //capture current video time
	        vm.playingVideoObject.videoTime = vm.videoPlayer.getPosition();
	        //load new video based on quality
	        loadNewVideo();

	    }

	    function changeVideoPlayer() {
	        var currentTime = vm.videoPlayer.getPosition();
	        $scope.$emit('changeVideoPlayer', vm.currentVideoUrl, currentTime);
	    }

	    function videoQualities() {
	        vm.videoQualities = {
	            '720p': 'assets/video/1-720.mp4',
	            '360p': 'assets/video/1-360.mp4',
	            '244p': 'assets/video/1-244.mp4',
	        }
	    }

	    function videoSpeeds() {

	        vm.videoSpeeds = {
	            '0.5x': '0.5',
	            '1x': '1',
	            '1.5x': '1.5',
	            '2x': '2',
	            '4x': '4',
	            '16x': '16',
	        }
	    }

	    function toggleOverlay($event) {
	        var ele = $($event.currentTarget).parent();
	        $event.stopPropagation();
	        if ($(ele).hasClass('open')) {
	            $('.ds-control').removeClass('open');
	            $('.ds-control').find('.overlay').hide();
	        } else {
	            $('.ds-control').removeClass('open');
	            $('.ds-control').find('.overlay').hide();
	            $(ele).addClass('open');
	            $(ele).find('.overlay').show();
	        }
	    }

	    function toggleContinuousPlay(isAutoplayOn) {
	        // vm.isContinuousPlay = !vm.isContinuousPlay;
	        vm.isContinuousPlay = isAutoplayOn;
	        setContinuousPlay(vm.isContinuousPlay);
	    }

	    function setContinuousPlay(isContinuousPlay) {
	        authToken.playerAutoplay(isContinuousPlay);
	        vm.isContinuousPlay = isContinuousPlay;
	    }

	    function toggleMute() {
	        var parent = $('.ds-sound-progress-wrapper.' + vm.videoId);
	        //set width of sound progress bar
	        var real_progress_bar = $(parent).find('.ds-sound-real-progress');
	        // console.log(vm.isPlayerMute);
	        if (vm.isPlayerMute) {
	            var volume = vm.prevVolume ? vm.prevVolume : 100;
	            // vm.videoPlayer.setVolume(volume);
	            // $(real_progress_bar).width(volume + "%");
	            vm.setPlayerMute(false);
	            vm.setPlayerVolume(volume);
	            vm.isPlayerMute = false;
	        } else {
	            vm.prevVolume = vm.videoPlayer.getVolume();
	            // vm.videoPlayer.setVolume(0);
	            // $(real_progress_bar).width("0%");
	            vm.setPlayerMute(true);
	            vm.setPlayerVolume(0);
	            vm.isPlayerMute = true;
	        }
	    }

	    function popoutVideo() {
	        //capture current video time
	        var videoTime = vm.videoPlayer.getPosition();
	        $scope.$emit('popoutVideo', videoTime);
	    }

	    function inBrowserFullScreen() {
	        vm.isInBrowserFullScreen = !vm.isInBrowserFullScreen;
	    }

	    // $(".main-video-player").on('click',function(e){

	    // }

	    function trackStatsOfPlayer() {
	        $(".main-video-player").on('click', function(e) {
	            var target = e.target;
	            // e.stopPropagation();
	            var title = $(target).data("click-title");
	            var value = $(target).attr("data-click-value");
	            if (vm.videoPlayer) {
	                var obj = {
	                    usr_id: authToken.getUserId() || 'n/a',
	                    // subtitle : ''+vm.isSubtitleOn,
	                    spd_ctrl: '' + vm.currentPlayBackRate,
	                    vdo_qlty: '' + vm.videoQuality,
	                    vdo_full_scrn: '' + vm.isFullScreen,
	                    // theaterMode : (vm.isInTheaterMode) ? 'true' : 'false',
	                    vdo_vw_perc: '' + ((vm.videoPlayer.getPosition() / vm.videoPlayer.getDuration()) * 100),
	                    vdo_pop_out: (vm.isPopoutVideo !== undefined) ? 'true' : 'false',
	                    vdo_id: vm.playingVideoObject.videoId,
	                    vdo_vw_dt: new Date()
	                }

	                videoViews.submitVideoStats({
	                    topic: "videostats",
	                    stats: JSON.stringify(obj)
	                });
	            }

	        })
	        window.addEventListener("beforeunload", function(e) {
	            // console.log("test....");
	            //before browser close or tab close 
	            //send event to server
	            if (vm.videoPlayer && vm.videoPlayer.getPosition) {
	                var obj = {
	                    usr_id: authToken.getUserId() || 'n/a',
	                    // subtitle : vm.isSubtitleOn,
	                    spd_ctrl: vm.currentPlayBackRate,
	                    vdo_qlty: vm.videoQuality,
	                    vdo_full_scrn: vm.isFullScreen,
	                    // theaterMode : (vm.isInTheaterMode) ? 'true':'false',
	                    vdo_vw_perc: '' + ((vm.videoPlayer.getPosition() / vm.videoPlayer.getDuration()) * 100),
	                    vdo_pop_out: (vm.isPopoutVideo !== undefined) ? 'true' : 'false',
	                    vdo_id: vm.playingVideoObject.videoId,
	                    vdo_vw_dt: new Date()
	                }

	                videoViews.submitVideoStats({
	                    topic: "videostats",
	                    stats: JSON.stringify(obj)
	                });
	            }
	        });
	    }

	    function setPlayerVolume(volume) {
	        var ele = $('.ds-sound-progress-wrapper.' + vm.videoId);
	        var sound_progress = $(ele).find('.ds-sound-real-progress');
	        $(sound_progress).width(volume + "%");
	        vm.playerVolume = authToken.playerVolume(volume);
	        vm.videoPlayer.setVolume(volume);
	        if (volume === 0) {
	            setPlayerMute(true);
	        } else {
	            setPlayerMute(false);
	        }
	    }

	    function setPlayerMute(isMute) {
	        if (isMute) {
	            var ele = $('.ds-sound-progress-wrapper.' + vm.videoId);
	            var sound_progress = $(ele).find('.ds-sound-real-progress');
	            $(sound_progress).width("0%");


	            vm.playerVolume = authToken.playerVolume(0);
	            vm.videoPlayer.setVolume(0);
	        }
	        vm.isPlayerMute = authToken.playerMute(isMute);
	        vm.videoPlayer.setMute(vm.isPlayerMute)
	    }

	    function setTheaterMode(isInTheaterMode) {
	        vm.isInTheaterMode = isInTheaterMode;
	        authToken.playerTheaterMode(isInTheaterMode);
	        vm.theaterModeCallback({
	            isVideoInTheaterMode: isInTheaterMode
	        });
	        if (vm.isFullScreen) {
	            fullScreen.exitFullScreen();
	            vm.isFullScreen = false;
	        }
	    }

	    vm.downloadVTTFile = function(url){
	        $.ajax({
	            url : url,
	            method : 'GET',
	            success : function(data){
	                var parser = new WebVTTParser();
	                vm.parsedVTT = parser.parse(data);
	                var pos = url.lastIndexOf("/");
	                vm.thumnnailBasePath = url.substring(0,pos);
	            },
	            error : function(err){
	                $log.error(err);
	            }
	        })
	    }
	}

	VideoControls.$inject = ['$log', '$interval', '$timeout', 'videoPlayer', 'authToken'];

	function VideoControls($log, $interval, $timeout, videoPlayer, authToken) {
	    return {
	        restrict: 'AE',
	        // replace : true,
	        scope: {},
	        bindToController: {
	            playerId: "@",
	            playerUrl: "@",
	            autoStart: "@",
	            isVideoPage: '@',
	            isPopoutVideo: '@',
	            isRestricted: '=?',
	            courseId: '@?',
	            theaterModeCallback: '&?',
	            playingVideoObject: '=?',
	            componentDelegate: '=?',
	            onMetadata: "&?",
	            onVideoSeek: "&?",
	            onVideoLoadError: "&?",
	            onVideoTimeUpdate: "&?",
	            isOtherThingInFullscreen :  '=?'
	        },
	        template: function(ele, attrs) {
	            if (!attrs.player) {
	                return __webpack_require__(199);
	            } else if (attrs.player === 'minimal') {
	                return __webpack_require__(215);
	            }
	        },
	        controller: VideoControlsCtrl,
	        controllerAs: 'vm',
	        link: function postLink(scope, element, attrs, vm) {
	            vm.componentDelegate = vm;

	            $timeout(function() {


	                vm.videoPlayer = jwplayer(vm.videoId);
	                if (vm.playerId && vm.playerUrl && vm.videoPlayer.setup) {
	                    if (!vm.isPlayerReady) {
	                        vm.videoPlayer.setup({
	                            playlist: [{
	                                sources: [{
	                                    file: vm.playerUrl
	                                }]
	                            }],
	                            width: '100%',
	                            height: '100%',
	                            primary: 'html5',
	                            controls: false,
	                            autostart: (vm.autoStart == 'true')
	                        });
	                        vm.isDirectURL = true;
	                        vm.setupVideoPlayer();
	                    }
	                }
	            }, 100);
	            vm.setupVideoPlayer = setupVideoPlayer;
	            vm.isFlash = false;
	            vm.isPlaying = false;


	            var controlbar = $('.ds-controlbar');
	            var video_tag = $('#video_jw');

	            var speedSlider = $('.speed-slider')[0];
	            vm.speedSlider = speedSlider;
	            var max_speed = 2;
	            var min_speed = 0.5;
	            var captionObserver = null;

	            // 

	            function setupVideoPlayer() {
	                registerEvents();
	                bindKeyEvents();
	                setupProgressBarMouseMove();
	            }

	            function registerEvents() {

	                //On Time Update
	                vm.videoPlayer.on('time', function() {
	                    if (vm.onVideoTimeUpdate) {
	                        var time = vm.videoPlayer.getPosition();
	                        vm.onVideoTimeUpdate({
	                            time: changeTimeFormat(time),
	                            percent : (time / vm.videoPlayer.getDuration()) * 100
	                        })
	                    }
	                    vm.playerError = false;
	                    vm.isVideoComplete = false;
	                    vm.isVideoStarted = true;
	                    vm.isPlaying = true;
	                    vm.isLoading = false;
	                    //Fetch current time
	                    var time = vm.videoPlayer.getPosition();
	                    scope.$apply(function() {
	                        vm.video_current = changeTimeFormat(time);
	                        vm.currentVideoTimeInSec = time;
	                    })

	                    //Progress bar width
	                    var progressbar_width = ((vm.videoPlayer.getPosition() / vm.videoPlayer.getDuration()) * 100) + '%';
	                    //Set progress bar width
	                    $('.ds-progress-bar.' + vm.videoId).css('width', progressbar_width);
	                    scope.$emit("videoTimeUpdate");
	                });

	                vm.videoPlayer.on("playlistComplete", function() {
	                    stopObservingCaptions();
	                    scope.$apply(function() {
	                        vm.isPlaying = false;
	                        vm.videoPlayer.play(false);
	                    });
	                });

	                vm.videoPlayer.on("complete", function() {
	                    $log.debug("video complete");
	                    vm.isVideoComplete = true;
	                    vm.isVideoStarted = false;
	                    vm.nextVideo();
	                });

	                // vm.videoPlayer.on('all', function(e) {
	                //     if (e !== 'bufferChange' && e !== 'time') {
	                //         $log.debug("all", e);
	                //     }
	                // });

	                vm.videoPlayer.on('error', function(e) {
	                    vm.playerError = true;
	                    vm.playerErrorMsg = e.message;
	                    if (vm.onVideoLoadError) {
	                        e.videoQuality = vm.videoQuality;
	                        vm.onVideoLoadError({
	                            err: e
	                        });
	                    }

	                    $log.error(e);
	                    scope.$apply();
	                });

	                vm.videoPlayer.on('setupError', function(e) {
	                    vm.playerError = true;
	                    vm.playerErrorMsg = e.message;
	                    if (vm.onVideoLoadError) {
	                        e.videoQuality = vm.videoQuality;
	                        vm.onVideoLoadError({
	                            err: e
	                        });
	                    }
	                    $log.error(e);
	                    scope.$apply();
	                });





	                vm.videoPlayer.on('firstFrame', function(e) {
	                    $timeout(function() {
	                        vm.playerSpeed = authToken.playerSpeed();
	                        vm.changeSpeed(vm.playerSpeed || 1);
	                    });
	                    $('#' + vm.videoId).height('100%');
	                })

	                vm.videoPlayer.on('playlist', function(e) {
	                    // vm.isLoading = true;
	                })

	                vm.videoPlayer.on('seek', function(e) {
	                    // vm.isLoading = true;
	                })
	                vm.videoPlayer.on('seeked', function(e) {
	                    vm.isLoading = false;
	                });
	                vm.videoPlayer.on('providerFirstFrame', function(e) {
	                    vm.isLoading = false;
	                });


	                vm.videoPlayer.on('play', function() {
	                    vm.isLoading = false;
	                    startObservingCaptions();
	                    $('.jw-captions').css('display', 'none');
	                })
	                vm.videoPlayer.on('pause', function() {
	                    vm.shouldShowLoader = false;
	                    stopObservingCaptions();
	                })



	                vm.videoPlayer.on('ready', function() {
	                    vm.isPlayerReady = true;
	                    vm.shouldShowLoader = true;
	                    vm.isLoading = false;

	                    videoPlayer.setVideoPlayer(vm.videoPlayer);
	                    //Duration of video
	                    //Set Current time of video
	                    vm.video_current = "00:00";
	                    if (vm.autoStart === 'false') {
	                        // vm.isPlaying = false;
	                    } else {
	                        vm.videoPlayer.play(true);
	                        vm.isVideoStarted = true;
	                        vm.isPlaying = true;
	                    }
	                });
	                //Only Once when all meta data loaded
	                vm.videoPlayer.on('meta', function(e) {
	                    if (vm.onMetadata) {
	                        vm.onMetadata({
	                            metadata: e
	                        });
	                    }

	                    vm.isLoading = false;

	                    if (vm.autoStart === 'false') {
	                        // vm.isPlaying = false;
	                    } else {
	                        vm.videoPlayer.play(true);
	                        vm.isVideoStarted = true;
	                        vm.isPlaying = true;
	                    }

	                    if (e.duration) {
	                        var time = e.duration;
	                        scope.$apply(function() {
	                            vm.video_duration = changeTimeFormat(time);
	                        })
	                    }
	                    if (e.metadata.duration) {
	                        var time = e.metadata.duration;
	                        scope.$apply(function() {
	                            vm.video_duration = changeTimeFormat(time);
	                        })
	                    }
	                });
	                vm.videoPlayer.on('playlistItem', function(e) {
	                    //Check Whether video is HTML5 or Flash
	                    var index = e.index;
	                    var type = e.item.sources[index].type;
	                    var isFlash = false;
	                    if (type === "rtmp") {
	                        isFlash = true;
	                    }
	                    vm.isFlash = isFlash;
	                });



	                vm.videoPlayer.on('captionsList', function(e) {
	                    vm.videoPlayer.setCurrentCaptions(1);
	                })

	                vm.videoPlayer.on('bufferChange', function() {
	                    var bufferbar_width = (vm.videoPlayer.getBuffer()) + '%';
	                    $('.ds-buffered-bar.' + vm.videoId).css('width', bufferbar_width);
	                })

	                //Volume Change event
	                vm.videoPlayer.on('volume', function() {
	                    //Set Volume Control Height
	                    if (vm.videoPlayer.getVolume() == 0) {
	                        vm.isMute = true;
	                    } else {
	                        vm.isMute = false;
	                    }
	                });


	                //Click Listener For Progress Bar
	                $('.ds-progress-bar-wrapper.' + vm.videoId).on('click', function(e) {
	                    //Percent where user clicked on progressbar
	                    var percent = (e.offsetX / $(this).width()) * 100;
	                    //Get desire time from where user clicked
	                    var desireTime = vm.videoPlayer.getDuration() * percent / 100;

	                    vm.cureentVideoProgressBarStatsTime = vm.video_current;
	                    vm.seekVideoProgressBarStatsTime = changeTimeFormat(desireTime);

	                    if (vm.onVideoSeek) {
	                        vm.onVideoSeek({
	                            currentTime: vm.video_current,
	                            seekTime: vm.seekVideoProgressBarStatsTime
	                        })
	                    }


	                    //Set current time to desire time
	                    vm.videoPlayer.seek(desireTime);

	                    vm.isPlaying = true;
	                });

	                //Click Listener For Volume bar
	                $('.ds-sound-progress-wrapper.' + vm.videoId).on('click', function(e) {
	                    //Percent where user clicked on progressbar
	                    var percent = Math.floor((e.offsetX / $(this).width()) * 100);

	                    //set volume
	                    // vm.videoPlayer.setVolume(percent);

	                    // //set width of sound progress bar
	                    // var real_progress_bar = $(this).find('.ds-sound-real-progress');
	                    // $(real_progress_bar).width(percent + "%");

	                    vm.setPlayerVolume(percent);

	                });

	                $(document).on('click', function(e) {
	                    $('.ds-control').removeClass('open');
	                    $('.ds-control').find('.overlay').hide();
	                });
	            }

	            function startObservingCaptions() {
	                captionObserver = $interval(function() {
	                    var window_active = $('.jw-captions-window-active');
	                    if (window_active.length > 0) {
	                        var cap = $('.jw-captions-text').html();
	                        cap = cap.split('<br>').join(' ');
	                    } else {
	                        var cap = "";
	                    }
	                    vm.subtitle = cap;
	                }, 100);
	            }

	            function stopObservingCaptions() {
	                $interval.cancel(captionObserver);
	            }



	            function bindKeyEvents() {

	                $('video-controls-jw').bind('keypress', function(e) {
	                    // 99/67 - C/c
	                    var codes = {
	                        CAPITAL_C: 99,
	                        SMALL_C: 67,

	                        CAPITAL_F: 70,
	                        SMALL_F: 102,

	                        SMALL_P: 112,
	                        CAPITAL_P: 80
	                    }
	                    scope.$apply(function() {
	                        if (e.keyCode == codes.CAPITAL_C || e.keyCode == codes.SMALL_C) {
	                            vm.subtitleToggle();
	                        } else if (e.keyCode == codes.CAPITAL_F || e.keyCode == codes.SMALL_F) {
	                            vm.fullscreen();
	                        } else if ((e.keyCode == codes.SMALL_P) || (e.keyCode == codes.CAPITAL_P)) {
	                            vm.playToggle();
	                        }
	                    });

	                })
	                $('video-controls-jw').bind('keydown', function(e) {
	                    e.stopPropagation();
	                    //32 - Space
	                    //37 - Left Key
	                    //38 - Up Key
	                    //39 - Right Key
	                    //40 - Down Key
	                    var offset_time = 8; //8 seconds Forward and Backward
	                    var codes = {
	                        SPACE: 32,
	                        LEFT: 37,
	                        UP: 38,
	                        RIGHT: 39,
	                        DOWN: 40,
	                        GREATER: 190,
	                        LESSTHAN: 188
	                    }


	                    if (e.keyCode == codes.SPACE) {
	                        vm.playToggle();
	                    } else if (!e.ctrlKey && e.keyCode == codes.UP) {

	                        //Volume UP

	                        var currentVolume = vm.videoPlayer.getVolume();
	                        if (currentVolume < 100) {
	                            var nextVolume = currentVolume + 10;
	                            vm.volumeSlider.noUiSlider.set(nextVolume);
	                            vm.videoPlayer.setVolume(nextVolume);
	                        }
	                    } else if (!e.ctrlKey && e.keyCode == codes.DOWN) {

	                        //Volume Down

	                        var currentVolume = vm.videoPlayer.getVolume();
	                        if (currentVolume > 0) {
	                            var nextVolume = currentVolume - 10;
	                            vm.volumeSlider.noUiSlider.set(nextVolume);
	                            vm.videoPlayer.setVolume(nextVolume);
	                        }
	                    } else if (!e.ctrlKey && e.keyCode == codes.RIGHT) {

	                        //FAST FORWARD offset_time
	                        vm.fastForward();

	                    } else if (!e.ctrlKey && e.keyCode == codes.LEFT) {

	                        //Backward offest_time
	                        vm.rewind();

	                    } else if (e.ctrlKey && e.keyCode == codes.GREATER) {

	                        //Speed UP

	                        var currentSpeed = vm.getPlayBackRate();
	                        if (currentSpeed < max_speed) {
	                            var nextSpeed = currentSpeed + 0.1;
	                            vm.changeSpeed(nextSpeed);
	                        }
	                    } else if (e.ctrlKey && e.keyCode == codes.LESSTHAN) {

	                        //Speed Down

	                        var currentSpeed = vm.getPlayBackRate();
	                        if (currentSpeed > min_speed) {
	                            var nextSpeed = currentSpeed - 0.1;
	                            vm.changeSpeed(nextSpeed);
	                        }
	                    }


	                })
	            }

	            function findImageForTime(time){
	                if(!vm.parsedVTT){
	                    return;
	                }
	                var obj = (vm.parsedVTT.cues.filter(function(v,i){
	                    if(v.startTime >= time && time <= v.endTime){
	                        return true;
	                    }
	                    return false;
	                }) || [])[0];
	                if(obj){
	                    var pos = obj.text.indexOf("#");
	                    var img = obj.text.substring(0,pos);
	                    var key_val = obj.text.substring(pos+1).split("=")[1].split(",");
	                    var x = key_val[0]
	                    var y = key_val[1]
	                    return{
	                        img : vm.thumnnailBasePath + "" + img,
	                        x : -x,
	                        y : -y
	                    }
	                }
	                
	            }

	            function setupProgressBarMouseMove() {
	                setTimeout(function() {
	                    $(".ds-progress-bar-wrapper." + vm.videoId).on('mousemove', function(e) {
	                        //get percentage , where is the current mouse position
	                        var percent = (e.offsetX / $(this).width()) * 100;
	                        //get video time from percentage value
	                        var time = vm.videoPlayer.getDuration() * percent / 100;

	                        vm.mouseHoverTime = changeTimeFormat(time);

	                        //mouse hover element
	                        var ele = $(this).find(".mouse-hover-time");
	                        //position element
	                        $(ele).css('left', e.offsetX - 100);
	                        //display mouse hover element
	                        $(ele).css('display', 'block');
	                        var i = findImageForTime(time || 0)
	                        if(i){
	                            $(ele).css("font-size","20px").css("background-position",i.x + "px " + i.y + "px").css("background-image","url('" + i.img + "')");    
	                        }
	                        scope.$apply();
	                    });
	                    $(".ds-progress-bar-wrapper." + vm.videoId).on('mouseout', function(e) {
	                        vm.mouseHoverTime = "";

	                        //mouse hover element
	                        var ele = $(this).find(".mouse-hover-time");
	                        //position element
	                        $(ele).css('left', "0px");
	                        //display mouse hover element
	                        $(ele).css('display', 'none');
	                        scope.$apply();
	                    });
	                })

	            }

	            vm.changeTimeFormat = changeTimeFormat;

	            function changeTimeFormat(sec) {
	                var time = sec || 0;
	                var hours = parseInt(time / 3600) % 24;
	                var minutes = parseInt(time / 60) % 60;
	                var seconds = parseInt(time % 60);

	                hours = (hours < 10 ? "0" + hours : hours)
	                minutes = (minutes < 10 ? "0" + minutes : minutes)
	                seconds = (seconds < 10 ? "0" + seconds : seconds)
	                var ret_time;
	                if (hours == "00") {
	                    ret_time = minutes + ":" + seconds;
	                } else {
	                    ret_time = hours + ":" + minutes + ":" + seconds;
	                }

	                return ret_time;
	            }

	        }
	    }

	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 189:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {!function(e,t){ true?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports.jwplayer=t():e.jwplayer=t()}(this,function(){return function(e){function t(n){if(i[n])return i[n].exports;var r=i[n]={exports:{},id:n,loaded:!1};return e[n].call(r.exports,r,r.exports,t),r.loaded=!0,r.exports}var n=window.webpackJsonpjwplayer;window.webpackJsonpjwplayer=function(i,o){for(var a,s,l=0,c=[];l<i.length;l++)s=i[l],r[s]&&c.push.apply(c,r[s]),r[s]=0;for(a in o)e[a]=o[a];for(n&&n(i,o);c.length;)c.shift().call(null,t)};var i={},r={2:0};return t.e=function(e,n){if(0===r[e])return n.call(null,t);if(void 0!==r[e])r[e].push(n);else{r[e]=[n];var i=document.getElementsByTagName("head")[0],o=document.createElement("script");o.type="text/javascript",o.charset="utf-8",o.async=!0,o.src=t.p+""+({0:"provider.youtube",1:"provider.caterpillar",3:"provider.shaka",4:"provider.cast",5:"polyfills.promise",6:"polyfills.base64"}[e]||e)+".js",i.appendChild(o)}},t.m=e,t.c=i,t.p="",t(0)}([function(e,t,n){e.exports=n(319)},function(e,t,n){var i,r;i=[],r=function(){var e={},t=Array.prototype,n=Object.prototype,i=Function.prototype,r=t.slice,o=t.concat,a=n.toString,s=n.hasOwnProperty,l=t.map,c=t.reduce,u=t.forEach,d=t.filter,p=t.every,h=t.some,f=t.indexOf,g=Array.isArray,m=Object.keys,v=i.bind,w=function(e){return e instanceof w?e:this instanceof w?void 0:new w(e)},y=w.each=w.forEach=function(t,n,i){if(null==t)return t;if(u&&t.forEach===u)t.forEach(n,i);else if(t.length===+t.length){for(var r=0,o=t.length;o>r;r++)if(n.call(i,t[r],r,t)===e)return}else for(var a=w.keys(t),r=0,o=a.length;o>r;r++)if(n.call(i,t[a[r]],a[r],t)===e)return;return t};w.map=w.collect=function(e,t,n){var i=[];return null==e?i:l&&e.map===l?e.map(t,n):(y(e,function(e,r,o){i.push(t.call(n,e,r,o))}),i)};var j="Reduce of empty array with no initial value";w.reduce=w.foldl=w.inject=function(e,t,n,i){var r=arguments.length>2;if(null==e&&(e=[]),c&&e.reduce===c)return i&&(t=w.bind(t,i)),r?e.reduce(t,n):e.reduce(t);if(y(e,function(e,o,a){r?n=t.call(i,n,e,o,a):(n=e,r=!0)}),!r)throw new TypeError(j);return n},w.find=w.detect=function(e,t,n){var i;return b(e,function(e,r,o){return t.call(n,e,r,o)?(i=e,!0):void 0}),i},w.filter=w.select=function(e,t,n){var i=[];return null==e?i:d&&e.filter===d?e.filter(t,n):(y(e,function(e,r,o){t.call(n,e,r,o)&&i.push(e)}),i)},w.reject=function(e,t,n){return w.filter(e,function(e,i,r){return!t.call(n,e,i,r)},n)},w.compact=function(e){return w.filter(e,w.identity)},w.every=w.all=function(t,n,i){n||(n=w.identity);var r=!0;return null==t?r:p&&t.every===p?t.every(n,i):(y(t,function(t,o,a){return(r=r&&n.call(i,t,o,a))?void 0:e}),!!r)};var b=w.some=w.any=function(t,n,i){n||(n=w.identity);var r=!1;return null==t?r:h&&t.some===h?t.some(n,i):(y(t,function(t,o,a){return r||(r=n.call(i,t,o,a))?e:void 0}),!!r)};w.size=function(e){return null==e?0:e.length===+e.length?e.length:w.keys(e).length},w.last=function(e,t,n){return null!=e?null==t||n?e[e.length-1]:r.call(e,Math.max(e.length-t,0)):void 0},w.after=function(e,t){return function(){return--e<1?t.apply(this,arguments):void 0}},w.before=function(e,t){var n;return function(){return--e>0&&(n=t.apply(this,arguments)),1>=e&&(t=null),n}};var E=function(e){return null==e?w.identity:w.isFunction(e)?e:w.property(e)},k=function(e){return function(t,n,i){var r={};return n=E(n),y(t,function(o,a){var s=n.call(i,o,a,t);e(r,s,o)}),r}};w.groupBy=k(function(e,t,n){w.has(e,t)?e[t].push(n):e[t]=[n]}),w.indexBy=k(function(e,t,n){e[t]=n}),w.sortedIndex=function(e,t,n,i){n=E(n);for(var r=n.call(i,t),o=0,a=e.length;a>o;){var s=o+a>>>1;n.call(i,e[s])<r?o=s+1:a=s}return o};var b=w.some=w.any=function(t,n,i){n||(n=w.identity);var r=!1;return null==t?r:h&&t.some===h?t.some(n,i):(y(t,function(t,o,a){return r||(r=n.call(i,t,o,a))?e:void 0}),!!r)};w.contains=w.include=function(e,t){return null==e?!1:(e.length!==+e.length&&(e=w.values(e)),w.indexOf(e,t)>=0)},w.pluck=function(e,t){return w.map(e,w.property(t))},w.where=function(e,t){return w.filter(e,w.matches(t))},w.findWhere=function(e,t){return w.find(e,w.matches(t))},w.max=function(e,t,n){if(!t&&w.isArray(e)&&e[0]===+e[0]&&e.length<65535)return Math.max.apply(Math,e);var i=-(1/0),r=-(1/0);return y(e,function(e,o,a){var s=t?t.call(n,e,o,a):e;s>r&&(i=e,r=s)}),i},w.difference=function(e){var n=o.apply(t,r.call(arguments,1));return w.filter(e,function(e){return!w.contains(n,e)})},w.without=function(e){return w.difference(e,r.call(arguments,1))},w.indexOf=function(e,t,n){if(null==e)return-1;var i=0,r=e.length;if(n){if("number"!=typeof n)return i=w.sortedIndex(e,t),e[i]===t?i:-1;i=0>n?Math.max(0,r+n):n}if(f&&e.indexOf===f)return e.indexOf(t,n);for(;r>i;i++)if(e[i]===t)return i;return-1};var A=function(){};w.bind=function(e,t){var n,i;if(v&&e.bind===v)return v.apply(e,r.call(arguments,1));if(!w.isFunction(e))throw new TypeError;return n=r.call(arguments,2),i=function(){if(!(this instanceof i))return e.apply(t,n.concat(r.call(arguments)));A.prototype=e.prototype;var o=new A;A.prototype=null;var a=e.apply(o,n.concat(r.call(arguments)));return Object(a)===a?a:o}},w.partial=function(e){var t=r.call(arguments,1);return function(){for(var n=0,i=t.slice(),r=0,o=i.length;o>r;r++)i[r]===w&&(i[r]=arguments[n++]);for(;n<arguments.length;)i.push(arguments[n++]);return e.apply(this,i)}},w.once=w.partial(w.before,2),w.memoize=function(e,t){var n={};return t||(t=w.identity),function(){var i=t.apply(this,arguments);return w.has(n,i)?n[i]:n[i]=e.apply(this,arguments)}},w.delay=function(e,t){var n=r.call(arguments,2);return setTimeout(function(){return e.apply(null,n)},t)},w.defer=function(e){return w.delay.apply(w,[e,1].concat(r.call(arguments,1)))},w.throttle=function(e,t,n){var i,r,o,a=null,s=0;n||(n={});var l=function(){s=n.leading===!1?0:w.now(),a=null,o=e.apply(i,r),i=r=null};return function(){var c=w.now();s||n.leading!==!1||(s=c);var u=t-(c-s);return i=this,r=arguments,0>=u?(clearTimeout(a),a=null,s=c,o=e.apply(i,r),i=r=null):a||n.trailing===!1||(a=setTimeout(l,u)),o}},w.keys=function(e){if(!w.isObject(e))return[];if(m)return m(e);var t=[];for(var n in e)w.has(e,n)&&t.push(n);return t},w.invert=function(e){for(var t={},n=w.keys(e),i=0,r=n.length;r>i;i++)t[e[n[i]]]=n[i];return t},w.defaults=function(e){return y(r.call(arguments,1),function(t){if(t)for(var n in t)void 0===e[n]&&(e[n]=t[n])}),e},w.extend=function(e){return y(r.call(arguments,1),function(t){if(t)for(var n in t)e[n]=t[n]}),e},w.pick=function(e){var n={},i=o.apply(t,r.call(arguments,1));return y(i,function(t){t in e&&(n[t]=e[t])}),n},w.omit=function(e){var n={},i=o.apply(t,r.call(arguments,1));for(var a in e)w.contains(i,a)||(n[a]=e[a]);return n},w.clone=function(e){return w.isObject(e)?w.isArray(e)?e.slice():w.extend({},e):e},w.isArray=g||function(e){return"[object Array]"==a.call(e)},w.isObject=function(e){return e===Object(e)},y(["Arguments","Function","String","Number","Date","RegExp"],function(e){w["is"+e]=function(t){return a.call(t)=="[object "+e+"]"}}),w.isArguments(arguments)||(w.isArguments=function(e){return!(!e||!w.has(e,"callee"))}),w.isFunction=function(e){return"function"==typeof e},w.isFinite=function(e){return isFinite(e)&&!isNaN(parseFloat(e))},w.isNaN=function(e){return w.isNumber(e)&&e!=+e},w.isBoolean=function(e){return e===!0||e===!1||"[object Boolean]"==a.call(e)},w.isNull=function(e){return null===e},w.isUndefined=function(e){return void 0===e},w.has=function(e,t){return s.call(e,t)},w.identity=function(e){return e},w.constant=function(e){return function(){return e}},w.property=function(e){return function(t){return t[e]}},w.propertyOf=function(e){return null==e?function(){}:function(t){return e[t]}},w.matches=function(e){return function(t){if(t===e)return!0;for(var n in e)if(e[n]!==t[n])return!1;return!0}},w.now=Date.now||function(){return(new Date).getTime()},w.result=function(e,t){if(null!=e){var n=e[t];return w.isFunction(n)?n.call(e):n}};var L=0;return w.uniqueId=function(e){var t=++L+"";return e?e+t:t},w}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(14),n(1),n(127),n(128),n(45),n(86),n(58),n(284),n(87),n(130),n(132)],r=function(e,t,n,i,r,o,a,s,l,c,u){var d={};return d.log=function(){window.console&&("object"==typeof console.log?console.log(Array.prototype.slice.call(arguments,0)):console.log.apply(console,arguments))},d.between=function(e,t,n){return Math.max(Math.min(e,n),t)},d.foreach=function(e,t){var n,i;for(n in e)"function"===d.typeOf(e.hasOwnProperty)?e.hasOwnProperty(n)&&(i=e[n],t(n,i)):(i=e[n],t(n,i))},d.indexOf=t.indexOf,d.noop=function(){},d.seconds=e.seconds,d.prefix=e.prefix,d.suffix=e.suffix,t.extend(d,o,a,l,n,s,i,r,c,u),d}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t=[],n=t.slice,i={on:function(e,t,n){if(!o(this,"on",e,[t,n])||!t)return this;this._events||(this._events={});var i=this._events[e]||(this._events[e]=[]);return i.push({callback:t,context:n}),this},once:function(t,n,i){if(!o(this,"once",t,[n,i])||!n)return this;var r=this,a=e.once(function(){r.off(t,a),n.apply(this,arguments)});return a._callback=n,this.on(t,a,i)},off:function(t,n,i){var r,a,s,l,c,u,d,p;if(!this._events||!o(this,"off",t,[n,i]))return this;if(!t&&!n&&!i)return this._events=void 0,this;for(l=t?[t]:e.keys(this._events),c=0,u=l.length;u>c;c++)if(t=l[c],s=this._events[t]){if(this._events[t]=r=[],n||i)for(d=0,p=s.length;p>d;d++)a=s[d],(n&&n!==a.callback&&n!==a.callback._callback||i&&i!==a.context)&&r.push(a);r.length||delete this._events[t]}return this},trigger:function(e){if(!this._events)return this;var t=n.call(arguments,1);if(!o(this,"trigger",e,t))return this;var i=this._events[e],r=this._events.all;return i&&a(i,t,this),r&&a(r,arguments,this),this},triggerSafe:function(e){if(!this._events)return this;var t=n.call(arguments,1);if(!o(this,"trigger",e,t))return this;var i=this._events[e],r=this._events.all;return i&&s(i,t,this),r&&s(r,arguments,this),this}},r=/\s+/,o=function(e,t,n,i){if(!n)return!0;if("object"==typeof n){for(var o in n)e[t].apply(e,[o,n[o]].concat(i));return!1}if(r.test(n)){for(var a=n.split(r),s=0,l=a.length;l>s;s++)e[t].apply(e,[a[s]].concat(i));return!1}return!0},a=function(e,t,n){var i,r=-1,o=e.length,a=t[0],s=t[1],l=t[2];switch(t.length){case 0:for(;++r<o;)(i=e[r]).callback.call(i.context||n);return;case 1:for(;++r<o;)(i=e[r]).callback.call(i.context||n,a);return;case 2:for(;++r<o;)(i=e[r]).callback.call(i.context||n,a,s);return;case 3:for(;++r<o;)(i=e[r]).callback.call(i.context||n,a,s,l);return;default:for(;++r<o;)(i=e[r]).callback.apply(i.context||n,t);return}},s=function(e,t,n){for(var i,r=-1,o=e.length;++r<o;)try{(i=e[r]).callback.apply(i.context||n,t)}catch(a){}};return i}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e={DRAG:"drag",DRAG_START:"dragStart",DRAG_END:"dragEnd",CLICK:"click",DOUBLE_CLICK:"doubleClick",TAP:"tap",DOUBLE_TAP:"doubleTap",OVER:"over",MOVE:"move",OUT:"out"},t={COMPLETE:"complete",ERROR:"error",JWPLAYER_AD_CLICK:"adClick",JWPLAYER_AD_COMPANIONS:"adCompanions",JWPLAYER_AD_COMPLETE:"adComplete",JWPLAYER_AD_ERROR:"adError",JWPLAYER_AD_IMPRESSION:"adImpression",JWPLAYER_AD_META:"adMeta",JWPLAYER_AD_PAUSE:"adPause",JWPLAYER_AD_PLAY:"adPlay",JWPLAYER_AD_SKIPPED:"adSkipped",JWPLAYER_AD_TIME:"adTime",JWPLAYER_CAST_AD_CHANGED:"castAdChanged",JWPLAYER_MEDIA_COMPLETE:"complete",JWPLAYER_READY:"ready",JWPLAYER_MEDIA_SEEK:"seek",JWPLAYER_MEDIA_BEFOREPLAY:"beforePlay",JWPLAYER_MEDIA_BEFORECOMPLETE:"beforeComplete",JWPLAYER_MEDIA_BUFFER_FULL:"bufferFull",JWPLAYER_DISPLAY_CLICK:"displayClick",JWPLAYER_PLAYLIST_COMPLETE:"playlistComplete",JWPLAYER_CAST_SESSION:"cast",JWPLAYER_MEDIA_ERROR:"mediaError",JWPLAYER_MEDIA_FIRST_FRAME:"firstFrame",JWPLAYER_MEDIA_PLAY_ATTEMPT:"playAttempt",JWPLAYER_MEDIA_LOADED:"loaded",JWPLAYER_MEDIA_SEEKED:"seeked",JWPLAYER_SETUP_ERROR:"setupError",JWPLAYER_ERROR:"error",JWPLAYER_PLAYER_STATE:"state",JWPLAYER_CAST_AVAILABLE:"castAvailable",JWPLAYER_MEDIA_BUFFER:"bufferChange",JWPLAYER_MEDIA_TIME:"time",JWPLAYER_MEDIA_TYPE:"mediaType",JWPLAYER_MEDIA_VOLUME:"volume",JWPLAYER_MEDIA_MUTE:"mute",JWPLAYER_MEDIA_META:"meta",JWPLAYER_MEDIA_LEVELS:"levels",JWPLAYER_MEDIA_LEVEL_CHANGED:"levelsChanged",JWPLAYER_CONTROLS:"controls",JWPLAYER_FULLSCREEN:"fullscreen",JWPLAYER_RESIZE:"resize",JWPLAYER_PLAYLIST_ITEM:"playlistItem",JWPLAYER_PLAYLIST_LOADED:"playlist",JWPLAYER_AUDIO_TRACKS:"audioTracks",JWPLAYER_AUDIO_TRACK_CHANGED:"audioTrackChanged",JWPLAYER_LOGO_CLICK:"logoClick",JWPLAYER_CAPTIONS_LIST:"captionsList",JWPLAYER_CAPTIONS_CHANGED:"captionsChanged",JWPLAYER_PROVIDER_CHANGED:"providerChanged",JWPLAYER_PROVIDER_FIRST_FRAME:"providerFirstFrame",JWPLAYER_USER_ACTION:"userAction",JWPLAYER_PROVIDER_CLICK:"providerClick",JWPLAYER_VIEW_TAB_FOCUS:"tabFocus",JWPLAYER_CONTROLBAR_DRAGGING:"scrubbing",JWPLAYER_INSTREAM_CLICK:"instreamClick"};return t.touchEvents=e,t}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i,r;i=[],r=function(){return{BUFFERING:"buffering",IDLE:"idle",COMPLETE:"complete",PAUSED:"paused",PLAYING:"playing",ERROR:"error",LOADING:"loading",STALLED:"stalled"}}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,function(e,t,n){var i,r;i=[n(3),n(4),n(1),n(2)],r=function(e,t,n,i){function r(e,t){return/touch/.test(e.type)?(e.originalEvent||e).changedTouches[0]["page"+t]:e["page"+t]}function o(e){var t=e||window.event;return e instanceof MouseEvent?"which"in t?3===t.which:"button"in t?2===t.button:!1:!1}function a(e,t,n){var i;return i=t instanceof MouseEvent||!t.touches&&!t.changedTouches?t:t.touches&&t.touches.length?t.touches[0]:t.changedTouches[0],{type:e,target:t.target,currentTarget:n,pageX:i.pageX,pageY:i.pageY}}function s(e){(e instanceof MouseEvent||e instanceof window.TouchEvent)&&(e.preventManipulation&&e.preventManipulation(),e.cancelable&&e.preventDefault&&e.preventDefault())}var l=!n.isUndefined(window.PointerEvent),c=!l&&i.isMobile(),u=!l&&!c,d=i.isFF()&&i.isOSX(),p=function(e,i){function c(e){(u||l&&"touch"!==e.pointerType)&&v(t.touchEvents.OVER,e)}function p(e){(u||l&&"touch"!==e.pointerType)&&v(t.touchEvents.MOVE,e)}function h(n){(u||l&&"touch"!==n.pointerType&&!e.contains(document.elementFromPoint(n.x,n.y)))&&v(t.touchEvents.OUT,n)}function f(t){w=t.target,E=r(t,"X"),k=r(t,"Y"),o(t)||(l?t.isPrimary&&(i.preventScrolling&&(y=t.pointerId,e.setPointerCapture(y)),e.addEventListener("pointermove",g),e.addEventListener("pointercancel",m),e.addEventListener("pointerup",m)):u&&(document.addEventListener("mousemove",g),d&&"object"===t.target.nodeName.toLowerCase()?e.addEventListener("click",m):document.addEventListener("mouseup",m)),w.addEventListener("touchmove",g),w.addEventListener("touchcancel",m),w.addEventListener("touchend",m))}function g(e){var n=t.touchEvents,o=6;if(b)v(n.DRAG,e);else{var a=r(e,"X"),l=r(e,"Y"),c=a-E,u=l-k;c*c+u*u>o*o&&(v(n.DRAG_START,e),b=!0,v(n.DRAG,e))}i.preventScrolling&&s(e)}function m(n){var r=t.touchEvents;l?(i.preventScrolling&&e.releasePointerCapture(y),e.removeEventListener("pointermove",g),e.removeEventListener("pointercancel",m),e.removeEventListener("pointerup",m)):u&&(document.removeEventListener("mousemove",g),document.removeEventListener("mouseup",m)),w.removeEventListener("touchmove",g),w.removeEventListener("touchcancel",m),w.removeEventListener("touchend",m),b?v(r.DRAG_END,n):i.directSelect&&n.target!==e||-1!==n.type.indexOf("cancel")||(l&&n instanceof window.PointerEvent?"touch"===n.pointerType?v(r.TAP,n):v(r.CLICK,n):u?v(r.CLICK,n):(v(r.TAP,n),s(n))),w=null,b=!1}function v(e,r){var o;if(i.enableDoubleTap&&(e===t.touchEvents.CLICK||e===t.touchEvents.TAP))if(n.now()-A<L){var s=e===t.touchEvents.CLICK?t.touchEvents.DOUBLE_CLICK:t.touchEvents.DOUBLE_TAP;o=a(s,r,j),_.trigger(s,o),A=0}else A=n.now();o=a(e,r,j),_.trigger(e,o)}var w,y,j=e,b=!1,E=0,k=0,A=0,L=300;i=i||{},l?(e.addEventListener("pointerdown",f),i.useHover&&(e.addEventListener("pointerover",c),e.addEventListener("pointerout",h)),i.useMove&&e.addEventListener("pointermove",p)):u&&(e.addEventListener("mousedown",f),i.useHover&&(e.addEventListener("mouseover",c),e.addEventListener("mouseout",h)),i.useMove&&e.addEventListener("mousemove",p)),e.addEventListener("touchstart",f);var _=this;return this.triggerEvent=v,this.destroy=function(){e.removeEventListener("touchstart",f),e.removeEventListener("mousedown",f),w&&(w.removeEventListener("touchmove",g),w.removeEventListener("touchcancel",m),w.removeEventListener("touchend",m)),l&&(i.preventScrolling&&e.releasePointerCapture(y),e.removeEventListener("pointerover",c),e.removeEventListener("pointerdown",f),e.removeEventListener("pointermove",g),e.removeEventListener("pointermove",p),e.removeEventListener("pointercancel",m),e.removeEventListener("pointerout",h),e.removeEventListener("pointerup",m)),e.removeEventListener("click",m),e.removeEventListener("mouseover",c),e.removeEventListener("mousemove",p),e.removeEventListener("mouseout",h),document.removeEventListener("mousemove",g),document.removeEventListener("mouseup",m)},this};return n.extend(p.prototype,e),p}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i,r;i=[n(1)],r=function(e){function t(e){return/[\(,]format=m3u8-/i.test(e)?"m3u8":!1}var n=function(e){return e.replace(/^\s+|\s+$/g,"")},i=function(e,t,n){for(e=""+e,n=n||"0";e.length<t;)e=n+e;return e},r=function(e,t){for(var n=0;n<e.attributes.length;n++)if(e.attributes[n].name&&e.attributes[n].name.toLowerCase()===t.toLowerCase())return e.attributes[n].value.toString();return""},o=function(e){if(!e||"rtmp"===e.substr(0,4))return"";var n=t(e);return n?n:(e=e.split("?")[0].split("#")[0],e.lastIndexOf(".")>-1?e.substr(e.lastIndexOf(".")+1,e.length).toLowerCase():void 0)},a=function(e){var t=parseInt(e/3600),n=parseInt(e/60)%60,r=e%60;return i(t,2)+":"+i(n,2)+":"+i(r.toFixed(3),6)},s=function(t){if(e.isNumber(t))return t;t=t.replace(",",".");var n=t.split(":"),i=0;return"s"===t.slice(-1)?i=parseFloat(t):"m"===t.slice(-1)?i=60*parseFloat(t):"h"===t.slice(-1)?i=3600*parseFloat(t):n.length>1?(i=parseFloat(n[n.length-1]),i+=60*parseFloat(n[n.length-2]),3===n.length&&(i+=3600*parseFloat(n[n.length-3]))):i=parseFloat(t),i},l=function(t,n){return e.map(t,function(e){return n+e})},c=function(t,n){return e.map(t,function(e){return e+n})};return{trim:n,pad:i,xmlAttribute:r,extension:o,hms:a,seconds:s,suffix:c,prefix:l}}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){e.exports=n(245)["default"]},,,,function(e,t){"use strict";function n(e){return u[e]}function i(e){for(var t=1;t<arguments.length;t++)for(var n in arguments[t])Object.prototype.hasOwnProperty.call(arguments[t],n)&&(e[n]=arguments[t][n]);return e}function r(e,t){for(var n=0,i=e.length;i>n;n++)if(e[n]===t)return n;return-1}function o(e){if("string"!=typeof e){if(e&&e.toHTML)return e.toHTML();if(null==e)return"";if(!e)return e+"";e=""+e}return p.test(e)?e.replace(d,n):e}function a(e){return e||0===e?!(!g(e)||0!==e.length):!0}function s(e){var t=i({},e);return t._parent=e,t}function l(e,t){return e.path=t,e}function c(e,t){return(e?e+".":"")+t}t.__esModule=!0,t.extend=i,t.indexOf=r,t.escapeExpression=o,t.isEmpty=a,t.createFrame=s,t.blockParams=l,t.appendContextPath=c;var u={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;","=":"&#x3D;"},d=/[&<>"'`=]/g,p=/[&<>"'`=]/,h=Object.prototype.toString;t.toString=h;var f=function(e){return"function"==typeof e};f(/x/)&&(t.isFunction=f=function(e){return"function"==typeof e&&"[object Function]"===h.call(e)}),t.isFunction=f;var g=Array.isArray||function(e){return e&&"object"==typeof e?"[object Array]"===h.call(e):!1};t.isArray=g},,,,,,,,,,,function(e,t,n){var i,r;i=[n(2),n(4),n(6),n(1)],r=function(e,t,n,i){var r=e.noop,o=i.constant(!1),a={supports:o,play:r,load:r,stop:r,volume:r,mute:r,seek:r,resize:r,remove:r,destroy:r,setVisibility:r,setFullscreen:o,getFullscreen:r,getContainer:r,setContainer:o,getName:r,getQualityLevels:r,getCurrentQuality:r,setCurrentQuality:r,getAudioTracks:r,getCurrentAudioTrack:r,setCurrentAudioTrack:r,checkComplete:r,setControls:r,attachMedia:r,detachMedia:r,setState:function(e){var i=this.state||n.IDLE;this.state=e,e!==i&&this.trigger(t.JWPLAYER_PLAYER_STATE,{newstate:e})},sendMediaType:function(e){var n=e[0].type,i="oga"===n||"aac"===n||"mp3"===n||"mpeg"===n||"vorbis"===n;this.trigger(t.JWPLAYER_MEDIA_TYPE,{mediaType:i?"audio":"video"})}};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(4),n(3),n(1)],r=function(e,t,n){var i={},r={NEW:0,LOADING:1,ERROR:2,COMPLETE:3},o=function(o,a){function s(t){u=r.ERROR,c.trigger(e.ERROR,t)}function l(t){u=r.COMPLETE,c.trigger(e.COMPLETE,t)}var c=n.extend(this,t),u=r.NEW;this.addEventListener=this.on,this.removeEventListener=this.off,this.makeStyleLink=function(e){var t=document.createElement("link");return t.type="text/css",t.rel="stylesheet",t.href=e,t},this.makeScriptTag=function(e){var t=document.createElement("script");return t.src=e,t},this.makeTag=a?this.makeStyleLink:this.makeScriptTag,this.load=function(){if(u===r.NEW){var t=i[o];if(t&&(u=t.getStatus(),2>u))return t.on(e.ERROR,s),void t.on(e.COMPLETE,l);var n=document.getElementsByTagName("head")[0]||document.documentElement,c=this.makeTag(o),d=!1;c.onload=c.onreadystatechange=function(e){d||this.readyState&&"loaded"!==this.readyState&&"complete"!==this.readyState||(d=!0,l(e),c.onload=c.onreadystatechange=null,n&&c.parentNode&&!a&&n.removeChild(c))},c.onerror=s,n.insertBefore(c,n.firstChild),u=r.LOADING,i[o]=this}},this.getStatus=function(){return u}};return o.loaderstatus=r,o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(283),n(1)],r=function(e,t){var i=e.prototype.reorderProviders;return e.prototype.reorderProviders=function(){if(i.apply(this),"flash"!==this.config.primary&&this.config.hlshtml===!0){var e=t.indexOf(this.providers,t.findWhere(this.providers,{name:"caterpillar"})),n=this.providers.splice(e,1)[0],r=t.indexOf(this.providers,t.findWhere(this.providers,{name:"flash"}));this.providers.splice(r,0,n)}},e.prototype.providerSupports=function(e,t){return e.supports(t,this.config.edition)},e.load=function(i,r){return Promise.all(t.map(i,function(t){return new Promise(function(e){switch(t.name){case"caterpillar":n.e(1,function(require){e(n(137))});break;case"html5":!function(require){e(n(43))}(n);break;case"flash":!function(require){e(n(42))}(n);break;case"shaka":n.e(3,function(require){e(n(138))});break;case"youtube":n.e(0,function(require){e(n(57))});break;default:e()}}).then(function(t){t&&(t.setEdition&&t.setEdition(r),e.registerProvider(t))})}))},e}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,function(e,t){"use strict";function n(e,t){var r=t&&t.loc,o=void 0,a=void 0;r&&(o=r.start.line,a=r.start.column,e+=" - "+o+":"+a);for(var s=Error.prototype.constructor.call(this,e),l=0;l<i.length;l++)this[i[l]]=s[i[l]];Error.captureStackTrace&&Error.captureStackTrace(this,n),r&&(this.lineNumber=o,this.column=a)}t.__esModule=!0;var i=["description","fileName","lineNumber","message","name","number","stack"];n.prototype=new Error,t["default"]=n,e.exports=t["default"]},function(e,t,n){var i,r;i=[n(14)],r=function(e){return{localName:function(e){return e?e.localName?e.localName:e.baseName?e.baseName:"":""},textContent:function(t){return t?t.textContent?e.trim(t.textContent):t.text?e.trim(t.text):"":""},getChildNode:function(e,t){return e.childNodes[t]},numChildren:function(e){return e.childNodes?e.childNodes.length:0}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(279),n(280),n(124),n(56)],r=function(e,t,n,i){var r={},o={},a=function(n,i){return o[n]=new e(new t(r),i),o[n]},s=function(e,t,o,a){var s=i.getPluginName(e);r[s]||(r[s]=new n(e)),r[s].registerPlugin(e,t,o,a)};return{loadPlugins:a,registerPlugin:s}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(1),n(4),n(6),n(285),n(31),n(3)],r=function(e,t,n,i,r,o,a){function s(e){return e+"_swf_"+u++}function l(t){var n=document.createElement("a");n.href=t.flashplayer;var i=n.hostname===window.location.host;return e.isChrome()&&!i}function c(c,u){function d(e){if(I)for(var t=0;t<e.length;t++){var n=e[t];if(n.bitrate){var i=Math.round(n.bitrate/1e3);n.label=p(i)}}}function p(e){var t=I[e];if(!t){for(var n=1/0,i=I.bitrates.length;i--;){var r=Math.abs(I.bitrates[i]-e);if(r>n)break;n=r}t=I.labels[I.bitrates[i+1]],I[e]=t}return t}function h(){var e=u.hlslabels;if(!e)return null;var t={},n=[];for(var i in e){var r=parseFloat(i);if(!isNaN(r)){var o=Math.round(r);t[o]=e[i],n.push(o)}}return 0===n.length?null:(n.sort(function(e,t){return e-t}),{labels:t,bitrates:n})}function f(){b=setTimeout(function(){a.trigger.call(P,"flashBlocked")},4e3),w.once("embedded",function(){m(),a.trigger.call(P,"flashUnblocked")},P)}function g(){m(),f()}function m(){clearTimeout(b),window.removeEventListener("focus",g)}var v,w,y,j=null,b=-1,E=!1,k=-1,A=null,L=-1,_=null,x=!0,C=!1,P=this,T=function(){return w&&w.__ready},R=function(){w&&w.triggerFlash.apply(w,arguments)},I=h();t.extend(this,a,{init:function(e){e.preload&&"none"!==e.preload&&!u.autostart&&(j=e)},load:function(e){j=e,E=!1,this.setState(i.LOADING),R("load",e),e.sources.length&&"hls"!==e.sources[0].type&&this.sendMediaType(e.sources)},play:function(){R("play")},pause:function(){R("pause"),this.setState(i.PAUSED)},stop:function(){R("stop"),k=-1,j=null,this.setState(i.IDLE)},seek:function(e){R("seek",e)},volume:function(e){if(t.isNumber(e)){var n=Math.min(Math.max(0,e),100);T()&&R("volume",n)}},mute:function(e){T()&&R("mute",e)},setState:function(){return o.setState.apply(this,arguments)},checkComplete:function(){return E},attachMedia:function(){x=!0,E&&(this.setState(i.COMPLETE),this.trigger(n.JWPLAYER_MEDIA_COMPLETE),E=!1)},detachMedia:function(){return x=!1,null},getSwfObject:function(e){var t=e.getElementsByTagName("object")[0];return t?(t.off(null,null,this),t):r.embed(u.flashplayer,e,s(c),u.wmode)},getContainer:function(){return v},setContainer:function(r){if(v!==r){v=r,w=this.getSwfObject(r),document.hasFocus()?f():window.addEventListener("focus",g),w.once("ready",function(){m(),w.once("pluginsLoaded",function(){w.queueCommands=!1,R("setupCommandQueue",w.__commandQueue),w.__commandQueue=[]});var e=t.extend({},u),i=w.triggerFlash("setup",e);i===w?w.__ready=!0:this.trigger(n.JWPLAYER_MEDIA_ERROR,i),j&&R("init",j)},this);var o=[n.JWPLAYER_MEDIA_META,n.JWPLAYER_MEDIA_ERROR,n.JWPLAYER_MEDIA_SEEK,n.JWPLAYER_MEDIA_SEEKED,"subtitlesTracks","subtitlesTrackChanged","subtitlesTrackData","mediaType"],s=[n.JWPLAYER_MEDIA_BUFFER,n.JWPLAYER_MEDIA_TIME],c=[n.JWPLAYER_MEDIA_BUFFER_FULL];w.on(n.JWPLAYER_MEDIA_LEVELS,function(e){d(e.levels),k=e.currentQuality,A=e.levels,this.trigger(e.type,e)},this),w.on(n.JWPLAYER_MEDIA_LEVEL_CHANGED,function(e){d(e.levels),k=e.currentQuality,A=e.levels,this.trigger(e.type,e)},this),w.on(n.JWPLAYER_AUDIO_TRACKS,function(e){L=e.currentTrack,_=e.tracks,this.trigger(e.type,e)},this),w.on(n.JWPLAYER_AUDIO_TRACK_CHANGED,function(e){L=e.currentTrack,_=e.tracks,this.trigger(e.type,e)},this),w.on(n.JWPLAYER_PLAYER_STATE,function(e){var t=e.newstate;t!==i.IDLE&&this.setState(t)},this),w.on(s.join(" "),function(e){"Infinity"===e.duration&&(e.duration=1/0),this.trigger(e.type,e)},this),w.on(o.join(" "),function(e){this.trigger(e.type,e)},this),w.on(c.join(" "),function(e){this.trigger(e.type)},this),w.on(n.JWPLAYER_MEDIA_BEFORECOMPLETE,function(e){E=!0,this.trigger(e.type),x===!0&&(E=!1)},this),w.on(n.JWPLAYER_MEDIA_COMPLETE,function(e){E||(this.setState(i.COMPLETE),this.trigger(e.type))},this),w.on("visualQuality",function(e){e.reason=e.reason||"api",this.trigger("visualQuality",e),this.trigger(n.JWPLAYER_PROVIDER_FIRST_FRAME,{})},this),w.on(n.JWPLAYER_PROVIDER_CHANGED,function(e){y=e.message,this.trigger(n.JWPLAYER_PROVIDER_CHANGED,e)},this),w.on(n.JWPLAYER_ERROR,function(t){e.log("Error playing media: %o %s",t.code,t.message,t),this.trigger(n.JWPLAYER_MEDIA_ERROR,t)},this),l(u)&&w.on("throttle",function(e){m(),"resume"===e.state?a.trigger.call(P,"flashThrottle",e):b=setTimeout(function(){a.trigger.call(P,"flashThrottle",e)},250)},this)}},remove:function(){k=-1,A=null,r.remove(w)},setVisibility:function(e){e=!!e,v.style.opacity=e?1:0},resize:function(e,t,n){n&&R("stretch",n)},setControls:function(e){R("setControls",e)},setFullscreen:function(e){C=e,R("fullscreen",e)},getFullScreen:function(){return C},setCurrentQuality:function(e){R("setCurrentQuality",e)},getCurrentQuality:function(){return k},setSubtitlesTrack:function(e){R("setSubtitlesTrack",e)},getName:function(){return y?{name:"flash_"+y}:{name:"flash"}},getQualityLevels:function(){return A||j.sources},getAudioTracks:function(){return _},getCurrentAudioTrack:function(){return L},setCurrentAudioTrack:function(e){R("setCurrentAudioTrack",e)},destroy:function(){m(),this.remove(),w&&(w.off(),w=null),v=null,j=null,this.off()}}),this.trigger=function(e,t){return x?a.trigger.call(this,e,t):void 0}}var u=0,d=function(){};return d.prototype=o,c.prototype=new d,c.getName=function(){return{name:"flash"}},c}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(45),n(2),n(128),n(1),n(4),n(6),n(31),n(3)],r=function(e,t,n,i,r,o,a,s){function l(e,n){t.foreach(e,function(e,t){n.addEventListener(e,t,!1)})}function c(e,n){t.foreach(e,function(e,t){n.removeEventListener(e,t,!1)})}function u(e,t,n){"addEventListener"in e?e.addEventListener(t,n):e["on"+t]=n}function d(e,t,n){e&&("removeEventListener"in e?e.removeEventListener(t,n):e["on"+t]=null)}function p(e){if("hls"===e.type)if(e.androidhls!==!1){var n=t.isAndroidNative;if(n(2)||n(3)||n("4.0"))return!1;if(t.isAndroid())return!0}else if(t.isAndroid())return!1;return null}function h(h,A){function L(){Me&&(he(Ge.audioTracks),ve(Ge.textTracks),Ge.setAttribute("jw-loaded","data"))}function _(){Me&&Ge.setAttribute("jw-loaded","started")}function x(e){xe.trigger("click",e)}function C(){Me&&!Oe&&(O(S()),I(ie(),Ae,ke))}function P(){Me&&I(ie(),Ae,ke)}function T(){f(Re),Pe=!0,Me&&(xe.state===o.STALLED?xe.setState(o.PLAYING):xe.state===o.PLAYING&&(Re=setTimeout(ne,g)),Oe&&Ge.duration===1/0&&0===Ge.currentTime||(O(S()),M(Ge.currentTime),I(ie(),Ae,ke),xe.state===o.PLAYING&&(xe.trigger(r.JWPLAYER_MEDIA_TIME,{position:Ae,duration:ke}),R())))}function R(){var e=He.level;if(e.width!==Ge.videoWidth||e.height!==Ge.videoHeight){if(e.width=Ge.videoWidth,e.height=Ge.videoHeight,je(),!e.width||!e.height)return;He.reason=He.reason||"auto",He.mode="hls"===_e[Se].type?"auto":"manual",He.bitrate=0,e.index=Se,e.label=_e[Se].label,xe.trigger("visualQuality",He),He.reason=""}}function I(e,t,n){e===Ie&&n===ke||(Ie=e,xe.trigger(r.JWPLAYER_MEDIA_BUFFER,{bufferPercent:100*e,position:t,duration:n}))}function M(e){0>ke&&(e=-(Z()-e)),Ae=e}function S(){var e=Ge.duration,t=Z();if(e===1/0&&t){var n=t-Ge.seekable.start(0);n!==1/0&&n>120&&(e=-n)}return e}function O(e){ke=e,Te&&e&&e!==1/0&&xe.seek(Te)}function D(){var e=S();Oe&&e===1/0&&(e=0),xe.trigger(r.JWPLAYER_MEDIA_META,{duration:e,height:Ge.videoHeight,width:Ge.videoWidth}),O(e)}function Y(){Me&&(Pe=!0,Oe||je(),W())}function N(){Me&&(Ge.muted&&(Ge.muted=!1,Ge.muted=!0),Ge.setAttribute("jw-loaded","meta"),D())}function W(){Le||(Le=!0,xe.trigger(r.JWPLAYER_MEDIA_BUFFER_FULL))}function F(){xe.setState(o.PLAYING),Ge.hasAttribute("jw-played")||Ge.setAttribute("jw-played",""),xe.trigger(r.JWPLAYER_PROVIDER_FIRST_FRAME,{})}function J(){xe.state!==o.COMPLETE&&Ge.currentTime!==Ge.duration&&xe.setState(o.PAUSED)}function V(){Oe||Ge.paused||Ge.ended||xe.state!==o.LOADING&&xe.state!==o.ERROR&&(xe.seeking||xe.setState(o.STALLED))}function B(){Me&&(t.log("Error playing media: %o %s",Ge.error,Ge.src),xe.trigger(r.JWPLAYER_MEDIA_ERROR,{message:"Error loading media: File could not be played"}))}function U(e){var n;return"array"===t.typeOf(e)&&e.length>0&&(n=i.map(e,function(e,t){return{label:e.label||t}})),n}function H(e){_e=e,Se=z(e);var t=U(e);t&&xe.trigger(r.JWPLAYER_MEDIA_LEVELS,{levels:t,currentQuality:Se})}function z(e){var t=Math.max(0,Se),n=A.qualityLabel;
	if(e)for(var i=0;i<e.length;i++)if(e[i]["default"]&&(t=i),n&&e[i].label===n)return i;return He.reason="initial choice",He.level={},t}function G(e,n){Te=0,f(Re);var i=document.createElement("source");i.src=_e[Se].file;var r=Ge.src!==i.src,a=Ge.getAttribute("jw-loaded"),s=Ge.hasAttribute("jw-played");r||"none"===a||"started"===a?(ke=n,K(_e[Se]),q(Ue),Ge.load()):(0===e&&Ge.currentTime>0&&(Te=-1,xe.seek(e)),Ge.play()),Ae=Ge.currentTime,w&&!s&&(W(),Ge.paused||xe.state===o.PLAYING||xe.setState(o.LOADING)),t.isIOS()&&xe.getFullScreen()&&(Ge.controls=!0),e>0&&xe.seek(e)}function K(e){We=null,Fe=null,Ve=-1,Je=-1,Be=-1,He.reason||(He.reason="initial choice",He.level={}),Pe=!1,Le=!1,Oe=p(e),e.preload&&e.preload!==Ge.getAttribute("preload")&&Ge.setAttribute("preload",e.preload);var t=document.createElement("source");t.src=e.file;var n=Ge.src!==t.src;n&&(Ge.setAttribute("jw-loaded","none"),Ge.src=e.file)}function Q(){Ge&&(be(),Ge.removeAttribute("crossorigin"),Ge.removeAttribute("preload"),Ge.removeAttribute("src"),Ge.removeAttribute("jw-loaded"),Ge.removeAttribute("jw-played"),n.emptyElement(Ge),Se=-1,Ue=null,!v&&"load"in Ge&&Ge.load())}function q(e){var i=t.isChrome()||t.isIOS()||t.isSafari();!De&&i&&(e!==Ue||e&&e.length&&!Ge.textTracks.length)&&(be(),n.emptyElement(Ge),Ue=e,X(e))}function X(e){if(e)for(var n=!1,i=0;i<e.length;i++){var r=e[i];if(/\.(?:web)?vtt(?:\?.*)?$/i.test(r.file)&&/subtitles|captions|descriptions|chapters|metadata/i.test(r.kind)){n||!Ge.hasAttribute("crossorigin")&&t.crossdomain(r.file)&&(Ge.setAttribute("crossorigin","anonymous"),n=!0);var o=document.createElement("track");o.src=r.file,o.kind=r.kind,o.srclang=r.language||"",o.label=r.label,o.mode="disabled",o.id=r["default"]||r.defaulttrack?"default":"",Ge.appendChild(o)}}}function $(){for(var e=Ge.seekable?Ge.seekable.length:0,t=1/0;e--;)t=Math.min(t,Ge.seekable.start(e));return t}function Z(){for(var e=Ge.seekable?Ge.seekable.length:0,t=0;e--;)t=Math.max(t,Ge.seekable.end(e));return t}function ee(){xe.seeking=!1,xe.trigger(r.JWPLAYER_MEDIA_SEEKED)}function te(){xe.trigger("volume",{volume:Math.round(100*Ge.volume)}),xe.trigger("mute",{mute:Ge.muted})}function ne(){Ge.currentTime===Ae&&V()}function ie(){var e=Ge.buffered,n=Ge.duration;return!e||0===e.length||0>=n||n===1/0?0:t.between(e.end(e.length-1)/n,0,1)}function re(){if(Me&&xe.state!==o.IDLE&&xe.state!==o.COMPLETE){if(f(Re),Se=-1,Ye=!0,xe.trigger(r.JWPLAYER_MEDIA_BEFORECOMPLETE),!Me)return;oe()}}function oe(){f(Re),xe.setState(o.COMPLETE),Ye=!1,xe.trigger(r.JWPLAYER_MEDIA_COMPLETE)}function ae(e){Ne=!0,pe(e),t.isIOS()&&(Ge.controls=!1)}function se(){var e=-1,t=0;if(We)for(t;t<We.length;t++)if("showing"===We[t].mode){e=t;break}we(e+1)}function le(){for(var e=-1,t=0;t<Ge.audioTracks.length;t++)if(Ge.audioTracks[t].enabled){e=t;break}fe(e)}function ce(e){ue(e.currentTarget.activeCues)}function ue(e){if(e&&e.length&&Be!==e[0].startTime){var n=t.parseID3(e);Be=e[0].startTime,xe.trigger("meta",{metadataTime:Be,metadata:n})}}function de(e){Ne=!1,pe(e),t.isIOS()&&(Ge.controls=!1)}function pe(e){xe.trigger("fullscreenchange",{target:e.target,jwstate:Ne})}function he(e){if(Fe=null,e){if(e.length){for(var t=0;t<e.length;t++)if(e[t].enabled){Ve=t;break}-1===Ve&&(Ve=0,e[Ve].enabled=!0),Fe=i.map(e,function(e){var t={name:e.label||e.language,language:e.language};return t})}u(e,"change",le),Fe&&xe.trigger("audioTracks",{currentTrack:Ve,tracks:Fe})}}function fe(e){Ge&&Ge.audioTracks&&Fe&&e>-1&&e<Ge.audioTracks.length&&e!==Ve&&(Ge.audioTracks[Ve].enabled=!1,Ve=e,Ge.audioTracks[Ve].enabled=!0,xe.trigger("audioTrackChanged",{currentTrack:Ve,tracks:Fe}))}function ge(){return Fe||[]}function me(){return Ve}function ve(e){if(We=null,e){if(e.length){var t=0,n=e.length;for(t;n>t;t++)"metadata"===e[t].kind?(e[t].oncuechange=ce,e[t].mode="showing"):"subtitles"!==e[t].kind&&"captions"!==e[t].kind||(e[t].mode="disabled",We||(We=[]),We.push(e[t]))}u(e,"change",se),We&&We.length&&xe.trigger("subtitlesTracks",{tracks:We})}}function we(e){We&&Je!==e-1&&(Je>-1&&Je<We.length?We[Je].mode="disabled":i.each(We,function(e){e.mode="disabled"}),e>0&&e<=We.length?(Je=e-1,We[Je].mode="showing"):Je=-1,xe.trigger("subtitlesTrackChanged",{currentTrack:Je+1,tracks:We}))}function ye(){return Je}function je(){if("hls"===_e[0].type){var e="video";0===Ge.videoHeight&&(e="audio"),xe.trigger("mediaType",{mediaType:e})}}function be(){We&&We[Je]&&(We[Je].mode="disabled")}this.state=o.IDLE,this.seeking=!1,i.extend(this,s),this.trigger=function(e,t){return Me?s.trigger.call(this,e,t):void 0},this.setState=function(e){return Me?a.setState.call(this,e):void 0};var Ee,ke,Ae,Le,_e,xe=this,Ce={click:x,durationchange:C,ended:re,error:B,loadstart:_,loadeddata:L,loadedmetadata:N,canplay:Y,playing:F,progress:P,pause:J,seeked:ee,timeupdate:T,volumechange:te,webkitbeginfullscreen:ae,webkitendfullscreen:de},Pe=!1,Te=0,Re=-1,Ie=-1,Me=!0,Se=-1,Oe=null,De=!!A.sdkplatform,Ye=!1,Ne=!1,We=null,Fe=null,Je=-1,Ve=-1,Be=-1,Ue=null,He={level:{}},ze=document.getElementById(h),Ge=ze?ze.querySelector("video"):void 0;Ge=Ge||document.createElement("video"),Ge.className="jw-video jw-reset",i.isObject(A.cast)&&A.cast.appid&&Ge.setAttribute("disableRemotePlayback",""),l(Ce,Ge),b||(Ge.controls=!0,Ge.controls=!1),Ge.setAttribute("x-webkit-airplay","allow"),Ge.setAttribute("webkit-playsinline",""),this.stop=function(){f(Re),Me&&(Q(),t.isIETrident()&&Ge.pause(),this.setState(o.IDLE))},this.destroy=function(){c(Ce,Ge),d(Ge.audioTracks,"change",le),d(Ge.textTracks,"change",se),this.remove(),this.off()},this.init=function(e){Me&&(Ue=null,_e=e.sources,Se=z(e.sources),e.sources.length&&"hls"!==e.sources[0].type&&this.sendMediaType(e.sources),Ae=e.starttime||0,ke=e.duration||0,He.reason="",K(_e[Se]),q(e.tracks))},this.load=function(e){Me&&(H(e.sources),e.sources.length&&"hls"!==e.sources[0].type&&this.sendMediaType(e.sources),w&&!Ge.hasAttribute("jw-played")||xe.setState(o.LOADING),G(e.starttime||0,e.duration||0))},this.play=function(){return xe.seeking?(xe.setState(o.LOADING),void xe.once(r.JWPLAYER_MEDIA_SEEKED,xe.play)):void Ge.play()},this.pause=function(){f(Re),Ge.pause(),this.setState(o.PAUSED)},this.seek=function(e){if(Me)if(0>e&&(e+=$()+Z()),0===Te&&this.trigger(r.JWPLAYER_MEDIA_SEEK,{position:Ge.currentTime,offset:e}),Pe||(Pe=!!Z()),Pe){Te=0;try{xe.seeking=!0,Ge.currentTime=e}catch(t){xe.seeking=!1,Te=e}}else Te=e,y&&Ge.paused&&Ge.play()},this.volume=function(e){e=t.between(e/100,0,1),Ge.volume=e},this.mute=function(e){Ge.muted=!!e},this.checkComplete=function(){return Ye},this.detachMedia=function(){return f(Re),be(),Me=!1,Ge},this.attachMedia=function(){Me=!0,Pe=!1,this.seeking=!1,Ge.loop=!1,Ye&&oe()},this.setContainer=function(e){Ee=e,e.appendChild(Ge)},this.getContainer=function(){return Ee},this.remove=function(){Q(),f(Re),Ee===Ge.parentNode&&Ee.removeChild(Ge)},this.setVisibility=function(t){t=!!t,t||j?e.style(Ee,{visibility:"visible",opacity:1}):e.style(Ee,{visibility:"",opacity:0})},this.resize=function(t,n,i){if(!(t&&n&&Ge.videoWidth&&Ge.videoHeight))return!1;var r={objectFit:""};if("uniform"===i){var o=t/n,a=Ge.videoWidth/Ge.videoHeight;Math.abs(o-a)<.09&&(r.objectFit="fill",i="exactfit")}var s=m||j||b||E;if(s){var l=-Math.floor(Ge.videoWidth/2+1),c=-Math.floor(Ge.videoHeight/2+1),u=Math.ceil(100*t/Ge.videoWidth)/100,d=Math.ceil(100*n/Ge.videoHeight)/100;"none"===i?u=d=1:"fill"===i?u=d=Math.max(u,d):"uniform"===i&&(u=d=Math.min(u,d)),r.width=Ge.videoWidth,r.height=Ge.videoHeight,r.top=r.left="50%",r.margin=0,e.transform(Ge,"translate("+l+"px, "+c+"px) scale("+u.toFixed(2)+", "+d.toFixed(2)+")")}return e.style(Ge,r),!1},this.setFullscreen=function(e){if(e=!!e){var n=t.tryCatch(function(){var e=Ge.webkitEnterFullscreen||Ge.webkitEnterFullScreen;e&&e.apply(Ge)});return n instanceof t.Error?!1:xe.getFullScreen()}var i=Ge.webkitExitFullscreen||Ge.webkitExitFullScreen;return i&&i.apply(Ge),e},xe.getFullScreen=function(){return Ne||!!Ge.webkitDisplayingFullscreen},this.setCurrentQuality=function(e){if(Se!==e&&e>=0&&_e&&_e.length>e){Se=e,He.reason="api",He.level={},this.trigger(r.JWPLAYER_MEDIA_LEVEL_CHANGED,{currentQuality:e,levels:U(_e)}),A.qualityLabel=_e[e].label;var t=Ge.currentTime||0,n=Ge.duration||0;0>=n&&(n=ke),xe.setState(o.LOADING),G(t,n)}},this.getCurrentQuality=function(){return Se},this.getQualityLevels=function(){return U(_e)},this.getName=function(){return{name:k}},this.setCurrentAudioTrack=fe,this.getAudioTracks=ge,this.getCurrentAudioTrack=me,this.setSubtitlesTrack=we,this.getSubtitlesTrack=ye}var f=window.clearTimeout,g=256,m=t.isIE(),v=t.isMSIE(),w=t.isMobile(),y=t.isFF(),j=t.isAndroidNative(),b=t.isIOS(7),E=t.isIOS(8),k="html5",A=function(){};return A.prototype=a,h.prototype=new A,h.getName=function(){return{name:"html5"}},h}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return{repo:"http://ssl.p.jwpcdn.com/player/v/",SkinsIncluded:["seven"],SkinsLoadable:["beelden","bekle","five","glow","roundster","six","stormtrooper","vapor"],dvrSeekLimit:-25}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(14)],r=function(e){function t(e){e=e.split("-");for(var t=1;t<e.length;t++)e[t]=e[t].charAt(0).toUpperCase()+e[t].slice(1);return e.join("")}function n(t,n,i){if(""===n||void 0===n||null===n)return"";var r=i?" !important":"";return"string"==typeof n&&isNaN(n)?/png|gif|jpe?g/i.test(n)&&n.indexOf("url")<0?"url("+n+")":n+r:0===n||"z-index"===t||"opacity"===t?""+n+r:/color/i.test(t)?"#"+e.pad(n.toString(16).replace(/^0x/i,""),6)+r:Math.ceil(n)+"px"+r}var i,r={},o=function(e,t){i||(i=document.createElement("style"),i.type="text/css",document.getElementsByTagName("head")[0].appendChild(i));var n="";if("object"==typeof t){var o=document.createElement("div");a(o,t),n="{"+o.style.cssText+"}"}else"string"==typeof t&&(n=t);var s=document.createTextNode(e+n);r[e]&&i.removeChild(r[e]),r[e]=s,i.appendChild(s)},a=function(e,i){if(void 0!==e&&null!==e){void 0===e.length&&(e=[e]);var r,o={};for(r in i)o[r]=n(r,i[r]);for(var a=0;a<e.length;a++){var s,l=e[a];if(void 0!==l&&null!==l)for(r in o)s=t(r),l.style[s]!==o[r]&&(l.style[s]=o[r])}}},s=function(e){for(var t in r)t.indexOf(e)>=0&&(i.removeChild(r[t]),delete r[t])},l=function(e,t){a(e,{transform:t,webkitTransform:t,msTransform:t,mozTransform:t,oTransform:t})},c=function(e,t){var n="rgb";e?(e=String(e).replace("#",""),3===e.length&&(e=e[0]+e[0]+e[1]+e[1]+e[2]+e[2])):e="000000";var i=[parseInt(e.substr(0,2),16),parseInt(e.substr(2,2),16),parseInt(e.substr(4,2),16)];return void 0!==t&&100!==t&&(n+="a",i.push(t/100)),n+"("+i.join(",")+")"};return{css:o,style:a,clearCss:s,transform:l,hexToRgba:c}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return"7.4.3+commercial_v7-4-3.99.commercial.367437.jwplayer.3385ed.googima.94075d.vast.e9c65d.analytics.852ed0.plugin-gapro.4c6bfd.plugin-related.180896.plugin-sharing.c16f4f"}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(129),n(2)],r=function(e,t){var n=e.extend({constructor:function(e){this.el=document.createElement("div"),this.el.className="jw-icon jw-icon-tooltip "+e+" jw-button-color jw-reset jw-hidden",this.container=document.createElement("div"),this.container.className="jw-overlay jw-reset",this.openClass="jw-open",this.componentType="tooltip",this.el.appendChild(this.container)},addContent:function(e){this.content&&this.removeContent(),this.content=e,this.container.appendChild(e)},removeContent:function(){this.content&&(this.container.removeChild(this.content),this.content=null)},hasContent:function(){return!!this.content},element:function(){return this.el},openTooltip:function(e){this.trigger("open-"+this.componentType,e,{isOpen:!0}),this.isOpen=!0,t.toggleClass(this.el,this.openClass,this.isOpen)},closeTooltip:function(e){this.trigger("close-"+this.componentType,e,{isOpen:!1}),this.isOpen=!1,t.toggleClass(this.el,this.openClass,this.isOpen)},toggleOpenState:function(e){this.isOpen?this.closeTooltip(e):this.openTooltip(e)}});return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t="free",n="premium",i="enterprise",r="ads",o="unlimited",a="trial",s={setup:[t,n,i,r,o,a],dash:[n,i,r,o,a],drm:[i,r,o,a],hls:[n,r,i,o,a],ads:[r,o,a],casting:[n,i,r,o,a],jwpsrv:[t,n,i,r,a]},l=function(t){return function(n){return e.contains(s[n],t)}};return l}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,,function(e,t,n){var i,r;i=[n(2),n(33),n(271),n(1),n(3),n(287),n(4),n(6)],r=function(e,t,n,i,r,o,a,s){var l=function(){function o(e,t){var n=i.extend({},t,{type:e});switch(e){case"flashThrottle":var r="resume"!==t.state;this.set("flashThrottle",r),this.set("flashBlocked",r);break;case"flashBlocked":return void this.set("flashBlocked",!0);case"flashUnblocked":return void this.set("flashBlocked",!1);case"volume":case"mute":return void this.set(e,t[e]);case a.JWPLAYER_MEDIA_TYPE:return void(this.mediaModel.get("mediaType")!==t.mediaType&&(this.mediaModel.set("mediaType",t.mediaType),this.mediaController.trigger(e,n)));case a.JWPLAYER_PLAYER_STATE:return void this.mediaModel.set("state",t.newstate);case a.JWPLAYER_MEDIA_BUFFER:this.set("buffer",t.bufferPercent);case a.JWPLAYER_MEDIA_META:var o=t.duration;i.isNumber(o)&&(this.mediaModel.set("duration",o),this.set("duration",o));break;case a.JWPLAYER_MEDIA_BUFFER_FULL:this.mediaModel.get("playAttempt")?this.playVideo():this.mediaModel.on("change:playAttempt",function(){this.playVideo()},this);break;case a.JWPLAYER_MEDIA_TIME:this.mediaModel.set("position",t.position),this.set("position",t.position),i.isNumber(t.duration)&&(this.mediaModel.set("duration",t.duration),this.set("duration",t.duration));break;case a.JWPLAYER_PROVIDER_CHANGED:this.set("provider",u.getName());break;case a.JWPLAYER_MEDIA_LEVELS:this.setQualityLevel(t.currentQuality,t.levels),this.mediaModel.set("levels",t.levels);break;case a.JWPLAYER_MEDIA_LEVEL_CHANGED:this.setQualityLevel(t.currentQuality,t.levels),this.persistQualityLevel(t.currentQuality,t.levels);break;case a.JWPLAYER_AUDIO_TRACKS:this.setCurrentAudioTrack(t.currentTrack,t.tracks),this.mediaModel.set("audioTracks",t.tracks);break;case a.JWPLAYER_AUDIO_TRACK_CHANGED:this.setCurrentAudioTrack(t.currentTrack,t.tracks);break;case"subtitlesTrackChanged":this.setVideoSubtitleTrack(t.currentTrack,t.tracks);break;case"visualQuality":var s=i.extend({},t);this.mediaModel.set("visualQuality",s)}this.mediaController.trigger(e,n)}var l,u,d=this,p=e.noop;this.mediaController=i.extend({},r),this.mediaModel=new c,n.model(this),this.set("mediaModel",this.mediaModel),this.setup=function(t){return i.extend(this.attributes,t,{item:0,state:s.IDLE,flashBlocked:!1,fullscreen:!1,compactUI:!1,scrubbing:!1,duration:0,position:0,buffer:0}),e.isMobile()&&!t.mobileSdk&&this.set("autostart",!1),this.updateProviders(),this},this.getConfiguration=function(){return i.omit(this.clone(),["mediaModel"])},this.updateProviders=function(){l=new t(this.getConfiguration())},this.setQualityLevel=function(e,t){e>-1&&t.length>1&&"youtube"!==u.getName().name&&this.mediaModel.set("currentLevel",parseInt(e))},this.persistQualityLevel=function(e,t){var n=t[e]||{},i=n.label;this.set("qualityLabel",i)},this.setCurrentAudioTrack=function(e,t){e>-1&&t.length>0&&e<t.length&&this.mediaModel.set("currentAudioTrack",parseInt(e))},this.onMediaContainer=function(){var e=this.get("mediaContainer");p.setContainer(e)},this.changeVideoProvider=function(e){if(this.off("change:mediaContainer",this.onMediaContainer),u&&(u.off(null,null,this),u.getContainer()&&u.remove()),!e)return u=p=e,void this.set("provider",void 0);p=new e(d.get("id"),d.getConfiguration());var t=this.get("mediaContainer");t?p.setContainer(t):this.once("change:mediaContainer",this.onMediaContainer),this.set("provider",p.getName()),-1===p.getName().name.indexOf("flash")&&(this.set("flashThrottle",void 0),this.set("flashBlocked",!1)),u=p,u.volume(d.get("volume")),u.mute(d.get("mute")),u.on("all",o,this)},this.destroy=function(){this.off(),u&&(u.off(null,null,this),u.destroy())},this.getVideo=function(){return u},this.setFullscreen=function(e){e=!!e,e!==d.get("fullscreen")&&d.set("fullscreen",e)},this.chooseProvider=function(e){return l.choose(e).provider},this.setActiveItem=function(e){this.mediaModel.off(),this.mediaModel=new c,this.set("mediaModel",this.mediaModel),this.set("position",e.starttime||0),this.set("duration",e.duration||0),this.setProvider(e)},this.setProvider=function(e){var t=e&&e.sources&&e.sources[0];if(void 0!==t){var n=this.chooseProvider(t);n&&p instanceof n||d.changeVideoProvider(n),p&&(p.init&&p.init(e),this.trigger("itemReady",e))}},this.getProviders=function(){return l},this.resetProvider=function(){p=null},this.setVolume=function(e){e=Math.round(e),d.set("volume",e),u&&u.volume(e);var t=0===e;t!==d.get("mute")&&d.setMute(t)},this.setMute=function(t){if(e.exists(t)||(t=!d.get("mute")),d.set("mute",t),u&&u.mute(t),!t){var n=Math.max(10,d.get("volume"));this.setVolume(n)}},this.loadVideo=function(e){if(!e){var t=this.get("item");e=this.get("playlist")[t]}this.set("position",e.starttime||0),this.set("duration",e.duration||0),this.mediaModel.set("playAttempt",!0),this.mediaController.trigger(a.JWPLAYER_MEDIA_PLAY_ATTEMPT,{playReason:this.get("playReason")}),u.load(e)},this.stopVideo=function(){u&&u.stop()},this.playVideo=function(){u.play()},this.persistCaptionsTrack=function(){var e=this.get("captionsTrack");e?this.set("captionLabel",e.label):this.set("captionLabel","Off")},this.setVideoSubtitleTrack=function(e,t){this.set("captionsIndex",e),e&&t&&e<=t.length&&t[e-1].data&&this.set("captionsTrack",t[e-1]),u&&u.setSubtitlesTrack&&u.setSubtitlesTrack(e)},this.persistVideoSubtitleTrack=function(e){this.setVideoSubtitleTrack(e),this.persistCaptionsTrack()}},c=l.MediaModel=function(){this.set("state",s.IDLE)};return i.extend(l.prototype,o),i.extend(c.prototype,o),l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(14)],r=function(e){var t={},n=t.pluginPathType={ABSOLUTE:0,RELATIVE:1,CDN:2};return t.getPluginPathType=function(t){if("string"==typeof t){t=t.split("?")[0];var i=t.indexOf("://");if(i>0)return n.ABSOLUTE;var r=t.indexOf("/"),o=e.extension(t);return!(0>i&&0>r)||o&&isNaN(o)?n.RELATIVE:n.CDN}},t.getPluginName=function(e){return e.replace(/^(.*\/)?([^-]*)-?.*\.(swf|js)$/,"$2")},t.getPluginVersion=function(e){return e.replace(/[^-]*-?([^\.]*).*$/,"$1")},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i,r;i=[n(1)],r=function(e){var t={},n={TIT2:"title",TT2:"title",WXXX:"url",TPE1:"artist",TP1:"artist",TALB:"album",TAL:"album"};return t.utf8ArrayToStr=function(e,t){var n,i,r,o,a,s;for(n="",r=e.length,i=t||0;r>i;)if(o=e[i++],0!==o&&3!==o)switch(o>>4){case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:n+=String.fromCharCode(o);break;case 12:case 13:a=e[i++],n+=String.fromCharCode((31&o)<<6|63&a);break;case 14:a=e[i++],s=e[i++],n+=String.fromCharCode((15&o)<<12|(63&a)<<6|(63&s)<<0)}return n},t.utf16BigEndianArrayToStr=function(e,t){var n,i,r;for(n="",r=e.length-1,i=t||0;r>i;)254===e[i]&&255===e[i+1]||(n+=String.fromCharCode((e[i]<<8)+e[i+1])),i+=2;return n},t.syncSafeInt=function(e){var n=t.arrayToInt(e);return 127&n|(32512&n)>>1|(8323072&n)>>2|(2130706432&n)>>3},t.arrayToInt=function(e){for(var t="0x",n=0;n<e.length;n++)t+=e[n].toString(16);return parseInt(t)},t.parseID3=function(i){return e.reduce(i,function(i,r){if(!("value"in r)&&"data"in r&&r.data instanceof ArrayBuffer){var o=r,a=new Uint8Array(o.data),s=a.length;r={value:{key:"",data:""}};for(var l=10;14>l&&l<a.length&&0!==a[l];)r.value.key+=String.fromCharCode(a[l]),l++;var c=19,u=a[c];3!==u&&0!==u||(u=a[++c],s--);var d=0;if(1!==u&&2!==u)for(var p=c+1;s>p;p++)if(0===a[p]){d=p-c;break}if(d>0){var h=t.utf8ArrayToStr(a.subarray(c,c+=d),0);if("PRIV"===r.value.key){if("com.apple.streaming.transportStreamTimestamp"===h){var f=1&t.syncSafeInt(a.subarray(c,c+=4)),g=t.syncSafeInt(a.subarray(c,c+=4));f&&(g+=4294967296),r.value.data=g}else r.value.data=t.utf8ArrayToStr(a,c+1);r.value.info=h}else r.value.info=h,r.value.data=t.utf8ArrayToStr(a,c+1)}else{var m=a[c];1===m||2===m?r.value.data=t.utf16BigEndianArrayToStr(a,c+1):r.value.data=t.utf8ArrayToStr(a,c+1)}}if(n.hasOwnProperty(r.value.key)&&(i[n[r.value.key]]=r.value.data),r.value.info){var v=i[r.value.key];e.isObject(v)||(v={},i[r.value.key]=v),v[r.value.info]=r.value.data}else i[r.value.key]=r.value.data;return i},{})},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e=window.chrome,t={};return t.NS="urn:x-cast:com.longtailvideo.jwplayer",t.debug=!1,t.availability=null,t.available=function(n){n=n||t.availability;var i="available";return e&&e.cast&&e.cast.ReceiverAvailability&&(i=e.cast.ReceiverAvailability.AVAILABLE),n===i},t.log=function(){if(t.debug){var e=Array.prototype.slice.call(arguments,0);console.log.apply(console,e)}},t.error=function(){var e=Array.prototype.slice.call(arguments,0);console.error.apply(console,e)},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,,,,,,,,,,,,,,,,,,,function(e,t,n){var i,r;i=[n(6)],r=function(e){function t(t){return t===e.COMPLETE||t===e.ERROR?e.IDLE:t}return function(e,n,i){if(n=t(n),i=t(i),n!==i){var r=n.replace(/(?:ing|d)$/,""),o={type:r,newstate:n,oldstate:i,reason:e.mediaModel.get("state")};"play"===r&&(o.playReason=e.get("playReason")),this.trigger(r,o)}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(14)],r=function(e,t){function n(e){var t={},n=e.split("\r\n");1===n.length&&(n=e.split("\n"));var r=1;if(n[0].indexOf(" --> ")>0&&(r=0),n.length>r+1&&n[r+1]){var o=n[r],a=o.indexOf(" --> ");a>0&&(t.begin=i(o.substr(0,a)),t.end=i(o.substr(a+5)),t.text=n.slice(r+1).join("<br/>"))}return t}var i=e.seconds;return function(e){var i=[];e=t.trim(e);var r=e.split("\r\n\r\n");1===r.length&&(r=e.split("\n\n"));for(var o=0;o<r.length;o++)if("WEBVTT"!==r[o]){var a=n(r[o]);a.text&&i.push(a)}if(!i.length)throw new Error("Invalid SRT file");return i}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(123),n(278)],r=function(e,t,n){var i={sources:[],tracks:[]},r=function(r){r=r||{},e.isArray(r.tracks)||delete r.tracks;var o=e.extend({},i,r);e.isObject(o.sources)&&!e.isArray(o.sources)&&(o.sources=[t(o.sources)]),e.isArray(o.sources)&&0!==o.sources.length||(r.levels?o.sources=r.levels:o.sources=[t(r)]);for(var a=0;a<o.sources.length;a++){var s=o.sources[a];if(s){var l=s["default"];l?s["default"]="true"===l.toString():s["default"]=!1,o.sources[a].label||(o.sources[a].label=a.toString()),o.sources[a]=t(o.sources[a])}}return o.sources=e.compact(o.sources),e.isArray(o.tracks)||(o.tracks=[]),e.isArray(o.captions)&&(o.tracks=o.tracks.concat(o.captions),delete o.captions),o.tracks=e.compact(e.map(o.tracks,n)),o};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(87)],r=function(e,t){function n(e){return/^(?:(?:https?|file)\:)?\/\//.test(e)}function i(t){return e.some(t,function(e){return"parsererror"===e.nodeName})}var r={};return r.getAbsolutePath=function(e,i){if(t.exists(i)||(i=document.location.href),t.exists(e)){if(n(e))return e;var r,o=i.substring(0,i.indexOf("://")+3),a=i.substring(o.length,i.indexOf("/",o.length+1));if(0===e.indexOf("/"))r=e.split("/");else{var s=i.split("?")[0];s=s.substring(o.length+a.length+1,s.lastIndexOf("/")),r=s.split("/").concat(e.split("/"))}for(var l=[],c=0;c<r.length;c++)r[c]&&t.exists(r[c])&&"."!==r[c]&&(".."===r[c]?l.pop():l.push(r[c]));return o+a+"/"+l.join("/")}},r.getScriptPath=e.memoize(function(e){for(var t=document.getElementsByTagName("script"),n=0;n<t.length;n++){var i=t[n].src;if(i&&i.indexOf(e)>=0)return i.substr(0,i.indexOf(e))}return""}),r.parseXML=function(e){var t=null;try{"DOMParser"in window?(t=(new window.DOMParser).parseFromString(e,"text/xml"),(i(t.childNodes)||t.childNodes&&i(t.childNodes[0].childNodes))&&(t=null)):(t=new window.ActiveXObject("Microsoft.XMLDOM"),t.async="false",t.loadXML(e))}catch(n){}return t},r.serialize=function(e){if(void 0===e)return null;if("string"==typeof e&&e.length<6){var t=e.toLowerCase();if("true"===t)return!0;if("false"===t)return!1;if(!isNaN(Number(e))&&!isNaN(parseFloat(e)))return Number(e)}return e},r.parseDimension=function(e){return"string"==typeof e?""===e?0:e.lastIndexOf("%")>-1?e:parseInt(e.replace("px",""),10):e},r.timeFormat=function(e,t){if(0>=e&&!t)return"00:00";var n=0>e?"-":"";e=Math.abs(e);var i=Math.floor(e/3600),r=Math.floor((e-3600*i)/60),o=Math.floor(e%60);return n+(i?i+":":"")+(10>r?"0":"")+r+":"+(10>o?"0":"")+o},r.adaptiveType=function(e){if(0!==e){var t=-120;if(t>=e)return"DVR";if(0>e||e===1/0)return"LIVE"}return"VOD"},r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t={};return t.exists=function(e){switch(typeof e){case"string":return e.length>0;case"object":return null!==e;case"undefined":return!1}return!0},t.isHTTPS=function(){return 0===window.location.href.indexOf("https")},t.isRtmp=function(e,t){return 0===e.indexOf("rtmp")||"rtmp"===t},t.isYouTube=function(e,t){return"youtube"===t||/^(http|\/\/).*(youtube\.com|youtu\.be)\/.+/.test(e)},t.youTubeID=function(e){var t=/v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(e);return t?t.slice(1).join("").replace("?",""):""},t.typeOf=function(t){if(null===t)return"null";var n=typeof t;return"object"===n&&e.isArray(t)?"array":n},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(129),n(12),n(243),n(2)],r=function(e,t,n,i){var r=e.extend({constructor:function(e,t){this.className=e+" jw-background-color jw-reset",this.orientation=t,this.dragStartListener=this.dragStart.bind(this),this.dragMoveListener=this.dragMove.bind(this),this.dragEndListener=this.dragEnd.bind(this),this.tapListener=this.tap.bind(this),this.setup()},setup:function(){var e={"default":this["default"],className:this.className,orientation:"jw-slider-"+this.orientation};this.el=i.createElement(n(e)),this.elementRail=this.el.getElementsByClassName("jw-slider-container")[0],this.elementBuffer=this.el.getElementsByClassName("jw-buffer")[0],this.elementProgress=this.el.getElementsByClassName("jw-progress")[0],this.elementThumb=this.el.getElementsByClassName("jw-knob")[0],this.userInteract=new t(this.element(),{preventScrolling:!0}),this.userInteract.on("dragStart",this.dragStartListener),this.userInteract.on("drag",this.dragMoveListener),this.userInteract.on("dragEnd",this.dragEndListener),this.userInteract.on("tap click",this.tapListener)},dragStart:function(){this.trigger("dragStart"),this.railBounds=i.bounds(this.elementRail)},dragEnd:function(e){this.dragMove(e),this.trigger("dragEnd")},dragMove:function(e){var t,n,r=this.railBounds=this.railBounds?this.railBounds:i.bounds(this.elementRail);"horizontal"===this.orientation?(t=e.pageX,n=t<r.left?0:t>r.right?100:100*i.between((t-r.left)/r.width,0,1)):(t=e.pageY,n=t>=r.bottom?0:t<=r.top?100:100*i.between((r.height-(t-r.top))/r.height,0,1));var o=this.limit(n);return this.render(o),this.update(o),!1},tap:function(e){this.railBounds=i.bounds(this.elementRail),this.dragMove(e)},limit:function(e){return e},update:function(e){this.trigger("update",{percentage:e})},render:function(e){e=Math.max(0,Math.min(e,100)),"horizontal"===this.orientation?(this.elementThumb.style.left=e+"%",this.elementProgress.style.width=e+"%"):(this.elementThumb.style.bottom=e+"%",this.elementProgress.style.height=e+"%")},updateBuffer:function(e){this.elementBuffer.style.width=e+"%"},element:function(){return this.el}});return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(127),n(48),n(1),n(282)],r=function(e,t,n,i){function r(){return!!navigator.requestMediaKeySystemAccess&&!!MediaKeySystemAccess.prototype.getConfiguration||!!HTMLMediaElement.prototype.webkitGenerateKeyRequest||!!window.MSMediaKeys}function o(t){return!!t.clearkey||!!t.widevine&&e.isChrome()||!!t.playready&&e.isIE()}function a(i,a){var s=t(a);if(!e.isChrome()&&!e.isIETrident(11)&&!e.isFF())return!1;if(!s("dash"))return!1;if(i.drm&&(!s("drm")||!o(i.drm)||!r()))return!1;var l=window.MediaSource;if(!window.HTMLVideoElement||!l)return!1;var c=!0;return i.mediaTypes&&(c=n.all(i.mediaTypes,function(e){return l.isTypeSupported(e)})),c&&("dash"===i.type||"mpd"===i.type||(i.file||"").indexOf("mpd-time-csf")>-1)}var s=n.find(i,n.matches({name:"flash"})),l=s.supports;return s.supports=function(n,i){if(!e.isFlashSupported())return!1;var r=n&&n.type;if("hls"===r||"m3u8"===r){var o=t(i);return o("hls")}return l.apply(this,arguments)},i.push({name:"shaka",supports:a}),i.push({name:"caterpillar",supports:function(n,i){if(e.isChrome()&&!e.isMobile()){var r=n&&n.type,o=n&&n.file;if(o.indexOf(".m3u8")>-1||"hls"===r||"m3u8"===r){var a=t(i);return a("hls")}}}}),i}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,,,,,,,,,,,,,,,,,,,,,,,,function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e,t,n){this.helpers=e||{},this.partials=t||{},this.decorators=n||{},l.registerDefaultHelpers(this),c.registerDefaultDecorators(this)}t.__esModule=!0,t.HandlebarsEnvironment=r;var o=n(20),a=n(39),s=i(a),l=n(248),c=n(246),u=n(256),d=i(u),p="4.0.5";t.VERSION=p;var h=7;t.COMPILER_REVISION=h;var f={1:"<= 1.0.rc.2",2:"== 1.0.0-rc.3",3:"== 1.0.0-rc.4",4:"== 1.x.x",5:"== 2.0.0-alpha.x",6:">= 2.0.0-beta.1",7:">= 4.0.0"};t.REVISION_CHANGES=f;var g="[object Object]";r.prototype={constructor:r,logger:d["default"],log:d["default"].log,registerHelper:function(e,t){if(o.toString.call(e)===g){if(t)throw new s["default"]("Arg not supported with multiple helpers");o.extend(this.helpers,e)}else this.helpers[e]=t},unregisterHelper:function(e){delete this.helpers[e]},registerPartial:function(e,t){if(o.toString.call(e)===g)o.extend(this.partials,e);else{if("undefined"==typeof t)throw new s["default"]('Attempting to register a partial called "'+e+'" as undefined');this.partials[e]=t}},unregisterPartial:function(e){delete this.partials[e]},registerDecorator:function(e,t){if(o.toString.call(e)===g){if(t)throw new s["default"]("Arg not supported with multiple decorators");o.extend(this.decorators,e)}else this.decorators[e]=t},unregisterDecorator:function(e){delete this.decorators[e]}};var m=d["default"].log;t.log=m,t.createFrame=o.createFrame,t.logger=d["default"]},function(e,t,n){var i,r;i=[n(2),n(1)],r=function(e,t){function i(n){t.each(n,function(t,i){n[i]=e.serialize(t)})}function r(e){return e.slice&&"px"===e.slice(-2)&&(e=e.slice(0,-2)),e}function o(t,n){if(-1===n.toString().indexOf("%"))return 0;if("string"!=typeof t||!e.exists(t))return 0;if(/^\d*\.?\d+%$/.test(t))return t;var i=t.indexOf(":");if(-1===i)return 0;var r=parseFloat(t.substr(0,i)),o=parseFloat(t.substr(i+1));return 0>=r||0>=o?0:o/r*100+"%"}var a={autostart:!1,controls:!0,displaytitle:!0,displaydescription:!0,mobilecontrols:!1,repeat:!1,castAvailable:!1,skin:"seven",stretching:"uniform",mute:!1,volume:90,width:480,height:270},s=function(s,l){var c=l&&l.getAllItems(),u=t.extend({},(window.jwplayer||{}).defaults,c,s);i(u);var d=t.extend({},a,u);if("."===d.base&&(d.base=e.getScriptPath("jwplayer.js")),d.base=(d.base||e.loadFrom()).replace(/\/?$/,"/"),n.p=d.base,d.width=r(d.width),d.height=r(d.height),d.flashplayer=d.flashplayer||e.getScriptPath("jwplayer.js")+"jwplayer.flash.swf","http:"===window.location.protocol&&(d.flashplayer=d.flashplayer.replace("https","http")),d.aspectratio=o(d.aspectratio,d.width),t.isObject(d.skin)&&(d.skinUrl=d.skin.url,d.skinColorInactive=d.skin.inactive,d.skinColorActive=d.skin.active,d.skinColorBackground=d.skin.background,d.skin=t.isString(d.skin.name)?d.skin.name:a.skin),t.isString(d.skin)&&d.skin.indexOf(".xml")>0&&(console.log("JW Player does not support XML skins, please update your config"),d.skin=d.skin.replace(".xml","")),d.aspectratio||delete d.aspectratio,!d.playlist){var p=t.pick(d,["title","description","type","mediaid","image","file","sources","tracks","preload"]);d.playlist=[p]}return d};return s}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;
	i=[n(14),n(40),n(276),n(277),n(85)],r=function(e,t,n,i,r){function o(t){for(var o={},s=0;s<t.childNodes.length;s++){var l=t.childNodes[s],u=c(l);if(u)switch(u.toLowerCase()){case"enclosure":o.file=e.xmlAttribute(l,"url");break;case"title":o.title=a(l);break;case"guid":o.mediaid=a(l);break;case"pubdate":o.date=a(l);break;case"description":o.description=a(l);break;case"link":o.link=a(l);break;case"category":o.tags?o.tags+=a(l):o.tags=a(l)}}return o=i(t,o),o=n(t,o),new r(o)}var a=t.textContent,s=t.getChildNode,l=t.numChildren,c=t.localName,u={};return u.parse=function(e){for(var t=[],n=0;n<l(e);n++){var i=s(e,n),r=c(i).toLowerCase();if("channel"===r)for(var a=0;a<l(i);a++){var u=s(i,a);"item"===c(u).toLowerCase()&&t.push(o(u))}}return t},u}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(40),n(120),n(2),n(4),n(3),n(1)],r=function(e,t,n,i,r,o){var a=function(){function a(r){var a=n.tryCatch(function(){var n,a=r.responseXML?r.responseXML.childNodes:null,s="";if(a){for(var u=0;u<a.length&&(s=a[u],8===s.nodeType);u++);"xml"===e.localName(s)&&(s=s.nextSibling),"rss"===e.localName(s)&&(n={playlist:t.parse(s)})}if(!n)try{var d=JSON.parse(r.responseText);if(o.isArray(d))n={playlist:d};else{if(!o.isArray(d.playlist))throw null;n=d}}catch(p){return void l("Not a valid RSS/JSON feed")}c.trigger(i.JWPLAYER_PLAYLIST_LOADED,n)});a instanceof n.Error&&l()}function s(e){l("Playlist load error: "+e)}function l(e){c.trigger(i.JWPLAYER_ERROR,{message:e?e:"Error loading file"})}var c=o.extend(this,r);this.load=function(e){n.ajax(e,a,s)},this.destroy=function(){this.off()}};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(85),n(123),n(1),n(33)],r=function(e,t,n,i){function r(e,t){for(var n=0;n<e.length;n++){var i=e[n],r=t.choose(i);if(r)return i.type}return null}var o=function(t){return t=n.isArray(t)?t:[t],n.compact(n.map(t,e))};o.filterPlaylist=function(e,t,i,r,o,l){var c=[];return n.each(e,function(e){e=n.extend({},e),e.allSources=a(e.sources,i,e.drm||r,e.preload||o),e.sources=s(e.allSources,t),e.sources.length&&(e.file=e.sources[0].file,(e.preload||o)&&(e.preload=e.preload||o),(e.feedid||l)&&(e.feedid=e.feedid||l),c.push(e))}),c};var a=function(e,i,r,o){return n.compact(n.map(e,function(e){return n.isObject(e)?(void 0!==i&&null!==i&&(e.androidhls=i),(e.drm||r)&&(e.drm=e.drm||r),(e.preload||o)&&(e.preload=e.preload||o),t(e)):void 0}))},s=function(e,t){t&&t.choose||(t=new i({primary:t?"flash":null}));var o=r(e,t);return n.where(e,{type:o})};return o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(14),n(1)],r=function(e,t,n){var i={"default":!1},r=function(r){if(r&&r.file){var o=n.extend({},i,r);o.file=t.trim(""+o.file);var a=/^[^\/]+\/(?:x-)?([^\/]+)$/;if(e.isYouTube(o.file)?o.type="youtube":e.isRtmp(o.file)?o.type="rtmp":a.test(o.type)?o.type=o.type.replace(a,"$1"):o.type||(o.type=t.extension(o.file)),o.type){switch(o.type){case"m3u8":case"vnd.apple.mpegurl":o.type="hls";break;case"dash+xml":o.type="dash";break;case"smil":o.type="rtmp";break;case"m4a":o.type="aac"}return n.each(o,function(e,t){""===e&&delete o[t]}),o}}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(56),n(4),n(3),n(32),n(1)],r=function(e,t,n,i,r,o){var a={FLASH:0,JAVASCRIPT:1,HYBRID:2},s=function(s){function l(){switch(t.getPluginPathType(s)){case t.pluginPathType.ABSOLUTE:return s;case t.pluginPathType.RELATIVE:return e.getAbsolutePath(s,window.location.href)}}function c(){o.defer(function(){m=r.loaderstatus.COMPLETE,g.trigger(n.COMPLETE)})}function u(){m=r.loaderstatus.ERROR,g.trigger(n.ERROR,{url:s})}var d,p,h,f,g=o.extend(this,i),m=r.loaderstatus.NEW;this.load=function(){if(m===r.loaderstatus.NEW){if(s.lastIndexOf(".swf")>0)return d=s,m=r.loaderstatus.COMPLETE,void g.trigger(n.COMPLETE);if(t.getPluginPathType(s)===t.pluginPathType.CDN)return m=r.loaderstatus.COMPLETE,void g.trigger(n.COMPLETE);m=r.loaderstatus.LOADING;var e=new r(l());e.on(n.COMPLETE,c),e.on(n.ERROR,u),e.load()}},this.registerPlugin=function(e,t,i,o){f&&(clearTimeout(f),f=void 0),h=t,i&&o?(d=o,p=i):"string"==typeof i?d=i:"function"==typeof i?p=i:i||o||(d=e),m=r.loaderstatus.COMPLETE,g.trigger(n.COMPLETE)},this.getStatus=function(){return m},this.getPluginName=function(){return t.getPluginName(s)},this.getFlashPath=function(){if(d)switch(t.getPluginPathType(d)){case t.pluginPathType.ABSOLUTE:return d;case t.pluginPathType.RELATIVE:return s.lastIndexOf(".swf")>0?e.getAbsolutePath(d,window.location.href):e.getAbsolutePath(d,l())}return null},this.getJS=function(){return p},this.getTarget=function(){return h},this.getPluginmode=function(){return void 0!==typeof d&&void 0!==typeof p?a.HYBRID:void 0!==typeof d?a.FLASH:void 0!==typeof p?a.JAVASCRIPT:void 0},this.getNewInstance=function(e,t,n){return new p(e,t,n)},this.getURL=function(){return s}};return s}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,function(e,t,n){var i,r;i=[n(1)],r=function(e){function t(e){return function(){return i(e)}}var n={},i=e.memoize(function(e){var t=navigator.userAgent.toLowerCase();return null!==t.match(e)}),r=n.isInt=function(e){return parseFloat(e)%1===0};n.isFlashSupported=function(){var e=n.flashVersion();return e&&e>=11.2},n.isFF=t(/firefox/i),n.isIPod=t(/iP(hone|od)/i),n.isIPad=t(/iPad/i),n.isSafari602=t(/Macintosh.*Mac OS X 10_8.*6\.0\.\d* Safari/i),n.isOSX=t(/Mac OS X/i),n.isEdge=t(/\sedge\/\d+/i);var o=n.isIETrident=function(e){return n.isEdge()?!0:e?(e=parseFloat(e).toFixed(1),i(new RegExp("trident/.+rv:\\s*"+e,"i"))):i(/trident/i)},a=n.isMSIE=function(e){return e?(e=parseFloat(e).toFixed(1),i(new RegExp("msie\\s*"+e,"i"))):i(/msie/i)},s=t(/chrome/i);n.isChrome=function(){return s()&&!n.isEdge()},n.isIE=function(e){return e?(e=parseFloat(e).toFixed(1),e>=11?o(e):a(e)):a()||o()},n.isSafari=function(){return i(/safari/i)&&!i(/chrome/i)&&!i(/chromium/i)&&!i(/android/i)};var l=n.isIOS=function(e){return i(e?new RegExp("iP(hone|ad|od).+\\s(OS\\s"+e+"|.*\\sVersion/"+e+")","i"):/iP(hone|ad|od)/i)};n.isAndroidNative=function(e){return c(e,!0)};var c=n.isAndroid=function(e,t){return t&&i(/chrome\/[123456789]/i)&&!i(/chrome\/18/)?!1:e?(r(e)&&!/\./.test(e)&&(e=""+e+"."),i(new RegExp("Android\\s*"+e,"i"))):i(/Android/i)};return n.isMobile=function(){return l()||c()},n.isIframe=function(){return window.frameElement&&"IFRAME"===window.frameElement.nodeName},n.flashVersion=function(){if(n.isAndroid())return 0;var e,t=navigator.plugins;if(t&&(e=t["Shockwave Flash"],e&&e.description))return parseFloat(e.description.replace(/\D+(\d+\.?\d*).*/,"$1"));if("undefined"!=typeof window.ActiveXObject){try{if(e=new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash"))return parseFloat(e.GetVariable("$version").split(" ")[1].replace(/\s*,\s*/,"."))}catch(i){return 0}return e}return 0},n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(14),n(1),n(286)],r=function(e,t,n){var i={};i.createElement=function(e){var t=document.createElement("div");return t.innerHTML=e,t.firstChild},i.styleDimension=function(e){return e+(e.toString().indexOf("%")>0?"":"px")};var r=function(e){return t.isString(e.className)?e.className.split(" "):[]},o=function(t,n){n=e.trim(n),t.className!==n&&(t.className=n)};return i.classList=function(e){return e.classList?e.classList:r(e)},i.hasClass=n.hasClass,i.addClass=function(e,n){var i=r(e),a=t.isArray(n)?n:n.split(" ");t.each(a,function(e){t.contains(i,e)||i.push(e)}),o(e,i.join(" "))},i.removeClass=function(e,n){var i=r(e),a=t.isArray(n)?n:n.split(" ");o(e,t.difference(i,a).join(" "))},i.replaceClass=function(e,t,n){var i=e.className||"";t.test(i)?i=i.replace(t,n):n&&(i+=" "+n),o(e,i)},i.toggleClass=function(e,n,r){var o=i.hasClass(e,n);r=t.isBoolean(r)?r:!o,r!==o&&(r?i.addClass(e,n):i.removeClass(e,n))},i.emptyElement=function(e){for(;e.firstChild;)e.removeChild(e.firstChild)},i.addStyleSheet=function(e){var t=document.createElement("link");t.rel="stylesheet",t.href=e,document.getElementsByTagName("head")[0].appendChild(t)},i.empty=function(e){if(e)for(;e.childElementCount>0;)e.removeChild(e.children[0])},i.bounds=function(e){var t={left:0,right:0,width:0,height:0,top:0,bottom:0};if(!e||!document.body.contains(e))return t;var n=e.getBoundingClientRect(e),i=window.pageYOffset,r=window.pageXOffset;return n.width||n.height||n.left||n.top?(t.left=n.left+r,t.right=n.right+r,t.top=n.top+i,t.bottom=n.bottom+i,t.width=n.right-n.left,t.height=n.bottom-n.top,t):t},i}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(3),n(1)],r=function(e,t){function n(){}var i=function(e,n){var i,r=this;i=e&&t.has(e,"constructor")?e.constructor:function(){return r.apply(this,arguments)},t.extend(i,r,n);var o=function(){this.constructor=i};return o.prototype=r.prototype,i.prototype=new o,e&&t.extend(i.prototype,e),i.__super__=r.prototype,i};return n.extend=i,t.extend(n.prototype,e),n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(44),n(1),n(87),n(86),n(46)],r=function(e,t,n,i,r){var o={};return o.repo=t.memoize(function(){var t=r.split("+")[0],i=e.repo+t+"/";return n.isHTTPS()?i.replace(/^http:/,"https:"):i}),o.versionCheck=function(e){var t=("0"+e).split(/\W/),n=r.split(/\W/),i=parseFloat(t[0]),o=parseFloat(n[0]);return i>o?!1:!(i===o&&parseFloat("0"+t[1])>parseFloat(n[1]))},o.loadFrom=function(){return o.repo()},o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t=function(){var t={},n={},i={},r={};return{start:function(n){t[n]=e.now(),i[n]=i[n]+1||1},end:function(i){if(t[i]){var r=e.now()-t[i];n[i]=n[i]+r||r}},dump:function(){return{counts:i,sums:n,events:r}},tick:function(t,n){r[t]=n||e.now()},between:function(e,t){return r[t]&&r[e]?r[t]-r[e]:-1}}};return t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e=function(e,n,i){if(n=n||this,i=i||[],window.jwplayer&&window.jwplayer.debug)return e.apply(n,i);try{return e.apply(n,i)}catch(r){return new t(e.name,r)}},t=function(e,t){this.name=e,this.message=t.message||t.toString(),this.error=t};return{tryCatch:e,Error:t}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return document.createElement("video")}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i,r;i=[n(264),n(41),n(1)],r=function(e,t,n){var i=e.selectPlayer,r=function(){var e=i.apply(this,arguments);return e?e:{registerPlugin:function(e,n,i){"jwpsrv"!==e&&t.registerPlugin(e,n,i)}}};return n.extend(e,{selectPlayer:r})}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,function(e,t,n){var i,r;i=[n(2),n(140),n(48)],r=function(e,t,n){var i="invalid",r="RnXcsftYjWRDA^Uy",o=function(o){function a(o){e.exists(o)||(o="");try{o=t.decrypt(o,r);var a=o.split("/");s=a[0],"pro"===s&&(s="premium");var u=n(s);if(a.length>2&&u("setup")){l=a[1];var d=parseInt(a[2]);d>0&&(c=new Date,c.setTime(d))}else s=i}catch(p){s=i}}var s,l,c;this.edition=function(){return c&&c.getTime()<(new Date).getTime()?i:s},this.token=function(){return l},this.expiration=function(){return c},a(o)};return o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e=function(e){return window.atob(e)},t=function(e){return unescape(encodeURIComponent(e))},n=function(e){try{return decodeURIComponent(escape(e))}catch(t){return e}},i=function(e){for(var t=new Array(Math.ceil(e.length/4)),n=0;n<t.length;n++)t[n]=e.charCodeAt(4*n)+(e.charCodeAt(4*n+1)<<8)+(e.charCodeAt(4*n+2)<<16)+(e.charCodeAt(4*n+3)<<24);return t},r=function(e){for(var t=new Array(e.length),n=0;n<e.length;n++)t[n]=String.fromCharCode(255&e[n],e[n]>>>8&255,e[n]>>>16&255,e[n]>>>24&255);return t.join("")};return{decrypt:function(o,a){if(o=String(o),a=String(a),0==o.length)return"";for(var s,l,c=i(e(o)),u=i(t(a).slice(0,16)),d=c.length,p=c[d-1],h=c[0],f=2654435769,g=Math.floor(6+52/d),m=g*f;0!=m;){l=m>>>2&3;for(var v=d-1;v>=0;v--)p=c[v>0?v-1:d-1],s=(p>>>5^h<<2)+(h>>>3^p<<4)^(m^h)+(u[3&v^l]^p),h=c[v]-=s;m-=f}var w=r(c);return w=w.replace(/\0+$/,""),n(w)}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(305),n(4),n(321)],r=function(e,t,n){var i=function(i,r){var o=new e(i,r),a=o.setup;return o.setup=function(){if(a.call(this),"trial"===r.get("edition")){var e=document.createElement("div");e.className="jw-icon jw-watermark",this.element().appendChild(e)}r.on("change:skipButton",this.onSkipButton,this),r.on("change:castActive change:playlistItem",this.showDisplayIconImage,this)},o.showDisplayIconImage=function(e){var t=e.get("castActive"),n=e.get("playlistItem"),i=o.controlsContainer().getElementsByClassName("jw-display-icon-container")[0];t&&n&&n.image?(i.style.backgroundImage='url("'+n.image+'")',i.style.backgroundSize="contain"):(i.style.backgroundImage="",i.style.backgroundSize="")},o.onSkipButton=function(e,t){t?this.addSkipButton():this._skipButton&&(this._skipButton.destroy(),this._skipButton=null)},o.addSkipButton=function(){this._skipButton=new n(this.instreamModel),this._skipButton.on(t.JWPLAYER_AD_SKIPPED,function(){this.api.skipAd()},this),this.controlsContainer().appendChild(this._skipButton.element())},o};return i}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,function(e,t,n){t=e.exports=n(231)(),t.push([e.id,".jw-reset{color:inherit;background-color:transparent;padding:0;margin:0;float:none;font-family:Arial,Helvetica,sans-serif;font-size:1em;line-height:1em;list-style:none;text-align:left;text-transform:none;vertical-align:baseline;border:0;direction:ltr;font-variant:inherit;font-stretch:inherit;-webkit-tap-highlight-color:rgba(255,255,255,0)}@font-face{font-family:jw-icons;src:url("+n(233)+") format('woff'),url("+n(232)+') format(\'truetype\');font-weight:400;font-style:normal}.jw-controlbar .jw-menu .jw-option:before,.jw-icon-display,.jw-icon-inline,.jw-icon-tooltip{font-family:jw-icons;-webkit-font-smoothing:antialiased;font-style:normal;font-weight:400;text-transform:none;background-color:transparent;font-variant:normal;-webkit-font-feature-settings:"liga";-ms-font-feature-settings:"liga" 1;-o-font-feature-settings:"liga";font-feature-settings:"liga";-moz-osx-font-smoothing:grayscale}.jw-icon-audio-tracks:before{content:"\\E600"}.jw-icon-buffer:before{content:"\\E601"}.jw-icon-cast:before{content:"\\E603"}.jw-icon-cast.jw-off:before{content:"\\E602"}.jw-icon-cc:before{content:"\\E605"}.jw-icon-cue:before,.jw-icon-menu-bullet:before{content:"\\E606"}.jw-icon-error:before{content:"\\E607"}.jw-icon-fullscreen:before{content:"\\E608"}.jw-icon-fullscreen.jw-off:before{content:"\\E613"}.jw-icon-hd:before{content:"\\E60A"}.jw-rightclick-logo:before,.jw-watermark:before{content:"\\E60B"}.jw-icon-next:before{content:"\\E60C"}.jw-icon-pause:before{content:"\\E60D"}.jw-icon-play:before{content:"\\E60E"}.jw-icon-prev:before{content:"\\E60F"}.jw-icon-replay:before{content:"\\E610"}.jw-icon-volume:before{content:"\\E612"}.jw-icon-volume.jw-off:before{content:"\\E611"}.jw-icon-more:before{content:"\\E614"}.jw-icon-close:before{content:"\\E615"}.jw-icon-playlist:before{content:"\\E616"}.jwplayer{width:100%;font-size:16px;position:relative;display:block;min-height:0;overflow:hidden;box-sizing:border-box;font-family:Arial,Helvetica,sans-serif;background-color:#000;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.jwplayer *{box-sizing:inherit}.jwplayer.jw-flag-aspect-mode{height:auto!important}.jwplayer.jw-flag-aspect-mode .jw-aspect{display:block}.jwplayer .jw-aspect{display:none}.jwplayer.jw-no-focus:focus,.jwplayer .jw-swf{outline:none}.jwplayer.jw-ie:focus{outline:1px dotted #585858}.jwplayer:hover .jw-display-icon-container{background-color:#333;background:#333;background-size:#333}.jw-controls,.jw-media,.jw-overlays,.jw-preview{position:absolute;width:100%;height:100%;top:0;left:0;bottom:0;right:0}.jw-media{overflow:hidden;cursor:pointer}.jw-overlays{cursor:auto}.jw-media.jw-media-show{visibility:visible;opacity:1}.jw-controls.jw-controls-disabled{display:none}.jw-controls .jw-controls-right{position:absolute;top:0;right:0;left:0;bottom:2em}.jw-text{height:1em;font-family:Arial,Helvetica,sans-serif;font-size:.75em;font-style:normal;font-weight:400;color:#fff;text-align:center;font-variant:normal;font-stretch:normal}.jw-plugin{position:absolute;bottom:2.5em}.jw-plugin .jw-banner{max-width:100%;opacity:0;cursor:pointer;position:absolute;margin:auto auto 0;left:0;right:0;bottom:0;display:block}.jw-cast-screen{width:100%;height:100%}.jw-instream{position:absolute;top:0;right:0;bottom:0;left:0;display:none}.jw-icon-playback:before{content:"\\E60E"}.jw-captions,.jw-controls,.jw-overlays,.jw-preview,.jw-title{pointer-events:none}.jw-controlbar,.jw-display-icon-container,.jw-dock,.jw-logo,.jw-media,.jw-overlays>div,.jw-skip{pointer-events:all}.jwplayer video{position:absolute;top:0;right:0;bottom:0;left:0;width:100%;height:100%;margin:auto;background:transparent}.jwplayer video::-webkit-media-controls-start-playback-button{display:none}.jwplayer.jw-stretch-uniform video{-o-object-fit:contain;object-fit:contain}.jwplayer.jw-stretch-none video{-o-object-fit:none;object-fit:none}.jwplayer.jw-stretch-fill video{-o-object-fit:cover;object-fit:cover}.jwplayer.jw-stretch-exactfit video{-o-object-fit:fill;object-fit:fill}.jw-click,.jw-preview{position:absolute;width:100%;height:100%}.jw-preview{display:none;opacity:1;visibility:visible;background:#000 no-repeat 50% 50%}.jw-error .jw-preview,.jw-stretch-uniform .jw-preview,.jwplayer .jw-preview{background-size:contain}.jw-stretch-none .jw-preview{background-size:auto auto}.jw-stretch-fill .jw-preview{background-size:cover}.jw-stretch-exactfit .jw-preview{background-size:100% 100%}.jw-display-icon-container{position:relative;top:50%;display:table;height:3.5em;width:3.5em;margin:-1.75em auto 0;cursor:pointer}.jw-display-icon-container .jw-icon-display{position:relative;display:table-cell;text-align:center;vertical-align:middle!important;background-position:50% 50%;background-repeat:no-repeat;font-size:2em}.jw-flag-audio-player .jw-display-icon-container,.jw-flag-dragging .jw-display-icon-container{display:none}.jw-icon{font-family:jw-icons;-webkit-font-smoothing:antialiased;font-style:normal;font-weight:400;text-transform:none;background-color:transparent;font-variant:normal;-webkit-font-feature-settings:"liga";-ms-font-feature-settings:"liga" 1;-o-font-feature-settings:"liga";font-feature-settings:"liga";-moz-osx-font-smoothing:grayscale}.jw-controlbar{display:table;position:absolute;right:0;left:0;bottom:0;height:2em;padding:0 .25em}.jw-controlbar .jw-hidden{display:none}.jw-controlbar.jw-drawer-expanded .jw-controlbar-center-group,.jw-controlbar.jw-drawer-expanded .jw-controlbar-left-group{opacity:0}.jw-background-color{background-color:#414040}.jw-group{display:table-cell}.jw-controlbar-center-group{width:100%;padding:0 .25em}.jw-controlbar-center-group .jw-slider-time,.jw-controlbar-center-group .jw-text-alt{padding:0}.jw-controlbar-center-group .jw-text-alt{display:none}.jw-controlbar-left-group,.jw-controlbar-right-group{white-space:nowrap}.jw-icon-display:hover,.jw-icon-inline:hover,.jw-icon-tooltip:hover,.jw-knob:hover,.jw-option:before:hover{color:#eee}.jw-icon-inline,.jw-icon-tooltip,.jw-slider-horizontal,.jw-text-duration,.jw-text-elapsed{display:inline-block;height:2em;position:relative;line-height:2em;vertical-align:middle;cursor:pointer}.jw-icon-inline,.jw-icon-tooltip{min-width:1.25em;text-align:center}.jw-icon-playback{min-width:2.25em}.jw-icon-volume{min-width:1.75em;text-align:left}.jw-time-tip{line-height:1em;pointer-events:none}.jw-icon-inline:after,.jw-icon-tooltip:after{width:100%;height:100%;font-size:1em}.jw-icon-cast,.jw-icon-inline.jw-icon-volume,.jw-slider-volume.jw-slider-horizontal{display:none}.jw-dock{margin:.75em;display:block;opacity:1;clear:right}.jw-dock:after{content:\'\';clear:both;display:block}.jw-dock-button{cursor:pointer;float:right;position:relative;width:2.5em;height:2.5em;margin:.5em}.jw-dock-button .jw-arrow{display:none;position:absolute;bottom:-.2em;width:.5em;height:.2em;left:50%;margin-left:-.25em}.jw-dock-button .jw-overlay{display:none;position:absolute;top:2.5em;right:0;margin-top:.25em;padding:.5em;white-space:nowrap}.jw-dock-button:hover .jw-arrow,.jw-dock-button:hover .jw-overlay{display:block}.jw-dock-image{width:100%;height:100%;background-position:50% 50%;background-repeat:no-repeat;opacity:.75}.jw-title{display:none;position:absolute;top:0;width:100%;font-size:.875em;height:8em;background:-webkit-linear-gradient(top,#000,#000 18%,transparent);background:linear-gradient(180deg,#000 0,#000 18%,transparent)}.jw-title-primary,.jw-title-secondary{padding:.75em 1.5em;min-height:2.5em;width:100%;color:#fff;white-space:nowrap;text-overflow:ellipsis;overflow-x:hidden}.jw-title-primary{font-weight:700}.jw-title-secondary{margin-top:-.5em}.jw-slider-container{display:inline-block;height:1em;position:relative;touch-action:none}.jw-buffer,.jw-progress,.jw-rail{position:absolute;cursor:pointer}.jw-progress{background-color:#fff}.jw-rail{background-color:#aaa}.jw-buffer{background-color:#202020}.jw-cue,.jw-knob{position:absolute;cursor:pointer}.jw-cue{background-color:#fff;width:.1em;height:.4em}.jw-knob{background-color:#aaa;width:.4em;height:.4em}.jw-slider-horizontal{width:4em;height:1em}.jw-slider-horizontal.jw-slider-volume{margin-right:5px}.jw-slider-horizontal .jw-buffer,.jw-slider-horizontal .jw-progress,.jw-slider-horizontal .jw-rail{width:100%;height:.4em}.jw-slider-horizontal .jw-buffer,.jw-slider-horizontal .jw-progress{width:0}.jw-slider-horizontal .jw-progress,.jw-slider-horizontal .jw-rail,.jw-slider-horizontal .jw-slider-container{width:100%}.jw-slider-horizontal .jw-knob{left:0;margin-left:-.325em}.jw-slider-vertical{width:.75em;height:4em;bottom:0;position:absolute;padding:1em}.jw-slider-vertical .jw-buffer,.jw-slider-vertical .jw-progress,.jw-slider-vertical .jw-rail{bottom:0;height:100%}.jw-slider-vertical .jw-buffer,.jw-slider-vertical .jw-progress{height:0}.jw-slider-vertical .jw-progress,.jw-slider-vertical .jw-rail,.jw-slider-vertical .jw-slider-container{bottom:0;width:.75em;height:100%;left:0;right:0;margin:0 auto}.jw-slider-vertical .jw-slider-container{height:4em;position:relative}.jw-slider-vertical .jw-knob{bottom:0;left:0;right:0;margin:0 auto}.jw-slider-time{right:0;left:0;width:100%}.jw-tooltip-time{position:absolute}.jw-slider-volume .jw-buffer{display:none}.jw-captions{position:absolute;display:none;margin:0 auto;width:100%;left:0;bottom:3em;right:0;max-width:90%;text-align:center}.jw-captions.jw-captions-enabled{display:block}.jw-captions-window{display:none;padding:.25em;border-radius:.25em}.jw-captions-text,.jw-captions-window.jw-captions-window-active{display:inline-block}.jw-captions-text{color:#fff;background-color:#000;word-wrap:break-word;white-space:pre-line;font-style:normal;font-weight:400;text-align:center;text-decoration:none;line-height:1.3em;padding:.1em .8em}.jwplayer video::-webkit-media-controls{-webkit-box-pack:start;justify-content:flex-start}.jwplayer video::-webkit-media-text-track-container{max-height:84.5%;line-height:1.3em}.jwplayer.jw-flag-compact-player video::-webkit-media-text-track-container{max-height:82%}.jwplayer .jw-rightclick{display:none;position:absolute;white-space:nowrap}.jwplayer .jw-rightclick.jw-open{display:block}.jwplayer .jw-rightclick ul{list-style:none;font-weight:700;border-radius:.15em;margin:0;border:1px solid #444;padding:0}.jwplayer .jw-rightclick ul li{background-color:#000;border-bottom:1px solid #444;margin:0}.jwplayer .jw-rightclick ul li .jw-rightclick-logo{font-size:2em;color:#ff0147;vertical-align:middle;padding-right:.3em;margin-right:.3em;border-right:1px solid #444}.jwplayer .jw-rightclick ul li a{color:#fff;text-decoration:none;padding:1em;display:block;font-size:.6875em;line-height:1em}.jwplayer .jw-rightclick ul li:last-child{border-bottom:none}.jwplayer .jw-rightclick ul li:hover{background-color:#1a1a1a;cursor:pointer}.jwplayer .jw-rightclick ul .jw-featured{background-color:#252525;vertical-align:middle}.jwplayer .jw-rightclick ul .jw-featured a{color:#777}.jw-logo{position:absolute;margin:.75em;cursor:pointer;pointer-events:all;background-repeat:no-repeat;background-size:contain;top:auto;right:auto;left:auto;bottom:auto}.jw-logo .jw-flag-audio-player{display:none}.jw-logo-top-right{top:0;right:0}.jw-logo-top-left{top:0;left:0}.jw-logo-bottom-left{bottom:0;left:0}.jw-logo-bottom-right,.jw-watermark{bottom:0;right:0}.jw-watermark{position:absolute;top:50%;left:0;text-align:center;font-size:14em;color:#eee;opacity:.33;pointer-events:none}.jw-icon-tooltip.jw-open .jw-overlay{opacity:1;visibility:visible}.jw-icon-tooltip.jw-hidden,.jw-icon-tooltip.jw-open-drawer:before,.jw-overlay-horizontal{display:none}.jw-icon-tooltip.jw-open-drawer .jw-overlay-horizontal{opacity:1;display:inline-block;vertical-align:top}.jw-overlay:before{position:absolute;top:0;bottom:0;left:-50%;width:100%;background-color:transparent;content:" "}.jw-slider-time .jw-overlay:before{height:1em;top:auto}.jw-menu,.jw-time-tip,.jw-volume-tip{position:relative;left:-50%;border:1px solid #000;margin:0}.jw-volume-tip{width:100%;height:100%;display:block}.jw-time-tip{text-align:center;font-family:inherit;color:#aaa;bottom:1em;border:4px solid #000}.jw-time-tip .jw-text{line-height:1em}.jw-controlbar .jw-overlay{margin:0;position:absolute;bottom:2em;left:50%;opacity:0;visibility:hidden}.jw-controlbar .jw-overlay .jw-contents{position:relative}.jw-controlbar .jw-option{position:relative;white-space:nowrap;cursor:pointer;list-style:none;height:1.5em;font-family:inherit;line-height:1.5em;color:#aaa;padding:0 .5em;font-size:.8em}.jw-controlbar .jw-option:before:hover,.jw-controlbar .jw-option:hover{color:#eee}.jw-controlbar .jw-option:before{padding-right:.125em}.jw-playlist-container ::-webkit-scrollbar-track{background-color:#333;border-radius:10px}.jw-playlist-container ::-webkit-scrollbar{width:5px;border:10px solid #000;border-bottom:0;border-top:0}.jw-playlist-container ::-webkit-scrollbar-thumb{background-color:#fff;border-radius:5px}.jw-tooltip-title{border-bottom:1px solid #444;text-align:left;padding-left:.7em}.jw-playlist{max-height:11em;min-height:4.5em;overflow-x:hidden;overflow-y:scroll;width:calc(100% - 4px)}.jw-playlist .jw-option{height:3em;margin-right:5px;color:#fff;padding-left:1em;font-size:.8em}.jw-playlist .jw-label,.jw-playlist .jw-name{display:inline-block;line-height:3em;text-align:left;overflow:hidden;white-space:nowrap}.jw-playlist .jw-label{width:1em}.jw-playlist .jw-name{width:11em}.jw-skip{cursor:default;position:absolute;float:right;display:inline-block;right:.75em;bottom:3em}.jw-skip.jw-skippable{cursor:pointer}.jw-skip.jw-hidden{visibility:hidden}.jw-skip .jw-skip-icon{display:none;margin-left:-.75em}.jw-skip .jw-skip-icon:before{content:"\\E60C"}.jw-skip .jw-skip-icon,.jw-skip .jw-text{color:#aaa;vertical-align:middle;line-height:1.5em;font-size:.7em}.jw-skip.jw-skippable:hover{cursor:pointer}.jw-skip.jw-skippable:hover .jw-skip-icon,.jw-skip.jw-skippable:hover .jw-text{color:#eee}.jw-skip.jw-skippable .jw-skip-icon{display:inline;margin:0}.jwplayer.jw-state-paused.jw-flag-casting .jw-display-icon-container,.jwplayer.jw-state-playing.jw-flag-casting .jw-display-icon-container{display:table}.jwplayer.jw-flag-casting .jw-display-icon-container{border-radius:0;border:1px solid #fff;position:absolute;top:auto;left:.5em;right:.5em;bottom:50%;margin-bottom:-12.5%;height:50%;width:50%;padding:0;background-repeat:no-repeat;background-position:50%}.jwplayer.jw-flag-casting .jw-display-icon-container .jw-icon{font-size:3em}.jwplayer.jw-flag-casting.jw-state-complete .jw-preview{display:none}.jw-cast{position:absolute;width:100%;height:100%;background-repeat:no-repeat;background-size:auto;background-position:50% 50%}.jw-cast-label{position:absolute;left:.5em;right:.5em;bottom:75%;margin-bottom:1.5em;text-align:center}.jw-cast-name{color:#ccc}.jw-state-idle .jw-preview{display:block}.jw-state-idle .jw-icon-display:before{content:"\\E60E"}.jw-state-idle .jw-captions,.jw-state-idle .jw-controlbar{display:none}.jw-state-idle .jw-title{display:block}.jwplayer.jw-state-playing .jw-display-icon-container{display:none}.jwplayer.jw-state-playing .jw-display-icon-container .jw-icon-display:before,.jwplayer.jw-state-playing .jw-icon-playback:before{content:"\\E60D"}.jwplayer.jw-state-paused .jw-display-icon-container{display:none}.jwplayer.jw-state-paused .jw-display-icon-container .jw-icon-display:before,.jwplayer.jw-state-paused .jw-icon-playback:before{content:"\\E60E"}.jwplayer.jw-state-buffering .jw-display-icon-container .jw-icon-display{-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite}.jwplayer.jw-state-buffering .jw-display-icon-container .jw-icon-display:before{content:"\\E601"}@-webkit-keyframes spin{to{-webkit-transform:rotate(1turn)}}@keyframes spin{to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.jwplayer.jw-state-buffering .jw-display-icon-container .jw-text{display:none}.jwplayer.jw-state-buffering .jw-icon-playback:before{content:"\\E60D"}.jwplayer.jw-state-complete .jw-preview{display:block}.jwplayer.jw-state-complete .jw-display-icon-container .jw-icon-display:before{content:"\\E610"}.jwplayer.jw-state-complete .jw-display-icon-container .jw-text{display:none}.jwplayer.jw-state-complete .jw-icon-playback:before{content:"\\E60E"}.jwplayer.jw-state-complete .jw-captions{display:none}.jwplayer.jw-state-error .jw-title,body .jw-error .jw-title{display:block}.jwplayer.jw-state-error .jw-title .jw-title-primary,body .jw-error .jw-title .jw-title-primary{white-space:normal}.jwplayer.jw-state-error .jw-preview,body .jw-error .jw-preview{display:block}.jwplayer.jw-state-error .jw-captions,.jwplayer.jw-state-error .jw-controlbar,body .jw-error .jw-captions,body .jw-error .jw-controlbar{display:none}.jwplayer.jw-state-error:hover .jw-display-icon-container,body .jw-error:hover .jw-display-icon-container{cursor:default;color:#fff;background:#000}.jwplayer.jw-state-error .jw-icon-display,body .jw-error .jw-icon-display{cursor:default;font-family:jw-icons;-webkit-font-smoothing:antialiased;font-style:normal;font-weight:400;text-transform:none;background-color:transparent;font-variant:normal;-webkit-font-feature-settings:"liga";-ms-font-feature-settings:"liga" 1;-o-font-feature-settings:"liga";font-feature-settings:"liga";-moz-osx-font-smoothing:grayscale}.jwplayer.jw-state-error .jw-icon-display:before,body .jw-error .jw-icon-display:before{content:"\\E607"}.jwplayer.jw-state-error .jw-icon-display:hover,body .jw-error .jw-icon-display:hover{color:#fff}body .jw-error{font-size:16px;background-color:#000;color:#eee;width:100%;height:100%;display:table;opacity:1;position:relative}body .jw-error .jw-icon-container{position:absolute;width:100%;height:100%;top:0;left:0;bottom:0;right:0}.jwplayer.jw-flag-cast-available .jw-controlbar{display:table}.jwplayer.jw-flag-cast-available .jw-icon-cast{display:inline-block}.jwplayer.jw-flag-skin-loading .jw-captions,.jwplayer.jw-flag-skin-loading .jw-controls,.jwplayer.jw-flag-skin-loading .jw-title{display:none}.jwplayer.jw-flag-fullscreen{width:100%!important;height:100%!important;top:0;right:0;bottom:0;left:0;z-index:1000;margin:0;position:fixed}.jwplayer.jw-flag-live .jw-controlbar .jw-slider-time,.jwplayer.jw-flag-live .jw-controlbar .jw-text-duration,.jwplayer.jw-flag-live .jw-controlbar .jw-text-elapsed{display:none}.jwplayer.jw-flag-live .jw-controlbar .jw-text-alt{display:inline}.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-controlbar,.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-dock,.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-logo.jw-hide{display:none}.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-captions,.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-plugin{bottom:.5em}.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-media{cursor:none;-webkit-cursor-visibility:auto-hide}.jwplayer.jw-flag-user-inactive.jw-state-playing video::-webkit-media-text-track-container{max-height:none}.jwplayer.jw-flag-user-inactive.jw-state-buffering .jw-controlbar{display:none}.jwplayer.jw-flag-media-audio .jw-controlbar,.jwplayer.jw-flag-media-audio.jw-flag-user-inactive .jw-controlbar{display:table}.jwplayer.jw-flag-media-audio.jw-flag-user-inactive.jw-state-playing .jw-captions,.jwplayer.jw-flag-media-audio.jw-flag-user-inactive.jw-state-playing .jw-plugin{bottom:3em}.jwplayer.jw-flag-media-audio.jw-flag-user-inactive.jw-state-playing video::-webkit-media-text-track-container{max-height:84.5%}.jw-flag-media-audio .jw-preview{display:block}.jwplayer.jw-flag-ads .jw-captions.jw-captions-enabled,.jwplayer.jw-flag-ads .jw-dock,.jwplayer.jw-flag-ads .jw-logo,.jwplayer.jw-flag-ads .jw-preview{display:none}.jwplayer.jw-flag-ads video::-webkit-media-text-track-container{display:none}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-inline,.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-tooltip,.jwplayer.jw-flag-ads .jw-controlbar .jw-slider-horizontal,.jwplayer.jw-flag-ads .jw-controlbar .jw-text{display:none}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-fullscreen,.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-playback,.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-volume,.jwplayer.jw-flag-ads .jw-controlbar .jw-slider-volume{display:inline-block}.jwplayer.jw-flag-ads .jw-controlbar .jw-text-alt{display:inline}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-inline.jw-icon-volume,.jwplayer.jw-flag-ads .jw-controlbar .jw-slider-volume.jw-slider-horizontal{display:inline-block}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-tooltip.jw-icon-volume{display:none}.jwplayer.jw-flag-ads-googleima .jw-controlbar{display:table;bottom:0}.jwplayer.jw-flag-ads-googleima.jw-flag-touch .jw-controlbar{font-size:1em}.jwplayer.jw-flag-ads-googleima.jw-flag-touch.jw-state-paused .jw-display-icon-container{display:none}.jwplayer.jw-flag-ads-googleima.jw-skin-seven .jw-controlbar{font-size:.9em}.jwplayer.jw-flag-ads-vpaid .jw-controlbar{display:none}.jwplayer.jw-flag-ads-hide-controls .jw-controls{display:none!important}.jwplayer.jw-flag-ads.jw-flag-touch .jw-controlbar{display:table}.jwplayer.jw-flag-overlay-open-related .jw-controls,.jwplayer.jw-flag-overlay-open-related .jw-title,.jwplayer.jw-flag-overlay-open-sharing .jw-controls,.jwplayer.jw-flag-overlay-open-sharing .jw-title,.jwplayer.jw-flag-overlay-open .jw-controls-right .jw-logo,.jwplayer.jw-flag-overlay-open .jw-title{display:none}.jwplayer.jw-flag-rightclick-open{overflow:visible}.jwplayer.jw-flag-rightclick-open .jw-rightclick{z-index:16777215}.jw-flag-controls-disabled .jw-controls{visibility:hidden}.jw-flag-controls-disabled .jw-logo{visibility:visible}.jw-flag-controls-disabled .jw-media{cursor:auto}body .jwplayer.jw-flag-flash-blocked .jw-title{display:block}body .jwplayer.jw-flag-flash-blocked .jw-controls,body .jwplayer.jw-flag-flash-blocked .jw-overlays,body .jwplayer.jw-flag-flash-blocked .jw-preview{display:none}.jw-flag-touch .jw-controlbar,.jw-flag-touch .jw-plugin,.jw-flag-touch .jw-skip{font-size:1.5em}.jw-flag-touch .jw-captions{bottom:4.25em}.jw-flag-touch video::-webkit-media-text-track-container{max-height:70%}.jw-flag-touch .jw-icon-tooltip.jw-open-drawer:before{display:inline;content:"\\E615"}.jw-flag-touch .jw-display-icon-container{pointer-events:none}.jw-flag-touch.jw-state-paused .jw-display-icon-container{display:table}.jw-flag-compact-player .jw-icon-playlist,.jw-flag-compact-player .jw-text-duration,.jw-flag-compact-player .jw-text-elapsed,.jw-flag-touch.jw-state-paused.jw-flag-dragging .jw-display-icon-container{display:none}.jwplayer.jw-flag-audio-player{background-color:transparent}.jwplayer.jw-flag-audio-player .jw-media{visibility:hidden}.jwplayer.jw-flag-audio-player .jw-media object{width:1px;height:1px}.jwplayer.jw-flag-audio-player .jw-display-icon-container,.jwplayer.jw-flag-audio-player .jw-preview{display:none}.jwplayer.jw-flag-audio-player .jw-controlbar{display:table;height:auto;left:0;bottom:0;margin:0;width:100%;min-width:100%;opacity:1}.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-fullscreen,.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-tooltip{display:none}.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-inline.jw-icon-volume,.jwplayer.jw-flag-audio-player .jw-controlbar .jw-slider-volume.jw-slider-horizontal{display:inline-block}.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-tooltip.jw-icon-volume{display:none}.jwplayer.jw-flag-audio-player.jw-flag-user-inactive .jw-controlbar{display:table}.jw-skin-seven .jw-background-color{background:#000}.jw-skin-seven .jw-controlbar{border-top:1px solid #333;height:2.5em}.jw-skin-seven .jw-group{vertical-align:middle}.jw-skin-seven .jw-playlist{background-color:rgba(0,0,0,.5)}.jw-skin-seven .jw-playlist-container{left:-43%;background-color:rgba(0,0,0,.5)}.jw-skin-seven .jw-playlist-container .jw-option{border-bottom:1px solid #444}.jw-skin-seven .jw-playlist-container .jw-option.jw-active-option,.jw-skin-seven .jw-playlist-container .jw-option:hover{background-color:#000}.jw-skin-seven .jw-playlist-container .jw-option:hover .jw-label{color:#ff0046}.jw-skin-seven .jw-playlist-container .jw-icon-playlist{margin-left:0}.jw-skin-seven .jw-playlist-container .jw-label .jw-icon-play{color:#ff0046}.jw-skin-seven .jw-playlist-container .jw-label .jw-icon-play:before{padding-left:0}.jw-skin-seven .jw-tooltip-title{background-color:#000;color:#fff}.jw-skin-seven .jw-button-color,.jw-skin-seven .jw-text{color:#fff}.jw-skin-seven .jw-button-color:hover,.jw-skin-seven .jw-toggle{color:#ff0046}.jw-skin-seven .jw-toggle.jw-off{color:#fff}.jw-skin-seven .jw-controlbar .jw-icon:before,.jw-skin-seven .jw-text-duration,.jw-skin-seven .jw-text-elapsed{padding:0 .7em}.jw-skin-seven .jw-controlbar .jw-icon-prev:before{padding-right:.25em}.jw-skin-seven .jw-controlbar .jw-icon-playlist:before{padding:0 .45em}.jw-skin-seven .jw-controlbar .jw-icon-next:before{padding-left:.25em}.jw-skin-seven .jw-icon-next,.jw-skin-seven .jw-icon-prev{font-size:.7em}.jw-skin-seven .jw-icon-prev:before{border-left:1px solid #666}.jw-skin-seven .jw-icon-next:before{border-right:1px solid #666}.jw-skin-seven .jw-icon-display{color:#fff}.jw-skin-seven .jw-icon-display:before{padding-left:0}.jw-skin-seven .jw-display-icon-container{border-radius:50%;border:1px solid #333}.jw-skin-seven .jw-rail{background-color:#384154;box-shadow:none}.jw-skin-seven .jw-buffer{background-color:#666f82}.jw-skin-seven .jw-progress{background:#ff0046}.jw-skin-seven .jw-knob{width:.6em;height:.6em;background-color:#fff;box-shadow:0 0 0 1px #000;border-radius:1em}.jw-skin-seven .jw-slider-horizontal .jw-slider-container{height:.95em}.jw-skin-seven .jw-slider-horizontal .jw-buffer,.jw-skin-seven .jw-slider-horizontal .jw-progress,.jw-skin-seven .jw-slider-horizontal .jw-rail{height:.2em;border-radius:0}.jw-skin-seven .jw-slider-horizontal .jw-knob{top:-.2em}.jw-skin-seven .jw-slider-horizontal .jw-cue{top:-.05em;width:.3em;height:.3em;background-color:#fff;border-radius:50%}.jw-skin-seven .jw-slider-vertical .jw-buffer,.jw-skin-seven .jw-slider-vertical .jw-progress,.jw-skin-seven .jw-slider-vertical .jw-rail{width:.2em}.jw-skin-seven .jw-slider-vertical .jw-knob{margin-bottom:-.3em}.jw-skin-seven .jw-volume-tip{width:100%;left:-45%;padding-bottom:.7em}.jw-skin-seven .jw-text-duration{color:#666f82}.jw-skin-seven .jw-controlbar-right-group .jw-icon-inline:before,.jw-skin-seven .jw-controlbar-right-group .jw-icon-tooltip:before{border-left:1px solid #666}.jw-skin-seven .jw-controlbar-right-group .jw-icon-inline:first-child:before{border:none}.jw-skin-seven .jw-dock .jw-dock-button{border-radius:50%;border:1px solid #333}.jw-skin-seven .jw-dock .jw-overlay{border-radius:2.5em}.jw-skin-seven .jw-icon-tooltip .jw-active-option{background-color:#ff0046;color:#fff}.jw-skin-seven .jw-icon-volume{min-width:2.6em}.jw-skin-seven .jw-menu,.jw-skin-seven .jw-skip,.jw-skin-seven .jw-time-tip,.jw-skin-seven .jw-volume-tip{border:1px solid #333}.jw-skin-seven .jw-time-tip{padding:.2em;bottom:1.3em}.jw-skin-seven .jw-menu,.jw-skin-seven .jw-volume-tip{bottom:.24em}.jw-skin-seven .jw-skip{padding:.4em;border-radius:1.75em}.jw-skin-seven .jw-skip .jw-icon-inline,.jw-skin-seven .jw-skip .jw-text{color:#fff;line-height:1.75em}.jw-skin-seven .jw-skip.jw-skippable:hover .jw-icon-inline,.jw-skin-seven .jw-skip.jw-skippable:hover .jw-text{color:#ff0046}.jw-skin-seven.jw-flag-touch .jw-controlbar .jw-icon:before,.jw-skin-seven.jw-flag-touch .jw-text-duration,.jw-skin-seven.jw-flag-touch .jw-text-elapsed{padding:0 .35em}.jw-skin-seven.jw-flag-touch .jw-controlbar .jw-icon-prev:before{padding:0 .125em 0 .7em}.jw-skin-seven.jw-flag-touch .jw-controlbar .jw-icon-next:before{padding:0 .7em 0 .125em}.jw-skin-seven.jw-flag-touch .jw-controlbar .jw-icon-playlist:before{padding:0 .225em}',""]);
	},function(e,t){e.exports=function(){var e=[];return e.toString=function(){for(var e=[],t=0;t<this.length;t++){var n=this[t];n[2]?e.push("@media "+n[2]+"{"+n[1]+"}"):e.push(n[1])}return e.join("")},e.i=function(t,n){"string"==typeof t&&(t=[[null,t,""]]);for(var i={},r=0;r<this.length;r++){var o=this[r][0];"number"==typeof o&&(i[o]=!0)}for(r=0;r<t.length;r++){var a=t[r];"number"==typeof a[0]&&i[a[0]]||(n&&!a[2]?a[2]=n:n&&(a[2]="("+a[2]+") and ("+n+")"),e.push(a))}},e}},function(e,t,n){e.exports=n.p+"jw-icons.ttf"},function(e,t,n){e.exports=n.p+"jw-icons.woff"},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){return'<div class="jw-skip jw-background-color jw-hidden jw-reset">\n    <span class="jw-text jw-skiptext jw-reset"></span>\n    <span class="jw-icon-inline jw-skip-icon jw-reset"></span>\n</div>'},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){return'<div class="jw-display-icon-container jw-background-color jw-reset">\n    <div class="jw-icon jw-icon-display jw-button-color jw-reset"></div>\n</div>\n'},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({1:function(e,t,n,i,r){var o,a,s=null!=t?t:{};return'    <div class="jw-dock-button jw-background-color jw-reset'+(null!=(o=n["if"].call(s,null!=t?t.btnClass:t,{name:"if",hash:{},fn:e.program(2,r,0),inverse:e.noop,data:r}))?o:"")+'" button="'+e.escapeExpression((a=null!=(a=n.id||(null!=t?t.id:t))?a:n.helperMissing,"function"==typeof a?a.call(s,{name:"id",hash:{},data:r}):a))+'">\n        <div class="jw-icon jw-dock-image jw-reset" '+(null!=(o=n["if"].call(s,null!=t?t.img:t,{name:"if",hash:{},fn:e.program(4,r,0),inverse:e.noop,data:r}))?o:"")+'></div>\n        <div class="jw-arrow jw-reset"></div>\n'+(null!=(o=n["if"].call(s,null!=t?t.tooltip:t,{name:"if",hash:{},fn:e.program(6,r,0),inverse:e.noop,data:r}))?o:"")+"    </div>\n"},2:function(e,t,n,i,r){var o;return" "+e.escapeExpression((o=null!=(o=n.btnClass||(null!=t?t.btnClass:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"btnClass",hash:{},data:r}):o))},4:function(e,t,n,i,r){var o;return"style='background-image: url(\""+e.escapeExpression((o=null!=(o=n.img||(null!=t?t.img:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"img",hash:{},data:r}):o))+"\")'"},6:function(e,t,n,i,r){var o;return'        <div class="jw-overlay jw-background-color jw-reset">\n            <span class="jw-text jw-dock-text jw-reset">'+e.escapeExpression((o=null!=(o=n.tooltip||(null!=t?t.tooltip:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"tooltip",hash:{},data:r}):o))+"</span>\n        </div>\n"},compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div class="jw-dock jw-reset">\n'+(null!=(o=n.each.call(null!=t?t:{},t,{name:"each",hash:{},fn:e.program(1,r,0),inverse:e.noop,data:r}))?o:"")+"</div>"},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o,a=null!=t?t:{},s=n.helperMissing,l="function",c=e.escapeExpression;return'<div id="'+c((o=null!=(o=n.id||(null!=t?t.id:t))?o:s,typeof o===l?o.call(a,{name:"id",hash:{},data:r}):o))+'"class="jw-skin-'+c((o=null!=(o=n.skin||(null!=t?t.skin:t))?o:s,typeof o===l?o.call(a,{name:"skin",hash:{},data:r}):o))+' jw-error jw-reset">\n    <div class="jw-title jw-reset">\n        <div class="jw-title-primary jw-reset">'+c((o=null!=(o=n.title||(null!=t?t.title:t))?o:s,typeof o===l?o.call(a,{name:"title",hash:{},data:r}):o))+'</div>\n        <div class="jw-title-secondary jw-reset">'+c((o=null!=(o=n.body||(null!=t?t.body:t))?o:s,typeof o===l?o.call(a,{name:"body",hash:{},data:r}):o))+'</div>\n    </div>\n\n    <div class="jw-icon-container jw-reset">\n        <div class="jw-display-icon-container jw-background-color jw-reset">\n            <div class="jw-icon jw-icon-display jw-reset"></div>\n        </div>\n    </div>\n</div>\n'},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){return'<div class="jw-logo jw-reset"></div>'},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({1:function(e,t,n,i,r){var o,a=e.escapeExpression;return"        <li class='jw-text jw-option jw-item-"+a((o=null!=(o=n.index||r&&r.index)?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"index",hash:{},data:r}):o))+" jw-reset'>"+a(e.lambda(null!=t?t.label:t,t))+"</li>\n"},compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<ul class="jw-menu jw-background-color jw-reset">\n'+(null!=(o=n.each.call(null!=t?t:{},t,{name:"each",hash:{},fn:e.program(1,r,0),inverse:e.noop,data:r}))?o:"")+"</ul>"},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div id="'+e.escapeExpression((o=null!=(o=n.id||(null!=t?t.id:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"id",hash:{},data:r}):o))+'" class="jwplayer jw-reset" tabindex="0">\n    <div class="jw-aspect jw-reset"></div>\n    <div class="jw-media jw-reset"></div>\n    <div class="jw-preview jw-reset"></div>\n    <div class="jw-title jw-reset">\n        <div class="jw-title-primary jw-reset"></div>\n        <div class="jw-title-secondary jw-reset"></div>\n    </div>\n    <div class="jw-overlays jw-reset"></div>\n    <div class="jw-controls jw-reset"></div>\n</div>'},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({1:function(e,t,n,i,r){var o;return null!=(o=n["if"].call(null!=t?t:{},null!=t?t.active:t,{name:"if",hash:{},fn:e.program(2,r,0),inverse:e.program(4,r,0),data:r}))?o:""},2:function(e,t,n,i,r){var o,a=e.escapeExpression;return"                <li class='jw-option jw-text jw-active-option jw-item-"+a((o=null!=(o=n.index||r&&r.index)?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"index",hash:{},data:r}):o))+' jw-reset\'>\n                    <span class="jw-label jw-reset"><span class="jw-icon jw-icon-play jw-reset"></span></span>\n                    <span class="jw-name jw-reset">'+a(e.lambda(null!=t?t.title:t,t))+"</span>\n                </li>\n"},4:function(e,t,n,i,r){var o,a=e.escapeExpression,s=e.lambda;return"                <li class='jw-option jw-text jw-item-"+a((o=null!=(o=n.index||r&&r.index)?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"index",hash:{},data:r}):o))+' jw-reset\'>\n                    <span class="jw-label jw-reset">'+a(s(null!=t?t.label:t,t))+'</span>\n                    <span class="jw-name jw-reset">'+a(s(null!=t?t.title:t,t))+"</span>\n                </li>\n"},compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div class="jw-menu jw-playlist-container jw-background-color jw-reset">\n\n    <div class="jw-tooltip-title jw-reset">\n        <span class="jw-icon jw-icon-inline jw-icon-playlist jw-reset"></span>\n        <span class="jw-text jw-reset">PLAYLIST</span>\n    </div>\n\n    <ul class="jw-playlist jw-reset">\n'+(null!=(o=n.each.call(null!=t?t:{},t,{name:"each",hash:{},fn:e.program(1,r,0),inverse:e.noop,data:r}))?o:"")+"    </ul>\n</div>"},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({1:function(e,t,n,i,r){var o,a,s=null!=t?t:{},l=n.helperMissing,c="function",u=e.escapeExpression;return'        <li class="jw-reset'+(null!=(o=n["if"].call(s,null!=t?t.featured:t,{name:"if",hash:{},fn:e.program(2,r,0),inverse:e.noop,data:r}))?o:"")+'">\n            <a href="'+u((a=null!=(a=n.link||(null!=t?t.link:t))?a:l,typeof a===c?a.call(s,{name:"link",hash:{},data:r}):a))+'" class="jw-reset" target="_top">\n'+(null!=(o=n["if"].call(s,null!=t?t.showLogo:t,{name:"if",hash:{},fn:e.program(4,r,0),inverse:e.noop,data:r}))?o:"")+"                "+u((a=null!=(a=n.title||(null!=t?t.title:t))?a:l,typeof a===c?a.call(s,{name:"title",hash:{},data:r}):a))+"\n            </a>\n        </li>\n"},2:function(e,t,n,i,r){return" jw-featured"},4:function(e,t,n,i,r){return'                <span class="jw-icon jw-rightclick-logo jw-reset"></span>\n'},compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div class="jw-rightclick jw-reset">\n    <ul class="jw-reset">\n'+(null!=(o=n.each.call(null!=t?t:{},null!=t?t.items:t,{name:"each",hash:{},fn:e.program(1,r,0),inverse:e.noop,data:r}))?o:"")+"    </ul>\n</div>"},useData:!0})},function(e,t,n){var i=n(16);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o,a=null!=t?t:{},s=n.helperMissing,l="function",c=e.escapeExpression;return'<div class="'+c((o=null!=(o=n.className||(null!=t?t.className:t))?o:s,typeof o===l?o.call(a,{name:"className",hash:{},data:r}):o))+" "+c((o=null!=(o=n.orientation||(null!=t?t.orientation:t))?o:s,typeof o===l?o.call(a,{name:"orientation",hash:{},data:r}):o))+' jw-reset">\n    <div class="jw-slider-container jw-reset">\n        <div class="jw-rail jw-reset"></div>\n        <div class="jw-buffer jw-reset"></div>\n        <div class="jw-progress jw-reset"></div>\n        <div class="jw-knob jw-reset"></div>\n    </div>\n</div>'},useData:!0})},,function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){if(e&&e.__esModule)return e;var t={};if(null!=e)for(var n in e)Object.prototype.hasOwnProperty.call(e,n)&&(t[n]=e[n]);return t["default"]=e,t}function o(){var e=new s.HandlebarsEnvironment;return h.extend(e,s),e.SafeString=c["default"],e.Exception=d["default"],e.Utils=h,e.escapeExpression=h.escapeExpression,e.VM=g,e.template=function(t){return g.template(t,e)},e}t.__esModule=!0;var a=n(118),s=r(a),l=n(259),c=i(l),u=n(39),d=i(u),p=n(20),h=r(p),f=n(258),g=r(f),m=n(257),v=i(m),w=o();w.create=o,v["default"](w),w["default"]=w,t["default"]=w,e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){a["default"](e)}t.__esModule=!0,t.registerDefaultDecorators=r;var o=n(247),a=i(o)},function(e,t,n){"use strict";t.__esModule=!0;var i=n(20);t["default"]=function(e){e.registerDecorator("inline",function(e,t,n,r){var o=e;return t.partials||(t.partials={},o=function(r,o){var a=n.partials;n.partials=i.extend({},a,t.partials);var s=e(r,o);return n.partials=a,s}),t.partials[r.args[0]]=r.fn,o})},e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){a["default"](e),l["default"](e),u["default"](e),p["default"](e),f["default"](e),m["default"](e),w["default"](e)}t.__esModule=!0,t.registerDefaultHelpers=r;var o=n(249),a=i(o),s=n(250),l=i(s),c=n(251),u=i(c),d=n(252),p=i(d),h=n(253),f=i(h),g=n(254),m=i(g),v=n(255),w=i(v)},function(e,t,n){"use strict";t.__esModule=!0;var i=n(20);t["default"]=function(e){e.registerHelper("blockHelperMissing",function(t,n){var r=n.inverse,o=n.fn;if(t===!0)return o(this);if(t===!1||null==t)return r(this);if(i.isArray(t))return t.length>0?(n.ids&&(n.ids=[n.name]),e.helpers.each(t,n)):r(this);if(n.data&&n.ids){var a=i.createFrame(n.data);a.contextPath=i.appendContextPath(n.data.contextPath,n.name),n={data:a}}return o(t,n)})},e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}t.__esModule=!0;var r=n(20),o=n(39),a=i(o);t["default"]=function(e){e.registerHelper("each",function(e,t){function n(t,n,o){c&&(c.key=t,c.index=n,c.first=0===n,c.last=!!o,u&&(c.contextPath=u+t)),l+=i(e[t],{data:c,blockParams:r.blockParams([e[t],t],[u+t,null])})}if(!t)throw new a["default"]("Must pass iterator to #each");var i=t.fn,o=t.inverse,s=0,l="",c=void 0,u=void 0;if(t.data&&t.ids&&(u=r.appendContextPath(t.data.contextPath,t.ids[0])+"."),r.isFunction(e)&&(e=e.call(this)),t.data&&(c=r.createFrame(t.data)),e&&"object"==typeof e)if(r.isArray(e))for(var d=e.length;d>s;s++)s in e&&n(s,s,s===e.length-1);else{var p=void 0;for(var h in e)e.hasOwnProperty(h)&&(void 0!==p&&n(p,s-1),p=h,s++);void 0!==p&&n(p,s-1,!0)}return 0===s&&(l=o(this)),l})},e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}t.__esModule=!0;var r=n(39),o=i(r);t["default"]=function(e){e.registerHelper("helperMissing",function(){if(1!==arguments.length)throw new o["default"]('Missing helper: "'+arguments[arguments.length-1].name+'"')})},e.exports=t["default"]},function(e,t,n){"use strict";t.__esModule=!0;var i=n(20);t["default"]=function(e){e.registerHelper("if",function(e,t){return i.isFunction(e)&&(e=e.call(this)),!t.hash.includeZero&&!e||i.isEmpty(e)?t.inverse(this):t.fn(this)}),e.registerHelper("unless",function(t,n){return e.helpers["if"].call(this,t,{fn:n.inverse,inverse:n.fn,hash:n.hash})})},e.exports=t["default"]},function(e,t){"use strict";t.__esModule=!0,t["default"]=function(e){e.registerHelper("log",function(){for(var t=[void 0],n=arguments[arguments.length-1],i=0;i<arguments.length-1;i++)t.push(arguments[i]);var r=1;null!=n.hash.level?r=n.hash.level:n.data&&null!=n.data.level&&(r=n.data.level),t[0]=r,e.log.apply(e,t)})},e.exports=t["default"]},function(e,t){"use strict";t.__esModule=!0,t["default"]=function(e){e.registerHelper("lookup",function(e,t){return e&&e[t]})},e.exports=t["default"]},function(e,t,n){"use strict";t.__esModule=!0;var i=n(20);t["default"]=function(e){e.registerHelper("with",function(e,t){i.isFunction(e)&&(e=e.call(this));var n=t.fn;if(i.isEmpty(e))return t.inverse(this);var r=t.data;return t.data&&t.ids&&(r=i.createFrame(t.data),r.contextPath=i.appendContextPath(t.data.contextPath,t.ids[0])),n(e,{data:r,blockParams:i.blockParams([e],[r&&r.contextPath])})})},e.exports=t["default"]},function(e,t,n){"use strict";t.__esModule=!0;var i=n(20),r={methodMap:["debug","info","warn","error"],level:"info",lookupLevel:function(e){if("string"==typeof e){var t=i.indexOf(r.methodMap,e.toLowerCase());e=t>=0?t:parseInt(e,10)}return e},log:function(e){if(e=r.lookupLevel(e),"undefined"!=typeof console&&r.lookupLevel(r.level)<=e){var t=r.methodMap[e];console[t]||(t="log");for(var n=arguments.length,i=Array(n>1?n-1:0),o=1;n>o;o++)i[o-1]=arguments[o];console[t].apply(console,i)}}};t["default"]=r,e.exports=t["default"]},function(e,t){(function(n){"use strict";t.__esModule=!0,t["default"]=function(e){var t="undefined"!=typeof n?n:window,i=t.Handlebars;e.noConflict=function(){return t.Handlebars===e&&(t.Handlebars=i),e}},e.exports=t["default"]}).call(t,function(){return this}())},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){if(e&&e.__esModule)return e;var t={};if(null!=e)for(var n in e)Object.prototype.hasOwnProperty.call(e,n)&&(t[n]=e[n]);return t["default"]=e,t}function o(e){var t=e&&e[0]||1,n=v.COMPILER_REVISION;if(t!==n){if(n>t){var i=v.REVISION_CHANGES[n],r=v.REVISION_CHANGES[t];throw new m["default"]("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version ("+i+") or downgrade your runtime to an older version ("+r+").")}throw new m["default"]("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version ("+e[1]+").")}}function a(e,t){function n(n,i,r){r.hash&&(i=f.extend({},i,r.hash),r.ids&&(r.ids[0]=!0)),n=t.VM.resolvePartial.call(this,n,i,r);var o=t.VM.invokePartial.call(this,n,i,r);if(null==o&&t.compile&&(r.partials[r.name]=t.compile(n,e.compilerOptions,t),o=r.partials[r.name](i,r)),null!=o){if(r.indent){for(var a=o.split("\n"),s=0,l=a.length;l>s&&(a[s]||s+1!==l);s++)a[s]=r.indent+a[s];o=a.join("\n")}return o}throw new m["default"]("The partial "+r.name+" could not be compiled when running in runtime-only mode")}function i(t){function n(t){return""+e.main(r,t,r.helpers,r.partials,a,l,s)}var o=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],a=o.data;i._setup(o),!o.partial&&e.useData&&(a=d(t,a));var s=void 0,l=e.useBlockParams?[]:void 0;return e.useDepths&&(s=o.depths?t!==o.depths[0]?[t].concat(o.depths):o.depths:[t]),(n=p(e.main,n,r,o.depths||[],a,l))(t,o)}if(!t)throw new m["default"]("No environment passed to template");if(!e||!e.main)throw new m["default"]("Unknown template object: "+typeof e);e.main.decorator=e.main_d,t.VM.checkRevision(e.compiler);var r={strict:function(e,t){if(!(t in e))throw new m["default"]('"'+t+'" not defined in '+e);return e[t]},lookup:function(e,t){for(var n=e.length,i=0;n>i;i++)if(e[i]&&null!=e[i][t])return e[i][t]},lambda:function(e,t){return"function"==typeof e?e.call(t):e},escapeExpression:f.escapeExpression,invokePartial:n,fn:function(t){var n=e[t];return n.decorator=e[t+"_d"],n},programs:[],program:function(e,t,n,i,r){var o=this.programs[e],a=this.fn(e);return t||r||i||n?o=s(this,e,a,t,n,i,r):o||(o=this.programs[e]=s(this,e,a)),o},data:function(e,t){for(;e&&t--;)e=e._parent;return e},merge:function(e,t){var n=e||t;return e&&t&&e!==t&&(n=f.extend({},t,e)),n},noop:t.VM.noop,compilerInfo:e.compiler};return i.isTop=!0,i._setup=function(n){n.partial?(r.helpers=n.helpers,r.partials=n.partials,r.decorators=n.decorators):(r.helpers=r.merge(n.helpers,t.helpers),e.usePartial&&(r.partials=r.merge(n.partials,t.partials)),(e.usePartial||e.useDecorators)&&(r.decorators=r.merge(n.decorators,t.decorators)))},i._child=function(t,n,i,o){if(e.useBlockParams&&!i)throw new m["default"]("must pass block params");if(e.useDepths&&!o)throw new m["default"]("must pass parent depths");return s(r,t,e[t],n,0,i,o)},i}function s(e,t,n,i,r,o,a){function s(t){var r=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],s=a;return a&&t!==a[0]&&(s=[t].concat(a)),n(e,t,e.helpers,e.partials,r.data||i,o&&[r.blockParams].concat(o),s)}return s=p(n,s,e,a,i,o),s.program=t,s.depth=a?a.length:0,s.blockParams=r||0,s}function l(e,t,n){return e?e.call||n.name||(n.name=e,e=n.partials[e]):e="@partial-block"===n.name?n.data["partial-block"]:n.partials[n.name],e}function c(e,t,n){n.partial=!0,n.ids&&(n.data.contextPath=n.ids[0]||n.data.contextPath);var i=void 0;if(n.fn&&n.fn!==u&&(n.data=v.createFrame(n.data),i=n.data["partial-block"]=n.fn,i.partials&&(n.partials=f.extend({},n.partials,i.partials))),void 0===e&&i&&(e=i),void 0===e)throw new m["default"]("The partial "+n.name+" could not be found");return e instanceof Function?e(t,n):void 0}function u(){return""}function d(e,t){return t&&"root"in t||(t=t?v.createFrame(t):{},t.root=e),t}function p(e,t,n,i,r,o){if(e.decorator){var a={};t=e.decorator(t,a,n,i&&i[0],r,o,i),f.extend(t,a)}return t}t.__esModule=!0,t.checkRevision=o,t.template=a,t.wrapProgram=s,t.resolvePartial=l,t.invokePartial=c,t.noop=u;var h=n(20),f=r(h),g=n(39),m=i(g),v=n(118)},function(e,t){"use strict";function n(e){this.string=e}t.__esModule=!0,n.prototype.toString=n.prototype.toHTML=function(){return""+this.string},t["default"]=n,e.exports=t["default"]},function(e,t,n){var i,r;i=[n(41),n(1)],r=function(e,t){return function(n,i){var r=["seek","skipAd","stop","playlistNext","playlistPrev","playlistItem","resize","addButton","removeButton","registerPlugin","attachMedia"];t.each(r,function(e){n[e]=function(){return i[e].apply(i,arguments),n}}),n.registerPlugin=e.registerPlugin}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){return function(t,n){var i=["buffer","controls","position","duration","fullscreen","volume","mute","item","stretching","playlist"];e.each(i,function(e){var i=e.slice(0,1).toUpperCase()+e.slice(1);t["get"+i]=function(){return n._model.get(e)}});var r=["getAudioTracks","getCaptionsList","getWidth","getHeight","getCurrentAudioTrack","setCurrentAudioTrack","getCurrentCaptions","setCurrentCaptions","getCurrentQuality","setCurrentQuality","getQualityLevels","getVisualQuality","getConfig","getState","getSafeRegion","isBeforeComplete","isBeforePlay","getProvider","detachMedia"],o=["setControls","setFullscreen","setVolume","setMute","setCues"];e.each(r,function(e){t[e]=function(){return n[e]?n[e].apply(n,arguments):null}}),e.each(o,function(e){t[e]=function(){return n[e].apply(n,arguments),t}}),t.getPlaylistIndex=t.getItem}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(4),n(6),n(3),n(2),n(131),n(132),n(1),n(317),n(260),n(261),n(263),n(46)],r=function(e,t,n,i,r,o,a,s,l,c,u,d){var p=function(o,p){var h,f=this,g=!1,m={};a.extend(this,n),this.utils=i,this._=a,this.Events=n,this.version=d,this.trigger=function(e,t){return t=a.isObject(t)?a.extend({},t):{},t.type=e,window.jwplayer&&window.jwplayer.debug?n.trigger.call(f,e,t):n.triggerSafe.call(f,e,t)},this.dispatchEvent=this.trigger,this.removeEventListener=this.off.bind(this);var v=function(){h=new s(o),l(f,h),c(f,h),h.on(e.JWPLAYER_PLAYLIST_ITEM,function(){m={}}),h.on(e.JWPLAYER_MEDIA_META,function(e){a.extend(m,e.metadata)}),h.on(e.JWPLAYER_READY,function(e){g=!0,w.tick("ready"),e.setupTime=w.between("setup","ready")}),h.on("all",f.trigger)};v(),u(this),this.id=o.id;var w=this._qoe=new r;w.tick("init");var y=function(){g=!1,m={},f.off(),h&&h.off(),h&&h.playerDestroy&&h.playerDestroy()};return this.getPlugin=function(e){return f.plugins&&f.plugins[e]},this.addPlugin=function(e,t){this.plugins=this.plugins||{},this.plugins[e]=t,this.onReady(t.addToPlayer),t.resize&&this.onResize(t.resizeHandler)},this.setup=function(e){return w.tick("setup"),y(),v(),i.foreach(e.events,function(e,t){var n=f[e];"function"==typeof n&&n.call(f,t)}),e.id=f.id,h.setup(e,this),f},this.qoe=function(){var t=h.getItemQoe(),n=w.between("setup","ready"),i=t.between(e.JWPLAYER_MEDIA_PLAY_ATTEMPT,e.JWPLAYER_MEDIA_FIRST_FRAME);return{setupTime:n,firstFrame:i,player:w.dump(),item:t.dump()}},this.getContainer=function(){return h.getContainer?h.getContainer():o},this.getMeta=this.getItemMeta=function(){return m},this.getPlaylistItem=function(e){if(!i.exists(e))return h._model.get("playlistItem");var t=f.getPlaylist();return t?t[e]:null},this.getRenderingMode=function(){return"html5"},this.load=function(e){var t=this.getPlugin("vast")||this.getPlugin("googima");return t&&t.destroy(),h.load(e),f},this.play=function(e,n){if(a.isBoolean(e)||(n=e),n||(n={reason:"external"}),e===!0)return h.play(n),f;if(e===!1)return h.pause(),f;switch(e=f.getState()){case t.PLAYING:case t.BUFFERING:h.pause();break;default:h.play(n)}return f},this.pause=function(e){return a.isBoolean(e)?this.play(!e):this.play()},this.createInstream=function(){return h.createInstream()},this.castToggle=function(){h&&h.castToggle&&h.castToggle()},this.playAd=this.pauseAd=i.noop,this.remove=function(){return p(f),f.trigger("remove"),y(),f},this};return p}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(4)],r=function(e,t){return function(n){var i={onBufferChange:t.JWPLAYER_MEDIA_BUFFER,onBufferFull:t.JWPLAYER_MEDIA_BUFFER_FULL,onError:t.JWPLAYER_ERROR,onSetupError:t.JWPLAYER_SETUP_ERROR,onFullscreen:t.JWPLAYER_FULLSCREEN,onMeta:t.JWPLAYER_MEDIA_META,onMute:t.JWPLAYER_MEDIA_MUTE,onPlaylist:t.JWPLAYER_PLAYLIST_LOADED,onPlaylistItem:t.JWPLAYER_PLAYLIST_ITEM,onPlaylistComplete:t.JWPLAYER_PLAYLIST_COMPLETE,onReady:t.JWPLAYER_READY,onResize:t.JWPLAYER_RESIZE,onComplete:t.JWPLAYER_MEDIA_COMPLETE,onSeek:t.JWPLAYER_MEDIA_SEEK,onTime:t.JWPLAYER_MEDIA_TIME,onVolume:t.JWPLAYER_MEDIA_VOLUME,onBeforePlay:t.JWPLAYER_MEDIA_BEFOREPLAY,onBeforeComplete:t.JWPLAYER_MEDIA_BEFORECOMPLETE,onDisplayClick:t.JWPLAYER_DISPLAY_CLICK,onControls:t.JWPLAYER_CONTROLS,onQualityLevels:t.JWPLAYER_MEDIA_LEVELS,onQualityChange:t.JWPLAYER_MEDIA_LEVEL_CHANGED,onCaptionsList:t.JWPLAYER_CAPTIONS_LIST,onCaptionsChange:t.JWPLAYER_CAPTIONS_CHANGED,onAdError:t.JWPLAYER_AD_ERROR,onAdClick:t.JWPLAYER_AD_CLICK,onAdImpression:t.JWPLAYER_AD_IMPRESSION,onAdTime:t.JWPLAYER_AD_TIME,onAdComplete:t.JWPLAYER_AD_COMPLETE,onAdCompanions:t.JWPLAYER_AD_COMPANIONS,onAdSkipped:t.JWPLAYER_AD_SKIPPED,onAdPlay:t.JWPLAYER_AD_PLAY,onAdPause:t.JWPLAYER_AD_PAUSE,onAdMeta:t.JWPLAYER_AD_META,onCast:t.JWPLAYER_CAST_SESSION,onAudioTrackChange:t.JWPLAYER_AUDIO_TRACK_CHANGED,onAudioTracks:t.JWPLAYER_AUDIO_TRACKS},r={onBuffer:"buffer",onPause:"pause",onPlay:"play",onIdle:"idle"};e.each(r,function(t,i){n[i]=e.partial(n.on,t,e)}),e.each(i,function(t,i){n[i]=e.partial(n.on,t,e)})}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(262),n(1),n(33),n(89),n(41)],r=function(e,t,n,i,r){var o=[],a=0,s=function(t){var n,i;return t?"string"==typeof t?(n=l(t),n||(i=document.getElementById(t))):"number"==typeof t?n=o[t]:t.nodeType&&(i=t,n=l(i.id)):n=o[0],n?n:i?c(new e(i,u)):{registerPlugin:r.registerPlugin}},l=function(e){for(var t=0;t<o.length;t++)if(o[t].id===e)return o[t];return null},c=function(e){return a++,e.uniqueId=a,o.push(e),e},u=function(e){for(var t=o.length;t--;)if(o[t].uniqueId===e.uniqueId){o.splice(t,1);break}},d={selectPlayer:s,registerProvider:n.registerProvider,availableProviders:i,registerPlugin:r.registerPlugin};return s.api=d,d}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(318),n(3),n(1),n(4)],r=function(e,t,n,i){var r=function(t,r,o,a){function s(){p("Setup Timeout Error","Setup took longer than "+m+" seconds to complete.")}function l(){n.each(g,function(e){e.complete!==!0&&e.running!==!0&&null!==t&&u(e.depends)&&(e.running=!0,c(e))})}function c(e){var n=function(t){t=t||{},d(e,t)};e.method(n,r,t,o,a)}function u(e){return n.all(e,function(e){return g[e].complete})}function d(e,t){"error"===t.type?p(t.msg,t.reason):"complete"===t.type?(clearTimeout(h),f.trigger(i.JWPLAYER_READY)):(e.complete=!0,l())}function p(e,t){clearTimeout(h),f.trigger(i.JWPLAYER_SETUP_ERROR,{message:e+": "+t}),f.destroy()}var h,f=this,g=e.getQueue(),m=30;this.start=function(){h=setTimeout(s,1e3*m),l()},this.destroy=function(){clearTimeout(h),this.off(),g.length=0,t=null,r=null,o=null}};return r.prototype=t,r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(40),n(84),n(275),n(2)],r=function(e,t,n,i){var r=function(r,o){function a(e){if(e.tracks.length){o.mediaController.off("meta",s),j=[],b={},E={},k=0;for(var t=e.tracks||[],n=0;n<t.length;n++){var i=t[n];i.id=i.name,i.label=i.name||i.language,p(i)}var r=g();this.setCaptionsList(r),m()}}function s(e){var t=e.metadata;if(t&&"textdata"===t.type){if(!t.text)return;var n=b[t.trackid];if(!n){n={kind:"captions",id:t.trackid,data:[]},p(n);var i=g();this.setCaptionsList(i)}var r,a;t.useDTS?(n.source||(n.source=t.source||"mpegts"),r=t.begin,a=t.begin+"_"+t.text):(r=e.position||o.get("position"),a=""+Math.round(10*r)+"_"+t.text);var s=E[a];s||(s={begin:r,text:t.text},t.end&&(s.end=t.end),E[a]=s,n.data.push(s))}}function l(e){i.log("CAPTIONS("+e+")")}function c(e,t){y=t,j=[],b={},E={},k=0}function u(e){c(o,e),o.mediaController.off("meta",s),o.mediaController.off("subtitlesTracks",a);var t,n,r,l,u=e.tracks,d="html5"===o.get("provider").name,f=i.isChrome()||i.isIOS()||i.isSafari();for(l=0;l<u.length;l++)t=u[l],r=t.file&&/\.(?:web)?vtt(?:\?.*)?$/i.test(t.file),d&&r&&!w&&f||(n=t.kind.toLowerCase(),"captions"!==n&&"subtitles"!==n||(t.file?(p(t),h(t)):t.data&&p(t)));j.length||(o.mediaController.on("meta",s,this),o.mediaController.on("subtitlesTracks",a,this));var v=g();this.setCaptionsList(v),m()}function d(e,t){var n=null;0!==t&&(n=j[t-1]),e.set("captionsTrack",n)}function p(e){"number"!=typeof e.id&&(e.id=e.name||e.file||"cc"+j.length),e.data=e.data||[],e.label||(e.label="Unknown CC",k++,k>1&&(e.label+=" ("+k+")")),j.push(e),b[e.id]=e}function h(e){i.ajax(e.file,function(t){f(t,e)},l)}function f(r,o){var a,s=r.responseXML?r.responseXML.firstChild:null;if(s)for("xml"===e.localName(s)&&(s=s.nextSibling);s.nodeType===s.COMMENT_NODE;)s=s.nextSibling;a=s&&"tt"===e.localName(s)?i.tryCatch(function(){o.data=n(r.responseXML)}):i.tryCatch(function(){o.data=t(r.responseText)}),a instanceof i.Error&&l(a.message+": "+o.file)}function g(){for(var e=[{id:"off",label:"Off"}],t=0;t<j.length;t++)e.push({id:j[t].id,label:j[t].label||"Unknown CC"});return e}function m(){var e=0,t=o.get("captionLabel");if("Off"===t)return void o.set("captionsIndex",0);for(var n=0;n<j.length;n++){var i=j[n];if(t&&t===i.label){e=n+1;break}i["default"]||i.defaulttrack||"default"===i.id?e=n+1:i.autoselect}v(e)}function v(e){j.length?o.setVideoSubtitleTrack(e,j):o.set("captionsIndex",e)}o.on("change:playlistItem",c,this),o.on("change:captionsIndex",d,this),o.on("itemReady",u,this),o.mediaController.on("subtitlesTracks",a,this),o.mediaController.on("subtitlesTrackData",function(e){var t=b[e.name];if(t){t.source=e.source;for(var n=e.captions||[],i=!1,r=0;r<n.length;r++){var o=n[r],a=e.name+"_"+o.begin+"_"+o.end;E[a]||(E[a]=o,t.data.push(o),i=!0)}i&&t.data.sort(function(e,t){return e.begin-t.begin})}},this),o.mediaController.on("meta",s,this);var w=!!o.get("sdkplatform"),y={},j=[],b={},E={},k=0;this.getCurrentIndex=function(){return o.get("captionsIndex")},this.getCaptionsList=function(){return o.get("captionsList")},this.setCaptionsList=function(e){o.set("captionsList",e)}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(119),n(268),n(1),n(265),n(266),n(55),n(273),n(122),n(121),n(2),n(141),n(33),n(3),n(83),n(6),n(4),n(300)],r=function(e,t,n,i,r,o,a,s,l,c,u,d,p,h,f,g,m){function v(e){return function(){var t=Array.prototype.slice.call(arguments,0);this._model.getVideo()?this["_"+e].apply(this,t):this.eventsQueue.push([e,t])}}function w(e){return e===f.LOADING||e===f.STALLED?f.BUFFERING:e}var y=function(e){this.originalContainer=this.currentContainer=e,this.eventsQueue=[],n.extend(this,p),this._model=new o};return y.prototype={play:v("play"),pause:v("pause"),seek:v("seek"),stop:v("stop"),load:v("load"),playlistNext:v("next"),playlistPrev:v("prev"),playlistItem:v("item"),setCurrentCaptions:v("setCurrentCaptions"),setCurrentQuality:v("setCurrentQuality"),setVolume:v("setVolume"),setMute:v("setMute"),setFullscreen:v("setFullscreen"),setup:function(o,p){function m(){ee.mediaModel.on("change:state",function(e,t){var n=w(t);ee.set("state",n)})}function v(){X=null,R(ee.get("item")),ee.on("change:state",h,this),ee.on("change:castState",function(e,t){ie.trigger(g.JWPLAYER_CAST_SESSION,t)}),ee.on("change:fullscreen",function(e,t){ie.trigger(g.JWPLAYER_FULLSCREEN,{fullscreen:t})}),ee.on("itemReady",function(){ie.trigger(g.JWPLAYER_PLAYLIST_ITEM,{index:ee.get("item"),item:ee.get("playlistItem")})}),ee.on("change:playlist",function(e,t){t.length&&ie.trigger(g.JWPLAYER_PLAYLIST_LOADED,{playlist:t})}),ee.on("change:volume",function(e,t){ie.trigger(g.JWPLAYER_MEDIA_VOLUME,{volume:t})}),ee.on("change:mute",function(e,t){ie.trigger(g.JWPLAYER_MEDIA_MUTE,{mute:t})}),ee.on("change:controls",function(e,t){ie.trigger(g.JWPLAYER_CONTROLS,{controls:t})}),ee.on("change:scrubbing",function(e,t){t?_():A()}),ee.on("change:captionsList",function(e,t){ie.trigger(g.JWPLAYER_CAPTIONS_LIST,{tracks:t,track:U()})}),ee.mediaModel.set("mediaType",null),ee.mediaController.on("all",ie.trigger.bind(ie)),Q.on("all",ie.trigger.bind(ie)),this.showView(Q.element()),window.addEventListener("beforeunload",function(){X&&X.destroy(),ee&&ee.destroy()}),n.defer(y)}function y(){ie.trigger(g.JWPLAYER_READY,{setupTime:0}),ie.trigger(g.JWPLAYER_PLAYLIST_LOADED,{playlist:ee.get("playlist")}),ie.trigger(g.JWPLAYER_PLAYLIST_ITEM,{index:ee.get("item"),item:ee.get("playlistItem")}),ie.trigger(g.JWPLAYER_CAPTIONS_LIST,{tracks:ee.get("captionsList"),track:ee.get("captionsIndex")}),ee.get("autostart")&&A({reason:"autostart"}),j()}function j(){for(;ie.eventsQueue.length>0;){var e=ie.eventsQueue.shift(),t=e[0],n=e[1]||[];ie["_"+t].apply(ie,n)}}function b(e){switch(ee.get("state")===f.ERROR&&ee.set("state",f.IDLE),L(!0),ee.get("autostart")&&ee.once("itemReady",A),typeof e){case"string":E(e);break;case"object":var t=s(e),n=ee.get("edition"),i=ee.getProviders(),r=i.required(t,n);d.load(r,n).then(function(){ie.getProvider()||(ee.setProvider(ee.get("playlistItem")),j())});var o=T(e);o&&R(0);break;case"number":R(e)}}function E(e){var t=new l;t.on(g.JWPLAYER_PLAYLIST_LOADED,function(e){b(e.playlist)}),t.on(g.JWPLAYER_ERROR,function(e){
	e.message="Error loading playlist: "+e.message,this.triggerError(e)},this),t.load(e)}function k(){var e=ie._instreamAdapter&&ie._instreamAdapter.getState();return n.isString(e)?e:ee.get("state")}function A(e){var t;if(e&&ee.set("playReason",e.reason),ee.get("state")!==f.ERROR){var i=ie._instreamAdapter&&ie._instreamAdapter.getState();if(n.isString(i))return p.pauseAd(!1);if(ee.get("state")===f.COMPLETE&&(L(!0),R(0)),!te&&(te=!0,ie.trigger(g.JWPLAYER_MEDIA_BEFOREPLAY,{playReason:ee.get("playReason")}),te=!1,Z))return Z=!1,void($=null);if(x()){if(0===ee.get("playlist").length)return!1;t=c.tryCatch(function(){ee.loadVideo()})}else ee.get("state")===f.PAUSED&&(t=c.tryCatch(function(){ee.playVideo()}));return t instanceof c.Error?(ie.triggerError(t),$=null,!1):!0}}function L(e){ee.off("itemReady",A);var t=!e;$=null;var n=c.tryCatch(function(){ee.stopVideo()},ie);return n instanceof c.Error?(ie.triggerError(n),!1):(t&&(ne=!0),te&&(Z=!0),!0)}function _(){$=null;var e=ie._instreamAdapter&&ie._instreamAdapter.getState();if(n.isString(e))return p.pauseAd(!0);switch(ee.get("state")){case f.ERROR:return!1;case f.PLAYING:case f.BUFFERING:var t=c.tryCatch(function(){re().pause()},this);if(t instanceof c.Error)return ie.triggerError(t),!1;break;default:te&&(Z=!0)}return!0}function x(){var e=ee.get("state");return e===f.IDLE||e===f.COMPLETE||e===f.ERROR}function C(e){ee.get("state")!==f.ERROR&&(ee.get("scrubbing")||ee.get("state")===f.PLAYING||A(!0),re().seek(e))}function P(e,t){L(!0),R(e),A(t)}function T(e){var t=s(e);return t=s.filterPlaylist(t,ee.getProviders(),ee.get("androidhls"),ee.get("drm"),ee.get("preload"),ee.get("feedid")),ee.set("playlist",t),n.isArray(t)&&0!==t.length?!0:(ie.triggerError({message:"Error loading playlist: No playable sources found"}),!1)}function R(e){var t=ee.get("playlist");e=parseInt(e,10)||0,e=(e+t.length)%t.length,ee.set("item",e),ee.set("playlistItem",t[e]),ee.setActiveItem(t[e])}function I(e){P(ee.get("item")-1,e||{reason:"external"})}function M(e){P(ee.get("item")+1,e||{reason:"external"})}function S(){if(x()){if(ne)return void(ne=!1);$=S;var e=ee.get("item");return e===ee.get("playlist").length-1?void(ee.get("repeat")?M({reason:"repeat"}):(ee.set("state",f.COMPLETE),ie.trigger(g.JWPLAYER_PLAYLIST_COMPLETE,{}))):void M({reason:"playlist"})}}function O(e){re()&&(e=parseInt(e,10)||0,re().setCurrentQuality(e))}function D(){return re()?re().getCurrentQuality():-1}function Y(){return this._model?this._model.getConfiguration():void 0}function N(){if(this._model.mediaModel)return this._model.mediaModel.get("visualQuality");var e=W();if(e){var t=D(),i=e[t];if(i)return{level:n.extend({index:t},i),mode:"",reason:""}}return null}function W(){return re()?re().getQualityLevels():null}function F(e){re()&&(e=parseInt(e,10)||0,re().setCurrentAudioTrack(e))}function J(){return re()?re().getCurrentAudioTrack():-1}function V(){return re()?re().getAudioTracks():null}function B(e){e=parseInt(e,10)||0,ee.persistVideoSubtitleTrack(e),ie.trigger(g.JWPLAYER_CAPTIONS_CHANGED,{tracks:H(),track:e})}function U(){return q.getCurrentIndex()}function H(){return q.getCaptionsList()}function z(){var e=ee.getVideo();if(e){var t=e.detachMedia();if(t instanceof HTMLVideoElement)return t}return null}function G(){var e=c.tryCatch(function(){ee.getVideo().attachMedia()});return e instanceof c.Error?void c.log("Error calling _attachMedia",e):void("function"==typeof $&&$())}function K(e){n.isBoolean(e)||(e=!ee.get("fullscreen")),ee.set("fullscreen",e),this._instreamAdapter&&this._instreamAdapter._adModel&&this._instreamAdapter._adModel.set("fullscreen",e)}var Q,q,X,$,Z,ee=this._model,te=!1,ne=!1,ie=this,re=function(){return ee.getVideo()},oe=new a;oe.track(ee);var ae=new e(o,oe);ee.setup(ae,oe),Q=this._view=new u(p,ee),q=new r(p,ee),X=new i(p,ee,Q,T),X.on(g.JWPLAYER_READY,v,this),X.on(g.JWPLAYER_SETUP_ERROR,this.setupError,this),ee.mediaController.on(g.JWPLAYER_MEDIA_COMPLETE,function(){n.defer(S)}),ee.mediaController.on(g.JWPLAYER_MEDIA_ERROR,this.triggerError,this),ee.on("change:flashBlocked",function(e,t){if(!t)return void this._model.set("errorEvent",void 0);var n=!!e.get("flashThrottle"),i={message:n?"Click to run Flash":"Flash plugin failed to load"};n||this.trigger(g.JWPLAYER_ERROR,i),this._model.set("errorEvent",i)},this),m(),ee.on("change:mediaModel",m),this._play=A,this._pause=_,this._seek=C,this._stop=L,this._load=b,this._next=M,this._prev=I,this._item=P,this._setCurrentCaptions=B,this._setCurrentQuality=O,this.detachMedia=z,this.attachMedia=G,this.getCurrentQuality=D,this.getQualityLevels=W,this.setCurrentAudioTrack=F,this.getCurrentAudioTrack=J,this.getAudioTracks=V,this.getCurrentCaptions=U,this.getCaptionsList=H,this.getVisualQuality=N,this.getConfig=Y,this.getState=k,this._setVolume=ee.setVolume,this._setMute=ee.setMute,this.getProvider=function(){return ee.get("provider")},this.getWidth=function(){return ee.get("containerWidth")},this.getHeight=function(){return ee.get("containerHeight")},this.getContainer=function(){return this.currentContainer},this.resize=Q.resize,this.getSafeRegion=Q.getSafeRegion,this.setCues=Q.addCues,this._setFullscreen=K,this.addButton=function(e,t,i,r,o){var a={img:e,tooltip:t,callback:i,id:r,btnClass:o},s=ee.get("dock");s=s?s.slice(0):[],s=n.reject(s,n.matches({id:a.id})),s.push(a),ee.set("dock",s)},this.removeButton=function(e){var t=ee.get("dock")||[];t=n.reject(t,n.matches({id:e})),ee.set("dock",t)},this.checkBeforePlay=function(){return te},this.getItemQoe=function(){return ee._qoeItem},this.setControls=function(e){n.isBoolean(e)||(e=!ee.get("controls")),ee.set("controls",e);var t=ee.getVideo();t&&t.setControls(e)},this.playerDestroy=function(){this.stop(),this.showView(this.originalContainer),Q&&Q.destroy(),ee&&ee.destroy(),X&&(X.destroy(),X=null)},this.isBeforePlay=this.checkBeforePlay,this.isBeforeComplete=function(){return ee.getVideo().checkComplete()},this.createInstream=function(){return this.instreamDestroy(),this._instreamAdapter=new t(this,ee,Q),this._instreamAdapter},this.skipAd=function(){this._instreamAdapter&&this._instreamAdapter.skipAd()},this.instreamDestroy=function(){ie._instreamAdapter&&ie._instreamAdapter.destroy()},X.start()},showView:function(e){(document.documentElement.contains(this.currentContainer)||(this.currentContainer=document.getElementById(this._model.get("id")),this.currentContainer))&&(this.currentContainer.parentElement&&this.currentContainer.parentElement.replaceChild(e,this.currentContainer),this.currentContainer=e)},triggerError:function(e){this._model.set("errorEvent",e),this._model.set("state",f.ERROR),this._model.once("change:state",function(){this._model.set("errorEvent",void 0)},this),this.trigger(g.JWPLAYER_ERROR,e)},setupError:function(e){var t=e.message,i=c.createElement(m(this._model.get("id"),this._model.get("skin"),t)),r=this._model.get("width"),o=this._model.get("height");c.style(i,{width:r.toString().indexOf("%")>0?r:r+"px",height:o.toString().indexOf("%")>0?o:o+"px"}),this.showView(i);var a=this;n.defer(function(){a.trigger(g.JWPLAYER_SETUP_ERROR,{message:t})})}},y}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(270),n(269),n(4),n(6),n(2),n(3),n(1)],r=function(e,t,n,i,r,o,a){function s(n){var i=n.get("provider").name||"";return i.indexOf("flash")>=0?t:e}var l={skipoffset:null,tag:null},c=function(e,t,o){function c(e,t){t=t||{},j.tag&&!t.tag&&(t.tag=j.tag),this.trigger(e,t)}function u(e){w._adModel.set("duration",e.duration),w._adModel.set("position",e.position)}function d(e){if(p&&y+1<p.length){w._adModel.set("state","buffering"),t.set("skipButton",!1),y++;var i,r=p[y];h&&(i=h[y]),this.loadItem(r,i)}else e.type===n.JWPLAYER_MEDIA_COMPLETE&&(c.call(this,e.type,e),this.trigger(n.JWPLAYER_PLAYLIST_COMPLETE,{})),this.destroy()}var p,h,f,g,m,v=s(t),w=new v(e,t),y=0,j={},b=a.bind(function(e){e=e||{},e.hasControls=!!t.get("controls"),this.trigger(n.JWPLAYER_INSTREAM_CLICK,e),w&&w._adModel&&(w._adModel.get("state")===i.PAUSED?e.hasControls&&w.instreamPlay():w.instreamPause())},this),E=a.bind(function(){w&&w._adModel&&w._adModel.get("state")===i.PAUSED&&t.get("controls")&&(e.setFullscreen(),e.play())},this);this.type="instream",this.init=function(){f=t.getVideo(),g=t.get("position"),m=t.get("playlist")[t.get("item")],w.on("all",c,this),w.on(n.JWPLAYER_MEDIA_TIME,u,this),w.on(n.JWPLAYER_MEDIA_COMPLETE,d,this),w.init(),f.detachMedia(),t.mediaModel.set("state",i.BUFFERING),e.checkBeforePlay()||0===g&&!f.checkComplete()?(g=0,t.set("preInstreamState","instream-preroll")):f&&f.checkComplete()||t.get("state")===i.COMPLETE?t.set("preInstreamState","instream-postroll"):t.set("preInstreamState","instream-midroll");var a=t.get("state");return a!==i.PLAYING&&a!==i.BUFFERING||f.pause(),o.setupInstream(w._adModel),w._adModel.set("state",i.BUFFERING),o.clickHandler().setAlternateClickHandlers(r.noop,null),this.setText("Loading ad"),this},this.loadItem=function(e,i){if(r.isAndroid(2.3))return void this.trigger({type:n.JWPLAYER_ERROR,message:"Error loading instream: Cannot play instream on Android 2.3"});a.isArray(e)&&(p=e,h=i,e=p[y],h&&(i=h[y])),this.trigger(n.JWPLAYER_PLAYLIST_ITEM,{index:y,item:e}),j=a.extend({},l,i),w.load(e),this.addClickHandler();var o=e.skipoffset||j.skipoffset;o&&(w._adModel.set("skipMessage",j.skipMessage),w._adModel.set("skipText",j.skipText),w._adModel.set("skipOffset",o),t.set("skipButton",!0))},this.applyProviderListeners=function(e){w.applyProviderListeners(e),this.addClickHandler()},this.play=function(){w.instreamPlay()},this.pause=function(){w.instreamPause()},this.hide=function(){w.hide()},this.addClickHandler=function(){o.clickHandler().setAlternateClickHandlers(b,E),w.on(n.JWPLAYER_MEDIA_META,this.metaHandler,this)},this.skipAd=function(e){var t=n.JWPLAYER_AD_SKIPPED;this.trigger(t,e),d.call(this,{type:t})},this.metaHandler=function(e){e.width&&e.height&&o.resizeMedia()},this.destroy=function(){if(this.off(),t.set("skipButton",!1),w){o.clickHandler()&&o.clickHandler().revertAlternateClickHandlers(),w.instreamDestroy(),o.destroyInstream(),w=null,e.attachMedia();var n=t.get("preInstreamState");switch(n){case"instream-preroll":case"instream-midroll":var s=a.extend({},m);s.starttime=g,t.loadVideo(s),r.isMobile()&&t.mediaModel.get("state")===i.BUFFERING&&f.setState(i.BUFFERING),f.play();break;case"instream-postroll":case"instream-idle":f.stop()}}},this.getState=function(){return w&&w._adModel?w._adModel.get("state"):!1},this.setText=function(e){o.setAltText(e?e:"")},this.hide=function(){o.useExternalControls()}};return a.extend(c.prototype,o),c}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(3),n(55),n(83),n(4),n(6),n(2),n(1)],r=function(e,t,n,i,r,o,a){var s=function(e,i){this.model=i,this._adModel=(new t).setup({id:i.get("id"),volume:i.get("volume"),fullscreen:i.get("fullscreen"),mute:i.get("mute")}),this._adModel.on("change:state",n,this);var r=e.getContainer();this.swf=r.querySelector("object")};return s.prototype=a.extend({init:function(){if(o.isChrome()){var e=-1,t=!1;this.swf.on("throttle",function(n){if(clearTimeout(e),"resume"===n.state)t&&(t=!1,this.instreamPlay());else{var i=this;e=setTimeout(function(){i._adModel.get("state")===r.PLAYING&&(t=!0,i.instreamPause())},250)}},this)}this.swf.on("instream:state",function(e){switch(e.newstate){case r.PLAYING:this._adModel.set("state",e.newstate);break;case r.PAUSED:this._adModel.set("state",e.newstate)}},this).on("instream:time",function(e){this._adModel.set("position",e.position),this._adModel.set("duration",e.duration),this.trigger(i.JWPLAYER_MEDIA_TIME,e)},this).on("instream:complete",function(e){this.trigger(i.JWPLAYER_MEDIA_COMPLETE,e)},this).on("instream:error",function(e){this.trigger(i.JWPLAYER_MEDIA_ERROR,e)},this),this.swf.triggerFlash("instream:init"),this.applyProviderListeners=function(e){this.model.on("change:volume",function(t,n){e.volume(n)},this),this.model.on("change:mute",function(t,n){e.mute(n)},this)}},instreamDestroy:function(){this._adModel&&(this.off(),this.swf.off(null,null,this),this.swf.triggerFlash("instream:destroy"),this.swf=null,this._adModel.off(),this._adModel=null,this.model=null)},load:function(e){this.swf.triggerFlash("instream:load",e)},instreamPlay:function(){this.swf.triggerFlash("instream:play")},instreamPause:function(){this.swf.triggerFlash("instream:pause")}},e),s}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(3),n(83),n(4),n(6),n(55)],r=function(e,t,n,i,r,o){var a=function(a,s){function l(t){var r=t||p.getVideo();if(h!==r){if(h=r,!r)return;r.off(),r.on("all",function(t,n){n=e.extend({},n,{type:t}),this.trigger(t,n)},f),r.on(i.JWPLAYER_MEDIA_BUFFER_FULL,d),r.on(i.JWPLAYER_PLAYER_STATE,c),r.attachMedia(),r.volume(s.get("volume")),r.mute(s.get("mute")),p.on("change:state",n,f)}}function c(e){switch(e.newstate){case r.PLAYING:p.set("state",e.newstate);break;case r.PAUSED:p.set("state",e.newstate)}}function u(e){s.trigger(e.type,e),f.trigger(i.JWPLAYER_FULLSCREEN,{fullscreen:e.jwstate})}function d(){p.getVideo().play()}var p,h,f=e.extend(this,t);return a.on(i.JWPLAYER_FULLSCREEN,function(e){this.trigger(i.JWPLAYER_FULLSCREEN,e)},f),this.init=function(){p=(new o).setup({id:s.get("id"),volume:s.get("volume"),fullscreen:s.get("fullscreen"),mute:s.get("mute")}),p.on("fullscreenchange",u),this._adModel=p},f.load=function(e){p.set("item",0),p.set("playlistItem",e),p.setActiveItem(e),l(),p.off(i.JWPLAYER_ERROR),p.on(i.JWPLAYER_ERROR,function(e){this.trigger(i.JWPLAYER_ERROR,e)},f),p.loadVideo(e)},f.applyProviderListeners=function(e){l(e),e.off(i.JWPLAYER_ERROR),e.on(i.JWPLAYER_ERROR,function(e){this.trigger(i.JWPLAYER_ERROR,e)},f),s.on("change:volume",function(e,t){h.volume(t)},f),s.on("change:mute",function(e,t){h.mute(t)},f)},this.instreamDestroy=function(){p&&(p.off(),this.off(),h&&(h.detachMedia(),h.off(),p.getVideo()&&h.destroy()),p=null,a.off(null,null,this),a=null)},f.instreamPlay=function(){p.getVideo()&&p.getVideo().play(!0)},f.instreamPause=function(){p.getVideo()&&p.getVideo().pause(!0)},f};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(131),n(4),n(1)],r=function(e,t,n){function i(e){e.mediaController.off(t.JWPLAYER_MEDIA_PLAY_ATTEMPT,e._onPlayAttempt),e.mediaController.off(t.JWPLAYER_PROVIDER_FIRST_FRAME,e._triggerFirstFrame),e.mediaController.off(t.JWPLAYER_MEDIA_TIME,e._onTime)}function r(e){i(e),e._triggerFirstFrame=n.once(function(){var n=e._qoeItem;n.tick(t.JWPLAYER_MEDIA_FIRST_FRAME);var r=n.between(t.JWPLAYER_MEDIA_PLAY_ATTEMPT,t.JWPLAYER_MEDIA_FIRST_FRAME);e.mediaController.trigger(t.JWPLAYER_MEDIA_FIRST_FRAME,{loadTime:r}),i(e)}),e._onTime=a(e._triggerFirstFrame),e._onPlayAttempt=function(){e._qoeItem.tick(t.JWPLAYER_MEDIA_PLAY_ATTEMPT)},e.mediaController.on(t.JWPLAYER_MEDIA_PLAY_ATTEMPT,e._onPlayAttempt),e.mediaController.once(t.JWPLAYER_PROVIDER_FIRST_FRAME,e._triggerFirstFrame),e.mediaController.on(t.JWPLAYER_MEDIA_TIME,e._onTime)}function o(n){function i(n,i,o){n._qoeItem&&o&&n._qoeItem.end(o.get("state")),n._qoeItem=new e,n._qoeItem.tick(t.JWPLAYER_PLAYLIST_ITEM),n._qoeItem.start(i.get("state")),r(n),i.on("change:state",function(e,t,i){n._qoeItem.end(i),n._qoeItem.start(t)})}n.on("change:mediaModel",i)}var a=function(e){var t=Number.MIN_VALUE;return function(n){n.position>t&&e(),t=n.position}};return{model:o}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(33),n(41),n(121),n(32),n(44),n(1),n(2),n(4)],r=function(e,t,i,r,o,a,s,l){function c(){var e={LOAD_PROMISE_POLYFILL:{method:u,depends:[]},LOAD_BASE64_POLYFILL:{method:d,depends:[]},LOADED_POLYFILLS:{method:p,depends:["LOAD_PROMISE_POLYFILL","LOAD_BASE64_POLYFILL"]},LOAD_PLUGINS:{method:h,depends:["LOADED_POLYFILLS"]},INIT_PLUGINS:{method:f,depends:["LOAD_PLUGINS","SETUP_VIEW"]},LOAD_PROVIDERS:{method:E,depends:["FILTER_PLAYLIST"]},LOAD_SKIN:{method:b,depends:["LOADED_POLYFILLS"]},LOAD_PLAYLIST:{method:m,depends:["LOADED_POLYFILLS"]},FILTER_PLAYLIST:{method:v,depends:["LOAD_PLAYLIST"]},SETUP_VIEW:{method:k,depends:["LOAD_SKIN"]},SEND_READY:{method:A,depends:["INIT_PLUGINS","LOAD_PROVIDERS","SETUP_VIEW"]}};return e}function u(e){window.Promise?e():n.e(5,function(require){n(126),e()})}function d(e){window.btoa&&window.atob?e():n.e(6,function(require){n(125),e()})}function p(e){e()}function h(e,n){_=t.loadPlugins(n.get("id"),n.get("plugins")),_.on(l.COMPLETE,e),_.on(l.ERROR,a.partial(g,e)),_.load()}function f(e,t,n){_.setupPlugins(n,t),e()}function g(e,t){L(e,"Could not load plugin",t.message)}function m(e,t){var n=t.get("playlist");a.isString(n)?(x=new i,x.on(l.JWPLAYER_PLAYLIST_LOADED,function(n){t.set("playlist",n.playlist),t.set("feedid",n.feedid),e()}),x.on(l.JWPLAYER_ERROR,a.partial(w,e)),x.load(n)):e()}function v(e,t,n,i,r){var o=t.get("playlist"),a=r(o);a?e():w(e)}function w(e,t){t&&t.message?L(e,"Error loading playlist",t.message):L(e,"Error loading player","No playable sources found")}function y(e,t){return a.contains(o.SkinsLoadable,e)?t+"skins/"+e+".css":void 0}function j(e){for(var t=document.styleSheets,n=0,i=t.length;i>n;n++)if(t[n].href===e)return!0;return!1}function b(e,t){var n=t.get("skin"),i=t.get("skinUrl");if(a.contains(o.SkinsIncluded,n))return void e();if(i||(i=y(n,t.get("base"))),a.isString(i)&&!j(i)){t.set("skin-loading",!0);var s=!0,c=new r(i,s);c.addEventListener(l.COMPLETE,function(){t.set("skin-loading",!1)}),c.addEventListener(l.ERROR,function(){t.set("skin","seven"),t.set("skin-loading",!1)}),c.load()}a.defer(function(){e()})}function E(t,n){var i=n.getProviders(),r=n.get("playlist"),o=i.required(r);e.load(o).then(t)}function k(e,t,n,i){i.setup(),e()}function A(e){e({type:"complete"})}function L(e,t,n){e({type:"error",msg:t,reason:n})}var _,x;return{getQueue:c,error:L}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2)],r=function(e,t){function n(e){return"jwplayer."+e}function i(){return e.reduce(this.persistItems,function(e,i){var r=c[n(i)];return r&&(e[i]=t.serialize(r)),e},{})}function r(e,t){try{c[n(e)]=t}catch(i){l&&l.debug&&console.error(i)}}function o(){e.each(this.persistItems,function(e){c.removeItem(n(e))})}function a(){this.persistItems=["volume","mute","captionLabel","qualityLabel"]}function s(t){e.each(this.persistItems,function(e){t.on("change:"+e,function(t,n){r(e,n)})})}var l=window.jwplayer,c={removeItem:t.noop};try{c=window.localStorage}catch(u){}return e.extend(a.prototype,{getAllItems:i,track:s,clear:o}),a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(135),n(2)],r=function(e,t){return n.p=t.loadFrom(),e.selectPlayer}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(14)],r=function(e){function t(e){e||n()}function n(){throw new Error("Invalid DFXP file")}var i=e.seconds;return function(r){t(r);var o=[],a=r.getElementsByTagName("p");t(a),a.length||(a=r.getElementsByTagName("tt:p"),a.length||(a=r.getElementsByTagName("tts:p")));for(var s=0;s<a.length;s++){var l=a[s],c=l.innerHTML||l.textContent||l.text||"",u=e.trim(c).replace(/>\s+</g,"><").replace(/tts?:/g,"");if(u){var d=l.getAttribute("begin"),p=l.getAttribute("dur"),h=l.getAttribute("end"),f={begin:i(d),text:u};h?f.end=i(h):p&&(f.end=f.begin+i(p)),o.push(f)}}return o.length||n(),o}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(40),n(14),n(2)],r=function(e,t,n){var i="jwplayer",r=function(r,o){for(var a=[],s=[],l=t.xmlAttribute,c="default",u="label",d="file",p="type",h=0;h<r.childNodes.length;h++){var f=r.childNodes[h];if(f.prefix===i){var g=e.localName(f);"source"===g?(delete o.sources,a.push({file:l(f,d),"default":l(f,c),label:l(f,u),type:l(f,p)})):"track"===g?(delete o.tracks,s.push({file:l(f,d),"default":l(f,c),kind:l(f,"kind"),label:l(f,u)})):(o[g]=n.serialize(e.textContent(f)),"file"===g&&o.sources&&delete o.sources)}o[d]||(o[d]=o.link)}if(a.length)for(o.sources=[],h=0;h<a.length;h++)a[h].file.length>0&&(a[h][c]="true"===a[h][c],a[h].label.length||delete a[h].label,o.sources.push(a[h]));if(s.length)for(o.tracks=[],h=0;h<s.length;h++)s[h].file.length>0&&(s[h][c]="true"===s[h][c],s[h].kind=s[h].kind.length?s[h].kind:"captions",s[h].label.length||delete s[h].label,o.tracks.push(s[h]));return o};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(40),n(14),n(2)],r=function(e,t,n){function i(e){for(var t=[],n=0;n<s(e);n++){var i=e.childNodes[n];"jwplayer"===i.prefix&&"mediatypes"===o(i).toLowerCase()&&t.push(a(i))}return t}var r=t.xmlAttribute,o=e.localName,a=e.textContent,s=e.numChildren,l="media",c=function(e,t){function u(e){var t={zh:"Chinese",nl:"Dutch",en:"English",fr:"French",de:"German",it:"Italian",ja:"Japanese",pt:"Portuguese",ru:"Russian",es:"Spanish"};return t[e]?t[e]:e}var d,p,h="tracks",f=[];for(p=0;p<s(e);p++)if(d=e.childNodes[p],d.prefix===l){if(!o(d))continue;switch(o(d).toLowerCase()){case"content":if(r(d,"duration")&&(t.duration=n.seconds(r(d,"duration"))),r(d,"url")){t.sources||(t.sources=[]);var g={file:r(d,"url"),type:r(d,"type"),width:r(d,"width"),label:r(d,"label")},m=i(d);m.length&&(g.mediaTypes=m),t.sources.push(g)}s(d)>0&&(t=c(d,t));break;case"title":t.title=a(d);break;case"description":t.description=a(d);break;case"guid":t.mediaid=a(d);break;case"thumbnail":t.image||(t.image=r(d,"url"));break;case"player":break;case"group":c(d,t);break;case"subtitle":var v={};v.file=r(d,"url"),v.kind="captions",r(d,"lang").length>0&&(v.label=u(r(d,"lang"))),f.push(v)}}for(t.hasOwnProperty(h)||(t[h]=[]),p=0;p<f.length;p++)t[h].push(f[p]);return t};return c}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t={kind:"captions","default":!1},n=function(n){return n&&n.file?e.extend({},t,n):void 0};return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(56),n(2),n(4),n(3),n(1),n(32)],r=function(e,t,n,i,r,o){function a(e,t,n){return function(){var i=e.getContainer().getElementsByClassName("jw-overlays")[0];i&&(i.appendChild(n),n.left=i.style.left,n.top=i.style.top,t.displayArea=i)}}function s(e){function t(){var t=e.displayArea;t&&e.resize(t.clientWidth,t.clientHeight)}return function(){t(),setTimeout(t,400)}}var l=function(l,c){function u(){m||(m=!0,g=o.loaderstatus.COMPLETE,f.trigger(n.COMPLETE))}function d(){if(!w&&(c&&0!==r.keys(c).length||u(),!m)){var i=l.getPlugins();h=r.after(v,u),r.each(c,function(r,a){var s=e.getPluginName(a),l=i[s],c=l.getJS(),u=l.getTarget(),d=l.getStatus();d!==o.loaderstatus.LOADING&&d!==o.loaderstatus.NEW&&(c&&!t.versionCheck(u)&&f.trigger(n.ERROR,{message:"Incompatible player version"}),h())})}}function p(e){if(!w){var i="File not found";e.url&&t.log(i,e.url),this.off(),this.trigger(n.ERROR,{message:i}),d()}}var h,f=r.extend(this,i),g=o.loaderstatus.NEW,m=!1,v=r.size(c),w=!1;this.setupPlugins=function(n,i){var o=[],c=l.getPlugins(),u=i.get("plugins");r.each(u,function(i,l){var d=e.getPluginName(l),p=c[d],h=p.getFlashPath(),f=p.getJS(),g=p.getURL();if(h){var m=r.extend({name:d,swf:h,pluginmode:p.getPluginmode()},i);o.push(m)}var v=t.tryCatch(function(){if(f&&u[g]){var e=document.createElement("div");e.id=n.id+"_"+d,e.className="jw-plugin jw-reset";var t=r.extend({},u[g]),i=p.getNewInstance(n,t,e);i.addToPlayer=a(n,i,e),i.resizeHandler=s(i),n.addPlugin(d,i,e)}});v instanceof t.Error&&t.log("ERROR: Failed to load "+d+".")}),i.set("flashPlugins",o)},this.load=function(){if(t.exists(c)&&"object"!==t.typeOf(c))return void d();g=o.loaderstatus.LOADING,r.each(c,function(e,i){if(t.exists(i)){var r=l.addPlugin(i);r.on(n.COMPLETE,d),r.on(n.ERROR,p)}});var e=l.getPlugins();r.each(e,function(e){e.load()}),d()},this.destroy=function(){w=!0,this.off()},this.getStatus=function(){return g}};return l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(56),n(124)],r=function(e,t){var n=function(n){this.addPlugin=function(i){var r=e.getPluginName(i);return n[r]||(n[r]=new t(i)),n[r]},this.getPlugins=function(){return n}};return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(43),n(42)],r=function(e,t){var n={html5:e,flash:t};return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(1),n(133)],r=function(e,t,n){function i(t){if("hls"===t.type)if(t.androidhls!==!1){var n=e.isAndroidNative;if(n(2)||n(3)||n("4.0"))return!1;if(e.isAndroid())return!0}else if(e.isAndroid())return!1;return null}var r=[{name:"youtube",supports:function(t){return e.isYouTube(t.file,t.type)}},{name:"html5",supports:function(t){var r={aac:"audio/mp4",mp4:"video/mp4",f4v:"video/mp4",m4v:"video/mp4",mov:"video/mp4",mp3:"audio/mpeg",mpeg:"audio/mpeg",ogv:"video/ogg",ogg:"video/ogg",oga:"video/ogg",vorbis:"video/ogg",webm:"video/webm",f4a:"video/aac",m3u8:"application/vnd.apple.mpegurl",m3u:"application/vnd.apple.mpegurl",hls:"application/vnd.apple.mpegurl"},o=t.file,a=t.type,s=i(t);if(null!==s)return s;if(e.isRtmp(o,a))return!1;if(!r[a])return!1;if(n.canPlayType){var l=n.canPlayType(r[a]);return!!l}return!1}},{name:"flash",supports:function(n){var i={flv:"video",f4v:"video",mov:"video",m4a:"video",m4v:"video",mp4:"video",aac:"video",f4a:"video",mp3:"sound",mpeg:"sound",smil:"rtmp"},r=t.keys(i);if(!e.isFlashSupported())return!1;var o=n.file,a=n.type;return e.isRtmp(o,a)?!0:t.contains(r,a)}}];return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(31),n(89),n(281),n(1)],r=function(e,t,i,r){function o(e){this.providers=t.slice(),this.config=e||{},this.reorderProviders()}return o.registerProvider=function(n){var o=n.getName().name;if(!i[o]){if(!r.find(t,r.matches({name:o}))){if(!r.isFunction(n.supports))throw{message:"Tried to register a provider with an invalid object"};t.unshift({name:o,supports:n.supports})}var a=function(){};a.prototype=e,n.prototype=new a,i[o]=n}},o.load=function(e){return Promise.all(r.map(e,function(e){return new Promise(function(t){switch(e.name){case"html5":!function(require){t(n(43))}(n);break;case"flash":!function(require){t(n(42))}(n);break;case"youtube":n.e(0,function(require){t(n(57))});break;default:t()}}).then(function(e){e&&o.registerProvider(e)})}))},r.extend(o.prototype,{reorderProviders:function(){if("flash"===this.config.primary){var e=r.indexOf(this.providers,r.findWhere(this.providers,{name:"flash"})),t=this.providers.splice(e,1)[0],n=r.indexOf(this.providers,r.findWhere(this.providers,{name:"html5"}));this.providers.splice(n,0,t)}},providerSupports:function(e,t){return e.supports(t)},required:function(e,t){return e=e.slice(),r.compact(r.map(this.providers,function(n){for(var i=!1,r=e.length;r--;){var o=e[r],a=n.supports(o.sources[0],t);a&&e.splice(r,1),i=i||a}return i?n:void 0}))},choose:function(e){e=r.isObject(e)?e:{};for(var t=this.providers.length,n=0;t>n;n++){var o=this.providers[n];if(this.providerSupports(o,e)){var a=t-n-1;return{priority:a,name:o.name,type:e.type,provider:i[o.name]}}}return null}}),o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(86)],r=function(e,t){function n(e){e.onload=null,e.onprogress=null,e.onreadystatechange=null,e.onerror=null,"abort"in e&&e.abort()}function i(t,i){return function(r){var o=r.currentTarget||i.xhr;if(clearTimeout(i.timeoutId),i.retryWithoutCredentials&&i.xhr.withCredentials){n(o);var a=e.extend({},i,{xhr:null,withCredentials:!1,retryWithoutCredentials:!1});return void d(a)}i.onerror(t,i.url,o)}}function r(e){return function(t){var n=t.currentTarget||e.xhr;if(4===n.readyState){if(clearTimeout(e.timeoutId),n.status>=400){var i;return i=404===n.status?"File not found":""+n.status+"("+n.statusText+")",e.onerror(i,e.url,n)}if(200===n.status)return o(e)(t)}}}function o(e){return function(n){var i=n.currentTarget||e.xhr;if(clearTimeout(e.timeoutId),e.responseType){if("json"===e.responseType)return a(i,e)}else{var r,o=i.responseXML;if(o)try{r=o.firstChild}catch(l){}if(o&&r)return s(i,o,e);if(c&&i.responseText&&!o&&(o=t.parseXML(i.responseText),o&&o.firstChild))return s(i,o,e);if(e.requireValidXML)return void e.onerror("Invalid XML",e.url,i)}e.oncomplete(i)}}function a(t,n){if(!t.response||e.isString(t.response)&&'"'!==t.responseText.substr(1))try{t=e.extend({},t,{response:JSON.parse(t.responseText)})}catch(i){return void n.onerror("Invalid JSON",n.url,t)}return n.oncomplete(t)}function s(t,n,i){var r=n.documentElement;return i.requireValidXML&&("parsererror"===r.nodeName||r.getElementsByTagName("parsererror").length)?void i.onerror("Invalid XML",i.url,t):(t.responseXML||(t=e.extend({},t,{responseXML:n})),i.oncomplete(t))}var l=function(){},c=!1,u=function(e){var t=document.createElement("a"),n=document.createElement("a");t.href=location.href;try{return n.href=e,n.href=n.href,t.protocol+"//"+t.host!=n.protocol+"//"+n.host}catch(i){}return!0},d=function(t,a,s,d){e.isObject(t)&&(d=t,t=d.url);var p,h=e.extend({xhr:null,url:t,withCredentials:!1,retryWithoutCredentials:!1,timeout:6e4,timeoutId:-1,oncomplete:a||l,onerror:s||l,mimeType:d&&!d.responseType?"text/xml":"",requireValidXML:!1,responseType:d&&d.plainText?"text":""},d);if("XDomainRequest"in window&&u(t))p=h.xhr=new window.XDomainRequest,p.onload=o(h),p.ontimeout=p.onprogress=l,c=!0;else{if(!("XMLHttpRequest"in window))return void h.onerror("",t);p=h.xhr=new window.XMLHttpRequest,p.onreadystatechange=r(h)}var f=i("Error loading file",h);p.onerror=f,"overrideMimeType"in p?h.mimeType&&p.overrideMimeType(h.mimeType):c=!0;try{t=t.replace(/#.*$/,""),p.open("GET",t,!0)}catch(g){return f(g),p}if(h.responseType)try{p.responseType=h.responseType}catch(g){}h.timeout&&(h.timeoutId=setTimeout(function(){n(p),h.onerror("Timeout",t,p)},h.timeout));try{h.withCredentials&&"withCredentials"in p&&(p.withCredentials=!0),p.send()}catch(g){f(g)}return p};return{ajax:d,crossdomain:u}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(3),n(1)],r=function(e,t,n){function i(e,t,n){var i=document.createElement("param");i.setAttribute("name",t),i.setAttribute("value",n),e.appendChild(i)}function r(r,o,l,c){var u;if(c=c||"opaque",e.isMSIE()){var d=document.createElement("div");o.appendChild(d),d.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%" id="'+l+'" name="'+l+'" tabindex="0"><param name="movie" value="'+r+'"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="wmode" value="'+c+'"><param name="bgcolor" value="'+s+'"><param name="menu" value="false"></object>';for(var p=o.getElementsByTagName("object"),h=p.length;h--;)p[h].id===l&&(u=p[h])}else u=document.createElement("object"),u.setAttribute("type","application/x-shockwave-flash"),u.setAttribute("data",r),u.setAttribute("width","100%"),u.setAttribute("height","100%"),u.setAttribute("bgcolor",s),u.setAttribute("id",l),u.setAttribute("name",l),i(u,"allowfullscreen","true"),i(u,"allowscriptaccess","always"),i(u,"wmode",c),i(u,"menu","false"),o.appendChild(u,o);return u.className="jw-swf jw-reset",u.style.display="block",u.style.position="absolute",u.style.left=0,u.style.right=0,u.style.top=0,u.style.bottom=0,n.extend(u,t),u.queueCommands=!0,u.triggerFlash=function(t){var i=this;if("setup"!==t&&i.queueCommands||!i.__externalCall){for(var r=i.__commandQueue,o=r.length;o--;)r[o][0]===t&&r.splice(o,1);return r.push(Array.prototype.slice.call(arguments)),i}var s=Array.prototype.slice.call(arguments,1),l=e.tryCatch(function(){if(s.length){for(var e=s.length;e--;)"object"==typeof s[e]&&n.each(s[e],a);var r=JSON.stringify(s);i.__externalCall(t,r)}else i.__externalCall(t)});return l instanceof e.Error&&(console.error(t,l),"setup"===t)?(l.name="Failed to setup flash",l):i},u.__commandQueue=[],u}function o(e){e&&e.parentNode&&(e.style.display="none",e.parentNode.removeChild(e))}function a(e,t,n){e instanceof window.HTMLElement&&delete n[t]}var s="#000000";return{embed:r,remove:o}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return{hasClass:function(e,t){var n=" "+t+" ";return 1===e.nodeType&&(" "+e.className+" ").replace(/[\t\r\n\f]/g," ").indexOf(n)>=0}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){
	var i,r;i=[n(1),n(3)],r=function(e,t){var n=e.extend({get:function(e){return this.attributes=this.attributes||{},this.attributes[e]},set:function(e,t){if(this.attributes=this.attributes||{},this.attributes[e]!==t){var n=this.attributes[e];this.attributes[e]=t,this.trigger("change:"+e,this,t,n)}},clone:function(){return e.clone(this.attributes)}},t);return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(45),n(6),n(1)],r=function(e,t,n,i){var r=t.style,o={back:!0,fontSize:15,fontFamily:"Arial,sans-serif",fontOpacity:100,color:"#FFF",backgroundColor:"#000",backgroundOpacity:100,edgeStyle:null,windowColor:"#FFF",windowOpacity:0,preprocessor:i.identity},a=function(a){function s(t){t=t||"";var n="jw-captions-window jw-reset";t?(y.innerHTML=t,w.className=n+" jw-captions-window-active"):(w.className=n,e.empty(y))}function l(e){m=e,u(f,m)}function c(e,t){var n=e.source,r=t.metadata;return n?r&&i.isNumber(r[n])?r[n]:!1:t.position}function u(e,t){if(e&&e.data&&t){var n=c(e,t);if(n!==!1){var i=e.data;if(!(g>=0&&d(i,g,n))){for(var r=-1,o=0;o<i.length;o++)if(d(i,o,n)){r=o;break}-1===r?s(""):r!==g&&(g=r,s(j.preprocessor(i[g].text)))}}}}function d(e,t,n){return e[t].begin<=n&&(!e[t].end||e[t].end>=n)&&(t===e.length-1||e[t+1].begin>=n)}function p(e,n,i){if(t.css("#"+e+" .jw-video::-webkit-media-text-track-display",n),t.css("#"+e+" .jw-video::cue",i),i.backgroundColor){var r="{background-color: "+i.backgroundColor+" !important;}";t.css("#"+e+" .jw-video::-webkit-media-text-track-display-backdrop",r)}}function h(e,n,i){var r=t.hexToRgba("#000000",i);"dropshadow"===e?n.textShadow="0 2px 1px "+r:"raised"===e?n.textShadow="0 0 5px "+r+", 0 1px 5px "+r+", 0 2px 5px "+r:"depressed"===e?n.textShadow="0 -2px 1px "+r:"uniform"===e&&(n.textShadow="-2px 0 1px "+r+",2px 0 1px "+r+",0 -2px 1px "+r+",0 2px 1px "+r+",-1px 1px 1px "+r+",1px 1px 1px "+r+",1px -1px 1px "+r+",1px 1px 1px "+r)}var f,g,m,v,w,y,j={};v=document.createElement("div"),v.className="jw-captions jw-reset",this.show=function(){v.className="jw-captions jw-captions-enabled jw-reset"},this.hide=function(){v.className="jw-captions jw-reset"},this.populate=function(e){return g=-1,f=e,e?void u(e,m):void s("")},this.resize=function(){var e=v.clientWidth,t=Math.pow(e/400,.6);if(t){var n=j.fontSize*t;r(v,{fontSize:Math.round(n)+"px"})}},this.setup=function(e,n){if(w=document.createElement("div"),y=document.createElement("span"),w.className="jw-captions-window jw-reset",y.className="jw-captions-text jw-reset",j=i.extend({},o,n),n){var s=j.fontOpacity,l=j.windowOpacity,c=j.edgeStyle,u=j.backgroundColor,d={},f={color:t.hexToRgba(j.color,s),fontFamily:j.fontFamily,fontStyle:j.fontStyle,fontWeight:j.fontWeight,textDecoration:j.textDecoration};l&&(d.backgroundColor=t.hexToRgba(j.windowColor,l)),h(c,f,s),j.back?f.backgroundColor=t.hexToRgba(u,j.backgroundOpacity):null===c&&h("uniform",f),r(w,d),r(y,f),p(e,d,f)}w.appendChild(y),v.appendChild(w),this.populate(a.get("captionsTrack"))},this.element=function(){return v},a.on("change:playlistItem",function(){m=null,g=-1,s("")},this),a.on("change:captionsTrack",function(e,t){this.populate(t)},this),a.mediaController.on("seek",function(){g=-1},this),a.mediaController.on("time seek",l,this),a.mediaController.on("subtitlesTrackData",function(){u(f,m)},this),a.on("change:state",function(e,t){switch(t){case n.IDLE:case n.ERROR:case n.COMPLETE:this.hide();break;default:this.show()}},this)};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(12),n(4),n(3),n(1)],r=function(e,t,n,i){var r=function(r,o,a){function s(e){return r.get("flashBlocked")?void 0:u?void u(e):void f.trigger(e.type===t.touchEvents.CLICK?"click":"tap")}function l(){return d?void d():void f.trigger("doubleClick")}var c,u,d,p={enableDoubleTap:!0,useMove:!0};i.extend(this,n),c=o,this.element=function(){return c};var h=new e(c,i.extend(p,a));h.on("click tap",s),h.on("doubleClick doubleTap",l),h.on("move",function(){f.trigger("move")}),h.on("over",function(){f.trigger("over")}),h.on("out",function(){f.trigger("out")}),this.clickHandler=s;var f=this;this.setAlternateClickHandlers=function(e,t){u=e,d=t||null},this.revertAlternateClickHandlers=function(){u=null,d=null}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2),n(84)],r=function(e,t,n){function i(e,t){this.time=e,this.text=t,this.el=document.createElement("div"),this.el.className="jw-cue jw-reset"}e.extend(i.prototype,{align:function(e){if("%"===this.time.toString().slice(-1))this.pct=this.time;else{var t=this.time/e*100;this.pct=t+"%"}this.el.style.left=this.pct}});var r={loadChapters:function(e){t.ajax(e,this.chaptersLoaded.bind(this),this.chaptersFailed,{plainText:!0})},chaptersLoaded:function(t){var i=n(t.responseText);e.isArray(i)&&(e.each(i,this.addCue,this),this.drawCues())},chaptersFailed:function(){},addCue:function(e){this.cues.push(new i(e.begin,e.text))},drawCues:function(){var t=this._model.mediaModel.get("duration");if(!t||0>=t)return void this._model.mediaModel.once("change:duration",this.drawCues,this);var n=this;e.each(this.cues,function(e){e.align(t),e.el.addEventListener("mouseover",function(){n.activeCue=e}),e.el.addEventListener("mouseout",function(){n.activeCue=null}),n.elementRail.appendChild(e.el)})},resetChapters:function(){e.each(this.cues,function(e){e.el.parentNode&&e.el.parentNode.removeChild(e.el)},this),this.cues=[]}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(47),n(2),n(1),n(12)],r=function(e,t,n,i){var r=e.extend({constructor:function(t){e.call(this,t),this.container.className="jw-overlay-horizontal jw-reset",this.openClass="jw-open-drawer",this.componentType="drawer"},setup:function(e,r){this.iconUI||(this.iconUI=new i(this.el,{useHover:!0,directSelect:!0}),this.toggleOpenStateListener=this.toggleOpenState.bind(this),this.openTooltipListener=this.openTooltip.bind(this),this.closeTooltipListener=this.closeTooltip.bind(this)),this.reset(),e=n.isArray(e)?e:[],this.activeContents=n.filter(e,function(e){return e.isActive}),t.toggleClass(this.el,"jw-hidden",!r||this.activeContents.length<2),r&&this.activeContents.length>1&&(t.removeClass(this.el,"jw-off"),this.iconUI.on("tap",this.toggleOpenStateListener).on("over",this.openTooltipListener).on("out",this.closeTooltipListener),n.each(e,function(e){this.container.appendChild(e.el)},this))},reset:function(){t.addClass(this.el,"jw-off"),this.iconUI.off(),this.contentUI&&this.contentUI.off().destroy(),this.removeContent()}});return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(47),n(2),n(1),n(12),n(239)],r=function(e,t,n,i,r){var o=e.extend({setup:function(e,o,a){this.iconUI||(this.iconUI=new i(this.el,{useHover:!0,directSelect:!0}),this.toggleValueListener=this.toggleValue.bind(this),this.toggleOpenStateListener=this.toggleOpenState.bind(this),this.openTooltipListener=this.openTooltip.bind(this),this.closeTooltipListener=this.closeTooltip.bind(this),this.selectListener=this.select.bind(this)),this.reset(),e=n.isArray(e)?e:[],t.toggleClass(this.el,"jw-hidden",e.length<2);var s=e.length>2||2===e.length&&a&&a.toggle===!1,l=!s&&2===e.length;if(t.toggleClass(this.el,"jw-toggle",l),t.toggleClass(this.el,"jw-button-color",!l),this.isActive=s||l,s){t.removeClass(this.el,"jw-off"),this.iconUI.on("tap",this.toggleOpenStateListener).on("over",this.openTooltipListener).on("out",this.closeTooltipListener);var c=r(e),u=t.createElement(c);this.addContent(u),this.contentUI=new i(this.content).on("click tap",this.selectListener)}else l&&this.iconUI.on("click tap",this.toggleValueListener);this.selectItem(o)},toggleValue:function(){this.trigger("toggleValue")},select:function(e){if(e.target.parentElement===this.content){var i=t.classList(e.target),r=n.find(i,function(e){return 0===e.indexOf("jw-item")});r&&(this.trigger("select",parseInt(r.split("-")[2])),this.closeTooltipListener())}},selectItem:function(e){if(this.content)for(var n=0;n<this.content.children.length;n++)t.toggleClass(this.content.children[n],"jw-active-option",e===n);else t.toggleClass(this.el,"jw-off",0===e)},reset:function(){t.addClass(this.el,"jw-off"),this.iconUI.off(),this.contentUI&&this.contentUI.off().destroy(),this.removeContent()}});return o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(1),n(47),n(12),n(241)],r=function(e,t,n,i,r){var o=n.extend({setup:function(n,r){if(this.iconUI||(this.iconUI=new i(this.el,{useHover:!0}),this.toggleOpenStateListener=this.toggleOpenState.bind(this),this.openTooltipListener=this.openTooltip.bind(this),this.closeTooltipListener=this.closeTooltip.bind(this),this.selectListener=this.onSelect.bind(this)),this.reset(),n=t.isArray(n)?n:[],e.toggleClass(this.el,"jw-hidden",n.length<2),n.length>=2){this.iconUI=new i(this.el,{useHover:!0}).on("tap",this.toggleOpenStateListener).on("over",this.openTooltipListener).on("out",this.closeTooltipListener);var o=this.menuTemplate(n,r),a=e.createElement(o);this.addContent(a),this.contentUI=new i(this.content),this.contentUI.on("click tap",this.selectListener)}this.originalList=n},menuTemplate:function(n,i){var o=t.map(n,function(t,n){var r=t.title?e.createElement(t.title).textContent:"";return{active:n===i,label:n+1+".",title:r}});return r(o)},onSelect:function(n){var i=n.target;if("UL"!==i.tagName){"LI"!==i.tagName&&(i=i.parentElement);var r=e.classList(i),o=t.find(r,function(e){return 0===e.indexOf("jw-item")});o&&(this.trigger("select",parseInt(o.split("-")[2])),this.closeTooltip())}},selectItem:function(e){this.setup(this.originalList,e)},reset:function(){this.iconUI.off(),this.contentUI&&this.contentUI.off().destroy(),this.removeContent()}});return o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2),n(84)],r=function(e,t,n){function i(e){this.begin=e.begin,this.end=e.end,this.img=e.text}var r={loadThumbnails:function(e){e&&(this.vttPath=e.split("?")[0].split("/").slice(0,-1).join("/"),this.individualImage=null,t.ajax(e,this.thumbnailsLoaded.bind(this),this.thumbnailsFailed.bind(this),{plainText:!0}))},thumbnailsLoaded:function(t){var r=n(t.responseText);e.isArray(r)&&(e.each(r,function(e){this.thumbnails.push(new i(e))},this),this.drawCues())},thumbnailsFailed:function(){},chooseThumbnail:function(t){var n=e.sortedIndex(this.thumbnails,{end:t},e.property("end"));n>=this.thumbnails.length&&(n=this.thumbnails.length-1);var i=this.thumbnails[n].img;return i.indexOf("://")<0&&(i=this.vttPath?this.vttPath+"/"+i:i),i},loadThumbnail:function(t){var n=this.chooseThumbnail(t),i={display:"block",margin:"0 auto",backgroundPosition:"0 0"},r=n.indexOf("#xywh");if(r>0)try{var o=/(.+)\#xywh=(\d+),(\d+),(\d+),(\d+)/.exec(n);n=o[1],i.backgroundPosition=-1*o[2]+"px "+-1*o[3]+"px",i.width=o[4],i.height=o[5]}catch(a){return}else this.individualImage||(this.individualImage=new Image,this.individualImage.onload=e.bind(function(){this.individualImage.onload=null,this.timeTip.image({width:this.individualImage.width,height:this.individualImage.height})},this),this.individualImage.src=n);return i.backgroundImage='url("'+n+'")',i},showThumbnail:function(e){this.thumbnails.length<1||this.timeTip.image(this.loadThumbnail(e))},resetThumbnails:function(){this.timeTip.image({backgroundImage:"",width:0,height:0}),this.thumbnails=[]}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2),n(44),n(88),n(47),n(290),n(294)],r=function(e,t,n,i,r,o,a){var s=r.extend({setup:function(){this.text=document.createElement("span"),this.text.className="jw-text jw-reset",this.img=document.createElement("div"),this.img.className="jw-reset";var e=document.createElement("div");e.className="jw-time-tip jw-background-color jw-reset",e.appendChild(this.img),e.appendChild(this.text),t.removeClass(this.el,"jw-hidden"),this.addContent(e)},image:function(e){t.style(this.img,e)},update:function(e){this.text.innerHTML=e}}),l=i.extend({constructor:function(t,n){this._model=t,this._api=n,this.timeTip=new s("jw-tooltip-time"),this.timeTip.setup(),this.cues=[],this.seekThrottled=e.throttle(this.performSeek,400),this._model.on("change:playlistItem",this.onPlaylistItem,this).on("change:position",this.onPosition,this).on("change:duration",this.onDuration,this).on("change:buffer",this.onBuffer,this),i.call(this,"jw-slider-time","horizontal")},setup:function(){i.prototype.setup.apply(this,arguments),this._model.get("playlistItem")&&this.onPlaylistItem(this._model,this._model.get("playlistItem")),this.elementRail.appendChild(this.timeTip.element()),this.el.addEventListener("mousemove",this.showTimeTooltip.bind(this),!1),this.el.addEventListener("mouseout",this.hideTimeTooltip.bind(this),!1)},limit:function(i){if(this.activeCue&&e.isNumber(this.activeCue.pct))return this.activeCue.pct;var r=this._model.get("duration"),o=t.adaptiveType(r);if("DVR"===o){var a=(1-i/100)*r,s=this._model.get("position"),l=Math.min(a,Math.max(n.dvrSeekLimit,s)),c=100*l/r;return 100-c}return i},update:function(e){this.seekTo=e,this.seekThrottled(),i.prototype.update.apply(this,arguments)},dragStart:function(){this._model.set("scrubbing",!0),i.prototype.dragStart.apply(this,arguments)},dragEnd:function(){i.prototype.dragEnd.apply(this,arguments),this._model.set("scrubbing",!1)},onSeeked:function(){this._model.get("scrubbing")&&this.performSeek()},onBuffer:function(e,t){this.updateBuffer(t)},onPosition:function(e,t){this.updateTime(t,e.get("duration"))},onDuration:function(e,t){this.updateTime(e.get("position"),t)},updateTime:function(e,n){var i=0;if(n){var r=t.adaptiveType(n);"DVR"===r?i=(n-e)/n*100:"VOD"===r&&(i=e/n*100)}this.render(i)},onPlaylistItem:function(t,n){this.reset(),t.mediaModel.on("seeked",this.onSeeked,this);var i=n.tracks;e.each(i,function(e){e&&e.kind&&"thumbnails"===e.kind.toLowerCase()?this.loadThumbnails(e.file):e&&e.kind&&"chapters"===e.kind.toLowerCase()&&this.loadChapters(e.file)},this)},performSeek:function(){var e,n=this.seekTo,i=this._model.get("duration"),r=t.adaptiveType(i);0===i?this._api.play():"DVR"===r?(e=(100-n)/100*i,this._api.seek(e)):(e=n/100*i,this._api.seek(Math.min(e,i-.25)))},showTimeTooltip:function(e){var i=this._model.get("duration");if(0!==i){var r=t.bounds(this.elementRail),o=e.pageX?e.pageX-r.left:e.x;o=t.between(o,0,r.width);var a=o/r.width,s=i*a;0>i&&(s=i-s);var l;if(this.activeCue)l=this.activeCue.text;else{var c=!0;l=t.timeFormat(s,c),0>i&&s>n.dvrSeekLimit&&(l="Live")}this.timeTip.update(l),this.showThumbnail(s),t.addClass(this.timeTip.el,"jw-open"),this.timeTip.el.style.left=100*a+"%"}},hideTimeTooltip:function(){t.removeClass(this.timeTip.el,"jw-open")},reset:function(){this.resetChapters(),this.resetThumbnails()}});return e.extend(l.prototype,o,a),l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(47),n(88),n(12),n(2)],r=function(e,t,n,i){var r=e.extend({constructor:function(r,o){this._model=r,e.call(this,o),this.volumeSlider=new t("jw-slider-volume jw-volume-tip","vertical"),this.addContent(this.volumeSlider.element()),this.volumeSlider.on("update",function(e){this.trigger("update",e)},this),i.removeClass(this.el,"jw-hidden"),new n(this.el,{useHover:!0,directSelect:!0}).on("click",this.toggleValue,this).on("tap",this.toggleOpenState,this).on("over",this.openTooltip,this).on("out",this.closeTooltip,this),this._model.on("change:volume",this.onVolume,this)},toggleValue:function(){this.trigger("toggleValue")}});return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(1),n(3),n(44),n(12),n(88),n(295),n(292),n(293),n(296),n(291)],r=function(e,t,n,i,r,o,a,s,l,c,u){function d(e,t){var n=document.createElement("div");return n.className="jw-icon jw-icon-inline jw-button-color jw-reset "+e,n.style.display="none",t&&new r(n).on("click tap",function(){t()}),{element:function(){return n},toggle:function(e){e?this.show():this.hide()},show:function(){n.style.display=""},hide:function(){n.style.display="none"}}}function p(e){var t=document.createElement("span");return t.className="jw-text jw-reset "+e,t}function h(e){var t=new s(e);return t}function f(e,n){var i=document.createElement("div");return i.className="jw-group jw-controlbar-"+e+"-group jw-reset",t.each(n,function(e){e.element&&(e=e.element()),i.appendChild(e)}),i}function g(t,n){this._api=t,this._model=n,this._isMobile=e.isMobile(),this._compactModeMaxSize=400,this._maxCompactWidth=-1,this.setup()}return t.extend(g.prototype,n,{setup:function(){this.build(),this.initialize()},build:function(){var e,n,i,r,s=new a(this._model,this._api),g=new u("jw-icon-more");this._model.get("visualplaylist")!==!1&&(e=new l("jw-icon-playlist")),this._isMobile||(r=d("jw-icon-volume",this._api.setMute),n=new o("jw-slider-volume","horizontal"),i=new c(this._model,"jw-icon-volume")),this.elements={alt:p("jw-text-alt"),play:d("jw-icon-playback",this._api.play.bind(this,{reason:"interaction"})),prev:d("jw-icon-prev",this._api.playlistPrev.bind(this,{reason:"interaction"})),next:d("jw-icon-next",this._api.playlistNext.bind(this,{reason:"interaction"})),playlist:e,elapsed:p("jw-text-elapsed"),time:s,duration:p("jw-text-duration"),drawer:g,hd:h("jw-icon-hd"),cc:h("jw-icon-cc"),audiotracks:h("jw-icon-audio-tracks"),mute:r,volume:n,volumetooltip:i,cast:d("jw-icon-cast jw-off",this._api.castToggle),fullscreen:d("jw-icon-fullscreen",this._api.setFullscreen)},this.layout={left:[this.elements.play,this.elements.prev,this.elements.playlist,this.elements.next,this.elements.elapsed],center:[this.elements.time,this.elements.alt],right:[this.elements.duration,this.elements.hd,this.elements.cc,this.elements.audiotracks,this.elements.drawer,this.elements.mute,this.elements.cast,this.elements.volume,this.elements.volumetooltip,this.elements.fullscreen],drawer:[this.elements.hd,this.elements.cc,this.elements.audiotracks]},this.menus=t.compact([this.elements.playlist,this.elements.hd,this.elements.cc,this.elements.audiotracks,this.elements.volumetooltip]),this.layout.left=t.compact(this.layout.left),this.layout.center=t.compact(this.layout.center),this.layout.right=t.compact(this.layout.right),this.layout.drawer=t.compact(this.layout.drawer),this.el=document.createElement("div"),this.el.className="jw-controlbar jw-background-color jw-reset",this.elements.left=f("left",this.layout.left),this.elements.center=f("center",this.layout.center),this.elements.right=f("right",this.layout.right),this.el.appendChild(this.elements.left),this.el.appendChild(this.elements.center),this.el.appendChild(this.elements.right)},initialize:function(){this.elements.play.show(),this.elements.fullscreen.show(),this.elements.mute&&this.elements.mute.show(),this.onVolume(this._model,this._model.get("volume")),this.onPlaylist(this._model,this._model.get("playlist")),this.onPlaylistItem(this._model,this._model.get("playlistItem")),this.onMediaModel(this._model,this._model.get("mediaModel")),this.onCastAvailable(this._model,this._model.get("castAvailable")),this.onCastActive(this._model,this._model.get("castActive")),this.onCaptionsList(this._model,this._model.get("captionsList")),this._model.on("change:volume",this.onVolume,this),this._model.on("change:mute",this.onMute,this),this._model.on("change:playlist",this.onPlaylist,this),this._model.on("change:playlistItem",this.onPlaylistItem,this),this._model.on("change:mediaModel",this.onMediaModel,this),this._model.on("change:castAvailable",this.onCastAvailable,this),this._model.on("change:castActive",this.onCastActive,this),this._model.on("change:duration",this.onDuration,this),this._model.on("change:position",this.onElapsed,this),this._model.on("change:fullscreen",this.onFullscreen,this),this._model.on("change:captionsList",this.onCaptionsList,this),this._model.on("change:captionsIndex",this.onCaptionsIndex,this),this._model.on("change:compactUI",this.onCompactUI,this),this.elements.volume&&this.elements.volume.on("update",function(e){var t=e.percentage;this._api.setVolume(t)},this),this.elements.volumetooltip&&(this.elements.volumetooltip.on("update",function(e){var t=e.percentage;this._api.setVolume(t)},this),this.elements.volumetooltip.on("toggleValue",function(){this._api.setMute()},this)),this.elements.playlist&&this.elements.playlist.on("select",function(e){this._model.once("itemReady",function(){this._api.play({reason:"interaction"})},this),this._api.load(e)},this),this.elements.hd.on("select",function(e){this._model.getVideo().setCurrentQuality(e)},this),this.elements.hd.on("toggleValue",function(){this._model.getVideo().setCurrentQuality(0===this._model.getVideo().getCurrentQuality()?1:0)},this),this.elements.cc.on("select",function(e){this._api.setCurrentCaptions(e)},this),this.elements.cc.on("toggleValue",function(){var e=this._model.get("captionsIndex");this._api.setCurrentCaptions(e?0:1)},this),this.elements.audiotracks.on("select",function(e){this._model.getVideo().setCurrentAudioTrack(e)},this),new r(this.elements.duration).on("click tap",function(){if("DVR"===e.adaptiveType(this._model.get("duration"))){var t=this._model.get("position");this._api.seek(Math.max(i.dvrSeekLimit,t))}},this),new r(this.el).on("click tap drag",function(){this.trigger("userAction")},this),this.elements.drawer.on("open-drawer close-drawer",function(t,n){e.toggleClass(this.el,"jw-drawer-expanded",n.isOpen),n.isOpen||this.closeMenus()},this),t.each(this.menus,function(e){e.on("open-tooltip",this.closeMenus,this)},this)},onCaptionsList:function(e,t){var n=e.get("captionsIndex");this.elements.cc.setup(t,n),this.clearCompactMode()},onCaptionsIndex:function(e,t){this.elements.cc.selectItem(t)},onPlaylist:function(e,t){var n=t.length>1;this.elements.next.toggle(n),this.elements.prev.toggle(n),this.elements.playlist&&this.elements.playlist.setup(t,e.get("item"))},onPlaylistItem:function(e){this.elements.time.updateBuffer(0),this.elements.time.render(0),this.elements.duration.innerHTML="00:00",this.elements.elapsed.innerHTML="00:00",this.clearCompactMode();var t=e.get("item");this.elements.playlist&&this.elements.playlist.selectItem(t),this.elements.audiotracks.setup()},onMediaModel:function(n,i){i.on("change:levels",function(e,t){this.elements.hd.setup(t,e.get("currentLevel")),this.clearCompactMode()},this),i.on("change:currentLevel",function(e,t){this.elements.hd.selectItem(t)},this),i.on("change:audioTracks",function(e,n){var i=t.map(n,function(e){return{label:e.name}});this.elements.audiotracks.setup(i,e.get("currentAudioTrack"),{toggle:!1}),this.clearCompactMode()},this),i.on("change:currentAudioTrack",function(e,t){this.elements.audiotracks.selectItem(t)},this),i.on("change:state",function(t,n){"complete"===n&&(this.elements.drawer.closeTooltip(),e.removeClass(this.el,"jw-drawer-expanded"))},this)},onVolume:function(e,t){this.renderVolume(e.get("mute"),t)},onMute:function(e,t){this.renderVolume(t,e.get("volume"))},renderVolume:function(t,n){this.elements.mute&&e.toggleClass(this.elements.mute.element(),"jw-off",t),this.elements.volume&&this.elements.volume.render(t?0:n),this.elements.volumetooltip&&(this.elements.volumetooltip.volumeSlider.render(t?0:n),e.toggleClass(this.elements.volumetooltip.element(),"jw-off",t))},onCastAvailable:function(e,t){this.elements.cast.toggle(t),this.clearCompactMode()},onCastActive:function(t,n){e.toggleClass(this.elements.cast.element(),"jw-off",!n)},onElapsed:function(t,n){var i,r=t.get("duration");i="DVR"===e.adaptiveType(r)?"-"+e.timeFormat(-r):e.timeFormat(n),this.elements.elapsed.innerHTML=i},onDuration:function(t,n){var i;"DVR"===e.adaptiveType(n)?(i="Live",this.clearCompactMode()):i=e.timeFormat(n),this.elements.duration.innerHTML=i},onFullscreen:function(t,n){e.toggleClass(this.elements.fullscreen.element(),"jw-off",n)},element:function(){return this.el},getVisibleBounds:function(){var t,n=this.el,i=getComputedStyle?getComputedStyle(n):n.currentStyle;return"table"===i.display?e.bounds(n):(n.style.visibility="hidden",n.style.display="table",t=e.bounds(n),n.style.visibility=n.style.display="",t)},setAltText:function(e){this.elements.alt.innerHTML=e},addCues:function(e){this.elements.time&&(t.each(e,function(e){this.elements.time.addCue(e)},this),this.elements.time.drawCues())},closeMenus:function(e){t.each(this.menus,function(t){e&&e.target===t.el||t.closeTooltip(e)})},hideComponents:function(){this.closeMenus(),this.elements.drawer.closeTooltip(),e.removeClass(this.el,"jw-drawer-expanded")},clearCompactMode:function(){this._maxCompactWidth=-1,this._model.set("compactUI",!1),this._containerWidth&&this.checkCompactMode(this._containerWidth)},setCompactModeBounds:function(){if(this.element().offsetWidth>0){var t=this.elements.left.offsetWidth+this.elements.right.offsetWidth;if("LIVE"===e.adaptiveType(this._model.get("duration")))this._maxCompactWidth=t+this.elements.alt.offsetWidth;else{var n=t+(this.elements.center.offsetWidth-this.elements.time.el.offsetWidth),i=.2;this._maxCompactWidth=n/(1-i)}}},checkCompactMode:function(e){-1===this._maxCompactWidth&&this.setCompactModeBounds(),this._containerWidth=e,-1!==this._maxCompactWidth&&(e>=this._compactModeMaxSize&&e>this._maxCompactWidth?this._model.set("compactUI",!1):(e<this._compactModeMaxSize||e<=this._maxCompactWidth)&&this._model.set("compactUI",!0))},onCompactUI:function(n,i){e.removeClass(this.el,"jw-drawer-expanded"),this.elements.drawer.setup(this.layout.drawer,i),(!i||this.elements.drawer.activeContents.length<2)&&t.each(this.layout.drawer,function(e){this.elements.right.insertBefore(e.el,this.elements.drawer.el)},this)}}),g}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(3),n(12),n(235),n(1)],r=function(e,t,n,i,r){var o=function(o){r.extend(this,t),this.model=o,this.el=e.createElement(i({}));var a=this;this.iconUI=new n(this.el).on("click tap",function(e){a.trigger(e.type)})};return r.extend(o.prototype,{element:function(){return this.el}}),o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(236),n(2),n(1),n(12)],r=function(e,t,n,i){var r=function(e){this.model=e,this.setup(),this.model.on("change:dock",this.render,this)};return n.extend(r.prototype,{setup:function(){var n=this.model.get("dock"),r=this.click.bind(this),o=e(n);this.el=t.createElement(o),new i(this.el).on("click tap",r)},getDockButton:function(e){return t.hasClass(e.target,"jw-dock-button")?e.target:t.hasClass(e.target,"jw-dock-text")?e.target.parentElement.parentElement:e.target.parentElement},click:function(e){var t=this.getDockButton(e),i=t.getAttribute("button"),r=this.model.get("dock"),o=n.findWhere(r,{id:i});o&&o.callback&&o.callback(e)},render:function(){var n=this.model.get("dock"),i=e(n),r=t.createElement(i);this.el.innerHTML=r.innerHTML},element:function(){return this.el}}),r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(237)],r=function(e){function t(t,n,i,r){return e({id:t,skin:n,title:i,body:r})}return t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(12),n(2),n(4),n(1),n(3),n(238)],r=function(e,t,n,i,r,o){var a=t.style,s={linktarget:"_blank",margin:8,hide:!1,position:"top-right"},l=function(l){var c,u,d=new Image,p=i.extend({},l.get("logo"));return i.extend(this,r),this.setup=function(r){if(u=i.extend({},s,p),u.hide="true"===u.hide.toString(),c=t.createElement(o()),u.file){u.hide&&t.addClass(c,"jw-hide"),t.addClass(c,"jw-logo-"+(u.position||s.position)),"top-right"===u.position&&(l.on("change:dock",this.topRight,this),l.on("change:controls",this.topRight,this),this.topRight(l)),l.set("logo",u),d.onload=function(){var e={backgroundImage:'url("'+this.src+'")',width:this.width,height:this.height};if(u.margin!==s.margin){var t=/(\w+)-(\w+)/.exec(u.position);3===t.length&&(e["margin-"+t[1]]=u.margin,e["margin-"+t[2]]=u.margin)}a(c,e),l.set("logoWidth",e.width)},d.src=u.file;var h=new e(c);h.on("click tap",function(e){t.exists(e)&&e.stopPropagation&&e.stopPropagation(),this.trigger(n.JWPLAYER_LOGO_CLICK,{link:u.link,linktarget:u.linktarget})},this),r.appendChild(c)}},this.topRight=function(e){var t=e.get("controls"),n=e.get("dock"),i=t&&(n&&n.length||e.get("sharing")||e.get("related"));a(c,{top:i?"3.5em":0})},this.element=function(){return c},this.position=function(){return u.position},this.destroy=function(){d.onload=null},this};return l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2)],r=function(e,t){function n(e,t){t.off("change:mediaType",null,this),t.on("change:mediaType",function(t,n){"audio"===n&&this.setImage(e.get("playlistItem").image)},this)}function i(e,n){var i=e.get("autostart")&&!t.isMobile()||e.get("item")>0;return i?(this.setImage(null),e.off("change:state",null,this),void e.on("change:state",function(e,t){"complete"!==t&&"idle"!==t&&"error"!==t||this.setImage(n.image)},this)):void this.setImage(n.image)}var r=function(e){this.model=e,e.on("change:playlistItem",i,this),e.on("change:mediaModel",n,this)};return e.extend(r.prototype,{setup:function(e){this.el=e;var t=this.model.get("playlistItem");t&&this.setImage(t.image)},setImage:function(n){this.model.off("change:state",null,this);var i="";e.isString(n)&&(i='url("'+n+'")'),t.style(this.el,{backgroundImage:i})},element:function(){return this.el}}),r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(242),n(1),n(12),n(46)],r=function(e,t,n,i,r){var o=function(){};return n.extend(o.prototype,{buildArray:function(){var t=r.split("+"),n=t[0],i={items:[{title:"Powered by JW Player "+n,featured:!0,showLogo:!0,link:"https://jwplayer.com/learn-more"}]},o=n.indexOf("-")>0,a=t[1];if(o&&a){var s=a.split(".");i.items.push({title:"build: ("+s[0]+"."+s[1]+")",link:"#"})}var l=this.model.get("provider");if(l&&l.name.indexOf("flash")>=0){var c="Flash Version "+e.flashVersion();i.items.push({title:c,link:"http://www.adobe.com/software/flash/about/"})}return i},buildMenu:function(){var n=this.buildArray();return e.createElement(t(n))},updateHtml:function(){this.el.innerHTML=this.buildMenu().innerHTML},rightClick:function(e){return this.lazySetup(),this.mouseOverContext?!1:(this.hideMenu(),this.showMenu(e),!1)},getOffset:function(e){for(var t=e.target,n=e.offsetX||e.layerX,i=e.offsetY||e.layerY;t!==this.playerElement;)n+=t.offsetLeft,i+=t.offsetTop,t=t.parentNode;return{x:n,y:i}},showMenu:function(t){var n=this.getOffset(t);return this.el.style.left=n.x+"px",this.el.style.top=n.y+"px",e.addClass(this.playerElement,"jw-flag-rightclick-open"),e.addClass(this.el,"jw-open"),!1},hideMenu:function(){this.mouseOverContext||(e.removeClass(this.playerElement,"jw-flag-rightclick-open"),e.removeClass(this.el,"jw-open"))},lazySetup:function(){this.el||(this.el=this.buildMenu(),this.layer.appendChild(this.el),this.hideMenuHandler=this.hideMenu.bind(this),this.addOffListener(this.playerElement),this.addOffListener(document),this.model.on("change:provider",this.updateHtml,this),this.elementUI=new i(this.el,{useHover:!0}).on("over",function(){this.mouseOverContext=!0},this).on("out",function(){this.mouseOverContext=!1},this))},setup:function(e,t,n){this.playerElement=t,this.model=e,this.mouseOverContext=!1,this.layer=n,t.oncontextmenu=this.rightClick.bind(this)},addOffListener:function(e){e.addEventListener("mousedown",this.hideMenuHandler),e.addEventListener("touchstart",this.hideMenuHandler),e.addEventListener("pointerdown",this.hideMenuHandler)},removeOffListener:function(e){e.removeEventListener("mousedown",this.hideMenuHandler),e.removeEventListener("touchstart",this.hideMenuHandler),e.removeEventListener("pointerdown",this.hideMenuHandler)},destroy:function(){this.el&&(this.hideMenu(),this.elementUI.off(),this.removeOffListener(this.playerElement),this.removeOffListener(document),this.hideMenuHandler=null,this.el=null),this.playerElement&&(this.playerElement.oncontextmenu=null,this.playerElement=null),this.model&&(this.model.off("change:provider",this.updateHtml),this.model=null)}}),o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2)],r=function(e,t){var n=function(e){this.model=e,this.model.on("change:playlistItem",this.playlistItem,this)};return e.extend(n.prototype,{
	hide:function(){this.el.style.display="none"},show:function(){this.el.style.display=""},setup:function(e){this.el=e;var t=this.el.getElementsByTagName("div");this.title=t[0],this.description=t[1],this.model.get("playlistItem")&&this.playlistItem(this.model,this.model.get("playlistItem")),this.model.on("change:logoWidth",this.update,this),this.model.on("change:dock",this.update,this)},update:function(e){var n={paddingLeft:0,paddingRight:0},i=e.get("controls"),r=e.get("dock"),o=e.get("logo");if(o){var a=1*(""+o.margin).replace("px",""),s=e.get("logoWidth")+(isNaN(a)?0:a);"top-left"===o.position?n.paddingLeft=s:"top-right"===o.position&&(n.paddingRight=s)}if(i&&r&&r.length){var l=56*r.length;n.paddingRight=Math.max(n.paddingRight,l)}t.style(this.el,n)},playlistItem:function(e,t){if(e.get("displaytitle")||e.get("displaydescription")){var n="",i="";t.title&&e.get("displaytitle")&&(n=t.title),t.description&&e.get("displaydescription")&&(i=t.description),this.updateText(n,i)}else this.hide()},updateText:function(e,t){this.title.innerHTML=e,this.description.innerHTML=t,this.title.firstChild||this.description.firstChild?this.show():this.hide()},element:function(){return this.el}}),n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(4),n(3),n(44),n(6),n(288),n(289),n(298),n(299),n(301),n(297),n(302),n(322),n(304),n(1),n(240)],r=function(e,t,i,r,o,a,s,l,c,u,d,p,h,f,g,m){var v=e.style,w=e.bounds,y=e.isMobile(),j=["fullscreenchange","webkitfullscreenchange","mozfullscreenchange","MSFullscreenChange"],b=function(b,E){function k(t){var n=0,i=E.get("duration"),o=E.get("position");"DVR"===e.adaptiveType(i)&&(n=i,i=Math.max(o,r.dvrSeekLimit));var a=e.between(o+t,n,i);b.seek(a)}function A(t){var n=e.between(E.get("volume")+t,0,100);b.setVolume(n)}function L(e){return e.ctrlKey||e.metaKey?!1:!!E.get("controls")}function _(e){if(!L(e))return!0;switch(Me||te(),e.keyCode){case 27:b.setFullscreen(!1);break;case 13:case 32:b.play({reason:"interaction"});break;case 37:Me||k(-5);break;case 39:Me||k(5);break;case 38:A(10);break;case 40:A(-10);break;case 77:b.setMute();break;case 70:b.setFullscreen();break;default:if(e.keyCode>=48&&e.keyCode<=59){var t=e.keyCode-48,n=t/10*E.get("duration");b.seek(n)}}return/13|32|37|38|39|40/.test(e.keyCode)?(e.preventDefault(),!1):void 0}function x(){Ne=!1,e.removeClass(ue,"jw-no-focus")}function C(){Ne=!0,e.addClass(ue,"jw-no-focus")}function P(){Ne||x(),Me||te()}function T(){var e=w(ue),n=Math.round(e.width),i=Math.round(e.height);return document.body.contains(ue)?n&&i&&(n===he&&i===fe||(he=n,fe=i,clearTimeout(Oe),Oe=setTimeout(q,50),E.set("containerWidth",n),E.set("containerHeight",i),We.trigger(t.JWPLAYER_RESIZE,{width:n,height:i}))):(window.removeEventListener("resize",T),y&&window.removeEventListener("orientationchange",T)),e}function R(t,n){n=n||!1,e.toggleClass(ue,"jw-flag-casting",n)}function I(t,n){e.toggleClass(ue,"jw-flag-cast-available",n),e.toggleClass(de,"jw-flag-cast-available",n)}function M(t,n){e.replaceClass(ue,/jw-stretch-\S+/,"jw-stretch-"+n)}function S(t,n){e.toggleClass(ue,"jw-flag-compact-player",n)}function O(e){e&&!y&&(e.element().addEventListener("mousemove",N,!1),e.element().addEventListener("mouseout",W,!1))}function D(){E.get("state")!==o.IDLE&&E.get("state")!==o.COMPLETE&&E.get("state")!==o.PAUSED||!E.get("controls")||b.play({reason:"interaction"}),Se?ee():te()}function Y(e){e.link?(b.pause(!0),b.setFullscreen(!1),window.open(e.link,e.linktarget)):E.get("controls")&&b.play({reason:"interaction"})}function N(){clearTimeout(Te)}function W(){te()}function F(e){We.trigger(e.type,e)}function J(t,n){n?(Le&&Le.destroy(),e.addClass(ue,"jw-flag-flash-blocked")):(Le&&Le.setup(E,ue,ue),e.removeClass(ue,"jw-flag-flash-blocked"))}function V(){E.get("controls")&&b.setFullscreen()}function B(){var n=ue.getElementsByClassName("jw-overlays")[0];n.addEventListener("mousemove",te),we=new s(E,pe,{useHover:!0}),we.on("click",function(){F({type:t.JWPLAYER_DISPLAY_CLICK}),E.get("controls")&&b.play({reason:"interaction"})}),we.on("tap",function(){F({type:t.JWPLAYER_DISPLAY_CLICK}),D()}),we.on("doubleClick",V),we.on("move",te),we.on("over",te);var i=new l(E);i.on("click",function(){F({type:t.JWPLAYER_DISPLAY_CLICK}),b.play({reason:"interaction"})}),i.on("tap",function(){F({type:t.JWPLAYER_DISPLAY_CLICK}),D()}),e.isChrome()&&i.el.addEventListener("mousedown",function(){var e=E.getVideo(),t=e&&0===e.getName().name.indexOf("flash");if(t){var n=function(){document.removeEventListener("mouseup",n),i.el.style.pointerEvents="auto"};this.style.pointerEvents="none",document.addEventListener("mouseup",n)}}),de.appendChild(i.element()),je=new c(E),be=new u(E),be.on(t.JWPLAYER_LOGO_CLICK,Y);var r=document.createElement("div");r.className="jw-controls-right jw-reset",be.setup(r),r.appendChild(je.element()),de.appendChild(r),ke=new a(E),ke.setup(ue.id,E.get("captions")),de.parentNode.insertBefore(ke.element(),Ee.element());var o=E.get("height");y&&("string"==typeof o||o>=1.5*Ie)?e.addClass(ue,"jw-flag-touch"):(Le=new h,Le.setup(E,ue,ue)),me=new d(b,E),me.on(t.JWPLAYER_USER_ACTION,te),E.on("change:scrubbing",H),E.on("change:compactUI",S),de.appendChild(me.element()),ue.addEventListener("focus",P),ue.addEventListener("blur",x),ue.addEventListener("keydown",_),ue.onmousedown=C}function U(t){return t.get("state")===o.PAUSED?void t.once("change:state",U):void(t.get("scrubbing")===!1&&e.removeClass(ue,"jw-flag-dragging"))}function H(t,n){t.off("change:state",U),n?e.addClass(ue,"jw-flag-dragging"):U(t)}function z(t,n,i){var r,o=ue.className;i=!!i,i&&(o=o.replace(/\s*aspectMode/,""),ue.className!==o&&(ue.className=o),v(ue,{display:"block"},i)),e.exists(t)&&e.exists(n)&&(E.set("width",t),E.set("height",n)),r={width:t},e.hasClass(ue,"jw-flag-aspect-mode")||(r.height=n),v(ue,r,!0),G(n),q(t,n)}function G(t){if(Ae=K(t),me&&!Ae){var n=Me?ge:E;ce(n,n.get("state"))}e.toggleClass(ue,"jw-flag-audio-player",Ae)}function K(e){if(E.get("aspectratio"))return!1;if(g.isString(e)&&e.indexOf("%")>-1)return!1;var t=g.isNumber(e)?e:E.get("containerHeight");return Q(t)}function Q(e){return e&&Ie*(y?1.75:1)>=e}function q(t,n){if(!t||isNaN(Number(t))){if(!pe)return;t=pe.clientWidth}if(!n||isNaN(Number(n))){if(!pe)return;n=pe.clientHeight}e.isMSIE(9)&&document.all&&!window.atob&&(t=n="100%");var i=E.getVideo();if(i){var r=i.resize(t,n,E.get("stretching"));r&&(clearTimeout(Oe),Oe=setTimeout(q,250)),ke.resize(),me.checkCompactMode(t)}}function X(){if(Ye){var e=document.fullscreenElement||document.webkitCurrentFullScreenElement||document.mozFullScreenElement||document.msFullscreenElement;return!(!e||e.id!==E.get("id"))}return Me?ge.getVideo().getFullScreen():E.getVideo().getFullScreen()}function $(e){var t=E.get("fullscreen"),n=void 0!==e.jwstate?e.jwstate:X();t!==n&&E.set("fullscreen",n),clearTimeout(Oe),Oe=setTimeout(q,200)}function Z(t,n){n?(e.addClass(t,"jw-flag-fullscreen"),v(document.body,{"overflow-y":"hidden"}),te()):(e.removeClass(t,"jw-flag-fullscreen"),v(document.body,{"overflow-y":""})),q()}function ee(){Se=!1,clearTimeout(Te),me.hideComponents(),e.addClass(ue,"jw-flag-user-inactive")}function te(){Se||(e.removeClass(ue,"jw-flag-user-inactive"),me.checkCompactMode(pe.clientWidth)),Se=!0,clearTimeout(Te),Te=setTimeout(ee,Re)}function ne(){b.setFullscreen(!1)}function ie(){ye&&ye.setState(E.get("state")),re(E,E.mediaModel.get("mediaType")),E.mediaModel.on("change:mediaType",re,this)}function re(t,n){var i="audio"===n,r=E.getVideo(),o=r&&0===r.getName().name.indexOf("flash");e.toggleClass(ue,"jw-flag-media-audio",i),i&&!o?ue.insertBefore(ve.el,pe):ue.insertBefore(ve.el,ke.element())}function oe(t,n){var i="LIVE"===e.adaptiveType(n);e.toggleClass(ue,"jw-flag-live",i),We.setAltText(i?"Live Broadcast":"")}function ae(e,t){return t?void(t.name?Ee.updateText(t.name,t.message):Ee.updateText(t.message,"")):void Ee.playlistItem(e,e.get("playlistItem"))}function se(){var e=E.getVideo();return e?e.isCaster:!1}function le(){e.replaceClass(ue,/jw-state-\S+/,"jw-state-"+_e)}function ce(t,n){if(_e=n,clearTimeout(De),n===o.COMPLETE||n===o.IDLE?De=setTimeout(le,100):le(),se())return void e.addClass(pe,"jw-media-show");switch(n){case o.PLAYING:q();break;case o.PAUSED:te()}}var ue,de,pe,he,fe,ge,me,ve,we,ye,je,be,Ee,ke,Ae,Le,_e,xe,Ce,Pe,Te=-1,Re=y?4e3:2e3,Ie=40,Me=!1,Se=!1,Oe=-1,De=-1,Ye=!1,Ne=!1,We=g.extend(this,i);window.webpackJsonpjwplayer&&n(309),this.model=E,this.api=b,ue=e.createElement(m({id:E.get("id")})),e.isIE()&&e.addClass(ue,"jw-ie");var Fe=E.get("width"),Je=E.get("height");v(ue,{width:Fe.toString().indexOf("%")>0?Fe:Fe+"px",height:Je.toString().indexOf("%")>0?Je:Je+"px"}),Ce=ue.requestFullscreen||ue.webkitRequestFullscreen||ue.webkitRequestFullScreen||ue.mozRequestFullScreen||ue.msRequestFullscreen,Pe=document.exitFullscreen||document.webkitExitFullscreen||document.webkitCancelFullScreen||document.mozCancelFullScreen||document.msExitFullscreen,Ye=Ce&&Pe,this.onChangeSkin=function(t,n){e.replaceClass(ue,/jw-skin-\S+/,n?"jw-skin-"+n:"")},this.handleColorOverrides=function(){function t(t,i,r){if(r){t=e.prefix(t,"#"+n+" ");var o={};o[i]=r,e.css(t.join(", "),o)}}var n=E.get("id"),i=E.get("skinColorActive"),r=E.get("skinColorInactive"),o=E.get("skinColorBackground");t([".jw-toggle",".jw-button-color:hover"],"color",i),t([".jw-active-option",".jw-progress",".jw-playlist-container .jw-option.jw-active-option",".jw-playlist-container .jw-option:hover"],"background",i),t([".jw-text",".jw-option",".jw-button-color",".jw-toggle.jw-off",".jw-tooltip-title",".jw-skip .jw-skip-icon",".jw-playlist-container .jw-icon"],"color",r),t([".jw-cue",".jw-knob"],"background",r),t([".jw-playlist-container .jw-option"],"border-bottom-color",r),t([".jw-background-color",".jw-tooltip-title",".jw-playlist",".jw-playlist-container .jw-option"],"background",o),t([".jw-playlist-container ::-webkit-scrollbar"],"border-color",o)},this.setup=function(){this.handleColorOverrides(),E.get("skin-loading")===!0&&(e.addClass(ue,"jw-flag-skin-loading"),E.once("change:skin-loading",function(){e.removeClass(ue,"jw-flag-skin-loading")})),this.onChangeSkin(E,E.get("skin"),""),E.on("change:skin",this.onChangeSkin,this),pe=ue.getElementsByClassName("jw-media")[0],de=ue.getElementsByClassName("jw-controls")[0];var n=ue.getElementsByClassName("jw-preview")[0];ve=new p(E),ve.setup(n);var i=ue.getElementsByClassName("jw-title")[0];Ee=new f(E),Ee.setup(i),B(),te(),E.set("mediaContainer",pe),E.mediaController.on("fullscreenchange",$);for(var r=j.length;r--;)document.addEventListener(j[r],$,!1);window.removeEventListener("resize",T),window.addEventListener("resize",T,!1),y&&(window.removeEventListener("orientationchange",T),window.addEventListener("orientationchange",T,!1)),E.on("change:errorEvent",ae),E.on("change:controls",Ve),Ve(E,E.get("controls")),E.on("change:state",ce),E.on("change:duration",oe,this),E.on("change:flashBlocked",J),J(E,E.get("flashBlocked")),b.onPlaylistComplete(ne),b.onPlaylistItem(ie),E.on("change:castAvailable",I),I(E,E.get("castAvailable")),E.on("change:castActive",R),R(E,E.get("castActive")),E.get("stretching")&&M(E,E.get("stretching")),E.on("change:stretching",M),ce(E,o.IDLE),E.on("change:fullscreen",Be),O(me),O(be);var a=E.get("aspectratio");if(a){e.addClass(ue,"jw-flag-aspect-mode");var s=ue.getElementsByClassName("jw-aspect")[0];v(s,{paddingTop:a})}b.on(t.JWPLAYER_READY,function(){T(),z(E.get("width"),E.get("height"))})};var Ve=function(t,n){if(n){var i=Me?ge.get("state"):E.get("state");ce(t,i)}e.toggleClass(ue,"jw-flag-controls-disabled",!n)},Be=function(t,n){var i=E.getVideo();Ye?(n?Ce.apply(ue):Pe.apply(document),Z(ue,n)):e.isIE()?Z(ue,n):(ge&&ge.getVideo()&&ge.getVideo().setFullscreen(n),i.setFullscreen(n)),i&&0===i.getName().name.indexOf("flash")&&i.setFullscreen(n)};this.resize=function(e,t){var n=!0;z(e,t,n),T()},this.resizeMedia=q,this.reset=function(){document.contains(ue)&&ue.parentNode.replaceChild(xe,ue),e.emptyElement(ue)},this.setupInstream=function(t){this.instreamModel=ge=t,ge.on("change:controls",Ve,this),ge.on("change:state",ce,this),Me=!0,e.addClass(ue,"jw-flag-ads"),te()},this.setAltText=function(e){me.setAltText(e)},this.useExternalControls=function(){e.addClass(ue,"jw-flag-ads-hide-controls")},this.destroyInstream=function(){if(Me=!1,ge&&(ge.off(null,null,this),ge=null),this.setAltText(""),e.removeClass(ue,"jw-flag-ads"),e.removeClass(ue,"jw-flag-ads-hide-controls"),E.getVideo){var t=E.getVideo();t.setContainer(pe)}oe(E,E.get("duration")),we.revertAlternateClickHandlers()},this.addCues=function(e){me&&me.addCues(e)},this.clickHandler=function(){return we},this.controlsContainer=function(){return de},this.getContainer=this.element=function(){return ue},this.getSafeRegion=function(t){var n={x:0,y:0,width:E.get("containerWidth")||0,height:E.get("containerHeight")||0},i=E.get("dock");return i&&i.length&&E.get("controls")&&(n.y=je.element().clientHeight,n.height-=n.y),t=t||!e.exists(t),t&&E.get("controls")&&(n.height-=me.element().clientHeight),n},this.destroy=function(){window.removeEventListener("resize",T),window.removeEventListener("orientationchange",T);for(var t=j.length;t--;)document.removeEventListener(j[t],$,!1);E.mediaController&&E.mediaController.off("fullscreenchange",$),ue.removeEventListener("keydown",_,!1),Le&&Le.destroy(),ye&&(E.off("change:state",ye.statusDelegate),ye.destroy(),ye=null),Me&&this.destroyInstream(),be&&be.destroy(),e.clearCss("#"+E.get("id"))}};return b}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,function(e,t,n){function i(e,t){for(var n=0;n<e.length;n++){var i=e[n],r=d[i.id];if(r){r.refs++;for(var o=0;o<r.parts.length;o++)r.parts[o](i.parts[o]);for(;o<i.parts.length;o++)r.parts.push(l(i.parts[o],t))}else{for(var a=[],o=0;o<i.parts.length;o++)a.push(l(i.parts[o],t));d[i.id]={id:i.id,refs:1,parts:a}}}}function r(e){for(var t=[],n={},i=0;i<e.length;i++){var r=e[i],o=r[0],a=r[1],s=r[2],l={css:a,media:s};n[o]?n[o].parts.push(l):t.push(n[o]={id:o,parts:[l]})}return t}function o(e,t){var n=f(),i=v[v.length-1];if("top"===e.insertAt)i?i.nextSibling?n.insertBefore(t,i.nextSibling):n.appendChild(t):n.insertBefore(t,n.firstChild),v.push(t);else{if("bottom"!==e.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");n.appendChild(t)}}function a(e){e.parentNode.removeChild(e);var t=v.indexOf(e);t>=0&&v.splice(t,1)}function s(e){var t=document.createElement("style");return t.type="text/css",o(e,t),t}function l(e,t){var n,i,r;if(t.singleton){var o=m++;n=g||(g=s(t)),i=c.bind(null,n,o,!1),r=c.bind(null,n,o,!0)}else n=s(t),i=u.bind(null,n),r=function(){a(n)};return i(e),function(t){if(t){if(t.css===e.css&&t.media===e.media)return;i(e=t)}else r()}}function c(e,t,n,i){var r=n?"":i.css;if(e.styleSheet)e.styleSheet.cssText=w(t,r);else{var o=document.createTextNode(r),a=e.childNodes;a[t]&&e.removeChild(a[t]),a.length?e.insertBefore(o,a[t]):e.appendChild(o)}}function u(e,t){var n=t.css,i=t.media;if(i&&e.setAttribute("media",i),e.styleSheet)e.styleSheet.cssText=n;else{for(;e.firstChild;)e.removeChild(e.firstChild);e.appendChild(document.createTextNode(n))}}var d={},p=function(e){var t;return function(){return"undefined"==typeof t&&(t=e.apply(this,arguments)),t}},h=p(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),f=p(function(){return document.head||document.getElementsByTagName("head")[0]}),g=null,m=0,v=[];e.exports=function(e,t){t=t||{},"undefined"==typeof t.singleton&&(t.singleton=h()),"undefined"==typeof t.insertAt&&(t.insertAt="bottom");var n=r(e);return i(n,t),function(e){for(var o=[],a=0;a<n.length;a++){var s=n[a],l=d[s.id];l.refs--,o.push(l)}if(e){var c=r(e);i(c,t)}for(var a=0;a<o.length;a++){var l=o[a];if(0===l.refs){for(var u=0;u<l.parts.length;u++)l.parts[u]();delete d[l.id]}}}};var w=function(){var e=[];return function(t,n){return e[t]=n,e.filter(Boolean).join("\n")}}()},function(e,t,n){var i=n(230);"string"==typeof i&&(i=[[e.id,i,""]]);n(308)(i,{});i.locals&&(e.exports=i.locals)},,,function(e,t,n){var i,r;i=[n(135),n(1),n(46),n(2),n(14),n(12),n(139),n(32),n(140),n(133),n(4),n(6),n(122),n(85),n(59),n(120),n(41)],r=function(e,t,n,i,r,o,a,s,l,c,u,d,p,h,f,g,m){var v={};return v.api=e,v._=t,v.version=n,v.utils=t.extend(i,r,{canCast:f.available,key:a,extend:t.extend,scriptloader:s,rssparser:g,tea:l,UI:o}),v.utils.css.style=v.utils.style,v.vid=c,v.events=t.extend({},u,{state:d}),v.playlist=t.extend({},p,{item:h}),v.plugins=m,v.cast=f,v}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,function(e,t,n){var i,r;i=[n(267),n(48),n(320)],r=function(e,t,i){var r=e.prototype.setup;return e.prototype.setup=function(e,o){e.analytics&&(e.sdkplatform=e.sdkplatform||e.analytics.sdkplatform),r.apply(this,arguments);var a=this._model.get("edition"),s=t(a),l=this._model.get("cast"),c=this;s("casting")&&l&&l.appid&&n.e(4,function(require){var e=n(136);c._castController=new e(c,c._model),c.castToggle=c._castController.castToggle.bind(c._castController)});var u=i.setup();this.once("ready",u.onReady,this),o.getAdBlock=u.checkAdBlock},e}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(139),n(48),n(33),n(89),n(1),n(2),n(130),n(272)],r=function(e,t,i,r,o,a,s,l){function c(e,t,n){if(t){var i=t.client;delete t.client,/\.(js|swf)$/.test(i||"")||(i=s.repo()+n),e[i]=t}}function u(e,n){var i=o.clone(n.get("plugins"))||{},r=n.get("edition"),a=t(r),l=/^(vast|googima)$/,u=/\.(js|swf)$/,d=s.repo(),p=n.get("advertising");if(a("ads")&&p&&(u.test(p.client)?i[p.client]=p:l.test(p.client)&&(i[d+p.client+".js"]=p),delete p.client),a("jwpsrv")){var h=n.get("analytics");o.isObject(h)||(h={}),c(i,h,"jwpsrv.js")}c(i,n.get("ga"),"gapro.js"),c(i,n.get("sharing"),"sharing.js"),c(i,n.get("related"),"related.js"),n.set("plugins",i),e()}function d(t,i){var r=i.get("key")||window.jwplayer&&window.jwplayer.key,o=new e(r),c=o.edition();if(i.set("key",r),i.set("edition",c),"unlimited"===c){var u=a.getScriptPath("jwplayer.js");if(!u)return void l.error(t,"Error setting up player","Could not locate jwplayer.js script tag");n.p=u,a.repo=s.repo=s.loadFrom=function(){return u}}i.updateProviders(),"invalid"===c?l.error(t,"Error setting up player",(void 0===r?"Missing":"Invalid")+" license key"):t()}function p(e,t){var n=t.getProviders(),r=t.get("playlist"),o=t.get("edition"),a=n.required(r,o);i.load(a,o).then(e)}function h(){var e=l.getQueue();return e.LOAD_PROVIDERS={method:p,depends:["CHECK_KEY","FILTER_PLAYLIST"]},e.CHECK_KEY={method:d,depends:["LOADED_POLYFILLS"]},e.FILTER_PLUGINS={method:u,depends:["CHECK_KEY"]},e.FILTER_PLAYLIST.depends.push("CHECK_KEY"),e.LOAD_PLUGINS.depends.push("FILTER_PLUGINS"),e.SETUP_VIEW.depends.push("CHECK_KEY"),e.SEND_READY.depends.push("LOAD_PROVIDERS"),e}return{getQueue:h}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(274),n(312),n(1)],r=function(e,t,n){return window.jwplayer?window.jwplayer:n.extend(e,t)}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){function e(){var e=document.createElement("div");return e.className=n,e.innerHTML="&nbsp;",e.style.width="1px",e.style.height="1px",e.style.position="absolute",e.style.background="transparent",e}function t(){function t(){var e=this,t=e._view.element();t.appendChild(o),i()&&e.trigger("adBlock")}function i(){return r?!0:(""!==o.innerHTML&&o.className===n&&null!==o.offsetParent&&0!==o.clientHeight||(r=!0),r)}var r=!1,o=e();return{onReady:t,checkAdBlock:i}}var n="afs_ads";return{setup:t}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(4),n(12),n(3),n(1),n(234)],r=function(e,t,n,i,r,o){var a=function(e){this.model=e,this.setup()};return r.extend(a.prototype,i,{setup:function(){this.destroy(),this.skipMessage=this.model.get("skipText"),this.skipMessageCountdown=this.model.get("skipMessage"),this.setWaitTime(this.model.get("skipOffset"));var t=o();this.el=e.createElement(t),this.skiptext=this.el.getElementsByClassName("jw-skiptext")[0],this.skipAdOnce=r.once(this.skipAd.bind(this)),new n(this.el).on("click tap",r.bind(function(){this.skippable&&this.skipAdOnce()},this)),this.model.on("change:duration",this.onChangeDuration,this),this.model.on("change:position",this.onChangePosition,this),this.onChangeDuration(this.model,this.model.get("duration")),this.onChangePosition(this.model,this.model.get("position"))},updateMessage:function(e){this.skiptext.innerHTML=e},updateCountdown:function(e){this.updateMessage(this.skipMessageCountdown.replace(/xx/gi,Math.ceil(this.waitTime-e)))},onChangeDuration:function(t,n){if(n){if(this.waitPercentage){if(!n)return;this.itemDuration=n,this.setWaitTime(this.waitPercentage),delete this.waitPercentage}e.removeClass(this.el,"jw-hidden")}},onChangePosition:function(t,n){this.waitTime-n>0?this.updateCountdown(n):(this.updateMessage(this.skipMessage),this.skippable=!0,e.addClass(this.el,"jw-skippable"))},element:function(){return this.el},setWaitTime:function(t){if(r.isString(t)&&"%"===t.slice(-1)){var n=parseFloat(t);return void(this.itemDuration&&!isNaN(n)?this.waitTime=this.itemDuration*n/100:this.waitPercentage=t)}r.isNumber(t)?this.waitTime=t:"string"===e.typeOf(t)?this.waitTime=e.seconds(t):isNaN(Number(t))?this.waitTime=0:this.waitTime=Number(t)},skipAd:function(){this.trigger(t.JWPLAYER_AD_SKIPPED)},destroy:function(){this.el&&(this.el.removeEventListener("click",this.skipAdOnce),this.el.parentElement&&this.el.parentElement.removeChild(this.el)),delete this.skippable,delete this.itemDuration,delete this.waitPercentage}}),a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(303),n(1)],r=function(e,t){var n=function(){};return t.extend(n.prototype,e.prototype,{buildArray:function(){var t=e.prototype.buildArray.apply(this,arguments);if(this.model.get("abouttext")){t.items[0].showLogo=!1,t.items.push(t.items.shift());var n={title:this.model.get("abouttext"),link:this.model.get("aboutlink")||t.items[0].link};t.items.unshift(n)}return t}}),n}.apply(t,i),!(void 0!==r&&(e.exports=r))}])});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 190:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(191);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!./nouislider.min.css", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!./nouislider.min.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 191:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "/*! nouislider - 8.2.1 - 2015-12-02 21:43:15 */\r\n\r\n\r\n.noUi-target,.noUi-target *{-webkit-touch-callout:none;-webkit-user-select:none;-ms-touch-action:none;touch-action:none;-ms-user-select:none;-moz-user-select:none;-moz-box-sizing:border-box;box-sizing:border-box}.noUi-target{position:relative;direction:ltr}.noUi-base{width:100%;height:100%;position:relative;z-index:1}.noUi-origin{position:absolute;right:0;top:0;left:0;bottom:0}.noUi-handle{position:relative;z-index:1}.noUi-stacking .noUi-handle{z-index:10}.noUi-state-tap .noUi-origin{-webkit-transition:left .3s,top .3s;transition:left .3s,top .3s}.noUi-state-drag *{cursor:inherit!important}.noUi-base{-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}.noUi-horizontal{height:18px}.noUi-horizontal .noUi-handle{width:34px;height:28px;left:-17px;top:-6px}.noUi-vertical{width:18px}.noUi-vertical .noUi-handle{width:28px;height:34px;left:-6px;top:-17px}.noUi-background{background:#FAFAFA;box-shadow:inset 0 1px 1px #f0f0f0}.noUi-connect{background:#3FB8AF;box-shadow:inset 0 0 3px rgba(51,51,51,.45);-webkit-transition:background 450ms;transition:background 450ms}.noUi-origin{border-radius:2px}.noUi-target{border-radius:4px;border:1px solid #D3D3D3;box-shadow:inset 0 1px 1px #F0F0F0,0 3px 6px -5px #BBB}.noUi-target.noUi-connect{box-shadow:inset 0 0 3px rgba(51,51,51,.45),0 3px 6px -5px #BBB}.noUi-draggable{cursor:w-resize}.noUi-vertical .noUi-draggable{cursor:n-resize}.noUi-handle{border:1px solid #D9D9D9;border-radius:3px;background:#FFF;cursor:default;box-shadow:inset 0 0 1px #FFF,inset 0 1px 7px #EBEBEB,0 3px 6px -3px #BBB}.noUi-active{box-shadow:inset 0 0 1px #FFF,inset 0 1px 7px #DDD,0 3px 6px -3px #BBB}.noUi-handle:after,.noUi-handle:before{content:\"\";display:block;position:absolute;height:14px;width:1px;background:#E8E7E6;left:14px;top:6px}.noUi-handle:after{left:17px}.noUi-vertical .noUi-handle:after,.noUi-vertical .noUi-handle:before{width:14px;height:1px;left:6px;top:14px}.noUi-vertical .noUi-handle:after{top:17px}[disabled] .noUi-connect,[disabled].noUi-connect{background:#B8B8B8}[disabled] .noUi-handle,[disabled].noUi-origin{cursor:not-allowed}.noUi-pips,.noUi-pips *{-moz-box-sizing:border-box;box-sizing:border-box}.noUi-pips{position:absolute;color:#999}.noUi-value{width:40px;position:absolute;text-align:center}.noUi-value-sub{color:#ccc;font-size:10px}.noUi-marker{position:absolute;background:#CCC}.noUi-marker-large,.noUi-marker-sub{background:#AAA}.noUi-pips-horizontal{padding:10px 0;height:50px;top:100%;left:0;width:100%}.noUi-value-horizontal{margin-left:-20px;padding-top:20px}.noUi-value-horizontal.noUi-value-sub{padding-top:15px}.noUi-marker-horizontal.noUi-marker{margin-left:-1px;width:2px;height:5px}.noUi-marker-horizontal.noUi-marker-sub{height:10px}.noUi-marker-horizontal.noUi-marker-large{height:15px}.noUi-pips-vertical{padding:0 10px;height:100%;top:0;left:100%}.noUi-value-vertical{width:15px;margin-left:20px;margin-top:-5px}.noUi-marker-vertical.noUi-marker{width:5px;height:2px;margin-top:-1px}.noUi-marker-vertical.noUi-marker-sub{width:10px}.noUi-marker-vertical.noUi-marker-large{width:15px}.noUi-tooltip{display:block;position:absolute;border:1px solid #D9D9D9;border-radius:3px;background:#fff;padding:5px;text-align:center}.noUi-horizontal .noUi-handle-lower .noUi-tooltip{top:-32px}.noUi-horizontal .noUi-handle-upper .noUi-tooltip{bottom:-32px}.noUi-vertical .noUi-handle-lower .noUi-tooltip{left:120%}.noUi-vertical .noUi-handle-upper .noUi-tooltip{right:120%}", ""]);

	// exports


/***/ },

/***/ 192:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function($) {/*! nouislider - 8.2.1 - 2015-12-02 21:43:14 */

	!function(a){ true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (a), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):"object"==typeof exports?module.exports=a():window.noUiSlider=a()}(function(){"use strict";function a(a){return a.filter(function(a){return this[a]?!1:this[a]=!0},{})}function b(a,b){return Math.round(a/b)*b}function c(a){var b=a.getBoundingClientRect(),c=a.ownerDocument,d=c.documentElement,e=m();return/webkit.*Chrome.*Mobile/i.test(navigator.userAgent)&&(e.x=0),{top:b.top+e.y-d.clientTop,left:b.left+e.x-d.clientLeft}}function d(a){return"number"==typeof a&&!isNaN(a)&&isFinite(a)}function e(a){var b=Math.pow(10,7);return Number((Math.round(a*b)/b).toFixed(7))}function f(a,b,c){j(a,b),setTimeout(function(){k(a,b)},c)}function g(a){return Math.max(Math.min(a,100),0)}function h(a){return Array.isArray(a)?a:[a]}function i(a){var b=a.split(".");return b.length>1?b[1].length:0}function j(a,b){a.classList?a.classList.add(b):a.className+=" "+b}function k(a,b){a.classList?a.classList.remove(b):a.className=a.className.replace(new RegExp("(^|\\b)"+b.split(" ").join("|")+"(\\b|$)","gi")," ")}function l(a,b){a.classList?a.classList.contains(b):new RegExp("(^| )"+b+"( |$)","gi").test(a.className)}function m(){var a=void 0!==window.pageXOffset,b="CSS1Compat"===(document.compatMode||""),c=a?window.pageXOffset:b?document.documentElement.scrollLeft:document.body.scrollLeft,d=a?window.pageYOffset:b?document.documentElement.scrollTop:document.body.scrollTop;return{x:c,y:d}}function n(a){a.stopPropagation()}function o(a){return function(b){return a+b}}function p(a,b){return 100/(b-a)}function q(a,b){return 100*b/(a[1]-a[0])}function r(a,b){return q(a,a[0]<0?b+Math.abs(a[0]):b-a[0])}function s(a,b){return b*(a[1]-a[0])/100+a[0]}function t(a,b){for(var c=1;a>=b[c];)c+=1;return c}function u(a,b,c){if(c>=a.slice(-1)[0])return 100;var d,e,f,g,h=t(c,a);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],f+r([d,e],c)/p(f,g)}function v(a,b,c){if(c>=100)return a.slice(-1)[0];var d,e,f,g,h=t(c,b);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],s([d,e],(c-f)*p(f,g))}function w(a,c,d,e){if(100===e)return e;var f,g,h=t(e,a);return d?(f=a[h-1],g=a[h],e-f>(g-f)/2?g:f):c[h-1]?a[h-1]+b(e-a[h-1],c[h-1]):e}function x(a,b,c){var e;if("number"==typeof b&&(b=[b]),"[object Array]"!==Object.prototype.toString.call(b))throw new Error("noUiSlider: 'range' contains invalid value.");if(e="min"===a?0:"max"===a?100:parseFloat(a),!d(e)||!d(b[0]))throw new Error("noUiSlider: 'range' value isn't numeric.");c.xPct.push(e),c.xVal.push(b[0]),e?c.xSteps.push(isNaN(b[1])?!1:b[1]):isNaN(b[1])||(c.xSteps[0]=b[1])}function y(a,b,c){return b?void(c.xSteps[a]=q([c.xVal[a],c.xVal[a+1]],b)/p(c.xPct[a],c.xPct[a+1])):!0}function z(a,b,c,d){this.xPct=[],this.xVal=[],this.xSteps=[d||!1],this.xNumSteps=[!1],this.snap=b,this.direction=c;var e,f=[];for(e in a)a.hasOwnProperty(e)&&f.push([a[e],e]);for(f.length&&"object"==typeof f[0][0]?f.sort(function(a,b){return a[0][0]-b[0][0]}):f.sort(function(a,b){return a[0]-b[0]}),e=0;e<f.length;e++)x(f[e][1],f[e][0],this);for(this.xNumSteps=this.xSteps.slice(0),e=0;e<this.xNumSteps.length;e++)y(e,this.xNumSteps[e],this)}function A(a,b){if(!d(b))throw new Error("noUiSlider: 'step' is not numeric.");a.singleStep=b}function B(a,b){if("object"!=typeof b||Array.isArray(b))throw new Error("noUiSlider: 'range' is not an object.");if(void 0===b.min||void 0===b.max)throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");if(b.min===b.max)throw new Error("noUiSlider: 'range' 'min' and 'max' cannot be equal.");a.spectrum=new z(b,a.snap,a.dir,a.singleStep)}function C(a,b){if(b=h(b),!Array.isArray(b)||!b.length||b.length>2)throw new Error("noUiSlider: 'start' option is incorrect.");a.handles=b.length,a.start=b}function D(a,b){if(a.snap=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'snap' option must be a boolean.")}function E(a,b){if(a.animate=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'animate' option must be a boolean.")}function F(a,b){if("lower"===b&&1===a.handles)a.connect=1;else if("upper"===b&&1===a.handles)a.connect=2;else if(b===!0&&2===a.handles)a.connect=3;else{if(b!==!1)throw new Error("noUiSlider: 'connect' option doesn't match handle count.");a.connect=0}}function G(a,b){switch(b){case"horizontal":a.ort=0;break;case"vertical":a.ort=1;break;default:throw new Error("noUiSlider: 'orientation' option is invalid.")}}function H(a,b){if(!d(b))throw new Error("noUiSlider: 'margin' option must be numeric.");if(a.margin=a.spectrum.getMargin(b),!a.margin)throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")}function I(a,b){if(!d(b))throw new Error("noUiSlider: 'limit' option must be numeric.");if(a.limit=a.spectrum.getMargin(b),!a.limit)throw new Error("noUiSlider: 'limit' option is only supported on linear sliders.")}function J(a,b){switch(b){case"ltr":a.dir=0;break;case"rtl":a.dir=1,a.connect=[0,2,1,3][a.connect];break;default:throw new Error("noUiSlider: 'direction' option was not recognized.")}}function K(a,b){if("string"!=typeof b)throw new Error("noUiSlider: 'behaviour' must be a string containing options.");var c=b.indexOf("tap")>=0,d=b.indexOf("drag")>=0,e=b.indexOf("fixed")>=0,f=b.indexOf("snap")>=0,g=b.indexOf("hover")>=0;if(d&&!a.connect)throw new Error("noUiSlider: 'drag' behaviour must be used with 'connect': true.");a.events={tap:c||f,drag:d,fixed:e,snap:f,hover:g}}function L(a,b){var c;if(b!==!1)if(b===!0)for(a.tooltips=[],c=0;c<a.handles;c++)a.tooltips.push(!0);else{if(a.tooltips=h(b),a.tooltips.length!==a.handles)throw new Error("noUiSlider: must pass a formatter for all handles.");a.tooltips.forEach(function(a){if("boolean"!=typeof a&&("object"!=typeof a||"function"!=typeof a.to))throw new Error("noUiSlider: 'tooltips' must be passed a formatter or 'false'.")})}}function M(a,b){if(a.format=b,"function"==typeof b.to&&"function"==typeof b.from)return!0;throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")}function N(a,b){if(void 0!==b&&"string"!=typeof b)throw new Error("noUiSlider: 'cssPrefix' must be a string.");a.cssPrefix=b}function O(a){var b,c={margin:0,limit:0,animate:!0,format:T};b={step:{r:!1,t:A},start:{r:!0,t:C},connect:{r:!0,t:F},direction:{r:!0,t:J},snap:{r:!1,t:D},animate:{r:!1,t:E},range:{r:!0,t:B},orientation:{r:!1,t:G},margin:{r:!1,t:H},limit:{r:!1,t:I},behaviour:{r:!0,t:K},format:{r:!1,t:M},tooltips:{r:!1,t:L},cssPrefix:{r:!1,t:N}};var d={connect:!1,direction:"ltr",behaviour:"tap",orientation:"horizontal"};return Object.keys(b).forEach(function(e){if(void 0===a[e]&&void 0===d[e]){if(b[e].r)throw new Error("noUiSlider: '"+e+"' is required.");return!0}b[e].t(c,void 0===a[e]?d[e]:a[e])}),c.pips=a.pips,c.style=c.ort?"top":"left",c}function P(b,d){function e(a,b,c){var d=a+b[0],e=a+b[1];return c?(0>d&&(e+=Math.abs(d)),e>100&&(d-=e-100),[g(d),g(e)]):[d,e]}function p(a,b){a.preventDefault();var c,d,e=0===a.type.indexOf("touch"),f=0===a.type.indexOf("mouse"),g=0===a.type.indexOf("pointer"),h=a;return 0===a.type.indexOf("MSPointer")&&(g=!0),e&&(c=a.changedTouches[0].pageX,d=a.changedTouches[0].pageY),b=b||m(),(f||g)&&(c=a.clientX+b.x,d=a.clientY+b.y),h.pageOffset=b,h.points=[c,d],h.cursor=f||g,h}function q(a,b){var c=document.createElement("div"),d=document.createElement("div"),e=["-lower","-upper"];return a&&e.reverse(),j(d,da[3]),j(d,da[3]+e[b]),j(c,da[2]),c.appendChild(d),c}function r(a,b,c){switch(a){case 1:j(b,da[7]),j(c[0],da[6]);break;case 3:j(c[1],da[6]);case 2:j(c[0],da[7]);case 0:j(b,da[6])}}function s(a,b,c){var d,e=[];for(d=0;a>d;d+=1)e.push(c.appendChild(q(b,d)));return e}function t(a,b,c){j(c,da[0]),j(c,da[8+a]),j(c,da[4+b]);var d=document.createElement("div");return j(d,da[1]),c.appendChild(d),d}function u(a,b){if(!d.tooltips[b])return!1;var c=document.createElement("div");return c.className=da[18],a.firstChild.appendChild(c)}function v(){d.dir&&d.tooltips.reverse();var a=Y.map(u);d.dir&&(a.reverse(),d.tooltips.reverse()),U("update",function(b,c,e){a[c]&&(a[c].innerHTML=d.tooltips[c]===!0?b[c]:d.tooltips[c].to(e[c]))})}function w(a,b,c){if("range"===a||"steps"===a)return aa.xVal;if("count"===a){var d,e=100/(b-1),f=0;for(b=[];(d=f++*e)<=100;)b.push(d);a="positions"}return"positions"===a?b.map(function(a){return aa.fromStepping(c?aa.getStep(a):a)}):"values"===a?c?b.map(function(a){return aa.fromStepping(aa.getStep(aa.toStepping(a)))}):b:void 0}function x(b,c,d){function e(a,b){return(a+b).toFixed(7)/1}var f=aa.direction,g={},h=aa.xVal[0],i=aa.xVal[aa.xVal.length-1],j=!1,k=!1,l=0;return aa.direction=0,d=a(d.slice().sort(function(a,b){return a-b})),d[0]!==h&&(d.unshift(h),j=!0),d[d.length-1]!==i&&(d.push(i),k=!0),d.forEach(function(a,f){var h,i,m,n,o,p,q,r,s,t,u=a,v=d[f+1];if("steps"===c&&(h=aa.xNumSteps[f]),h||(h=v-u),u!==!1&&void 0!==v)for(i=u;v>=i;i=e(i,h)){for(n=aa.toStepping(i),o=n-l,r=o/b,s=Math.round(r),t=o/s,m=1;s>=m;m+=1)p=l+m*t,g[p.toFixed(5)]=["x",0];q=d.indexOf(i)>-1?1:"steps"===c?2:0,!f&&j&&(q=0),i===v&&k||(g[n.toFixed(5)]=[i,q]),l=n}}),aa.direction=f,g}function y(a,b,c){function e(a){return["-normal","-large","-sub"][a]}function f(a,b,c){return'class="'+b+" "+b+"-"+h+" "+b+e(c[1])+'" style="'+d.style+": "+a+'%"'}function g(a,d){aa.direction&&(a=100-a),d[1]=d[1]&&b?b(d[0],d[1]):d[1],i.innerHTML+="<div "+f(a,da[21],d)+"></div>",d[1]&&(i.innerHTML+="<div "+f(a,da[22],d)+">"+c.to(d[0])+"</div>")}var h=["horizontal","vertical"][d.ort],i=document.createElement("div");return j(i,da[20]),j(i,da[20]+"-"+h),Object.keys(a).forEach(function(b){g(b,a[b])}),i}function z(a){var b=a.mode,c=a.density||1,d=a.filter||!1,e=a.values||!1,f=a.stepped||!1,g=w(b,e,f),h=x(c,b,g),i=a.format||{to:Math.round};return $.appendChild(y(h,d,i))}function A(){return X["offset"+["Width","Height"][d.ort]]}function B(a,b,c){void 0!==b&&1!==d.handles&&(b=Math.abs(b-d.dir)),Object.keys(ca).forEach(function(d){var e=d.split(".")[0];a===e&&ca[d].forEach(function(a){a.call(Z,h(P()),b,h(C(Array.prototype.slice.call(ba))),c||!1)})})}function C(a){return 1===a.length?a[0]:d.dir?a.reverse():a}function D(a,b,c,e){var f=function(b){return $.hasAttribute("disabled")?!1:l($,da[14])?!1:(b=p(b,e.pageOffset),a===R.start&&void 0!==b.buttons&&b.buttons>1?!1:e.hover&&b.buttons?!1:(b.calcPoint=b.points[d.ort],void c(b,e)))},g=[];return a.split(" ").forEach(function(a){b.addEventListener(a,f,!1),g.push([a,f])}),g}function E(a,b){if(-1===navigator.appVersion.indexOf("MSIE 9")&&0===a.buttons&&0!==b.buttonsProperty)return F(a,b);var c,d,f=b.handles||Y,g=!1,h=100*(a.calcPoint-b.start)/b.baseSize,i=f[0]===Y[0]?0:1;if(c=e(h,b.positions,f.length>1),g=L(f[0],c[i],1===f.length),f.length>1){if(g=L(f[1],c[i?0:1],!1)||g)for(d=0;d<b.handles.length;d++)B("slide",d)}else g&&B("slide",i)}function F(a,b){var c=X.querySelector("."+da[15]),d=b.handles[0]===Y[0]?0:1;null!==c&&k(c,da[15]),a.cursor&&(document.body.style.cursor="",document.body.removeEventListener("selectstart",document.body.noUiListener));var e=document.documentElement;e.noUiListeners.forEach(function(a){e.removeEventListener(a[0],a[1])}),k($,da[12]),B("set",d),B("change",d),void 0!==b.handleNumber&&B("end",b.handleNumber)}function G(a,b){"mouseout"===a.type&&"HTML"===a.target.nodeName&&null===a.relatedTarget&&F(a,b)}function H(a,b){var c=document.documentElement;if(1===b.handles.length&&(j(b.handles[0].children[0],da[15]),b.handles[0].hasAttribute("disabled")))return!1;a.preventDefault(),a.stopPropagation();var d=D(R.move,c,E,{start:a.calcPoint,baseSize:A(),pageOffset:a.pageOffset,handles:b.handles,handleNumber:b.handleNumber,buttonsProperty:a.buttons,positions:[_[0],_[Y.length-1]]}),e=D(R.end,c,F,{handles:b.handles,handleNumber:b.handleNumber}),f=D("mouseout",c,G,{handles:b.handles,handleNumber:b.handleNumber});if(c.noUiListeners=d.concat(e,f),a.cursor){document.body.style.cursor=getComputedStyle(a.target).cursor,Y.length>1&&j($,da[12]);var g=function(){return!1};document.body.noUiListener=g,document.body.addEventListener("selectstart",g,!1)}void 0!==b.handleNumber&&B("start",b.handleNumber)}function I(a){var b,e,g=a.calcPoint,h=0;return a.stopPropagation(),Y.forEach(function(a){h+=c(a)[d.style]}),b=h/2>g||1===Y.length?0:1,g-=c(X)[d.style],e=100*g/A(),d.events.snap||f($,da[14],300),Y[b].hasAttribute("disabled")?!1:(L(Y[b],e),B("slide",b,!0),B("set",b,!0),B("change",b,!0),void(d.events.snap&&H(a,{handles:[Y[b]]})))}function J(a){var b=a.calcPoint-c(X)[d.style],e=aa.getStep(100*b/A()),f=aa.fromStepping(e);Object.keys(ca).forEach(function(a){"hover"===a.split(".")[0]&&ca[a].forEach(function(a){a.call(Z,f)})})}function K(a){var b,c;if(!a.fixed)for(b=0;b<Y.length;b+=1)D(R.start,Y[b].children[0],H,{handles:[Y[b]],handleNumber:b});if(a.tap&&D(R.start,X,I,{handles:Y}),a.hover)for(D(R.move,X,J,{hover:!0}),b=0;b<Y.length;b+=1)["mousemove MSPointerMove pointermove"].forEach(function(a){Y[b].children[0].addEventListener(a,n,!1)});a.drag&&(c=[X.querySelector("."+da[7])],j(c[0],da[10]),a.fixed&&c.push(Y[c[0]===Y[0]?1:0].children[0]),c.forEach(function(a){D(R.start,a,H,{handles:Y})}))}function L(a,b,c){var e=a!==Y[0]?1:0,f=_[0]+d.margin,h=_[1]-d.margin,i=_[0]+d.limit,l=_[1]-d.limit;return Y.length>1&&(b=e?Math.max(b,f):Math.min(b,h)),c!==!1&&d.limit&&Y.length>1&&(b=e?Math.min(b,i):Math.max(b,l)),b=aa.getStep(b),b=g(parseFloat(b.toFixed(7))),b===_[e]?!1:(window.requestAnimationFrame?window.requestAnimationFrame(function(){a.style[d.style]=b+"%"}):a.style[d.style]=b+"%",a.previousSibling||(k(a,da[17]),b>50&&j(a,da[17])),_[e]=b,ba[e]=aa.fromStepping(b),B("update",e),!0)}function M(a,b){var c,e,f;for(d.limit&&(a+=1),c=0;a>c;c+=1)e=c%2,f=b[e],null!==f&&f!==!1&&("number"==typeof f&&(f=String(f)),f=d.format.from(f),(f===!1||isNaN(f)||L(Y[e],aa.toStepping(f),c===3-d.dir)===!1)&&B("update",e))}function N(a){var b,c,e=h(a);for(d.dir&&d.handles>1&&e.reverse(),d.animate&&-1!==_[0]&&f($,da[14],300),b=Y.length>1?3:1,1===e.length&&(b=1),M(b,e),c=0;c<Y.length;c++)B("set",c)}function P(){var a,b=[];for(a=0;a<d.handles;a+=1)b[a]=d.format.to(ba[a]);return C(b)}function Q(){da.forEach(function(a){a&&k($,a)}),$.innerHTML="",delete $.noUiSlider}function T(){var a=_.map(function(a,b){var c=aa.getApplicableStep(a),d=i(String(c[2])),e=ba[b],f=100===a?null:c[2],g=Number((e-c[2]).toFixed(d)),h=0===a?null:g>=c[1]?c[2]:c[0]||!1;return[h,f]});return C(a)}function U(a,b){ca[a]=ca[a]||[],ca[a].push(b),"update"===a.split(".")[0]&&Y.forEach(function(a,b){B("update",b)})}function V(a){var b=a.split(".")[0],c=a.substring(b.length);Object.keys(ca).forEach(function(a){var d=a.split(".")[0],e=a.substring(d.length);b&&b!==d||c&&c!==e||delete ca[a]})}function W(a){var b,c=P(),e=O({start:[0,0],margin:a.margin,limit:a.limit,step:a.step,range:a.range,animate:a.animate,snap:void 0===a.snap?d.snap:a.snap});for(["margin","limit","step","range","animate"].forEach(function(b){void 0!==a[b]&&(d[b]=a[b])}),aa=e.spectrum,_=[-1,-1],N(c),b=0;b<Y.length;b++)B("update",b)}var X,Y,Z,$=b,_=[-1,-1],aa=d.spectrum,ba=[],ca={},da=["target","base","origin","handle","horizontal","vertical","background","connect","ltr","rtl","draggable","","state-drag","","state-tap","active","","stacking","tooltip","","pips","marker","value"].map(o(d.cssPrefix||S));if($.noUiSlider)throw new Error("Slider was already initialized.");return X=t(d.dir,d.ort,$),Y=s(d.handles,d.dir,X),r(d.connect,$,Y),d.pips&&z(d.pips),d.tooltips&&v(),Z={destroy:Q,steps:T,on:U,off:V,get:P,set:N,updateOptions:W},K(d.events),Z}function Q(a,b){if(!a.nodeName)throw new Error("noUiSlider.create requires a single element.");var c=O(b,a),d=P(a,c);return d.set(c.start),a.noUiSlider=d,d}var R=window.navigator.pointerEnabled?{start:"pointerdown",move:"pointermove",end:"pointerup"}:window.navigator.msPointerEnabled?{start:"MSPointerDown",move:"MSPointerMove",end:"MSPointerUp"}:{start:"mousedown touchstart",move:"mousemove touchmove",end:"mouseup touchend"},S="noUi-";z.prototype.getMargin=function(a){return 2===this.xPct.length?q(this.xVal,a):!1},z.prototype.toStepping=function(a){return a=u(this.xVal,this.xPct,a),this.direction&&(a=100-a),a},z.prototype.fromStepping=function(a){return this.direction&&(a=100-a),e(v(this.xVal,this.xPct,a))},z.prototype.getStep=function(a){return this.direction&&(a=100-a),a=w(this.xPct,this.xSteps,this.snap,a),this.direction&&(a=100-a),a},z.prototype.getApplicableStep=function(a){var b=t(a,this.xPct),c=100===a?2:1;return[this.xNumSteps[b-2],this.xVal[b-c],this.xNumSteps[b-c]]},z.prototype.convert=function(a){return this.getStep(this.toStepping(a))};var T={to:function(a){return void 0!==a&&a.toFixed(2)},from:Number};return{create:Q}});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 193:
/***/ function(module, exports) {

	//This Service is used to detect Browser , OS , Resolution , IP , Location of the user
	angular
		.module('service')
		.service('infoDetection',InfoDetection);

	InfoDetection.$inject = ['$http'];

	function InfoDetection($http){

		var self = this;
		self.ip = "";
		self.location = {};
		self.resolution = {};
		self.platform = {};

		self.getResolution = getResolution;
		self.getBrowserAndOs = getBrowserAndOs;
		self.getLocation = getLocation;
		self.getIp = getIp;
		self.getCompatibility = getCompatibility;
		self.init = init;

		//
		var BROWSERS = {
			CHROME : 30,
			FIREFOX : 35,
			SAFARI : 6,
			IE : 12,
			'MICROSOFT EDGE' : 13
		}


		function init(){
			getResolution();
			getBrowserAndOs();
			return getIp()
					.then(getLocation);

		}

		function getBrowserAndOs(){
			self.platform = platform;
		}

		function getResolution(){
			self.resolution = {
				width : screen.width,
				height : screen.height
			}
		}

		function getIp(){
			return $http.get('http://jsonip.com/')
					.then(function success(res){
						self.ip = res.data.ip;
					})
					.catch(function error(err){
						console.error(err);
					})
		}

		function getLocation(){
			return $http.get('http://ip-api.com/json/' + self.ip)
					.then(function success(res){
						self.location = res.data;
					})
					.catch(function error(err){
						console.error(err);
					})
		}

		function getCompatibility(){
			var isIE = false;
			var isModern = false;
			var browser_name = self.platform.name.toUpperCase();
			var browser_version = parseInt(self.platform.version);
			var os = self.platform.os.family.toUpperCase();
			if(browser_name === "IE" || browser_name === "MICROSOFT EDGE"){
				isIE = true;
			}

			if(browser_version >= BROWSERS[browser_name]){
				isModern = true;
			}
			var compability;
			if(os.indexOf("OS X") >= 0){
				compability = "mac+";
			}else{
				compability = "windows+";

				if(isIE){
					compability  += 'ie+';
				}
			}
			if(isModern){
				compability += 'modern';
			}else{
				compability += 'old';
			}

			return compability;
		}

	}


/***/ },

/***/ 194:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {angular
		.module('service')
		.service('fullScreen',FullScreen);

	FullScreen.$inject = [];	
	function FullScreen(){	

		var vm = this;
		vm.launchFullScreen = function(element){
			vm.fullscreenElement = element;
	        var fullscreenchange;
	        if (element.requestFullscreen) {
	            element.requestFullscreen();
	            fullscreenchange = "fullscreenchange";
	        } else if (element.mozRequestFullScreen) {
	            element.mozRequestFullScreen();
	            fullscreenchange = "mozfullscreenchange";
	        } else if (element.webkitRequestFullscreen) {
	            element.webkitRequestFullscreen();
	            fullscreenchange = "webkitfullscreenchange";
	        } else if (element.msRequestFullscreen) {
	            element.msRequestFullscreen();
	            fullscreenchange = "msfullscreenchange";
	        }
	        vm.fullscreenElement.addEventListener(fullscreenchange, function(e) {
	            var fullscreenEnabled = document.isFullScreen || document.mozFullScreen || document.webkitIsFullScreen;
	            console.log("triggering on element...",vm.fullscreenElement);
	            $(vm.fullscreenElement).trigger('fullscreenchange',[fullscreenEnabled]);
	            // if (fullscreenEnabled) {
	            //     vm.isFullScreen = true;
	            // } else {
	            //     vm.isFullScreen = false;
	            // }
	        });
		}

		vm.exitFullScreen = function(){
	        console.log("doing exitFullScreen")
			if (document.exitFullscreen) {
	            document.exitFullscreen();
	        } else if (document.mozCancelFullScreen) {
	            document.mozCancelFullScreen();
	        } else if (document.webkitExitFullscreen) {
	            document.webkitExitFullscreen();
	        }
		}

	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 195:
/***/ function(module, exports) {

	'use strict';

	angular.module("service")
	    .service('videoPlayer', VideoPlayer);


	VideoPlayer.$inject = [];

	function VideoPlayer() {


	    this.setVideoPlayer = setVideoPlayer;
	    this.getVideoPlayer = getVideoPlayer;
	    var videoPlayer = null;

	    function setVideoPlayer(videoPlayer) {
	        this.videoPlayer = videoPlayer;
	    }
	    function getVideoPlayer(){
	        return this.videoPlayer;
	    }


	};

/***/ },

/***/ 196:
/***/ function(module, exports) {

	angular.module('service').service('videoUrl',VideoUrl);

	VideoUrl.$inject = ['$http','BASE_API'];

	function VideoUrl($http,BASE_API) {
	    this.fetchVideoUrl = function (data) {
	        return $http.post(BASE_API + '/videoUrl/fetch', data).then(function (res) {
	            return res.data;
	        });
	    }
	}

/***/ },

/***/ 197:
/***/ function(module, exports) {

	angular.module('service').service('videoViews',VideoViews);

	VideoViews.$inject = ['$http','BASE_API'];

	function VideoViews($http,BASE_API) {
	    this.incrementVideoViews = function (data) {
	        return $http.post(BASE_API + '/videoviews/incrementVideoViews', data);
	    }
	    
	    this.getVideoViews = function (data) {
	        return $http.post(BASE_API + '/videoviews/getVideoViews', data);
	    }

	    this.submitVideoStats = function (data) {
	        return $http.post(BASE_API + '/userstats', data);
	    }
	}

/***/ },

/***/ 198:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module) {var WebVTTParser = function() {
	    this.parse = function(input) {
	        //XXX need global search and replace for \0
	        //XXX skyp byte order mark
	        var NEWLINE = /\r\n|\r|\n/,
	            startTime = Date.now(),
	            linePos = 0,
	            lines = input.split(NEWLINE),
	            alreadyCollected = false,
	            cues = [],
	            errors = []

	        function err(message, col) {
	            errors.push({
	                message: message,
	                line: linePos + 1,
	                col: col
	            })
	        }

	        /* SIGNATURE */
	        if (
	            lines[linePos].length < 6 ||
	            lines[linePos].indexOf("WEBVTT") != 0 ||
	            lines[linePos].length > 6 &&
	            lines[linePos][6] != " " &&
	            lines[linePos][6] != "\t"
	        ) {
	            err("No valid signature. (File needs to start with \"WEBVTT\".)")
	        }

	        linePos++

	        /* HEADER */
	        while (lines[linePos] != "" && lines[linePos] != undefined) {
	            err("No blank line after the signature.")
	            if (lines[linePos].indexOf("-->") != -1) {
	                alreadyCollected = true
	                break
	            }
	            linePos++
	        }

	        /* CUE LOOP */
	        while (lines[linePos] != undefined) {
	            var cue
	            while (!alreadyCollected && lines[linePos] == "") {
	                linePos++
	            }
	            if (!alreadyCollected && lines[linePos] == undefined)
	                break

	            /* CUE CREATION */
	            cue = {
	                id: "",
	                startTime: 0,
	                endTime: 0,
	                pauseOnExit: false,
	                direction: "horizontal",
	                snapToLines: true,
	                linePosition: "auto",
	                textPosition: 50,
	                size: 100,
	                alignment: "middle",
	                text: "",
	                tree: null
	            }

	            if (lines[linePos].indexOf("-->") == -1) {
	                cue.id = lines[linePos]

	                linePos++

	                if (lines[linePos] == "" || lines[linePos] == undefined) {
	                    err("Cue identifier cannot be standalone.")
	                    continue
	                }

	            }

	            /* TIMINGS */
	            alreadyCollected = false
	            var timings = new WebVTTCueTimingsAndSettingsParser(lines[linePos], err)
	            var previousCueStart = 0
	            if (cues.length > 0) {
	                previousCueStart = cues[cues.length - 1].startTime
	            }
	            if (!timings.parse(cue, previousCueStart)) {
	                /* BAD CUE */

	                cue = null
	                linePos++

	                /* BAD CUE LOOP */
	                while (lines[linePos] != "" && lines[linePos] != undefined) {
	                    if (lines[linePos].indexOf("-->") != -1) {
	                        alreadyCollected = true
	                        break
	                    }
	                    linePos++
	                }
	                continue
	            }
	            linePos++

	            /* CUE TEXT LOOP */
	            while (lines[linePos] != "" && lines[linePos] != undefined) {
	                if (lines[linePos].indexOf("-->") != -1) {
	                    err("Blank line missing before cue.")
	                    alreadyCollected = true
	                    break
	                }
	                if (cue.text != "")
	                    cue.text += "\n"
	                cue.text += lines[linePos]
	                linePos++
	            }

	            /* CUE TEXT PROCESSING */
	            var cuetextparser = new WebVTTCueTextParser(cue.text, err)
	            cue.tree = cuetextparser.parse(cue.startTime, cue.endTime)
	            cues.push(cue)
	        }
	        cues.sort(function(a, b) {
	                if (a.startTime < b.startTime)
	                    return -1
	                if (a.startTime > b.startTime)
	                    return 1
	                if (a.endTime > b.endTime)
	                    return -1
	                if (a.endTime < b.endTime)
	                    return 1
	                return 0
	            })
	            /* END */
	        return {
	            cues: cues,
	            errors: errors,
	            time: Date.now() - startTime
	        }
	    }
	}

	var WebVTTCueTimingsAndSettingsParser = function(line, errorHandler) {
	    var SPACE = /[\u0020\t\f]/,
	        NOSPACE = /[^\u0020\t\f]/,
	        line = line,
	        pos = 0,
	        err = function(message) {
	            errorHandler(message, pos + 1)
	        },
	        spaceBeforeSetting = true

	    function skip(pattern) {
	        while (
	            line[pos] != undefined &&
	            pattern.test(line[pos])
	        ) {
	            pos++
	        }
	    }

	    function collect(pattern) {
	        var str = ""
	        while (
	            line[pos] != undefined &&
	            pattern.test(line[pos])
	        ) {
	            str += line[pos]
	            pos++
	        }
	        return str
	    }
	    /* http://dev.w3.org/html5/webvtt/#collect-a-webvtt-timestamp */
	    function timestamp() {
	        var units = "minutes",
	            val1,
	            val2,
	            val3,
	            val4
	            // 3
	        if (line[pos] == undefined) {
	            err("No timestamp found.")
	            return
	        }
	        // 4
	        if (!/\d/.test(line[pos])) {
	            err("Timestamp must start with a character in the range 0-9.")
	            return
	        }
	        // 5-7
	        val1 = collect(/\d/)
	        if (val1.length > 2 || parseInt(val1, 10) > 59) {
	            units = "hours"
	        }
	        // 8
	        if (line[pos] != ":") {
	            err("No time unit separator found.")
	            return
	        }
	        pos++
	        // 9-11
	        val2 = collect(/\d/)
	        if (val2.length != 2) {
	            err("Must be exactly two digits.")
	            return
	        }
	        // 12
	        if (units == "hours" || line[pos] == ":") {
	            if (line[pos] != ":") {
	                err("No seconds found or minutes is greater than 59.")
	                return
	            }
	            pos++
	            val3 = collect(/\d/)
	            if (val3.length != 2) {
	                err("Must be exactly two digits.")
	                return
	            }
	        } else {
	            val3 = val2
	            val2 = val1
	            val1 = "0"
	        }
	        // 13
	        if (line[pos] != ".") {
	            err("No decimal separator (\".\") found.")
	            return
	        }
	        pos++
	        // 14-16
	        val4 = collect(/\d/)
	        if (val4.length != 3) {
	            err("Milliseconds must be given in three digits.")
	            return
	        }
	        // 17
	        if (parseInt(val2, 10) > 59) {
	            err("You cannot have more than 59 minutes.")
	            return
	        }
	        if (parseInt(val3, 10) > 59) {
	            err("You cannot have more than 59 seconds.")
	            return
	        }
	        return parseInt(val1, 10) * 60 * 60 + parseInt(val2, 10) * 60 + parseInt(val3, 10) + parseInt(val4, 10) / 1000
	    }

	    /* http://dev.w3.org/html5/webvtt/#parse-the-webvtt-settings */
	    function parseSettings(input, cue) {
	        var settings = input.split(SPACE),
	            seen = []
	        for (var i = 0; i < settings.length; i++) {
	            if (settings[i] == "")
	                continue

	            var index = settings[i].indexOf(':'),
	                setting = settings[i].slice(0, index)
	            value = settings[i].slice(index + 1)

	            if (seen.indexOf(setting) != -1) {
	                err("Duplicate setting.")
	            }
	            seen.push(setting)

	            if (value == "") {
	                err("No value for setting defined.")
	                return
	            }

	            if (setting == "vertical") { // writing direction
	                if (value != "rl" && value != "lr") {
	                    err("Writing direction can only be set to 'rl' or 'rl'.")
	                    continue
	                }
	                cue.direction = value
	            } else if (setting == "line") { // line position
	                if (!/\d/.test(value)) {
	                    err("Line position takes a number or percentage.")
	                    continue
	                }
	                if (value.indexOf("-", 1) != -1) {
	                    err("Line position can only have '-' at the start.")
	                    continue
	                }
	                if (value.indexOf("%") != -1 && value.indexOf("%") != value.length - 1) {
	                    err("Line position can only have '%' at the end.")
	                    continue
	                }
	                if (value[0] == "-" && value[value.length - 1] == "%") {
	                    err("Line position cannot be a negative percentage.")
	                    continue
	                }
	                if (value[value.length - 1] == "%") {
	                    if (parseInt(value, 10) > 100) {
	                        err("Line position cannot be >100%.")
	                        continue
	                    }
	                    cue.snapToLines = false
	                }
	                cue.linePosition = parseInt(value, 10)
	            } else if (setting == "position") { // text position
	                if (value[value.length - 1] != "%") {
	                    err("Text position must be a percentage.")
	                    continue
	                }
	                if (parseInt(value, 10) > 100) {
	                    err("Size cannot be >100%.")
	                    continue
	                }
	                cue.textPosition = parseInt(value, 10)
	            } else if (setting == "size") { // size
	                if (value[value.length - 1] != "%") {
	                    err("Size must be a percentage.")
	                    continue
	                }
	                if (parseInt(value, 10) > 100) {
	                    err("Size cannot be >100%.")
	                    continue
	                }
	                cue.size = parseInt(value, 10)
	            } else if (setting == "align") { // alignment
	                if (value != "start" && value != "middle" && value != "end") {
	                    err("Alignment can only be set to 'start', 'middle', or 'end'.")
	                    continue
	                }
	                cue.alignment = value
	            } else {
	                err("Invalid setting.")
	            }
	        }
	    }

	    this.parse = function(cue, previousCueStart) {
	        skip(SPACE)
	        cue.startTime = timestamp()
	        if (cue.startTime == undefined) {
	            return
	        }
	        if (cue.startTime < previousCueStart) {
	            err("Start timestamp is not greater than or equal to start timestamp of previous cue.")
	        }
	        if (NOSPACE.test(line[pos])) {
	            err("Timestamp not separated from '-->' by whitespace.")
	        }
	        skip(SPACE)
	            // 6-8
	        if (line[pos] != "-") {
	            err("No valid timestamp separator found.")
	            return
	        }
	        pos++
	        if (line[pos] != "-") {
	            err("No valid timestamp separator found.")
	            return
	        }
	        pos++
	        if (line[pos] != ">") {
	            err("No valid timestamp separator found.")
	            return
	        }
	        pos++
	        if (NOSPACE.test(line[pos])) {
	            err("'-->' not separated from timestamp by whitespace.")
	        }
	        skip(SPACE)
	        cue.endTime = timestamp()
	        if (cue.endTime == undefined) {
	            return
	        }
	        if (cue.endTime <= cue.startTime) {
	            err("End timestamp is not greater than start timestamp.")
	        }

	        if (NOSPACE.test(line[pos])) {
	            spaceBeforeSetting = false
	        }
	        skip(SPACE)
	        parseSettings(line.substring(pos), cue)
	        return true
	    }
	    this.parseTimestamp = function() {
	        var ts = timestamp()
	        if (line[pos] != undefined) {
	            err("Timestamp must not have trailing characters.")
	            return
	        }
	        return ts
	    }
	}

	var WebVTTCueTextParser = function(line, errorHandler) {
	    var line = line,
	        pos = 0,
	        err = function(message) {
	            errorHandler(message, pos + 1)
	        }

	    this.parse = function(cueStart, cueEnd) {
	        var result = {
	                children: []
	            },
	            current = result,
	            timestamps = []

	        function attach(token) {
	            current.children.push({
	                type: "object",
	                name: token[1],
	                classes: token[2],
	                children: [],
	                parent: current
	            })
	            current = current.children[current.children.length - 1]
	        }

	        function inScope(name) {
	            var node = current
	            while (node) {
	                if (node.name == name)
	                    return true
	                node = node.parent
	            }
	            return
	        }

	        while (line[pos] != undefined) {
	            var token = nextToken()
	            if (token[0] == "text") {
	                current.children.push({
	                    type: "text",
	                    value: token[1],
	                    parent: current
	                })
	            } else if (token[0] == "start tag") {
	                var name = token[1]
	                if (name != "v" && token[3] != "") {
	                    err("Only <v> can have an annotation.")
	                }
	                if (
	                    name == "c" ||
	                    name == "i" ||
	                    name == "b" ||
	                    name == "u" ||
	                    name == "ruby"
	                ) {
	                    attach(token)
	                } else if (name == "rt" && current.name == "ruby") {
	                    attach(token)
	                } else if (name == "v") {
	                    if (inScope("v")) {
	                        err("<v> cannot be nested inside itself.")
	                    }
	                    attach(token)
	                    current.value = token[3] // annotation
	                    if (!token[3]) {
	                        err("<v> requires an annotation.")
	                    }
	                } else {
	                    err("Incorrect start tag.")
	                }
	            } else if (token[0] == "end tag") {
	                // XXX check <ruby> content
	                if (token[1] == current.name) {
	                    current = current.parent
	                } else if (token[1] == "ruby" && current.name == "rt") {
	                    current = current.parent.parent
	                } else {
	                    err("Incorrect end tag.")
	                }
	            } else if (token[0] == "timestamp") {
	                var timings = new WebVTTCueTimingsAndSettingsParser(token[1], err),
	                    timestamp = timings.parseTimestamp()
	                if (timestamp != undefined) {
	                    if (timestamp <= cueStart || timestamp >= cueEnd) {
	                        err("Timestamp tag must be between start timestamp and end timestamp.")
	                    }
	                    if (timestamps.length > 0 && timestamps[timestamps.length - 1] >= timestamp) {
	                        err("Timestamp tag must be greater than any previous timestamp tag.")
	                    }
	                    current.children.push({
	                        type: "timestamp",
	                        value: timestamp,
	                        parent: current
	                    })
	                    timestamps.push(timestamp)
	                }
	            }
	        }
	        while (current.parent) {
	            if (current.name != "v") {
	                err("Required end tag missing.")
	            }
	            current = current.parent
	        }
	        return result
	    }

	    function nextToken() {
	        var state = "data",
	            result = "",
	            buffer = "",
	            classes = []
	        while (line[pos - 1] != undefined || pos == 0) {
	            var c = line[pos]
	            if (state == "data") {
	                if (c == "&") {
	                    buffer = c
	                    state = "escape"
	                } else if (c == "<" && result == "") {
	                    state = "tag"
	                } else if (c == "<" || c == undefined) {
	                    return ["text", result]
	                } else {
	                    result += c
	                }
	            } else if (state == "escape") {
	                if (c == "&") {
	                    // XXX is this non-conforming?
	                    result += buffer
	                    buffer = c
	                } else if (/[ampltg]/.test(c)) {
	                    buffer += c
	                } else if (c == ";") {
	                    if (buffer == "&amp") {
	                        result += "&"
	                    } else if (buffer == "&lt") {
	                        result += "<"
	                    } else if (buffer == "&gt") {
	                        result += ">"
	                    } else {
	                        err("Incorrect escape.")
	                        result += buffer + ";"
	                    }
	                    state = "data"
	                } else if (c == "<" || c == undefined) {
	                    err("Incorrect escape.")
	                    result += buffer
	                    return ["text", result]
	                } else {
	                    err("Incorrect escape.")
	                    result += buffer + c
	                    state = "data"
	                }
	            } else if (state == "tag") {
	                if (c == "\t" || c == "\n" || c == "\f" || c == " ") {
	                    state = "start tag annotation"
	                } else if (c == ".") {
	                    state = "start tag class"
	                } else if (c == "/") {
	                    state = "end tag"
	                } else if (/\d/.test(c)) {
	                    result = c
	                    state = "timestamp tag"
	                } else if (c == ">" || c == undefined) {
	                    if (c == ">") {
	                        pos++
	                    }
	                    return ["start tag", "", [], ""]
	                } else {
	                    result = c
	                    state = "start tag"
	                }
	            } else if (state == "start tag") {
	                if (c == "\t" || c == "\f" || c == " ") {
	                    state = "start tag annotation"
	                } else if (c == "\n") {
	                    buffer = c
	                    state = "start tag annotation"
	                } else if (c == ".") {
	                    state = "start tag class"
	                } else if (c == ">" || c == undefined) {
	                    if (c == ">") {
	                        pos++
	                    }
	                    return ["start tag", result, [], ""]
	                } else {
	                    result += c
	                }
	            } else if (state == "start tag class") {
	                if (c == "\t" || c == "\f" || c == " ") {
	                    classes.push(buffer)
	                    buffer = ""
	                    state = "start tag annotation"
	                } else if (c == "\n") {
	                    classes.push(buffer)
	                    buffer = c
	                    state = "start tag annotation"
	                } else if (c == ".") {
	                    classes.push(buffer)
	                    buffer = ""
	                } else if (c == ">" || c == undefined) {
	                    if (c == ">") {
	                        pos++
	                    }
	                    classes.push(buffer)
	                    return ["start tag", result, classes, ""]
	                } else {
	                    buffer += c
	                }
	            } else if (state == "start tag annotation") {
	                if (c == ">" || c == undefined) {
	                    if (c == ">") {
	                        pos++
	                    }
	                    buffer = buffer.split(/[\u0020\t\f\r\n]+/).filter(function(item) {
	                        if (item) return true
	                    }).join(" ")
	                    return ["start tag", result, classes, buffer]
	                } else {
	                    buffer += c
	                }
	            } else if (state == "end tag") {
	                if (c == ">" || c == undefined) {
	                    if (c == ">") {
	                        pos++
	                    }
	                    return ["end tag", result]
	                } else {
	                    result += c
	                }
	            } else if (state == "timestamp tag") {
	                if (c == ">" || c == undefined) {
	                    if (c == ">") {
	                        pos++
	                    }
	                    return ["timestamp", result]
	                } else {
	                    result += c
	                }
	            } else {
	                err("Never happens.") // The joke is it might.
	            }
	            // 8
	            pos++
	        }
	    }
	}

	var WebVTTSerializer = function() {
	    function serializeTree(tree) {
	        var result = ""
	        for (var i = 0; i < tree.length; i++) {
	            var node = tree[i]
	            if (node.type == "text") {
	                result += node.value
	            } else if (node.type == "object") {
	                result += "<" + node.name
	                if (node.classes) {
	                    for (var y = 0; y < node.classes.length; y++) {
	                        result += "." + node.classes[y]
	                    }
	                }
	                if (node.value) {
	                    result += " " + node.value
	                }
	                result += ">"
	                if (node.children)
	                    result += serializeTree(node.children)
	                result += "</" + node.name + ">"
	            } else {
	                result += "<" + node.value + ">"
	            }
	        }
	        return result
	    }

	    function serializeCue(cue) {
	        return cue.startTime + " " + cue.endTime + "\n" + serializeTree(cue.tree.children) + "\n\n"
	    }
	    this.serialize = function(cues) {
	        var result = ""
	        for (var i = 0; i < cues.length; i++) {
	            result += serializeCue(cues[i])
	        }
	        return result
	    }
	}

	if (module && module.exports) {
	    module.exports = WebVTTParser
	} else {
	    window.WebVTTParser = WebVTTParser;
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(117)(module)))

/***/ },

/***/ 199:
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div style=\"width:100%;position: relative;background: black;\" class=\"main-video-player\" ng-class=\"{'not-video-page-player' : !vm.isVideoPage,'in-browser-fullscreen':vm.isInBrowserFullScreen}\">\n\t<!-- Video Tag -->\n\t<div id=\"jw_video_wrapper\" ng-if=\"!vm.isRestricted\" ng-click=\"vm.playToggle()\" ng-class=\"{'full-screen':vm.isFullScreen,'popout-video':(vm.isPopoutVideo !== undefined)}\">\n\t\t<div id=\"{{vm.videoId}}\" class=\"video-player-wrapper\"  style=\"outline: none;\"></div>\n\t</div>\n\t<div id=\"jw_video_wrapper\" ng-show=\"vm.isRestricted\" ng-class=\"{'full-screen':vm.isFullScreen,'popout-video':(vm.isPopoutVideo !== undefined)}\">\n\t\t<div class=\"video-player-wrapper restricted-section\" ng-show=\"vm.isRestricted\">\n\t\t\t<h3 class=\"title\">Ready to watch this course?</h3>\n\t\t\t<div class=\"buttons\">\n\t\t\t\t<button class=\"btn btn-prmary error-btn orange-color\" \n\t\t\t\t\t\tng-if=\"!vm.isUserLoggedIn()\"\n\t\t\t\t\t\tng-click=\"vm.goToLogin()\">Login</button>\n\t\t\t\t<a class=\"btn btn-prmary error-btn\" ng-href=\"/buy/\">Buy</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<!-- ./Video Tag -->\n\t<div class=\"player-error-wrapper\" ng-if=\"vm.playerError\">\n\t\t<div class=\"error-text\">\n\t\t\t<div>\n\t\t\t\t<i class=\"fa fa-info-circle\"></i>\n\t\t\t</div>\n\t\t\t<div>\n\t\t\t\t<span>{{vm.playerErrorMsg || \"Error in playing video\"}}</span><br />\n\t\t\t\tRefresh the page or <a href=\"/contactus\" target=\"_blank\">Contact Us</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"ds-play-button\" ng-if=\"vm.showAnimatedPauseBtn\"  ng-class=\"{'animate':vm.showAnimatedPauseBtn}\" ng-click=\"vm.playToggle()\">\n\t\t<i class=\"fa fa-pause-circle\"></i>\n\t</div>\n\t<div class=\"ds-play-button\" ng-if=\"vm.showAnimatedPlayBtn\" ng-class=\"{'animate':vm.showAnimatedPlayBtn}\" ng-click=\"vm.playToggle()\">\n\t\t<i class=\"fa fa-play-circle\"></i>\n\t</div>\n\t<div class=\"ds-spinner\"  ng-if=\"!vm.isVideoStarted && !vm.isRestricted && !vm.isLoading\"  ng-click=\"vm.playToggle()\">\n\t\t<i class=\"fa fa-play-circle\"></i>\n\t</div>\n\t<div class=\"ds-spinner\"  ng-if=\"vm.isLoading && vm.shouldShowLoader && !vm.isRestricted\">\n\t\t<i class=\"fa fa-circle-o-notch fa-spin red\"></i>\n\t</div>\n\t\n\t\n\t<!-- Subtitle -->\n\t<div class=\"ds-subtitle\" ng-class=\"{'full-screen':vm.isFullScreen}\" ng-if=\"vm.isSubtitleOn\">\n\t\t<span ng-bind-html=\"vm.subtitle\"></span>\n\t</div><!-- ./Subtitle -->\n\t<!-- Control Bar -->\n\t<div class=\"ds-controlbar\" ng-class=\"{'full-screen':vm.isFullScreen}\" ng-if=\"!vm.isRestricted\">\n\t\t<!-- Progress Bar Wrapper -->\n\t\t<div class=\"ds-progress-bar-wrapper {{vm.videoId}}\" id=\"progress_bar_{{vm.videoId}}\" >\n\t\t\t<div class=\"mouse-hover-time\">\n\t\t\t\t<span class=\"time\">{{vm.mouseHoverTime}}</span>\n\t\t\t</div>\n\t\t\t<!-- Progress Bar -->\n\t\t\t<div class=\"ds-progress-bar {{vm.videoId}}\">\n\t\t\t\t\n\t\t\t</div>\n\t\t\t<!-- Buffered Bar -->\n\t\t\t<div class=\"ds-buffered-bar {{vm.videoId}}\">\n\t\t\t\t\n\t\t\t</div>\n\t\t</div> <!-- ./Progress Bar Wrapper -->\n\t\t\t\n\t\t<!-- Left Side Controls Wrapper -->\n\t\t<span class=\"ds-controls-left\">\n\t\t\t<!-- Prev Video Button -->\n\t\t\t<!-- <button class=\"btn btn-primary ds-control prev-btn\" title=\"Prev Video\" ng-click=\"vm.prevVideo(true)\"><i class=\"fa fa-angle-left\"></i></button> -->\n\t\t\t<!-- Rewind Video Button -->\n\t\t\t<button class=\"btn btn-primary ds-control rewind-btn\" data-ew-click=\"REWIND:{{vm.video_duration}},{{vm.video_current}},{{vm.changeTimeFormat(vm.currentVideoTimeInSec - 8)}}\" data-ew-video-page-stats=\"true\" ng-click=\"vm.rewind()\">\n\t\t\t\t<!-- <i tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Rewind {{vm.offset_time}} Seconds\" tooltip-side=\"top\" class=\"fa fa-angle-double-left\"></i> -->\n\t\t\t\t<img tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Rewind {{vm.offset_time}} Seconds\" tooltip-side=\"top\" src=\"" + __webpack_require__(200) + "\" alt=\"\">\n\t\t\t</button>\n\t\t\t<!-- Play/Push Button -->\n\t\t\t<button ng-if=\"!vm.isVideoComplete\" class=\"btn btn-primary ds-control play-pause-button\" data-ew-click=\"{{vm.isPlaying?'PLAY':'PAUSE'}}:{{vm.video_duration}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" ng-click=\"vm.playToggle()\">\n\t\t\t\t<!-- <i  tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Play/Pause\" tooltip-side=\"top\"  ng-show=\"!vm.isPlaying\" data-ew-click=\"PLAY:{{vm.video_duration}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" class=\"fa fa-play\"></i> -->\n\t\t\t\t<img src=\"" + __webpack_require__(201) + "\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Play/Pause\" tooltip-side=\"top\"  ng-show=\"!vm.isPlaying\" data-ew-click=\"PLAY:{{vm.video_duration}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" alt=\"\" />\n\t\t\t\t<img src=\"" + __webpack_require__(202) + "\" style=\"min-width: 20px;\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Play/Pause\" tooltip-side=\"top\"  ng-show=\"vm.isPlaying\" data-ew-click=\"PAUSE:{{vm.video_duration}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" />\n\t\t\t\t<!-- <i  tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Play/Pause\" tooltip-side=\"top\"  ng-show=\"vm.isPlaying\" data-ew-click=\"PAUSE:{{vm.video_duration}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" class=\"fa fa-pause\"></i> -->\n\t\t\t</button>\n\t\t\t<!-- RePlay Button -->\n\t\t\t<button ng-if=\"vm.isVideoComplete\" class=\"btn btn-primary ds-control play-pause-button\" data-ew-click=\"Replay:true\" data-ew-video-page-stats=\"true\" ng-click=\"vm.playToggle()\">\n\t\t\t\t<!-- <i tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Replay\" tooltip-side=\"top\" data-ew-click=\"Replay:true\" data-ew-video-page-stats=\"true\" class=\"fa fa-repeat\"></i> -->\n\t\t\t\t<img tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Replay\" tooltip-side=\"top\" data-ew-click=\"Replay:true\" data-ew-video-page-stats=\"true\" src=\"" + __webpack_require__(203) + "\" alt=\"\">\n\t\t\t</button>\n\t\t\t<!-- Fast Forward Video Button -->\n\t\t\t<button class=\"btn btn-primary ds-control forward-btn\" data-ew-click=\"FORWARD:{{vm.video_duration}},{{vm.video_current}},{{vm.changeTimeFormat(vm.currentVideoTimeInSec + 8)}}\" data-ew-video-page-stats=\"true\" ng-click=\"vm.fastForward()\">\n\t\t\t\t<!-- <i tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Fast Forward {{vm.offset_time}} Seconds\" tooltip-side=\"top\" class=\"fa fa-angle-double-right\" ></i> -->\n\t\t\t\t<img tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Fast Forward {{vm.offset_time}} Seconds\" tooltip-side=\"top\" src=\"" + __webpack_require__(204) + "\" alt=\"\">\n\t\t\t</button>\n\t\t\t<!-- Next Video Button -->\n\t\t\t<!-- <button class=\"btn btn-primary ds-control next-btn\" title=\"Next Video\" ng-click=\"vm.nextVideo(true)\"><i class=\"fa fa-angle-right\"></i></button> -->\n\t\t\t<!-- Current/Total Duration -->\n\t\t\t<span class=\"ds-control time-span ew-desktop\">\n\t\t\t\t<span style=\"width: 55px;display: inline-block;text-align: right;\">{{vm.video_current}}</span> / <span style=\"width: 55px;display: inline-block;text-align: left;\">{{vm.video_duration}}</span>\n\t\t\t</span>\n\t\t\t<!-- Current Playing Video Title -->\n\t\t\t<span class=\"ds-control title-span ew-desktop\">\n\t\t\t\t<div class=\"p-btn\" ng-class=\"{'disabled' : !vm.statsPrevVideoObj.prevVideoName}\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"{{vm.statsPrevVideoObj.prevVideoName ? 'Previous Video - ' + vm.statsPrevVideoObj.prevVideoName : 'No Previous Video'}}\" tooltip-side=\"top\" data-ew-click=\"PREV_VIDEO:{{vm.statsPrevVideoObj.currentVideoId}},{{vm.statsPrevVideoObj.prevVideoId}}\" data-ew-video-page-stats=\"true\"  ng-click=\"vm.prevVideo(true)\">\n\t\t\t\t\t<!-- <i class=\"fa fa-angle-left\"></i> -->\n\t\t\t\t\t<img src=\"" + __webpack_require__(205) + "\" alt=\"\">\n\t\t\t\t\t<!-- <span>Prev</span> -->\n\t\t\t\t</div>\n\t\t\t\t<div class=\"p-btn\" ng-class=\"{'disabled' : !vm.statsNextVideoObj.nextVideoName}\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"{{vm.statsNextVideoObj.nextVideoName ? 'Next Video - ' + vm.statsNextVideoObj.nextVideoName : 'No Next Video'}}\" tooltip-side=\"top\" data-ew-click=\"NEXT_VIDEO:{{vm.statsNextVideoObj.currentVideoId}},{{vm.statsNextVideoObj.nextVideoId}}\" data-ew-video-page-stats=\"true\"  ng-click=\"vm.nextVideo(true)\">\n\t\t\t\t\t<!-- <span>Next</span> -->\n\t\t\t\t\t<!-- <i class=\"fa fa-angle-right\"></i> -->\n\t\t\t\t\t<img src=\"" + __webpack_require__(206) + "\" alt=\"\">\n\t\t\t\t</div>\n\t\t\t</span>\n\t\t</span>\n\t\t\n\t\t<!-- Right Side Controls Wrapper -->\n\t\t<span class=\"ds-controls-right\">\n\t\t\t<!-- <div class=\"btn btn-primary ds-control continuous-btn ew-desktop\" data-ew-click=\"AUTOPLAY:{{vm.isContinuousPlay ? 'ON' : 'OFF'}}\" data-ew-video-page-stats=\"true\" ng-click=\"vm.toggleContinuousPlay()\">\n\t\t\t\t<img ng-if=\"!vm.isContinuousPlay\" data-ew-click=\"AUTOPLAY:ON\" data-ew-video-page-stats=\"true\" width=\"20\" style=\"margin-right: 5px;\" src=\"" + __webpack_require__(207) + "\" alt=\"\">\n\t\t\t\t<img ng-if=\"vm.isContinuousPlay\" data-ew-click=\"AUTOPLAY:OFF\" data-ew-video-page-stats=\"true\" width=\"20\" style=\"margin-right: 5px;\" src=\"" + __webpack_require__(208) + "\" alt=\"\">\n\t\t\t\t<span ng-class=\"{'green':vm.isContinuousPlay}\">Auto Play</span>\n\t\t\t</div> -->\n\t\t\t<div class=\"btn btn-primary ds-control\" data-click-title=\"subtitle\" data-ew-video-page-stats=\"true\"  data-ew-click=\"SUBTITLE:{{vm.isSubtitleOn ? 'ON' : 'OFF'}},{{vm.video_current}}\" data-click-value=\"{{vm.isSubtitleOn}}\" ng-click=\"vm.subtitleToggle()\">\n\t\t\t\t<div class=\"subtitle-btn\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Turn {{vm.isSubtitleOn ? 'OFF' : 'ON'}} Subtitle\" tooltip-side=\"top\" data-click-title=\"subtitle\" data-ew-click=\"SUBTITLE:{{vm.isSubtitleOn ? 'ON' : 'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isSubtitleOn}}\" ng-class=\"{'ON':vm.isSubtitleOn,'OFF':!vm.isSubtitleOn}\">\n\t\t\t\t\tCC\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- Volume Button -->\n\t\t\t<div class=\"btn btn-primary ds-control\">\n\t\t\t\t<div ng-click=\"vm.toggleMute()\" data-ew-click=\"VOLUME:{{vm.playerVolume}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\">\n\t\t\t\t\t<!-- <i tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Mute Volume\" tooltip-side=\"top\" ng-if=\"!vm.isPlayerMute\" data-ew-click=\"VOLUME:{{vm.playerVolume}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" class=\"fa fa-volume-up\" style=\"width: 15px;\"></i> -->\n\t\t\t\t\t<img src=\"" + __webpack_require__(209) + "\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Unmute Volume\" tooltip-side=\"top\" ng-if=\"vm.isPlayerMute\" data-ew-click=\"VOLUME:{{vm.playerVolume}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\">\n\t\t\t\t\t<img  src=\"" + __webpack_require__(210) + "\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Mute Volume\" tooltip-side=\"top\" ng-if=\"!vm.isPlayerMute\" data-ew-click=\"VOLUME:{{vm.playerVolume}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" alt=\"\">\n\t\t\t\t\t<!-- <i tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Unmute Volume\" tooltip-side=\"top\" ng-if=\"vm.isPlayerMute\" data-ew-click=\"VOLUME:{{vm.playerVolume}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" style=\"color: red\" class=\"fa fa-volume-up\" style=\"width: 15px;\"></i> -->\n\t\t\t\t</div>\n\t\t\t\t<div class='ds-sound-progress-wrapper {{vm.videoId}}' data-ew-click=\"VOLUME:{{vm.playerVolume}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\">\n\t\t\t\t\t<div class='ds-sound-real-progress' data-ew-click=\"VOLUME:{{vm.playerVolume}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\"></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"btn btn-primary ds-control\" >\n\t\t\t\t<img src=\"" + __webpack_require__(211) + "\" alt=\"\" style=\"min-width: 21px;min-height: 21px;\" ng-class=\"{'disabled':vm.isVideoComplete}\" ng-click=\"vm.toggleOverlay($event)\">\n\t\t\t\t<!-- <i class=\"fa fa-gear\" ng-class=\"{'disabled':vm.isVideoComplete}\" ng-click=\"vm.toggleOverlay($event)\"></i> -->\n\t\t\t\t<!-- <img style=\"width: 30px;\" src=\"../assets/images/player-icons/settings.png\" alt=\"\">\t -->\n\t\t\t\t<span class=\"overlay combine\" ng-if=\"!vm.isFlash\">\n\t\t\t\t\t<ul class=\"list-unstyled\">\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<span class=\"title\">Quality</span>\n\t\t\t\t\t\t\t<span class=\"value\">\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.videoQuality === 'HIGH')}\" data-ew-click=\"VIDEO_QUALITY:HIGH,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"quality\" data-click-value=\"HIGH\" ng-click=\"vm.changeVideoQuality('HIGH')\">HIGH</span>\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.videoQuality === 'MED')}\" data-ew-click=\"VIDEO_QUALITY:MED,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"quality\" data-click-value=\"MED\" ng-click=\"vm.changeVideoQuality('MED')\">MED</span>\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.videoQuality === 'LOW')}\" data-ew-click=\"VIDEO_QUALITY:LOW,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"quality\" data-click-value=\"LOW\" ng-click=\"vm.changeVideoQuality('LOW')\">LOW</span>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<span class=\"title\">Autoplay</span>\n\t\t\t\t\t\t\t<span class=\"value\">\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':vm.isContinuousPlay}\" data-ew-click=\"AUTOPLAY:{{vm.isContinuousPlay ? 'ON' : 'OFF'}}\" data-ew-video-page-stats=\"true\" ng-click=\"vm.toggleContinuousPlay(true)\">ON</span>\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':!vm.isContinuousPlay}\" data-ew-click=\"AUTOPLAY:{{vm.isContinuousPlay ? 'ON' : 'OFF'}}\" data-ew-video-page-stats=\"true\" ng-click=\"vm.toggleContinuousPlay(false)\">OFF</span>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<span class=\"title\" title=\"Change Speed (ctrl + >/ ctrl + <)\">Speed</span>\n\t\t\t\t\t\t\t<span class=\"value\">\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.currentPlayBackRate == '0.5')}\"  data-ew-click=\"VIDEO_SPEED:0.5,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"speed-control\" data-click-value=\"0.5\" ng-click=\"vm.changeSpeed(0.5)\">0.5x</span>\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.currentPlayBackRate == '1.0')}\"  data-ew-click=\"VIDEO_SPEED:1.0,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"speed-control\" data-click-value=\"1\" ng-click=\"vm.changeSpeed(1)\">1x</span>\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.currentPlayBackRate == '1.2')}\"  data-ew-click=\"VIDEO_SPEED:1.2,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"speed-control\" data-click-value=\"1.2\" ng-click=\"vm.changeSpeed(1.2)\">1.2x</span>\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.currentPlayBackRate == '1.5')}\"  data-ew-click=\"VIDEO_SPEED:1.5,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"speed-control\" data-click-value=\"1.5\" ng-click=\"vm.changeSpeed(1.5)\">1.5x</span>\n\t\t\t\t\t\t\t\t<span class=\"item\" ng-class=\"{'active':(vm.currentPlayBackRate == '2.0')}\"  data-ew-click=\"VIDEO_SPEED:2.0,{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-title=\"speed-control\" data-click-value=\"2\" ng-click=\"vm.changeSpeed(2)\">2x</span>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<span class=\"title\" title=\"Popout video\" data-click-title=\"popout\" data-click-value=\"true\" data-ew-click=\"POPOUT_VIDEO:{{vm.video_current}}\" data-ew-video-page-stats=\"true\" ng-click=\"vm.popoutVideo()\"><i class=\"fa fa-share-square-o\"></i> Popout Video</span>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</span>\n\t\t\t\t<span class=\"overlay flash\" ng-if=\"vm.isFlash\">\n\t\t\t\t\t<ul class=\"list-unstyled\">\n\t\t\t\t\t\t<li><span class=\"item\" ng-class=\"{'active':(vm.videoQuality === 'HIGH')}\" ng-click=\"vm.changeVideoQuality('HIGH')\">HIGH</span></li>\n\t\t\t\t\t\t<li><span class=\"item\" ng-class=\"{'active':(vm.videoQuality === 'MED')}\" ng-click=\"vm.changeVideoQuality('MED')\">MED</span></li>\n\t\t\t\t\t\t<li><span class=\"item\" ng-class=\"{'active':(vm.videoQuality === 'LOW')}\" ng-click=\"vm.changeVideoQuality('LOW')\">LOW</span></li>\n\t\t\t\t\t</ul>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<div class=\"btn btn-primary ds-control ew-desktop\" data-click-title=\"theater\" data-ew-click=\"THEATER_MODE:{{vm.isInTheaterMode?'ON':'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isInTheaterMode}}\" ng-click=\"vm.setTheaterMode(!vm.isInTheaterMode)\">\n\t\t\t\t<img src=\"" + __webpack_require__(212) + "\" style=\"max-height: 19px;min-height: 19px;max-width: initial;\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Turn {{vm.isInTheaterMode?'OFF':'ON'}} Theater Mode\" tooltip-side=\"left\" data-click-title=\"theater\" data-ew-click=\"THEATER_MODE:{{vm.isInTheaterMode?'ON':'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isInTheaterMode}}\" alt=\"\" />\n\t\t\t\t<!-- <i class=\"fa fa-desktop\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Turn {{vm.isInTheaterMode?'OFF':'ON'}} Theater Mode\" tooltip-side=\"left\" data-click-title=\"theater\" data-ew-click=\"THEATER_MODE:{{vm.isInTheaterMode?'ON':'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isInTheaterMode}}\"></i> -->\n\t\t\t</div>\n\t\t\t<!-- Toggle Full Screen Button -->\n\t\t\t<button class=\"btn btn-primary ds-control\" data-click-title=\"fullscreen\" data-ew-click=\"FULLSCREEN:{{vm.isFullScreen ? 'ON' : 'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isFullScreen}}\" ng-click=\"vm.fullscreen()\">\n\t\t\t\t<!-- <i class=\"fa\" data-click-title=\"fullscreen\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Turn {{vm.isFullScreen?'OFF':'ON'}} Fullscreen\" tooltip-side=\"left\" data-ew-click=\"FULLSCREEN:{{vm.isFullScreen ? 'ON' : 'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isFullScreen}}\" ng-class=\"{'fa-expand':!vm.isFullScreen,'fa-compress':vm.isFullScreen}\"></i> -->\n\t\t\t\t<img src=\"" + __webpack_require__(213) + "\" ng-if=\"!vm.isFullScreen\" alt=\"\" data-click-title=\"fullscreen\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Turn {{vm.isFullScreen?'OFF':'ON'}} Fullscreen\" tooltip-side=\"left\" data-ew-click=\"FULLSCREEN:{{vm.isFullScreen ? 'ON' : 'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isFullScreen}}\">\n\t\t\t\t<img src=\"" + __webpack_require__(214) + "\" ng-if=\"vm.isFullScreen\" alt=\"\" data-click-title=\"fullscreen\" tooltips tooltip-hidden=\"{{vm.isMobileTabletDevice}}\" tooltip-template=\"Turn {{vm.isFullScreen?'OFF':'ON'}} Fullscreen\" tooltip-side=\"left\" data-ew-click=\"FULLSCREEN:{{vm.isFullScreen ? 'ON' : 'OFF'}},{{vm.video_current}}\" data-ew-video-page-stats=\"true\" data-click-value=\"{{vm.isFullScreen}}\">\n\t\t\t</button>\n\t\t</span> <!-- ./Right Side Controls Wrapper -->\n\t</div> \n\t<!-- ./ControlBar -->\n\t<!-- Mobile Control Bar -->\n\t<div class=\"ds-controlbar ew-mobile\" ng-class=\"{'full-screen':vm.isFullScreen}\" ng-if=\"!vm.isRestricted\">\n\t\t<!-- Left Side Controls Wrapper -->\n\t\t<span class=\"ds-controls-left\">\n\t\t\t<!-- Current/Total Duration -->\n\t\t\t<span class=\"ds-control time-span\" style=\"padding: 0px;\">\n\t\t\t\t<span style=\"display: inline-block;text-align: right;\">{{vm.video_current}}</span> / <span style=\"display: inline-block;text-align: left;\">{{vm.video_duration}}</span>\n\t\t\t</span>\n\t\t\t<!-- Current Playing Video Title -->\n\t\t\t<span class=\"ds-control title-span\" title=\"Video Name\">\n\t\t\t\t<div class=\"p-btn\" ng-click=\"vm.prevVideo(true)\">\n\t\t\t\t\t<!-- <i class=\"fa fa-angle-left\"></i>Prev -->\n\t\t\t\t\t<img src=\"" + __webpack_require__(205) + "\" alt=\"\">\n\t\t\t\t</div>\n\t\t\t\t<div class=\"p-btn\" ng-click=\"vm.nextVideo(true)\">\n\t\t\t\t\t<!-- Next<i class=\"fa fa-angle-right\"></i> -->\n\t\t\t\t\t<img src=\"" + __webpack_require__(206) + "\" alt=\"\">\n\t\t\t\t</div>\n\t\t\t</span>\n\t\t</span>\n\t</div> \n\t<!-- ./Mobile Control Bar -->\n</div>";

/***/ },

/***/ 200:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABBCAYAAADFXxqoAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAt5JREFUeNrsneF12jAUhVUd/Q+dAJgg6QRtJiBMAJkgdII6G9AN6AQlE+Bs4GxANiATOHpFPuVwXGKoLN1r653z+EMgur5P6JMskCrLUpHm3Oa2/Bu5zQFBu29srg/avXNa1Cd5IIsbm0ubX2uee7b5Dbjtmc0f/3huzGbGKTFVfLa5A2u3FMjK5vDE3zxqEhNEzLaBEVXPQYmB68WbD4z4024DbsLA9YYHxRd3rjdcNdVqOiQGJUauN0zOfaEGFbO2+ZvQiIXN4hIjJAygmIzQhFOER2eGFzGRxrRFQ7CgMCPzJQYQV2nM8C4mMK7OfL+xjihmQ2jEnZvvzNp4cxNBDCuurtoe03RAMey42jpcmEBiWHFVesN1qH9oWhbTe1yNbUY0MeyEZ7okBhFXY5gBIYad8EyXxCDiaii0FTF5wtX4PSPhKoAZ0GLYCc90SQw74ZkuiakpIGn3hKXBuoEYxtXVudqvrk6YGm06hquKtBfXmgHH3hfEkLXhGp29+xRVz8iTCRg9Y5mMwAjZ+FymywARzzpdA455RooIZvxMlwHHjMzmS7oUGGbIt3xkNfYxXQ6cMUN6yFjtvxeXAmAA36r9Ku13m2+kml67RlMyERzZfCLUNGctplNoK2OJrN5OCauNspiazDPWboBnQ2C6YtJnCJNV3S+EGExTTOfOwIsDDH4j6yVSTLfIxXTpckjmTGHD4Bx5TvU/a1MVBt8TkgvknMrHQuGKFIPh5lS+Vm0rcrlNGBzfjOPP5ITBAGYkDAYzoysYHLyYQtzpY8Xg4MUU6rYrOwYHKabQ98DZMbjVYoqxIYEZg1stppi7Q9gx2HsxaQBhrBjsvZhQ9k1V5MJ2h85rMaFtYlv2GYMRdxRW5DLtGwYjb+9cO3L51RcMRt9rK5/J855g8I5l43Oumt+hK0gxuGA9JUCqru6HAZBPCfjop8THivj8jMXR+RmFzRHJ+Rl53fkZ7wIMAGSsHIIYt6/EAAAAAElFTkSuQmCC"

/***/ },

/***/ 201:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE4AAABNCAYAAAAIPlKzAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAdVJREFUeNrs3NtNw0AQBVCnAkqgBCiBElJCSkgJKSElJBUAHUAHoQPTQahgmVH8EQQR2tnXPO6VrvYjkT+Oxkos27tKKU2UHXVLvZsu+aDuqYcJ+TsEt023M1M3jIv+7IpxyO/+H9/PZSoxgUsYLmV8H4BCOAAWwoUHLIULC1gLLhxgbbgwgK3g3AO2hrsG5CuTF8DJ8r5M4BvgggKOgjMPOBrOLKAWOHOA2uDMAGqFUw+oHU4toBU4dYDW4NQAWoUbDmgdbhigF7jugN7gugF6hWsO6B2uGWAUuOqA0eCqAUaFKwaMDicGBJwQEHBCQMAJAQEnBARcPuCGOgMuP1/UR8DJ8go44SkLOFmOgMsPvwPyBLi8H4X90jPgMsHwP04IBjghGOCEYIATggFOCBYZrggsKtxxurw2cC49UBQ4BttR51oH9A5XHcw7XDMwr3DNwbzBdQPzAtcdzDrcMDCrcMPBrMGpAbMCpw5MO5xaMK1w6sG0wZkB0wJnDmw0nFmwUXDmwXrDuQHrBecOrDWcW7BWcO7BasOFAasFFw6sFC4smBQuPNg13InWB4Dlw61pfb7xOT/Xz3e+T6D6Dcfrepkonjx+toJ3FjxMDjbIa5VvAQYA2Lu7h9jJbJoAAAAASUVORK5CYII="

/***/ },

/***/ 202:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAABMCAYAAADZXmGYAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAASNJREFUeNrs3FGNwkAQBuAFBTgAB4eESqiESkDCSTgJ5wANOAAHRQGgYG+5KwmvLd3JhXyTTNKH3czfL/s8Keecht6V7vO8dSzdPs2Y2p+1sz0Gfee61b2AEJLt8RIiqpn4EkKyLVNKuxRTXdCdSdkW+fddhNS59GbE+VXpS1C20zLF1Xrk+W1gto9IiH9dIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAFCgQABAgQIECBAgAABAgQIECBAgAABAgQIECBAgAABYva6vivEYeT5Y2S2O8QpaNiUHwvLdl+205aPfeVBt/S3M6YfeS8u27Dvqau44+laevvCQq6QbM8Dm2ELWD/jCrSv0psZVrRVz/YjwACGMtdzulhyhQAAAABJRU5ErkJggg=="

/***/ },

/***/ 203:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGsAAABdCAYAAACinJi8AAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABiBJREFUeNrsXetx20gMhjn5L3ZgpgIrFYipwHQFZiqIUoHpCiJXEKmCUyqIXMFJFYSqIFIFOu4M9szjkRYpAktgRcxgLsnMiVx+eC8We3M6nUAYhQVPC44Ljkp82+L/3RV8KHhbcI7/3YAndCMALANOguDELUHpSjsEba0ZvKHAsgAZvnf87COCZnkEq4GMOcsQpImA9e8LXha8QPM5goXmzYA0E/odrLZl6OuuEqwIJXcGeuhZqqYFjD7JLPi3MqAMPaF2za9BsxLUpgnop28odN6BZbXpEfyhI65LBH0g+p0patMdjCTaZyWYaPoIVOaTz0oL/uGpIK9wfV6AZfzTV0+B2qFp9yIaXHoWSFSDisiXPGvhOVAxCC09dQUr9dj0ASbCWx+iwcTjYMLQC5p3sdTWZ00xPJ94CtQrmj/1eVYI/pSPmiK/RMOLtqlgLAZOeF/hv9v0gH/OUeNtOSjGKG7a4X2P6IcPKsTKmMF3ODm5p0PBS3w2XMhhwWnB6zPP6vOMqOBFwXnlN3P896jHb9fyuQXnDkHK8QOHxIs0Hy1DISjTvMdvpjW/Vyd0qSuwFg41KaWWwgbhm6O2xT2B6kILqjU0RYPG9v92FC5nanzG5bXQLxRpQRNYJkzn3OE9wlu1Xgv1TV8+9U2468AyUdUvB6FyrgioED/0bU8B7VVzrMuzMmagYmVAGVpD/+bTSV9LEtT4qhkzUAdlQC0Iv8ldH98VONIqrUCZgIK6cP0IF25qln1WiOaJuqx0ROeszfRx10M7BxxlzeJqaU4UAhUCf+F6Ax07p6pgceRRG9BH3EBdFHBYM2gQ/sNg/nqFqgPREtzuhLduzAkYtWquNKBw3bLwCC1bta1mUUvTHrVKW0Dx94DP/3zOLAalqgUlZcqACgX41vU5ATeaRe2vNPqqLcjoKH43Hw2AvplxrQyoJchp/TbvsXjPDMYMYI0BBUPAETAEAlrAMkIqtbXue50SUYP1qgSoSIFQ/S/goAZrqwCoED+E9Na6Cb5nWAaLckhIrgCsoVvrugYcy2qedS2aNQd9hyrubd5q8izKE+Bns/CB/dQW9HYWf6TWrFy4VmluAc+oNetmrFKw0Z4arI+CtesEyonaDEaC17pTjtUugOuhTPv7U4MVC68IfFaoYeZ9H8z7f8C/UDneSPjCTVoxhbdRrl2ApookV9C+dzAvxwAGLMrtjKkSac07BEIhccifX5qLBsRVhzsQNJhKqGm/+HsHDKF24hlYCYNWgwTNGsFi1qwN8cvcg77OpiZKif1Vr/2+gClhTD0CS4RWlcGi1q65B4GGCSyojz9tJII1AYEDgbtWDJjyvIupfOTnAPRbCJILu+fMH3Uzzc++wUpQydKpaakQKDuYmZp6f19usGYKzSFXMw05WHuGl/wOCiaOlfwUx5nqFRCU9QJHZssIgvS6ofFTT0y/TfJdq3MwOA7VWbIjTbdCgeLqziUbjlzVrAOqLAfZY5nTKwIKKIOVugkzEfDObbIz/iS0L2eMpg+A+FBh3U6xyYteGBdgNOwvGHab3bZQPzE/hzQSbhq0xTUTo86ez8FtY2iKpol7beRzd5t6MA6OJN9sVv7CaCliflaMQvED3DR7kueX56ZPu26MtP0JG2JNSsHtpWvPHMJ+DqyhTrDv4e22067AhahF9rZW1y3TbHOq2sx1n2MVYkjaQfvp07cDv2vvIZB9wLIViHsY6RyxXjvYFiw7J2K8ee59f5tyPqDLlUy+X3Eh0k+1Cd2bIsMYKxAjOQaqK1gWsPmIz7/k9HqMSw4mmDzoy4iT+12ES0+RGMAertgk7mCA7Z6+t6leY9Ax2HDmvueztgjY7kqAWsGAU7QpDtPluICV50B9g4Hv2rqhPf/tbPvBJZk6ZQIC2hGoj6ku0Sy+egLUC65HRN8IxwFwaxYfgKe1zQUZYfsEwoYyc57WtyPYnhWF+HsUshgEdmFR+6wmClFKjU+7FQjSDn3tUrIkuQKrGoQYnglYP8fOtFdgWYowyjLAudx6+Qlvu9CqLgkYEqwqcDHylBg8e8/xBlnbLQ7iwKqjGN6Gi1gu06wmRdgiGNVLqL2gfwQYAIjv8kwoYcQ0AAAAAElFTkSuQmCC"

/***/ },

/***/ 204:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABBCAYAAADFXxqoAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAt9JREFUeNrsne9xm0AQxS87fLc7iDoIHVipwHIHpAI7FcSpILiDpIJIFZhUEKWCkA6kCshijglSMJIQgvdu9s3sF/1B9/T27n4CzeCKorjWyop/yrUetRxBJVqbxti/as1Ixv5fOW+gTWutOfDg41fGvSFqpp0S59zCteud1rNWqnXt8JS88viV1iettdbcEUn84Lt0740twMYeH3gevZlawzhGb7W+ay1ZjBE0U+8wat1q5VoPZIFQNJP0eE+5rH3RyrRmZKFAN5Oc8d4brd9aj2SBwDaTDHAMSnJBbCYZ6Dh05ILYTDLw8e79mrwgCwSimeQCx7xqkAvbBj8pBssFj33rjRkGA4SxTy4xWSijY7CM9Dklufw0DMYIo0kuuWEwRhj1mmwYDBKGYTBgGCFg8KDNJCDGWDF40GYSMGPMGHx2MwmgsRAwOA4ljBAwuFczCbgxdgw+qZmExBgrBp/UTEJkrCaXUU5NTNFMb4qXv7LRadsIiE0rT115KGGwa+s3+NTCwNEPP0vWbHtGiLrxe2BiMwNr2YptZuCQYmJh4GhuYeAosz3D9gzTnl5+BEb2PUyqXx5r7XfGxMvSZ1dd91jXD0bEZmokZPzVXc6GfP8JITWz01FEDXTnqusbedsLhMzMhy4zwPrmqtP+y64XsSxTKz+1N2Qh/PHjzo55sRCYee+qizJsQdQbdHbsG5BnxpOrzvmzhbCDq6coCskMwJ5WNk/a9wARmJnU8f1fqhNXGcMYxMxEDZQcoqRjJQBm7kLGVZaZUZp5CB1X0cO4mJmRcDW9VANFIZlhJ7woJDOIuIoUxqhm2AkvCskMIq5OjbbMuPo0NK5OOTMMVwFmRn12NXGcZ1dnCKgdDWSGEVd3/nSMoHPCMFwFWKZKMx8d53XolR83JGpHPaa24erEM8NwdaQwtqRmDi2RNeHRoLZ0fMnoZpYMuHqSWu6fsSG7f0ZTS38rB8r7Z/wVYADFvXE7diYwKQAAAABJRU5ErkJggg=="

/***/ },

/***/ 205:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFgAAABJCAYAAAC5H+EKAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAApNJREFUeNrsndFRwkAQho8M79gBsQKwAkMFhg5CB9gBdCAVCB1gBYQKhAqEDqACvNPNDDMyCEk2d3v378y+IBj58ufyBXNH63Q6KVTletD9pjvV3aHHZrrHrTPAsXlAd7/EBg66l7rnAcJN6X13LvxsVADOaA90Km5sqzsh4L5XTGCfrzxnbQAbIKsaN7wteRRIKnOkT24I5N4Azv/ZC2Vq5Olw0af31bv1BREDXEXDhI8nsc974JpqM45PvlRCqe2WeXFboa6l1oB9qfJLInC8WMaqdlXhIsHl1AsJLllGuzZ1n/SR4BLqhQQzqxcSzKxeAMysXhgimNULCWZWLySYWb2QYGb1CjnBjahXqAlOCW7XpT+q7UlqG1WvkIaIsQ31CiHBVtXL9wQb9fqSAFdaghM6ifUkpUFCggv1WkmDKyHBTqqXD4CdVi/pQ4Tz6iU1wWLUS2KCRamXpASLVC8pgDPd78rzsjVEpCHAtQl4ogIpW4B7AMxbewDmrTkA84/BCwDm17QpAPMn+VH3GoD5akdXdK+6jwDMV+aSOdb9AcB8daCrvKEvKufqv4zMvGdz+9MMgHnTbD58H6jf6bkAzFQ5pXkKwFA60YBFKp3Ue9PEKJ3km/9EKJ0PN2A7rXS+3OHurNL5NoXgXOmOAMyrdH0XlM7naVyF0o1spjmEmZ5zm0oXylTaQukGTStdaMsZ5E0rXYjrRRRK99SE0oW8IMemCaXDmj3MSgfAzEoHwMxKB8DMSgfAzEoXMaYASkeA90x7H0qnyyzQnKl6b+c/0onC12XGY3XHdLOInryoEW6i/F7DvVC64Q1pXhdjcEYOWOXScUGH0SaQk+CS0nwtnHkL36NRSyXq71KNP9/I8C3AAMKXnQvG6dc7AAAAAElFTkSuQmCC"

/***/ },

/***/ 206:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAABJCAYAAABIFLqHAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqFJREFUeNrsnNFRwkAQho8M79CBdAAdECtAK5ASsAPpACowViBUYKjAUIGhA6zgvBs2mmEiE8Ilt7u3/8y+KMkw68fmC16up7UeKqU2pqbqlIOpxNSLktyUnmmubeRTxe/2pham0sB6YmGbm3pocOwRQE2K5tofDC4csAaKjwE0dgIwDW48z87+cWxzdY0XH4DiDePGjkxlDhpbZFu3ub8HwEeGI8X/jcfGia58/cxUDhRzS9zGBU03PHYHFOdMmqtdnzC64Virbl+ibO2Qy03bUJFbztjUh6kVeKLEIbnlfMMs3gi57mM98R2aOwqZ3KjFc89AyhcyFtrNDpqcyVhwH6ttn6FpW1fknn9PMUeobWTJLecuFG3zQS5WbWNBbjDaFiF5Hyy1zfdYwKRtmiu5LLUNI7m+tC0IctloG3Zyu9S24Mit0raUirZFil6mVLSN0lioyh5GRSZjwX3GJW0bCrl4tU3IraFtCRaKOZF7q7YJuZS0jSu55xTHNYxCS3ObN9iuvc1lLLQzJjq/6QiluQrIlea2lFya214SaW47WSoP6yT6AVjCtTcTQm6NvMENhLc1ERzJRbNcihu5S/X3oJ73cCHX5ZfmQm7pgvUMtKJb+0uZXPTPwfWJ0upNrziPBe96xZFcrKvRyZO7BFpTYp8y1ORSeAKIHLmFXsWUG4uR3C3QmisG6SOilYReURsLa0p6RYVcknpFgVyyeoWZXPJ6hZFcNnqFjVxWeoWluSz1CsNYYKtXPsllr1e+yA1Cr7oml9u2gyjILeuVNNYhuRy3erWwDHySay9Yj+q05TS3PXRTn2PB6tWEsV6tHJ9vX+eZiJA2hrej7tXRiIkjmJ+X9GqiwtGrxNQ9GFDTphb//s8suVU7zIteOciPAAMAvTb1Puzo8NsAAAAASUVORK5CYII="

/***/ },

/***/ 207:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAAkxJREFUSA21Vj1PG0EQfWdsAwKkRKSkDUJOB+mi5C/QJFF+QByRJj3d0UT5ByALJHqjpKeElGnjjlRpE0EEgtxhX96b2cWmwHKwb0+zn7Pv7czuzl6CmLawjgJv2XxMqQRJWCb8PBVWU6sIPYNFjw1Jl3JC2UOKzyzD9BQfMYdNgz7HX/Zfa5BQDpZYzalU9zGRS8MXYp1Wr2IeddO6wCcSbSbMXrKzjXP8pt571HCMnMo1quWm6tOH5Rn156hQcJkJrcnxjK1tLOARzvCmysZGgGpG84bhjTh2QKxr0n0hbVMky7jAH5ZHBpBiBg2upWOmj4gZ1Bpcboc+SHFFpx+z94woKyKZpmQUbTaM4LVtnjX/O3sVZiwQLyNVgWkB1yjxZOBeFgyu5EnwQGaboENSF8lUIHFVmTxOivP9fGnxUyJxN40DfPdcLTiJJGIsI9nd6lvh5k2ayFwfSRKeBL/Rk6W5ZUkRAsFkKQJa2ZYYTSQpy5Jbe6I7X8ae3FjSI3xpBGKRu3q842WRCLdQ/NdLFvdm/NgVo7e7XyTdCq3IA4kTfR8zdsX5dfOOMHOFeoV5PZiyCHgO9QExZFtjxKxNPc1vEyszPEX4KwH+wCxe4BJrrB/ig73xoOL9kgg8rRL3IXG/Vrnt+7RCJNvY4t/KDL6x3g//s3TfryF8ixy7vDk4FQJ3+dI+Ze8OccH6vp+qFC08QJPKoB0/mfvfCitMo7wvg6ezyrd2iWTAKXaRotkfTPGO3ZIGxVMcHXbE+79IcZbKDqVFgpYa/wBYoZjk+M6h+gAAAABJRU5ErkJggg=="

/***/ },

/***/ 208:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAZEUlEQVR42u2df5CV1XnHP3u5s123W8IgpQxDGUooQwglxBLGKjW3xBrrpNZfQWN+aAwxxN8GKWEYystQwzjG+AOMGqPVkBBEozQQYtTSrUVjCMNQYillqEMJZShDCd1sN5vN5qZ/POfCZdkLd+89773ved/vZ+aOJLv7vueec57vec6v5wEhRGZpSWzJInLAWGAWMA7oAM5x/82V/Tfv/j2QDvezgZT/bSXa3KdefD1nKAw/y3dLK/1A9xB+v+h+v1jlz4tAj3sPQO+AT7f7/A9w1H0OAwfcv4tEEoBqDP4i4INAAZiU0c4s0sUxYBewFXgV2Ab0JkEQWhJi9NcDnwSmyuBFRgRhE7AGeB3oa5YYtDTB6Evu+UeAG4A5QKv6hMgoB4C1wBPA/kYLQUuDDX8kcDNwNzBabS/ECXqAF4AHgV1EFdcmAhOAk4Y/H7gdGKO2FqIi/U4IlgL74vYIWmI2/g7gFjfiy/CFqJ4u4BHgASKOhyUAtrg3C1gFzFRbClEze4FbgS1xTAuGxTTq/w22qDFe7SdEXZyL7ZL9LgV+RCe/SKYHYHOV84EngWlqNyG8swfbLt/ua21gmCfjzwHXAs8Bv692EiIWRgFzgZ9S4N/orH9KMMyD8bcC97j5/jlqIyFipQ24EjiHAlvpPHE0uQkCYPP91cBCdIJPiEbRAlwIjKfAa3TS13gBiBgOrHMuiRCi8bwPmEqBH9BJb61KUovxtwPPAteoDYRoOp3A1UQci98DiGhzbv/HVe9CJIIJwB9RYBOd/DI+AbAFvy9hp/uEEMnhD4F3U2AznfzKvwDYVt8d2CGfFtW3EInjvcC5bk2g6FcAClwIPI2u7gqRZM4DDlJgB51n/+V8laP/aOApoL2JX6zIyfBLxzkZqqmLU8M69cAZt0VKz6iHbqhv/1XURKXwb7U+q53K29eDhY4rD7dWHu4tN+CTdwNlM7bGc8ADwNvAW2f75ZYqjL8Vi1zSjO2+XmA/8CbwY/el3o7zdpQQQ8Kmxu1l4jQCO7E3DfhL7Hh8RxPEYA/wJ2ezlZazfDmwe/yPNbDg/cAR4LvA88BWotoPOgjRRHHACcFHgeuAiVV73X74ErD0TLcIzyYA44B/wQJ6NMLFP+Dcl7W17GkKkWAx6MBu9S1ooBD0ABcSsXPoAmCuzWNYCK+4OQJ8DVhFxBH1FpFiIRgJzAPuxALixk0n8OFKXvSZBGAm8AbxrvoXgS3A7UTsUe8QGRKCyVj8v0sa4A18Bnh6sCvEuQqFywP3xmz83cCXgStl/CKDArAXu9W3AGKf7i6mwg5epZXJjzhlitPl/zSwiGhI2VyESJMI9BHxCPAx4GCMb5pEhXs7LRVG/zewmH5xcBi4gYhX1AOEOGF3s7ALdlNiesPbwB8PXAsYzAO4SMYvRMMFYBtwNbAvpjdMc579GaYAtvK/IKYCHAc+I+MXoqII7HZT47h2whY4G6/oAUyMae7fD6wkYrNaWYgzisBWbIswjrWx852NVxSA6/G/JVHETvU9pNYVoirWYedifOcByDHgSH9LmfLkgZ/EsAixF/gQUayrnEKkzRMYCfwDMMPzk3cAHygdD84NWCTwbfx9wAoZvxBDFoBjWLBd31OBGeV2Xi4Al8XwNd4C1qs1haiJ14BNMUwDrjpVACIA/sLzi7rd6K+bfELU5gWAXY7r8vzkD5WOBZc8gBH43/vf4j5CiNrZDt53z2bhgpmUBGAmfs/99wFPxZHNVIgMegGPYld7fdGOhQ47IQC+R/8Dbv4ihKift8D7hbnZ5QLwfo8PLgLribwqlhBZ9gL6gW/j91zAn5YLwFSPD+4FvqNWE8IrG/C7JTijXAA6PD74KLBL7SWEV/a5qbUvhpcLwAivBY0UMlsIz9OAkgj4ooOIXBwegGL6CREPhz0+Kwd05Mr+hy8Us1+IePBtW7m8S/zhky61k0igC90KjMe2vN8LjHY/+VdgK5Zwpjfh3+J/PT+vtZQeySc/V28TCTL8PLbivQCYU2b45fQCG4hYAexJ8AE23xeD2nP4T1mk038iKcbfAXwR+D6WmWd0hd9scz//PnCz+7sk4j0+QCkBYpJVSohajL8NWAksw3L1VcN4YBXwbSLOGxg+KwH0ehaBjjg8AG0BimYbP1jkm3kMPcJVHgue+T3gDiLbL08Ivm0rF4cACNFspriRv62OZ4wB7ge+RcSMBHoDfhQAv4uAReemCNHMef9yBgS/rJFyb2B+AtYGeuKYAvgOAqopgGiW8eeAG4ErPD95LJbHbw0R05voDfR7FoC83H+RJs4DlhBPTstWJywbgXkJ3ikY8hTAN9oGFM0Y/UdgCW3HxPym0k7Bk0RMGizjbmgC0ObZ+BUHQDTa+PPAfODiBr2xFTs3sBG4wr0/RFrjEAB5AKLRzMJO+jV6SjsFWINlvRoVYL21aQ1AhD76j8IO/DTLADuAL2CHh6bFPCXwvsMmARChu/534uLbNXkqfTEWCeviGHcJ+nx72BIAETIF4LYE9ePJwLNOBIIgRzxbJkLEPfqPda7/iISVbCwWxntCKGsAbepNIjDjbwUWYfksksgk4PoAvAAtAorgjB8sj+W8hJf0ygR6J1oDEMEzATvw057wck4l/kNJEgCRqdG/DTvqOzWA0ra7qUDiBcC3CCgbsIjL9b8C+ERApU76fQHvEYF0HVjExSRgBWEtWif9ZmyHAoKIEEb/duyO/6SASl3Ebyaf2DwAIZJs/Dns4s01gZV8D7A/BlHxvgYgRJKZioX3CunAWp+brvjOkuU7IpAEQCR69O9whjQ+sJK/iOUZSDwSAJFk1/8mLCZfSBwAVgSQZUgCIBLNDGAxBBVso+T67wmlwBIAkcTRfzh20WdMYCXfAKxNcGoxCYAIwvWfj+XxC831X04UVkg833kBhKiX84CFgbn+/c5j2RNYXbcrHoBI0ug/AsvGE1p8vc3AN0Ny/R2tmgKIJLn+twAXBVbyQ8AyojCT4koARFI4n+ZE9vXh+u8KtdIlACIJo/9I4D5gZICu/zcCdP0lACIxxp8H7gYuCKzkB4GlRHSFXP0SANFsZgN3BOj63wu8HXrlSwBEM0f/0diq//DASr6JMFf9JQAiUa7/QmzfP0TXvzsNzSABEM1iDrbtF6LrvzstjSABEM0Y/cc61z+0U6ipcf0lAKJZxt+K3fKbLtdfAiCyZfwAl5L8pB4D6Uub618uABIB0SjGYQd+QktHF+pZ/6oEwHfs8qL6uajg+i8Hpsj1T68HUISw7kOLhrn+VxFWUo9Uu/5aAxCNZCJ2aSa0q+epW/WXAIhGj/5tzvgnBFby/Wl2/SUAolGu/3XO/ZfrLwEQGWOyM6R8YOV+EQvuiQRAiNpd//uAsYGV/B0swk8mFrMlACIO488BNxJeUo9eLK7/3qw0lQRAxOX6Lw/Q9X8BWJcF118CIOJ0/R8ERgdW8n1YXP/eLDWXBED4dv3nAZcE6PovcyKABECI2iil8g6tX60DXsiS6y8BEL5H/3bgYcJL6rHXuf59WWw2CYDw5frfDBQCK3kPsBQ79YcEQIjamB6o678W2JBF118CIHy7/iMCK/luYEVWXX8JgPDl+t+BxfYPiW7n+h/IehNKAEQ9zACWBNiPvgl8N8uuvwRA1Dv6dwCP4j+iVNzsAu4lol+NKAEQtbv+C4FZAbr+S4g4qEaUAIjamQXcE2D/eRp4Wc0nARC1j/7DgccIL6nHDmClXH8JgKjP9V+MLf6FRBewmIjDakQJgKidC4C7AitzEXgceE3NJwEQ9bv+oSX12AHcn+bIvhIA0QjXfzkwLbCSHwcWEXFUjSgBELVTAG4L0PX/KtCp5pMAiNpH/xHAE4QX3msb8KBcfwmAqM/1XwlMkusvARDZ4xLCS+VdBB4BXlfzSQBE7aP/qEBd/zed6y8kAKIO1/8BYHxgJT8GLCDiuBpRAiBq5wrCS+VdxMKRb1PzSQBE7aP/GOyab2h9oxN4SK6/BEDU5/qvAsYEVvKj2Kp/txpxaOQz2MkBJmJhrMZgkWF3AtuyHh8OmAtcE1iZ+7Gtyu0yZwnA2Yy/lK76cqB1wPxxDxFLsFBR2Ts8EjHWjf6h0Qk8LtdfU4Azde48EfOBH7oRrnWQepgKPA886S6+ZM0reozwknocxlb9e2TKEoBKnXs4tp/9KDCyCo/oJuB7REzIUD+40XlFobn+9xKxS2YsAahk/KOB55xRD+W7zgZeJeK8DAjkOGz7LDReBr4uE5YAVOrYE4CNwKU1PmESsJGIOamdX9qq/1OEl9TjMLAwa6m8JQBDM/6XqD9q7Vi3LnB5SkUgxFTefcAyIvbIfCUAgxn/ZGf8vuLWjQS+BXwiVSIQMQm4P8CSbwaekelKAAbr1FOAv8d/0MoObJV8nnObQ6+nPPAkBLfbccC5/n0yXQnAYMb/EjAlpjd0YPvkaRCB2wgvlXfJ9d8ns5UADDT+icB3YjT+Em1YJtzb3CgaYl1NxQ5DhcaLWE4/IQEY1PinNuiNbW7ufEdwIhDRCjxLeEk99mNx/ZXUQwJwmvE/T+MTVbRi58/DEYEIgC8CMwNr5V4sn99+masEoLxDjwPWQNMO64QmAjOxrD6hsR5YJ1OVAJQb/2hn/Bc0uSRhiEBEO3bgJ7SkHnvd6K/IvhKAE515pOvMhYSUKNkiYK7/MsJL6tGDUnlLAAZ05g7s3PpHElayJIvAbCyfX0htXcRW/F+UiUoASsafBxYBn0poCZMnAnYT8klOv/6cdHZje/5y/SUAJ9zYy4F7El7S5IiA1dkKYHJgfbIbpfKWAAxgtOvMISxiJUUE5gDzA3T9nwY2yTQlAOUUaNxBH98i0Hj32xZKnwjQ9d8FrFB4LwnAQFf2zwOs25II3NZQEbB7CvdigU9Dogu76KN8fhKAU2iDYMNzNUMELmPoEZCS4Po/Drwmk5QADGZEIQfpbJwIWFKPhwN0/bcDK+X6SwAqjQ6h3/8uicA9RDEtZJrrf1+Arr/y+UkAzkgP8E4K6roVWA78dUwiMBe4PrA66QdWE7FVpigBqDSyFYF/TEl954Gl3kUgYjx2RTm0GAVvEWZYMglAg3klJV6AfxGwswYPA+MCq4ejwN3K5ycBqIZDzmi6UyYCy91NvXq4keTdjajG9b+fSPn8JADVjXJgd8IXuTWBtIjAPdiZ9/Ya62UydkIyNNe/E1gt85MADHUt4GvAEkhNVNicE4H73U3HodRHK3YzMrRU3srnJwGoWQT6ga9iq+lpEoFbgAeGmJT0ZsJM6qF8fhKAukSgD/gK8OUUiUDJoB8lqiJNV8Q0LMhHaK7/K86LExKAukSg1819V0OqIsV+AniK6Axpum29YBXhpfI+6Fx/JfWQAHgTgaVuSpAmEbgKWEPE2EG+M1h0n9kBuv7Lidgrk5MA+BSBHizSbdpE4FLgOSImDjgfPwtYGJjrXwS+i/L5SQBiFIElroOlSQRmY6nOphNRHg8xtFTeB4FFSuohAYhTBLqBBcA3UiYC04GN2BXfxcD5gZXfpmlRak5xSgASLAJdwN0pFIHxWPajewjvjv8GYK3MTALQaBFY7zpgWmgnvDv++1E+v0wKQC4BInA78ELKRCA011/5/AIRgKLn5zU/82zEMeDzKfQEQnH917m6FwEIQDqvY5oI3CoRaDj7nOuvOs+gB5BUEdB0oDHYboySemR6DSCpIrBJIhC76/8NlNRDApBAETgKfE4iECtvY3v+QgKQSBE4LBGIDdt+NW9LSAAkAhlz/R8HtqgqJAASgeyxHQvyISQAEoGMcdy5/l2qCglAqCKwWSJQs+v/ABFvqiokACGLwGclAjWxFXhI1SABkAhkj2MoqYcEQCKQSfqxRb8dqgoJQBpFQAuDZ2YLFoJNpEQAFKX1VBHQ7kBljjjXv1dVkQr6cqAMLRKBql3/ZUTsVlWkhh5NASQC1bIZeFrVoDUAiUD2OATcqaQeEgCJQDZd/0UK7yUBkAhkUwTWYyG+hARAIpAxEdgPLFRkXwmAyJ4I9GFbfofU+Imh3bfNSgBqE4EsnBh8GsvpJ1I8YOfQCnctIpD2Y8O7sbj+6hvppug7LHgOaJMIBE0PcKvCe2WC7jg8gNZMVF16ReARoFO2kR0PQNQvAmlZGNyGwntpUUEMWQTSsDvQ7Vx/3fGXAIgMisC9RGxXY2ZPAHS1068IbAhQBF5H4b2ySK/iAcSzJhBSLkLLpKw7/pkVAOFXBEqpyUMRgaW64681AJFNEdgEfF0NFgyt6ChwcCKwFhJ5meYwcLvu+AeF90N2vhcBcxKV00TgVixtdpJEoB9YoDv+WgOIQwDaVa+niIBlzk2WCKzF7vmLbNMXx2gtD6CyCHwtASKwD93xFzLWpojAQiyVdrOMr8/N+4+oQURJAHx3xryqtaII9ACLsEM3zVh8+yrwihoiWPKeB+1+33kBsnEduH4RWAp8mcaewtwFLNcd/6DxHRGoWwFBmiMCvcByYEWDRKAbO+13XJUvBo7YigfQHBHoc17AAqAr5retAN5UpadiCuAT7xGBSm6KqF4EHscODB2N6S0vA6t1xz8VtIUwBdDOwtBEoIjF3f8keD+YcwBb9Vf+x/R47N49AN+d43fUTjWJwMvAR4Ednp56ELgW2/cX6aDD8/N6cjGcBR+udqpJBHABOf7KeQT1bM/uBq4E3pLrnyre5fl5J04C+pwGjFA71SUEB4HPYCcHh5qUoxtYDXwY2C7jTx2+bauYL+s4vkbu0WqnukWgh4jVwGvAncBlwLgKc8B+N+JvBb4FbNMx39QyxqfxA90trsP9JzDe04MPAO9WJ/QmBjnX8GOBKWXzwOPYld7DwBHgmEb8lE8R4SfANE9P7CLiXeUegC9GAedhIaZF/Q1fdFOBQ6CgnRlmksdB2gSgzKXc6fHB7cDVai8hvHI5fncB3i4XgB96LuxVRDoQJIQnLzAPfAy/5wC2lQvAW56LPB6Yo5YTwgszgamen/mjcgHY5XkdoBW7fKJTgULUz+fxe8S+f6AH0Iclh/BJAbhYbSdEXe7/dDf/98lO3N2TnHsJwPc8v6QdCz2lACFC1M7d+D8AtLm0ZZw75f/0fzFoNnCV2lCImkb/i2Kyn++X/lEuAAfcWoBP2rCkk2PVmkIMyfhHAg/i/27NAcounOXKXlgE1sTwVSYCK7UgKETVxg8WNm5GDE9fR1k8yoFGuRb/14NzwFzgJrWsEFUxF5hHDPf/gTXlR8YHvuCIWwvwTRtwHxGXqW2FOOu8fxX+7/6DnffZPXB0ZsA04DHiCRQ6EniCiJlqZSEGNf5pwFPEd6N21cCo0IO5GK/j/2RgiXHAc07lhBAnjX8m8Dx26ScO9gEbBpufDyxIP3B/jF91IvA8EXPV6kKAmxq/hF33jouHGSQEfUuFArUCb0Cs7noXcB/wkIJWiowafhtwC7DETZHjYh/wfqLTj/sPG/TXO/k1Bf4Di1TbElOhfgv4IHAhBXbSyX+rR4iMzff/DrgZ+O2Y33Yr0eDBZodV/JMCPwXeA7w3xoLlgD8ArqZAKwX+nU7+T71DpNjwR1HgLixP43TiD6O/FVhMJ78e7IctZynsZODHNCbSbxE7pbQKeIaIY+otIkWGPxK4EbgVmEBj8mf0AB900aapRQAA7sKOJDaKfiz81YvAc1h0W8UXFCEafR5bR/socA0W17GRl+OWAX97poSwLVV8iVZgI3BJE6qwBzu48BIWwugQFgyz2336XaJNIZpl5G3OqDvcZwQWxHUGluNhKs1Jl7fdjf5nXGBvqfJLTsLCho1qYlX3Oe+g300XimVTB04IgtHL2bPuFp3A9MdUVgmTX9qIJ/FsnurSbreXvb9k8JT9Xc598u7TzCS5x4A/Izr75b5q3ZF9wKexgwptTfpSrWep1FGyESEounWGqm72DqvqkZ1AgX1ulL2E+LYGhRD18QCDHPmtTwBMBH5DgR1upP2A6lmIxLEWuGso+T6HDenxdkBoC3ZeeZrqW4jE8ApwA9HQztEMG/JrOumnwA+wc8vvUb0L0XQ6geuI+NlQ/3BYja/ro8DLTgCmqP6FaBqvAdcSWZTfxgiAicAvKbAR+D0sF6AQorFscsb/s1ofMKyu13fyKwq8im0nXoB2B4RoBEXgCeBzRPy8ngcNq7sotibwT8A7wIewW35CiHjoBb6ARdv+Rb0P8zdiRwDMwiILT1Y7CeGd/cBngS3V7vPH7wGc9ASgk/+iwHrgXOB9mhII4c3lX4NFC/4JEb/x9eB4DNRuQV2KXe2doPYToq5RfyGwIY5bsfGO0HYHegFwG42JKSBEWugCHsGO9h4vj+UfjgCcXBuYgN1Nvo7mXSYSIgR6sOw9K4F9cRl+4wTgpBDksIjAtwOfwn/GUyFC5jjwjJs2vxO34TdeAE71CEYD1wMfByUKEZlmGxYcdD1wrFGG3zwBON0rmAZciy0azgAlERWppgjsBF7G4mvs8rWlF54AnC4GY4CLsXDhM7FwSnn1GREw/cBeN9L/M3Zr71AzjT6ZAjC4KAzH7hmMx3YR2tEiokg2fdhCXhcW5XoHEceTWtj/B/+NzAtf6lgEAAAAAElFTkSuQmCC"

/***/ },

/***/ 209:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAABgCAYAAAAXZvyIAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA6BJREFUeNrs3O9x2jAYx3HB5T3pBHEnKBvU2cAbxJmgzgaMkE4ATNBsUJigZIImG8AE1Cpy6hLAkiXHeuD7u9Ob/PFd9MkjS7LMYLvdqjPLddkmZcvLNqp9fV62omxr6X/g4MzQNNTjHlY9z2VLpcMNzwxsegJM54upQiotgujq+mb5sxszhFJpPWbmAKZMJSag9Qt21+L3QOtphtgWTHyuhIItzKTiIjMEDDTAQHvLuGwvgMlBG5sKG8ElAw0wYWg5YLKm/BpsCo+cSgNMGFoBmKzhcaYudFtKaqUBJghN73I8ASZneEwMGLscQiotLdsKMBloejjURwN+smiOf3jUWJnaHaq5odvDoyUq3OP4sWkZldUNmq6CnEqQg8Y6SdhEZAKYPLScbpA3PHIPa7cxUE3W1mbN+WHXH9L/zrPhRdl+m7Wmbr/U7vxK1vH1c9Dck5kO/Hrge3q0+mEmdL5gx64/ra5/hYX1cGUDUk3o8pZgI4vrL6g0uxQOmwN3jhXnenhpApr90OgSW7g2p81uQLPsqBa/0wTX+nggaN3mGJzXeU7Q7LIMCOd9ABc0u8wCVVyIE9NL/c719gIRbk3nuSSGJ+36ffGUSrNPqnYfadErmP7nAc0+6x7h3sC4p8mA+w8MtPjh3oGBFjfcQTDQ4oU7CgZanHAnwUCLE+4kGGjhkqhwZ0WLph8AzT+hX+ZvfKwDWlxgVnCgxQfWCAdanGAn4UCLF+woHGhxgx2EAy1+sHdwoNnlOgDYJsACXMNloNkveH3BUhVm5+QRNLvkAcD01lSILS/OPdp2VACwKt5woHWXU7v1XnCg2eU5IJg3HGh2mQUG84FbgmY5Y7PsWBewtnAFaPZp6tg2YC5w+vr3inOPzh2rd0Ue9jr3tWzf1e4h6CrA9TXMcu/6c/O9v8M0x8IFZqj6PepMWqJN6AZ5aE9mHCWC0Kp1yK254b3SLXGn/pEUiw5uzon6t7vN528FrrSu8mKqOC/bZ1PJJHK0fcDcDMMMwULQ6sPwWPm9fA5aT7sLqdlJIELQqhQsN+ShVcsN4ISh1deJG0jkoFUTlBQ4WWg6K+DkodXhePogCA04oWj1tRxwgtDqcOyeCEKrw7HZLAitSg6cPDTghKJVcA+gyYs+8XsPmrzMLhVucAZnVTMD6PKm5iczKwWtx7i8xK6POiQMj/3HZaN5wj0tLrimsydz5f8Z+wyPHSU1S4NqGFwbrKdz+OP+CDAAALL8s5E3ZZMAAAAASUVORK5CYII="

/***/ },

/***/ 210:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABgCAYAAAAJr8w7AAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABFpJREFUeNrsne9xmzAYxhUu3+1OEDqBvUHIBKYTxJmg7gSlG7gTBCaoO0HtDWADsoE9gYvuXqfUEaDXErYQz3OnD/WJ2tFP7z8JxN3xeBSeaVq1pGrLqk1qn2dVW1Vt7+oPv/MMhgSwPoNQV1G1yFUggWcgXltASM3IamAZPUpaw1fNvgdyZbCMHpQyQAiynBAw+gHxfMF1gGE5Y7oUhLO6HyiILQVjrxQABGAAxIBhzKtW+gxiKDDmZBET4bkCgAAMHS3HBMLl1FaCeBUjUwAQgNGk1VhBuOamUuHZ8sZQYfgEIqpaTJngnpKQVGhsaN16P+O04Le48vc+0SBda1IdCFLuaswIaUAWnljEusW6J/S3hi7COM0SX5Y3pEvq2uCaEDBnYEzpB/3xrJiLNfstaCLeFMaU6odc8LZIfVTKyaZCYW9bck4tFiNa1ujQA03MtC2bSqjTwwgGxHY2FZHr1dWbasIHNdP5PhIQfUiC3TGtI1bBSMZe+VrSyrR/QK4JMpdMTjJG/0eKp//BgGuyp4TZf+lKBT4UhRRTj7WWN9QWJdM6AIOZmueKmCpXDn411Awc65jUoQJGuzYd9dGzwkJKZmYFGJqDpBNPE06V3VCjAIaGi9LRTNFXwjgwao45YPRbY2y41gEYzeI8ahYDRv/Bm5MVRWefbbkuETCaJbOi3wbWIS2rYMSNKWC0K+XObhPrAIxuV6WbFT0qPssBw644sztSuDpdwU1piDO7p3BT7ljG3OB7YBmWLUMl7XUqwLBb/EWwDD80AwyHBBiAAQGGW8H+Q9YFGHZrB1UaHMIy7Ck0vP4BMG5jGefVOufEtxIwuhVzBtQAJGB0VcVC/+mqgyGMHDDsWcXW0MXtAaNdK0MYEed6wGgP3DMDGCEjkyqQTdlzUW+KGoNzfQ4Y9rQ2dVGAYU8bRRa2AIz+BrhJmSKlXTK+pzhdDxjtfrzrJjZZWySGWdg7dMBol5zhuxYQS4VVRIL3aN47jHuMd3shJv4dWRTVijjp41Ohvi8qYbqoHDD48UMnhkhgj4z/N63/IxD6N+dC3UqY/T/ASDCG1uILxyoycbYzGJD5vWAsjTQVHWdJ6VhR/eyQJ6L1hrFli3tq0E4V/O/PqsCt5R8ZUlCTzefzSUIbsaXvOqMkq5P+9LPgnR7gq7KmSX/Noq8kKE8eusK9qVXcqgLfUvG08whGKvSecPohWh6gCW44k2Qc+emRZXStRxVdZcSt16ZWHqXV0jq+NBTRmdDY33DlDZYyllzzQPq+Tnyu1x0xuaRSaD7b59LrRCPRfYrNUGBcJJeW0LcE5CBGKtf2M/IxA3Fxc+kEpAAMAAGMllqkAAy3gOwAwy0gGWC4o6XvQIZ2q47XQIZ435QE8g0w3JHcb34BDHeU+gbEpYXCSxUTGM4C4ydh9gA9YLSI895w5St24KbsibPAmCBmXAdI1956JnjHo8JNWVBEKXBYq+LXwsENpbr+CjAAwOT2wljiwIQAAAAASUVORK5CYII="

/***/ },

/***/ 211:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAbAAAAGwCAYAAADITjAqAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAGV9JREFUeNrs3e2RG8e1BuAm6/7XKgKNIuAqAkIRaB0BwQhMRaB1BKYjEBiByQi0jMBkBAQjMBkBL1rba4Hk7hJfA0yf8zxVU+WSfa+9jcG88zYOBg8+ffpUIImXq+OX4H/j29Vx7qUmgwcCjCSG1fEuyd/60+p44yUnuoeWgCTmif7WZ15uNDCIY7k6fkjyt35sjfODlx0NDPp2kSi8qu/a3wwCDDo3T/g320YkPFuIRDeUPMMbXzLMgQYG2pcWBhoYHNey5Pr8a51hDjQw6FS24Y0vGeZAgEGn5pbANiJx2UIkqqHkHd74kmEONDDQvrQw0MBgXMuS+/OvdYY50MCgE9mHN75kmAMBBp2YW4Kv2EYkHFuIRDMUwxt3McyBBgbalxYGGhgc1rL4/OsuhjnQwGCiDG/czzAHAgwmam4Jvsk2ImHYQiSKoRje2JRhDjQw0L60MNDAYD/L4vOvTRnmQAODiTC8sR3DHAgwmIi5JdiabUS6ZwuR3g3F8MauDHOggYH2pYWBBgbbWRaff+3KMAcaGJyI4Y39GOZAgMGJzC3B3mwj0i1biPRqKIY3DsUwBxoYaF9aGGhgcL9l8fnXoRjmQAODIzG8cViGORBgcCRzS3BwthHpji1EejMUwxtjMcyBBgbalxYGGhh8bll8/jUWwxxoYDASwxvjMsyBAIORzC3B6Gwj0g1biPRiKIY3jsUwBxoYaF9aGAiw6Ttvd62Xq+PMcgiwjl04hw/urJ3DS+eyAJtieF2tjker47fV8d/VsWj/nMNcUA1vHI9hjsMZVsfzFly/t/P4dyF2GD4DO1x4fXfHv/+6hdnCUu3s5er4xTIc1Vs3YHuZleut2PvO26euCwJsyuG17n07WevdmO/ZbHcHa3jjNAxzbOesNdfLLXYMhNgebCEeJ7xKO6FtL25vbglOxjDH5jdZX24Tbsp2ogY2+fC6i+3Fb1sWn3+diidz3G9Wvr1NuClNTAPrKryqx+0ObFlML97G8MZpGeb42vo04R/lcJ/NamIaWFfhdZcX5Xo7wmcPhjemwDDHtaG1rfnI739NTIB1G17rsm8v1guG4Y1pyDzMMSuH2yYUYgdmC3Ga4VVl316cO+0mI9swx1jbhJuynaiBdR1ed8myvVjXvG4f+vxrGj62JhL9vBvKcbYJNTEBli681kXcXjxvFw6DG9MOspdrRxSzcvxtQiEmwFKG17revxwttPoPs0V7n/Rmly8dCzEBJrxG0sv24tBCay60wnjfwuzmy7xTP/+mtE0oxARY+vBaN9XtxZvQeuwUC+1tC7KXE9sVmJXpbhMKMQEmvG65K16U024v9nq3y/4+rrWyU+0K9LRNKMQEmPC6w7G3F7Utbmtlx7rwRr9xEmICLE14rRtze/Fs7aLhsy3uamXPR9wVmJX+twmFmAATXt9wyO3F6He7jLcrcFn2H/qIuE0oxASY8NriQrLL9uLQLhpPXIvZ8/xblO1H8d04CbHUASa8Prfp9qLgYqzz73KDIJuVPNuEQkyACa8t3bW9KLg4VZBl3iYUYgJMeO3oZntnLrg4QZA9b41r7v0qxASY8AKEWBiZfk5FeAHRpfopliwBJrwAISbAhBeAEBNgwgtAiAkw4QWQJ8SiBpjwAggeYhEDTHgBJAixaAEmvACShFikABNeAIlCLEqACS+AZCEWIcCEF0DCEOs9wIQXQNIQ6znAhBdA4hDrNcCEF0DyEOsxwIQXgBDrLsCEF4AQ+1NvP2j5ZnU8cp4BjOLj6jjTwMaxcH4BuMb22MDqncGy2EIEGMOP7RqrgY3gw+p46RwDOLhXPYVXjw2sGlbHO+cawEH9XK6H5LrR4xh9vUN47VwDOJj3vYVXrwFWPXe+ARzMZY//o3vcQlxvYj847wD2Ukfnh3I9Y6CBaWEA3Vj0GF69NzAj9QD762p0PkoDM1IPsJ/uRuejNLBqKEbqAXbV3eh8lAZWipF6gF11OTofKcAqwxwA27vs/Q/ofQtxvYkZqQfYTLej89EamBYGsJ1F7+EVqYEZqQfYXLej8xEbmJF6gM10PTofsYFVQzFSD/AtXY/OR2xgpRipB/iW7kfnowZYZZgD4G6Xkf6YSFuI603MSD3A50KMzkduYFoYwO0WkcIragMzUg/wtRCj89EbmJF6gM+FGZ2P3sCqoRipB7gRZnQ+egMrxUg9wI1Qo/MZAqwyzAEQbHR+XdQtxPUmZqQeyCrc6HyWBqaFAdktooZXhgZmpB7ILNzofKYGZqQeyCrk6HymBlYNxUg9kE/I0flMDawUI/VAPmFH57MFWGWYA8jkMsMfmWELcb2JGakHogs9Op+xgWlhQBaLDOGVrYEZqQcyCD06n7WBGakHogs/Op+1gVVDMVIPxBV+dD5rAyvFSD0QV4rR+cwBVhnmACK6zPYHZ9tCXG9iRuqBKNKMzmdvYFoYEM0iW3hlbmBG6oFI0ozOa2BG6oE4Uo3Oa2DXhmKkHuhfqtF5DexavWMxUg/0LN3ovAD7i2EOoGeXmf/4zFuI603MSD3Qm5Sj8xqYFgb0b5E5vDSwa0bqgR6lHJ3XwD5npB7oTdrReQF2exUH6IWbbgH2P3NLALhm9cVnYL7QDPQp7ReYb/yfcyD39yjYWv3i6HLtqJ+hvln79791QZl9cfNUjzpIdN7+ta90sM21a6aBaV9wm7ctnG6OY93tztq5ed7+9SMvBVqYAPvSYnU88R5gLbCu1o4pfcemBtmFQOMLrzO3MA/zJbs6jvyyBdayk//NZy3M6vGLl1ALy9rCMgeY9pW7aT1vwdX7kwyEGWlbWNYA077y+dhuWp6XuF8Aref1vB2GQXL5qXw+TJRC1u+BPXO+p2pbT1tTeVZiP72g/m2XLcieFj8X5JqmgYXj2Yc5vG4X86vk6zBr6/DYKRFeumcjZmxgz4RX+OD6uV24ryzHn2swa2uikWlhGljn6h2KzwfiqV8wngstjSyxdL8Plq2BzYVXyDftr+2NK7w2b2RP29oRR91ZutDAYr953XnG8aJcb5t8sBQ7OWtt7O+WIoz37WZOgAVTX1Sj83HepHON62DqI6sWxRM+okjzxeZMW4hG52P4V7vgCq/DedPW9B+WIoS5BhZP3WYyfdivj+2N6Yf8xjVrbcxnxX37viTYWs/SwObCq2t1/HsQXkdx1drYK0uhhQkwLyb7+VdrBQY1jqeudZ1m+9VSdCvFRyYZthDrnbvhjf58bG/ChaU4qVlrvnYw+hP++YgZGpj21Wd4zYTXJFy11+KtpdDCNLDjWxYfSPekXigvSrJnunXgrIWZUfu+bgTPNLB+nQuv7sJrJrwm6UN7bTxPsR/hn8wRPcDmzuHuwsuwxvRD7IWl6EboAIu+hbjUwIQXo1gUv2jei7DfCYvcwGwfCi/GM9fEtDABNu4bDOHFuO8xX3gWYCcTeQtxqYEJL0ZnOnH6wk4jRm1gtg+n/4a6EF4h3Ax2+J7YdIWdRowWYEO5/n0jz8ybdnjNilH5aCE2L34gc8oWq+N5CfZbYVG2EC/aG+gX5+nkPS2esBFVvTH5wzJM3uv2Huz+fdhzgA0ttOphu7AP9cG8fpcttvr6/tMydOHjWjNbCjBti/vv+maWIYWX3p9amQDTtiLd6dXX0NBGDnXi7Y33qlaWPcC0rRj+VgzWZFMngf9jGbSybAGmbcXic6+8LlfHb5ZBK8sQYNpWPO/bnbitw7zqVqIvOWtlIQNM24rt53L9lAbyspWolYULMG0rvhfF8yi5dllsJWplnQeYtpXr7qy+3rYOqUwlamXdBpi2lc+v7SSG9evAvy2DVtZDgGlbeb0vwZ63xsFcrY7HlkErm2qAaVsY3OAus+JZiVrZAVvZIQJM22L9BJ1ZBu5RL15PLINWdohWtk+AaVtoX+xyw/vOMnCIVrZtgGlbaF9oYUyilW0aYNoW2hdaGJNqZfcFmLbFpurPyZ9bBrZQb3ZMJLJXK7stwLQttuVXltnWrJhIZM9WdhNg2hb73CWdWQZ2sHS9YZ9WVgPMr6eyDz+Xwq7qefNPy8CurawG2CfrwB5+LB38ciuTVJv7fy0Du3poCdjDW+HFHurDnl9YBgQYp+CBvezrpSVgV7YQ2cf3xU+mcJgm9p1lQAPjWF4JL7QwBBguOjiXYEu2ENmV6UMOyXUIDYyjMH3Iob2yBAgwjuHKEuCcQoDhYgPOKXbgMzB2YXyeMRinRwNjVG+FFyN5YwkQYLjI0KMrS4AAQ4Dh3EKAgYsMzi2myBAHW58zloARuR6hgTGK95aAkb22BAgwxrC0BIzMhCsCDAFGl3wOhgBDgKGBIcDAxQUNDAGGiwuAAAOiWVoCBBggwAjNF5nZ6nyxBByBaxIaGAACDAAEGAAIMAAEGAAIMAAQYAAgwNjdzBIAAgzga2eWAAEG9OjcEiDAGMNgCQABhgAD5xgCjCPx+QQCDAFGl3w+gQBDgOHiAs4x9uH3wNj6nLEEjMj1CA2M0cwsAdoXAgwXGXBuIcA4EoMcaPcIMFxkwLnFrgxxsNN5YwkYwYfV8Z1lQAPDnTI9ORdeCDCO4cIS4KYIAYaLDTin2IHPwNjV9+X6Mws4BNchNDCOxjYiziUEGC46OJdgW7YQ2YdtRA7B+DwaGO6c6fIcEl4IMAQYziHysIXIvn5cHUvLwA7O2rmjgbFzA3trGdjD3BKwR/sSXuzqfQ2w+giXn1bHi9Xx0ZogwDiSZ5aAHdSs+nl1DA++2EE8a3dF9cR6ZJ3Y0NPVsbAMbGG2Ov6wDGzatlbH83ad+d/k85dDHB/af0ArQwvDOcNk2lYLsM++tvNggxkOrYxN1JPsyjKwgXoxemcZ2KZt3ebBlkOI5y3IfPjKl14XD2RlM/XC9MQycEvbWmxzI/xgxyl6rQwtDO2Lo7WtQwaYVoYWhvbF0drWWAGmlaGFoX0xatsaO8C0MrQw7lNvbB5bBm3rUB6M/CQprSwn3wvjS/Wmxve+tK2uAkwry6l+d3AofmqFvyxXxw+WQds6pGM+jf5Nuf7y4tDu0D2DMa56g3JpGWguhVf4tvVruf59wHk54mfgD078MHqtLLaf2o0LeQ3tHPD+1rbCBdgNn5XF9LbdpJBXvbgZ3IjVtkb/bGtTU/lBS89gjKnejFxahrSeCa9QbevOZxJmb2BaWWy+G5bPeXvNbR1qWykD7Ms3g8/K+n4jnBdTiVmctfBy49lv21r0cNPZS4BpZf171V474qsXP4+L0rYEmFYWyq/tTUJc89Xxu2XQtgSYVhaRz8Pi8rmXtiXAtLLQ6nTprPh+WDT1RnLpfadtCbDDBNl/nK+T9baFmKGOOOFVL4p2QKbrb6vjZbQ/KmqAlXZxdDcoxBhfvTD+Yhkm7fuI77WHwd9UTNcjr1EIC+E1ea+i3igKME7pcfGzK72Hl3F518KTibyFWIptxF7UD5fnlkF4MYqQ24fRG5gW1o8nmpjwYhRhtw8zBJiLYl8hdlWuJ9oQXriJ/6boW4jVsvgxvZ6YTpyms2Jgozfhfxn9YYIX0TZiXx61JuZ3xKYVXlfCq8trX+gbwQwNrN6BvHMud3n3eFE8durUPB6qX+Ef25ahgS3L9bYUfakXzD/K9aPBOI258OrW+ww3fw+TvJiegN6vf5brrRDDHcdz83nX78KrW4sMf2SGLcSbN+TSm7H7O8q6peghwOM6bxc/zzXs24/tmqeBBfChGOboXZ0krQ9ovrQUo6nbtVfCq3uvMoRXpgZ2c2fpCfUx1M8059rYwQytdT22FCGEfPJ89gAr7YLn7jKOf5Trzzd9Z2x3l6152V6P4X27IUnhYbIX1zBHLL+1m5ILS7G1WbneZvpNeIWyyPTHZmtgpXjAb1SvW5u4shT3GortwsjCPrhXA9PCIqsX5D/axXmwHHcG1zvhFdaLkmw7PWMDqyP1/3Wup3gz10a2FFx/roMH8Mb3Y7bzPWMD+9AubsT2pLWNq3L9eU82s7XGJbzie53xZi1jA7u5K/V8xFzqdNbzdlGPus1SdxfqQEudKjRtm0v45x4KsM8t3JmmVRv4yxLnuzIXa4cBpZzta5bxD88cYPUF/8O5n9rHtSC76qyZCS1St6/sAVbai24ii/U72Zswm9pTPs7bTVc9/C4XN1J9cVmAaWFs3s7erIVZPZZH+u8e2nETWOdaFnd4WpJ9eVmAaWHs19I+tED7sNbUllsE3LB211zD6WztnzkX0b4EmBYGhJbmob0CTAsDYu0EzLIvwkPnwZ88XgroycISCLAbM0sAuGb1xRbi9Qfoy2LKC+hLumcfamBf80VQoEdzDUwDq3cwP3gvAJ2p31U808DymgkvoFPfZW9h2QPsmfcA4BrWp8xbiEPxkypA/9I+zDdzA9O+gAjmGlguRueBSFKO1GdtYEbnAS1MA+tSvVMxfQhEkXKkPmMDmwkvIJiUI/UZA8zwBuDaFkC2LcShGJ0H4ko1Up+tgWlfQGRzDSwmo/NABmlG6jM1MKPzgBamgXWp3pGYPgSiSzNSn6WBzYQXkESakfosAWZ4A8gkxTUvwxbiUIzOA/mEH6nP0MC0LyCjuQbWN6PzQGahR+qjNzCj84AWpoF1qd55mD4Esgo9Uh+5gc2EF5Bc6JH6yAFmeAMg8LUw6hbiUIzOA9wIOVIftYFpXwB/mWtgfTA6D/C1cCP1ERuY0XmABC0sYgOrdximDwE+F26kPloDmwkvgFuFG6mPFmCGNwCSXCMjbSEOxeg8wLeEGamP1MC0L4Bvm2tg02J0HmBzIUbqozQwo/MAyVpYlAZW7yRMHwJsJsRIfYQGNhNeAFsJMVIfIcAMbwAkvHb2voU4FKPzALvqeqS+9wamfQHsbq6BnYbReYD9dTtS33MDMzoPkLiF9dzA6h2D6UOA/XQ7Ut9rA5sJL4CD6HakvtcAM7wBkPya2uMW4lCMzgMcWncj9T02MO0L4PDmGti4jM4DjKerkfreGpjReQAtrMsGVu8MTB8CjKOrkfreGtil8wtgNF3NGPQWYIvV8dQ5BnBwT9s1VoAJMQDhJcCEGIDwChJgQgwgcXj1HmBCDCBpeEUIMCEGkDC8ogSYEANIFl6RAkyIASQKr2gBJsQAkoRXxAATYgAJwitqgAkxgODhFTnAhBhA4PCKHmBCDBBegT1M8CIKMUB4CTAhBiC8BJgQAxBeAkyIjeJVW6/XloIje93OvReWQnjd5cGnT58yvtDz1fG78/1WH9ub4PnqWK7981m5/kXsx5aIEb0v178K/HLtnw3tPVv/+XeWSHhlDzAhdvuF47JdOD7c858TZIx5/i02eN/WIHtkyXKHV/YAE2LXXrW2dbXl/50g4xBet/Nol/Ovvn+fCK+c4SXA8obYXduEuzhvd8TZLyQcJ7i+NJS824upw0uA5QuxTbcJd72QPGtr6XMK7rpxetnOweVI7+Ms24vpw0uA5QmxXbcJd3G2Oi7aReoHpxXtxumm8X84wn/frMTeXhReAix8iB1ym9CFhF1vnBbl84nCYxpKvO1F4SXAQofYmNuE+7SymwuJVpajbS1OeON01/u69+1F4SXAwobYMbcJ93Ez9HFRfFYWxc1nW4sOzr9edwWElwALF2JT2Cbcx0U7bDH26VULrim1/U0NpZ/tReElwEKF2BS3CfdxthZmM81MaJ3gfT7V7UXhJcDChFgv24SHaGYLQTa5C2mk0LrNrExre1F4CbDuQ6z3bcJd1b/37069SXjR3gNZDOX024vCS4B1HWLRtgl3uYi8c9pNws8JWv997/tjby8KLwHWbYhl2SbcRF0Dz1o8rfftZiK7WTnO9qLw2sJDS7CVRRnn98TqNuG/VseP5frzH+H113pzWs8twf9upubtPfqP9p4VXhpY6iaWfZtwE3VdDHOczvfOzXuvA4faXhReGlg3TaxuE9bPFYb2/8sFQgubohfOzW+em+ftvbzPL0cLLw1s8k0s6zThvmrIG+Y4jczDG7ueqzetbNNdA+ElwCYdYrYJ91cvooY5jsvwxv7XhW9tLwqvPdlCPMw2wm3bibYJD7vGHJfhjf3P2fu2F4WXBja5O67nxTbhWAxzHJfhjcMa1lrZM+ElwMjXCDyZ4ziyPXkDAQaj38Ea5jgOwxt0wWdg9GK5Ol5bhtG9F14IMDi8hSUYneENumELkd4Y5hiX4Q00MNDCuuPJG2hgMKKhGOYYi+ENNDAY0bIY5hiD4Q0EGBzBwhIcnOENumMLkV4Z5jgswxtoYKCFdcfwBhoYHNFQDHMciuENNDA4omUxzHEIhjcQYHACC0uwN8MbdMsWIr0zzLEfwxtoYKCFdcfwBhoYnNBQDHPsyvAGGhic0LIY5tiF4Q0EGEzAwhJszfAG3bOFSBSGObZjeAMNDLSw7hjeQAODCRmKYY5NGd5AA4MJWRbDHJswvIEAgwlaWIJvMrxBGLYQicYwx/0Mb6CBgRbWHcMbaGAwYUMxzHEXwxtoYDBhy2KY4zaGNxBg0IGFJfiK4Q3CsYVIVIY5Pmd4Aw0MtLDuGN5AA4OODMUwxw3DG2hg0JFlMcxRGd5AgEGHFpbA8AZx2UIkuuzDHIY30MBAC+uO4Q00MOjYUPIOcxjeQAODji1LzmEOwxsIMAhgkfBvNrxBeLYQySLbMIfhDTQw0MK6Y3gDDQwCGUqeYQ7DG2hgEMiy5BjmMLxBGv8vwADAjdvLSGNr/wAAAABJRU5ErkJggg=="

/***/ },

/***/ 212:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAUCAYAAAB1aeb6AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTBBNTRCODUxRTc4MTFFN0FDM0U5ODFDNUQ5RkE2QzUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTBBNTRCODYxRTc4MTFFN0FDM0U5ODFDNUQ5RkE2QzUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBMEE1NEI4MzFFNzgxMUU3QUMzRTk4MUM1RDlGQTZDNSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBMEE1NEI4NDFFNzgxMUU3QUMzRTk4MUM1RDlGQTZDNSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuhEJNQAAABPSURBVHjaYvz///9vBgYGFiD+z0A/wAjEf0CWMiMJ0BMwMzEMIGDBEhy0BvDoHVCfj1o+avmo5aOWj1o+/CxnwVXjjAif/x2gZtRfgAADABrpCywGj/ffAAAAAElFTkSuQmCC"

/***/ },

/***/ 213:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkUzNDBCRDM5MURENjExRTdCNzlFQkM3ODY0OURBQjc0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkUzNDBCRDNBMURENjExRTdCNzlFQkM3ODY0OURBQjc0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTM0MEJEMzcxREQ2MTFFN0I3OUVCQzc4NjQ5REFCNzQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTM0MEJEMzgxREQ2MTFFN0I3OUVCQzc4NjQ5REFCNzQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5/lluwAAABCUlEQVR42sxUOw7CMAxNotJWqGVgAyGxsjFzAcQVuAF34xpFYmeHgc+GhBrxEQKCDQ6KIM3QdOBJr41c59WxY3Ol1JYxljI7hsD5l60LXBT4ywAebVaMhsUWOwJIUVCSww24BiqgIIe9ZdMJuAJy8lUUVPzSgiPn6o0lkJVkRhq5MP78YOVx1wsUjIzclIXeG6FgqBPqIZjQO8SiDCjKo4fgGNgEXjgmtUoIVjH+XzCg3qxhQoGbkjotqvQVb7mkW77y6JQZaUhh3KHE46R1rSFoOCByD0GtISvv5cAwdoAZjSNOnFiGKRZgSms9vvqfr8qNkaUAPdcGjHDnGAwHi+1s5Ownl08BBgCupvl8g6pF8QAAAABJRU5ErkJggg=="

/***/ },

/***/ 214:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjExNjk2MTY0MURENzExRTdBNzhERTI4QTg1QjhBQUJCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjExNjk2MTY1MURENzExRTdBNzhERTI4QTg1QjhBQUJCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTE2OTYxNjIxREQ3MTFFN0E3OERFMjhBODVCOEFBQkIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTE2OTYxNjMxREQ3MTFFN0E3OERFMjhBODVCOEFBQkIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Wlf14AAAA1UlEQVR42uxTMQ7CMAxMAqIdgAeAeAEj/994Axt0QLCTMFAEDY7qSJVx3EZ05KSTo559sWJXe+8VwQ24xLNWPGLRAbjtCoZJdhifKo2o3anAGT4w1oJhTXJFQ6OGY0I/TIEVnht8s02G4Q54xLpArz0zlQ76hqK4Dl1Cc8JFV+CC7UBuMB9GjYy/4e8Ia2OFtVkntIu0NqMvdhV/G+QKWPYstsMOX8Az1rXPFxok3PsWltEiLeacqMYN5Z0xg2bIlEuMhWBUkFzRcI5xJhhG7WvSHwEGAGZAcA92W5KIAAAAAElFTkSuQmCC"

/***/ },

/***/ 215:
/***/ function(module, exports) {

	module.exports = "<div style=\"width:100%;position: relative;background: #000\" class=\"minimal-controlbar-wrapper\">\n\t<!-- Video Tag -->\n\t<!-- style=\"height: calc(100% - 58px);width: 100%;\" -->\n\t<div id=\"jw_video_wrapper\" ng-class=\"{'full-screen':vm.isFullScreen}\" ng-click=\"vm.playToggle()\"  >\n\t\t<div id=\"{{vm.videoId}}\" style=\"height: 100%;width: 100%;outline: none;border:0px;border-radius: 0px;\"></div>\n\t</div>\n\t\n\t<!-- ./Video Tag -->\n\t<div class=\"ds-play-button\" ng-if=\"!vm.isPlaying\" ng-click=\"vm.playToggle()\">\n\t\t<i class=\"fa fa-play-circle\"></i>\n\t</div>\n\t<div class=\"ds-play-button\"  ng-if=\"vm.isLoading && vm.shouldShowLoader\">\n\t\t<i class=\"fa fa-circle-o-notch fa-spin\"></i>\n\t</div>\n\t\n\t<!-- Control Bar -->\n\t<div class=\"ds-controlbar\" ng-class=\"{'full-screen':vm.isFullScreen}\">\n\t\t<!-- Progress Bar Wrapper -->\t\n\t\t\t<!-- Left Side Controls Wrapper -->\n\t\t\t<span class=\"ds-controls-left\">\n\t\t\t\t<!-- Play/Push Button -->\n\t\t\t\t<button class=\"btn btn-primary ds-control play-pause-button icon-btn\" title=\"Play/Pause (P/Space)\" ng-click=\"vm.playToggle()\"><i class=\"\" ng-class=\"{'play-btn':!vm.isPlaying,'pause-btn':vm.isPlaying}\"></i></button>\n\t\t\t\t<!-- Current/Total Duration -->\n\t\t\t\t<span class=\"ds-control time-span\">{{vm.video_current}}</span>\n\t\t\t\t<div class=\"ds-progress-bar-wrapper {{vm.videoId}}\">\n\t\t\t\t\t<!-- Progress Bar -->\n\t\t\t\t\t<div class=\"ds-progress-bar {{vm.videoId}}\">\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t\t<!-- Buffered Bar -->\n\t\t\t\t\t<div class=\"ds-buffered-bar {{vm.videoId}}\">\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div> <!-- ./Progress Bar Wrapper -->\n\t\t\t\t<span class=\"ds-control time-span\">{{vm.video_duration}}</span>\n\t\t\t</span>\n\t\t\t\n\t\t\t<!-- Right Side Controls Wrapper -->\n\t\t\t<span class=\"ds-controls-right\">\n\t\t\t\t<!-- Volume Button -->\n\t\t\t\t\t<div class=\"btn btn-primary ds-control icon-btn\" title=\"Change Volume (Arrow Up/Down)\" style=\"width: auto;\">\n\t\t\t\t\t\t<!-- <i class=\"volume-btn\"></i> -->\n\t\t\t\t\t\t<!--  ng-click=\"vm.toggleVolume()\" -->\n\t\t\t\t\t\t<!-- Volume Slider Wrapper -->\n\t\t\t\t\t\t<!-- <span class=\"overlay volume\">\n\t\t\t\t\t\t\t<span class=\"volume-slider {{vm.videoId}}\"></span>\n\t\t\t\t\t\t</span> -->\n\t\t\t\t\t\t<!-- ./Volume Slider Wrapper -->\n\t\t\t\t\t\t<div ng-click=\"vm.toggleMute()\">\n\t\t\t\t\t\t\t<i class=\"volume-btn\"></i>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class='ds-sound-progress-wrapper {{vm.videoId}}' style=\"top: -2px;margin-right: 10px;\">\n\t\t\t\t\t\t\t<div class='ds-sound-real-progress'></div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t<!-- Toggle Full Screen Button -->\n\t\t\t\t<button ng-if=\"!vm.isOtherThingInFullscreen\" class=\"btn btn-primary ds-control icon-btn\" ng-click=\"vm.fullscreen()\" title=\"Toggle Fullscreen (F)\"><i class=\"fullscreen-btn\"></i></button>\n\t\t\t\t</span> <!-- ./Right Side Controls Wrapper -->\n\t\t\t\t</div> <!-- ./ControlBar -->\n\t\t\t</div>";

/***/ },

/***/ 229:
/***/ function(module, exports, __webpack_require__) {

	var require;/* WEBPACK VAR INJECTION */(function(module) {(function commonJS(require, module) {
	    'use strict';

	    __webpack_require__(230);

	    module.exports = '720kb.socialshare';
	}(require, module));

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(117)(module)))

/***/ },

/***/ 230:
/***/ function(module, exports) {

	/*
	 * angular-socialshare
	 * 2.2.4
	 * 
	 * A social media url and content share module for angularjs.
	 * http://720kb.githb.io/angular-socialshare
	 * 
	 * MIT license
	 * Thu Aug 04 2016
	 */
	/*global angular*/
	/*eslint no-loop-func:0, func-names:0*/

	(function withAngular(angular) {
	  'use strict';

	  var directiveName = 'socialshare'
	    , serviceName = 'Socialshare'
	    , socialshareProviderNames = ['facebook', 'facebook-messenger', 'twitter', 'linkedin', 'google', 'pinterest', 'tumblr', 'reddit', 'stumbleupon', 'buffer', 'digg', 'delicious', 'vk', 'pocket', 'wordpress', 'flipboard', 'xing', 'hackernews', 'evernote', 'whatsapp', 'telegram', 'viber', 'skype', 'email', 'ok']
	    , socialshareConfigurationProvider = /*@ngInject*/ function socialshareConfigurationProvider() {

	      var socialshareConfigurationDefault = [{
	        'provider': 'email',
	        'conf': {
	          'subject': '',
	          'body': '',
	          'to': '',
	          'cc': '',
	          'bcc': '',
	          'trigger': 'click'
	        }
	      },
	      {
	        'provider': 'facebook',
	        'conf': {
	          'url':'',
	          'text': '',
	          'media': '',
	          'type': '',
	          'via': '',
	          'to': '',
	          'from': '',
	          'ref': '',
	          'display': '',
	          'source': '',
	          'caption': '',
	          'redirectUri': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'facebook-messenger',
	        'conf': {
	          'url': ''
	        }
	      },
	      {
	        'provider': 'twitter',
	        'conf': {
	          'url': '',
	          'text': '',
	          'via': '',
	          'hashtags': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'linkedin',
	        'conf': {
	          'url': '',
	          'text': '',
	          'description': '',
	          'source': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'reddit',
	        'conf': {
	          'url': '',
	          'text': '',
	          'subreddit': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'vk',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'ok',
	        'conf': {
	          'url': '',
	          'text': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'digg',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'delicious',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'stumbleupon',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'pinterest',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'google',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'tumblr',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'buffer',
	        'conf': {
	          'url': '',
	          'text': '',
	          'via': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'pocket',
	        'conf': {
	          'url': '',
	          'text': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'flipboard',
	        'conf': {
	          'url': '',
	          'text': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'hackernews',
	        'conf': {
	          'url': '',
	          'text': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'wordpress',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'xing',
	        'conf': {
	          'url': '',
	          'text': '',
	          'media': '',
	          'follow' : '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'evernote',
	        'conf': {
	          'url': '',
	          'text': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'whatsapp',
	        'conf': {
	          'url': '',
	          'text': ''
	        }
	      },
	      {
	        'provider': 'telegram',
	        'conf': {
	          'url': '',
	          'text': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      },
	      {
	        'provider': 'viber',
	        'conf': {
	          'url': '',
	          'text': ''
	        }
	      },
	      {
	        'provider': 'skype',
	        'conf': {
	          'url': '',
	          'text': '',
	          'trigger': 'click',
	          'popupHeight': 600,
	          'popupWidth': 500
	        }
	      }];

	      return {
	        'configure': function configure(configuration) {

	          var configIndex = 0
	            , configurationKeys
	            , configurationIndex
	            , aConfigurationKey
	            , configElement
	            , internIndex = 0
	          //this is necessary becuase provider run before any service
	          //so i have to take the log from another injector
	          , $log = angular.injector(['ng']).get('$log');

	          if (configuration && configuration.length > 0) {
	            for (; configIndex < configuration.length; configIndex += 1) {
	              if (configuration[configIndex].provider && socialshareProviderNames.indexOf(configuration[configIndex].provider) > -1) {

	                for (; internIndex < socialshareConfigurationDefault.length; internIndex += 1) {
	                  configElement = socialshareConfigurationDefault[internIndex];

	                  if (configElement &&
	                    configElement.provider &&
	                    configuration[configIndex].provider === configElement.provider) {

	                      configurationKeys = Object.keys(configElement.conf);
	                      configurationIndex = 0;

	                      for (; configurationIndex < configurationKeys.length; configurationIndex += 1) {

	                        aConfigurationKey = configurationKeys[configurationIndex];
	                        if (aConfigurationKey && configuration[configIndex].conf[aConfigurationKey]) {

	                          configElement.conf[aConfigurationKey] = configuration[configIndex].conf[aConfigurationKey];
	                        }
	                      }

	                      // once the provider has been found and configuration applied
	                      // we should reset the internIndex for the next provider match to work correctly
	                      // and break, to skip loops on unwanted next providers
	                      internIndex = 0;
	                      break;
	                    }
	                  }
	                } else {
	                  $log.warn('Invalid provider at element ' + configIndex + ' with name:' + configuration[configIndex].provider);
	                }
	              }
	            }
	        }
	        , '$get': /*@ngInject*/ function instantiateProvider() {

	            return socialshareConfigurationDefault;
	        }
	      };
	    }
	    , manageFacebookShare = function manageFacebookShare($window, attrs) {

	      var urlString;

	      if (attrs.socialshareType && attrs.socialshareType === 'feed') {
	        // if user specifies that they want to use the Facebook feed dialog (https://developers.facebook.com/docs/sharing/reference/feed-dialog/v2.4)
	        urlString = 'https://www.facebook.com/dialog/feed?';

	        if (attrs.socialshareVia) {
	          urlString += '&app_id=' + encodeURIComponent(attrs.socialshareVia);
	        }

	        if (attrs.socialshareRedirectUri) {
	          urlString += '&redirect_uri=' + encodeURIComponent(attrs.socialshareRedirectUri);
	        }
	        if (attrs.socialshareUrl) {
	          urlString += '&link=' + encodeURIComponent(attrs.socialshareUrl);
	        }

	        if (attrs.socialshareTo) {
	          urlString += '&to=' + encodeURIComponent(attrs.socialshareTo);
	        }

	        if (attrs.socialshareDisplay) {
	          urlString += '&display=' + encodeURIComponent(attrs.socialshareDisplay);
	        }

	        if (attrs.socialshareRef) {
	          urlString += '&ref=' + encodeURIComponent(attrs.socialshareRef);
	        }

	        if (attrs.socialshareFrom) {
	          urlString += '&from=' + encodeURIComponent(attrs.socialshareFrom);
	        }

	        if (attrs.socialshareDescription) {
	          urlString += '&description=' + encodeURIComponent(attrs.socialshareDescription);
	        }

	        if (attrs.socialshareText) {
	          urlString += '&name=' + encodeURIComponent(attrs.socialshareText);
	        }

	        if (attrs.socialshareCaption) {
	          urlString += '&caption=' + encodeURIComponent(attrs.socialshareCaption);
	        }

	        if (attrs.socialshareMedia) {
	          urlString += '&picture=' + encodeURIComponent(attrs.socialshareMedia);
	        }

	        if (attrs.socialshareSource) {
	          urlString += '&source=' + encodeURIComponent(attrs.socialshareSource);
	        }

	        $window.open(
	          urlString,
	          'Facebook', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	          + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);

	      } else if (attrs.socialshareType && attrs.socialshareType === 'send') {
	        // if user specifies that they want to use the Facebook send dialog (https://developers.facebook.com/docs/sharing/reference/send-dialog)
	        urlString = 'https://www.facebook.com/dialog/send?';

	        if (attrs.socialshareVia) {
	          urlString += '&app_id=' + encodeURIComponent(attrs.socialshareVia);
	        }

	        if (attrs.socialshareRedirectUri) {
	          urlString += '&redirect_uri=' + encodeURIComponent(attrs.socialshareRedirectUri);
	        }

	        if (attrs.socialshareUrl) {
	          urlString += '&link=' + encodeURIComponent(attrs.socialshareUrl);
	        }

	        if (attrs.socialshareTo) {
	          urlString += '&to=' + encodeURIComponent(attrs.socialshareTo);
	        }

	        if (attrs.socialshareDisplay) {
	          urlString += '&display=' + encodeURIComponent(attrs.socialshareDisplay);
	        }

	        if (attrs.socialshareRef) {
	          urlString += '&ref=' + encodeURIComponent(attrs.socialshareRef);
	        }

	        $window.open(
	          urlString,
	          'Facebook', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	          + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);

	      } else {
	        //otherwise default to using sharer.php
	        $window.open(
	          'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href)
	          , 'Facebook', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	          + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	      }
	    }
	    , manageEmailShare = function manageEmailShare($window, attrs) {
	      var urlString = 'mailto:';

	      if (attrs.socialshareTo) {

	        urlString += encodeURIComponent(attrs.socialshareTo);
	      }

	      urlString += '?';

	      if (attrs.socialshareBody) {

	        urlString += 'body=' + attrs.socialshareBody;
	      }

	      if (attrs.socialshareSubject) {

	        urlString += '&subject=' + encodeURIComponent(attrs.socialshareSubject);
	      }
	      if (attrs.socialshareCc) {

	        urlString += '&cc=' + encodeURIComponent(attrs.socialshareCc);
	      }
	      if (attrs.socialshareBcc) {

	        urlString += '&bcc=' + encodeURIComponent(attrs.socialshareBcc);
	      }

	      $window.open(urlString, '_self');
	    }
	    , facebookMessengerShare = function facebookMessengerShare($window, attrs, element) {

	      var href = 'fb-messenger://share?link=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      element.attr('href', href);
	      element.attr('target', '_top');
	  }
	    , manageTwitterShare = function manageTwitterShare($window, attrs) {
	      var urlString = 'https://www.twitter.com/intent/tweet?';

	      if (attrs.socialshareText) {
	        urlString += 'text=' + encodeURIComponent(attrs.socialshareText);
	      }

	      if (attrs.socialshareVia) {
	        urlString += '&via=' + encodeURIComponent(attrs.socialshareVia);
	      }

	      if (attrs.socialshareHashtags) {
	        urlString += '&hashtags=' + encodeURIComponent(attrs.socialshareHashtags);
	      }

	      //default to the current page if a URL isn't specified
	      urlString += '&url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      $window.open(
	        urlString,
	        'Twitter', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageGooglePlusShare = function manageGooglePlusShare($window, attrs) {

	      $window.open(
	        'https://plus.google.com/share?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href)
	        , 'Google+', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	      }
	    , manageRedditShare = function manageRedditShare($window, attrs) {
	      var urlString = 'https://www.reddit.com/';

	      if (attrs.socialshareSubreddit) {
	        urlString += 'r/' + attrs.socialshareSubreddit + '/submit?url=';
	      } else {
	        urlString += 'submit?url=';
	      }
	      /*-
	      * Reddit isn't responsive and at default width for our popups (500 x 500), everything is messed up.
	      * So, overriding the width if it is less than 900 (played around to settle on this) and height if
	      * it is less than 650px.
	      */
	      if (attrs.socialsharePopupWidth < 900) {
	        attrs.socialsharePopupWidth = 900;
	      }

	      if (attrs.socialsharePopupHeight < 650) {
	        attrs.socialsharePopupHeight = 650;
	      }

	      $window.open(
	        urlString + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + '&title=' + encodeURIComponent(attrs.socialshareText)
	        , 'Reddit', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	      }
	    , manageStumbleuponShare = function manageStumbleuponShare($window, attrs) {

	      $window.open(
	        'https://www.stumbleupon.com/submit?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + '&title=' + encodeURIComponent(attrs.socialshareText)
	        , 'StumbleUpon', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageLinkedinShare = function manageLinkedinShare($window, attrs) {
	      /*
	      * Refer: https://developer.linkedin.com/docs/share-on-linkedin
	      * Tab: Customized URL
	      */
	      var urlString = 'https://www.linkedin.com/shareArticle?mini=true';

	      urlString += '&url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      if (attrs.socialshareText) {
	        urlString += '&title=' + encodeURIComponent(attrs.socialshareText);
	      }

	      if (attrs.socialshareDescription) {
	        urlString += '&summary=' + encodeURIComponent(attrs.socialshareDescription);
	      }

	      if (attrs.socialshareSource) {
	        urlString += '&source=' + encodeURIComponent(attrs.socialshareSource);
	      }

	      $window.open(
	        urlString,
	        'Linkedin', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , managePinterestShare = function managePinterestShare($window, attrs) {

	      $window.open(
	        'https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + '&media=' + encodeURIComponent(attrs.socialshareMedia) + '&description=' + encodeURIComponent(attrs.socialshareText)
	        , 'Pinterest', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageDiggShare = function manageDiggShare($window, attrs) {

	      $window.open(
	        'https://www.digg.com/submit?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + '&title=' + encodeURIComponent(attrs.socialshareText)
	        , 'Digg', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageTumblrShare = function manageTumblrShare($window, attrs) {

	      if (attrs.socialshareMedia) {
	        var urlString = 'https://www.tumblr.com/share/photo?source=' + encodeURIComponent(attrs.socialshareMedia);

	        if (attrs.socialshareText) {
	          urlString += '&caption=' + encodeURIComponent(attrs.socialshareText);
	        }

	        $window.open(
	          urlString,
	          'Tumblr', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	          + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	      } else {

	        $window.open(
	          'https://www.tumblr.com/share/link?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + '&description=' + encodeURIComponent(attrs.socialshareText)
	          , 'Tumblr', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	          + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	      }
	    }
	    , manageVkShare = function manageVkShare($window, attrs) {
	      var urlString = 'https://www.vk.com/share.php?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      if (attrs.socialshareText) {
	        urlString += '&title=' + encodeURIComponent(attrs.socialshareText);
	      }

	      if (attrs.socialshareMedia) {
	        urlString += '&image=' + encodeURIComponent(attrs.socialshareMedia);
	      }

	      if (attrs.socialshareDescription) {
	        urlString += '&description=' + encodeURIComponent(attrs.socialshareDescription);
	      }

	      $window.open(
	       urlString
	       , 'Vk', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	       + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageOkShare = function manageOkShare($window, attrs) {
	      $window.open(
	        'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + '&st.comments=' + encodeURIComponent(attrs.socialshareText)
	        , 'Ok', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageDeliciousShare = function manageDeliciousShare($window, attrs) {

	     $window.open(
	       'https://www.delicious.com/save?v=5&noui&jump=close&url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + '&title=' + encodeURIComponent(attrs.socialshareText)
	       , 'Delicious', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	       + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageBufferShare = function manageBufferShare($window, attrs) {
	      var urlString = 'https://bufferapp.com/add?';

	      if (attrs.socialshareText) {
	        urlString += 'text=' + encodeURIComponent(attrs.socialshareText);
	      }

	      if (attrs.socialshareVia) {
	        urlString += '&via=' + encodeURIComponent(attrs.socialshareVia);
	      }

	      //default to the current page if a URL isn't specified
	      urlString += '&url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      $window.open(
	        urlString,
	        'Buffer', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageHackernewsShare = function manageHackernewsShare($window, attrs) {
	      var urlString = 'https://news.ycombinator.com/submitlink?';

	      if (attrs.socialshareText) {
	        urlString += 't=' + encodeURIComponent(attrs.socialshareText) + '&';
	      }
	      //default to the current page if a URL isn't specified
	      urlString += 'u=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      $window.open(
	       urlString,
	       'Hackernews', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	      + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageFlipboardShare = function manageFlipboardShare($window, attrs) {
	      var urlString = 'https://share.flipboard.com/bookmarklet/popout?v=2&';

	      if (attrs.socialshareText) {
	        urlString += 'title=' + encodeURIComponent(attrs.socialshareText) + '&';
	      }

	      //default to the current page if a URL isn't specified
	      urlString += 'url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      $window.open(
	        urlString,
	        'Flipboard', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , managePocketShare = function managePocketShare($window, attrs) {
	      var urlString = 'https://getpocket.com/save?';

	      if (attrs.socialshareText) {
	        urlString += 'text=' + encodeURIComponent(attrs.socialshareText) + '&';
	      }

	      //default to the current page if a URL isn't specified
	      urlString += 'url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      $window.open(
	        urlString,
	        'Pocket', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageWordpressShare = function manageWordpressShare($window, attrs) {
	      var urlString = 'http://wordpress.com/press-this.php?';

	      if (attrs.socialshareText) {
	        urlString += 't=' + encodeURIComponent(attrs.socialshareText) + '&';
	      }
	      if (attrs.socialshareMedia) {
	        urlString += 'i=' + encodeURIComponent(attrs.socialshareMedia) + '&';
	      }

	      //default to the current page if a URL isn't specified
	      urlString += 'u=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      $window.open(
	        urlString,
	        'Wordpress', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageXingShare = function manageXingShare($window, attrs) {
	      var followUrl = '';

	      if (attrs.socialshareFollow) {
	        followUrl = '&follow_url=' + encodeURIComponent(attrs.socialshareFollow);
	      }
	      $window.open(
	        'https://www.xing.com/spi/shares/new?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href) + followUrl
	        , 'Xing', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageEvernoteShare = function manageEvernoteShare($window, attrs) {

	      var urlString = 'http://www.evernote.com/clip.action?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      if (attrs.socialshareText) {
	        urlString += '&title=' + encodeURIComponent(attrs.socialshareText);
	      }

	      $window.open(
	        urlString
	        , 'Evernote', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , manageWhatsappShare = function manageWhatsappShare($window, attrs, element) {

	      var href = 'whatsapp://send?text=' + encodeURIComponent(attrs.socialshareText + ' ') + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      element.attr('href', href);
	      element.attr('target', '_top');

	    }
	    , manageViberShare = function manageViberShare($window, attrs, element) {

	      var href = 'viber://forward?text=' + encodeURIComponent(attrs.socialshareText + ' ') + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      element.attr('href', href);
	      element.attr('target', '_top');
	    }
	    , manageTelegramShare = function manageTelegramShare($window, attrs) {

	      var urlString = 'https://telegram.me/share/url?url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      if (attrs.socialshareText) {
	        urlString += '&text=' + encodeURIComponent(attrs.socialshareText);
	      }

	      $window.open(
	        urlString
	        , 'Telegram', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , skypeShare = function skypeShare($window, attrs) {
	      var urlString = 'https://web.skype.com/share?source=button&url=' + encodeURIComponent(attrs.socialshareUrl || $window.location.href);

	      if (attrs.socialshareText) {
	        urlString += '&text=' + encodeURIComponent(attrs.socialshareText);
	      }

	      $window.open(
	        urlString
	        , 'Skype', 'toolbar=0,status=0,resizable=yes,width=' + attrs.socialsharePopupWidth + ',height=' + attrs.socialsharePopupHeight
	        + ',top=' + ($window.innerHeight - attrs.socialsharePopupHeight) / 2 + ',left=' + ($window.innerWidth - attrs.socialsharePopupWidth) / 2);
	    }
	    , socialshareService = /*@ngInject*/  ['$window', function socialshareService($window) {

	      this.emailShare = manageEmailShare;
	      this.facebookShare = manageFacebookShare;
	      this.twitterShare = manageTwitterShare;
	      //**** Fb Messenger can't open without an element clicked (href)
	      //this.facebookMessengerShare = facebookMessengerShare;
	      this.stumbleuponShare = manageStumbleuponShare;
	      this.pinterestShare = managePinterestShare;
	      this.googleShare = manageGooglePlusShare;
	      this.bufferShare = manageBufferShare;
	      this.hackernewsShare = manageHackernewsShare;
	      this.okShare = manageOkShare;
	      this.deliciousShare = manageDeliciousShare;
	      this.pocketShare = managePocketShare;
	      this.vkShare = manageVkShare;
	      this.flipboardShare = manageFlipboardShare;
	      this.xingShare = manageXingShare;
	      this.diggShare = manageDiggShare;
	      this.linkedinShare = manageLinkedinShare;
	      this.wordpressShare = manageWordpressShare;
	      this.telegramShare = manageTelegramShare;
	      this.redditShare = manageRedditShare;
	      this.evernoteShare = manageEvernoteShare;
	      this.tumblrShare = manageTumblrShare;
	      //**** viber can't share without an element clicked (href)
	      //this.viberShare = manageViberShare;
	      //**** whatsapp can't share without an element clicked (href)
	      //this.whatsappShare = manageWhatsappShare;
	      this.skypeShare = skypeShare;

	      this.share = function shareTrigger(serviceShareConf) {

	        switch (serviceShareConf.provider) {
	          case 'email': {
	            this.emailShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'facebook': {
	            this.facebookShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'twitter': {
	            this.twitterShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'pinterest': {
	            this.pinterestShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'ok': {
	            this.okShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'vk': {
	            this.vkShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'delicious': {
	            this.deliciousShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'digg': {
	            this.diggShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'google+': {
	            this.googleShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'reddit': {
	            this.redditShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'hackernews': {
	            this.hackernewsShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'skype': {
	            this.skypeShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'evernote': {
	            this.evernoteShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'pocket': {
	            this.pocketShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'tumblr': {
	            this.tumblrShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'telegram': {
	            this.telegramShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'xing': {
	            this.xingShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'buffer': {
	            this.bufferShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'stumbleupon': {
	            this.stumbleuponShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'linkedin': {
	            this.linkedinShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'wordpress': {
	            this.wordpressShare($window, serviceShareConf.attrs);
	            break;
	          }
	          case 'flipboard': {
	            this.flipboardShare($window, serviceShareConf.attrs);
	            break;
	          }
	          default: {
	            return;
	          }
	        }
	      };
	    }]
	    , socialshareDirective = /*@ngInject*/ ['$window', 'socialshareConf', 'Socialshare', '$log', function socialshareDirective($window, socialshareConf, $log) {

	      var linkingFunction = function linkingFunction($scope, element, attrs) {

	        // observe the values in each of the properties so that if they're updated elsewhere,
	        // they are updated in this directive.
	        var configurationElement
	        , index = 0
	        , onEventTriggered = function onEventTriggered() {
	          /*eslint-disable no-use-before-define*/
	          if (attrs.socialshareProvider in sharingFunctions) {
	            sharingFunctions[attrs.socialshareProvider]($window, attrs, element);
	          } else {
	            return true;
	          }
	        };
	        /*eslint-enable no-use-before-define*/
	        //looking into configuration if there is a config for the current provider
	        for (; index < socialshareConf.length; index += 1) {
	          if (socialshareConf[index].provider === attrs.socialshareProvider) {
	            configurationElement = socialshareConf[index];
	            break;
	          }
	        }

	        if (socialshareProviderNames.indexOf(configurationElement.provider) === -1) {
	          $log.warn('Invalid Provider Name : ' + attrs.socialshareProvider);
	        }

	        //if some attribute is not define provide a default one
	        attrs.socialshareUrl = attrs.socialshareUrl || configurationElement.conf.url;
	        attrs.socialshareText = attrs.socialshareText || configurationElement.conf.text;
	        attrs.socialshareMedia = attrs.socialshareMedia || configurationElement.conf.media;
	        attrs.socialshareType =  attrs.socialshareType || configurationElement.conf.type;
	        attrs.socialshareVia = attrs.socialshareVia || configurationElement.conf.via;
	        attrs.socialshareTo =  attrs.socialshareTo || configurationElement.conf.to;
	        attrs.socialshareFrom =  attrs.socialshareFrom || configurationElement.conf.from;
	        attrs.socialshareRef = attrs.socialshareRef || configurationElement.conf.ref;
	        attrs.socialshareDislay = attrs.socialshareDislay || configurationElement.conf.display;
	        attrs.socialshareSource = attrs.socialshareSource || configurationElement.conf.source;
	        attrs.socialshareCaption = attrs.socialshareCaption || configurationElement.conf.caption;
	        attrs.socialshareRedirectUri = attrs.socialshareRedirectUri || configurationElement.conf.redirectUri;
	        attrs.socialshareTrigger =  attrs.socialshareTrigger || configurationElement.conf.trigger;
	        attrs.socialsharePopupHeight = attrs.socialsharePopupHeight || configurationElement.conf.popupHeight;
	        attrs.socialsharePopupWidth = attrs.socialsharePopupWidth || configurationElement.conf.popupWidth;
	        attrs.socialshareSubreddit = attrs.socialshareSubreddit || configurationElement.conf.subreddit;
	        attrs.socialshareDescription = attrs.socialshareDescription || configurationElement.conf.description;
	        attrs.socialshareFollow = attrs.socialshareFollow || configurationElement.conf.follow;
	        attrs.socialshareHashtags = attrs.socialshareHashtags || configurationElement.conf.hashtags;
	        attrs.socialshareBody = attrs.socialshareBody || configurationElement.conf.body;
	        attrs.socialshareSubject = attrs.socialshareSubject || configurationElement.conf.subject;
	        attrs.socialshareCc = attrs.socialshareCc || configurationElement.conf.cc;
	        attrs.socialshareBcc = attrs.socialshareBcc || configurationElement.conf.bcc;

	        if (attrs.socialshareTrigger) {

	          element.bind(attrs.socialshareTrigger, onEventTriggered);
	        } else {

	          onEventTriggered();
	        }
	      };

	      return {
	        'restrict': 'A',
	        'link': linkingFunction
	      };
	    }]
	    , sharingFunctions = {
	        'email': manageEmailShare
	      , 'facebook': manageFacebookShare
	      , 'facebook-messenger': facebookMessengerShare
	      , 'twitter': manageTwitterShare
	      , 'google': manageGooglePlusShare
	      , 'reddit': manageRedditShare
	      , 'stumbleupon': manageStumbleuponShare
	      , 'linkedin': manageLinkedinShare
	      , 'pinterest': managePinterestShare
	      , 'digg': manageDiggShare
	      , 'tumblr': manageTumblrShare
	      , 'vk': manageVkShare
	      , 'ok': manageOkShare
	      , 'delicious': manageDeliciousShare
	      , 'buffer': manageBufferShare
	      , 'hackernews': manageHackernewsShare
	      , 'flipboard': manageFlipboardShare
	      , 'pocket': managePocketShare
	      , 'wordpress': manageWordpressShare
	      , 'xing': manageXingShare
	      , 'evernote': manageEvernoteShare
	      , 'whatsapp': manageWhatsappShare
	      , 'telegram': manageTelegramShare
	      , 'viber': manageViberShare
	      , 'skype': skypeShare
	    };


	  angular.module('720kb.socialshare', [])
	  .provider(directiveName + 'Conf', socialshareConfigurationProvider)
	  .service(serviceName, socialshareService)
	  .directive(directiveName, socialshareDirective);
	}(angular));


/***/ },

/***/ 232:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(59);
	__webpack_require__(43);
	__webpack_require__(57);
	__webpack_require__(233);
	__webpack_require__(188);
	__webpack_require__(234);
	__webpack_require__(125);
	__webpack_require__(235);

	__webpack_require__(236);
	//tooltip
	__webpack_require__(238);



	angular.module("directive")
	    .component('itemComments', {
	        template: __webpack_require__(244),
	        controller: Controller,
	        bindings : {
	        	postId : '=',				// post id for which we will fetch comments
	        	showCommentField : '=?',		// show main comment field
	        	commentId : '=?',			// if we want to show only particular comment
	        	getLinkToShare : '&?',		// get function for seperate link to comment
	        	showRefLink : '=?',			// should show reference link button or not
	        	noCommentCallback : '&?',		// this callback will be called if no comment is there
	        	showBigCommnentField : '=?', 	// should show big comment field or one liner field
	        	canUploadMedia : '=?',			// should show upload media button or not
	        	isQaPage : '=?',				// page is qa page or profile page(for styling/changes based on pages )
	        	onDeleteComment : '&?',		// delete comment callback
	        	onPostComment : '&?',		// after successfull post comment callback
	        	postType : '@'
	        },
	        controllerAs: 'vm'
	    });

	Controller.$inject = ['authToken','itemComments','Upload','$state','$filter','forums','utility','$timeout'];

	function Controller(authToken,comment,Upload,$state,$filter,forums,utility,$timeout) {
		var vm = this;

		vm.videoAttachments = {};
		vm.photoAttachments = {};
		vm.reportReasonValue = "wrong";

		console.log(vm.postType);

		//////////////
		// Function //
		//////////////

		//Helper methods
		vm.isUserLoggedIn = authToken.isAuthenticated;
		vm.getUserPic = authToken.getUserPic;
		vm.getCommentTextFieldValue = getCommentTextFieldValue;
		vm.toggleOptions = toggleOptions;
		vm.isAuthorOfComment = isAuthorOfComment;
		vm.trustAsHTML = utility.trustAsHTML;
	  vm.getCareerBookProfileUrl = utility.getCareerBookProfileUrl;

		//Events
		vm.commentFieldFocused = commentFieldFocused;
		vm.commentFieldKeyUp = commentFieldKeyUp;
		vm.cancelComment = cancelComment;
		vm.keyDownEventOnTextField = keyDownEventOnTextField;
		vm.keyUpEventOnEditCommentTextField = keyUpEventOnEditCommentTextField;

		//comment attchments
		vm.changeCommentAttachment = changeCommentAttachment;
		vm.removeCommentPhotoAttachment = removeCommentPhotoAttachment;
		vm.removeCommentVideoAttachment = removeCommentVideoAttachment;

		//edit commets
		vm.editComment = editComment;
		vm.cancelEditComment = cancelEditComment;
		vm.openEditCommentModal = openEditCommentModal;
		vm.closeEditCommentModal =closeEditCommentModal;
		vm.changeEditCommentAttachments = changeEditCommentAttachments;
		vm.removeEditCommentAlreadyAddedPhoto = removeEditCommentAlreadyAddedPhoto;
		vm.removeEditCommentPhotoAttachment = removeEditCommentPhotoAttachment;
		vm.removeEditCommentAlreadyAddedVideo = removeEditCommentAlreadyAddedVideo;
		vm.removeEditCommentVideoAttachment = removeEditCommentVideoAttachment;

		//delete comment modal helper
		vm.showDeleteCommentModal = showDeleteCommentModal;
		vm.closeDeleteCommentModal = closeDeleteCommentModal;


		//report modal helper
		vm.showReportModal = showReportModal;
		vm.closeReportModal = closeReportModal;
		vm.reportComment = reportComment;

		//copy modal helper
		vm.showCopyModal = showCopyModal;
		vm.closeCopyModal = closeCopyModal;

		//Comment API
		vm.addComment = addComment;
		vm.deleteComment = deleteComment;
		vm.saveEditedComment = saveEditedComment;
		vm.likeDislikeComment = likeDislikeComment;
		vm.toggleCommentLike = toggleCommentLike;	// only in profile not forum
		vm.isCommentLikedByUser = isCommentLikedByUser;
		vm.getDisLikeCount = getDisLikeCount;
		vm.getLikeCount = getLikeCount;

		//Sublevel comments helper methods
		vm.showSublevelCommentTextField= showSublevelCommentTextField;
		vm.collapseSublevelCommentTextField = collapseSublevelCommentTextField;
		vm.showSublevelComments = showSublevelComments;

		vm.giveIdOfCommentToShare= giveIdOfCommentToShare;

		//fetch comment users
		vm.fetchCommentUsers = fetchCommentUsers;

		$(document).on('click',function() {
			//Close comment toggle options if open
			$('.post-options').find('.overlay').hide();
	        $('.post-options').removeClass('open');
		})

		vm.$onInit = function(){

			// setupAutoExpandTextArea();
			setTimeout(function(){
				// $("#"+vm.postId+"_textarea").focus();
			})
			//Get Post Comments
			getPostComments('0');

			//Get Like Dislike of comments by user
			getLikeDislikeComments();

			//Get comments like dislike counts
			getCommentLikeDislikeCount();
		}

		function setupAutoExpandTextArea() {
	        $(document)
	            .one('focus.autoExpand', 'textarea.autoExpand', function(e){
	                console.log(e);
	                var savedValue = this.value;
	                this.value = '';
	                this.baseScrollHeight = this.scrollHeight;
	                this.value = savedValue;
	            })
	            .on('input.autoExpand', 'textarea.autoExpand', function(e){
	                console.log("...",e);
	                var minRows = this.getAttribute('data-min-rows')|0, rows;
	                this.rows = minRows;
	                rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
	                this.rows = minRows + rows;
	            });
	    }

	    vm.showMore0LevelComment = function(){
	    	getPostComments('0');
	    }

	    vm.showMore1LevelComment = function(cmnt_cmnt_id){
	    	getPostComments('1',cmnt_cmnt_id);
	    }

		function getPostComments(cmnt_lvl,cmnt_cmnt_id) {
			var last_cmnt_id;
			vm.last_cmnt_id_1 = vm.last_cmnt_id_1 || {}
			if(cmnt_lvl === '0'){
				last_cmnt_id = vm.last_cmnt_id_0;
			}else{
				last_cmnt_id = vm.last_cmnt_id_1[cmnt_cmnt_id];
			}
			comment.getPostComments({
				pst_id : vm.postId,
				cmnt_lvl : cmnt_lvl,
				cmnt_cmnt_id : cmnt_cmnt_id,
				last_cmnt_id : last_cmnt_id
			})
				.then(function(res){
					vm.subLevelComments = vm.subLevelComments || {};
					vm.comments = vm.comments || [];
					vm.allComments = vm.allComments || [];
					vm.allComments = vm.allComments.concat(res.data);
					var allCmnts = res.data;



					if(vm.commentId){
						//if we want to show only one particular comment
						allCmnts = allCmnts.filter(function(v){
							if(v.cmnt_id === vm.commentId || v.cmnt_cmnt_id === vm.commentId){
								return true;
							}
							return false;
						})
					}

					if(cmnt_lvl === '0'){
						vm.comments = vm.comments.concat(allCmnts || [])
						if(allCmnts && allCmnts.length > 0){
							vm.last_cmnt_id_0 = allCmnts[allCmnts.length - 1].cmnt_id;
						}

						if(allCmnts && allCmnts.length < 5){
							vm.hide0LevelMoreLink = true;
						}
					}else if(cmnt_lvl === '1'){
						vm.subLevelComments[cmnt_cmnt_id] = vm.subLevelComments[cmnt_cmnt_id] || {};
						vm.subLevelComments[cmnt_cmnt_id]['comments'] = vm.subLevelComments[cmnt_cmnt_id]['comments'] || [];
						//Add comment to this sublevel
						vm.subLevelComments[cmnt_cmnt_id]['comments'] = vm.subLevelComments[cmnt_cmnt_id]['comments'].concat(allCmnts || []);

						//Hide comment text field of this sublevel
						vm.subLevelComments[cmnt_cmnt_id]['showSublevelCommentTextField'] = true;

						//Hide all comments of this sublevel
						vm.subLevelComments[cmnt_cmnt_id]['showSublevelAllComments'] = true;

						vm.last_cmnt_id_1 = vm.last_cmnt_id_1 || {};
						if(allCmnts && allCmnts.length > 0){
							vm.last_cmnt_id_1[cmnt_cmnt_id] = allCmnts[allCmnts.length - 1].cmnt_id;
						}

						if(allCmnts && allCmnts.length < 5){
							vm.subLevelComments[cmnt_cmnt_id]['hide1LevelMoreLink'] = true;
						}
					}

					// allCmnts.map(function(v,i){
					// 	if(v.cmnt_lvl === "0"){
					// 		vm.comments.push(v);
					// 	}else if(v.cmnt_lvl === "1"){
					// 		vm.subLevelComments[v.cmnt_cmnt_id] = vm.subLevelComments[v.cmnt_cmnt_id] || {};
					// 		vm.subLevelComments[v.cmnt_cmnt_id]['comments'] = vm.subLevelComments[v.cmnt_cmnt_id]['comments'] || [];

					// 		//Add comment to this sublevel
					// 		vm.subLevelComments[v.cmnt_cmnt_id]['comments'].push(v);

					// 		//Hide comment text field of this sublevel
					// 		vm.subLevelComments[v.cmnt_cmnt_id]['showSublevelCommentTextField'] = false;

					// 		//Hide all comments of this sublevel
					// 		vm.subLevelComments[v.cmnt_cmnt_id]['showSublevelAllComments'] = false;
					// 	}
					// });

					// if there is no zero level comments
					if(vm.comments.length <= 0 && vm.noCommentCallback){
						vm.noCommentCallback();
					}
				})
				.catch(function(err){
					console.error(err);
				})
		}

		//get comment text field value
		function getCommentTextFieldValue(ele) {
			return $(ele).text();
		}

		//toggle options
		function toggleOptions($event) {
	        var ele = $event.currentTarget;
	        $event.stopPropagation();
	        if ($(ele).hasClass('open')) {
	            $('.post-options').find('.overlay').hide();
	            $('.post-options').removeClass('open');
	        } else {
	            $('.post-options').find('.overlay').hide();
	            $('.post-options').removeClass('open');
	            $(ele).find('.overlay').show();
	            $(ele).addClass('open');
	        }
	    }

	    //check whether user is author of comment
	    function isAuthorOfComment(cmnt_id){
	    	if(!authToken.isAuthenticated()){
	    		return false;
	    	}

	    	var cmnt = $filter('filter')(vm.allComments, {
	            'cmnt_id': cmnt_id
	        })[0];
	        if(cmnt.usr_id === authToken.getUserId()){
	        	return true;
	        }
	        return false;

	    }

		//clear comment text field value
		function clearCommentTextFieldValue(ele) {
			if(!ele){
				var arr = $("[contenteditable]");
				arr.each(function(i,block){
					$(block).text("");
					$(block).removeAttr("style");
				});
			}
			$(ele).text("");
		}

		//Increase height of filed on focus
		function commentFieldFocused($event) {
			var ele = $event.target;
			if(getCommentTextFieldValue(ele).length <= 0){
				$(ele).text(" ");
			}
			$(ele).css("height","150px").removeClass('with-full-border');

			//Show bottom bar which contain submit button
			$(ele).siblings(".bottom-tf").addClass("show-bar");
		}

		//Cancel comment and Decrease height of comment text field
		function cancelComment($event) {
			var tf = $($event.target).parent().siblings(".input-comment");
			clearCommentTextFieldValue(tf);
			$(tf).text("");
			$(tf).removeAttr("style").addClass('with-full-border');

			//remove bottom bar which contain submit button
			$(tf).siblings(".bottom-tf").removeClass("show-bar");
		}

		//Key up event on comment text field
		function commentFieldKeyUp($event) {
			var ele = $event.target;
			if(getCommentTextFieldValue(ele).length <= 0){
				$(ele).text(" ");
			}
		}

		// Key down event
		// when user press any key on comment textfield
		function keyDownEventOnTextField($event,uniqueid,cmnt_cmnt_id,cmnt_lvl){
			var eve = $event;
			vm.comment = vm.comment || {};
			if(eve.keyCode === 13 && eve.shiftKey === false){
				if(vm.comment[uniqueid] && vm.comment[uniqueid].length > 0){
					addComment($event,uniqueid,cmnt_cmnt_id,cmnt_lvl);
					return;
				}else{
					vm.comment[uniqueid] = "";
					eve.stopPropagation();
					return;
				}
			}
		}

		// Key down event on edit comment field
		// when user press any key on edit comment textfield
		function keyUpEventOnEditCommentTextField($event,uniqueid){
			var eve = $event;
			vm.comment = vm.comment || {};
			if(eve.keyCode === 13  && eve.shiftKey === false){
				if(vm.comment[uniqueid] && vm.comment[uniqueid].length > 0){
					saveEditedComment(uniqueid,vm.comment[uniqueid]);
					return;
				}else{
					vm.comment[uniqueid] = "";
					eve.stopPropagation();
					return;
				}
			}
		}

		//Seperate image and video in attachement whenever attachment changes
		function changeCommentAttachment(id,$files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
			vm.photoAttachments = vm.photoAttachments || {};
			vm.videoAttachments = vm.videoAttachments || {};
			vm.photoAttachments[id] = [];
	        vm.videoAttachments[id] = [];
	        vm.commentAttchmentError = false;
	        vm.commentAttachments[id].forEach(function(v,i){
	            if(v.type.indexOf('image/') > -1){
	                vm.photoAttachments[id].push(v);
	            }else if(v.type.indexOf('video/mp4') > -1){
	                vm.videoAttachments[id].push(v);
	            }else{
	                vm.statusAttchmentError = true;
	            }
	        });
		}

		//remove attached photo
		function removeCommentPhotoAttachment(id, index) {
	        var pos = vm.commentAttachments[id].indexOf(vm.photoAttachments[id][index]);
	        vm.commentAttachments[id].splice(pos,1);
	        vm.photoAttachments[id].splice(index, 1);
	    }

	    //remove attached video
		function removeCommentVideoAttachment(id, index) {
	        var pos = vm.commentAttachments[id].indexOf(vm.videoAttachments[id][index]);
	        vm.commentAttachments[id].splice(pos,1);
	        vm.videoAttachments[id].splice(index, 1);
	    }

	    //open edit comment dialog
	    function openEditCommentModal() {
	        vm.showEditCommentModal = true;
	        $("body").addClass('ds-modal-open');
	    }

	    //close edit comment dialog
	    function closeEditCommentModal() {
	        vm.showEditCommentModal = false;
	        $("body").removeClass('ds-modal-open');
	    }

	    //change edit comment attachement
	    //Seperate image and video in attachement whenever attachment changes
	    function changeEditCommentAttachments($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
	        vm.editCommentPhotoAttachments = [];
			vm.editCommentVideoAttachments = [];
	        vm.editCommentAttachmentError = false;
	        vm.editCommentAttachments.forEach(function(v,i){
	            if(v.type.indexOf('image/') > -1){
	                vm.editCommentPhotoAttachments.push(v);
	            }else if(v.type.indexOf('video/mp4') > -1){
	                vm.editCommentVideoAttachments.push(v);
	            }else{
	                vm.editCommentAttachmentError = true;
	            }
	        });
	    }

	    function editComment_old(commentId) {
	        var comment = $filter('filter')(vm.allComments, {
	            'cmnt_id': commentId
	        })[0];
	        vm.currentCommentEditId = commentId;

	        vm.alreadyAddedPhotos = angular.copy(comment.cmnt_img || []) || [];
	        vm.alreadyAddedVideos = angular.copy(comment.cmnt_vid || []) || [];
	        vm.alreadyAddedVideosName = vm.alreadyAddedVideos.map(function(v) {
	            var pos = v.lastIndexOf('/');
	            var tempName = v.substr(pos + 1);
	            var posOfUnderScore = tempName.lastIndexOf('_');
	            var posOfDot = tempName.lastIndexOf('.');
	            var name = tempName.substr(0, posOfUnderScore) + tempName.substr(posOfDot);
	            console.log(name);
	            return name;
	        })
	        vm.editCommentCreatedAt = comment.cmnt_ts;
	        vm.editCommentLevel = comment.cmnt_lvl;
	        openEditCommentModal();
	        setTimeout(function() {
	            $('.edit-comment-area').html(comment.cmnt_txt);
	            $('.edit-comment-area').froalaEditor({
		            toolbarButtons : ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertTable', 'undo', 'redo', 'clearFormatting', 'html'],
		            heightMin: 300,
		            fontSizeDefaultSelection: '18',
		            fontSizeSelection: true,
		            placeholderText: 'Type Here',
		            imageUploadParam: 'upload',
		            imageUploadURL: '/ckeditor/save',
		            imageUploadMethod: 'POST',
		            imageMaxSize: 5 * 1024 * 1024,
		            imageAllowedTypes: ['jpeg', 'jpg', 'png']
		        });
	        }, 100);
	    }

	    function editComment(commentId){
	    	vm.isCommentEditingInProcess=true;
	    	vm.currentCommentEditing=commentId;
	    }

	    function cancelEditComment(commentId){
	    	vm.isCommentEditingInProcess=false;
	    	vm.currentCommentEditing='';
	    	vm.comment[commentId] = '';
	    }


	    //remove edit comment already added photos
	    function removeEditCommentAlreadyAddedPhoto(index) {
	        vm.alreadyAddedPhotos.splice(index, 1);
	    }

	    //remove edit comment photo attachments(newly added photos)
	    function removeEditCommentPhotoAttachment(index) {
	        var pos = vm.editCommentAttachments.indexOf(vm.editCommentPhotoAttachments[index]);
	        vm.editCommentAttachments.splice(pos,1);
	        vm.editCommentPhotoAttachments.splice(index, 1);
	    }

	    //remove edit comment already added photos
	    function removeEditCommentAlreadyAddedVideo(index) {
	        vm.alreadyAddedVideos.splice(index, 1);
	    }

	    //remove edit comment video attachments(newly added videos)
	    function removeEditCommentVideoAttachment(index) {
	        var pos = vm.editCommentAttachments.indexOf(vm.editCommentVideoAttachments[index]);
	        vm.editCommentAttachments.splice(pos,1);
	        vm.editCommentVideoAttachments.splice(index, 1);
	    }

	    //save edited comments
	    function saveEditedComment(commentId,cmnt_txt) {
	        vm.isPostUploading = true;

	        var cmnt = $filter('filter')(vm.allComments,{cmnt_id:commentId})[0];

	        var obj_edit = {
	            pst_id: vm.postId,
	            cmnt_txt: cmnt_txt,//$('.edit-comment-area').find('.fr-view').html(),
	            cmnt_id: commentId,
	            usr_id: authToken.getUserId(),
	            cmnt_lvl: cmnt.cmnt_lvl,
	            cmnt_ts: cmnt.cmnt_ts,
	            cmnt_cmnt_id: cmnt.cmnt_cmnt_id,
	        }

	        comment.editComment(obj_edit)
	            .then(function(d) {

	    			vm.isCommentEditingInProcess=false;

	    			vm.currentCommentEditing='';

	    			vm.comment[commentId] = "";

	    			// getPostComments();
	    			if(cmnt.cmnt_lvl === '0'){
	    				//if level 0
	    				var c = $filter('filter')(vm.comments,{cmnt_id:commentId})[0];
	    				c.cmnt_txt = cmnt_txt;
	    			}else if(cmnt.cmnt_lvl === '1'){
	    				var tempComments = vm.subLevelComments[cmnt.cmnt_cmnt_id].comments;
	    				var c = $filter('filter')(tempComments,{cmnt_id:commentId})[0];
	    				c.cmnt_txt = cmnt_txt;
	    			}

	                // closeEditCommentModal();
	                // $('.edit-comment-area').html(''),
	                // $(".edit-comment-modal .progressbar").css('width', '0%');
	                // $state.reload();
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	            .finally(function() {
	                vm.isPostUploading = false;
	            }, function(evt) {
	                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                $(".edit-comment-modal .progressbar").css('width', progressPercentage + '%');
	                console.log('progress: ' + progressPercentage + '% ');
	            })
	    }

		//Add Comment to database on submit button click
		//if user is logged in
		function addComment($event,uniqueid,cmnt_cmnt_id,cmnt_lvl){
			if(!vm.isUserLoggedIn()){
				vm.commentPostingErr = "You need to login to comment"
				return;
			}
			var txt_ele = $($event.target).parent().siblings('.input-comment');
			var progress_ele = $($event.target).parent().find('.progressbar');
			var usr_id = authToken.getUserId();
			var pst_id = vm.postId;
			var cmnt_txt = vm.comment[uniqueid];
			var cmnt_atch = [];
			var cmnt_vid = vm.videoAttachments[uniqueid] || [];
			var cmnt_img = vm.photoAttachments[uniqueid] || [];
			var cmnt_cmnt_id = cmnt_cmnt_id || undefined;
			var cmnt_lvl = cmnt_lvl;

			var obj = {
				usr_id : usr_id,
				pst_id : pst_id,
				cmnt_txt : cmnt_txt,
				cmnt_atch : cmnt_atch,
				cmnt_vid : cmnt_vid,
				cmnt_img : cmnt_img,
				cmnt_cmnt_id : cmnt_cmnt_id,
				cmnt_lvl : cmnt_lvl,
				pst_typ : vm.postType
			}

			vm.isPostSubmitting = true;
			comment.saveComment(obj)
				.then(function(res){
					obj.cmnt_id = res.data.cmnt_id;
					obj.cmnt_ts = res.data.cmnt_ts;

					if(vm.onPostComment){
						vm.onPostComment({
							cmnt : obj
						});
					}


					vm.comment[uniqueid] = "";

					vm.commentAttachments = vm.commentAttachments || {};
					vm.commentAttachments[uniqueid] = [];

					if(cmnt_img.length > 0 || cmnt_vid.length > 0){
						//if Attachment is there then reload
						$state.reload();
					}

					obj['usr_info'] = authToken.getUser();

					vm.allComments.push(obj);
					if(cmnt_lvl === "1"){
						vm.subLevelComments[cmnt_cmnt_id] = vm.subLevelComments[cmnt_cmnt_id] || {};
						vm.subLevelComments[cmnt_cmnt_id]['comments'] = vm.subLevelComments[cmnt_cmnt_id]['comments'] || [];

						//Add comment to this sublevel
						vm.subLevelComments[cmnt_cmnt_id]['comments'].push(obj);

						//Show comment text field of this sublevel
						vm.subLevelComments[cmnt_cmnt_id]['showSublevelCommentTextField'] = true;

						//Show all comments of this sublevel
						vm.subLevelComments[cmnt_cmnt_id]['showSublevelAllComments'] = true;
					}else{
						//Cmnt_lvl == 0
						vm.comments.push(obj);
					}

					cancelComment($event);
				},null,function progressNotification(progress) {
					var percentage = (progress.loaded / progress.total) * 100;
					console.info(percentage);
					$(progress_ele).css('width',percentage + '%');
				})
				.catch(function(err){
					console.error(err);
				})
				.finally(function() {
					vm.isPostSubmitting = false;
					$(progress_ele).css('width','0%');
				})
		}


		//open delete comment modal
		function showDeleteCommentModal(cmnt_id){
			vm.currentlyDeletedComment = cmnt_id;
			vm.shouldShowDeleteCommentModal = true;
		}

		//close delete comment modal
		function closeDeleteCommentModal(){
			vm.currentlyDeletedComment = '';
			vm.shouldShowDeleteCommentModal = false;
		}

		//open report modal
		function showReportModal(cmnt_id){
			vm.currentlyReportedComment = cmnt_id;
			vm.shouldShowReportModal = true;
		}

		//close report modal
		function closeReportModal(){
			vm.currentlyReportedComment = '';
			vm.shouldShowReportModal = false;
		}

		//report comment to server
		function reportComment(cmnt_id){
			var rsn = vm.reportReasonValue;
	        if(vm.reportReasonValue === 'other'){
	            //get value from rsn textarea
	            rsn = vm.reportReasonOtherValue;
	        }

	        //report to server
	        forums.reportQuestion({
	            cmpln_id: cmnt_id,
	            usr_id : authToken.getUserId(),
	            cmpln_rsn : rsn,
	            cmpln_typ : 'answer'
	        })
	            .then(function(res) {
	                closeReportModal();
	            })
	            .catch(function(err) {
	                console.log(err);
	            });
		}

		//open copy modal
		function showCopyModal(cmnt_id){
			vm.currentlyCopiedComment = cmnt_id;
			vm.link = window.location.origin + '/question/' + vm.postId + '/answer/' + cmnt_id;
			vm.shouldShowCopyModal = true;
		}

		//close copy modal
		function closeCopyModal(){
			vm.currentlyCopiedComment = '';
			vm.shouldShowCopyModal = false;
		}

		//delete comment from server using api
		function deleteComment(cmnt_id){
			var cmnt = $filter('filter')(vm.allComments, {
	            'cmnt_id': cmnt_id
	        })[0];
	        var delete_obj = {
	        	pst_id : vm.postId,
	        	cmnt_id : cmnt.cmnt_id,
	        	cmnt_ts :cmnt.cmnt_ts,
	        	cmnt_lvl : cmnt.cmnt_lvl,
	        	cmnt_cmnt_id : cmnt.cmnt_cmnt_id,
	        }
			comment.deleteComment(delete_obj)
				.then(function(res){

					if(vm.onDeleteComment){
						vm.onDeleteComment();
					}else{
						if(cmnt.cmnt_lvl === '0'){
							//if it is 0 level comment
							//then direct remove from 0th level comments array
							vm.comments = vm.comments.filter(function(v,i){
								if(v.cmnt_id === cmnt_id){
									return false;
								}
								return true;
							})
						}else{
							//if it is 1st level comment
							//then first find that comment from parent's all sublevel comments
							//then delete it
							var tempComments = vm.subLevelComments[cmnt.cmnt_cmnt_id].comments;
							vm.subLevelComments[cmnt.cmnt_cmnt_id].comments = tempComments.filter(function(v,i){
								if(v.cmnt_id === cmnt_id){
									return false;
								}
								return true;
							})
						}

					}
					closeDeleteCommentModal();
				})
				.catch(function(err) {
					console.error(err);
				})
		}

		//Toggle like button (for now only in ewcb)
		//Like or Unlike
		function toggleCommentLike(cmnt_id,isLike) {
			//Fetch comment from comments data
			var cmnt = $filter('filter')(vm.commentsLikeData,{cmnt_id:cmnt_id})[0];
			var cmnt_like_stats = $filter('filter')(vm.commentsLikeCountStats,{cmnt_id:cmnt_id})[0];

			if(!cmnt_like_stats){
				var tmp_obj = {
					pst_id : vm.postId,
					cmnt_id : cmnt_id,
					like_cnt : 0,
					dlike_cnt : 0
				}
				vm.commentsLikeCountStats.push(tmp_obj);
				cmnt_like_stats = tmp_obj;
			}

			var cnt_obj = {
				pst_id : vm.postId,
				cmnt_id : cmnt_id,
			}
			if(cmnt){
				//if comment is already in comment data array means user has liked or unliked earlier
				if(isLike){
					//Increment like count
					cnt_obj['like'] = 1;

					cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;
				}else if(!isLike){
					//decrement like count
					cnt_obj['like'] = -1;

					cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) - 1;
				}
			}else{
				if(isLike){
					//Increment like count
					cnt_obj['like'] = 1;

					cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;

				}else if(!isLike){
					//keep like count as it is
					cnt_obj['like'] = -1;

					cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;
				}
			}

			comment.updateCommentLikeDislikeCount(cnt_obj)
				.then(function(res) {
				})
				.catch(function(err) {
					console.error(err);
				});

			var obj = {
				pst_id : vm.postId,
				usr_id : authToken.getUserId(),
				cmnt_id : cmnt_id,
				like_dlike_flg : isLike
			}
			comment.likeDislikeComment(obj)
				.then(function(res) {
					if(cmnt){
						//if comment is already added update it
						cmnt.like_dlike_flg = isLike;
					}else{
						//if comment is not there push it
						vm.commentsLikeData.push(obj);
					}
				})
				.catch(function(err) {
					console.error(err);
				});
		}

		//like/dislike comment by user
		function likeDislikeComment(cmnt_id,isLike){
			//Fetch comment from comments data
			var cmnt = $filter('filter')(vm.commentsLikeData,{cmnt_id:cmnt_id})[0];
			var cmnt_like_stats = $filter('filter')(vm.commentsLikeCountStats,{cmnt_id:cmnt_id})[0];

			if(!cmnt_like_stats){
				var tmp_obj = {
					pst_id : vm.postId,
					cmnt_id : cmnt_id,
					like_cnt : 0,
					dlike_cnt : 0
				}
				vm.commentsLikeCountStats.push(tmp_obj);
				cmnt_like_stats = tmp_obj;
			}

			var cnt_obj = {
				pst_id : vm.postId,
				cmnt_id : cmnt_id,
			}
			if(cmnt){
				//if comment is already in comment data array means user has liked or unliked earlier
				if(isLike){
					//Increment like count
					cnt_obj['like'] = 1;
					//decrement dislike count
					cnt_obj['dislike'] = -1;

					cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;
					cmnt_like_stats.dlike_cnt = parseInt(cmnt_like_stats.dlike_cnt) - 1;
				}else if(!isLike){
					//decrement like count
					cnt_obj['like'] = -1;
					//increment dislike count
					cnt_obj['dislike'] = 1;

					cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) - 1;
					cmnt_like_stats.dlike_cnt = parseInt(cmnt_like_stats.dlike_cnt) + 1;
				}
			}else{
				if(isLike){
					//Increment like count
					cnt_obj['like'] = 1;
					//keep dislike count as it is
					cnt_obj['dislike'] = 0;

					cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;

				}else if(!isLike){
					//keep like count as it is
					cnt_obj['like'] = 0;
					//increment dislike count
					cnt_obj['dislike'] = 1;

					cmnt_like_stats.dlike_cnt = parseInt(cmnt_like_stats.dlike_cnt) + 1;
				}
			}

			comment.updateCommentLikeDislikeCount(cnt_obj)
				.then(function(res) {
				})
				.catch(function(err) {
					console.error(err);
				});

			var obj = {
				pst_id : vm.postId,
				usr_id : authToken.getUserId(),
				cmnt_id : cmnt_id,
				like_dlike_flg : isLike
			}
			comment.likeDislikeComment(obj)
				.then(function(res) {
					if(cmnt){
						//if comment is already added update it
						cmnt.like_dlike_flg = isLike;
					}else{
						//if comment is not there push it
						vm.commentsLikeData.push(obj);
					}
				})
				.catch(function(err) {
					console.error(err);
				});
		}

		//get users like/dislike comments
		function getLikeDislikeComments(){
			if(!authToken.isAuthenticated()){
				//If user is not logged in
				return;
			}
			var obj = {
				pst_id : vm.postId,
				usr_id : authToken.getUserId()
			}
			comment.getLikeDislikeComments(obj)
				.then(function(res) {
					vm.commentsLikeData = res.data;
				})
				.catch(function(err) {
					console.error(err);
				});
		}

		//get like/dislike count from server
		function getCommentLikeDislikeCount(){
			var obj = {
				pst_id : vm.postId,
			}
			comment.getCommentLikeDislikeCount(obj)
				.then(function(res) {
					vm.commentsLikeCountStats = res.data;
				})
				.catch(function(err) {
					console.error(err);
				});
		}

		//get like count from array
		function getLikeCount(cmnt_id) {
			vm.commentsLikeCountStats = vm.commentsLikeCountStats || [];
			var cmnt = $filter('filter')(vm.commentsLikeCountStats,{cmnt_id:cmnt_id})[0];
			if(cmnt){
				return cmnt.like_cnt;
			}
			return 0;
		}
		//get dislike count from array
		function getDisLikeCount(cmnt_id) {
			vm.commentsLikeCountStats = vm.commentsLikeCountStats || [];
			var cmnt = $filter('filter')(vm.commentsLikeCountStats,{cmnt_id:cmnt_id})[0];
			if(cmnt){
				return cmnt.dlike_cnt;
			}
			return 0;
		}

		//whether comment is liked or disliked by user
		function isCommentLikedByUser(cmnt_id) {
			if(authToken.isAuthenticated()){
				vm.commentsLikeData = vm.commentsLikeData || [];
				var cmnt = $filter('filter')(vm.commentsLikeData,{cmnt_id:cmnt_id})[0];
				if(!cmnt){
					//If not liked or disliked
					return 0;
				}
				if(cmnt['like_dlike_flg'] === 0){
					//if dislike
					return false;
				}else{
					//if like
					return true;
				}
			}
		}

		//show sublevel comment textfield
		function showSublevelCommentTextField(id,idIfLevel1Comment){
			vm.subLevelComments[id] = vm.subLevelComments[id] || {};
			var level1Comment = {};
			if(idIfLevel1Comment){
				level1Comment = vm.subLevelComments[id].comments.filter(function(v,i){
					if(v.cmnt_id === idIfLevel1Comment){
						return true;
					}
					return false;
				})[0];
				var usr_nm = level1Comment.usr_info.dsp_nm;
				console.log(usr_nm);
				$timeout(function(){
					vm.comment = vm.comment || {};
					vm.comment[id] = usr_nm + " : ";
				});
			}else{
				getPostComments('1',id);
			}
			vm.subLevelComments[id]['showSublevelCommentTextField'] = true;
			setTimeout(function(){
				$("#"+id + "_textarea").focus();
				if(!checkVisible($("#"+id + "_textarea"))){
					$('html,body').animate({
					        scrollTop: $("#"+id + "_textarea").offset().top - 230},
					        'fast');
				}
			});
		}

		function checkVisible( elm, evalType ) {
		    evalType = evalType || "visible";
		    var vpH = $(window).height(), // Viewport Height
		        st = $(window).scrollTop() + 230, // Scroll Top
		        y = $(elm).offset().top,
		        elementHeight = $(elm).height();

		    if (evalType === "visible") return ((y < (vpH + st)) && (y > (st - elementHeight)));
		    if (evalType === "above") return ((y < (vpH + st)));
		}

		//collapse sublevel comments
		function collapseSublevelCommentTextField(id){
			vm.subLevelComments[id] = vm.subLevelComments[id] || {};
			vm.subLevelComments[id]['showSublevelCommentTextField'] = false;
			vm.comment[id] = "";
		}

		function showSublevelComments(id){
			vm.subLevelComments[id] = vm.subLevelComments[id] || {};
			vm.subLevelComments[id]['showSublevelAllComments'] = true;
		}

		//give id of comment
		function giveIdOfCommentToShare(id){
			vm.getLinkToShare({id:id});
		}


		function fetchCommentUsers(cmnt_id) {
			comment.getCommentUsers({cmnt_id:cmnt_id})
				.then(function(res){
					console.log(res);
					var usersName = res.data.map(function(v,i){
						return v.dsp_nm;
					})

					if(usersName.length > 0){
						var text = "<div>" + usersName.join(",") + " like this.</div>";
						vm.commentsUsers = vm.commentsUsers || {};
						vm.commentsUsers[cmnt_id] = text;
					}
				})
				.catch(function(err){
					console.log(err);
				})
		}
	}

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 233:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(43);
	angular
	.module('service')
	.service('itemComments', Service);

	Service.$inject = ['$http','Upload','BASE_API'];

	function Service($http,Upload,BASE_API){

		this.saveComment = function(data){

			return Upload.upload({
	            url: BASE_API + '/itemComments/save',
	            data: data,
	            method: 'POST'
	        });

	        // return $http.post(BASE_API + '/itemComments/save',data);
	    }

	    this.editComment = function(data) {
	        return Upload.upload({
	            url: BASE_API + '/itemComments/edit',
	            data: data,
	            method: 'POST'
	        });
	    }

	    this.deleteComment = function(data){
	        return $http.post(BASE_API + '/itemComments/delete',data);
	    }

	    this.likeDislikeComment = function(data) {
	        return $http.post(BASE_API + '/itemComments/like_dislike',data);
	    }
	    // 
	    this.getLikeDislikeComments = function(data) {
	        return $http.post(BASE_API + '/itemComments/getLikeDislikeComments',data);
	    }

	    this.getCommentLikeDislikeCount = function(data) {
	        return $http.post(BASE_API + '/itemComments/getCommentLikeDislikeCount',data);   
	    }

	    this.updateCommentLikeDislikeCount = function(data) {
	        return $http.post(BASE_API + '/itemComments/updateCommentLikeDislikeCount',data);   
	    }
	    
	    this.getPostComments = function(data){
	        return $http.post(BASE_API + '/itemComments/get',data);
	    }

	    // 
	    this.getCommentUsers = function(data){
	        return $http.post(BASE_API + '/itemComments/getCommentUsers',data);
	    }




	    
	    
	}


/***/ },

/***/ 234:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(51);
	angular.module('service').service('forums',Forums);
	Forums.$inject = ['$http','BASE_API'];
	function Forums($http,BASE_API) {
	    this.saveQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/saveQuestion',data);
	    }

	    this.updateQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/updateQuestion',data);
	    }

	    this.deleteQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/deleteQuestion',data);
	    }

	    this.getFirstFewQuestions = function (data) {
	        return $http.post(BASE_API + '/forums/getFirstFewQuestions',data);
	    }

	    this.getNextFewQuestions = function (data) {
	        return $http.post(BASE_API + '/forums/getNextFewQuestions',data);
	    }

		this.updateTrendingViews = function (data) {
	        return $http.post(BASE_API + '/forums/updateTrendingViews',data);
	    }

	    this.getTrendingQrys = function (data) {
	        return $http.post(BASE_API + '/forums/getTrendingQrys',data);
	    }

	    this.updateTrendingCounts = function (data) {
	        return $http.post(BASE_API + '/forums/updateTrendingCounts',data);
	    }

	    this.getQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/getQuestion',data);
	    }

	    this.getAllQuestion = function (data) {
	        return $http.post(BASE_API + '/forums/getAllQuestion',data);
	    }

	    ////////////////////
	    // Categories API //
	    ////////////////////
	    this.getAllCategories = function (data) {
	        return $http.post(BASE_API + '/forums/category/getAllCategories',data);
	    }
	    this.saveCategory = function (data) {
	        //if coming with id then update or else add new
	        return $http.post(BASE_API + '/forums/category/saveCategory',data);
	    }
	    this.deleteCategory = function (data) {
	        return $http.post(BASE_API + '/forums/category/deleteCategory',data);
	    }

	    ///////////////////////
	    // qry stats counter //
	    ///////////////////////
	    this.updateQuestionStatsCounter = function (data) {
	        return $http.post(BASE_API + '/forums/updateQuestionStatsCounter',data);
	    }
	    this.getQuestionStatsCounter = function (data) {
	        return $http.post(BASE_API + '/forums/getQuestionStatsCounter',data);
	    }

	    ///////////////////////
	    // user qry stats counter //
	    ///////////////////////

	    this.updateUserQuestionStats = function (data) {
	        return $http.post(BASE_API + '/forums/updateUserQuestionStats',data);
	    }
	    this.getUserQuestionStats = function (data) {
	        return $http.post(BASE_API + '/forums/getUserQuestionStats',data);
	    }

	    //update question stats + update user question stats
	    this.updateQuestionStats = function (data) {
	        return $http.post(BASE_API + '/forums/updateQuestionStats',data);
	    }

	    ///////////////
	    //Follow API //
	    ///////////////

	    this.updateFollower = function (data) {
	        // data = {usr_id , qry_id}
	        return $http.post(BASE_API + '/forums/follow/updateFollower',data);
	    }

	    this.deleteFollower = function (data) {
	        // data = {usr_id , qry_id}
	        return $http.post(BASE_API + '/forums/follow/deleteFollower',data);
	    }

	    this.updateNotifications = function(data){
	        // data = {ans_usr_id , qry_id}
	        return $http.post(BASE_API + '/forums/follow/updateNotifications',data);
	    }

	    // answer later
	    this.updateAnswerLater = function(data){
	        // data = {usr_id , qry_id , ans_flg}
	        return $http.post(BASE_API + '/forums/updateAnswerLater',data);
	    }

	    this.getAnswerLater = function(data){
	        // data = {usr_id}
	        return $http.post(BASE_API + '/forums/getAnswerLater',data);
	    }

	    //Follow Notifications
	    this.getAllNotifications = function(data){
	        //
	        // data = {usr_id}
	        return $http.post(BASE_API + '/forums/getAllNotifications',data);
	    }

	    this.getDetailedFollowNotifications = function(data){
	        return $http.post(BASE_API + '/forums/getDetailedFollowNotifications',data);
	    }

	    this.markFollowNotificationRead = function(data){
	        return $http.post(BASE_API + '/forums/follow/markFollowNotificationRead',data);      
	    }





	    //report question
	    this.reportQuestion = function(data){
	        return $http.post(BASE_API + '/forums/saveComplain',data);
	    }

	    this.saveQAAnswer = function(data){
	        return $http.post(BASE_API + '/forums/ans/saveAnswer',data);
	    }

	    this.deleteQAAnswer = function(data){
	        return $http.post(BASE_API + '/forums/ans/deleteAnswer',data);
	    }

	    this.editQAAnswer = function(data){
	        return $http.post(BASE_API + '/forums/ans/editAnswer',data);
	    }

	    this.getAllAnswersOfQuestion = function(data){
	        return $http.post(BASE_API + '/forums/ans/getAllAnswersOfQuestion',data);
	    }

	    this.getAnswerOfQuestionUsingId = function(data){
	        return $http.post(BASE_API + '/forums/ans/getAnswerOfQuestionUsingId',data);
	    }

	    this.likeDislikeComment = function(data) {
	        return $http.post(BASE_API + '/forums/ans/likeDislike',data);
	    }
	    this.updateCommentLikeDislikeCount = function(data) {
	        return $http.post(BASE_API + '/forums/ans/updateLikeDislikeCount',data);
	    }
	}


/***/ },

/***/ 235:
/***/ function(module, exports) {

	var app = angular.module("directive");

	app.directive('userDefaultPic', function() {
	    return {
	        link: function(scope, element, attrs) {
	            if (!attrs.src) {
	                attrs.$set('src', "/assets/images/dummy-user-pic.png");
	            }
	            
	            element.bind('error', function(e) {
	                attrs.$set('src', "/assets/images/dummy-user-pic.png");
	            });
	        }
	    }
	});

/***/ },

/***/ 236:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(237);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_comments_component.scss", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_comments_component.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 237:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.comment-component-wrapper .comment-loader-wrapper {\n  background: #e5e5e5;\n  padding: 10px; }\n  .comment-component-wrapper .comment-loader-wrapper .loader {\n    width: 5em;\n    height: 5em; }\n\n.comment-component-wrapper .post {\n  background: #fff;\n  max-width: 1280px; }\n  .comment-component-wrapper .post:first-child {\n    margin-top: 0px; }\n  .comment-component-wrapper .post .content-wrapper {\n    position: relative;\n    padding: 10px; }\n  .comment-component-wrapper .post .head {\n    margin-bottom: 10px;\n    display: block;\n    position: relative; }\n  .comment-component-wrapper .post .profile-pic {\n    width: 50px;\n    height: 50px;\n    float: left; }\n  .comment-component-wrapper .post .post-options {\n    position: absolute;\n    top: -5px;\n    right: 10px;\n    cursor: pointer;\n    font-size: 20px;\n    display: none; }\n    .comment-component-wrapper .post .post-options .overlay {\n      position: absolute;\n      right: 0px;\n      top: 30px;\n      display: none;\n      z-index: 1; }\n      .comment-component-wrapper .post .post-options .overlay .list-group-item {\n        padding: 8px 12px;\n        white-space: nowrap; }\n      .comment-component-wrapper .post .post-options .overlay h6 {\n        margin: 0px; }\n  .comment-component-wrapper .post .post-btn {\n    background: #1d99d1 !important;\n    margin-right: 0px !important;\n    color: white !important;\n    cursor: pointer; }\n    .comment-component-wrapper .post .post-btn.disabled {\n      pointer-events: none;\n      cursor: none; }\n  .comment-component-wrapper .post .status-attachment-wrapper {\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row;\n    -webkit-box-lines: multiple;\n    -moz-box-lines: multiple;\n    box-lines: multiple;\n    -webkit-flex-wrap: wrap;\n    -moz-flex-wrap: wrap;\n    -ms-flex-wrap: wrap;\n    flex-wrap: wrap; }\n  .comment-component-wrapper .post .status-attachment {\n    display: inline-block;\n    margin-right: 10px;\n    margin-bottom: 10px;\n    position: relative; }\n    .comment-component-wrapper .post .status-attachment.video {\n      display: block;\n      width: 100%;\n      margin-right: 0px;\n      background: #f6f7f8;\n      padding: 0px 10px; }\n      .comment-component-wrapper .post .status-attachment.video .videoName {\n        font-family: 'Aller';\n        font-size: 18px;\n        color: #6186b5; }\n      .comment-component-wrapper .post .status-attachment.video .remove-icon {\n        position: relative;\n        display: inline-block;\n        font-size: 18px;\n        cursor: pointer;\n        float: right; }\n    .comment-component-wrapper .post .status-attachment .image {\n      width: 100px;\n      height: 100px;\n      background-repeat: no-repeat;\n      position: relative;\n      display: inline-block; }\n    .comment-component-wrapper .post .status-attachment .remove-icon {\n      position: relative;\n      display: block;\n      text-align: right;\n      cursor: pointer; }\n  .comment-component-wrapper .post .progressbar {\n    width: 00%;\n    height: 5px;\n    background: #93cede;\n    background: -moz-linear-gradient(-45deg, #93cede 0%, #75bdd1 41%, #49a5bf 100%);\n    background: -webkit-gradient(left top, right bottom, color-stop(0%, #93cede), color-stop(41%, #75bdd1), color-stop(100%, #49a5bf));\n    background: -webkit-linear-gradient(-45deg, #93cede 0%, #75bdd1 41%, #49a5bf 100%);\n    background: -o-linear-gradient(-45deg, #93cede 0%, #75bdd1 41%, #49a5bf 100%);\n    background: -ms-linear-gradient(-45deg, #93cede 0%, #75bdd1 41%, #49a5bf 100%);\n    background: linear-gradient(135deg, #93cede 0%, #75bdd1 41%, #49a5bf 100%);\n    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#93cede', endColorstr='#49a5bf', GradientType=1 ); }\n  .comment-component-wrapper .post .content {\n    font-family: 'Aller';\n    font-size: 15px;\n    background: transparent;\n    border: 0px;\n    padding: 0px;\n    white-space: pre-wrap;\n    max-height: 100px;\n    line-height: 25px;\n    overflow: hidden; }\n    .comment-component-wrapper .post .content.full-post {\n      max-height: none;\n      overflow: visible; }\n  .comment-component-wrapper .post .more-btn {\n    background: none;\n    border: 0px;\n    border-radius: 0px;\n    color: #1d99d1;\n    font-size: 15px;\n    font-family: 'Aller';\n    float: right; }\n  .comment-component-wrapper .post .post-image-attachment {\n    display: block;\n    display: -webkit-box;\n    display: -moz-box;\n    display: box;\n    display: -webkit-flex;\n    display: -moz-flex;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -moz-box-align: center;\n    box-align: center;\n    -webkit-align-items: center;\n    -moz-align-items: center;\n    -ms-align-items: center;\n    -o-align-items: center;\n    align-items: center;\n    -ms-flex-align: center;\n    -webkit-box-pack: center;\n    -moz-box-pack: center;\n    box-pack: center;\n    -webkit-justify-content: center;\n    -moz-justify-content: center;\n    -ms-justify-content: center;\n    -o-justify-content: center;\n    justify-content: center;\n    -ms-flex-pack: center;\n    -webkit-box-orient: horizontal;\n    -moz-box-orient: horizontal;\n    box-orient: horizontal;\n    -webkit-box-direction: normal;\n    -moz-box-direction: normal;\n    box-direction: normal;\n    -webkit-flex-direction: row;\n    -moz-flex-direction: row;\n    flex-direction: row;\n    -ms-flex-direction: row; }\n    .comment-component-wrapper .post .post-image-attachment img {\n      max-width: 100%;\n      height: auto;\n      margin-bottom: 10px;\n      cursor: pointer; }\n  .comment-component-wrapper .post .post-video-attachment {\n    max-width: 100%;\n    width: 100%;\n    min-height: 400px;\n    margin-bottom: 10px;\n    height: 400px; }\n  .comment-component-wrapper .post .action-btns {\n    margin: 0px 10px;\n    padding: 0px;\n    color: #6186b5; }\n  .comment-component-wrapper .post .action-btn {\n    padding: 7px 0px;\n    display: inline-block;\n    margin-right: 5px;\n    cursor: pointer;\n    font-family: waverly;\n    color: #1d99d1;\n    font-size: 15px; }\n  .comment-component-wrapper .post .comment-wrapper {\n    background: #f6f7f8; }\n  .comment-component-wrapper .post input, .comment-component-wrapper .post textarea {\n    background: white;\n    border-radius: 3px;\n    border: 1px solid #ccc;\n    height: 32px;\n    line-height: 28px;\n    width: 100%;\n    padding: 0px  10px;\n    box-shadow: 0px 0px 0px;\n    font-family: helvetica, arial, sans-serif;\n    color: #222;\n    font-size: 14px; }\n    .comment-component-wrapper .post input:focus, .comment-component-wrapper .post textarea:focus {\n      border: 1px solid #1d99d1; }\n  .comment-component-wrapper .post .comment-field {\n    padding: 10px;\n    position: relative;\n    min-height: 50px;\n    margin-left: 0px; }\n    .comment-component-wrapper .post .comment-field .unauthorized {\n      position: absolute;\n      left: 0px;\n      right: 0px;\n      top: 0px;\n      bottom: 0px;\n      background: rgba(0, 0, 0, 0.6);\n      color: white;\n      font-family: waverly;\n      font-size: 20px;\n      z-index: 10;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: center;\n      -moz-box-pack: center;\n      box-pack: center;\n      -webkit-justify-content: center;\n      -moz-justify-content: center;\n      -ms-justify-content: center;\n      -o-justify-content: center;\n      justify-content: center;\n      -ms-flex-pack: center;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; }\n    .comment-component-wrapper .post .comment-field .profile-pic {\n      float: left !important;\n      width: 32px !important;\n      height: 32px !important; }\n    .comment-component-wrapper .post .comment-field .tf {\n      padding-left: 42px;\n      position: relative; }\n      .comment-component-wrapper .post .comment-field .tf .input-comment {\n        background: white;\n        padding-left: 10px;\n        box-shadow: 0px 0px 0px;\n        font-family: 'Aller';\n        display: inline-block;\n        min-height: 32px;\n        line-height: 32px;\n        font-size: 15px;\n        width: calc(100%);\n        border: 1px solid #a0abb5;\n        border-bottom: 0px;\n        border-top-left-radius: 3px;\n        border-top-right-radius: 3px;\n        padding-right: 30px;\n        overflow: auto;\n        word-spacing: 1px; }\n      .comment-component-wrapper .post .comment-field .tf .input-comment.with-full-border {\n        border: 1px solid #a0abb5;\n        border-bottom: 1px solid #a0abb5;\n        border-top-left-radius: 0px;\n        border-top-right-radius: 0px; }\n      .comment-component-wrapper .post .comment-field .tf .input-comment.one-liner {\n        width: calc(100% - 90px); }\n      .comment-component-wrapper .post .comment-field .tf .input-comment:empty:before {\n        content: attr(placeholder);\n        color: #555;\n        float: left;\n        height: 32px;\n        line-height: 32px;\n        overflow: hidden; }\n      .comment-component-wrapper .post .comment-field .tf .input-comment:empty:focus:before {\n        content: \" \";\n        color: #555;\n        float: left;\n        height: 30px;\n        line-height: 30px;\n        overflow: hidden; }\n      .comment-component-wrapper .post .comment-field .tf .one-line-submit-button {\n        float: right; }\n      .comment-component-wrapper .post .comment-field .tf .bottom-tf {\n        position: relative;\n        top: -5px;\n        padding: 7px 14px;\n        background: #d5d5d5;\n        border-bottom-left-radius: 3px;\n        border-bottom-right-radius: 3px;\n        color: #222;\n        border: 1px solid #a0abb5;\n        border-top: 0px;\n        display: none; }\n        .comment-component-wrapper .post .comment-field .tf .bottom-tf.show-bar {\n          display: block; }\n      .comment-component-wrapper .post .comment-field .tf .camera-icon {\n        display: inline-block;\n        right: 86px;\n        line-height: 30px;\n        top: 1px;\n        width: 30px;\n        text-align: center;\n        cursor: pointer;\n        z-index: 10;\n        font-size: 15px; }\n        .comment-component-wrapper .post .comment-field .tf .camera-icon.disable {\n          pointer-events: none;\n          background: #e5e5e5;\n          color: #ccc; }\n      .comment-component-wrapper .post .comment-field .tf .post-comment-btn {\n        padding: 4px 15px;\n        display: inline-block;\n        background: #6186b5;\n        color: white;\n        float: right;\n        border-radius: 3px;\n        cursor: pointer;\n        word-break: break-word;\n        height: 32px;\n        margin-left: 10px;\n        font-size: 15px;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center; }\n        .comment-component-wrapper .post .comment-field .tf .post-comment-btn.disable {\n          pointer-events: none;\n          background: #e5e5e5;\n          color: #ccc; }\n    .comment-component-wrapper .post .comment-field .comment-attachment-wrapper {\n      padding-left: 42px;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row;\n      -webkit-box-lines: multiple;\n      -moz-box-lines: multiple;\n      box-lines: multiple;\n      -webkit-flex-wrap: wrap;\n      -moz-flex-wrap: wrap;\n      -ms-flex-wrap: wrap;\n      flex-wrap: wrap; }\n  .comment-component-wrapper .post .comment {\n    margin: 00px 0px;\n    padding: 0px 10px;\n    padding-bottom: 5px; }\n    .comment-component-wrapper .post .comment .head-wrapper {\n      padding-right: 30px;\n      position: relative;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: start;\n      -moz-box-align: start;\n      box-align: start;\n      -webkit-align-items: flex-start;\n      -moz-align-items: flex-start;\n      -ms-align-items: flex-start;\n      -o-align-items: flex-start;\n      align-items: flex-start;\n      -ms-flex-align: start;\n      -webkit-box-pack: start;\n      -moz-box-pack: start;\n      box-pack: start;\n      -webkit-justify-content: flex-start;\n      -moz-justify-content: flex-start;\n      -ms-justify-content: flex-start;\n      -o-justify-content: flex-start;\n      justify-content: flex-start;\n      -ms-flex-pack: start;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; }\n      .comment-component-wrapper .post .comment .head-wrapper:hover .post-options {\n        display: block; }\n      .comment-component-wrapper .post .comment .head-wrapper .profile-pic {\n        border: 0px;\n        height: auto;\n        width: 32px;\n        margin-top: 5px;\n        margin-right: 10px; }\n        .comment-component-wrapper .post .comment .head-wrapper .profile-pic img {\n          width: 32px;\n          height: 32px;\n          display: inline-block; }\n      .comment-component-wrapper .post .comment .head-wrapper .username {\n        font-size: 12px;\n        color: #365899;\n        cursor: pointer;\n        font-weight: bold;\n        font-family: helvetica, arial, sans-serif;\n        text-transform: capitalize; }\n      .comment-component-wrapper .post .comment .head-wrapper .answer-by-text {\n        color: #222; }\n    .comment-component-wrapper .post .comment .sub-comment-wrapper {\n      margin-left: 40px;\n      border-left: 1px solid #ccc; }\n      .comment-component-wrapper .post .comment .sub-comment-wrapper .post-options {\n        right: 0px; }\n      .comment-component-wrapper .post .comment .sub-comment-wrapper .comment-field {\n        margin-left: 0px; }\n    .comment-component-wrapper .post .comment .head .profile-name {\n      padding-left: 0px; }\n      .comment-component-wrapper .post .comment .head .profile-name .comment, .comment-component-wrapper .post .comment .head .profile-name .comment-field {\n        border-left: 1px solid #a0abb5;\n        padding-bottom: 0px; }\n    .comment-component-wrapper .post .comment .text {\n      font-family: helvetica, arial, sans-serif;\n      font-size: 13px;\n      overflow: hidden;\n      color: #222;\n      white-space: pre-wrap;\n      word-break: break-word; }\n      .comment-component-wrapper .post .comment .text.full-comment {\n        max-height: none;\n        overflow: visible; }\n    .comment-component-wrapper .post .comment .comment-image-attachment {\n      display: block;\n      margin-bottom: 00px; }\n      .comment-component-wrapper .post .comment .comment-image-attachment img {\n        max-width: 300px;\n        height: auto;\n        cursor: pointer; }\n    .comment-component-wrapper .post .comment .action-btns {\n      border-top: 0px solid #f6f7f8;\n      margin: 0px;\n      margin-top: 0px;\n      padding: 0px;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: start;\n      -moz-box-pack: start;\n      box-pack: start;\n      -webkit-justify-content: flex-start;\n      -moz-justify-content: flex-start;\n      -ms-justify-content: flex-start;\n      -o-justify-content: flex-start;\n      justify-content: flex-start;\n      -ms-flex-pack: start; }\n    .comment-component-wrapper .post .comment .action-btn {\n      padding: 0px 0px;\n      padding-right: 0px;\n      font-family: helvetica, arial, sans-serif;\n      font-size: 12px;\n      color: #365899;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center; }\n      .comment-component-wrapper .post .comment .action-btn.disable {\n        color: #bbb;\n        background: #eee;\n        padding: 3px 10px;\n        pointer-events: none; }\n\n.comment-component-wrapper .edit-comment-modal {\n  position: fixed;\n  top: 0px;\n  left: 0px;\n  bottom: 0px;\n  right: 0px;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: auto;\n  z-index: 10000;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: center;\n  -moz-box-pack: center;\n  box-pack: center;\n  -webkit-justify-content: center;\n  -moz-justify-content: center;\n  -ms-justify-content: center;\n  -o-justify-content: center;\n  justify-content: center;\n  -ms-flex-pack: center;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n  .comment-component-wrapper .edit-comment-modal .post {\n    min-width: 500px; }\n  .comment-component-wrapper .edit-comment-modal .content-wrapper {\n    padding: 0px; }\n  .comment-component-wrapper .edit-comment-modal .profile-name {\n    padding-left: 0px; }\n  .comment-component-wrapper .edit-comment-modal .action-btns {\n    -webkit-box-pack: start;\n    -moz-box-pack: start;\n    box-pack: start;\n    -webkit-justify-content: flex-start;\n    -moz-justify-content: flex-start;\n    -ms-justify-content: flex-start;\n    -o-justify-content: flex-start;\n    justify-content: flex-start;\n    -ms-flex-pack: start; }\n\n.comment-component-wrapper .delete-comment-modal-wrapper {\n  position: fixed;\n  top: 0px;\n  left: 0px;\n  bottom: 0px;\n  right: 0px;\n  background: rgba(0, 0, 0, 0.5);\n  overflow: auto;\n  z-index: 9999;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: center;\n  -moz-box-pack: center;\n  box-pack: center;\n  -webkit-justify-content: center;\n  -moz-justify-content: center;\n  -ms-justify-content: center;\n  -o-justify-content: center;\n  justify-content: center;\n  -ms-flex-pack: center;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n  .comment-component-wrapper .delete-comment-modal-wrapper .delete-comment-modal {\n    max-width: 500px;\n    background: white;\n    border-radius: 3px;\n    padding: 15px; }\n  .comment-component-wrapper .delete-comment-modal-wrapper .msg {\n    font-size: 20px;\n    font-family: waverly; }\n  .comment-component-wrapper .delete-comment-modal-wrapper .action-btns {\n    float: right;\n    padding-top: 30px;\n    color: white; }\n    .comment-component-wrapper .delete-comment-modal-wrapper .action-btns .ew-btn {\n      padding: 4px 10px;\n      font-family: aller;\n      font-size: 15px;\n      display: inline-block;\n      margin: 0px 2px;\n      background: #1d99d1;\n      border-radius: 2px;\n      cursor: pointer; }\n\n.comment-component-wrapper .show-more-comment-link {\n  color: #1778a4;\n  cursor: pointer;\n  padding: 0px 10px; }\n", ""]);

	// exports


/***/ },

/***/ 238:
/***/ function(module, exports, __webpack_require__) {

	window.Drop = __webpack_require__(239);
	__webpack_require__(241);
	__webpack_require__(243);

	var app = angular
	    .module("directive");

	app.directive('myDrop', ['dropWrapper', function($drop) {
	    return {
	        restrict: 'EA',
	        scope: {
	            elem: '=myDrop',
	            fn: '=callback',
	            content : '='
	        },
	        link: function(scope, elem) {
	        	scope.$watch(function(){
	        		return scope.content;
	        	}, function(newValue, oldValue, scope) {
	        		if(newValue){
	        			createDrop();	
	        		}
	        	});

	        	function createDrop(){
	        		if(scope.drop){
	        			scope.drop.destroy();
	        		}
	        		var drop = $drop({
		                target: elem,
		                scope: scope,
		                template: scope.content,
		                position: 'top center',
		                constrainToWindow: true,
		                constrainToScrollParent: true,
		                classes: 'drop-theme-arrows-bounce-dark',
	                    blurDelay : 1000,
		                tetherOptions: {},
		            });

		            scope.drop = drop;
		            scope.drop.open();
		            scope.custom_close = function(text) {
	                    scope.fn(text);
	                    scope.drop.close();
	                }
	                    // easy way
		                // elem.on('click', drop.toogle);
		                // hard way
		            elem.on('click', function(event) {
	                    console.log(event);
		                if (scope.drop.isOpened())
		                    scope.drop.close();
		                else
		                    scope.drop.open();
		            });
	        	}

	        }
	    };
	}]);

/***/ },

/***/ 239:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! tether-drop 1.4.1 */

	(function(root, factory) {
	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(240)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (typeof exports === 'object') {
	    module.exports = factory(require('tether'));
	  } else {
	    root.Drop = factory(root.Tether);
	  }
	}(this, function(Tether) {

	/* global Tether */
	'use strict';

	var _bind = Function.prototype.bind;

	var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	var _get = function get(_x2, _x3, _x4) { var _again = true; _function: while (_again) { var object = _x2, property = _x3, receiver = _x4; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x2 = parent; _x3 = property; _x4 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var _Tether$Utils = Tether.Utils;
	var extend = _Tether$Utils.extend;
	var addClass = _Tether$Utils.addClass;
	var removeClass = _Tether$Utils.removeClass;
	var hasClass = _Tether$Utils.hasClass;
	var Evented = _Tether$Utils.Evented;

	function sortAttach(str) {
	  var _str$split = str.split(' ');

	  var _str$split2 = _slicedToArray(_str$split, 2);

	  var first = _str$split2[0];
	  var second = _str$split2[1];

	  if (['left', 'right'].indexOf(first) >= 0) {
	    var _ref = [second, first];
	    first = _ref[0];
	    second = _ref[1];
	  }
	  return [first, second].join(' ');
	}

	function removeFromArray(arr, item) {
	  var index = undefined;
	  var results = [];
	  while ((index = arr.indexOf(item)) !== -1) {
	    results.push(arr.splice(index, 1));
	  }
	  return results;
	}

	var clickEvents = ['click'];
	if ('ontouchstart' in document.documentElement) {
	  clickEvents.push('touchstart');
	}

	var transitionEndEvents = {
	  'WebkitTransition': 'webkitTransitionEnd',
	  'MozTransition': 'transitionend',
	  'OTransition': 'otransitionend',
	  'transition': 'transitionend'
	};

	var transitionEndEvent = '';
	for (var _name in transitionEndEvents) {
	  if (({}).hasOwnProperty.call(transitionEndEvents, _name)) {
	    var tempEl = document.createElement('p');
	    if (typeof tempEl.style[_name] !== 'undefined') {
	      transitionEndEvent = transitionEndEvents[_name];
	    }
	  }
	}

	var MIRROR_ATTACH = {
	  left: 'right',
	  right: 'left',
	  top: 'bottom',
	  bottom: 'top',
	  middle: 'middle',
	  center: 'center'
	};

	var allDrops = {};

	// Drop can be included in external libraries.  Calling createContext gives you a fresh
	// copy of drop which won't interact with other copies on the page (beyond calling the document events).

	function createContext() {
	  var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

	  var drop = function drop() {
	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }

	    return new (_bind.apply(DropInstance, [null].concat(args)))();
	  };

	  extend(drop, {
	    createContext: createContext,
	    drops: [],
	    defaults: {}
	  });

	  var defaultOptions = {
	    classPrefix: 'drop',
	    defaults: {
	      position: 'bottom left',
	      openOn: 'click',
	      beforeClose: null,
	      constrainToScrollParent: true,
	      constrainToWindow: true,
	      classes: '',
	      remove: false,
	      openDelay: 0,
	      closeDelay: 50,
	      // inherited from openDelay and closeDelay if not explicitly defined
	      focusDelay: null,
	      blurDelay: null,
	      hoverOpenDelay: null,
	      hoverCloseDelay: null,
	      tetherOptions: {}
	    }
	  };

	  extend(drop, defaultOptions, options);
	  extend(drop.defaults, defaultOptions.defaults, options.defaults);

	  if (typeof allDrops[drop.classPrefix] === 'undefined') {
	    allDrops[drop.classPrefix] = [];
	  }

	  drop.updateBodyClasses = function () {
	    // There is only one body, so despite the context concept, we still iterate through all
	    // drops which share our classPrefix.

	    var anyOpen = false;
	    var drops = allDrops[drop.classPrefix];
	    var len = drops.length;
	    for (var i = 0; i < len; ++i) {
	      if (drops[i].isOpened()) {
	        anyOpen = true;
	        break;
	      }
	    }

	    if (anyOpen) {
	      addClass(document.body, drop.classPrefix + '-open');
	    } else {
	      removeClass(document.body, drop.classPrefix + '-open');
	    }
	  };

	  var DropInstance = (function (_Evented) {
	    _inherits(DropInstance, _Evented);

	    function DropInstance(opts) {
	      _classCallCheck(this, DropInstance);

	      _get(Object.getPrototypeOf(DropInstance.prototype), 'constructor', this).call(this);
	      this.options = extend({}, drop.defaults, opts);
	      this.target = this.options.target;

	      if (typeof this.target === 'undefined') {
	        throw new Error('Drop Error: You must provide a target.');
	      }

	      var dataPrefix = 'data-' + drop.classPrefix;

	      var contentAttr = this.target.getAttribute(dataPrefix);
	      if (contentAttr && this.options.content == null) {
	        this.options.content = contentAttr;
	      }

	      var attrsOverride = ['position', 'openOn'];
	      for (var i = 0; i < attrsOverride.length; ++i) {

	        var override = this.target.getAttribute(dataPrefix + '-' + attrsOverride[i]);
	        if (override && this.options[attrsOverride[i]] == null) {
	          this.options[attrsOverride[i]] = override;
	        }
	      }

	      if (this.options.classes && this.options.addTargetClasses !== false) {
	        addClass(this.target, this.options.classes);
	      }

	      drop.drops.push(this);
	      allDrops[drop.classPrefix].push(this);

	      this._boundEvents = [];
	      this.bindMethods();
	      this.setupElements();
	      this.setupEvents();
	      this.setupTether();
	    }

	    _createClass(DropInstance, [{
	      key: '_on',
	      value: function _on(element, event, handler) {
	        this._boundEvents.push({ element: element, event: event, handler: handler });
	        element.addEventListener(event, handler);
	      }
	    }, {
	      key: 'bindMethods',
	      value: function bindMethods() {
	        this.transitionEndHandler = this._transitionEndHandler.bind(this);
	      }
	    }, {
	      key: 'setupElements',
	      value: function setupElements() {
	        var _this = this;

	        this.drop = document.createElement('div');
	        addClass(this.drop, drop.classPrefix);

	        if (this.options.classes) {
	          addClass(this.drop, this.options.classes);
	        }

	        this.content = document.createElement('div');
	        addClass(this.content, drop.classPrefix + '-content');

	        if (typeof this.options.content === 'function') {
	          var generateAndSetContent = function generateAndSetContent() {
	            // content function might return a string or an element
	            var contentElementOrHTML = _this.options.content.call(_this, _this);

	            if (typeof contentElementOrHTML === 'string') {
	              _this.content.innerHTML = contentElementOrHTML;
	            } else if (typeof contentElementOrHTML === 'object') {
	              _this.content.innerHTML = '';
	              _this.content.appendChild(contentElementOrHTML);
	            } else {
	              throw new Error('Drop Error: Content function should return a string or HTMLElement.');
	            }
	          };

	          generateAndSetContent();
	          this.on('open', generateAndSetContent.bind(this));
	        } else if (typeof this.options.content === 'object') {
	          this.content.appendChild(this.options.content);
	        } else {
	          this.content.innerHTML = this.options.content;
	        }

	        this.drop.appendChild(this.content);
	      }
	    }, {
	      key: 'setupTether',
	      value: function setupTether() {
	        // Tether expects two attachment points, one in the target element, one in the
	        // drop.  We use a single one, and use the order as well, to allow us to put
	        // the drop on either side of any of the four corners.  This magic converts between
	        // the two:
	        var dropAttach = this.options.position.split(' ');
	        dropAttach[0] = MIRROR_ATTACH[dropAttach[0]];
	        dropAttach = dropAttach.join(' ');

	        var constraints = [];
	        if (this.options.constrainToScrollParent) {
	          constraints.push({
	            to: 'scrollParent',
	            pin: 'top, bottom',
	            attachment: 'together none'
	          });
	        } else {
	          // To get 'out of bounds' classes
	          constraints.push({
	            to: 'scrollParent'
	          });
	        }

	        if (this.options.constrainToWindow !== false) {
	          constraints.push({
	            to: 'window',
	            attachment: 'together'
	          });
	        } else {
	          // To get 'out of bounds' classes
	          constraints.push({
	            to: 'window'
	          });
	        }

	        var opts = {
	          element: this.drop,
	          target: this.target,
	          attachment: sortAttach(dropAttach),
	          targetAttachment: sortAttach(this.options.position),
	          classPrefix: drop.classPrefix,
	          offset: '0 0',
	          targetOffset: '0 0',
	          enabled: false,
	          constraints: constraints,
	          addTargetClasses: this.options.addTargetClasses
	        };

	        if (this.options.tetherOptions !== false) {
	          this.tether = new Tether(extend({}, opts, this.options.tetherOptions));
	        }
	      }
	    }, {
	      key: 'setupEvents',
	      value: function setupEvents() {
	        var _this2 = this;

	        if (!this.options.openOn) {
	          return;
	        }

	        if (this.options.openOn === 'always') {
	          setTimeout(this.open.bind(this));
	          return;
	        }

	        var events = this.options.openOn.split(' ');

	        if (events.indexOf('click') >= 0) {
	          var openHandler = function openHandler(event) {
	            _this2.toggle(event);
	            event.preventDefault();
	          };

	          var closeHandler = function closeHandler(event) {
	            if (!_this2.isOpened()) {
	              return;
	            }

	            // Clicking inside dropdown
	            if (event.target === _this2.drop || _this2.drop.contains(event.target)) {
	              return;
	            }

	            // Clicking target
	            if (event.target === _this2.target || _this2.target.contains(event.target)) {
	              return;
	            }

	            _this2.close(event);
	          };

	          for (var i = 0; i < clickEvents.length; ++i) {
	            var clickEvent = clickEvents[i];
	            this._on(this.target, clickEvent, openHandler);
	            this._on(document, clickEvent, closeHandler);
	          }
	        }

	        var inTimeout = null;
	        var outTimeout = null;

	        var inHandler = function inHandler(event) {
	          if (outTimeout !== null) {
	            clearTimeout(outTimeout);
	          } else {
	            inTimeout = setTimeout(function () {
	              _this2.open(event);
	              inTimeout = null;
	            }, (event.type === 'focus' ? _this2.options.focusDelay : _this2.options.hoverOpenDelay) || _this2.options.openDelay);
	          }
	        };

	        var outHandler = function outHandler(event) {
	          if (inTimeout !== null) {
	            clearTimeout(inTimeout);
	          } else {
	            outTimeout = setTimeout(function () {
	              _this2.close(event);
	              outTimeout = null;
	            }, (event.type === 'blur' ? _this2.options.blurDelay : _this2.options.hoverCloseDelay) || _this2.options.closeDelay);
	          }
	        };

	        if (events.indexOf('hover') >= 0) {
	          this._on(this.target, 'mouseover', inHandler);
	          this._on(this.drop, 'mouseover', inHandler);
	          this._on(this.target, 'mouseout', outHandler);
	          this._on(this.drop, 'mouseout', outHandler);
	        }

	        if (events.indexOf('focus') >= 0) {
	          this._on(this.target, 'focus', inHandler);
	          this._on(this.drop, 'focus', inHandler);
	          this._on(this.target, 'blur', outHandler);
	          this._on(this.drop, 'blur', outHandler);
	        }
	      }
	    }, {
	      key: 'isOpened',
	      value: function isOpened() {
	        if (this.drop) {
	          return hasClass(this.drop, drop.classPrefix + '-open');
	        }
	      }
	    }, {
	      key: 'toggle',
	      value: function toggle(event) {
	        if (this.isOpened()) {
	          this.close(event);
	        } else {
	          this.open(event);
	        }
	      }
	    }, {
	      key: 'open',
	      value: function open(event) {
	        var _this3 = this;

	        /* eslint no-unused-vars: 0 */
	        if (this.isOpened()) {
	          return;
	        }

	        if (!this.drop.parentNode) {
	          document.body.appendChild(this.drop);
	        }

	        if (typeof this.tether !== 'undefined') {
	          this.tether.enable();
	        }

	        addClass(this.drop, drop.classPrefix + '-open');
	        addClass(this.drop, drop.classPrefix + '-open-transitionend');

	        setTimeout(function () {
	          if (_this3.drop) {
	            addClass(_this3.drop, drop.classPrefix + '-after-open');
	          }
	        });

	        if (typeof this.tether !== 'undefined') {
	          this.tether.position();
	        }

	        this.trigger('open');

	        drop.updateBodyClasses();
	      }
	    }, {
	      key: '_transitionEndHandler',
	      value: function _transitionEndHandler(e) {
	        if (e.target !== e.currentTarget) {
	          return;
	        }

	        if (!hasClass(this.drop, drop.classPrefix + '-open')) {
	          removeClass(this.drop, drop.classPrefix + '-open-transitionend');
	        }
	        this.drop.removeEventListener(transitionEndEvent, this.transitionEndHandler);
	      }
	    }, {
	      key: 'beforeCloseHandler',
	      value: function beforeCloseHandler(event) {
	        var shouldClose = true;

	        if (!this.isClosing && typeof this.options.beforeClose === 'function') {
	          this.isClosing = true;
	          shouldClose = this.options.beforeClose(event, this) !== false;
	        }

	        this.isClosing = false;

	        return shouldClose;
	      }
	    }, {
	      key: 'close',
	      value: function close(event) {
	        if (!this.isOpened()) {
	          return;
	        }

	        if (!this.beforeCloseHandler(event)) {
	          return;
	        }

	        removeClass(this.drop, drop.classPrefix + '-open');
	        removeClass(this.drop, drop.classPrefix + '-after-open');

	        this.drop.addEventListener(transitionEndEvent, this.transitionEndHandler);

	        this.trigger('close');

	        if (typeof this.tether !== 'undefined') {
	          this.tether.disable();
	        }

	        drop.updateBodyClasses();

	        if (this.options.remove) {
	          this.remove(event);
	        }
	      }
	    }, {
	      key: 'remove',
	      value: function remove(event) {
	        this.close(event);
	        if (this.drop.parentNode) {
	          this.drop.parentNode.removeChild(this.drop);
	        }
	      }
	    }, {
	      key: 'position',
	      value: function position() {
	        if (this.isOpened() && typeof this.tether !== 'undefined') {
	          this.tether.position();
	        }
	      }
	    }, {
	      key: 'destroy',
	      value: function destroy() {
	        this.remove();

	        if (typeof this.tether !== 'undefined') {
	          this.tether.destroy();
	        }

	        for (var i = 0; i < this._boundEvents.length; ++i) {
	          var _boundEvents$i = this._boundEvents[i];
	          var element = _boundEvents$i.element;
	          var _event = _boundEvents$i.event;
	          var handler = _boundEvents$i.handler;

	          element.removeEventListener(_event, handler);
	        }

	        this._boundEvents = [];

	        this.tether = null;
	        this.drop = null;
	        this.content = null;
	        this.target = null;

	        removeFromArray(allDrops[drop.classPrefix], this);
	        removeFromArray(drop.drops, this);
	      }
	    }]);

	    return DropInstance;
	  })(Evented);

	  return drop;
	}

	var Drop = createContext();

	document.addEventListener('DOMContentLoaded', function () {
	  Drop.updateBodyClasses();
	});
	return Drop;

	}));


/***/ },

/***/ 240:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! tether 1.4.0 */

	(function(root, factory) {
	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (typeof exports === 'object') {
	    module.exports = factory(require, exports, module);
	  } else {
	    root.Tether = factory();
	  }
	}(this, function(require, exports, module) {

	'use strict';

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	var TetherBase = undefined;
	if (typeof TetherBase === 'undefined') {
	  TetherBase = { modules: [] };
	}

	var zeroElement = null;

	// Same as native getBoundingClientRect, except it takes into account parent <frame> offsets
	// if the element lies within a nested document (<frame> or <iframe>-like).
	function getActualBoundingClientRect(node) {
	  var boundingRect = node.getBoundingClientRect();

	  // The original object returned by getBoundingClientRect is immutable, so we clone it
	  // We can't use extend because the properties are not considered part of the object by hasOwnProperty in IE9
	  var rect = {};
	  for (var k in boundingRect) {
	    rect[k] = boundingRect[k];
	  }

	  if (node.ownerDocument !== document) {
	    var _frameElement = node.ownerDocument.defaultView.frameElement;
	    if (_frameElement) {
	      var frameRect = getActualBoundingClientRect(_frameElement);
	      rect.top += frameRect.top;
	      rect.bottom += frameRect.top;
	      rect.left += frameRect.left;
	      rect.right += frameRect.left;
	    }
	  }

	  return rect;
	}

	function getScrollParents(el) {
	  // In firefox if the el is inside an iframe with display: none; window.getComputedStyle() will return null;
	  // https://bugzilla.mozilla.org/show_bug.cgi?id=548397
	  var computedStyle = getComputedStyle(el) || {};
	  var position = computedStyle.position;
	  var parents = [];

	  if (position === 'fixed') {
	    return [el];
	  }

	  var parent = el;
	  while ((parent = parent.parentNode) && parent && parent.nodeType === 1) {
	    var style = undefined;
	    try {
	      style = getComputedStyle(parent);
	    } catch (err) {}

	    if (typeof style === 'undefined' || style === null) {
	      parents.push(parent);
	      return parents;
	    }

	    var _style = style;
	    var overflow = _style.overflow;
	    var overflowX = _style.overflowX;
	    var overflowY = _style.overflowY;

	    if (/(auto|scroll)/.test(overflow + overflowY + overflowX)) {
	      if (position !== 'absolute' || ['relative', 'absolute', 'fixed'].indexOf(style.position) >= 0) {
	        parents.push(parent);
	      }
	    }
	  }

	  parents.push(el.ownerDocument.body);

	  // If the node is within a frame, account for the parent window scroll
	  if (el.ownerDocument !== document) {
	    parents.push(el.ownerDocument.defaultView);
	  }

	  return parents;
	}

	var uniqueId = (function () {
	  var id = 0;
	  return function () {
	    return ++id;
	  };
	})();

	var zeroPosCache = {};
	var getOrigin = function getOrigin() {
	  // getBoundingClientRect is unfortunately too accurate.  It introduces a pixel or two of
	  // jitter as the user scrolls that messes with our ability to detect if two positions
	  // are equivilant or not.  We place an element at the top left of the page that will
	  // get the same jitter, so we can cancel the two out.
	  var node = zeroElement;
	  if (!node || !document.body.contains(node)) {
	    node = document.createElement('div');
	    node.setAttribute('data-tether-id', uniqueId());
	    extend(node.style, {
	      top: 0,
	      left: 0,
	      position: 'absolute'
	    });

	    document.body.appendChild(node);

	    zeroElement = node;
	  }

	  var id = node.getAttribute('data-tether-id');
	  if (typeof zeroPosCache[id] === 'undefined') {
	    zeroPosCache[id] = getActualBoundingClientRect(node);

	    // Clear the cache when this position call is done
	    defer(function () {
	      delete zeroPosCache[id];
	    });
	  }

	  return zeroPosCache[id];
	};

	function removeUtilElements() {
	  if (zeroElement) {
	    document.body.removeChild(zeroElement);
	  }
	  zeroElement = null;
	};

	function getBounds(el) {
	  var doc = undefined;
	  if (el === document) {
	    doc = document;
	    el = document.documentElement;
	  } else {
	    doc = el.ownerDocument;
	  }

	  var docEl = doc.documentElement;

	  var box = getActualBoundingClientRect(el);

	  var origin = getOrigin();

	  box.top -= origin.top;
	  box.left -= origin.left;

	  if (typeof box.width === 'undefined') {
	    box.width = document.body.scrollWidth - box.left - box.right;
	  }
	  if (typeof box.height === 'undefined') {
	    box.height = document.body.scrollHeight - box.top - box.bottom;
	  }

	  box.top = box.top - docEl.clientTop;
	  box.left = box.left - docEl.clientLeft;
	  box.right = doc.body.clientWidth - box.width - box.left;
	  box.bottom = doc.body.clientHeight - box.height - box.top;

	  return box;
	}

	function getOffsetParent(el) {
	  return el.offsetParent || document.documentElement;
	}

	var _scrollBarSize = null;
	function getScrollBarSize() {
	  if (_scrollBarSize) {
	    return _scrollBarSize;
	  }
	  var inner = document.createElement('div');
	  inner.style.width = '100%';
	  inner.style.height = '200px';

	  var outer = document.createElement('div');
	  extend(outer.style, {
	    position: 'absolute',
	    top: 0,
	    left: 0,
	    pointerEvents: 'none',
	    visibility: 'hidden',
	    width: '200px',
	    height: '150px',
	    overflow: 'hidden'
	  });

	  outer.appendChild(inner);

	  document.body.appendChild(outer);

	  var widthContained = inner.offsetWidth;
	  outer.style.overflow = 'scroll';
	  var widthScroll = inner.offsetWidth;

	  if (widthContained === widthScroll) {
	    widthScroll = outer.clientWidth;
	  }

	  document.body.removeChild(outer);

	  var width = widthContained - widthScroll;

	  _scrollBarSize = { width: width, height: width };
	  return _scrollBarSize;
	}

	function extend() {
	  var out = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

	  var args = [];

	  Array.prototype.push.apply(args, arguments);

	  args.slice(1).forEach(function (obj) {
	    if (obj) {
	      for (var key in obj) {
	        if (({}).hasOwnProperty.call(obj, key)) {
	          out[key] = obj[key];
	        }
	      }
	    }
	  });

	  return out;
	}

	function removeClass(el, name) {
	  if (typeof el.classList !== 'undefined') {
	    name.split(' ').forEach(function (cls) {
	      if (cls.trim()) {
	        el.classList.remove(cls);
	      }
	    });
	  } else {
	    var regex = new RegExp('(^| )' + name.split(' ').join('|') + '( |$)', 'gi');
	    var className = getClassName(el).replace(regex, ' ');
	    setClassName(el, className);
	  }
	}

	function addClass(el, name) {
	  if (typeof el.classList !== 'undefined') {
	    name.split(' ').forEach(function (cls) {
	      if (cls.trim()) {
	        el.classList.add(cls);
	      }
	    });
	  } else {
	    removeClass(el, name);
	    var cls = getClassName(el) + (' ' + name);
	    setClassName(el, cls);
	  }
	}

	function hasClass(el, name) {
	  if (typeof el.classList !== 'undefined') {
	    return el.classList.contains(name);
	  }
	  var className = getClassName(el);
	  return new RegExp('(^| )' + name + '( |$)', 'gi').test(className);
	}

	function getClassName(el) {
	  // Can't use just SVGAnimatedString here since nodes within a Frame in IE have
	  // completely separately SVGAnimatedString base classes
	  if (el.className instanceof el.ownerDocument.defaultView.SVGAnimatedString) {
	    return el.className.baseVal;
	  }
	  return el.className;
	}

	function setClassName(el, className) {
	  el.setAttribute('class', className);
	}

	function updateClasses(el, add, all) {
	  // Of the set of 'all' classes, we need the 'add' classes, and only the
	  // 'add' classes to be set.
	  all.forEach(function (cls) {
	    if (add.indexOf(cls) === -1 && hasClass(el, cls)) {
	      removeClass(el, cls);
	    }
	  });

	  add.forEach(function (cls) {
	    if (!hasClass(el, cls)) {
	      addClass(el, cls);
	    }
	  });
	}

	var deferred = [];

	var defer = function defer(fn) {
	  deferred.push(fn);
	};

	var flush = function flush() {
	  var fn = undefined;
	  while (fn = deferred.pop()) {
	    fn();
	  }
	};

	var Evented = (function () {
	  function Evented() {
	    _classCallCheck(this, Evented);
	  }

	  _createClass(Evented, [{
	    key: 'on',
	    value: function on(event, handler, ctx) {
	      var once = arguments.length <= 3 || arguments[3] === undefined ? false : arguments[3];

	      if (typeof this.bindings === 'undefined') {
	        this.bindings = {};
	      }
	      if (typeof this.bindings[event] === 'undefined') {
	        this.bindings[event] = [];
	      }
	      this.bindings[event].push({ handler: handler, ctx: ctx, once: once });
	    }
	  }, {
	    key: 'once',
	    value: function once(event, handler, ctx) {
	      this.on(event, handler, ctx, true);
	    }
	  }, {
	    key: 'off',
	    value: function off(event, handler) {
	      if (typeof this.bindings === 'undefined' || typeof this.bindings[event] === 'undefined') {
	        return;
	      }

	      if (typeof handler === 'undefined') {
	        delete this.bindings[event];
	      } else {
	        var i = 0;
	        while (i < this.bindings[event].length) {
	          if (this.bindings[event][i].handler === handler) {
	            this.bindings[event].splice(i, 1);
	          } else {
	            ++i;
	          }
	        }
	      }
	    }
	  }, {
	    key: 'trigger',
	    value: function trigger(event) {
	      if (typeof this.bindings !== 'undefined' && this.bindings[event]) {
	        var i = 0;

	        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	          args[_key - 1] = arguments[_key];
	        }

	        while (i < this.bindings[event].length) {
	          var _bindings$event$i = this.bindings[event][i];
	          var handler = _bindings$event$i.handler;
	          var ctx = _bindings$event$i.ctx;
	          var once = _bindings$event$i.once;

	          var context = ctx;
	          if (typeof context === 'undefined') {
	            context = this;
	          }

	          handler.apply(context, args);

	          if (once) {
	            this.bindings[event].splice(i, 1);
	          } else {
	            ++i;
	          }
	        }
	      }
	    }
	  }]);

	  return Evented;
	})();

	TetherBase.Utils = {
	  getActualBoundingClientRect: getActualBoundingClientRect,
	  getScrollParents: getScrollParents,
	  getBounds: getBounds,
	  getOffsetParent: getOffsetParent,
	  extend: extend,
	  addClass: addClass,
	  removeClass: removeClass,
	  hasClass: hasClass,
	  updateClasses: updateClasses,
	  defer: defer,
	  flush: flush,
	  uniqueId: uniqueId,
	  Evented: Evented,
	  getScrollBarSize: getScrollBarSize,
	  removeUtilElements: removeUtilElements
	};
	/* globals TetherBase, performance */

	'use strict';

	var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	var _get = function get(_x6, _x7, _x8) { var _again = true; _function: while (_again) { var object = _x6, property = _x7, receiver = _x8; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x6 = parent; _x7 = property; _x8 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	if (typeof TetherBase === 'undefined') {
	  throw new Error('You must include the utils.js file before tether.js');
	}

	var _TetherBase$Utils = TetherBase.Utils;
	var getScrollParents = _TetherBase$Utils.getScrollParents;
	var getBounds = _TetherBase$Utils.getBounds;
	var getOffsetParent = _TetherBase$Utils.getOffsetParent;
	var extend = _TetherBase$Utils.extend;
	var addClass = _TetherBase$Utils.addClass;
	var removeClass = _TetherBase$Utils.removeClass;
	var updateClasses = _TetherBase$Utils.updateClasses;
	var defer = _TetherBase$Utils.defer;
	var flush = _TetherBase$Utils.flush;
	var getScrollBarSize = _TetherBase$Utils.getScrollBarSize;
	var removeUtilElements = _TetherBase$Utils.removeUtilElements;

	function within(a, b) {
	  var diff = arguments.length <= 2 || arguments[2] === undefined ? 1 : arguments[2];

	  return a + diff >= b && b >= a - diff;
	}

	var transformKey = (function () {
	  if (typeof document === 'undefined') {
	    return '';
	  }
	  var el = document.createElement('div');

	  var transforms = ['transform', 'WebkitTransform', 'OTransform', 'MozTransform', 'msTransform'];
	  for (var i = 0; i < transforms.length; ++i) {
	    var key = transforms[i];
	    if (el.style[key] !== undefined) {
	      return key;
	    }
	  }
	})();

	var tethers = [];

	var position = function position() {
	  tethers.forEach(function (tether) {
	    tether.position(false);
	  });
	  flush();
	};

	function now() {
	  if (typeof performance !== 'undefined' && typeof performance.now !== 'undefined') {
	    return performance.now();
	  }
	  return +new Date();
	}

	(function () {
	  var lastCall = null;
	  var lastDuration = null;
	  var pendingTimeout = null;

	  var tick = function tick() {
	    if (typeof lastDuration !== 'undefined' && lastDuration > 16) {
	      // We voluntarily throttle ourselves if we can't manage 60fps
	      lastDuration = Math.min(lastDuration - 16, 250);

	      // Just in case this is the last event, remember to position just once more
	      pendingTimeout = setTimeout(tick, 250);
	      return;
	    }

	    if (typeof lastCall !== 'undefined' && now() - lastCall < 10) {
	      // Some browsers call events a little too frequently, refuse to run more than is reasonable
	      return;
	    }

	    if (pendingTimeout != null) {
	      clearTimeout(pendingTimeout);
	      pendingTimeout = null;
	    }

	    lastCall = now();
	    position();
	    lastDuration = now() - lastCall;
	  };

	  if (typeof window !== 'undefined' && typeof window.addEventListener !== 'undefined') {
	    ['resize', 'scroll', 'touchmove'].forEach(function (event) {
	      window.addEventListener(event, tick);
	    });
	  }
	})();

	var MIRROR_LR = {
	  center: 'center',
	  left: 'right',
	  right: 'left'
	};

	var MIRROR_TB = {
	  middle: 'middle',
	  top: 'bottom',
	  bottom: 'top'
	};

	var OFFSET_MAP = {
	  top: 0,
	  left: 0,
	  middle: '50%',
	  center: '50%',
	  bottom: '100%',
	  right: '100%'
	};

	var autoToFixedAttachment = function autoToFixedAttachment(attachment, relativeToAttachment) {
	  var left = attachment.left;
	  var top = attachment.top;

	  if (left === 'auto') {
	    left = MIRROR_LR[relativeToAttachment.left];
	  }

	  if (top === 'auto') {
	    top = MIRROR_TB[relativeToAttachment.top];
	  }

	  return { left: left, top: top };
	};

	var attachmentToOffset = function attachmentToOffset(attachment) {
	  var left = attachment.left;
	  var top = attachment.top;

	  if (typeof OFFSET_MAP[attachment.left] !== 'undefined') {
	    left = OFFSET_MAP[attachment.left];
	  }

	  if (typeof OFFSET_MAP[attachment.top] !== 'undefined') {
	    top = OFFSET_MAP[attachment.top];
	  }

	  return { left: left, top: top };
	};

	function addOffset() {
	  var out = { top: 0, left: 0 };

	  for (var _len = arguments.length, offsets = Array(_len), _key = 0; _key < _len; _key++) {
	    offsets[_key] = arguments[_key];
	  }

	  offsets.forEach(function (_ref) {
	    var top = _ref.top;
	    var left = _ref.left;

	    if (typeof top === 'string') {
	      top = parseFloat(top, 10);
	    }
	    if (typeof left === 'string') {
	      left = parseFloat(left, 10);
	    }

	    out.top += top;
	    out.left += left;
	  });

	  return out;
	}

	function offsetToPx(offset, size) {
	  if (typeof offset.left === 'string' && offset.left.indexOf('%') !== -1) {
	    offset.left = parseFloat(offset.left, 10) / 100 * size.width;
	  }
	  if (typeof offset.top === 'string' && offset.top.indexOf('%') !== -1) {
	    offset.top = parseFloat(offset.top, 10) / 100 * size.height;
	  }

	  return offset;
	}

	var parseOffset = function parseOffset(value) {
	  var _value$split = value.split(' ');

	  var _value$split2 = _slicedToArray(_value$split, 2);

	  var top = _value$split2[0];
	  var left = _value$split2[1];

	  return { top: top, left: left };
	};
	var parseAttachment = parseOffset;

	var TetherClass = (function (_Evented) {
	  _inherits(TetherClass, _Evented);

	  function TetherClass(options) {
	    var _this = this;

	    _classCallCheck(this, TetherClass);

	    _get(Object.getPrototypeOf(TetherClass.prototype), 'constructor', this).call(this);
	    this.position = this.position.bind(this);

	    tethers.push(this);

	    this.history = [];

	    this.setOptions(options, false);

	    TetherBase.modules.forEach(function (module) {
	      if (typeof module.initialize !== 'undefined') {
	        module.initialize.call(_this);
	      }
	    });

	    this.position();
	  }

	  _createClass(TetherClass, [{
	    key: 'getClass',
	    value: function getClass() {
	      var key = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];
	      var classes = this.options.classes;

	      if (typeof classes !== 'undefined' && classes[key]) {
	        return this.options.classes[key];
	      } else if (this.options.classPrefix) {
	        return this.options.classPrefix + '-' + key;
	      } else {
	        return key;
	      }
	    }
	  }, {
	    key: 'setOptions',
	    value: function setOptions(options) {
	      var _this2 = this;

	      var pos = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

	      var defaults = {
	        offset: '0 0',
	        targetOffset: '0 0',
	        targetAttachment: 'auto auto',
	        classPrefix: 'tether'
	      };

	      this.options = extend(defaults, options);

	      var _options = this.options;
	      var element = _options.element;
	      var target = _options.target;
	      var targetModifier = _options.targetModifier;

	      this.element = element;
	      this.target = target;
	      this.targetModifier = targetModifier;

	      if (this.target === 'viewport') {
	        this.target = document.body;
	        this.targetModifier = 'visible';
	      } else if (this.target === 'scroll-handle') {
	        this.target = document.body;
	        this.targetModifier = 'scroll-handle';
	      }

	      ['element', 'target'].forEach(function (key) {
	        if (typeof _this2[key] === 'undefined') {
	          throw new Error('Tether Error: Both element and target must be defined');
	        }

	        if (typeof _this2[key].jquery !== 'undefined') {
	          _this2[key] = _this2[key][0];
	        } else if (typeof _this2[key] === 'string') {
	          _this2[key] = document.querySelector(_this2[key]);
	        }
	      });

	      addClass(this.element, this.getClass('element'));
	      if (!(this.options.addTargetClasses === false)) {
	        addClass(this.target, this.getClass('target'));
	      }

	      if (!this.options.attachment) {
	        throw new Error('Tether Error: You must provide an attachment');
	      }

	      this.targetAttachment = parseAttachment(this.options.targetAttachment);
	      this.attachment = parseAttachment(this.options.attachment);
	      this.offset = parseOffset(this.options.offset);
	      this.targetOffset = parseOffset(this.options.targetOffset);

	      if (typeof this.scrollParents !== 'undefined') {
	        this.disable();
	      }

	      if (this.targetModifier === 'scroll-handle') {
	        this.scrollParents = [this.target];
	      } else {
	        this.scrollParents = getScrollParents(this.target);
	      }

	      if (!(this.options.enabled === false)) {
	        this.enable(pos);
	      }
	    }
	  }, {
	    key: 'getTargetBounds',
	    value: function getTargetBounds() {
	      if (typeof this.targetModifier !== 'undefined') {
	        if (this.targetModifier === 'visible') {
	          if (this.target === document.body) {
	            return { top: pageYOffset, left: pageXOffset, height: innerHeight, width: innerWidth };
	          } else {
	            var bounds = getBounds(this.target);

	            var out = {
	              height: bounds.height,
	              width: bounds.width,
	              top: bounds.top,
	              left: bounds.left
	            };

	            out.height = Math.min(out.height, bounds.height - (pageYOffset - bounds.top));
	            out.height = Math.min(out.height, bounds.height - (bounds.top + bounds.height - (pageYOffset + innerHeight)));
	            out.height = Math.min(innerHeight, out.height);
	            out.height -= 2;

	            out.width = Math.min(out.width, bounds.width - (pageXOffset - bounds.left));
	            out.width = Math.min(out.width, bounds.width - (bounds.left + bounds.width - (pageXOffset + innerWidth)));
	            out.width = Math.min(innerWidth, out.width);
	            out.width -= 2;

	            if (out.top < pageYOffset) {
	              out.top = pageYOffset;
	            }
	            if (out.left < pageXOffset) {
	              out.left = pageXOffset;
	            }

	            return out;
	          }
	        } else if (this.targetModifier === 'scroll-handle') {
	          var bounds = undefined;
	          var target = this.target;
	          if (target === document.body) {
	            target = document.documentElement;

	            bounds = {
	              left: pageXOffset,
	              top: pageYOffset,
	              height: innerHeight,
	              width: innerWidth
	            };
	          } else {
	            bounds = getBounds(target);
	          }

	          var style = getComputedStyle(target);

	          var hasBottomScroll = target.scrollWidth > target.clientWidth || [style.overflow, style.overflowX].indexOf('scroll') >= 0 || this.target !== document.body;

	          var scrollBottom = 0;
	          if (hasBottomScroll) {
	            scrollBottom = 15;
	          }

	          var height = bounds.height - parseFloat(style.borderTopWidth) - parseFloat(style.borderBottomWidth) - scrollBottom;

	          var out = {
	            width: 15,
	            height: height * 0.975 * (height / target.scrollHeight),
	            left: bounds.left + bounds.width - parseFloat(style.borderLeftWidth) - 15
	          };

	          var fitAdj = 0;
	          if (height < 408 && this.target === document.body) {
	            fitAdj = -0.00011 * Math.pow(height, 2) - 0.00727 * height + 22.58;
	          }

	          if (this.target !== document.body) {
	            out.height = Math.max(out.height, 24);
	          }

	          var scrollPercentage = this.target.scrollTop / (target.scrollHeight - height);
	          out.top = scrollPercentage * (height - out.height - fitAdj) + bounds.top + parseFloat(style.borderTopWidth);

	          if (this.target === document.body) {
	            out.height = Math.max(out.height, 24);
	          }

	          return out;
	        }
	      } else {
	        return getBounds(this.target);
	      }
	    }
	  }, {
	    key: 'clearCache',
	    value: function clearCache() {
	      this._cache = {};
	    }
	  }, {
	    key: 'cache',
	    value: function cache(k, getter) {
	      // More than one module will often need the same DOM info, so
	      // we keep a cache which is cleared on each position call
	      if (typeof this._cache === 'undefined') {
	        this._cache = {};
	      }

	      if (typeof this._cache[k] === 'undefined') {
	        this._cache[k] = getter.call(this);
	      }

	      return this._cache[k];
	    }
	  }, {
	    key: 'enable',
	    value: function enable() {
	      var _this3 = this;

	      var pos = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];

	      if (!(this.options.addTargetClasses === false)) {
	        addClass(this.target, this.getClass('enabled'));
	      }
	      addClass(this.element, this.getClass('enabled'));
	      this.enabled = true;

	      this.scrollParents.forEach(function (parent) {
	        if (parent !== _this3.target.ownerDocument) {
	          parent.addEventListener('scroll', _this3.position);
	        }
	      });

	      if (pos) {
	        this.position();
	      }
	    }
	  }, {
	    key: 'disable',
	    value: function disable() {
	      var _this4 = this;

	      removeClass(this.target, this.getClass('enabled'));
	      removeClass(this.element, this.getClass('enabled'));
	      this.enabled = false;

	      if (typeof this.scrollParents !== 'undefined') {
	        this.scrollParents.forEach(function (parent) {
	          parent.removeEventListener('scroll', _this4.position);
	        });
	      }
	    }
	  }, {
	    key: 'destroy',
	    value: function destroy() {
	      var _this5 = this;

	      this.disable();

	      tethers.forEach(function (tether, i) {
	        if (tether === _this5) {
	          tethers.splice(i, 1);
	        }
	      });

	      // Remove any elements we were using for convenience from the DOM
	      if (tethers.length === 0) {
	        removeUtilElements();
	      }
	    }
	  }, {
	    key: 'updateAttachClasses',
	    value: function updateAttachClasses(elementAttach, targetAttach) {
	      var _this6 = this;

	      elementAttach = elementAttach || this.attachment;
	      targetAttach = targetAttach || this.targetAttachment;
	      var sides = ['left', 'top', 'bottom', 'right', 'middle', 'center'];

	      if (typeof this._addAttachClasses !== 'undefined' && this._addAttachClasses.length) {
	        // updateAttachClasses can be called more than once in a position call, so
	        // we need to clean up after ourselves such that when the last defer gets
	        // ran it doesn't add any extra classes from previous calls.
	        this._addAttachClasses.splice(0, this._addAttachClasses.length);
	      }

	      if (typeof this._addAttachClasses === 'undefined') {
	        this._addAttachClasses = [];
	      }
	      var add = this._addAttachClasses;

	      if (elementAttach.top) {
	        add.push(this.getClass('element-attached') + '-' + elementAttach.top);
	      }
	      if (elementAttach.left) {
	        add.push(this.getClass('element-attached') + '-' + elementAttach.left);
	      }
	      if (targetAttach.top) {
	        add.push(this.getClass('target-attached') + '-' + targetAttach.top);
	      }
	      if (targetAttach.left) {
	        add.push(this.getClass('target-attached') + '-' + targetAttach.left);
	      }

	      var all = [];
	      sides.forEach(function (side) {
	        all.push(_this6.getClass('element-attached') + '-' + side);
	        all.push(_this6.getClass('target-attached') + '-' + side);
	      });

	      defer(function () {
	        if (!(typeof _this6._addAttachClasses !== 'undefined')) {
	          return;
	        }

	        updateClasses(_this6.element, _this6._addAttachClasses, all);
	        if (!(_this6.options.addTargetClasses === false)) {
	          updateClasses(_this6.target, _this6._addAttachClasses, all);
	        }

	        delete _this6._addAttachClasses;
	      });
	    }
	  }, {
	    key: 'position',
	    value: function position() {
	      var _this7 = this;

	      var flushChanges = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];

	      // flushChanges commits the changes immediately, leave true unless you are positioning multiple
	      // tethers (in which case call Tether.Utils.flush yourself when you're done)

	      if (!this.enabled) {
	        return;
	      }

	      this.clearCache();

	      // Turn 'auto' attachments into the appropriate corner or edge
	      var targetAttachment = autoToFixedAttachment(this.targetAttachment, this.attachment);

	      this.updateAttachClasses(this.attachment, targetAttachment);

	      var elementPos = this.cache('element-bounds', function () {
	        return getBounds(_this7.element);
	      });

	      var width = elementPos.width;
	      var height = elementPos.height;

	      if (width === 0 && height === 0 && typeof this.lastSize !== 'undefined') {
	        var _lastSize = this.lastSize;

	        // We cache the height and width to make it possible to position elements that are
	        // getting hidden.
	        width = _lastSize.width;
	        height = _lastSize.height;
	      } else {
	        this.lastSize = { width: width, height: height };
	      }

	      var targetPos = this.cache('target-bounds', function () {
	        return _this7.getTargetBounds();
	      });
	      var targetSize = targetPos;

	      // Get an actual px offset from the attachment
	      var offset = offsetToPx(attachmentToOffset(this.attachment), { width: width, height: height });
	      var targetOffset = offsetToPx(attachmentToOffset(targetAttachment), targetSize);

	      var manualOffset = offsetToPx(this.offset, { width: width, height: height });
	      var manualTargetOffset = offsetToPx(this.targetOffset, targetSize);

	      // Add the manually provided offset
	      offset = addOffset(offset, manualOffset);
	      targetOffset = addOffset(targetOffset, manualTargetOffset);

	      // It's now our goal to make (element position + offset) == (target position + target offset)
	      var left = targetPos.left + targetOffset.left - offset.left;
	      var top = targetPos.top + targetOffset.top - offset.top;

	      for (var i = 0; i < TetherBase.modules.length; ++i) {
	        var _module2 = TetherBase.modules[i];
	        var ret = _module2.position.call(this, {
	          left: left,
	          top: top,
	          targetAttachment: targetAttachment,
	          targetPos: targetPos,
	          elementPos: elementPos,
	          offset: offset,
	          targetOffset: targetOffset,
	          manualOffset: manualOffset,
	          manualTargetOffset: manualTargetOffset,
	          scrollbarSize: scrollbarSize,
	          attachment: this.attachment
	        });

	        if (ret === false) {
	          return false;
	        } else if (typeof ret === 'undefined' || typeof ret !== 'object') {
	          continue;
	        } else {
	          top = ret.top;
	          left = ret.left;
	        }
	      }

	      // We describe the position three different ways to give the optimizer
	      // a chance to decide the best possible way to position the element
	      // with the fewest repaints.
	      var next = {
	        // It's position relative to the page (absolute positioning when
	        // the element is a child of the body)
	        page: {
	          top: top,
	          left: left
	        },

	        // It's position relative to the viewport (fixed positioning)
	        viewport: {
	          top: top - pageYOffset,
	          bottom: pageYOffset - top - height + innerHeight,
	          left: left - pageXOffset,
	          right: pageXOffset - left - width + innerWidth
	        }
	      };

	      var doc = this.target.ownerDocument;
	      var win = doc.defaultView;

	      var scrollbarSize = undefined;
	      if (win.innerHeight > doc.documentElement.clientHeight) {
	        scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
	        next.viewport.bottom -= scrollbarSize.height;
	      }

	      if (win.innerWidth > doc.documentElement.clientWidth) {
	        scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
	        next.viewport.right -= scrollbarSize.width;
	      }

	      if (['', 'static'].indexOf(doc.body.style.position) === -1 || ['', 'static'].indexOf(doc.body.parentElement.style.position) === -1) {
	        // Absolute positioning in the body will be relative to the page, not the 'initial containing block'
	        next.page.bottom = doc.body.scrollHeight - top - height;
	        next.page.right = doc.body.scrollWidth - left - width;
	      }

	      if (typeof this.options.optimizations !== 'undefined' && this.options.optimizations.moveElement !== false && !(typeof this.targetModifier !== 'undefined')) {
	        (function () {
	          var offsetParent = _this7.cache('target-offsetparent', function () {
	            return getOffsetParent(_this7.target);
	          });
	          var offsetPosition = _this7.cache('target-offsetparent-bounds', function () {
	            return getBounds(offsetParent);
	          });
	          var offsetParentStyle = getComputedStyle(offsetParent);
	          var offsetParentSize = offsetPosition;

	          var offsetBorder = {};
	          ['Top', 'Left', 'Bottom', 'Right'].forEach(function (side) {
	            offsetBorder[side.toLowerCase()] = parseFloat(offsetParentStyle['border' + side + 'Width']);
	          });

	          offsetPosition.right = doc.body.scrollWidth - offsetPosition.left - offsetParentSize.width + offsetBorder.right;
	          offsetPosition.bottom = doc.body.scrollHeight - offsetPosition.top - offsetParentSize.height + offsetBorder.bottom;

	          if (next.page.top >= offsetPosition.top + offsetBorder.top && next.page.bottom >= offsetPosition.bottom) {
	            if (next.page.left >= offsetPosition.left + offsetBorder.left && next.page.right >= offsetPosition.right) {
	              // We're within the visible part of the target's scroll parent
	              var scrollTop = offsetParent.scrollTop;
	              var scrollLeft = offsetParent.scrollLeft;

	              // It's position relative to the target's offset parent (absolute positioning when
	              // the element is moved to be a child of the target's offset parent).
	              next.offset = {
	                top: next.page.top - offsetPosition.top + scrollTop - offsetBorder.top,
	                left: next.page.left - offsetPosition.left + scrollLeft - offsetBorder.left
	              };
	            }
	          }
	        })();
	      }

	      // We could also travel up the DOM and try each containing context, rather than only
	      // looking at the body, but we're gonna get diminishing returns.

	      this.move(next);

	      this.history.unshift(next);

	      if (this.history.length > 3) {
	        this.history.pop();
	      }

	      if (flushChanges) {
	        flush();
	      }

	      return true;
	    }

	    // THE ISSUE
	  }, {
	    key: 'move',
	    value: function move(pos) {
	      var _this8 = this;

	      if (!(typeof this.element.parentNode !== 'undefined')) {
	        return;
	      }

	      var same = {};

	      for (var type in pos) {
	        same[type] = {};

	        for (var key in pos[type]) {
	          var found = false;

	          for (var i = 0; i < this.history.length; ++i) {
	            var point = this.history[i];
	            if (typeof point[type] !== 'undefined' && !within(point[type][key], pos[type][key])) {
	              found = true;
	              break;
	            }
	          }

	          if (!found) {
	            same[type][key] = true;
	          }
	        }
	      }

	      var css = { top: '', left: '', right: '', bottom: '' };

	      var transcribe = function transcribe(_same, _pos) {
	        var hasOptimizations = typeof _this8.options.optimizations !== 'undefined';
	        var gpu = hasOptimizations ? _this8.options.optimizations.gpu : null;
	        if (gpu !== false) {
	          var yPos = undefined,
	              xPos = undefined;
	          if (_same.top) {
	            css.top = 0;
	            yPos = _pos.top;
	          } else {
	            css.bottom = 0;
	            yPos = -_pos.bottom;
	          }

	          if (_same.left) {
	            css.left = 0;
	            xPos = _pos.left;
	          } else {
	            css.right = 0;
	            xPos = -_pos.right;
	          }

	          if (window.matchMedia) {
	            // HubSpot/tether#207
	            var retina = window.matchMedia('only screen and (min-resolution: 1.3dppx)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3)').matches;
	            if (!retina) {
	              xPos = Math.round(xPos);
	              yPos = Math.round(yPos);
	            }
	          }

	          css[transformKey] = 'translateX(' + xPos + 'px) translateY(' + yPos + 'px)';

	          if (transformKey !== 'msTransform') {
	            // The Z transform will keep this in the GPU (faster, and prevents artifacts),
	            // but IE9 doesn't support 3d transforms and will choke.
	            css[transformKey] += " translateZ(0)";
	          }
	        } else {
	          if (_same.top) {
	            css.top = _pos.top + 'px';
	          } else {
	            css.bottom = _pos.bottom + 'px';
	          }

	          if (_same.left) {
	            css.left = _pos.left + 'px';
	          } else {
	            css.right = _pos.right + 'px';
	          }
	        }
	      };

	      var moved = false;
	      if ((same.page.top || same.page.bottom) && (same.page.left || same.page.right)) {
	        css.position = 'absolute';
	        transcribe(same.page, pos.page);
	      } else if ((same.viewport.top || same.viewport.bottom) && (same.viewport.left || same.viewport.right)) {
	        css.position = 'fixed';
	        transcribe(same.viewport, pos.viewport);
	      } else if (typeof same.offset !== 'undefined' && same.offset.top && same.offset.left) {
	        (function () {
	          css.position = 'absolute';
	          var offsetParent = _this8.cache('target-offsetparent', function () {
	            return getOffsetParent(_this8.target);
	          });

	          if (getOffsetParent(_this8.element) !== offsetParent) {
	            defer(function () {
	              _this8.element.parentNode.removeChild(_this8.element);
	              offsetParent.appendChild(_this8.element);
	            });
	          }

	          transcribe(same.offset, pos.offset);
	          moved = true;
	        })();
	      } else {
	        css.position = 'absolute';
	        transcribe({ top: true, left: true }, pos.page);
	      }

	      if (!moved) {
	        if (this.options.bodyElement) {
	          this.options.bodyElement.appendChild(this.element);
	        } else {
	          var offsetParentIsBody = true;
	          var currentNode = this.element.parentNode;
	          while (currentNode && currentNode.nodeType === 1 && currentNode.tagName !== 'BODY') {
	            if (getComputedStyle(currentNode).position !== 'static') {
	              offsetParentIsBody = false;
	              break;
	            }

	            currentNode = currentNode.parentNode;
	          }

	          if (!offsetParentIsBody) {
	            this.element.parentNode.removeChild(this.element);
	            this.element.ownerDocument.body.appendChild(this.element);
	          }
	        }
	      }

	      // Any css change will trigger a repaint, so let's avoid one if nothing changed
	      var writeCSS = {};
	      var write = false;
	      for (var key in css) {
	        var val = css[key];
	        var elVal = this.element.style[key];

	        if (elVal !== val) {
	          write = true;
	          writeCSS[key] = val;
	        }
	      }

	      if (write) {
	        defer(function () {
	          extend(_this8.element.style, writeCSS);
	          _this8.trigger('repositioned');
	        });
	      }
	    }
	  }]);

	  return TetherClass;
	})(Evented);

	TetherClass.modules = [];

	TetherBase.position = position;

	var Tether = extend(TetherClass, TetherBase);
	/* globals TetherBase */

	'use strict';

	var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

	var _TetherBase$Utils = TetherBase.Utils;
	var getBounds = _TetherBase$Utils.getBounds;
	var extend = _TetherBase$Utils.extend;
	var updateClasses = _TetherBase$Utils.updateClasses;
	var defer = _TetherBase$Utils.defer;

	var BOUNDS_FORMAT = ['left', 'top', 'right', 'bottom'];

	function getBoundingRect(tether, to) {
	  if (to === 'scrollParent') {
	    to = tether.scrollParents[0];
	  } else if (to === 'window') {
	    to = [pageXOffset, pageYOffset, innerWidth + pageXOffset, innerHeight + pageYOffset];
	  }

	  if (to === document) {
	    to = to.documentElement;
	  }

	  if (typeof to.nodeType !== 'undefined') {
	    (function () {
	      var node = to;
	      var size = getBounds(to);
	      var pos = size;
	      var style = getComputedStyle(to);

	      to = [pos.left, pos.top, size.width + pos.left, size.height + pos.top];

	      // Account any parent Frames scroll offset
	      if (node.ownerDocument !== document) {
	        var win = node.ownerDocument.defaultView;
	        to[0] += win.pageXOffset;
	        to[1] += win.pageYOffset;
	        to[2] += win.pageXOffset;
	        to[3] += win.pageYOffset;
	      }

	      BOUNDS_FORMAT.forEach(function (side, i) {
	        side = side[0].toUpperCase() + side.substr(1);
	        if (side === 'Top' || side === 'Left') {
	          to[i] += parseFloat(style['border' + side + 'Width']);
	        } else {
	          to[i] -= parseFloat(style['border' + side + 'Width']);
	        }
	      });
	    })();
	  }

	  return to;
	}

	TetherBase.modules.push({
	  position: function position(_ref) {
	    var _this = this;

	    var top = _ref.top;
	    var left = _ref.left;
	    var targetAttachment = _ref.targetAttachment;

	    if (!this.options.constraints) {
	      return true;
	    }

	    var _cache = this.cache('element-bounds', function () {
	      return getBounds(_this.element);
	    });

	    var height = _cache.height;
	    var width = _cache.width;

	    if (width === 0 && height === 0 && typeof this.lastSize !== 'undefined') {
	      var _lastSize = this.lastSize;

	      // Handle the item getting hidden as a result of our positioning without glitching
	      // the classes in and out
	      width = _lastSize.width;
	      height = _lastSize.height;
	    }

	    var targetSize = this.cache('target-bounds', function () {
	      return _this.getTargetBounds();
	    });

	    var targetHeight = targetSize.height;
	    var targetWidth = targetSize.width;

	    var allClasses = [this.getClass('pinned'), this.getClass('out-of-bounds')];

	    this.options.constraints.forEach(function (constraint) {
	      var outOfBoundsClass = constraint.outOfBoundsClass;
	      var pinnedClass = constraint.pinnedClass;

	      if (outOfBoundsClass) {
	        allClasses.push(outOfBoundsClass);
	      }
	      if (pinnedClass) {
	        allClasses.push(pinnedClass);
	      }
	    });

	    allClasses.forEach(function (cls) {
	      ['left', 'top', 'right', 'bottom'].forEach(function (side) {
	        allClasses.push(cls + '-' + side);
	      });
	    });

	    var addClasses = [];

	    var tAttachment = extend({}, targetAttachment);
	    var eAttachment = extend({}, this.attachment);

	    this.options.constraints.forEach(function (constraint) {
	      var to = constraint.to;
	      var attachment = constraint.attachment;
	      var pin = constraint.pin;

	      if (typeof attachment === 'undefined') {
	        attachment = '';
	      }

	      var changeAttachX = undefined,
	          changeAttachY = undefined;
	      if (attachment.indexOf(' ') >= 0) {
	        var _attachment$split = attachment.split(' ');

	        var _attachment$split2 = _slicedToArray(_attachment$split, 2);

	        changeAttachY = _attachment$split2[0];
	        changeAttachX = _attachment$split2[1];
	      } else {
	        changeAttachX = changeAttachY = attachment;
	      }

	      var bounds = getBoundingRect(_this, to);

	      if (changeAttachY === 'target' || changeAttachY === 'both') {
	        if (top < bounds[1] && tAttachment.top === 'top') {
	          top += targetHeight;
	          tAttachment.top = 'bottom';
	        }

	        if (top + height > bounds[3] && tAttachment.top === 'bottom') {
	          top -= targetHeight;
	          tAttachment.top = 'top';
	        }
	      }

	      if (changeAttachY === 'together') {
	        if (tAttachment.top === 'top') {
	          if (eAttachment.top === 'bottom' && top < bounds[1]) {
	            top += targetHeight;
	            tAttachment.top = 'bottom';

	            top += height;
	            eAttachment.top = 'top';
	          } else if (eAttachment.top === 'top' && top + height > bounds[3] && top - (height - targetHeight) >= bounds[1]) {
	            top -= height - targetHeight;
	            tAttachment.top = 'bottom';

	            eAttachment.top = 'bottom';
	          }
	        }

	        if (tAttachment.top === 'bottom') {
	          if (eAttachment.top === 'top' && top + height > bounds[3]) {
	            top -= targetHeight;
	            tAttachment.top = 'top';

	            top -= height;
	            eAttachment.top = 'bottom';
	          } else if (eAttachment.top === 'bottom' && top < bounds[1] && top + (height * 2 - targetHeight) <= bounds[3]) {
	            top += height - targetHeight;
	            tAttachment.top = 'top';

	            eAttachment.top = 'top';
	          }
	        }

	        if (tAttachment.top === 'middle') {
	          if (top + height > bounds[3] && eAttachment.top === 'top') {
	            top -= height;
	            eAttachment.top = 'bottom';
	          } else if (top < bounds[1] && eAttachment.top === 'bottom') {
	            top += height;
	            eAttachment.top = 'top';
	          }
	        }
	      }

	      if (changeAttachX === 'target' || changeAttachX === 'both') {
	        if (left < bounds[0] && tAttachment.left === 'left') {
	          left += targetWidth;
	          tAttachment.left = 'right';
	        }

	        if (left + width > bounds[2] && tAttachment.left === 'right') {
	          left -= targetWidth;
	          tAttachment.left = 'left';
	        }
	      }

	      if (changeAttachX === 'together') {
	        if (left < bounds[0] && tAttachment.left === 'left') {
	          if (eAttachment.left === 'right') {
	            left += targetWidth;
	            tAttachment.left = 'right';

	            left += width;
	            eAttachment.left = 'left';
	          } else if (eAttachment.left === 'left') {
	            left += targetWidth;
	            tAttachment.left = 'right';

	            left -= width;
	            eAttachment.left = 'right';
	          }
	        } else if (left + width > bounds[2] && tAttachment.left === 'right') {
	          if (eAttachment.left === 'left') {
	            left -= targetWidth;
	            tAttachment.left = 'left';

	            left -= width;
	            eAttachment.left = 'right';
	          } else if (eAttachment.left === 'right') {
	            left -= targetWidth;
	            tAttachment.left = 'left';

	            left += width;
	            eAttachment.left = 'left';
	          }
	        } else if (tAttachment.left === 'center') {
	          if (left + width > bounds[2] && eAttachment.left === 'left') {
	            left -= width;
	            eAttachment.left = 'right';
	          } else if (left < bounds[0] && eAttachment.left === 'right') {
	            left += width;
	            eAttachment.left = 'left';
	          }
	        }
	      }

	      if (changeAttachY === 'element' || changeAttachY === 'both') {
	        if (top < bounds[1] && eAttachment.top === 'bottom') {
	          top += height;
	          eAttachment.top = 'top';
	        }

	        if (top + height > bounds[3] && eAttachment.top === 'top') {
	          top -= height;
	          eAttachment.top = 'bottom';
	        }
	      }

	      if (changeAttachX === 'element' || changeAttachX === 'both') {
	        if (left < bounds[0]) {
	          if (eAttachment.left === 'right') {
	            left += width;
	            eAttachment.left = 'left';
	          } else if (eAttachment.left === 'center') {
	            left += width / 2;
	            eAttachment.left = 'left';
	          }
	        }

	        if (left + width > bounds[2]) {
	          if (eAttachment.left === 'left') {
	            left -= width;
	            eAttachment.left = 'right';
	          } else if (eAttachment.left === 'center') {
	            left -= width / 2;
	            eAttachment.left = 'right';
	          }
	        }
	      }

	      if (typeof pin === 'string') {
	        pin = pin.split(',').map(function (p) {
	          return p.trim();
	        });
	      } else if (pin === true) {
	        pin = ['top', 'left', 'right', 'bottom'];
	      }

	      pin = pin || [];

	      var pinned = [];
	      var oob = [];

	      if (top < bounds[1]) {
	        if (pin.indexOf('top') >= 0) {
	          top = bounds[1];
	          pinned.push('top');
	        } else {
	          oob.push('top');
	        }
	      }

	      if (top + height > bounds[3]) {
	        if (pin.indexOf('bottom') >= 0) {
	          top = bounds[3] - height;
	          pinned.push('bottom');
	        } else {
	          oob.push('bottom');
	        }
	      }

	      if (left < bounds[0]) {
	        if (pin.indexOf('left') >= 0) {
	          left = bounds[0];
	          pinned.push('left');
	        } else {
	          oob.push('left');
	        }
	      }

	      if (left + width > bounds[2]) {
	        if (pin.indexOf('right') >= 0) {
	          left = bounds[2] - width;
	          pinned.push('right');
	        } else {
	          oob.push('right');
	        }
	      }

	      if (pinned.length) {
	        (function () {
	          var pinnedClass = undefined;
	          if (typeof _this.options.pinnedClass !== 'undefined') {
	            pinnedClass = _this.options.pinnedClass;
	          } else {
	            pinnedClass = _this.getClass('pinned');
	          }

	          addClasses.push(pinnedClass);
	          pinned.forEach(function (side) {
	            addClasses.push(pinnedClass + '-' + side);
	          });
	        })();
	      }

	      if (oob.length) {
	        (function () {
	          var oobClass = undefined;
	          if (typeof _this.options.outOfBoundsClass !== 'undefined') {
	            oobClass = _this.options.outOfBoundsClass;
	          } else {
	            oobClass = _this.getClass('out-of-bounds');
	          }

	          addClasses.push(oobClass);
	          oob.forEach(function (side) {
	            addClasses.push(oobClass + '-' + side);
	          });
	        })();
	      }

	      if (pinned.indexOf('left') >= 0 || pinned.indexOf('right') >= 0) {
	        eAttachment.left = tAttachment.left = false;
	      }
	      if (pinned.indexOf('top') >= 0 || pinned.indexOf('bottom') >= 0) {
	        eAttachment.top = tAttachment.top = false;
	      }

	      if (tAttachment.top !== targetAttachment.top || tAttachment.left !== targetAttachment.left || eAttachment.top !== _this.attachment.top || eAttachment.left !== _this.attachment.left) {
	        _this.updateAttachClasses(eAttachment, tAttachment);
	        _this.trigger('update', {
	          attachment: eAttachment,
	          targetAttachment: tAttachment
	        });
	      }
	    });

	    defer(function () {
	      if (!(_this.options.addTargetClasses === false)) {
	        updateClasses(_this.target, addClasses, allClasses);
	      }
	      updateClasses(_this.element, addClasses, allClasses);
	    });

	    return { top: top, left: left };
	  }
	});
	/* globals TetherBase */

	'use strict';

	var _TetherBase$Utils = TetherBase.Utils;
	var getBounds = _TetherBase$Utils.getBounds;
	var updateClasses = _TetherBase$Utils.updateClasses;
	var defer = _TetherBase$Utils.defer;

	TetherBase.modules.push({
	  position: function position(_ref) {
	    var _this = this;

	    var top = _ref.top;
	    var left = _ref.left;

	    var _cache = this.cache('element-bounds', function () {
	      return getBounds(_this.element);
	    });

	    var height = _cache.height;
	    var width = _cache.width;

	    var targetPos = this.getTargetBounds();

	    var bottom = top + height;
	    var right = left + width;

	    var abutted = [];
	    if (top <= targetPos.bottom && bottom >= targetPos.top) {
	      ['left', 'right'].forEach(function (side) {
	        var targetPosSide = targetPos[side];
	        if (targetPosSide === left || targetPosSide === right) {
	          abutted.push(side);
	        }
	      });
	    }

	    if (left <= targetPos.right && right >= targetPos.left) {
	      ['top', 'bottom'].forEach(function (side) {
	        var targetPosSide = targetPos[side];
	        if (targetPosSide === top || targetPosSide === bottom) {
	          abutted.push(side);
	        }
	      });
	    }

	    var allClasses = [];
	    var addClasses = [];

	    var sides = ['left', 'top', 'right', 'bottom'];
	    allClasses.push(this.getClass('abutted'));
	    sides.forEach(function (side) {
	      allClasses.push(_this.getClass('abutted') + '-' + side);
	    });

	    if (abutted.length) {
	      addClasses.push(this.getClass('abutted'));
	    }

	    abutted.forEach(function (side) {
	      addClasses.push(_this.getClass('abutted') + '-' + side);
	    });

	    defer(function () {
	      if (!(_this.options.addTargetClasses === false)) {
	        updateClasses(_this.target, addClasses, allClasses);
	      }
	      updateClasses(_this.element, addClasses, allClasses);
	    });

	    return true;
	  }
	});
	/* globals TetherBase */

	'use strict';

	var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

	TetherBase.modules.push({
	  position: function position(_ref) {
	    var top = _ref.top;
	    var left = _ref.left;

	    if (!this.options.shift) {
	      return;
	    }

	    var shift = this.options.shift;
	    if (typeof this.options.shift === 'function') {
	      shift = this.options.shift.call(this, { top: top, left: left });
	    }

	    var shiftTop = undefined,
	        shiftLeft = undefined;
	    if (typeof shift === 'string') {
	      shift = shift.split(' ');
	      shift[1] = shift[1] || shift[0];

	      var _shift = shift;

	      var _shift2 = _slicedToArray(_shift, 2);

	      shiftTop = _shift2[0];
	      shiftLeft = _shift2[1];

	      shiftTop = parseFloat(shiftTop, 10);
	      shiftLeft = parseFloat(shiftLeft, 10);
	    } else {
	      shiftTop = shift.top;
	      shiftLeft = shift.left;
	    }

	    top += shiftTop;
	    left += shiftLeft;

	    return { top: top, left: left };
	  }
	});
	return Tether;

	}));


/***/ },

/***/ 241:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(242);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../css-loader/index.js!./drop-theme-arrows-bounce-dark.css", function() {
				var newContent = require("!!../../../css-loader/index.js!./drop-theme-arrows-bounce-dark.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 242:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, ".drop-element, .drop-element:after, .drop-element:before, .drop-element *, .drop-element *:after, .drop-element *:before {\n  box-sizing: border-box; }\n\n.drop-element {\n  position: absolute;\n  display: none; }\n  .drop-element.drop-open {\n    display: block; }\n\n.drop-element.drop-theme-arrows-bounce-dark {\n  max-width: 100%;\n  max-height: 100%; }\n  .drop-element.drop-theme-arrows-bounce-dark .drop-content {\n    border-radius: 5px;\n    position: relative;\n    font-family: inherit;\n    background: #000;\n    color: #fff;\n    padding: 1em;\n    font-size: 1.1em;\n    line-height: 1.5em; }\n    .drop-element.drop-theme-arrows-bounce-dark .drop-content:before {\n      content: \"\";\n      display: block;\n      position: absolute;\n      width: 0;\n      height: 0;\n      border-color: transparent;\n      border-width: 12px;\n      border-style: solid; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-center .drop-content {\n    margin-bottom: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-center .drop-content:before {\n      top: 100%;\n      left: 50%;\n      margin-left: -12px;\n      border-top-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-center .drop-content {\n    margin-top: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-center .drop-content:before {\n      bottom: 100%;\n      left: 50%;\n      margin-left: -12px;\n      border-bottom-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-right.drop-element-attached-middle .drop-content {\n    margin-right: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-right.drop-element-attached-middle .drop-content:before {\n      left: 100%;\n      top: 50%;\n      margin-top: -12px;\n      border-left-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-left.drop-element-attached-middle .drop-content {\n    margin-left: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-left.drop-element-attached-middle .drop-content:before {\n      right: 100%;\n      top: 50%;\n      margin-top: -12px;\n      border-right-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-left.drop-target-attached-bottom .drop-content {\n    margin-top: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-left.drop-target-attached-bottom .drop-content:before {\n      bottom: 100%;\n      left: 12px;\n      border-bottom-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-right.drop-target-attached-bottom .drop-content {\n    margin-top: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-right.drop-target-attached-bottom .drop-content:before {\n      bottom: 100%;\n      right: 12px;\n      border-bottom-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-left.drop-target-attached-top .drop-content {\n    margin-bottom: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-left.drop-target-attached-top .drop-content:before {\n      top: 100%;\n      left: 12px;\n      border-top-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-right.drop-target-attached-top .drop-content {\n    margin-bottom: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-right.drop-target-attached-top .drop-content:before {\n      top: 100%;\n      right: 12px;\n      border-top-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-right.drop-target-attached-left .drop-content {\n    margin-right: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-right.drop-target-attached-left .drop-content:before {\n      top: 12px;\n      left: 100%;\n      border-left-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-left.drop-target-attached-right .drop-content {\n    margin-left: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-left.drop-target-attached-right .drop-content:before {\n      top: 12px;\n      right: 100%;\n      border-right-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-right.drop-target-attached-left .drop-content {\n    margin-right: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-right.drop-target-attached-left .drop-content:before {\n      bottom: 12px;\n      left: 100%;\n      border-left-color: #000; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-left.drop-target-attached-right .drop-content {\n    margin-left: 12px; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-left.drop-target-attached-right .drop-content:before {\n      bottom: 12px;\n      right: 100%;\n      border-right-color: #000; }\n\n.drop-element.drop-theme-arrows-bounce-dark {\n  -webkit-transform: translateZ(0);\n          transform: translateZ(0);\n  -webkit-transition: opacity 100ms;\n          transition: opacity 100ms;\n  opacity: 0; }\n  .drop-element.drop-theme-arrows-bounce-dark .drop-content {\n    -webkit-transition: -webkit-transform 0.3s cubic-bezier(0, 0, 0.265, 1.55);\n            transition: transform 0.3s cubic-bezier(0, 0, 0.265, 1.55);\n    -webkit-transform: scale(0) translateZ(0);\n            transform: scale(0) translateZ(0); }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-open {\n    display: none; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-open-transitionend {\n    display: block; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-after-open {\n    -webkit-transition: none;\n            transition: none;\n    opacity: 1; }\n    .drop-element.drop-theme-arrows-bounce-dark.drop-after-open .drop-content {\n      -webkit-transform: scale(1) translateZ(0);\n              transform: scale(1) translateZ(0); }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-center .drop-content {\n    -webkit-transform-origin: 50% calc(100% + 12px);\n        -ms-transform-origin: 50% calc(100% + 12px);\n            transform-origin: 50% calc(100% + 12px); }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-center .drop-content {\n    -webkit-transform-origin: 50% -12px;\n        -ms-transform-origin: 50% -12px;\n            transform-origin: 50% -12px; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-right.drop-element-attached-middle .drop-content {\n    -webkit-transform-origin: calc(100% + 12px) 50%;\n        -ms-transform-origin: calc(100% + 12px) 50%;\n            transform-origin: calc(100% + 12px) 50%; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-left.drop-element-attached-middle .drop-content {\n    -webkit-transform-origin: -12px 50%;\n        -ms-transform-origin: -12px 50%;\n            transform-origin: -12px 50%; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-left.drop-target-attached-bottom .drop-content {\n    -webkit-transform-origin: 0 -12px;\n        -ms-transform-origin: 0 -12px;\n            transform-origin: 0 -12px; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-right.drop-target-attached-bottom .drop-content {\n    -webkit-transform-origin: 100% -12px;\n        -ms-transform-origin: 100% -12px;\n            transform-origin: 100% -12px; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-left.drop-target-attached-top .drop-content {\n    -webkit-transform-origin: 0 calc(100% + 12px);\n        -ms-transform-origin: 0 calc(100% + 12px);\n            transform-origin: 0 calc(100% + 12px); }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-right.drop-target-attached-top .drop-content {\n    -webkit-transform-origin: 100% calc(100% + 12px);\n        -ms-transform-origin: 100% calc(100% + 12px);\n            transform-origin: 100% calc(100% + 12px); }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-right.drop-target-attached-left .drop-content {\n    -webkit-transform-origin: calc(100% + 12px) 0;\n        -ms-transform-origin: calc(100% + 12px) 0;\n            transform-origin: calc(100% + 12px) 0; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-top.drop-element-attached-left.drop-target-attached-right .drop-content {\n    -webkit-transform-origin: -12px 0;\n        -ms-transform-origin: -12px 0;\n            transform-origin: -12px 0; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-right.drop-target-attached-left .drop-content {\n    -webkit-transform-origin: calc(100% + 12px) 100%;\n        -ms-transform-origin: calc(100% + 12px) 100%;\n            transform-origin: calc(100% + 12px) 100%; }\n  .drop-element.drop-theme-arrows-bounce-dark.drop-element-attached-bottom.drop-element-attached-left.drop-target-attached-right .drop-content {\n    -webkit-transform-origin: -12px 100%;\n        -ms-transform-origin: -12px 100%;\n            transform-origin: -12px 100%; }\n", ""]);

	// exports


/***/ },

/***/ 243:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {(function(angular) {
	    'use strict';

	    var module = angular.module('angularEffDrop',[]),
	        extend = angular.extend;

	    module.provider('dropWrapper', [function() {
	        // Default template for tooltips.
	        var defaultTemplateUrl = 'template/eff-drop.html'
	        this.setDefaultTemplateUrl = function(templateUrl) {
	            defaultTemplateUrl = templateUrl;
	        };

	        var defaultTetherOptions = {};
	        this.setDefaultTetherOptions = function(options) {
	            extend(defaultTetherOptions, options);
	        };

	        this.$get = ['$rootScope', '$compile', '$templateCache', function($rootScope, $compile, $templateCache) {
	            return function(options) {
	                options = options || {};
	                var templateUrl = options.templateUrl || defaultTemplateUrl,
	                    template = options.template || $templateCache.get(templateUrl),
	                    scope = options.scope || $rootScope.$new(),
	                    elem = $compile(template)(scope),
	                    drop;
	                var opts = {
	                    target: options.target[0],
	                    content: elem[0],
	                    position: options.position || 'bottom left',
	                    openOn: options.openOn || undefined,
	                    openDelay: options.openDelay || undefined,
	                    closeDelay: options.closeDelay || undefined,
	                    constrainToWindow: options.constrainToWindow || true,
	                    constrainToScrollParent: options.constrainToScrollParent || true,
	                    classes: options.classes || 'drop-theme-arrows-bounce-dark',
	                    remove: false,
	                    tetherOptions: options.tetherOptions || defaultTetherOptions,
	                };
	                if ((opts.content == null) || (opts.content == undefined)) {
	                    console.error('content of (', templateUrl || template, ')', opts.content);
	                    throw ("'templateUrl' or 'template' parameter is incorrect !");
	                }

	                /**
	                 * Create a drop for the target and the template.
	                 */
	                function attachDrop() {
	                    if (drop)
	                        detachDrop();
	                    drop = new Drop(opts);
	                };

	                /**
	                 * Detach the drop.
	                 */
	                function detachDrop() {
	                    if ((drop) && (drop.isOpened())) {
	                        drop.destroy();
	                        drop = undefined;
	                    }
	                };

	                /**
	                 * Open the drop
	                 */
	                function open(fn) {
	                    if (typeof(fn) == 'function')
	                        fn(scope, true);
	                    attachDrop();
	                    if (!drop.isOpened()) {
	                        drop.open();
	                    }
	                };

	                /**
	                 * Close the drop
	                 */
	                function close(fn) {
	                    if (typeof(fn) == 'function')
	                        fn(scope, false);
	                    detachDrop();
	                };

	                function toogle(fn) {
	                    if ((drop) && (drop.isOpened())) {
	                        close(fn);
	                    } else {
	                        open(fn);
	                    }
	                };

	                function isOpened() {
	                    if ((drop) && (drop.isOpened()))
	                        return true;
	                    return false;
	                }

	                // Close the tooltip when the scope is destroyed.
	                scope.$on('$destroy', close);
	                // Prepare the tooltip to be shown when the scope is created.
	                attachDrop();

	                return {
	                    open: open,
	                    close: close,
	                    toogle: toogle,
	                    isOpened: isOpened
	                };
	            };
	        }];
	    }]);

	    module.provider('effDropFactory', [function() {
	        /**
	         * Returns a factory function for building a directive for tooltips.
	         *
	         * @param {String} name - The name of the directive.
	         */
	        this.$get = ['dropWrapper', function(wrapper) {
	            return function(name, options) {
	                return {
	                    restrict: 'EA',
	                    scope: {
	                        content: '@' + name
	                    },
	                    link: function(scope, elem, attrs) {
	                        var drop = wrapper(angular.extend({
	                            target: elem,
	                            scope: scope,

	                        }, options));

	                        /**
	                         * Toggle the drop.
	                         */
	                        $(elem).hover(function mouseenter() {
	                            scope.$apply(drop.open);
	                        }, function mouseleave() {
	                            scope.$apply(drop.close);
	                        });
	                    }
	                };
	            };
	        }];
	    }]);

	    module.directive('effDrop', ['effDropFactory', function(provider) {
	        return provider('effDrop');
	    }]);

	    module.run(['$templateCache', function($templateCache) {
	        $templateCache.put('template/eff-drop.html', '<div class="eff-drop">{{content}}</div>');
	    }]);

	})(angular);
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 244:
/***/ function(module, exports) {

	module.exports = "\n<div class=\"comment-component-wrapper\">\n\t<div class=\"post\">\n\t\t<div class=\"comment-wrapper\">\n\t\t\t<div ng-if=\"vm.showCommentField\" class=\"comment-field\">\n\t\t\t\t<div class=\"unauthorized\" ng-if=\"!vm.isUserLoggedIn()\">Login to add comment.</div>\n\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.commentPostingErr\">{{vm.commentPostingErr}}</div>\n\t\t\t\t<div class=\"profile-pic\"><img ng-src=\"{{vm.getUserPic()}}\" width=\"100%\" height=\"100%\" alt=\"\"></div>\n\t\t\t\t<div class=\"tf\">\n\t\t\t\t\t<textarea rows='1' id=\"{{vm.postId}}_textarea\" ng-keydown=\"vm.keyDownEventOnTextField($event,vm.postId,'','0')\" ng-model=\"vm.comment[vm.postId]\" placeholder=\"Write a comment\" name=\"\"></textarea>\n\t\t\t\t\t<div class=\"bottom-tf\" ng-if=\"vm.showBigCommnentField\">\n\t\t\t\t\t\t<span ng-if=\"vm.canUploadMedia\" ng-class=\"{'disable':vm.isPostSubmitting}\" class=\"camera-icon\" ngf-select ngf-multiple=true ngf-accept=\"'image/*,video/mp4'\" ng-model=\"vm.commentAttachments[vm.postId]\" ngf-change=\"vm.changeCommentAttachment(vm.postId,$files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event)\"  ngf-keep=\"true\" title=\"Attach Photo\"><i class=\"fa fa-camera\"></i></span>\n\t\t\t\t\t\t<div ng-class=\"{'disable':vm.isPostSubmitting}\" ng-click=\"vm.cancelComment($event)\" class=\"post-comment-btn\">Cancel</div>\n\t\t\t\t\t\t<div ng-class=\"{'disable':vm.isPostSubmitting}\" ng-click=\"vm.addComment($event,vm.postId,'','0')\" class=\"post-comment-btn\">Submit</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"progressbar\"></div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t<div class=\"comment-attachment-wrapper\" >\n\t\t\t\t\t<span class=\"status-attachment image\" ng-repeat=\"file in vm.photoAttachments[vm.postId] track by $index\" >\n\t\t\t\t\t\t<span ng-click=\"vm.removeCommentPhotoAttachment(vm.postId,$index)\" class=\"remove-icon\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t<span class=\"image\" ngf-thumbnail=\"file\" ngf-size=\"{width: 100, height: 100, quality: 1}\" ngf-as-background=\"true\"></span>\n\t\t\t\t\t</span>\n\t\t\t\t\t<span class=\"status-attachment video\" ng-repeat=\"file in vm.videoAttachments[vm.postId] track by $index\" >\n\t\t\t\t\t\t<span class=\"videoName\">{{file.name}}</span>\n\t\t\t\t\t\t<span class=\"remove-icon\" ng-click=\"vm.removeCommentVideoAttachment(vm.postId,$index)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t</span>\n\t\t\t\t</div>\n\n\t\t\t\t<div ng-if=\"vm.commentAttchmentError\">\n\t\t\t\t\t<div class=\"alert alert-danger\">{{vm.attachmentErrorStr}}</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"comment-loader-wrapper\" ng-if=\"!vm.comments\">\n\t\t\t\t<div class=\"loader\"></div>\n\t\t\t</div>\n\t\t\t<div class=\"comment\" ng-repeat=\"comment in vm.comments | orderBy:'cmnt_ts':true track by $index\">\n\t\t\t\t<!-- <div class=\"head\"> -->\n\t\t\t\t\t<div class=\"head-wrapper\">\n\t\t\t\t\t\t<div class=\"profile-pic\">\n\t\t\t\t\t\t\t<img ng-src=\"{{comment.usr_info.usr_pic}}\" user-default-pic width=\"100%\" height=\"100%\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div style=\"width: 100%\">\n\t\t\t\t\t\t\t<span class=\"answer-by-text\" ng-if=\"vm.isQaPage\">Answered By</span>\n\t\t\t\t\t\t\t<a href=\"/dashboardApp/profile/{{comment.usr_id}}\" class=\"username\">{{comment.usr_info.dsp_nm}}</a>\n\t\t\t\t\t\t\t<!-- Comment Text -->\n\t\t\t\t\t\t\t<span class=\"text\" ng-if=\"!vm.isCommentEditingInProcess || vm.currentCommentEditing != comment.cmnt_id\" id=\"{{comment.cmnt_id}}\" ng-class=\"{'full-comment':vm.commentMore[comment.cmnt_id]}\" ng-bind-html=\"vm.trustAsHTML(comment.cmnt_txt)\"></span>\n\t\t\t\t\t\t\t<!-- Edit Comment Textarea -->\n\t\t\t\t\t\t\t<textarea\n\t\t\t\t\t\t\t\tng-if=\"vm.isCommentEditingInProcess && vm.currentCommentEditing === comment.cmnt_id\"\n\t\t\t\t\t\t\t\trows='1' data-min-rows='1'\n\t\t\t\t\t\t\t\tclass=\"\"\n\t\t\t\t\t\t\t\tng-init=\"vm.comment[comment.cmnt_id] = comment.cmnt_txt\"\n\t\t\t\t\t\t\t\tng-model=\"vm.comment[comment.cmnt_id]\"\n\t\t\t\t\t\t\t\tplaceholder=\"Write a reply\" name=\"\" style=\"display: block\"\n\t\t\t\t\t\t\t\tng-keydown=\"vm.keyUpEventOnEditCommentTextField($event,comment.cmnt_id)\"></textarea>\n\t\t\t\t\t\t\t<div ng-if=\"comment.cmnt_img\" class=\"comment-image-attachment-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"comment-image-attachment\" ng-repeat=\"pic in comment.cmnt_img track by $index\">\n\t\t\t\t\t\t\t\t\t<img ng-src=\"{{pic}}\" class=\"pic_{{comment.cmnt_id}} {{$index}}\" ng-click=\"vm.openImageInGallery(comment.cmnt_id,$index)\" data-mfp-src=\"{{pic}}\" alt=\"image\">\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"comment.cmnt_vid\">\n\t\t\t\t\t\t\t\t<div class=\"post-video-attachment\" ng-repeat=\"video in comment.cmnt_vid track by $index\">\n\t\t\t\t\t\t\t\t\t<video-controls-jw style=\"height: 100%;width: 100%;display: block\" player-id=\"{{comment.cmnt_id}}_{{$index}}\" player-url=\"{{video}}\" auto-start=\"false\"></video-controls-jw>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"action-btns\">\n\t\t\t\t\t\t\t\t<!-- show only in profile page -->\n\t\t\t\t\t\t\t\t<!-- like/unlike button -->\n\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"!vm.isQaPage\">\n\t\t\t\t\t\t\t\t\t<span ng-click=\"vm.toggleCommentLike(comment.cmnt_id,0)\" ng-if=\"vm.isCommentLikedByUser(comment.cmnt_id) === true\">Unlike</span>\n\t\t\t\t\t\t\t\t\t<span ng-click=\"vm.toggleCommentLike(comment.cmnt_id,1)\" ng-if=\"vm.isCommentLikedByUser(comment.cmnt_id) != true\">Like</span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<!-- show only in qa page -->\n\t\t\t\t\t\t\t\t<span class=\"action-btn\"\n\t\t\t\t\t\t\t\t\tng-if=\"vm.isQaPage\"\n\t\t\t\t\t\t\t\t\tng-class=\"{'disable':vm.isCommentLikedByUser(comment.cmnt_id) === true}\"\n\t\t\t\t\t\t\t\t\tng-click=\"vm.likeDislikeComment(comment.cmnt_id,1)\">\n\t\t\t\t\t\t\t\t\t<span class=\"fb-icon fb-like-icon\"></span> {{vm.getLikeCount(comment.cmnt_id)}}\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<!-- show dislike button only in qa page -->\n\t\t\t\t\t\t\t\t<span class=\"action-btn\"\n\t\t\t\t\t\t\t\t\tng-if=\"vm.isQaPage\"\n\t\t\t\t\t\t\t\t\tng-class=\"{'disable':vm.isCommentLikedByUser(comment.cmnt_id) === false}\"\n\t\t\t\t\t\t\t\t\tng-click=\"vm.likeDislikeComment(comment.cmnt_id,0)\">\n\t\t\t\t\t\t\t\t\t<span class=\"fb-icon fb-like-icon fb-icon-rotate-180\"></span> {{vm.getDisLikeCount(comment.cmnt_id)}}\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"vm.isQaPage\" ng-click=\"vm.showSublevelCommentTextField(comment.cmnt_id)\">\n\t\t\t\t\t\t\t\t\t<span class=\"fb-icon fb-comment-icon\"></span>{{vm.subLevelComments[comment.cmnt_id]['comments'].length}} Reply\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<!-- show reply only in profile page -->\n\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"!vm.isQaPage\" ng-click=\"vm.showSublevelCommentTextField(comment.cmnt_id)\">\n\t\t\t\t\t\t\t\t\tReply\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<!-- Show only in profile page (likes count) -->\n\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"!vm.isQaPage\"  my-drop content=\"vm.commentsUsers[comment.cmnt_id]\" ng-click=\"vm.fetchCommentUsers(comment.cmnt_id)\">\n\t\t\t\t\t\t\t\t\t<span class=\"fb-emotion fb-emotion-like\"></span>\n\t\t\t\t\t\t\t\t\t{{vm.getLikeCount(comment.cmnt_id)}}\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span class=\"action-btn\"\n\t\t\t\t\t\t\t\t\tng-click=\"vm.cancelEditComment()\"\n\t\t\t\t\t\t\t\t\tng-if=\"vm.isCommentEditingInProcess && vm.currentCommentEditing === comment.cmnt_id\" >\n\t\t\t\t\t\t\t\t\tCancel\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-click=\"vm.showSublevelCommentTextField(comment.cmnt_id)\" ng-if=\"vm.subLevelComments[comment.cmnt_id]['comments'].length > 0 && !vm.isQaPage\" ng-show=\"!vm.subLevelComments[comment.cmnt_id]['showSublevelCommentTextField']\" class=\"action-btn\">\n\t\t\t\t\t\t\t\t<span class=\"fb-icon fb-replies-icon\"></span>\n\t\t\t\t\t\t\t\t{{vm.subLevelComments[comment.cmnt_id]['comments'].length}} Reply\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-click=\"vm.collapseSublevelCommentTextField(comment.cmnt_id)\" ng-if=\"vm.subLevelComments[comment.cmnt_id]['showSublevelCommentTextField'] && !vm.isQaPage\" class=\"action-btn\">\n\t\t\t\t\t\t\t\tCollase\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<span class=\"post-options\" ng-click=\"vm.toggleOptions($event)\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>\n\t\t\t\t\t\t\t\t<div class=\"overlay\">\n\t\t\t\t\t\t\t\t\t<ul class=\"list-group\">\n\t\t\t\t\t\t\t\t\t\t<a ng-if=\"vm.isAuthorOfComment(comment.cmnt_id)\" ng-click=\"vm.editComment(comment.cmnt_id)\" class=\"list-group-item\">\n\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"list-group-item-heading\">Edit</h6>\n\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t<a ng-if=\"vm.isAuthorOfComment(comment.cmnt_id)\" ng-click=\"vm.showDeleteCommentModal(comment.cmnt_id)\" class=\"list-group-item\">\n\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"list-group-item-heading\">Delete</h6>\n\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t<a ng-click=\"vm.showReportModal(comment.cmnt_id)\" class=\"list-group-item\">\n\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"list-group-item-heading\">Report</h6>\n\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t<a class=\"list-group-item\" ng-if=\"vm.showRefLink\" ng-click=\"vm.showCopyModal(comment.cmnt_id)\">\n\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"list-group-item-heading\">Get Link</h6>\n\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"sub-comment-wrapper\">\n\t\t\t\t\t\t<!-- <button class=\"more-btn\" ng-show=\"!vm.commentMore[comment.cmnt_id]\" ng-if=\"vm.shouldShowMoreButton(comment.cmnt_id)\" ng-click=\"vm.commentMore[comment.cmnt_id] = !vm.commentMore[comment.cmnt_id]\">More &gt;&gt;</button>\n\t\t\t\t\t\t<button class=\"more-btn\" ng-show=\"vm.commentMore[comment.cmnt_id]\" ng-if=\"vm.shouldShowMoreButton(comment.cmnt_id)\" ng-click=\"vm.commentMore[comment.cmnt_id] = !vm.commentMore[comment.cmnt_id]\">&lt;&lt; Less</button>\n\t\t\t\t\t\t<div class=\"clearfix\"></div> -->\n\t\t\t\t\t\t<div class=\"comment-field\" ng-if=\"!vm.isCommentEditingInProcess\" id=\"comment-field-{{comment.cmnt_id}}\" ng-show=\"vm.subLevelComments[comment.cmnt_id]['showSublevelCommentTextField']\">\n\t\t\t\t\t\t\t<div class=\"unauthorized\" ng-if=\"!vm.isUserLoggedIn()\">Login to add comment.</div>\n\t\t\t\t\t\t\t<div class=\"profile-pic\"><img ng-src=\"{{vm.getUserPic()}}\" width=\"100%\" height=\"100%\" alt=\"\"></div>\n\t\t\t\t\t\t\t<div class=\"tf\">\n\t\t\t\t\t\t\t\t<!-- <div class=\"input-comment with-full-border\"\n\t\t\t\t\t\t\t\t\tng-class=\"{'one-liner':!vm.showBigCommnentField}\"\n\t\t\t\t\t\t\t\t\tid=\"input-comment-inner\"\n\t\t\t\t\t\t\t\t\tng-focus=\"vm.showBigCommnentField && vm.commentFieldFocused($event)\"\n\t\t\t\t\t\t\t\t\tng-keyup=\"vm.showBigCommnentField && vm.commentFieldKeyUp($event)\"\n\t\t\t\t\t\t\t\t\tcontenteditable=\"true\"\n\t\t\t\t\t\t\t\tplaceholder=\"Write a reply\"></div> -->\n\t\t\t\t\t\t\t\t<textarea rows=\"1\" type=\"text\" id=\"{{comment.cmnt_id}}_textarea\" placeholder=\"Write a reply\" ng-keydown=\"vm.keyDownEventOnTextField($event,comment.cmnt_id,comment.cmnt_id,'1')\" ng-model=\"vm.comment[comment.cmnt_id]\" name=\"\"> </textarea>\n\t\t\t\t\t\t\t\t<!-- <div class=\"one-line-submit-button\" ng-if=\"!vm.showBigCommnentField\">\n\t\t\t\t\t\t\t\t\t<div ng-class=\"{'disable':vm.isPostSubmitting}\" ng-click=\"vm.addComment($event,comment.cmnt_id,comment.cmnt_id,'1')\" class=\"post-comment-btn\">Submit</div>\n\t\t\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t\t\t<div class=\"one-line-submit-button\" ng-if=\"vm.comment[comment.cmnt_id].length > 0\" ng-click=\"vm.comment[comment.cmnt_id] = ''\">\n\t\t\t\t\t\t\t\t\t<div class=\"post-comment-btn\"  style=\"background: #D24D57;font-family: 'Roboto Condensed';font-size: 15px;padding-left: 7px;padding-right: 7px;margin-left: 0px;\">Cancel</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<!--  -->\n\t\t\t\t\t\t\t\t<div class=\"bottom-tf\" ng-if=\"vm.showBigCommnentField\">\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.canUploadMedia\" ng-class=\"{'disable':vm.isPostSubmitting}\" class=\"camera-icon\" ngf-select ngf-multiple=true ngf-accept=\"'image/*,video/mp4'\" ng-model=\"vm.commentAttachments[comment.cmnt_id]\" ngf-change=\"vm.changeCommentAttachment(comment.cmnt_id,$files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event)\"  ngf-keep=\"true\" title=\"Attach Photo\"><i class=\"fa fa-camera\"></i></span>\n\t\t\t\t\t\t\t\t\t<div ng-class=\"{'disable':vm.isPostSubmitting}\" ng-click=\"vm.cancelComment($event)\" class=\"post-comment-btn\">Cancel</div>\n\t\t\t\t\t\t\t\t\t<div ng-class=\"{'disable':vm.isPostSubmitting}\" ng-click=\"vm.addComment($event,comment.cmnt_id,comment.cmnt_id,'1')\" class=\"post-comment-btn\">Submit</div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t<div class=\"progressbar\"></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t<div class=\"comment-attachment-wrapper\" >\n\t\t\t\t\t\t\t\t<span class=\"status-attachment image\" ng-repeat=\"file in vm.photoAttachments[comment.cmnt_id] track by $index\" >\n\t\t\t\t\t\t\t\t\t<span ng-click=\"vm.removeCommentPhotoAttachment(comment.cmnt_id,$index)\" class=\"remove-icon\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t\t<span class=\"image\" ngf-thumbnail=\"file\" ngf-size=\"{width: 100, height: 100, quality: 1}\" ngf-as-background=\"true\"></span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span class=\"status-attachment video\" ng-repeat=\"file in vm.videoAttachments[comment.cmnt_id] track by $index\" >\n\t\t\t\t\t\t\t\t\t<span class=\"videoName\">{{file.name}}</span>\n\t\t\t\t\t\t\t\t\t<span class=\"remove-icon\" ng-click=\"vm.removeCommentVideoAttachment(comment.cmnt_id,$index)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.commentAttchmentError\">\n\t\t\t\t\t\t\t\t<div class=\"alert alert-danger\">{{vm.attachmentErrorStr}}</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- ng-show=\"vm.subLevelComments[comment.cmnt_id]['showSublevelAllComments']\" -->\n\t\t\t\t\t\t<div ng-show=\"vm.subLevelComments[comment.cmnt_id]['showSublevelCommentTextField']\">\n\t\t\t\t\t\t\t<div class=\"comment\" ng-repeat=\"comment in vm.subLevelComments[comment.cmnt_id]['comments'] | orderBy:'cmnt_ts':true \">\n\t\t\t\t\t\t\t\t<div class=\"head-wrapper\">\n\t\t\t\t\t\t\t\t\t<div class=\"profile-pic\">\n\t\t\t\t\t\t\t\t\t\t<img ng-src=\"{{comment.usr_info.usr_pic}}\" user-default-pic width=\"100%\" height=\"100%\" alt=\"\">\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div style=\"width: 100%\">\n\t\t\t\t\t\t\t\t\t\t<span>\n\t\t\t\t\t\t\t\t\t\t\t<a href=\"/dashboardApp/profile/{{comment.usr_id}}\" class=\"username\">{{comment.usr_info.dsp_nm}}</a>\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"text\" ng-if=\"!vm.isCommentEditingInProcess || vm.currentCommentEditing != comment.cmnt_id\" id=\"{{comment.cmnt_id}}\" ng-class=\"{'full-comment':vm.commentMore[comment.cmnt_id]}\" ng-bind-html=\"vm.trustAsHTML(comment.cmnt_txt)\"></span>\n\n\t\t\t\t\t\t\t\t\t\t\t<!-- edit Comment Textarea -->\n\t\t\t\t\t\t\t\t\t\t\t<textarea\n\t\t\t\t\t\t\t\t\t\t\t\tng-if=\"vm.isCommentEditingInProcess && vm.currentCommentEditing === comment.cmnt_id\"\n\t\t\t\t\t\t\t\t\t\t\t\trows='1' data-min-rows='1'\n\t\t\t\t\t\t\t\t\t\t\t\tclass=\"\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-init=\"vm.comment[comment.cmnt_id] = comment.cmnt_txt\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-model=\"vm.comment[comment.cmnt_id]\"\n\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Write a reply\" name=\"\" style=\"display: block\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-keydown=\"vm.keyUpEventOnEditCommentTextField($event,comment.cmnt_id)\"></textarea>\n\t\t\t\t\t\t\t\t\t\t</span>\n\n\t\t\t\t\t\t\t\t\t\t<button class=\"more-btn\" ng-show=\"!vm.commentMore[comment.cmnt_id]\" ng-if=\"vm.shouldShowMoreButton(comment.cmnt_id)\" ng-click=\"vm.commentMore[comment.cmnt_id] = !vm.commentMore[comment.cmnt_id]\">More &gt;&gt;</button>\n\t\t\t\t\t\t\t\t\t\t<button class=\"more-btn\" ng-show=\"vm.commentMore[comment.cmnt_id]\" ng-if=\"vm.shouldShowMoreButton(comment.cmnt_id)\" ng-click=\"vm.commentMore[comment.cmnt_id] = !vm.commentMore[comment.cmnt_id]\">&lt;&lt; Less</button>\n\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t\t<div ng-if=\"comment.cmnt_img\" class=\"comment-image-attachment-wrapper\">\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"comment-image-attachment\" ng-repeat=\"pic in comment.cmnt_img track by $index\">\n\t\t\t\t\t\t\t\t\t\t\t\t<img ng-src=\"{{pic}}\" class=\"pic_{{comment.cmnt_id}} {{$index}}\" ng-click=\"vm.openImageInGallery(comment.cmnt_id,$index)\" data-mfp-src=\"{{pic}}\" alt=\"image\">\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div ng-if=\"comment.cmnt_vid\">\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"post-video-attachment\" ng-repeat=\"video in comment.cmnt_vid track by $index\">\n\t\t\t\t\t\t\t\t\t\t\t\t<video-controls-jw style=\"height: 100%;width: 100%;display: block\" player-id=\"{{comment.cmnt_id}}_{{$index}}\" player-url=\"{{video}}\" auto-start=\"false\"></video-controls-jw>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"action-btns\">\n\t\t\t\t\t\t\t\t\t\t\t<!-- Show Like/unlike button in profile page -->\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"!vm.isQaPage\">\n\t\t\t\t\t\t\t\t\t\t\t\t<span ng-click=\"vm.toggleCommentLike(comment.cmnt_id,0)\" ng-if=\"vm.isCommentLikedByUser(comment.cmnt_id) === true\">Unlike</span>\n\t\t\t\t\t\t\t\t\t\t\t\t<span ng-click=\"vm.toggleCommentLike(comment.cmnt_id,1)\" ng-if=\"vm.isCommentLikedByUser(comment.cmnt_id) != true\">Like</span>\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<!-- Show Upvote button only in forum page -->\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"action-btn\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-if=\"vm.isQaPage\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-class=\"{'disable':vm.isCommentLikedByUser(comment.cmnt_id) === true}\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-click=\"vm.likeDislikeComment(comment.cmnt_id,1)\">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fb-icon fb-like-icon\"></span> {{vm.getLikeCount(comment.cmnt_id)}}\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<!-- show dislike button only in qa page -->\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"action-btn\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-if=\"vm.isQaPage\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-class=\"{'disable':vm.isCommentLikedByUser(comment.cmnt_id) === false}\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-click=\"vm.likeDislikeComment(comment.cmnt_id,0)\">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fb-icon fb-like-icon fb-icon-rotate-180\"></span> {{vm.getDisLikeCount(comment.cmnt_id)}}\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<!-- Show Comment Button in forum page -->\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"vm.isQaPage\" ng-click=\"vm.showSublevelCommentTextField(comment.cmnt_cmnt_id,comment.cmnt_id)\">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fb-icon fb-comment-icon\"></span> Comment\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<!-- Show reply button in profile page -->\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"!vm.isQaPage\" ng-click=\"vm.showSublevelCommentTextField(comment.cmnt_cmnt_id,comment.cmnt_id)\">\n\t\t\t\t\t\t\t\t\t\t\t\tReply\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<!-- Show Like count only in profile page -->\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"action-btn\" ng-if=\"!vm.isQaPage\" my-drop content=\"vm.commentsUsers[comment.cmnt_id]\" ng-click=\"vm.fetchCommentUsers(comment.cmnt_id)\">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fb-emotion fb-emotion-like\"></span>\n\t\t\t\t\t\t\t\t\t\t\t\t{{vm.getLikeCount(comment.cmnt_id)}}\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"action-btn\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-click=\"vm.cancelEditComment()\"\n\t\t\t\t\t\t\t\t\t\t\t\tng-if=\"vm.isCommentEditingInProcess && vm.currentCommentEditing === comment.cmnt_id\" >\n\t\t\t\t\t\t\t\t\t\t\t\tCancel\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<span class=\"post-options\" ng-click=\"vm.toggleOptions($event)\">\n\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"overlay\">\n\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"list-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<a ng-if=\"vm.isAuthorOfComment(comment.cmnt_id)\" ng-click=\"vm.editComment(comment.cmnt_id)\" class=\"list-group-item\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"list-group-item-heading\">Edit</h6>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<a ng-if=\"vm.isAuthorOfComment(comment.cmnt_id)\" ng-click=\"vm.showDeleteCommentModal(comment.cmnt_id)\" class=\"list-group-item\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"list-group-item-heading\">Delete</h6>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<a ng-click=\"vm.reportComment(comment.cmnt_id)\" class=\"list-group-item\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"list-group-item-heading\">Report</h6>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"show-more-comment-link\" ng-if=\"!vm.subLevelComments[comment.cmnt_id]['hide1LevelMoreLink']\" ng-click=\"vm.showMore1LevelComment(comment.cmnt_id)\">\n\t\t\t\t\t\t\t\tShow More Comments\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t<!-- </div> -->\n\t\t\t</div>\n\t\t\t<div class=\"show-more-comment-link\" ng-if=\"!vm.hide0LevelMoreLink\" ng-click=\"vm.showMore0LevelComment()\">\n\t\t\t\tShow More Comments\n\t\t\t</div>\n\n\t\t</div>\n\t</div>\n\t<div class=\"edit-comment-modal\" ng-if=\"vm.showEditCommentModal\">\n\t\t<div class=\"post\">\n\t\t\t<div class=\"content-wrapper\">\n\t\t\t\t<div class=\"head\">\n\t\t\t\t\t<div class=\"profile-name\">\n\t\t\t\t\t\t<div contenteditable=\"true\" class=\"edit-comment-area\" style=\"width: 100%\"></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t<div class=\"status-attachment-wrapper\" >\n\t\t\t\t\t\t<span class=\"status-attachment\" ng-repeat=\"file in vm.alreadyAddedPhotos track by $index\" >\n\t\t\t\t\t\t\t<span ng-click=\"vm.removeEditCommentAlreadyAddedPhoto($index)\" class=\"remove-icon\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t<span class=\"image\"><img ng-src=\"{{file}}\" width=\"100px\" height=\"auto\" alt=\"\"></span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"status-attachment\" ng-repeat=\"file in vm.editCommentPhotoAttachments track by $index\" >\n\t\t\t\t\t\t\t<span ng-click=\"vm.removeEditCommentPhotoAttachment($index)\" class=\"remove-icon\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t<span class=\"image\" ngf-thumbnail=\"file\" ngf-size=\"{width: 100, height: 100, quality: 1}\" ngf-as-background=\"true\"></span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"status-attachment video\" ng-repeat=\"file in vm.alreadyAddedVideo track by $index\" >\n\t\t\t\t\t\t\t<span class=\"videoName\">{{file}}</span>\n\t\t\t\t\t\t\t<span class=\"remove-icon\" ng-click=\"vm.removeEditPostAlreadyAddedVideo($index)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"status-attachment video\" ng-repeat=\"file in vm.editCommentVideoAttachments track by $index\" >\n\t\t\t\t\t\t\t<span class=\"videoName\">{{file.name}}</span>\n\t\t\t\t\t\t\t<span class=\"remove-icon\" ng-click=\"vm.removeEditCommentVideoAttachment($index)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"progressbar\">\n\n\t\t\t\t\t</div>\n\t\t\t\t\t<div ng-if=\"vm.editCommentAttchmentError\">\n\t\t\t\t\t\t<div class=\"alert alert-danger\">{{vm.attachmentErrorStr}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"action-btns\">\n\t\t\t\t<span class=\"action-btn\" ng-if=\"vm.canUploadMedia\" ngf-select ngf-multiple=true ngf-accept=\"'image/*'\" ngf-change=\"vm.changeEditCommentAttachments($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event)\" ng-model=\"vm.editCommentAttachments\"  ngf-keep=\"true\" title=\"Attach Photo\"><i class=\"fa fa-camera\"></i></span>\n\t\t\t\t<span class=\"action-btn pull-right post-btn\" style=\"margin-left: 10px;\" ng-click=\"vm.closeEditCommentModal()\">Close</span>\n\t\t\t\t<span class=\"action-btn pull-right post-btn\" ng-class=\"{'disabled':vm.isPostUploading}\" ng-click=\"vm.saveEditedComment(vm.currentCommentEditId)\">\n\t\t\t\t\t<span ng-if=\"!vm.isPostUploading\">Save</span>\n\t\t\t\t\t<span ng-if=\"vm.isPostUploading\">Saving <i class=\"fa fa-circle-o-notch fa-spin\"></i></span>\n\t\t\t\t</span>\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<!-- delete confirm dialgo -->\n\t<div class=\"ew-small-modal-wrapper\" ng-if=\"vm.shouldShowDeleteCommentModal\">\n\t\t<div class=\"ew-modal\">\n\t\t\t<div class=\"msg\">\n\t\t\t\tAre you sure want to delete reply?\n\t\t\t</div>\n\t\t\t<div class=\"action-btns\">\n\t\t\t\t<div class=\"ew-rounded-btn red\" ng-click=\"vm.deleteComment(vm.currentlyDeletedComment)\">\n\t\t\t\t\t<i class=\"fa fa-trash right\"></i>\n\t\t\t\t\t<span class=\"text\">Delete</span>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn\" ng-click=\"vm.closeDeleteCommentModal()\">\n\t\t\t\t\t<i class=\"fa fa-times right\"></i>\n\t\t\t\t\t<span class=\"text\">Cancel</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<!-- report dialog -->\n\t<div class=\"ew-small-modal-wrapper\" ng-if=\"vm.shouldShowReportModal\">\n\t\t<div class=\"ew-modal\" style=\"min-width: 400px\">\n\t\t\t<div class=\"\">\n\t\t\t\t<div class=\"msg\">Report Answer</div>\n\t\t\t\t<form>\n\t\t\t\t\t<div class=\"frm-element\">\n\t\t\t\t\t\t<input type=\"radio\" class=\"\" name=\"reason\" ng-model=\"vm.reportReasonValue\" value=\"wrong\">Wrong Answer</input>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"frm-element\">\n\t\t\t\t\t\t<input type=\"radio\" class=\"\" name=\"reason\" ng-model=\"vm.reportReasonValue\" value=\"spam\">Spam</input>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"frm-element\">\n\t\t\t\t\t\t<input type=\"radio\" class=\"\" name=\"reason\" ng-model=\"vm.reportReasonValue\" value=\"notrelated\">Not Related</input>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"frm-element\">\n\t\t\t\t\t\t<input type=\"radio\" class=\"\" name=\"reason\" ng-model=\"vm.reportReasonValue\" value=\"other\">Other</input>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"frm-element\">\n\t\t\t\t\t\t<textarea class=\"\" style=\"width: 100%\" placeholder=\"Reason to report answer\" ng-model=\"vm.reportReasonOtherValue\" ng-if=\"vm.reportReasonValue === 'other'\"></textarea>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t\t<div class=\"action-btns\">\n\t\t\t\t<div class=\"ew-rounded-btn\" ng-click=\"vm.reportComment(vm.currentlyReportedComment)\">\n\t\t\t\t\t<i class=\"fa fa-pencil right\"></i>\n\t\t\t\t\t<span class=\"text\">Report</span>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn\" ng-click=\"vm.closeReportModal()\">\n\t\t\t\t\t<i class=\"fa fa-times right\"></i>\n\t\t\t\t\t<span class=\"text\">Cancel</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<!-- copy link dialog -->\n\t<div class=\"ew-small-modal-wrapper\" ng-if=\"vm.shouldShowCopyModal\">\n\t\t<div class=\"ew-modal\">\n\t\t\t<div class=\"msg\" style=\"font-family: aller;font-size: 15px;\">\n\t\t\t\t{{vm.link}}\n\t\t\t</div>\n\t\t\t<div class=\"action-btns\">\n\t\t\t\t<div class=\"ew-btn\" ng-click=\"vm.closeCopyModal()\">Cancel</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n";

/***/ },

/***/ 245:
/***/ function(module, exports) {

	angular
	.module('service')
	.service('itemQueries', Service);

	Service.$inject = ['$http','Upload','BASE_API'];

	function Service($http,Upload,BASE_API){
		this.save = function(data){
	        return $http.post(BASE_API + '/itemQueries/save',data);
	    }

	    this.getAll = function(data) {
	        return $http.post(BASE_API + '/itemQueries/getAll',data);
	    }
	}


/***/ },

/***/ 250:
/***/ function(module, exports) {

	var app = angular.module("directive");

	app.directive('compile', ['$compile', function ($compile) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attrs) {
	            scope.$watch(function () {
	                return scope.$eval(attrs.compile);
	            }, function (value) {
	                element.html(value && value.toString());
	                
	                var compileScope = scope;
	                if (attrs.bindHtmlScope) {
	                    compileScope = scope.$eval(attrs.bindHtmlScope);
	                }
	                
	                $compile(element.contents())(compileScope);
	            });
	        }
	    };
	}]);

/***/ },

/***/ 340:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABQCAMAAACTf/MwAAAB4FBMVEUAAAA1S3k7RCo6WZg6WZg6WZg6WZgqKik6WZg6WZg6WZg6WZg6WZg6WZg0OCg6WZg6WZg6WZg7WJc6WZg6WZiHrzg6WZg6WZc6WZg6WZg6WJY6WZg0MTE6WZgsKieWxjqSwTo6WZg6WZg6WZg6WZg6WZg6WZg6WJdlfzE6WZhYbS86WZhVZy46WZg6WZhOXi06WZg6WZg7Nyg3OSk6WZgtLiY5VY+YyjuQvDk6WZiIsTeErDeCqTdsijNqhzI6WZg6WZhedTBSZS46WZg6WZiUxDqRxjuNuTmFrTc6WZh8oDZ7nTVohDI6WZhacS86WZhCTis+SCo6WZiVyDuLvzo8WJY6WZh3hC9ykjQ6WZhoN05bcjBcKSxJWCxJSSlKKChBQCg6WZg6WZgwKSWRvjqJxDyUnDGLtTiKtDhZZHl+uDuFnDN9sDp8oTZkXWp3mjV1lzRrPF5ujDNxfy9sOVZCU4tEUIZZP2BrHRpSay86WZj///+ZyjvGz+FNaaI8W5misM7L0+RCYJzg5e9MaKHm6vLI0OO0wNils9Dw8vfP1+avvNaQocWX0DzDzN/Ayt6+yN2ers1GZJ8/XpvZ3+vT2uhngLBbdapSbaVJZqA+XJqmtNF+krx1i7dyiLWU3EAR9yjYAAAAenRSTlMABC4GJfvqB9RRQgrmsR4WDPXy79nWt5N+eTsdGBIK+fHeysW9r6Kai3FubGdfWVhENikkIQ8O/eri2dHMm5eLg3xiTkj18eTSz767k3Z0ZT81K/jkwquqqY+IdlpNRzs3MC8S7ePi393TzczHv7u0r62fn52Wgn5sZN1DGScAAAPqSURBVFjDrdjnV9pQGMfxX9hTUFEsAmqtkylQW+us2j21e++9d3sNWnBW6+je7b/aKGGV3JjRzxtfgN9zk+c8gQOk2GxEhl/T/rhn19abe1tHIFvZdg1WGQ8/3MHyzkAuZnvVWsbbuoXNOSf/MHeOxlb/PGDztmgg18Xr1zSAdy9boGcz5NJs26qB8SRbqJ1RkKkfQXtRZVcZ5Geq6jRlu4oyg5AifmZ/b+/+oCZ7mrpYa1Glzwgpguyqut29I5nM1WBVYaVX4v09V89mVA2uXdSO7h35yNaDEiuI97C8+oNcZjebV7//IqRi+vL/FoGmO3eQk+0aP6Q7fCPXqYrFd7P7+vpa2yOH41cYyMEcrMt1Wst62Ijfb+QKCtZxJ8vrHrzHRhQUjBwG5+8e4bdwbzc7WPyOdRM+nXmgpfnZi5cXvJsj+47u3LK4uHikjg3yL3c6Eh3mQ4HQ8Kijkx5JHGqsIDxDywVv2fntT06c2Ldz8RXA+L2egL2Bf13fZQ/ptBDiCDSQIqbnZ+NX/Ijd/hP0xs66TaSYrUaHUpfspFTFo6enT9+/dWxTJRFgCpccKOoiCtQ4UcTSRRTpt6LAqIsotKHgunzVRKmKIWQxG4hyplrwPAaiwgH+sjqbiRo2/jiWciLmw9efcyup1Mzy7FsiKKCVcJgvC2/GeJNE0CYfOJf1hG7i9/RYziciSN8BQDtARHzmKjnztJvMAI49hO798liBFBF2zAlYiIhffED8NHodmBqxIb3L3tzZhYVZblIUw7CK7cG3lUzl4+fv4ouO2gpCN57MZH4QcXZ0EAmZdxPimT0YkpJ5vU7Ghbb/kemCm4iYkpgxoIk67HQ6/TWbSWfQM5sIxRy31PP8Vk6nZlKcWQWZT2MlJgmFjZ5JlWbmCUUDmmRkkoTChBYZmRVC0Qjqh8LH0swyoWiGmTqpZHIplZ1UcinJoW54GzzUJ9b41NSXmUxmbnyKM54mFFEkysUefkv8ahJR+lFYG9Xv1J5OaPvVZ05pgbD6TBiArlJtplIHwNmkNmN3AmBCajOHGHB0BnWZCh1WWd3qMs1OrLHoVWXMyPA1qclsrAXPrCbThiyHXXnGdRk5Q+WKMzXI07YozZguoUBHg3Am+7z5Tv2SjiIbBIf+bWF6kvPm7QRtTDoUcSj6uq8P4B8Wm4JMtQ+8wsuSq3IYJazHZWdOQYBuI6ETXwNQpi5FpQXCorJucxtohg3SK/1aUJnLpVbcTogIS+wMOCEqaiIFyt3mqMdjMdcUT7ErbMU6ag+48r9TePi3a32W6vyI3Amsj3EMDTQ1Nlb3hxJa5FlDx002g81lbxP4jeMvqxM7zwMKQwoAAAAASUVORK5CYII="

/***/ },

/***/ 341:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABQCAMAAACTf/MwAAAB9VBMVEUAAABCyPRCyPQ7RCpCyPRCyPQqNDZCyPRCyPRCyPRCyPRCyPRCyPRDx/NCyPRCyPSErDdCyPRCyPRCyPQuLSYqKyeZyTuWxjpCyPRCyPRCx/NCx/NCyPRTZy5CyPRBQiw8RSpCyPQ0Nic1Pj9CyPRCyPRCyPSSwDqQvDlCyPRCyPSHsDhCyPRCyPR8nzVCyPRCyPRsiTNqhjJCyPRCyPRCyPRYbS9CyPQ6Oio3OitCyPQsLCmCqTdCyPRedTBCyPRCyPROXSxCyPRCyPRCyPQrMDGUxDqRxzyNuTmGrjd5njVacS9CyPRRYy5CyPRCyPRJVCtFQilCyPRCyPSXxzuLvzqLtTiIsTdCyPRCyPRCyPR3hC9ykjRvhjFmgjJlfzFkfjFsaHZbcjBCyPRXZy9CyPRGSylCyPRCyPSWzDyJxDyUnDFftbx+uDuFnDN9sDpFw+xooaN1lzRvfZRwcoZJt91LsNVef5VrHhtgREhCyPRcKydCyPRDuN84doxCyPT///+ZyjvZ9P31/P6C2/hj0fbs+v75/v+M3vjf9v3o+f5HyvSs5/qZ4vmW4flVzvVSzfWX0Dzv+/7P8fzD7vyk5fqf4/mR4Plm0/Zc0Pbj9/3U8/3J7/y56/uw6PqL3fh62Pdy1vdq1PZaz/WE2/he0PaU3EAWTL9/AAAAf3RSTlMABgIuJQ0DFvrx5lEJ69jT0bF4OhAI/vjtt5mMfWRDNjAiHRgE/PXw6uLd18rFvq+im5WSgnJuRCgjEAvMvHxtX1dKLxwG9fHk1Lh0ZWBaT0w9Niv65N7ZzsCrqqmfj4uJiHZqaVhDQB/64+LTzczHxLuvrZ2Wgn5sX11VRz4OJaBuqQAABANJREFUWMOt2FdXGkEYgOGPJiBdICpFQOyJBhGpJvb0xJbee++9f6yJMaiJXdN78juDh82yuCPOEJ8bLoD3zE47HICGzQ4ZdkX4WNu2uv1NJzzArHKzIhMZuL2F450GVkWbt3shzX9yLyfoZx/M9X3RpZcdXNY6BbDyXLmsAPA3cSJtNmClOFDnBftRTixcVECm1gNhTmxbDNgz60u9lds4MTfQiJ4ONjcfd3uFTPT4OnGl2Q403NySveZmTyZTd3Z9TsUGVPpruYz17qXM9i3mLZygbidlBWJtHK92Zzpj5rKPVBv00O/bYPZru8FrFgZyNOxNAL2B/UJnezRm5nYEgyfDnedjtiK2Y7SzVNj2JyrbuM5Ews4XGI/jVo5n3t3EdQKz3t5ee28RnL9Ryg+nyczthix5GuQnjyt1IUfg3oOHHr+tc8e+rZemp6dLS9e5+TE+V/usul1nOrqG1KoVW1W+XY1a5JU5BvyV/ZuPHTnStHXanU4k/M/aLfX8+5p6S4dSBiTq9gbMYbz/JGZLQPTan7N+xSOnCXMVu5Qg1W1BKcOdu6dO3bp6c1MZEhj3SAYUaUB21SEV5Kiox4I49SAyZMQClchAoC7HQmn7sitdgoUz9QDPakBB4Y+lCuD/KL7Ir1INSoz/+jSWmniHNM7IIC0eIFQmkktmviGFTXFI69agxMtkRmqKIqMZBABZCKXm+MzoV6pJrgJQm1BqOMl7gxQOqQAq8L8zGiVUufJm3iKNLtCXr0HGBT3avJmJ3yMiP8eRyAKDmDcz+kJs7Mt7JDkIfXkyBMPEjWSEVrYMeenqoYUxM0w8nbCRMTOGBAbYxJhJkTOHGDMTxIdiHc0H4oo3sM3NzBz5IjOBI3/m80ux7+NI1Agl+TOLSCMAOlyDo9kK1rXIRMBXgwQf2a6tIdA3IsEnYYqRwkEVyJxI8EXY+z8oMk4ZyM8hwWLyn9mfuKo9cgBfGUr9EB3phdf/fP1G3DdaJQCoSPt4KpXMGhXMDH9HKYsKAOQdSLCQJHs1ghK7qiBNaUCpdy9oTrjwTGl60gU4Pk993wRU/A8TDUpNztLefjrIiBMvi/cpurt4w0Xg6aqRYOQjKbMgOZZy4KktSDI5PyapzE5hLmM3CLpqkBxanPvwSiQ1P4nLuOQgkDmwQKZuELE2YEGqXZCjRIOF2KAEQeE/9zXtsMyFYmRXroblSqqRVdlTkNAfZp7fFiBQbmCd3x4gGWRbde0FIIswTXM7rKSrDKk5ZbAiXQ1SalFBHue0SCWUtwLyiAlFahy6iNVaoXPlrmL9Hj2soqfViDyDw8p/XBavKM/uuhafHFZVpe4LbWxsLHd2+GSQpX982FRsKDZaWgn/cfwFnweIkJqZ0fUAAAAASUVORK5CYII="

/***/ },

/***/ 342:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABQCAMAAACTf/MwAAABwlBMVEUAAAA7QyodvtAdvtAnMDAdvtAdvtAqLSodvtAdvtAdvtAdvtAdvtAdvtAdvtAdvtAdvtA7RCodvtAdvtAdvtAsLSgdvtAdvtAdvtBYay8dvtAdvtAdvtAdvtCWxTqSwTodvtCQvDkdvtAdvtCHsDgdvtAdvtAdvc8dvtBacS8dvtBTaC4dvtAdvtAdvtA3Oyo0OCgvLCaYyjsdvtAdvtAdvtCErDeCqTd8oDYdvtAdvtAdvtBsijMdvc9qhzJkfjEdvtBedTAdvtBSZC4dvtAdvtAdvtBFRik+SSo6OSkdvtAdvtAyOzUwOzWWyDsdvtCTxDqNuTmGrzeFrTcevc4dvtAdvtBPYS1MWixITiodvtCLvzqLtTiIsTd5oDV3hC9ykjQdvtBphTJmgjJcZGYdvtAdvtBENS0juMmRyDyQxTyJxDyUnDFEraJ+uDuFnDN9sDp9mDRSm413mjV1lzRaeH9ujDNxfy9dbXMorr0rp7ZJeYBqHhodvtBYQUFaKiUir78mcXodvtD///+Zyjtxz9w4wtP1+/zW8PTA6O5gy9nh9PdNx9aO2OLr9/m05Ouo4Oic3OWA1N+X0DzL7PGU3EDJxY28AAAAgnRSTlMALQ0FAwEHB/CxOQP6eFHm4S8kIBYMyrd+bUMSCv348ezr6drX1sWSi3VuZVlEMCQeEP3y1dHRzL68rqKbl5eKg3xyYV5OSD42KSYcGxf59vTk1dLBmmlbU0kr5N7Zuqqpp5SPiGRAN/Ly7+Pi083Mx727tK+tn5+dloJ+bGBfVT4OuTr3xAAABDpJREFUWMOt12dXGkEUBuBLl15VCNYkQJCeosbYYi+pdk3vvfeezO5SpIma/xvM7llgZ8Adk+ebK77nzr3D7AhyJHTA0yk87/sud1/tH5kBaufOKviQ6bcdrCCoAUrqswt2KDs/cpwVrdMX8+LKLADE37AVxxVA68HTJ4pyLf1slb5moGW/1m0H3U22mkcNtBTXOmfAU5NyOQ70MUc77PHLNTHjsuYUC94eHLwTsosxs3dqUoZ1IEeI3XNhcXCGj+kOHa1OGUyALGudLO/o+F7MwqXeS5WQ7qDcKcX6WEFnsByzyFZ03p7RyN63w5U/WwV7r1jITY+dZsNMXxVzFmZji+z14eERz+p0LEG3XdTBDjFnJN7HrjU369QaAPqvo9jU3vF+dhVoadS6MrVm/eUF4VvY38uOV3/AaDRCY0avwzK6fObTl68PzifWrl95fHxnZ+dCBxsSavzlchos91YmpjZcyrpZKueK/wQSdA1Nn4+vn/1w48arRzuhckTzuZ89elsT/1uTTf9tTgskrh4bquH7/COWaNbMPv/tOaf4HjiJalndDsBF9Qj38N3Hu3dfP1s63YQIfGGsoMgRRK/drYQah2zoQI61QZUNoRZ6ZhWIvK3ooJomK5M2o4M7NQ8CQxf6B2ZhXMoz6F9YncKULqJGkhyXQ42sqPYpJpnPZJm/CrululmnvVAWNSGyXCbFVEtvIyJTFAC0o3UqyTCY9GbdveM6hUi2UwxJCREsKcsNJqYUGbJUkrSqOVC5SSniggpFrryQHJcX+kRuzxS0tTZISXNVD/MFhtlFRG6YJ5wkeWEBJUnTi/yScHowEAad4lPwsfApuBaYxB+mxRS5fDCGPeMYoZny2WCoTjEZROEwDGCd4YvJ0cWclj4qCcXQxSyR18TRHTl4NfyaEBUb6ImtSdPFnIRl4rgzdDF+MBNjtuhiboHlf8T0gIEYU6SLiYBT8lrYPECLTRvQ5icNPEUVc0oJqmOSZ/wbZZPqWqEFYxg7hambE9YAOJpIzUklKS4VDgBQSvdxgXbkeiUAGCewk5iyO/dUUOboIpZTSMpd0xzs0QbIp2gWzyEmn1EKFxMT+ZWZ4qTLzZJiLMDzDuBHFy+Tqx5hmtj4FicILO3S4rOMYDef4zNK9bblmBEELj3WhDRTUaj6AVuWLwqiyRNIaoshyki77DaCSDWEMFwWD0ltY8dnFKoYSLf0fFYSsoUNvN0MNczEC+BmUUwqZPKkO8Ac1HDVve5zHFfiOPKWNvWAxH0rotfqBSlzO3VK0xRgtAO0Ke0BIHC0UMa0zAOJ4Qjdku4DWcRK946rZ6qL4nWggrosJ+SmBJTQQFhmzmjDFNBEav4TuThkiRgMhyzu2inawm2wj/kxcWCHlw3Cx7XeQ5VvS1fAqYF9qVyTowN+f+uxCacWKtombp20Hrb69GMOLUj9AdGBs89w4nP4AAAAAElFTkSuQmCC"

/***/ },

/***/ 343:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABQCAMAAACTf/MwAAABoVBMVEUAAAC9Tjy9Tjy9Tjy9Tjw7Qyq9Tjy9TjwtKye9Tjy9Tjy9Tjy9Tjy9Tjy9TjyErDe9Tjy9Tjw7RCq9Tjy9Tjy9Tjy9Tjy9Tjy9Tjy9Tjy9Tjy9Tjy9Tjy8TTtqhjK9TTy9Tjw9Lii9Tjw3KyiVxTqSwDqQvDmHsTh7oTZsijO9Tjy9Tjy9Tjy9TjxYbS+9Tjy9Tjw/RSk5OSi9Tjw0OCgtLiYvKSaYyjuWxzuSxTu9TjyCqTdkfjFedTC9Tjy9TjxTaC5SZS69Tjw5Oye9Tjw6NCdCMCi9TjyNuTmGrje9Tjx0jTK9Tjy9TjyjNCe9TjxacS+9TjxPYS29TjxCTiu9TjyLvzqLtTiIsjd7mzS9Tjx2mTR0gi9mgjJbcjBYaC5NWixJWCxJSSm6TTtVJyK9Tjy9TjyJxDyUnDG5XDZ+uDuFnDN9sDq0VjK4NiiwMya1RzdsHRl6KiFiJR5OJSMzIyMuMSe9Tjz///+ZyjvgtbDCXk/oyMX37u3z5ePGbWD79vbs0s/bqqTXn5jTlIvjvrrKe3Dw3NnPh36X0T2U3EAosORPAAAAd3RSTlMABgJQ+i0kDQjksDnwCvPRQy8vFuzp3tnVysW9t5iWkn0MBAP38OrYvZyLg3lxbkpENiQhHhAE/fny0cyKfGxmZWJfKB4bFxPk1Kyqo5+FdnRbW1g/EOTe2byysqSPdmpVTUc/Oygb4+LTzczHu62dgmxfVSwTEdnKeO0AAAQTSURBVFjDpdb3X9pAAAXwB0H2EqggFRQHBSlDoK3burd1du+9dxuiuFftX11i0kCa5ELS7y/w0eORvMvdgXpcsYJjbcgkB2bitxKDPdBscqiBC1m/18nwHkKrjqFZHyqmBq8xgifaL+bdzXEAxXtM1bUGaNXz+lUDMJVgagxcgVa+23EfrPNMrUwHtGq43dWDjChlpgjtMZc6fcUZUcwo6pF/GE0mH1z3/Y3pHX8gSolaUY/rDKvXk+zhYuKPLtWmJOvs90kXw7k0ysbM9nk6GUF8ud5Zyg8wvK7lSoyHqeqK1r8MrkarH1uFT4iJz2d8MdRv/ZaQMzue9zB3otHBzOp6/meHtmW0XC1jsDjAPI7FrFeh3dWhPobnGU0wqzoSrBUdePq+l1+FCY/ocTNWgMy4YaZSkcXI0pfnU1ceJ272XTs7O+vtZK7z3xDzFxzUyMhYttFvUcxqL4yE3TQvEHk+VXw6dP/+/J0+NqYjNvls2N7k5P5rstnHzAbI8Q830SKhz9/yP2IYf/v70eT3r60hWszWZoZU411ayv3x09LShzfhiwFaRmhFckG5IK1DygIRr43WpdsluqMgrdNlAwTTLbReTqo605dp/fonwHME6P9wg78tyyL9P2z85XjdxGEHRyo5w+eXs7FAHrWzrxJzceN8sk3EQSel0i45xtQIwJAiDyqXSptqJbcD/n7ikK29Umlb7a4sgJc85LBUoVKyyYz2NpWC2Ri1krNwtagVzNoix7RhwkkccMzFHJBj7HCQB+yxIaolN4NSKZj3izgsiDRNclrilcnLCq00wS6XoVryBcyRCxYckmMukgsW7OiPOeICts9fTkjdEGP2uWfmWLXkJthVC97iXvcIJYcQoRVtlvj1tKNWchiEQ4Hr5OjvQ3iqPHIBlErBe+yew74hbYJpOFQKPuZ2QO6tkhwKbnLBJ6ILk2d6AVdY6Vypfeq2iSX3W2DoJhZ8IKwKwibYbYBxjZb1S7Qid4klrxkBs1PhXGGVRVuywknjNLMnuPQ5rs7x/ibvlLAJ3rUAMI7JFyyheNKMtKPCHFAoWEyxZOcLsFytcueKhGLJCxac85pkCpZQPGkocKbtMgc3q8yWq1Zy8wR4lPRcke4wuwonTdoInt8uObhZZcIfBcFGCLJuScHSLz6UPWnajBAYIpKCpTVsyZUcakQNR5O04E356dsRXwxELptoPZrNEPHr+rlvGsY/nl3QEdMyDYH+23JmIeGa0xzTChnmZq39voRAadbVOb2Ql7NpiUlDSTZAE0iOA0WUu+56LSBYqzMnRUyBMdcvekwjVM7h8FJt4llsWnFBxcsbQeEn3aKDH26Y9lZXS6C1YISqdj+VsofDLd1jhdoWXStzIdsFW9CeNkvL/QOtPg9M5N6ZfwAAAABJRU5ErkJggg=="

/***/ },

/***/ 487:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(57);
	__webpack_require__(120);
	__webpack_require__(488);
	__webpack_require__(250);
	__webpack_require__(121);

	__webpack_require__(122);
	__webpack_require__(498);

	angular.module('blog')
	    .component('articlesPage', {
	        template: __webpack_require__(500),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['authToken','admin','pageLoader','$state'];

	function Controller(authToken,admin,pageLoader,$state) {
	    var vm = this;

	    vm.getBlogArticleUrl  = getBlogArticleUrl;

	    vm.$onInit = function(){
	    	pageLoader.show();
	    	admin.getAllSideLinksArticlePage()
	    		.then(function(res){
	    			vm.allArticles = res.data;
	    		})
	    		.catch(function(err){
	    			console.error(err);
	    		})
	    		.finally(function(){
	    			pageLoader.hide();
	    		})
	    }

	    function getBlogArticleUrl(cat,id){
	        return $state.href("home",{
	            cat : cat + "-tutorials",
	            id : id
	        })
	    }
	}

/***/ },

/***/ 488:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(59);
	__webpack_require__(51);
	__webpack_require__(57);
	__webpack_require__(232);
	__webpack_require__(245);
	__webpack_require__(125);
	__webpack_require__(229);
	__webpack_require__(158);
	__webpack_require__(112);
	__webpack_require__(489);
	__webpack_require__(490);
	__webpack_require__(491);

	angular.module("directive")
	    .component('ewArticle', {
	        template: __webpack_require__(493),
	        controller: Controller,
	        controllerAs: 'vm',
	        bindings: {
	            delegate : "=?",
	            content: "@",
	            name: "@?",
	            articleId: "@?",
	            showSidebar: "@?"
	        }
	    });

	Controller.$inject = ['$log', '$scope', 'itemQueries', 'authToken', 'utility', 'Mail', 'Socialshare','EditorConfig','blog','notesService','alertify'];

	function Controller($log, $scope, itemQueries, authToken, utility, Mail, Socialshare,EditorConfig,blogService,notesService,alertify) {

	    var vm = this;

	    var MainClass = ".blog-side-link-article-wrapper";

	    $scope.$watch(function() {
	        return vm.content
	    }, function(newValue, oldValue, scope) {
	        if (newValue.length > 0) {
	            vm.renderDOM();
	        }
	    });

	    vm.$onInit = function() {
	        vm.EditorConfig = EditorConfig;
	        EditorConfig.toolbarButtons = EditorConfig.toolbarButtonsMD = EditorConfig.toolbarButtonsSM = EditorConfig.toolbarButtonsXS = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertImage', 'insertVideo', 'insertTable', 'html'],
	        EditorConfig.quickInsertButtons = []
	        EditorConfig.pluginsEnabled = ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"]
	        vm.delegate = vm.delegate || {};
	        vm.activeRelatedTab = "comments";
	        vm.activeItem = vm.articleId;
	        vm.trustAsHTML = utility.trustAsHTML;
	        vm.renderDOM = renderDOM;
	        vm.saveQueryForItem = saveQueryForItem;
	        vm.getQueriesForItem = getQueriesForItem;
	        vm.scrollToTab = vm.delegate.scrollToTab = scrollToTab;
	        vm.subscribeUser = subscribeUser;
	        vm.validateInputField = validateInputField;
	        vm.getQAQuestionPageUrl = utility.getQAQuestionPageUrl;
	    }

	    function renderDOM() {
	        var current_end_pos = 0;
	        var indexPos = 0;
	        var link_arr = link_arr || [];
	        var content_arr = content_arr || [];

	        while (current_end_pos <= vm.content.length) {
	            var start_pos_of_hash = vm.content.indexOf("###", indexPos);
	            var end_pos_of_hash = start_pos_of_hash + 3;

	            var start_pos_of_next_hash = vm.content.indexOf("###", start_pos_of_hash + 1);
	            var end_pos_of_next_hash = start_pos_of_next_hash + 3;

	            if (start_pos_of_next_hash === -1) {
	                break;
	            }

	            var link_title = vm.content.substr(end_pos_of_hash, (start_pos_of_next_hash - end_pos_of_hash));

	            link_arr.push(link_title);

	            var start_pos_of_next_next_hash = vm.content.indexOf("###", start_pos_of_next_hash + 1);
	            var end_pos_of_next_next_hash = current_end_pos = start_pos_of_next_next_hash + 3;

	            var content_text = vm.content.substr(end_pos_of_next_hash, (start_pos_of_next_next_hash - end_pos_of_next_hash));

	            content_arr.push(content_text);

	            indexPos = start_pos_of_next_next_hash - 1;
	        }

	        vm.link_arr = link_arr;
	        vm.content_arr = content_arr;


	        setTimeout(function() {
	            bindOnClickEvent();
	            $("table").each(function(index, block) {
	                $(block).addClass("table").addClass("table-striped table-hover").wrap("<div class='table-responsive'></div>");
	            })
	        })
	    }

	    function renderDOM_old() {
	        var link_arr = link_arr || [];
	        var content_arr = content_arr || [];
	        $("div[ew-section='']").each(function(i, ele) {
	            var link_title = $(ele).attr("title");
	            var id = "link--" + i;

	            var anchorTag = $("<a></a>");
	            $(anchorTag).attr('data-id', id).html(link_title);
	            link_arr.push(anchorTag);

	            var div_ele = $("<div></div>");
	            $(div_ele).attr('id', id).html($(this).html());
	            content_arr.push(div_ele);
	        })
	        if (link_arr.length > 0 && content_arr.length > 0) {
	            console.log(link_arr, content_arr);
	            var link_wrapper = $("<div></div>").addClass("links").append(link_arr);
	            var content_wrapper = $("<div></div>").addClass("links-content-wrapper").append(content_arr);
	            $(MainClass).html(link_wrapper).append(content_wrapper);
	        }
	    }

	    function bindOnClickEvent() {
	        vm.linksWidth = $(".links").width()
	        $("[data-id]").on('click', function(e) {
	            var id = $(this).data("id");
	            $('html,body').animate({
	                scrollTop: $("#" + id).offset().top - 100
	            }, 'slow');
	        })
	    }

	    function bindScrollEvent() {
	        var linksOffset = $(".links").offset();
	        $(window).scroll(function() {
	            var scrollTop = $("body").scrollTop() + 60;
	            var wrapperTop = $(MainClass).offset().top;

	            if (window.innerWidth > 767) {
	                if ($("body").scrollTop() === 0) {
	                    $(".links").css("position", "initial").css("width", '25%');
	                    $(".links-content-wrapper").removeClass("fixed-side");
	                }
	                if ((scrollTop - wrapperTop) > 0) {
	                    $(".links").css("position", "fixed").css("width", '20%');
	                    $(".links-content-wrapper").addClass("fixed-side");
	                } else if ((scrollTop - wrapperTop) < 0) {
	                    $(".links").css("position", "initial").css("width", '25%');
	                    $(".links-content-wrapper").removeClass("fixed-side");
	                }
	            } else {
	                $(".links").css("position", "initial").css("width", '100%');
	                $(".links-content-wrapper").removeClass("fixed-side");
	            }
	        })
	    }

	    function scrollToTab(tab) {
	        if(tab !== 'comments' && !authToken.isAuthenticated()){
	            vm.showLoginAlertModal = true;
	            vm.needLoginErrModalMsg = "Need to login to access " + tab;
	            return;
	        }
	        vm.showLoginAlertModal = false;
	        vm.activeRelatedTab = tab;
	        if(tab === 'qa'){
	            vm.getQueriesForItem();
	        }else if(tab === 'notes'){
	            getNotes();
	        }
	        $('html,body').animate({
	            scrollTop: $("#tabs").offset().top - 100
	        }, 'slow');
	    }

	    function saveQueryForItem() {
	        var obj = {
	            itm_id: vm.activeItem,
	            usr_id: authToken.getUserId() || 'n/a',
	            qry_title: vm.queTitle,
	            crs_id : 'blog',
	            mdl_id : vm.articleId,
	            crs_nm : 'Blog',
	            mdl_nm : vm.name
	        }
	        itemQueries.save(obj)
	            .then(function(res) {
	                vm.itemQueries = vm.itemQueries || [];
	                vm.itemQueries.push(res.data);
	                vm.queTitle = "";
	                getQueriesForItem();
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }

	    function getQueriesForItem() {
	        var obj = {
	            itm_id: vm.activeItem,
	        }
	        itemQueries.getAll(obj)
	            .then(function(res) {
	                vm.itemQueries = res.data;
	                console.log(res.data);
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }

	    // function subscribeUser() {
	    //     vm.subscribeStatus = undefined;
	    //     Mail.subscribeUser({
	    //             usr_email: vm.subscribeEmail
	    //         })
	    //         .then(function(res) {
	    //             vm.subscribeEmail = "";
	    //             vm.subscribeStatus = true;
	    //             $log.debug(res);
	    //         })
	    //         .catch(function(err) {
	    //             vm.subscribeStatus = false
	    //             $log.error(err);
	    //         })
	    // }

	    vm.shareToFB = shareToFB;
	    vm.shareToTwitter = shareToTwitter;
	    vm.shareToEWProfile = shareToEWProfile;
	    vm.shareToQA = shareToQA;


	    function shareToFB(title) {
	        // e.stopPropagation();
	        var tempDiv = $("<div></div>");
	        tempDiv.html(vm.desc || '');
	        var desc = tempDiv.text();

	        Socialshare.share({
	            'provider': 'facebook',
	            'attrs': {
	                'socialshareUrl': window.location.href,
	                'socialshareText': title,
	                'socialshareDescription': desc,
	                'socialshareType': 'feed',
	                'socialshareVia': '1680795622155218',
	                'socialshareMedia': 'http://www.digitalplatforms.co.za/wp-content/uploads/2014/09/laptop-on-work-desk.jpg',
	                'socialsharePopupHeight': '500',
	                'socialsharePopupWidth': '500',
	            }
	        });
	    }

	    function shareToTwitter(title) {
	        // e.stopPropagation();
	        Socialshare.share({
	            'provider': 'twitter',
	            'attrs': {
	                'socialshareUrl': window.location.href,
	                'socialshareText': title,
	                'socialshareVia': 'hcPYSQevRhsR8016f0MllEWbU',
	                'socialshareHashtags': 'examwarrior , ' + title,
	                'socialsharePopupHeight': '500',
	                'socialsharePopupWidth': '500',
	            }
	        });
	    }

	    function shareToEWProfile(title) {
	        // e.stopPropagation();
	        var title = title;
	        var tempDiv = $("<div></div>");
	        tempDiv.html(vm.desc || "");
	        var desc = tempDiv.text();
	        utility.shareToEWProfile(title, desc);
	    }

	    function shareToQA(title) {
	        // e.stopPropagation();
	        var title = title;
	        var tempDiv = $("<div></div>");
	        tempDiv.html(vm.desc || "");
	        var desc = tempDiv.text();
	        utility.shareToQA(title, desc);
	    }


	    function validateInputField() {

	        var input = vm.subscribeInput;
	        console.log(input);
	        if(input.length <= 0 || !validateEmail(input)){
	            vm.showInputErrorMsg = true;
	        }else{
	            vm.showInputErrorMsg = false;
	        }
	    }

	    function validateEmail(email) {
	        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	        return re.test(email);
	    }

	    function subscribeUser() {
	        if(vm.showInputErrorMsg || !vm.subscribeInput || (vm.subscribeInput && vm.subscribeInput.length <= 0)){
	            vm.showInputErrorMsg = true;
	            return;
	        }
	        Mail.subscribeUser({
	            subs_email : vm.subscribeInput,
	            subs_typ : 'blog'
	        })
	        .then(function(res) {
	            vm.showSuccessMsg = true;
	            vm.showErrorMsg = false;
	            vm.subscribeInput= "";
	            vm.showInputErrorMsg = false;
	            vm.subs_typ = "";
	        })
	        .catch(function(err) {
	            vm.showErrorMsg = true;
	            vm.showSuccessMsg = false;
	        })
	    }

	    function openSubscribeModal(title,type) {
	        vm.subs_typ = type;
	        vm.showSuccessMsg = false;
	        vm.showInputErrorMsg = false;
	        vm.showErrorMsg = false;
	        vm.subscribeInput= "";
	        vm.shouldShowSubscribeModal = true;
	        vm.subscribeTitle = title + " will be live shortly. Please Subscribe for Updates";
	    }

	    function closeSubscribeModal() {
	        vm.shouldShowSubscribeModal = false;
	        vm.showInputErrorMsg = false;
	        vm.subscribeInput= "";
	        vm.subs_typ = "";
	    }

	    var getNotes = function(){
	        var dataToSend = {
	            "usr_id": authToken.getUserId() || 'n/a',
	            "src_id": vm.articleId
	        }
	        notesService.getNotes(dataToSend)
	            .then(function(res){
	                vm.notes = res.data;
	            })
	            .catch(function(err){
	                $log.error(err)
	            })
	    }

	    vm.addNotes = function(){
	        vm.isAddingNote = true;
	        var dataToSend = {
	            "usr_id": authToken.getUserId() || 'n/a',
	            "src_id": vm.articleId,
	            "note": vm.noteDesc,
	            "note_src" : "blog"
	        }
	        notesService.insertNote(dataToSend)
	            .then(function(res){
	                vm.notes = vm.notes || []
	                vm.notes.push(res.data)
	                vm.noteDesc = "";
	                alertify.success("Note saved successfully")
	            })
	            .catch(function(err){
	                $log.error(err)
	                alertify.error("Error Occured")
	            })
	            .finally(function(err){
	                vm.isAddingNote = false;
	            })
	    }

	    vm.editNote = function(note_id){
	        vm.editingNote = note_id;
	    }

	    vm.saveNote = function(note){
	        var dataToSend = {
	            "usr_id": authToken.getUserId(),
	            "src_id": note.src_id,
	            "note": note.note,
	            "note_id" : note.note_id
	        }
	        notesService.updateNote(dataToSend)
	            .then(function(res){
	                vm.editingNote = undefined
	                alertify.success("Note updated successfully")
	            })
	            .catch(function(err){
	                $log.error(err)
	                alertify.error("Error Occured")
	            })
	    }

	    vm.showRemoveNoteConfirmModal = function(note){
	        vm.deletingNote = note;
	        vm.shouldShowRemoveNoteConfirmModal = true;
	    }

	    vm.closeRemoveNoteConfirmModal = function(){
	        vm.deletingNote = undefined;
	        vm.shouldShowRemoveNoteConfirmModal = false;   
	    }

	    vm.removeNote = function(){
	        var dataToSend = {
	            "usr_id": authToken.getUserId(),
	            "src_id": vm.deletingNote.src_id,
	            "note_id" : vm.deletingNote.note_id
	        }
	        vm.isRemovingNote = true;
	        notesService.deleteNote(dataToSend)
	            .then(function(res){
	                vm.notes = vm.notes.filter(function(v,i){
	                    return v.note_id != vm.deletingNote.note_id
	                })
	                vm.shouldShowRemoveNoteConfirmModal = false;
	                alertify.success("Note removed successfully")
	            })
	            .catch(function(err){
	                $log.error(err)
	                alertify.error("Error Occured")
	            })
	            .finally(function(){
	                vm.isRemovingNote = false;
	            })
	    }


	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 489:
/***/ function(module, exports) {

	angular.module('service').service('blog',Blog);

	Blog.$inject = ['$http','BASE_API'];

	function Blog($http,BASE_API) {
	    this.deleteBlogNote = function (data) {
	        return $http.post(BASE_API + '/blognotes/deleteBlogNote', data);
	    }

	    this.updateBlogNote = function (data) {
	        return $http.post(BASE_API + '/blognotes/updateBlogNote', data);
	    }

	    this.insertBlogNotes = function (data) {
	        return $http.post(BASE_API + '/blognotes/insertBlogNote', data);
	    }
	}

/***/ },

/***/ 490:
/***/ function(module, exports) {

	angular.module('service').service('notesService',Notes);

	Notes.$inject = ['$http','BASE_API'];

	function Notes($http,BASE_API) {

	    this.deleteNote = function (data) {
	    	return $http.post(BASE_API + '/api/notes/deleteNote',data);
	    }

	    this.updateNote = function (data) {
	        return $http.post(BASE_API + '/api/notes/updateNote',data);
	    }

	    this.getNotes = function (data) {
	        return $http.post(BASE_API + '/api/notes/getNotes',data);
	    }

	    this.insertNote = function (data) {
	        return $http.post(BASE_API + '/api/notes/insertNote',data);
	    }
	}

/***/ },

/***/ 491:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(492);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_sideLinks.scss", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./_sideLinks.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 492:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n/* custom css */\n.blog-side-link-article-wrapper {\n  float: left;\n  width: 100%;\n  padding: 20px;\n  /* Landscape */\n  /* Landscape */\n  /* Landscape */\n  /* Landscape */ }\n  .blog-side-link-article-wrapper .left-content {\n    position: fixed;\n    float: left;\n    width: 17%;\n    color: #333; }\n    @media screen and (max-width: 767px) {\n      .blog-side-link-article-wrapper .left-content {\n        position: relative;\n        width: 100%;\n        margin-bottom: 20px; } }\n    .blog-side-link-article-wrapper .left-content h2 {\n      font-weight: bold;\n      font-size: 16px;\n      background-color: #f5f5f5;\n      border: 1px solid #ddd;\n      border-bottom: 0px;\n      padding: 10px 15px;\n      float: left;\n      width: 100%;\n      margin: 0px; }\n    .blog-side-link-article-wrapper .left-content .sidebar-link-wrapper {\n      background: white;\n      border-radius: 5px;\n      box-shadow: 1px 1px 1px #e5e5e5; }\n      .blog-side-link-article-wrapper .left-content .sidebar-link-wrapper .sidebar-title {\n        padding: 10px;\n        font-family: 'nunito',sans-serif;\n        font-size: 20px;\n        border-bottom: 1px solid #eee; }\n    .blog-side-link-article-wrapper .left-content ul {\n      margin: 0px;\n      padding: 0px; }\n      .blog-side-link-article-wrapper .left-content ul.click {\n        list-style: none;\n        width: 100%;\n        padding: 10px;\n        border-color: #ddd;\n        -webkit-border-radius: 5px;\n        -moz-border-radius: 5px;\n        border-radius: 5px; }\n        .blog-side-link-article-wrapper .left-content ul.click li {\n          width: 100%;\n          list-style: none;\n          margin: 5px 0px;\n          font-family: 'nunito',sans-serif;\n          border-bottom: 1px solid #e5e5e5;\n          display: -webkit-box;\n          display: -moz-box;\n          display: box;\n          display: -webkit-flex;\n          display: -moz-flex;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n          -moz-box-align: center;\n          box-align: center;\n          -webkit-align-items: center;\n          -moz-align-items: center;\n          -ms-align-items: center;\n          -o-align-items: center;\n          align-items: center;\n          -ms-flex-align: center;\n          -webkit-box-orient: horizontal;\n          -moz-box-orient: horizontal;\n          box-orient: horizontal;\n          -webkit-box-direction: normal;\n          -moz-box-direction: normal;\n          box-direction: normal;\n          -webkit-flex-direction: row;\n          -moz-flex-direction: row;\n          flex-direction: row;\n          -ms-flex-direction: row; }\n          .blog-side-link-article-wrapper .left-content ul.click li:last-child {\n            border-bottom: 0px; }\n          .blog-side-link-article-wrapper .left-content ul.click li a {\n            width: 100%;\n            position: relative;\n            display: block;\n            padding: 10px 15px;\n            color: #555;\n            text-decoration: none;\n            margin-bottom: -1px;\n            cursor: pointer; }\n      .blog-side-link-article-wrapper .left-content ul.social {\n        width: 100%;\n        margin-bottom: 10px;\n        list-style: none;\n        padding: 10px 0px;\n        background: white;\n        border-radius: 5px;\n        box-shadow: 1px 1px 1px #e5e5e5; }\n        .blog-side-link-article-wrapper .left-content ul.social li {\n          float: left;\n          width: 25%;\n          border: 0px;\n          margin: 0px;\n          text-align: center; }\n          .blog-side-link-article-wrapper .left-content ul.social li a {\n            float: left;\n            width: 100%;\n            margin: 0px;\n            text-align: center;\n            border: 0px; }\n  .blog-side-link-article-wrapper .right-content {\n    float: left;\n    margin-left: 19%;\n    width: 59.5%; }\n    .blog-side-link-article-wrapper .right-content.full {\n      width: 100%;\n      margin-left: 0%; }\n    @media screen and (max-width: 767px) {\n      .blog-side-link-article-wrapper .right-content {\n        width: 100%;\n        margin-left: 0%; } }\n    .blog-side-link-article-wrapper .right-content .article-name {\n      font-family: 'Nunito',sans-serif;\n      font-size: 35px;\n      text-transform: capitalize;\n      text-align: center;\n      word-break: break-word;\n      margin: 0px;\n      background: white;\n      border-top-left-radius: 5px;\n      border-top-right-radius: 5px;\n      padding: 10px;\n      border-bottom: 1px solid #e5e5e5; }\n    .blog-side-link-article-wrapper .right-content h1, .blog-side-link-article-wrapper .right-content h2, .blog-side-link-article-wrapper .right-content h3, .blog-side-link-article-wrapper .right-content h4, .blog-side-link-article-wrapper .right-content h5, .blog-side-link-article-wrapper .right-content h6 {\n      font-family: 'Nunito',sans-serif; }\n    .blog-side-link-article-wrapper .right-content .content-area {\n      background-color: #fff;\n      width: 100%;\n      float: left;\n      padding: 10px 15px;\n      margin-bottom: 20px;\n      border-bottom-left-radius: 5px;\n      border-bottom-right-radius: 5px; }\n      .blog-side-link-article-wrapper .right-content .content-area img {\n        max-width: 100% !important; }\n      .blog-side-link-article-wrapper .right-content .content-area p {\n        color: #555;\n        font-size: 12px;\n        line-height: 22px;\n        margin-bottom: 20px; }\n    .blog-side-link-article-wrapper .right-content .tabs {\n      width: 100%;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: center;\n      -moz-box-pack: center;\n      box-pack: center;\n      -webkit-justify-content: center;\n      -moz-justify-content: center;\n      -ms-justify-content: center;\n      -o-justify-content: center;\n      justify-content: center;\n      -ms-flex-pack: center;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; }\n      .blog-side-link-article-wrapper .right-content .tabs .tab {\n        padding: 10px;\n        background: #1d99d1;\n        color: white;\n        font-family: 'waverly';\n        font-size: 18px;\n        text-transform: lowercase;\n        height: 60px;\n        cursor: pointer;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row;\n        -webkit-box-flex: 1;\n        -moz-box-flex: 1;\n        box-flex: 1;\n        -webkit-flex: 1;\n        -moz-flex: 1;\n        -ms-flex: 1;\n        flex: 1; }\n        .blog-side-link-article-wrapper .right-content .tabs .tab.active {\n          background: #1778a4; }\n      .blog-side-link-article-wrapper .right-content .tabs .close-btn {\n        padding: 10px 20px;\n        background: #1778a4;\n        color: white;\n        font-family: 'waverly';\n        font-size: 18px;\n        text-transform: lowercase;\n        height: 60px;\n        cursor: pointer;\n        border-left: 1px solid #1d99d1;\n        display: -webkit-box;\n        display: -moz-box;\n        display: box;\n        display: -webkit-flex;\n        display: -moz-flex;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -moz-box-align: center;\n        box-align: center;\n        -webkit-align-items: center;\n        -moz-align-items: center;\n        -ms-align-items: center;\n        -o-align-items: center;\n        align-items: center;\n        -ms-flex-align: center;\n        -webkit-box-pack: center;\n        -moz-box-pack: center;\n        box-pack: center;\n        -webkit-justify-content: center;\n        -moz-justify-content: center;\n        -ms-justify-content: center;\n        -o-justify-content: center;\n        justify-content: center;\n        -ms-flex-pack: center;\n        -webkit-box-orient: horizontal;\n        -moz-box-orient: horizontal;\n        box-orient: horizontal;\n        -webkit-box-direction: normal;\n        -moz-box-direction: normal;\n        box-direction: normal;\n        -webkit-flex-direction: row;\n        -moz-flex-direction: row;\n        flex-direction: row;\n        -ms-flex-direction: row; }\n        .blog-side-link-article-wrapper .right-content .tabs .close-btn img {\n          border-radius: 3px; }\n    .blog-side-link-article-wrapper .right-content .tab-content {\n      padding: 10px;\n      background: #e5e5e5; }\n    .blog-side-link-article-wrapper .right-content .note-qa-tab textarea {\n      margin-bottom: 10px;\n      border: 1px solid #1d99d1;\n      padding: 10px;\n      border-radius: 3px;\n      box-shadow: 0px 0px 0px;\n      background-color: white; }\n    .blog-side-link-article-wrapper .right-content .qa-question-wrapper {\n      text-decoration: none; }\n      .blog-side-link-article-wrapper .right-content .qa-question-wrapper:hover {\n        text-decoration: none; }\n      .blog-side-link-article-wrapper .right-content .qa-question-wrapper .qa-info {\n        padding: 10px 0px;\n        border-bottom: 1px solid #e5e5e5; }\n      .blog-side-link-article-wrapper .right-content .qa-question-wrapper .qa-title {\n        color: #1d99d1; }\n      .blog-side-link-article-wrapper .right-content .qa-question-wrapper .qa-answer-info {\n        color: #444; }\n  .blog-side-link-article-wrapper .ads {\n    float: right;\n    width: 20%;\n    position: fixed;\n    right: 20px; }\n    .blog-side-link-article-wrapper .ads h2 {\n      font-weight: bold;\n      font-size: 16px;\n      background-color: #f5f5f5;\n      border: 1px solid #ddd;\n      border-bottom: 0px;\n      padding: 10px 15px;\n      margin-bottom: 0px; }\n    .blog-side-link-article-wrapper .ads p {\n      color: #555;\n      font-size: 12px;\n      line-height: 22px;\n      margin-bottom: 20px; }\n  .blog-side-link-article-wrapper .ads-wrapper {\n    padding: 10px;\n    border-radius: 5px;\n    box-shadow: 1px 1px 1px #e5e5e5;\n    background: white;\n    margin: 20px 0px; }\n    .blog-side-link-article-wrapper .ads-wrapper .ads-title {\n      font-family: 'nunito',sans-serif;\n      font-size: 20px;\n      border-bottom: 1px solid #e5e5e5;\n      margin-bottom: 5px; }\n  .blog-side-link-article-wrapper .ads-content {\n    background-color: #fff;\n    width: 100%; }\n    .blog-side-link-article-wrapper .ads-content img {\n      width: 100%; }\n  .blog-side-link-article-wrapper .subscribe-wrapper {\n    padding: 10px;\n    background: white;\n    border-radius: 5px; }\n    .blog-side-link-article-wrapper .subscribe-wrapper .subscribe-title {\n      font-family: 'nunito',sans-serif;\n      font-size: 18px; }\n    .blog-side-link-article-wrapper .subscribe-wrapper .subscribe-tf {\n      width: 100%;\n      border: 1px solid #e5e5e5;\n      border-radius: 3px;\n      padding: 5px;\n      height: 40px;\n      margin-bottom: 5px;\n      box-shadow: 0px 0px 0px; }\n      .blog-side-link-article-wrapper .subscribe-wrapper .subscribe-tf:focus {\n        border: 1px solid #1d99d1; }\n    .blog-side-link-article-wrapper .subscribe-wrapper .subscribe-btn {\n      width: 140px;\n      margin-left: 50%;\n      position: relative;\n      left: -70px; }\n  .blog-side-link-article-wrapper .subscribe-input input {\n    height: 40px;\n    border: 1px solid #f4f4f4;\n    border-radius: 5px;\n    margin: 5px 0px;\n    padding: 5px;\n    font-family: 'nunito',sans-serif;\n    font-size: 16px;\n    width: 100%;\n    outline: none;\n    box-shadow: 0px 0px 0px; }\n  .blog-side-link-article-wrapper .input-error-msg {\n    color: red;\n    font-family: 'nunito',sans-serif;\n    font-size: 12px; }\n  .blog-side-link-article-wrapper .subscribe-input input.error {\n    border: 1px solid #f00; }\n  .blog-side-link-article-wrapper .success-msg {\n    background: green; }\n  .blog-side-link-article-wrapper .error-msg {\n    background: #EC644B; }\n  .blog-side-link-article-wrapper .success-msg, .blog-side-link-article-wrapper .error-msg {\n    color: white;\n    padding: 10px;\n    border-radius: 5px;\n    text-align: center;\n    font-family: nunito;\n    font-size: 17px;\n    margin: 10px 0px; }\n  @media screen and (max-width: 1221px) and (min-width: 1125px) {\n    .blog-side-link-article-wrapper .search {\n      float: left;\n      width: 24%;\n      padding-top: 10px;\n      padding-left: 1%; }\n    .blog-side-link-article-wrapper .hdr-right {\n      width: 50%; }\n      .blog-side-link-article-wrapper .hdr-right ul.links li a {\n        font-size: 15px; } }\n  @media screen and (max-width: 1124px) and (min-width: 992px) {\n    .blog-side-link-article-wrapper .search {\n      float: left;\n      width: 24%;\n      padding-top: 10px;\n      padding-left: 1%; }\n    .blog-side-link-article-wrapper .hdr-right {\n      width: 50%; }\n      .blog-side-link-article-wrapper .hdr-right ul.links li a {\n        font-size: 15px; } }\n  @media screen and (max-width: 991px) and (min-width: 851px) {\n    .blog-side-link-article-wrapper #sub-menu .col-1 ul li a {\n      font-size: 12px; }\n    .blog-side-link-article-wrapper .logo {\n      padding: 10px 10px 0px !important;\n      width: 193px !important; } }\n  @media screen and (max-width: 991px) and (min-width: 768px) {\n    .blog-side-link-article-wrapper .logo {\n      padding: 18px 10px 0px;\n      width: 21%; }\n    .blog-side-link-article-wrapper .search {\n      width: 25%;\n      padding-left: 2%; }\n    .blog-side-link-article-wrapper .hdr-right {\n      width: 52%;\n      padding-left: 0;\n      padding-right: 10px; }\n      .blog-side-link-article-wrapper .hdr-right ul.links li {\n        margin-left: 14px; }\n        .blog-side-link-article-wrapper .hdr-right ul.links li a {\n          font-size: 13px; }\n    .blog-side-link-article-wrapper #wrapper {\n      padding: 120px 10px 20px; }\n    .blog-side-link-article-wrapper .left-content {\n      width: 21%; }\n    .blog-side-link-article-wrapper .right-content {\n      margin-left: 24%;\n      width: 53%; }\n    .blog-side-link-article-wrapper .ads {\n      width: 21%; }\n    .blog-side-link-article-wrapper .left-content h2, .blog-side-link-article-wrapper .left-content ul.click li a {\n      padding-left: 10px;\n      padding-right: 10px; }\n    .blog-side-link-article-wrapper .right-content h2, .blog-side-link-article-wrapper .content-area, .blog-side-link-article-wrapper .tab-content, .blog-side-link-article-wrapper .ads h2, .blog-side-link-article-wrapper .ads-content {\n      padding-left: 10px;\n      padding-right: 10px; } }\n  @media screen and (min-width: 851px) {\n    .blog-side-link-article-wrapper .mobile-menu, .blog-side-link-article-wrapper #close, .blog-side-link-article-wrapper #close2, .blog-side-link-article-wrapper #close3, .blog-side-link-article-wrapper #close4 {\n      display: none; }\n    .blog-side-link-article-wrapper #sub-menu {\n      background: #fff;\n      max-width: 1000px;\n      position: fixed;\n      right: 0;\n      bottom: 0px;\n      z-index: 999;\n      height: 84%;\n      width: 100%; }\n      .blog-side-link-article-wrapper #sub-menu .col-1 {\n        float: left;\n        width: 25%;\n        height: 100%;\n        position: relative; }\n        .blog-side-link-article-wrapper #sub-menu .col-1 .col-head {\n          padding: 10px;\n          background-image: -webkit-linear-gradient(top, #337ab7 0, #2b669a 100%);\n          background-image: -o-linear-gradient(top, #337ab7 0, #2b669a 100%);\n          background-image: -webkit-gradient(linear, left top, left bottom, from(#337ab7), to(#2b669a));\n          background-image: linear-gradient(to bottom, #337ab7 0, #2b669a 100%);\n          filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff337ab7', endColorstr='#ff2b669a', GradientType=0);\n          background-repeat: repeat-x;\n          color: #fff;\n          text-align: center;\n          font-size: 15px;\n          font-weight: bold;\n          border-left: 1px solid #fff; }\n          .blog-side-link-article-wrapper #sub-menu .col-1 .col-head .fa {\n            margin-right: 7px; }\n        .blog-side-link-article-wrapper #sub-menu .col-1 .col-content {\n          border-left: 1px solid #ccc;\n          overflow: auto;\n          float: left;\n          height: 89%;\n          padding: 10px 0px;\n          width: 100%; }\n        .blog-side-link-article-wrapper #sub-menu .col-1:first-child .col-ftr {\n          border-left: 1px solid #ccc; }\n        .blog-side-link-article-wrapper #sub-menu .col-1 .col-ftr {\n          float: left;\n          width: 100%;\n          padding: 10px;\n          background-image: -webkit-linear-gradient(top, #337ab7 0, #2b669a 100%);\n          position: absolute;\n          bottom: 0px;\n          background-image: -o-linear-gradient(top, #337ab7 0, #2b669a 100%);\n          background-image: -webkit-gradient(linear, left top, left bottom, from(#337ab7), to(#2b669a));\n          background-image: linear-gradient(to bottom, #337ab7 0, #2b669a 100%);\n          filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff337ab7', endColorstr='#ff2b669a', GradientType=0);\n          background-repeat: repeat-x;\n          color: #fff;\n          text-align: center;\n          font-size: 15px;\n          font-weight: bold;\n          border-left: 1px solid #fff; }\n        .blog-side-link-article-wrapper #sub-menu .col-1 ul {\n          float: left;\n          width: 100%; }\n          .blog-side-link-article-wrapper #sub-menu .col-1 ul li {\n            float: left;\n            width: 100%;\n            padding: 10px 10px;\n            border-bottom: 1px solid #ddd; }\n            .blog-side-link-article-wrapper #sub-menu .col-1 ul li:last-child {\n              border: 0px; }\n            .blog-side-link-article-wrapper #sub-menu .col-1 ul li a {\n              float: left;\n              width: 100%;\n              text-decoration: none;\n              font-size: 13px;\n              color: #888;\n              font-weight: bold; }\n              .blog-side-link-article-wrapper #sub-menu .col-1 ul li a span {\n                float: left;\n                margin-right: 10px; }\n              .blog-side-link-article-wrapper #sub-menu .col-1 ul li a:hover, .blog-side-link-article-wrapper #sub-menu .col-1 ul li a:focus {\n                color: #337ab7; }\n        .blog-side-link-article-wrapper #sub-menu .col-1.third ul li:last-child {\n          text-align: right; } }\n  @media screen and (max-width: 850px) {\n    .blog-side-link-article-wrapper .mobile-menu {\n      display: block; }\n    .blog-side-link-article-wrapper .hdr-right .top-bottom {\n      top: 83px;\n      padding-top: 0px; }\n    .blog-side-link-article-wrapper .logo {\n      float: left;\n      width: 35%;\n      padding: 0 10px; }\n    .blog-side-link-article-wrapper .search {\n      float: right;\n      width: 50%;\n      padding-left: 0%;\n      padding-right: 10px;\n      padding-top: 12px; }\n    .blog-side-link-article-wrapper .header {\n      height: 80px;\n      position: inherit; }\n    .blog-side-link-article-wrapper .hdr-right {\n      padding: 0 10px 0 0;\n      position: fixed;\n      width: 14%;\n      z-index: 9999999; }\n    .blog-side-link-article-wrapper .left-content {\n      width: 100%;\n      margin-bottom: 20px;\n      position: relative; }\n    .blog-side-link-article-wrapper #wrapper {\n      padding-top: 45px;\n      padding-bottom: 65px; }\n    .blog-side-link-article-wrapper .right-content {\n      width: 100%;\n      margin-left: 0; }\n    .blog-side-link-article-wrapper .ads {\n      width: 100%;\n      margin-top: 20px; }\n    .blog-side-link-article-wrapper .mobile-menu {\n      float: left;\n      width: 100%;\n      position: fixed;\n      bottom: 0px;\n      left: 0px;\n      width: 100%;\n      z-index: 999; }\n      .blog-side-link-article-wrapper .mobile-menu > ul {\n        float: left;\n        width: 100%;\n        background: #f5f5f5;\n        box-shadow: 0px -1px 6px rgba(0, 0, 0, 0.2); }\n        .blog-side-link-article-wrapper .mobile-menu > ul > li {\n          float: left;\n          width: 25%;\n          border-bottom: 1px solid #ccc;\n          text-align: center;\n          border-top: 1px solid #ccc;\n          border-left: 1px solid #ccc; }\n          .blog-side-link-article-wrapper .mobile-menu > ul > li:last-child {\n            border-right: 1px solid #ccc; }\n          .blog-side-link-article-wrapper .mobile-menu > ul > li > a {\n            display: inline-block;\n            width: 100%;\n            text-align: center;\n            text-decoration: none;\n            font-size: 13px;\n            font-weight: bold;\n            color: #444;\n            padding: 10px 0px; }\n            .blog-side-link-article-wrapper .mobile-menu > ul > li > a:hover {\n              background-image: -webkit-linear-gradient(top, #337ab7 0, #2b669a 100%);\n              background-image: -o-linear-gradient(top, #337ab7 0, #2b669a 100%);\n              background-image: -webkit-gradient(linear, left top, left bottom, from(#337ab7), to(#2b669a));\n              background-image: linear-gradient(to bottom, #337ab7 0, #2b669a 100%);\n              filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff337ab7', endColorstr='#ff2b669a', GradientType=0);\n              background-repeat: repeat-x;\n              color: #fff;\n              border-color: #2b669a; }\n          .blog-side-link-article-wrapper .mobile-menu > ul > li.active > a, .blog-side-link-article-wrapper .mobile-menu > ul > li > a:focus {\n            background-image: -webkit-linear-gradient(top, #337ab7 0, #2b669a 100%);\n            background-image: -o-linear-gradient(top, #337ab7 0, #2b669a 100%);\n            background-image: -webkit-gradient(linear, left top, left bottom, from(#337ab7), to(#2b669a));\n            background-image: linear-gradient(to bottom, #337ab7 0, #2b669a 100%);\n            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff337ab7', endColorstr='#ff2b669a', GradientType=0);\n            background-repeat: repeat-x;\n            color: #fff;\n            border-color: #2b669a; }\n    .blog-side-link-article-wrapper #wrapper {\n      z-index: 10;\n      position: relative; }\n    .blog-side-link-article-wrapper #sub-menu {\n      display: block !important;\n      position: fixed;\n      /* z-index: 999999; */\n      width: 100%;\n      top: 0; }\n    .blog-side-link-article-wrapper .col-1 {\n      display: none; }\n      .blog-side-link-article-wrapper .col-1.show {\n        position: fixed;\n        display: block;\n        background: white;\n        width: 100%;\n        height: 94%;\n        left: 0px;\n        top: 0px;\n        right: 0px;\n        bottom: 0px;\n        z-index: 99;\n        visibility: visible; }\n      .blog-side-link-article-wrapper .col-1 .col-head, .blog-side-link-article-wrapper .col-1 .col-ftr {\n        display: none; }\n      .blog-side-link-article-wrapper .col-1 .col-content {\n        float: none;\n        height: 100%;\n        width: 97%;\n        box-shadow: 0px 0px 14px rgba(0, 0, 0, 0.5);\n        margin: auto; }\n        .blog-side-link-article-wrapper .col-1 .col-content ul {\n          display: table-row;\n          width: 100%;\n          height: 100%;\n          float: left;\n          padding: 50px 20px 0px 20px;\n          overflow: auto; }\n          .blog-side-link-article-wrapper .col-1 .col-content ul li {\n            width: 100%;\n            float: left;\n            padding: 7px 0px;\n            border-bottom: 1px solid #ddd; }\n            .blog-side-link-article-wrapper .col-1 .col-content ul li a {\n              font-size: 16px;\n              color: #222;\n              text-decoration: none; }\n              .blog-side-link-article-wrapper .col-1 .col-content ul li a span {\n                float: left;\n                margin-right: 20px; }\n              .blog-side-link-article-wrapper .col-1 .col-content ul li a:hover, .blog-side-link-article-wrapper .col-1 .col-content ul li a:focus {\n                color: #337ab7; }\n    .blog-side-link-article-wrapper #close, .blog-side-link-article-wrapper #close2, .blog-side-link-article-wrapper #close3, .blog-side-link-article-wrapper #close4 {\n      position: absolute;\n      right: 25px;\n      top: 15px;\n      font-size: 14px;\n      color: #ccc;\n      padding: 3px 7px;\n      border: 1px solid #ccc;\n      border-radius: 32px;\n      z-index: 9999; } }\n  @media screen and (max-width: 767px) {\n    .blog-side-link-article-wrapper .hdr-right .top-bottom li {\n      margin-left: 4px; }\n    .blog-side-link-article-wrapper .logo a {\n      width: 180px;\n      float: left;\n      padding-top: 10px; }\n      .blog-side-link-article-wrapper .logo a img {\n        width: 100%; } }\n  @media screen and (max-width: 479px) {\n    .blog-side-link-article-wrapper .logo {\n      float: left;\n      width: 100%;\n      text-align: center;\n      padding: 0 10px; }\n      .blog-side-link-article-wrapper .logo a {\n        width: 100%; }\n      .blog-side-link-article-wrapper .logo img {\n        max-width: 200px; }\n    .blog-side-link-article-wrapper .search {\n      padding-left: 10px;\n      float: left;\n      width: 68%;\n      display: inline-block; }\n    .blog-side-link-article-wrapper .hdr-right .top-bottom li a {\n      color: #fff; }\n    .blog-side-link-article-wrapper #wrapper {\n      padding: 36px 10px 60px; }\n    .blog-side-link-article-wrapper #close, .blog-side-link-article-wrapper #close2, .blog-side-link-article-wrapper #close3, .blog-side-link-article-wrapper #close4 {\n      top: 15px; }\n    .blog-side-link-article-wrapper .header {\n      height: 123px;\n      text-align: center; }\n    .blog-side-link-article-wrapper .hdr-right {\n      width: 24%; }\n    .blog-side-link-article-wrapper .right-content h1 {\n      margin-bottom: 10px; }\n    .blog-side-link-article-wrapper .tabs > ul li {\n      width: 100%;\n      margin-top: -1px; }\n      .blog-side-link-article-wrapper .tabs > ul li:first-child {\n        margin: 0px; }\n    .blog-side-link-article-wrapper .tab-content {\n      padding: 0px 10px; }\n    .blog-side-link-article-wrapper .mobile-menu ul li a {\n      font-size: 11px; } }\n  @media only screen and (min-device-width: 320px) and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: landscape) {\n    .blog-side-link-article-wrapper .col-1 .col-content ul {\n      height: 94%; } }\n  @media only screen and (min-device-width: 320px) and (max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: landscape) {\n    .blog-side-link-article-wrapper .col-1 .col-content ul {\n      height: 94%; } }\n  @media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: landscape) {\n    .blog-side-link-article-wrapper .col-1 .col-content ul {\n      height: 94%; } }\n  @media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (-webkit-min-device-pixel-ratio: 3) and (orientation: landscape) {\n    .blog-side-link-article-wrapper .col-1 .col-content ul {\n      height: 94%; } }\n  .blog-side-link-article-wrapper .new-notes-wrapper {\n    font-family: 'nunito',sans-serif; }\n    .blog-side-link-article-wrapper .new-notes-wrapper .note {\n      background-color: white;\n      padding: 10px;\n      border-radius: 5px;\n      margin-bottom: 10px; }\n    .blog-side-link-article-wrapper .new-notes-wrapper .note-top {\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: end;\n      -moz-box-pack: end;\n      box-pack: end;\n      -webkit-justify-content: flex-end;\n      -moz-justify-content: flex-end;\n      -ms-justify-content: flex-end;\n      -o-justify-content: flex-end;\n      justify-content: flex-end;\n      -ms-flex-pack: end; }\n      .blog-side-link-article-wrapper .new-notes-wrapper .note-top .item {\n        color: #1778a4;\n        margin-left: 10px;\n        cursor: pointer; }\n    .blog-side-link-article-wrapper .new-notes-wrapper .note-text {\n      font-size: 16px; }\n    .blog-side-link-article-wrapper .new-notes-wrapper .note-info {\n      margin-top: 5px;\n      color: #777;\n      font-size: 12px; }\n", ""]);

	// exports


/***/ },

/***/ 493:
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div>\n\t<div id=\"wrapper\" class=\"blog-side-link-article-wrapper\">\n\t\t<div class=\"left-content links\" ng-if=\"vm.showSidebar != 'false'\">\n\t\t\t<ul class=\"social\">\n\t\t\t\t<li>\n\t\t\t\t\t<a href=\"\" ng-click=\"vm.shareToFB(vm.name)\">\n\t\t\t\t\t\t<img style=\"height: 28px;margin-top: 5px;\" src=\"" + __webpack_require__(340) + "\" alt=\"\"/>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li>\n\t\t\t\t\t<a href=\"\" ng-click=\"vm.shareToTwitter(vm.name)\">\n\t\t\t\t\t\t<img style=\"height: 28px;margin-top: 5px;\" src=\"" + __webpack_require__(341) + "\" alt=\"\" >\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li>\n\t\t\t\t\t<a href=\"\" ng-click=\"vm.shareToEWProfile(vm.name)\">\n\t\t\t\t\t\t<img src=\"" + __webpack_require__(342) + "\" alt=\"\">\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li>\n\t\t\t\t\t<a href=\"\" ng-click=\"vm.shareToQA(vm.name)\">\n\t\t\t\t\t\t<img src=\"" + __webpack_require__(343) + "\" alt=\"\">\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t</ul>\n\t\t\t<div class=\"sidebar-link-wrapper\">\n\t\t\t\t<div class=\"sidebar-title\">\n\t\t\t\t\tContent\n\t\t\t\t</div>\n\t\t\t\t<ul class=\"click\">\n\t\t\t\t\t<li class=\"\" ng-repeat=\"link in vm.link_arr track by $index\">\n\t\t\t\t\t\t<i class=\"fa fa-gear\"></i>\n\t\t\t\t\t\t<a data-id=\"link--{{$index}}\" ng-bind-html=\"link\"></a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li ng-click=\"vm.scrollToTab('comments')\"><i class=\"fa fa-comments\"></i><a>Comments</a></li>\n\t\t\t\t\t<li ng-click=\"vm.scrollToTab('notes')\"><i class=\"fa fa-sticky-note\"></i><a>Notes</a></li>\n\t\t\t\t\t<li ng-click=\"vm.scrollToTab('qa')\"><i class=\"fa fa-question-circle\"></i><a>Q&A</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t\n\t\t</div>\n\t\t<!-- left-content closed -->\n\t\t<!-- right-content start -->\n\t\t<div class=\"right-content links-content-wrapper\" ng-class=\"{'full':vm.showSidebar === 'false'}\">\n\t\t\t<h4 class=\"article-name\" ng-if=\"vm.showSidebar != 'false'\">{{vm.name}}</h4>\n\t\t\t<div class=\"content-area\">\n\t\t\t\t<div id=\"link--{{$index}}\" ng-repeat=\"content in vm.content_arr track by $index\">\n\t\t\t\t\t<div ng-bind-html=\"vm.trustAsHTML(content)\"></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"tabs\" id=\"tabs\" ng-if=\"vm.showSidebar != 'false'\">\n\t\t\t\t<div class=\"tab\" ng-class=\"{'active': vm.activeRelatedTab ==='comments'}\" ng-click=\"vm.scrollToTab('comments')\">\n\t\t\t\t\t<img src=\"" + __webpack_require__(494) + "\" alt=\"\">\n\t\t\t\t\tComments\n\t\t\t\t</div>\t\n\t\t\t\t<div class=\"tab\" ng-class=\"{'active': vm.activeRelatedTab ==='notes'}\" ng-click=\"vm.scrollToTab('notes')\">\n\t\t\t\t\t<img src=\"" + __webpack_require__(495) + "\" alt=\"\">\n\t\t\t\t\tNotes\n\t\t\t\t</div>\t\n\t\t\t\t<div class=\"tab\" ng-class=\"{'active': vm.activeRelatedTab ==='qa'}\" ng-click=\"vm.scrollToTab('qa');\">\n\t\t\t\t\t<img src=\"" + __webpack_require__(496) + "\" alt=\"\">\n\t\t\t\t\tQ&A\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- Video Comments Tab -->\n\t\t\t<div class=\"tab-content\" ng-if=\"vm.activeRelatedTab === 'comments' && vm.showSidebar != 'false'\">\n\t\t\t\t<item-comments class=\"comments-component\" post-type=\"article-comment\" post-id=\"vm.activeItem\" show-comment-field=\"true\" ></item-comments>\n\t\t\t</div>\n\t\t\t<!-- Video Notes Tab -->\n\t    \t<div class=\"tab-content noteTab note-qa-tab\" ng-if=\"vm.activeRelatedTab === 'notes' && vm.showSidebar != 'false'\">\n\t\t\t\t<div class=\"video-note-form\">\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<textarea froala=\"vm.EditorConfig\" class=\"form-control\" placeholder=\"Enter Note\" ng-model=\"vm.noteDesc\"></textarea>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<button class=\"ew-rounded-btn pull-right\" style=\"margin-top: 10px;padding: 0px;\" ng-click=\"vm.addNotes()\" ng-if=\"!vm.isAddingNote\">\n\t\t\t\t\t\t\t<i class=\"fa fa-sticky-note right\"></i>\n\t\t\t\t\t\t\t<span class=\"text\">Add Note</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t\t<button class=\"ew-rounded-btn pull-right\" style=\"margin-top: 10px;padding: 0px;\" ng-if=\"vm.isAddingNote\">\n\t\t\t\t\t\t\t<i class=\"fa fa-circle-o-notch right\"></i>\n\t\t\t\t\t\t\t<span class=\"text\">Adding.....</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"new-notes-wrapper\">\n\t\t\t\t\t<div class=\"note\" ng-repeat=\"v in vm.notes | orderBy:'crt_dt':true\">\n\t\t\t\t\t\t<div class=\"note-top\">\n\t\t\t\t\t\t\t<span class=\"item edit-button\" ng-if=\"vm.editingNote != v.note_id\" ng-click=\"vm.editNote(v.note_id)\">Edit</span>\n\t\t\t\t\t\t\t<span class=\"item edit save-button\" ng-if=\"vm.editingNote === v.note_id\" ng-click=\"vm.saveNote(v)\">Save</span>\n\t\t\t\t\t\t\t<span class=\"item\" ng-click=\"vm.showRemoveNoteConfirmModal(v)\">Remove</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"note-text\" ng-show=\"vm.editingNote != v.note_id\" ng-bind-html=\"vm.trustAsHTML(v.note)\"></div>\n\t\t\t\t\t\t<div ng-show=\"vm.editingNote === v.note_id\">\n\t\t\t\t\t\t\t<textarea froala=\"vm.EditorConfig\"  rows=\"6\" ng-model=\"v.note\"></textarea>\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"note-info\">\n\t\t\t\t\t\t\t<span class=\"item\">\n\t\t\t\t\t\t\t\t{{v.crt_dt | date:'dd-MMM-yyyy  hh:mm a'}}\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- Video QA Tab -->\n\t\t\t<div class=\"tab-content noteTab note-qa-tab\" ng-if=\"vm.activeRelatedTab === 'qa' && vm.showSidebar != 'false'\">\n\t\t\t\t<div class=\"video-note-form\">\n\t\t\t\t\t<textarea class=\"form-control\" placeholder=\"Enter question\" ng-model=\"vm.queTitle\"></textarea>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<button class=\"ew-rounded-btn pull-right\" style=\"padding: 0px;\" ng-click=\"vm.saveQueryForItem()\">\n\t\t\t\t\t\t\t<i class=\"fa fa-question-circle right\"></i>\n\t\t\t\t\t\t\t<span class=\"text\">Post Question</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<!-- <button class=\"form-control btn btn-primary\" >Post</button> -->\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<a class=\"qa-question-wrapper\" ng-href=\"{{vm.getQAQuestionPageUrl(false,query.qry_id)}}\" target=\"_blank\" ng-repeat=\"query in vm.itemQueries | orderBy:'qry_ts':true track by $index\">\n\t\t    \t\t<div class=\"video-info qa-info\">\n\t\t    \t\t\t<div class=\"qa-title\">{{$index + 1}}. {{query.qry_title}}</div>\n\t\t    \t\t\t<div class=\"qa-answer-info\">By - {{query.usr_id}} , {{query[\"qry_stats\"][\"qry_ans\"] || 0}} answer(s)</div>\n\t\t    \t\t</div>\n\t\t    \t</a>\n\t\t\t</div>\n\t\t</div>\n\t\t<!-- right-content closed -->\n\t\t<!-- ad-content start -->\n\t\t<div class=\"right-most-content ads\"  ng-if=\"vm.showSidebar != 'false'\">\n\t\t\t<div class=\"subscribe-wrapper\">\n\t\t\t\t<div class=\"subscribe-title\">\n\t\t\t\t\tSubscribe\n\t\t\t\t</div>\n\t\t\t\t<div class=\"subscribe-input\" ng-if=\"!vm.showSuccessMsg\">\n\t\t\t\t\t<input type=\"text\" id=\"subscribe-tf\" ng-class=\"{'error':vm.showInputErrorMsg}\" ng-model=\"vm.subscribeInput\" ng-change=\"vm.validateInputField()\" placeholder=\"Email Address Here\">\n\t\t\t\t\t<div class=\"input-error-msg\" ng-if=\"vm.showInputErrorMsg\">*Please Enter valid Email Address</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn subscribe-btn\" ng-click=\"vm.subscribeUser()\" ng-if=\"!vm.showSuccessMsg\">\n\t\t\t\t\t<i class=\"fa fa-envelope right\"></i>\n\t\t\t\t\t<span class=\"text\">Subscribe</span>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"alert alert-success success-msg\" ng-if=\"vm.showSuccessMsg\">\n\t\t\t\t\tThank you for subscribption.\n\t\t\t\t</div>\n\t\t\t\t<div class=\"alert alert-danger error-msg\" ng-if=\"vm.showErrorMsg\">\n\t\t\t\t\tSome Error occured.\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- <div class=\"ads-wrapper\">\n\t\t\t\t<div class=\"ads-title\">Ads</div>\n\t\t\t\t<div class=\"ads-content\">\n\t\t\t\t\t<img src=\"" + __webpack_require__(497) + "\" alt=\"\">\t\n\t\t\t\t</div>\t\n\t\t\t</div> -->\n\t\t</div>\n\t\t<!-- ad-content close -->\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n<!-- Login alert modal -->\n<div class=\"ew-small-modal-wrapper\" ng-if=\"vm.showLoginAlertModal\">\n    <div class=\"ew-modal\">\n        <div class=\"msg\">\n            {{vm.needLoginErrModalMsg}}\n        </div>\n        <div class=\"action-btns\">\n            <div class=\"ew-rounded-btn\" ng-click=\"vm.showLoginAlertModal = false;\">\n            \t<i class=\"fa fa-times-circle-o right\"></i>\n            \t<span class=\"text\">Close</span>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- Removve Note Modal Confirm -->\n<div class=\"ew-small-modal-wrapper\" ng-if=\"vm.shouldShowRemoveNoteConfirmModal\">\n    <div class=\"ew-modal\">\n        <div class=\"msg\">\n            Are you sure you want to delete the note?\n        </div>\n        <div class=\"action-btns action-btns-flex-end\">\n        \t<div class=\"ew-rounded-btn red\" ng-click=\"vm.removeNote();\" ng-if=\"!vm.isRemovingNote\">\n            \t<i class=\"fa fa-trash right\"></i>\n            \t<span class=\"text\">Remove</span>\n            </div>\n            <div class=\"ew-rounded-btn red\" ng-if=\"vm.isRemovingNote\">\n            \t<i class=\"fa fa-circle-o-notch right\"></i>\n            \t<span class=\"text\">Removing...</span>\n            </div>\n            <div class=\"ew-rounded-btn\" ng-click=\"vm.closeRemoveNoteConfirmModal();\">\n            \t<i class=\"fa fa-times-circle-o right\"></i>\n            \t<span class=\"text\">Close</span>\n            </div>\n        </div>\n    </div>\n</div>";

/***/ },

/***/ 494:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkE2RUU3MzgzODdBODExRTY5NkUzRENEQzAwQUM3Nzc1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkE2RUU3Mzg0ODdBODExRTY5NkUzRENEQzAwQUM3Nzc1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTZFRTczODE4N0E4MTFFNjk2RTNEQ0RDMDBBQzc3NzUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QTZFRTczODI4N0E4MTFFNjk2RTNEQ0RDMDBBQzc3NzUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz52IGyLAAAITElEQVR42rRYW2xcVxVd9955P+3MxG87duzYztNuTQ2BKG0JpYKWVkgtLUgEtahKf5BA4vHRwgcNhZ+CEAq0CKqKIBXVolUVVW0oagnFoSVNk9AHsWPHceK3PWN7bF/PyzOsfeaOcewZO2npsZbvnXMfZ5291977nKvteu0crrOVEHuJm4gdRDVRQejEODFCnCdOEW9afdfcbNdx723EQ8QdhKfIPVtX/U4Tx4nfEy9cyyD6NdyzizhG/IW4dx0yxSYsE3ie6Cb2fVRCjxDvEnfio7dPE28QP/+whJ4lDuP/375DvF7M0sUInSTux8fXbiEkmlzXQuhPVhR93K2JeHUjQqKZ+zZ6k0ZkslnMp5cwmUxjNJ7CeCKFmdQSEpns9ZASkT991btX5KFdloCLh4ymYSopg6fht+nY4nGizu3EJrsNi5kMhhaTGIonFEGXrqPG7YChCfkNiX2BeGV1Hnq2qPJJJMUBP5hbRHvQi+9vq8J91SGUOewF7//bVAxPDU7gxbFp+A0d1SSWWp/VH4nwSgvts0JyTTNIZo6uuWQm8O3GSjyxs071n4jE8NpkDO+T5JXFBAI2AzsCHrQRd1eUIuSw4Z/ReRx8pw9jdGWz14nkUmY9UgeJo3lCLxFfLKSVOGfWvxDH0Rub8LWaEJ7oH8WRi+MYJAm7rikiXlohRU3NcuAFDupj34FwAEc7muDjtT3H38alrI5tPhcSdLmmaYUIvUfsFkIBnsxY469yFWiBOI7sqcehLWV4+NwAnuobQZ3fgxKHAXJAtsAzaU7i/HwcHurs8u0dyMTmEH7+H9i6tQ4BujmdSHC0gqS2SZTdXoiMtAlGUGeJV5F55sqkItMRDioSvRwwyYHlwT5acJJR5qQ1BuhaQXvQo4LgUyfeQ6g0iKfb6nHx1DlFxuF2F3Pb3UKos9jVCKPpYG1YETjcO4xqvxtjiST2h/z49Z4GhKkTIXWE5/dS5APU0w+ba/CLXfXUXQbVLgd6Ywt4ddrEAx3b4aTFBnovYn56Bja7HbphQJn5f61TCG0vRkhyjUTVoMx6IaGEOszQPrA5iAfqNqPMZccS73mQ518loej8Ir7XVIlD9WVIs9+knpwc9GRklj7QESgNME9lEB0Zw8z4BLlkYVwdqS229QiJPyJ0WyvFKEKVpCfC/B1D+mR0DhfoNmnfPHuRkZZEHcnfc+oCnCIkTjxgNzBAAl49l3/tPIq+PB4XFufmkeE136ZS2EkqQ/IkWC53Vhbj4+fsukaiKGHiEwv00CVyPka9PDcSUQnPS6J/5j3neW0zE+UbTAevTMwo0Usmd/EdB+hiFcp3fAZBtwuznKSDx8zSEuamooqcRJ6u6z59vYpfQZd0cWApC0+2NaDM6WAKSCBIEpKhxRDislomviCtkeAsy5x2dW2ag/bPLuCR5ircEMwV9nYGxHc7WjBrJpQLDRt1JJY3F2HOzYnFlC2nixFy8LLkkZu7P1A55s39OxXJSCqdq2eyJCQhsZQgbeWiszETM7TOzziJR5urry7zlSGAE5NnLavAoMAzvD++YCaF0H/WE7XUoz5q5c63etBAl3yZWVjqmUTXCHUjlhiOJxWEjESepInufTvxg6aqNe9MqGydJRGNRiJoKUXMsMlxSkTdQ9xajJQMXEdS0VRa/RYXmSTRx/4n25vwjVqGO9PDKCNMorCRpN1G8XXfH5jLhIRNzxFRWVtZSh37hNCZjUqxlIPdjCBpx8ZnVCi/fksb9vpd6HrrXdzcWo9mS7jrtWf6hknoCmoCPqUhla1JRLeOdMkZIfTyRkuOCbqmgxk7Rj/fVR3G4xSqSSF+4jddOP3vPqCmHD/9XCe+dWMrvPbiG5kfn71AEduYIFl2LOsoA+lWodC0l/PFVazUXugl09TLFs6oe2/Lct8L53rwlRf/jvTCIny1FTAZhRm6rL62HJ9vqEILE6BBl8zy2Qe316PGm1upfvb4v3BifBr1fF9OP6IjPS/uiGbo4Tyhr4t714haqi7/PRR0YM9SEmco7ufe70fvJe4FWZ/8Qb6YfxK6gmlqKUsSEA3ptBTzTChcgsOd2/FwSx2ODU3irr+eRkOpX0WXvkLUJPYYDz/KE9Ks8A+uJuVmiE7MxDDIwojBIS6jSuEqDysL6Ia81IDGGmXwKLVJU/2GGsxGklMMAJMryPtb6/DJslL88vxlZDiyQ+6xtCOkmKZDjJfoyiXsPUTXakJZRpPN5YSNxCKXhzDc06dyhodmV6GaJyZkjDwxsVCuX/3m+aAZV7+b+NwSw16zRC3XWDIeJX6yek0Na4e6dlNI6jKwy+eFORvDSG8/TK5xnF5PrmqLDixryaB5K6mj5Roorehr8g/RQzKtxXYdX5Klztoiq6nKLCSkBjXcsBsVjfW0XobWYtZWLspZQg2m68tiVXkmf54XskWG/2KsZ51SZPOwFdnEXSC8a3lpSHJXIbMP19Yoi0WHR5FOJuHkokuslHfDMoFlEtqKRJhTLd9zq263xVYuiQql1FGijbhccEUi1uJMEqZJHflRua0RgXBIVW7pNwx9mYRuZeGryOQycpIy2kcrvmMwbxkUfx7Fcny/tU5aN2mmaC1pm6oqUFK+WblJ1jXKJXnd6Dmr5UmyvU3/71BfQ2gaCRqRQx7rfWwwrZ3IIWsTUMhcyjIpusxTEsCmygq1XpY+yU/KKuKGfGjLzjiTvUlNWPvw34d+SzQSjxFXCt7BmaWZECU5BkKlcPt8ah1uJbxp/viV7CiIxzfcpl/nJz3N2qXstz7ptVg7zvw2Ik4SEYZ+P8V/OmkudpOQbJEX1ILeIg/LfbJAE91kV6j6vwIMACy2PekDDZQ3AAAAAElFTkSuQmCC"

/***/ },

/***/ 495:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjZENTdFM0YwODdBOTExRTY4OUEyOTM3N0RDQzE2QkUwIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjZENTdFM0YxODdBOTExRTY4OUEyOTM3N0RDQzE2QkUwIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NkQ1N0UzRUU4N0E5MTFFNjg5QTI5Mzc3RENDMTZCRTAiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NkQ1N0UzRUY4N0E5MTFFNjg5QTI5Mzc3RENDMTZCRTAiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz56IakMAAAI30lEQVR42qRYa3BUZxl+znXvm4RcCQkJASENKVQJMzGVlI4TsJ1xrD+caoRBoaJ2HC8wlIpWfii0KGNnnNKRH7UFy9hBfzjFTmm1VbAToNRCqLAkISRAWHLbbDabvZw9N9/vnJO4uSwk9Ow8yZ49t+d73+e9He7AmZ9jnttywqOEJkIdYSEh6ByLE+4QQoQ2wj8JV+dzc3Ee57YSnnLI5NoChHLCGsIm57fThFcIR+fyEH4O5zxO+Jhw7B5kcm3NhCOES4SvfFpCvyO8RfgsPv32IOGvhMP34zIf4R3Cw5PMBY7+EkwTuqHfFyPOvtN2+vM5wkbCyFwIeQj/IaxgO4ZuQvYI8AQkIsNBU1XEYzGY7AEcNw86pv1AjsmMb6B9JoPVhNi9CJ2aIMPRx1cgoic0hNNHe4hcBhU1NfjG9h0QBBGqqswjelxIGQMYSJ8gLjotRqyinz9wXJmT0CHC2gkyqqHCTYYpl6rR3daO0eEQzHULUSCvgCQD6fmEM6lVVisxaJ6EwSXp7taj6wmvEb41GyEWDU9P7Bj00YhQXbARw/kK9h/4IV49ehRxI46MkYFpSEjpKWi6eW/tuD3w8DydOzJbLG0hvEE4OZ3QkeyzVOaeQDWqS6uw7/AzCHxchtKggEjEsI5rpGuvxMHv45GLkk5kFwRJkqqJjhRdI3vAjdOFwowrXnUS7CTVJwjVU00sYiARRlJJoLVxE9589328/a+zWFRaYhM2TIvQwoCQAyIalrH4SEF5cTueOH0UqWAe0m4fOHMGoTLCN7MttGf6GQIRSqkpnLr5dwh6GQKFFVC1MRiGZhudAkwjYyVVcxbLGChY4Malm0ls+9EuDIc+wZkb1/GUL4k9D1FGiY/DrVpBm70xDseYhRZPCHlKkNIqTNLRItcS5PmDePYn30FlWQndK+Ect4XqFrkpcFG+WlrhhpFI4eDeZxHvvQqzsBxrLmcQev0YdrVdg06aMiQe03zN6uJy0SkNM7aMrmChvxIry+ux5/hvUH6zBnVLitEeTlrHXfTwaMrAKMHKeKatmZIyHy5fTeKXP92F/t4OlOYHoaUSuEKo+1snXivwwVdDbivwWtcYmjGRMdn2ZdGJrpk1hRMQV0YtK62rbcaP9+6Hz2VgdfNj1vG0ZqLQw6MiT4TpLLW0UKKkqWDfr3YTmasoygsglUwgER8Fei+jvKEJ7zy6FMU9/fBHgyiuLgITQBapZuayB2YjJAkSEuo4zt45DY40ATmPCkoRXWdMakilr3GFrJSkREcJq/1WCl/dtgOfhEIoIctkUkkiE0M4dB4PNj2OX7zwW9QUeCihqoiERzHYG4EgCeBFbsJ9S/mJcJst01srT4soKy3Czh9shVcUoCgZW7h0SBaoEZI51Fa6Sd0p/Pq53bh+7SqFegBpxzLh0Ieo+/wG7Nz/klWGRodHINCFskfCSDiKoRsRiJJIpCxNFTFC/lwaKvKUoKGyEacunUGx34UvrKnF0MioXX0p5AfjGj4akXCkLY7Wp3fh/KUrKC8IQpkkcx51jRuwY/8hik4DI0P9VHIEa7EcmdgidTtKlhp2LMX7xbvVZoNCyeWSKcrysXfXQeQHOSxe/YidyZmFPB4EpEEcO7wbFy/0oXYxs0wSScdNtmUOkdh1RCP9Vv3L9gArzral7EWWVBeZzEKJ2ejIgoyR9BD+O3wBxZ5CKBke42RMl/T/tB9TBCzxcti3xYvFBRH09KUxTpa5zdzU2IId+2wytmVmX7tFivQX7Y9h8EYkyTs9cK7mBX1Dt1FZUYZNm78OPZ2GrulOHuLAiganCahf34K3Dy4Fr15H+MqHqG9qwc7nX6Z6d3cyk4+i3CW5RIwNjUX4XE04q2UBiqymyvXo6O3BI42r0NK8FuGBIVtjFGFBF4cqH/Xz3f/AssJevPW9TqzbuAGbnyHNUGRG5kAmq1Vi4u5mZ/+b8OT0cwzTIOH6URAIonOwCy/uO4KSBTLyy+vsnEPV58Sf29DZsQebHwsgFF6JzKpN2LJ2PeJJjcgMzo3MVP99IDo980szNeRCePwWbsR6sPGBDXjDOIex/jgal3ttC1HCXlKzCO2DW/D7vmpkPMvAJznkpwZhaqPzJsP6L47Hm+yqXsKF2Rp5vxTAxYHz6L4RQ0lBCVJKlPppslxQtmpZ/UNVqG/YiovneqGMhUmcHqde52NmsfZB0BRwad6qk1nlwtrneL6T57nQxDKeJxyf6VoTfneQxBlB37UuKu8xdFD7cP5sO/lbQpoyseyiQkri7e3qpsycgMfnsyqvOcMCHmiZMKTSNESKKnPanEBkDpDLwGVNrjcJlTNqmjsDZbgI6TMeckWUrOCGy7KEXVHZ6kTqZ9lef7gPMRoA3JSfskmxY7zOYUzQIa+MwePSqX7xk9bheX5IlMUSdmK2o1lf+950QuOU5IKBWvzsuW9nS97uPyamDuu7wEYUdHV2IDpCxGV5ilvyybLnbg/h5PU/kj41OiRPXktZexsjb1K2zSb0PuEPhK3ZhBYUFuNORxdav/sCAj7SDmfkjFyJ3MgSYf+tm+RGlXKLbJNlS8gI0L1JLG7WIIoy5TObAC+KxwVJPGHpiONmTB3bnGZtcjTRFQn+BWkoFW0YU1iuyD3sWu4TRXiKRUT6IhgZTUH2StYxRVBRXOYnd1VZ/ThLmmSZa5JbfNKa78zcc9k6QjuhyrpRKgOX14svbW6yCqJp3nvKEKgr0FQd4Y5+JMfScFG94gUembSGRNQenuheA5JbepiFO7PU3QbFmDPqvsveYpD6rawb7U/MfUYl0qwUBArzEI+kMDwcoyIqO7nP+ndZdIktRHIwm8zdXjawAarBeY0y/xmenqoqmmXR8hVl8OZ5KCVkbAEL/OuCLLER+s5s1r7X2w/2Puhr833pNIUU/V/4mTL48n1dZIxWXhQ2k5f0XMPcXN4P/cVpcxm5s/dB6iPKMd8PFvtXEIc/WVF3Fx3Op+C84mAV4YvOq5plhOKsrnOcMEzodl7pvUekLjAN6poxp4f8T4ABADrvkXEGyC/1AAAAAElFTkSuQmCC"

/***/ },

/***/ 496:
/***/ function(module, exports) {

	module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAlCAYAAADFniADAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkZGMzMxNEM3ODA5MDExRTY4RkU4OERERjQyMDg1NEFEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkZGMzMxNEM4ODA5MDExRTY4RkU4OERERjQyMDg1NEFEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RkYzMzE0QzU4MDkwMTFFNjhGRTg4RERGNDIwODU0QUQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RkYzMzE0QzY4MDkwMTFFNjhGRTg4RERGNDIwODU0QUQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4QjwGZAAAFe0lEQVR42sSYeUwUVxjAv52dPWZZdmEXBXRhtSAUEFwqGo13bam2KY3VYDxakkaDNhralLaJIbbBmrZ/2NC0tWnTI/5TE5uQSKvSEAsqIGlBLBChHJFrAYWdPdhlj5md6ZvhEISdPTj6JRM2w/fe+813ve89UWVhGoQiGMbKleG0QRFGp0ikzHIcZyIYRuSiaZHN5cQ7HXa82eUUd4cyNx6MsljMKqOiXQeiY5xHVRHUFg5MSJ+DMo3Iyh4NEJfso5J7ga4jCsRSmJhVxOkdhSvjxgpwCaMJ5estpPTP7q7wIptVcnfelorUeF5MSrX+IJN742EeEqHxPG/QmGqGjMSPXe2qd71ekd2nEYSsqH/G/nF6Jlk+X6Dpc8asdB7L3Gj6m1DQyUFBiUQg5qyDoD4CkSB4SIKS41nDBrImXEVlBQyVkGz7KmaF8y1YRJFIGO1ag/k6ofAm+oWKXTl2fIVu7CQsgaBSsixtnfk3Lqt9QiHqNQlJoyWwhMK5EnmmxCdUInIbl/5+Z5LrFhSMCxWVmto8C0od4dkWqXW/5G+C2+ZcqMW/gT7sFWBYkU89o0UO1Z0auN2hDSgrVyWMnptVp3R6x/t+R8YegF9+V8CWTUaoMmZCdzsFW+MewD7DEITLaXDTGJQ2xkJVexToYxQQn7Aeymt7YHvKLQDa5q+O7VaGU5mo8jfiEwG3XBPl3iM0qLR9J9y4SsCwaQTyjuTy72jaCxWln8M7VyrhjU29cOmuHnJ26ODiyVMg0T7H61ypeBOOX8mGnfGNcCSrSxAsOtaZx0Hx7tNo3XtQbZL4Uh5m10L94ww4nLsfVsREP9kOcDHszT0DxWdOQfG1FDh3vgT25/80BcTvCJFqOHzoKDSQm6HPTAhCaZe5c6ZiKiLSs0tI+dMyDVTXNQBByFF3MLu0xaW9BnI5AfGrU2fnhEzGj6tveghny5KF84fwruZ2D36FMCWdLqTsoZh5FMknDuBizp8glgxsklBQkw0dyuPxTP2Wilm/+mhPXMND+W9HQoeiKCq4Bg9nwzEU4P4bPZZdsgrPNY4YWo9GbaxbGMq7ZFA0jdmwcb9jQ35MNc1oDNjtjoAWcLndqOqzQcYgNsS7zunA2+Ryrz4QS6WlJMOHZz+B0VEH7N61DXL3vcqn/FSGuT1w9Vo53KyqBqlUAhvXG4KC4lh4KNQ31wnve+NfS5JmeD3n5anFb96qhrz8Aij6oIB/96CtA4o/+wLWpafCe6fzQakM49/fu9880TwKW407DdlH8fv8wQFtxlvXZZF3fCmfupwOTUaV76InEQHlZUEuFYHD5XvhzDgrfHmwxef/zaS0ovmeJpu3lNUqrXW7xf0ymXfOnuTghgFYpR2bHZQMBhWty8A1kfUcENc37E4ZBoVkZnLgqEZtTSQFLTU8RFx+0iWwwAz2K75F7cP5uZS3JZr4Zy55AQE09KjHiyPOgEFng8x4a/BZR2Hk8CP5rzNal4E+4qIu3lGICmlkMJNl6S38M1/p71WUTB67sGn1wdLdpSyC/0FcLnFPf2/YhTnb4UGj4jurRVq9xExsZ6vqBOMVjc0Jheqct61ZfQgFvXGpiHofKs+TJlm54BGLy8KWxsi9nDsXG2hogPgZhczZgA6j3DXOP/WaHR43NrhYQAN9iq/bW9XHZuxh/u4SEFhT41/ajTaLtGYhYVDVdna0qvI7/1Wd5kpRsBccvCvvN2i2cxMshDvNJtkfDXVRGSihvp/3/dRE86WO1Y29jY70J4K5hUHJQ5Ejsuv9PWEXUGbfWbBLs6fHcKdZtIFnc39R+5oklTGxqDmTTVZml1P80OHAWyyktBJl1g3Kgz1etOvFyY9HXUUt9zwNO1fQhiL/CTAAt38s4iZhspAAAAAASUVORK5CYII="

/***/ },

/***/ 497:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "images/enroll.png";

/***/ },

/***/ 498:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(499);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_articles.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_articles.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 499:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.articles-page-wrapper {\n  max-width: 1000px;\n  margin: 0px auto; }\n  .articles-page-wrapper .article {\n    margin: 10px 0px;\n    border: 1px solid #ccc;\n    padding: 10px 20px;\n    border-radius: 5px;\n    cursor: pointer;\n    color: #222;\n    font-size: 16px;\n    font-family: 'Roboto',sans-serif; }\n    .articles-page-wrapper .article a {\n      text-decoration: none;\n      display: -webkit-box;\n      display: -moz-box;\n      display: box;\n      display: -webkit-flex;\n      display: -moz-flex;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -moz-box-align: center;\n      box-align: center;\n      -webkit-align-items: center;\n      -moz-align-items: center;\n      -ms-align-items: center;\n      -o-align-items: center;\n      align-items: center;\n      -ms-flex-align: center;\n      -webkit-box-pack: justify;\n      -moz-box-pack: justify;\n      box-pack: justify;\n      -webkit-justify-content: space-between;\n      -moz-justify-content: space-between;\n      -ms-justify-content: space-between;\n      -o-justify-content: space-between;\n      justify-content: space-between;\n      -ms-flex-pack: justify;\n      -webkit-box-orient: horizontal;\n      -moz-box-orient: horizontal;\n      box-orient: horizontal;\n      -webkit-box-direction: normal;\n      -moz-box-direction: normal;\n      box-direction: normal;\n      -webkit-flex-direction: row;\n      -moz-flex-direction: row;\n      flex-direction: row;\n      -ms-flex-direction: row; }\n      .articles-page-wrapper .article a:hover {\n        text-decoration: none; }\n      .articles-page-wrapper .article a i {\n        font-size: 25px; }\n    .articles-page-wrapper .article:hover {\n      background: rgba(255, 255, 255, 0.2); }\n", ""]);

	// exports


/***/ },

/***/ 500:
/***/ function(module, exports) {

	module.exports = "<div class=\"articles-page-wrapper\">\n\t<div ng-repeat=\"article in vm.allArticles\">\n\t\t<div class=\"article\">\n\t\t\t<a ng-href=\"{{vm.getBlogArticleUrl(article.page_cat,article.page_id)}}\">\n\t\t\t\t{{article.page_nm}}\n\t\t\t\t<i class=\"fa fa-angle-right\"></i>\n\t\t\t</a>\n\t\t</div>\n\t</div>\n</div>";

/***/ }

});