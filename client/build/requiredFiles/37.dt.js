webpackJsonp([37],{

/***/ 416:
/***/ function(module, exports, __webpack_require__) {

	angular.module(window.moduleName)
	    .component('courseSubgroupPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(417),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state'];
	function Controller(admin,$filter,$scope,$state){
		var vm = this;

		vm.saveSubgroup = saveSubgroup;
		vm.deleteSubgroup = deleteSubgroup;

		vm.$onInit = function(){
			getAllSubgroup();
		}

		function getAllSubgroup(){
			admin.getAllSubgroup()
				.then(function(res){
					vm.allSubgroup = res.data
				})
		}

		function saveSubgroup(){
			admin.addSubgroup(vm.form)
				.then(function(res){
					getAllSubgroup();
					vm.form = {};
				})
				.catch(function(err){
					console.log(err);
				})
		}

		function deleteSubgroup(id){
			admin.deleteSubgroup({sbgrp_id : id})
				.then(function(res){
					getAllSubgroup();
				})
				.catch(function(err){
					console.log(err);
				})
		}
	}

/***/ },

/***/ 417:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Subgroup</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"Subgroup name\">Subgroup Display Name</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Subgroup name\" ng-model=\"vm.form.subgroupName\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"subgroup\">Subgroup name(in url)</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Subgroup name in url\" ng-model=\"vm.form.subgroupInUrl\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.form= {}\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.saveSubgroup()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allSubgroup\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allSubgroup\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"subgroup in vm.allSubgroup\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteSubgroup(subgroup.sbgrp_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t{{subgroup.subgroupName}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row skill-info-row\" ng-repeat=\"(key,value) in subgroup\">\n\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});