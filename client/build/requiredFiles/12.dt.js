webpackJsonp([12],{

/***/ 171:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(43);
	angular
	.module('service')
	.service('awsService', awsService);

	awsService.$inject = ['$http','Upload','BASE_API'];

	function awsService($http,Upload,BASE_API){

		this.uploadFileToS3 = function(data){
			return Upload.upload({
	            url: BASE_API + '/aws/s3/uploadFile',
	            data: data,
	            method: 'POST'
	        });
		}

		this.getS3SignedUrl = function(data){
			return $http.post(BASE_API + '/aws/s3/getSignedUrl',data);
		}
	}

/***/ },

/***/ 180:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(181);
	__webpack_require__(182);
	__webpack_require__(183);


/***/ },

/***/ 181:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {(function () {
		var tbioDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Directive');
			var link = function (scope, element, attrs, controllers) {
				//var idOfElement = '[' + element.prop('id') + '] ';
				//$log.log('In TBIO directive for ID ' + idOfElement);
				var tbioCtrl = controllers[0];
				var ngModelCtrl = controllers[1];
				if (ngModelCtrl) {
					tbioCtrl.init(element, attrs, ngModelCtrl);
				}
			};
			return {
				restrict: 'A',
				priority: 100,
				require: ['tbio', 'ngModel'],
				controller: 'TextboxioController',
				scope: {
					configuration: '=' //tbioConfiguration JavaScript object
				},
				link: link
			};
		}];

		var tbioRequiredDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Required Directive');
			var link = function (scope, element, attrs, ngModelCtrl) {
				if (!ngModelCtrl) return;
				attrs.required = true; // force truthy in case we are on non input element
				attrs.$observe('tbioRequired', function () {
					ngModelCtrl.$validate();
				});
				ngModelCtrl.$validators.tbioRequired = function (modelValue, viewValue) {
					var jStrippedString = jQuery(modelValue).text().trim();
					//$log.log('REQUIRED: ' + (!attrs.required || !ngModelCtrl.$isEmpty(jStrippedString)));
					return !attrs.required || !ngModelCtrl.$isEmpty(jStrippedString);
				};
			};
			return {
				restrict: 'A',
				require: '?ngModel',
				link: link
			};
		}];

		var tbioMinLengthDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Min Length Directive');
			var link = function (scope, element, attrs, ngModelCtrl) {
				if (!ngModelCtrl) return;
				var minlength = 0;
				attrs.$observe('tbioMinlength', function (value) {
					minlength = parseInt(value) || 0;
					ngModelCtrl.$validate();
				});
				ngModelCtrl.$validators.tbioMinlength = function (modelValue, viewValue) {
					var jStrippedString = jQuery(modelValue).text().trim();
					//$log.log('Min Length? ' + (ngModelCtrl.$isEmpty(jStrippedString) || jStrippedString.length >= minlength));
					return ngModelCtrl.$isEmpty(jStrippedString) || jStrippedString.length >= minlength;
				};
			};
			return {
				restrict: 'A',
				require: 'ngModel',
				link: link
			};
		}];

		var tbioMaxLengthDirective = ['$log', function ($log) {
			//$log.log('Loading Textbox.io Max Length Directive');
			var link = function (scope, element, attrs, ngModelCtrl) {
				if (!ngModelCtrl) return;
				var maxlength = -1;
				attrs.$observe('tbioMaxlength', function (value) {
					maxlength = isNaN(parseInt(value)) ? -1 : parseInt(value);
					ngModelCtrl.$validate();
				});
				ngModelCtrl.$validators.tbioMaxlength = function (modelValue, viewValue) {
					var jStrippedString = jQuery(modelValue).text().trim();
					//$log.log('Max Length? ' + ((maxlength < 0) || ngModelCtrl.$isEmpty(jStrippedString) || (jStrippedString.length <= maxlength)));
					return (maxlength < 0) || ngModelCtrl.$isEmpty(jStrippedString) || (jStrippedString.length <= maxlength);
				};
			};
			return {
				restrict: 'A',
				require: 'ngModel',
				link: link
			};
		}];

		var tbioController = ['$scope', '$interval', '$log', 'tbioConfigFactory', 'tbioValidationsFactory',
							  function ($scope, $interval, $log, tbioConfigFactory, tbioValidationsFactory) {
				//$log.log('Loading Textbox.io Controller');
				this.init = function (element, attrs, ngModelController) {
					//var idOfElement = '[' + element.prop('id') + '] ';
					//$log.log('In this.init for ' + idOfElement);
					var theEditor;
					var config = attrs['configuration'];

					//Populate the editor once the modelValue exists
					//would reload the editor if the model is changed in the background.
					ngModelController.$render = function () {
						if (!theEditor) { //only load the editor the first time
							if (tbioConfigFactory.hasOwnProperty(config)) {
								theEditor = textboxio.replace(element[0], tbioConfigFactory[config]);
							} else {
								theEditor = textboxio.replace(element[0]);
							}
						}
						if (ngModelController.$modelValue) {
							theEditor.content.set(ngModelController.$modelValue);
							ngModelController.$setPristine()
						}
					}; //$render end

					// In lieu of events I just update the model every X seconds.
					// Once the editor has event(s) this gets replaced by event code.
					var interval = $interval(function () {
						//Workaround to keep $pristine accurate until you type into the editor
						if (ngModelController.$viewValue && (ngModelController.$viewValue != theEditor.content.get())) {
							//$log.log('Content Changed!');
							ngModelController.$setViewValue(theEditor.content.get());
							return;
						}
						//Check for the default "empty" string and don't put anything in the view when "empty" is there
						//to start with.  The prior if will catch having editor content that is completely deleted to revert
						//to "empty".
						if (!('<p><br /></p>' == theEditor.content.get())) {
							ngModelController.$setViewValue(theEditor.content.get());
						}
					}, 500); // interval end

					// When the DOM element is removed from the page AngularJS will trigger the $destroy event on
					// the scope. This gives us a chance to cancel the $interval.
					$scope.$on("$destroy", function (event) {
						$interval.cancel(interval);
					}); //$on end

					//Allow developer to inject custom validation functions via tbioValidationsFactory
					for (var validationFn in tbioValidationsFactory) {
						$log.log('Adding custom validation!');
						ngModelController.$validators[validationFn] = tbioValidationsFactory[validationFn];
					};
				}; //init end
		}];

		//Create the actual Controller and Directive
		angular.module('ephox.textboxio', [])
			.controller('TextboxioController', tbioController)
			.directive('tbio', tbioDirective)
			.directive('tbioMinlength', tbioMinLengthDirective)
			.directive('tbioMaxlength', tbioMaxLengthDirective)
			.directive('tbioRequired', tbioRequiredDirective);
	}());

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 182:
/***/ function(module, exports) {

	angular.module('ephox.textboxio').factory('tbioConfigFactory', ['$log', function ($log) {
		//    $log.log('Loading tbioConfigFactory');
		var configurations = {};

		configurations.default = {
			// basePath: '../textboxio/resources',
			css: {
				stylesheets: [''],
				styles: [
					{
						rule: 'p',
						text: 'Paragraph'
						},
					{
						rule: 'h1',
						text: 'H1'
						},
					{
						rule: 'h2',
						text: 'H2'
						},
					{
						rule: 'h3',
						text: 'H3'
						},
					{
						rule: 'h4',
						text: 'H4'
						},
					{
						rule: 'div',
						text: 'div'
						},
					{
						rule: 'pre',
						text: 'pre'
						}
				]
			},
			codeview: {
				enabled: true,
				showButton: true
			},
			images: {
				upload : {
					url : '/ckeditor/uploadTextboxIOImage'
				},
				allowLocal: true,
				editing: {
	                enabled: true,
	                preferredWidth : 1024,
	                preferredHeight : 1024
	            }
			},
			languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
			// locale : '', // Default locale is inferred from client browser
			paste: {
				style: 'prompt'
			},
			// spelling : {},
			ui: {
				toolbar: {
					items: ['undo', 'insert', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
				}
			}
		};

		configurations.simple = {
			//        basePath: '/sites/all/libraries/textboxio/resources',
			css: {
				stylesheets: [''],
				styles: [
					{
						rule: 'p',
						text: 'block.p'
					},
					{
						rule: 'div',
						text: 'block.div'
					},
					{
						rule: 'h1',
						text: 'block.h1'
					},
					{
						rule: 'h2',
						text: 'block.h2'
					},
					{
						rule: 'h3',
						text: 'block.h3'
					},
					{
						rule: 'h4',
						text: 'block.h4'
					}
				]
			},
			codeview: {
				enabled: false,
				showButton: false
			},
			images: {
				// upload : {},
				allowLocal: true
			},
			languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
			// locale : '', // Default locale is inferred from client browser
			paste: {
				style: 'prompt'
			},
			// spelling : {},
			ui: {
				toolbar: {
					items: ['emphasis']
				}
			}
		};

		configurations.videonotes = {
			// basePath: '../textboxio/resources',
			css: {
				stylesheets: [''],
				styles: [
					{
						rule: 'p',
						text: 'Paragraph'
						},
					{
						rule: 'h1',
						text: 'H1'
						},
					{
						rule: 'h2',
						text: 'H2'
						},
					{
						rule: 'h3',
						text: 'H3'
						},
					{
						rule: 'h4',
						text: 'H4'
						},
					{
						rule: 'div',
						text: 'div'
						},
					{
						rule: 'pre',
						text: 'pre'
						}
				]
			},
			codeview: {
				enabled: false,
				showButton: false
			},
			images: {
				upload : {
					url : '/ckeditor/uploadTextboxIOImage'
				},
				allowLocal: true,
				editing: {
	                enabled: true,
	                preferredWidth : 1024,
	                preferredHeight : 1024
	            }
			},
			languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
			// locale : '', // Default locale is inferred from client browser
			paste: {
				style: 'prompt'
			},
			// spelling : {},
			ui: {
				toolbar: {
					items: ['undo', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
				}
			}
		};

		return configurations;
	}]);


/***/ },

/***/ 183:
/***/ function(module, exports) {

	// You can add your own custom validation functions that will be added to the $validators pipeline for all textboxio
	// instances on the page.

	angular.module('ephox.textboxio').factory('tbioValidationsFactory', ['$log', function ($log) {
		//    $log.log('Loading tbioValidationsFactory');
		var validations = [];
		/*
			validations['sampleValidation1'] = function (modelValue, viewValue) {
				//        $log.log('Model value: [' + modelValue + '  View value:[' + viewValue + ']');
				return true;
			};

			validations['sampleValidation2'] = function (modelValue, viewValue) {
				//        $log.log('Model value: [' + modelValue + '  View value:[' + viewValue + ']');
				return true;
			}
		*/
		return validations;
	}]);


/***/ },

/***/ 348:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {/*

		jQuery Tags Input Plugin 1.3.3

		Copyright (c) 2011 XOXCO, Inc

		Documentation for this plugin lives here:
		http://xoxco.com/clickable/jquery-tags-input

		Licensed under the MIT license:
		http://www.opensource.org/licenses/mit-license.php

		ben@xoxco.com

	*/

	(function($) {

		var delimiter = new Array();
		var tags_callbacks = new Array();
		$.fn.doAutosize = function(o){
		    var minWidth = $(this).data('minwidth'),
		        maxWidth = $(this).data('maxwidth'),
		        val = '',
		        input = $(this),
		        testSubject = $('#'+$(this).data('tester_id'));

		    if (val === (val = input.val())) {return;}

		    // Enter new content into testSubject
		    var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		    testSubject.html(escaped);
		    // Calculate new width + whether to change
		    var testerWidth = testSubject.width(),
		        newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
		        currentWidth = input.width(),
		        isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
		                             || (newWidth > minWidth && newWidth < maxWidth);

		    // Animate width
		    if (isValidWidthChange) {
		        input.width(newWidth);
		    }


	  };
	  $.fn.resetAutosize = function(options){
	    // alert(JSON.stringify(options));
	    var minWidth =  $(this).data('minwidth') || options.minInputWidth || $(this).width(),
	        maxWidth = $(this).data('maxwidth') || options.maxInputWidth || ($(this).closest('.tagsinput').width() - options.inputPadding),
	        val = '',
	        input = $(this),
	        testSubject = $('<tester/>').css({
	            position: 'absolute',
	            top: -9999,
	            left: -9999,
	            width: 'auto',
	            fontSize: input.css('fontSize'),
	            fontFamily: input.css('fontFamily'),
	            fontWeight: input.css('fontWeight'),
	            letterSpacing: input.css('letterSpacing'),
	            whiteSpace: 'nowrap'
	        }),
	        testerId = $(this).attr('id')+'_autosize_tester';
	    if(! $('#'+testerId).length > 0){
	      testSubject.attr('id', testerId);
	      testSubject.appendTo('body');
	    }

	    input.data('minwidth', minWidth);
	    input.data('maxwidth', maxWidth);
	    input.data('tester_id', testerId);
	    input.css('width', minWidth);
	  };

		$.fn.addTag = function(value,options) {
				options = jQuery.extend({focus:false,callback:true},options);
				this.each(function() {
					var id = $(this).attr('id');

					var tagslist = $(this).val().split(delimiter[id]);
					if (tagslist[0] == '') {
						tagslist = new Array();
					}

					value = jQuery.trim(value);

					if (options.unique) {
						var skipTag = $(this).tagExist(value);
						if(skipTag == true) {
						    //Marks fake input as not_valid to let styling it
	    				    $('#'+id+'_tag').addClass('not_valid');
	    				}
					} else {
						var skipTag = false;
					}

					if (value !='' && skipTag != true) {
	                    $('<span>').addClass('tag').append(
	                        $('<span>').text(value).append('&nbsp;&nbsp;'),
	                        $('<a>', {
	                            href  : '#',
	                            title : 'Removing tag',
	                            text  : 'x'
	                        }).click(function () {
	                            return $('#' + id).removeTag(escape(value));
	                        })
	                    ).insertBefore('#' + id + '_addTag');

						tagslist.push(value);

						$('#'+id+'_tag').val('');
						if (options.focus) {
							$('#'+id+'_tag').focus();
						} else {
							$('#'+id+'_tag').blur();
						}

						$.fn.tagsInput.updateTagsField(this,tagslist);

						if (options.callback && tags_callbacks[id] && tags_callbacks[id]['onAddTag']) {
							var f = tags_callbacks[id]['onAddTag'];
							f.call(this, value);
						}
						if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
						{
							var i = tagslist.length;
							var f = tags_callbacks[id]['onChange'];
							f.call(this, $(this), tagslist[i-1]);
						}
					}

				});

				return false;
			};

		$.fn.removeTag = function(value) {
				value = unescape(value);
				this.each(function() {
					var id = $(this).attr('id');

					var old = $(this).val().split(delimiter[id]);

					$('#'+id+'_tagsinput .tag').remove();
					str = '';
					for (i=0; i< old.length; i++) {
						if (old[i]!=value) {
							str = str + delimiter[id] +old[i];
						}
					}

					$.fn.tagsInput.importTags(this,str);

					if (tags_callbacks[id] && tags_callbacks[id]['onRemoveTag']) {
						var f = tags_callbacks[id]['onRemoveTag'];
						f.call(this, value);
					}
				});

				return false;
			};

		$.fn.tagExist = function(val) {
			var id = $(this).attr('id');
			var tagslist = $(this).val().split(delimiter[id]);
			return (jQuery.inArray(val, tagslist) >= 0); //true when tag exists, false when not
		};

	   // clear all existing tags and import new ones from a string
	   $.fn.importTags = function(str) {
	      var id = $(this).attr('id');
	      $('#'+id+'_tagsinput .tag').remove();
	      $.fn.tagsInput.importTags(this,str);
	   }

		$.fn.tagsInput = function(options) {
	    var settings = jQuery.extend({
	      interactive:true,
	      defaultText:'add a tag',
	      minChars:0,
	      width:'300px',
	      height:'100px',
	      autocomplete: {selectFirst: false },
	      hide:true,
	      delimiter: ',',
	      unique:true,
	      removeWithBackspace:true,
	      placeholderColor:'#666666',
	      autosize: true,
	      comfortZone: 20,
	      inputPadding: 6*2
	    },options);

	    	var uniqueIdCounter = 0;

			this.each(function() {
	         // If we have already initialized the field, do not do it again
	         if (typeof $(this).attr('data-tagsinput-init') !== 'undefined') {
	            return;
	         }

	         // Mark the field as having been initialized
	         $(this).attr('data-tagsinput-init', true);

				if (settings.hide) {
					$(this).hide();
				}
				var id = $(this).attr('id');
				if (!id || delimiter[$(this).attr('id')]) {
					id = $(this).attr('id', 'tags' + new Date().getTime() + (uniqueIdCounter++)).attr('id');
				}

				var data = jQuery.extend({
					pid:id,
					real_input: '#'+id,
					holder: '#'+id+'_tagsinput',
					input_wrapper: '#'+id+'_addTag',
					fake_input: '#'+id+'_tag'
				},settings);

				delimiter[id] = data.delimiter;

				if (settings.onAddTag || settings.onRemoveTag || settings.onChange) {
					tags_callbacks[id] = new Array();
					tags_callbacks[id]['onAddTag'] = settings.onAddTag;
					tags_callbacks[id]['onRemoveTag'] = settings.onRemoveTag;
					tags_callbacks[id]['onChange'] = settings.onChange;
				}

				var markup = '<div id="'+id+'_tagsinput" class="tagsinput"><div id="'+id+'_addTag">';

				if (settings.interactive) {
					markup = markup + '<input id="'+id+'_tag" value="" data-default="'+settings.defaultText+'" />';
				}

				markup = markup + '</div><div class="tags_clear"></div></div>';

				$(markup).insertAfter(this);

				$(data.holder).css('width',settings.width);
				$(data.holder).css('min-height',settings.height);
				$(data.holder).css('height',settings.height);

				if ($(data.real_input).val()!='') {
					$.fn.tagsInput.importTags($(data.real_input),$(data.real_input).val());
				}
				if (settings.interactive) {
					$(data.fake_input).val($(data.fake_input).attr('data-default'));
					$(data.fake_input).css('color',settings.placeholderColor);
			        $(data.fake_input).resetAutosize(settings);

					$(data.holder).bind('click',data,function(event) {
						$(event.data.fake_input).focus();
					});

					$(data.fake_input).bind('focus',data,function(event) {
						if ($(event.data.fake_input).val()==$(event.data.fake_input).attr('data-default')) {
							$(event.data.fake_input).val('');
						}
						$(event.data.fake_input).css('color','#000000');
					});

					if (settings.autocomplete_url != undefined) {
						autocomplete_options = {source: settings.autocomplete_url};
						for (attrname in settings.autocomplete) {
							autocomplete_options[attrname] = settings.autocomplete[attrname];
						}

						if (jQuery.Autocompleter !== undefined) {
							$(data.fake_input).autocomplete(settings.autocomplete_url, settings.autocomplete);
							$(data.fake_input).bind('result',data,function(event,data,formatted) {
								if (data) {
									$('#'+id).addTag(data[0] + "",{focus:true,unique:(settings.unique)});
								}
						  	});
						} else if (jQuery.ui.autocomplete !== undefined) {
							$(data.fake_input).autocomplete(autocomplete_options);
							$(data.fake_input).bind('autocompleteselect',data,function(event,ui) {
								$(event.data.real_input).addTag(ui.item.value,{focus:true,unique:(settings.unique)});
								return false;
							});
						}


					} else {
							// if a user tabs out of the field, create a new tag
							// this is only available if autocomplete is not used.
							$(data.fake_input).bind('blur',data,function(event) {
								var d = $(this).attr('data-default');
								if ($(event.data.fake_input).val()!='' && $(event.data.fake_input).val()!=d) {
									if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
										$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
								} else {
									$(event.data.fake_input).val($(event.data.fake_input).attr('data-default'));
									$(event.data.fake_input).css('color',settings.placeholderColor);
								}
								return false;
							});

					}
					// if user types a default delimiter like comma,semicolon and then create a new tag
					$(data.fake_input).bind('keypress',data,function(event) {
						if (_checkDelimiter(event)) {
						    event.preventDefault();
							if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
								$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
						  	$(event.data.fake_input).resetAutosize(settings);
							return false;
						} else if (event.data.autosize) {
				            $(event.data.fake_input).doAutosize(settings);

	          			}
					});
					//Delete last tag on backspace
					data.removeWithBackspace && $(data.fake_input).bind('keydown', function(event)
					{
						if(event.keyCode == 8 && $(this).val() == '')
						{
							 event.preventDefault();
							 var last_tag = $(this).closest('.tagsinput').find('.tag:last').text();
							 var id = $(this).attr('id').replace(/_tag$/, '');
							 last_tag = last_tag.replace(/[\s]+x$/, '');
							 $('#' + id).removeTag(escape(last_tag));
							 $(this).trigger('focus');
						}
					});
					$(data.fake_input).blur();

					//Removes the not_valid class when user changes the value of the fake input
					if(data.unique) {
					    $(data.fake_input).keydown(function(event){
					        if(event.keyCode == 8 || String.fromCharCode(event.which).match(/\w+|[áéíóúÁÉÍÓÚñÑ,/]+/)) {
					            $(this).removeClass('not_valid');
					        }
					    });
					}
				} // if settings.interactive
			});

			return this;

		};

		$.fn.tagsInput.updateTagsField = function(obj,tagslist) {
			var id = $(obj).attr('id');
			$(obj).val(tagslist.join(delimiter[id]));
		};

		$.fn.tagsInput.importTags = function(obj,val) {
			$(obj).val('');
			var id = $(obj).attr('id');
			var tags = val.split(delimiter[id]);
			for (i=0; i<tags.length; i++) {
				$(obj).addTag(tags[i],{focus:false,callback:false});
			}
			if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
			{
				var f = tags_callbacks[id]['onChange'];
				f.call(obj, obj, tags[i]);
			}
		};

	   /**
	     * check delimiter Array
	     * @param event
	     * @returns {boolean}
	     * @private
	     */
	   var _checkDelimiter = function(event){
	      var found = false;
	      if (event.which == 13) {
	         return true;
	      }

	      if (typeof event.data.delimiter === 'string') {
	         if (event.which == event.data.delimiter.charCodeAt(0)) {
	            found = true;
	         }
	      } else {
	         $.each(event.data.delimiter, function(index, delimiter) {
	            if (event.which == delimiter.charCodeAt(0)) {
	               found = true;
	            }
	         });
	      }

	      return found;
	   }
	})(jQuery);

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 349:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(350);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!./jquery.tagsinput.min.css", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!./jquery.tagsinput.min.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 350:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "div.tagsinput{border:1px solid #CCC;background:#FFF;padding:5px;width:300px;height:100px;overflow-y:auto}div.tagsinput span.tag{border:1px solid #a5d24a;-moz-border-radius:2px;-webkit-border-radius:2px;display:block;float:left;padding:5px;text-decoration:none;background:#cde69c;color:#638421;margin-right:5px;margin-bottom:5px;font-family:helvetica;font-size:13px}div.tagsinput span.tag a{font-weight:700;color:#82ad2b;text-decoration:none;font-size:11px}div.tagsinput input{width:80px;margin:0 5px 5px 0;font-family:helvetica;font-size:13px;border:1px solid transparent;padding:5px;background:0 0;color:#000;outline:0}div.tagsinput div{display:block;float:left}.tags_clear{clear:both;width:100%;height:0}.not_valid{background:#FBD8DB!important;color:#90111A!important}", ""]);

	// exports


/***/ },

/***/ 351:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(43);
	angular
	.module('service')
	.service('uploadFileAPI', UploadFileAPI);

	UploadFileAPI.$inject = ['$http','Upload','BASE_API'];

	function UploadFileAPI($http,Upload,BASE_API){

		this.uploadVideoEntity = function(data){
			return Upload.upload({
	            url: BASE_API + '/videoentity/uploadVideoEntity',
	            data: data,
	            method: 'POST'
	        });
		}

		this.uploadUserMaterial = function(data){
			return Upload.upload({
	            url: BASE_API + '/moduleItems/uploadUserMaterial',
	            data: data,
	            method: 'POST'
	        });
		}
	}

/***/ },

/***/ 353:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(348);
	__webpack_require__(349);
	__webpack_require__(171);
	__webpack_require__(351);
	__webpack_require__(112);
	__webpack_require__(180);

	angular.module(window.moduleName)
	    .component('bookTopicPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(354),
	        controller: AdminVideoEntityController,
	        controllerAs: 'vm'
	    });

	AdminVideoEntityController.$inject = ['$log', 'admin', 'pageLoader', '$filter', '$scope', 'EditorConfig', 'awsService', 'uploadFileAPI'];

	function AdminVideoEntityController($log, admin, pageLoader, $filter, $scope, EditorConfig, awsService, uploadFileAPI) {

	    var vm = this;
	    vm.EditorConfig = EditorConfig;
	    vm.selectedSubjects = [];
	    vm.selectedAuthors = [];
	    vm.selectedPublications = [];
	    vm.selectedBooks = [];
	    vm.selectedExams = [];
	    vm.selectedTopics = [];
	    vm.selectedRelatedTopics = [];
	    vm.selectedRelatedVideos = [];
	    // vm.selectedRelatedCourses = [];
	    vm.selectedTags = [];
	    vm.noOfRefLinks = 1;
	    vm.form = {};
	    vm.form.video = {};
	    vm.form.video.thumbUrl = ""; //"https://examwarrior.com/images/no-thumb.png";
	    vm.form.video.videoSource = "harddrive";
	    vm.form.video.refLinks = [];
	    vm.videosOfModule = [];
	    vm.shouldShowVideoForm = false;
	    vm.isVideoUploadButtonDisabled = true;


	    
	    vm.$onInit = function() {
	        $scope.$watch(function() {
	            return vm.parent.courselistdata;
	        }, function(v) {
	            vm.courselist = v;
	        });
	        updateBookTopics();
	        findAllBookTopics();
	        admin.getAllSubject()
	            .then(function(d) {
	                vm.subjects = d.data;
	            });
	        admin.getAllAuthor()
	            .then(function(d) {
	                vm.authors = d.data;
	            });
	        admin.getAllPublication()
	            .then(function(d) {
	                vm.publications = d.data;
	            });
	        admin.getAllBook()
	            .then(function(d) {
	                vm.books = d.data;
	            });
	        admin.getAllExam()
	            .then(function(d) {
	                vm.exams = d.data;
	            });
	        admin.getAllTopic()
	            .then(function(d) {
	                vm.topics = d.data;
	            });
	        admin.getAllTag()
	            .then(function(d) {
	                vm.tags = d.data;
	            });
	    };


	    vm.updateBookTopic = updateBookTopic;
	    vm.removeBookTopic = removeBookTopic;
	    vm.editBookTopic = editBookTopic;
	    vm.resetFormDetail = resetFormDetail;
	    vm.updateBookTopics = updateBookTopics;

	    function resetFormDetail() {
	        vm.selectedAuthors = [];
	        vm.selectedPublications = [];
	        vm.selectedBooks = [];
	        vm.selectedExams = [];
	        vm.selectedTopics = [];
	        vm.selectedRelatedTopics = [];
	        vm.selectedRelatedVideos = [];
	        vm.selectedTags = [];
	        vm.form = {};
	        vm.form.bookTopicContent = "<p><br/></p>";
	    }

	    function findAllBookTopics() {
	        admin.findBookTopicsByQuery({})
	            .then(function(data) {
	                if (data.data.length > 0) {
	                    vm.relatedbookTopics = data.data;
	                    vm.relatedbookTopics.sort(compare);
	                    vm.relatedbookTopicNumber = vm.relatedbookTopics[vm.relatedbookTopics.length - 1]['bookTopicNumber'] + 1;
	                } else {
	                    vm.relatedbookTopicNumber = 1;
	                    vm.relatedbookTopics = undefined;
	                }
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }

	    function updateBookTopics() {
	        if (vm.selectedSubjects.length > 0) {
	            admin.getAllTopic({
	                    subjectId: vm.selectedSubjects[0]
	                })
	                .then(function(d) {
	                    vm.topics = d.data;
	                });
	        }
	        if (vm.selectedTopics.length > 0) {
	            var topic = $filter('filter')(vm.topics, {
	                topicId: vm.selectedTopics[0]
	            })[0];
	            vm.form = vm.form || {};
	            vm.form.bookTopicName = topic.topicName;
	        } else {
	            vm.form = vm.form || {};
	            vm.form.bookTopicName = "";
	        }

	        var query = {
	            subjects: vm.selectedSubjects,
	            authors: vm.selectedAuthors,
	            publications: vm.selectedPublications,
	            books: vm.selectedBooks,
	            exams: vm.selectedExams,
	            topics: vm.selectedTopics,
	            tags: vm.selectedTags
	        }
	        admin.findBookTopicsByQuery(query)
	            .then(function(data) {
	                if (data.data.length > 0) {
	                    vm.bookTopics = data.data;
	                    vm.bookTopics.sort(compare);
	                    vm.bookTopicNumber = vm.bookTopics[vm.bookTopics.length - 1]['bookTopicNumber'] + 1;
	                } else {
	                    vm.bookTopicNumber = 1;
	                    vm.bookTopics = undefined;
	                }
	            })
	            .catch(function(err) {
	                console.error(err);
	            })
	    }


	    function updateBookTopic() {
	        //Add Video Id
	        vm.form.bookTopicId = (vm.selectedTopics[0] || '') + "-" + vm.bookTopicNumber;
	        
	        var data = {
	                bookTopicId: vm.form.bookTopicId,
	                bookTopicNumber: vm.bookTopicNumber,
	                bookTopicName: vm.form.bookTopicName,
	                bookTopicContent: vm.form.bookTopicContent,
	                subjects: vm.selectedSubjects,
	                authors: vm.selectedAuthors,
	                publications: vm.selectedPublications,
	                books: vm.selectedBooks,
	                exams: vm.selectedExams,
	                topics: vm.selectedTopics,
	                tags: vm.selectedTags
	            }
	            // console.log(data);
	        pageLoader.show();
	        admin.updateBookTopic(data)
	            .then(function() {
	                vm.resetFormDetail();
	                vm.submitted = true;
	                vm.form.error = false;
	                updateBookTopics();
	            })
	            .catch(function() {
	                vm.submitted = true;
	                vm.form.error = false;
	            })
	            .finally(function() {
	                vm.form = {};
	                pageLoader.hide();
	            });
	    }


	    function editBookTopic(bookTopicId) {
	        var bookTopic = $filter('filter')(vm.bookTopics, {
	            bookTopicId: bookTopicId
	        })[0];
	        // console.log(video);
	        vm.form = bookTopic;

	        vm.bookTopicNumber = bookTopic.bookTopicNumber;

	        vm.selectedSubjects = bookTopic.subjects;
	        vm.selectedAuthors = bookTopic.authors;
	        vm.selectedPublications = bookTopic.publications;
	        vm.selectedBooks = bookTopic.books;
	        vm.selectedExams = bookTopic.exams;
	        vm.selectedTopics = bookTopic.topics;
	        vm.selectedTags = bookTopic.tags;
	    }

	    function removeBookTopic(bookTopicId) {
	        var result = confirm("Want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        pageLoader.show();
	        admin.removeBookTopic({
	                bookTopicId: bookTopicId
	            })
	            .then(function(d) {
	                updateBookTopics();
	            })
	            .catch(function() {
	                console.error(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            });
	    }

	    function compare(a, b) {
	        return a.bookTopicNumber - b.bookTopicNumber;
	    }
	}

/***/ },

/***/ 354:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" name=\"form\" onsubmit=\"return false;\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Book Topic</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Subject</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedSubjects\" theme=\"bootstrap\" on-select=\"vm.updateBookTopics($item, $model)\" on-remove=\"vm.updateBookTopics($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.subjectId as item in (vm.subjects | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"author\">Author</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedAuthors\" theme=\"bootstrap\" on-select=\"vm.updateBookTopics($item, $model)\" on-remove=\"vm.updateBookTopics($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Author...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.authorId as item in (vm.authors | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"publication\">Publication</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedPublications\" theme=\"bootstrap\" on-select=\"vm.updateBookTopics($item, $model)\" on-remove=\"vm.updateBookTopics($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Publication...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.publicationName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.publicationId as item in (vm.publications | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.publicationName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"book\">Book</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBooks\" theme=\"bootstrap\" on-select=\"vm.updateBookTopics($item, $model)\" on-remove=\"vm.updateBookTopics($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Book...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.bookName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.bookId as item in (vm.books | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.bookName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"exam\">Exam</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedExams\" theme=\"bootstrap\" on-select=\"vm.updateBookTopics($item, $model)\" on-remove=\"vm.updateBookTopics($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Exam...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.examName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.examId as item in (vm.exams | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.examName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Topic</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTopics\" theme=\"bootstrap\" on-select=\"vm.updateBookTopics($item, $model)\" on-remove=\"vm.updateBookTopics($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Related Topic</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedRelatedTopics\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Related Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tag\">Tag</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTags\" theme=\"bootstrap\" on-select=\"vm.updateBookTopics($item, $model)\" on-remove=\"vm.updateBookTopics($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Tag...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.tagName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.tagId as item in (vm.tags | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.tagName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"booktopicname\">Name</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"booktopicname\" class=\"form-control\" placeholder=\"Book Topic Name\" ng-model=\"vm.form.bookTopicName\" name=\"booktopicname\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topicContent\">Topic Content</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea tbio configuration=\"default\" id=\"topicContent\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.bookTopicContent\" name=\"desc\" rows=\"10\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Book Topic</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"vm.selectedTopics.length <= 0 || !vm.form.bookTopicName\" ng-click=\"vm.updateBookTopic()\">\n\t\t\t\t\t\t\t\t\tSave\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.bookTopics\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.bookTopics\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"bookTopic in vm.bookTopics\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div  class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeBookTopic(bookTopic.bookTopicId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editBookTopic(bookTopic.bookTopicId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{bookTopic.bookTopicName}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in bookTopic\">\n\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t.video-upload-btn.disabled{\n\t\tpointer-events: none;\n\t}\n</style>";

/***/ }

});