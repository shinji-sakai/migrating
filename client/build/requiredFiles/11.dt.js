webpackJsonp([11],{

/***/ 171:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(43);
	angular
	.module('service')
	.service('awsService', awsService);

	awsService.$inject = ['$http','Upload','BASE_API'];

	function awsService($http,Upload,BASE_API){

		this.uploadFileToS3 = function(data){
			return Upload.upload({
	            url: BASE_API + '/aws/s3/uploadFile',
	            data: data,
	            method: 'POST'
	        });
		}

		this.getS3SignedUrl = function(data){
			return $http.post(BASE_API + '/aws/s3/getSignedUrl',data);
		}
	}

/***/ },

/***/ 347:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(348);
	__webpack_require__(349);
	__webpack_require__(171);
	__webpack_require__(351);
		angular.module(window.moduleName)
	        .component('videoEntity', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(352),
	            controller: AdminVideoEntityController,
	            controllerAs: 'vm'
	        });

		AdminVideoEntityController.$inject=['$log','admin','pageLoader','$filter','$scope','EditorConfig','awsService','uploadFileAPI'];
		function AdminVideoEntityController($log,admin,pageLoader,$filter,$scope,EditorConfig,awsService,uploadFileAPI) {

	    	var vm = this;

	    	vm.selectedSubjects = [];
	    	vm.selectedAuthors = [];
	    	vm.selectedPublications = [];
	    	vm.selectedBooks = [];
	    	vm.selectedExams = [];
	    	vm.selectedTopics = [];
	    	vm.selectedRelatedTopics = [];
	    	vm.selectedRelatedVideos = [];
	    	// vm.selectedRelatedCourses = [];
	    	vm.selectedTags = [];
	    	vm.noOfRefLinks = 1;
	    	vm.form = {};
	    	vm.form.video = {};
	    	vm.form.video.thumbUrl = "";//"https://examwarrior.com/images/no-thumb.png";
	    	vm.form.video.videoSource = "harddrive";
	    	vm.form.video.refLinks = [];
			vm.videosOfModule = [];
			vm.shouldShowVideoForm = false;
			vm.isVideoUploadButtonDisabled = true;


			vm.videolist = Array.apply(null, {length: 10}).map(Number.call, Number);
			vm.hrlist = Array.apply(null, {length: 24}).map(Number.call, Number);
			vm.minlist = Array.apply(null, {length: 60}).map(Number.call, Number);
			vm.seclist = Array.apply(null, {length: 60}).map(Number.call, Number);

	    	$scope.tinymceOptions = {
			    plugins : 'advlist autolink link image lists charmap print preview',
			    menubar : false,
			    statusbar: false
			  };
			$('#shortdesc').froalaEditor(EditorConfig);

			vm.$onInit = function() {
	        	$scope.$watch(function(){return vm.parent.courselistdata;},function(v){
	        		vm.courselist = v;
	        	});
	        	updateVideoEntities();
	        	findAllVideoEntities();
	        	admin.getAllSubject()
	        		.then(function(d){
	        			vm.subjects = d.data;
	        		});
	        	admin.getAllAuthor()
	        		.then(function(d){
	        			vm.authors = d.data;
	        		});
	        	admin.getAllPublication()
	        		.then(function(d){
	        			vm.publications = d.data;
	        		});
	        	admin.getAllBook()
	        		.then(function(d){
	        			vm.books = d.data;
	        		});
	        	admin.getAllExam()
	        		.then(function(d){
	        			vm.exams = d.data;
	        		});
	        	admin.getAllTopic()
	        		.then(function(d){
	        			vm.topics = d.data;
	        		});
	        	admin.getAllTag()
		    		.then(function(d){
		    			vm.tags = d.data;
		    		});

		    	//Observer files
				$scope.$watch('vm.videoFile',function(newValue,oldValue){
					if(newValue){
						vm.videoFileName = newValue.name;	
						vm.isVideoUploadInProgress = false;
						vm.isVideoUploaded = false;
					}else{
						vm.videoFileName = "";	
						vm.isVideoUploadInProgress = false;
						vm.isVideoUploaded = false;
					}
					
				})
				//Observer files
				$scope.$watch('vm.videoMEDFile',function(newValue,oldValue){
					if(newValue){
						vm.videoMEDFileName = newValue.name;
						vm.isVideoMEDUploadInProgress = false;
						vm.isVideoMEDUploaded = false;
					}else{
						vm.videoMEDFileName = "";
						vm.isVideoMEDUploadInProgress = false;
						vm.isVideoMEDUploaded = false;
					}
				})
				//Observer files
				$scope.$watch('vm.videoLOWFile',function(newValue,oldValue){
					if(newValue){
						vm.videoLOWFileName = newValue.name;
						vm.isVideoLOWUploadInProgress = false;
						vm.isVideoLOWUploaded = false;
					}else{
						vm.videoLOWFileName = "";
						vm.isVideoLOWUploadInProgress = false;
						vm.isVideoLOWUploaded = false;
					}
				})

				//Observer files
				$scope.$watch('vm.subtitleFile',function(newValue,oldValue){
					if(newValue){
						vm.subtitleFileName = newValue.name;	
						vm.isSubtitleUploadInProgress = false;
						vm.isSubtitleUploaded = false;
					}
					
				})
		    };


			vm.getNumber = getNumber;

	    	vm.updateVideoEntity = updateVideoEntity;
	    	vm.removeVideoEntity = removeVideoEntity;
	    	vm.editVideoEntity = editVideoEntity;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.updateVideoEntities = updateVideoEntities;

	    	vm.removeRefLink = removeRefLink;
	    	vm.uploadVideoFile = uploadVideoFile;
	    	vm.uploadVideoMEDFile = uploadVideoMEDFile;
	    	vm.uploadVideoLOWFile = uploadVideoLOWFile;
	    	vm.uploadSubtitleFile = uploadSubtitleFile;

	    	vm.checkVideoFilenameExistance = checkVideoFilenameExistance;

	    	function getNumber(num) {
			    return Array.apply(null,{length:num});   
			}

			function resetFormDetail(){
				// vm.selectedSubjects = [];
		    	vm.selectedAuthors = [];
		    	vm.selectedPublications = [];
		    	vm.selectedBooks = [];
		    	vm.selectedExams = [];
		    	vm.selectedTopics = [];
		    	vm.selectedRelatedTopics = [];
		    	vm.selectedRelatedVideos = [];
		    	// vm.selectedRelatedCourses = [];
		    	vm.selectedTags = [];
		    	vm.noOfRefLinks = 1;    	
				vm.form.video = {};
				vm.videoLOWFile = "";
				vm.videoFile = "";
				vm.videoMEDFile = "";
				vm.isVideoUploadButtonDisabled = true;
				vm.isVideoInfoInEditMode = false;
				vm.isVideoFilenameExist = false;
			}

			function checkVideoFilenameExistance(){
				if(vm.form.video.videoFilename && vm.form.video.videoFilename.length > 0){
					vm.isVideoUploadButtonDisabled = true;
					admin.isVideoFilenameExist({videoFilename : vm.form.video.videoFilename})
						.then(function(res){
							vm.isVideoFilenameExist = res.data.exist;
							if(vm.isVideoFilenameExist === false){
								vm.isVideoUploadButtonDisabled = false;
							}
						})
						.catch(function(err){
							$log.error(err);
						})
				}else{
					vm.isVideoFilenameExist = false;
					vm.isVideoUploadButtonDisabled = true;
				}
			}

			function findAllVideoEntities() {
				admin.findVideoEntityByQuery({})
					.then(function(data){
						if(data.data.length > 0){
							console.log(data.data.length)
							vm.relatedVideoEntities = data.data;
							vm.relatedVideoEntities.sort(compare);
							vm.relatedVideoNumber = vm.relatedVideoEntities[vm.relatedVideoEntities.length - 1]['videoNumber'] + 1; 
						}else{
							vm.relatedVideoNumber = 1;
							vm.relatedVideoEntities = undefined;
						}
					})
					.catch(function(err){
						console.error(err);
					})
			}

			function updateVideoEntities(){
				if(vm.selectedSubjects.length > 0){
					admin.getAllTopic({subjectId : vm.selectedSubjects[0]})
	        		.then(function(d){
	        			vm.topics = d.data;
	        		});	
				}
				if(vm.selectedTopics.length > 0){
					var topic = $filter('filter')(vm.topics,{topicId:vm.selectedTopics[0]})[0];
					vm.form = vm.form || {};
					vm.form.video = vm.form.video || {};
					vm.form.video.videoName = topic.topicName;
				}else{
					vm.form = vm.form || {};
					vm.form.video = vm.form.video || {};
					vm.form.video.videoName = "";
				}

				var query = {
					subjects	: vm.selectedSubjects,
			    	authors	: vm.selectedAuthors,
			    	publications	: vm.selectedPublications,
			    	books	: vm.selectedBooks,
			    	exams	: vm.selectedExams,
			    	topics	: vm.selectedTopics,
			    	tags	: vm.selectedTags
				}
				admin.findVideoEntityByQuery(query)
					.then(function(data){
						if(data.data.length > 0){
							vm.videoEntities = data.data;
							vm.videoEntities.sort(compare);
							vm.videoNumber = vm.videoEntities[vm.videoEntities.length - 1]['videoNumber'] + 1; 
						}else{
							vm.videoNumber = 1;
							vm.videoEntities = undefined;
						}
					})
					.catch(function(err){
						console.error(err);
					})
			}
			

			function updateVideoEntity(){
				//Add Video Id
				vm.form.video.videoId = (vm.selectedTopics[0] || '') + "-" + vm.videoNumber;
				// if(!vm.form.video.videoDuration){
				// 	vm.form.video.videoDuration = {};
				// }
				// if(!vm.form.video.videoDuration.hr){
				// 	vm.form.video.videoDuration.hr = 0;
				// }
				// if(!vm.form.video.videoDuration.min){
				// 	vm.form.video.videoDuration.min = 0;
				// }
				// if(!vm.form.video.videoDuration.sec){
				// 	vm.form.video.videoDuration.sec = 0;
				// }
				
				var data = {
					videoId : vm.form.video.videoId,
					videoNumber : vm.videoNumber,
					videoName : vm.form.video.videoName,
					videoDuration : vm.form.video.videoDuration,
					videoDescription : videoDescription(),
					thumbUrl : vm.form.video.thumbUrl,
					videoUrl : vm.form.video.videoUrl,
					videoFilename : vm.form.video.videoFilename,
					subtitleUrl : vm.form.video.subtitleUrl,
					videoSource : vm.form.video.videoSource,
					videoRefLinks : vm.form.video.refLinks,
					subjects : vm.selectedSubjects,
					authors : vm.selectedAuthors,
					publications : vm.selectedPublications,
					books : vm.selectedBooks,
					exams : vm.selectedExams,
					topics : vm.selectedTopics,
					relatedTopics : vm.selectedRelatedTopics,
					// relatedCourses : vm.selectedRelatedCourses,
					tags : vm.selectedTags,
					relatedVideos : vm.selectedRelatedVideos,
					isHIGHDefVideoUploaded : vm.form.video.isHIGHDefVideoUploaded,
					isMEDDefVideoUploaded : vm.form.video.isMEDDefVideoUploaded,
					isLOWDefVideoUploaded : vm.form.video.isLOWDefVideoUploaded,
					isSubtitleUploaded : vm.form.video.isSubtitleUploaded,
					thumbnailPath : vm.form.video.thumbnailPath
				}
				// console.log(data);
				pageLoader.show();
				admin.updateVideoEntity(data)
					.then(function(){
						vm.resetFormDetail();
						vm.submitted = true;
						vm.form.error = false;

						updateVideoEntities();

					})
					.catch(function(){
						vm.submitted = true;
						vm.form.error = false;
					})
					.finally(function(){
						vm.form = {};
						vm.form.video = {};
						pageLoader.hide()
					});
			}


			function editVideoEntity(videoId){
				var video = $filter('filter')(vm.videoEntities, {videoId: videoId})[0];
				// console.log(video);
				vm.form.video = video;
				vm.form.videoId = videoId;

				vm.videoFileName = (vm.form.video.isHIGHDefVideoUploaded) ? vm.form.video.videoFilename : "Select Video";
				vm.videoMEDFileName = (vm.form.video.isMEDDefVideoUploaded) ? vm.form.video.videoFilename + "-480" : "Select Video";
				vm.videoLOWFileName = (vm.form.video.isLOWDefVideoUploaded) ? vm.form.video.videoFilename + "-360" : "Select Video";
				vm.subtitleFileName = (vm.form.video.isSubtitleUploaded) ? vm.form.video.subtitleUrl : "Select Subtitle";

				vm.thumbnailPath = video.thumbnailPath;
				vm.videoNumber = video.videoNumber;
				
				videoDescription(video.videoDescription);

				vm.selectedSubjects = video.subjects;
		    	vm.selectedAuthors = video.authors;
		    	vm.selectedPublications = video.publications;
		    	vm.selectedBooks = video.books;
		    	vm.selectedExams = video.exams;
		    	vm.selectedTopics = video.topics;
		    	vm.selectedRelatedTopics = video.relatedTopics;
		    	// vm.selectedRelatedCourses = video.relatedCourses;
		    	vm.selectedTags = video.tags;
		    	vm.selectedRelatedVideos = video.relatedVideos;

	            vm.shouldShowVideoForm = true;
	            if(vm.form.video.videoFilename.length > 0){
	            	vm.isVideoUploadButtonDisabled = false;
	            }else{
	            	vm.isVideoUploadButtonDisabled = true;
	            }
	            
	            vm.isVideoInfoInEditMode = true;
	            vm.isVideoFilenameExist = false;

			}
			function removeVideoEntity(videoId){
				var result = confirm("Want to delete?");
				if (!result) {
				    //Declined
				   	return;
				}
				pageLoader.show();
				admin.removeVideoEntity({videoId:videoId})
					.then(function(d){
						updateVideoEntities();
						console.log("Removed Video")
					})
					.catch(function(){
						console.error(err);
					})
					.finally(function(){pageLoader.hide();});
			}

			function removeRefLink(index){
				if(vm.noOfRefLinks > 1){
					vm.form.video.refLinks.splice(index,1);
					vm.noOfRefLinks--;
				}
			}

			function compare(a,b){
				return a.videoNumber - b.videoNumber;
			}

			//set or get
			function videoDescription(desc){
				if(desc){
					$('#shortdesc').froalaEditor('html.set',desc);
					return;
				}
				return $('#shortdesc').froalaEditor('html.get',true);
			}

			function uploadVideoFile(){
				var file = {
		    		type : 'video',
		    		file : vm.videoFile,
		    		fileName : vm.form.video.videoFilename,
		    		generateThumbs : true,
		    		target : vm.form.video.videoSource
		    	}
				// if(vm.form.video.videoSource === "harddrive"){
					vm.isVideoUploadInProgress = true;
			    	uploadFileAPI.uploadVideoEntity(file)
			    		.then(function(d){
							var data = d.data;
							console.log(data);
							vm.form = vm.form || {};
							vm.form.video = vm.form.video || {};
							vm.form.video.videoUrl = data.Key;
							vm.form.video.isHIGHDefVideoUploaded = true;
							vm.form.video.thumbnailPath = data.thumbnailPath; 
							var durArr = data.duration.split(":");
							if(durArr.length == 2){
								vm.form.video.videoDuration = vm.form.video.videoDuration || {};
								vm.form.video.videoDuration["min"] = parseInt(durArr[0] || "0");
								vm.form.video.videoDuration["sec"] = parseInt(durArr[1] || "0");
							}else{
								vm.form.video.videoDuration = vm.form.video.videoDuration || {};
								vm.form.video.videoDuration["hr"] = parseInt(durArr[0] || "0");
								vm.form.video.videoDuration["min"] = parseInt(durArr[1] || "0");
								vm.form.video.videoDuration["sec"] = parseInt(durArr[2] || "0");
							}
							console.log(vm.form.video.videoDuration)
							vm.isVideoUploadInProgress = false;
							vm.isVideoUploaded = true;
							console.log(vm.form)
						},function(err){
							console.error(err);
						},function(evt){
							var width = parseInt(100.0 * evt.loaded / evt.total);
							vm.uploadProgress = width;
						})
				// }else if(vm.form.video.videoSource === 's3'){
			 //    	vm.isVideoUploadInProgress = true;
			 //    	awsService.uploadFileToS3(file)
			 //    		.then(function(d){
				// 			var data = d.data;
				// 			vm.form = vm.form || {};
				// 			vm.form.video = vm.form.video || {};
				// 			vm.form.video.videoUrl = data.Key;
				// 			vm.form.video.isHIGHDefVideoUploaded = true;
				// 			vm.isVideoUploadInProgress = false;
				// 			vm.isVideoUploaded = true;
				// 		},function(err){
				// 			console.error(err);
				// 		},function(evt){
				// 			var width = parseInt(100.0 * evt.loaded / evt.total);
				// 			vm.uploadProgress = width;
				// 		})
				// }
		    }

		    function uploadVideoMEDFile(){
		    	var file = {
		    		type : 'video',
		    		file : vm.videoMEDFile,
		    		fileName : vm.form.video.videoFilename + "-480",
		    		target : vm.form.video.videoSource
		    	}
		    	// if(vm.form.video.videoSource === "harddrive"){
		    		vm.isVideoMEDUploadInProgress = true;
		    		uploadFileAPI.uploadVideoEntity(file)
			    		.then(function(d){
							var data = d.data;
							vm.form = vm.form || {};
							vm.isVideoMEDUploadInProgress = false;
							vm.isVideoMEDUploaded = true;
							vm.form.video.isMEDDefVideoUploaded = true;
						},function(err){
							console.error(err);
						},function(evt){
							var width = parseInt(100.0 * evt.loaded / evt.total);
							vm.MEDuploadProgress = width;
						})
		    	// }else if(vm.form.video.videoSource === "s3"){
		    // 		vm.isVideoMEDUploadInProgress = true;
		    // 		awsService.uploadFileToS3(file)
			   //  		.then(function(d){
						// 	var data = d.data;
						// 	vm.form = vm.form || {};
						// 	vm.isVideoMEDUploadInProgress = false;
						// 	vm.isVideoMEDUploaded = true;
						// 	vm.form.video.isMEDDefVideoUploaded = true;
						// },function(err){
						// 	console.error(err);
						// },function(evt){
						// 	var width = parseInt(100.0 * evt.loaded / evt.total);
						// 	vm.MEDuploadProgress = width;
						// })
		    	// }
		    	
		    }
		    function uploadVideoLOWFile(){
		    	var file = {
		    		type : 'video',
		    		file : vm.videoLOWFile,
		    		fileName : vm.form.video.videoFilename + "-360",
		    		target : vm.form.video.videoSource
		    	}
		    	// if(vm.form.video.videoSource === "harddrive"){
		    		vm.isVideoLOWUploadInProgress = true;
			    	$log.debug(file);
			    	uploadFileAPI.uploadVideoEntity(file)
			    		.then(function(d){
							var data = d.data;
							vm.form = vm.form || {};
							vm.isVideoLOWUploadInProgress = false;
							vm.isVideoLOWUploaded = true;
							vm.form.video.isLOWDefVideoUploaded = true;
						},function(err){
							console.error(err);
						},function(evt){
							var width = parseInt(100.0 * evt.loaded / evt.total);
							vm.LOWuploadProgress = width;
						})
		    // 	}else if(vm.form.video.videoSource === "s3"){
		    // 		vm.isVideoLOWUploadInProgress = true;
			   //  	$log.debug(file);
			   //  	awsService.uploadFileToS3(file)
			   //  		.then(function(d){
						// 	var data = d.data;
						// 	vm.form = vm.form || {};
						// 	vm.isVideoLOWUploadInProgress = false;
						// 	vm.isVideoLOWUploaded = true;
						// 	vm.form.video.isLOWDefVideoUploaded = true;
						// },function(err){
						// 	console.error(err);
						// },function(evt){
						// 	var width = parseInt(100.0 * evt.loaded / evt.total);
						// 	vm.LOWuploadProgress = width;
						// })
		    	// }
		    	
		    }
		    function uploadSubtitleFile(){
		    	var file = {
		    		type : 'video',
		    		file : vm.subtitleFile,
		    		target : vm.form.video.videoSource
		    	}

		    	// if(vm.form.video.videoSource === "harddrive"){
		    		vm.isSubtitleUploadInProgress = true;
			    	uploadFileAPI.uploadVideoEntity(file)
			    		.then(function(d){
							var data = d.data;
							vm.form = vm.form || {};
							vm.form.video = vm.form.video || {};
							vm.form.video.subtitleUrl = data.Key;
							vm.form.video.isSubtitleUploaded = true;
							vm.isSubtitleUploadInProgress = false;
							vm.isSubtitleUploaded = true;
						},function(err){
							console.error(err);
						},function(evt){
							var width = parseInt(100.0 * evt.loaded / evt.total);
							vm.subtitleUploadProgress = width;
						})
		    // 	}else if(vm.form.video.videoSource === "s3"){
		    // 		vm.isSubtitleUploadInProgress = true;
			   //  	awsService.uploadFileToS3(file)
			   //  		.then(function(d){
						// 	var data = d.data;
						// 	vm.form = vm.form || {};
						// 	vm.form.video = vm.form.video || {};
						// 	vm.form.video.subtitleUrl = data.Key;
						// 	vm.isSubtitleUploadInProgress = false;
						// 	vm.form.video.isSubtitleUploaded = true;
						// 	vm.isSubtitleUploaded = true;
						// },function(err){
						// 	console.error(err);
						// },function(evt){
						// 	var width = parseInt(100.0 * evt.loaded / evt.total);
						// 	vm.subtitleUploadProgress = width;
						// })
		    // 	}
		    	
		    }
	    }
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 348:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {/*

		jQuery Tags Input Plugin 1.3.3

		Copyright (c) 2011 XOXCO, Inc

		Documentation for this plugin lives here:
		http://xoxco.com/clickable/jquery-tags-input

		Licensed under the MIT license:
		http://www.opensource.org/licenses/mit-license.php

		ben@xoxco.com

	*/

	(function($) {

		var delimiter = new Array();
		var tags_callbacks = new Array();
		$.fn.doAutosize = function(o){
		    var minWidth = $(this).data('minwidth'),
		        maxWidth = $(this).data('maxwidth'),
		        val = '',
		        input = $(this),
		        testSubject = $('#'+$(this).data('tester_id'));

		    if (val === (val = input.val())) {return;}

		    // Enter new content into testSubject
		    var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		    testSubject.html(escaped);
		    // Calculate new width + whether to change
		    var testerWidth = testSubject.width(),
		        newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
		        currentWidth = input.width(),
		        isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
		                             || (newWidth > minWidth && newWidth < maxWidth);

		    // Animate width
		    if (isValidWidthChange) {
		        input.width(newWidth);
		    }


	  };
	  $.fn.resetAutosize = function(options){
	    // alert(JSON.stringify(options));
	    var minWidth =  $(this).data('minwidth') || options.minInputWidth || $(this).width(),
	        maxWidth = $(this).data('maxwidth') || options.maxInputWidth || ($(this).closest('.tagsinput').width() - options.inputPadding),
	        val = '',
	        input = $(this),
	        testSubject = $('<tester/>').css({
	            position: 'absolute',
	            top: -9999,
	            left: -9999,
	            width: 'auto',
	            fontSize: input.css('fontSize'),
	            fontFamily: input.css('fontFamily'),
	            fontWeight: input.css('fontWeight'),
	            letterSpacing: input.css('letterSpacing'),
	            whiteSpace: 'nowrap'
	        }),
	        testerId = $(this).attr('id')+'_autosize_tester';
	    if(! $('#'+testerId).length > 0){
	      testSubject.attr('id', testerId);
	      testSubject.appendTo('body');
	    }

	    input.data('minwidth', minWidth);
	    input.data('maxwidth', maxWidth);
	    input.data('tester_id', testerId);
	    input.css('width', minWidth);
	  };

		$.fn.addTag = function(value,options) {
				options = jQuery.extend({focus:false,callback:true},options);
				this.each(function() {
					var id = $(this).attr('id');

					var tagslist = $(this).val().split(delimiter[id]);
					if (tagslist[0] == '') {
						tagslist = new Array();
					}

					value = jQuery.trim(value);

					if (options.unique) {
						var skipTag = $(this).tagExist(value);
						if(skipTag == true) {
						    //Marks fake input as not_valid to let styling it
	    				    $('#'+id+'_tag').addClass('not_valid');
	    				}
					} else {
						var skipTag = false;
					}

					if (value !='' && skipTag != true) {
	                    $('<span>').addClass('tag').append(
	                        $('<span>').text(value).append('&nbsp;&nbsp;'),
	                        $('<a>', {
	                            href  : '#',
	                            title : 'Removing tag',
	                            text  : 'x'
	                        }).click(function () {
	                            return $('#' + id).removeTag(escape(value));
	                        })
	                    ).insertBefore('#' + id + '_addTag');

						tagslist.push(value);

						$('#'+id+'_tag').val('');
						if (options.focus) {
							$('#'+id+'_tag').focus();
						} else {
							$('#'+id+'_tag').blur();
						}

						$.fn.tagsInput.updateTagsField(this,tagslist);

						if (options.callback && tags_callbacks[id] && tags_callbacks[id]['onAddTag']) {
							var f = tags_callbacks[id]['onAddTag'];
							f.call(this, value);
						}
						if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
						{
							var i = tagslist.length;
							var f = tags_callbacks[id]['onChange'];
							f.call(this, $(this), tagslist[i-1]);
						}
					}

				});

				return false;
			};

		$.fn.removeTag = function(value) {
				value = unescape(value);
				this.each(function() {
					var id = $(this).attr('id');

					var old = $(this).val().split(delimiter[id]);

					$('#'+id+'_tagsinput .tag').remove();
					str = '';
					for (i=0; i< old.length; i++) {
						if (old[i]!=value) {
							str = str + delimiter[id] +old[i];
						}
					}

					$.fn.tagsInput.importTags(this,str);

					if (tags_callbacks[id] && tags_callbacks[id]['onRemoveTag']) {
						var f = tags_callbacks[id]['onRemoveTag'];
						f.call(this, value);
					}
				});

				return false;
			};

		$.fn.tagExist = function(val) {
			var id = $(this).attr('id');
			var tagslist = $(this).val().split(delimiter[id]);
			return (jQuery.inArray(val, tagslist) >= 0); //true when tag exists, false when not
		};

	   // clear all existing tags and import new ones from a string
	   $.fn.importTags = function(str) {
	      var id = $(this).attr('id');
	      $('#'+id+'_tagsinput .tag').remove();
	      $.fn.tagsInput.importTags(this,str);
	   }

		$.fn.tagsInput = function(options) {
	    var settings = jQuery.extend({
	      interactive:true,
	      defaultText:'add a tag',
	      minChars:0,
	      width:'300px',
	      height:'100px',
	      autocomplete: {selectFirst: false },
	      hide:true,
	      delimiter: ',',
	      unique:true,
	      removeWithBackspace:true,
	      placeholderColor:'#666666',
	      autosize: true,
	      comfortZone: 20,
	      inputPadding: 6*2
	    },options);

	    	var uniqueIdCounter = 0;

			this.each(function() {
	         // If we have already initialized the field, do not do it again
	         if (typeof $(this).attr('data-tagsinput-init') !== 'undefined') {
	            return;
	         }

	         // Mark the field as having been initialized
	         $(this).attr('data-tagsinput-init', true);

				if (settings.hide) {
					$(this).hide();
				}
				var id = $(this).attr('id');
				if (!id || delimiter[$(this).attr('id')]) {
					id = $(this).attr('id', 'tags' + new Date().getTime() + (uniqueIdCounter++)).attr('id');
				}

				var data = jQuery.extend({
					pid:id,
					real_input: '#'+id,
					holder: '#'+id+'_tagsinput',
					input_wrapper: '#'+id+'_addTag',
					fake_input: '#'+id+'_tag'
				},settings);

				delimiter[id] = data.delimiter;

				if (settings.onAddTag || settings.onRemoveTag || settings.onChange) {
					tags_callbacks[id] = new Array();
					tags_callbacks[id]['onAddTag'] = settings.onAddTag;
					tags_callbacks[id]['onRemoveTag'] = settings.onRemoveTag;
					tags_callbacks[id]['onChange'] = settings.onChange;
				}

				var markup = '<div id="'+id+'_tagsinput" class="tagsinput"><div id="'+id+'_addTag">';

				if (settings.interactive) {
					markup = markup + '<input id="'+id+'_tag" value="" data-default="'+settings.defaultText+'" />';
				}

				markup = markup + '</div><div class="tags_clear"></div></div>';

				$(markup).insertAfter(this);

				$(data.holder).css('width',settings.width);
				$(data.holder).css('min-height',settings.height);
				$(data.holder).css('height',settings.height);

				if ($(data.real_input).val()!='') {
					$.fn.tagsInput.importTags($(data.real_input),$(data.real_input).val());
				}
				if (settings.interactive) {
					$(data.fake_input).val($(data.fake_input).attr('data-default'));
					$(data.fake_input).css('color',settings.placeholderColor);
			        $(data.fake_input).resetAutosize(settings);

					$(data.holder).bind('click',data,function(event) {
						$(event.data.fake_input).focus();
					});

					$(data.fake_input).bind('focus',data,function(event) {
						if ($(event.data.fake_input).val()==$(event.data.fake_input).attr('data-default')) {
							$(event.data.fake_input).val('');
						}
						$(event.data.fake_input).css('color','#000000');
					});

					if (settings.autocomplete_url != undefined) {
						autocomplete_options = {source: settings.autocomplete_url};
						for (attrname in settings.autocomplete) {
							autocomplete_options[attrname] = settings.autocomplete[attrname];
						}

						if (jQuery.Autocompleter !== undefined) {
							$(data.fake_input).autocomplete(settings.autocomplete_url, settings.autocomplete);
							$(data.fake_input).bind('result',data,function(event,data,formatted) {
								if (data) {
									$('#'+id).addTag(data[0] + "",{focus:true,unique:(settings.unique)});
								}
						  	});
						} else if (jQuery.ui.autocomplete !== undefined) {
							$(data.fake_input).autocomplete(autocomplete_options);
							$(data.fake_input).bind('autocompleteselect',data,function(event,ui) {
								$(event.data.real_input).addTag(ui.item.value,{focus:true,unique:(settings.unique)});
								return false;
							});
						}


					} else {
							// if a user tabs out of the field, create a new tag
							// this is only available if autocomplete is not used.
							$(data.fake_input).bind('blur',data,function(event) {
								var d = $(this).attr('data-default');
								if ($(event.data.fake_input).val()!='' && $(event.data.fake_input).val()!=d) {
									if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
										$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
								} else {
									$(event.data.fake_input).val($(event.data.fake_input).attr('data-default'));
									$(event.data.fake_input).css('color',settings.placeholderColor);
								}
								return false;
							});

					}
					// if user types a default delimiter like comma,semicolon and then create a new tag
					$(data.fake_input).bind('keypress',data,function(event) {
						if (_checkDelimiter(event)) {
						    event.preventDefault();
							if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
								$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
						  	$(event.data.fake_input).resetAutosize(settings);
							return false;
						} else if (event.data.autosize) {
				            $(event.data.fake_input).doAutosize(settings);

	          			}
					});
					//Delete last tag on backspace
					data.removeWithBackspace && $(data.fake_input).bind('keydown', function(event)
					{
						if(event.keyCode == 8 && $(this).val() == '')
						{
							 event.preventDefault();
							 var last_tag = $(this).closest('.tagsinput').find('.tag:last').text();
							 var id = $(this).attr('id').replace(/_tag$/, '');
							 last_tag = last_tag.replace(/[\s]+x$/, '');
							 $('#' + id).removeTag(escape(last_tag));
							 $(this).trigger('focus');
						}
					});
					$(data.fake_input).blur();

					//Removes the not_valid class when user changes the value of the fake input
					if(data.unique) {
					    $(data.fake_input).keydown(function(event){
					        if(event.keyCode == 8 || String.fromCharCode(event.which).match(/\w+|[áéíóúÁÉÍÓÚñÑ,/]+/)) {
					            $(this).removeClass('not_valid');
					        }
					    });
					}
				} // if settings.interactive
			});

			return this;

		};

		$.fn.tagsInput.updateTagsField = function(obj,tagslist) {
			var id = $(obj).attr('id');
			$(obj).val(tagslist.join(delimiter[id]));
		};

		$.fn.tagsInput.importTags = function(obj,val) {
			$(obj).val('');
			var id = $(obj).attr('id');
			var tags = val.split(delimiter[id]);
			for (i=0; i<tags.length; i++) {
				$(obj).addTag(tags[i],{focus:false,callback:false});
			}
			if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
			{
				var f = tags_callbacks[id]['onChange'];
				f.call(obj, obj, tags[i]);
			}
		};

	   /**
	     * check delimiter Array
	     * @param event
	     * @returns {boolean}
	     * @private
	     */
	   var _checkDelimiter = function(event){
	      var found = false;
	      if (event.which == 13) {
	         return true;
	      }

	      if (typeof event.data.delimiter === 'string') {
	         if (event.which == event.data.delimiter.charCodeAt(0)) {
	            found = true;
	         }
	      } else {
	         $.each(event.data.delimiter, function(index, delimiter) {
	            if (event.which == delimiter.charCodeAt(0)) {
	               found = true;
	            }
	         });
	      }

	      return found;
	   }
	})(jQuery);

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 349:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(350);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js!./jquery.tagsinput.min.css", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js!./jquery.tagsinput.min.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 350:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "div.tagsinput{border:1px solid #CCC;background:#FFF;padding:5px;width:300px;height:100px;overflow-y:auto}div.tagsinput span.tag{border:1px solid #a5d24a;-moz-border-radius:2px;-webkit-border-radius:2px;display:block;float:left;padding:5px;text-decoration:none;background:#cde69c;color:#638421;margin-right:5px;margin-bottom:5px;font-family:helvetica;font-size:13px}div.tagsinput span.tag a{font-weight:700;color:#82ad2b;text-decoration:none;font-size:11px}div.tagsinput input{width:80px;margin:0 5px 5px 0;font-family:helvetica;font-size:13px;border:1px solid transparent;padding:5px;background:0 0;color:#000;outline:0}div.tagsinput div{display:block;float:left}.tags_clear{clear:both;width:100%;height:0}.not_valid{background:#FBD8DB!important;color:#90111A!important}", ""]);

	// exports


/***/ },

/***/ 351:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(43);
	angular
	.module('service')
	.service('uploadFileAPI', UploadFileAPI);

	UploadFileAPI.$inject = ['$http','Upload','BASE_API'];

	function UploadFileAPI($http,Upload,BASE_API){

		this.uploadVideoEntity = function(data){
			return Upload.upload({
	            url: BASE_API + '/videoentity/uploadVideoEntity',
	            data: data,
	            method: 'POST'
	        });
		}

		this.uploadUserMaterial = function(data){
			return Upload.upload({
	            url: BASE_API + '/moduleItems/uploadUserMaterial',
	            data: data,
	            method: 'POST'
	        });
		}
	}

/***/ },

/***/ 352:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" name=\"form\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Video</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Subject</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedSubjects\" theme=\"bootstrap\" on-select=\"vm.updateVideoEntities($item, $model)\" on-remove=\"vm.updateVideoEntities($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.subjectId as item in (vm.subjects | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"author\">Author</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedAuthors\" theme=\"bootstrap\" on-select=\"vm.updateVideoEntities($item, $model)\" on-remove=\"vm.updateVideoEntities($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Author...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.authorId as item in (vm.authors | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"publication\">Publication</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedPublications\" theme=\"bootstrap\" on-select=\"vm.updateVideoEntities($item, $model)\" on-remove=\"vm.updateVideoEntities($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Publication...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.publicationName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.publicationId as item in (vm.publications | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.publicationName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"book\">Book</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBooks\" theme=\"bootstrap\" on-select=\"vm.updateVideoEntities($item, $model)\" on-remove=\"vm.updateVideoEntities($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Book...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.bookName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.bookId as item in (vm.books | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.bookName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"exam\">Exam</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedExams\" theme=\"bootstrap\" on-select=\"vm.updateVideoEntities($item, $model)\" on-remove=\"vm.updateVideoEntities($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Exam...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.examName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.examId as item in (vm.exams | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.examName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Topic</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTopics\" theme=\"bootstrap\" on-select=\"vm.updateVideoEntities($item, $model)\" on-remove=\"vm.updateVideoEntities($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Related Topic</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedRelatedTopics\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Related Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- <div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Related Course</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedRelatedCourses\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Related Course...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.courseId as item in (vm.courselist | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.courseName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> -->\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tag\">Tag</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTags\" theme=\"bootstrap\" on-select=\"vm.updateVideoEntities($item, $model)\" on-remove=\"vm.updateVideoEntities($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Tag...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.tagName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.tagId as item in (vm.tags | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.tagName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"videoname\">Name</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"videoname\" class=\"form-control\" placeholder=\"Video Name\" ng-model=\"vm.form.video.videoName\" name=\"videoname\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"shortdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea  id=\"shortdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.video.videoDesc\" name=\"desc\" rows=\"6\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"videoduration\">Source</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"\" class=\"form-control\" ng-model=\"vm.form.video.videoSource\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Source</option>\n\t\t\t\t\t\t\t\t\t<option value=\"s3\">S3</option>\n\t\t\t\t\t\t\t\t\t<option value=\"harddrive\">Hard Drive</option>\n\t\t\t\t\t\t\t\t\t<option value=\"cloud\">Cloud</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"videofilename\">Video Filename</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"videofilename\" class=\"form-control\" placeholder=\"Video Filename\" ng-model=\"vm.form.video.videoFilename\" name=\"videofilename\" ng-disabled=\"vm.isVideoInfoInEditMode && vm.form.video.videoFilename.length > 0\" ng-blur=\"vm.checkVideoFilenameExistance()\"/>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div style=\"color: red\" class=\"col-sm-10 col-sm-offset-2\" ng-if=\"vm.isVideoFilenameExist\">\n\t\t\t\t\t\t\t\tVideo Filename exist.\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"video-link\">Video High DEF</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<div class=\"btn btn-primary video-upload-btn\"\n\t\t\t\t\t\t\t\t\tng-class=\"{'disabled':vm.isVideoUploadButtonDisabled}\"\n\t\t\t\t\t\t\t\t\tstyle=\"margin-right: 10px;\" \n\t\t\t\t\t\t\t\t\tngf-select\n\t\t\t\t\t\t\t\t\tngf-pattern=\"'video/mp4'\" ngf-accept=\"'video/mp4'\"\n\t\t\t\t\t\t\t\t\tng-model=\"vm.videoFile\"\n\t\t\t\t\t\t\t\t\tngf-multiple=\"false\"\n\t\t\t\t\t\t\t\t\t>{{vm.videoFileName || 'Select Video'}}</div>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.videoFile && !vm.isVideoUploadInProgress && !vm.isVideoUploaded\">\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\" ng-click=\"vm.uploadVideoFile()\">Upload</div>\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-danger\" \n\t\t\t\t\t\t\t\t\t\tng-click=\"vm.videoFileName = undefined\">Reset</div>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isVideoUploadInProgress\">\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.uploadProgress < 100\">Video Upload in progress {{vm.uploadProgress}}%</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.uploadProgress >= 100\">Processing...</span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isVideoUploaded\">\n\t\t\t\t\t\t\t\t\tVideo is Uploaded\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"video-link\">Video MED DEF</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<div class=\"btn btn-primary video-upload-btn\"\n\t\t\t\t\t\t\t\t\tstyle=\"margin-right: 10px;\" \n\t\t\t\t\t\t\t\t\tng-class=\"{'disabled':vm.isVideoUploadButtonDisabled}\"\n\t\t\t\t\t\t\t\t\tngf-select\n\t\t\t\t\t\t\t\t\tngf-pattern=\"'video/mp4'\" ngf-accept=\"'video/mp4'\"\n\t\t\t\t\t\t\t\t\tng-model=\"vm.videoMEDFile\"\n\t\t\t\t\t\t\t\t\tngf-multiple=\"false\"\n\t\t\t\t\t\t\t\t\t>{{vm.videoMEDFileName || 'Select Video'}}</div>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.videoMEDFile && !vm.isVideoMEDUploadInProgress && !vm.isVideoMEDUploaded\">\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\" ng-click=\"vm.uploadVideoMEDFile()\">Upload</div>\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-danger\" \n\t\t\t\t\t\t\t\t\t\tng-click=\"vm.videoMEDFileName = undefined\">Reset</div>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isVideoMEDUploadInProgress\">\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.MEDuploadProgress < 100\">Video Upload in progress {{vm.MEDuploadProgress}}%</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.MEDuploadProgress >= 100\">Processing...</span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isVideoMEDUploaded\">\n\t\t\t\t\t\t\t\t\tVideo is Uploaded\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"video-link\">Video LOW DEF</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<div class=\"btn btn-primary video-upload-btn\"\n\t\t\t\t\t\t\t\t\tstyle=\"margin-right: 10px;\" \n\t\t\t\t\t\t\t\t\tng-class=\"{'disabled':vm.isVideoUploadButtonDisabled}\"\n\t\t\t\t\t\t\t\t\tngf-select\n\t\t\t\t\t\t\t\t\tngf-pattern=\"'video/mp4'\" ngf-accept=\"'video/mp4'\"\n\t\t\t\t\t\t\t\t\tng-model=\"vm.videoLOWFile\"\n\t\t\t\t\t\t\t\t\tngf-multiple=\"false\"\n\t\t\t\t\t\t\t\t\t>{{vm.videoLOWFileName || 'Select Video'}}</div>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.videoLOWFile && !vm.isVideoLOWUploadInProgress && !vm.isVideoLOWUploaded\">\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\" ng-click=\"vm.uploadVideoLOWFile()\">Upload</div>\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-danger\" \n\t\t\t\t\t\t\t\t\t\tng-click=\"vm.videoLOWFileName = undefined\">Reset</div>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isVideoLOWUploadInProgress\">\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.LOWuploadProgress < 100\">Video Upload in progress {{vm.LOWuploadProgress}}%</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.LOWuploadProgress >= 100\">Processing...</span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isVideoLOWUploaded\">\n\t\t\t\t\t\t\t\t\tVideo is Uploaded\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"duration col-sm-2 control-label\" for=\"videoduration\">Duration(Read Only)</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"\" class=\"duration\" ng-model=\"vm.form.video.videoDuration.hr\"\n\t\t\t\t\t\t\t\t\tng-options='(hr) for hr in vm.hrlist' disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Hour(s)</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t<select id=\"\" class=\"duration\" ng-model=\"vm.form.video.videoDuration.min\"\n\t\t\t\t\t\t\t\t\tng-options='(min) for min in vm.minlist' disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Minute(s)</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t<select id=\"\" class=\"duration\" ng-model=\"vm.form.video.videoDuration.sec\"\n\t\t\t\t\t\t\t\t\tng-options='(sec) for sec in vm.seclist' disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Seconds</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"videourl\">URL</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"videourl\" class=\"form-control\" placeholder=\"Video URL\" ng-model=\"vm.form.video.videoUrl\" name=\"url\" readonly=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.form.video.videoUrl\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"video-link\">Subtitle</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\"\n\t\t\t\t\t\t\t\t\tstyle=\"margin-right: 10px;\" \n\t\t\t\t\t\t\t\t\tngf-select\n\t\t\t\t\t\t\t\t\tng-model=\"vm.subtitleFile\"\n\t\t\t\t\t\t\t\t\tngf-multiple=\"false\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{vm.subtitleFileName || 'Select Subtitle'}}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.subtitleFile && !vm.isSubtitleUploadInProgress && !vm.isSubtitleUploaded\">\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-primary\" ng-click=\"vm.uploadSubtitleFile()\">Upload</div>\n\t\t\t\t\t\t\t\t\t<div class=\"btn btn-danger\" \n\t\t\t\t\t\t\t\t\t\tng-click=\"vm.subtitleFileName = undefined\">Reset</div>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isSubtitleUploadInProgress\">\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.subtitleUploadProgress < 100\">Subtitle Upload in progress {{vm.subtitleUploadProgress}}%</span>\n\t\t\t\t\t\t\t\t\t<span ng-if=\"vm.subtitleUploadProgress >= 100\">Processing...</span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.isSubtitleUploaded\">\n\t\t\t\t\t\t\t\t\tSubtitle is Uploaded\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"videourl\">Thumbnail URL</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"videourl\" class=\"form-control\" placeholder=\"Thumbnail URL\" ng-model=\"vm.form.video.thumbUrl\" name=\"url\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Related Videos</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedRelatedVideos\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Related Videos...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.videoName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.videoId as item in (vm.relatedVideoEntities | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.videoNumber}} - {{item.videoName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div style=\"position: relative;padding-right: 35px;\">\n\t\t\t\t\t\t\t<div class=\"form-group\" style=\"position: relative;\" ng-repeat=\"i in vm.getNumber(vm.noOfRefLinks) track by $index\">\n\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"reflink_{{$index + 1}}\">Reflink #{{$index + 1}}</label>\n\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"reflink_{{$index + 1}}\" class=\"form-control\" placeholder=\"Reference Link {{$index + 1}}\" ng-model=\"vm.form.video.refLinks[$index]\"/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"ref-link-btn\" style=\"right: 20px;\" ng-click=\"vm.removeRefLink($index)\"><i class=\"fa fa-times\"></i></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"ref-link-btn\" ng-click=\"vm.noOfRefLinks = vm.noOfRefLinks + 1\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Video</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"vm.selectedTopics.length <= 0 || !vm.form.video.videoName || vm.isVideoUploadInProgress\" ng-click=\"vm.updateVideoEntity()\">\n\t\t\t\t\t\t\t\t\t{{vm.isVideoUploadInProgress ? 'Video upload is in progress...' : 'Save'}}\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.videoEntities\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.videoEntities\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"video in vm.videoEntities\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div  class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeVideoEntity(video.videoId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editVideoEntity(video.videoId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{video.videoName}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in video\">\n\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t.video-upload-btn.disabled{\n\t\tpointer-events: none;\n\t}\n</style>";

/***/ }

});