webpackJsonp([34],{

/***/ 404:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(405);
	angular.module(window.moduleName)
	    .component('sessionDetailsPage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(407),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['admin','$filter','$scope','$state','EditorConfig'];
	function Controller(admin,$filter,$scope,$state,EditorConfig){

		var vm = this;
		vm.form = {};
		vm.selectedBatch = [];

		vm.onBatchChange = onBatchChange;
		vm.addItemToList = addItemToList;
		vm.removeItemFromList = removeItemFromList;
		vm.addSessionItem = addSessionItem;
		vm.resetForm = resetForm;
		vm.onSessionChange = onSessionChange;

		vm.$onInit = function(){
			getAllTiming();
		}

		function getAllTiming(){
			admin.getAllBatchTiming()
				.then(function(res){
					vm.allTimings = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function onBatchChange(){
			vm.form.bat_id = vm.selectedBatch[0];
			var bat = $filter('filter')(vm.allTimings,{bat_id : vm.form.bat_id})[0];
			vm.crs_id = bat.crs_id;
			console.log(vm.crs_id);
			getAllSessions();
		}

		function onSessionChange(){
			getSessionDetailsForBatchAndSession();
		}

		function getAllSessions(){

			admin.getSessionsByCourseId({crs_id : vm.crs_id})
				.then(function(res){
					vm.allSessions = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function getSessionDetailsForBatchAndSession(){
			admin.getSessionDetailsForBatchAndSession({bat_id : vm.form.bat_id , sess_id : vm.form.sess_id})
				.then(function(res){
					var obj = res.data;
					if(!obj.bat_id || !obj.sess_id){
						return;
					}
					var dt = new Date(obj.sess_dt);
					vm.sess_dt = "" + (dt.getDate());
					vm.sess_mn = "" + (dt.getMonth() + 1);
					vm.sess_yr = "" + (dt.getFullYear());

					delete obj.sess_dt;

					vm.form = obj;
					vm.form.sess_id = parseInt(obj.sess_id);
				})
				.catch(function(err){
					console.error(err);
				})
		}


		function addItemToList(){
			vm.form = vm.form || {};
			vm.form.sess_items = vm.form.sess_items || {};
			if(vm.title && vm.link){
				vm.form.sess_items[vm.itemType + "_" + vm.title] = vm.link;
				vm.title = "";
				vm.link = "";
				vm.itemType = "";
			}
		}

		function removeItemFromList(key){
			delete vm.form.sess_items[key];
		}


		function addSessionItem(){
			var dt = vm.sess_mn + "-" + vm.sess_dt + "-" + vm.sess_yr;
			vm.form.sess_dt = new Date(dt);
			admin.addSessionItems(vm.form)
				.then(function(res){
					getSessionDetailsForBatchAndSession();
					resetForm();
				})
				.catch(function(err){
					console.error(err);
				})
		}
		
		function resetForm(){
			vm.form = {};
			vm.sess_mn = "";
			vm.sess_dt = "";
			vm.sess_yr = "";
		}

	}

/***/ },

/***/ 405:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(406);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_sessionDetails.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_sessionDetails.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 406:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, ".question-title {\n  font-family: aller;\n  font-size: 17px;\n  font-weight: bold; }\n\n.question-ans {\n  font-family: aller;\n  font-size: 15px; }\n\n.gray-back {\n  background: #e5e5e5;\n  padding: 0px 10px; }\n\n.remove-btn {\n  position: absolute;\n  right: 5px;\n  top: 5px;\n  font-size: 15px;\n  cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 407:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<fieldset>\n\t\t\t\t\t<legend>Add Session Details</legend>\n\t\t\t\t\t<form class=\"form form-horizontal\" name=\"form\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Batch</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<!-- <select ng-model=\"vm.form.bat_id\" id=\"course\" ng-change=\"vm.onBatchChange()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"bat.bat_id as (bat.crs_id + '' + (bat.cls_start_dt|date:'dd-MMM')) for bat in vm.allTimings \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Batch</b> </option>\n\t\t\t\t\t\t\t\t</select> -->\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBatch\" theme=\"bootstrap\" on-select=\"vm.onBatchChange()\" on-remove=\"vm.onBatchChange()\" limit=\"1\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Batch...\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{$item.crs_id + '--->' + ($item.cls_start_dt|date:'dd-MMM')}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.bat_id as item in (vm.allTimings | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{item.crs_id + '--->' + (item.cls_start_dt|date:'dd-MMM')}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"sess name\">Session</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.sess_id\" ng-change=\"vm.onSessionChange()\" id=\"sess\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options=\"sess.sessionNumber as sess.sessionName for sess in vm.allSessions \">\n\t\t\t\t\t\t\t\t\t<option value=\"\"> <b>Select Session</b> </option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"\">Session Date</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-3\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.sess_dt\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Date</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=31) && _) track by $index\" value=\"{{$index + 1}}\">{{$index + 1}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-3\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.sess_mn\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Month</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=12) && _) track by $index\" >{{$index + 1}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.sess_yr\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Year</option>\n\t\t\t\t\t\t\t\t\t<option ng-repeat=\"_ in ((_ = []) && (_.length=12) && _) track by $index\">{{$index + 2016}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"title\">Session Item</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.itemType\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Item Type</option>\n\t\t\t\t\t\t\t\t\t<option value=\"v\">Video</option>\n\t\t\t\t\t\t\t\t\t<option value=\"d\">Download</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin-top: 10px;\" class=\"form-control\" placeholder=\"Enter Title\" ng-model=\"vm.title\"/>\n\t\t\t\t\t\t\t\t<input type=\"text\" style=\"margin-top: 10px;\" class=\"form-control\" placeholder=\"Enter Link\" ng-model=\"vm.link\"/>\n\t\t\t\t\t\t\t\t<div style=\"margin: 10px 0px;\" ng-click=\"vm.addItemToList()\" class=\"btn btn-primary pull-right\">Add</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-repeat=\"(key,val) in vm.form.sess_items track by $index\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"curriculam\">Item {{$index+1}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 gray-back\">\n\t\t\t\t\t\t\t\t<div class=\"question-title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"question-ans\" ng-bind-html=\"val\"></div>\n\t\t\t\t\t\t\t\t<div class=\"remove-btn\" ng-click=\"vm.removeItemFromList(key)\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\" style=\"color: red\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\"  ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.addSessionItem()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</form>\n\t\t\t\t</fieldset>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<!-- <div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allSessions\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allSessions\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"session in vm.allSessions | orderBy:'sessionWeight'\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteSession(session.sessionNumber)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editSession(session.sessionNumber)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{session.sessionNumber}} - {{session.sessionName}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in session\">\n\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div> -->\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});