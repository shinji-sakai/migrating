webpackJsonp([21],{

/***/ 372:
/***/ function(module, exports, __webpack_require__) {

		angular.module(window.moduleName)
	        .component('subtopicPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(373),
	            controller: AdminSubTopicController,
	            controllerAs: 'vm'
	        });

		AdminSubTopicController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminSubTopicController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;

	        vm.changeTopic = changeTopic;
	    	vm.updateSubTopic = updateSubTopic;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removeSubTopic = removeSubTopic;
	    	vm.editSubTopic = editSubTopic;
	       vm.no_of_item_already_fetch=0;
	    	vm.$onInit = function() {

	            admin.getAllTopic()
	                .then(function(data){
	                    vm.topics = data.data;
	                })
	                .catch(function(err){
	                    console.log(err);
	                })

		    };

	        function changeTopic(){
	            //updateSubTopicsList(vm.form.topic);
	        }

				vm.fetchSubTopics = function(){
	            vm.subTopics = [];
				vm.isTopicsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'subtopic'
				})
				.then(function(res){
					$log.debug(res)
					vm.subTopics = res.data;
					vm.no_of_item_already_fetch = 5;
	                vm.last_update_dt = vm.subTopics[vm.subTopics.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTopicsLoading = false;
				})
			}

			vm.fetchMoreSubTopics = function(){
				vm.isTopicsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : vm.no_of_item_already_fetch,
	                collection : 'subTopic',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.subTopics, {subTopicId: v.subTopicId})[0];
							if(!item){
								vm.topics.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
	                    vm.last_update_dt = vm.subTopics[vm.subTopics.length - 1].update_dt
						
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isTopicsLoading = false;
				})
			}
	   

		    function updateSubTopicsList(id){
		    	pageLoader.show();
		    	admin.getAllSubTopic({topicId : id})
	        		.then(function(data){
	        			if(data.data.length > 0)
	        				vm.subTopics = data.data;
	                    else
	                        vm.subTopics = undefined;
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }

	    	function updateSubTopic(){
	            var topic = $filter('filter')(vm.topics,{topicId:vm.form.topic})[0];
	    		var data = {
	                topicId : topic.topicId,
	                topicName : topic.topicName,
	    			subTopicId : vm.form.subTopicId,
	    			subTopicName : vm.form.subTopicName,
	    			subTopicDesc : vm.form.subTopicDesc 
	    		}
				
	            if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }
	    		pageLoader.show();
	    		admin.updateSubTopic(data)
	    			.then(function(res){
						vm.fetchSubTopics();
	    				vm.submitted = true;
	    				vm.form.error = false;
	    				vm.form.subTopicName = "";
	    				vm.form.subTopicDesc = "";
	                    vm.form.subTopicId = "";
						vm.formIsInEditMode=false;
	    				//updateSubTopicsList(vm.form.topic);
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}

	    	function resetFormDetail(){
	    		vm.submitted = false;
	            var topic = vm.form.topic;
	    		vm.form = {};
	            vm.form.topic = topic;
	    	}

	    	function removeSubTopic(id){
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	    		admin.removeSubTopic({subTopicId:id})
	    			.then(function(d){
	    				console.log(d);
						var item = $filter('filter')(vm.subTopics || [], {subTopicId: id})[0];								
						 if (vm.subTopics.indexOf(item) > -1) {
	                        var pos = vm.subTopics.indexOf(item);
	                       vm.subTopics.splice(pos, 1);
	                    }
	    				//updateSubTopicsList(vm.form.topic);
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}

	    	function editSubTopic(id){
	    		var obj = $filter('filter')(vm.subTopics,{subTopicId:id})[0];
	            console.log(obj);
	    		vm.form = {};
	            vm.form.topic = obj.topicId;
	    		vm.form.subTopicId = obj.subTopicId;
	    		vm.form.subTopicName = obj.subTopicName;
	    		vm.form.subTopicDesc = obj.subTopicDesc;
				vm.formIsInEditMode=true;
	    	}
	    }

/***/ },

/***/ 373:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateSubTopic()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add SubTopic</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Topic<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select id=\"topic\"  ng-model=\"vm.form.topic\" ng-change=\"vm.changeTopic()\" class=\"form-control\"\n\t\t\t\t\t\t\t\t\tng-options='topic.topicId as topic.topicName for topic in vm.topics'>\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Topic</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subTopicId\">SubTopic Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"subTopicId\" class=\"form-control\" placeholder=\"Id\" ng-model=\"vm.form.subTopicId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subTopicname\">SubTopic Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"subTopicname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.subTopicName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subTopicdesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"subTopicdesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.subTopicDesc\" required=\"\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New SubTopic</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"!vm.form.topic\" type=\"submit\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchSubTopics()\" ng-if=\"!vm.subTopics || vm.subTopics.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTopicsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTopicsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show subTopics</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.subTopics\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.subTopics\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"subTopic in vm.subTopics\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeSubTopic(subTopic.subTopicId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editSubTopic(subTopic.subTopicId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{subTopic.subTopicName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in subTopic\">\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreSubTopics()\" ng-if=\"(vm.subTopics==[] || vm.subTopics.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isTopicsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isTopicsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});