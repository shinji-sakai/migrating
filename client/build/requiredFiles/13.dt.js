webpackJsonp([13],{

/***/ 355:
/***/ function(module, exports, __webpack_require__) {

	
		angular.module(window.moduleName)
	        .component('authorPage', {
	        	require: {
	        		parent : '^adminPage'
	        	},
	            template: __webpack_require__(356),
	            controller: AdminAuthorController,
	            controllerAs: 'vm'
	        });

		AdminAuthorController.$inject=['$log','admin','pageLoader','$filter','$scope'];

		function AdminAuthorController($log,admin,pageLoader,$filter,$scope) {
	    	var vm = this;
	        vm.authors = [];
	        vm.no_of_item_already_fetch=0;
	        vm.checkValidityOfAuthorID = checkValidityOfAuthorID;
	    	vm.updateAuthor = updateAuthor;
	    	vm.resetFormDetail = resetFormDetail;
	    	vm.removeAuthor = removeAuthor;
	    	vm.editAuthor = editAuthor;

	    	vm.$onInit = function() {
	            //update author list on page load
	        	//updateAuthorsList();
		    };

			vm.fetchAuthors = function(){
	            vm.authors = [];
				vm.isAuthorsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : 0,
	                collection : 'author'
				})
				.then(function(res){
					$log.debug(res)
					vm.authors = res.data;
					vm.no_of_item_already_fetch = 5;
	                vm.last_update_dt = vm.authors[vm.authors.length - 1].update_dt
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isAuthorsLoading = false;
				})
			}
	   
			vm.fetchMoreAuthors = function(){
				vm.isAuthorsLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					// no_of_item_to_skip : vm.no_of_item_already_fetch,
	                collection : 'author',
	                update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.authors, {authorId: v.authorId})[0];
							if(!item){
								vm.authors.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
	                    vm.last_update_dt = vm.authors[vm.authors.length - 1].update_dt
						
					}else{
						vm.noMoreData = true;
					}
					// vm.no_of_item_already_fetch = vm.no_of_item_already_fetch + 5;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isAuthorsLoading = false;
				})
			}
	        function checkValidityOfAuthorID() {
				vm.form.authorId = vm.form.authorId || "";
	            vm.form.authorId = vm.form.authorId.replace(/\s/g , "-");
				admin.isIdExist({
					collection :'author',
					key:"authorId",
					id_value:vm.form.authorId			
				}).then(function(data)
				{				
	              console.log(data.data);
				  if(data.data.idExist)
				  {
					    vm.authorIdExists = true;
				  }
				  else
				  {
					   vm.authorIdExists = false;
				  }
				}).catch(function(err){
	             console.log(err);
				})
				
				
	           
	        }

	        //update author list
		    function updateAuthorsList(){
	            //show page laoder
		    	pageLoader.show();
	            //get all author from db
		    	admin.getAllAuthor()
	        		.then(function(data){
	        			if(data.data.length > 0)
	        				vm.authors = data.data;
	        		})
	        		.catch(function(err){
	        			console.log(err);
	        		})
	        		.finally(function(){
	        			pageLoader.hide();
	        		})
		    }

	        //update author to db
	    	function updateAuthor(){

	    		var data = {
	    			authorId : vm.form.authorId,
	    			authorName : vm.form.authorName,
	    			authorDesc : vm.form.authorDesc 
	    		}

	            if(!vm.formIsInEditMode){
	                data.create_dt = new Date();
	                data.update_dt = new Date();
	            }else{
	                data.update_dt = new Date();
	            }

	            //show page laoder
	    		pageLoader.show();
	            //update authpr to db
	    		admin.updateAuthor(data)
	    			.then(function(res){

	                    vm.fetchAuthors();
	    				vm.submitted = true;
	    				vm.form.error = false;
	                    //resest form value to defaults
	    				vm.form.authorName = "";
	    				vm.form.authorDesc = "";
	                    vm.form.authorId = "";
	                    vm.formIsInEditMode = false;
	    				//updateAuthorsList();
	    			})
	    			.catch(function(){
	    				vm.submitted = true;
	    				vm.form.error = true;	
	    			})
	    			.finally(function(){
	    				pageLoader.hide();
	    			})
	    	}

	    	function resetFormDetail(){
	    		vm.submitted = false;
	            vm.authorIdExists = false;
	            vm.formIsInEditMode = false;
	    		vm.form = {};
	    	}

	        //remove author
	    	function removeAuthor(id){
	            //ask for confirmation
	            var result = confirm("Want to delete?");
	            if (!result) {
	                //Declined
	                return;
	            }
	    		pageLoader.show();
	            //remove author api to remove from db
	    		admin.removeAuthor({authorId:id})
	    			.then(function(d){
	    				var item = $filter('filter')(vm.authors || [], {authorId: id})[0];								
						 if (vm.authors.indexOf(item) > -1) {
	                        var pos = vm.authors.indexOf(item);
	                       vm.authors.splice(pos, 1);
	                    }					  
	    			//	updateAuthorsList();
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			})
	    			.finally(function(){pageLoader.hide();})
	    	}

	        //this function will be called when user click on edit icon
	    	function editAuthor(id){
	            //find author using author id
	    		var author = $filter('filter')(vm.authors,{authorId:id})[0];
	    		vm.form = {};
	            //set form value to edited author
	    		vm.form.authorId = author.authorId;
	    		vm.form.authorName = author.authorName;
	    		vm.form.authorDesc = author.authorDesc;
	            vm.formIsInEditMode = true;
	    	}
	    }

/***/ },

/***/ 356:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\" ng-submit=\"vm.updateAuthor()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Author</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"authorId\">Author Id<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"authorId\" class=\"form-control\" placeholder=\"Id\" ng-change=\"vm.checkValidityOfAuthorID()\" ng-disabled=\"vm.formIsInEditMode\" ng-model=\"vm.form.authorId\" required=\"\" />\n\t\t\t\t\t\t\t</div>\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"alert alert-danger\" ng-if=\"vm.authorIdExists\">\n\t\t\t\t\t\t\t\tAuthor Id exists\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"authorname\">Author Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" id=\"authorname\" class=\"form-control\" placeholder=\"Name\" ng-model=\"vm.form.authorName\" required=\"\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"authordesc\">Description</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<textarea id=\"authordesc\" class=\"form-control\" placeholder=\"Description\" ng-model=\"vm.form.authorDesc\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-5 col-sm-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-info\" ng-click=\"vm.resetFormDetail()\" style=\"margin-top: 25px;\">New Author</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-5 \">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-disabled=\"vm.authorIdExists\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchAuthors()\" ng-if=\"!vm.authors || vm.authors.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isAuthorsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isAuthorsLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Author</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.authors\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.authors\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"author in vm.authors\">\n\t\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.removeAuthor(author.authorId)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editAuthor(author.authorId)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{author.authorName}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</uib-accordion-heading>\n\t\t\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in author\">\n\n\t\t\t\t\t\t\t<div ng-if=\"key != '_id'\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreAuthors()\" ng-if=\"(vm.authors==[] || vm.authors.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isAuthorsLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isAuthorsLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});