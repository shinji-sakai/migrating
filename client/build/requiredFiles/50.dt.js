webpackJsonp([50],{

/***/ 120:
/***/ function(module, exports) {

	  angular
	    .module('service')
	    .service('admin', admin);

	admin.$inject = ['$http','BASE_API'];

	function admin($http,BASE_API) {


	    this.isIdExist = function(data) {
	        return $http.post(BASE_API + '/admin/general/isIdExist',data);
	    }


	    this.fetchItemUsingLimit = function(data) {
	        return $http.post(BASE_API + '/admin/general/fetchItemUsingLimit',data);
	    }

	    /* Course Master API */

	    this.coursemaster = function() {
	        //  /admin/coursemaster
	        return $http.post(BASE_API + '/courseMaster/getAll').then(function(res) {
	            return res.data;
	        });
	    }

	    

	    this.addCourseMaster = function(data) {
	        //      /admin/addCourseMaster
	        return $http.post(BASE_API + '/courseMaster/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseMaster = function(data) {
	        //      /admin/deleteCourseMaster
	        return $http.post(BASE_API + '/courseMaster/delete', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.fetchCourseMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/courseMaster/fetchCourseMasterUsingLimit', data);
	    }

	    this.getRelatedCoursesByCourseId = function(data) {
	        return $http.post(BASE_API + '/courseMaster/getRelatedCoursesByCourseId',data);
	    }

	    /* Course API */
	    this.courselistfull = function(data) {
	        return $http.post(BASE_API + '/course/getAll',data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourse = function(data) {
	        // /admin/addCourse
	        return $http.post(BASE_API + '/course/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.removeCourse = function(data) {
	        //  /admin/removeCourse
	        return $http.post(BASE_API + '/course/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Videos API */
	    this.addVideos = function(data) {
	        //  /admin/addVideos
	        return $http.post(BASE_API + '/courseVideos/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getModuleVideos = function(data) {
	        //  /admin/getModuleVideos
	        return $http.post(BASE_API + '/courseVideos/getModuleVideos', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getCourseVideos = function(data) {
	        //      /admin/getCourseVideos
	        return $http.post(BASE_API + '/courseVideos/getCourseVideos', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Course Modules API */
	    this.getCourseModules = function(data) {
	        //      /admin/getCourseModules
	        return $http.post(BASE_API + '/courseModule/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.addCourseModule = function(data) {
	        //  /admin/addCourseModule
	        return $http.post(BASE_API + '/courseModule/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteCourseModule = function(data) {
	        //  /admin/deleteCourseModule
	        return $http.post(BASE_API + '/courseModule/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Video Slides API */
	    this.addVideoSlide = function(data) {
	        //  /admin/addVideoSlide
	        return $http.post(BASE_API + '/videoSlide/add', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getVideoSlides = function(data) {
	        //  /admin/getVideoSlides
	        return $http.post(BASE_API + '/videoSlide/getAll', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.deleteVideoSlide = function(data) {
	        //  /admin/deleteVideoSlide
	        return $http.post(BASE_API + '/videoSlide/delete', data).then(function(res) {
	            return res.data;
	        });
	    }

	    /* Authorized Items API */

	    this.getAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/getItems',data);
	    }

	    this.updateAuthorizedItems = function(data){
	        return $http.post(BASE_API + '/ai/updateItems',data);
	    }

	    this.addAuthorizedItem = function(data){
	        return $http.post(BASE_API + '/ai/addItem',data);
	    }

	    this.getAuthorizedVideoItemFullInfo = function(data){
	        return $http.post(BASE_API + '/ai/getVideoItemFullInfo',data);
	    }
	    this.getVideoPermission = function(data){
	        return $http.post(BASE_API + '/ai/getVideoPermission',data);
	    }


	    /* practice Question API */

	    this.savePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestion',data);
	    }
	    this.savePreviewPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/savePreviewQuestion',data);
	    }
	    this.saveQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveQuestionStats',data);
	    }
	    this.saveTestQuestionStats = function(data){
	        return $http.post(BASE_API + '/practice/saveTestQuestionStats',data);
	    }
	    this.saveTestReviews  = function(data){
	        return $http.post(BASE_API + '/practice/saveTestReviews',data);
	    }
	    this.removePracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/removeQuestion',data);
	    }
	    this.getAllPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getAllQuestions',data);
	    }
	    this.findPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsByQuery',data);
	    }
	    this.findPracticeQuestionsIdByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsIdByQuery',data);
	    }
	    this.findPracticeQuestionsTextByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findQuestionsTextByQuery',data);
	    }
	    this.findPreviewPracticeQuestionsByQuery = function(data){
	        return $http.post(BASE_API + '/practice/findPreviewQuestionsByQuery',data);
	    }
	    this.getPracticeQuestionsById = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsById',data);
	    }

	    this.getPracticeQuestionsByLimit = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeQuestionsByLimit',data);
	    }

	    this.updateMultiplePracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/updateMultiplePracticeQuestions',data);
	    }
	    this.getAllAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersQuestionPerfStats',data);
	    }
	    this.getAllMonthAvgQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllMonthAvgQuestionPerfStats',data);
	    }
	    this.getAllUsersMonthQuestionPerfStats = function(data){
	        return $http.post(BASE_API + '/practice/getAllUsersMonthQuestionPerfStats',data);
	    }
	    this.updateVerifyPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/updateVerifyPracticeQuestion',data);
	    }
	    this.getAllVerifyStatusOfPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getAllVerifyStatusOfPracticeQuestion',data);
	    }
	    this.getLatestVerifyStatusOfGivenPracticeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getLatestVerifyStatusOfGivenPracticeQuestions',data);
	    }
	    this.getUserMonthQuestionAttemptStats = function(data){
	        return $http.post(BASE_API + '/practice/getUserMonthQuestionAttemptStats',data);
	    }
	    this.startUserExam = function(data){
	        return $http.post(BASE_API + '/practice/startUserExam',data);
	    }
	    this.finishUserExam = function(data){
	        return $http.post(BASE_API + '/practice/finishUserExam',data);
	    }
	    this.checkForPreviousExam = function(data){
	        return $http.post(BASE_API + '/practice/checkForPreviousExam',data);
	    }
	    this.getUserExamQuestion = function(data){
	        return $http.post(BASE_API + '/practice/getUserExamQuestion',data);
	    }
	    this.saveUserExamQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserExamQuestionData',data);
	    }
	    this.startUserPractice = function(data){
	        return $http.post(BASE_API + '/practice/startUserPractice',data);
	    }
	    this.saveUserPracticeQuestionData = function(data){
	        return $http.post(BASE_API + '/practice/saveUserPracticeQuestionData',data);
	    }
	    this.fetchNextUserPracticeQuestion = function(data){
	        return $http.post(BASE_API + '/practice/fetchNextUserPracticeQuestion',data);
	    }
	    this.getPracticeNodeQuestions = function(data){
	        return $http.post(BASE_API + '/practice/getPracticeNodeQuestions',data);
	    }






	    /* Highlight APi */

	    this.saveHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/save',data);
	    }
	    this.getQuestionHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getQuestionHighlights',data);
	    }
	    this.getUserHighlights = function(data){
	        return $http.post(BASE_API + '/highlight/getUserHighlights',data);
	    }
	    this.deleteHighlightedText = function(data){
	        return $http.post(BASE_API + '/highlight/delete',data);
	    }

	    this.updateHighlightNote = function(data){
	        return $http.post(BASE_API + '/highlight/updateHighlightNote',data);
	    }

	    /*
	    Author API
	     */
	    this.getAllAuthor  = function(){
	        return $http.post(BASE_API + '/author/getAll');
	    }
	    this.updateAuthor  = function(data){
	        return $http.post(BASE_API + '/author/update',data);
	    }
	    this.removeAuthor  = function(data){
	        return $http.post(BASE_API + '/author/remove',data);
	    }
	    this.fetchAuthorUsingLimit=function(data){
	        return $http.post(BASE_API+'/author/fetchAuthorUsingLimit',data);
	    }
	    this.AuthorsIdIsExits=function(data){
	        return $http.post(BASE_API+'/author/AuthorsIdIsExits',data);
	    }

	    /*
	    Publication API
	     */
	    this.getAllPublication  = function(){
	        return $http.post(BASE_API + '/publication/getAll');
	    }
	    this.updatePublication  = function(data){
	        return $http.post(BASE_API + '/publication/update',data);
	    }
	    this.removePublication  = function(data){
	        return $http.post(BASE_API + '/publication/remove',data);
	    }
	   this.fetchPublicationsUsingLimit=function(data){
	       return $http.post(BASE_API+'/publication/fetchPublicationsUsingLimit',data);
	   }
	    /*
	    Book API
	     */
	    this.getAllBook  = function(){
	        return $http.post(BASE_API + '/book/getAll');
	    }
	    this.updateBook  = function(data){
	        return $http.post(BASE_API + '/book/update',data);
	    }
	    this.removeBook  = function(data){
	        return $http.post(BASE_API + '/book/remove',data);
	    }
	    this.fetchBooksUsingLimit=function(data){
	        return $http.post(BASE_API+'/book/fetchBooksUsingLimit',data);
	    }

	    /*
	    JobOpenings API
	     */
	    this.getAllJobOpenings  = function(){
	        return $http.post(BASE_API + '/jobOpenings/getAllJobOpenings');
	    }
	    this.updateJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/updateJobOpening',data);
	    }
	    this.removeJobOpening  = function(data){
	        return $http.post(BASE_API + '/jobOpenings/removeJobOpening',data);
	    }

	    /*
	    Subject API
	     */
	    this.getAllSubject  = function(){
	        return $http.post(BASE_API + '/subject/getAll');
	    }
	    this.updateSubject  = function(data){
	        return $http.post(BASE_API + '/subject/update',data);
	    }
	    this.removeSubject  = function(data){
	        return $http.post(BASE_API + '/subject/remove',data);
	    }
	   this.fetchSubjectMasterUsingLimit = function(data){
	        return $http.post(BASE_API + '/subject/fetchSubjectMasterUsingLimit',data);
	    }
	    this.SubjectIdIsExits=function(data){
	        return $http.post(BASE_API+'/subject/SubjectIdIsExits',data);
	    }
	    /*
	    Exam API
	     */
	    this.getAllExam  = function(){
	        return $http.post(BASE_API + '/exam/getAll');
	    }
	    this.updateExam  = function(data){
	        return $http.post(BASE_API + '/exam/update',data);
	    }
	    this.removeExam  = function(data){
	        return $http.post(BASE_API + '/exam/remove',data);
	    }
	    this.fetchExamUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/exam/fetchExamUsingLimit',data);
	    }
	    /*
	    Topic API
	     */
	    this.getAllTopic  = function(data){
	        return $http.post(BASE_API + '/topic/getAll',data);
	    }
	    this.updateTopic  = function(data){
	        return $http.post(BASE_API + '/topic/update',data);
	    }
	    this.removeTopic  = function(data){
	        return $http.post(BASE_API + '/topic/remove',data);
	    }
	   this.fetchTopicsUsingLimit=function(data)
	   {
	       return $http.post(BASE_API+'/topic/fetchTopicsUsingLimit',data);
	   }
	    /*
	    Topic Group API
	     */
	    this.getAllTopicGroups  = function(data){
	        return $http.post(BASE_API + '/topicGroup/getAllTopicGroups',data);
	    }
	    this.updateTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/updateTopicGroup',data);
	    }
	    this.removeTopicGroup  = function(data){
	        return $http.post(BASE_API + '/topicGroup/removeTopicGroup',data);
	    }
	    this.fetchTopicGroupUsingLimit=function(data)
	    {
	        return $http.post(BASE_API+'/topicGroup/fetchTopicGroupUsingLimit',data);
	    }
	    /*
	    Tag API
	     */
	    this.getAllTag  = function(){
	        return $http.post(BASE_API + '/tag/getAll');
	    }
	    this.updateTag  = function(data){
	        return $http.post(BASE_API + '/tag/update',data);
	    }
	    this.removeTag  = function(data){
	        return $http.post(BASE_API + '/tag/remove',data);
	    }
	    this.fetchTagsUsingLimit=function(data){
	        return $http.post(BASE_API+'/tag/fetchTagsUsingLimit',data);
	    }
	    this.TagsIdIsExits=function(data)
	    {
	        return $http.post(BASE_API+'/tag/TagsIdIsExits',data);
	    }

	    /*
	    SubTopic API
	     */
	    this.getAllSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/getAll',data);
	    }
	    this.updateSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/update',data);
	    }
	    this.removeSubTopic  = function(data){
	        return $http.post(BASE_API + '/subtopic/remove',data);
	    }

	    /* videoentity api */
	    this.updateVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/update',data);
	    }
	    this.removeVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/remove',data);
	    }
	    this.getAllVideoEntity = function(data){
	        return $http.post(BASE_API + '/videoentity/getAll',data);
	    }
	    this.isVideoFilenameExist = function(data){
	        return $http.post(BASE_API + '/videoentity/isVideoFilenameExist',data);
	    }
	    this.findVideoEntityByQuery = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityByQuery',data);
	    }
	            //One Id
	    this.findVideoEntityById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntityById',data);
	    }
	            //Multiple Ids
	    this.findVideoEntitiesById = function(data){
	        return $http.post(BASE_API + '/videoentity/findVideoEntitiesById',data);
	    }
	    this.getRelatedTopicsVideoByVideoId = function(data){
	        return $http.post(BASE_API + '/videoentity/getRelatedTopicsVideoByVideoId',data);
	    }
	    this.getRelatedVideosByVideoId = function(data) {
	        return $http.post(BASE_API + '/videoentity/getRelatedVideosByVideoId',data);
	    }
	    this.updateVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/updateVideoLastViewTime',data);
	    }
	    this.getVideoLastViewTime = function(data) {
	        return $http.post(BASE_API + '/videoentity/getVideoLastViewTime',data);
	    }


	    /* moduleItems api */
	    this.updateModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/update',data);
	    }
	    this.getAllModuleItems = function(data){
	        return $http.post(BASE_API + '/moduleItems/getAll',data);
	    }
	    this.getCourseItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getCourseItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getFullCourseDetails = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getFullCourseDetails', data);
	    }
	    
	    this.getModuleItems = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getModuleItems', data).then(function(res) {
	            return res.data;
	        });
	    }
	    this.getUserBatchEnrollUploads = function(data) {
	        return $http.post(BASE_API + '/moduleItems/getUserBatchEnrollUploads',data);
	    }

	    /* createtest api */
	    this.createTest = function(data){
	        return $http.post(BASE_API + '/createtest/save',data);
	    }
	    this.getAllTest = function(data){
	        return $http.post(BASE_API + '/createtest/getAll',data);
	    }
	    this.getTestById = function(data){
	        return $http.post(BASE_API + '/createtest/getById',data);
	    }

	    /*
	        CourseBundle API
	     */
	    this.getAllCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/getAll',data);
	    }
	    this.updateCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/update',data);
	    }
	    this.removeCourseBundle  = function(data){
	        return $http.post(BASE_API + '/courseBundle/remove',data);
	    }



	    // Employee types api
	    this.addEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/add',data);
	    }
	    this.getAllEmployeeTypes  = function(data){
	        return $http.post(BASE_API + '/employee/types/getAll',data);
	    }
	    this.deleteEmployeeType  = function(data){
	        return $http.post(BASE_API + '/employee/types/delete',data);
	    }

	    // Employee Details api
	    this.addEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/add',data);
	    }
	    this.getAllEmployees  = function(data){
	        return $http.post(BASE_API + '/employee/getAll',data);
	    }
	    this.deleteEmployee  = function(data){
	        return $http.post(BASE_API + '/employee/delete',data);
	    }

	    // Employee Skills
	    this.addEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/add',data);
	    }
	    this.getAllEmployeeSkills  = function(data){
	        return $http.post(BASE_API + '/employee/skills/getAll',data);
	    }
	    this.deleteEmployeeSkill  = function(data){
	        return $http.post(BASE_API + '/employee/skills/delete',data);
	    }

	    // All links category
	    this.addAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/add',data);
	    }
	    this.getAllLinksCategories  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/getAll',data);
	    }
	    this.deleteAllLinksCategory  = function(data){
	        return $http.post(BASE_API + '/allLinksCategory/delete',data);
	    }


	    // Sidelinks article
	    this.addSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/update',data);
	    }
	    this.getAllSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getAll',data);
	    }
	    this.getSideLinkArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/get',data);
	    }
	    this.getMultipleSideLinkArticles = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/getMultipleArticles',data);
	    }
	    this.deleteSideLinksArticlePage  = function(data){
	        return $http.post(BASE_API + '/sideLinksArticle/remove',data);
	    }


	    // All Links
	    this.addLinkToAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/update',data);
	    }
	    this.getAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/getAll',data);
	    }
	    this.deleteLinkFromAllLinks  = function(data){
	        return $http.post(BASE_API + '/allLinks/remove',data);
	    }


	    // BoardCompetitiveCourse
	    this.updateBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/update',data);
	    }
	    this.getBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/get',data);
	    }
	    this.getBoardCompetitiveCoursesByCourseType  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/getCoursesByCourseType',data);
	    }
	    this.deleteBoardCompetitiveCourse  = function(data){
	        return $http.post(BASE_API + '/boardCompetitive/remove',data);
	    }


	    // Course Subgroup
	    this.addSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/add',data);
	    }
	    this.getAllSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/getAll',data);
	    }
	    this.deleteSubgroup  = function(data){
	        return $http.post(BASE_API + '/subgroup/delete',data);
	    }

	    // Save Batch Course
	    this.addBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/add',data);
	    }
	    this.getAllBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getAll',data);
	    }
	    this.deleteBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/delete',data);
	    }
	    this.getBatchCourse  = function(data){
	        return $http.post(BASE_API + '/batch_course/getBatchCourse',data);
	    }

	    // Save Batch Timing
	    this.addBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/add',data);
	    }
	    this.getAllBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getAll',data);
	    }
	    this.deleteBatchTiming  = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/delete',data);
	    }
	    this.getTimingByCourseId = function(data){
	        return $http.post(BASE_API + '/batch_course/timing/getTimingByCourseId',data);
	    }


	    // Save Batch Timing Dashboard
	    this.addBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/addBatchTimingDashboard',data);
	    }
	    this.getAllBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/getAllBatchTimingDashboard',data);
	    }
	    this.deleteBatchTimingDashboard  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/deleteBatchTimingDashboard',data);
	    }
	    this.setBatchTimingDashboardAsCompleted  = function(data){
	        return $http.post(BASE_API + '/batch_course/timingDashboard/setBatchTimingDashboardAsCompleted',data);
	    }

	    //

	    // Currency
	    this.addCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/add',data);
	    }
	    this.getAllCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/getAll',data);
	    }
	    this.deleteCurrency  = function(data){
	        return $http.post(BASE_API + '/batch_course/currency/delete',data);
	    }

	    // Getting STarted
	    this.addGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/add',data);
	    }
	    this.getAllGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getAll',data);
	    }
	    this.deleteGettingStarted  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/delete',data);
	    }
	    this.getGettingStartedByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/gettingStarted/getByCourseId',data);
	    }

	    // Sessions
	    this.addSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/add',data);
	    }
	    this.getAllSessions  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getAll',data);
	    }
	    this.deleteSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/delete',data);
	    }
	    this.getSessionsByCourseId  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessions/getByCourseId',data);
	    }

	    // Sessions Items
	    this.addSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/add',data);
	    }
	    this.getAllSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getAll',data);
	    }
	    this.deleteSessionItems  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/delete',data);
	    }
	    this.getSessionDetailsForBatchAndSession  = function(data){
	        return $http.post(BASE_API + '/batch_course/sessionItems/getSessionDetailsForBatchAndSession',data);
	    }


	    // Pricelist Course
	    this.savePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/add',data);
	    }
	    this.getAllPricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/getAll',data);
	    }
	    this.deletePricelist  = function(data){
	        return $http.post(BASE_API + '/pricelist/delete',data);
	    }
	    this.getDetailedPricelist = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelist',data);
	    }
	    this.getPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getPricelistByCourseId',data);
	    }
	    this.getDetailedPricelistByCourseId = function(data){
	        return $http.post(BASE_API + '/pricelist/getDetailedPricelistByCourseId',data);
	    }


	    // Bundle
	    this.saveBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/add',data);
	    }
	    this.getAllBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/getAll',data);
	    }
	    this.deleteBundle  = function(data){
	        return $http.post(BASE_API + '/bundle/delete',data);
	    }

	    // Email Templates
	    this.addEmailTemplates  = function(data){
	        return $http.post(BASE_API + '/emailTemplates/addEmailTemplates',data);
	    }

	    this.getAllEmailTemplates = function(data){
	        return $http.post(BASE_API + '/emailTemplates/getAllEmailTemplates',data);
	    }

	    this.deleteEmailTemplate = function(data){
	        return $http.post(BASE_API + '/emailTemplates/deleteEmailTemplate',data);
	    }

	    // UserEmail Group
	    this.addUserEmailGroup  = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/addUserEmailGroup',data);
	    }

	    this.getAllUserEmailGroups = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/getAllUserEmailGroups',data);
	    }

	    this.deleteUserEmailGroup = function(data){
	        return $http.post(BASE_API + '/userEmailGroup/deleteUserEmailGroup',data);
	    }

	    // Send Email Templates
	    this.addSendEmailTemplate  = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/addSendEmailTemplate',data);
	    }

	    this.getAllSendEmailTemplates = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/getAllSendEmailTemplates',data);
	    }

	    this.deleteSendEmailTemplate = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/deleteSendEmailTemplate',data);
	    }

	    this.sendEmailTemplatesToUsers = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendEmailTemplatesToUsers',data);
	    }

	    this.sendNotificationOfEmail = function(data){
	        return $http.post(BASE_API + '/sendEmailTemplate/sendNotificationOfEmail',data);
	    }

	    // Send Notification
	    this.addSendNotification  = function(data){
	        return $http.post(BASE_API + '/sendNotification/addSendNotification',data);
	    }

	    this.getAllSendNotifications = function(data){
	        return $http.post(BASE_API + '/sendNotification/getAllSendNotifications',data);
	    }

	    this.deleteSendNotification = function(data){
	        return $http.post(BASE_API + '/sendNotification/deleteSendNotification',data);
	    }

	    this.getSendNotificationByUserId = function(data){
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationByUserId',data);
	    }

	    this.markSendNotificationAsRead = function(data){
	        return $http.post(BASE_API + '/sendNotification/markSendNotificationAsRead',data);
	    }

	    this.getSendNotificationCount = function(data) {
	        return $http.post(BASE_API + '/sendNotification/getSendNotificationCount',data);
	    }


	    // Roles
	    this.addRole  = function(data){
	        return $http.post(BASE_API + '/adminRoles/addRole',data);
	    }

	    this.getAllRoles = function(data){
	        return $http.post(BASE_API + '/adminRoles/getAllRoles',data);
	    }

	    this.deleteRole = function(data){
	        return $http.post(BASE_API + '/adminRoles/deleteRole',data);
	    }


	    // forms
	    this.addForm  = function(data){
	        return $http.post(BASE_API + '/adminForms/addForm',data);
	    }

	    this.getAllForms = function(data){
	        return $http.post(BASE_API + '/adminForms/getAllForms',data);
	    }

	    this.deleteForm = function(data){
	        return $http.post(BASE_API + '/adminForms/deleteForm',data);
	    }

	    // forms  help
	    this.addFormHelp  = function(data){
	        return $http.post(BASE_API + '/formsHelp/updateFormHelp',data);
	    }

	    this.getAllFormsHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getAllFormsHelp',data);
	    }

	    this.getFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/getFormHelp',data);
	    }

	    this.deleteFormHelp = function(data){
	        return $http.post(BASE_API + '/formsHelp/deleteFormHelp',data);
	    }

	    // user roles
	    this.addUserRole  = function(data){
	        return $http.post(BASE_API + '/userRoles/addUserRole',data);
	    }

	    this.getAllUserRoles = function(data){
	        return $http.post(BASE_API + '/userRoles/getAllUserRoles',data);
	    }

	    this.deleteUserRole = function(data){
	        return $http.post(BASE_API + '/userRoles/deleteUserRole',data);
	    }

	    this.getUserAdminRoleFormAccessList = function(data) {
	        return $http.post(BASE_API + '/userRoles/getUserAdminRoleFormAccessList',data);
	    }

	    // user batch enroll
	    this.getUserEnrollAllBatch = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/getUserEnrollAllBatch',data);
	    }
	    this.addUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/addUserBatchEnroll',data);
	    }
	    this.removeUserBatchEnroll = function(data) {
	        return $http.post(BASE_API + '/userBatchEnroll/removeUserBatchEnroll',data);
	    }



	    this.updateBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/updateBookTopic',data);
	    }
	    this.removeBookTopic = function(data) {
	        return $http.post(BASE_API + '/booktopic/removeBookTopic',data);
	    }
	    this.getAllBookTopics = function(data) {
	        return $http.post(BASE_API + '/booktopic/getAllBookTopics',data);
	    }
	    this.findBookTopicsByQuery = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsByQuery',data);
	    }
	    this.findBookTopicById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicById',data);
	    }
	    this.findBookTopicsById = function(data) {
	        return $http.post(BASE_API + '/booktopic/findBookTopicsById',data);
	    }


	    this.updatePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/update',data);
	    }
	    this.removePreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/remove',data);
	    }
	    this.getAllPreviousPapers = function(data) {
	        return $http.post(BASE_API + '/previousPapers/getAll',data);
	    }
	    this.getPreviousPaper = function(data) {
	        return $http.post(BASE_API + '/previousPapers/get',data);
	    }


	    //Save Purchased Course Based on COurse ids
	    this.purchasedCourse = function(data) {
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnCourseId',data);
	    }
	    //Save purchased courses based on bundle ids
	    this.purchasedBundle = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnBundleId',data);
	    }
	    //save purchased courses based on training ids
	    this.purchasedTraining = function(data){
	        return $http.post(BASE_API + '/uc/saveCoursesBasedOnTrainingId',data);
	    }
	}


/***/ },

/***/ 360:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(51);
	__webpack_require__(43);

	angular.module('service').service('profile',Profile);
	Profile.$inject = ['$http','Upload','BASE_API'];
	function Profile($http,Upload,BASE_API) {
		var self = this;

		self.saveProfilePic = function(data){
			return Upload.upload({
	            url: BASE_API + '/dashboard/profile/saveProfilePic',
	            mehtod: 'POST',
	            data: data
	        });
		}

		self.getUserFriends = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserFriendsAndFollowings',data);
		}

		self.getAllUsersShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAllUsersShortDetails',data);
		}

		self.getUserShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserShortDetails',data);
		}

		self.getRecommendFriendForUser = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getRecommendFriendForUser',data);
		}

		self.sendFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/sendFriendRequest',data);	
		}

		self.acceptFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/acceptFriendRequest',data);	
		}

		self.deleteFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteFriendRequest',data);	
		}

		// unfriendFriend
		self.unfriendFriend = function(data){
			return $http.post(BASE_API + '/dashboard/profile/unfriendFriend',data);	
		}

		self.cancelFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/cancelFriendRequest',data);	
		}

		self.blockPerson = function(data){
			return $http.post(BASE_API + '/dashboard/profile/blockPerson',data);	
		}

		self.getFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequest',data);	
		}

		self.getFriendStatus = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendStatus',data);		
		}

		// Follower
		self.addFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addFollower',data);		
		}	
		self.getFollowers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFollowers',data);		
		}	
		self.removeFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/removeFollower',data);		
		}

		//user personal details
		self.getUserPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPersonalDetails',data);		
		}
		self.saveUserBasicPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveUserBasicPersonalDetails',data);		
		}

		self.getUserEmails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserEmails',data);		
		}

		self.getUserPhoneNumbers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPhoneNumbers',data);		
		}

		self.addUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserEmail',data);		
		}

		self.addUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserPhoneNumber',data);		
		}

		self.verifyUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserEmail',data);		
		}

		self.verifyUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserPhoneNumber',data);		
		}

		self.deleteUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserEmail',data);		
		}

		self.deleteUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		}
		
		// self.saveUserPhoneDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserPhoneDetails',data);		
		// }
		// self.saveUserEmailDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserEmailDetails',data);		
		// }
		// startVerifingEmailAddress
		self.startVerifingEmailAddress = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingEmailAddress',data);		
		}
		// startVerifingPhoneNumber
		self.startVerifingPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingPhoneNumber',data);		
		}

		// // confirmVerificationOfEmail
		// self.confirmVerificationOfEmail = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfEmail',data);		
		// }
		// // confirmVerificationOfPhoneNumber
		// self.confirmVerificationOfPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfPhoneNumber',data);		
		// }

		// //deleteUserPhoneNumber
		// self.deleteUserPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		// }

		// // deleteUserEmailAddress
		// self.deleteUserEmailAddress = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserEmailAddress',data);		
		// }

		// getUserContactDetails
		self.getUserContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserContactDetails',data);		
		}
		// addNonVerifiedEmailInContactDetails
		self.addNonVerifiedEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addNonVerifiedEmailInContactDetails',data);		
		}

		// confirmVerificationOfContactEmail
		self.confirmVerificationOfContactEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfContactEmail',data);		
		}

		// deleteEmailInContactDetails
		self.deleteEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEmailInContactDetails',data);		
		}

		// changePrimaryEmailInContactDetails
		self.changePrimaryEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/changePrimaryEmailInContactDetails',data);		
		}	


		// Work Form Helper
		self.saveWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveWorkInfo',data);		
		}	
		self.getWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getWorkInfo',data);		
		}
		self.deleteWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteWorkInfo',data);		
		}

		// Edu Form Helper
		self.saveEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveEduInfo',data);		
		}	
		self.getEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getEduInfo',data);		
		}
		self.deleteEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEduInfo',data);		
		}

		// Exam Form Helper
		self.saveExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveExamInfo',data);		
		}	
		self.getExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getExamInfo',data);		
		}
		self.deleteExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteExamInfo',data);		
		}

		// Interested Course Form Helper
		self.saveInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveInterestedCourseInfo',data);		
		}	
		self.getInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getInterestedCourseInfo',data);		
		}
		self.deleteInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteInterestedCourseInfo',data);		
		}

		// Author Form Helper
		self.saveAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorInfo',data);		
		}	
		self.getAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAuthorInfo',data);		
		}
		self.deleteAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteAuthorInfo',data);		
		}
		self.saveAuthorDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorDescInfo',data);		
		}

		// Teacher info Form Helper
		self.saveTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherInfo',data);		
		}	
		self.getTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTeacherInfo',data);		
		}
		self.deleteTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteTeacherInfo',data);		
		}
		self.saveTeacherDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherDescInfo',data);		
		}

		// Kid info Form Helper
		self.saveKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveKidInfo',data);		
		}	
		self.getKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getKidInfo',data);		
		}
		self.deleteKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteKidInfo',data);		
		}


		// Friend request notification api
		// getFriendRequestNotifications
		self.getFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequestNotifications',data);		
		}
		// updateFriendRequestNotifications
		self.updateFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateFriendRequestNotifications',data);		
		}

		// getTaggedFriendsNotifications
		self.getTaggedFriendsNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTaggedFriendsNotifications',data);		
		}
		// updateTaggedFriendNotifications
		self.updateTaggedFriendNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateTaggedFriendNotifications',data);		
		}

		self.getFriendsListForMessage = function(data){
			return $http.post(BASE_API + '/chat/friends/find',data);		
		}
	}

/***/ },

/***/ 447:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(120);
	__webpack_require__(360)
	__webpack_require__(448);
	angular.module(window.moduleName)
	    .component('userRolesPage', {
	        template: __webpack_require__(450),
	        controller: UserRolesController,
	        controllerAs: 'vm'
	    })

	UserRolesController.$inject = ['$log','$filter', '$scope', '$state', 'EditorConfig', '$timeout','admin','profile'];

	function UserRolesController($log,$filter, $scope, $state, EditorConfig, $timeout,admin,profile) {
	    var vm = this;

	    vm.selectedUsers = [];
	    vm.selectedForms = [];
	    vm.form = {};
	    vm.form.roles = [];
	    vm.form_access = {};

	    vm.addUserRole = addUserRole;
	    vm.editUserRole = editUserRole;
	    vm.deleteUserRole = deleteUserRole;
	    vm.onUserChange = onUserChange;
	    vm.addRoleBtnClick = addRoleBtnClick;
	    vm.addRoleToArray = addRoleToArray;
	    vm.onFormChange = onFormChange;
	    vm.getStringOfForms = getStringOfForms;
	    vm.editRoleInArray = editRoleInArray;
	    vm.deleteRoleFromArray = deleteRoleFromArray;
	    
	    vm.$onInit = function() {
	        getAllUsers();
	        getAllRoles();
	        getAllForms();
	        getAllUserRoles();

	    }

	    function getAllUserRoles(){
	        admin.getAllUserRoles()
	            .then(function(res){
	                vm.allUserRoles = res.data;
	                $log.debug(res);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function getAllUsers(){
	        profile.getAllUsersShortDetails()
	            .then(function(d){
	                vm.allUsers = d.data;
	            })
	            .catch(function(err){
	                console.log(err);
	            })
	    }

	    function getAllRoles(){
	        admin.getAllRoles()
	            .then(function(res){
	                vm.allRoles = res.data;
	                $log.debug(res);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function getAllForms(){
	        admin.getAllForms()
	            .then(function(res){
	                vm.allForms = res.data;
	                $log.debug(res);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function onUserChange(){
	        vm.form = vm.form || {};
	        vm.form.usr_id = vm.selectedUsers[0] || "";
	        editUserRole(vm.form.usr_id);
	    }

	    function onFormChange(){
	        vm.form_access = {};
	        vm.selectedForms.map(function(v,i){
	            vm.form_access[v] = "yes";
	        })
	        console.log(vm.form_access)
	    }

	    function addUserRole(){
	        vm.formSuccess = undefined;
	        admin.addUserRole(vm.form)
	            .then(function(res){

	                var usr_id = res.data.usr_id;
	                var group = $filter("filter")(vm.allUserRoles,{usr_id : usr_id})[0];
	                if(group){
	                    var pos = vm.allUserRoles.indexOf(group);
	                    vm.allUserRoles.splice(pos,1);    
	                }

	                vm.allUserRoles.push(res.data);
	                vm.formSuccess = true;
	                vm.form = {};
	                vm.form.roles = [];
	                vm.form_access = {};
	                vm.selectedForms = [];
	                vm.selectedUsers = [];
	            })
	            .catch(function(err){
	                $log.error(err);
	                vm.formSuccess = false;
	            })
	    }

	    function editUserRole(usr_id){
	        if(!usr_id || usr_id.length <= 0){
	            vm.form = {};
	            vm.form.roles = [];
	            vm.selectedUsers = [];
	            return;
	        }
	        vm.formSuccess = undefined;
	        var group = $filter("filter")(vm.allUserRoles,{usr_id : usr_id});
	        if(group && group[0]){
	            vm.form = angular.copy(group[0]);
	            vm.selectedUsers = [group[0]["usr_id"]];
	        }
	    }

	    function deleteUserRole(usr_id){
	        var result = confirm("Are you sure you want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        vm.formSuccess = undefined;
	        admin.deleteUserRole({usr_id : usr_id})
	            .then(function(res){
	                var usr_id = res.data.usr_id;
	                var group = $filter("filter")(vm.allUserRoles,{usr_id : usr_id})[0];
	                var pos = vm.allUserRoles.indexOf(group);
	                vm.allUserRoles.splice(pos,1);
	            })
	            .catch(function(err){
	                $log.error(err);
	            })
	    }

	    function addRoleBtnClick(){
	        vm.addingRoleIsInProgress = true;
	    }

	    function addRoleToArray(){
	        vm.form = vm.form || {};
	        vm.form.roles = vm.form.roles || {};
	        vm.form.roles.push({
	            role_id : vm.role_id,
	            form_access : vm.form_access
	        }) 


	        vm.role_id='';
	        vm.selectedForms=[];
	        vm.form_access={};
	        vm.addingRoleIsInProgress = false;
	    }

	    function editRoleInArray(index){
	        var role = vm.form.roles[index];
	        vm.role_id=role.role_id;
	        vm.selectedForms = Object.keys(role.form_access || {});
	        vm.form_access=role.form_access;
	        vm.addingRoleIsInProgress = true;
	        vm.form.roles.splice(index,1);
	    }

	    function deleteRoleFromArray(index) {
	        vm.form.roles.splice(index,1);
	    }

	    function getStringOfForms(obj){
	        var allKeys = Object.keys(obj);
	        return allKeys.join(" , ");
	    }
	}

/***/ },

/***/ 448:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(449);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_userRoles.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_userRoles.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 449:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.add-role-btn {\n  border: 1px dashed #1d99d1;\n  font-family: 'nunito',sans-serif;\n  font-size: 20px;\n  cursor: pointer;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: center;\n  -moz-box-pack: center;\n  box-pack: center;\n  -webkit-justify-content: center;\n  -moz-justify-content: center;\n  -ms-justify-content: center;\n  -o-justify-content: center;\n  justify-content: center;\n  -ms-flex-pack: center;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n\n.usr-add-role-form {\n  font-family: 'nunito',sans-serif;\n  position: relative;\n  margin: 5px 0px;\n  cursor: pointer;\n  padding: 10px;\n  background: #f4f4f4;\n  border-radius: 5px; }\n\n.action-btns {\n  position: absolute;\n  right: 5px;\n  top: 5px;\n  font-size: 16px;\n  z-index: 10;\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n  .action-btns .action-btn {\n    margin: 0px 5px;\n    cursor: pointer; }\n", ""]);

	// exports


/***/ },

/***/ 450:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" onsubmit=\"return false;\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>User Roles</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"usrs\">Select User</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedUsers\" theme=\"bootstrap\" on-select=\"vm.onUserChange()\" on-remove=\"vm.onUserChange()\" limit=\"1\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Users...\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{$item.usr_id}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.usr_id as item in (vm.allUsers | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t        \t{{item.usr_id}}\n\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- Show Added Role -->\n\t\t\t\t\t\t<div class=\"usr-add-role-form\" ng-repeat=\"sep_role in vm.form.roles track by $index\">\n\t\t\t\t\t\t\t<div class=\"action-btns\">\n\t\t\t\t\t\t\t\t<div class=\"action-btn\" ng-click=\"vm.editRoleInArray($index)\"><i class=\"fa fa-edit\"></i></div>\n\t\t\t\t\t\t\t\t<div class=\"action-btn\" ng-click=\"vm.deleteRoleFromArray($index)\"><i class=\"fa fa-times\"></i></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"usrs\">Role</label>\n\t\t\t\t\t\t\t\t<label class=\"col-xs-10 control-label\" style=\"text-align: left;\">\n\t\t\t\t\t\t\t\t\t{{sep_role.role_id}}\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"sep_role.role_id === 'admin'\">\n\t\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"usrs\">Form access</label>\n\t\t\t\t\t\t\t\t<label class=\"col-xs-10 control-label\" style=\"text-align: left;\">\n\t\t\t\t\t\t\t\t\t{{vm.getStringOfForms(sep_role.form_access)}}\t\t\t\t\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- Add seperate role form -->\n\t\t\t\t\t\t<div class=\"usr-add-role-form\" ng-if=\"vm.addingRoleIsInProgress\">\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"usrs\">Select Role</label>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"vm.role_id\">\n\t\t\t\t\t\t\t\t\t\t<option value=\"\">Select Role</option>\n\t\t\t\t\t\t\t\t\t\t<option value=\"{{role.role_id}}\" ng-repeat=\"role in vm.allRoles\">{{role.role_name}}</option>\n\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"vm.role_id === 'admin'\">\n\t\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"usrs\">Select Forms</label>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedForms\" theme=\"bootstrap\" on-select=\"vm.onFormChange()\" on-remove=\"vm.onFormChange()\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Forms...\">\n\t\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t\t        \t{{$item.form_name}}\n\t\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.form_id as item in (vm.allForms | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t\t        <span>\n\t\t\t\t\t\t\t\t\t        \t{{item.form_name}}\n\t\t\t\t\t\t\t\t\t        </span>\n\t\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-3 col-xs-offset-6\">\n\t\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.role_id='';vm.selectedForms=[];vm.form_access={};vm.addingRoleIsInProgress=false;\">Cancel</button>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">\n\t\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" \n\t\t\t\t\t\t\t\t\t\tng-click=\"vm.addRoleToArray()\">Save</button>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"!vm.addingRoleIsInProgress && vm.form.roles.length < 3\" ng-click=\"vm.addRoleBtnClick()\">\n\t\t\t\t\t\t\t<label class=\"add-role-btn\" for=\"usrs\">Add Role</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\" ng-if=\"!vm.addingRoleIsInProgress\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.form= {};vm.form.roles=[]\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" \n\t\t\t\t\t\t\t\t\tng-disabled=\"!vm.form.usr_id || vm.form.roles.length < 1\"\n\t\t\t\t\t\t\t\t\tng-click=\"vm.addUserRole()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ds-alert\" ng-if=\"!vm.allUserRoles || vm.allUserRoles.length <= 0\">No Data to display</div>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allUserRoles\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"form in vm.allUserRoles\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteUserRole(form.usr_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editUserRole(form.usr_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t\t\t{{form.usr_id}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in form\">\n\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t<div class=\"col-xs-10 desc\" style=\"word-break: break-word;\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});