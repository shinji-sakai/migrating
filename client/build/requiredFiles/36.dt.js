webpackJsonp([36],{

/***/ 412:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(413);
	angular.module(window.moduleName)
	    .component('bundlePage', {
	    	require: {
	    		parent : '^adminPage'
	    	},
	        template: __webpack_require__(415),
	        controller: Controller,
	        controllerAs: 'vm'
	    })

	Controller.$inject = ['$log','admin','$filter','$scope','$state'];
	function Controller($log,admin,$filter,$scope,$state){

		var vm = this;
		vm.form = {};
		vm.form.includedCourses = [];
		vm.no_of_item_already_fetch = 0;
		vm.saveBundle = saveBundle;
		vm.editBundle = editBundle;
		vm.deleteBundle = deleteBundle;
		vm.resetForm  = resetForm;

		vm.$onInit = function(){
			getAllCoursesFromMaster();
		//	getAllBundle();
		}

		function getAllCoursesFromMaster() {
			admin.coursemaster()
				.then(function(d){
					vm.allCourses = d;
				})
				.catch(function(err){
					console.error(err);
				});
		}
	  	vm.fetchBundles = function(){
				vm.isBundlesLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
					collection : 'bundle'
				})
				.then(function(res){
					$log.debug(res)
					vm.allBundle= res.data;
					vm.no_of_item_already_fetch = 5;
					vm.last_update_dt = vm.allBundle[vm.allBundle.length - 1].update_dt;
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isBundlesLoading = false;
				})
			}
				vm.fetchMoreBundleks = function(){
				vm.isBundlesLoading = true;
				admin.fetchItemUsingLimit({
					no_of_item_to_fetch : 5,
						collection : 'bundle',
					  update_dt : vm.last_update_dt
				})
				.then(function(res){
					if(res.data.length > 1){
						res.data.forEach(function(v,i){
							var item = $filter('filter')(vm.allBundle, {bndl_id: v.bndl_id})[0];
							if(!item){
								vm.allCourses.push(v)		
								vm.no_of_item_already_fetch++;
							}
						})
						vm.last_update_dt = vm.allBundle[vm.allBundle.length - 1].update_dt;
					}else{
						vm.noMoreData = true;
					}
					
				})
				.catch(function(err){
					$log.error(err)
				})
				.finally(function(){
					vm.isBundlesLoading = false;
				})
			}

		function getAllBundle() {
			admin.getAllBundle()
				.then(function(res){
					vm.allBundle = res.data;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function saveBundle() {
				if(!vm.formIsInEditMode){
	                vm.form.create_dt = new Date();
	                vm.form.update_dt = new Date();
	            }else{
	                vm.form.update_dt = new Date();
	            }
			admin.saveBundle(vm.form)
				.then(function(res){
				//	getAllBundle();
					vm.fetchBundles();
					resetForm();
					vm.formIsInEditMode=false;
				})
				.catch(function(err){
					console.error(err);
				})
		}

		function deleteBundle(id){
			admin.deleteBundle({bndl_id : id})
				.then(function(res){
					var item = $filter('filter')(vm.allBundle || [], {bndl_id: id})[0];								
						 if (vm.allBundle.indexOf(item) > -1) {
	                        var pos = vm.allBundle.indexOf(item);
	                       vm.allBundle.splice(pos, 1);
	                    }
				//	getAllBundle();
					resetForm();
				})
				.catch(function(err){
					console.error(err);
				})	
		}

		function editBundle(id){
			var pl = $filter('filter')(vm.allBundle,{bndl_id:id})[0];
			vm.form = pl;
			vm.formIsInEditMode=true;
		}

		function resetForm(){
			vm.form = {};
		}
	}

/***/ },

/***/ 413:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(414);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_bundle.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_bundle.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 414:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "", ""]);

	// exports


/***/ },

/***/ 415:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-courselist-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form-horizontal\" ng-submit=\"vm.saveBundle()\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Course Bundle</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" for=\"course name\">Bundle Name<i style=\"color: red\">*</i></label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Bundle Name\" ng-model=\"vm.form.bndl_nm\" name=\"\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-2 control-label\" >Include Courses</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.form.includedCourses\" theme=\"bootstrap\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Course...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.courseId as item in (vm.allCourses | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.courseName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-5 col-xs-offset-2\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" ng-click=\"vm.resetForm()\">Reset</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t\t<button class=\"form-control btn btn-primary submit\" type=\"submit\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === true;\">Data saved successfully.</div>\n\t\t\t\t\t\t<div class=\"alert alert-success\" ng-if=\"vm.formSuccess === false;\">Error in data saving.</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchBundles()\" ng-if=\"!vm.allBundle || vm.allBundle.length <= 0\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isBundlesLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isBundlesLoading\"></i>\n\t\t\t\t<span class=\"text\">Show Bundles</span>\n\t\t\t</div>\t\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<!--<div class=\"ds-alert\" ng-if=\"!vm.allBundle\">No Data to display</div>-->\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" ng-if=\"vm.allBundle\">\n\t\t\t\t<uib-accordion close-others=\"false\" ng-repeat=\"bundle in vm.allBundle\">\n\t\t\t\t<div uib-accordion-group is-open=\"status.open\">\n\t\t\t\t<uib-accordion-heading>\n\t\t\t\t\t<div ng-click=\"\" class=\"\">\n\t\t\t\t\t\t<i class=\"fa pull-right panel-arrow\" ng-class=\"{'fa-arrow-right':!status.open,'fa-arrow-down':status.open}\"></i>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons edit\" ng-click=\"vm.editBundle(bundle.bndl_id)\"><i class=\"fa fa-pencil\"></i></span>\n\t\t\t\t\t\t<span class=\"pull-right panel-header-buttons remove\" ng-click=\"vm.deleteBundle(bundle.bndl_id)\"><i class=\"fa fa-times\"></i></span>\n\t\t\t\t\t\t{{bundle.bndl_nm}}\n\t\t\t\t\t</div>\n\t\t\t\t</uib-accordion-heading>\n\t\t\t\t<div class=\"row course-info-row\" ng-repeat=\"(key,value) in bundle\">\n\t\t\t\t\t\t\t<div class=\"col-xs-2 title\">{{key}}</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-10 desc\">{{value}}</div>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t</uib-accordion>\n\t\t\t</div>\n\t\t\t\t<div class=\"ew-rounded-btn margin-top-bottom-10\" ng-click=\"vm.fetchMoreBundleks()\" ng-if=\"(vm.allBundle==[] || vm.allBundle.length > 0) && !vm.noMoreData\">\n\t\t\t\t<i class=\"fa fa-plus right\" ng-if=\"!vm.isBundlesLoading\"></i>\n\t\t\t\t<i class=\"fa fa-circle-o-notch right\" ng-if=\"vm.isBundlesLoading\"></i>\n\t\t\t\t<span class=\"text\">More</span>\n\t\t\t</div>\t\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});