webpackJsonp([54],{

/***/ 360:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(51);
	__webpack_require__(43);

	angular.module('service').service('profile',Profile);
	Profile.$inject = ['$http','Upload','BASE_API'];
	function Profile($http,Upload,BASE_API) {
		var self = this;

		self.saveProfilePic = function(data){
			return Upload.upload({
	            url: BASE_API + '/dashboard/profile/saveProfilePic',
	            mehtod: 'POST',
	            data: data
	        });
		}

		self.getUserFriends = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserFriendsAndFollowings',data);
		}

		self.getAllUsersShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAllUsersShortDetails',data);
		}

		self.getUserShortDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserShortDetails',data);
		}

		self.getRecommendFriendForUser = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getRecommendFriendForUser',data);
		}

		self.sendFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/sendFriendRequest',data);	
		}

		self.acceptFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/acceptFriendRequest',data);	
		}

		self.deleteFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteFriendRequest',data);	
		}

		// unfriendFriend
		self.unfriendFriend = function(data){
			return $http.post(BASE_API + '/dashboard/profile/unfriendFriend',data);	
		}

		self.cancelFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/cancelFriendRequest',data);	
		}

		self.blockPerson = function(data){
			return $http.post(BASE_API + '/dashboard/profile/blockPerson',data);	
		}

		self.getFriendRequest = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequest',data);	
		}

		self.getFriendStatus = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendStatus',data);		
		}

		// Follower
		self.addFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addFollower',data);		
		}	
		self.getFollowers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFollowers',data);		
		}	
		self.removeFollower = function(data){
			return $http.post(BASE_API + '/dashboard/profile/removeFollower',data);		
		}

		//user personal details
		self.getUserPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPersonalDetails',data);		
		}
		self.saveUserBasicPersonalDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveUserBasicPersonalDetails',data);		
		}

		self.getUserEmails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserEmails',data);		
		}

		self.getUserPhoneNumbers = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserPhoneNumbers',data);		
		}

		self.addUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserEmail',data);		
		}

		self.addUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addUserPhoneNumber',data);		
		}

		self.verifyUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserEmail',data);		
		}

		self.verifyUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/verifyUserPhoneNumber',data);		
		}

		self.deleteUserEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserEmail',data);		
		}

		self.deleteUserPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		}
		
		// self.saveUserPhoneDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserPhoneDetails',data);		
		// }
		// self.saveUserEmailDetails = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/saveUserEmailDetails',data);		
		// }
		// startVerifingEmailAddress
		self.startVerifingEmailAddress = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingEmailAddress',data);		
		}
		// startVerifingPhoneNumber
		self.startVerifingPhoneNumber = function(data){
			return $http.post(BASE_API + '/dashboard/profile/startVerifingPhoneNumber',data);		
		}

		// // confirmVerificationOfEmail
		// self.confirmVerificationOfEmail = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfEmail',data);		
		// }
		// // confirmVerificationOfPhoneNumber
		// self.confirmVerificationOfPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfPhoneNumber',data);		
		// }

		// //deleteUserPhoneNumber
		// self.deleteUserPhoneNumber = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
		// }

		// // deleteUserEmailAddress
		// self.deleteUserEmailAddress = function(data){
		// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserEmailAddress',data);		
		// }

		// getUserContactDetails
		self.getUserContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getUserContactDetails',data);		
		}
		// addNonVerifiedEmailInContactDetails
		self.addNonVerifiedEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/addNonVerifiedEmailInContactDetails',data);		
		}

		// confirmVerificationOfContactEmail
		self.confirmVerificationOfContactEmail = function(data){
			return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfContactEmail',data);		
		}

		// deleteEmailInContactDetails
		self.deleteEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEmailInContactDetails',data);		
		}

		// changePrimaryEmailInContactDetails
		self.changePrimaryEmailInContactDetails = function(data){
			return $http.post(BASE_API + '/dashboard/profile/changePrimaryEmailInContactDetails',data);		
		}	


		// Work Form Helper
		self.saveWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveWorkInfo',data);		
		}	
		self.getWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getWorkInfo',data);		
		}
		self.deleteWorkInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteWorkInfo',data);		
		}

		// Edu Form Helper
		self.saveEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveEduInfo',data);		
		}	
		self.getEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getEduInfo',data);		
		}
		self.deleteEduInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteEduInfo',data);		
		}

		// Exam Form Helper
		self.saveExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveExamInfo',data);		
		}	
		self.getExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getExamInfo',data);		
		}
		self.deleteExamInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteExamInfo',data);		
		}

		// Interested Course Form Helper
		self.saveInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveInterestedCourseInfo',data);		
		}	
		self.getInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getInterestedCourseInfo',data);		
		}
		self.deleteInterestedCourseInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteInterestedCourseInfo',data);		
		}

		// Author Form Helper
		self.saveAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorInfo',data);		
		}	
		self.getAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getAuthorInfo',data);		
		}
		self.deleteAuthorInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteAuthorInfo',data);		
		}
		self.saveAuthorDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveAuthorDescInfo',data);		
		}

		// Teacher info Form Helper
		self.saveTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherInfo',data);		
		}	
		self.getTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTeacherInfo',data);		
		}
		self.deleteTeacherInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteTeacherInfo',data);		
		}
		self.saveTeacherDescInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveTeacherDescInfo',data);		
		}

		// Kid info Form Helper
		self.saveKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/saveKidInfo',data);		
		}	
		self.getKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getKidInfo',data);		
		}
		self.deleteKidInfo = function(data){
			return $http.post(BASE_API + '/dashboard/profile/deleteKidInfo',data);		
		}


		// Friend request notification api
		// getFriendRequestNotifications
		self.getFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getFriendRequestNotifications',data);		
		}
		// updateFriendRequestNotifications
		self.updateFriendRequestNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateFriendRequestNotifications',data);		
		}

		// getTaggedFriendsNotifications
		self.getTaggedFriendsNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/getTaggedFriendsNotifications',data);		
		}
		// updateTaggedFriendNotifications
		self.updateTaggedFriendNotifications = function(data){
			return $http.post(BASE_API + '/dashboard/profile/updateTaggedFriendNotifications',data);		
		}

		self.getFriendsListForMessage = function(data){
			return $http.post(BASE_API + '/chat/friends/find',data);		
		}
	}

/***/ },

/***/ 461:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(360);
	__webpack_require__(462)
	angular.module(window.moduleName)
	    .component('verifyPracticeQuestionPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(464),
	        controller: AdminVerifyPracticeQuestionController,
	        controllerAs: 'vm'
	    });

	AdminVerifyPracticeQuestionController.$inject = ['admin', 'pageLoader', '$filter', '$scope', "profile"];

	function AdminVerifyPracticeQuestionController(admin, pageLoader, $filter, $scope, profile) {
	    var vm = this;

	    vm.selectedSubjects = [];
	    vm.selectedTopics = [];
	    vm.selectedQuestion = [];
	    vm.selectedTesters = [];

	    vm.nowDate = (new Date());
	    vm.yesterday = new Date(vm.nowDate);
	    vm.yesterday.setDate(vm.nowDate.getDate() - 1);
	    vm.yesterday = vm.yesterday.toString();


	    vm.checkValidityOfID = checkValidityOfID;
	    vm.updateExam = updateExam;
	    vm.resetFormDetail = resetFormDetail;
	    vm.removeExam = removeExam;
	    vm.editExam = editExam;

	    vm.$onInit = function() {
	        getAllSubjects();
	        getAllTopics();
	        getAllUsers();
	    };

	    function getAllUsers() {
	        profile.getAllUsersShortDetails()
	            .then(function(d) {
	                vm.allUsers = d.data;
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	    }

	    function getAllSubjects() {
	        admin.getAllSubject()
	            .then(function(d) {
	                vm.subjects = d.data;
	            });
	    }

	    function getAllTopics() {
	        admin.getAllTopic()
	            .then(function(d) {
	                vm.topics = d.data;
	            });
	    }

	    vm.onFilterChange = function onFilterChange(item, model) {
	        if (vm.selectedSubjects.length > 0) {
	            admin.getAllTopic({
	                    subjectId: vm.selectedSubjects[0]
	                })
	                .then(function(d) {
	                    vm.topics = d.data;
	                });
	        }
	        if (!vm.selectedTopics[0] || !vm.selectedSubjects[0]) {
	            return;
	        }
	        var query = {
	            subjects: vm.selectedSubjects,
	            topics: vm.selectedTopics
	        }
	        admin.findPracticeQuestionsByQuery(query)
	            .then(function(d) {
	                if (d.data.length > 0) {
	                    vm.questions = d.data;
	                    vm.getAllQuestionStatues();
	                } else {
	                    vm.questions = [];
	                }
	            })
	            .catch(function(err) {
	                console.log(err);
	            });
	    }

	    vm.makeRowEditable = function(index) {
	        if(vm.currentEditableRow || vm.currentEditableRow === 0){
	            vm.shouldShowAlert = true;
	            vm.goingToBeEditableRow = index;
	            return;
	        }
	        vm.currentEditableRow = index;
	    }
	    vm.saveEditableRow = function(index) {
	        var status = vm.visibleQuestionStatues[index];
	        status.test_comments = vm.test_comments || status.test_comments;
	        status.q_state_end_dt = status.q_state_end_dt ? new Date(status.q_state_end_dt) : undefined;
	        status.tested_dt = status.tested_dt ? new Date(status.tested_dt) : undefined;
	        status.tester_id = vm.selectedTesters[0];
	        vm.isStatusUpdating = true;
	        delete status["verify_id"]
	        admin.updateVerifyPracticeQuestion(status)
	            .then(function(res) {
	                vm.getAllQuestionStatues();
	                vm.test_comments = "";
	                vm.currentEditableRow = undefined;
	                vm.isStatusUpdating = false;
	            })
	            .catch(function(err) {

	            })
	    }

	    vm.getAllQuestionStatues = function() {
	        if (vm.questions.length > 0) {
	            var ids = vm.questions.map(function(v, i) {
	                return v.questionId
	            });
	            admin.getLatestVerifyStatusOfGivenPracticeQuestions({
	                    q_id: ids
	                })
	                .then(function(res) {
	                    console.log("getAllQuestionStatues..", res);
	                    if (res.data) {
	                        vm.allQuestionStatuses = res.data;
	                        vm.visibleQuestionStatues = angular.copy(vm.allQuestionStatuses)
	                    }
	                })
	                .catch(function(err) {
	                    console.error(err);
	                })
	        }
	    }

	    vm.onSelectedQuestionChange = function onSelectedQuestionChange(item, model) {
	        if (vm.selectedQuestion && vm.selectedQuestion.length > 0) {
	            vm.visibleQuestionStatues = vm.allQuestionStatuses.filter(function(v, i) {
	                if (vm.selectedQuestion.indexOf(v.q_id) > -1) {
	                    return true;
	                }
	                return false
	            })


	            // admin.getAllVerifyStatusOfPracticeQuestion({
	            //         q_id: vm.selectedQuestion[0]
	            //     })
	            //     .then(function(res) {
	            //         if (res.data) {
	            //             vm.selectedQuestionStatuses = res.data;
	            //         }
	            //     })
	            //     .catch(function(err) {
	            //         console.error(err);
	            //     })
	        } else {
	            vm.visibleQuestionStatues = vm.allQuestionStatuses
	        }
	    }

	    vm.onFilterStatusChange = function() {
	        if(!vm.form.test_status){
	            vm.visibleQuestionStatues = angular.copy(vm.allQuestionStatuses);
	            return;
	        }
	        vm.visibleQuestionStatues = vm.allQuestionStatuses.filter(function(v, i) {
	            if (v.test_status === vm.form.test_status) {
	                return true;
	            }
	            return false;
	        })
	    }

	    vm.updateQuestionStatus = function() {
	        admin.updateVerifyPracticeQuestion({
	                q_id: vm.selectedQuestion[0],
	                test_status: vm.form.test_status,
	                q_state_start_dt: new Date()
	            })
	            .then(function(res) {
	                vm.onSelectedQuestionChange();
	                vm.getAllQuestionStatues();
	                vm.form.test_status = "";
	            })
	            .catch(function(err) {

	            })
	    }


	    function checkValidityOfID() {
	        vm.form.examId = vm.form.examId || "";
	        vm.form.examId = vm.form.examId.replace(/\s/g, "-");
	        var arr = vm.exams.filter(function(v, i) {
	            if (vm.form.examId === v.examId) {
	                return true;
	            }
	            return false;
	        })
	        if (arr.length > 0) {
	            vm.idExists = true;
	        } else {
	            vm.idExists = false;
	        }
	    }

	    function updateExamsList() {
	        pageLoader.show();
	        admin.getAllExam()
	            .then(function(data) {
	                if (data.data.length > 0)
	                    vm.exams = data.data;
	                else
	                    vm.exams = [];
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function updateExam() {
	        var data = {
	            examId: vm.form.examId,
	            examName: vm.form.examName,
	            examDesc: vm.form.examDesc
	        }
	        pageLoader.show();
	        admin.updateExam(data)
	            .then(function(res) {
	                vm.submitted = true;
	                vm.form.error = false;
	                vm.form.examName = "";
	                vm.form.examDesc = "";
	                vm.form.examId = "";
	                vm.idExists = false;
	                vm.formIsInEditMode = false;
	                updateExamsList();
	            })
	            .catch(function() {
	                vm.submitted = true;
	                vm.form.error = true;
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function resetFormDetail() {
	        vm.submitted = false;
	        vm.idExists = false;
	        vm.formIsInEditMode = false;
	        vm.form = {};
	    }

	    function removeExam(id) {
	        var result = confirm("Want to delete?");
	        if (!result) {
	            //Declined
	            return;
	        }
	        pageLoader.show();
	        admin.removeExam({
	                examId: id
	            })
	            .then(function(d) {
	                console.log(d);
	                updateExamsList();
	            })
	            .catch(function(err) {
	                console.log(err);
	            })
	            .finally(function() {
	                pageLoader.hide();
	            })
	    }

	    function editExam(id) {

	        var book = $filter('filter')(vm.exams, {
	            examId: id
	        })[0];
	        vm.form = {};
	        vm.form.examId = book.examId;
	        vm.form.examName = book.examName;
	        vm.form.examDesc = book.examDesc;
	        vm.formIsInEditMode = true;
	    }
	}

/***/ },

/***/ 462:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(463);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(11)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_style.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./_style.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 463:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(5)();
	// imports


	// module
	exports.push([module.id, "body, button, input {\n  -webkit-font-smoothing: antialiased;\n  letter-spacing: .1px; }\n\nbody {\n  font-family: Roboto,\"Helvetica Neue\",Helvetica,Arial,sans-serif;\n  font-size: 13px;\n  line-height: 1.846;\n  color: #666; }\n\n*, :after, :before {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n/*********************\nBREAKPOINTS\n*********************/\n.ew-small-modal-wrapper .verify-question-modal {\n  width: 700px;\n  max-width: initial; }\n\n.ew-small-modal-wrapper .verfiy-stats-options {\n  font-family: 'nunito' , sans-serif; }\n  .ew-small-modal-wrapper .verfiy-stats-options .opt {\n    padding: 5px;\n    display: block; }\n  .ew-small-modal-wrapper .verfiy-stats-options textarea {\n    width: 100%;\n    box-shadow: 0px 0px 0px;\n    border: 1px solid #e5e5e5;\n    border-radius: 5px;\n    padding: 10px; }\n\n.ew-small-modal-wrapper .action-btns {\n  display: -webkit-box;\n  display: -moz-box;\n  display: box;\n  display: -webkit-flex;\n  display: -moz-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -moz-box-align: center;\n  box-align: center;\n  -webkit-align-items: center;\n  -moz-align-items: center;\n  -ms-align-items: center;\n  -o-align-items: center;\n  align-items: center;\n  -ms-flex-align: center;\n  -webkit-box-orient: horizontal;\n  -moz-box-orient: horizontal;\n  box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -moz-box-direction: normal;\n  box-direction: normal;\n  -webkit-flex-direction: row;\n  -moz-flex-direction: row;\n  flex-direction: row;\n  -ms-flex-direction: row; }\n\n.ew-small-modal-wrapper .info-icon {\n  background: #1778a4;\n  color: white;\n  border-radius: 50%;\n  padding: 5px 12px;\n  margin-right: 10px; }\n", ""]);

	// exports


/***/ },

/***/ 464:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t<form class=\"form form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Verify PracticeQuestion Status</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Subject</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedSubjects\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.subjectId as item in (vm.subjects | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">Topic</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTopics\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"question\">Filter Question</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedQuestion\" theme=\"bootstrap\" on-select=\"vm.onSelectedQuestionChange($item, $model)\" on-remove=\"vm.onSelectedQuestionChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Question...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.questionId\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.questionId as item in (vm.questions | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.questionNumber}} - {{item.questionId}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"question\">Filter Status</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-change=\"vm.onFilterStatusChange()\" ng-model=\"vm.form.test_status\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">Select Question Status</option>\n\t\t\t\t\t\t\t\t\t<option value=\"ready_to_test\">Ready To Test</option>\n\t\t\t\t\t\t\t\t\t<option value=\"approved\">Approved</option>\n\t\t\t\t\t\t\t\t\t<option value=\"rejected\">Rejected</option>\n\t\t\t\t\t\t\t\t\t<option value=\"bug_fixing\">Bug Fixing</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">Error in Submiting Data</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data added succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n\t\t\t<h5 ng-if=\"vm.visibleQuestionStatues && vm.visibleQuestionStatues.length > 0\">All Question Latest Statuses</h6>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" style=\"background: white;\" ng-if=\"vm.visibleQuestionStatues && vm.visibleQuestionStatues.length > 0\">\n\t\t\t\t<table class=\"table table-condensed table-bordered\">\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th></th>\n\t\t\t\t\t\t<th>Q ID</th>\n\t\t\t\t\t\t<th>Test Status</th>\n\t\t\t\t\t\t<th>Test Comments</th>\n\t\t\t\t\t\t<th>Tested Date</th>\n\t\t\t\t\t\t<th>Tester Id</th>\t\t\t\t\t\t\n\t\t\t\t\t\t<th>Q State Start Date</th>\n\t\t\t\t\t\t<th>Q State End Date</th>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr ng-repeat=\"status in vm.visibleQuestionStatues track by $index\">\n\t\t\t\t\t\t<td style=\"text-align: center;\">\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow != $index\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\" style=\"color: green;cursor: pointer;\" ng-click=\"vm.makeRowEditable($index)\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow === $index\">\n\t\t\t\t\t\t\t\t<i class=\"fa\" ng-class=\"{'fa-save':!vm.isStatusUpdating,'fa-circle-o-notch fa-spin':vm.isStatusUpdating}\"  style=\"color: red;cursor: pointer;\" ng-click=\"vm.saveEditableRow($index)\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td><a target=\"practice_view\" ui-sref=\"practiceView({topic:vm.selectedTopics[0],question:status.q_id})\">{{status.q_id}}</a></td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow != $index\">\n\t\t\t\t\t\t\t\t{{status.test_status}}\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow === $index\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" ng-model=\"status.test_status\">\n\t\t\t\t\t\t\t\t\t<option value=\"ready_to_test\">Ready To Test</option>\n\t\t\t\t\t\t\t\t\t<option value=\"approved\">Approved</option>\n\t\t\t\t\t\t\t\t\t<option value=\"rejected\">Rejected</option>\n\t\t\t\t\t\t\t\t\t<option value=\"bug_fixing\">Bug Fixing</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow != $index\">\n\t\t\t\t\t\t\t\t{{status.test_comments | limitTo : 20}}...\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow === $index\">\n\t\t\t\t\t\t\t\t<span ng-if=\"vm.test_comments || status.test_comments\">{{vm.test_comments || status.test_comments | limitTo : 10}}...</span>\n\t\t\t\t\t\t\t\t<span class=\"ew-rounded-btn\" ng-click=\"vm.shouldShowEditCommentModal = true;vm.test_comments = status.test_comments;\">\n\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil right\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"text\">Edit</span>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow != $index\">\n\t\t\t\t\t\t\t\t{{status.tested_dt | date : 'dd-MMM-yyyy'}}\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow === $index\">\n\t\t\t\t\t\t\t\t<datepicker date-format=\"MM-dd-yyyy\" class=\"col-xs-10\" date-min-limit=\"{{vm.yesterday}}\">\n\t\t\t\t\t\t\t\t\t<input class=\"form-control\" ng-model=\"status.tested_dt\" placeholder=\"Tested Date\">\n\t\t\t\t\t\t\t\t</datepicker>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow != $index\">\n\t\t\t\t\t\t\t\t{{status.tester_id}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow === $index\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-init=\"vm.selectedTesters = status.tester_id ? [status.tester_id] : []\" ng-model=\"vm.selectedTesters\" theme=\"bootstrap\" limit=\"1\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select User...\">\n\t\t\t\t\t\t\t\t        <span>{{$item.dsp_nm}} - {{$item.usr_id}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.usr_id as item in (vm.allUsers | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.dsp_nm}} - {{item.usr_id}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t{{status.q_state_start_dt | date : 'dd-MMM-yyyy'}}\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow != $index\">\n\t\t\t\t\t\t\t\t{{status.q_state_end_dt | date : 'dd-MMM-yyyy'}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div ng-if=\"vm.currentEditableRow === $index\">\n\t\t\t\t\t\t\t\t<datepicker date-format=\"MM-dd-yyyy\" class=\"col-xs-10\" date-min-limit=\"{{vm.yesterday}}\">\n\t\t\t\t\t\t\t\t\t<input class=\"form-control\" ng-model=\"status.q_state_end_dt\" placeholder=\"State End Date\">\n\t\t\t\t\t\t\t\t</datepicker>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</table>\n\t\t\t</div>\n\t\t</div>\n\t\t<!-- <div class=\"col-md-12\">\n\t\t\t<h5 ng-if=\"vm.selectedQuestionStatuses && vm.selectedQuestionStatuses.length > 0\">Selected Question Statuses</h6>\n\t\t\t<div class=\"courses-wrapper accordion-panel-wrapper\" style=\"background: white;\" ng-if=\"vm.selectedQuestionStatuses && vm.selectedQuestionStatuses.length > 0\">\n\t\t\t\t<table class=\"table table-condensed table-bordered\">\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>Edit</th>\n\t\t\t\t\t\t<th>Q ID</th>\n\t\t\t\t\t\t<th>Test Status</th>\n\t\t\t\t\t\t<th>Test Comments</th>\n\t\t\t\t\t\t<th>Tested Date</th>\n\t\t\t\t\t\t<th>Tester Id</th>\t\t\t\t\t\t\n\t\t\t\t\t\t<th>Q State Start Date</th>\n\t\t\t\t\t\t<th>Q State End Date</th>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr ng-repeat=\"status in vm.selectedQuestionStatuses\">\n\t\t\t\t\t\t<th>Edit</th>\n\t\t\t\t\t\t<td><a target=\"_blank\" ui-sref=\"practiceView({topic:vm.selectedTopics[0],question:status.q_id})\">{{status.q_id}}</a></td>\n\t\t\t\t\t\t<td>{{status.test_status}}</td>\n\t\t\t\t\t\t<td>{{status.test_comments}}</td>\n\t\t\t\t\t\t<td>{{status.tested_dt | date : 'dd-MMM-yyyy'}}</td>\n\t\t\t\t\t\t<td>{{status.tester_id}}</td>\n\t\t\t\t\t\t<td>{{status.q_state_start_dt | date : 'dd-MMM-yyyy'}}</td>\n\t\t\t\t\t\t<td>{{status.q_state_end_dt | date : 'dd-MMM-yyyy'}}</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</table>\n\t\t\t</div>\n\t\t</div> -->\n\t\t\n\t</div>\n</div>\n<div class=\"ew-small-modal-wrapper\" ng-show=\"vm.shouldShowEditCommentModal\">\n    <div class=\"ew-modal verify-question-modal\">\n        <div class=\"msg\">Enter Comments</div>\n        <div class=\"verfiy-stats-options\">\n        \t<textarea placeholder=\"Comments\" tbio configuration=\"default\" rows=\"10\" ng-model=\"vm.test_comments\"></textarea>\n        </div>\n        <div class=\"action-btns\">\n            <div class=\"ew-btn\" ng-click=\"vm.shouldShowEditCommentModal = false;\">\n            \t<i class=\"fa fa-times right\"></i>\n            \t<span class=\"text\">Close</span>\n            </div>\n            <div class=\"ew-rounded-btn\" ng-click=\"vm.shouldShowEditCommentModal = false;\">\n            \t<i class=\"fa fa-pencil right\"></i>\n            \t<span class=\"text\">Save</span>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"ew-small-modal-wrapper\" ng-show=\"vm.shouldShowAlert\">\n    <div class=\"ew-modal verify-question-modal\">\n        <div class=\"msg\">Unsaved changes will be lost.Are you sure you want to change?</div>\n        <div class=\"action-btns\">\n            <div class=\"ew-btn\" ng-click=\"vm.shouldShowAlert = false;\">\n            \t<i class=\"fa fa-times right\"></i>\n            \t<span class=\"text\">No</span>\n            </div>\n            <div class=\"ew-rounded-btn\" ng-click=\"vm.shouldShowAlert = false;vm.currentEditableRow=vm.goingToBeEditableRow\">\n            \t<i class=\"fa fa-pencil right\"></i>\n            \t<span class=\"text\">Yes</span>\n            </div>\n        </div>\n    </div>\n</div>\n<style type=\"text/css\">\n\ttd{\n\t\tvertical-align: middle !important;\n    \ttext-align: center !important;\n\t}\n\tdatepicker{\n\t    width: 100% !important;\n\t    padding: 0px;\n\t    border: 1px solid #e5e5e5;\n\t    box-shadow: 0px 0px 0px;\n\t    padding: 0px 10px;\n\t    border-radius: 5px;\n\t    height: 32px !important;\n\t}\n\tdatepicker input{\n\t\twidth: 100% !important;\n    box-shadow: 0px 0px 0px !important;\n    height: 30px !important;\n    font-size: 16px !important;\n    font-family: 'nunito',sans-serif;\n\t}\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n\t\n</style>";

/***/ }

});