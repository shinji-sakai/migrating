webpackJsonp([6],{

/***/ 173:
/***/ function(module, exports, __webpack_require__) {

	var Base64 = __webpack_require__(58).Base64;
	angular.module(window.moduleName)
	    .component('itemtransferPage', {
	        require: {
	            parent: '^adminPage'
	        },
	        template: __webpack_require__(174),
	        controller: AdminItemTransferController,
	        controllerAs: 'vm'
	    });

	AdminItemTransferController.$inject = ['$scope', 'admin', '$filter', 'pageLoader'];

	function AdminItemTransferController($scope, admin, $filter, pageLoader) {


	    var vm = this;

	    //////////////
	    //Variables //
	    //////////////
	    vm.optionCount = 1;
	    vm.form = {};
	    vm.form.questionTime = 1;
	    vm.form.action = "copy";
	    vm.form.options = [];
	    // vm.form.textExplanation = "Enter Your Explanation Here";
	    vm.currentPage = 1;
	    vm.itemPerPage = 10;
	    // vm.form.questionNumber = 1;


	    vm.selectedSubjects = [];
	    vm.selectedAuthors = [];
	    vm.selectedPublications = [];
	    vm.selectedBooks = [];
	    vm.selectedExams = [];
	    vm.selectedFromTopics = [];
	    vm.selectedToTopics = [];
	    vm.selectedTags = [];

	    vm.filteredQuestionsPageNumber = 1;
	    vm.filteredQuestionsPageCount = 1;
	    vm.selectedQuestionsPageNumber = 1;
	    vm.selectedQuestionsPageCount = 1;
	    vm.questionsInOnePage = 3;


	    /////////////
	    //Function //
	    /////////////
	    vm.getNumber = getNumber;
	    vm.onFilterChange = onFilterChange;

	    vm.fecthFilteredQuestions = fecthFilteredQuestions;
	    vm.filteredQuestionPrev = filteredQuestionPrev;
	    vm.filteredQuestionNext = filteredQuestionNext;
	    vm.addToSelectedQuestions = addToSelectedQuestions;

	    vm.fecthSelectedQuestions = fecthSelectedQuestions;
	    vm.selectedQuestionPrev = selectedQuestionPrev;
	    vm.selectedQuestionNext = selectedQuestionNext;
	    vm.removeFromSelectedQuestions = removeFromSelectedQuestions;

	    vm.isQuestionSelected = isQuestionSelected;

	    vm.updateQuestionTopics = updateQuestionTopics;

	    vm.$onInit = function() {
	        $scope.$watch(function() {
	            return vm.parent.courselistdata;
	        }, function(v) {
	            vm.courselist = v;
	        });

	        admin.getAllSubject()
	            .then(function(d) {
	                vm.subjects = d.data;
	            });
	        admin.getAllAuthor()
	            .then(function(d) {
	                vm.authors = d.data;
	            });
	        admin.getAllPublication()
	            .then(function(d) {
	                vm.publications = d.data;
	            });
	        admin.getAllBook()
	            .then(function(d) {
	                vm.books = d.data;
	            });
	        admin.getAllExam()
	            .then(function(d) {
	                vm.exams = d.data;
	            });
	        admin.getAllTopic()
	            .then(function(d) {
	                vm.topics = d.data;
	            });
	        admin.getAllTag()
	            .then(function(d) {
	                vm.tags = d.data;
	            });
	        // updateQuestionList();
	    }

	    function getNumber(num) {
	        return Array.apply(null, {
	            length: num
	        });
	    }

	    function onFilterChange(item, model) {
	        if (vm.selectedSubjects.length > 0) {
	            admin.getAllTopic({
	                subjectId: vm.selectedSubjects[0]
	            })
	                .then(function(d) {
	                    vm.topics = d.data;
	                });
	        }
	        var query = {
	            subjects: vm.selectedSubjects,
	            authors: vm.selectedAuthors,
	            publications: vm.selectedPublications,
	            books: vm.selectedBooks,
	            exams: vm.selectedExams,
	            topics: vm.selectedFromTopics,
	            tags: vm.selectedTags
	        }
	        console.log(query);
	        admin.findPracticeQuestionsByQuery(query)
	            .then(function(d) {
	                console.log(d.data);
	                if (d.data.length > 0) {
	                    vm.filteredQuestions = d.data;
	                    vm.filteredQuestionsPageNumber = 1;
					    vm.filteredQuestionsPageCount = 1;
					    vm.selectedQuestionsPageNumber = 1;
					    vm.selectedQuestionsPageCount = 1;
					    if (vm.filteredQuestions.length > vm.questionsInOnePage) {
		                    vm.filteredQuestionsPageCount = Math.ceil(vm.filteredQuestions.length / vm.questionsInOnePage);
		                }
	                }
	            })
	            .catch(function(err) {
	                console.log(err);
	            });
	    }


	    function fecthFilteredQuestions() {
	        var start = (vm.filteredQuestionsPageNumber - 1) * vm.questionsInOnePage;
	        if (vm.filteredQuestions) {
	            return vm.filteredQuestions.slice(start, start + vm.questionsInOnePage);
	        } else {
	            return [];
	        }
	    }

	    function filteredQuestionPrev() {
	        if (vm.filteredQuestionsPageNumber > 1) {
	            vm.filteredQuestionsPageNumber--;
	        }
	    }

	    function filteredQuestionNext() {
	        if (vm.filteredQuestionsPageNumber < vm.filteredQuestionsPageCount) {
	            vm.filteredQuestionsPageNumber++;
	        }
	    }

	    function fecthSelectedQuestions() {
	        var start = (vm.selectedQuestionsPageNumber - 1) * vm.questionsInOnePage;
	        if (vm.selectedQuestions) {
	            return vm.selectedQuestions.slice(start, start + vm.questionsInOnePage);
	        } else {
	            return [];
	        }
	    }

	    function selectedQuestionPrev() {
	        if (vm.selectedQuestionsPageNumber > 1) {
	            vm.selectedQuestionsPageNumber--;
	        }
	    }

	    function selectedQuestionNext() {
	        if (vm.selectedQuestionsPageNumber < vm.selectedQuestionsPageCount) {
	            vm.selectedQuestionsPageNumber++;
	        }
	    }

	    function addToSelectedQuestions(questionId) {
	        var que = $filter('filter')(vm.filteredQuestions, {
	            questionId: questionId
	        })[0];
	        vm.selectedQuestions = vm.selectedQuestions || [];
	        vm.selectedQuestions.push(que);

	        if (vm.selectedQuestions.length > vm.questionsInOnePage) {
	            vm.selectedQuestionsPageCount = Math.ceil(vm.selectedQuestions.length / vm.questionsInOnePage);
	        }
	    }

	    function removeFromSelectedQuestions(questionId) {
	        var que = $filter('filter')(vm.selectedQuestions, {
	            questionId: questionId
	        })[0];
	        var pos = vm.selectedQuestions.indexOf(que);
	        vm.selectedQuestions.splice(pos, 1);
	    }

	    function isQuestionSelected(questionId) {
	        var que = $filter('filter')(vm.selectedQuestions, {
	            questionId: questionId
	        }) || [];
	        return (que.length > 0);
	    }

	    function updateQuestionTopics() {
	    	var qs = vm.fecthSelectedQuestions();
	    	console.log(vm.selectedToTopics);
	    	if(vm.form.action === "copy"){
	    		qs = qs.map(function(q) {
	    			q.topics = q.topics || [];
	    			q.topics = q.topics.concat(vm.selectedToTopics);
	    			return q;
	    		});
	    	}else if(vm.form.action === "move"){
	    		qs = qs.map(function(q) {
	    			vm.selectedFromTopics.forEach(function(v) {
	    				var pos = q.topics.indexOf(v);
	    				if(pos >= -1){
	    					q.topics.splice(pos,1);
	    				}
	    			});
	    			q.topics = q.topics || [];
	    			q.topics = q.topics.concat(vm.selectedToTopics);
	    			return q;
	    		});
	    	}
	    	console.log(qs);
	    	vm.submitted = false;
	    	admin.updateMultiplePracticeQuestions(qs)
	    		.then(function(res) {
	    			console.log(res);
	    			vm.form.error = false;
	    			vm.submitted = true;
	    			vm.selectedSubjects = [];
				    vm.selectedAuthors = [];
				    vm.selectedPublications = [];
				    vm.selectedBooks = [];
				    vm.selectedExams = [];
				    vm.selectedFromTopics = [];
				    vm.selectedToTopics = [];
				    vm.selectedTags = [];
				    vm.selectedQuestions = [];
				    vm.filteredQuestions = [];
				    vm.filteredQuestionsPageNumber = 1;
				    vm.filteredQuestionsPageCount = 1;
				    vm.selectedQuestionsPageNumber = 1;
				    vm.selectedQuestionsPageCount = 1;
	    		})
	    		.catch(function(err) {
	    			console.error(err);
	    			vm.form.error = true;
	    			vm.submitted = true;
	    			vm.error = err;
	    		})
	    }
	}

/***/ },

/***/ 174:
/***/ function(module, exports) {

	module.exports = "<div class=\"admin-page admin-practice-wrapper item-transfer-page-wrapper\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-12\">\n\t\t\t<div class=\"form-wrapper\">\n\t\t\t\t\n\t\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t<legend>Add Question</legend>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"subject\">Subject</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedSubjects\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Subject...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.subjectId as item in (vm.subjects | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.subjectName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"author\">Author</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedAuthors\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Author...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.authorId as item in (vm.authors | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.authorName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"publication\">Publication</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedPublications\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Publication...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.publicationName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.publicationId as item in (vm.publications | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.publicationName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"book\">Book</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedBooks\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Book...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.bookName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.bookId as item in (vm.books | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.bookName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"exam\">Exam</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedExams\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Exam...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.examName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.examId as item in (vm.exams | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.examName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"tag\">Tag</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedTags\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Tag...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.tagName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.tagId as item in (vm.tags | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"item.tagName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">From Topic</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedFromTopics\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"topic\">To Topic</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<ui-select multiple ng-model=\"vm.selectedToTopics\" theme=\"bootstrap\" on-select=\"vm.onFilterChange($item, $model)\" on-remove=\"vm.onFilterChange($item, $model)\" close-on-select=\"true\">\n\t\t\t\t\t\t\t\t    <ui-select-match placeholder=\"Select Topic...\">\n\t\t\t\t\t\t\t\t        <span ng-bind=\"$item.topicName\"></span>\n\t\t\t\t\t\t\t\t    </ui-select-match>\n\t\t\t\t\t\t\t\t    <ui-select-choices repeat=\"item.topicId as item in (vm.topics | filter: $select.search) track by $index\">\n\t\t\t\t\t\t\t\t        <span>{{item.topicNumber}} - {{item.topicName}}</span>\n\t\t\t\t\t\t\t\t    </ui-select-choices>\n\t\t\t\t\t\t\t\t</ui-select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"action\">Action</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t\t\t\t<select ng-model=\"vm.form.action\" class=\"form-control\">\n\t\t\t\t\t\t\t\t\t<option value=\"copy\" selected=\"selected\">Copy</option>\n\t\t\t\t\t\t\t\t\t<option value=\"move\">Move</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t\t<div class=\"filtered-question-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"total-questions\">\n\t\t\t\t\t\t\t\t\t<div class=\"head\">All Questions</div>\n\t\t\t\t\t\t\t\t\t<div class=\"page-changer\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"count\">{{vm.filteredQuestionsPageNumber}}/{{ vm.filteredQuestionsPageCount}}</div>\n\t\t\t\t\t\t\t\t\t\t<span class=\"prev\" ng-class=\"{'disabled':!(vm.filteredQuestionsPageNumber > 1)}\" ng-click=\"vm.filteredQuestionPrev()\"><i class=\"fa fa-angle-left\"></i></span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"next\" ng-class=\"{'disabled':!(vm.filteredQuestionsPageNumber < vm.filteredQuestionsPageCount)}\" ng-click=\"vm.filteredQuestionNext()\"><i class=\"fa fa-angle-right\"></i></span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" ng-class=\"{'selected':vm.isQuestionSelected(question.questionId)}\" ng-repeat=\"question in vm.fecthFilteredQuestions()\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\" ng-bind-html=\"question.question\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"oneLine\" ng-bind-html=\"question.questionOneLine\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"action-btn\" ng-if=\"!vm.isQuestionSelected(question.questionId)\" ng-click=\"vm.addToSelectedQuestions(question.questionId)\"><i class=\"fa fa-plus\"></i></div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" style=\"padding: 10px;\" ng-if=\"vm.fecthFilteredQuestions().length <= 0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\">No Question Found - Change Filter</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"selected-questions\">\n\t\t\t\t\t\t\t\t\t<div class=\"head\">Selected Questions</div>\n\t\t\t\t\t\t\t\t\t<div class=\"page-changer\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"count\">{{vm.selectedQuestionsPageNumber}}/{{vm.selectedQuestionsPageCount}}</div>\n\t\t\t\t\t\t\t\t\t\t<span class=\"prev\" ng-class=\"{'disabled':!(vm.selectedQuestionsPageNumber > 1)}\" ng-click=\"vm.selectedQuestionPrev()\"><i class=\"fa fa-angle-left\"></i></span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"next\" ng-class=\"{'disabled':!(vm.selectedQuestionsPageNumber < vm.selectedQuestionsPageCount)}\" ng-click=\"vm.selectedQuestionNext()\"><i class=\"fa fa-angle-right\"></i></span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" ng-repeat=\"question in vm.fecthSelectedQuestions()\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\" ng-bind-html=\"question.question\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"oneLine\" ng-bind-html=\"question.questionOneLine\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"action-btn\" ng-click=\"vm.removeFromSelectedQuestions(question.questionId)\"><i class=\"fa fa-times\"></i></div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"question-wrapper\" style=\"padding: 10px;\" ng-if=\"vm.fecthSelectedQuestions().length <= 0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"code\">No Question Selected</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-xs-offset-8 col-xs-4\">\n\t\t\t\t\t\t\t\t<button\n\t\t\t\t\t\t\t\tclass=\"form-control btn btn-primary submit\" ng-disabled=\"vm.fecthSelectedQuestions().length <= 0 || vm.selectedToTopics.length <= 0\" ng-click=\"vm.updateQuestionTopics()\">Save</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\t\t\t\t<div ng-if=\"vm.submitted\">\n\t\t\t\t\t<div class=\"ds-alert\" ng-if=\"vm.form.error\">{{vm.error}}</div>\n\t\t\t\t\t<div class=\"ds-success\" ng-if=\"!vm.form.error\">Data updated succefully</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<style type=\"text/css\">\n\tbody{\n\t\tbackground-color: #ecf0f1;\n\t}\n</style>";

/***/ }

});