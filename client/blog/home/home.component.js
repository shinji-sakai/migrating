require('authToken');
require('service.admin');
require('directive.SideLinksArticleComponent');
require('directive.Compile');
require('service.pageLoader');
require('directive.pageLoading');
require('directive.menu');
require('directive.itemComments');
require('service.utility');
require('service.itemQueries');
require('service.aws');


require('./_home.scss');

angular.module('blog')
    .component('homePage', {
        template: require('./home.html'),
        controller: HomeController,
        controllerAs: 'vm'
    })

HomeController.$inject = ['authToken','admin','$stateParams','pageLoader','$location','$scope','itemQueries','utility','awsService'];

function HomeController(authToken,admin,$stateParams,pageLoader,$location,$scope,itemQueries,utility,awsService) {
    var vm = this;

    vm.articleContent="";
    vm.activeRelatedTab = "comments"
    vm.articleId = $stateParams.id;

    vm.shouldShowSearchResult = ($stateParams.q) ? true : false;

    vm.activeItem = vm.articleId;

    vm.searchInGoogle = searchInGoogle;

    vm.getNotificationsPageUrl = utility.getNotificationsPageUrl;
    vm.getSpecificNotificationPageUrl = utility.getSpecificNotificationPageUrl;
    vm.getCourseDetailsPageUrl = utility.getCourseDetailsPageUrl;
     vm.getUserId = authToken.getUserId;
    vm.getUser = authToken.getUser;
    vm.getDashboardUrl = utility.getDashboardUrl;
    vm.isUserLoggedIn = authToken.isAuthenticated;
    vm.getSitemapUrl = utility.getSitemapUrl;
    vm.goToLogin = utility.goToLogin;
    vm.goToLogout = utility.goToLogout;
    vm.goToRegister = utility.goToRegister;
    vm.buyPage = utility.buyPage;

    vm.scrollUp = scrollUp;
    vm.scrollDown = scrollDown;
    vm.toggleSidebar = toggleSidebar;
    vm.openSidebar = openSidebar;
    vm.closeSidebar = closeSidebar;
    vm.saveQueryForItem = saveQueryForItem;
    vm.getQueriesForItem = getQueriesForItem;

    vm.getSignedDownloadUrl = getSignedDownloadUrl;

    vm.$onInit = function(){
    	
    	pageLoader.show();

        $(window).scroll(function() {
            var scrollTop = $("body").scrollTop();
            if(scrollTop > 100){
                vm.shouldShowScrollUpButton = true;
            }else{
                vm.shouldShowScrollUpButton = false;
            }

            $scope.$apply();
        })

    	admin.getSideLinkArticlePage({page_id : vm.articleId})
    		.then(function(res){
    			vm.articleName = res.data.page_nm;
    			console.log(vm.articleName);
                vm.entireArticle = res.data;
                vm.articleCourse = res.data.courseDetails;
    			vm.articleContent = res.data.page_content;
    		})
    		.catch(function(err){
    			console.error(err);
    		})
    		.finally(function(){
    			pageLoader.hide();
    		})
    }


    function searchInGoogle(event) {
        if(event.keyCode === 13){
            var searchText = vm.searchText.split(" ").join("+");

            var url = "https://www.google.com/search?q="+searchText;

            $location.search("q",searchText);
            location.reload();
        }
    }

    function scrollUp() {
        $('html,body').animate({scrollTop: 0},'slow');
    }

    function scrollDown() {
        $('html,body').animate({scrollTop: $(document).height()},'slow');
    }

    function openSidebar() {
        vm.showSidebar = true;
    }

    function closeSidebar() {
        vm.showSidebar = false;   
    }

    function toggleSidebar() {
        if(vm.showSidebar){
            closeSidebar();
        }else{
            openSidebar();
        }
    }

    function saveQueryForItem() {
        var obj = {
            itm_id: vm.activeItem,
            usr_id: authToken.getUserId() || 'n/a',
            qry_title: vm.queTitle
        }
        itemQueries.save(obj)
            .then(function(res) {
                vm.itemQueries = vm.itemQueries || [];
                vm.itemQueries.push(res.data);
                vm.queTitle = "";
                getQueriesForItem();
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function getQueriesForItem() {
        var obj = {
            itm_id: vm.activeItem,
        }
        itemQueries.getAll(obj)
            .then(function(res) {
                vm.itemQueries = res.data;
                console.log(res.data);
            })
            .catch(function(err) {
                console.error(err);
            })
    }


    vm.openNotificationModal = openNotificationModal;
    vm.closeNotificationModal = closeNotificationModal;
    vm.toggleNotificationModal = toggleNotificationModal;
    vm.getSendNotificationCount = getSendNotificationCount;
    vm.getNotificationByUserId = getNotificationByUserId;
    vm.markSendNotificationAsRead = markSendNotificationAsRead;

    function openNotificationModal() {
        if (!vm.allNotifications) {
            getNotificationByUserId();
        }
        vm.showNotificationModal = true;
    }

    function closeNotificationModal() {
        vm.showNotificationModal = false;
    }

    function toggleNotificationModal() {
        if (!vm.showNotificationModal) {
            openNotificationModal();
        } else {
            closeNotificationModal();
        }
    }

    function getSendNotificationCount() {
        admin.getSendNotificationCount({
                usr_id: authToken.getUserId()
            })
            .then(function(res) {
                console.log(res);
                var rows = res.data;
                if(rows[0]){
                    vm.notificaionCount = rows[0].noti_cnt;    
                }
                
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function getNotificationByUserId() {
        admin.getSendNotificationByUserId({
                usr_id: authToken.getUserId()
            })
            .then(function(res) {
                vm.allNotifications = res.data;
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function markSendNotificationAsRead($event,usr_noti_pk) {
        $event.stopPropagation();
        $event.preventDefault();
        console.log($event);

        admin.markSendNotificationAsRead({
                usr_id: authToken.getUserId(),
                usr_noti_pk: usr_noti_pk
            })
            .then(function(res) {
                getNotificationByUserId();
            })
            .catch(function(err) {})
    }

    function getSignedDownloadUrl(info) {
        var key = info;
        awsService.getS3SignedUrl({
                type: 'download',
                key: key
            }).then(function(res) {
                $scope.$broadcast('pauseVideo');
                var url = res.data;
                window.location.href = url;
            })
            .catch(function(err) {
                console.log(err);
            })
    }

}