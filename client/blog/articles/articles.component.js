require('authToken');
require('service.admin');
require('directive.SideLinksArticleComponent');
require('directive.Compile');
require('service.pageLoader');

require('directive.pageLoading');
require('./_articles.scss');

angular.module('blog')
    .component('articlesPage', {
        template: require('./articles.html'),
        controller: Controller,
        controllerAs: 'vm'
    })

Controller.$inject = ['authToken','admin','pageLoader','$state'];

function Controller(authToken,admin,pageLoader,$state) {
    var vm = this;

    vm.getBlogArticleUrl  = getBlogArticleUrl;

    vm.$onInit = function(){
    	pageLoader.show();
    	admin.getAllSideLinksArticlePage()
    		.then(function(res){
    			vm.allArticles = res.data;
    		})
    		.catch(function(err){
    			console.error(err);
    		})
    		.finally(function(){
    			pageLoader.hide();
    		})
    }

    function getBlogArticleUrl(cat,id){
        return $state.href("home",{
            cat : cat + "-tutorials",
            id : id
        })
    }
}