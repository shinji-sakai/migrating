require('angular');
require('jquery');
require('theme_css');
window.Config = require('config_js');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-animate');
require('angular-sanitize');
require('oclazyload');
require('angular-local-storage');
require('angular-jwt');
require('serviceModule');
require('directiveModule');
require('base_scss');
require('authToken')

var Stats = require('stats_js');


// Http Interceptor
HttpInter.$inject = ['localStorageService'];
function HttpInter(localStorageService) {
    return {
        request: function(config) {
            return config;
        },
        response: function(response) {
        	var imageBaseUrl = response.headers('imageBaseUrl');
        	localStorageService.set('imageBaseUrl', imageBaseUrl);
            return response;
        }
    };
}

angular.module("blog",[
		'ui.router',
		'ui.bootstrap',
		'ngAnimate',
		'ngSanitize',
		"oc.lazyLoad",
		'LocalStorageModule',
		"angular-jwt"
	])
	.constant("EditorConfig",{
        toolbarButtons : ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', '|', 'insertLink',  'insertTable', 'undo', 'redo', 'clearFormatting', 'selectAll'],
        quickInsertButtons : ['image'],
        toolbarSticky: false,
        heightMin: 200,
        fontSizeDefaultSelection: '18',
        fontSizeSelection: true,
        placeholderText: 'Type Here',
        imageUploadParam: 'upload',
        imageUploadURL: '/ckeditor/save',
        imageUploadMethod: 'POST',
        imageMaxSize: 5 * 1024 * 1024,
        imageAllowedTypes: ['jpeg', 'jpg', 'png'],
        htmlAllowedTags : ['a', 'abbr', 'address', 'area', 'article', 'aside', 'audio', 'b', 'base', 'bdi', 'bdo', 'blockquote', 'br', 'button', 'canvas', 'caption', 'cite', 'code', 'col', 'colgroup', 'datalist', 'dd', 'del', 'details', 'dfn', 'dialog', 'div', 'dl', 'dt', 'em', 'embed', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'header', 'hgroup', 'hr', 'i', 'iframe', 'img', 'input', 'ins', 'kbd', 'keygen', 'label', 'legend', 'li', 'link', 'main', 'map', 'mark', 'menu', 'menuitem', 'meter', 'nav', 'noscript', 'object', 'ol', 'optgroup', 'option', 'output', 'p', 'param', 'pre', 'progress', 'queue', 'rp', 'rt', 'ruby', 's', 'samp', 'script', 'style', 'section', 'select', 'small', 'source', 'span', 'strike', 'strong', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'textarea', 'tfoot', 'th', 'thead', 'time', 'title', 'tr', 'track', 'u', 'ul', 'var', 'video', 'wbr','ew-img','side-links'],
        htmlAllowedEmptyTags : ['textarea', 'a', 'iframe', 'object', 'video', 'style', 'script', '.fa' , 'ew-img'],
        htmlAllowedAttrs : ['accept', 'accept-charset', 'accesskey', 'action', 'align', 'alt', 'async', 'autocomplete', 'autofocus', 'autoplay', 'autosave', 'background', 'bgcolor', 'border', 'charset', 'cellpadding', 'cellspacing', 'checked', 'cite', 'class', 'color', 'cols', 'colspan', 'content', 'contenteditable', 'contextmenu', 'controls', 'coords', 'data', 'data-.*', 'datetime', 'default', 'defer', 'dir', 'dirname', 'disabled', 'download', 'draggable', 'dropzone', 'enctype', 'for', 'form', 'formaction', 'headers', 'height', 'hidden', 'high', 'href', 'hreflang', 'http-equiv', 'icon', 'id', 'ismap', 'itemprop', 'keytype', 'kind', 'label', 'lang', 'language', 'list', 'loop', 'low', 'max', 'maxlength', 'media', 'method', 'min', 'multiple', 'name', 'novalidate', 'open', 'optimum', 'pattern', 'ping', 'placeholder', 'poster', 'preload', 'pubdate', 'radiogroup', 'readonly', 'rel', 'required', 'reversed', 'rows', 'rowspan', 'sandbox', 'scope', 'scoped', 'scrolling', 'seamless', 'selected', 'shape', 'size', 'sizes', 'span', 'src', 'srcdoc', 'srclang', 'srcset', 'start', 'step', 'summary', 'spellcheck', 'style', 'tabindex', 'target', 'title', 'type', 'translate', 'usemap', 'value', 'valign', 'width', 'wrap','side-links','ew-article','ew-section']
    })
	.factory('HttpInter', HttpInter)
	.run(['$rootScope','$state','$stateParams',function($rootScope,$state,$stateParams) {
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
			new Stats();
	  	});
	  	$rootScope.$state = $state;
  		$rootScope.$stateParams = $stateParams;
	}])
	.config(['$stateProvider','$urlRouterProvider','$httpProvider','$locationProvider','$ocLazyLoadProvider',function($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider,$ocLazyLoadProvider) {
		
		$stateProvider
			.state('articles',{
				url : "/articles",
				template : "<articles-page></articles-page>",
				resolve : {
					loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
                        var defer = $q.defer();
                        $ocLazyLoad.toggleWatch(true);
                        require(['./articles/articles.component.js'],function(){
                            $ocLazyLoad.inject();
                            $ocLazyLoad.toggleWatch(false);
                            defer.resolve();
                        });
                        return defer.promise;
                    }]	
				}
			})
			.state('home',{
				url : "/:cat/:id?q",
				template : "<home-page></home-page>",
				resolve : {
					loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
                        var defer = $q.defer();
                        $ocLazyLoad.toggleWatch(true);
                        require(['./home/home.component.js'],function(){
                            $ocLazyLoad.inject();
                            $ocLazyLoad.toggleWatch(false);
                            defer.resolve();
                        });
                        return defer.promise;
                    }]	
				},
				data : { pageTitle: 'Blog - examwarrior.com' },
				reloadOnSearch : true
			});

		$urlRouterProvider.otherwise("/hi/hi");
		// use the HTML5 History API
        $locationProvider.html5Mode({
        	enabled: true,
  			requireBase: false
        });
        $httpProvider.interceptors.push('HttpInter');
	}])