var webpack = require("webpack");
var path = require('path');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
var commonsPlugin = new webpack.optimize.CommonsChunkPlugin('common.js');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
    entry: {
        adminApp: "./admin/app.js",
        blog : './blog/app.js',
        "responsive-auth-pages" : './responsive-auth-pages/app.js'
    },
    devtool: 'false',
    output: {
        filename: '[name].js',
        path: './build',
        publicPath : '../',
        chunkFilename : 'requiredFiles/[name].dt.js',
    },
    externals: {
        // require("jquery") is external and available
        //  on the global var jQuery
        "jquery": "jQuery",
        "angular" : 'angular',
        'ocLazyLoad':'ocLazyLoad'
        // 'angular-ui-router' : 'angular-ui-router',
    },
    plugins: [
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery',
            'hljs'  : 'highlight_js'
        }),
        new ProgressBarPlugin(),
        commonsPlugin,
        // new ExtractTextPlugin('[name].css')
    ],
    module: {
        loaders: [{
            test: /\.scss$/,
            loader: "style-loader!css-loader!sass-loader"
            // loader : ExtractTextPlugin.extract('style', 'css!sass'),
        }, {
            test: /\.css$/,
            loader: "style-loader!css-loader"
        }, {
            test: /\.html$/,
            loader: "html-loader"
        }, {
            test: /\.(png|jpg|jpeg|gif|svg)$/,
            loader: 'url-loader?limit=10000&name=images/[name].[ext]'
        },{
            test: /\.(mp3)$/,
            loader: 'file-loader?limit=10000&name=audios/[name].[ext]'
        }, {
            test: /\.(woff|woff2|eot|otf|ttf)$/,
            loader: 'url-loader?limit=10000&name=fonts/[name].[ext]'
        }, {
            test: /\.json/,
            loader: "json-loader"
        }]
    },
    htmlLoader: {
        ignoreCustomFragments: [/\{\{.*?}}/]
    },
    resolve: {
        modulesDirectories: [
            'node_modules',
            'bower_components'
        ],
        alias: {

            'bootstrap_js'  : 'bootstrap/dist/js/bootstrap.min',
            'bootstrap_css' : 'bootstrap/dist/css/bootstrap.min.css',

            //css
            'theme_css': path.resolve(__dirname,'./assets/css/theme.min.css'),
            'fontawesome': path.resolve(__dirname,'./assets/css/fontawesome.min.css'),
            'datePicker_css' : path.resolve(__dirname,'./assets/css/datepicker.css'),
            'noUISlider_css' : path.resolve(__dirname,'./assets/js/noUiSlider.8.2.1/nouislider.min.css'),
            'prism_css' :  path.resolve(__dirname,'./assets/js/prism/prism.css'),
            'highlight_css' :  path.resolve(__dirname,'./assets/js/highlight/styles/github.css'),
            'tagsinput_css' : 'jquery.tagsinput/dist/jquery.tagsinput.min.css',
            'annotator_css' : path.resolve(__dirname,'./assets/js/annotator/annotator.css'),
            'videojs_css' : 'video.js/dist/video-js.min.css',

            //SCSS

            'base_scss': path.resolve(__dirname,'./assets/scss/base.scss'),
            'admin_scss': path.resolve(__dirname,'./assets/scss/admin.scss'),
            'home_scss': path.resolve(__dirname,'./assets/scss/home.scss'),
            'auth_scss': path.resolve(__dirname,'./assets/scss/auth.scss'),
            'dashboard_scss': path.resolve(__dirname,'./assets/scss/dashboard.scss'),
            'forums_scss': path.resolve(__dirname,'./assets/scss/forums.scss'),
            'books_scss': path.resolve(__dirname,'./assets/scss/books.scss'),
            'bourbon_scss' : path.resolve(__dirname,'./assets/scss/bourbon/_bourbon.scss'),
            'common_resource_scss' : path.resolve(__dirname,'./assets/scss/common/_common_resourse.scss'),
            'common_scss' : path.resolve(__dirname,'./assets/scss/common/_common.scss'),
            'comments_component_scss' : path.resolve(__dirname,'./assets/scss/component/_comments_component.scss'),


            //Javascript
            'cities_js': path.resolve(__dirname,'./assets/js/cities.js'),
            'custom_js': path.resolve(__dirname,'./assets/js/custom'),
            'config_js': path.resolve(__dirname,'./assets/js/config'),
            'paths_js': path.resolve(__dirname,'./assets/js/paths'),
            'auth_js': path.resolve(__dirname,'./assets/js/auth'),
            'flashDetect_js': path.resolve(__dirname,'./assets/js/flashDetect'),
            'hash_js': path.resolve(__dirname,'./assets/js/hash'),
            'stats_js': path.resolve(__dirname,'./assets/js/stats'),
            'datePicker_js' : path.resolve(__dirname,'./assets/js/bootstrap-datepicker.js'),
            'noUISlider_js' : path.resolve(__dirname,'./assets/js/noUiSlider.8.2.1/nouislider.min.js'),
            'jwplayer' : path.resolve(__dirname,'./assets/js/jwplayer/jwplayer.js'),
            'prism_js' : path.resolve(__dirname,'./assets/js/prism/prism.js'),
            'highlight_js' :  path.resolve(__dirname,'./assets/js/highlight/highlight.pack.js'),
            'tagsinput_js' : 'jquery.tagsinput/src/jquery.tagsinput.js',
            'annotator_js' : path.resolve(__dirname,'./assets/js/annotator/annotator.min.js'),
            'videojs_js' : 'video.js/dist/video-js.min',
            'html_docx' : path.resolve(__dirname,'./assets/js/html-docx/html-docx.js'),
            'angular-froala' : path.resolve(__dirname,'./assets/js/angular-froala.js'),
            'angular-textbox-io' : path.resolve(__dirname,'./assets/js/angular-textbox-io/index.js'),
            'angular-drop' : path.resolve(__dirname,'./assets/js/angular-drop/angular-drop.js'),
            'click_stats_js' : path.resolve(__dirname,'./assets/js/ClickStats.js'),
            'WebVTTParser'  : path.resolve(__dirname,'./assets/js/WebVTTParser.js'),
            "jquery.debounce" : path.resolve(__dirname,'./assets/js/jquery.debounce.js'),
            // 'custom_text_selector_js' : path.resolve(__dirname,'./assets/js/annotator/textselector.js'),


            'socket.io_client' : 'socket.io-client/socket.io.js',

            //Services
            'serviceModule': path.resolve(__dirname,'./shared/services/serviceModule'),
            'service.admin': path.resolve(__dirname,'./shared/services/admin'),
            'authToken': path.resolve(__dirname,'./shared/services/authToken.js'),
            'auth': path.resolve(__dirname,'./shared/services/auth'),
            'mail' : path.resolve(__dirname,'./shared/services/mail.service'),
            'UserCourses' : path.resolve(__dirname,'./shared/services/usercourses.service'),
            'service.post' : path.resolve(__dirname,'./shared/services/post.service'),
            'service.videoplayer' : path.resolve(__dirname,'./shared/services/videoplayer.service'),
            'service.fullscreen' : path.resolve(__dirname,'./shared/services/fullscreen.service'),
            'service.infoDetection' : path.resolve(__dirname,'./shared/services/infoDetection.service'),
            'service.pageLoader': path.resolve(__dirname,'./shared/services/pageLoader.service'),
            'service.preloadImages': path.resolve(__dirname,'./shared/services/preloadImages'),
            'service.bookmark': path.resolve(__dirname,'./shared/services/bookmark.service'),
            'service.qa': path.resolve(__dirname,'./shared/services/qa'),
            'service.videoURL': path.resolve(__dirname,'./shared/services/videoUrl'),
            'service.videoNotes': path.resolve(__dirname,'./shared/services/videoNotes'),
            'service.chat': path.resolve(__dirname,'./shared/services/chat.service'),
            'service.utility': path.resolve(__dirname,'./shared/services/utility.service'),
            'service.chatUtility': path.resolve(__dirname,'./shared/services/utility.chat'),
            'service.fileGenerator':path.resolve(__dirname,'./shared/services/fileGenerator.js'),
            'service.forums':path.resolve(__dirname,'./shared/services/forums.service.js'),
            'service.comments':path.resolve(__dirname,'./shared/services/comments.service.js'),
            'service.profile':path.resolve(__dirname,'./shared/services/profile.service.js'),
            'service.paypal':path.resolve(__dirname,'./shared/services/paypal.service.js'),
            'service.itemComments':path.resolve(__dirname,'./shared/services/itemComments.service.js'),
            'service.itemQueries':path.resolve(__dirname,'./shared/services/itemQueries.service.js'),
            'service.aws':path.resolve(__dirname,'./shared/services/aws.service.js'),
            'service.videoviews': path.resolve(__dirname,'./shared/services/videoviews.service.js'),
            'service.videorating': path.resolve(__dirname,'./shared/services/videorating.service.js'),
            'service.videovotes': path.resolve(__dirname,'./shared/services/videovotes.service.js'),
            'service.job': path.resolve(__dirname,'./shared/services/job.service.js'),
            'service.sms': path.resolve(__dirname,'./shared/services/sms.service.js'),
            'service.websocket': path.resolve(__dirname,'./shared/services/websocket.service.js'),
            'service.dashboardStats': path.resolve(__dirname,'./shared/services/dashboardStats.service.js'),
            'service.uploadFileAPI': path.resolve(__dirname,'./shared/services/uploadFileAPI.service.js'),
            'service.blog': path.resolve(__dirname,'./shared/services/blog.service.js'),
            'service.contactus': path.resolve(__dirname,'./shared/services/contact.service.js'),
            'service.praticeQuestions': path.resolve(__dirname,'./shared/services/practiceQuestions.service.js'),
            'service.bookHighlights': path.resolve(__dirname,'./shared/services/bookHighlights.service.js'),
            'service.notes': path.resolve(__dirname,'./shared/services/notes.service.js'),

            //Directives
            'directiveModule': path.resolve(__dirname,'./shared/directive/directiveModule'),
            'Header' : path.resolve(__dirname,'./shared/directive/header.directive'),
            'ErrImage' : path.resolve(__dirname,'./shared/directive/errImage.directive'),
            'directive.videoplayer_jw' :path.resolve(__dirname,'./shared/directive/videoControls.jw.directive'),
            'directive.videoplayer_videojs' :path.resolve(__dirname,'./shared/directive/videoControls.directive'),
            'directive.pageLoading' : path.resolve(__dirname,'./shared/directive/pageloading.directive'),
            'directive.modal' : path.resolve(__dirname,'./shared/directive/modal.directive'),
            'directive.comments' : path.resolve(__dirname,'./shared/directive/commentComponent/comments.component.js'),
            'directive.menu' : path.resolve(__dirname,'./shared/directive/menu/menu.component.js'),
            'directive.rating' : path.resolve(__dirname,'./shared/directive/rating.directive.js'),
            'directive.login' : path.resolve(__dirname,'./shared/directive/login/login.component.js'),
            'directive.chat' : path.resolve(__dirname,'./shared/directive/chat/chat.component.js'),
            'directive.profilePic' : path.resolve(__dirname,'./shared/directive/profilePic/profilePic.component.js'),
            'directive.AnsModalComponent': path.resolve(__dirname,'./shared/directive/ansModal/ansModal.component.js'),
            'directive.AnswerModalComponent': path.resolve(__dirname,'./shared/directive/answerModal/answerModal.component.js'),
            'directive.itemComments' : path.resolve(__dirname,'./shared/directive/ItemComments/itemComments.component.js'),
            'directive.itemCommentsResponsive' : path.resolve(__dirname,'./shared/directive/ItemCommentsResponsive/itemComments.component.js'),
            'directive.SlidesComponent': path.resolve(__dirname,'./shared/directive/slidesComponent/slides.component.js'),
            'directive.SideLinksArticleComponent': path.resolve(__dirname,'./shared/directive/blogSideLinksArticle/sideLinks.component.js'),
            'directive.Compile': path.resolve(__dirname,'./shared/directive/compile.directive.js'),
            'directive.myDrop': path.resolve(__dirname,'./shared/directive/myDrop.directive.js'),
            'directive.personalInfoForm': path.resolve(__dirname,'./shared/directive/personalInfoForm/personalInfoForm.component.js'),
            'directive.shareButtons': path.resolve(__dirname,'./shared/directive/shareButtons/shareButtons.component.js'),
            'directive.headerMenu': path.resolve(__dirname,'./shared/directive/headerMenu/headerMenu.component.js'),
            'directive.loginModal': path.resolve(__dirname,'./shared/directive/loginModal/loginModal.component.js'),
            'directive.subscribeModal': path.resolve(__dirname,'./shared/directive/subscribeModal/subscribe.component.js'),
            'directive.qaAnswers' : path.resolve(__dirname,'./shared/directive/qaAnswers/qaAnswers.component.js'),
            'directive.userDefaultPic' : path.resolve(__dirname,'./shared/directive/user-default-pic-directive.js'),
            'directive.treeComponent' : path.resolve(__dirname,'./shared/directive/treeComponent/treeComponent.js'),
            'directive.commentComponentResponsive' : path.resolve(__dirname,'./shared/directive/commentComponentResponsive/comment.js'),

            //Partials
            'partials.footer' : path.resolve(__dirname,'./partials/footer.html'),
            'partials.header' : path.resolve(__dirname,'./partials/header.html'),
            'partials.minimalControlBar' : path.resolve(__dirname,'./partials/minimalControlBar.html'),
            'partials.videoControlBar' :path.resolve(__dirname,'./partials/VideoControlBar.html'),
            'partials.videoControlBar_JW' :path.resolve(__dirname,'./partials/VideoControlBar.jw.html'),
            'partials.comments' :path.resolve(__dirname,'./partials/comments.html'),

            //Admin App
            'admin.AdminComponent': path.resolve(__dirname,'./admin/admin/admin.component'),
            'admin.AuthorComponent': path.resolve(__dirname,'./admin/author/author.component'),
            'admin.BookComponent': path.resolve(__dirname,'./admin/book/book.component'),
            'admin.CourselistComponent': path.resolve(__dirname,'./admin/courselist/courselist.component'),
            'admin.CourseMasterComponent': path.resolve(__dirname,'./admin/coursemaster/coursemaster.component'),
            'admin.DemoVideosComponent': path.resolve(__dirname,'./admin/demovideos/demovideos.component'),
            'admin.ExamComponent': path.resolve(__dirname,'./admin/exam/exam.component.js'),
            'admin.ModulesComponent': path.resolve(__dirname,'./admin/modules/modules.component'),
            'admin.PracticeComponent': path.resolve(__dirname,'./admin/practice/practice.component'),
            'admin.PublicationComponent': path.resolve(__dirname,'./admin/publication/publication.component'),
            'admin.PurchasedCoursesComponent': path.resolve(__dirname,'./admin/purchasedCourses/purchasedCourses.component'),
            'admin.SlidesComponent': path.resolve(__dirname,'./admin/slides/slides.component'),
            'admin.SubjectComponent': path.resolve(__dirname,'./admin/subject/subject.component'),
            'admin.SubtopicComponent': path.resolve(__dirname,'./admin/subtopic/subtopic.component'),
            'admin.TagsComponent': path.resolve(__dirname,'./admin/tags/tags.component'),
            'admin.TopicComponent': path.resolve(__dirname,'./admin/topic/topic.component'),
            'admin.VideoEntityComponent': path.resolve(__dirname,'./admin/videoEntity/videoEntity.component'),
            'admin.VideosComponent': path.resolve(__dirname,'./admin/videos/videos.component'),
            'admin.ItemTransfer': path.resolve(__dirname,'./admin/itemtransfer/itemtransfer.component'),
            'admin.QuestionAnalyticsComponent' : path.resolve(__dirname,'./admin/questionAnalytics/questionAnalytics.component.js'),
            'admin.ForumsCategoryComponent' : path.resolve(__dirname,'./admin/forumsCategory/forumsCategory.component.js'),
        }
    }
}
