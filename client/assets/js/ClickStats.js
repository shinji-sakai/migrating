function captureClicks(selector,callback) {
	//	selector will be parent element under which we want to capture all clicks
	//	callback will be called when click is captured
	$(selector).on('click',function(e) {
		var clickableElements = $(selector).find("a,button,select,input[type='reset'],input[type='button'],input[type='submit'],[onclick],[onmousedown],[onsubmit],[ondblclick],[data-ew-click]");
        var dsTarget = undefined;
        var isClickableElementFound = false;
        var isParent = false;
        dsTarget = e.target;

        //First Match Element From Array `this.elements`
        //Try To find target/Parent Of Target from `this.elements`
        if (clickableElements.index(dsTarget) > -1) {
            isClickableElementFound = true;
            // console.log("Element is Clickable from elements array");
        }else if(clickableElements.index(dsTarget.parentNode) > -1){
            isClickableElementFound = true;
            isParent = true;
            // dsTarget = dsTarget.parentNode;
            // console.log("Element Parent is Clickable from elements array");
        }

        //If Element is not exist in `this.elements`
        //Then retrieve DOM Node `events` using jquery functions
        //Ex : $._data($(ele).get(0),"events")
        if (!isClickableElementFound) {
            var events = $._data($(dsTarget).get(0), "events");
            if (events) {
                if (events["click"] || events["dblclick"] || events["mousedown"] || events["mouseup"] || events["submit"]) {
                    //Element is Clickable
                    isClickableElementFound = true;
                    // console.log("Clickable element found");
                }
            }
            //Check For Parent
            if (!isClickableElementFound) {
                var events = $._data($(dsTarget).parent().get(0), "events");
                if (events) {
                    if (events["click"] || events["dblclick"] || events["mousedown"] || events["mouseup"] || events["submit"]) {
                        //Element is Clickable
                        isClickableElementFound = true;
                        isParent = true;
                        // console.log("Clickable element found in parent");
                    }
                }
            }
        }

        if(isClickableElementFound){
            var ele = dsTarget;
            if(isParent){
                ele = dsTarget.parentNode;
            }

            var tag = $(ele).prop("tagName");
            var go_to_pg;
            if(tag && tag.toLowerCase() === "a"){

                go_to_pg = $(ele).attr("href");
                // if(go_to_pg.indexOf("4"))
            }


            var desc;
            if($(ele).data('title')){
                desc = $(ele).data('title');
            }else if($(ele).data('ew-click')){
                desc = $(ele).data('ew-click');
            }else if($(ele).attr('alt')){
                desc = $(ele).attr('alt');
            }else{
                desc = $(ele).attr('value');
                if(!desc)
                    desc = $(ele).text().trim();
            }
            if(!desc){
                var eleClass = $(ele).attr('class');
                var dynEle = document.createElement(tag);
                $(dynEle).attr('class',eleClass);
                desc = dynEle.outerHTML;

            }	

            var dataToSend = {
            	link_nm : desc,
                tag_typ : tag,
                element : ele
            }

            callback(dataToSend);
        }
	});
}


window.ClickStatsCapturer = captureClicks;
if(module.exports){
	module.exports = captureClicks;
}