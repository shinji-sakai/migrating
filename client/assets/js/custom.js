
$(function(){
    
})

//Use for dot navigation in note page
function notesDotNavigation(e) {
    $(e).parent().find('.active').removeClass("active");
    var id = $(e).data('target');
    $(e).addClass("active");
    var pos = parseInt($("#" + id).offset().top, 10);
    $('html,body').animate({
        scrollTop: pos
    }, 1000);
}


//Used to hide/show description in note page
function hideNoteDescription(ele) {
    var noteImage = $(ele).parent().find(".note-image");
    var noteDesc = $(ele).parent().find(".note-description");
    if ($(ele).parent().hasClass('description-opened')) {
        //Description is already opened 
        //Need to hide now
        var pWidth = $(ele).parent().width();
        var imageWidth = parseInt(noteImage.width()) + noteDesc.width() + 25;
        var percentWidth = (imageWidth / pWidth) * 100;
        noteImage.animate({
            width: percentWidth + "%",
        }, {
            duration: 200,
            queue: false
        });
        noteDesc.hide({
            duration: 200,
            queue: false
        });
        $(ele).find("i").removeClass('fa-arrow-left').addClass('fa-arrow-right');
        $(ele).parent().removeClass("description-opened");
    } else {
        //Description is already opened 
        //Need to hide now
        noteImage.animate({
            width: '70%'
        }, {
            duration: 200,
            queue: false
        });
        noteDesc.show({
            duration: 200,
            queue: false
        });
        $(ele).find("i").addClass('fa-arrow-left').removeClass('fa-arrow-right');
        $(ele).parent().addClass("description-opened");
    }
}


//Used to hide slide desc 
function hideSlideDescription(ele){
    var slideImage = $(ele).parent().find(".slide-image");
    var slideDesc = $(ele).parent().find(".slide-desc-wrapper");
    if ($(ele).parent().hasClass('description-opened')) {
        //Description is already opened 
        //Need to hide now
        var pWidth = $(ele).parent().width();
        var imageWidth = parseInt(slideImage.width()) + slideDesc.width() - 25;
        var percentWidth = (imageWidth / pWidth) * 100;
        slideImage.animate({
            width: percentWidth + "%",
            'margin-left' : '25px'
        }, {
            duration: 200,
            queue: false
        });
        slideDesc.hide({
            duration: 200,
            queue: false
        });
        $(ele).find("i").removeClass('fa-angle-right').addClass('fa-angle-left');
        $(ele).parent().removeClass("description-opened");
    } else {
        //Description is already opened 
        //Need to hide now
        slideImage.animate({
            width: '66.6%',
            'margin-left' : '0px'
        }, {
            duration: 200,
            queue: false
        });
        slideDesc.show({
            duration: 200,
            queue: false
        });
        $(ele).find("i").addClass('fa-angle-right').removeClass('fa-angle-left');
        $(ele).parent().addClass("description-opened");
    }
}

function isSupportsStorage(){
    try {
        window.localStorage.setItem('test', "test");
        window.localStorage.removeItem('test');
        return true;
    } catch(e) {
        return false;
    }
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
function deleteCookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function setCookie( name , value) {
  document.cookie = name + '=' + value;
}


//Shuffle Method for Array
Array.prototype.shuffle = function() {
    var o = this;
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}



$(".menu-button").on('click',function() {
    $('.home-menu-modal').slideToggle();
});
$(".close-modal-icon").on('click',function() {
    $('.home-menu-modal').slideToggle();
});

module.exports = {
    deleteCookie : deleteCookie,
    getCookie : getCookie,
    setCookie : setCookie,
    isSupportsStorage : isSupportsStorage
}