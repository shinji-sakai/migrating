var globals = require('custom_js');
function Auth(){
	this.tokenKey = "ls.dsToken";
	this.hashTokenKey = "ls.dsHashToken";
	this.userKey = "ls.dsUser";
	this.prevPageKey = "ls.dsPrevPage";
	this.sessionKey = "ls.ewSess";
	this.pageToRedirectKey = "dsPageToRedirect";
}
Auth.prototype.isAuthenticated = function(){
	return !!this.getToken() && !!this.getUser();
}
Auth.prototype.getToken = function(){
	return this.get(this.tokenKey);
}
Auth.prototype.setToken = function(value){
	return this.set(this.tokenKey,value);
}
Auth.prototype.getSessionId = function() {
    return this.get(this.sessionKey); //this.get(this.tokenKey);
}
Auth.prototype.setSessionId = function(value) {
    return this.set(this.sessionKey, value);
}
Auth.prototype.removeToken = function(){
	this.remove(this.tokenKey);
}

Auth.prototype.setHashToken = function(value){
	return this.set(this.hashTokenKey,value);
}
Auth.prototype.getHashToken = function(){
	return this.get(this.hashTokenKey);
}
Auth.prototype.removeHashToken = function(){
	this.remove(this.hashTokenKey);
}

Auth.prototype.getUser = function(){
	var str = this.get(this.userKey);
	if(str){
		str = JSON.parse(str);
	}
	return str;
}
Auth.prototype.setUser = function(user){
	return this.set(this.userKey,user);
}
Auth.prototype.removeUser = function(){
	this.remove(this.userKey);
}

Auth.prototype.setPrevPage = function(value){
	return sessionStorage[this.prevPageKey] = value;
}
Auth.prototype.getPrevPage = function(){
	return sessionStorage[this.prevPageKey];
}

Auth.prototype.setPageToRedirect = function(value){
	return sessionStorage[this.pageToRedirectKey] = value;
}
Auth.prototype.getPageToRedirect = function(){
	return sessionStorage[this.pageToRedirectKey];
}

Auth.prototype.set = function(key,value){
	if(globals.isSupportsStorage()){
		localStorage.setItem(key,value);
	}else{
		globals.setCookie(key,value);
	}
}
Auth.prototype.get = function(key){
	var token;
	if(globals.isSupportsStorage()){
		token = localStorage[key];
	}else{
		token = globals.getCookie(key);
	}
	return token;
}
Auth.prototype.remove = function(key){
	if(globals.isSupportsStorage()){
		localStorage.removeItem(key);
	}else{
		globals.deleteCookie(key);
	}
}


var auth = new Auth();
module.exports = auth;