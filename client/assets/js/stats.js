var auth = require('auth_js');
var platform = require('platform');
var FlashDetect = require('flashDetect_js');
var calcMD5 = require('hash_js').calcMD5;

function Stats() {
    var self = this;
    if (auth.getUser()) {
        this.usr_id = auth.getUser().usr_id;
    }
    this.GetFlashInfo();
    this.GetGeoLocationInfo();
    this.GetPrevPageInfo();
    this.GetCurrentPageInfo();
    this.GetBrowserInfo();
    this.GetOSInfo();
    this.GetResolution();
    this.GetIp().then(function() {
        self.GetLocation(self);
    }).then(function() {
        self.SendStats();
    });
    this.RegisterFocusAndBlurEvent();
    this.FindAllClickableElements();
    this.RegisterIdleTimer();

}

Stats.prototype.GetFlashInfo = function() {
    this.flashInfo = {
        flash_install: FlashDetect.installed,
    }

    // major: FlashDetect.major,
    // minor: FlashDetect.minor,
    // description: FlashDetect.raw,
    return this.flashInfo;
}
Stats.prototype.GetGeoLocationInfo = function() {
    if (navigator.geolocation) {
        this.isGeoLocationSupported = true;
    } else {
        this.isGeoLocationSupported = false;
    }
    return this.isGeoLocationSupported;
}
Stats.prototype.GetPrevPageInfo = function() {
    var prevPage = auth.getPrevPage();
    auth.setPageToRedirect(prevPage || "");
    auth.setPrevPage(window.location.href);
    this.prev_pg = prevPage;

    return prevPage;
}
Stats.prototype.ChangeAngularPrevPageInfo = function() {
    var prevPage = auth.getPrevPage();
    auth.setPrevPage(window.location.href);
    this.prev_pg = prevPage;
}

Stats.prototype.GetCurrentPageInfo = function() {
    return window.location.href;
}
Stats.prototype.GetBrowserInfo = function() {
    this.browser = {
        browser_nm: platform.name,
        browser_ver: platform.version,
        browser_desc: platform.description
    }
    return this.browser;
}
Stats.prototype.GetOSInfo = function() {
    this.os = {
        os_family: platform.os.family,
        os_ver: platform.os.version,
        os_archi: platform.os.architecture
    }
    return this.os;
}
Stats.prototype.GetResolution = function() {
    this.resolution = {
        res_width: screen.width,
        res_height: screen.height,
        device_pixel_ratio: window.devicePixelRatio
    }
    return this.resolution;
}
Stats.prototype.GetIp = function() {
        var stats = this;
        if (this.ip) {
            return this.ip;
        }
        return $.ajax({
                // 
                // http://jsonip.com/
                url: 'https://api.ipify.org?format=json',
                method: 'GET'
            })
            .done(function success(d) {
                stats.ip = d.ip;
                return stats.ip;
            })
            .fail(function error(err) {
                console.error(err);
            })
    }
    // window.ewLocation = undefined;
Stats.prototype.GetLocation = function(self) {
    if (!this)
        return;

    //TODO Temporary
    //Find new site with https instead of http://ip-api.com/json
    var stats = this;
    // stats.location = {};
    // return {};


    if (window.ewLocation) {
        // console.log("location....",window.ewLocation);
        return window.ewLocation;
    }

    // console.log("Fetching ip-api", new Date().getTime())
    return $.ajax({
            // url: 'http://ip-api.com/json/' + stats.ip,
            // https://ipinfo.io/json
            url: 'https://pro.ip-api.com/json?key=EkdeYkAQ5Y3wGG1',
            method: 'GET'
        })
        .done(function success(res) {
            // console.log("ip....",res);
            delete res["postal"];
            var t = res;
            delete t["postal"];

            t["ctry_cd"] = res["countryCode"] + "";
            t["hostname"] = res["as"] + "";
            t["ip"] = res["query"] + "";
            t["ip_gather_status"] = res["status"] + "";
            t["loc"] = res["lat"] + "|" + res["lon"];
            t["tm_zone"] = res["timezone"] + "";
            t["zip_cd"] = res["zip"] + "";

            // console.log(t);
            window.ewLocation = t;
            stats.location = t;
            // console.log("Done ip-api", new Date().getTime())
        })
        .fail(function error(err) {
            console.error(err);
        })
}
var IDLE_TIME = 60 * 1000;
Stats.prototype.RegisterFocusAndBlurEvent = function() {
    var counter = 0;
    var self = this;
    window.onblur = function() {
        //Start Idle Timer
        this.timerId = setInterval(function() {
            counter++;
            if (counter >= 5) {
                var data = {
                    idl_msg: 'User is Idle',
                    idl_rsn: 'By changing tab/minimizing',
                    curr_pg: window.location.href,
                    tkn: auth.getHashToken(),
                    login_tkn: auth.getToken(),
                    usr_id: self.usr_id,
                    clk_tm: new Date()
                }
                if (window.ewSendIdleEvent) {
                    self.SendData(data, 'useridle');
                }
                counter = 0;
            }
        }, IDLE_TIME);
    }
    window.onfocus = function() {
        //Stop Idle Timer
        if (this.timerId)
            clearInterval(this.timerId);
    }
}
Stats.prototype.RegisterIdleTimer = function() {
    var self = this;
    var idleTime = 0;
    var idleInterval = setInterval(function() {
        idleTime = idleTime + 1;
        if (idleTime >= 5) { // 5 minutes

            var data = {
                idl_msg: 'User is Idle',
                idl_rsn: 'With no mouse/keyboard activity',
                curr_pg: window.location.href,
                tkn: auth.getHashToken(),
                login_tkn: auth.getToken(),
                usr_id: self.usr_id,
                clk_tm: new Date()
            }
            if (window.ewSendIdleEvent) {
                self.SendData(data, 'useridle');
            }
            idleTime = 0;
        }
    }, IDLE_TIME);
    window.onmousemove = function(e) {
        idleTime = 0;
    };
    window.onkeypress = function(e) {
        idleTime = 0;
    };
}
Stats.prototype.FindAllClickableElements = function() {

    var self = this;
    document.onclick = function(e) {
        window.dsElements = $("a,button,select,input[type='reset'],input[type='button'],input[type='submit'],[onclick],[onmousedown],[onsubmit],[ondblclick],[data-ew-click]");
        window.dsTarget = undefined;
        var isClickableElementFound = false;
        var isParent = false;
        window.dsTarget = e.target;

        //First Match Element From Array `this.elements`
        //Try To find target/Parent Of Target from `this.elements`
        if (window.dsElements.index(window.dsTarget) > -1) {
            isClickableElementFound = true;
            // console.log("Element is Clickable from elements array");
        } else if (window.dsElements.index(window.dsTarget.parentNode) > -1) {
            isClickableElementFound = true;
            isParent = true;
            // window.dsTarget = window.dsTarget.parentNode;
            // console.log("Element Parent is Clickable from elements array");
        }

        //If Element is not exist in `this.elements`
        //Then retrieve DOM Node `events` using jquery functions
        //Ex : $._data($(ele).get(0),"events")
        if (!isClickableElementFound) {
            var events = $._data($(window.dsTarget).get(0), "events");
            if (events) {
                if (events["click"] || events["dblclick"] || events["mousedown"] || events["mouseup"] || events["submit"]) {
                    //Element is Clickable
                    isClickableElementFound = true;
                    // console.log("Clickable element found");
                }
            }
            //Check For Parent
            if (!isClickableElementFound) {
                var events = $._data($(window.dsTarget).parent().get(0), "events");
                if (events) {
                    if (events["click"] || events["dblclick"] || events["mousedown"] || events["mouseup"] || events["submit"]) {
                        //Element is Clickable
                        isClickableElementFound = true;
                        isParent = true;
                        // console.log("Clickable element found in parent");
                    }
                }
            }
        }

        if (isClickableElementFound) {
            var ele = window.dsTarget;
            if (isParent) {
                ele = window.dsTarget.parentNode;
            }

            var tag = $(ele).prop("tagName");
            var go_to_pg;
            if (tag && tag.toLowerCase() === "a") {

                go_to_pg = $(ele).attr("href");
                // if(go_to_pg.indexOf("4"))
            }


            var desc;
            if ($(ele).data('title')) {
                desc = $(ele).data('title');
            } else if ($(ele).data('ew-click')) {
                desc = $(ele).data('ew-click');
            } else if ($(ele).attr('alt')) {
                desc = $(ele).attr('alt');
            } else {
                desc = $(ele).attr('value');
                if (!desc)
                    desc = $(ele).text().trim();
            }
            if (!desc) {
                var eleClass = $(ele).attr('class');
                var dynEle = document.createElement(tag);
                $(dynEle).attr('class', eleClass);
                desc = dynEle.outerHTML;

            }


            var data = {
                    link_nm: desc,
                    tag_typ: tag,
                    curr_pg: window.location.href,
                    go_to_pg: go_to_pg,
                    tkn: auth.getHashToken(),
                    login_tkn: auth.getToken(),
                    usr_id: self.usr_id,
                    clk_tm: new Date()
                }
                // console.log("Click",data);
            self.SendData(data, 'userclick');
        }
    }
}


//Modern Browsers
var BROWSERS = {
    CHROME: 30,
    FIREFOX: 35,
    SAFARI: 6,
    IE: 12,
    'MICROSOFT EDGE': 13
}
Stats.prototype.GetBrowserCompatibility = function() {
    var isIE = false;
    var isModern = false;
    var browser_name = stats.GetBrowserInfo().name.toUpperCase();
    var browser_version = parseInt(stats.GetBrowserInfo().version);
    var os = stats.GetOSInfo().family.toUpperCase();
    if (browser_name === "IE" || browser_name === "MICROSOFT EDGE") {
        isIE = true;
    }

    if (browser_version >= BROWSERS[browser_name]) {
        isModern = true;
    }
    var compability;
    if (os.indexOf("OS X") >= 0) {
        compability = "mac+";
    } else {
        compability = "windows+";

        if (isIE) {
            compability += 'ie+';
        }
    }
    if (isModern) {
        compability += 'modern';
    } else {
        compability += 'old';
    }

    return compability;
}

Stats.prototype.GenerateHash = function(str) {
    // var loginToken = auth.getToken();
    // var hashToken = auth.getHashToken();
    // if(loginToken)  return loginToken;
    // if(hashToken)   return hashToken;
    var hashToken = calcMD5(str);
    // auth.setHashToken(hashToken);
    return hashToken;
}

Stats.prototype.isSameHash = function(hashStr) {
    var hashToken = auth.getHashToken();
    if (hashToken && (hashToken === hashStr)) {
        return true;
    }
    return false;
}

Stats.prototype.SendStats = function() {


    var that = this;
    that.statsIneterval = setInterval(function() {
        if (that.statsSent) {
            clearInterval(that.statsIneterval);
            return;
        }
        if (window.ewLocation) {

            var data = {
                browser: that.GetBrowserInfo(),
                os: that.GetOSInfo(),
                ip: that.GetIp(),
                location: that.GetLocation(),
            }

            var loginToken = auth.getToken() || "";

            var token = that.GenerateHash(JSON.stringify(data));
            var dataToSend;
            if (that.isSameHash(token)) {
                //Token is already Generated
                //So we will not send all details again
                dataToSend = {
                    stats: {
                        usr_id: that.usr_id,
                        browser: that.GetBrowserInfo(),
                        os: that.GetOSInfo(),
                        // ip : that.GetIp(),
                        flashInfo: that.GetFlashInfo(),
                        location: that.GetLocation(),
                        geo_loc_info: that.GetGeoLocationInfo(),
                        prev_pg: that.prev_pg,
                        curr_pg: that.GetCurrentPageInfo(),
                        resolution: that.GetResolution(),
                        tkn: token,
                        login_tkn: loginToken,
                        pg_vw_tm: new Date()
                    }
                }
            } else {
                //Token is not Generated
                //So we will send all details
                //Save token to storage
                auth.setHashToken(token);
                dataToSend = {
                    stats: {
                        usr_id: that.usr_id,
                        browser: that.GetBrowserInfo(),
                        os: that.GetOSInfo(),
                        // ip : that.GetIp(),
                        location: that.GetLocation(),
                        flashInfo: that.GetFlashInfo(),
                        geo_loc_info: that.GetGeoLocationInfo(),
                        prev_pg: that.prev_pg,
                        curr_pg: that.GetCurrentPageInfo(),
                        resolution: that.GetResolution(),
                        tkn: token,
                        login_tkn: loginToken,
                        pg_vw_tm: new Date()
                    }
                }
            }
            that.SendData(dataToSend.stats, 'userstats');
            that.statsSent = true;
        }
    }, 20);
}

Stats.prototype.SendData = function(data, topic) {
    // console.log("Sending Stats to kafka",new Date().getTime())
    $.ajax({
        url: '/userstats/',
        method: 'POST',
        data: {
            topic: topic,
            stats: JSON.stringify(data)
        }
    });
}



if (typeof angular === 'undefined') {
    var stats = new Stats();
    module.exports = stats;
} else {
    module.exports = Stats;
}