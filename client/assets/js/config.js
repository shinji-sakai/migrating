var Config = {};

Config.fallBack = {
    'windows+modern': ['mpeg', 'hls', 'html5', 'flash'],
    'windows+old': ['flash', 'html5'],
    'windows+ie+modern': ['mpeg', 'hls', 'html5', 'flash'],
    'windows+ie+old': ['html5', 'flash'],
    'mac+modern': ['hls', 'html5', 'flash'],
    'mac+old': ['html5', 'flash'],
}

Config.itemTypes = {
	"demo" : {
		type : "demo",
		color : "#87D37C",
		authenticate : false,
		requirePurchase : false,
	},
	"demologin" : {
		type : "demologin",
		color : "#4183D7",
		authenticate : true,
		requirePurchase : false,
	},
	"paid" : {
		type : "paid",
		color : "#D24D57",
		authenticate : true,
		requirePurchase : true,
	}
}

module.exports = Config;