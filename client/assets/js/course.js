function Course(){}

Course.prototype.addBookMark = function addBookMark(ele, userId, courseId, courseName, moduleId, moduleName, videoId, videoName) {
    if ($(ele).find("i").hasClass('fa-bookmark')) {
        // already  bookmarked
        return;
    }
    var data = {
        userId: userId,
        courseId: courseId,
        courseName: courseName,
        moduleId: moduleId,
        moduleName: moduleName,
        videoId: videoId,
        videoName: videoName
    }

    $.ajax({
        url: "/bookmarks/addBookmark",
        method: 'POST',
        data: data,
        dataType: 'json'
    })
        .done(function() {
            $(ele).find("i").removeClass('fa-bookmark-o').addClass('fa-bookmark');
            course.getBookMarks(courseId, userId);
        })
}

Course.prototype.getBookMarks = function getBookMarks(courseId, userId) {
    var data = {
        courseId: courseId,
        userId: userId
    }
    $.ajax({
        url: "/bookmarks/getBookmarks",
        method: 'POST',
        data: data,
        dataType: 'json'
    })
        .done(function(data) {
            if (data.length > 0) {
                var ul = $('#bookmarks');
                var items = [];
                data.forEach(function(val, index) {
                    var li = $('<li></li>');
                    var videoName = val.videoName;
                    if (videoName.length > 25) {
                        videoName = videoName.substr(0, 25) + '...';
                    }

                    // var url = "/#/video?course=" + val.courseId + "&module=" + val.moduleId + "&video=" + val.videoId + "&videoName=" + videoName.replace(' ','-') ;
                    var videoName_href = $('<span></span>').attr('title', val.videoName).attr('data-title', val.videoName + ' in bookmarks').html(videoName).css('cursor',"pointer");
                    $(videoName_href).on('click',function(){
                    	course.goToVideo(val.courseId,val.moduleId,val.videoId,val.videoName);
                    })
                    var delete_href = $('<a class="more-button delete-button">Delete</a>');
                    items[index] = $(li).html($(videoName_href)).append($(delete_href));
                    $(delete_href).on('click', function() {
                        course.deleteBookMarks(val.courseId, val.videoId, val.userId);
                    });
                });
                $(ul).html(items);
            } else {
                var ul = $('#bookmarks');
                var li = $("<li></li>");
                $(li).html('No Bookmarks to Show');
                $(ul).html($(li));
            }
        })
}

Course.prototype.deleteBookMarks = function deleteBookMarks(courseId, videoId, userId) {
    var data = {
        videoId: videoId,
        userId: userId
    }
    $.ajax({
        url: "/bookmarks/deleteBookmarks",
        method: 'POST',
        data: data,
        dataType: 'json'
    })
        .done(function() {
            course.getBookMarks(courseId, userId);
            //Remove ICON
            var id = "#" + videoId + "_bookmark";
            $(id).find("i").addClass('fa-bookmark-o').removeClass('fa-bookmark');
        })
}

Course.prototype.getAllCourseBookmarks = function getAllCourseBookmarks(courseId, userId) {
    var data = {
        courseId: courseId,
        userId: userId,
        fullbookmark: true
    }
    $.ajax({
        url: "/bookmarks/getBookmarks",
        method: 'POST',
        data: data,
        dataType: 'json'
    })
        .done(function(d) {
            d.forEach(function(val) {
                var id = "#" + val.videoId + "_bookmark";
                $(id).find("i").removeClass('fa-bookmark-o').addClass('fa-bookmark');
            })
            var arrBookmarkIcons = $("[id*=_bookmark]");
            arrBookmarkIcons.each(function(i,v){
                var userId = data.userId;
                var courseId = data.courseId;
                var courseName = $('.header-course-name').text().trim();
                var splitArr = $(v).attr('id').split("_");
                var moduleId = splitArr[0] + "_" + splitArr[1];
                var moduleName = $('#' + moduleId + "_wrapper").find(".module-name").attr("title");
                var videoId = moduleId + "_" + splitArr[2];
                var videoName = $('#' + videoId + "_wrapper").find(".video-name").attr("title");
                // console.log(userId , courseId , courseName , moduleId , moduleName , videoId , videoName);
                $(v).on('click',function(){
                    course.addBookMark(this,userId , courseId , courseName , moduleId , moduleName , videoId , videoName);
                })
            })
        })
}

Course.prototype.getAuthorizeItems = function getAuthorizeItems(id) {
    
    var data = {
        courseId: id,
    }
    $.ajax({
        url: "/ai/getItems",
        method: 'POST',
        data: data,
        dataType: 'json'
    })
    .done(function(data) {

        var items = data;
        vm.authorizedItemsMap = {};
        vm.configItemTypes = Config.itemTypes; 
        vm.isUserAuthenticated = auth.isAuthenticated();
        if(!vm.isUserAuthenticated || !vm.isCoursePurchased){
            $('.bookmark-icon').remove();
        }
        if(!vm.isUserAuthenticated){
            vm.authorizedModuleItems = items.map(function(v){
                //Reuire Login
                if(vm.configItemTypes[v.itemType].authenticate){
                    return v.moduleId;
                }
                return "";
            });
            vm.itemIds = items.map(function(v){
                if(vm.configItemTypes[v.itemType].authenticate){
                    vm.authorizedItemsMap[v.itemId] = v.itemType;    
                }
                return v;
            });
        }
        else if(vm.isUserAuthenticated && !vm.isCoursePurchased){
            vm.authorizedModuleItems = items.map(function(v){
                //Reuire Login And Purchase
                if(vm.configItemTypes[v.itemType].authenticate && vm.configItemTypes[v.itemType].requirePurchase){
                        return v.moduleId;
                    }
                return "";
            });
            vm.itemIds = items.map(function(v){
                if(vm.configItemTypes[v.itemType].authenticate && vm.configItemTypes[v.itemType].requirePurchase){
                    vm.authorizedItemsMap[v.itemId] = v.itemType;    
                }
                return v;
            });    
        }
        else if(vm.isUserAuthenticated && vm.isCoursePurchased){
            vm.authorizedModuleItems = [];
            vm.authorizedItemsMap = {};
        }

        vm.authorizedModuleItems= unique(vm.authorizedModuleItems);
        for(var k in vm.authorizedItemsMap){
            $('#'+k+'_wrapper .video-name').append("<sup><i class='fa fa-lock'></i></sup>");
        }
        vm.authorizedModuleItems.forEach(function(v,index){
            if(!vm.isUserAuthenticated){
                $("#"+v+"_wrapper .module-name").append("<sup>Needs Login</sup>")
            }
            if(vm.isUserAuthenticated && !vm.isCoursePurchased){
                $("#"+v+"_wrapper .module-name").append("<sup>Needs to Purchase</sup>")
            }
            
        })
        // console.log(vm.authorizedItemsMap , vm.authorizedModuleItems);
    })
}
function unique(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

Course.prototype.goToVideo = function goToVideo(courseId,moduleId,videoId,videoName){
    if(vm.authorizedItemsMap[videoId]){
        $('.course-container').addClass('blur-div');
        $('#errorModal').modal('show');
        $('#errorModal').on('hidden.bs.modal', function modalHidden(e) {
            $('.course-container').removeClass('blur-div');
        })
        return;
    }
    window.location = '/app/#/video?course=' + courseId + '&module='  + moduleId + '&video=' + videoId + "&videoName=" + videoName.replace(/ /g,'-');
}

Course.prototype.getUserCourses = function(courseId){

    if(!auth.isAuthenticated()){
        vm.isCoursePurchased = false;
        return jQuery.when();
    }

    return $.ajax({
        url: "/uc/getPurchasedCourses",
        method: 'POST',
        data: {userId : 's@g.c'},
        dataType: 'json'
    })
    .done(function(data){
        var arr = jQuery.grep(data[0].courses,function(v){
            return v.courseId === courseId;
        });
        if(arr.length > 0 && arr[0].courseId === courseId){
            vm.isCoursePurchased = true;
        }else{
            vm.isCoursePurchased = false;
        }
        console.log("before" , vm.isCoursePurchased);
    })
}

var course = new Course();
var vm = {};
$(document).ready(function() {
    var id = $(".header-course-name").attr("id");

    var videoWrapperArr = $("li[id*=_wrapper]");
    videoWrapperArr.each(function(i,v){
        var splitArr = $(v).attr('id').split("_");
        var courseId = splitArr[0];
        var moduleId = courseId + "_" + splitArr[1];
        var videoId = moduleId + "_" + splitArr[2];
        var ele = $(v).find('.video-name');
        $(ele).on('click',function(){
            course.goToVideo(courseId,moduleId,videoId,$(ele).attr('title'));
        })
    })

    course.getUserCourses(id)
        .then(function(){
            console.log("after" , vm.isCoursePurchased);
            course.getAuthorizeItems(id);    

                var ul = $('#bookmarks');
                var li = $("<li></li>");
                $(li).html('Purchase required');
                $(ul).html($(li));

            if(auth.isAuthenticated() && vm.isCoursePurchased){
                console.log(auth.getUser().userId);
                course.getBookMarks(id, auth.getUser().userId);
                course.getAllCourseBookmarks(id, auth.getUser().userId);
            }

        })
});
module.exports = course;