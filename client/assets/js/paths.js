var FILE = {

	//Controller

	'VideosController' : '/app/video/video.controller.js',
	'NotesController' : '/app/notes/notes.controller.js',
	'QAController' : '/app/qa/qa.controller.js',
	'TestController' : '/app/testPage/test.controller.js',

	'LoginController' : '/authApp/login/login.controller.js',
	'RegisterController' : '/authApp/register/register.controller.js',
	'ChangePasswordController' : '/authApp/changepassword/changepassword.controller.js',
	'ForgotPasswordController' : '/authApp/forgotpassword/forgotpassword.controller.js',

	'BookmarksController' : '/dashboard/bookmarks/bookmarks.controller.js',
	'DashboardController' : '/dashboard/dashboard/dashboard.controller.js',

	//Module
	'ServiceModule' : '/shared/services/serviceModule.js',
	'DirectiveModule' : '/shared/directive/directiveModule.js',

	//Service - Directives
	'auth' : '/shared/services/auth.js',
	'authToken' : '/shared/services/authtoken.js',
	'qaService' : '/shared/services/qa.js',
	'preloadImages' : '/shared/services/preloadImages.js',
	'fullscreen' : '/shared/services/fullscreen.service.js',
	'pageLoader' : '/shared/services/pageLoader.service.js',
	'admin' : '/shared/services/admin.js',
	'videoUrl' : '/shared/services/videoUrl.js',
	'videoNotes' : '/shared/services/videoNotes.js',
	'videoPlayer' : '/shared/services/videoplayer.service.js',
	'bookmark' : '/shared/services/bookmark.service.js',
	'infoDetection' : '/shared/services/infoDetection.service.js',
	'mail' : '/shared/services/mail.service.js',
	'UserCourses' : '/shared/services/usercourses.service.js',
	'post' : '/shared/services/post.service.js',

	//Directive
	'pageLoading' : '/shared/directive/pageloading.directive.js',
	'videoplayer_videojs' : '/shared/directive/videoControls.directive.js',
	'videoplayer_jw' : '/shared/directive/videoControls.jw.directive.js',
	'modal' : '/shared/directive/modal.directive.js',
	'Header' : '/shared/directive/header.directive.js',
	'Footer' : '/shared/directive/footer.directive.js',
	'error_image' : '/shared/directive/errImage.directive.js',


	//Angular 3rd Party
	'uiTree' : '/bower_components/angular-ui-tree/dist/angular-ui-tree.min.js',
	'uiTreeCSS' : '/bower_components/angular-ui-tree/dist/angular-ui-tree.min.css',
	'sanitize' : '/bower_components/angular-sanitize/angular-sanitize.min.js',
	'jwt' : '/bower_components/angular-jwt/dist/angular-jwt.min.js',
	'localStorage' : '/bower_components/angular-local-storage/dist/angular-local-storage.min.js',
	'tagsinputCSS' : '/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css',
	'tagsinputJS' : '/bower_components/jquery.tagsinput/src/jquery.tagsinput.js',
	
	//Scripts 3rd party
	'HE' : '/bower_components/he/he.js',

	'noUISlider' : '/assets/js/noUiSlider.8.2.1/nouislider.min.js',
	'noUISlider_css' : '/assets/js/noUiSlider.8.2.1/nouislider.min.css',

	'dash' : '/bower_components/dash.js/dist/dash.all.js',
	'videojs' : '/bower_components/video.js/dist/video.min.js',
	'videojs_css' : '/bower_components/video.js/dist/video-js.min.css',
	'videojs_dash' : '/bower_components/videojs-contrib-dash/src/js/videojs-dash.js',
	'videojs_dash_css' : '/bower_components/videojs-contrib-dash/src/css/videojs-dash.css',

	'videojs_hls' : '/assets/js/videojs-contrib-hls/dist/videojs.hls.min.js',

	'jwplayer' : 'http://static.aka.yupp.yuppcdn.net/staticstorage/jwplayer/jw_7_2_4/jwplayer.js', 
	'socket.io' : '/node_modules/socket.io-client/socket.io.js',
	'typed.js' : '/bower_components/typed.js/dist/typed.min.js'

}

module.exports = FILE;