angular.module('ephox.textboxio').factory('tbioConfigFactory', ['$log', function ($log) {
	//    $log.log('Loading tbioConfigFactory');
	var configurations = {};

	configurations.default = {
		// basePath: '../textboxio/resources',
		css: {
			stylesheets: [''],
			styles: [
				{
					rule: 'p',
					text: 'Paragraph'
					},
				{
					rule: 'h1',
					text: 'H1'
					},
				{
					rule: 'h2',
					text: 'H2'
					},
				{
					rule: 'h3',
					text: 'H3'
					},
				{
					rule: 'h4',
					text: 'H4'
					},
				{
					rule: 'div',
					text: 'div'
					},
				{
					rule: 'pre',
					text: 'pre'
					}
			]
		},
		codeview: {
			enabled: true,
			showButton: true
		},
		images: {
			upload : {
				url : '/ckeditor/uploadTextboxIOImage'
			},
			allowLocal: true,
			editing: {
                enabled: true,
                preferredWidth : 1024,
                preferredHeight : 1024
            }
		},
		languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
		// locale : '', // Default locale is inferred from client browser
		paste: {
			style: 'prompt'
		},
		// spelling : {},
		ui: {
			toolbar: {
				items: ['undo', 'insert', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
			}
		}
	};

	configurations.simple = {
		//        basePath: '/sites/all/libraries/textboxio/resources',
		css: {
			stylesheets: [''],
			styles: [
				{
					rule: 'p',
					text: 'block.p'
				},
				{
					rule: 'div',
					text: 'block.div'
				},
				{
					rule: 'h1',
					text: 'block.h1'
				},
				{
					rule: 'h2',
					text: 'block.h2'
				},
				{
					rule: 'h3',
					text: 'block.h3'
				},
				{
					rule: 'h4',
					text: 'block.h4'
				}
			]
		},
		codeview: {
			enabled: false,
			showButton: false
		},
		images: {
			// upload : {},
			allowLocal: true
		},
		languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
		// locale : '', // Default locale is inferred from client browser
		paste: {
			style: 'prompt'
		},
		// spelling : {},
		ui: {
			toolbar: {
				items: ['emphasis']
			}
		}
	};

	configurations.videonotes = {
		// basePath: '../textboxio/resources',
		css: {
			stylesheets: [''],
			styles: [
				{
					rule: 'p',
					text: 'Paragraph'
					},
				{
					rule: 'h1',
					text: 'H1'
					},
				{
					rule: 'h2',
					text: 'H2'
					},
				{
					rule: 'h3',
					text: 'H3'
					},
				{
					rule: 'h4',
					text: 'H4'
					},
				{
					rule: 'div',
					text: 'div'
					},
				{
					rule: 'pre',
					text: 'pre'
					}
			]
		},
		codeview: {
			enabled: false,
			showButton: false
		},
		images: {
			upload : {
				url : '/ckeditor/uploadTextboxIOImage'
			},
			allowLocal: true,
			editing: {
                enabled: true,
                preferredWidth : 1024,
                preferredHeight : 1024
            }
		},
		languages: ['en', 'es', 'fr', 'de', 'pt', 'zh'],
		// locale : '', // Default locale is inferred from client browser
		paste: {
			style: 'prompt'
		},
		// spelling : {},
		ui: {
			toolbar: {
				items: ['undo', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
			}
		}
	};

	return configurations;
}]);
