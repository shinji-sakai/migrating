var currentOffer = 0;


function prevCourseOffer(ele) {
    currentOffer -= 1;
    if (currentOffer < 0) {
        currentOffer = courseOffers.length - 1;
    }

    changeDom('prev',ele);
}

function nextCourseOffer(ele) {
    currentOffer += 1;
    if (currentOffer > (courseOffers.length - 1)) {
        currentOffer = 0;
    }
    changeDom('next',ele);
}

function changeDom(action,ele) {
    var title = $(ele).parent().find('.title');
    var innerCourses = $(ele).parent().parent().find('.inner-courses');
    
    //Change Title
    $(title).html(courseOffers[currentOffer].title);

    //Change Inner Courses
    var items = []; 
    courseOffers[currentOffer].innerCourses.forEach(function(value,index){
    	items[index] = $('<li></li>').html(value);
    })
    $(innerCourses).html(items);
}