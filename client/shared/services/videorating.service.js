angular.module('service').service('videoRating',VideoRating);

VideoRating.$inject = ['$http','BASE_API'];

function VideoRating($http,BASE_API) {
    this.saveVideoRating = function (data) {
        return $http.post(BASE_API + '/videorating/saveVideoRating', data);
    }

    this.updateVideoRating = function (data) {
        return $http.post(BASE_API + '/videorating/updateVideoRating', data);
    }

    this.getVideoRatingForUser = function (data) {
        return $http.post(BASE_API + '/videorating/getVideoRatingForUser', data);
    }
}