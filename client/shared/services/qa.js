angular.module('service').service('qaService', qaService);


qaService.$inject = ['$http','BASE_API'];

function qaService($http,BASE_API) {


    this.getQuestions = function(start,range) {
    	var data = {
    		start : start
    	};
        return $http.post(BASE_API + 'qa/question',data);
    }

    this.onlyQues = function(){
        return $http.get(BASE_API + 'qa/onlyQues');
    }

    this.saveQuestionStats = function(){
        return $http.get(BASE_API + '/practice/saveQuestionStats');
    }

    
    
}
