var io = require('socket.io_client');
require('authToken');
require('alertify.js/dist/js/alertify.js');
require('alertify.js/dist/js/ngAlertify.js');
require('alertify.js/dist/css/alertify.css');
angular
    .module('service')
    .service('websocket', WebSocket);

WebSocket.$inject = ['$log', '$http', 'BASE_API', 'alertify','authToken'];

function WebSocket($log, $http, BASE_API, alertify,authToken) {

    var vm = this;

    var host = window.location.hostname;
    vm.socket = io(BASE_API + '/');
    // if (host.indexOf("localhost") <= -1) {
    //     //If not localhost
    //     vm.socket = io('https://' + host + '/');
    // } else {
    //     //If localhost
    //     vm.socket = io('http://' + host + '/');
    // }

    vm.socket.on("connect", function() {
        if(authToken.isAuthenticated()){
            vm.socket.emit('userInfo', {
                socketId: vm.socket.id,
                user_id: authToken.getUserId(),
                display_name: authToken.getUser()["dsp_nm"]
            });    
        }
    })

    vm.socket.on("connect_error", function() {
        $log.info("Server is down.Failed to established socket")
    })

    vm.socket.on("reconnect_error", function() {
        $log.info("Server is down.Failed to established socket")
    })

    vm.socket.on("reconnecting", function() {
        $log.info("Reconnecting to server.....")
    })

    vm.socket.on('logoutMsg', function(data) {
        console.info("logoutMsg in websockets service", data);
        alertify.alert(data.msg, function() {
            window.location.href = "/login/session-expired"
        });
    });


    vm.sendMsgToSocket = function(msg, data) {
        vm.socket.emit(msg, data);
    }

    vm.joinRoomForClickStats = function(usr_id) {
        vm.socket.emit("joinRoomForClickStats", {
            usr_id: usr_id
        });
    }

    vm.leaveRoomForClickStats = function(usr_id) {
        vm.socket.emit("leaveRoomForClickStats", {
            usr_id: usr_id
        });
    }

    vm.leaveRedisVideoStatsChannelForUser = function(usr_id) {
        $log.debug("Unsubscribing  video-stats-" + usr_id + " redis channel.");
        vm.socket.emit("unsubscribe-redis-channel", "video-stats-" + usr_id);
    }

    vm.joinRedisVideoStatsChannelForUser = function(usr_id) {
        $log.debug("Subscribing  video-stats-" + usr_id + " redis channel.");
        vm.socket.emit("subscribe-redis-channel", "video-stats-" + usr_id);
    }

    vm.leaveRedisPracticeStatsChannelForUser = function(usr_id) {
        $log.debug("Unsubscribing  cnl_" + usr_id + " redis channel.");
        vm.socket.emit("unsubscribe-redis-channel", "cnl_" + usr_id);
    }

    vm.joinRedisPracticeStatsChannelForUser = function(usr_id) {
        $log.debug("Subscribing  cnl_" + usr_id + " redis channel.");
        vm.socket.emit("subscribe-redis-channel", "cnl_" + usr_id);
    }

    vm.leaveRedisClickStatsChannelForUser = function(usr_id) {
        $log.debug("Unsubscribing  click-stats-" + usr_id + " redis channel.");
        vm.socket.emit("unsubscribe-redis-channel", "click-stats-" + usr_id);
    }

    vm.joinRedisClickStatsChannelForUser = function(usr_id) {
        $log.debug("Subscribing  click-stats-" + usr_id + " redis channel.");
        vm.socket.emit("subscribe-redis-channel", "click-stats-" + usr_id);
    }

    vm.leaveRedisPageStatsChannelForUser = function(usr_id) {
        $log.debug("Unsubscribing  page-stats-" + usr_id + " redis channel.");
        vm.socket.emit("unsubscribe-redis-channel", "page-stats-" + usr_id);
    }

    vm.joinRedisPageStatsChannelForUser = function(usr_id) {
        $log.debug("Subscribing  page-stats-" + usr_id + " redis channel.");
        vm.socket.emit("subscribe-redis-channel", "page-stats-" + usr_id);
    }
}
