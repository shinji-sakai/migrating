angular.module('service').service('videoVotes',VideoVotes);

VideoVotes.$inject = ['$http','BASE_API'];

function VideoVotes($http,BASE_API) {
    this.upvoteVideo = function (data) {
        return $http.post(BASE_API + '/videovotes/upvoteVideo', data);
    }
    
    this.downvoteVideo = function (data) {
        return $http.post(BASE_API + '/videovotes/downvoteVideo', data);
    }

    this.getVideoVotes = function (data) {
        return $http.post(BASE_API + '/videovotes/getVideoVotes', data);
    }

    
}