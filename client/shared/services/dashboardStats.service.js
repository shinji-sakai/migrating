angular.module('service').service('dashboardStatsService',DashboardStats);

DashboardStats.$inject = ['$http','BASE_API'];

function DashboardStats($http,BASE_API) {

    this.getAllDashboardStatsOfUser = function (data) {
        return $http.post(BASE_API + '/dashboard/stats/getAllDashboardStatsOfUser',data);
    }
    
    this.incrementQuestionLearnedViews = function (data) {
    	return $http.post(BASE_API + '/dashboard/stats/incrementQuestionLearnedViews',data);
    }

    this.incrementTestTakenCounter = function(data){
        return $http.post(BASE_API + '/dashboard/stats/incrementTestTakenCounter',data);
    }

    this.incrementNoOfQuestionPracticedCounter = function(data){
        return $http.post(BASE_API + '/dashboard/stats/incrementNoOfQuestionPracticedCounter',data);
    }

    this.incrementNoOfQuestionAttemptedInTestCounter = function(data){
        return $http.post(BASE_API + '/dashboard/stats/incrementNoOfQuestionAttemptedInTestCounter',data);
    }

    this.updateNoOfCorrectQuestionAttemptedCounter = function(data){
        return $http.post(BASE_API + '/dashboard/stats/updateNoOfCorrectQuestionAttemptedCounter',data);
    }

    this.updateNoOfWrongQuestionAttemptedCounter = function(data){
        return $http.post(BASE_API + '/dashboard/stats/updateNoOfWrongQuestionAttemptedCounter',data);
    }

}