require('authToken');
require('highlight_css');

angular.module("service")
    .service('utility', Utility);


Utility.$inject = ['authToken','$sce','$http','BASE_API'];

function Utility(authToken,$sce,$http,BASE_API) {

    var self = this;

    self.BASE_URL = BASE_API;
    self.QA_QUESTION_ID_URL_LENGTH = 300;

    self.goToLogin = goToLogin;
    self.getLoginUrl = getLoginUrl;
    self.goToLogout = goToLogout;
    self.getLogoutUrl = getLogoutUrl;
    self.goToRegister = goToRegister;
    self.getRegisterUrl = getRegisterUrl;
    self.goToChangePassword = goToChangePassword;
    self.goToForgotPassword = goToForgotPassword;
    self.getForgotPasswordUrl = getForgotPasswordUrl;
    self.goToAdminApp = goToAdminApp;
    self.goToDashboardApp = goToDashboardApp;
    self.goToPracticePage = goToPracticePage;

    self.goToCareerBook = goToCareerBook;
    self.getCareerBookUrl = getCareerBookUrl;

    self.goToQA = goToQA;
    self.getQAUrl = getQAUrl;
    self.getQAQuestionPageUrl = getQAQuestionPageUrl;
    self.getQAAnswerPageUrl = getQAAnswerPageUrl;

    self.goToWhiteboard = goToWhiteboard;
    self.getWhiteboardUrl = getWhiteboardUrl;

    self.goToDashboard = goToDashboard;
    self.getDashboardUrl = getDashboardUrl;

    self.goToBooks = goToBooks;
    self.getBooksUrl = getBooksUrl;

    self.getUserProfileUrl = getUserProfileUrl;
    self.goToUserProfile = goToUserProfile;

    self.getSitemapUrl = getSitemapUrl;

    self.getITTrainingsUrl = getITTrainingsUrl;

    self.getNotificationsPageUrl = getNotificationsPageUrl;
    self.getSpecificNotificationPageUrl = getSpecificNotificationPageUrl;

    self.doSyntaxHighlighting = doSyntaxHighlighting;
    self.getLanguageForSyntaxHighlighting = getLanguageForSyntaxHighlighting;
    self.changeTimeFormat = changeTimeFormat;
    self.trustAsHTML = trustAsHTML;
    self.trustAsHTMLReturnPlainText = trustAsHTMLReturnPlainText;
    self.shareToEWProfile = shareToEWProfile;trustAsHTMLReturnPlainText;
    self.shareToQA = shareToQA;
    self.openPopUp = openPopUp;

    self.buyPage = buyPage;
    self.buyCoursePage = buyCoursePage;
    self.getBuyCoursePageURL = getBuyCoursePageURL;
    self.buyPaymentPage = buyPaymentPage;
    self.buyCoursePricePage = buyCoursePricePage;
    self.getCoursePricePageURL = getCoursePricePageURL;

    self.getVideoPageURL = getVideoPageURL;
    self.getQuestionPageURL = getQuestionPageURL;



    self.getTrainingPageUrl = getTrainingPageUrl;

    self.getNextTrainingDay = getNextTrainingDay;
    self.getCourseDetailsPageUrl = getCourseDetailsPageUrl;

    self.getAdminPracticePageUrl = getAdminPracticePageUrl;

    self.convertTimeTo_AM_PM = convertTimeTo_AM_PM;



    self.submitQuestionStats = function(data){
        return $http.post(BASE_API + '/userstats', data);
    }

    function goToLogin(){
        authToken.cachedUrl(window.location.href);
        window.location = '/login';
    }

    function getLoginUrl(){
        return '/login';
    }

    function goToRegister(){
        authToken.cachedUrl(window.location.href);
        window.location = '/register';
    }

    function getRegisterUrl(){
        return '/register';
    }

    function goToLogout(){
        window.location = '/logout';
    }

    function  getLogoutUrl() {
        return '/logout';
    }

    function goToChangePassword(code) {
        if(code){
            window.location = '/changepassword/' + code;
        }else{
            window.location = '/changepassword/';
        }
    }

    function goToForgotPassword() {
        window.location = '/forgotpassword';
    }

    function getForgotPasswordUrl(){
        return '/forgotpassword';
    }


    function goToAdminApp() {
        window.location = '/adminApp/';
    }

    function goToDashboardApp() {
        window.location = '/dashboard/';
    }


    function buyPage() {
        return self.BASE_URL + '/buy/'
    }
    function buyCoursePricePage(crs_id) {
        return self.BASE_URL + '/buy/price/' + crs_id;
    }
    function getCoursePricePageURL(shouldRedirect,crs_id) {
        var url = self.BASE_URL + '/buy/price/' + crs_id;
        if(shouldRedirect){
            window.location = url;
        }
        return url;
    }
    function buyCoursePage(crs_id) {
        return self.BASE_URL + '/buy/course/' + crs_id;
    }
    function getBuyCoursePageURL(shouldRedirect,crs_id) {
        var url = self.BASE_URL + '/buy/course/' + crs_id;;
        if(shouldRedirect){
            window.location = url;
        }
        return url;
    }
    function buyPaymentPage(crs_id,bat_id) {
        return self.BASE_URL + '/buy/payment?course=' + crs_id + '&bat_id=' + bat_id ;
    }

    function goToPracticePage(courseId,moduleId,itemId){
        var url = "/" + courseId + "/video/" + itemId;
        // var url = "/app/practice?module="+ moduleId + "&course=" + courseId + "&item=" + itemId;
        // window.location.href = url;
        var win = window.open(url, '_blank');
        win.focus();
    }

    /////////////////////

    function goToQA(){
        window.location = '/question-answers';
    }
    function getQAUrl() {
        return '/question-answers';
    }

    function getQAQuestionPageUrl(shouldRedirect,que_id,que_text){
        var url = '/question-answers/question/' + que_id + '/' + (que_text ? que_text : '');
        if(shouldRedirect){
            window.location = url;
        }
        return url;
    }

    function getQAAnswerPageUrl(shouldRedirect,que_id,ans_id,que_text){
        var url = '/question-answers/question/' + que_id + "/" + (que_text || "text") +  "/answer/" + ans_id;
        if(shouldRedirect){
            window.location = url;
        }
        return url;
    }

    /////////////////////

    function goToCareerBook(){
        window.location = '/career-book/';
    }
    function getCareerBookUrl(){
        return '/career-book/';
    }

    self.getCareerBookProfileUrl = function(usr_id){
        return '/career-book/' + usr_id;
    }

    /////////////////////

    function goToWhiteboard(){
        window.location = '/whiteboard/';
    }
    function getWhiteboardUrl(){
        return '/whiteboard/';
    }

    /////////////////////

    function goToBooks(){
        window.location = '/books/';
    }
    function getBooksUrl(){
        return '/books/';
    }

    /////////////////////


    function goToDashboard(){
        window.location = '/dashboard/';
    }
    function getDashboardUrl(){
        return '/dashboard/';
    }

    /////////////////////


    function getUserProfileUrl(){
        return '/user-profile';
    }
    function goToUserProfile(){
        window.location = '/user-profile';
    }

    /////////////////////


    function getSitemapUrl(shouldRedirect){
        var url = '/sitemap';
        if(shouldRedirect){
            window.location = url;
        }
        return url;
    }

    /////////////////////

    function getVideoPageURL(courseId,itemId,time){
        if(time){
            return self.BASE_URL + '/' + courseId + '/video/' + itemId + '?time=' + time;
        }else{
            return self.BASE_URL + '/' + courseId + '/video/' + itemId;
        }
    }

    function getQuestionPageURL(courseId,itemId,question){
        if(question){
            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/' + question;
        }else{
            return self.BASE_URL + '/' + courseId + '/questions/' + itemId + '/';
        }
    }

    self.getBookPageURL = getBookPageURL;
    function getBookPageURL(courseId,itemId){
        return self.BASE_URL + '/' + courseId + '/book-topic/' + itemId;
    }

    function getITTrainingsUrl(shouldRedirect){
        var url = "/it-trainings";
        if(shouldRedirect){
            window.location = url;
        }else{
            return url;
        }
    }

    function getCourseDetailsPageUrl(shouldRedirect,type,subgroup,crs_id){
        // var url = `${type}/${subgroup}-trainings/${crs_id}`;
        var url = "/" + type + "/" + subgroup + "-trainings/" + crs_id;
        if(shouldRedirect){
            window.location = url;
        }else{
            return url;
        }
    }

    function getNotificationsPageUrl(shouldRedirect,usr_id){
        var url = "/notifications";
        if(shouldRedirect){
            window.location = url;
        }else{
            return url;
        }
    }

    function getSpecificNotificationPageUrl(shouldRedirect,usr_id,notification_id){
        var url = "/notifications/" + notification_id;
        if(shouldRedirect){
            window.location = url;
        }else{
            return url;
        }
    }

    function getTrainingPageUrl(shouldRedirect,crs_id){
        var url = "/training/" + crs_id;
        if(shouldRedirect){
            window.location = url;
        }else{
            return url;
        }
    }

    function getAdminPracticePageUrl(shouldRedirect,q_id){
        var url = "/adminApp/practice/" + q_id;
        if(shouldRedirect){
            window.location = url;
        }else{
            return url;
        }
    }

    //do highlighting
    function doSyntaxHighlighting() {
        var x = 0;
        var intervalID = setInterval(function() {
            // Prism.highlightAll();
            $('pre code').each(function(i, block) {
                hljs.highlightBlock(block);
            });
            $('.syntax').each(function(i, block) {
                hljs.highlightBlock(block);
            });
            if (++x === 10) {
                window.clearInterval(intervalID);
            }
        }, 1);
    }

    //Get language for syntax highlighting
    function getLanguageForSyntaxHighlighting(coursename){
        if(coursename.toLowerCase().indexOf("javascript") > -1){
            return "javascript";
        }else if(coursename.toLowerCase().indexOf("java") > -1){
            return "java";
        }else if(coursename.toLowerCase().indexOf("sql") > -1){
            return "sql";
        }
    }

    var vm = this;
    function getNextTrainingDay(frm_wk_dy,to_wk_dy,cls_start_dt,bat_frm_tm_hr,bat_frm_tm_min) {
        vm.daysOfWeek = ["Monday", 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

        var training_days = [];
        var start_counting = false;
        for (var i = 0; i < vm.daysOfWeek.length; i++) {
            var d = vm.daysOfWeek[i];
            if (start_counting) {
                training_days.push(d);
                if (d === to_wk_dy) {
                    start_counting = false;
                    break;
                }
            }
            if (d === frm_wk_dy) {
                start_counting = true;
                training_days.push(d);
            }
        }

        var today_name = getDayName();
        var today_date = new Date();
        var next_training_date = "";
        bat_frm_tm_hr = parseInt(bat_frm_tm_hr);
        bat_frm_tm_min = parseInt(bat_frm_tm_min);
        if(today_date < new Date(cls_start_dt)){
            next_training_date = new Date(cls_start_dt);
            // console.log("next date is start date.")
        }else if (training_days.indexOf(today_name) > -1) {
            //check if today is in training_days
            var current_hour = today_date.getHours();
            var current_min = today_date.getMinutes();
            // console.log(current_hour,bat_frm_tm_hr,current_min,bat_frm_tm_min)
            if (current_hour > bat_frm_tm_hr) {
                // console.log("time is exceeded , find next day");
                var daysToGo = nextTrainingDay(training_days)
                // console.log("daysToGo", daysToGo);
                var today = new Date();
                next_training_date = new Date(today);
                next_training_date.setDate(today.getDate() + daysToGo);
            }else if(current_hour === bat_frm_tm_hr && current_min > bat_frm_tm_min) {
                // console.log("time is exceeded , find next day");
                var daysToGo = nextTrainingDay(training_days)
                // console.log("daysToGo", daysToGo);
                var today = new Date();
                next_training_date = new Date(today);
                next_training_date.setDate(today.getDate() + daysToGo);
            }else {
                // console.log("today is the next batch day");
                next_training_date = new Date();
            }
        } else {
            //if today is not in training_days
            // console.log("today is not training_days , find next day");
            var daysToGo = nextTrainingDay(training_days);
            // console.log("daysToGo", daysToGo);
            var today = new Date();
            next_training_date = new Date(today);
            next_training_date.setDate(today.getDate() + daysToGo);
        }
        return next_training_date;
    }

    function getDayName(dateString) {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        if (!dateString) {
            dateString = (new Date()).toString();
        }
        var d = new Date(dateString);
        var dayName = days[d.getDay()];
        return dayName;
    }

    function nextTrainingDay(training_days) {
        var today_name = getDayName();
        var today_day_index = vm.daysOfWeek.indexOf(today_name);
        var day_index = today_day_index + 1;
        var daysToGo = 1;
        var nextDayFound = false;
        while (!nextDayFound) {
            var day = vm.daysOfWeek[day_index];
            if (training_days.indexOf(day) > -1) {
                nextDayFound = true;
            } else {
                day_index = (day_index + 1) % 7;
                daysToGo++;
                nextDayFound = false;
            }
        }
        return daysToGo;
    }

    function changeTimeFormat(sec) {
        var time = sec || 0;
        var hours = parseInt(time / 3600) % 24;
        var minutes = parseInt(time / 60) % 60;
        var seconds = parseInt(time % 60);

        hours = ("0" + hours).slice(-2);
        minutes = ("0" + minutes).slice(-2);
        seconds = ("0" + seconds).slice(-2);

        var ret_time;
        if (hours == "00") {
            ret_time = minutes + ":" + seconds;
        } else {
            ret_time = hours + ":" + minutes + ":" + seconds;
        }

        return ret_time;
    }

    function convertTimeTo_AM_PM(time) {
        // time = 10:10
        time = time || "00:00";
        var split_time = time.split(":");
        var hour = parseInt(split_time[0]);
        if(hour < 12){
            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " AM";
        }else if(hour === 12){
            return ('00' + split_time[0]).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
        }else if(hour > 12){
            return ('00' + (split_time[0] - 12)).slice(-2) + ":" + ('00' + split_time[1]).slice(-2) + " PM";
        }
    }

    function trustAsHTML(html){
        return $sce.trustAsHtml(html);
    }

    function trustAsHTMLReturnPlainText(html){
        var html2 = html.replace(/(<([^>]+)>)/g, "");
        return $sce.trustAsHtml(html2);
    }

    function shareToEWProfile(title,desc,url){
        if(!url){
            url = encodeURIComponent(window.location.href);
        }else{
            url = encodeURIComponent(url);
        }
        var w = 500;
        var h = 560;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;

        var popup_url = window.location.origin + '/app/share?type=cb&title=' + title + "&url=" + url + "&desc=" + (desc || '');
        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
    }

    function shareToQA(title,desc,url){
        if(!url){
            url = encodeURIComponent(window.location.href);
        }else{
            url = encodeURIComponent(url);
        }
        var w = 500;
        var h = 560;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;

        var popup_url = window.location.origin + '/app/share?type=qa&title=' + title + "&url=" + url + "&desc=" + (desc || '');
        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
    }

    function openPopUp(url){
        var w = 500;
        var h = 500;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;

        var popup_url = url;
        window.open(popup_url,"_blank", "height="+h+",width="+w+",top="+top+",left="+left);
    }

    self.getSelectionText = getSelectionText;
    function getSelectionText() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        return text;
    }

    self.getPlainQuestionIdText = function(text){
        var text = $("<div></div>").html(text).text().split(" ").join("-").toLowerCase();
        text = text.substring(0,self.QA_QUESTION_ID_URL_LENGTH);
        text = text.replace(/[^a-zA-Z0-9-]/g, "")
        return text
    }
};
