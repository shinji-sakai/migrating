angular
	.module('service')
	.service('Mail',Mail);

Mail.$inject = ['$http','BASE_API'];

function Mail($http,BASE_API) {
    this.sendMail = function (data) {
        return $http.post(BASE_API + '/mail/send', data).then(function (res) {
            return res.data;
        });
    }

    this.sendBulkEmails = function (data) {
        return $http.post(BASE_API + '/mail/sendBulkEmails', data);
    }

    this.subscribeUser = function (data) {
        return $http.post(BASE_API + '/mail/subscribeUser', data);
    }

    this.sendMailToSupportForEnrollBtn = function(data){
        return $http.post(BASE_API + '/mail/sendMailToSupportForEnrollBtn', data);   
    }

    this.sendMailToSupportForPaySecurelyBtn = function(data){
        return $http.post(BASE_API + '/mail/sendMailToSupportForPaySecurelyBtn', data);   
    }
}
