require('serviceModule');
require('ng-file-upload');

angular.module('service').service('profile',Profile);
Profile.$inject = ['$http','Upload','BASE_API'];
function Profile($http,Upload,BASE_API) {
	var self = this;

	self.saveProfilePic = function(data){
		return Upload.upload({
            url: BASE_API + '/dashboard/profile/saveProfilePic',
            mehtod: 'POST',
            data: data
        });
	}

	self.getUserFriends = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getUserFriendsAndFollowings',data);
	}

	self.getAllUsersShortDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getAllUsersShortDetails',data);
	}

	self.getUserShortDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getUserShortDetails',data);
	}

	self.getRecommendFriendForUser = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getRecommendFriendForUser',data);
	}

	self.sendFriendRequest = function(data){
		return $http.post(BASE_API + '/dashboard/profile/sendFriendRequest',data);	
	}

	self.acceptFriendRequest = function(data){
		return $http.post(BASE_API + '/dashboard/profile/acceptFriendRequest',data);	
	}

	self.deleteFriendRequest = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteFriendRequest',data);	
	}

	// unfriendFriend
	self.unfriendFriend = function(data){
		return $http.post(BASE_API + '/dashboard/profile/unfriendFriend',data);	
	}

	self.cancelFriendRequest = function(data){
		return $http.post(BASE_API + '/dashboard/profile/cancelFriendRequest',data);	
	}

	self.blockPerson = function(data){
		return $http.post(BASE_API + '/dashboard/profile/blockPerson',data);	
	}

	self.getFriendRequest = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getFriendRequest',data);	
	}

	self.getFriendStatus = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getFriendStatus',data);		
	}

	// Follower
	self.addFollower = function(data){
		return $http.post(BASE_API + '/dashboard/profile/addFollower',data);		
	}	
	self.getFollowers = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getFollowers',data);		
	}	
	self.removeFollower = function(data){
		return $http.post(BASE_API + '/dashboard/profile/removeFollower',data);		
	}

	//user personal details
	self.getUserPersonalDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getUserPersonalDetails',data);		
	}
	self.saveUserBasicPersonalDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveUserBasicPersonalDetails',data);		
	}

	self.getUserEmails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getUserEmails',data);		
	}

	self.getUserPhoneNumbers = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getUserPhoneNumbers',data);		
	}

	self.addUserEmail = function(data){
		return $http.post(BASE_API + '/dashboard/profile/addUserEmail',data);		
	}

	self.addUserPhoneNumber = function(data){
		return $http.post(BASE_API + '/dashboard/profile/addUserPhoneNumber',data);		
	}

	self.verifyUserEmail = function(data){
		return $http.post(BASE_API + '/dashboard/profile/verifyUserEmail',data);		
	}

	self.verifyUserPhoneNumber = function(data){
		return $http.post(BASE_API + '/dashboard/profile/verifyUserPhoneNumber',data);		
	}

	self.deleteUserEmail = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteUserEmail',data);		
	}

	self.deleteUserPhoneNumber = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
	}
	
	// self.saveUserPhoneDetails = function(data){
	// 	return $http.post(BASE_API + '/dashboard/profile/saveUserPhoneDetails',data);		
	// }
	// self.saveUserEmailDetails = function(data){
	// 	return $http.post(BASE_API + '/dashboard/profile/saveUserEmailDetails',data);		
	// }
	// startVerifingEmailAddress
	self.startVerifingEmailAddress = function(data){
		return $http.post(BASE_API + '/dashboard/profile/startVerifingEmailAddress',data);		
	}
	// startVerifingPhoneNumber
	self.startVerifingPhoneNumber = function(data){
		return $http.post(BASE_API + '/dashboard/profile/startVerifingPhoneNumber',data);		
	}

	// // confirmVerificationOfEmail
	// self.confirmVerificationOfEmail = function(data){
	// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfEmail',data);		
	// }
	// // confirmVerificationOfPhoneNumber
	// self.confirmVerificationOfPhoneNumber = function(data){
	// 	return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfPhoneNumber',data);		
	// }

	// //deleteUserPhoneNumber
	// self.deleteUserPhoneNumber = function(data){
	// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserPhoneNumber',data);		
	// }

	// // deleteUserEmailAddress
	// self.deleteUserEmailAddress = function(data){
	// 	return $http.post(BASE_API + '/dashboard/profile/deleteUserEmailAddress',data);		
	// }

	// getUserContactDetails
	self.getUserContactDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getUserContactDetails',data);		
	}
	// addNonVerifiedEmailInContactDetails
	self.addNonVerifiedEmailInContactDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/addNonVerifiedEmailInContactDetails',data);		
	}

	// confirmVerificationOfContactEmail
	self.confirmVerificationOfContactEmail = function(data){
		return $http.post(BASE_API + '/dashboard/profile/confirmVerificationOfContactEmail',data);		
	}

	// deleteEmailInContactDetails
	self.deleteEmailInContactDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteEmailInContactDetails',data);		
	}

	// changePrimaryEmailInContactDetails
	self.changePrimaryEmailInContactDetails = function(data){
		return $http.post(BASE_API + '/dashboard/profile/changePrimaryEmailInContactDetails',data);		
	}	


	// Work Form Helper
	self.saveWorkInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveWorkInfo',data);		
	}	
	self.getWorkInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getWorkInfo',data);		
	}
	self.deleteWorkInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteWorkInfo',data);		
	}

	// Edu Form Helper
	self.saveEduInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveEduInfo',data);		
	}	
	self.getEduInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getEduInfo',data);		
	}
	self.deleteEduInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteEduInfo',data);		
	}

	// Exam Form Helper
	self.saveExamInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveExamInfo',data);		
	}	
	self.getExamInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getExamInfo',data);		
	}
	self.deleteExamInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteExamInfo',data);		
	}

	// Interested Course Form Helper
	self.saveInterestedCourseInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveInterestedCourseInfo',data);		
	}	
	self.getInterestedCourseInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getInterestedCourseInfo',data);		
	}
	self.deleteInterestedCourseInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteInterestedCourseInfo',data);		
	}

	// Author Form Helper
	self.saveAuthorInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveAuthorInfo',data);		
	}	
	self.getAuthorInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getAuthorInfo',data);		
	}
	self.deleteAuthorInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteAuthorInfo',data);		
	}
	self.saveAuthorDescInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveAuthorDescInfo',data);		
	}

	// Teacher info Form Helper
	self.saveTeacherInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveTeacherInfo',data);		
	}	
	self.getTeacherInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getTeacherInfo',data);		
	}
	self.deleteTeacherInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteTeacherInfo',data);		
	}
	self.saveTeacherDescInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveTeacherDescInfo',data);		
	}

	// Kid info Form Helper
	self.saveKidInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/saveKidInfo',data);		
	}	
	self.getKidInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getKidInfo',data);		
	}
	self.deleteKidInfo = function(data){
		return $http.post(BASE_API + '/dashboard/profile/deleteKidInfo',data);		
	}


	// Friend request notification api
	// getFriendRequestNotifications
	self.getFriendRequestNotifications = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getFriendRequestNotifications',data);		
	}
	// updateFriendRequestNotifications
	self.updateFriendRequestNotifications = function(data){
		return $http.post(BASE_API + '/dashboard/profile/updateFriendRequestNotifications',data);		
	}

	// getTaggedFriendsNotifications
	self.getTaggedFriendsNotifications = function(data){
		return $http.post(BASE_API + '/dashboard/profile/getTaggedFriendsNotifications',data);		
	}
	// updateTaggedFriendNotifications
	self.updateTaggedFriendNotifications = function(data){
		return $http.post(BASE_API + '/dashboard/profile/updateTaggedFriendNotifications',data);		
	}

	self.getFriendsListForMessage = function(data){
		return $http.post(BASE_API + '/chat/friends/find',data);		
	}
}