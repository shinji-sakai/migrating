'use strict';
angular.module("service")
    .service('preloadImage', preloadImage);
preloadImage.$inject = [];
function preloadImage() {
    this.loadImages = loadImages;
    function loadImages(imgArr) {
        var imageArr = [].concat(imgArr);
        if (!this.list) {
            this.list = [];
        }
        var list = this.list;
        for (var i = 0; i < imageArr.length; i++) {
            var img = new Image();
            img.onload = function() {
                var index = list.indexOf(this);
                if (index !== -1) {
                    // remove image from the array once it's loaded
                    // for memory consumption reasons
                    list.splice(index, 1);
                }
                console.log("load completed");
            }
            list.push(img);
            img.src = imageArr[i];
        }
    }
}