angular.module('service').service('blog',Blog);

Blog.$inject = ['$http','BASE_API'];

function Blog($http,BASE_API) {
    this.deleteBlogNote = function (data) {
        return $http.post(BASE_API + '/blognotes/deleteBlogNote', data);
    }

    this.updateBlogNote = function (data) {
        return $http.post(BASE_API + '/blognotes/updateBlogNote', data);
    }

    this.insertBlogNotes = function (data) {
        return $http.post(BASE_API + '/blognotes/insertBlogNote', data);
    }
}