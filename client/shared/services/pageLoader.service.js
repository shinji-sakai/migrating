angular
	.module('service')
	.service('pageLoader',PageLoader);

PageLoader.$inject = ['$compile','$rootScope'];	
function PageLoader($compile,$rootScope){	


	this.show = function(){
		var el = $compile("<page-loading></page-loading>")($rootScope);
		// $("body").css("overflow","hidden");
		$("body>div").append(el);
	}

	this.hide = function(){
		// $("body").css("overflow","auto");
		$(".page-spinner").remove();
	}

}