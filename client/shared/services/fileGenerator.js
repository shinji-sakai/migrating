var jsPDF = require('jspdf');
var HtmlDocx = require('html_docx');
var f = require('file-saver');

angular.module('service').service('fileGenerator', FileGenerator);
FileGenerator.$inject = [];

function FileGenerator() {

    this.generatePDF = generatePDF;
    this.generateWord = generateWord;

    function generatePDF(id) {
        var doc = new jsPDF('p', 'pt', 'a4');
        var elementHandler = {
            '#ignorePDF': function(element, renderer) {
                return true;
            }
        };
        var source = window.document.getElementById(id);
        return html2canvas($("#" + id), {
            background: '#fff'
        })
            .then(function(canvas) {
                // var a = document.createElement('a');
                // a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
                // a.download = 'somefilename'+Math.random()+'.jpeg';
                // a.click();
                var imgData = canvas.toDataURL('image/jpeg');
                var imgWidth = 210;
                var pageHeight = 842;
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;
                var position = 0;

                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }
                doc.save('save.pdf');
                return "";
            })
    }

    function generateWord(id) {
        var style = "<style>img{max-width: 100%}</style>";
        var source = $('#' + id).html();
        source = "<html><head>" + style + "</head><body>" + source + "</body></html>"
        var docx = HtmlDocx.asBlob(source);
        console.log(docx);
        saveAs(docx, 'test.docx');
    }
}