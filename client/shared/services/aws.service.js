require('ng-file-upload');
angular
.module('service')
.service('awsService', awsService);

awsService.$inject = ['$http','Upload','BASE_API'];

function awsService($http,Upload,BASE_API){

	this.uploadFileToS3 = function(data){
		return Upload.upload({
            url: BASE_API + '/aws/s3/uploadFile',
            data: data,
            method: 'POST'
        });
	}

	this.getS3SignedUrl = function(data){
		return $http.post(BASE_API + '/aws/s3/getSignedUrl',data);
	}
}