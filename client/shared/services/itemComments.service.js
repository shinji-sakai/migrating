require('ng-file-upload');
angular
.module('service')
.service('itemComments', Service);

Service.$inject = ['$http','Upload','BASE_API'];

function Service($http,Upload,BASE_API){

	this.saveComment = function(data){

		return Upload.upload({
            url: BASE_API + '/itemComments/save',
            data: data,
            method: 'POST'
        });

        // return $http.post(BASE_API + '/itemComments/save',data);
    }

    this.editComment = function(data) {
        return Upload.upload({
            url: BASE_API + '/itemComments/edit',
            data: data,
            method: 'POST'
        });
    }

    this.deleteComment = function(data){
        return $http.post(BASE_API + '/itemComments/delete',data);
    }

    this.likeDislikeComment = function(data) {
        return $http.post(BASE_API + '/itemComments/like_dislike',data);
    }
    // 
    this.getLikeDislikeComments = function(data) {
        return $http.post(BASE_API + '/itemComments/getLikeDislikeComments',data);
    }

    this.getCommentLikeDislikeCount = function(data) {
        return $http.post(BASE_API + '/itemComments/getCommentLikeDislikeCount',data);   
    }

    this.updateCommentLikeDislikeCount = function(data) {
        return $http.post(BASE_API + '/itemComments/updateCommentLikeDislikeCount',data);   
    }
    
    this.getPostComments = function(data){
        return $http.post(BASE_API + '/itemComments/get',data);
    }

    // 
    this.getCommentUsers = function(data){
        return $http.post(BASE_API + '/itemComments/getCommentUsers',data);
    }




    
    
}
