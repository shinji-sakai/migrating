require('ng-file-upload');
angular
.module('service')
.service('uploadFileAPI', UploadFileAPI);

UploadFileAPI.$inject = ['$http','Upload','BASE_API'];

function UploadFileAPI($http,Upload,BASE_API){

	this.uploadVideoEntity = function(data){
		return Upload.upload({
            url: BASE_API + '/videoentity/uploadVideoEntity',
            data: data,
            method: 'POST'
        });
	}

	this.uploadUserMaterial = function(data){
		return Upload.upload({
            url: BASE_API + '/moduleItems/uploadUserMaterial',
            data: data,
            method: 'POST'
        });
	}
}