angular
	.module('service')
	.service('fullScreen',FullScreen);

FullScreen.$inject = [];	
function FullScreen(){	

	var vm = this;
	vm.launchFullScreen = function(element){
		vm.fullscreenElement = element;
        var fullscreenchange;
        if (element.requestFullscreen) {
            element.requestFullscreen();
            fullscreenchange = "fullscreenchange";
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
            fullscreenchange = "mozfullscreenchange";
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
            fullscreenchange = "webkitfullscreenchange";
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
            fullscreenchange = "msfullscreenchange";
        }
        vm.fullscreenElement.addEventListener(fullscreenchange, function(e) {
            var fullscreenEnabled = document.isFullScreen || document.mozFullScreen || document.webkitIsFullScreen;
            console.log("triggering on element...",vm.fullscreenElement);
            $(vm.fullscreenElement).trigger('fullscreenchange',[fullscreenEnabled]);
            // if (fullscreenEnabled) {
            //     vm.isFullScreen = true;
            // } else {
            //     vm.isFullScreen = false;
            // }
        });
	}

	vm.exitFullScreen = function(){
        console.log("doing exitFullScreen")
		if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
	}

}