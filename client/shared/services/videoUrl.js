angular.module('service').service('videoUrl',VideoUrl);

VideoUrl.$inject = ['$http','BASE_API'];

function VideoUrl($http,BASE_API) {
    this.fetchVideoUrl = function (data) {
        return $http.post(BASE_API + '/videoUrl/fetch', data).then(function (res) {
            return res.data;
        });
    }
}