angular.module('service').service('contactus',Contactus);

Contactus.$inject = ['$http','BASE_API'];

function Contactus($http,BASE_API) {
    this.getContactUsDetails = function (data) {
        return $http.post(BASE_API + '/contact/getContactUsDetails', data);
    }
}