'use strict';

angular.module("service")
    .service('userCourses', UserCourses);


UserCourses.$inject = ['$http','BASE_API'];

function UserCourses($http,BASE_API) {

    //deprecated
    this.getPurchasedCourses = function(data){
        return $http.post(BASE_API + '/uc/getPurchasedCourseOfUser',data);
    }

    this.updatePurchasedCourses = function(data){
        return $http.post(BASE_API + '/uc/updatePurchasedCourses',data);
    }

    // addPurchasedCourse
    this.addPurchasedCourse = function(data){
        return $http.post(BASE_API + '/uc/addPurchasedCourse',data);
    }

    this.getPurchasedCourseOfUser = function(data) {
        return $http.post(BASE_API + '/uc/getPurchasedCourseOfUser',data);
    }

    this.getPurchasedCourseDetails = function(data) {
        return $http.post(BASE_API + '/uc/getPurchasedCourseDetails',data);
    }
};