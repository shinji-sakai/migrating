require('serviceModule');
angular.module('service').service('forums',Forums);
Forums.$inject = ['$http','BASE_API'];
function Forums($http,BASE_API) {
    this.saveQuestion = function (data) {
        return $http.post(BASE_API + '/forums/saveQuestion',data);
    }

    this.updateQuestion = function (data) {
        return $http.post(BASE_API + '/forums/updateQuestion',data);
    }

    this.deleteQuestion = function (data) {
        return $http.post(BASE_API + '/forums/deleteQuestion',data);
    }

    this.getFirstFewQuestions = function (data) {
        return $http.post(BASE_API + '/forums/getFirstFewQuestions',data);
    }

    this.getNextFewQuestions = function (data) {
        return $http.post(BASE_API + '/forums/getNextFewQuestions',data);
    }

	this.updateTrendingViews = function (data) {
        return $http.post(BASE_API + '/forums/updateTrendingViews',data);
    }

    this.getTrendingQrys = function (data) {
        return $http.post(BASE_API + '/forums/getTrendingQrys',data);
    }

    this.updateTrendingCounts = function (data) {
        return $http.post(BASE_API + '/forums/updateTrendingCounts',data);
    }

    this.getQuestion = function (data) {
        return $http.post(BASE_API + '/forums/getQuestion',data);
    }

    this.getAllQuestion = function (data) {
        return $http.post(BASE_API + '/forums/getAllQuestion',data);
    }

    ////////////////////
    // Categories API //
    ////////////////////
    this.getAllCategories = function (data) {
        return $http.post(BASE_API + '/forums/category/getAllCategories',data);
    }
    this.saveCategory = function (data) {
        //if coming with id then update or else add new
        return $http.post(BASE_API + '/forums/category/saveCategory',data);
    }
    this.deleteCategory = function (data) {
        return $http.post(BASE_API + '/forums/category/deleteCategory',data);
    }

    ///////////////////////
    // qry stats counter //
    ///////////////////////
    this.updateQuestionStatsCounter = function (data) {
        return $http.post(BASE_API + '/forums/updateQuestionStatsCounter',data);
    }
    this.getQuestionStatsCounter = function (data) {
        return $http.post(BASE_API + '/forums/getQuestionStatsCounter',data);
    }

    ///////////////////////
    // user qry stats counter //
    ///////////////////////

    this.updateUserQuestionStats = function (data) {
        return $http.post(BASE_API + '/forums/updateUserQuestionStats',data);
    }
    this.getUserQuestionStats = function (data) {
        return $http.post(BASE_API + '/forums/getUserQuestionStats',data);
    }

    //update question stats + update user question stats
    this.updateQuestionStats = function (data) {
        return $http.post(BASE_API + '/forums/updateQuestionStats',data);
    }

    ///////////////
    //Follow API //
    ///////////////

    this.updateFollower = function (data) {
        // data = {usr_id , qry_id}
        return $http.post(BASE_API + '/forums/follow/updateFollower',data);
    }

    this.deleteFollower = function (data) {
        // data = {usr_id , qry_id}
        return $http.post(BASE_API + '/forums/follow/deleteFollower',data);
    }

    this.updateNotifications = function(data){
        // data = {ans_usr_id , qry_id}
        return $http.post(BASE_API + '/forums/follow/updateNotifications',data);
    }

    // answer later
    this.updateAnswerLater = function(data){
        // data = {usr_id , qry_id , ans_flg}
        return $http.post(BASE_API + '/forums/updateAnswerLater',data);
    }

    this.getAnswerLater = function(data){
        // data = {usr_id}
        return $http.post(BASE_API + '/forums/getAnswerLater',data);
    }

    //Follow Notifications
    this.getAllNotifications = function(data){
        //
        // data = {usr_id}
        return $http.post(BASE_API + '/forums/getAllNotifications',data);
    }

    this.getDetailedFollowNotifications = function(data){
        return $http.post(BASE_API + '/forums/getDetailedFollowNotifications',data);
    }

    this.markFollowNotificationRead = function(data){
        return $http.post(BASE_API + '/forums/follow/markFollowNotificationRead',data);      
    }





    //report question
    this.reportQuestion = function(data){
        return $http.post(BASE_API + '/forums/saveComplain',data);
    }

    this.saveQAAnswer = function(data){
        return $http.post(BASE_API + '/forums/ans/saveAnswer',data);
    }

    this.deleteQAAnswer = function(data){
        return $http.post(BASE_API + '/forums/ans/deleteAnswer',data);
    }

    this.editQAAnswer = function(data){
        return $http.post(BASE_API + '/forums/ans/editAnswer',data);
    }

    this.getAllAnswersOfQuestion = function(data){
        return $http.post(BASE_API + '/forums/ans/getAllAnswersOfQuestion',data);
    }

    this.getAnswerOfQuestionUsingId = function(data){
        return $http.post(BASE_API + '/forums/ans/getAnswerOfQuestionUsingId',data);
    }

    this.likeDislikeComment = function(data) {
        return $http.post(BASE_API + '/forums/ans/likeDislike',data);
    }
    this.updateCommentLikeDislikeCount = function(data) {
        return $http.post(BASE_API + '/forums/ans/updateLikeDislikeCount',data);
    }
}
