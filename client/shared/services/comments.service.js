require('ng-file-upload');
angular
.module('service')
.service('comment', Comment);

Comment.$inject = ['$http','Upload','BASE_API'];

function Comment($http,Upload,BASE_API){

	this.saveComment = function(data){

		return Upload.upload({
            url: BASE_API + '/dashboard/comment/save',
            data: data,
            method: 'POST'
        });

        // return $http.post(BASE_API + '/dashboard/comment/save',data);
    }

    this.editComment = function(data) {
        return Upload.upload({
            url: BASE_API + '/dashboard/comment/edit',
            data: data,
            method: 'POST'
        });
    }

    this.deleteComment = function(data){
        return $http.post(BASE_API + '/dashboard/comment/delete',data);
    }

    this.likeDislikeComment = function(data) {
        return $http.post(BASE_API + '/dashboard/comment/like_dislike',data);
    }
    // 
    this.getLikeDislikeComments = function(data) {
        return $http.post(BASE_API + '/dashboard/comment/getLikeDislikeComments',data);
    }

    this.getCommentLikeDislikeCount = function(data) {
        return $http.post(BASE_API + '/dashboard/comment/getCommentLikeDislikeCount',data);   
    }

    this.updateCommentLikeDislikeCount = function(data) {
        return $http.post(BASE_API + '/dashboard/comment/updateCommentLikeDislikeCount',data);   
    }
    
    this.getPostComments = function(data){
        return $http.post(BASE_API + '/dashboard/comment/get',data);
    }

    this.getCommentUsers = function(data){
        return $http.post(BASE_API + '/dashboard/comment/getCommentUsers',data);
    }
    
}
