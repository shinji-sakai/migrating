angular.module('service').service('bookHighlightsService',BookHighlights);

BookHighlights.$inject = ['$http','BASE_API'];

function BookHighlights($http,BASE_API) {
    this.saveBookHighlight = function (data) {
        return $http.post(BASE_API + '/book/highlights/saveBookHighlight',data);
    }
    this.removeBookHighlight = function (data) {
        return $http.post(BASE_API + '/book/highlights/removeBookHighlight',data);
    }
    this.getBookItemHighlights = function (data) {
        return $http.post(BASE_API + '/book/highlights/getBookItemHighlights',data);
    }
}
