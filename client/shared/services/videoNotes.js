angular.module('service').service('videoNotes',VideoNotes);

VideoNotes.$inject = ['$http','BASE_API'];

function VideoNotes($http,BASE_API) {
    this.getNotesModuleNames = function (data) {
        return $http.post(BASE_API + '/videonotes/getNotesModuleNames', data).then(function (res) {
            return res.data;
        });
    }
    this.getModuleNotes = function (data) {
        return $http.post(BASE_API + '/videonotes/getModuleNotes', data).then(function (res) {
            return res.data;
        });
    }
    this.updateModuleNotes = function (data) {
        return $http.post(BASE_API + '/videonotes/updateModuleNotes', data).then(function (res) {
            return res.data;
        });
    }


    this.getVideoNotesModules = function (data) {
        return $http.post(BASE_API + '/videonotes/getVideoNotesModules', data);
    }

    this.getVideoNotesForModules = function (data) {
        return $http.post(BASE_API + '/videonotes/getVideoNotesForModules', data);
    }

    this.deleteVideoNote = function (data) {
        return $http.post(BASE_API + '/videonotes/deleteVideoNote', data);
    }

    this.updateVideoNote = function (data) {
        return $http.post(BASE_API + '/videonotes/updateVideoNote', data);
    }

    this.insertVideoNotes = function (data) {
        return $http.post(BASE_API + '/videonotes/insertVideoNotes', data);
    }

    this.getVideoNotesOfUser = function(data) {
        return $http.post(BASE_API + '/videonotes/getVideoNotesOfUser', data);   
    }
}