require('authToken');
angular.module('service').service('auth', auth);
auth.$inject = ['$http', 'authToken','BASE_API'];

function auth($http, authToken,BASE_API) {

    function authSuccessful(res) {
        if(res.data && res.data.token || res.data.user){
            authToken.setToken(res.data.token);
            authToken.setUser(res.data.user);
            authToken.setUserCourses(res.data.uc);
        }
        return res
    }

    this.login = function(data) {
        return $http.post(BASE_API + '/auth/login',data).then(authSuccessful);
    }
    this.logout = function(data) {
        return $http.post(BASE_API + '/auth/logout',data);
    }
    this.register = function(data){
        return $http.post(BASE_API + '/auth/register',data);   
    }
    this.verifyOTP = function(data){
        return $http.post(BASE_API + '/auth/verifyOTP',data).then(authSuccessful);   
    }
    this.resendOTP = function(data){
        return $http.post(BASE_API + '/auth/resendOTP',data);   
    }
    this.removeUser = function(data){
        return $http.post(BASE_API + '/auth/removeUser',data);   
    }
    this.checkUserExistance = function(data){
        return $http.post(BASE_API + '/auth/checkUserExistance',data)
                .then(function(res){
                    return res.data;
                });
    }
    this.checkPassword = function checkPassword(data){
        return $http.post(BASE_API + '/auth/checkPassword',data);
    }
    this.changePassword = function changePassword(data){
        console.log("data.........",data);
        return $http.post(BASE_API + '/auth/changePassword',data);   
    }

    this.forgotPassword = function (data){
        return $http.post(BASE_API + '/auth/forgotPassword',data);   
    }
    this.resendForgotPasswordOTP = function (data){
        return $http.post(BASE_API + '/auth/resendForgotPasswordOTP',data);   
    }
    this.verifyForgotPasswordOTP = function(data){
        return $http.post(BASE_API + '/auth/verifyForgotPasswordOTP',data);   
    }
}