angular
.module('service')
.service('itemQueries', Service);

Service.$inject = ['$http','Upload','BASE_API'];

function Service($http,Upload,BASE_API){
	this.save = function(data){
        return $http.post(BASE_API + '/itemQueries/save',data);
    }

    this.getAll = function(data) {
        return $http.post(BASE_API + '/itemQueries/getAll',data);
    }
}
