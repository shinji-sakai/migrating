angular.module('service').service('videoViews',VideoViews);

VideoViews.$inject = ['$http','BASE_API'];

function VideoViews($http,BASE_API) {
    this.incrementVideoViews = function (data) {
        return $http.post(BASE_API + '/videoviews/incrementVideoViews', data);
    }
    
    this.getVideoViews = function (data) {
        return $http.post(BASE_API + '/videoviews/getVideoViews', data);
    }

    this.submitVideoStats = function (data) {
        return $http.post(BASE_API + '/userstats', data);
    }
}