angular.module('service').service('bookmark',Bookmark);

Bookmark.$inject = ['$http','BASE_API'];

function Bookmark($http,BASE_API) {
    this.getAllBookmarks = function (data) {
    	//UserId 
        return $http.post(BASE_API + '/bookmarks/getBookmarks',data).then(function (res) {
            return res.data;
        });
    }

    // 
    this.bookmarkMultipleItems = function (data) { 
        return $http.post(BASE_API + '/bookmarks/bookmarkMultipleItems',data).then(function (res) {
            return res.data;
        });
    }

    this.getFullCourseBookmarks = function (data) {
    	//CourseId , UserId , fullbookmark:true
        return $http.post(BASE_API + '/bookmarks/getBookmarks',data).then(function (res) {
            return res.data;
        });
    }

    this.bookmarkItem = function (data) {
    	//CourseId , UserId , fullbookmark:true
        return $http.post(BASE_API + '/bookmarks/addBookmark',data).then(function (res) {
            return res.data;
        });
    }

    this.getUserBookmarks = function (data) {
        //CourseId , UserId , fullbookmark:true
        return $http.post(BASE_API + '/bookmarks/getUserBookmarks',data);
    }


}