'use strict';
require('angular-local-storage');
require('angular-jwt');
var Base64 = require('hash_js').Base64;
angular.module("service")
    .factory('authToken', authToken);


authToken.$inject = ['localStorageService', 'jwtHelper'];

function authToken(localStorageService, jwtHelper) {

    var cachedToken;
    var cachedUser;
    var cachedPrevUrl;
    var cachedSocketId;
    var cachedHashToken;
    var urlKey = "dsPageToRedirect";
    var hashTokenKey = "dsHashToken";
    var userKey = "dsUser";
    var socketKey = "dsSocket";
    var userToken = 'dsToken';
    var sessionKey = 'ewSess';
    var userCoursesKey = 'dsuc';
    var imageBaseUrlKey = "imageBaseUrl";
    var playerMute = "playerMute";
    var playerVolume = "playerVolume";
    var subtitleStatus = "subtitleStatus";
    var playerAutoplay = "playerAutoplay";
    var playerSpeed = "playerSpeed";
    var playerQuality = "playerQuality";
    var playerTheaterMode = "playerTheaterMode";
    var isAuthenticated = false;
    var authToken = {
        setSessionId: function(sessId) {
            localStorageService.set(sessionKey, sessId);
        },
        getSessionId: function() {
            return localStorageService.get(sessionKey);
        },
        removeSessionId: function() {
            return localStorageService.remove(sessionKey);
        },
        setToken: function(token) {
            cachedToken = token;
            localStorageService.set(userToken, token);
            // localStorageService.cookie.set(userToken, token , null);
            isAuthenticated = true;
        },
        getToken: function() {
            if (!cachedToken)
                cachedToken = localStorageService.get(userToken);
            //  || localStorageService.get(userToken)
            return cachedToken;
        },
        isAuthenticated: function() {
            var isAuth = !authToken.isExpired() && !!authToken.getToken() && !!authToken.getUser();
            if(!isAuth){
                authToken.removeUser();
                authToken.removeToken();
                localStorageService.remove("ques");
                localStorageService.remove("quesData");
            }
            return isAuth;
        },
        removeToken: function() {
            cachedToken = null;
            localStorageService.remove(userToken);
            isAuthenticated = false;
        },
        isExpired: function() {
            //var expireDate = jwtHelper.getTokenExpirationDate(authToken.getToken());
            if (authToken.getToken()) {
                var isExpired = jwtHelper.isTokenExpired(authToken.getToken());
                // console.log("isExpired" , isExpired);
                return isExpired;
            } else {
                return true;
            }
        },
        setUser: function(user) {
            cachedUser = user;
            localStorageService.set(userKey, user);
        },
        getUser: function() {
            if (!cachedUser)
                cachedUser = localStorageService.get(userKey);

            return cachedUser;
        },
        getHashToken: function() {
            if (!cachedHashToken)
                cachedHashToken = localStorageService.get(hashTokenKey);

            return cachedHashToken;
        },
        setUserDisplayName: function(dsp_nm){
            if(authToken.isAuthenticated()){
                var user = authToken.getUser();
                user["dsp_nm"] = dsp_nm;
                authToken.setUser(user);
            }
        },
        getUserId : function() {
            if(authToken.isAuthenticated()){
                return authToken.getUser()['usr_id'];
            }
            return "";
        },
        getUserPic : function(){
            if(authToken.isAuthenticated()){
                return authToken.getUser()['usr_pic'] || "";   
            }
            return "";
        },
        getUserRoles : function(){
            if(authToken.isAuthenticated()){
                return authToken.getUser()['usr_role'] || [];   
            }
            return [];
        },
        setUserPic : function(pic){
            var user = authToken.getUser();
            user["usr_pic"] = pic;
            authToken.setUser(user);
        },
        removeUser: function() {
            cachedUser = null;
            localStorageService.remove(userKey);
        },
        cachedUrl: function(url) {
            if (url) {
                cachedPrevUrl = sessionStorage[urlKey] = url;
                return cachedPrevUrl;
            }
            cachedPrevUrl = sessionStorage[urlKey];
            return cachedPrevUrl;
        },
        clearCachedUrl : function() {
            // console.log("clearing cachedUrl")
            delete sessionStorage[urlKey];
            cachedPrevUrl = "";
            return cachedPrevUrl;
        },
        logout: function() {
            authToken.removeToken();
            authToken.removeUser();
        },
        setSocketId: function(id) {
            cachedSocketId = id;
            localStorageService.set(socketKey, id);
        },
        getSocketId: function() {
            if (!cachedSocketId)
                cachedSocketId = localStorageService.get(socketKey);
            return cachedSocketId;
        },
        removeSocketId: function() {
            cachedSocketId = null;
            localStorageService.remove(socketKey);
        },
        setUserCourses: function(courses) {
            if(courses.length > 0){
                courses = Base64.encode(JSON.stringify(courses));
            }
            localStorageService.set(userCoursesKey, courses);
        },
        addUserCourses: function(courses) {
            var alreadyAddedCourses = this.getUserCourses();
            courses = [].concat(courses);
            for (var i = 0; i < courses.length; i++) {
                var id = courses[i];
                if(alreadyAddedCourses.indexOf(id) <= -1){
                    alreadyAddedCourses.push(id);
                }
            }
            this.setUserCourses(alreadyAddedCourses);
        },
        removeUserCourses: function(courses) {
            localStorageService.remove(userCoursesKey);
        },
        getUserCourses: function() {
            var courses = localStorageService.get(userCoursesKey) || [];
            if(courses.length > 0){
                courses = JSON.parse(Base64.decode(courses))
            }
            return courses;
        },
        isCoursePurchased : function(course){
            var courses = this.getUserCourses();
            return (courses.indexOf(course) > -1);
        },
        setImageBaseUrl : function(url) {
            localStorageService.set(imageBaseUrlKey, url);
        },
        getImageBaseUrl: function() {
            return (localStorageService.get(imageBaseUrlKey) || '');
        },
        playerMute : function(isPlayerMute){
            if(isPlayerMute !== undefined){
                localStorageService.set(playerMute, isPlayerMute);
                return isPlayerMute;
            }else{
                return (localStorageService.get(playerMute) || false);
            }
        },
        playerVolume : function(volume){
            if(volume !== undefined){
                localStorageService.set(playerVolume, volume);
                return volume;
            }else{
                var volume = localStorageService.get(playerVolume);
                if(volume === undefined){
                    return 100;
                }
                return volume;
            }
        },
        subtitleStatus : function(isSubtitleOn){
            if(isSubtitleOn !== undefined){
                localStorageService.set(subtitleStatus, isSubtitleOn);
                return isSubtitleOn;
            }else{
                var isSubtitleOn = localStorageService.get(subtitleStatus);
                if(isSubtitleOn === undefined){
                    return false;
                }
                return isSubtitleOn;
            }
        },
        playerAutoplay : function(isAutoplay){
            if(isAutoplay !== undefined){
                localStorageService.set(playerAutoplay, isAutoplay);
                return isAutoplay;
            }else{
                var isAutoplay = localStorageService.get(playerAutoplay);
                if(isAutoplay === undefined){
                    return false;
                }
                return isAutoplay;
            }
        },
        playerSpeed : function(speed){
            if(speed !== undefined){
                localStorageService.set(playerSpeed, speed);
                return speed;
            }else{
                var speed = localStorageService.get(playerSpeed);
                if(speed === undefined){
                    return 1;
                }
                return speed;
            }
        },
        playerQuality : function(quality){
            if(quality !== undefined){
                localStorageService.set(playerQuality, quality);
                return quality;
            }else{
                var quality_val = localStorageService.get(playerQuality);
                if(quality_val === undefined){
                    return "HIGH";
                }
                return quality_val;
            }
        },
        playerTheaterMode : function(isInTheaterMode){
            if(isInTheaterMode !== undefined){
                localStorageService.set(playerTheaterMode, isInTheaterMode);
                return isInTheaterMode;
            }else{
                var isInTheaterMode_val = localStorageService.get(playerTheaterMode);
                if(isInTheaterMode_val === undefined){
                    return false;
                }
                return isInTheaterMode_val;
            }
        }

    }

    return authToken;
};