angular.module('service').service('chat',Chat);

Chat.$inject = ['$http','BASE_API'];

function Chat($http,BASE_API) {
    this.findFriends = function (data) {
        //Userid and keyword to find
    	return $http.post(BASE_API + '/chat/friends/find',data);
    }


    this.findFriendWithId = function (data) {
        //Userid as a friend id to search
        return $http.post(BASE_API + '/chat/friends/findFriendWithId',data);
    }

    this.fetchMsgs = function (data) {
        //Userid and fr_id to find
    	return $http.post(BASE_API + '/chat/msgs/find',data);
    }
    this.clearMsgs = function (data) {
        //Userid and fr_id to clear
        return $http.post(BASE_API + '/chat/msgs/clear',data);
    }

    this.deleteMsgs = function (data) {
        return $http.post(BASE_API + '/chat/msgs/deleteMsgs',data);
    }

    this.editMsgs = function (data) {
        return $http.post(BASE_API + '/chat/msgs/editMsgs',data);
    }

    this.createGroup = function (data) {
        //grp_nm , grp_own,members
    	return $http.post(BASE_API + '/chat/grp/create',data);
    }

    this.addMembersToGroup = function (data) {
        //grp_nm , grp_own,members
        return $http.post(BASE_API + '/chat/grp/addMembers',data);
    }

    this.findMembers = function (data) {
        //grp_id
        return $http.post(BASE_API + '/chat/grp/findMembers',data);
    }

}