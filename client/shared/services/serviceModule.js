require("angular-loading-bar")
require("angular-loading-bar/build/loading-bar.min.css")
require("angular-animate")
require('angular-local-storage');
require('angular-jwt');

var app = angular.module('service', ['ngAnimate','LocalStorageModule',"angular-jwt"]);

// Dont change position of this module. This require after angular.module
require('./authInterceptor.js');

app.config(['$httpProvider', function($httpProvider) {
    // cfpLoadingBarProvider.latencyThreshold = 1;
    $httpProvider.interceptors.push('serviceInterceptor');
}])

//Set Base url for API
var location = window.location.href;
var host = window.location.host;
//  || location.indexOf('http://examwarrior.com') > -1
// if (location.indexOf('ew-test') > -1 || location.indexOf('localhost') > -1) {
if (host.indexOf("examwarrior.com") <= -1) {
    app.constant("BASE_API", 'http://' + host + '');
} else {
    app.constant("BASE_API", 'https://' + host + '');
}