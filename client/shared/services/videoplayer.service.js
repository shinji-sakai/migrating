'use strict';

angular.module("service")
    .service('videoPlayer', VideoPlayer);


VideoPlayer.$inject = [];

function VideoPlayer() {


    this.setVideoPlayer = setVideoPlayer;
    this.getVideoPlayer = getVideoPlayer;
    var videoPlayer = null;

    function setVideoPlayer(videoPlayer) {
        this.videoPlayer = videoPlayer;
    }
    function getVideoPlayer(){
        return this.videoPlayer;
    }


};