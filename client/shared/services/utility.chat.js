'use strict';
require('angular-local-storage');
var Base64 = require('hash_js').Base64;
angular.module("service")
    .factory('chatUtility', chatUtility);
chatUtility.$inject = ['localStorageService'];
function chatUtility(localStorageService) {
    var chatUtility = {
        isOpenDialog: function(val) {
        	localStorageService.set('chatUtility', {'isOpenDialog':val});
        },
        openChatList: function(user_id){
        	var obj = localStorageService.get('chatUtility');
    		if (obj.userList!=undefined) {
    			obj.userList.push(String(user_id));
        		localStorageService.set('chatUtility', obj);
        	}
        	else{
        		obj.userList = [];
        		obj.userList[0] = user_id;
	        	localStorageService.set('chatUtility', obj);
        	}
        },
        getInfo: function(){
            return localStorageService.get('chatUtility');
        },
        deleteMember: function(user_id){
            console.log(user_id);
            var obj = localStorageService.get('chatUtility');
            var a = obj.userList.indexOf(String(user_id));
            obj.userList.splice(a,1);
            console.log(obj.userList);
            localStorageService.set('chatUtility', obj);
        },
        clearChatInfo: function(){
            localStorageService.remove('chatUtility');
        }
    }
    return chatUtility;
};