angular
.module('service')
.service('post', Post);

Post.$inject = ['$http','BASE_API'];

function Post($http,BASE_API){

	this.savePost = function(data){
		return $http.post(BASE_API + '/dashboard/post/save',data);
	}

	this.getPost = function(data){
		return $http.post(BASE_API + '/dashboard/post/getPosts',data);
	}

    this.getFeed = function(data){
        return $http.post(BASE_API + '/dashboard/post/getFeed',data);
    }

    this.getUserPublicPosts = function(data){
        return $http.post(BASE_API + '/dashboard/post/getUserPublicPosts',data);
    }


    this.getUserPostsForFriends = function(data){
        return $http.post(BASE_API + '/dashboard/post/getUserPostsForFriends',data);
    }
    // 

    // 

    this.deletePost = function(data){
        console.log("data....",data);
        return $http.post(BASE_API + '/dashboard/post/delete',data);
    }

    this.editPost = function(data){
        return $http.post(BASE_API + '/dashboard/post/edit',data);
    }

    this.saveComment = function(data){
        return $http.post(BASE_API + '/dashboard/comment/save',data);
    }

    this.getComment = function(data){
        return $http.post(BASE_API + '/dashboard/comment/get',data);
    }
    this.deleteComment = function(data){
        return $http.post(BASE_API + '/dashboard/comment/delete',data);
    }

    //report post
    this.report = function(data){
        return $http.post(BASE_API + '/dashboard/post/saveComplain',data);      
    }

    // post Like api
    this.getLikeByUser = function(data){
        return $http.post(BASE_API + '/dashboard/post/getLikeByUser',data);
    }
    this.updateLikeByUser = function(data){
        return $http.post(BASE_API + '/dashboard/post/updateLikeByUser',data);
    }
    this.getLikeStatsForPosts = function(data){
        return $http.post(BASE_API + '/dashboard/post/getLikeStatsForPosts',data);
    }
    this.updateLikeStatsForPosts = function(data) {
        return $http.post(BASE_API + '/dashboard/post/updateLikeStatsForPosts',data);
    }
}
