angular
.module('service')
.service('paypal', paypal);

paypal.$inject = ['$http','BASE_API'];

function paypal($http,BASE_API){

	this.createPayment = function(data){
		return $http.post(BASE_API + '/payment/paypal/create',data);
	}

	this.executePayment = function(data){
		return $http.post(BASE_API + '/payment/paypal/'+ data.paymentId + '/execute',data);
	}

	// save payment details
	this.savePaymentDetails = function(data){
		return $http.post(BASE_API + '/payment/savePaymentDetails',data);
	}


	this.paymentUsingCCAvenue = function(data){
		return $http.post(BASE_API + '/payment/ccavenue/ccavRequestHandler',data);	
	}

	this.getPurchasedTrainingDetails = function(data){
		return $http.post(BASE_API + '/payment/getPurchasedTrainingDetails',data);	
	}

	this.saveUserEnrolledBatchDetails = function(data){
		return $http.post(BASE_API + '/payment/saveUserEnrolledBatchDetails',data);	
	}

	this.getUserEnrolledBatchDetails = function(data){
		return $http.post(BASE_API + '/payment/getUserEnrolledBatchDetails',data);	
	}
}