angular.module('service').service('notesService',Notes);

Notes.$inject = ['$http','BASE_API'];

function Notes($http,BASE_API) {

    this.deleteNote = function (data) {
    	return $http.post(BASE_API + '/api/notes/deleteNote',data);
    }

    this.updateNote = function (data) {
        return $http.post(BASE_API + '/api/notes/updateNote',data);
    }

    this.getNotes = function (data) {
        return $http.post(BASE_API + '/api/notes/getNotes',data);
    }

    this.insertNote = function (data) {
        return $http.post(BASE_API + '/api/notes/insertNote',data);
    }
}