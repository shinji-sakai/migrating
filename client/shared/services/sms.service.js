angular
	.module('service')
	.service('SMS',SMS);

SMS.$inject = ['$http','BASE_API'];

function SMS($http,BASE_API) {
    this.sendSMS = function (data) {
        return $http.post(BASE_API + '/sms/send', data);
    }
}
