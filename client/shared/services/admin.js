  angular
    .module('service')
    .service('admin', admin);

admin.$inject = ['$http','BASE_API'];

function admin($http,BASE_API) {


    this.isIdExist = function(data) {
        return $http.post(BASE_API + '/admin/general/isIdExist',data);
    }


    this.fetchItemUsingLimit = function(data) {
        return $http.post(BASE_API + '/admin/general/fetchItemUsingLimit',data);
    }

    /* Course Master API */

    this.coursemaster = function() {
        //  /admin/coursemaster
        return $http.post(BASE_API + '/courseMaster/getAll').then(function(res) {
            return res.data;
        });
    }

    

    this.addCourseMaster = function(data) {
        //      /admin/addCourseMaster
        return $http.post(BASE_API + '/courseMaster/add', data).then(function(res) {
            return res.data;
        });
    }
    this.deleteCourseMaster = function(data) {
        //      /admin/deleteCourseMaster
        return $http.post(BASE_API + '/courseMaster/delete', data).then(function(res) {
            return res.data;
        });
    }
    this.fetchCourseMasterUsingLimit = function(data){
        return $http.post(BASE_API + '/courseMaster/fetchCourseMasterUsingLimit', data);
    }

    this.getRelatedCoursesByCourseId = function(data) {
        return $http.post(BASE_API + '/courseMaster/getRelatedCoursesByCourseId',data);
    }

    /* Course API */
    this.courselistfull = function(data) {
        return $http.post(BASE_API + '/course/getAll',data).then(function(res) {
            return res.data;
        });
    }
    this.addCourse = function(data) {
        // /admin/addCourse
        return $http.post(BASE_API + '/course/add', data).then(function(res) {
            return res.data;
        });
    }
    this.removeCourse = function(data) {
        //  /admin/removeCourse
        return $http.post(BASE_API + '/course/delete', data).then(function(res) {
            return res.data;
        });
    }

    /* Course Videos API */
    this.addVideos = function(data) {
        //  /admin/addVideos
        return $http.post(BASE_API + '/courseVideos/add', data).then(function(res) {
            return res.data;
        });
    }
    this.getModuleVideos = function(data) {
        //  /admin/getModuleVideos
        return $http.post(BASE_API + '/courseVideos/getModuleVideos', data).then(function(res) {
            return res.data;
        });
    }
    this.getCourseVideos = function(data) {
        //      /admin/getCourseVideos
        return $http.post(BASE_API + '/courseVideos/getCourseVideos', data).then(function(res) {
            return res.data;
        });
    }

    /* Course Modules API */
    this.getCourseModules = function(data) {
        //      /admin/getCourseModules
        return $http.post(BASE_API + '/courseModule/getAll', data).then(function(res) {
            return res.data;
        });
    }
    this.addCourseModule = function(data) {
        //  /admin/addCourseModule
        return $http.post(BASE_API + '/courseModule/add', data).then(function(res) {
            return res.data;
        });
    }
    this.deleteCourseModule = function(data) {
        //  /admin/deleteCourseModule
        return $http.post(BASE_API + '/courseModule/delete', data).then(function(res) {
            return res.data;
        });
    }

    /* Video Slides API */
    this.addVideoSlide = function(data) {
        //  /admin/addVideoSlide
        return $http.post(BASE_API + '/videoSlide/add', data).then(function(res) {
            return res.data;
        });
    }
    this.getVideoSlides = function(data) {
        //  /admin/getVideoSlides
        return $http.post(BASE_API + '/videoSlide/getAll', data).then(function(res) {
            return res.data;
        });
    }
    this.deleteVideoSlide = function(data) {
        //  /admin/deleteVideoSlide
        return $http.post(BASE_API + '/videoSlide/delete', data).then(function(res) {
            return res.data;
        });
    }

    /* Authorized Items API */

    this.getAuthorizedItems = function(data){
        return $http.post(BASE_API + '/ai/getItems',data);
    }

    this.updateAuthorizedItems = function(data){
        return $http.post(BASE_API + '/ai/updateItems',data);
    }

    this.addAuthorizedItem = function(data){
        return $http.post(BASE_API + '/ai/addItem',data);
    }

    this.getAuthorizedVideoItemFullInfo = function(data){
        return $http.post(BASE_API + '/ai/getVideoItemFullInfo',data);
    }
    this.getVideoPermission = function(data){
        return $http.post(BASE_API + '/ai/getVideoPermission',data);
    }


    /* practice Question API */

    this.savePracticeQuestion = function(data){
        return $http.post(BASE_API + '/practice/saveQuestion',data);
    }
    this.savePreviewPracticeQuestion = function(data){
        return $http.post(BASE_API + '/practice/savePreviewQuestion',data);
    }
    this.saveQuestionStats = function(data){
        return $http.post(BASE_API + '/practice/saveQuestionStats',data);
    }
    this.saveTestQuestionStats = function(data){
        return $http.post(BASE_API + '/practice/saveTestQuestionStats',data);
    }
    this.saveTestReviews  = function(data){
        return $http.post(BASE_API + '/practice/saveTestReviews',data);
    }
    this.removePracticeQuestion = function(data){
        return $http.post(BASE_API + '/practice/removeQuestion',data);
    }
    this.getAllPracticeQuestions = function(data){
        return $http.post(BASE_API + '/practice/getAllQuestions',data);
    }
    this.findPracticeQuestionsByQuery = function(data){
        return $http.post(BASE_API + '/practice/findQuestionsByQuery',data);
    }
    this.findPracticeQuestionsIdByQuery = function(data){
        return $http.post(BASE_API + '/practice/findQuestionsIdByQuery',data);
    }
    this.findPracticeQuestionsTextByQuery = function(data){
        return $http.post(BASE_API + '/practice/findQuestionsTextByQuery',data);
    }
    this.findPreviewPracticeQuestionsByQuery = function(data){
        return $http.post(BASE_API + '/practice/findPreviewQuestionsByQuery',data);
    }
    this.getPracticeQuestionsById = function(data){
        return $http.post(BASE_API + '/practice/getPracticeQuestionsById',data);
    }

    this.getPracticeQuestionsByLimit = function(data){
        return $http.post(BASE_API + '/practice/getPracticeQuestionsByLimit',data);
    }

    this.updateMultiplePracticeQuestions = function(data){
        return $http.post(BASE_API + '/practice/updateMultiplePracticeQuestions',data);
    }
    this.getAllAvgQuestionPerfStats = function(data){
        return $http.post(BASE_API + '/practice/getAllAvgQuestionPerfStats',data);
    }
    this.getAllUsersQuestionPerfStats = function(data){
        return $http.post(BASE_API + '/practice/getAllUsersQuestionPerfStats',data);
    }
    this.getAllMonthAvgQuestionPerfStats = function(data){
        return $http.post(BASE_API + '/practice/getAllMonthAvgQuestionPerfStats',data);
    }
    this.getAllUsersMonthQuestionPerfStats = function(data){
        return $http.post(BASE_API + '/practice/getAllUsersMonthQuestionPerfStats',data);
    }
    this.updateVerifyPracticeQuestion = function(data){
        return $http.post(BASE_API + '/practice/updateVerifyPracticeQuestion',data);
    }
    this.getAllVerifyStatusOfPracticeQuestion = function(data){
        return $http.post(BASE_API + '/practice/getAllVerifyStatusOfPracticeQuestion',data);
    }
    this.getLatestVerifyStatusOfGivenPracticeQuestions = function(data){
        return $http.post(BASE_API + '/practice/getLatestVerifyStatusOfGivenPracticeQuestions',data);
    }
    this.getUserMonthQuestionAttemptStats = function(data){
        return $http.post(BASE_API + '/practice/getUserMonthQuestionAttemptStats',data);
    }
    this.startUserExam = function(data){
        return $http.post(BASE_API + '/practice/startUserExam',data);
    }
    this.finishUserExam = function(data){
        return $http.post(BASE_API + '/practice/finishUserExam',data);
    }
    this.checkForPreviousExam = function(data){
        return $http.post(BASE_API + '/practice/checkForPreviousExam',data);
    }
    this.getUserExamQuestion = function(data){
        return $http.post(BASE_API + '/practice/getUserExamQuestion',data);
    }
    this.saveUserExamQuestionData = function(data){
        return $http.post(BASE_API + '/practice/saveUserExamQuestionData',data);
    }
    this.startUserPractice = function(data){
        return $http.post(BASE_API + '/practice/startUserPractice',data);
    }
    this.saveUserPracticeQuestionData = function(data){
        return $http.post(BASE_API + '/practice/saveUserPracticeQuestionData',data);
    }
    this.fetchNextUserPracticeQuestion = function(data){
        return $http.post(BASE_API + '/practice/fetchNextUserPracticeQuestion',data);
    }
    this.getPracticeNodeQuestions = function(data){
        return $http.post(BASE_API + '/practice/getPracticeNodeQuestions',data);
    }






    /* Highlight APi */

    this.saveHighlightedText = function(data){
        return $http.post(BASE_API + '/highlight/save',data);
    }
    this.getQuestionHighlights = function(data){
        return $http.post(BASE_API + '/highlight/getQuestionHighlights',data);
    }
    this.getUserHighlights = function(data){
        return $http.post(BASE_API + '/highlight/getUserHighlights',data);
    }
    this.deleteHighlightedText = function(data){
        return $http.post(BASE_API + '/highlight/delete',data);
    }

    this.updateHighlightNote = function(data){
        return $http.post(BASE_API + '/highlight/updateHighlightNote',data);
    }

    /*
    Author API
     */
    this.getAllAuthor  = function(){
        return $http.post(BASE_API + '/author/getAll');
    }
    this.updateAuthor  = function(data){
        return $http.post(BASE_API + '/author/update',data);
    }
    this.removeAuthor  = function(data){
        return $http.post(BASE_API + '/author/remove',data);
    }
    this.fetchAuthorUsingLimit=function(data){
        return $http.post(BASE_API+'/author/fetchAuthorUsingLimit',data);
    }
    this.AuthorsIdIsExits=function(data){
        return $http.post(BASE_API+'/author/AuthorsIdIsExits',data);
    }

    /*
    Publication API
     */
    this.getAllPublication  = function(){
        return $http.post(BASE_API + '/publication/getAll');
    }
    this.updatePublication  = function(data){
        return $http.post(BASE_API + '/publication/update',data);
    }
    this.removePublication  = function(data){
        return $http.post(BASE_API + '/publication/remove',data);
    }
   this.fetchPublicationsUsingLimit=function(data){
       return $http.post(BASE_API+'/publication/fetchPublicationsUsingLimit',data);
   }
    /*
    Book API
     */
    this.getAllBook  = function(){
        return $http.post(BASE_API + '/book/getAll');
    }
    this.updateBook  = function(data){
        return $http.post(BASE_API + '/book/update',data);
    }
    this.removeBook  = function(data){
        return $http.post(BASE_API + '/book/remove',data);
    }
    this.fetchBooksUsingLimit=function(data){
        return $http.post(BASE_API+'/book/fetchBooksUsingLimit',data);
    }

    /*
    JobOpenings API
     */
    this.getAllJobOpenings  = function(){
        return $http.post(BASE_API + '/jobOpenings/getAllJobOpenings');
    }
    this.updateJobOpening  = function(data){
        return $http.post(BASE_API + '/jobOpenings/updateJobOpening',data);
    }
    this.removeJobOpening  = function(data){
        return $http.post(BASE_API + '/jobOpenings/removeJobOpening',data);
    }

    /*
    Subject API
     */
    this.getAllSubject  = function(){
        return $http.post(BASE_API + '/subject/getAll');
    }
    this.updateSubject  = function(data){
        return $http.post(BASE_API + '/subject/update',data);
    }
    this.removeSubject  = function(data){
        return $http.post(BASE_API + '/subject/remove',data);
    }
   this.fetchSubjectMasterUsingLimit = function(data){
        return $http.post(BASE_API + '/subject/fetchSubjectMasterUsingLimit',data);
    }
    this.SubjectIdIsExits=function(data){
        return $http.post(BASE_API+'/subject/SubjectIdIsExits',data);
    }
    /*
    Exam API
     */
    this.getAllExam  = function(){
        return $http.post(BASE_API + '/exam/getAll');
    }
    this.updateExam  = function(data){
        return $http.post(BASE_API + '/exam/update',data);
    }
    this.removeExam  = function(data){
        return $http.post(BASE_API + '/exam/remove',data);
    }
    this.fetchExamUsingLimit=function(data)
    {
        return $http.post(BASE_API+'/exam/fetchExamUsingLimit',data);
    }
    /*
    Topic API
     */
    this.getAllTopic  = function(data){
        return $http.post(BASE_API + '/topic/getAll',data);
    }
    this.updateTopic  = function(data){
        return $http.post(BASE_API + '/topic/update',data);
    }
    this.removeTopic  = function(data){
        return $http.post(BASE_API + '/topic/remove',data);
    }
   this.fetchTopicsUsingLimit=function(data)
   {
       return $http.post(BASE_API+'/topic/fetchTopicsUsingLimit',data);
   }
    /*
    Topic Group API
     */
    this.getAllTopicGroups  = function(data){
        return $http.post(BASE_API + '/topicGroup/getAllTopicGroups',data);
    }
    this.updateTopicGroup  = function(data){
        return $http.post(BASE_API + '/topicGroup/updateTopicGroup',data);
    }
    this.removeTopicGroup  = function(data){
        return $http.post(BASE_API + '/topicGroup/removeTopicGroup',data);
    }
    this.fetchTopicGroupUsingLimit=function(data)
    {
        return $http.post(BASE_API+'/topicGroup/fetchTopicGroupUsingLimit',data);
    }
    /*
    Tag API
     */
    this.getAllTag  = function(){
        return $http.post(BASE_API + '/tag/getAll');
    }
    this.updateTag  = function(data){
        return $http.post(BASE_API + '/tag/update',data);
    }
    this.removeTag  = function(data){
        return $http.post(BASE_API + '/tag/remove',data);
    }
    this.fetchTagsUsingLimit=function(data){
        return $http.post(BASE_API+'/tag/fetchTagsUsingLimit',data);
    }
    this.TagsIdIsExits=function(data)
    {
        return $http.post(BASE_API+'/tag/TagsIdIsExits',data);
    }

    /*
    SubTopic API
     */
    this.getAllSubTopic  = function(data){
        return $http.post(BASE_API + '/subtopic/getAll',data);
    }
    this.updateSubTopic  = function(data){
        return $http.post(BASE_API + '/subtopic/update',data);
    }
    this.removeSubTopic  = function(data){
        return $http.post(BASE_API + '/subtopic/remove',data);
    }

    /* videoentity api */
    this.updateVideoEntity = function(data){
        return $http.post(BASE_API + '/videoentity/update',data);
    }
    this.removeVideoEntity = function(data){
        return $http.post(BASE_API + '/videoentity/remove',data);
    }
    this.getAllVideoEntity = function(data){
        return $http.post(BASE_API + '/videoentity/getAll',data);
    }
    this.isVideoFilenameExist = function(data){
        return $http.post(BASE_API + '/videoentity/isVideoFilenameExist',data);
    }
    this.findVideoEntityByQuery = function(data){
        return $http.post(BASE_API + '/videoentity/findVideoEntityByQuery',data);
    }
            //One Id
    this.findVideoEntityById = function(data){
        return $http.post(BASE_API + '/videoentity/findVideoEntityById',data);
    }
            //Multiple Ids
    this.findVideoEntitiesById = function(data){
        return $http.post(BASE_API + '/videoentity/findVideoEntitiesById',data);
    }
    this.getRelatedTopicsVideoByVideoId = function(data){
        return $http.post(BASE_API + '/videoentity/getRelatedTopicsVideoByVideoId',data);
    }
    this.getRelatedVideosByVideoId = function(data) {
        return $http.post(BASE_API + '/videoentity/getRelatedVideosByVideoId',data);
    }
    this.updateVideoLastViewTime = function(data) {
        return $http.post(BASE_API + '/videoentity/updateVideoLastViewTime',data);
    }
    this.getVideoLastViewTime = function(data) {
        return $http.post(BASE_API + '/videoentity/getVideoLastViewTime',data);
    }


    /* moduleItems api */
    this.updateModuleItems = function(data){
        return $http.post(BASE_API + '/moduleItems/update',data);
    }
    this.getAllModuleItems = function(data){
        return $http.post(BASE_API + '/moduleItems/getAll',data);
    }
    this.getCourseItems = function(data) {
        return $http.post(BASE_API + '/moduleItems/getCourseItems', data).then(function(res) {
            return res.data;
        });
    }
    this.getFullCourseDetails = function(data) {
        return $http.post(BASE_API + '/moduleItems/getFullCourseDetails', data);
    }
    
    this.getModuleItems = function(data) {
        return $http.post(BASE_API + '/moduleItems/getModuleItems', data).then(function(res) {
            return res.data;
        });
    }
    this.getUserBatchEnrollUploads = function(data) {
        return $http.post(BASE_API + '/moduleItems/getUserBatchEnrollUploads',data);
    }

    /* createtest api */
    this.createTest = function(data){
        return $http.post(BASE_API + '/createtest/save',data);
    }
    this.getAllTest = function(data){
        return $http.post(BASE_API + '/createtest/getAll',data);
    }
    this.getTestById = function(data){
        return $http.post(BASE_API + '/createtest/getById',data);
    }

    /*
        CourseBundle API
     */
    this.getAllCourseBundle  = function(data){
        return $http.post(BASE_API + '/courseBundle/getAll',data);
    }
    this.updateCourseBundle  = function(data){
        return $http.post(BASE_API + '/courseBundle/update',data);
    }
    this.removeCourseBundle  = function(data){
        return $http.post(BASE_API + '/courseBundle/remove',data);
    }



    // Employee types api
    this.addEmployeeType  = function(data){
        return $http.post(BASE_API + '/employee/types/add',data);
    }
    this.getAllEmployeeTypes  = function(data){
        return $http.post(BASE_API + '/employee/types/getAll',data);
    }
    this.deleteEmployeeType  = function(data){
        return $http.post(BASE_API + '/employee/types/delete',data);
    }

    // Employee Details api
    this.addEmployee  = function(data){
        return $http.post(BASE_API + '/employee/add',data);
    }
    this.getAllEmployees  = function(data){
        return $http.post(BASE_API + '/employee/getAll',data);
    }
    this.deleteEmployee  = function(data){
        return $http.post(BASE_API + '/employee/delete',data);
    }

    // Employee Skills
    this.addEmployeeSkill  = function(data){
        return $http.post(BASE_API + '/employee/skills/add',data);
    }
    this.getAllEmployeeSkills  = function(data){
        return $http.post(BASE_API + '/employee/skills/getAll',data);
    }
    this.deleteEmployeeSkill  = function(data){
        return $http.post(BASE_API + '/employee/skills/delete',data);
    }

    // All links category
    this.addAllLinksCategory  = function(data){
        return $http.post(BASE_API + '/allLinksCategory/add',data);
    }
    this.getAllLinksCategories  = function(data){
        return $http.post(BASE_API + '/allLinksCategory/getAll',data);
    }
    this.deleteAllLinksCategory  = function(data){
        return $http.post(BASE_API + '/allLinksCategory/delete',data);
    }


    // Sidelinks article
    this.addSideLinksArticlePage  = function(data){
        return $http.post(BASE_API + '/sideLinksArticle/update',data);
    }
    this.getAllSideLinksArticlePage  = function(data){
        return $http.post(BASE_API + '/sideLinksArticle/getAll',data);
    }
    this.getSideLinkArticlePage  = function(data){
        return $http.post(BASE_API + '/sideLinksArticle/get',data);
    }
    this.getMultipleSideLinkArticles = function(data){
        return $http.post(BASE_API + '/sideLinksArticle/getMultipleArticles',data);
    }
    this.deleteSideLinksArticlePage  = function(data){
        return $http.post(BASE_API + '/sideLinksArticle/remove',data);
    }


    // All Links
    this.addLinkToAllLinks  = function(data){
        return $http.post(BASE_API + '/allLinks/update',data);
    }
    this.getAllLinks  = function(data){
        return $http.post(BASE_API + '/allLinks/getAll',data);
    }
    this.deleteLinkFromAllLinks  = function(data){
        return $http.post(BASE_API + '/allLinks/remove',data);
    }


    // BoardCompetitiveCourse
    this.updateBoardCompetitiveCourse  = function(data){
        return $http.post(BASE_API + '/boardCompetitive/update',data);
    }
    this.getBoardCompetitiveCourse  = function(data){
        return $http.post(BASE_API + '/boardCompetitive/get',data);
    }
    this.getBoardCompetitiveCoursesByCourseType  = function(data){
        return $http.post(BASE_API + '/boardCompetitive/getCoursesByCourseType',data);
    }
    this.deleteBoardCompetitiveCourse  = function(data){
        return $http.post(BASE_API + '/boardCompetitive/remove',data);
    }


    // Course Subgroup
    this.addSubgroup  = function(data){
        return $http.post(BASE_API + '/subgroup/add',data);
    }
    this.getAllSubgroup  = function(data){
        return $http.post(BASE_API + '/subgroup/getAll',data);
    }
    this.deleteSubgroup  = function(data){
        return $http.post(BASE_API + '/subgroup/delete',data);
    }

    // Save Batch Course
    this.addBatchCourse  = function(data){
        return $http.post(BASE_API + '/batch_course/add',data);
    }
    this.getAllBatchCourse  = function(data){
        return $http.post(BASE_API + '/batch_course/getAll',data);
    }
    this.deleteBatchCourse  = function(data){
        return $http.post(BASE_API + '/batch_course/delete',data);
    }
    this.getBatchCourse  = function(data){
        return $http.post(BASE_API + '/batch_course/getBatchCourse',data);
    }

    // Save Batch Timing
    this.addBatchTiming  = function(data){
        return $http.post(BASE_API + '/batch_course/timing/add',data);
    }
    this.getAllBatchTiming  = function(data){
        return $http.post(BASE_API + '/batch_course/timing/getAll',data);
    }
    this.deleteBatchTiming  = function(data){
        return $http.post(BASE_API + '/batch_course/timing/delete',data);
    }
    this.getTimingByCourseId = function(data){
        return $http.post(BASE_API + '/batch_course/timing/getTimingByCourseId',data);
    }


    // Save Batch Timing Dashboard
    this.addBatchTimingDashboard  = function(data){
        return $http.post(BASE_API + '/batch_course/timingDashboard/addBatchTimingDashboard',data);
    }
    this.getAllBatchTimingDashboard  = function(data){
        return $http.post(BASE_API + '/batch_course/timingDashboard/getAllBatchTimingDashboard',data);
    }
    this.deleteBatchTimingDashboard  = function(data){
        return $http.post(BASE_API + '/batch_course/timingDashboard/deleteBatchTimingDashboard',data);
    }
    this.setBatchTimingDashboardAsCompleted  = function(data){
        return $http.post(BASE_API + '/batch_course/timingDashboard/setBatchTimingDashboardAsCompleted',data);
    }

    //

    // Currency
    this.addCurrency  = function(data){
        return $http.post(BASE_API + '/batch_course/currency/add',data);
    }
    this.getAllCurrency  = function(data){
        return $http.post(BASE_API + '/batch_course/currency/getAll',data);
    }
    this.deleteCurrency  = function(data){
        return $http.post(BASE_API + '/batch_course/currency/delete',data);
    }

    // Getting STarted
    this.addGettingStarted  = function(data){
        return $http.post(BASE_API + '/batch_course/gettingStarted/add',data);
    }
    this.getAllGettingStarted  = function(data){
        return $http.post(BASE_API + '/batch_course/gettingStarted/getAll',data);
    }
    this.deleteGettingStarted  = function(data){
        return $http.post(BASE_API + '/batch_course/gettingStarted/delete',data);
    }
    this.getGettingStartedByCourseId  = function(data){
        return $http.post(BASE_API + '/batch_course/gettingStarted/getByCourseId',data);
    }

    // Sessions
    this.addSession  = function(data){
        return $http.post(BASE_API + '/batch_course/sessions/add',data);
    }
    this.getAllSessions  = function(data){
        return $http.post(BASE_API + '/batch_course/sessions/getAll',data);
    }
    this.deleteSession  = function(data){
        return $http.post(BASE_API + '/batch_course/sessions/delete',data);
    }
    this.getSessionsByCourseId  = function(data){
        return $http.post(BASE_API + '/batch_course/sessions/getByCourseId',data);
    }

    // Sessions Items
    this.addSessionItems  = function(data){
        return $http.post(BASE_API + '/batch_course/sessionItems/add',data);
    }
    this.getAllSessionItems  = function(data){
        return $http.post(BASE_API + '/batch_course/sessionItems/getAll',data);
    }
    this.deleteSessionItems  = function(data){
        return $http.post(BASE_API + '/batch_course/sessionItems/delete',data);
    }
    this.getSessionDetailsForBatchAndSession  = function(data){
        return $http.post(BASE_API + '/batch_course/sessionItems/getSessionDetailsForBatchAndSession',data);
    }


    // Pricelist Course
    this.savePricelist  = function(data){
        return $http.post(BASE_API + '/pricelist/add',data);
    }
    this.getAllPricelist  = function(data){
        return $http.post(BASE_API + '/pricelist/getAll',data);
    }
    this.deletePricelist  = function(data){
        return $http.post(BASE_API + '/pricelist/delete',data);
    }
    this.getDetailedPricelist = function(data){
        return $http.post(BASE_API + '/pricelist/getDetailedPricelist',data);
    }
    this.getPricelistByCourseId = function(data){
        return $http.post(BASE_API + '/pricelist/getPricelistByCourseId',data);
    }
    this.getDetailedPricelistByCourseId = function(data){
        return $http.post(BASE_API + '/pricelist/getDetailedPricelistByCourseId',data);
    }


    // Bundle
    this.saveBundle  = function(data){
        return $http.post(BASE_API + '/bundle/add',data);
    }
    this.getAllBundle  = function(data){
        return $http.post(BASE_API + '/bundle/getAll',data);
    }
    this.deleteBundle  = function(data){
        return $http.post(BASE_API + '/bundle/delete',data);
    }

    // Email Templates
    this.addEmailTemplates  = function(data){
        return $http.post(BASE_API + '/emailTemplates/addEmailTemplates',data);
    }

    this.getAllEmailTemplates = function(data){
        return $http.post(BASE_API + '/emailTemplates/getAllEmailTemplates',data);
    }

    this.deleteEmailTemplate = function(data){
        return $http.post(BASE_API + '/emailTemplates/deleteEmailTemplate',data);
    }

    // UserEmail Group
    this.addUserEmailGroup  = function(data){
        return $http.post(BASE_API + '/userEmailGroup/addUserEmailGroup',data);
    }

    this.getAllUserEmailGroups = function(data){
        return $http.post(BASE_API + '/userEmailGroup/getAllUserEmailGroups',data);
    }

    this.deleteUserEmailGroup = function(data){
        return $http.post(BASE_API + '/userEmailGroup/deleteUserEmailGroup',data);
    }

    // Send Email Templates
    this.addSendEmailTemplate  = function(data){
        return $http.post(BASE_API + '/sendEmailTemplate/addSendEmailTemplate',data);
    }

    this.getAllSendEmailTemplates = function(data){
        return $http.post(BASE_API + '/sendEmailTemplate/getAllSendEmailTemplates',data);
    }

    this.deleteSendEmailTemplate = function(data){
        return $http.post(BASE_API + '/sendEmailTemplate/deleteSendEmailTemplate',data);
    }

    this.sendEmailTemplatesToUsers = function(data){
        return $http.post(BASE_API + '/sendEmailTemplate/sendEmailTemplatesToUsers',data);
    }

    this.sendNotificationOfEmail = function(data){
        return $http.post(BASE_API + '/sendEmailTemplate/sendNotificationOfEmail',data);
    }

    // Send Notification
    this.addSendNotification  = function(data){
        return $http.post(BASE_API + '/sendNotification/addSendNotification',data);
    }

    this.getAllSendNotifications = function(data){
        return $http.post(BASE_API + '/sendNotification/getAllSendNotifications',data);
    }

    this.deleteSendNotification = function(data){
        return $http.post(BASE_API + '/sendNotification/deleteSendNotification',data);
    }

    this.getSendNotificationByUserId = function(data){
        return $http.post(BASE_API + '/sendNotification/getSendNotificationByUserId',data);
    }

    this.markSendNotificationAsRead = function(data){
        return $http.post(BASE_API + '/sendNotification/markSendNotificationAsRead',data);
    }

    this.getSendNotificationCount = function(data) {
        return $http.post(BASE_API + '/sendNotification/getSendNotificationCount',data);
    }


    // Roles
    this.addRole  = function(data){
        return $http.post(BASE_API + '/adminRoles/addRole',data);
    }

    this.getAllRoles = function(data){
        return $http.post(BASE_API + '/adminRoles/getAllRoles',data);
    }

    this.deleteRole = function(data){
        return $http.post(BASE_API + '/adminRoles/deleteRole',data);
    }


    // forms
    this.addForm  = function(data){
        return $http.post(BASE_API + '/adminForms/addForm',data);
    }

    this.getAllForms = function(data){
        return $http.post(BASE_API + '/adminForms/getAllForms',data);
    }

    this.deleteForm = function(data){
        return $http.post(BASE_API + '/adminForms/deleteForm',data);
    }

    // forms  help
    this.addFormHelp  = function(data){
        return $http.post(BASE_API + '/formsHelp/updateFormHelp',data);
    }

    this.getAllFormsHelp = function(data){
        return $http.post(BASE_API + '/formsHelp/getAllFormsHelp',data);
    }

    this.getFormHelp = function(data){
        return $http.post(BASE_API + '/formsHelp/getFormHelp',data);
    }

    this.deleteFormHelp = function(data){
        return $http.post(BASE_API + '/formsHelp/deleteFormHelp',data);
    }

    // user roles
    this.addUserRole  = function(data){
        return $http.post(BASE_API + '/userRoles/addUserRole',data);
    }

    this.getAllUserRoles = function(data){
        return $http.post(BASE_API + '/userRoles/getAllUserRoles',data);
    }

    this.deleteUserRole = function(data){
        return $http.post(BASE_API + '/userRoles/deleteUserRole',data);
    }

    this.getUserAdminRoleFormAccessList = function(data) {
        return $http.post(BASE_API + '/userRoles/getUserAdminRoleFormAccessList',data);
    }

    // user batch enroll
    this.getUserEnrollAllBatch = function(data) {
        return $http.post(BASE_API + '/userBatchEnroll/getUserEnrollAllBatch',data);
    }
    this.addUserBatchEnroll = function(data) {
        return $http.post(BASE_API + '/userBatchEnroll/addUserBatchEnroll',data);
    }
    this.removeUserBatchEnroll = function(data) {
        return $http.post(BASE_API + '/userBatchEnroll/removeUserBatchEnroll',data);
    }



    this.updateBookTopic = function(data) {
        return $http.post(BASE_API + '/booktopic/updateBookTopic',data);
    }
    this.removeBookTopic = function(data) {
        return $http.post(BASE_API + '/booktopic/removeBookTopic',data);
    }
    this.getAllBookTopics = function(data) {
        return $http.post(BASE_API + '/booktopic/getAllBookTopics',data);
    }
    this.findBookTopicsByQuery = function(data) {
        return $http.post(BASE_API + '/booktopic/findBookTopicsByQuery',data);
    }
    this.findBookTopicById = function(data) {
        return $http.post(BASE_API + '/booktopic/findBookTopicById',data);
    }
    this.findBookTopicsById = function(data) {
        return $http.post(BASE_API + '/booktopic/findBookTopicsById',data);
    }


    this.updatePreviousPaper = function(data) {
        return $http.post(BASE_API + '/previousPapers/update',data);
    }
    this.removePreviousPaper = function(data) {
        return $http.post(BASE_API + '/previousPapers/remove',data);
    }
    this.getAllPreviousPapers = function(data) {
        return $http.post(BASE_API + '/previousPapers/getAll',data);
    }
    this.getPreviousPaper = function(data) {
        return $http.post(BASE_API + '/previousPapers/get',data);
    }


    //Save Purchased Course Based on COurse ids
    this.purchasedCourse = function(data) {
        return $http.post(BASE_API + '/uc/saveCoursesBasedOnCourseId',data);
    }
    //Save purchased courses based on bundle ids
    this.purchasedBundle = function(data){
        return $http.post(BASE_API + '/uc/saveCoursesBasedOnBundleId',data);
    }
    //save purchased courses based on training ids
    this.purchasedTraining = function(data){
        return $http.post(BASE_API + '/uc/saveCoursesBasedOnTrainingId',data);
    }
}
