require('authToken');

angular.module('service').factory('serviceInterceptor', ['authToken','$window',function(authToken,$window) {
    return {
        request: function(config) {
            var token = authToken.getToken();
            if (token)
                config.headers.Authorization = 'JWT ' + token;

            var sessionId = authToken.getSessionId();

            if (sessionId)
                config.headers["x-session-id"] = sessionId;

            config.headers["x-track-session"] = "true";
            return config;
        },
        response: function(response) {
        	var sessionId = response.headers("sess-id");
        	if(sessionId){
				authToken.setSessionId(sessionId)
        	}
            var shouldLogout = response.headers("shouldLogout");
            if(shouldLogout){
                $window.location.href = "/logout/session-expired"
            }

            return response;
        }
    };
}]);

