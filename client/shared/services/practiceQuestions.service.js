'use strict';
require('angular-local-storage');
require('angular-jwt');
angular.module("service")
    .factory('practiceQuestionsService', PracticeQuestionsService);


PracticeQuestionsService.$inject = ['localStorageService', 'jwtHelper'];

function PracticeQuestionsService(localStorageService, jwtHelper) {

    var vm = {};
    var practiceQuestionsKey = "ques"
    var practiceQuestionDataKey = "quesData"

    vm.savePracticeQuestions = function(course,module,item,totalQuestions,questions){
        var obj = {
            courseId : course,
            moduleId : module,
            itemId : item,
            totalQuestions : totalQuestions || questions.length,
            questions : questions,
            questionsId : questions.map(function(v,i){return v.questionId})
        }
        localStorageService.set(practiceQuestionsKey, obj);
    }


    vm.addPracticeQuestions = function(course,module,item,totalQuestions,questions){
        var obj = this.getPracticeQuestions(item)
        
        if(obj){
            if(questions && questions.length > 0){
                questions = questions.filter(function(v,i){
                    if(obj["questionsId"].indexOf(v.questionId) <= -1){
                        obj["questionsId"].push(v.questionId)
                        return true
                    }
                    return false;
                })
            }
            obj["questions"] = obj["questions"] || [];
            obj["questions"] = obj["questions"].concat(questions);
            obj["totalQuestions"] = obj["questions"].length;
            this.savePracticeQuestions(course,module,item,obj["totalQuestions"],obj["questions"])
        }else{
            this.savePracticeQuestions(course,module,item,totalQuestions,questions)
        }
    }

    vm.getPracticeQuestions = function(item){
        var d =  localStorageService.get(practiceQuestionsKey);
        if(d && d.itemId === item){
            return localStorageService.get(practiceQuestionsKey);    
        }
        return undefined
    }

    vm.getPracticeQuestionData = function(item,q_id){
        var d = localStorageService.get(practiceQuestionDataKey);
        if(d && d[q_id]){
            return d[q_id];
        }
        return undefined
    }

    vm.savePracticeQuestionData = function(item,q_id,q_data){
        var d = localStorageService.get(practiceQuestionDataKey);
        d = d || {};
        d[q_id] = q_data;
        localStorageService.set(practiceQuestionDataKey,d);
    }

    vm.getPracticeQuestionByIndex = function(item,index){
        var d = this.getPracticeQuestions(item);
        d = d || {};
        if(d.questions){

            var que = d.questions[index];
            var que_data = this.getPracticeQuestionData(item,que.questionId) || {};
            return angular.merge({},que,que_data);
        }
        return undefined
    }


    return vm;
};