//This Service is used to detect Browser , OS , Resolution , IP , Location of the user
angular
	.module('service')
	.service('infoDetection',InfoDetection);

InfoDetection.$inject = ['$http'];

function InfoDetection($http){

	var self = this;
	self.ip = "";
	self.location = {};
	self.resolution = {};
	self.platform = {};

	self.getResolution = getResolution;
	self.getBrowserAndOs = getBrowserAndOs;
	self.getLocation = getLocation;
	self.getIp = getIp;
	self.getCompatibility = getCompatibility;
	self.init = init;

	//
	var BROWSERS = {
		CHROME : 30,
		FIREFOX : 35,
		SAFARI : 6,
		IE : 12,
		'MICROSOFT EDGE' : 13
	}


	function init(){
		getResolution();
		getBrowserAndOs();
		return getIp()
				.then(getLocation);

	}

	function getBrowserAndOs(){
		self.platform = platform;
	}

	function getResolution(){
		self.resolution = {
			width : screen.width,
			height : screen.height
		}
	}

	function getIp(){
		return $http.get('http://jsonip.com/')
				.then(function success(res){
					self.ip = res.data.ip;
				})
				.catch(function error(err){
					console.error(err);
				})
	}

	function getLocation(){
		return $http.get('http://ip-api.com/json/' + self.ip)
				.then(function success(res){
					self.location = res.data;
				})
				.catch(function error(err){
					console.error(err);
				})
	}

	function getCompatibility(){
		var isIE = false;
		var isModern = false;
		var browser_name = self.platform.name.toUpperCase();
		var browser_version = parseInt(self.platform.version);
		var os = self.platform.os.family.toUpperCase();
		if(browser_name === "IE" || browser_name === "MICROSOFT EDGE"){
			isIE = true;
		}

		if(browser_version >= BROWSERS[browser_name]){
			isModern = true;
		}
		var compability;
		if(os.indexOf("OS X") >= 0){
			compability = "mac+";
		}else{
			compability = "windows+";

			if(isIE){
				compability  += 'ie+';
			}
		}
		if(isModern){
			compability += 'modern';
		}else{
			compability += 'old';
		}

		return compability;
	}

}
