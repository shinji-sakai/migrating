angular.module('service').service('jobService',JobService);

JobService.$inject = ['$http','BASE_API'];

function JobService($http,BASE_API) {
    this.applyForJob = function (data) {
        return $http.post(BASE_API + '/api/job/applyForJob', data);
    }
}