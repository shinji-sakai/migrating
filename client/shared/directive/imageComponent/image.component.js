require('authToken')

angular.module("directive")
    .component('ewImg', {
        template: require('./image.html'),
        controller: Controller,
        controllerAs: 'vm',
        bindings: {
            id : '@',
        }
    });

Controller.$inject = ['authToken'];

function Controller(authToken) {
    var vm = this;
    vm.getImageBaseUrl = authToken.getImageBaseUrl
    vm.url = vm.getImageBaseUrl() + vm.id;
}