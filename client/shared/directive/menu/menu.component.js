require('directiveModule');
require('serviceModule');
require('authToken');
require('service.admin');
require('service.utility');
require('UserCourses');
require('directive.login');
require('./_menu.scss');
require('service.paypal');
require('service.pageLoader');
require('service.contactus');
require("directive.chat");
require('directive.pageLoading');
require('mail');
require("service.chatUtility");


angular.module("directive")
    .component('menuComponent', {
        template: require('./menu.html'),
        controller: MenuController,
        controllerAs: 'vm',
        bindings: {
            menuDelegate: '=',
            showLoginButtons: '@',
            showHeaderButtons: '@'
        }
    });

MenuController.$inject = ['$log','$compile', '$ocLazyLoad', 'authToken', 'admin', 'utility', '$timeout', '$scope', 'userCourses', 'paypal', 'pageLoader', '$filter','Mail','contactus','chatUtility'];

function MenuController($log,$compile, $ocLazyLoad, authToken, admin, utility, $timeout, $scope, userCourses, paypal, pageLoader, $filter,Mail,contactus,chatUtility) {

    ///////////////////////////////////////////////
    //Load Chat Module Seperatly for code splitting
    ///////////////////////////////////////////////
    // $ocLazyLoad.toggleWatch(true);
    // require(['directive.chat'], function() {
    //     //inject using oclazyload
    //     $ocLazyLoad.inject()
    //         .then(function(res) {
    //             //chat html
    //             var html = '<chat show-chat-component="vm.shouldShowChatComponent"></chat>';
    //             //compile html
    //             var ele = $compile(html)($scope);
    //             //append it to parent
    //             $(".menu-component-wrapper").append(ele);
    //         })
    //     $ocLazyLoad.toggleWatch(false);
    // });
    /////////////////////////////////////////////


    var vm = this;
    

    if(window.innerWidth < 1280){
        vm.shouldShowChatComponent = false;
    }else{
        vm.shouldShowChatComponent = false;
    }

    vm.activeNotificationTab = "web";

    
    

    vm.isUserLoggedIn = authToken.isAuthenticated;
    vm.goToLogin = utility.goToLogin;
    vm.goToLogout = utility.goToLogout;
    vm.goToRegister = utility.goToRegister;
    vm.goToAdminApp = utility.goToAdminApp;
    vm.goToDashboardApp = utility.goToDashboardApp;
    vm.getDashboardUrl = utility.getDashboardUrl;
    vm.goToChangePassword = utility.goToChangePassword;
    vm.goToForgotPassword = utility.goToForgotPassword;
    vm.getUserProfileUrl = utility.getUserProfileUrl;
    vm.getNotificationsPageUrl = utility.getNotificationsPageUrl;
    vm.getSpecificNotificationPageUrl = utility.getSpecificNotificationPageUrl;
    vm.getUserId = authToken.getUserId;
    vm.getUser = authToken.getUser;
    vm.getUserPic = authToken.getUserPic;
    vm.goToURL = goToURL;
    vm.getCareerBookUrl = utility.getCareerBookUrl;
    vm.goToCareerBook = utility.goToCareerBook;
    vm.trainingURL = trainingURL;
    vm.trainingCourseURL = trainingCourseURL;
    vm.getQAUrl = utility.getQAUrl;

    vm.toggleHiddenMenu = toggleHiddenMenu;
    //menu delegate to access in parent page
   
    vm.toggleSmallScreenTab = toggleSmallScreenTab;
    vm.closeSmallScreenTab = closeSmallScreenTab;

    vm.openAllLinks = openAllLinks;
    vm.closeAllLinks = closeAllLinks;

    vm.getSendNotificationCount = getSendNotificationCount;
    vm.getNotificationByUserId = getNotificationByUserId;
    vm.markSendNotificationAsRead = markSendNotificationAsRead;

    vm.toggleContactUsModal = toggleContactUsModal;

    vm.toggleSmallMenuModal = toggleSmallMenuModal;
    vm.toggleUserMenuModal = toggleUserMenuModal;

    vm.openSubscribeModal = openSubscribeModal;
    vm.closeSubscribeModal = closeSubscribeModal;
    vm.validateInputField = validateInputField;
    vm.subscribeUser = subscribeUser;


    vm.$onInit = function() {
        vm.showLoginButtons = (vm.showLoginButtons === "false") ? false : true;
        vm.showHeaderButtons = (vm.showHeaderButtons === "false") ? false : true;
        vm.menuDelegate = vm.menuDelegate || {};
        vm.toggleChatModal = vm.menuDelegate.toggleChatModal = toggleChatModal;
        vm.menuDelegate.toggleMenuModal = vm.toggleMenuModal = toggleMenuModal;
        vm.menuDelegate.openMenuModal = vm.openMenuModal = openMenuModal;
        vm.menuDelegate.closeMenuModal = vm.closeMenuModal = closeMenuModal;
        vm.menuDelegate.openNotificationModal = vm.openNotificationModal = openNotificationModal;
        vm.menuDelegate.toggleNotificationModal = vm.toggleNotificationModal = toggleNotificationModal;

        init();
        getContactUsDetails();
        // getAllLinks(); 
        if(vm.isUserLoggedIn()){
            getSendNotificationCount();    
        }
    }

    function getContactUsDetails(argument) {
        contactus.getContactUsDetails()
            .then(function(res) {
                vm.contactusDetails = res.data[0] || {};
            })
            .catch(function(err) {
                $log.error(err);
            })
    }

    function init() {
        //bind document click
        //when user click outside of menu then hide it
        $(document).on('click', function(e) {

            // console.log("document click menu")

            var captureClick = $(e.target).attr("data-capture-document-click");

            if (!captureClick) {
                element = $(e.target).closest("[data-capture-document-click]");
                captureClick = $(element).attr("data-capture-document-click");
            }

            if (captureClick === "false") {
                return;
            }

            resetMenuModals();

            $scope.$apply();

            if (!vm.isMenuModalOpen) {
                //if it is not opened then return
                return;
            }
            toggleMenuModal();

            $scope.$apply();
        })

        $(".modal-close-btn").on('click', function(e) {
            e.stopPropagation();
        })

        //if user click on menu modal
        //then don't propagate that click to document
        $("#menu-modal").on('click', function(e) {
            e.stopPropagation();
        })

        $(".menu-component-wrapper").on('click', function(e) {
            // e.stopPropagation();
        })

        //fetch all courses
        admin.courselistfull()
            .then(function(d) {
                vm.courses = d;
            })
            .catch(function(d) {
                console.error(d);
            })

        // if (vm.isUserLoggedIn) {
        //     userCourses.getPurchasedCourses({
        //             userId: authToken.getUserId()
        //         })
        //         .then(function(d) {
        //             vm.myCourses = d.data;
        //         })
        //         .catch(function(err) {
        //             console.error(err);
        //         })
        // }
    }

    //toggle hidden menu
    function toggleHiddenMenu($event) {
        var target = $event.target;
        //find from clicked
        var menu = $(target).find('.hidden-menu');

        //if not found any hidden element
        if (menu.length === 0) {
            //try from finding from parent
            menu = $(target).parent().siblings('.hidden-menu');
        }

        if (target.tagName === 'A') {
            // console.log("toggling", target);
            $event.stopPropagation();
            // don't toggle
            return;
        }

        //hide all menu with 'hidden-menu' class
        $('.menu-component-wrapper').find('.hidden-menu').each(function(i, v) {
            if ($(v)[0] != $(menu)[0]) {
                $(v).hide();
            }
        });

        $(menu).toggle();

    }

    //toggle entire menu modal
    function toggleMenuModal(evt) {
        console.log("toggleMenuModal")
        if (evt) {
            evt.stopPropagation();
        }
        if (!vm.isMenuModalOpen) {
            //It is not opened
            //Now it is going to open
            openMenuModal();
        } else {
            //if it is opened
            //then close it
            closeMenuModal();
        }
    }

    function openMenuModal() {
        resetMenuModals();
        vm.isMenuModalOpen = true;
        // $("#menu-modal").css('width', '40%').css('min-width', '1000px');
        // setTimeout(function() {
        //     // $("#menu-modal").css('min-width','500px');
        // });
        // $timeout(function() {
        //     vm.isMenuModalOpen = true;
        // }, 300);
    }

    function closeMenuModal() {
        // $("#menu-modal").width(0).css('min-width', '0px');;
        vm.isMenuModalOpen = false;
    }

    function goToURL(url) {
        window.location = url;
    }

    function trainingURL(type) {
        var url = '/' + type + '-trainings';
        return url;
    }

    function trainingCourseURL(type, courseId, courseGroup) {
        var url = '/' + type + '/' + courseGroup + '-trainings/' + courseId;
        return url;
    }

    function toggleSmallScreenTab(tab) {
        if (vm.smallScreenActiveTab && vm.smallScreenActiveTab != tab) {
            vm.smallScreenActiveTab = tab;
            return;
        }
        if (vm.smallScreenActiveTab) {
            closeSmallScreenTab();
        } else {
            openSmallScreenTab(tab);
        }
    }

    function openSmallScreenTab(tab) {
        vm.smallScreenActiveTab = tab;
        $('.ew-tab-content-wrapper').addClass('ew-open');
    }

    function closeSmallScreenTab() {
        $('.ew-tab-content-wrapper').removeClass('ew-open');
        vm.smallScreenActiveTab = '';
    }

    function openAllLinks() {
        vm.shouldShowAllLinksModal = true;
    }

    function closeAllLinks() {
        vm.shouldShowAllLinksModal = false;
    }

    function getAllLinks() {
        admin.getAllLinksCategories()
            .then(function(res) {
                vm.allLinksCategories = res.data;
                //get all links
                admin.getAllLinks()
                    .then(function(res) {
                        var links = res.data;
                        var allLinksMap = {};
                        links.forEach(function(v, i) {
                            var cat_obj = $filter('filter')(vm.allLinksCategories, {
                                cat_id: v.link_cat
                            })[0] || {};
                            allLinksMap[cat_obj.cat_nm] = allLinksMap[cat_obj.cat_nm] || [];
                            allLinksMap[cat_obj.cat_nm].push(v);
                        })
                        vm.allLinks = allLinksMap;
                    })
                    .catch(function(err) {
                        console.log(err);
                    })
            })

    }

    function openNotificationModal() {
        resetMenuModals();
        if (!vm.allNotifications) {
            getNotificationByUserId();
        }
        vm.showNotificationModal = true;
    }

    function closeNotificationModal() {
        vm.showNotificationModal = false;
    }

    function toggleNotificationModal() {
        if (!vm.showNotificationModal) {
            openNotificationModal();
        } else {
            closeNotificationModal();
        }
    }

    

    function getSendNotificationCount() {
        admin.getSendNotificationCount({
                usr_id: authToken.getUserId()
            })
            .then(function(res) {
                var rows = res.data;
                if(rows[0]){
                    vm.notificaionCount = rows[0].noti_cnt;    
                }
                
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function getNotificationByUserId() {
        admin.getSendNotificationByUserId({
                usr_id: authToken.getUserId()
            })
            .then(function(res) {
                vm.allNotifications = res.data;
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function markSendNotificationAsRead($event,usr_noti_pk) {
        $event.stopPropagation();
        $event.preventDefault();

        admin.markSendNotificationAsRead({
                usr_id: authToken.getUserId(),
                usr_noti_pk: usr_noti_pk
            })
            .then(function(res) {
                getNotificationByUserId();
            })
            .catch(function(err) {})
    }

    function resetMenuModals(){
        closeNotificationModal();
        closeContactUsModal();
        closeSmallMenuModal();
        closeMenuModal();
        closeUserMenuModal();
    }

    function openContactUsModal() {
        resetMenuModals()
        vm.showContactUsModal = true;
    }

    function closeContactUsModal() {
        vm.showContactUsModal = false;
    }

    function toggleContactUsModal() {
        if (!vm.showContactUsModal) {
            openContactUsModal();
        } else {
            closeContactUsModal();
        }
    }

    function openSmallMenuModal() {
        resetMenuModals();
        vm.showSmallMenuModal = true;
    }

    function closeSmallMenuModal() {
        vm.showSmallMenuModal = false;
    }

    function toggleSmallMenuModal() {
        if (!vm.showSmallMenuModal) {
            openSmallMenuModal();
        } else {
            closeSmallMenuModal();
        }
    }


    function openUserMenuModal() {
        resetMenuModals();
        vm.showUserMenuModal = true;
    }

    function closeUserMenuModal() {
        vm.showUserMenuModal = false;
    }

    function toggleUserMenuModal() {
        if (!vm.showUserMenuModal) {
            openUserMenuModal();
        } else {
            closeUserMenuModal();
        }
    }


    function toggleChatModal() {
        vm.shouldShowChatComponent = !vm.shouldShowChatComponent;
    }


    function validateInputField() {

        var input = vm.subscribeInput;
        console.log(input);
        if(input.length <= 0 || !validateEmail(input)){
            vm.showInputErrorMsg = true;
        }else{
            vm.showInputErrorMsg = false;
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function subscribeUser() {
        if(vm.showInputErrorMsg){
            return;
        }
        Mail.subscribeUser({
            subs_email : vm.subscribeInput,
            subs_typ : vm.subs_typ
        })
        .then(function(res) {
            vm.showSuccessMsg = true;
            vm.showErrorMsg = false;
            vm.subscribeInput= "";
            vm.showInputErrorMsg = false;
            vm.subs_typ = "";
        })
        .catch(function(err) {
            vm.showErrorMsg = true;
            vm.showSuccessMsg = false;
        })
    }

    function openSubscribeModal(title,type) {
        closeMenuModal();
        vm.subs_typ = type;
        vm.showSuccessMsg = false;
        vm.showInputErrorMsg = false;
        vm.showErrorMsg = false;
        vm.subscribeInput= "";
        vm.shouldShowSubscribeModal = true;
        vm.subscribeTitle = title + " will be live shortly. Please Subscribe for Updates";
    }
    function closeSubscribeModal() {
        vm.shouldShowSubscribeModal = false;
        vm.showInputErrorMsg = false;
        vm.subscribeInput= "";
        vm.subs_typ = "";
    }
    $('body').on('click','.chat_fixed', function(){
        $('.fa-angle-up').click();
    });
    var chatHistory = chatUtility.getInfo();
    if (chatHistory!=null) {
        if (chatHistory.isOpenDialog==true) {
            vm.shouldShowChatComponent = true;
        }   
    };
    $('body').on('click','.chat_fixed', function(){
        $('.fa-angle-up').click();
        chatUtility.isOpenDialog(vm.shouldShowChatComponent);
    });
    $('body').on('click','.logout', function(){
        chatUtility.clearChatInfo();
    });
}