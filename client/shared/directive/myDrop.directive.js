window.Drop = require('tether-drop');
require('tether-drop/dist/css/drop-theme-arrows-bounce-dark.css');
require('angular-drop');

var app = angular
    .module("directive");

app.directive('myDrop', ['dropWrapper', function($drop) {
    return {
        restrict: 'EA',
        scope: {
            elem: '=myDrop',
            fn: '=callback',
            content : '='
        },
        link: function(scope, elem) {
        	scope.$watch(function(){
        		return scope.content;
        	}, function(newValue, oldValue, scope) {
        		if(newValue){
        			createDrop();	
        		}
        	});

        	function createDrop(){
        		if(scope.drop){
        			scope.drop.destroy();
        		}
        		var drop = $drop({
	                target: elem,
	                scope: scope,
	                template: scope.content,
	                position: 'top center',
	                constrainToWindow: true,
	                constrainToScrollParent: true,
	                classes: 'drop-theme-arrows-bounce-dark',
                    blurDelay : 1000,
	                tetherOptions: {},
	            });

	            scope.drop = drop;
	            scope.drop.open();
	            scope.custom_close = function(text) {
                    scope.fn(text);
                    scope.drop.close();
                }
                    // easy way
	                // elem.on('click', drop.toogle);
	                // hard way
	            elem.on('click', function(event) {
                    console.log(event);
	                if (scope.drop.isOpened())
	                    scope.drop.close();
	                else
	                    scope.drop.open();
	            });
        	}

        }
    };
}]);