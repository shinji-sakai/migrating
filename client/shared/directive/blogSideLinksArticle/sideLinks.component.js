require('directiveModule');
require('serviceModule');
require('authToken');
require('directive.itemComments');
require('service.itemQueries');
require('service.utility');
require('angular-socialshare');
require('mail');
require('angular-froala');
require('service.blog');
require('service.notes');
require('./_sideLinks.scss');

angular.module("directive")
    .component('ewArticle', {
        template: require('./sideLinks.html'),
        controller: Controller,
        controllerAs: 'vm',
        bindings: {
            delegate : "=?",
            content: "@",
            name: "@?",
            articleId: "@?",
            showSidebar: "@?"
        }
    });

Controller.$inject = ['$log', '$scope', 'itemQueries', 'authToken', 'utility', 'Mail', 'Socialshare','EditorConfig','blog','notesService','alertify'];

function Controller($log, $scope, itemQueries, authToken, utility, Mail, Socialshare,EditorConfig,blogService,notesService,alertify) {

    var vm = this;

    var MainClass = ".blog-side-link-article-wrapper";

    $scope.$watch(function() {
        return vm.content
    }, function(newValue, oldValue, scope) {
        if (newValue.length > 0) {
            vm.renderDOM();
        }
    });

    vm.$onInit = function() {
        vm.EditorConfig = EditorConfig;
        EditorConfig.toolbarButtons = EditorConfig.toolbarButtonsMD = EditorConfig.toolbarButtonsSM = EditorConfig.toolbarButtonsXS = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertImage', 'insertVideo', 'insertTable', 'html'],
        EditorConfig.quickInsertButtons = []
        EditorConfig.pluginsEnabled = ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"]
        vm.delegate = vm.delegate || {};
        vm.activeRelatedTab = "comments";
        vm.activeItem = vm.articleId;
        vm.trustAsHTML = utility.trustAsHTML;
        vm.renderDOM = renderDOM;
        vm.saveQueryForItem = saveQueryForItem;
        vm.getQueriesForItem = getQueriesForItem;
        vm.scrollToTab = vm.delegate.scrollToTab = scrollToTab;
        vm.subscribeUser = subscribeUser;
        vm.validateInputField = validateInputField;
        vm.getQAQuestionPageUrl = utility.getQAQuestionPageUrl;
    }

    function renderDOM() {
        var current_end_pos = 0;
        var indexPos = 0;
        var link_arr = link_arr || [];
        var content_arr = content_arr || [];

        while (current_end_pos <= vm.content.length) {
            var start_pos_of_hash = vm.content.indexOf("###", indexPos);
            var end_pos_of_hash = start_pos_of_hash + 3;

            var start_pos_of_next_hash = vm.content.indexOf("###", start_pos_of_hash + 1);
            var end_pos_of_next_hash = start_pos_of_next_hash + 3;

            if (start_pos_of_next_hash === -1) {
                break;
            }

            var link_title = vm.content.substr(end_pos_of_hash, (start_pos_of_next_hash - end_pos_of_hash));

            link_arr.push(link_title);

            var start_pos_of_next_next_hash = vm.content.indexOf("###", start_pos_of_next_hash + 1);
            var end_pos_of_next_next_hash = current_end_pos = start_pos_of_next_next_hash + 3;

            var content_text = vm.content.substr(end_pos_of_next_hash, (start_pos_of_next_next_hash - end_pos_of_next_hash));

            content_arr.push(content_text);

            indexPos = start_pos_of_next_next_hash - 1;
        }

        vm.link_arr = link_arr;
        vm.content_arr = content_arr;


        setTimeout(function() {
            bindOnClickEvent();
            $("table").each(function(index, block) {
                $(block).addClass("table").addClass("table-striped table-hover").wrap("<div class='table-responsive'></div>");
            })
        })
    }

    function renderDOM_old() {
        var link_arr = link_arr || [];
        var content_arr = content_arr || [];
        $("div[ew-section='']").each(function(i, ele) {
            var link_title = $(ele).attr("title");
            var id = "link--" + i;

            var anchorTag = $("<a></a>");
            $(anchorTag).attr('data-id', id).html(link_title);
            link_arr.push(anchorTag);

            var div_ele = $("<div></div>");
            $(div_ele).attr('id', id).html($(this).html());
            content_arr.push(div_ele);
        })
        if (link_arr.length > 0 && content_arr.length > 0) {
            console.log(link_arr, content_arr);
            var link_wrapper = $("<div></div>").addClass("links").append(link_arr);
            var content_wrapper = $("<div></div>").addClass("links-content-wrapper").append(content_arr);
            $(MainClass).html(link_wrapper).append(content_wrapper);
        }
    }

    function bindOnClickEvent() {
        vm.linksWidth = $(".links").width()
        $("[data-id]").on('click', function(e) {
            var id = $(this).data("id");
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top - 100
            }, 'slow');
        })
    }

    function bindScrollEvent() {
        var linksOffset = $(".links").offset();
        $(window).scroll(function() {
            var scrollTop = $("body").scrollTop() + 60;
            var wrapperTop = $(MainClass).offset().top;

            if (window.innerWidth > 767) {
                if ($("body").scrollTop() === 0) {
                    $(".links").css("position", "initial").css("width", '25%');
                    $(".links-content-wrapper").removeClass("fixed-side");
                }
                if ((scrollTop - wrapperTop) > 0) {
                    $(".links").css("position", "fixed").css("width", '20%');
                    $(".links-content-wrapper").addClass("fixed-side");
                } else if ((scrollTop - wrapperTop) < 0) {
                    $(".links").css("position", "initial").css("width", '25%');
                    $(".links-content-wrapper").removeClass("fixed-side");
                }
            } else {
                $(".links").css("position", "initial").css("width", '100%');
                $(".links-content-wrapper").removeClass("fixed-side");
            }
        })
    }

    function scrollToTab(tab) {
        if(tab !== 'comments' && !authToken.isAuthenticated()){
            vm.showLoginAlertModal = true;
            vm.needLoginErrModalMsg = "Need to login to access " + tab;
            return;
        }
        vm.showLoginAlertModal = false;
        vm.activeRelatedTab = tab;
        if(tab === 'qa'){
            vm.getQueriesForItem();
        }else if(tab === 'notes'){
            getNotes();
        }
        $('html,body').animate({
            scrollTop: $("#tabs").offset().top - 100
        }, 'slow');
    }

    function saveQueryForItem() {
        var obj = {
            itm_id: vm.activeItem,
            usr_id: authToken.getUserId() || 'n/a',
            qry_title: vm.queTitle,
            crs_id : 'blog',
            mdl_id : vm.articleId,
            crs_nm : 'Blog',
            mdl_nm : vm.name
        }
        itemQueries.save(obj)
            .then(function(res) {
                vm.itemQueries = vm.itemQueries || [];
                vm.itemQueries.push(res.data);
                vm.queTitle = "";
                getQueriesForItem();
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function getQueriesForItem() {
        var obj = {
            itm_id: vm.activeItem,
        }
        itemQueries.getAll(obj)
            .then(function(res) {
                vm.itemQueries = res.data;
                console.log(res.data);
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    // function subscribeUser() {
    //     vm.subscribeStatus = undefined;
    //     Mail.subscribeUser({
    //             usr_email: vm.subscribeEmail
    //         })
    //         .then(function(res) {
    //             vm.subscribeEmail = "";
    //             vm.subscribeStatus = true;
    //             $log.debug(res);
    //         })
    //         .catch(function(err) {
    //             vm.subscribeStatus = false
    //             $log.error(err);
    //         })
    // }

    vm.shareToFB = shareToFB;
    vm.shareToTwitter = shareToTwitter;
    vm.shareToEWProfile = shareToEWProfile;
    vm.shareToQA = shareToQA;


    function shareToFB(title) {
        // e.stopPropagation();
        var tempDiv = $("<div></div>");
        tempDiv.html(vm.desc || '');
        var desc = tempDiv.text();

        Socialshare.share({
            'provider': 'facebook',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText': title,
                'socialshareDescription': desc,
                'socialshareType': 'feed',
                'socialshareVia': '1680795622155218',
                'socialshareMedia': 'http://www.digitalplatforms.co.za/wp-content/uploads/2014/09/laptop-on-work-desk.jpg',
                'socialsharePopupHeight': '500',
                'socialsharePopupWidth': '500',
            }
        });
    }

    function shareToTwitter(title) {
        // e.stopPropagation();
        Socialshare.share({
            'provider': 'twitter',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText': title,
                'socialshareVia': 'hcPYSQevRhsR8016f0MllEWbU',
                'socialshareHashtags': 'examwarrior , ' + title,
                'socialsharePopupHeight': '500',
                'socialsharePopupWidth': '500',
            }
        });
    }

    function shareToEWProfile(title) {
        // e.stopPropagation();
        var title = title;
        var tempDiv = $("<div></div>");
        tempDiv.html(vm.desc || "");
        var desc = tempDiv.text();
        utility.shareToEWProfile(title, desc);
    }

    function shareToQA(title) {
        // e.stopPropagation();
        var title = title;
        var tempDiv = $("<div></div>");
        tempDiv.html(vm.desc || "");
        var desc = tempDiv.text();
        utility.shareToQA(title, desc);
    }


    function validateInputField() {

        var input = vm.subscribeInput;
        console.log(input);
        if(input.length <= 0 || !validateEmail(input)){
            vm.showInputErrorMsg = true;
        }else{
            vm.showInputErrorMsg = false;
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function subscribeUser() {
        if(vm.showInputErrorMsg || !vm.subscribeInput || (vm.subscribeInput && vm.subscribeInput.length <= 0)){
            vm.showInputErrorMsg = true;
            return;
        }
        Mail.subscribeUser({
            subs_email : vm.subscribeInput,
            subs_typ : 'blog'
        })
        .then(function(res) {
            vm.showSuccessMsg = true;
            vm.showErrorMsg = false;
            vm.subscribeInput= "";
            vm.showInputErrorMsg = false;
            vm.subs_typ = "";
        })
        .catch(function(err) {
            vm.showErrorMsg = true;
            vm.showSuccessMsg = false;
        })
    }

    function openSubscribeModal(title,type) {
        vm.subs_typ = type;
        vm.showSuccessMsg = false;
        vm.showInputErrorMsg = false;
        vm.showErrorMsg = false;
        vm.subscribeInput= "";
        vm.shouldShowSubscribeModal = true;
        vm.subscribeTitle = title + " will be live shortly. Please Subscribe for Updates";
    }

    function closeSubscribeModal() {
        vm.shouldShowSubscribeModal = false;
        vm.showInputErrorMsg = false;
        vm.subscribeInput= "";
        vm.subs_typ = "";
    }

    var getNotes = function(){
        var dataToSend = {
            "usr_id": authToken.getUserId() || 'n/a',
            "src_id": vm.articleId
        }
        notesService.getNotes(dataToSend)
            .then(function(res){
                vm.notes = res.data;
            })
            .catch(function(err){
                $log.error(err)
            })
    }

    vm.addNotes = function(){
        vm.isAddingNote = true;
        var dataToSend = {
            "usr_id": authToken.getUserId() || 'n/a',
            "src_id": vm.articleId,
            "note": vm.noteDesc,
            "note_src" : "blog"
        }
        notesService.insertNote(dataToSend)
            .then(function(res){
                vm.notes = vm.notes || []
                vm.notes.push(res.data)
                vm.noteDesc = "";
                alertify.success("Note saved successfully")
            })
            .catch(function(err){
                $log.error(err)
                alertify.error("Error Occured")
            })
            .finally(function(err){
                vm.isAddingNote = false;
            })
    }

    vm.editNote = function(note_id){
        vm.editingNote = note_id;
    }

    vm.saveNote = function(note){
        var dataToSend = {
            "usr_id": authToken.getUserId(),
            "src_id": note.src_id,
            "note": note.note,
            "note_id" : note.note_id
        }
        notesService.updateNote(dataToSend)
            .then(function(res){
                vm.editingNote = undefined
                alertify.success("Note updated successfully")
            })
            .catch(function(err){
                $log.error(err)
                alertify.error("Error Occured")
            })
    }

    vm.showRemoveNoteConfirmModal = function(note){
        vm.deletingNote = note;
        vm.shouldShowRemoveNoteConfirmModal = true;
    }

    vm.closeRemoveNoteConfirmModal = function(){
        vm.deletingNote = undefined;
        vm.shouldShowRemoveNoteConfirmModal = false;   
    }

    vm.removeNote = function(){
        var dataToSend = {
            "usr_id": authToken.getUserId(),
            "src_id": vm.deletingNote.src_id,
            "note_id" : vm.deletingNote.note_id
        }
        vm.isRemovingNote = true;
        notesService.deleteNote(dataToSend)
            .then(function(res){
                vm.notes = vm.notes.filter(function(v,i){
                    return v.note_id != vm.deletingNote.note_id
                })
                vm.shouldShowRemoveNoteConfirmModal = false;
                alertify.success("Note removed successfully")
            })
            .catch(function(err){
                $log.error(err)
                alertify.error("Error Occured")
            })
            .finally(function(){
                vm.isRemovingNote = false;
            })
    }


}