angular
    .module('directive')
    .directive('scrollSpy', ScrollSpyDirective);


function ScrollSpyDirective() {
    return {
        restrict: 'A',
        link: {
            post: function postLink(scope, element, attrs) {
                $(window).scroll(function() {
                    var scrollPos = $(window).scrollTop();
                    var height = $(window).height();
                    var number = Math.floor(scrollPos / height);
                    var children = element.children();
                    children.each(function(index,ele){
                        if(index == number && !($(ele).hasClass('active'))){
                            $(ele).parent().find('.active').removeClass("active");
                            $(ele).addClass('active');
                        }
                    })
                });
            }
        }
    };
}