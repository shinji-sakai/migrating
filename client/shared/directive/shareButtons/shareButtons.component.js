require('./_shareButtons.scss');
require('angular-socialshare');
require('service.utility');

angular.module("directive")
    .component('shareButtons', {
        template: require('./shareButtons.html'),
        controller: Controller,
        controllerAs: 'vm',
        bindings: {
            desc : '@',
            title : '@'
        }
    });

Controller.$inject = ['$log','Socialshare','utility'];

function Controller($log,Socialshare,utility) {
    var vm = this;

    vm.title = vm.title || "";
    vm.desc = vm.desc || "";

    vm.shareToFB = shareToFB;
    vm.shareToTwitter = shareToTwitter;
    vm.shareToEWProfile = shareToEWProfile;
    vm.shareToQA = shareToQA;


    function shareToFB(e) {
        // e.stopPropagation();
        var tempDiv = $("<div></div>");
        tempDiv.html(vm.desc);
        var desc = tempDiv.text();

        Socialshare.share({
            'provider': 'facebook',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText' : vm.title,
                'socialshareDescription' : desc,
                'socialshareType' : 'feed',
                'socialshareVia' : '1680795622155218',
                'socialshareMedia' : 'http://www.digitalplatforms.co.za/wp-content/uploads/2014/09/laptop-on-work-desk.jpg',
                'socialsharePopupHeight' : '500',
                'socialsharePopupWidth' : '500',
            }
        });
    }

    function shareToTwitter(e) {
        // e.stopPropagation();
        Socialshare.share({
            'provider': 'twitter',
            'attrs': {
                'socialshareUrl': window.location.href,
                'socialshareText' : vm.title,
                'socialshareVia' : 'hcPYSQevRhsR8016f0MllEWbU',
                'socialshareHashtags' : 'examwarrior , ' + vm.title,
                'socialsharePopupHeight' : '500',
                'socialsharePopupWidth' : '500',
            }
        });
    }

    function shareToEWProfile(e){
        // e.stopPropagation();
        var title = vm.title; 
        var tempDiv = $("<div></div>");
        tempDiv.html(vm.desc);
        var desc = tempDiv.text();
        utility.shareToEWProfile(title,desc);
    }

    function shareToQA(e){
        // e.stopPropagation();
        var title = vm.title; 
        var tempDiv = $("<div></div>");
        tempDiv.html(vm.desc);
        var desc = tempDiv.text();
        utility.shareToQA(title,desc);
    }
}