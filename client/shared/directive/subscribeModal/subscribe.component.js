require('directiveModule');
require('serviceModule');
require('service.utility');
require('service.admin');
require("authToken")
require('mail');
require('./_subscribe.scss');

angular.module("directive")
    .component('subscribeModalComponent', {
        template: require('./subscribe.html'),
        controller: SubscribeComponent,
        controllerAs: 'vm',
        bindings: {
            modalType : '@'
        }
    });

SubscribeComponent.$inject = ['$log','$compile','utility','authToken','admin','Mail'];

function SubscribeComponent($log,$compile,utility,authToken,admin,Mail) {

    var vm = this;


    vm.validateInputField = function() {
        var input = vm.subscribeInput;

        if(input.length <= 0 || !vm.validateEmail(input)){
            vm.showInputErrorMsg = true;
        }else{
            vm.showInputErrorMsg = false;
        }
    }

    vm.validateEmail = function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    vm.resetData = function() {
        vm.showSuccessMsg = false;
        vm.showErrorMsg = false;
        vm.subscribeInput= "";
        vm.showInputErrorMsg = false;
        vm.subs_typ = "";
    }

    vm.subscribeUser = function() {
        if(vm.showInputErrorMsg){
            return;
        }
        Mail.subscribeUser({
            subs_email : vm.subscribeInput,
            subs_typ : vm.modalType
        })
        .then(function(res) {
            vm.showSuccessMsg = true;
            vm.showErrorMsg = false;
            vm.subscribeInput= "";
            vm.showInputErrorMsg = false;
            vm.subs_typ = "";
        })
        .catch(function(err) {
            vm.showErrorMsg = true;
            vm.showSuccessMsg = false;
        })
    }
}

