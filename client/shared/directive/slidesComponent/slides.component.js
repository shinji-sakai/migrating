require('service.fullscreen');
require('ErrImage');
require('./_slide_continue.scss');

/*
Example : 
<slides-component 
    topic-title="vm.topicTitle" 
    slide-desc="vm.slideDesc" 
    slides="vm.slides"
    close-modal-event-delegate = "vm.closeSlideModal()"
    template="continue/step"
     />
 */

angular.module('directive')
    .component('slidesComponent', {
    	bindings: {
            slides : '=',
            topicTitle : '=',
            slideDesc : '=',
            closeModalEventDelegate : '&'
        },
        template: ['$element','$attrs',function($element, $attrs) {
            console.log($attrs.template);
            if($attrs.template === "continue"){ 
                return require('./slides_continue.html');
            }else if($attrs.template === "step"){
                return require('./slides.html');
            }else{
                //Default
                return require('./slides.html');
            }
        }],
        controller: SlidesController,
        controllerAs: 'vm'
    });

SlidesController.$inject = ['fullScreen','$sce'];

function SlidesController(fullScreen,$sce) {
    var vm = this;

    vm.currentSlide = 0;
    vm.isLastSlide = false;
    vm.isFirstSlide = true;
    vm.slider_opened = true;
    vm.isSlideFullScreen = false;
    vm.slidination = {
        start: 1,
        maximum: 10,
        shouldShowSlidinationArrow: false
    }

    //Modal Functions
    vm.trustAsHTML = function(html){
        return $sce.trustAsHtml(html);
    }
    vm.firstSlide = firstSlide;
    vm.prevSlide = prevSlide;
    vm.nextSlide = nextSlide;
    vm.lastSlide = lastSlide;
    vm.changeState = changeState;
    vm.goToSlideNumber = goToSlideNumber;
    vm.toggleSlideDesc = toggleSlideDesc;
    // vm.toggleSlidination = toggleSlidination;
    vm.closeModal = closeModal;
    vm.slideFullScreen = slideFullScreen;
    vm.isSlideNumberInRange = isSlideNumberInRange;
    vm.slidinationPrevArrow = slidinationPrevArrow;
    vm.slidinationNextArrow = slidinationNextArrow;
    vm.increaseSidebarWidth = increaseSidebarWidth;
    vm.decreaseSidebarWidth = decreaseSidebarWidth;


    //Modal Functions
    //
    $(document.documentElement).on('fullscreenchange', function(e, isFullScreen) {
        if (isFullScreen) {
            vm.isSlideFullScreen = true;
        } else {
            vm.isSlideFullScreen = false;
        }
    });

    function keyEvents() {
        var codes = {
            LEFT: 37,
            RIGHT: 39,
        }
        var target = $('.note-modal-wrapper');
        $(target).on('keydown', function(e) {
            if (e.keyCode == codes.LEFT) {
                $scope.$apply(function() {
                    vm.slidinationPrevArrow();
                });

            } else if (e.keyCode == codes.RIGHT) {
                $scope.$apply(function() {
                    vm.slidinationNextArrow();
                });

            }
        })

    }

    function firstSlide() {
        vm.currentSlide = 0;
        vm.changeState();
    }

    function prevSlide(callingFrom) {
        if (!callingFrom) {
            if (vm.slidination.start > 1) {
                vm.slidination.start -= 1;
            }
        }

        if (vm.currentSlide <= 0) vm.currentSlide = 0;
        else vm.currentSlide--;
        vm.changeState();
        setTimeout(function() {
            Prism.highlightAll(false);
        }, 10);
    }

    function nextSlide(callingFrom) {
        if (!callingFrom) {
            if (vm.slidination.start + vm.slidination.maximum - 1 < vm.slides.length) {
                vm.slidination.start += 1;
            }
        }

        if (vm.currentSlide >= vm.slides.length - 1) vm.currentSlide = vm.slides.length - 1;
        else vm.currentSlide++;
        vm.changeState();
        setTimeout(function() {
            Prism.highlightAll(false);
        }, 10);

    }

    function lastSlide() {
        vm.currentSlide = vm.slides.length - 1;

        vm.changeState();
    }

    function goToSlideNumber(number) {
        vm.currentSlide = number;
        vm.changeState();
    }

    function changeState() {
        if (vm.currentSlide == 0) {
            vm.isFirstSlide = true;
            vm.isLastSlide = false;
        } else if (vm.currentSlide === vm.slides.length - 1) {
            vm.isFirstSlide = false;
            vm.isLastSlide = true;
        } else {
            vm.isFirstSlide = false;
            vm.isLastSlide = false;
        }
    }

    function closeModal() {
        vm.closeModalEventDelegate();
    }

    function toggleSlideDesc() {
        var image_wrapper = $('.slide-image-wrapper');
        var desc_wrapper = $('.slide-desc-wrapper');
        var hide_show_arrow = $(image_wrapper).find('.hide-show-arrow');
        if (vm.slider_opened) {
            $(desc_wrapper).css('width', '0%');
            $(image_wrapper).css('width', '100%');
            $(hide_show_arrow).show();
        } else {
            $(desc_wrapper).css('width', '25%');
            $(image_wrapper).css('width', '75%');
            $(hide_show_arrow).hide();
        }
        vm.slider_opened = !vm.slider_opened;
    }

    function isSlideNumberInRange(number) {
        var start = vm.slidination.start;
        var end = start + vm.slidination.maximum;

        if (number >= start && number < end) {
            return true;
        } else {
            return false;
        }
    }

    function slidinationPrevArrow() {
        if (vm.slidination.start > 1) {
            vm.slidination.start -= 1;
        }
        prevSlide(true);
    }

    function slidinationNextArrow() {
        if (vm.slidination.start + vm.slidination.maximum - 1 < vm.slides.length) {
            vm.slidination.start += 1;
        }
        nextSlide(true);
    }

    function slideFullScreen() {

        if (vm.isSlideFullScreen) {
            fullScreen.exitFullScreen();
        } else {
            fullScreen.launchFullScreen(document.documentElement);
        }

        vm.isSlideFullScreen = !vm.isSlideFullScreen;
    }

    function decreaseSidebarWidth() {
        console.log(vm.currentSlide);
        var image_wrapper = $('.slide-image-wrapper')[vm.currentSlide];
        var desc_wrapper = $('.slide-desc-wrapper')[vm.currentSlide];
        var parent = $(image_wrapper).parent();
        var image_wrapper_w = Math.round($(image_wrapper).width() / $(parent).width() * 100) + 10;
        var desc_wrapper_w = Math.round($(desc_wrapper).width() / $(parent).width() * 100) - 10;
        console.log("after" + image_wrapper_w + " " + desc_wrapper_w);
        if (desc_wrapper_w >= 25) {
            $('.slide-image-wrapper').css('width', image_wrapper_w + '%');
            $('.slide-desc-wrapper').css('width', desc_wrapper_w + '%');
        } else {
            desc_wrapper_w = 25;
            image_wrapper_w = 75;
            $('.slide-image-wrapper').css('width', image_wrapper_w + '%');
            $('.slide-desc-wrapper').css('width', desc_wrapper_w + '%');
        }

        // $(image_wrapper).width()
    }

    function increaseSidebarWidth() {
        var image_wrapper = $('.slide-image-wrapper')[vm.currentSlide];
        var desc_wrapper = $('.slide-desc-wrapper')[vm.currentSlide];
        var parent = $(image_wrapper).parent();
        var image_wrapper_w = Math.round($(image_wrapper).width() / $(parent).width() * 100) - 10;
        var desc_wrapper_w = Math.round($(desc_wrapper).width() / $(parent).width() * 100) + 10;
        console.log("after" + image_wrapper_w + " " + desc_wrapper_w);
        if (desc_wrapper_w <= 50) {
            $('.slide-image-wrapper').css('width', image_wrapper_w + '%');
            $('.slide-desc-wrapper').css('width', desc_wrapper_w + '%');
        } else {
            desc_wrapper_w = 50;
            image_wrapper_w = 50;
            $('.slide-image-wrapper').css('width', image_wrapper_w + '%');
            $('.slide-desc-wrapper').css('width', desc_wrapper_w + '%');
        }
    }
}