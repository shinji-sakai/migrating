angular
    .module('directive')
    .directive('rating', starRating);


function starRating() {
    return {
        restrict: 'EA',
        template: '<ul data-ew-click="rating:{{ratingValue}}" data-ew-video-page-stats="true"  class="star-rating" ng-class="{readonly: readonly}">' +
            '  <li  data-ew-click="rating:{{ratingValue}}" data-ew-video-page-stats="true" ng-repeat="star in stars" class="star fa " ng-class="{filled: star.filled}" ng-click="toggle($index)">' +
            '    <i data-ew-click="rating:{{ratingValue}}" data-ew-video-page-stats="true"  class="fa fa-star"></i>' + // or &#9733
            '  </li>' +
            '</ul>',
        scope: {
            ratingValue: '=ngModel',
            max: '=?', // optional (default is 5)
            onRatingSelect: '&?',
            readonly: '=?'
        },
        link: function(scope, element, attributes) {
            if (scope.max == undefined) {
                scope.max = 5;
            }

            function updateStars() {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };
            scope.toggle = function(index) {
                if (scope.readonly == undefined || scope.readonly === false) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelect({
                        rating: index + 1
                    });
                }
            };
            scope.$watch('ratingValue', function(oldValue, newValue) {
                if (newValue) {
                    updateStars();
                }
            });
        }
    }
}