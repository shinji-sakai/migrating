require('directiveModule');
require('serviceModule');
require("authToken")
require('service.utility');
require('auth');
require('satellizer');
require('./_loginModal.scss');

angular.module("directive")
    .component('loginModalComponent', {
        template: require('./loginModal.html'),
        controller: LoginModalController,
        controllerAs: 'vm',
        bindings: {
            loginDelegate: '='
        }
    });

LoginModalController.$inject = ['$log','$window','$timeout','$compile','$auth','$state','authToken','utility','auth'];

function LoginModalController($log,$window,$timeout,$compile,$auth,$state,authToken,utility,auth) {
    var vm = this;

    vm.isUserLoggedIn = authToken.isAuthenticated;
    

    vm.$onInit = function(){
    }

    //check if already login
    vm.checkAlreadyLogin = function() {
        $('.jsModalLogin').modal('hide');
        $timeout(function(){
           $window.location.reload();
           // $state.reload(); 
        },500)
    }

    //do login
    vm.doLogin = function doLogin() {
        console.log("doLogin")
        if (!vm.userId || !vm.password) {
            vm.formError = "Please fill all required fields.";
            return;
        }
        var userloginData = {
            usr_id: vm.userId,
            pwd: vm.password
        };
        vm.isFormSubmitting = true;
        vm.formError = undefined;
        vm.userId = "";
        vm.password = "";
        auth.login(userloginData).then(function(resp) {
            var res = resp.data;
            if (res.err) {
                vm.formError = res.err;
                return;
            }
            vm.checkAlreadyLogin();
        }).catch(function(err) {
            console.error(err);
            if(err.status === 500){
                vm.formError = err.data.err;
            }else{
                vm.formError = err;
            }
        })
        .finally(function() {
            vm.isFormSubmitting = false;
        })
    }

    //authenticate with facebook,twitter,linkedin,google etc...
    vm.authenticateWith = function authenticateWith(provider) {
        vm.isFormSubmitting = true;
        $auth.authenticate(provider)
            .then(function(response) {
                authToken.setToken(response.data.token);
                authToken.setUser(response.data.user);
                vm.checkAlreadyLogin();
            })
            .catch(function(response) {
                console.error(response);
                vm.formError = response.error;
            })
            .finally(function() {
                vm.isFormSubmitting = false;
            })
    }
}

