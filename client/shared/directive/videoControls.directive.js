// window.videojs = require("video.js/dist/video.min.js")
// require("videojs-youtube/dist/Youtube.min.js")
// console.log(window.Youtube)
require("video.js/dist/video-js.min.css")

require('service.infoDetection');
require('service.fullscreen');
require('service.videoplayer');
angular
    .module('directive')
    .directive('videoControls', VideoControls)
    .controller('VideoControlsCtrl', VideoControlsCtrl);

VideoControlsCtrl.$inject = ['infoDetection', 'fullScreen', 'videoPlayer', '$scope'];

function VideoControlsCtrl(infoDetection, fullScreen, videoPlayer, $scope) {
    var vm = this;
    //Variables
    vm.isPlaying = false;
    vm.isFullScreen = false;
    vm.isMute = false;
    vm.isSubtitleOn = false;
    vm.currentPlayBackRate = '1.0';
    // vm.videoTitle = "Angular JS Best Practices";
    vm.videoQuality = "LOW";
    vm.offset_time = 8;
    vm.isDimLights = false;
    vm.video_current = "00:00";
    vm.video_duration = "00:00";
    vm.isLoading = true;

    //Funtions	
    vm.playToggle = playToggle;
    vm.rePlay = rePlay;
    vm.fullscreen = fullscreen;
    vm.toggleVolume = toggleVolume;
    vm.subtitleToggle = subtitleToggle;
    vm.changeSpeed = changeSpeed;
    vm.resetSpeed = resetSpeed;
    vm.changeVideoQuality = changeVideoQuality;
    vm.fastForward = fastForward;
    vm.rewind = rewind;
    vm.setFallBack = setFallBack;
    vm.changeVideoPlayer = changeVideoPlayer;

    vm.nextVideo = nextVideo;
    vm.prevVideo = prevVideo;

    vm.initVideoPlayer = initVideoPlayer;

    videoQualities();
    videoSpeeds();

    function initVideoPlayer() {
        infoDetection.init()
            .then(function(res) {
                setFallBack();
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    var videos = {
        'mpeg-dash': {
            type: 'application/dash+xml',
            src: 'http://rdmedia.bbc.co.uk/dash/ondemand/bbb/2/client_manifest-common_init.mpd'
        },
        'hls': {
            type: 'application/x-mpegURL',
            src: 'http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8'
        },
        'flash': {
            type: 'rtmp/mp4',
            src: 'rtmp://192.168.1.5:1935/vod/mp4:samplehd.mp4'
        },
        'html5': {
            type: 'video/mp4',
            src: 'assets/video/kafka3.mp4'
        }
    }

    function fallbackPossibilities() {

        return {
            'windows+modern': [
                //videos['mpeg-dash'], //Mpeg-dash
                // videos['hls'], //HLS
                videos['html5'], //HTML5
                //videos['flash'] //Flash
            ],
            'windows+old': [
                videos['flash'], //Flash
                videos['html5'] //HTML5
            ],
            'windows+ie+modern': [
                videos['mpeg-dash'], //Mpeg-dash
                videos['flash'], //flash
                videos['html5'] //html5
            ],
            'windows+ie+old': [
                videos['flash'], //flash
                // videos['html5'] //html5
            ],
            'mac+modern': [
                videos['hls'], //hls
                // videos['mpeg-dash'], //mpeg-dash
                videos['html5'], //html5
                videos['flash'] //flash
            ],
            'mac+old': [
                videos['html5'], //html5
                videos['flash'] //flash    
            ]
        }
    }

    function setFallBack() {
        var comp = infoDetection.getCompatibility();
        console.log(comp);
        var poss = fallbackPossibilities();
        // vm.videoPlayer.src(poss[comp],{"controls":false});
    }

    $scope.$on('loadVideo', function(event, video) {
        loadVideo(video);
    });
    $scope.$on('loadVideoFromNote', function(event, video, time) {
        loadVideo(video);
        vm.videoPlayer.currentTime(time);
    });

    $scope.$on('setupVideo', function(event, video) {
        vm.videoPlayer = videojs("video_tag", {});
        loadVideo(video);
        vm.setupVideoPlayer();
        vm.videoPlayer.play();
        vm.isPlaying = true;
    });

    $scope.$on('pauseVideo', function() {
        vm.videoPlayer.pause();
        vm.isPlaying = false;
    });
    $scope.$on('replayVideo', function() {
        console.log("replayVideo in directive")
        if (vm.videoPlayer.play) {
            vm.videoPlayer.play();
        }
    });
    $scope.$on('playVideo', function() {
        vm.videoPlayer.play();
        vm.isPlaying = true;
    });
    $scope.$on('videoName', function(eve, videoName) {
        vm.videoTitle = videoName;
    })

    vm.loadVideo = loadVideo;

    function loadVideo(video) {
        vm.video_current = "00:00";
        vm.video_duration = "00:00";
        vm.changeSpeed(1);
        vm.currentVideoUrl = video;
        console.info(video)
        vm.videoPlayer.src({
            // type: 'video/mp4',
            "type": "video/youtube",
            src: video
        }, {
            "controls": false
        });
        vm.videoPlayer.play();
        vm.isPlaying = true;
    }

    function playToggle() {
        if (vm.isPlaying) {
            vm.videoPlayer.pause();
        } else {
            vm.videoPlayer.play();
        }
        vm.isPlaying = !vm.isPlaying;
    }

    function rePlay() {
        setFallBack();
        vm.videoPlayer.play();
    }

    $(document.documentElement).on('fullscreenchange', function(e, fullScreenEnabled) {
        if (fullScreenEnabled) {
            vm.isFullScreen = true;
        } else {
            vm.isFullScreen = false;
        }
    });

    function fullscreen() {
        if (vm.isFullScreen) {
            fullScreen.exitFullScreen();
        } else {
            fullScreen.launchFullScreen(document.documentElement);
        }
        vm.isFullScreen = !vm.isFullScreen;
    }


    function toggleVolume() {
        if (vm.isMute) {
            vm.videoPlayer.volume(1);
            vm.volumeSlider.noUiSlider.set(100);
        } else {
            vm.videoPlayer.volume(0);
            vm.volumeSlider.noUiSlider.set(0);
        }
        vm.isMute = !vm.isMute;
    }


    function subtitleToggle(str) { //Str = ON/OFF/null
        var ele = vm.videoPlayer.el_;

        if (!str) {
            vm.isSubtitleOn = !vm.isSubtitleOn;
        } else if (str && str.toLowerCase() === "on") {
            vm.isSubtitleOn = true;
        } else if (str && str.toLowerCase() === "off") {
            vm.isSubtitleOn = false;
        }

        if (vm.isSubtitleOn) {
            var percent = (($(ele).height() - 50 - 40) / $(ele).height()) * 100;
            $("video").height(percent + '%'); //50px - Control Bar , 40px - Subtitle    
        } else {
            var percent = '100%';
            $("video").height(percent);
        }

    }


    function changeSpeed(val) {
        vm.currentPlayBackRate = val.toFixed(1);
        if (vm.videoPlayer) {
            vm.videoPlayer.playbackRate(val);
        }

        // vm.speedSlider.noUiSlider.set(val);
    }

    function resetSpeed() {
        vm.changeSpeed(1);
    }

    function rewind() {
        var currentTime = vm.videoPlayer.currentTime();
        var nextTime = currentTime - vm.offset_time;
        vm.videoPlayer.currentTime(nextTime);
    }

    function fastForward() {
        var currentTime = vm.videoPlayer.currentTime();
        if (currentTime < vm.videoPlayer.duration()) {
            var nextTime = currentTime + vm.offset_time;
            vm.videoPlayer.currentTime(nextTime);
        }
    }

    function prevVideo() {
        $scope.$emit('prevVideo');
    }

    function nextVideo() {
        $scope.$emit('nextVideo');
    }

    function changeVideoQuality(quality) { //quality = HIGH / MED / LOW
        vm.videoQuality = quality;
    }

    function changeVideoPlayer() {
        var currentTime = vm.videoPlayer.currentTime();
        $scope.$emit('changeVideoPlayer', vm.currentVideoUrl, currentTime);
    }

    function videoQualities() {
        vm.videoQualities = {
            '720p': 'assets/video/1-720.mp4',
            '360p': 'assets/video/1-360.mp4',
            '244p': 'assets/video/1-244.mp4',
        }
    }

    function videoSpeeds() {

        vm.videoSpeeds = {
            '0.5x': '0.5',
            '1x': '1',
            '1.5x': '1.5',
            '2x': '2',
            '4x': '4',
            '16x': '16',
        }
    }
}

VideoControls.$inject = ['$interval', 'videoPlayer'];

function VideoControls($interval, videoPlayer) {
    return {
        restrict: 'AE',
        scope: {},
        // replace : true,
        template: require('partials.videoControlBar'),
        controller: VideoControlsCtrl,
        controllerAs: 'vm',
        bindToController: {
            componentDelegate: '=?'
        },
        link: function postLink(scope, element, attrs, vm) {
            vm.componentDelegate = vm;
            console.log(vm)
            vm.videoPlayer = videojs("video_tag", {
                "techOrder": ["html5", "flash","youtube"],
            });
            vm.setupVideoPlayer = setupVideoPlayer;
            vm.isFlash = false;


            var controlbar = $('.ds-controlbar');
            var video_tag = $('#video_tag');
            var slider = $('.volume-slider')[0];
            vm.volumeSlider = slider;
            var speedSlider = $('.speed-slider')[0];
            vm.speedSlider = speedSlider;
            var max_speed = 2;
            var min_speed = 0.5

            // vm.initVideoPlayer();

            function setupVideoPlayer() {
                registerEvents();
                bindKeyEvents();
                setupVolumeSlider();
            }


            function registerEvents() {

                $("video").on('click', function() {
                    //Play / Pause Video when user click on Video itself
                    vm.playToggle();
                });
                $('video').height('100%');

                //On Time Update
                vm.videoPlayer.on('timeupdate', function() {
                    //Fetch current time
                    var time = vm.videoPlayer.currentTime();
                    vm.video_current = changeTimeFormat(time);
                    //Progress bar width
                    var progressbar_width = ((vm.videoPlayer.currentTime() / vm.videoPlayer.duration()) * 100) + '%';
                    //Set progress bar width
                    $('.ds-progress-bar').css('width', progressbar_width);
                });

                vm.videoPlayer.on('canplaythrough', function() {
                    $("#video_tag").removeClass('vjs-waiting');
                });
                vm.videoPlayer.on('playing', function() {
                    $("#video_tag").removeClass('vjs-waiting');
                });

                //Only Once when all meta data loaded
                vm.videoPlayer.on('loadedmetadata', function() {
                    //Duration of video
                    var time = vm.videoPlayer.duration();
                    vm.video_duration = changeTimeFormat(time);
                    //Set Current time of video
                    vm.video_current = "00:00";
                    //Start Observation for buffered data
                    $interval(function() {
                        var bufferbar_width = (vm.videoPlayer.bufferedPercent() * 100) + '%';
                        $('.ds-buffered-bar').css('width', bufferbar_width);
                    }, 100);

                    //Set Volume Control Height
                    var volume_control_height = (vm.videoPlayer.volume() * 100) + '%';
                    $('.volume-control').css('height', volume_control_height);

                    videoPlayer.setVideoPlayer(vm.videoPlayer);
                    var mimeType = vm.videoPlayer.currentType();
                    if (mimeType === "rtmp/mp4") {
                        vm.isFlash = true;
                    }
                    //Hide Default Subtitle
                    if (vm.videoPlayer.textTracks().tracks_[0]) {
                        vm.videoPlayer.textTracks().tracks_[0].mode = "hidden";
                        //Observe Subtitle Change
                        $interval(function() {
                            if (vm.videoPlayer.textTracks().tracks_[0].activeCues[0]) {
                                vm.subtitle = vm.videoPlayer.textTracks().tracks_[0].activeCues[0].text;
                            }
                        }, 100);
                    }


                    // $(vm.videoPlayer.el_).append($('.ds-controlbar'));
                    // $(vm.videoPlayer.el_).append($('.ds-subtitle'));

                });

                //Volume Change event
                vm.videoPlayer.on('volumechange', function() {
                    //Set Volume Control Height
                    // console.log('volume changed', vm.videoPlayer.volume());
                    if (vm.videoPlayer.volume() == 0) {
                        vm.isMute = true;
                    } else {
                        vm.isMute = false;
                    }
                });

                vm.videoPlayer.on('ended', function() {
                    vm.isPlaying = false;
                });


                //Click Listener For Progress Bar
                $('.ds-progress-bar-wrapper').on('click', function(e) {
                    //Percent where user clicked on progressbar
                    var percent = (e.offsetX / $(this).width()) * 100;
                    //Get desire time from where user clicked
                    var desireTime = vm.videoPlayer.duration() * percent / 100;
                    //Set current time to desire time
                    vm.videoPlayer.currentTime(desireTime);
                });

                var latest_overlay;
                var shouldFire = false;
                var prevOverlay;
                $('.ds-control').on('click', function() {
                    var overlay = $(this).find('.overlay')[0]
                    latest_overlay = ($(overlay)[0]);
                    if ($('.overlay.ds-show')[0] != $(latest_overlay)[0]) {
                        //Remove all showing overlay first
                        $('.overlay.ds-show').removeClass('ds-show').toggle();
                    }

                    if (!$(latest_overlay).hasClass('ds-show')) {
                        //Add show class if latest_overlay is going to be displayed
                        $(latest_overlay).addClass('ds-show');
                        $(latest_overlay).show();
                    } else {
                        $(latest_overlay).removeClass('ds-show');
                        $(latest_overlay).hide();
                    }
                    event.stopPropagation();
                    document.addEventListener("click", onlyOneClick, false);
                })

                function onlyOneClick() {
                    if ($(latest_overlay).hasClass('ds-show')) {
                        $(latest_overlay).removeClass('ds-show');
                        $(latest_overlay).toggle();
                    }
                    document.removeEventListener('click', onlyOneClick, false);
                }
            }

            function bindKeyEvents() {

                $('video-controls').bind('keypress', function(e) {
                    // console.log("press",e);
                    // 99/67 - C/c
                    var codes = {
                        CAPITAL_C: 99,
                        SMALL_C: 67,

                        CAPITAL_F: 70,
                        SMALL_F: 102,

                        SMALL_P: 112,
                        CAPITAL_P: 80
                    }

                    // console.log(e);

                    if (e.keyCode == codes.CAPITAL_C || e.keyCode == codes.SMALL_C) {
                        vm.subtitleToggle();
                    } else if (e.keyCode == codes.CAPITAL_F || e.keyCode == codes.SMALL_F) {
                        vm.fullscreen();
                    } else if ((e.keyCode == codes.SMALL_P) || (e.keyCode == codes.CAPITAL_P)) {
                        vm.playToggle();
                    }
                })
                $('video-controls').bind('keydown', function(e) {
                    // console.log("down " , e);
                    //32 - Space
                    //37 - Left Key
                    //38 - Up Key
                    //39 - Right Key
                    //40 - Down Key
                    var offset_time = 8; //8 seconds Forward and Backward
                    var codes = {
                        SPACE: 32,
                        LEFT: 37,
                        UP: 38,
                        RIGHT: 39,
                        DOWN: 40,
                        GREATER: 190,
                        LESSTHAN: 188
                    }

                    // console.log(e);

                    if (e.keyCode == codes.SPACE) {
                        vm.playToggle();
                    } else if (!e.ctrlKey && e.keyCode == codes.UP) {

                        //Volume UP

                        var currentVolume = vm.videoPlayer.volume();
                        if (currentVolume < 1) {
                            var nextVolume = currentVolume + 0.1;
                            slider.noUiSlider.set(nextVolume * 100);
                            vm.videoPlayer.volume(nextVolume);
                        }
                    } else if (!e.ctrlKey && e.keyCode == codes.DOWN) {

                        //Volume Down

                        var currentVolume = vm.videoPlayer.volume();
                        if (currentVolume > 0) {
                            var nextVolume = currentVolume - 0.1;
                            slider.noUiSlider.set(nextVolume * 100);
                            vm.videoPlayer.volume(nextVolume);
                        }
                    } else if (!e.ctrlKey && e.keyCode == codes.RIGHT) {

                        //FAST FORWARD offset_time
                        vm.fastForward();

                    } else if (!e.ctrlKey && e.keyCode == codes.LEFT) {

                        //Backward offest_time
                        vm.rewind();

                    } else if (e.ctrlKey && e.keyCode == codes.GREATER) {

                        //Speed UP

                        var currentSpeed = vm.videoPlayer.playbackRate();
                        if (currentSpeed < max_speed) {
                            var nextSpeed = currentSpeed + 0.1;
                            vm.changeSpeed(nextSpeed);
                        }
                    } else if (e.ctrlKey && e.keyCode == codes.LESSTHAN) {

                        //Speed Down

                        var currentSpeed = vm.videoPlayer.playbackRate();
                        if (currentSpeed > min_speed) {
                            var nextSpeed = currentSpeed - 0.1;
                            vm.changeSpeed(nextSpeed);
                        }
                    }


                })
            }

            function setupVolumeSlider() {
                //Create Volume Slider
                noUiSlider.create(slider, {
                    start: 100,
                    orientation: 'vertical',
                    // tooltips: true,
                    direction: 'rtl',
                    connect: 'lower',
                    range: {
                        'min': 0,
                        'max': 100
                    }
                });

                slider.noUiSlider.on('update', function(values, handle) {
                    vm.videoPlayer.volume(parseInt(values[0]) / 100);
                });
            }

            function setupSpeedSlider() {
                noUiSlider.create(speedSlider, {
                    start: 1,
                    connect: 'lower',
                    step: 0.1,
                    range: {
                        'min': min_speed,
                        'max': max_speed
                    }
                });
            }

            function changeTimeFormat(sec) {
                var time = sec;
                var hours = parseInt(time / 3600) % 24;
                var minutes = parseInt(time / 60) % 60;
                var seconds = parseInt(time % 60);

                hours = (hours < 10 ? "0" + hours : hours)
                minutes = (minutes < 10 ? "0" + minutes : minutes)
                seconds = (seconds < 10 ? "0" + seconds : seconds)
                var ret_time;
                if (hours == "00") {
                    ret_time = minutes + ":" + seconds;
                } else {
                    ret_time = hours + ":" + minutes + ":" + seconds;
                }

                return ret_time;
            }

        }
    }

}