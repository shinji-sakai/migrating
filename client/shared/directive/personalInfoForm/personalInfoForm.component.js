require('authToken');
require('service.profile');
require('directive.profilePic');

var countries = require('country-list')();
require('angular-ui-select/dist/select.min.css');
require('angular-ui-select/dist/select.min.js');

require('./_personalInfoForm.scss');
var cities = require('cities_js');

angular.module("directive")
    .component('personalInfoForm', {
        template: require('./personalInfoForm.html'),
        controller: PersonalInfoFormController,
        controllerAs: 'vm',
        bindings: {
            targetedUser: '=', // user whose information will be displayed
            relation: '=',
            showUserPic: '@'
        }
    });

PersonalInfoFormController.$inject = ['authToken', 'profile', '$filter', '$timeout', '$scope', 'getCountryCityService'];

function PersonalInfoFormController(authToken, profile, $filter, $timeout, $scope, getCountryCityService) {
    var vm = this;
    var user = authToken.getUser() || {};
    vm.countries = [];
    vm.cities = [];

    vm.form = {};
    vm.form.dsp_nm = user["dsp_nm"];
    vm.activeFormInnerTab = "general-personal";
    vm.shouldBasicFieldEditable = false;

    vm.isUserLoggedIn = authToken.isAuthenticated;
    vm.getUserId = authToken.getUserId;
    vm.userPic = authToken.getUserPic();
    vm.saveBasicInfo = saveBasicInfo;
    vm.canUserEditInfo = canUserEditInfo;
    vm.shouldShowField = shouldShowField;


    // Phone Number FOrm Helper
    vm.savePhoneInfo = savePhoneInfo;
    vm.editPhoneNumber = editPhoneNumber;
    vm.startVerifingPhoneNumber = startVerifingPhoneNumber;
    vm.confirmVerificationOfPhoneNumber = confirmVerificationOfPhoneNumber;
    vm.deletePhoneNumber = deletePhoneNumber;
    vm.isAnyPhoneNumberVisible = isAnyPhoneNumberVisible;

    //Email Form Helper
    vm.saveEmailInfo = saveEmailInfo;
    vm.editEmailAddress = editEmailAddress;
    vm.startVerifingEmailAddress = startVerifingEmailAddress;
    vm.confirmVerificationOfEmail = confirmVerificationOfEmail;
    vm.deleteEmailAddress = deleteEmailAddress;
    vm.isAnyEmailAddressVisible = isAnyEmailAddressVisible;

    // Work Form Helper
    vm.saveWorkInfo = saveWorkInfo;
    vm.resetWorkInfo = resetWorkInfo;
    vm.getWorkInfo = getWorkInfo;
    vm.deleteWorkInfo = deleteWorkInfo;
    vm.editWorkInfo = editWorkInfo;
    vm.isAnyWorkInfoVisible = isAnyWorkInfoVisible;

    // Edu Form Helper
    vm.saveEduInfo = saveEduInfo;
    vm.resetEduInfo = resetEduInfo;
    vm.getEduInfo = getEduInfo;
    vm.deleteEduInfo = deleteEduInfo;
    vm.editEduInfo = editEduInfo;
    vm.isAnyEduInfoVisible = isAnyEduInfoVisible;

    // Interested Course Form Helper
    vm.saveInterestedCourseInfo = saveInterestedCourseInfo;
    vm.resetInterestedCourseInfo = resetInterestedCourseInfo;
    vm.getInterestedCourseInfo = getInterestedCourseInfo;
    vm.deleteInterestedCourseInfo = deleteInterestedCourseInfo;
    vm.editInterestedCourseInfo = editInterestedCourseInfo;
    vm.isAnyInterestedCourseInfoVisible = isAnyInterestedCourseInfoVisible;

    // Exam Form Helper
    vm.saveExamInfo = saveExamInfo;
    vm.resetExamInfo = resetExamInfo;
    vm.getExamInfo = getExamInfo;
    vm.deleteExamInfo = deleteExamInfo;
    vm.editExamInfo = editExamInfo;
    vm.isAnyExamInfoVisible = isAnyExamInfoVisible;

    // Author Info Form Helper
    vm.saveAuthorInfo = saveAuthorInfo;
    vm.saveAuthorDescInfo = saveAuthorDescInfo;
    vm.resetAuthorInfo = resetAuthorInfo;
    vm.getAuthorInfo = getAuthorInfo;
    vm.deleteAuthorInfo = deleteAuthorInfo;
    vm.editAuthorInfo = editAuthorInfo;
    vm.isAnyAuthorInfoVisible = isAnyAuthorInfoVisible;

    // Teacher Info Form Helper
    vm.saveTeacherInfo = saveTeacherInfo;
    vm.saveTeacherDescInfo = saveTeacherDescInfo;
    vm.resetTeacherInfo = resetTeacherInfo;
    vm.getTeacherInfo = getTeacherInfo;
    vm.deleteTeacherInfo = deleteTeacherInfo;
    vm.editTeacherInfo = editTeacherInfo;
    vm.isAnyTeacherInfoVisible = isAnyTeacherInfoVisible;

    // Kid Info Form Helper
    vm.saveKidInfo = saveKidInfo;
    vm.resetKidInfo = resetKidInfo;
    vm.getKidInfo = getKidInfo;
    vm.deleteKidInfo = deleteKidInfo;
    vm.editKidInfo = editKidInfo;
    vm.isAnyKidInfoVisible = isAnyKidInfoVisible;

    // open edit popup
    vm.openEditConfirmPopup = openEditConfirmPopup;
    vm.confirmEditBox = confirmEditBox;
    vm.cancelEditBox = cancelEditBox;

    // For Date Picker
    vm.dateSelection = _dateSelection;
    vm.removeYearSelection = _removeYearSelection;
    // Get Counry List
    vm.getCountryList = _getCountryList;
    // Dropdown event
    // this is for positioning dropdown based on screen position
    $(document).on("shown.bs.dropdown", ".dropdown", function () {
        // calculate the required sizes, spaces
        var $ul = $(this).children(".dropdown-menu");
        var $button = $(this).children(".dropdown-toggle");
        var ulOffset = $ul.offset();
        // how much space would be left on the top if the dropdown opened that direction
        var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
        // how much space is left at the bottom
        var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
        // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
        if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
            $(this).addClass("dropup");
    }).on("hidden.bs.dropdown", ".dropdown", function () {
        // always reset after close
        $(this).removeClass("dropup");
    });

    vm.$onInit = function () {
        console.log(vm.relation);
        vm.relationship = (vm.getUserId() === vm.targetedUser) ? 'me' : vm.relation;
        getUserInfo();
        profile.getAllUsersShortDetails()
            .then(function (res) {
                vm.allUsers = res.data;
            })
        getKidInfo();
        getCountryCityService.getCountry()
            .then(function (res) {
                vm.countries = res.data;
            });
        getUserEmails();
        getUserPhoneNumbers();
    }

    //check whether logged in user can edit the info
    function canUserEditInfo() {
        if (!vm.isUserLoggedIn()) {
            return false;
        }
        return vm.getUserId() === vm.targetedUser;

    }

    //check whether field should show to user or not
    function shouldShowField(field, field_flg) {
        field_flg = field_flg || 'public';
        if (vm.relationship === 'me') {
            return true;
        }
        if (field_flg === "public" && field) {
            //if field is public and field has some value than show to user
            return true;
        }
        if (field_flg === "friends" && vm.relationship === 'friends' && field) {
            //if field is restricted to only friends and relation is friend and field has some value than show to user
            return true;
        }
        if (field_flg === "onlyme" && vm.relationship === 'me') {
            //if field is restricted to only friends and relation is friend and field has some value than show to user
            return true;
        }
        return false;
    }

    function getUserInfo() {
        //get user personal details
        profile.getUserPersonalDetails({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                if (res.data.length > 0) {
                    var data = res.data[0];
                    vm.form = data;
                    vm.getCountryList({ cntry_id: data.ctry }, 'cities_current', 'selectedCity', data.cty);
                    vm.getCountryList({ cntry_id: data.ht_ctry }, 'cities_home_town', 'selectedHomeTownCity', data.ht_cty);
                    console.log('vm.allUsers', data)
                    vm.selectedDob = vm.form.dob_mn + '/' + vm.form.dob_dt;
                    vm.emails_nv = angular.copy(data['email_nv'] || {});
                    vm.phs_nv = angular.copy(data['ph_nv'] || {});
                    vm.emails = angular.copy(data['email'] || {});
                    vm.phs = angular.copy(data['ph'] || {});
                }
            })
            .catch(function (err) {
                console.error(err);
            });
    }

    function getUserEmails() {
        profile.getUserEmails({
            usr_id: vm.targetedUser
        })
        .then(function(res){
            if(res.data && res.data.length > 0){
                vm.userEmails = res.data;
            }
        })
        .catch(function(err){
            console.error(err);
        })
    }

    function getUserPhoneNumbers() {
        profile.getUserPhoneNumbers({
            usr_id: vm.targetedUser
        })
        .then(function(res){
            if(res.data && res.data.length > 0){
                vm.userPhoneNumbers = res.data;
            }
        })
        .catch(function(err){
            console.error(err);
        })
    }



    function saveBasicInfo() {
        vm.form.dob_dt = vm.selectedDob.split('/')[1];
        vm.form.dob_mn = vm.selectedDob.split('/')[0];
        var obj = vm.form;
        obj.cty = vm.selectedCity || "";
        obj.ctry = vm.selectedCountry || "";
        obj.ht_cty = vm.selectedHomeTownCity || "";
        obj.ht_ctry = vm.selectedHomeTownCountry || "";
        obj.usr_id = vm.targetedUser;

        profile.saveUserBasicPersonalDetails(obj)
            .then(function (res) {
                vm.isBasicInfoInProgress = false;
                vm.basicInfoSuccess = "Successfully Saved.";
                vm.shouldBasicFieldEditable = false;
                authToken.setUserDisplayName(obj.dsp_nm);
                $timeout(function () {
                    vm.basicInfoSuccess = "";
                }, 5 * 1000)
            })
            .catch(function (err) {
                console.error(err);
                vm.basicInfoError = "Error in saving.";
                $timeout(function () {
                    vm.basicInfoError = "Error in saving.";
                }, 5 * 1000)
            })
    }

    //Observe email field
    $scope.$watch(function () {
        if (vm.form.email && vm.form.email['val']) {
            return vm.form.email['val'];
        } else {
            return vm.form.email;
        }
    }, function (newValue, oldValue, scope) {
        vm.emailAddressErr = '';
    });

    //get/update only email info
    function getEmailInfo() {
        //get user personal details
        profile.getUserEmails({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                if (res.data && res.data.length > 0) {
                    // var data = res.data[0];
                    // vm.emails_nv = angular.copy(data['email_nv'] || {});
                    // vm.emails = angular.copy(data['email'] || {});
                    vm.userEmails = res.data;
                }
            })
            .catch(function (err) {
                console.error(err);
            });
    }

    //check whether there is any email that is visible to user
    function isAnyEmailAddressVisible() {
        var isVisible = false;
        // itrate through all the fields
        for (k in vm.emails) {
            //check whether field is visible or not
            if (shouldShowField(k, vm.emails[k])) {
                //if visible
                //break loop and return
                isVisible = true;
                break;
            }
        }
        return isVisible;
    }

    //this function will save email address to db
    function saveEmailInfo() {
        var email_val = vm.form.email['val'];
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (email_val.length <= 0) {
            //if email is emapty
            vm.emailAddressErr = "Email address is required";
            return;
        }
        if (!regex.test(email_val)) {
            // if email is not valid email address
            vm.emailAddressErr = "Email address is not valid";
            return;
        }

        // if (!vm.isEditingOfEmailInProgress) {
            //if user is adding new email
            var obj = {
                usr_id: vm.targetedUser,
                email: vm.form.email['val'],
                // email_nv_permission: vm.form.email['permission'] || 'public'
            }
            vm.addingEmailToDB = true;
            profile.addUserEmail(obj)
                .then(function (res) {
                    vm.isAddEmailAddressInProgress = false;
                    var em = vm.form.email['val'];
                    vm.form.email['val'] = "";
                    vm.form.email['permisssion'] = 'public';
                    getEmailInfo();
                    vm.startVerifingEmailAddress(em);
                })
                .catch(function (err) {
                    console.error(err);
                })
                .finally(function(){
                    vm.addingEmailToDB = false;
                })
        // }
        //  else {
        //     //if user is editing already added email
        //     var obj = {
        //         usr_id: vm.targetedUser,
        //         old_email_nv: vm.currentlyEditingEmailAddress,
        //         email_nv: vm.form.email['val'],
        //         email_nv_permission: vm.form.email['permission'] || 'public'
        //     }
        //     profile.saveUserEmailDetails(obj)
        //         .then(function (res) {
        //             vm.isAddEmailAddressInProgress = false;
        //             vm.isEditingOfEmailInProgress = false;
        //             var em = vm.form.email['val'];
        //             vm.form.email['val'] = "";
        //             vm.form.email['permisssion'] = 'public';
        //             getEmailInfo();
        //             vm.startVerifingEmailAddress(em);
        //         })
        //         .catch(function (err) {
        //             console.error(err);
        //         })
        // }
    }

    // this function will be used to set currently editing value to form
    function editEmailAddress(key) {
        var permission = vm.emails[key] || vm.emails_nv[key];
        vm.form.email = vm.form.email || {};
        vm.form.email['val'] = key;
        vm.form.email['permission'] = permission;
        vm.currentlyEditingEmailAddress = key;
        vm.isEditingOfEmailInProgress = true;
        vm.isAddEmailAddressInProgress = false;
    }

    //this function will be called when user click on verify button
    function startVerifingEmailAddress(key) {
        vm.isSendingMail = true;
        vm.currentlyVerifingEmail = key;
        profile.startVerifingEmailAddress({
            email : key,
            usr_id: vm.targetedUser,
        })
        .then(function(res){
            if(res.data && res.data.status !== 'error'){
                vm.isSendingMail = false;
                vm.currentlyVerifingEmail = key;
                vm.isVerifingEmailStarted = true;
                vm.otpError = undefined;
                vm.otpSuccess = undefined;
            }else{
                vm.otpError = "Error in sending Mail";
            }
        })
    }

    //this function will be called when user click on confirm emil verification button
    function confirmVerificationOfEmail(email) {
        if (vm.userEnteredOTP) {
            vm.otpError = undefined;
            vm.otpSuccess = undefined;
            profile.verifyUserEmail({
                usr_id: vm.targetedUser,
                otp : vm.userEnteredOTP,
                email : email
            })
            .then(function (res) {
                if(res.data.status === 'success'){
                    vm.otpSuccess = "Email Varified";
                    getEmailInfo();    
                }else if(res.data.status === 'error'){
                    vm.otpError = res.data.message;
                }
            })
            .catch(function (err) {
                vm.otpError = "Some error occured";
            })


            // vm.otpError = undefined;
            // vm.otpSuccess = undefined;
            // if (vm.currentOTP != vm.userEnteredOTP) {
            //     vm.otpError = "OTP is wrong";
            //     return;
            // } else {

            //     var obj = {
            //         email: vm.currentlyVerifingEmail,
            //         permission: vm.emails_nv[vm.currentlyVerifingEmail],
            //         usr_id: vm.targetedUser
            //     }
            //     profile.confirmVerificationOfEmail(obj)
            //         .then(function () {
            //             vm.otpSuccess = "Email Varified";
            //             getEmailInfo();
            //         })
            //         .catch(function (err) {
            //             vm.otpError = "Some error occured";
            //         })
            // }
        }
    }

    //delete email address
    //this function will used to delete added email
    function deleteEmailAddress(key) {
        var obj = {
            usr_id: vm.targetedUser,
            email: key
        }
        profile.deleteUserEmail(obj)
            .then(function () {
                getEmailInfo();
            })
            .catch(function (err) {
                vm.otpError = "Some error occured";
            })
    }


    //Observe phone field
    $scope.$watch(function () {
        if (vm.form.ph && vm.form.ph['val']) {
            return vm.form.ph['val'];
        } else {
            return vm.form.ph;
        }
    }, function (newValue, oldValue, scope) {
        vm.phoneNumberErr = '';
    });

    //get/update only phone info
    function getPhoneInfo() {
        //get user personal details
        profile.getUserPersonalDetails({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                if (res.data.length > 0) {
                    var data = res.data[0];
                    vm.phs_nv = angular.copy(data['ph_nv'] || {});
                    vm.phs = angular.copy(data['ph'] || {});
                }
            })
            .catch(function (err) {
                console.error(err);
            });
    }

    //check whether there is any number that is visible to user
    function isAnyPhoneNumberVisible() {
        var isVisible = false;
        // itrate through all the fields
        for (k in vm.phs) {
            //check whether field is visible or not
            if (shouldShowField(k, vm.phs[k])) {
                //if visible
                //break loop and return
                isVisible = true;
                break;
            }
        }
        return isVisible;
    }


    //this function will save phone number to db
    function savePhoneInfo() {
        var val = vm.form.ph['val'];
        if (val.length <= 0) {
            vm.phoneNumberErr = "Phone Number is required";
            return;
        }

        var regex = /^[0-9]*$/;
        if (!regex.test(val)) {
            vm.phoneNumberErr = "Phone Number is not valid";
            return;
        }

        if (!vm.isEditingOfPhoneInProgress) {

            //if new value is being added
            var obj = {
                usr_id: vm.targetedUser,
                ph_nv: vm.form.ph['val'],
                ph_nv_permission: vm.form.ph['permission'] || 'public'
            }
            profile.saveUserPhoneDetails(obj)
                .then(function (res) {
                    vm.isAddPhoneInProgress = false;
                    var p = vm.form.ph['val'];
                    vm.form.ph['val'] = "";
                    vm.form.ph['permisssion'] = 'public';
                    getPhoneInfo();
                    vm.startVerifingPhoneNumber(p);
                })
                .catch(function (err) {
                    console.error(err);
                })
        } else {
            //if user is editing existing value
            var obj = {
                usr_id: vm.targetedUser,
                old_ph_nv: vm.currentlyEditingPhoneNumber,
                ph_nv: vm.form.ph['val'],
                ph_nv_permission: vm.form.ph['permission'] || 'public'
            }
            profile.saveUserPhoneDetails(obj)
                .then(function (res) {
                    vm.isAddPhoneInProgress = false;
                    vm.isEditingOfPhoneInProgress = false;
                    var p = vm.form.ph['val'];
                    vm.form.ph['val'] = "";
                    vm.form.ph['permisssion'] = 'public';
                    getPhoneInfo();
                    vm.startVerifingPhoneNumber(p);
                })
                .catch(function (err) {
                    console.error(err);
                })
        }

    }

    //this function will be used to set currently editing value to form
    function editPhoneNumber(key) {
        var permission = vm.phs[key] || vm.phs_nv[key];
        vm.form.ph = vm.form.ph || {};
        vm.form.ph['val'] = key;
        vm.form.ph['permission'] = permission;
        vm.currentlyEditingPhoneNumber = key;
        vm.isEditingOfPhoneInProgress = true;
        vm.isAddPhoneInProgress = false;

    }

    //this function will be called when user click on verify button
    function startVerifingPhoneNumber(key) {
        vm.currentlyVerifingPhoneNumber = key;
        vm.isVerifingPhoneNumberStarted = true;
        vm.otpPhoneError = undefined;
        vm.otpPhoneSuccess = undefined;
        vm.userEnteredPhoneOTP = '';
        profile.startVerifingPhoneNumber({
            ph: key
        })
            .then(function (res) {
                vm.currentOTP = res.data.otp;
            })
    }

    //this function will be called when user click on confirm emil verification button
    function confirmVerificationOfPhoneNumber() {
        if (vm.currentOTP) {
            vm.otpPhoneError = undefined;
            vm.otpPhoneSuccess = undefined;
            if (vm.currentOTP != vm.userEnteredPhoneOTP) {
                vm.otpPhoneError = "OTP is wrong";
                return;
            } else {
                var obj = {
                    ph: vm.currentlyVerifingPhoneNumber,
                    permission: vm.phs_nv[vm.currentlyVerifingPhoneNumber],
                    usr_id: vm.targetedUser
                }
                profile.confirmVerificationOfPhoneNumber(obj)
                    .then(function () {
                        vm.otpPhoneSuccess = "Phone number is Varified";
                        getPhoneInfo();
                    })
                    .catch(function (err) {
                        vm.otpPhoneError = "Some error occured";
                    })
            }
        }
    }

    //delete phone number
    //this function will used to delete added phonenumber
    function deletePhoneNumber(key) {
        var obj = {
            usr_id: vm.targetedUser,
            ph: key
        }
        profile.deleteUserPhoneNumber(obj)
            .then(function () {
                getPhoneInfo();
            })
            .catch(function (err) { })
    }



    // Work Form Helper

    $scope.$watch(function () {
        if (vm.workForm && vm.workForm.comp_nm) {
            return vm.workForm.comp_nm;
        } else {
            return vm.workForm;
        }
    }, function (newValue, oldValue, scope) {
        vm.companyErr = "";
    });


    function getWorkInfo() {
        profile.getWorkInfo({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                vm.workComps = res.data;
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function isAnyWorkInfoVisible() {
        var isVisible = false;
        vm.workComps = vm.workComps || [];

        if (vm.workComps.length <= 0) {
            return isVisible;
        }
        // itrate through all the fields
        for (var i = 0; i < vm.workComps.length; i++) {
            //check whether field is visible or not
            if (shouldShowField(true, vm.workComps[i]['work_flg'])) {
                //if visible
                //break loop and return
                isVisible = true;
                break;
            }
        }

        return isVisible;
    }

    function saveWorkInfo() {

        var obj = vm.workForm;
        console.log(obj)
        if (!obj.comp_nm || obj.comp_nm.length <= 0) {
            vm.companyErr = "Company Name is required";
            return;
        }

        obj.work_flg = obj.work_flg || 'public';
        obj.usr_id = vm.targetedUser;
        //set from date
        if (vm.workForm.frm_dt_present) {
            obj.frm_dt = "present"
        } else {
            obj.frm_dt = vm.workForm.frm_dt;
        }

        //set to date
        if (vm.workForm.to_dt_present) {
            obj.to_dt = "present";

        } else {
            obj.to_dt = vm.workForm.to_dt;
        }

        //delete unnecessary keys
        delete obj.frm_day;
        delete obj.frm_mn;
        delete obj.frm_yr;
        delete obj.frm_dt_present;

        delete obj.to_dt_present;
        delete obj.to_day;
        delete obj.to_mn;
        delete obj.to_yr;
        obj.city = vm.selectedCity;
        obj.country = vm.selectedCountry;

        profile.saveWorkInfo(obj)
            .then(function (res) {
                console.log(res);
                resetWorkInfo();
                getWorkInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function resetWorkInfo() {
        vm.workForm = {};
        vm.isAddWorkPlaceInProgress = false;
        vm.isEditWorkPlaceInProgress = false;
        vm.currentlyEditingWorkInfo = '';
    }

    function deleteWorkInfo(wrk_sk) {
        var obj = {
            usr_id: vm.targetedUser,
            usr_wrk_sk: wrk_sk
        }
        profile.deleteWorkInfo(obj)
            .then(function (res) {
                getWorkInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function editWorkInfo(wrk_sk) {
        vm.cities_work_exp = [];
        console.log(wrk_sk)
        // Set Currently editing work info
        vm.currentlyEditingWorkInfo = wrk_sk;
        // Set new add work info to false
        vm.isAddWorkPlaceInProgress = false;
        //set editing form to true
        vm.isEditWorkPlaceInProgress = true;

        var work_obj = $filter('filter')(vm.workComps, {
            usr_wrk_sk: wrk_sk
        })[0];

        vm.workForm = work_obj;
        if (vm.workForm.frm_dt && vm.workForm.frm_dt === "present") {
            vm.workForm.frm_dt_present = true;
        } else if(vm.workForm.frm_dt){
            //find fromdate component        
            var frm_dt_arr = vm.workForm.frm_dt.split("/");
            vm.workForm.frm_day = frm_dt_arr[0];
            vm.workForm.frm_mn = frm_dt_arr[1];
            vm.workForm.frm_yr = frm_dt_arr[2];
        }

        if (vm.workForm.to_dt === "present") {
            vm.workForm.to_dt_present = true;
        } else if(vm.workForm.to_dt){
            //find todate component
            var to_dt_arr = vm.workForm.to_dt.split("/");
            vm.workForm.to_day = to_dt_arr[0];
            vm.workForm.to_mn = to_dt_arr[1];
            vm.workForm.to_yr = to_dt_arr[2];
        }
        vm.selectedCity = vm.workForm.city;
        vm.selectedCountry = vm.workForm.country;
        console.log(vm.selectedCity,vm.selectedCountry )
        vm.getCountryList({ cntry_id: vm.workForm.country }, 'cities_work_exp', 'selectedCity',  vm.workForm.city);
    }


    /////////////////////
    // Edu form helper //
    /////////////////////

    $scope.$watch(function () {
        if (vm.eduForm && vm.eduForm.edu_nm) {
            return vm.eduForm.edu_nm;
        } else {
            return vm.eduForm;
        }
    }, function (newValue, oldValue, scope) {
        vm.eduErr = "";
    });


    function getEduInfo() {
        profile.getEduInfo({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                vm.eduComps = res.data;
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function isAnyEduInfoVisible() {
        var isVisible = false;
        vm.eduComps = vm.eduComps || [];

        if (vm.eduComps.length <= 0) {
            return isVisible;
        }
        // itrate through all the fields
        for (var i = 0; i < vm.eduComps.length; i++) {

            //check whether field is visible or not
            if (shouldShowField(true, vm.eduComps[i]['edu_flg'])) {
                //if visible
                //break loop and return
                isVisible = true;
                break;
            }
        }
        return isVisible;
    }

    function saveEduInfo() {
        var obj = vm.eduForm;

        if (!obj.edu_nm || obj.edu_nm.length <= 0) {
            vm.eduErr = "Institution Name is required";
            return;
        }

        obj.edu_flg = obj.edu_flg || 'public';
        obj.usr_id = vm.targetedUser;

        //set from date
        if (vm.eduForm.frm_dt_present) {
            obj.frm_dt = "present";
        } else {
            obj.frm_dt = vm.eduForm.frm_dt;
        }

        //set to date
        if (vm.eduForm.to_dt_present) {
            obj.to_dt = "present";
        } else {
            obj.to_dt = vm.eduForm.to_dt;
        }

        delete obj.frm_day;
        delete obj.frm_mn;
        delete obj.frm_yr;
        delete obj.frm_dt_present;
        delete obj.to_day;
        delete obj.to_mn;
        delete obj.to_yr;
        delete obj.to_dt_present;
        obj.city = vm.selectedCity;
        obj.country = vm.selectedCountry;

        profile.saveEduInfo(obj)
            .then(function (res) {
                console.log(res);
                resetEduInfo();
                getEduInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function resetEduInfo() {
        vm.eduForm = {};
        vm.isAddSchoolInProgress = false;
        vm.isEditSchoolInProgress = false;
        vm.currentlyEditingEduInfo = '';
    }

    function deleteEduInfo(wrk_sk) {
        var obj = {
            usr_id: vm.targetedUser,
            usr_wrk_sk: wrk_sk
        }
        profile.deleteEduInfo(obj)
            .then(function (res) {
                getEduInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function editEduInfo(wrk_sk) {
        vm.cities_edu_ref = [];
        // Set Currently editing work info
        vm.currentlyEditingEduInfo = wrk_sk;
        // Set new add work info to false
        vm.isAddSchoolInProgress = false;
        //set editing form to true
        vm.isEditSchoolInProgress = true;

        var edu_obj = $filter('filter')(vm.eduComps, {
            usr_wrk_sk: wrk_sk
        })[0];

        vm.eduForm = edu_obj;

        if (vm.eduForm.frm_dt === "present") {
            vm.eduForm.frm_dt_present = true;
        } else if(vm.eduForm.frm_dt){
            //find fromdate component        
            var frm_dt_arr = vm.eduForm.frm_dt.split("/");
            vm.eduForm.frm_day = frm_dt_arr[0];
            vm.eduForm.frm_mn = frm_dt_arr[1];
            vm.eduForm.frm_yr = frm_dt_arr[2];
        }

        if (vm.eduForm.to_dt === "present") {
            vm.eduForm.to_dt_present = true;
        } else if(vm.eduForm.to_dt ){
            //find todate component
            var to_dt_arr = vm.eduForm.to_dt.split("/");
            vm.eduForm.to_day = to_dt_arr[0];
            vm.eduForm.to_mn = to_dt_arr[1];
            vm.eduForm.to_yr = to_dt_arr[2];
        }
        vm.selectedCity = vm.eduForm.city;
        vm.selectedCountry = vm.eduForm.country;
        console.log(vm.eduForm)
       vm.getCountryList({ cntry_id: vm.eduForm.country }, 'cities_edu_ref', 'selectedCity', vm.eduForm.city);
    }

    ///////////////////////
    // Exam form helper //
    /////////////////////


    function getExamInfo() {
        profile.getExamInfo({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                vm.exams = res.data;
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function isAnyExamInfoVisible() {
        var isVisible = false;
        vm.exams = vm.exams || [];

        if (vm.exams.length <= 0) {
            return isVisible;
        }
        // itrate through all the fields
        for (var i = 0; i < vm.exams.length; i++) {

            //check whether field is visible or not
            if (shouldShowField(true, vm.exams[i]['exm_flg'])) {
                //if visible
                //break loop and return
                isVisible = true;
                break;
            }
        }
        return isVisible;
    }

    function saveExamInfo(examReqFlag, targetReqFlag) {
        var obj = vm.examForm;
        if (obj.exm_nm && obj.tgt_yr) {
            obj.exm_flg = obj.exm_flg || 'public';
            obj.usr_id = vm.targetedUser;
            profile.saveExamInfo(obj)
                .then(function (res) {
                    console.log(res);
                    resetExamInfo();
                    getExamInfo();
                })
                .catch(function (err) {
                    console.error(err);
                })
        } else if (!obj.exm_nm) {
            vm[examReqFlag] = true;
            $timeout(function () {
                vm[examReqFlag] = false;
            }, 2000)
        } else if (!obj.tgt_yr) {
            vm[targetReqFlag] = true;
            $timeout(function () {
                vm[targetReqFlag] = false;
            }, 2000)
        }

    }

    function resetExamInfo() {
        vm.examForm = {};
        vm.isAddExamInProgress = false;
        vm.isEditExamInProgress = false;
        vm.currentlyEditingExamInfo = '';
    }

    function deleteExamInfo(exm_sk) {
        var obj = {
            usr_id: vm.targetedUser,
            usr_exm_sk: exm_sk
        }
        profile.deleteExamInfo(obj)
            .then(function (res) {
                getExamInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function editExamInfo(exm_sk) {
        // Set Currently editing work info
        vm.currentlyEditingExamInfo = angular.copy(exm_sk);
        // Set new add work info to false
        vm.isAddExamInProgress = false;
        //set editing form to true
        vm.isEditExamInProgress = true;

        var exam_obj = $filter('filter')(vm.exams, {
            usr_exm_sk: angular.copy(exm_sk)
        })[0];

        vm.examForm = angular.copy(exam_obj);
    }

    ////////////////////////////////////
    // Interested Course form helper //
    //////////////////////////////////


    function getInterestedCourseInfo() {
        profile.getInterestedCourseInfo({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                vm.interestedCourses = res.data;
                console.log(vm.interestedCourses)
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function isAnyInterestedCourseInfoVisible() {
        var isVisible = false;
        vm.interestedCourses = vm.interestedCourses || [];

        if (vm.interestedCourses.length <= 0) {
            return isVisible;
        }
        // itrate through all the fields
        for (var i = 0; i < vm.interestedCourses.length; i++) {

            //check whether field is visible or not
            if (shouldShowField(true, vm.interestedCourses[i]['crs_flg'])) {
                //if visible
                //break loop and return
                isVisible = true;
                break;
            }
        }
        return isVisible;
    }

    function saveInterestedCourseInfo(errorFlag, requreErrorFlag) {
        if (!vm.interestedCourseForm) {
            vm[requreErrorFlag] = true;
            $timeout(function () {
                vm[requreErrorFlag] = false;
            }, 2000)
        } else if (vm.interestedCourses.map(function (obj) { return obj.crs_nm }).indexOf(vm.interestedCourseForm.crs_nm) === -1) {
            var obj = vm.interestedCourseForm;
            obj.crs_flg = obj.crs_flg || 'public';
            obj.usr_id = vm.targetedUser;
            profile.saveInterestedCourseInfo(obj)
                .then(function (res) {
                    console.log(res);
                    resetInterestedCourseInfo();
                    getInterestedCourseInfo();
                    vm.interestedCourseForm = {}
                })
                .catch(function (err) {
                    console.error(err);
                })
        } else {
            vm[errorFlag] = true;
            $timeout(function () {
                vm[errorFlag] = false;
            }, 2000)
        }
    }

    function resetInterestedCourseInfo() {
        vm.examForm = {};
        vm.isAddInterestedCourseInProgress = false;
        vm.isEditInterestedCourseInProgress = false;
        vm.currentlyEditingInterestedCourseInfo = '';
    }

    function deleteInterestedCourseInfo(crs_sk) {
        var obj = {
            usr_id: vm.targetedUser,
            usr_crs_sk: crs_sk
        }
        profile.deleteInterestedCourseInfo(obj)
            .then(function (res) {
                getInterestedCourseInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function editInterestedCourseInfo(crs_sk) {
        console.log(crs_sk)
        // Set Currently editing work info
        vm.currentlyEditingInterestedCourseInfo = crs_sk;
        // Set new add work info to false
        vm.isAddInterestedCourseInProgress = false;
        //set editing form to true
        vm.isEditInterestedCourseInProgress = true;

        var crs_obj = $filter('filter')(vm.interestedCourses, {
            usr_crs_sk: crs_sk
        })[0];

        vm.interestedCourseForm = angular.copy(crs_obj);
    }


    //////////////////////////////
    // Author Info form helper //
    ////////////////////////////


    function getAuthorInfo() {
        profile.getAuthorInfo({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                vm.authorCourses = res.data;
                if (vm.authorCourses.length > 0) {
                    vm.authorForm = vm.authorForm || {};
                    vm.authorForm.author_desc = vm.authorCourses[0].author_desc;
                }
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function isAnyAuthorInfoVisible() {
        vm.authorCourses = vm.authorCourses || [];
        if (vm.authorCourses.length <= 0) {
            return false;
        }
        return true;
    }

    function saveAuthorInfo() {
        var obj = vm.authorForm;
        obj.usr_id = vm.targetedUser;


        profile.saveAuthorInfo(obj)
            .then(function (res) {
                console.log(res);
                resetAuthorInfo();
                getAuthorInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function saveAuthorDescInfo() {
        var obj = {};
        obj.tch_desc = vm.authorForm.author_desc;
        obj.usr_id = vm.targetedUser;
        profile.saveAuthorDescInfo(obj)
            .then(function (res) {
                vm.editAuthorDescIsInProgress = false;
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function resetAuthorInfo() {
        vm.authorForm = {};
        vm.isAddAuthorCourseInProgress = false;
        vm.isEditAuthorCourseInProgress = false;
        vm.currentlyEditingAuthorCourseInfo = '';
    }

    function deleteAuthorInfo(bk_sk) {
        var obj = {
            usr_id: vm.targetedUser,
            usr_bk_sk: bk_sk
        }
        profile.deleteAuthorInfo(obj)
            .then(function (res) {
                getAuthorInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function editAuthorInfo(bk_sk) {
        // Set Currently editing work info
        vm.currentlyEditingAuthorCourseInfo = bk_sk;
        // Set new add work info to false
        vm.isAddAuthorCourseInProgress = false;
        //set editing form to true
        vm.isEditAuthorCourseInProgress = true;

        var author_obj = $filter('filter')(vm.authorCourses, {
            usr_bk_sk: bk_sk
        })[0];

        vm.authorForm = author_obj;
    }


    //////////////////////////////
    // Teacher Info form helper //
    ////////////////////////////


    function getTeacherInfo() {
        profile.getTeacherInfo({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                vm.teacherCourses = res.data;
                if (vm.teacherCourses.length > 0) {
                    vm.teacherForm = vm.teacherForm || {};
                    vm.teacherForm.tch_desc = vm.teacherCourses[0].tch_desc;
                }
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function isAnyTeacherInfoVisible() {
        vm.teacherCourses = vm.teacherCourses || [];
        if (vm.teacherCourses.length <= 0) {
            return false;
        }
        return true;
    }

    function saveTeacherInfo() {
        var obj = vm.teacherForm;
        obj.usr_id = vm.targetedUser;
        profile.saveTeacherInfo(obj)
            .then(function (res) {
                console.log(res);
                resetTeacherInfo();
                getTeacherInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function saveTeacherDescInfo() {
        var obj = {};
        obj.tch_desc = vm.teacherForm.tch_desc;
        obj.usr_id = vm.targetedUser;
        profile.saveTeacherDescInfo(obj)
            .then(function (res) {
                vm.editTeacherDescIsInProgress = false;
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function resetTeacherInfo() {
        vm.teacherForm = {};
        vm.isAddTeacherCourseInProgress = false;
        vm.isEditTeacherCourseInProgress = false;
        vm.currentlyEditingTeacherCourseInfo = '';
    }

    function deleteTeacherInfo(crs_sk) {
        var obj = {
            usr_id: vm.targetedUser,
            tch_crs_sk: crs_sk
        }
        profile.deleteTeacherInfo(obj)
            .then(function (res) {
                getTeacherInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function editTeacherInfo(crs_sk) {
        // Set Currently editing work info
        vm.currentlyEditingTeacherCourseInfo = crs_sk;
        // Set new add work info to false
        vm.isAddTeacherCourseInProgress = false;
        //set editing form to true
        vm.isEditTeacherCourseInProgress = true;

        var teacher_obj = $filter('filter')(vm.teacherCourses, {
            tch_crs_sk: crs_sk
        })[0];

        vm.teacherForm = teacher_obj;
    }

    //////////////////////////////
    // Kid Info form helper //
    ////////////////////////////


    function getKidInfo() {
        profile.getKidInfo({
            usr_id: vm.targetedUser
        })
            .then(function (res) {
                vm.kidDetails = res.data;
                vm.kidRows = [];
                vm.kidDetails.forEach(function (v, i) {
                    var obj = {
                        usr_id: v.usr_id,
                        kid_usr_id: v.kid_usr_id,
                        kid_edu_nm: v.kid_edu_nm,
                        kid_exm_nm: v.kid_exm_nm
                    }
                    vm.kidRows.push(obj);
                })
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function isAnyKidInfoVisible() {
        return true;
    }

    function saveKidInfo() {

        var obj = vm.kidForm;
        obj.kid_usr_id = vm.selectedKid[0];
        obj.usr_id = vm.targetedUser;
        profile.saveKidInfo(obj)
            .then(function (res) {
                console.log(res);
                resetKidInfo();
                getKidInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function resetKidInfo() {
        vm.teacherForm = {};
        vm.isAddKidInProgress = false;
        vm.isEditKidInProgress = false;
        vm.currentlyEditingKidInfo = '';
    }

    function deleteKidInfo(kid_usr_id, exm_nm) {
        var obj = {
            usr_id: vm.targetedUser,
            kid_usr_id: kid_usr_id,
            kid_exm_nm: exm_nm || ''
        }
        profile.deleteKidInfo(obj)
            .then(function (res) {
                getKidInfo();
            })
            .catch(function (err) {
                console.error(err);
            })
    }

    function editKidInfo(kid_usr_id) {
        // Set Currently editing work info
        vm.currentlyEditingKidInfo = kid_usr_id;
        // Set new add work info to false
        vm.isAddKidInProgress = false;
        //set editing form to true
        vm.isEditKidInProgress = true;

        var teacher_obj = $filter('filter')(vm.kids, {
            kid_usr_id: kid_usr_id
        })[0];

        vm.kidForm = teacher_obj;
    }
    // for edit popup.
    function openEditConfirmPopup() {
        console.log('p')

        if (!vm.shouldBasicFieldEditable && vm.canUserEditInfo()) {
            console.log('open edit popup.');
            vm.showEditConfirmDialogBox = true;
        }
    }
    function confirmEditBox() {
        console.log('confirm');
        $timeout(function () {
            vm.showEditConfirmDialogBox = false;
            vm.shouldBasicFieldEditable = true;
        }, 10);
    }
    function cancelEditBox() {
        console.log('confirm');
        $timeout(function () {
            vm.showEditConfirmDialogBox = false;
            vm.shouldBasicFieldEditable = false;
        }, 10);
    }

    function _dateSelection(data) {
        var _format = "dd/mm/yyyy";
        var _delimiter = "/";
        var formatLowerCase = _format.toLowerCase();
        var formatItems = formatLowerCase.split(_delimiter);
        var dateItems = data.split(_delimiter);
        var monthIndex = formatItems.indexOf("mm");
        var dayIndex = formatItems.indexOf("dd");
        var yearIndex = formatItems.indexOf("yyyy");
        var month = parseInt(dateItems[monthIndex]);
        month -= 1;
        var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
        vm.setMinLimit = new Date(formatedDate).toDateString()
    }

    function _getCountryList(country, cityVariableKey, selectKey, seletedData) {
        getCountryCityService.getCity(country.cntry_id)
            .then(function (res) {
                vm[cityVariableKey] = res.data;
                vm[selectKey] = seletedData;
            })
    }

    function _removeYearSelection() {
        $timeout(function () {
            $('#dobPicker').find('._720kb-datepicker-calendar').find('._720kb-datepicker-calendar-header-middle').find('a').remove();
        }, 100)
    }
}