var io = require('socket.io_client');
require('serviceModule');
require('directiveModule');
require('authToken');
require('service.chat');
require('angular-ui-select/dist/select.min.css');
require('angular-ui-select/dist/select.min.js');
require('angular-image-crop/image-crop-styles.css');
require('ng-file-upload');
require('angular-image-crop/image-crop.js');
require("ErrImage");

require('alertify.js/dist/js/alertify.js');
require('alertify.js/dist/js/ngAlertify.js');
require('alertify.js/dist/css/alertify.css');
// require('service.websocket');
require("service.chatUtility");

var notification_sound_path = require('../../../assets/audios/notification.mp3');
require('./_chat.scss');

var uuid = require('uuid');

angular.module('directive')
    .component('chat', {
        template: ['$element', '$attrs',
            function($element, $attrs) {
                return require('./chat.html');
            }
        ],
        controller: ChatController,
        controllerAs: 'vm',
        bindings: {
            showChatComponent: '=' // whether to show chat component or not
        }
    });

ChatController.$inject = ['authToken','$timeout', '$scope', '$filter', 'chat', 'Upload', 'alertify','BASE_API','chatUtility'];

function ChatController(authToken,$timeout, $scope, $filter, chat, Upload, alertify,BASE_API,chatUtility) {
    var vm = this;
    var userKey = "usr_id";

    var RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription;
    var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    var configuration = {
        'iceServers': [{
            'url': 'stun:stun.l.google.com:19302'
        }]
    };
    vm.rtcPeerConn;
    vm.videoStream;

    vm.friendListOpened = true;
    vm.openedChats = [];
    vm.socketId = "/#";
    vm.msgs = {};
    vm.isModalVisible = false;
    vm.chatMembers = [];
    vm.frndList = [];
    vm.connectedMembers = {}; // Online Members
    vm.newFriendsToGrp = [];
    vm.allFriendsInfo = {}; // id -> detail map

    vm.isCreateGrpModalVisible = false;

    vm.isUserLoggedIn = isUserLoggedIn;
    vm.openGroupModal = openGroupModal;
    vm.openChatOf = openChatOf;
    vm.closeChatOf = closeChatOf;
    vm.toggleChatOf = toggleChatOf;
    vm.sendMsg = sendMsg;
    vm.sendMsgFromModalOnKeyPress = sendMsgFromModalOnKeyPress;
    vm.openAndFetchMsgs = openAndFetchMsgs;
    vm.isUserGrpOnline = isUserGrpOnline;
    vm.shouldShowPlaceHolder = shouldShowPlaceHolder;
    vm.removePlaceHolder = removePlaceHolder;
    vm.setActiveMember = setActiveMember;
    vm.sendMsgFromModal = sendMsgFromModal;
    vm.clearAllMsgOf = clearAllMsgOf;
    vm.getOpenMemberIndex = getOpenMemberIndex;
    // vm.toggleHeightWidthOfFriendList = toggleHeightWidthOfFriendList;
    // vm.toggleHeightWidthOfMsgs = toggleHeightWidthOfMsgs;
    vm.visibleModalWithMember = visibleModalWithMember;

    vm.getUserInfo = getUserInfo;

    vm.findFriends = findFriends;
    vm.isUserOnline = isUserOnline;

    vm.createGroup = createGroup;
    vm.addMembersToGrp = addMembersToGrp;

    vm.startAudioCall = startAudioCall;
    vm.stopAudioCall = stopAudioCall;
    vm.startVideoCall = startVideoCall;
    vm.stopVideoCall = stopVideoCall;
    vm.acceptAudioCall = acceptAudioCall;
    vm.declineAudioCall = declineAudioCall;

    vm.acceptVideoCall = acceptVideoCall;
    vm.declineVideoCall = declineVideoCall;

    var mySound = new Audio(notification_sound_path);

    $scope.$watch('vm.grpPic', function(newValue, oldValue, scope) {
        if (newValue) {
            console.log(newValue);
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    vm.showGrpPicModal = true;
                    vm.image = evt.target.result;
                });
            };
            reader.readAsDataURL(newValue);
        }
    });

    vm.cropImage = function cropImage() {
        vm.initCrop = true;
        setTimeout(function() {
            vm.showGrpPicModal = false;
            $scope.$apply();
        }, 100);
    }

    vm.$onInit = function() {

        $(window).on('focus', function() {
            vm.isTabbedFocus = true;
        });
        $(window).on('blur', function() {
            vm.isTabbedFocus = false;
        });

        if (authToken.isAuthenticated()) {
            vm.getUserInfo();
            vm.findFriends()
                .then(function() {
                    // socketInit();
                })
                .catch(function() {
                    console.error(err);
                });
        }
    }

    function socketInit() {
        var host = window.location.hostname;
        vm.socket = {};//websocket.socket;//io(BASE_API + '/');
        console.log('Socket Trying to connect ' + BASE_API)
        // if (host.indexOf("localhost") <= -1) {
        //     //If not localhost
        //     vm.socket = io('https://' + host + '/');
        //     console.log('Socket Trying to connect https://' + host + '/')
        // } else {
        //     //If localhost
        //     vm.socket = io('http://' + host + '/');
        //     console.log('Socket Trying to connect http://' + host + '/')
        // }
        // vm.socket = websocket.socket;

        vm.socket.on('connect', function() {
            vm.socketId = vm.socket.id;
            console.info("Connect", vm.socket.id);
            authToken.setSocketId(vm.socket.id);
            var user = authToken.getUser();
            vm.userInfo = {
                user_id: user[userKey],
                display_name: user.dsp_nm
            };
            console.log("sending userInfo to server socket....")
            vm.socket.emit('userInfo', {
                socketId: vm.socketId,
                user_id: vm.userInfo.user_id,
                display_name: vm.userInfo.display_name
            });
        });

        vm.socket.on('connectedSockets', function(data) {
            var clients = data.clients;
            var idStr = vm.socketId;
            vm.connectedMembers = {};
            var members = clients.filter(function(v) {
                vm.connectedMembers[v.user_id] = v.user_id;
                if (v.user_id === vm.userInfo.user_id) return false;
                return true;
            });
            vm.activeMember = members[0];
            vm.activeGroup = '';

            console.log("Connected mems:", data);
            $scope.$apply();
        });

        vm.socket.on('disconnect', function() {
            console.info("Socket DisConnected");
            authToken.removeSocketId();
        });

        vm.socket.on('logoutMsg', function(data) {
            console.info("logoutMsg in chat component", data);
            alertify.alert(data.msg, function() {
                window.location.href = "/login/session-expired"
            });
        });

        vm.socket.on('disconnectedSockets', function(data) {
            var id = data.socket;
            console.log("Connected mems:", vm.connectedMembers);
            if (vm.connectedMembers[id]) {
                delete vm.connectedMembers[id];
            }
        });

        vm.socket.on('newMsg', function(d) {
            console.log('newMsg......', d);
            vm.msgs[d.frm_usr] = vm.msgs[d.frm_usr] || [];
            d.msg_ts = new Date(d.msg_ts);
            vm.msgs[d.frm_usr].push(d);
            openIncomingChat(d.frm_usr);
            scrollToEndOnNewMsg(d.frm_usr);
            if (!vm.isTabbedFocus) {
                mySound.play();
            }
            $scope.$apply();
        });

        vm.socket.on('newTypingMsg', function(d) {
            console.log('newTypingMsg......', d);
            vm.newMesageUserId = d.user_id;
            vm.msgs[d.frm_usr] = vm.msgs[d.frm_usr] || [];
            clearTimeout(vm.activeMember["timerId"]);

            if (!vm.isTabbedFocus) {
                vm.activeMember["isTyping"] = true;
                vm.activeMember["timerId"] = setTimeout(function(){
                    vm.activeMember.isTyping = false
                    $scope.$apply();
                },2000)
            }
            $scope.$apply();
        });

        vm.socket.on('selfNewMsg', function(d) {
            console.log('selfNewMsg......', d);
            vm.newMesageUserId = d.user_id;
            var isThereAnySameMsg = vm.msgs[d.fr_id].filter(function(v, i) {
                if (v.msg_id === d.msg_id) {
                    return true;
                }
                return false;
            })
            if (isThereAnySameMsg.length <= 0) {
                vm.msgs[d.fr_id] = vm.msgs[d.fr_id] || [];
                d.msg_ts = new Date(d.msg_ts);
                vm.msgs[d.fr_id].push(d);
                scrollToEndOnNewMsg(d.fr_id);
                $scope.$apply();
            }
        });

        vm.socket.on('newMsgInGroup', function(d) {
            //fr_id means grp_id
            // console.log(d);
            if (d.frm_usr !== vm.userInfo.user_id) {
                vm.msgs[d.fr_id] = vm.msgs[d.fr_id] || [];
                d.msg_ts = new Date(d.msg_ts);
                vm.msgs[d.fr_id].push(d);
                openIncomingChat(d.fr_id);
                scrollToEndOnNewMsg(d.fr_id);
                if (!vm.isTabbedFocus) {
                    mySound.play();
                }
                $scope.$apply();
            }
        });

         vm.socket.on('newTypingMsgInGroup', function(d) {
            //fr_id means grp_id
            // console.log(d);
            if (d.frm_usr !== vm.userInfo.user_id) {
                vm.msgs[d.fr_id] = vm.msgs[d.fr_id] || [];
                d.msg_ts = new Date(d.msg_ts);
                vm.msgs[d.fr_id].push(d);
                openIncomingChat(d.fr_id);
                scrollToEndOnNewMsg(d.fr_id);
                if (!vm.isTabbedFocus) {
                    //mySound.play();
                }
                $scope.$apply();
            }
        });

        vm.socket.on('startAudioCallRequest', function(d) {
            console.log("startAudioCallRequest", vm.allFriendsInfo[d.from]);
            vm.activeMediaPerson = vm.allFriendsInfo[d.from].display_name;
            vm.isCallAccepted = false;
            vm.isActiveAudioCall = true;
            vm.currentCallRequest = d;
            vm.isCallEnded = false;
            vm.isCallConnecting = true;
            vm.isCallConnected = false;
            $scope.$apply();
        });

        vm.socket.on('startVideoCallRequest', function(d) {
            console.log("startVideoCallRequest", vm.allFriendsInfo[d.from]);
            vm.activeMediaPerson = vm.allFriendsInfo[d.from].display_name;
            vm.isActiveVideoCall = true;
            vm.isCallAccepted = false;
            vm.currentCallRequest = d;
            vm.isCallEnded = false;
            vm.isCallConnecting = true;
            vm.isCallConnected = false;
            $scope.$apply();
        });

        vm.socket.on('acceptedAudioCall', function(d) {
            console.log("acceptedAudioCall", vm.allFriendsInfo[d.from]);
            if (!vm.rtcPeerConn) {
                startSignaling(true, false);
            }
            vm.isCallEnded = false;
            vm.isCallConnecting = false;
            vm.isCallConnected = true;
            $scope.$apply();
        });

        vm.socket.on('acceptedVideoCall', function(d) {
            console.log("acceptedAudioCall", vm.allFriendsInfo[d.from]);
            if (!vm.rtcPeerConn) {
                startSignaling(false, false);
            }
            vm.isCallEnded = false;
            vm.isCallConnecting = false;
            vm.isCallConnected = true;
            $scope.$apply();
        });

        vm.socket.on('endCall', function(d) {
            endCallOnSocketMsg();
            $scope.$apply();
        });

        vm.socket.on('signaling_message', function(data) {
            var message = JSON.parse(data.message);
            console.log(data);
            if (message.sdp) {
                vm.shouldAddICE = true;
                vm.rtcPeerConn.setRemoteDescription(new RTCSessionDescription(message.sdp), function() {
                    // if we received an offer, we need to answer
                    if (vm.rtcPeerConn.remoteDescription.type == 'offer') {
                        console.log("offer came,sent answer");
                        vm.rtcPeerConn.createAnswer(sendLocalDesc, function(err) {
                            console.error(err);
                        });
                    }
                }, function(err) {
                    console.error(err);
                });
            } else {
                if (vm.shouldAddICE) {
                    vm.rtcPeerConn.addIceCandidate(new RTCIceCandidate(message.candidate));
                }
            }
            $scope.$apply();
        });
    }

    function isUserLoggedIn() {
        var loggedIn = authToken.isAuthenticated();
        return loggedIn;
    }

    function getUserInfo() {
        var user = authToken.getUser();
        vm.userInfo = {
            display_name: user.displayName,
            user_id: user[userKey]
        }
        return vm.userInfo;
    }

    function openGroupModal() {
        vm.isCreateGrpModalVisible = true;
    }

    function openChatOf(member) {
        var item = $filter('filter')(vm.openedChats, {
            member: member
        })[0] || [];
        item.closed = false;
        item.minimized = false;
        item.color = false;
        scrollToEndOnNewMsg(member.user_id);
    }

    function closeChatOf(member) {
        var item = $filter('filter')(vm.openedChats, {
            member: member
        })[0];
        if (item) {
            var pos = vm.openedChats.indexOf(item);
            vm.openedChats[pos].closed = true;
            vm.openedChats[pos].minimized = true;
        }
        chatUtility.deleteMember(member.user_id);
    }

    function toggleChatOf(member) {
        var item = $filter('filter')(vm.openedChats, {
            member: member
        })[0] || [];

        if (!item.minimized) {
            item.minimized = true;
        } else {
            openChatOf(member);
        }
    }

    function openIncomingChat(user_id) {
        var item = $filter('filter')(vm.openedChats, {
            'member': {
                'user_id': user_id
            }
        })[0];
        item.closed = false;
        if (item.minimized) {
            item.color = true;
            item.minimized = true;
        }
    }

    function getOpenMemberIndex(user_id) {
        var item = $filter('filter')(vm.openedChats, {
            'member': {
                'user_id': user_id
            }
        })[0];
        var pos = vm.openedChats.indexOf(item);
        return pos;
    }

    function scrollToEndOnNewMsg(user_id) {
        if (vm.isModalVisible) {
            setTimeout(function() {
                var id = '#msgs';
                var ele = $(id)[0];
                ele.scrollTop = ele.scrollHeight;
            });
        } else {
            setTimeout(function() {
                var id = '#msgs-wrapper_' + getOpenMemberIndex(user_id);
                var ele = $(id)[0];
                ele.scrollTop = ele.scrollHeight;
            });
        }

    }

    function clearAllMsgOf(member) {
        var user_id = member.user_id;
        var confirmation = confirm("Do you really want to delete messages?");
        if (!confirmation) {
            return;
        }
        vm.msgs[user_id] = [];
        var o = {
            user_id: vm.userInfo.user_id,
            fr_id: member.user_id
        }
        chat.clearMsgs(o)
            .then(function(res) {
                console.log(res);
            })
            .catch(function(err) {
                console.error(err);
            })
    }


    function sendMsgFromSocket(msg, data) {
        vm.socket.emit(msg, data);
    }

    function send(member, fromModal) {
        var msgFrom = vm.getUserInfo();
        var msgTo = member;
        var id = "";

        if (fromModal) {
            id = '#msg';
        } else {
            id = '#msg_' + getOpenMemberIndex(member.user_id);
        }

        var msg = $(id).html();
        if (msg.length > 0 && $(id).text().trim().length > 0) {
            var t = Date.now();
            var format = new Date(t);

            var data = {
                    frm_usr: msgFrom.user_id,
                    user_id: msgFrom.user_id,
                    fr_id: msgTo.user_id,
                    msg: msg,
                    msg_id: '' + Math.floor(100000 + Math.random() * 900000),
                    msg_ts: new Date()
                }
                // vm.msgs[data.fr_id] = vm.msgs[data.fr_id] || [];
                // vm.msgs[data.fr_id].push(data);
                //
            vm.msgs[data.fr_id] = vm.msgs[data.fr_id] || [];
            vm.msgs[data.fr_id].push(data);
            scrollToEndOnNewMsg(data.fr_id);

            if (member.typ === 'grp') {
                sendMsgFromSocket('sendMsgToGroup', data);
            } else if (member.typ === 'usr') {
                sendMsgFromSocket('sendMsg', data);
            }

            setTimeout(function() {
                $(id).html('');
                $(id).trigger("click");
            }, 10);
            var ele;
            if (fromModal) {
                ele = $(id).parent().siblings('.msgs')[0];
            } else {
                ele = $(id).parent().siblings('.msgs-wrapper')[0];
            }
            setTimeout(function() {
                ele.scrollTop = ele.scrollHeight;
            });

        }
    }

    function sendTypingMsg(member) {
        debugger;
        var msgFrom = vm.getUserInfo();
        var msgTo = member;
        var id = "";

        var data = {
            frm_usr: msgFrom.user_id,
            user_id: msgFrom.user_id,
            fr_id: msgTo.user_id
        }
        console.log("sendTypingMsg "+member.typ)
        if (member.typ === 'grp') {
            sendMsgFromSocket('sendTypingMsgToGroup', data);
        } else if (member.typ === 'usr') {
            sendMsgFromSocket('sendTypingMsg', data);
        }

    }

    function sendMsg($event, member) {
        sendTypingMsg(member);

        if ($event.keyCode === 13 && !$event.shiftKey) {
            send(member, false);
        }
    }

    function sendMsgFromModalOnKeyPress($event, member) {
        sendTypingMsg(member);

        if ($event.keyCode === 13 && !$event.shiftKey) {
            send(member, true);
        }
    }

    function sendMsgFromModal(member) {
        sendTypingMsg(member);
        send(member, true);
    }


    function openAndFetchMsgs(member,flag) {
        console.log(member);
        vm.openChatOf(member);
        var d = {
            user_id: vm.userInfo.user_id,
            fr_id: member.user_id
        }
        if (vm.msgs[d.fr_id]) {
            return;
        }
        if (!flag) {
            chatUtility.openChatList(member.user_id);
        }
        chat.fetchMsgs(d)
            .then(function(d) {
                var msgs = d.data;

                msgs = msgs.map(function(v) {
                    v.msg_ts = new Date(v.msg_ts)
                    return v;
                });
                vm.msgs[member.user_id] = msgs;
                if (vm.isModalVisible) {
                    //If Modal is open then only one wrapper
                    setTimeout(function() {
                        var id = '#msgs';
                        var ele = $(id)[0];
                        ele.scrollTop = ele.scrollHeight;;
                    })
                } else {
                    //If Modal is open then multiple wrapper
                    setTimeout(function() {
                        var id = '#msgs-wrapper_' + getOpenMemberIndex(member.user_id);
                        var ele = $(id)[0];
                        ele.scrollTop = ele.scrollHeight;;
                    })
                }

            })
            .catch(function(err) {
                console.error(err);
            })

        if (member.typ == 'grp') {
            //Basically create room on server side
            sendMsgFromSocket('createGroup', {
                room_id: member.user_id
            });
        }
    }

    function setActiveMember(member) {
        vm.activeMember = member;
        vm.openAndFetchMsgs(member);
    }

    function shouldShowPlaceHolder($event, user_id) {
        // var id = '#msg_' + user_id.substr(2);
        if ($event.keyCode === 13)
            return;
        var id = $event.target;
        var element = $(id);
        if (element.text().length <= 0) {
            // element.html('Type a Message').addClass('gray-msg');
        }
    }

    function removePlaceHolder($event, user_id) {
        // var id = '#msg_' + user_id.substr(2);
        var id = $event.target;
        var element = $(id);
        if (element.text() === 'Type a Message') {
            element.html('').removeClass('gray-msg');
        }
    }

    function visibleModalWithMember(user_id) {
        var item = $filter('filter')(vm.chatMembers, {
            'user_id': user_id
        })[0];
        vm.activeMember = item;
        // console.log('visibleModalWithMember', vm.activeMember);
        vm.isModalVisible = true;
        // console.log(vm.chatMembers);
    }

    function isUserOnline(user_id) {
        // console.log("Userid......",user_id , !!vm.connectedMembers[user_id]);
        return !!vm.connectedMembers[user_id];
    }

    function isUserGrpOnline(grp_mem) {
        // console.log("Userid......",user_id , !!vm.connectedMembers[user_id]);

        var isOnline = false;
        for(var x in grp_mem) {
            if(x != vm.userInfo.user_id && vm.connectedMembers[x]) {
                isOnline = true;
                break;
            }
        }

        return isOnline;
    }

    function findFriends() {
        var user = authToken.getUser();
        var user_id = user[userKey];
        var keyword = vm.searchFriend || '';
        vm.openedChats = [];
        var d = {
            user_id: user_id,
            keyword: keyword
        }
        return chat.findFriends(d)
            .then(function(d) {
                var frnds = d.data;
                vm.chatMembers = vm.chatMembers || [];
                frnds.forEach(function(v, i) {
                    if (v && v.user_id != user_id) {
                        vm.chatMembers.push(v);
                        if(v.typ == "usr") {
                            vm.frndList.push(v);
                        }
                    }
                });
                if (chatHistory!=null) {
                    openChantWindow(vm.chatMembers);
                }
                arr = frnds.filter(function(v) {
                    //If it is grp , add it to connectedMembers list
                    if (v.typ === 'grp') {
                        vm.connectedMembers[v.user_id] = v.user_id;
                    }
                    var obj = {
                        member: v,
                        minimized: true,
                        closed: true
                    }

                    //Store all members as a closed and in minimize state
                    vm.openedChats.push(obj);
                    if (v.typ === 'usr') {
                        //Store details of members
                        vm.allFriendsInfo[v.user_id] = v;
                    }
                    //if its a grp , fetch members
                    if (v.typ === 'grp') {
                        // chat.findMembers({
                        //         grp_id: v.user_id
                        //     })
                        //     .then(function(res) {
                        //         var mems = res.data;

                        //         var item = $filter('filter')(vm.chatMembers, {
                        //             'user_id': v.user_id
                        //         })[0];
                        //         item.members = mems;
                        //         //Iterate for all members
                        //         for (v in mems) {
                        //             //If member detail is not there
                        //             if (!vm.allFriendsInfo[v]) {
                        //                 //find member detail
                        //                 // chat.findFriendWithId({
                        //                 //         user_id: v
                        //                 //     })
                        //                 //     .then(function(resUser) {
                        //                 //         var user = resUser.data;
                        //                 //         // store member details
                        //                 //         vm.allFriendsInfo[user.user_id] = user;
                        //                 //     })
                        //                 //     .catch(function(err) {
                        //                 //         console.error(err);
                        //                 //     })
                        //             }
                        //         }
                        //     })
                        //     .catch(function(err) {
                        //         console.error(err);
                        //     })
                    }
                    return true;
                });
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function createGroup() {
        console.log("createGroup");
        var grp_id = uuid.v4().replace(/-/g, '');
        var grp_nm = vm.grp_nm;
        var grp_owm = vm.userInfo.user_id;
        var members = [];
        if (vm.selectedFriends) {
            members = vm.selectedFriends.map(function(v) {
                return v;
            });
        }
        members.push(grp_owm);
        var obj = {
            grp_id: grp_id,
            grp_nm: grp_nm,
            grp_own: grp_owm,
            members: members
        }

        var blob = Upload.dataUrltoBlob(vm.myGrpPic, grp_id + "_pic.jpg");

        Upload.upload({
                url: '/chat/grp/create',
                data: {
                    file: blob,
                    grp_id: grp_id,
                    grp_nm: grp_nm,
                    grp_own: grp_owm,
                    members: members
                }
            })
            .then(function(d) {
                console.log("Group Created.");
                var o = {
                    user_id: obj.grp_id,
                    display_name: obj.grp_nm,
                    user_email: obj.grp_id,
                    typ: 'grp',
                    pic50: vm.myGrpPic
                }

                //Add Group to list
                vm.chatMembers.push(o);

                //Open Group Chat
                vm.openedChats.push({
                    member: o,
                    minimized: false,
                    closed: false
                });

                //As it is grp , it is always connected
                vm.connectedMembers[o.user_id] = o.user_id;

                vm.isCreateGrpModalVisible = false;
            })
            .catch(function(err) {
                console.log(err);
            })
            // chat.createGroup(obj)
            //     .then(function(d){

        //     })

    }

    function addMembersToGrp(obj) {
        // console.log(grp_id,vm.newFriendsToGrp);
        var o = {
            members: vm.newFriendsToGrp,
            grp_id: obj.member.user_id
        }
        chat.addMembersToGroup(o).then(function(res) {
                vm.newFriendsToGrp = [];
                obj.addMemberFieldVisible = false;
                vm.successInAddMember = vm.successInAddMember || {};
                vm.successInAddMember[obj.member.user_id] = true;
                setTimeout(function() {
                    vm.successInAddMember[obj.member.user_id] = false;
                    $scope.$apply();
                }, 2000);
                sendMsgFromSocket('createGroup', {
                    room_id: obj.member.user_id
                });
            })
            .catch(function(err) {
                console.log(err);
            })
    }

    function startAudioCall(calleeId) {
        var obj = {
            from: vm.userInfo.user_id,
            to: calleeId
        };
        vm.isCallAccepted = true;
        sendMsgFromSocket("startAudioCallRequest", obj);
        vm.activeMediaPerson = vm.allFriendsInfo[calleeId].display_name;
        vm.currentCallRequest = obj;
        vm.isActiveAudioCall = true;
        vm.isCallEnded = false;
        vm.isCallConnecting = true;
        vm.isCallConnected = false;

    }

    function stopAudioCall(calleeId) {
        vm.isActiveAudioCall = false;
        console.log(vm.rtcPeerConn);
        closeRTCPeerConnetion();
    }

    function startVideoCall(calleeId) {
        var obj = {
            from: vm.userInfo.user_id,
            to: calleeId
        };
        vm.isCallAccepted = true;
        sendMsgFromSocket("startVideoCallRequest", obj);
        vm.activeMediaPerson = vm.allFriendsInfo[calleeId].display_name;
        vm.currentCallRequest = obj;
        vm.isActiveVideoCall = true;
        vm.isCallEnded = false;
        vm.isCallConnecting = true;
        vm.isCallConnected = false;
    }

    function stopVideoCall(calleeId) {
        vm.isActiveVideoCall = false;
        console.log(vm.rtcPeerConn);
        closeRTCPeerConnetion();
    }

    function endCallOnSocketMsg() {
        if (vm.rtcPeerConn.removeStream) {
            try {
                vm.rtcPeerConn.removeStream(vm.stream);
            } catch (e) {
                console.log("Optional Warning....", e);
            }
        }
        vm.rtcPeerConn.close();
        vm.rtcPeerConn.onicecandidate = null;
        vm.rtcPeerConn.onaddstream = null;
        vm.rtcPeerConn = undefined;
        vm.shouldAddICE = false;

        if (vm.stream.stop) {
            vm.stream.stop();
        } else {
            var arrAudioTracks = vm.stream.getAudioTracks();
            arrAudioTracks.forEach(function(streamTrack) {
                streamTrack.stop();
            });
            var arrVideoTracks = vm.stream.getVideoTracks();
            arrVideoTracks.forEach(function(streamTrack) {
                streamTrack.stop();
            });
        }
        vm.isCallEnded = true;
        vm.isCallConnecting = false;
        vm.isCallConnected = false;
        vm.endCallMsg = "Call is ended.";
    }

    function closeRTCPeerConnetion() {
        if (vm.isCallEnded) {
            //Call is already ended.
            return;
        }
        vm.isActiveAudioCall = false;
        vm.isActiveVideoCall = false;

        if (vm.rtcPeerConn && vm.rtcPeerConn.removeStream) {
            try {
                vm.rtcPeerConn.removeStream(vm.stream);
            } catch (e) {
                console.log("Optional Warning....", e);
            }
        }
        if (vm.rtcPeerConn) {
            vm.rtcPeerConn.close();
            vm.rtcPeerConn.onicecandidate = null;
            vm.rtcPeerConn.onaddstream = null;
            vm.rtcPeerConn = undefined;
        }

        vm.shouldAddICE = false;


        if (vm.stream && vm.stream.stop) {
            vm.stream.stop();
        } else {
            var arrAudioTracks = vm.stream.getAudioTracks();
            arrAudioTracks.forEach(function(streamTrack) {
                streamTrack.stop();
            });
            var arrVideoTracks = vm.stream.getVideoTracks();
            arrVideoTracks.forEach(function(streamTrack) {
                streamTrack.stop();
            });
        }

        var room_id = vm.currentCallRequest.to + "_" + vm.currentCallRequest.from;
        vm.socket.emit('endCall', {
            room_id: room_id
        });
    }

    function acceptAudioCall() {
        vm.isCallAccepted = true;
        var obj = vm.currentCallRequest;
        sendMsgFromSocket("acceptedAudioCall", obj);
        startSignaling(true, true);
        vm.isCallEnded = false;
        vm.isCallConnecting = false;
        vm.isCallConnected = true;
    }

    function acceptVideoCall() {
        vm.isCallAccepted = true;
        var obj = vm.currentCallRequest;
        sendMsgFromSocket("acceptedVideoCall", obj);
        startSignaling(false, true);
        vm.isCallEnded = false;
        vm.isCallConnecting = false;
        vm.isCallConnected = true;
    }

    function declineAudioCall() {
        vm.isCallAccepted = true;
        vm.isActiveAudioCall = false;
    }

    function declineVideoCall() {
        vm.isCallAccepted = true;
        vm.isActiveVideoCall = false;
    }


    function startSignaling(isAudio, shouldCreateOffer) {
        var myVideoArea = document.querySelector("#myVideoTag");
        var theirVideoArea = document.querySelector("#theirVideoTag");
        vm.rtcPeerConn = new RTCPeerConnection(configuration);
        var room_id = vm.currentCallRequest.to + "_" + vm.currentCallRequest.from;
        console.log(room_id);
        // send any ice candidates to the other peer
        vm.rtcPeerConn.onicecandidate = function(evt) {
            if (evt && evt.candidate)
                vm.socket.emit('signal', {
                    "type": "ice candidate",
                    "message": JSON.stringify({
                        'candidate': evt.candidate
                    }),
                    "room": room_id
                });
        };

        // let the 'negotiationneeded' event trigger offer generation
        vm.rtcPeerConn.onnegotiationneeded = function() {
            if (shouldCreateOffer) {
                console.log("onnegotiationneeded");
                vm.rtcPeerConn.createOffer(sendLocalDesc, function(err) {
                    console.error(err);
                });
            }
        }

        // once remote stream arrives, show it in the remote video element
        vm.rtcPeerConn.onaddstream = function(evt) {
            // if(!isAudio){
            theirVideoArea.src = URL.createObjectURL(evt.stream);
            // }
        };

        // get a local stream, show it in our video tag and add it to be sent
        navigator.getUserMedia({
            'audio': true,
            'video': !isAudio
        }, function(stream) {
            vm.stream = stream;
            console.log(stream);

            // if(!isAudio){
            myVideoArea.src = URL.createObjectURL(stream);
            // }
            vm.rtcPeerConn.addStream(stream);
        }, function(err) {
            console.error(err);
        });
    }

    function sendLocalDesc(desc) {
        var room_id = vm.currentCallRequest.to + "_" + vm.currentCallRequest.from;
        vm.rtcPeerConn.setLocalDescription(desc, function() {
            vm.shouldAddICE = true;
            vm.socket.emit('signal', {
                "type": "SDP",
                "message": JSON.stringify({
                    'sdp': vm.rtcPeerConn.localDescription
                }),
                "room": room_id
            });
        }, function(err) {
            console.error(err);
        });
    }

    function logError(error) {
        console.error(error);
    }
    var chatHistory = chatUtility.getInfo();
    if (chatHistory!=null) {
        console.log(chatHistory.userList);
        if (chatHistory.isOpenDialog==true) {
            vm.friendListOpened = true;
        }
    }

    function openChantWindow(data){
        $timeout(function() {
            if(chatHistory.userList){
                for (var i = 0; i < chatHistory.userList.length; i++) {
                    for (var j = 0; j < data.length; j++) {
                        if (chatHistory.userList[i]==data[j].user_id) {
                            console.log('yes');
                            vm.openAndFetchMsgs(data[j],true);
                            break;
                        };
                    };
                };
            }
        });
    }
    $('body').on('click','.chat-action-bar', function(){
        $('body').css('position','relative');
    });
    $('body').on('click','.fa-square-o', function(){
        $('body').css('position','fixed');
    });
    $('body').on('click','.title', function(){
        chatUtility.isOpenDialog(vm.friendListOpened);
    });
    $('body').on('click','.mainclose', function(){
        console.log('ajay');
        chatUtility.isOpenDialog(String(false));
    });
}
