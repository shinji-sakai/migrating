require('authToken');
angular
    .module("directive")
    .directive('header', HeaderDirective)
    .controller('HeaderCtrl', HeaderCtrl)

HeaderCtrl.$inject = ['authToken','$window'];    
function HeaderCtrl(authToken,$window) {

    var vm = this;
	vm.login = login;
    vm.logout = logout;

    init();

	function login(){
		vm.user = {
			name: "Sarju H.",
			mail : "sarju@filmsmiles.com"
		}
	}

    function logout(){
        authToken.removeToken();
        authToken.removeUser();
        $window.location = "/auth/#/login";
    }

    function init(){
        if(authToken.isAuthenticated()){
            login();
        }
    }
}

HeaderDirective.$inject = [];

function HeaderDirective() {
    return {
        scope: {},
        restrict: 'AE',
        replace: true,
        template: require('partials.header'),
        controller: HeaderCtrl,
        controllerAs : 'vm'
    }
}