require('directiveModule');
require('serviceModule');
require('authToken');
require('service.utility');
require('auth');
require('satellizer');

require('./_login.scss');
angular.module("directive")
    .component('loginComponent', {
        template: require('./login.html'),
        controller: LoginController,
        controllerAs: 'vm',
        bindings: {
            loginDelegate : '='
        }
    });

LoginController.$inject = ['authToken', 'utility','$timeout','$scope','$window','auth','$auth','$state'];

function LoginController(authToken, utility,$timeout,$scope,$window,auth,$auth,$state) {
    var vm = this;

    vm.$onInit = function(){
        vm.loginDelegate  = vm.loginDelegate || {};

        vm.loginDelegate.toggleLoginModal =vm.toggleLoginModal = toggleLoginModal;

        vm.checkAlreadyLogin = checkAlreadyLogin;
        vm.doLogin = doLogin;
        vm.authenticateWith = authenticateWith;    
    }
    
    goToDashboardApp = utility.goToDashboardApp;

    //toggle login modal
    function toggleLoginModal(d) {
    	if (!vm.isLoginModalOpen) {
            //It is not opened
            //Now it is going to open
            $("#login-modal").css("height", "100vh");
            $timeout(function() {
                vm.isLoginModalOpen = true;
            }, 300);
        } else {
            //if it is opened
            //then close it
            $("#login-modal").css("height", "0px");
            vm.isLoginModalOpen = false;
        }
    }

    //check if already login
    function checkAlreadyLogin() {
        // vm.toggleLoginModal();
        $state.reload();
    }


    //do login
    function doLogin() {
        if (!vm.userId || !vm.password) {
            vm.formError = "Please fill all required fields.";
            return;
        }
        var userloginData = {
            usr_id: vm.userId,
            pwd: vm.password
        };
        vm.isFormSubmitting = true;
        vm.formError = undefined;
        vm.userId = "";
        vm.password = "";
        auth.login(userloginData).then(function(resp) {
            var res = resp.data;
            if (res.err) {
                vm.formError = res.err;
                return;
            }
            checkAlreadyLogin();
        }).catch(function(err) {
            console.error(err);
            if(err.status === 500){
                vm.formError = err.data.err;
            }else{
                vm.formError = err;
            }
        })
        .finally(function() {
            vm.isFormSubmitting = false;
        })
    }

    //authenticate with facebook,twitter,linkedin,google etc...
    function authenticateWith(provider) {
        vm.isFormSubmitting = true;
        $auth.authenticate(provider)
            .then(function(response) {
                authToken.setToken(response.data.token);
                authToken.setUser(response.data.user);
                checkAlreadyLogin();
            })
            .catch(function(response) {
                console.error(response);
                vm.formError = response.error;
            })
            .finally(function() {
                vm.isFormSubmitting = false;
            })
    }
}