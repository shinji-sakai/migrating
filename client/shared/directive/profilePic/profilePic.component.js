require('directiveModule');
require('serviceModule');
require('authToken');
require('ng-file-upload');
require('service.profile');
require('angular-image-crop/image-crop.js');
require('angular-image-crop/image-crop-styles.css');

require('./_profilePic.scss');
angular.module("directive")
    .component('profilePicComponent', {
        bindings : {
            shouldEdit : '=',       //whether to show camera icon or not to upload new pic
            userPic : '='           // user display pic
        },
        template: require('./profilePic.html'),
        controller: ProfilePicController,
        controllerAs: 'vm'
    });

ProfilePicController.$inject = ['$log','$scope','$timeout','authToken','profile'];

function ProfilePicController($log,$scope,$timeout,authToken,profile) {
    var vm = this;

    vm.myProfileImage = "";
    vm.myProfileImageBlob = "";

    vm.getUserPic = authToken.getUserPic;
    vm.cropImage = cropImage;

    $scope.$watch('vm.profileImage', function(newValue, oldValue, scope) {
        if (newValue) {
            var reader = new FileReader();
            reader.onload = function(evt) {
                $log.debug(evt);
                $scope.$apply(function($scope) {
                    vm.showProfileImageModal = true;
                    vm.image = evt.target.result;
                });
            };
            reader.readAsDataURL(newValue);
        }
    });

    function cropImage() {
        vm.initCrop = true;
        $timeout(function() {
            vm.showProfileImageModal = false;
            

            vm.myProfileImageBlob.name = authToken.getUserId() + "_pic.jpg";
            vm.myProfileImageBlob.lastModifiedDate = new Date();

            // console.log(vm.myProfileImage);
            vm.userPic = vm.myProfileImage;

            //upload pic to server
            var obj = {usr_id : authToken.getUserId() , pic : vm.myProfileImageBlob};
            profile.saveProfilePic(obj)
                .then(function(res){
                    authToken.setUserPic(res.data.usr_pic);
                })
                .catch(function(err){
                    $log.error(err);
                });
        }, 100);
    }
}