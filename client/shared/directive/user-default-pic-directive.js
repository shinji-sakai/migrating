var app = angular.module("directive");

app.directive('userDefaultPic', function() {
    return {
        link: function(scope, element, attrs) {
            if (!attrs.src) {
                attrs.$set('src', "/assets/images/dummy-user-pic.png");
            }
            
            element.bind('error', function(e) {
                attrs.$set('src', "/assets/images/dummy-user-pic.png");
            });
        }
    }
});