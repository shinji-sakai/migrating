window.jwplayer = require('jwplayer');
window.jwplayer.key = "fA95pIQiK4bMKdS9Arqw70sKJ3GIvpO7elB7XQ==";
require('noUISlider_css');
window.noUiSlider = require('noUISlider_js');
require('service.infoDetection');
require('service.fullscreen');
require('service.videoplayer');
require('authToken');
require('service.utility');
require('service.admin');
require('service.videoURL');
require('service.videoviews');
var WebVTTParser = require("WebVTTParser");

angular
    .module('directive')
    .directive('videoControlsJw', VideoControls)
    .controller('VideoControlsCtrl', VideoControlsCtrl);

VideoControlsCtrl.$inject = ['$log', '$timeout', 'infoDetection', 'fullScreen', 'videoPlayer', '$scope', 'authToken', 'utility', 'admin', 'videoUrl', 'videoViews'];

function VideoControlsCtrl($log, $timeout, infoDetection, fullScreen, videoPlayer, $scope, authToken, utility, admin, videoUrl, videoViews) {
    var vm = this;

    if(window.innerWidth < 1024){
        vm.isMobileTabletDevice = true;
    }else{
        vm.isMobileTabletDevice = false;
    }

    //Variables
    vm.isPlaying = false;
    vm.isFullScreen = false;
    vm.isMute = false;
    vm.isSubtitleOn = false;
    vm.isContinuousPlay = false;

    vm.currentPlayBackRate = '1.0';
    vm.videoQuality = "HIGH";
    vm.offset_time = 8;
    vm.isDimLights = false;
    vm.video_current = "00:00";
    vm.video_duration = "00:00";
    vm.isLoading = true;
    vm.isPlayerReady = false;

    vm.shouldShowLoader = true;
    vm.isLoading = true;
    // vm.isPlaying = true;
    vm.videoPlayer = {};

    vm.playerVolume = authToken.playerVolume();
    vm.playerSpeed = authToken.playerSpeed();
    vm.isPlayerMute = authToken.playerMute();
    vm.playerVolume = authToken.playerVolume();
    vm.subtitleStatus = authToken.subtitleStatus() || false;
    vm.isAutoplay = authToken.playerAutoplay() || false;
    vm.isInTheaterMode = authToken.playerTheaterMode() || false;
    vm.videoQuality = authToken.playerQuality()

    //Funtions	
    //
    vm.isUserLoggedIn = authToken.isAuthenticated;
    vm.goToLogin = utility.goToLogin;


    vm.playToggle = playToggle;
    vm.rePlay = rePlay;
    vm.fullscreen = fullscreen;
    vm.toggleVolume = toggleVolume;
    vm.subtitleToggle = subtitleToggle;
    vm.changeSpeed = changeSpeed;
    vm.resetSpeed = resetSpeed;
    vm.changeVideoQuality = changeVideoQuality;
    vm.fastForward = fastForward;
    vm.rewind = rewind;
    vm.setFallBack = setFallBack;
    vm.changeVideoPlayer = changeVideoPlayer;

    vm.nextVideo = nextVideo;
    vm.prevVideo = prevVideo;

    vm.setPlayBackRate = setPlayBackRate;
    vm.getPlayBackRate = getPlayBackRate;

    vm.initVideoPlayer = initVideoPlayer;
    vm.toggleOverlay = toggleOverlay;
    vm.toggleContinuousPlay = toggleContinuousPlay;
    vm.toggleMute = toggleMute;
    vm.popoutVideo = popoutVideo;

    vm.inBrowserFullScreen = inBrowserFullScreen;
    vm.setPlayerMute = setPlayerMute;
    vm.setPlayerVolume = setPlayerVolume;
    vm.setSubtitle = setSubtitle;
    vm.setContinuousPlay = setContinuousPlay;
    vm.setTheaterMode = setTheaterMode;

    videoQualities();
    videoSpeeds();
    initVideoPlayer();
    // trackStatsOfPlayer();
    // 

    // set variables based on localstorage settings
    vm.subtitleStatus = authToken.subtitleStatus() || false;
    vm.isAutoplay = authToken.playerAutoplay() || false;
    vm.setSubtitle(vm.subtitleStatus);
    vm.setContinuousPlay(vm.isAutoplay);
    // vm.isInTheaterMode = authToken.playerTheaterMode() || false;
    // if(vm.isInTheaterMode){
    //     vm.theaterModeCallback();
    // }

    function initVideoPlayer() {

        if (vm.playerId && vm.playerUrl) {
            vm.videoId = vm.playerId;
        } else {
            vm.videoId = "video_jw";
        }
    }

    $scope.$on('takePrevVideoName', function(event, statsPrevVideoObj) {
        // vm.prevVideoName = prevVideoName;
        vm.statsPrevVideoObj = statsPrevVideoObj;
    });
    $scope.$on('takeNextVideoName', function(event, statsNextVideoObj) {
        vm.statsNextVideoObj = statsNextVideoObj;
    });

    function sendRequestForPrevVideoName() {
        $scope.$emit('sendPrevVideoName');
    }

    function sendRequestForNextVideoName() {
        $scope.$emit('sendNextVideoName');
    }

    //observe video playing object
    //if change detect , play new video
    $scope.$watch(function() {
        return vm.playingVideoObject;
    }, function(newValue, oldValue) {
        if (newValue != oldValue && newValue) {
            loadNewVideo();
        }
    });

    function loadNewVideo() {
        var videoId = vm.playingVideoObject.videoId;
        var videoTime = vm.playingVideoObject.videoTime;
        //fetch video object from server
        return findVideoEntity(videoId)
            .then(fetchVideoUrl) //fetch video url from server
            .then(function(url) {
                if (url) {
                    //got and url and subtitleurl
                    var videoUrl = url.videoUrl;
                    var subtitleUrl = url.subtitleUrl;
                    //load video with new video url
                    loadVideo(videoUrl, subtitleUrl);

                    if (videoTime && vm.videoPlayer.seek) {
                        // if there is videotime then seek video to videotime
                        $log.debug("Setting video time to..", videoTime);
                        vm.videoPlayer.seek(videoTime);
                    }
                } else {
                    vm.playerError = true;
                }
                return;
            })
            .catch(function(err) {
                $log.error(err);
            })
    }

    function findVideoEntity(videoId) {

        vm.findVideoTimer = $timeout(function() {
            vm.playerError = true;
        }, 1000 * 20);


        return admin.findVideoEntityById({
                videoId: videoId
            })
            .then(function(res) {

                $timeout.cancel(vm.findVideoTimer);

                var v = res.data[0] || {};
                //Change video name based on video quality
                var videoFileNameWithExt = v.videoUrl;
                if (!videoFileNameWithExt) return;
                var extPos = videoFileNameWithExt.lastIndexOf(".");

                //get only video file name
                var videoFileName = videoFileNameWithExt.substring(0, extPos);
                //get only extension
                var ext = videoFileNameWithExt.substring(extPos)
                    //final video file name
                var finalVideoFileName = videoFileNameWithExt;

                vm.videoQuality = authToken.playerQuality() || "HIGH";

                if (vm.videoQuality === "HIGH") {
                    finalVideoFileName = videoFileName + ext;
                } else if (vm.videoQuality === "MED") {
                    finalVideoFileName = videoFileName + "-480" + ext;
                } else if (vm.videoQuality === "LOW") {
                    finalVideoFileName = videoFileName + "-360" + ext;
                }
                v.videoUrl = finalVideoFileName;
                if(v.thumbnailPath){
                    vm.downloadVTTFile(v.thumbnailPath);    
                }
                return v;
            })
            .catch(function(err) {
                $log.error(err);
            });
    }

    function fetchVideoUrl(video) {
        return videoUrl.fetchVideoUrl(video)
            .then(function(video) {
                var url = video.url;
                var subtitleUrl = video.subtitleUrl;
                return {
                    videoUrl: url,
                    subtitleUrl: subtitleUrl
                };
            })
            .catch(function(err) {
                $log.error(err);
            });
    }


    var videos = {
        'mpeg-dash': {
            type: 'application/dash+xml',
            src: 'http://rdmedia.bbc.co.uk/dash/ondemand/bbb/2/client_manifest-common_init.mpd'
        },
        'hls': {
            type: 'application/x-mpegURL',
            src: 'http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8'
        },
        'flash': {
            type: 'rtmp/mp4',
            src: 'rtmp://192.168.1.2:1935/vod/mp4:sanam.mp4'
        },
        'html5': {
            type: 'video/mp4',
            src: 'assets/video/1.mp4'
        }
    }

    function fallbackPossibilities() {

        return {
            'windows+modern': [
                //videos['mpeg-dash'], //Mpeg-dash
                // videos['hls'], //HLS
                videos['html5'], //HTML5
                //videos['flash'] //Flash
            ],
            'windows+old': [
                videos['flash'], //Flash
                videos['html5'] //HTML5
            ],
            'windows+ie+modern': [
                // videos['mpeg-dash'], //Mpeg-dash
                // videos['flash'], //flash
                videos['html5'] //html5
            ],
            'windows+ie+old': [
                // videos['flash'], //flash
                videos['html5'] //html5
            ],
            'mac+modern': [
                // videos['hls'], //hls
                // videos['mpeg-dash'], //mpeg-dash
                videos['html5'], //html5
                // videos['flash'] //flash
            ],
            'mac+old': [
                videos['html5'], //html5
                videos['flash'] //flash    
            ]
        }
    }

    function setFallBack() {
        var comp = infoDetection.getCompatibility();

        var poss = fallbackPossibilities();
        //vm.videoPlayer.load([{
        //    file: poss[comp][0].src,
        //    image: "assets/video/1.jpg",
        //    tracks: [{
        //        file: "assets/video/1.vtt"
        //    }]
        //}]);

    }

    $scope.$on('loadVideo', function(event, video, subtitleUrl) {
        loadVideo(video, subtitleUrl);
    });
    $scope.$on('loadVideoFromNote', function(event, video, time, subtitleUrl) {
        loadVideo(video, subtitleUrl);
        vm.videoPlayer.seek(time);
    });
    $scope.$on('setupVideo', function(event, video) {
        if (!vm.isPlayerReady && vm.videoPlayer) {
            vm.videoPlayer.setup({
                playlist: [{
                    sources: [{
                        file: video
                    }]
                }],
                width: '100%',
                height: '100%',
                // primary: 'html5',
                controls: false,
                // repeat : false,
                // autostart : false,
            });
            vm.setupVideoPlayer();
        }

        // vm.videoPlayer.play(true);
        // vm.isPlaying = true;
    });
    $scope.$on('pauseVideo', function() {
        if (vm.isPlaying) {
            vm.videoPlayer.play(false);
            vm.isPlaying = false;
        }
    });
    $scope.$on('replayVideo', function() {
        if (vm.videoPlayer.play) {
            vm.videoPlayer.play();
        }
    });

    //If Multiple videos on one page
    //Then this event will be fired
    //Pause all with except the video being played
    $scope.$on('pauseAllVideoExceptWithId', function(event, playerId) {
        if (vm.playerId !== playerId) {
            vm.videoPlayer.play(false);
            vm.isPlaying = false;
        }
    });
    $scope.$on('playVideo', function() {
        if (!vm.isPlaying) {
            vm.videoPlayer.play(true);
            vm.isPlaying = true;
        }
    });

    $scope.$on('videoName', function(eve, videoName) {
        vm.videoTitle = videoName;
    })
    
    vm.loadVideo = loadVideo;
    function loadVideo(video, subtitleUrl) {
        if (vm.isRestricted) {
            return;
        }
        if (vm.videoPlayer && vm.videoPlayer.remove) {
            // console.log("removing...");
            vm.videoPlayer.remove();
        }
        vm.videoPlayer = {};
        vm.isPlayerReady = false;

        $log.debug("Loading Video.......", video);
        $log.debug("Loading Subtitle.......", subtitleUrl);

        sendRequestForNextVideoName();
        sendRequestForPrevVideoName();

        vm.shouldShowLoader = true;
        vm.isLoading = true;
        vm.playerError = false;
        vm.isVideoComplete = false;
        vm.isVideoStarted = false;
        vm.isPlaying = false;
        if (!vm.videoPlayer.setup) {
            vm.videoPlayer = jwplayer(vm.videoId);
        }

        $timeout(function() {
            vm.isPlayerMute = authToken.playerMute();
            vm.playerVolume = authToken.playerVolume();
            vm.subtitleStatus = authToken.subtitleStatus() || false;
            vm.isAutoplay = authToken.playerAutoplay() || false;

            vm.setPlayerVolume(vm.playerVolume);
            vm.setSubtitle(vm.subtitleStatus);
            vm.setContinuousPlay(vm.isAutoplay);

            vm.isInTheaterMode = authToken.playerTheaterMode() || false;
            if (vm.isInTheaterMode) {
                vm.theaterModeCallback({
                    isVideoInTheaterMode: vm.isInTheaterMode
                });
            }
        })
        if (!vm.isPlayerReady) {
            vm.videoPlayer.setup({
                playlist: [{
                    sources: [{
                        file: video
                    }],
                    tracks: [{
                        file: subtitleUrl,
                        label: "English",
                        kind: "captions",
                        default: true
                    }]
                }],
                width: '100%',
                height: '100%',
                primary: 'html5',
                controls: false,
                mute: true,
                autostart: (vm.autoStart == 'true')
            });

            vm.setupVideoPlayer();

            return;
        }

        vm.video_current = "00:00";
        vm.video_duration = "00:00";

        vm.currentVideoUrl = video;
        vm.videoPlayer.load([{
            file: video
        }]);
        $timeout(function() {
            vm.playerSpeed = authToken.playerSpeed();
            // console.log(vm.playerSpeed);
            vm.changeSpeed(vm.playerSpeed || 1);
        });

        vm.videoPlayer.play(true);

    }

    function playToggle() {
        // console.log("playToggle...",vm.videoId,$("#"+vm.videoId),$("#"+vm.videoId).find("video"));
        // var videoElement = $("#"+vm.videoId).find("video");
        // if(videoElement.length <= 0){
        //     loadNewVideo();
        // }
        if (vm.isPlaying) {
            vm.videoPlayer.play(false);
            vm.isPlaying = false;
            vm.showAnimatedPauseBtn = true;
            vm.showAnimatedPlayBtn = false;
        } else {
            vm.videoPlayer.play(true);
            vm.isVideoStarted = true;
            vm.isPlaying = true;
            vm.showAnimatedPlayBtn = true;
            vm.showAnimatedPauseBtn = false;
            //If Multiple videos on one page 
            //Except The one which is being played
            $scope.$emit('notificationToPauseAllVideo', vm.playerId);
        }
    }

    function rePlay() {
        setFallBack();
        vm.videoPlayer.play(true);
    }



    $timeout(function() {
        $($("#" + vm.videoId).parent()[0]).on('fullscreenchange', function(e, fullScreenEnabled) {
            console.log("in controller fullscreen", vm.isFullScreen)
            if (fullScreenEnabled) {
                e.stopPropagation();
                vm.isFullScreen = true;
            } else {
                console.log("doing exitFullScreen controlbar")
                vm.isFullScreen = false;
            }
            $scope.$apply();
        });
    })

    function fullscreen() {
        if (vm.isFullScreen) {
            fullScreen.exitFullScreen();
        } else {
            if (vm.isInTheaterMode) {
                vm.isInTheaterMode = false;
                authToken.playerTheaterMode(false);
                vm.theaterModeCallback({
                    isVideoInTheaterMode: vm.isInTheaterMode
                });
            }
            fullScreen.launchFullScreen($("#" + vm.videoId).parent()[0]);
        }
        vm.isFullScreen = !vm.isFullScreen;
    }


    function toggleVolume() {
        if (vm.isMute) {
            vm.videoPlayer.volume(1);
            vm.volumeSlider.noUiSlider.set(100);
        } else {
            vm.videoPlayer.volume(0);
            vm.volumeSlider.noUiSlider.set(0);
        }
        vm.isMute = !vm.isMute;
    }


    function subtitleToggle(str) { //Str = ON/OFF/null
        // if (!str) {
        //     vm.isSubtitleOn = !vm.isSubtitleOn;
        // } else if (str && str.toLowerCase() === "on") {
        //     vm.isSubtitleOn = true;
        // } else if (str && str.toLowerCase() === "off") {
        //     vm.isSubtitleOn = false;
        // }

        vm.isSubtitleOn = !vm.isSubtitleOn;

        if (vm.isSubtitleOn) {
            setSubtitle(vm.isSubtitleOn);
            // var wrap = $('#jw_video_wrapper')[0];
            // $(wrap).css('height', 'calc(100% - 85px - 40px'); //58px - Control Bar , 40px - Subtitle    
        } else {
            setSubtitle(false);
            // var wrap = $('#jw_video_wrapper')[0];
            // $(wrap).css('height', 'calc(100% - 85px'); //58px - Control Bar
        }

    }

    function setSubtitle(isSubtitleOn) { //Str = ON/OFF/null
        if (isSubtitleOn) {
            var wrap = $('#jw_video_wrapper')[0];
            $(wrap).css('height', 'calc(100% - 85px - 40px'); //58px - Control Bar , 40px - Subtitle    
            vm.isSubtitleOn = authToken.subtitleStatus(true);
        } else {
            var wrap = $('#jw_video_wrapper')[0];
            $(wrap).css('height', 'calc(100% - 85px'); //58px - Control Bar
            vm.isSubtitleOn = authToken.subtitleStatus(false);
        }
    }


    function changeSpeed(val) {
        vm.currentPlayBackRate = val.toFixed(1);
        vm.setPlayBackRate(val);
        authToken.playerSpeed(val);
    }

    function setPlayBackRate(rate) {
        var video = document.getElementsByTagName('video')[0];
        if (video) {
            video.playbackRate = rate;
        }
    }

    function getPlayBackRate() {
        var video = document.getElementsByTagName('video')[0];
        if (video) {
            return video.playbackRate;
        }
        return 1;
    }

    function resetSpeed() {
        vm.changeSpeed(1);
    }

    function rewind() {
        var currentTime = vm.videoPlayer.getPosition();
        var nextTime = currentTime - vm.offset_time;
        vm.videoPlayer.seek(nextTime);
        vm.isPlaying = true;
    }

    function fastForward() {
        var currentTime = vm.videoPlayer.getPosition();
        if (currentTime < vm.videoPlayer.getDuration()) {
            var nextTime = currentTime + vm.offset_time;
            vm.videoPlayer.seek(nextTime);
            vm.isPlaying = true;
        }
    }

    function prevVideo(isUserClicked) {
        if (isUserClicked) {
            $scope.$emit('prevVideo', true);
        } else {
            $scope.$emit('prevVideo', vm.isContinuousPlay);
        }

    }

    function nextVideo(isUserClicked) {
        if (isUserClicked) {
            $scope.$emit('nextVideo', true);
        } else {
            $scope.$emit('nextVideo', vm.isContinuousPlay);
        }
    }

    function changeVideoQuality(quality) { //quality = HIGH / MED / LOW
        vm.videoQuality = quality;
        //set settings to local storage
        authToken.playerQuality(quality);
        //capture current video time
        vm.playingVideoObject.videoTime = vm.videoPlayer.getPosition();
        //load new video based on quality
        loadNewVideo();

    }

    function changeVideoPlayer() {
        var currentTime = vm.videoPlayer.getPosition();
        $scope.$emit('changeVideoPlayer', vm.currentVideoUrl, currentTime);
    }

    function videoQualities() {
        vm.videoQualities = {
            '720p': 'assets/video/1-720.mp4',
            '360p': 'assets/video/1-360.mp4',
            '244p': 'assets/video/1-244.mp4',
        }
    }

    function videoSpeeds() {

        vm.videoSpeeds = {
            '0.5x': '0.5',
            '1x': '1',
            '1.5x': '1.5',
            '2x': '2',
            '4x': '4',
            '16x': '16',
        }
    }

    function toggleOverlay($event) {
        var ele = $($event.currentTarget).parent();
        $event.stopPropagation();
        if ($(ele).hasClass('open')) {
            $('.ds-control').removeClass('open');
            $('.ds-control').find('.overlay').hide();
        } else {
            $('.ds-control').removeClass('open');
            $('.ds-control').find('.overlay').hide();
            $(ele).addClass('open');
            $(ele).find('.overlay').show();
        }
    }

    function toggleContinuousPlay(isAutoplayOn) {
        // vm.isContinuousPlay = !vm.isContinuousPlay;
        vm.isContinuousPlay = isAutoplayOn;
        setContinuousPlay(vm.isContinuousPlay);
    }

    function setContinuousPlay(isContinuousPlay) {
        authToken.playerAutoplay(isContinuousPlay);
        vm.isContinuousPlay = isContinuousPlay;
    }

    function toggleMute() {
        var parent = $('.ds-sound-progress-wrapper.' + vm.videoId);
        //set width of sound progress bar
        var real_progress_bar = $(parent).find('.ds-sound-real-progress');
        // console.log(vm.isPlayerMute);
        if (vm.isPlayerMute) {
            var volume = vm.prevVolume ? vm.prevVolume : 100;
            // vm.videoPlayer.setVolume(volume);
            // $(real_progress_bar).width(volume + "%");
            vm.setPlayerMute(false);
            vm.setPlayerVolume(volume);
            vm.isPlayerMute = false;
        } else {
            vm.prevVolume = vm.videoPlayer.getVolume();
            // vm.videoPlayer.setVolume(0);
            // $(real_progress_bar).width("0%");
            vm.setPlayerMute(true);
            vm.setPlayerVolume(0);
            vm.isPlayerMute = true;
        }
    }

    function popoutVideo() {
        //capture current video time
        var videoTime = vm.videoPlayer.getPosition();
        $scope.$emit('popoutVideo', videoTime);
    }

    function inBrowserFullScreen() {
        vm.isInBrowserFullScreen = !vm.isInBrowserFullScreen;
    }

    // $(".main-video-player").on('click',function(e){

    // }

    function trackStatsOfPlayer() {
        $(".main-video-player").on('click', function(e) {
            var target = e.target;
            // e.stopPropagation();
            var title = $(target).data("click-title");
            var value = $(target).attr("data-click-value");
            if (vm.videoPlayer) {
                var obj = {
                    usr_id: authToken.getUserId() || 'n/a',
                    // subtitle : ''+vm.isSubtitleOn,
                    spd_ctrl: '' + vm.currentPlayBackRate,
                    vdo_qlty: '' + vm.videoQuality,
                    vdo_full_scrn: '' + vm.isFullScreen,
                    // theaterMode : (vm.isInTheaterMode) ? 'true' : 'false',
                    vdo_vw_perc: '' + ((vm.videoPlayer.getPosition() / vm.videoPlayer.getDuration()) * 100),
                    vdo_pop_out: (vm.isPopoutVideo !== undefined) ? 'true' : 'false',
                    vdo_id: vm.playingVideoObject.videoId,
                    vdo_vw_dt: new Date()
                }

                videoViews.submitVideoStats({
                    topic: "videostats",
                    stats: JSON.stringify(obj)
                });
            }

        })
        window.addEventListener("beforeunload", function(e) {
            // console.log("test....");
            //before browser close or tab close 
            //send event to server
            if (vm.videoPlayer && vm.videoPlayer.getPosition) {
                var obj = {
                    usr_id: authToken.getUserId() || 'n/a',
                    // subtitle : vm.isSubtitleOn,
                    spd_ctrl: vm.currentPlayBackRate,
                    vdo_qlty: vm.videoQuality,
                    vdo_full_scrn: vm.isFullScreen,
                    // theaterMode : (vm.isInTheaterMode) ? 'true':'false',
                    vdo_vw_perc: '' + ((vm.videoPlayer.getPosition() / vm.videoPlayer.getDuration()) * 100),
                    vdo_pop_out: (vm.isPopoutVideo !== undefined) ? 'true' : 'false',
                    vdo_id: vm.playingVideoObject.videoId,
                    vdo_vw_dt: new Date()
                }

                videoViews.submitVideoStats({
                    topic: "videostats",
                    stats: JSON.stringify(obj)
                });
            }
        });
    }

    function setPlayerVolume(volume) {
        var ele = $('.ds-sound-progress-wrapper.' + vm.videoId);
        var sound_progress = $(ele).find('.ds-sound-real-progress');
        $(sound_progress).width(volume + "%");
        vm.playerVolume = authToken.playerVolume(volume);
        vm.videoPlayer.setVolume(volume);
        if (volume === 0) {
            setPlayerMute(true);
        } else {
            setPlayerMute(false);
        }
    }

    function setPlayerMute(isMute) {
        if (isMute) {
            var ele = $('.ds-sound-progress-wrapper.' + vm.videoId);
            var sound_progress = $(ele).find('.ds-sound-real-progress');
            $(sound_progress).width("0%");


            vm.playerVolume = authToken.playerVolume(0);
            vm.videoPlayer.setVolume(0);
        }
        vm.isPlayerMute = authToken.playerMute(isMute);
        vm.videoPlayer.setMute(vm.isPlayerMute)
    }

    function setTheaterMode(isInTheaterMode) {
        vm.isInTheaterMode = isInTheaterMode;
        authToken.playerTheaterMode(isInTheaterMode);
        vm.theaterModeCallback({
            isVideoInTheaterMode: isInTheaterMode
        });
        if (vm.isFullScreen) {
            fullScreen.exitFullScreen();
            vm.isFullScreen = false;
        }
    }

    vm.downloadVTTFile = function(url){
        $.ajax({
            url : url,
            method : 'GET',
            success : function(data){
                var parser = new WebVTTParser();
                vm.parsedVTT = parser.parse(data);
                var pos = url.lastIndexOf("/");
                vm.thumnnailBasePath = url.substring(0,pos);
            },
            error : function(err){
                $log.error(err);
            }
        })
    }
}

VideoControls.$inject = ['$log', '$interval', '$timeout', 'videoPlayer', 'authToken'];

function VideoControls($log, $interval, $timeout, videoPlayer, authToken) {
    return {
        restrict: 'AE',
        // replace : true,
        scope: {},
        bindToController: {
            playerId: "@",
            playerUrl: "@",
            autoStart: "@",
            isVideoPage: '@',
            isPopoutVideo: '@',
            isRestricted: '=?',
            courseId: '@?',
            theaterModeCallback: '&?',
            playingVideoObject: '=?',
            componentDelegate: '=?',
            onMetadata: "&?",
            onVideoSeek: "&?",
            onVideoLoadError: "&?",
            onVideoTimeUpdate: "&?",
            isOtherThingInFullscreen :  '=?'
        },
        template: function(ele, attrs) {
            if (!attrs.player) {
                return require('partials.videoControlBar_JW');
            } else if (attrs.player === 'minimal') {
                return require('partials.minimalControlBar');
            }
        },
        controller: VideoControlsCtrl,
        controllerAs: 'vm',
        link: function postLink(scope, element, attrs, vm) {
            vm.componentDelegate = vm;

            $timeout(function() {


                vm.videoPlayer = jwplayer(vm.videoId);
                if (vm.playerId && vm.playerUrl && vm.videoPlayer.setup) {
                    if (!vm.isPlayerReady) {
                        vm.videoPlayer.setup({
                            playlist: [{
                                sources: [{
                                    file: vm.playerUrl
                                }]
                            }],
                            width: '100%',
                            height: '100%',
                            primary: 'html5',
                            controls: false,
                            autostart: (vm.autoStart == 'true')
                        });
                        vm.isDirectURL = true;
                        vm.setupVideoPlayer();
                    }
                }
            }, 100);
            vm.setupVideoPlayer = setupVideoPlayer;
            vm.isFlash = false;
            vm.isPlaying = false;


            var controlbar = $('.ds-controlbar');
            var video_tag = $('#video_jw');

            var speedSlider = $('.speed-slider')[0];
            vm.speedSlider = speedSlider;
            var max_speed = 2;
            var min_speed = 0.5;
            var captionObserver = null;

            // 

            function setupVideoPlayer() {
                registerEvents();
                bindKeyEvents();
                setupProgressBarMouseMove();
            }

            function registerEvents() {

                //On Time Update
                vm.videoPlayer.on('time', function() {
                    if (vm.onVideoTimeUpdate) {
                        var time = vm.videoPlayer.getPosition();
                        vm.onVideoTimeUpdate({
                            time: changeTimeFormat(time),
                            percent : (time / vm.videoPlayer.getDuration()) * 100
                        })
                    }
                    vm.playerError = false;
                    vm.isVideoComplete = false;
                    vm.isVideoStarted = true;
                    vm.isPlaying = true;
                    vm.isLoading = false;
                    //Fetch current time
                    var time = vm.videoPlayer.getPosition();
                    scope.$apply(function() {
                        vm.video_current = changeTimeFormat(time);
                        vm.currentVideoTimeInSec = time;
                    })

                    //Progress bar width
                    var progressbar_width = ((vm.videoPlayer.getPosition() / vm.videoPlayer.getDuration()) * 100) + '%';
                    //Set progress bar width
                    $('.ds-progress-bar.' + vm.videoId).css('width', progressbar_width);
                    scope.$emit("videoTimeUpdate");
                });

                vm.videoPlayer.on("playlistComplete", function() {
                    stopObservingCaptions();
                    scope.$apply(function() {
                        vm.isPlaying = false;
                        vm.videoPlayer.play(false);
                    });
                });

                vm.videoPlayer.on("complete", function() {
                    $log.debug("video complete");
                    vm.isVideoComplete = true;
                    vm.isVideoStarted = false;
                    vm.nextVideo();
                });

                // vm.videoPlayer.on('all', function(e) {
                //     if (e !== 'bufferChange' && e !== 'time') {
                //         $log.debug("all", e);
                //     }
                // });

                vm.videoPlayer.on('error', function(e) {
                    vm.playerError = true;
                    vm.playerErrorMsg = e.message;
                    if (vm.onVideoLoadError) {
                        e.videoQuality = vm.videoQuality;
                        vm.onVideoLoadError({
                            err: e
                        });
                    }

                    $log.error(e);
                    scope.$apply();
                });

                vm.videoPlayer.on('setupError', function(e) {
                    vm.playerError = true;
                    vm.playerErrorMsg = e.message;
                    if (vm.onVideoLoadError) {
                        e.videoQuality = vm.videoQuality;
                        vm.onVideoLoadError({
                            err: e
                        });
                    }
                    $log.error(e);
                    scope.$apply();
                });





                vm.videoPlayer.on('firstFrame', function(e) {
                    $timeout(function() {
                        vm.playerSpeed = authToken.playerSpeed();
                        vm.changeSpeed(vm.playerSpeed || 1);
                    });
                    $('#' + vm.videoId).height('100%');
                })

                vm.videoPlayer.on('playlist', function(e) {
                    // vm.isLoading = true;
                })

                vm.videoPlayer.on('seek', function(e) {
                    // vm.isLoading = true;
                })
                vm.videoPlayer.on('seeked', function(e) {
                    vm.isLoading = false;
                });
                vm.videoPlayer.on('providerFirstFrame', function(e) {
                    vm.isLoading = false;
                });


                vm.videoPlayer.on('play', function() {
                    vm.isLoading = false;
                    startObservingCaptions();
                    $('.jw-captions').css('display', 'none');
                })
                vm.videoPlayer.on('pause', function() {
                    vm.shouldShowLoader = false;
                    stopObservingCaptions();
                })



                vm.videoPlayer.on('ready', function() {
                    vm.isPlayerReady = true;
                    vm.shouldShowLoader = true;
                    vm.isLoading = false;

                    videoPlayer.setVideoPlayer(vm.videoPlayer);
                    //Duration of video
                    //Set Current time of video
                    vm.video_current = "00:00";
                    if (vm.autoStart === 'false') {
                        // vm.isPlaying = false;
                    } else {
                        vm.videoPlayer.play(true);
                        vm.isVideoStarted = true;
                        vm.isPlaying = true;
                    }
                });
                //Only Once when all meta data loaded
                vm.videoPlayer.on('meta', function(e) {
                    if (vm.onMetadata) {
                        vm.onMetadata({
                            metadata: e
                        });
                    }

                    vm.isLoading = false;

                    if (vm.autoStart === 'false') {
                        // vm.isPlaying = false;
                    } else {
                        vm.videoPlayer.play(true);
                        vm.isVideoStarted = true;
                        vm.isPlaying = true;
                    }

                    if (e.duration) {
                        var time = e.duration;
                        scope.$apply(function() {
                            vm.video_duration = changeTimeFormat(time);
                        })
                    }
                    if (e.metadata.duration) {
                        var time = e.metadata.duration;
                        scope.$apply(function() {
                            vm.video_duration = changeTimeFormat(time);
                        })
                    }
                });
                vm.videoPlayer.on('playlistItem', function(e) {
                    //Check Whether video is HTML5 or Flash
                    var index = e.index;
                    var type = e.item.sources[index].type;
                    var isFlash = false;
                    if (type === "rtmp") {
                        isFlash = true;
                    }
                    vm.isFlash = isFlash;
                });



                vm.videoPlayer.on('captionsList', function(e) {
                    vm.videoPlayer.setCurrentCaptions(1);
                })

                vm.videoPlayer.on('bufferChange', function() {
                    var bufferbar_width = (vm.videoPlayer.getBuffer()) + '%';
                    $('.ds-buffered-bar.' + vm.videoId).css('width', bufferbar_width);
                })

                //Volume Change event
                vm.videoPlayer.on('volume', function() {
                    //Set Volume Control Height
                    if (vm.videoPlayer.getVolume() == 0) {
                        vm.isMute = true;
                    } else {
                        vm.isMute = false;
                    }
                });


                //Click Listener For Progress Bar
                $('.ds-progress-bar-wrapper.' + vm.videoId).on('click', function(e) {
                    //Percent where user clicked on progressbar
                    var percent = (e.offsetX / $(this).width()) * 100;
                    //Get desire time from where user clicked
                    var desireTime = vm.videoPlayer.getDuration() * percent / 100;

                    vm.cureentVideoProgressBarStatsTime = vm.video_current;
                    vm.seekVideoProgressBarStatsTime = changeTimeFormat(desireTime);

                    if (vm.onVideoSeek) {
                        vm.onVideoSeek({
                            currentTime: vm.video_current,
                            seekTime: vm.seekVideoProgressBarStatsTime
                        })
                    }


                    //Set current time to desire time
                    vm.videoPlayer.seek(desireTime);

                    vm.isPlaying = true;
                });

                //Click Listener For Volume bar
                $('.ds-sound-progress-wrapper.' + vm.videoId).on('click', function(e) {
                    //Percent where user clicked on progressbar
                    var percent = Math.floor((e.offsetX / $(this).width()) * 100);

                    //set volume
                    // vm.videoPlayer.setVolume(percent);

                    // //set width of sound progress bar
                    // var real_progress_bar = $(this).find('.ds-sound-real-progress');
                    // $(real_progress_bar).width(percent + "%");

                    vm.setPlayerVolume(percent);

                });

                $(document).on('click', function(e) {
                    $('.ds-control').removeClass('open');
                    $('.ds-control').find('.overlay').hide();
                });
            }

            function startObservingCaptions() {
                captionObserver = $interval(function() {
                    var window_active = $('.jw-captions-window-active');
                    if (window_active.length > 0) {
                        var cap = $('.jw-captions-text').html();
                        cap = cap.split('<br>').join(' ');
                    } else {
                        var cap = "";
                    }
                    vm.subtitle = cap;
                }, 100);
            }

            function stopObservingCaptions() {
                $interval.cancel(captionObserver);
            }



            function bindKeyEvents() {

                $('video-controls-jw').bind('keypress', function(e) {
                    // 99/67 - C/c
                    var codes = {
                        CAPITAL_C: 99,
                        SMALL_C: 67,

                        CAPITAL_F: 70,
                        SMALL_F: 102,

                        SMALL_P: 112,
                        CAPITAL_P: 80
                    }
                    scope.$apply(function() {
                        if (e.keyCode == codes.CAPITAL_C || e.keyCode == codes.SMALL_C) {
                            vm.subtitleToggle();
                        } else if (e.keyCode == codes.CAPITAL_F || e.keyCode == codes.SMALL_F) {
                            vm.fullscreen();
                        } else if ((e.keyCode == codes.SMALL_P) || (e.keyCode == codes.CAPITAL_P)) {
                            vm.playToggle();
                        }
                    });

                })
                $('video-controls-jw').bind('keydown', function(e) {
                    e.stopPropagation();
                    //32 - Space
                    //37 - Left Key
                    //38 - Up Key
                    //39 - Right Key
                    //40 - Down Key
                    var offset_time = 8; //8 seconds Forward and Backward
                    var codes = {
                        SPACE: 32,
                        LEFT: 37,
                        UP: 38,
                        RIGHT: 39,
                        DOWN: 40,
                        GREATER: 190,
                        LESSTHAN: 188
                    }


                    if (e.keyCode == codes.SPACE) {
                        vm.playToggle();
                    } else if (!e.ctrlKey && e.keyCode == codes.UP) {

                        //Volume UP

                        var currentVolume = vm.videoPlayer.getVolume();
                        if (currentVolume < 100) {
                            var nextVolume = currentVolume + 10;
                            vm.volumeSlider.noUiSlider.set(nextVolume);
                            vm.videoPlayer.setVolume(nextVolume);
                        }
                    } else if (!e.ctrlKey && e.keyCode == codes.DOWN) {

                        //Volume Down

                        var currentVolume = vm.videoPlayer.getVolume();
                        if (currentVolume > 0) {
                            var nextVolume = currentVolume - 10;
                            vm.volumeSlider.noUiSlider.set(nextVolume);
                            vm.videoPlayer.setVolume(nextVolume);
                        }
                    } else if (!e.ctrlKey && e.keyCode == codes.RIGHT) {

                        //FAST FORWARD offset_time
                        vm.fastForward();

                    } else if (!e.ctrlKey && e.keyCode == codes.LEFT) {

                        //Backward offest_time
                        vm.rewind();

                    } else if (e.ctrlKey && e.keyCode == codes.GREATER) {

                        //Speed UP

                        var currentSpeed = vm.getPlayBackRate();
                        if (currentSpeed < max_speed) {
                            var nextSpeed = currentSpeed + 0.1;
                            vm.changeSpeed(nextSpeed);
                        }
                    } else if (e.ctrlKey && e.keyCode == codes.LESSTHAN) {

                        //Speed Down

                        var currentSpeed = vm.getPlayBackRate();
                        if (currentSpeed > min_speed) {
                            var nextSpeed = currentSpeed - 0.1;
                            vm.changeSpeed(nextSpeed);
                        }
                    }


                })
            }

            function findImageForTime(time){
                if(!vm.parsedVTT){
                    return;
                }
                var obj = (vm.parsedVTT.cues.filter(function(v,i){
                    if(v.startTime >= time && time <= v.endTime){
                        return true;
                    }
                    return false;
                }) || [])[0];
                if(obj){
                    var pos = obj.text.indexOf("#");
                    var img = obj.text.substring(0,pos);
                    var key_val = obj.text.substring(pos+1).split("=")[1].split(",");
                    var x = key_val[0]
                    var y = key_val[1]
                    return{
                        img : vm.thumnnailBasePath + "" + img,
                        x : -x,
                        y : -y
                    }
                }
                
            }

            function setupProgressBarMouseMove() {
                setTimeout(function() {
                    $(".ds-progress-bar-wrapper." + vm.videoId).on('mousemove', function(e) {
                        //get percentage , where is the current mouse position
                        var percent = (e.offsetX / $(this).width()) * 100;
                        //get video time from percentage value
                        var time = vm.videoPlayer.getDuration() * percent / 100;

                        vm.mouseHoverTime = changeTimeFormat(time);

                        //mouse hover element
                        var ele = $(this).find(".mouse-hover-time");
                        //position element
                        $(ele).css('left', e.offsetX - 100);
                        //display mouse hover element
                        $(ele).css('display', 'block');
                        var i = findImageForTime(time || 0)
                        if(i){
                            $(ele).css("font-size","20px").css("background-position",i.x + "px " + i.y + "px").css("background-image","url('" + i.img + "')");    
                        }
                        scope.$apply();
                    });
                    $(".ds-progress-bar-wrapper." + vm.videoId).on('mouseout', function(e) {
                        vm.mouseHoverTime = "";

                        //mouse hover element
                        var ele = $(this).find(".mouse-hover-time");
                        //position element
                        $(ele).css('left', "0px");
                        //display mouse hover element
                        $(ele).css('display', 'none');
                        scope.$apply();
                    });
                })

            }

            vm.changeTimeFormat = changeTimeFormat;

            function changeTimeFormat(sec) {
                var time = sec || 0;
                var hours = parseInt(time / 3600) % 24;
                var minutes = parseInt(time / 60) % 60;
                var seconds = parseInt(time % 60);

                hours = (hours < 10 ? "0" + hours : hours)
                minutes = (minutes < 10 ? "0" + minutes : minutes)
                seconds = (seconds < 10 ? "0" + seconds : seconds)
                var ret_time;
                if (hours == "00") {
                    ret_time = minutes + ":" + seconds;
                } else {
                    ret_time = hours + ":" + minutes + ":" + seconds;
                }

                return ret_time;
            }

        }
    }

}