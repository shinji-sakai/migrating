var app = angular.module("directive");

app.directive('errImage', function() {
    return {
        link: function(scope, element, attrs) {
            // console.log(".............", attrs.errImage, attrs.src);
            if (!attrs.src) {
                attrs.$set('src', attrs.errImage);
            }
            
            element.bind('error', function(e) {
                if (attrs.src != attrs.errImage) {
                    attrs.errImage = attrs.errImage || "/assets/images/dummy-user-pic.png"
                    attrs.$set('src', attrs.errImage);
                }
            });
        }
    }
});