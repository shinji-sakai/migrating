angular
    .module("directive")
    .directive('footer', FooterDirective)
    .controller('FooterCtrl', FooterCtrl)


function FooterCtrl() {}

FooterDirective.$inject = [];

function FooterDirective() {
    return {
        restrict: 'AE',
        replace: true,
        template: require('partials.footer'),
        controller: FooterCtrl,
        controllerAs : 'footer'
    }
}