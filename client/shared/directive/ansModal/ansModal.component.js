require('directiveModule');
require('serviceModule');
require('./_ansModal.scss');
require('service.forums');
require('authToken');
require('angular-froala');


angular.module('directive')
    .component('ansModal', {
        template: require('./ansModal.html'),
        controller: AnsModalController,
        controllerAs: 'vm',
        bindings: {
            title: '=?',
            desc: '=?',
            id: '=',
            queId: '=',
            onAnsSubmit: '&', // delegate function to call when answer is sunmitted successfully
            ansModalDelegate: '=', // delegate object
            answerText: "=?",
            answerId: "=?",
            answerTimestamp: "=?"
        }
    })

AnsModalController.$inject = ['$scope', 'authToken', '$sce', 'forums'];

function AnsModalController($scope, authToken, $sce, forums) {
    var vm = this;

    var EditorConfig = {
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'align', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', 'clearFormatting', 'html'],
        quickInsertButtons: [],
        pluginsEnabled: ["align", "codeBeautifier", "codeView", "colors", "draggable", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "video", "wordPaste"],
        heightMin: 300,
        height: screen.availHeight - 320,
        fontSizeDefaultSelection: '18',
        fontSizeSelection: true,
        placeholderText: 'Type Here',
        imageUploadParam: 'upload',
        imageUploadURL: '/ckeditor/save',
        imageUploadMethod: 'POST',
        imageMaxSize: 5 * 1024 * 1024,
        imageAllowedTypes: ['jpeg', 'jpg', 'png']
    }
    vm.EditorConfig = EditorConfig

    vm.form = {};
    vm.form.userAnswer = vm.answerText || "";

    $scope.$watch(function() {
        return vm.answerText
    }, function(newValue, oldValue, scope) {
        vm.form.userAnswer = vm.answerText || "";
    });

    

    

    vm.submitAnswer = submitAnswer;
    vm.trustAsAHTML = trustAsAHTML;
    

    vm.$onInit = function(){
        vm.ansModalDelegate = vm.ansModalDelegate || {};
        vm.ansModalDelegate.openAnswerModal = vm.openAnswerModal = openAnswerModal;
        vm.ansModalDelegate.closeAnswerModal = vm.closeAnswerModal = closeAnswerModal;
        // console.log("vm.ansModalDelegate......",vm.ansModalDelegate)
    }


    setTimeout(function() {
        //close modal on esc key
        $(document).on('keydown', function(e) {
            if (e.key === 'Escape' && vm.shouldShowAnswerModal) {
                //close answer modal
                closeAnswerModal();
                $scope.$apply();
            }
        })
    })

    setTimeout(function() {
        vm.id = vm.queId;
        //parent dom element for this answer modal
        vm.parentElement = $("#ans_modal_" + vm.id);
        //find ans-area textarea under parent element
        // vm.froalaElement = $(vm.parentElement).find('.ans-area');
        //init froala editor
        // $(vm.froalaElement).froalaEditor(EditorConfig);
    }, 500);


    $('.ans-modal-wrapper').on('click', function(e) {
        e.stopPropagation();
    })

    //open answer modal event
    $scope.$on('openAnswerModal', function(event, obj) {
        //obj = {event : e , question : q}
        vm.openAnswerModal(obj.event, obj.question);
    });

    //submit answer
    function submitAnswer() {
        //plain text from editor
        var plainTitle = vm.form.userAnswer; //$(vm.parentElement).find(".ans-modal-wrapper").find(".fr-element").text();
        //html text from editor
        var title = vm.form.userAnswer; //$(vm.parentElement).find(".ans-modal-wrapper").find(".fr-element").html();

        if (!title || title.length <= 5) {
            //if title is empty
            vm.form.error = "Please provide answer.";
            return;
        }

        var usr_id = authToken.getUserId();
        var pst_id = vm.queId;
        var cmnt_txt = title;
        var cmnt_atch = [];
        var cmnt_vid = [];
        var cmnt_img = [];
        var cmnt_cmnt_id = undefined;
        var cmnt_lvl = "0";

        var obj = {
            usr_id: usr_id,
            pst_id: pst_id,
            cmnt_txt: cmnt_txt,
            cmnt_atch: cmnt_atch,
            cmnt_vid: cmnt_vid,
            cmnt_img: cmnt_img,
            cmnt_cmnt_id: cmnt_cmnt_id,
            cmnt_lvl: cmnt_lvl,
            from: 'qa-ans'
        }

        vm.form.error = '';
            //start loader
        vm.isAnsSubmitting = true;
        if (vm.answerId) {

            obj.cmnt_id = vm.answerId
            obj.cmnt_ts = vm.answerTimestamp;
            //edit answer to server
            forums.editQAAnswer(obj)
                .then(function(res) {
                    //call callback(delegate method) with new answer id
                    vm.onAnsSubmit({
                        id: vm.answerId
                    });
                    //update notification for this question followers
                    forums.updateNotifications({
                        qry_id: pst_id,
                        ans_usr_id: authToken.getUserId(),
                        ans_id: res.data.cmnt_id
                    });
                    //close answer modal
                    vm.closeAnswerModal();
                })
                .catch(function(err) {
                    console.error(err);
                })
                .finally(function() {
                    // stop loader
                    vm.isAnsSubmitting = false;
                    $(progress_ele).css('width', '0%');
                })
        } else {
            //submit answer to server
            forums.saveQAAnswer(obj)
                .then(function(res) {
                    //call callback(delegate method) with new answer id
                    vm.onAnsSubmit({
                        id: res.data.cmnt_id
                    });
                    //update notification for this question followers
                    forums.updateNotifications({
                        qry_id: pst_id,
                        ans_usr_id: authToken.getUserId(),
                        ans_id: res.data.cmnt_id
                    });

                    if (authToken.isAuthenticated()) {
                        //increment answer count
                        var update_stats_obj = {
                            qry_stats_update: {
                                qry_id: pst_id,
                                qry_ans: 1
                            },
                            usr_stats_update: {
                                usr_id: authToken.getUserId(),
                                qry_id: pst_id,
                                qry_ans: true
                            }
                        }

                        forums.updateQuestionStats(update_stats_obj);
                    }
                    //close answer modal
                    vm.closeAnswerModal();
                })
                .catch(function(err) {
                    console.error(err);
                    vm.form.error = "Error in saving answer.Please try again.";
                })
                .finally(function() {
                    // stop loader
                    vm.isAnsSubmitting = false;
                    $(progress_ele).css('width', '0%');
                })
        }


    }

    //open answer modal
    function openAnswerModal($event, question) {
        var e = $event;

        //question title to display
        vm.currentQueTitle = question.qry_title;
        //question desc to display
        vm.currentQueDesc = question.qry_txt;
        //question id
        vm.currentQueId = question.qry_id || vm.queId;
        vm.shouldShowAnswerModal = true;
    }

    //close answer modal
    function closeAnswerModal() {
        vm.shouldShowAnswerModal = false;
        // vm.isAnswerOverlayOpen = false;
        // close background overlay
        $(vm.parentElement).find('.ans-overlay').addClass('ds-close').removeClass('ds-open');
    }

    //trust html
    function trustAsAHTML(html) {
        return $sce.trustAsHtml(html);
    }
}