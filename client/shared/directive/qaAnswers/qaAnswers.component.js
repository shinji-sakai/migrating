require(['./_qaAnswers.scss']);
require('directiveModule');
require('ng-file-upload');
require('authToken');
require('service.comments');
require('directive.videoplayer_jw');
require('service.forums');
require('service.utility');

require('directive.AnsModalComponent');
require("ErrImage");

require('directive.login');

require('alertify.js/dist/js/alertify.js');
require('alertify.js/dist/js/ngAlertify.js');
require('alertify.js/dist/css/alertify.css');


//tooltip
require('directive.myDrop');

angular.module("directive")
    .component('qaAnswers', {
        template: require('./qaAnswers.html'),
        controller: CommentController,
        bindings: {
            postId: '=', // post id for which we will fetch comments
            showCommentField: '=?', // show main comment field 
            commentId: '=?', // if we want to show only particular comment
            getLinkToShare: '&?', // get function for seperate link to comment
            showRefLink: '=?', // should show reference link button or not
            noCommentCallback: '&?', // this callback will be called if no comment is there
            showBigCommnentField: '=?', // should show big comment field or one liner field
            canUploadMedia: '=?', // should show upload media button or not
            isQaPage: '=?', // page is qa page or profile page(for styling/changes based on pages )
            onDeleteComment: '&?' // delete comment callback
        },
        controllerAs: 'vm'
    });

CommentController.$inject = ['authToken', 'comment', 'Upload', '$state', '$filter', 'forums', 'utility', '$timeout','alertify'];

function CommentController(authToken, comment, Upload, $state, $filter, forums, utility, $timeout,alertify) {
    var vm = this;


    vm.videoAttachments = {};
    vm.photoAttachments = {};
    vm.reportReasonValue = "wrong";

    //////////////
    // Function //
    //////////////

    //Helper methods
    vm.isUserLoggedIn = authToken.isAuthenticated;
    vm.getUserPic = authToken.getUserPic;
    vm.toggleOptions = toggleOptions;
    vm.isAuthorOfComment = isAuthorOfComment;
    vm.trustAsHTML = utility.trustAsHTML;
    vm.getCareerBookProfileUrl = utility.getCareerBookProfileUrl

    vm.getQAAnswerPageUrl = utility.getQAAnswerPageUrl;
    vm.getRegisterUrl = utility.getRegisterUrl

    //Events
    vm.cancelComment = cancelComment;
    vm.keyDownEventOnTextField = keyDownEventOnTextField;
    vm.keyUpEventOnEditCommentTextField = keyUpEventOnEditCommentTextField;

    //edit commets 
    vm.editComment = editComment;
    vm.cancelEditComment = cancelEditComment;

    //delete comment modal helper
    vm.showDeleteCommentModal = showDeleteCommentModal;
    vm.closeDeleteCommentModal = closeDeleteCommentModal;


    //report modal helper
    vm.showReportModal = showReportModal;
    vm.closeReportModal = closeReportModal;
    vm.reportComment = reportComment;

    //copy modal helper
    vm.showCopyModal = showCopyModal;
    vm.closeCopyModal = closeCopyModal;

    //Comment API
    vm.addComment = addComment;
    vm.deleteComment = deleteComment;
    vm.saveEditedComment = saveEditedComment;
    vm.likeDislikeComment = likeDislikeComment;
    vm.toggleCommentLike = toggleCommentLike; // only in profile not forum
    vm.isCommentLikedByUser = isCommentLikedByUser;
    vm.getDisLikeCount = getDisLikeCount;
    vm.getLikeCount = getLikeCount;

    //Sublevel comments helper methods
    vm.showSublevelCommentTextField = showSublevelCommentTextField;
    vm.collapseSublevelCommentTextField = collapseSublevelCommentTextField;
    vm.showSublevelComments = showSublevelComments;

    vm.giveIdOfCommentToShare = giveIdOfCommentToShare;

    $(document).on('click', function() {
        //Close comment toggle options if open
        $('.post-options').find('.overlay').hide();
        $('.post-options').removeClass('open');
    })

    vm.$onInit = function() {
        setTimeout(function() {
            $("#" + vm.postId + "_textarea").focus();
        })

        $(document).on("shown.bs.dropdown", ".dropdown", function() {
            // calculate the required sizes, spaces
            var $ul = $(this).children(".dropdown-menu");
            var $button = $(this).children(".dropdown-toggle");
            var ulOffset = $ul.offset();
            // how much space would be left on the top if the dropdown opened that direction
            var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
            // how much space is left at the bottom
            var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
            // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
            if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
                $(this).addClass("dropup");
        }).on("hidden.bs.dropdown", ".dropdown", function() {
            // always reset after close
            $(this).removeClass("dropup");
        });

        //Get Post Comments
        getPostComments();

        //Get Like Dislike of comments by user
        getLikeDislikeComments();

        //Get comments like dislike counts
        getCommentLikeDislikeCount();
    }

    function getPostComments() {
        comment.getPostComments({
                pst_id: vm.postId
            })
            .then(function(res) {
                vm.subLevelComments = {};
                vm.comments = [];
                vm.allComments = [];
                var allCmnts = vm.allComments = res.data;

                if (vm.commentId) {
                    //if we want to show only one particular comment
                    allCmnts = allCmnts.filter(function(v) {
                        if (v.cmnt_id === vm.commentId || v.cmnt_cmnt_id === vm.commentId) {
                            return true;
                        }
                        return false;
                    })
                }
                allCmnts.map(function(v, i) {
                    if (v.cmnt_lvl === "0") {
                        vm.comments.push(v);
                    } else if (v.cmnt_lvl === "1") {
                        vm.subLevelComments[v.cmnt_cmnt_id] = vm.subLevelComments[v.cmnt_cmnt_id] || {};
                        vm.subLevelComments[v.cmnt_cmnt_id]['comments'] = vm.subLevelComments[v.cmnt_cmnt_id]['comments'] || [];

                        //Add comment to this sublevel
                        vm.subLevelComments[v.cmnt_cmnt_id]['comments'].push(v);

                        //Hide comment text field of this sublevel
                        vm.subLevelComments[v.cmnt_cmnt_id]['showSublevelCommentTextField'] = false;

                        //Hide all comments of this sublevel
                        vm.subLevelComments[v.cmnt_cmnt_id]['showSublevelAllComments'] = false;
                    }
                });

                // if there is no zero level comments
                if (vm.comments.length <= 0 && vm.noCommentCallback) {
                    vm.noCommentCallback();
                }
            })
            .catch(function(err) {
                console.error(err);
            })
    }


    //toggle options
    function toggleOptions($event) {
        var ele = $event.currentTarget;
        $event.stopPropagation();
        if ($(ele).hasClass('open')) {
            $('.post-options').find('.overlay').hide();
            $('.post-options').removeClass('open');
        } else {
            $('.post-options').find('.overlay').hide();
            $('.post-options').removeClass('open');
            $(ele).find('.overlay').show();
            $(ele).addClass('open');
        }
    }

    //check whether user is author of comment
    function isAuthorOfComment(cmnt_id) {
        if (!authToken.isAuthenticated()) {
            return false;
        }

        var cmnt = $filter('filter')(vm.allComments, {
            'cmnt_id': cmnt_id
        })[0];
        if (cmnt.usr_id === authToken.getUserId()) {
            return true;
        }
        return false;

    }

    //clear comment text field value
    function clearCommentTextFieldValue(ele) {
        if (!ele) {
            var arr = $("[contenteditable]");
            arr.each(function(i, block) {
                $(block).text("");
                $(block).removeAttr("style");
            });
        }
        $(ele).text("");
    }


    //Cancel comment and Decrease height of comment text field
    function cancelComment($event) {
        var tf = $($event.target).parent().siblings(".input-comment");
        clearCommentTextFieldValue(tf);
        $(tf).text("");
        $(tf).removeAttr("style").addClass('with-full-border');

        //remove bottom bar which contain submit button
        $(tf).siblings(".bottom-tf").removeClass("show-bar");
    }

    // Key down event
    // when user press any key on comment textfield
    function keyDownEventOnTextField($event, uniqueid, cmnt_cmnt_id, cmnt_lvl) {
        var eve = $event;
        vm.comment = vm.comment || {};
        if (eve.keyCode === 13 && eve.shiftKey === false) {
            if (vm.comment[uniqueid] && vm.comment[uniqueid].length > 0) {
                addComment($event, uniqueid, cmnt_cmnt_id, cmnt_lvl);
                return;
            } else {
                vm.comment[uniqueid] = "";
                eve.stopPropagation();
                return;
            }
        }
    }

    // Key down event on edit comment field
    // when user press any key on edit comment textfield
    function keyUpEventOnEditCommentTextField($event, uniqueid) {
        var eve = $event;
        vm.comment = vm.comment || {};
        if (eve.keyCode === 13 && eve.shiftKey === false) {
            if (vm.comment[uniqueid] && vm.comment[uniqueid].length > 0) {
                saveEditedComment(uniqueid, vm.comment[uniqueid]);
                return;
            } else {
                vm.comment[uniqueid] = "";
                eve.stopPropagation();
                return;
            }
        }
    }

    vm.autoIncreaseTextarea = function($event){
        var element = $event.target
        element.style.height = "auto";
        element.style.height = (element.scrollHeight)+"px";
    }

    vm.openAnswerModal = function($event) {
        if (!authToken.isAuthenticated()) {
            vm.onLoginRequired()
            return;
        }
        vm.ansModalDelegate.openAnswerModal($event, {})
    }

    vm.openAnswerModalForNewAnswer = function(){
        if (!authToken.isAuthenticated()) {
            vm.shouldShowAlertModal = true;
            vm.alertMsg = "Login is required to answer the question"
            return;
        }
        vm.currentEditedQAAnswer = "";
        vm.currentEditedQAAnswerId = "";
        vm.currentEditedQAAnswerTimestamp = ""
        vm.openAnswerModal();
    }

    vm.onAnsSubmit = function(id) {
        var ans_id = id;

        //redirect to answer page with question and ans id
        vm.getQAAnswerPageUrl(true, vm.postId, ans_id);
    }

    function editComment(commentId) {
        var cmnt = $filter('filter')(vm.allComments, {
            cmnt_id: commentId
        })[0];
        if (vm.isQaPage && cmnt.cmnt_lvl === "0") {
            vm.currentEditedQAAnswer = cmnt.cmnt_txt;
            vm.currentEditedQAAnswerId = commentId;
            vm.currentEditedQAAnswerTimestamp = cmnt.cmnt_ts
            vm.openAnswerModal();
        } else {
            vm.isCommentEditingInProcess = true;
            vm.currentCommentEditing = commentId;
        }

    }

    function cancelEditComment(commentId) {
        vm.isCommentEditingInProcess = false;
        vm.currentCommentEditing = '';
        vm.comment[commentId] = '';
    }

    //save edited comments
    function saveEditedComment(commentId, cmnt_txt) {
        vm.isPostUploading = true;

        var cmnt = $filter('filter')(vm.allComments, {
            cmnt_id: commentId
        })[0];

        var obj_edit = {
            pst_id: vm.postId,
            cmnt_txt: cmnt_txt, //$('.edit-comment-area').find('.fr-view').html(),
            cmnt_id: commentId,
            usr_id: authToken.getUserId(),
            cmnt_lvl: cmnt.cmnt_lvl,
            cmnt_ts: cmnt.cmnt_ts,
            alreadyAddedPhotos: [],
            cmnt_img: [],
            alreadyAddedVideos: [],
            cmnt_vid: []
        }

        comment.editComment(obj_edit)
            .then(function(d) {

                vm.isCommentEditingInProcess = false;

                vm.currentCommentEditing = '';

                vm.comment[commentId] = "";

                // getPostComments();
                if (cmnt.cmnt_lvl === '0') {
                    //if level 0
                    var c = $filter('filter')(vm.comments, {
                        cmnt_id: commentId
                    })[0];
                    c.cmnt_txt = cmnt_txt;
                } else if (cmnt.cmnt_lvl === '1') {
                    var tempComments = vm.subLevelComments[cmnt.cmnt_cmnt_id].comments;
                    var c = $filter('filter')(tempComments, {
                        cmnt_id: commentId
                    })[0];
                    c.cmnt_txt = cmnt_txt;
                }

                // closeEditCommentModal();
                // $('.edit-comment-area').html(''),
                // $(".edit-comment-modal .progressbar").css('width', '0%');
                // $state.reload();
            })
            .catch(function(err) {
                console.error(err);
            })
            .finally(function() {
                vm.isPostUploading = false;
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $(".edit-comment-modal .progressbar").css('width', progressPercentage + '%');
                console.log('progress: ' + progressPercentage + '% ');
            })
    }

    //Add Comment to database on submit button click
    //if user is logged in
    function addComment($event, uniqueid, cmnt_cmnt_id, cmnt_lvl) {
        if (!vm.isUserLoggedIn()) {
            vm.commentPostingErr = "You need to login to comment"
            return;
        }
        var txt_ele = $($event.target).parent().siblings('.input-comment');
        var progress_ele = $($event.target).parent().find('.progressbar');
        var usr_id = authToken.getUserId();
        var pst_id = vm.postId;
        var cmnt_txt = vm.comment[uniqueid];
        var cmnt_atch = [];
        var cmnt_vid = vm.videoAttachments[uniqueid] || [];
        var cmnt_img = vm.photoAttachments[uniqueid] || [];
        var cmnt_cmnt_id = cmnt_cmnt_id || undefined;
        var cmnt_lvl = cmnt_lvl;

        var obj = {
            usr_id: usr_id,
            pst_id: pst_id,
            cmnt_txt: cmnt_txt,
            cmnt_atch: cmnt_atch,
            cmnt_vid: cmnt_vid,
            cmnt_img: cmnt_img,
            cmnt_cmnt_id: cmnt_cmnt_id,
            cmnt_lvl: cmnt_lvl
        }

        vm.isPostSubmitting = true;
        comment.saveComment(obj)
            .then(function(res) {
                obj.cmnt_id = res.data.cmnt_id;
                obj.cmnt_ts = res.data.cmnt_ts;

                vm.comment[uniqueid] = "";

                vm.commentAttachments = vm.commentAttachments || {};
                vm.commentAttachments[uniqueid] = [];

                if (cmnt_img.length > 0 || cmnt_vid.length > 0) {
                    //if Attachment is there then reload
                    $state.reload();
                }

                obj['usr_info'] = authToken.getUser();

                vm.allComments.push(obj);
                if (cmnt_lvl === "1") {
                    vm.subLevelComments[cmnt_cmnt_id] = vm.subLevelComments[cmnt_cmnt_id] || {};
                    vm.subLevelComments[cmnt_cmnt_id]['comments'] = vm.subLevelComments[cmnt_cmnt_id]['comments'] || [];

                    //Add comment to this sublevel
                    vm.subLevelComments[cmnt_cmnt_id]['comments'].push(obj);

                    //Show comment text field of this sublevel
                    vm.subLevelComments[cmnt_cmnt_id]['showSublevelCommentTextField'] = true;

                    //Show all comments of this sublevel
                    vm.subLevelComments[cmnt_cmnt_id]['showSublevelAllComments'] = true;
                } else {
                    //Cmnt_lvl == 0
                    vm.comments.push(obj);
                }

                cancelComment($event);
            }, null, function progressNotification(progress) {
                var percentage = (progress.loaded / progress.total) * 100;
                console.info(percentage);
                $(progress_ele).css('width', percentage + '%');
            })
            .catch(function(err) {
                console.error(err);
            })
            .finally(function() {
                vm.isPostSubmitting = false;
                $(progress_ele).css('width', '0%');
            })
    }


    //open delete comment modal
    function showDeleteCommentModal(cmnt_id) {
        vm.currentlyDeletedComment = cmnt_id;
        vm.shouldShowDeleteCommentModal = true;
    }

    //close delete comment modal
    function closeDeleteCommentModal() {
        vm.currentlyDeletedComment = '';
        vm.shouldShowDeleteCommentModal = false;
    }

    //open report modal
    function showReportModal(cmnt_id) {
        if(!vm.isUserLoggedIn()){
            vm.shouldShowAlertModal = true;
            vm.alertMsg = "Login is required to report"
            return;
        }
        vm.currentlyReportedComment = cmnt_id;
        vm.shouldShowReportModal = true;
    }

    //close report modal
    function closeReportModal() {
        vm.currentlyReportedComment = '';
        vm.shouldShowReportModal = false;
    }

    //report comment to server
    function reportComment(cmnt_id) {
        if(!vm.isUserLoggedIn()){
            return;
        }
        var rsn = vm.reportReasonValue;
        if (vm.reportReasonValue === 'other') {
            //get value from rsn textarea
            rsn = vm.reportReasonOtherValue;
        }

        //report to server
        forums.reportQuestion({
                cmpln_id: cmnt_id,
                usr_id: authToken.getUserId(),
                cmpln_rsn: rsn,
                cmpln_typ: 'answer'
            })
            .then(function(res) {
                closeReportModal();
                alertify.success("Successfully Reported")
            })
            .catch(function(err) {
                console.log(err);
                alertify.error("Error in Reporting")
            });
    }

    //open copy modal
    function showCopyModal(cmnt_id) {
        vm.currentlyCopiedComment = cmnt_id;
        vm.link = window.location.origin + '/question/' + vm.postId + '/answer/' + cmnt_id;
        vm.shouldShowCopyModal = true;
    }

    //close copy modal
    function closeCopyModal() {
        vm.currentlyCopiedComment = '';
        vm.shouldShowCopyModal = false;
    }

    //delete comment from server using api
    function deleteComment(cmnt_id) {
        var cmnt = $filter('filter')(vm.allComments, {
            'cmnt_id': cmnt_id
        })[0];
        var delete_obj = {
            pst_id: vm.postId,
            cmnt_id: cmnt.cmnt_id,
            cmnt_ts: cmnt.cmnt_ts,
            usr_id: authToken.getUserId()
        }
        comment.deleteComment(delete_obj)
            .then(function(res) {


                if (cmnt.cmnt_lvl === '0') {
                    if (vm.onDeleteComment) {
                        vm.onDeleteComment({
                            isAnswer: true
                        });
                    }

                    // if it is answer 
                    // then update answer count
                    var update_stats_obj = {
                            qry_stats_update: {
                                qry_id: vm.postId,
                                qry_ans: -1
                            },
                            usr_stats_update: {
                                usr_id: authToken.getUserId(),
                                qry_id: vm.postId,
                                qry_ans: false
                            }
                        }
                        //update to server
                    forums.updateQuestionStats(update_stats_obj);

                    //if it is 0 level comment
                    //then direct remove from 0th level comments array
                    vm.comments = vm.comments.filter(function(v, i) {
                        if (v.cmnt_id === cmnt_id) {
                            return false;
                        }
                        return true;
                    })

                    alertify.success("Successfully deleted the answer")
                } else {
                    if (vm.onDeleteComment) {
                        vm.onDeleteComment({
                            isAnswer: false
                        });
                    }
                    //if it is 1st level comment 
                    //then first find that comment from parent's all sublevel comments
                    //then delete it
                    var tempComments = vm.subLevelComments[cmnt.cmnt_cmnt_id].comments;
                    vm.subLevelComments[cmnt.cmnt_cmnt_id].comments = tempComments.filter(function(v, i) {
                        if (v.cmnt_id === cmnt_id) {
                            return false;
                        }
                        return true;
                    })

                    alertify.success("Successfully deleted the comment")
                }
                closeDeleteCommentModal();
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    //Toggle like button (for now only in ewcb)
    //Like or Unlike
    function toggleCommentLike(cmnt_id, isLike) {
        //Fetch comment from comments data
        var cmnt = $filter('filter')(vm.commentsLikeData, {
            cmnt_id: cmnt_id
        })[0];
        var cmnt_like_stats = $filter('filter')(vm.commentsLikeCountStats, {
            cmnt_id: cmnt_id
        })[0];

        if (!cmnt_like_stats) {
            var tmp_obj = {
                qry_id: vm.postId,
                cmnt_id: cmnt_id,
                like_cnt: 0,
                dlike_cnt: 0
            }
            vm.commentsLikeCountStats.push(tmp_obj);
            cmnt_like_stats = tmp_obj;
        }

        var cnt_obj = {
            qry_id: vm.postId,
            cmnt_id: cmnt_id,
        }
        if (cmnt) {
            //if comment is already in comment data array means user has liked or unliked earlier
            if (isLike) {
                //Increment like count
                cnt_obj['like'] = 1;

                cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;
            } else if (!isLike) {
                //decrement like count
                cnt_obj['like'] = -1;

                cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) - 1;
            }
        } else {
            if (isLike) {
                //Increment like count
                cnt_obj['like'] = 1;

                cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;

            } else if (!isLike) {
                //keep like count as it is
                cnt_obj['like'] = -1;

                cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;
            }
        }

        comment.updateCommentLikeDislikeCount(cnt_obj)
            .then(function(res) {})
            .catch(function(err) {
                console.error(err);
            });

        var obj = {
            qry_id: vm.postId,
            usr_id: authToken.getUserId(),
            cmnt_id: cmnt_id,
            like_dlike_flg: isLike
        }
        comment.likeDislikeComment(obj)
            .then(function(res) {
                if (cmnt) {
                    //if comment is already added update it
                    cmnt.like_dlike_flg = isLike;
                } else {
                    //if comment is not there push it
                    vm.commentsLikeData.push(obj);
                }
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    //like/dislike comment by user
    function likeDislikeComment(cmnt_id, isLike) {
        if(!vm.isUserLoggedIn()){
            vm.shouldShowAlertModal = true;
            vm.alertMsg = "Login is require to like/dislike"
            return
        }
        //Fetch comment from comments data
        var cmnt = $filter('filter')(vm.commentsLikeData, {
            cmnt_id: cmnt_id
        })[0];
        var cmnt_like_stats = $filter('filter')(vm.commentsLikeCountStats, {
            cmnt_id: cmnt_id
        })[0];

        if (!cmnt_like_stats) {
            var tmp_obj = {
                qry_id: vm.postId,
                cmnt_id: cmnt_id,
                like_cnt: 0,
                dlike_cnt: 0
            }
            vm.commentsLikeCountStats.push(tmp_obj);
            cmnt_like_stats = tmp_obj;
        }

        var cnt_obj = {
            qry_id: vm.postId,
            cmnt_id: cmnt_id,
        }
        if (cmnt) {
            //if comment is already in comment data array means user has liked or unliked earlier
            if (isLike) {
                //Increment like count
                cnt_obj['like'] = 1;
                //decrement dislike count
                cnt_obj['dislike'] = -1;

                cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;
                cmnt_like_stats.dlike_cnt = parseInt(cmnt_like_stats.dlike_cnt) - 1;
            } else if (!isLike) {
                //decrement like count
                cnt_obj['like'] = -1;
                //increment dislike count
                cnt_obj['dislike'] = 1;

                cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) - 1;
                cmnt_like_stats.dlike_cnt = parseInt(cmnt_like_stats.dlike_cnt) + 1;
            }
        } else {
            if (isLike) {
                //Increment like count
                cnt_obj['like'] = 1;
                //keep dislike count as it is
                cnt_obj['dislike'] = 0;

                cmnt_like_stats.like_cnt = parseInt(cmnt_like_stats.like_cnt) + 1;

            } else if (!isLike) {
                //keep like count as it is
                cnt_obj['like'] = 0;
                //increment dislike count
                cnt_obj['dislike'] = 1;

                cmnt_like_stats.dlike_cnt = parseInt(cmnt_like_stats.dlike_cnt) + 1;
            }
        }

        comment.updateCommentLikeDislikeCount(cnt_obj)
            .then(function(res) {})
            .catch(function(err) {
                console.error(err);
            });

        var obj = {
            qry_id: vm.postId,
            usr_id: authToken.getUserId(),
            cmnt_id: cmnt_id,
            like_dlike_flg: isLike
        }
        comment.likeDislikeComment(obj)
            .then(function(res) {
                if (cmnt) {
                    //if comment is already added update it
                    cmnt.like_dlike_flg = isLike;
                } else {
                    //if comment is not there push it
                    vm.commentsLikeData.push(obj);
                }
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    //get users like/dislike comments
    function getLikeDislikeComments() {
        if (!authToken.isAuthenticated()) {
            //If user is not logged in
            return;
        }
        var obj = {
            qry_id: vm.postId,
            usr_id: authToken.getUserId()
        }
        comment.getLikeDislikeComments(obj)
            .then(function(res) {
                vm.commentsLikeData = res.data;
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    //get like/dislike count from server 
    function getCommentLikeDislikeCount() {
        var obj = {
            qry_id: vm.postId,
        }
        comment.getCommentLikeDislikeCount(obj)
            .then(function(res) {
                vm.commentsLikeCountStats = res.data;
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    //get like count from array
    function getLikeCount(cmnt_id) {
        vm.commentsLikeCountStats = vm.commentsLikeCountStats || [];
        var cmnt = $filter('filter')(vm.commentsLikeCountStats, {
            cmnt_id: cmnt_id
        })[0];
        if (cmnt) {
            return cmnt.like_cnt;
        }
        return 0;
    }
    //get dislike count from array
    function getDisLikeCount(cmnt_id) {
        vm.commentsLikeCountStats = vm.commentsLikeCountStats || [];
        var cmnt = $filter('filter')(vm.commentsLikeCountStats, {
            cmnt_id: cmnt_id
        })[0];
        if (cmnt) {
            return cmnt.dlike_cnt;
        }
        return 0;
    }

    //whether comment is liked or disliked by user
    function isCommentLikedByUser(cmnt_id) {
        if (authToken.isAuthenticated()) {
            vm.commentsLikeData = vm.commentsLikeData || [];
            var cmnt = $filter('filter')(vm.commentsLikeData, {
                cmnt_id: cmnt_id
            })[0];
            if (!cmnt) {
                //If not liked or disliked
                return 0;
            }
            if (cmnt['like_dlike_flg'] === 0) {
                //if dislike
                return false;
            } else {
                //if like
                return true;
            }
        }
    }

    //show sublevel comment textfield
    function showSublevelCommentTextField(id, idIfLevel1Comment) {
        vm.subLevelComments[id] = vm.subLevelComments[id] || {};
        var level1Comment = {};
        if (idIfLevel1Comment) {
            if(!vm.isUserLoggedIn()){
                vm.shouldShowAlertModal = true;
                vm.alertMsg = "Login is required to comment";
                return;
            }
            level1Comment = vm.subLevelComments[id].comments.filter(function(v, i) {
                if (v.cmnt_id === idIfLevel1Comment) {
                    return true;
                }
                return false;
            })[0];
            var usr_nm = level1Comment.usr_info.dsp_nm;
            console.log(usr_nm);
            $timeout(function() {
                vm.comment = vm.comment || {};
                vm.comment[id] = usr_nm + " : ";
            });
        }
        vm.subLevelComments[id]['showSublevelCommentTextField'] = true;
        setTimeout(function() {
            $("#" + id + "_textarea").focus();
            if (!checkVisible($("#" + id + "_textarea"))) {
                $('html,body').animate({
                        scrollTop: $("#" + id + "_textarea").offset().top - 230
                    },
                    'fast');
            }
        });
    }

    function checkVisible(elm, evalType) {
        evalType = evalType || "visible";
        var vpH = $(window).height(), // Viewport Height
            st = $(window).scrollTop() + 230, // Scroll Top
            y = $(elm).offset().top,
            elementHeight = $(elm).height();

        if (evalType === "visible") return ((y < (vpH + st)) && (y > (st - elementHeight)));
        if (evalType === "above") return ((y < (vpH + st)));
    }

    //collapse sublevel comments
    function collapseSublevelCommentTextField(id) {
        vm.subLevelComments[id] = vm.subLevelComments[id] || {};
        vm.subLevelComments[id]['showSublevelCommentTextField'] = false;
        vm.comment = vm.comment || {};
        vm.comment[id] = "";
    }

    function showSublevelComments(id) {
        vm.subLevelComments[id] = vm.subLevelComments[id] || {};
        vm.subLevelComments[id]['showSublevelAllComments'] = true;
    }

    //give id of comment
    function giveIdOfCommentToShare(id) {
        vm.getLinkToShare({
            id: id
        });
    }

}