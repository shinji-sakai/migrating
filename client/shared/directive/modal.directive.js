angular
    .module('directive')
    .directive('modal', ModalDirective)
    .controller('modalCtrl', ModalCtrl);

function ModalCtrl () {}

ModalDirective.$inject = ['$window'];
function ModalDirective($window) {
    return {
        template: '<div class="modal fade" style="padding:0px !important">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            // '<div class="modal-header">' +
            // '<button type="button" class="close modal-close-button" aria-hidden="true" ng-click="close()"><i class="fa fa-arrow-left"></i></button>' +
            // '<h4 class="modal-title">{{ title }}</h4>' +
            // '</div>' +
            '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {},

        controller : 'modalCtrl',
        controllerAs : 'vm',
        bindToController: {
            showModal: "=",
            width : "@",
            height: "@"
        },
        
        
        link: function postLink(scope, element, attrs, ctrl) {
            scope.title = attrs.title;
            var $modal = $($(element)[0]);
            // console.log($modal);
            var $modal_content = $modal.find(".modal-content");
            var $modal_dialog = $modal.find(".modal-dialog");
            var $modal_header = $modal.find(".modal-header > *");
            var $modal_body = $modal.find(".modal-body > *");

            
            $modal_dialog.css("width",ctrl.width).css("height","100vh").css("margin","0px");
            $modal_content.css("height",ctrl.height)
                            .css("border","0px")
                            .css("border-radius","0px")
                            .css("color","white")
                            .css("background-color","rgba(0,0,0,1)")
                            .css('box-shadow','0px 0px 0px');

            $modal_header.css("color","white").css("opacity","1");
            $modal.find(".modal-body").css("padding","0px").css("overflow","hidden");
            $modal_body.css("color","white").css("opacity","1");
            setTimeout(function(){
                $(".modal").css('padding-left','0px !important');    
            });
            

            scope.$watch(function(){return ctrl.showModal;}, function(value) {
                if (ctrl.showModal == true)
                    $(element).modal({
                        'show' : true
                    });
                else
                    $(element).modal('hide');
            });
            $(element).on('shown.bs.modal', function() {
                scope.$apply(function() {
                    ctrl.showModal = true;
                });
            });
            $(element).on('hidden.bs.modal', function() {
                scope.$apply(function() {
                    ctrl.showModal = false;
                });
            });

            scope.close = function(){
                $(element).modal('hide');
            }
        }
    };
}