require('directiveModule');
require('serviceModule');
require('service.utility');
require('service.forums');
require('service.profile');
require('service.admin');
require("authToken");
require("directive.login")
require("directive.chat");
require("directive.subscribeModal");
require('./_headerMenu.scss');
require("service.chatUtility");
require('directive.userDefaultPic');
angular.module("directive")
    .component('headerMenuComponent', {
        template: require('./headerMenu.html'),
        controller: HeaderMenuController,
        controllerAs: 'vm',
        bindings: {
            menuDelegate: '=?'
        }
    });

HeaderMenuController.$inject = ['$log','$compile','utility','authToken','admin','chatUtility','forums','profile'];

function HeaderMenuController($log,$compile,utility,authToken,admin,chatUtility,forums,profile) {

    var vm = this;

    vm.isUserLoggedIn = authToken.isAuthenticated;
    vm.getUser = authToken.getUser;
    vm.getCourseDetailsPageUrl = utility.getCourseDetailsPageUrl;
    vm.getITTrainingsUrl = utility.getITTrainingsUrl;
    vm.getSpecificNotificationPageUrl = utility.getSpecificNotificationPageUrl;
    vm.getNotificationsPageUrl = utility.getNotificationsPageUrl;
    vm.getQAQuestionPageUrl = utility.getQAQuestionPageUrl;

    vm.$onInit = function(){
        vm.menuDelegate = vm.menuDelegate || {};
        vm.getAllCourses();
        if(vm.isUserLoggedIn()){
            vm.getSendNotificationCount();
            vm.getNotificationByUserId();
            // vm.getDetailedFollowNotifications();
            // vm.getTaggedFriendsNotifications();
            // vm.getFriendRequestNotifications();
        }
    }

    vm.getAllCourses = function(){
        admin.courselistfull()
            .then(function(d) {
                vm.courses = d;
            })
            .catch(function(d) {
                console.error(d);
            })
    }

    vm.getSendNotificationCount = function getSendNotificationCount() {
        admin.getSendNotificationCount({
                usr_id: authToken.getUserId()
            })
            .then(function(res) {
                var rows = res.data;
                if(rows[0]){
                    var row = rows[0]
                    row.web_noti_cnt = (row.web_noti_cnt < 0) ? 0 : row.web_noti_cnt
                    row.cb_noti_cnt = (row.cb_noti_cnt < 0) ? 0 : row.cb_noti_cnt
                    row.qa_noti_cnt = (row.qa_noti_cnt < 0) ? 0 : row.qa_noti_cnt
                    vm.WebNotifCount = parseInt(row.web_noti_cnt || 0)
                    vm.CBNotifCount = parseInt(row.cb_noti_cnt || 0)
                    vm.QANotifCount = parseInt(row.qa_noti_cnt || 0)
                    vm.notificationCount = vm.WebNotifCount + vm.CBNotifCount + vm.QANotifCount;
                }
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    vm.getNotificationByUserId = function getNotificationByUserId() {
        admin.getSendNotificationByUserId({
                usr_id: authToken.getUserId()
            })
            .then(function(res) {
                vm.allUsersNotifications = res.data;
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    vm.getDetailedFollowNotifications = function getDetailedFollowNotifications(){
        forums.getDetailedFollowNotifications({
                usr_id: authToken.getUserId()
            })
            .then(function(res) {
                vm.allQANotifications = res.data;
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    vm.getTaggedFriendsNotifications = function getTaggedFriendsNotifications(){
        profile.getTaggedFriendsNotifications({
                to_usr_id : authToken.getUserId()
            })
            .then(function(res) {
                vm.allCBNotifications = vm.allCBNotifications || []
                vm.allCBNotifications = vm.allCBNotifications.concat(res.data);
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    vm.getFriendRequestNotifications = function getFriendRequestNotifications(){
        profile.getFriendRequestNotifications({
                to_usr_id : authToken.getUserId()
            })
            .then(function(res) {
                vm.allCBNotifications = vm.allCBNotifications || []
                vm.allCBNotifications = vm.allCBNotifications.concat(res.data);
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    vm.refreshNotification = function($event) {
        if($event){
            $event.stopPropagation();
            $event.preventDefault();

        }
        vm.getNotificationByUserId();
        vm.getSendNotificationCount();
    }

    vm.markSendNotificationAsRead = function markSendNotificationAsRead($event,usr_noti_pk) {
        $event.stopPropagation();
        $event.preventDefault();
        admin.markSendNotificationAsRead({
                usr_id: authToken.getUserId(),
                usr_noti_pk: usr_noti_pk
            })
            .then(function(res) {
                vm.getNotificationByUserId();
            })
            .catch(function(err) {})
    }

    var chatHistory = chatUtility.getInfo();
    if (chatHistory!=null) {
        if (chatHistory.isOpenDialog==true) {
            vm.shouldShowChatComponent = true;
        }
    };
    $('body').on('click','.chat_fixed', function(){
        $('.fa-angle-up').click();
        chatUtility.isOpenDialog(vm.shouldShowChatComponent);
    });
    $('body').on('click','.logout', function(){
        chatUtility.clearChatInfo();
    });
     //jquery for menu start
    $('.site-menu .list .item .link').click(function(e){
        e.stopPropagation();
        $('.site-menu .list .item .menu').removeClass('active');
        $(this).siblings('.menu').addClass('active');
    });

    $('.site-menu .list .item .menu ul > li.sub-menu').click(function(e){
         e.stopPropagation();
         if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $('.site-menu .list .item .menu ul li').removeClass('active');
            $(this).addClass('active');
        }

    });

    $('.site-menu .list .item .big-menu ul .sub-menu-link li a').click(function(){
        $('.site-menu .list .item .menu ul li .sub-menu-link').Class('active');
        $('.site-menu .list .item .menu').removeClass('active');
    });

    $('.site-menu .list .item .big-menu, .site-menu .list .item.notification .menu').click(function(e){
        e.stopPropagation();
    });

    $('.site-menu .list .item .big-menu .close-menu, .site-menu .list .item .menu ul li:not(.sub-menu) a, .site-menu .list .item.notification .menu .bottom .left, .site-menu .list .item.notification .menu .bottom .right').click(function(e){
        e.stopPropagation();
        $('.site-menu .list .item .menu').removeClass('active');
        $('.site-menu .list .item .menu ul > li.sub-menu').removeClass('active');
    });

    $(document).click(function(){
        $('.site-menu .list .item .menu').removeClass('active');
        $('.site-menu .list .item .menu ul > li.sub-menu').removeClass('active');
    });

    $('.tab-area .tab ul li').click(function(){
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('data-tabid');
        $(this).parents('.tab-area').children('.tab-conten').children('.item').removeClass('active');
        $('#'+id).addClass('active');
    });
    //jquery for menu end
}
