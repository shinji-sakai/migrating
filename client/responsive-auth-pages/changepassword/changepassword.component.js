var Base64 = require('hash_js').Base64;
require('service.utility');
require("directive.headerMenu");
require("auth");
require("authToken")
require("./_changepassword.scss")
angular.module(window.moduleName)
    .component('changePasswordPage', {
        template: require('./changepassword.html'),
        controller: ChangePasswordController,
        controllerAs: 'vm'
    });

ChangePasswordController.$inject = ['auth', 'authToken', '$stateParams', '$state', '$window', '$scope', 'utility','localStorageService'];

function ChangePasswordController(auth, authToken, $stateParams, $state, $window, $scope, utility,localStorageService) {

    var vm = this;
    //////////////
    //Variables //
    //////////////
    vm.isValidPassword = true;
    vm.validPasswordError = "";
    vm.formSuccess = false;
    vm.successMsg = "";
    vm.isFromForgotPassword = false;

    //////////////
    //Functions //
    //////////////
    vm.changePassword = changePassword;
    vm.checkPasswordValidity = checkPasswordValidity;

    vm.$onInit = function() {
    }

    if ($stateParams && $stateParams.q === "firsttime" && authToken.isAuthenticated()) {
        vm.firsttime = true;
        vm.firstTimeMsg = "You are logging in for the first time.Please Change the password";
    } else if ($stateParams && $stateParams.q) {
        try {
            var q = $stateParams.q;
            var obj = JSON.parse(Base64.decode(q));
            var time = new Date(obj.time);
            var now_d = new Date().getTime();
            // console.log((now_d - time));
            if ((now_d - time) >= 3600 * 1000 * 2) {
                vm.isLinkExpired = false;
                vm.linkExpiredError = "Link Expired.Link is valid for only two hours.";
                return;
            }
            vm.isFromForgotPassword = true;
            vm.user = obj;
        } catch (err) {
            console.log(err);
            $state.go("login");
        }
    } else {
        if (!authToken.isAuthenticated()) {
            $state.go("login");
        }
    }

    $scope.$watch(function() {
        return vm.newPassword;
    }, function() {
        passwordMatcher(vm.newPassword, vm.confirmNewPassword);
    })

    $scope.$watch(function() {
        return vm.confirmNewPassword;
    }, function() {
        passwordMatcher(vm.newPassword, vm.confirmNewPassword);
    })

    function passwordMatcher(pass, conf_pass) {
        if (pass != conf_pass) {
            vm.isPasswordMatching = false;
        } else {
            vm.isPasswordMatching = true;
        }
    }


    function checkPasswordValidity() {
        if (!vm.oldPassword)
            return;
        vm.formSuccess = false;
        var user = vm.user || authToken.getUser();
        var data = {
            usr_id: user.usr_id,
            pwd: vm.oldPassword
        }
        vm.isValidPassword = true;
        auth.checkPassword(data)
            .then(function(res) {
                var d = res.data;
                // console.log(d);
                if (d.err) {
                    vm.isValidPassword = false;
                    vm.validPasswordError = d.err;
                }
            })
            .catch(function(err) {
                console.error(err);
            })
    }

    function changePassword(form) {
        if (!form.$valid) {
            if (!form.oldpass.$valid) {
                $("#oldpass").focus();
            } else if (!form.newpass.$valid) {
                $("#newpass").focus();
            } else if (!form.confirmnewpass.$valid) {
                $("#confirmnewpass").focus();
            }
            return;
        }
        if (!vm.isPasswordMatching || !vm.isValidPassword) {
            return;
        }
        if (vm.isLinkExpired) {
            return;
        }
        var user = vm.user || authToken.getUser();
        var data = {
            usr_id: user.usr_id,
            pwd: vm.newPassword
        }
        vm.oldPassword = "";
        vm.newPassword = "";
        auth.changePassword(data)
            .then(function(d) {
                if (!d.err) {
                    vm.formSuccess = true;
                    vm.confirmNewPassword = "";
                    vm.successMsg = "Password changed successfully.";
                    if(vm.isFromForgotPassword){
                        $state.go('login', {changepassword: vm.successMsg})    
                    }
                    
                    $scope.form.$setUntouched();
                    $scope.form.$setPristine();
                }

            })
            .catch(function(err) {
                console.error(err);
            })
    }
}