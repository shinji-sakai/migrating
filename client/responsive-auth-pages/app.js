require('angular');
require('angular-ui-router');
require('angular-local-storage');
require('angular-sanitize');
require('angular-animate');
require('oclazyload');
require('angular-jwt');
require('satellizer');
require('serviceModule');
require('directiveModule');
require('authToken');
var Stats = require('stats_js');


// Http Interceptor
HttpInter.$inject = ['localStorageService'];
function HttpInter(localStorageService) {
    return {
        request: function(config) {
            return config;
        },
        response: function(response) {
        	var imageBaseUrl = response.headers('imageBaseUrl');
        	localStorageService.set('imageBaseUrl', imageBaseUrl);
            return response;
        }
    };
}

angular.module("responsiveAuthApp",[
		'ui.router',
		"oc.lazyLoad",
		'ngAnimate',
		'LocalStorageModule',
		"angular-jwt",
		"ngSanitize",
		'satellizer',
		'service'
	])
	.run(['$rootScope','authToken','$state','$window','$timeout', function($rootScope,authToken,$state,$window,$timeout){
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
        	//Check Auth Paths
			if(toState.authenticate){
				if(!authToken.isAuthenticated()){
					authToken.cachedUrl($window.location.href);
					event.preventDefault();
					window.location.href = "/login"
				}
			}
		  });
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            //Send Stats
            $timeout(function(){
            	var stats = new Stats();
            },10);
        });
	}])
	.factory('HttpInter', HttpInter)
	.config(['$stateProvider','$urlRouterProvider','$httpProvider','$locationProvider','$ocLazyLoadProvider','$authProvider',function($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider,$ocLazyLoadProvider,$authProvider) {

		 //    $ocLazyLoadProvider.config({
			//   debug: true
			// });
		$stateProvider
			.state('login',{
				url : "/login/:page_id",
				template : "<login-page></login-page>",
				resolve : {
					loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
						var defer = $q.defer();
						$ocLazyLoad.toggleWatch(true);
						require(['./login/login.component.js'],function(){
							$ocLazyLoad.inject();
							$ocLazyLoad.toggleWatch(false);
							defer.resolve();
						});
						return defer.promise;
			      	}]
				},
				params  :{
					logout : null,
					isSessionExpired : null,
					forgotpassword : null,
					changepassword : null,
					page_id : {
						value : null,
						squash : true
					}
				}
			})
			.state('logout',{
				url : "/logout/:msg",
				template : "<logout-page></logout-page>",
				resolve : {
					loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
						var defer = $q.defer();
						$ocLazyLoad.toggleWatch(true);
						require(['./logout/logout.component.js'],function(){
							$ocLazyLoad.inject();
							$ocLazyLoad.toggleWatch(false);
							defer.resolve();
						});
						return defer.promise;
			      	}]
				},
				params  :{
					msg : {
						value : null,
						squash : true
					}
				}
			})
			.state('register',{
				url : "/register",
				template : "<register-page></register-page>",
				resolve : {
					loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
						var defer = $q.defer();
						$ocLazyLoad.toggleWatch(true);
						require(['./register/register.component.js'],function(){
							$ocLazyLoad.inject();
							$ocLazyLoad.toggleWatch(false);
							defer.resolve();
						});
						return defer.promise;
			      	}]
				}
			})
			.state('forgotpassword',{
				url : "/forgotpassword",
				template: "<forgot-password-page></forgot-password-page>",
				resolve : {
					loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
						var defer = $q.defer();
						$ocLazyLoad.toggleWatch(true);
						require(['./forgotpassword/forgotpassword.component.js'],function(){
							$ocLazyLoad.inject();
							$ocLazyLoad.toggleWatch(false);
							defer.resolve();
						});
						return defer.promise;
			      	}]
				},
			})
			.state('changepassword',{
				url : "/changepassword/:q?",
				template: "<change-password-page></change-password-page>",
				resolve : {
					loadMyCtrl: ['$q','$ocLazyLoad', function($q,$ocLazyLoad) {
						var defer = $q.defer();
						$ocLazyLoad.toggleWatch(true);
						require(['./changepassword/changepassword.component.js'],function(){
							$ocLazyLoad.inject();
							$ocLazyLoad.toggleWatch(false);
							defer.resolve();
						});
						return defer.promise;
			      	}]
				},
				authenticate : false,
				params : {
					q : {
					    value: null,
					    squash: true
					}
				},
			})

		$authProvider.facebook({
	      clientId: '1680795622155218'
	    });
	    $authProvider.google({
	      clientId: '433533625448-me7tjr76pcuk0ho8av5gvpjh40o5g6mg.apps.googleusercontent.com'
	    });
	    $authProvider.twitter({
	      clientId: 'hcPYSQevRhsR8016f0MllEWbU'
	    });
	    $authProvider.linkedin({
	      clientId: '75w6qtwhhleno5'
	    });

		$urlRouterProvider.otherwise("/login");
		// use the HTML5 History API
        $locationProvider.html5Mode({
        	enabled: true,
  			requireBase: false,
  			rewriteLinks: false
        });
	}]);
