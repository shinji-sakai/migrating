require('authToken');
require('service.utility');
require('directive.headerMenu');
require('auth');
require("./_logout.scss")
angular.module(window.moduleName)
    .component('logoutPage', {
        template: ['$element', '$attrs',
            function($element, $attrs) {
                return require('./logout.html');
            }
        ],
        controller: LogoutController,
        controllerAs: 'vm'
    })
LogoutController.$inject = ['$location', '$window', '$auth', '$stateParams', 'utility', 'auth', 'authToken'];

function LogoutController($location, $window, $auth, $stateParams, utility, auth, authToken) {
    var vm = this;
    
    var page_id = $stateParams.page_id;
    vm.fromLogout = $stateParams.logout;
    vm.fromForgotPassword = $stateParams.forgotpassword;
    vm.fromChangePassword = $stateParams.changepassword;
    vm.isSessionExpired = $stateParams.isSessionExpired;
    vm.isFormSubmitting = false;
    vm.formError = undefined;

    vm.getLoginUrl = utility.getLoginUrl;
    vm.getRegisterUrl = utility.getRegisterUrl;
    vm.getForgotPasswordUrl = utility.getForgotPasswordUrl;
    vm.login = login;
    vm.checkAlreadyLogin = checkAlreadyLogin;
    vm.authenticateWith = authenticateWith;
    vm.verifyOTP = verifyOTP;
    vm.resendOTP = resendOTP;

    vm.$onInit = function() {
        if($stateParams.msg && $stateParams.msg === "session-expired"){
            vm.isSessionExpired = true;
        }else{
            vm.fromLogout = true;
            // api request
            if(authToken.getUserId()){
                auth.logout({
                    usr_id : authToken.getUserId()
                })    
            }
        }
        authToken.removeUser();
        authToken.removeToken();
        authToken.removeSocketId();
        authToken.removeUserCourses();
        authToken.removeSessionId();
        
        checkAlreadyLogin();
        setTimeout(function() {
            $("#email").focus();
        })
    }

    function login(form) {
        var cachedUrl = authToken.cachedUrl();
        if (vm.fromLogout || vm.fromForgotPassword || vm.fromChangePassword || vm.isSessionExpired) {
            // "if it is coming from logout/forgotpassword/changepassword page , clear cachedUrl"
            authToken.clearCachedUrl();
        }
        vm.fromChangePassword = undefined;
        vm.fromForgotPassword = undefined;

        if (!form.$valid) {
            if (!form.email.$valid) {
                $("#email").focus();
            } else {
                $("#password").focus();
            }
            return;
        }
        vm.fromLogout = undefined;
        vm.isSessionExpired = undefined;
        var userloginData = {
            usr_id: vm.usr_id,
            pwd: vm.pwd
        };
        vm.isFormSubmitting = true;
        vm.formError = undefined;

        auth.login(userloginData).then(function(resp) {
             console.log(resp)
                var res = resp.data;
                vm.isFormSubmitting = false;
                if (res.err) {
                    vm.formError = res.err;
                    if (res.firstTime) {
                        //if user is trying to login first time without verifying then show otp dialog
                        vm.shouldShowOTPDialog = true;
                    }
                    return;
                }

                if (res.firstTime) {
                    $window.location = "/changepassword/firsttime";
                } else {
                    checkAlreadyLogin();
                }
            }).catch(function(err) {
                vm.formError = "Something went wrong."
                vm.isFormSubmitting = false;
                console.log(err);
            })
            .finally(function() {
                vm.isFormSubmitting = false;
            })
    }

    function checkAlreadyLogin() {
        var cachedUrl = authToken.cachedUrl();
        if (cachedUrl && (cachedUrl.indexOf("/logout") > -1 || cachedUrl.indexOf("/login") > -1 || cachedUrl.indexOf("/register") > -1 || cachedUrl.indexOf("/forgotpassword") > -1 || cachedUrl.indexOf("/changepassword") > -1)) {
            if (!authToken.isExpired()) {
                $window.location.replace(utility.getDashboardUrl());
                // $window.location = utility.getDashboardUrl();
            }
            return;
        }
        if (authToken.cachedUrl() && !authToken.isExpired()) {
            console.log("cached", authToken.cachedUrl());
            $window.location.replace(authToken.cachedUrl());
            // $window.location.href = authToken.cachedUrl();
            return;
        }
        if (!authToken.isExpired()) {
            $window.location.replace(utility.getDashboardUrl());
            // $window.location = utility.getDashboardUrl();
        }
    }

    function authenticateWith(provider) {
        vm.fromChangePassword = undefined;
        vm.fromForgotPassword = undefined;
        vm.isFormSubmitting = true;
        $auth.authenticate(provider)
            .then(function(response) {
                console.log(response);
                authToken.setToken(response.data.token);
                authToken.setUser(response.data.user);
                checkAlreadyLogin();
            })
            .catch(function(response) {
                console.error(response);
                vm.formError = response.error;
            })
            .finally(function() {
                vm.isFormSubmitting = false;
            })
    }

    //this function will be called when user will click on verify
    function verifyOTP() {
        var d = {
            pwd: vm.otp,
            usr_id: vm.usr_id
        }
        vm.isFormSubmitting = true;
        vm.formError = false;
        auth.login(d)
            .then(function(d) {
                var data = d.data;
                if (!data.err) {
                    vm.formSuccess = "Successfully registered.";
                    vm.formError = false;
                    vm.successMsg = "Successfully registered.";
                    vm.otp = "";
                    //Redirect to authorize page
                    checkAlreadyLogin();
                } else {
                    vm.formError = "OTP doesn't match";
                    vm.formSuccess = undefined;
                    vm.errorMsg = data.err;
                }
            })
            .catch(function(d) {
                console.error(d);
                vm.formError = "Something went wrong.Please try again.";
                vm.errorMsg = "Something went wrong.Please try again.";
            })
            .finally(function(d) {
                vm.isFormSubmitting = false;
            })
    }

    function resendOTP() {
        var d = {
            usr_id: vm.usr_id
        }
        vm.isFormSubmitting = true;
        auth.resendOTP(d)
            .then(function(d) {
                var data = d.data;
                if (!data.err) {
                    vm.formSuccess = "OTP sent successfully";
                    vm.formError = undefined;
                    vm.successMsg = "OTP sent successfully";
                } else {
                    vm.formError = "Something went wrong.Please try again.";
                    vm.formSuccess = undefined;
                    vm.errorMsg = data.err;
                }
            })
            .catch(function(d) {
                console.error(d);
                vm.formError = "Something went wrong.Please try again.";
                vm.formSuccess = undefined;
                vm.errorMsg = "Something went wrong.Please try again.";
            })
            .finally(function(d) {
                vm.isFormSubmitting = false;
            })
    }
}