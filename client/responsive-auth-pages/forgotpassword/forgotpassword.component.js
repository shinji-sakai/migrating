require('authToken');
require('service.utility');
require('directive.headerMenu');
require('auth');
var Base64 = require('hash_js').Base64;
require("./_forgotpassword.scss")
angular.module(window.moduleName)
    .component('forgotPasswordPage', {
        template: ['$element', '$attrs',
            function($element, $attrs) {
                return require('./forgotpassword.html');
            }
        ],
        controller: ForgotPasswordController,
        controllerAs: 'vm'
    })

ForgotPasswordController.$inject = ['$location','$state', '$window', '$auth', '$stateParams', 'utility', 'auth', 'authToken'];

function ForgotPasswordController($location,$state, $window, $auth, $stateParams, utility, auth, authToken) {
    
    var vm = this;
    //////////////
    //Variables //
    //////////////
    vm.isUserExists = true;
    vm.userExistsError = undefined;
    vm.formError = false;
    vm.errorMsg = "";
    vm.formSuccess = false;
    vm.successMsg = "";

    //////////////
    //Functions //
    //////////////
    vm.forgotPassword = forgotPassword;
    vm.resendOTP = resendOTP;
    vm.verifyOTP = verifyOTP;

    vm.getLoginUrl = utility.getLoginUrl;
    vm.getRegisterUrl = utility.getRegisterUrl;
    vm.getForgotPasswordUrl = utility.getForgotPasswordUrl;

    vm.$onInit = function(){
        setTimeout(function() {
            $("#email").focus();
        })
    }

    function forgotPassword(form) {
        if(!form.$valid){
            if(!form.email.$valid){
                $("#email").focus();
            }
            return;
        }
        var randomPassword = Math.random().toString(36).substr(2, 5);
        var data = {
                usr_id: vm.usr_id
            }
        vm.isFormSubmitting = true;
        auth.forgotPassword(data)
            .then(function(res) {
                var d = res.data;
                if (!d.err) {
                    //If not error from prev call
                    vm.formSuccess = true;
                    vm.successMsg = "You will receive OTP in " + d.email + " / " + d.ph;
                    vm.formError = false;
                    vm.shouldShowOTPDialog = true;
                }else{
                    vm.formError = true;
                    vm.formSuccess = false;
                    vm.errorMsg = d.err;
                    vm.shouldShowOTPDialog = false;
                }
            })
            .catch(function(err) {
                console.error(err);
                vm.formError = true;
                vm.formSuccess = false;
                vm.shouldShowOTPDialog = false;
                vm.errorMsg = "Something went wrong, Please Try Again.";
            })
            .finally(function() {
                vm.isFormSubmitting = false;
            });
    }

    function verifyOTP() {
        var d = {
            otp: vm.otp,
            usr_id: vm.usr_id
        }
        vm.isFormSubmitting = true;
        auth.verifyForgotPasswordOTP(d)
            .then(function(d) {
                var data = d.data;
                if (!data.err) {
                    vm.formSuccess = true;
                    vm.formError = false;
                    vm.successMsg = "Successfull.";
                    vm.otp = "";

                    var obj = data;
                    obj.time = new Date();
                    var str = JSON.stringify(obj);
                    var bs_en = Base64.encode(str);

                    // utility.goToChangePassword(bs_en);
                    $state.go("changepassword",{q:bs_en});
                } else {
                    vm.formError = true;
                    vm.formSuccess = undefined;
                    vm.errorMsg = data.err;
                }
            })
            .catch(function(d) {
                console.error(d);
                vm.formError = true;
                vm.formSuccess = undefined;
                vm.errorMsg = "Something went wrong.Please try again.";
            })
            .finally(function(d) {
                vm.isFormSubmitting = false;
            })
        }

    function resendOTP() {
        var d = {
            usr_id: vm.usr_id
        }
        vm.isFormSubmitting = true;
        auth.resendForgotPasswordOTP(d)
            .then(function(d) {
                var data = d.data;
                if (!data.err) {
                    vm.formSuccess = true;
                    vm.formError = undefined;
                    vm.successMsg = "OTP sent successfully to " + data.email + ' / ' + data.ph;
                } else {
                    vm.formError = true;
                    vm.formSuccess = undefined;
                    vm.errorMsg = data.err;
                }
            })
            .catch(function(d) {
                console.error(d);
                vm.formError = true;
                vm.formSuccess = undefined;
                vm.errorMsg = "Something went wrong.Please try again.";
            })
            .finally(function(d) {
                vm.isFormSubmitting = false;
            })
        }
}