require('authToken');
require('service.utility');
require('directive.headerMenu');
require('auth');
require("./_register.scss")
angular.module(window.moduleName)
    .component('registerPage', {
        template: ['$element', '$attrs',
            function($element, $attrs) {
                return require('./register.html');
            }
        ],
        controller: RegisterController,
        controllerAs: 'vm'
    })
RegisterController.$inject = ['$scope', '$window', '$timeout', '$interval', '$auth','auth', 'authToken','utility'];

function RegisterController($scope, $window, $timeout, $interval, $auth,auth, authToken,utility) {
    
    var vm = this;
    //////////////
    //Variables //
    //////////////
    vm.isUserExists = false;
    vm.userExistsError = "";
    vm.formSuccess = false;
    vm.successMsg = "";
    vm.errorMsg = "";
    vm.formError = false;
    vm.usr_role = "student";
    vm.isPhoneValid = true;


    //////////////
    //Functions //
    //////////////
    
    vm.register = register;
    vm.checkUserExistance = checkUserExistance;
    vm.authenticateWith = authenticateWith;
    vm.verifyOTP = verifyOTP;
    vm.resendOTP = resendOTP;
    vm.getLoginUrl = utility.getLoginUrl;
    vm.getRegisterUrl = utility.getRegisterUrl;
    vm.getForgotPasswordUrl = utility.getForgotPasswordUrl;

    vm.$onInit = function(){
        checkAlreadyLogin();
        setTimeout(function() {
            $("#email").focus();
        })
        $scope.$watch(function(){
            return vm.usr_ph;
        },function(newValue, oldValue, scope) {
            vm.isPhoneValid = /^\d+$/.test(newValue);
        });
    }

    vm.checkUserRegex = function(){
        vm.usr_id = vm.usr_id || "";
        if(!(/^[a-zA-Z0-9]{4,}$/.test(vm.usr_id))){
            checkUserExistance();
            $("#userid").focus();
            vm.isUserNotValid = true;
            return;
        }else{
            checkUserExistance();
        }
        vm.isUserNotValid = false;

    }

    /**
     * checkUserExistance
     * To check whether user is exist or not
     */
    function checkUserExistance() {
        if (!vm.usr_id)
            return;
        var data = {
            usr_id: vm.usr_id
        }
        vm.isUserExists = false;
        return auth.checkUserExistance(data)
            .then(function(data) {
                if (data.userExists) {
                    vm.isUserExists = true;
                    vm.userExistsError = "User Already exists";
                }
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    function register(form) {
        if(vm.isUserExists){
            // vm.formError = true;
            // vm.formSuccess = false;
            // vm.errorMsg = "User is already exists.";
            return;
        }
        vm.usr_id = vm.usr_id || "";
        if(!(/^[a-zA-Z0-9]{4,}$/.test(vm.usr_id))){
            $("#userid").focus();
            vm.isUserNotValid = true;
            return;
        }
        vm.isUserNotValid = false;

        if (!form.$valid) {
            if(!form.email.$valid){
                $("#email").focus();
            }else if(!form.userid.$valid){
                $("#userid").focus();
            }else if(!form.dname.$valid){
                $("#dname").focus();
            }else if(!form.pwd.$valid){
                $("#pwd").focus();
            }
            return;
        }


        vm.isPhoneValid = /^\d+$/.test(vm.usr_ph);
        if(!vm.isPhoneValid){
            $("#phone").focus();
            return;
        }

        var userRegisterData = {
            usr_id: vm.usr_id,
            dsp_nm: vm.dsp_nm,
            usr_email: vm.usr_email,
            usr_ph: vm.usr_ph,
            pwd : vm.pwd,
            usr_role : vm.usr_role
        };

        // vm.usr_id = "";
        // vm.dsp_nm = "";
        // vm.usr_email = "";
        // vm.usr_ph = "";
        vm.usr_pwd = "";

        vm.isFormSubmitting = true;
        vm.shouldShowOTPDialog = false;
        
        auth.register(userRegisterData)
            .then(function(){
                vm.formError = false;
                vm.formSuccess = true;
                vm.counter = 5;
                vm.successMsg = "You will get OTP in " + userRegisterData.usr_email + "/" + vm.usr_ph; 
                vm.shouldShowOTPDialog = true;
                
            })
            .catch(function(err) {
                console.log(err);
                vm.formError = true;
                vm.formSuccess = false;
                vm.errorMsg = "Something went wrong.Please try again.";
            })
            .finally(function() {
                vm.isFormSubmitting = false;
            });
    }

    function verifyOTP(){
        var d = {
            pwd : vm.otp,
            usr_id : vm.usr_id,
            dsp_nm : vm.dsp_nm
        }
        vm.isFormSubmitting = true;
        auth.login(d)
            .then(function(d){
                var data = d.data;
                if(!data.err){
                    vm.formSuccess = true;
                    vm.formError = false;
                    vm.successMsg = "Successfully registered.";
                    vm.otp = "";
                    //Redirect to authorize page
                    checkAlreadyLogin();
                }else{
                    vm.formError = true;
                    vm.formSuccess = false;
                    vm.errorMsg = data.err;
                }
            })
            .catch(function(d){
                console.error(d);
                vm.formError = true;
                vm.errorMsg = "Something went wrong.Please try again.";
            })
            .finally(function(d){
                vm.isFormSubmitting = false;
            })
    }

    function resendOTP() {
        var d = {
            usr_id : vm.usr_id,
            usr_email : vm.usr_email
        }
        vm.isFormSubmitting = true;
        auth.resendOTP(d)
            .then(function(d){
                var data = d.data;
                if(!data.err){
                    vm.formSuccess = true;
                    vm.formError = false;
                    vm.successMsg = "OTP sent successfully to " + vm.usr_email;
                }else{
                    vm.formError = true;
                    vm.formSuccess = false;
                    vm.errorMsg = data.err;
                }
            })
            .catch(function(d){
                console.error(d);
                vm.formError = true;
                vm.formSuccess = false;
                vm.errorMsg = "Something went wrong.Please try again.";
            })
            .finally(function(d){
                vm.isFormSubmitting = false;
            })   
    }

    function checkAlreadyLogin() {
        var cachedUrl = authToken.cachedUrl();
        if(cachedUrl && (cachedUrl.indexOf("/logout") > -1 || cachedUrl.indexOf("/login") > -1 || cachedUrl.indexOf("/register") > -1 || cachedUrl.indexOf("/forgotpassword") > -1 || cachedUrl.indexOf("/changepassword") > -1)){
            if (!authToken.isExpired()) {
                $window.location = utility.getDashboardUrl();
            }
            return;
        }

        if (authToken.cachedUrl() && !authToken.isExpired()) {
            $window.location.href = authToken.cachedUrl();
            return;
        }
        if (!authToken.isExpired()) {
            $window.location = utility.getDashboardUrl();
        }
    }

    function authenticateWith(provider) {
        vm.isFormSubmitting = true;
        $auth.authenticate(provider)
            .then(function(response) {
                authToken.setToken(response.data.token);
                authToken.setUser(response.data.user);
                checkAlreadyLogin();
            })
            .catch(function(response) {
                console.error(response);
                vm.formError = response.error;
            })
            .finally(function() {
                vm.isFormSubmitting = false;
            })
    }
}